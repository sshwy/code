// https://projecteuler.net/problem=306
#include <algorithm>
#include <bitset>
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 1e5 + 5;

int f[N], vis[N];
int mex(bitset<N> x) {
  FOR(i, 0, N - 1) if (x[i] == 0) return i;
  return N;
}
int sg(int x) {
  if (x == 0) return 0;
  if (vis[x]) return f[x];
  bitset<N> bin;
  FOR(i, 0, x - 2) bin[sg(i) ^ sg(x - 2 - i)] = 1;
  vis[x] = 1, f[x] = mex(bin);
  return f[x];
}

int main() {
  FOR(i, 1, 5000) printf("%d", sg(i));
  return 0;
}
