// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

const int N = 1e5 + 5, TSZ = N * 48, SZ = N * 72;

template <class T> struct Treap {
  int tot;
  int lc[TSZ], rc[TSZ], sz[TSZ];
  unsigned int rnd[TSZ];
  T val[TSZ];
  Treap() {
    tot = 0;
    srand(clock() + time(0));
  }
  inline void pushup(int u) { sz[u] = sz[lc[u]] + sz[rc[u]] + 1; }
  void split_upper(int u, const T key, int &x, int &y) { // x:<=key; y:>key
    if (!u) return x = y = 0, void();
    if (val[u] <= key)
      x = u, split_upper(rc[u], key, rc[u], y);
    else
      y = u, split_upper(lc[u], key, x, lc[u]);
    pushup(u);
  }
  int merge(int x, int y) { // x<y
    if (!x || !y) return x + y;
    if (rnd[x] < rnd[y])
      return rc[x] = merge(rc[x], y), pushup(x), x;
    else
      return lc[y] = merge(x, lc[y]), pushup(y), y;
  }
  void insert(int &root, const T v) { // 插入 v
    int x, y, u = ++tot;
    val[u] = v, sz[u] = 1, rnd[u] = rand();
    split_upper(root, v, x, y);
    root = merge(merge(x, u), y);
  }
  int rank(int root, T v) {
    if (!root) return 0;
    if (val[root] >= v)
      return rank(lc[root], v);
    else
      return sz[lc[root]] + 1 + rank(rc[root], v);
  }
};
Treap<int> T;

int n, q, a[N], b[N], ans[N];

int lc[SZ], rc[SZ], tot, root;

int bs[SZ], cs[SZ];

struct Query {
  int c, d, coef, id;
};

vector<Query> qry[N];

void insert_1(int &u, int i, int dep = 23) {
  if (!u) u = ++tot;
  T.insert(bs[u], b[i]);
  if (dep == -1) { // leaf
    return;
  }
  if (!(a[i] >> dep & 1))
    insert_1(lc[u], i, dep - 1);
  else
    insert_1(rc[u], i, dep - 1);
}

int queryNode_1(int u, int d) {
  if (!u) return 0;
  return T.sz[bs[u]] - (T.rank(bs[u], d));
}
int query_1(int u, int c, int d, int dep = 23) {
  if (!u) return 0;
  if (dep == -1) { return queryNode_1(u, d); }
  if (!(c >> dep & 1)) {
    if (!(d >> dep & 1))
      return query_1(lc[u], c, d, dep - 1);
    else
      return queryNode_1(lc[u], d) + query_1(rc[u], c, d, dep - 1);
  } else {
    if (!(d >> dep & 1))
      return query_1(rc[u], c, d, dep - 1);
    else
      return queryNode_1(rc[u], d) + query_1(lc[u], c, d, dep - 1);
  }
}

bool try_insert(int &u, int i, int prefix, int dep) {
  int maxA = (a[i] ^ prefix) | ((1 << (dep + 1)) - 1);
  if (maxA <= b[i]) {
    // printf("maxA %d b %d dep %d\n", maxA, b[i], dep);
    if (!u) u = ++tot;
    T.insert(cs[u], b[i]);
    return true;
  }
  return false;
}
void insert_0(int &u, int i, int dep = 23) {
  if (!u) u = ++tot;
  if (dep == -1) { // leaf
    return;
  }
  int prefix = (((a[i] ^ b[i]) >> (dep + 1)) << (dep + 1));
  bool okl = try_insert(lc[u], i, prefix, dep - 1);
  bool okr = try_insert(rc[u], i, prefix + (1 << dep), dep - 1);
  if (!((a[i] ^ b[i]) >> dep & 1)) {
    if (!okl) insert_0(lc[u], i, dep - 1);
  } else {
    if (!okr) insert_0(rc[u], i, dep - 1);
  }
}
int queryNode_0(int u, int d) {
  if (!u) return 0;
  return T.rank(cs[u], d);
}
int query_0(int u, int c, int d, int dep = 23) {
  if (!u) return 0;
  int res = queryNode_0(u, d);
  if (dep == -1) { return res; }
  if (!(c >> dep & 1))
    res += query_0(lc[u], c, d, dep - 1);
  else
    res += query_0(rc[u], c, d, dep - 1);
  return res;
}

void add(int i) {
  // printf("add %d (a=%d, b=%d)\n", i, a[i], b[i]);
  insert_1(root, i);
  insert_0(root, i);
}

void calc(Query e) {
  int c1 = query_1(root, e.c, e.d);
  int c0 = query_0(root, e.c, e.d);
  // printf("calc c=%d d=%d id=%d coef=%d\n", e.c, e.d, e.id, e.coef);
  // printf("c1=%d c0=%d\n", c1, c0);
  ans[e.id] += e.coef * (c1 + c0);
}

void go() {
  FOR(i, 0, n) {
    if (i > 0) add(i);
    for (unsigned j = 0; j < qry[i].size(); j++) {
      Query e = qry[i][j];
      calc(e);
    }
  }
}

int main() {
  freopen("island.in", "r", stdin);
  freopen("island.out", "w", stdout);

  scanf("%d%d", &n, &q);
  FOR(i, 1, n) scanf("%d%d", a + i, b + i);

  FOR(i, 1, q) {
    int l, r, c, d;
    scanf("%d%d%d%d", &l, &r, &c, &d);
    qry[l - 1].pb((Query){c, d, -1, i});
    qry[r].pb((Query){c, d, 1, i});
  }

  go();

  FOR(i, 1, q) printf("%d\n", ans[i]);

  return 0;
}
