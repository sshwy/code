// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

const int SZ = 6005, ALP = 26;
struct SA {
  char s[SZ]; // SZ要开2倍，不然L24求rk的时候会越界
  int l;
  int sa[SZ], rk[SZ];
  int t[SZ], bin[SZ], sz;
  int h[SZ], he[SZ]; // h,height
  void qsort() {
    for (int i = 0; i <= sz; i++) bin[i] = 0;
    for (int i = 1; i <= l; i++) bin[rk[i]]++;
    for (int i = 1; i <= sz; i++) bin[i] += bin[i - 1];
    for (int i = l; i >= 1; i--) sa[bin[rk[t[i]]]--] = t[i];
  }
  void make(char *_s) {
    l = strlen(_s + 1), sz = max(l, ALP);
    FOR(i, 1, l) s[i] = _s[i];
    for (int i = 1; i <= l; i++) t[i] = i, rk[i] = s[i] - 'a' + 1;
    qsort();
    for (int j = 1; j <= l; j <<= 1) {
      int tot = 0;
      for (int i = l - j + 1; i <= l; i++) t[++tot] = i;
      for (int i = 1; i <= l; i++)
        if (sa[i] - j > 0) t[++tot] = sa[i] - j;
      qsort();
      t[sa[1]] = tot = 1;
      for (int i = 2; i <= l; i++)
        t[sa[i]] = rk[sa[i - 1]] == rk[sa[i]] && rk[sa[i - 1] + j] == rk[sa[i] + j]
                       ? tot
                       : ++tot;
      swap(t, rk);
    }
  }
  int move(int x, int y, int len) {
    while (x + len <= l && y + len <= l && s[x + len] == s[y + len]) ++len;
    return len;
  }
  void calc_h() {
    for (int i = 1; i <= l; i++)
      h[i] = rk[i] == 1 ? 0 : move(i, sa[rk[i] - 1], max(h[i - 1] - 1, 0));
  }
} sa;

const int N = 3005;

int n, nex[N][ALP], w[N][N];
bool ban[N][N];
char s[N], t[N];

void go() {
  FOR(i, 0, ALP - 1) nex[n + 1][i] = n + 1;
  ROF(i, n, 0) {
    FOR(j, 0, ALP - 1) {
      if (s[i + 1] - 'a' == j)
        nex[i][j] = i + 1;
      else
        nex[i][j] = nex[i + 1][j];
    }
  }
  FOR(i, 1, n) {
    int cur = 0;
    FOR(j, i, n) {
      cur = nex[cur][t[j] - 'a'];
      if (cur == n + 1) break;
      w[i][j]++;
    }
  }
  sa.make(t);
  sa.calc_h();
  // FOR(i, 1, n) printf("i %d sa %d rk %d h %d\n", i, sa.sa[i], sa.rk[i],
  // sa.h[i]);
  int ans = 0;
  FOR(i, 1, n) if (sa.h[i]) {
    FOR(k, 1, sa.h[i]) { ban[i][i + k - 1] = 1; }
  }
  FOR(i, 1, n) FOR(j, i, n) if (!ban[i][j]) ans += w[i][j];
  printf("%d\n", ans);
}

int main() {
  freopen("block.in", "r", stdin);
  freopen("block.out", "w", stdout);

  scanf("%d", &n);
  scanf("%s", s + 1);
  scanf("%s", t + 1);

  go();
  return 0;
}
