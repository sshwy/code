// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

const int N = 1e5 + 5;
int a[N], b[N], n, q;

int main() {
  scanf("%d%d", &n, &q);
  FOR(i, 1, n) scanf("%d%d", a + i, b + i);
  FOR(i, 1, q) {
    int l, r, c, d;
    scanf("%d%d%d%d", &l, &r, &c, &d);
    int ans = 0;
    FOR(j, l, r) if ((a[j] ^ c) <= min(b[j], d))++ ans;
    printf("%d\n", ans);
  }
  return 0;
}
