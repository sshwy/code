// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

const int N = 1e7, P = 1e9 + 7;

int n, k;
char sn[N];
int ans, a[N], pop[N];
int main() {
  freopen("angry.in", "r", stdin);
  freopen("angry.out", "w", stdout);
  scanf("%s", sn);
  int ls = strlen(sn);
  FOR(i, 0, ls - 1) n = n * 2 + (sn[i] - '0');
  scanf("%d", &k);
  FOR(i, 0, k - 1) scanf("%d", a + i);
  FOR(i, 0, n - 1) pop[i] = pop[i >> 1] + (i % 2);
  FOR(i, 0, n - 1) {
    if (pop[i] % 2) {
      long long coef = 1;
      FOR(j, 0, k - 1) {
        ans = (ans + coef * a[j] % P) % P;
        coef = coef * i % P;
      }
    }
  }
  printf("%d\n", ans);
  return 0;
}
