// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

using namespace RA;

int main() {
  srand(clock() + time(0));
  const int maxw = 15;
  int n = rnd(1, 100), q = rnd(1, 100);
  n = 7e4;
  q = 7e4;
  printf("%d %d\n", n, q);
  FOR(i, 1, n) printf("%d %d\n", rnd(1, maxw), rnd(1, maxw));
  FOR(i, 1, q) {
    int l = rnd(1, n), r = rnd(1, n), c = rnd(1, maxw), d = rnd(1, maxw);
    if (l > r) swap(l, r);
    printf("%d %d %d %d\n", l, r, c, d);
  }
  return 0;
}
