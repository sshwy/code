// by Sshwy
//#define DEBUGGER
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

#ifdef DEBUGGER

#define log_prefix()                                                \
  {                                                                 \
    fprintf(stderr, "\033[37mLine %-3d [%dms]:\033[0m  ", __LINE__, \
        (int)clock() / 1000);                                       \
  }
#define ilog(...) \
  { fprintf(stderr, __VA_ARGS__); }
#define llog(...)      \
  {                    \
    log_prefix();      \
    ilog(__VA_ARGS__); \
  }
#define log(...)           \
  {                        \
    log_prefix();          \
    ilog(__VA_ARGS__);     \
    fprintf(stderr, "\n"); \
  }
#define red(...)       \
  {                    \
    log_prefix();      \
    ilog("\033[31m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define green(...)     \
  {                    \
    log_prefix();      \
    ilog("\033[32m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define blue(...)      \
  {                    \
    log_prefix();      \
    ilog("\033[34m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }

#else

#define log_prefix() ;
#define ilog(...) ;
#define llog(...) ;
#define log(...) ;
#define red(...) ;
#define green(...) ;
#define blue(...) ;

#endif

namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 1e6 + 5, SZ = N << 2, P = 1e9 + 7;
int n, a[N], nex[N], prv[N], c[N];
map<int, int> mp;

int s0[SZ], s1[SZ], s2[SZ], tag[SZ];
void pushup(int u) {
  s0[u] = (s0[u << 1] + s0[u << 1 | 1]) % P;
  s1[u] = (s1[u << 1] + s1[u << 1 | 1]) % P;
  s2[u] = (s2[u << 1] + s2[u << 1 | 1]) % P;
}
void build(int u = 1, int l = 1, int r = n) {
  if (l == r) return s0[u] = 1, s1[u] = c[l], s2[u] = c[l] * 1ll * c[l] % P, void();
  int mid = (l + r) >> 1;
  build(u << 1, l, mid), build(u << 1 | 1, mid + 1, r);
  pushup(u);
}
void nodeadd(int u, int v) {
  s2[u] = (s2[u] + 2ll * s1[u] * v + s0[u] * 1ll * v % P * v) % P;
  s1[u] = (s1[u] + s0[u] * 1ll * v) % P;
  tag[u] += v;
}
void sub1(int L, int R, int u = 1, int l = 1, int r = n) {
  if (L <= l && r <= R) return nodeadd(u, -1), void();
  int mid = (l + r) >> 1;
  if (tag[u]) nodeadd(u << 1, tag[u]), nodeadd(u << 1 | 1, tag[u]), tag[u] = 0;
  if (L <= mid) sub1(L, R, u << 1, l, mid);
  if (mid < R) sub1(L, R, u << 1 | 1, mid + 1, r);
  pushup(u);
}
int main() {
  freopen("sequence.in", "r", stdin);
  freopen("sequence.out", "w", stdout);
  scanf("%d", &n);
  FOR(i, 1, n) { scanf("%d", &a[i]); }
  ROF(i, n, 1) {
    if (mp.count(a[i])) {
      nex[i] = mp[a[i]];
      prv[nex[i]] = i;
    }
    mp[a[i]] = i;
  }
  FOR(i, 1, n) if (prv[i] == 0) {
    c[i]++;
    c[n + 1]--;
  }
  FOR(i, 1, n) c[i] += c[i - 1];
  build();
  int ans = s2[1];

  FOR(i, 2, n) {
    int pos = nex[i - 1] == 0 ? n : nex[i - 1] - 1;
    sub1(i - 1, pos);
    ans = (ans + s2[1]) % P;
  }
  ans = (ans + P) % P;
  printf("%d\n", ans);
  return 0;
}
