// by Sshwy
//#define DEBUGGER
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

#ifdef DEBUGGER

#define log_prefix()                                                \
  {                                                                 \
    fprintf(stderr, "\033[37mLine %-3d [%dms]:\033[0m  ", __LINE__, \
        (int)clock() / 1000);                                       \
  }
#define ilog(...) \
  { fprintf(stderr, __VA_ARGS__); }
#define llog(...)      \
  {                    \
    log_prefix();      \
    ilog(__VA_ARGS__); \
  }
#define log(...)           \
  {                        \
    log_prefix();          \
    ilog(__VA_ARGS__);     \
    fprintf(stderr, "\n"); \
  }
#define red(...)       \
  {                    \
    log_prefix();      \
    ilog("\033[31m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define green(...)     \
  {                    \
    log_prefix();      \
    ilog("\033[32m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define blue(...)      \
  {                    \
    log_prefix();      \
    ilog("\033[34m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }

#else

#define log_prefix() ;
#define ilog(...) ;
#define llog(...) ;
#define log(...) ;
#define red(...) ;
#define green(...) ;
#define blue(...) ;

#endif

namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 5005, P = 998244353;
int n;
int c[N][N], fac[N], sz[N], cnt0[N], f[N][N],
    t[N]; // f[i,j]: i的子树内选j对祖孙匹配的方案数
int F[N], G[N];
char s[N];
vector<int> g[N];

void add(int &x, long long y) { x = (x + y) % P; }
void dfs(int u, int p) {
  for (int v : g[u])
    if (v != p) dfs(v, u);
  f[u][0] = 1;
  sz[u] = 1;
  cnt0[u] = s[u] == '0';
  for (int v : g[u])
    if (v != p) {
      fill(t, t + sz[u] + sz[v] + 1, 0);
      FOR(i, 0, sz[u]) {
        FOR(j, 0, sz[v]) {
          if (f[u][i] && f[v][j]) { add(t[i + j], f[u][i] * 1ll * f[v][j]); }
        }
      }
      FOR(i, 0, sz[u] + sz[v]) f[u][i] = t[i];
      sz[u] += sz[v];
      cnt0[u] += cnt0[v];
    }
  if (s[u] == '0') { // match with 1
    ROF(i, sz[u], 1) if (f[u][i - 1]) {
      long long coef = sz[u] - cnt0[u] - (i - 1);
      if (coef > 0) add(f[u][i], f[u][i - 1] * coef);
    }
  } else {
    ROF(i, sz[u], 1) if (f[u][i - 1]) {
      long long coef = cnt0[u] - (i - 1);
      if (coef > 0) add(f[u][i], f[u][i - 1] * coef);
    }
  }
  // printf("f[%d]: ",u);
  // FOR(i,0,sz[u])printf("%d%c",f[u][i]," \n"[i==sz[u]]);
}
int main() {
  freopen("match.in", "r", stdin);
  freopen("match.out", "w", stdout);
  scanf("%d", &n);
  scanf("%s", s + 1);
  FOR(i, 1, n - 1) {
    int u, v;
    scanf("%d%d", &u, &v);
    g[u].pb(v);
    g[v].pb(u);
  }
  dfs(1, 0);

  fac[0] = 1;
  FOR(i, 1, n) fac[i] = fac[i - 1] * 1ll * i % P;

  int m = n / 2;
  FOR(i, 0, n) F[i] = f[1][i];
  FOR(i, 0, n) F[i] = F[i] * 1ll * fac[m - i] % P;
  // FOR(i,0,n)printf("%d%c",F[i]," \n"[i==n]);

  c[0][0] = 1;
  FOR(i, 1, n) {
    c[i][0] = 1;
    FOR(j, 1, i) c[i][j] = (c[i - 1][j - 1] + c[i - 1][j]) % P;
  }
  FOR(i, 0, m) {
    int sig = 1;
    FOR(j, i, m) {
      long long coef = sig * c[j][i];
      G[i] = (G[i] + coef * F[j]) % P;
      sig = -sig;
    }
    G[i] = (G[i] + P) % P;
  }
  // int S=0,X=0;
  // FOR(i,0,m)S=(S+G[i])%P,X^=(G[i]+i);
  FOR(i, 0, m) printf("%d\n", G[i]);
  // fprintf(stderr,"Sum mod P = %d Xor(a[i]+i) = %d\n",S,X);
  return 0;
}
