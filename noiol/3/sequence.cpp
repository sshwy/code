// by Sshwy
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA
const int W = 1 << 18, N = 18, P = 1e9 + 7;
int n;
int c[W + 5], f[W + 5];
bool co[W + 5];
int pn[W + 5], lp;
int phi[W + 5];
void init() {
  co[0] = co[1] = 1;
  phi[1] = 1;
  FOR(i, 2, W) {
    if (co[i] == 0) pn[++lp] = i, phi[i] = i - 1;
    FOR(j, 1, lp) {
      if (i * pn[j] > W) break;
      co[i * pn[j]] = 1;
      if (i % pn[j] == 0) {
        phi[i * pn[j]] = phi[i] * pn[j];
        break;
      } else {
        phi[i * pn[j]] = phi[i] * phi[pn[j]];
      }
    }
  }
}
void add(int &x, int y) { x = (x + y) % P; }
void add(int &x, long long y) { x = (x + y) % P; }
int pw(int a, int m) {
  int res = 1;
  while (m) m & 1 ? res = 1ll * res * a % P : 0, a = 1ll * a * a % P, m >>= 1;
  return res;
}
int main() {
  freopen("sequence.in", "r", stdin);
  freopen("sequence.out", "w", stdout);
  scanf("%d", &n);
  init();
  FOR(i, 1, n) {
    int x;
    scanf("%d", &x);
    ++c[x];
  }

  f[0] = pw(2, c[0]);
  FOR(i, 1, W - 1) {
    int x = N - 1;
    while (!(i >> x & 1)) --x;
    int y = 1 << x;
    for (int j = i - y; j; j = (j - 1) & (i - y)) {
      if (c[j + y]) add(f[i], f[i - j - y] * 1ll * c[j + y]);
    }
    if (c[y]) add(f[i], f[i - y] * 1ll * c[y]);
  }
  int ans = f[0];
  // printf("f[0]=%d\n",f[0]);
  FOR(i, 1, W - 1) {
    if (f[i]) {
      // printf("f[%d]=%d\n",i,f[i]);
      add(ans, f[i] * 1ll * phi[i + 1]);
    }
  }
  add(ans, P);
  printf("%d\n", ans);
  return 0;
}
