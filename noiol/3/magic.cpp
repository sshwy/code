// by Sshwy
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA
const int MT = 105;
struct Matrix {
  int h, w;
  int c[MT][MT];
  Matrix() { h = w = 0; }
  Matrix(int _h, int _w) {
    h = _h, w = _w;
    FOR(i, 0, h - 1) fill(c[i], c[i] + w, 0);
  }
  Matrix(int _h, int _w, int _c[MT][MT]) {
    h = _h, w = _w;
    FOR(i, 0, h - 1) FOR(j, 0, w - 1) c[i][j] = _c[i][j];
  }
};
Matrix operator*(Matrix a, Matrix b) {
  assert(a.w == b.h);
  Matrix res(a.h, b.w);
  FOR(i, 0, a.h - 1) FOR(k, 0, a.w - 1) FOR(j, 0, b.w - 1) {
    res.c[i][j] ^= a.c[i][k] * b.c[k][j];
  }
  return res;
}
const int N = 105;
int n, m, q;
long long f[N], g[N], t[N];
int w[MT][MT];
Matrix pe[35];

long long qry(long long a) {
  memcpy(g, f, sizeof(f));
  ROF(x, 31, 0) {
    if (a >> x & 1) {
      FOR(i, 0, n - 1) {
        t[i] = 0;
        FOR(j, 0, n - 1) { t[i] ^= pe[x].c[i][j] * g[j]; }
      }
      memcpy(g, t, sizeof(t));
    }
  }
  return g[0];
}
int main() {
  freopen("magic.in", "r", stdin);
  freopen("magic.out", "w", stdout);
  scanf("%d%d%d", &n, &m, &q);
  FOR(i, 0, n - 1) scanf("%lld", &f[i]);
  FOR(i, 1, m) {
    int u, v;
    scanf("%d%d", &u, &v);
    --u, --v;
    w[u][v] = w[v][u] = 1;
  }
  Matrix e(n, n, w);
  pe[0] = e;
  FOR(i, 1, 31) pe[i] = pe[i - 1] * pe[i - 1];
  FOR(i, 1, q) {
    long long a;
    scanf("%lld", &a);
    printf("%lld\n", qry(a));
  }
  return 0;
}
