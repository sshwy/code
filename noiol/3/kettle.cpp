// by Sshwy
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA
const int N = 1e6 + 5;
int n, k;
int a[N];

int main() {
  freopen("kettle.in", "r", stdin);
  freopen("kettle.out", "w", stdout);
  scanf("%d%d", &n, &k);
  FOR(i, 1, n) scanf("%d", &a[i]), a[i] += a[i - 1];
  ++k;
  int ans = 0;
  FOR(i, k, n) ans = max(ans, a[i] - a[i - k]);
  printf("%d\n", ans);
  return 0;
}
