// by Sshwy
//#define DEBUGGER
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

#ifdef DEBUGGER

#define log_prefix()                                                \
  {                                                                 \
    fprintf(stderr, "\033[37mLine %-3d [%dms]:\033[0m  ", __LINE__, \
        (int)clock() / 1000);                                       \
  }
#define ilog(...) \
  { fprintf(stderr, __VA_ARGS__); }
#define llog(...)      \
  {                    \
    log_prefix();      \
    ilog(__VA_ARGS__); \
  }
#define log(...)           \
  {                        \
    log_prefix();          \
    ilog(__VA_ARGS__);     \
    fprintf(stderr, "\n"); \
  }
#define red(...)       \
  {                    \
    log_prefix();      \
    ilog("\033[31m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define green(...)     \
  {                    \
    log_prefix();      \
    ilog("\033[32m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define blue(...)      \
  {                    \
    log_prefix();      \
    ilog("\033[34m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }

#else

#define log_prefix() ;
#define ilog(...) ;
#define llog(...) ;
#define log(...) ;
#define red(...) ;
#define green(...) ;
#define blue(...) ;

#endif

namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 2e5 + 5;
typedef long long LL;
int n, m;
int a[N], p[N];
LL ans[N];

int gcd(int a, int b) { return b ? gcd(b, a % b) : a; }

int main() {
  freopen("ring.in", "r", stdin);
  freopen("ring.out", "w", stdout);
  scanf("%d%d", &n, &m);
  FOR(i, 1, n) scanf("%d", &a[i]);
  sort(a + 1, a + n + 1);

  FOR(i, 1, n) p[i] = i;
  FOR(i, 1, n) ans[0] += a[i] * 1ll * a[i];
  do {
    FOR(k, 1, n / 2) {
      int g = gcd(k, n);
      int len = n / g;
      LL s = 0;
      FOR(i, 0, g - 1) {
        FOR(j, 0, len - 1) {
          s += a[p[i * len + j + 1]] * 1ll * a[p[i * len + (j + 1) % len + 1]];
        }
      }
      ans[k] = max(ans[k], s);
    }
  } while (next_permutation(p + 1, p + n + 1));

  FOR(i, 1, m) {
    int k;
    scanf("%d", &k);
    printf("%lld\n", ans[k]);
  }
  return 0;
}
