// by Sshwy
//#define DEBUGGER
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

#ifdef DEBUGGER

#define log_prefix()                                                \
  {                                                                 \
    fprintf(stderr, "\033[37mLine %-3d [%dms]:\033[0m  ", __LINE__, \
        (int)clock() / 1000);                                       \
  }
#define ilog(...) \
  { fprintf(stderr, __VA_ARGS__); }
#define llog(...)      \
  {                    \
    log_prefix();      \
    ilog(__VA_ARGS__); \
  }
#define log(...)           \
  {                        \
    log_prefix();          \
    ilog(__VA_ARGS__);     \
    fprintf(stderr, "\n"); \
  }
#define red(...)       \
  {                    \
    log_prefix();      \
    ilog("\033[31m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define green(...)     \
  {                    \
    log_prefix();      \
    ilog("\033[32m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define blue(...)      \
  {                    \
    log_prefix();      \
    ilog("\033[34m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }

#else

#define log_prefix() ;
#define ilog(...) ;
#define llog(...) ;
#define log(...) ;
#define red(...) ;
#define green(...) ;
#define blue(...) ;

#endif

namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 2e5 + 5;
typedef long long LL;
int n, m;
int a[N], p[N];
LL ans[N];

int gcd(int a, int b) { return b ? gcd(b, a % b) : a; }

int t[N], t2[N], l2;
void calc(int g) {
  LL s = 0;
  int len = n / g;
  FOR(i, 0, g - 1) {
    FOR(j, 0, len - 1) t[j] = a[i * len + j + 1];
    l2 = 0;
    for (int j = 0; j < len; j += 2) t2[l2++] = t[j];
    for (int j = ((len - 1) % 2) ? len - 1 : len - 2; j > 0; j -= 2) t2[l2++] = t[j];
    assert(l2 == len);
    FOR(j, 0, len - 1) {
      // printf("%d%c",t2[j]," \n"[j==len-1]);
      s += t2[j] * 1ll * t2[(j + 1) % len];
    }
  }
  ans[g] = s;
  // printf("ans[%d]=%lld\n",g,ans[g]);
}
int main() {
  freopen("ring.in", "r", stdin);
  freopen("ring.out", "w", stdout);
  scanf("%d%d", &n, &m);
  FOR(i, 1, n) scanf("%d", &a[i]);
  sort(a + 1, a + n + 1);

  FOR(i, 1, n) ans[0] += a[i] * 1ll * a[i];

  FOR(k, 1, n) if (n % k == 0) calc(k);

  // LL XOR=0;

  FOR(i, 1, m) {
    int k;
    scanf("%d", &k);
    if (k == 0)
      printf("%lld\n", ans[k]); //,XOR^=ans[k];
    else
      printf("%lld\n", ans[gcd(n, k)]); //,XOR^=ans[gcd(n,k)];
  }
  // fprintf(stderr,"xor=%lld",XOR);
  return 0;
}
