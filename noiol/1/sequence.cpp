// by Sshwy
//#define DEBUGGER
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

#ifdef DEBUGGER

#define log_prefix()                                                \
  {                                                                 \
    fprintf(stderr, "\033[37mLine %-3d [%dms]:\033[0m  ", __LINE__, \
        (int)clock() / 1000);                                       \
  }
#define ilog(...) \
  { fprintf(stderr, __VA_ARGS__); }
#define llog(...)      \
  {                    \
    log_prefix();      \
    ilog(__VA_ARGS__); \
  }
#define log(...)           \
  {                        \
    log_prefix();          \
    ilog(__VA_ARGS__);     \
    fprintf(stderr, "\n"); \
  }
#define red(...)       \
  {                    \
    log_prefix();      \
    ilog("\033[31m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define green(...)     \
  {                    \
    log_prefix();      \
    ilog("\033[32m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define blue(...)      \
  {                    \
    log_prefix();      \
    ilog("\033[34m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }

#else

#define log_prefix() ;
#define ilog(...) ;
#define llog(...) ;
#define log(...) ;
#define red(...) ;
#define green(...) ;
#define blue(...) ;

#endif

namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 2e5 + 5, M = N * 2;

struct qxx {
  int nex, t;
} e[M];
int h[N], le = 1;
void add_path(int f, int t) { e[++le] = (qxx){h[f], t}, h[f] = le; }
void add_both(int f, int t) { add_path(f, t), add_path(t, f); }

int n, m;
int a[N], b[N];
int U[N], V[N], W[N];
long long c[N];

int fa[N];
int get(int u) { return fa[u] == u ? u : fa[u] = get(fa[u]); }
void merge(int u, int v) { fa[get(u)] = get(v); }

int vis[N];
bool dfs(int u, int col, long long &s1, long long &s2, long long &s) {
  if (vis[u]) {
    if (vis[u] != col) return 1;
    return 0;
  }
  vis[u] = col;
  if (col == 1)
    s1 += c[u];
  else
    s2 += c[u];
  s += c[u];
  bool res = 0;
  for (int i = h[u]; i; i = e[i].nex) {
    const int v = e[i].t;
    res |= dfs(v, col == 1 ? 2 : 1, s1, s2, s);
  }
  return res;
}

void go() {
  scanf("%d%d", &n, &m);
  FOR(i, 1, n) scanf("%d", &a[i]);
  FOR(i, 1, n) scanf("%d", &b[i]);
  FOR(i, 1, n) c[i] = b[i] - a[i];

  FOR(i, 0, n) h[i] = 0;
  le = 1;

  FOR(i, 1, m) {
    int t, u, v;
    scanf("%d%d%d", &t, &u, &v);
    U[i] = u, V[i] = v, W[i] = t;
  }
  FOR(i, 0, n) fa[i] = i;
  FOR(i, 1, m) if (W[i] == 2) merge(U[i], V[i]);
  FOR(u, 1, n) if (get(u) != u) c[get(u)] += c[u], c[u] = 0;

  // FOR(i,1,n)printf("%d%c",c[i]," \n"[i==n]);

  FOR(i, 1, m) add_both(get(U[i]), get(V[i]));

  FOR(i, 0, n) vis[i] = 0;
  FOR(i, 1, n) {
    if (!vis[i]) {
      long long x = 0, y = 0, z = 0;
      if (dfs(i, 1, x, y, z)) { //非二分图
        if (z & 1) {
          puts("NO");
          return;
        }
      } else {
        if (x != y) {
          puts("NO");
          return;
        }
      }
    }
  }
  puts("YES");
}
int main() {
  freopen("sequence.in", "r", stdin);
  freopen("sequence.out", "w", stdout);
  int t;
  scanf("%d", &t);
  FOR(i, 1, t) go();
  return 0;
}
// 2: 总和不变
// 1: 改变总和，总和的奇偶性不变
// 1的连通块：总和不变，点值随便变（生成树构造法）
//因此2可以缩点
//于是只剩下1构成的图
//问题：只有1操作，问能否把点权（c）全部变成0
//而u -1- v -1- w => u -2- w
//反向不一定成立（
//
//二分图：左部和右部中间连1
//非二分图：缩成一个点。但不能直接缩，因为这次缩的点里含有1，和上次的不大一样
//
//二分图：
//  意味着我们可以让左（右）部结点的点权随意变化，只要总和不变。
//  我们要让两边的所有点权都变成0
//  等价于，两边的点权和都变成0
//  而一个1只能同加同减
//  因此如果两边点权不同则GG
//
//非二分图：
//  意味着我们可以让整个连通块点权随意变化只要总和不变。
//  而一个1可以+2或-2
//  因此只要点权总和为偶数即可。
