// by Sshwy
//#define DEBUGGER
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

#ifdef DEBUGGER

#define log_prefix()                                                \
  {                                                                 \
    fprintf(stderr, "\033[37mLine %-3d [%dms]:\033[0m  ", __LINE__, \
        (int)clock() / 1000);                                       \
  }
#define ilog(...) \
  { fprintf(stderr, __VA_ARGS__); }
#define llog(...)      \
  {                    \
    log_prefix();      \
    ilog(__VA_ARGS__); \
  }
#define log(...)           \
  {                        \
    log_prefix();          \
    ilog(__VA_ARGS__);     \
    fprintf(stderr, "\n"); \
  }
#define red(...)       \
  {                    \
    log_prefix();      \
    ilog("\033[31m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define green(...)     \
  {                    \
    log_prefix();      \
    ilog("\033[32m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define blue(...)      \
  {                    \
    log_prefix();      \
    ilog("\033[34m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }

#else

#define log_prefix() ;
#define ilog(...) ;
#define llog(...) ;
#define log(...) ;
#define red(...) ;
#define green(...) ;
#define blue(...) ;

#endif

namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 2e5 + 5;
typedef long long LL;
int n, m;
int p[N];
int a[N], b[N];

LL c[N], d0[N], d1[N], d0_sum;
void add(LL *f, int pos, LL v) {
  for (int i = pos; i <= n + 1; i += i & -i) f[i] += v;
}
LL pre(LL *f, int pos) {
  LL res = 0;
  for (int i = pos; i > 0; i -= i & -i) res += f[i];
  return res;
}

void d_add(int x) {
  add(d0, x + 1, 1);
  add(d1, x + 1, x);
  ++d0_sum;
}
void d_rem(int x) {
  add(d0, x + 1, -1);
  add(d1, x + 1, -x);
  --d0_sum;
}
LL nn2;
// LL sum=0,XOR=0;
void modify(int pos) {
  swap(p[pos], p[pos + 1]);
  swap(b[pos], b[pos + 1]);
  if (p[pos] > p[pos + 1]) {
    ++nn2;
    d_rem(b[pos + 1]);
    b[pos + 1]++;
    d_add(b[pos + 1]);
  } else {
    --nn2;
    d_rem(b[pos]);
    b[pos]--;
    d_add(b[pos]);
  }
}
LL query(int k) {
  LL x = pre(d0, k), y = pre(d1, k);
  LL t = (d0_sum - x) * k + y;
  // sum+=nn2-t; XOR^=nn2-t;
  return nn2 - t;
}
int main() {
  freopen("bubble.in", "r", stdin);
  freopen("bubble.out", "w", stdout);
  scanf("%d%d", &n, &m);
  FOR(i, 1, n) scanf("%d", &p[i]);
  FOR(i, 1, n) {
    b[i] = i - 1 - pre(c, p[i]); //在i之前比p[i]大的数
    nn2 += b[i];
    add(c, p[i], 1);
  }
  // FOR(i,1,n)printf("%d%c",b[i]," \n"[i==n]);
  FOR(i, 1, n) d_add(b[i]);
  FOR(i, 1, m) {
    int x, y;
    scanf("%d%d", &x, &y);
    if (x == 1)
      modify(y);
    else
      printf("%lld\n", query(min(n - 1, y)));
  }
  // fprintf(stderr,"sum=%lld,xor=%lld\n",sum,XOR);
  return 0;
}
