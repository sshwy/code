#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/

const int N = 2e5 + 5;

int n;
int a[N];

map<int, int> c;
bool check(int k) {
  int len = 0;
  c.clear();
  FOR(i, 1, n) {
    if (a[i] <= len) {
      int pos = a[i];
      if (k == 1) return 0;
      while (pos && c.count(pos) && c[pos] == k - 1)
        --pos; // BUG#1:没有判count导致添加了许多没有的值，复杂度就不对了
      if (pos == 0) return 0;
      c[pos]++;
      map<int, int>::iterator it = c.find(pos);
      c.erase(++it, c.end());
    }
    len = a[i];
  }
  return 1;
}
int main() {
  scanf("%d", &n);
  FOR(i, 1, n) scanf("%d", &a[i]);
  int l = 1, r = n + 1, mid;
  while (l < r) mid = (l + r) >> 1, check(mid) ? r = mid : l = mid + 1;
  printf("%d\n", l);
  return 0;
}
