#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/

int n, k;

namespace BIN {
  int A[22][1 << 21];
}
int &f(int len, int status, int split) {
  assert(split <= len);
  assert(status < (1 << len));
  return BIN::A[split][status | (1 << len)];
}

char P[100];
char *print(int len, int status) {
  int l = 0;
  ROF(i, len - 1, 0) P[l++] = (status >> i & 1) + '0';
  P[l] = 0;
  return P;
}
void build() {
  ROF(i, n, 1) { // len
    int lim = 1 << i;
    FOR(j, 0, lim - 1) {
      // int last0=-1,last1=-1;
      int last0[25], last1[25];
      last0[0] = last1[0] = -1;
      FOR(k, 0, i) {
        last0[k + 1] = last0[k];
        last1[k + 1] = last1[k];
        if (j >> k & 1)
          last1[k + 1] = k;
        else
          last0[k + 1] = k;
      }
      ROF(k, i,
          0) { //中括号开始前的那个字符，从低位往高位枚举，实际上是倒着枚举
        if (!f(i, j, k)) continue;
        if (k > 0) { // then link to s[]
          f(i - k, j >> k, 0) += f(i, j, k);
        }
        if (last0[k] != -1) { // then link to s0[t']
          int i2 = i - (k - last0[k] - 1),
              j2 = ((j >> (k - last0[k] - 1)) & ((~0) << (last0[k] + 1))) |
                   (j & ((1 << (last0[k])) - 1)),
              k2 = last0[k];
          f(i2, j2, k2) += f(i, j, k);
        }
        if (last1[k] != -1) { // then link to s1[t']
          int i2 = i - (k - last1[k] - 1),
              j2 = ((j >> (k - last1[k] - 1)) & ((~0) << (last1[k] + 1))) |
                   (1 << (last1[k])) | (j & ((1 << (last1[k])) - 1)),
              k2 = last1[k];
          f(i2, j2, k2) += f(i, j, k);
        }
      }
    }
  }
}
char x[1 << 21];
int main() {
  scanf("%d%d", &n, &k);
  FOR(i, 0, n) {
    scanf("%s", x);
    int lim = 1 << i;
    FOR(j, 0, lim - 1) {
      if (x[j] == '1') { f(i, j, i)++; }
    }
  }
  build();
  ROF(i, n, 0) {
    int lim = 1 << i;
    FOR(j, 0, lim - 1) {
      if (f(i, j, 0) >= k) {
        ROF(l, i - 1, 0) printf("%d", j >> l & 1);
        return 0;
      }
    }
  }
  puts("ERROR!");
  exit(1);
  return 0;
}
