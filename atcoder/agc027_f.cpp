#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 100;

int n;
int ga[N][N], gb[N][N];
int col[N], d[N];

vector<int> glim[N];
void add_lim(int x, int y) {
  glim[x].pb(y);
  d[y]++;
}

void dfs(int u, int p) {
  col[u] = 1; //不动的点
  FOR(v, 1, n) if (ga[u][v] && gb[u][v] && v != p) dfs(v, u);
}
void dfsa(int u, int p) {
  FOR(v, 1, n) if (ga[u][v] && v != p) {
    dfsa(v, u);
    if (!col[u] && !col[v]) { add_lim(v, u); }
  }
}
void dfsb(int u, int p) {
  FOR(v, 1, n) if (gb[u][v] && v != p) {
    dfsb(v, u);
    if (!col[u] && !col[v]) { add_lim(u, v); }
  }
}
int vis[N], tim;
bool dfs_lim(int u) {
  if (vis[u] == tim) return 1;
  vis[u] = tim;
  for (int v : glim[u])
    if (dfs_lim(v)) return 1;
  return 0;
}
int get_ans(int r) {
  memset(col, 0, sizeof(col));
  dfs(r, 0);
  int res = 0;
  FOR(i, 1, n) res += !col[i];
  FOR(i, 1, n) glim[i].clear();
  memset(d, 0, sizeof(d));
  dfsa(r, 0), dfsb(r, 0);
  queue<int> q;
  FOR(i, 1, n) if (d[i] == 0) q.push(i);
  while (!q.empty()) {
    int u = q.front();
    q.pop();
    for (int v : glim[u]) {
      d[v]--;
      if (d[v] == 0) q.push(v);
    }
  }
  FOR(i, 1, n) if (d[i]) return -1;
  return res;
}
void go() {
  scanf("%d", &n);
  memset(ga, 0, sizeof(ga));
  memset(gb, 0, sizeof(gb));
  FOR(i, 1, n - 1) {
    int a, b;
    scanf("%d%d", &a, &b);
    ga[a][b] = ga[b][a] = 1;
  }
  FOR(i, 1, n - 1) {
    int a, b;
    scanf("%d%d", &a, &b);
    gb[a][b] = gb[b][a] = 1;
  }
  int ans = 0x3f3f3f3f;
  FOR(i, 1, n) {
    int t = get_ans(i);
    if (t != -1) ans = min(ans, t);
  }
  FOR(i, 1, n) {
    int dg = 0, p = 0;
    FOR(j, 1, n) if (ga[i][j]) p = j, ++dg;
    if (dg > 1) continue;
    ga[i][p] = ga[p][i] = 0;
    FOR(j, 1, n) {
      if (i == j) continue;
      ga[i][j] = ga[j][i] = 1;
      int t = get_ans(i);
      if (t != -1) ans = min(ans, t + 1);
      ga[i][j] = ga[j][i] = 0;
    }
    ga[i][p] = ga[p][i] = 1;
  }
  printf("%d\n", ans == 0x3f3f3f3f ? -1 : ans);
}
int main() {
  int t;
  scanf("%d", &t);
  FOR(i, 1, t) go();
  return 0;
}
