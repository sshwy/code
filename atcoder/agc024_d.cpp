#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 105;

int n;
vector<int> g[N];
long long a1 = 1e18, a2 = 1e18;

int d[N][N];
void dfs(int u, int p, int tag, int dep = 1) {
  int cnt = 0;
  for (int v : g[u])
    if (v != p) dfs(v, u, tag, dep + 1), cnt++;
  d[tag][dep] = max(d[tag][dep], cnt);
}
int dg[N];
int main() {
  scanf("%d", &n);
  FOR(i, 2, n) {
    int a, b;
    scanf("%d%d", &a, &b);
    g[a].pb(b);
    g[b].pb(a);
    dg[a]++, dg[b]++;
  }
  FOR(u, 1, n) {
    for (int v : g[u]) {
      // printf("u=%d,v=%d\n",u,v);
      FOR(i, 1, n) d[1][i] = 0;
      FOR(i, 1, n) d[2][i] = 0;
      dfs(u, v, 1);
      dfs(v, u, 2);
      long long x = 1, cnt = 0;
      FOR(i, 1, n) {
        // printf("d[%d,%d]=%d\n",1,i,d[1][i]);
        // printf("d[%d,%d]=%d\n",2,i,d[2][i]);
        long long t = max(d[1][i], d[2][i]);
        if (t == 0) break;
        cnt = i;
        if (1e18 / t < x)
          x = 1e18;
        else
          x *= t;
      }
      if (1e18 / 2 < x)
        x = 1e18;
      else
        x *= 2;
      ++cnt;
      if (cnt < a1)
        a1 = cnt, a2 = x;
      else if (cnt == a1)
        a2 = min(a2, x);
    }
  }
  FOR(u, 1, n) {
    FOR(i, 1, dg[u]) FOR(j, 1, n) d[i][j] = 0;
    int id = 0;
    long long x = 1, cnt = 0;
    for (int v : g[u]) {
      ++id;
      dfs(v, u, id);
    }
    assert(id == dg[u]);
    FOR(i, 1, n) {
      int t = d[1][i];
      FOR(j, 1, id) t = max(t, d[j][i]);
      if (t == 0) break;
      cnt = i;
      if (1e18 / t < x)
        x = 1e18;
      else
        x *= t;
    }
    if (1e18 / dg[u] < x)
      x = 1e18;
    else
      x *= dg[u];
    cnt += 2;
    if (cnt < a1)
      a1 = cnt, a2 = x;
    else if (cnt == a1)
      a2 = min(a2, x);
  }
  printf("%lld %lld\n", a1, a2);
  return 0;
}
