// by Sshwy
//#define DEBUGGER
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

#ifdef DEBUGGER

#define log_prefix()                                                \
  {                                                                 \
    fprintf(stderr, "\033[37mLine %-3d [%dms]:\033[0m  ", __LINE__, \
        (int)clock() / 1000);                                       \
  }
#define ilog(...) \
  { fprintf(stderr, __VA_ARGS__); }
#define llog(...)      \
  {                    \
    log_prefix();      \
    ilog(__VA_ARGS__); \
  }
#define log(...)           \
  {                        \
    log_prefix();          \
    ilog(__VA_ARGS__);     \
    fprintf(stderr, "\n"); \
  }
#define red(...)       \
  {                    \
    log_prefix();      \
    ilog("\033[31m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define green(...)     \
  {                    \
    log_prefix();      \
    ilog("\033[32m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define blue(...)      \
  {                    \
    log_prefix();      \
    ilog("\033[34m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }

#else

#define log_prefix() ;
#define ilog(...) ;
#define llog(...) ;
#define log(...) ;
#define red(...) ;
#define green(...) ;
#define blue(...) ;

#endif

namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 1e5 + 5;
vector<int> g[N];

int n;

int w[N], dep[N], fa[N];
void dfs(int u, int p) {
  w[u] = u > n ? u - n : u;
  w[u] ^= w[p];
  dep[u] = dep[p] + 1;
  fa[u] = p;
  printf("u %d fa %d w %d\n", u, fa[u], w[u]);
  for (int v : g[u])
    if (v != p) dfs(v, u);
}
int lca(int x, int y) {
  while (x != y) {
    if (dep[x] < dep[y]) swap(x, y);
    x = fa[x];
  }
  return x;
}
int main() {
  cin >> n;
  if (__builtin_popcount(n) == 1) return cout << "No" << endl, 0;
  assert(n >= 3);
  vector<pair<int, int>> ans;
  for (int x = 2; x < n; x += 2) {
    ans.pb({1, x});
    ans.pb({x, n + x + 1});
    ans.pb({1, x + 1});
    ans.pb({x + 1, n + x});
  }
  ans.pb({n + 1, n + 3});
  if (n % 2 == 0) {
    bool flag = 0;
    FOR(x, 2, n) {
      int y = x ^ 1 ^ n;
      if (1 <= y && y <= n) {
        ans.pb({x, n});
        ans.pb({y, n + n});
        flag = 1;
        break;
      }
    }
    assert(flag);
  }
  cout << "Yes" << endl;
  for (auto x : ans) cout << x.fi << " " << x.se << endl;
  // for(auto x:ans)g[x.fi].pb(x.se),g[x.se].pb(x.fi);
  // dfs(1,0);
  // FOR(i,1,n)assert(w[i]^w[n+i]^w[lca(i,n+i)]^w[fa[lca(i,n+i)]]==i);
  return 0;
}
