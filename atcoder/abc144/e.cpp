#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 2e5 + 5;

long long n, k;
long long a[N], f[N];

bool check(long long x) {
  long long res = 0;
  FOR(i, 1, n) {
    // a[i],f[n-i+1]
    res += max(a[n - i + 1] - x / f[i], 0ll);
  }
  return res <= k;
}
int main() {
  scanf("%lld%lld", &n, &k);
  FOR(i, 1, n) scanf("%lld", &a[i]);
  FOR(i, 1, n) scanf("%lld", &f[i]);
  sort(a + 1, a + n + 1);
  sort(f + 1, f + n + 1);
  long long l = 0, r = 1e18;
  while (l < r) {
    long long mid = (l + r) >> 1;
    if (check(mid))
      r = mid;
    else
      l = mid + 1;
  }
  printf("%lld\n", l);
  return 0;
}
