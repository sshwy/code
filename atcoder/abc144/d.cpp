#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/

double a, b, x;

int main() {
  scanf("%lf%lf%lf", &a, &b, &x);
  // cin>>a>>b>>x;
  x /= a;
  // printf("x=%lf\n",x);
  double s = a * b - x, s2 = x;
  double y = s * 2 / a, y2 = s2 * 2 / a;
  // printf("y=%lf,a=%lf\n",y,a);
  double ans = atan(y / a) * 180 / M_PI;
  double ag2 = atan(y2 / a) * 180 / M_PI;
  // printf("ag2=%lf\n",ag2);
  if (ag2 < ans) {
    y2 = s2 * 2 / b;
    ans = atan(b / y2) * 180 / M_PI;
    printf("%.10lf", ans);
  } else
    printf("%.10lf", ans);
  return 0;
}
