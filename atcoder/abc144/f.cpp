#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 700;

int n, m;
double f[N], INF = 1e18;
double p[N];

vector<int> g[N];
int main() {
  scanf("%d%d", &n, &m);
  FOR(i, 1, m) {
    int u, v;
    scanf("%d%d", &u, &v);
    g[u].pb(v);
  }
  f[n] = 0;
  ROF(u, n - 1, 1) {
    int c = g[u].size();
    double sum = 0;
    for (int v : g[u]) sum += f[v];
    f[u] = sum / c + 1;
  }
  double ans = f[1];
  p[1] = 1;
  FOR(u, 1, n - 1) {
    int c = g[u].size();
    for (int v : g[u]) { p[v] += p[u] / c; }
  }
  FOR(u, 1, n - 1) {
    int c = g[u].size();
    if (c == 1) continue;
    double sum = (f[u] - 1) * c;
    for (int v : g[u]) {
      double e = (sum - f[v]) / (c - 1) + 1;
      ans = min(ans, f[1] + (e - f[u]) * p[u]);
    }
  }
  printf("%.10lf", ans);
  return 0;
}
