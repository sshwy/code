#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 700;

int n, m;
double f[N], INF = 1e18;
double ans = INF;

vector<int> g[N];
int main() {
  scanf("%d%d", &n, &m);
  FOR(i, 1, m) {
    int u, v;
    scanf("%d%d", &u, &v);
    g[u].pb(v);
  }
  f[n] = 0;
  ROF(u, n - 1, 1) {
    int c = g[u].size();
    // printf("u=%d,c=%d\n",u,c);
    double sum = 0;
    for (int v : g[u]) { sum += f[v]; }
    f[u] = sum / c + 1;
  }
  ans = f[1];
  FOR(ban, 1, n - 1) {
    for (int banv : g[ban]) {
      // printf("\033[31mban %d -> %d\033[0m\n",ban,banv);
      ROF(u, n - 1, 1) {
        int c = g[u].size();
        // printf("u=%d,c=%d\n",u,c);
        double sum = 0;
        bool fl = 0;
        for (int v : g[u]) {
          if (u == ban && v == banv) {
            --c;
            continue;
          }
          fl = 1;
          sum += f[v];
        }
        if (!fl)
          f[u] = INF;
        else
          f[u] = sum / c + 1;
        // printf("f[%d]=%lf\n",u,f[u]);
      }
      // printf("f[%d]=%lf\n",1,f[1]);
      ans = min(ans, f[1]);
    }
  }
  printf("%.10lf", ans);
  // printf("%.10lf",min(f[1][0],f[1][1]));
  return 0;
}
