#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 503;

int k;
int a[N][N];

void make(int n) {
  FOR(i, 0, n - 1) FOR(j, 0, n - 1) a[i][j] = (i + j) % n;
  FOR(i, 0, n - 1) if (i & 1) FOR(j, 0, n - 1) a[i][j] += n;
}

int main() {
  cin >> k;
  if (k < 500) {
    printf("%d\n", k);
    FOR(i, 1, k) FOR(j, 1, k) printf("%d%c", i, " \n"[j == k]);
    return 0;
  }
  make(500);
  printf("%d\n", 500);
  FOR(i, 0, 499) FOR(j, 0, 499) if (k <= a[i][j]) a[i][j] -= 500;
  FOR(i, 0, 499) FOR(j, 0, 499) printf("%d%c", a[i][j] + 1, " \n"[j == 499]);
  return 0;
}
