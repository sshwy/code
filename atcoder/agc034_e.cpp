// by Sshwy
//#define DEBUGGER
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

#ifdef DEBUGGER

#define log_prefix()                                                \
  {                                                                 \
    fprintf(stderr, "\033[37mLine %-3d [%dms]:\033[0m  ", __LINE__, \
        (int)clock() / 1000);                                       \
  }
#define ilog(...) \
  { fprintf(stderr, __VA_ARGS__); }
#define llog(...)      \
  {                    \
    log_prefix();      \
    ilog(__VA_ARGS__); \
  }
#define log(...)           \
  {                        \
    log_prefix();          \
    ilog(__VA_ARGS__);     \
    fprintf(stderr, "\n"); \
  }
#define red(...)       \
  {                    \
    log_prefix();      \
    ilog("\033[31m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define green(...)     \
  {                    \
    log_prefix();      \
    ilog("\033[32m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define blue(...)      \
  {                    \
    log_prefix();      \
    ilog("\033[34m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }

#else

#define log_prefix() ;
#define ilog(...) ;
#define llog(...) ;
#define log(...) ;
#define red(...) ;
#define green(...) ;
#define blue(...) ;

#endif

namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 3000;
int n, f[N], c[N], d[N];
char s[N];
vector<int> T[N];

void dfs(int u, int p) {
  d[u] = 0;
  c[u] = s[u] == '1';
  for (int v : T[u])
    if (v != p) dfs(v, u);

  int s = 0, mx = 0, mxv = -1;

  for (int v : T[u])
    if (v != p) {
      c[u] += c[v];
      d[u] += d[v] + c[v];
      s += d[v] + c[v];
      if (d[v] + c[v] > mx) mx = d[v] + c[v], mxv = v;
    }
  if (mx * 2 <= s) {
    f[u] = s / 2;
  } else {
    f[u] = s - mx;
    s -= (s - mx) * 2;
    f[u] += min(f[mxv], s / 2);
  }
}
int calc(int root) {
  dfs(root, 0);
  if (f[root] * 2 == d[root])
    return f[root];
  else
    return 1e9;
}
int main() {
  scanf("%d", &n);
  scanf("%s", s + 1);
  FOR(i, 1, n - 1) {
    int a, b;
    scanf("%d%d", &a, &b);
    T[a].pb(b);
    T[b].pb(a);
  }
  int ans = 1e9;
  FOR(i, 1, n) { ans = min(ans, calc(i)); }
  printf("%d\n", ans == int(1e9) ? -1 : ans);
  return 0;
}
