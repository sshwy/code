#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
#define int long long
const int N = 3e5 + 5;

int n;
int a[N];
int ans, sum;

bool check(int x) {
  int pos = 0; // a[0]=0
  for (int i = 2; i <= n + 1; i += 2) {
    if (a[i - 1] - a[pos] <= x)
      if (a[i] > a[pos]) pos = i; // BUG#1:没判大小关系
  }
  return a[n] - a[pos] <= x;
}
signed main() {
  scanf("%lld", &n);
  FOR(i, 1, n) scanf("%lld", &a[i]), sum += a[i];
  int c[2] = {0, 0};
  FOR(i, 1, n) c[i & 1] += a[i];
  if (c[0] < c[1]) swap(c[0], c[1]);
  if (n % 2 == 0) {
    printf("%lld %lld\n", c[0], c[1]);
    return 0;
  }
  for (int i = 1; i <= n; i += 2) a[i] = -a[i];
  for (int i = 2; i <= n; i += 2) ans += a[i];
  FOR(i, 2, n + 1) a[i] += a[i - 1];
  int l = -1e9, r = 1e9, mid;
  while (l < r) mid = floor((l + r + 0.0) / 2), check(mid) ? r = mid : l = mid + 1;
  ans -= l;
  if (ans > c[0])
    printf("%lld %lld\n", ans, sum - ans);
  else
    printf("%lld %lld\n", c[0], c[1]);
  return 0;
}
