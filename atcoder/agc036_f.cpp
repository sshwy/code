#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 1000;

int n, P;
int F[N][N];

int f(int i) { return sqrt(n * n - i * i - 1) + 1; }
int g(int i) { return min(n * 2, (int)sqrt(4 * n * n - i * i) + 1); }
int sig(int x) { return 1 - 2 * (x & 1); }
pair<int, int> A[N * 3];
int lh;
int b[N];

signed main() {
  scanf("%d%d", &n, &P);
  FOR(i, 0, n - 1) A[++lh] = {f(i), 1};     // a
  FOR(i, 0, n - 1) A[++lh] = {g(i), 3};     // b
  FOR(i, n, n * 2 - 1) A[++lh] = {g(i), 2}; // c
  sort(A + 1, A + lh + 1);
  int pos = n * 2;
  FOR(i, 1, n * 2) {
    assert(A[i].se != 3);
    if (A[i].se == 1) b[i] = ++pos;
  }
  assert(pos == n * 3);
  int ans = 0;
  FOR(k, 0, n) {
    int C = 0; //统计之前出现过多少个c
    F[0][0] = 1;
    FOR(i, 1, n * 2) {
      FOR(j, 0, k) F[i][j] = 0;
      if (A[i].se == 1) { // a
        FOR(j, 0, k)
        F[i][j] =
            (1ll * F[i - 1][j] * (A[b[i]].fi - ((i - 1 - C - j) + n + k)) % P +
                (j == 0 ? 0ll : 1ll * F[i - 1][j - 1] * (A[i].fi - (j - 1 + C)))) %
            P;
      } else { // c
        FOR(j, 0, k) F[i][j] = F[i - 1][j] * 1ll * (A[i].fi - (j + C)) % P;
        ++C;
      }
    }
    ans = (ans + F[n * 2][k] * sig(k)) % P;
  }
  ans = (ans + P) % P;
  printf("%d\n", ans);
  return 0;
}
