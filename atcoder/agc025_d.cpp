#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 1005;

int n, d1, d2;

pii col[N * 2][N * 2];
vector<pii> c1, c2;
int cnt[2][2];

// vector<pii> ans;
int sqr(int x) { return x * x; }

bool v1[N * 2][N * 2], v2[N * 2][N * 2];
void dfs1(int x, int y, int c) {
  if (x < 0 || x > n * 2 - 1 || y < 0 || y > n * 2 - 1) return;
  if (v1[x][y]) return;
  assert(col[x][y].fi != 65536);
  col[x][y].fi = c;
  v1[x][y] = 1;
  for (pii p : c1) {
    int nx = x + p.fi, ny = y + p.se;
    dfs1(nx, ny, !c);
  }
}
void dfs2(int x, int y, int c) {
  if (x < 0 || x > n * 2 - 1 || y < 0 || y > n * 2 - 1) return;
  if (v2[x][y]) return;
  col[x][y].se = c;
  v2[x][y] = 1;
  for (pii p : c2) {
    int nx = x + p.fi, ny = y + p.se; // y写成x
    dfs2(nx, ny, !c);
  }
}

int main() {
  scanf("%d%d%d", &n, &d1, &d2);
  FOR(i, 0, n * 2 - 1) FOR(j, 0, n * 2 - 1) if (i * i + j * j == d1) {
    c1.pb({i, j});
    c1.pb({i, -j});
    c1.pb({-i, j});
    c1.pb({-i, -j});
  }
  FOR(i, 0, n * 2 - 1) FOR(j, 0, n * 2 - 1) if (i * i + j * j == d2) {
    c2.pb({i, j});
    c2.pb({i, -j});
    c2.pb({-i, j});
    c2.pb({-i, -j});
  }
  FOR(i, 0, n * 2 - 1) FOR(j, 0, n * 2 - 1) if (!v1[i][j]) dfs1(i, j, 0);
  FOR(i, 0, n * 2 - 1) FOR(j, 0, n * 2 - 1) if (!v2[i][j]) dfs2(i, j, 0);
  FOR(i, 0, n * 2 - 1) FOR(j, 0, n * 2 - 1) { cnt[col[i][j].fi][col[i][j].se]++; }
  assert(cnt[0][1] + cnt[1][0] + cnt[0][0] + cnt[1][1] == n * n * 4);
  FOR(x, 0, 1) FOR(y, 0, 1) if (cnt[x][y] >= n * n) {
    int tot = 0;
    FOR(i, 0, n * 2 - 1) FOR(j, 0, n * 2 - 1) {
      if (tot == n * n) break;
      if (col[i][j] == (pii){x, y}) {
        ++tot;
        printf("%d %d\n", i, j);
        if (tot == n * n) break;
      }
      if (tot == n * n) break;
    }
    assert(tot == n * n);
    return 0;
  }
  return 0;
}
