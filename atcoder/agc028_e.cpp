#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 2e5 + 5, SZ = N * 20;

int n;

namespace seg {
  int tot;
  int lc[SZ], rc[SZ], ;
  int insert(int u, int pos, int val, int l = 1, int r = n) {
    int u2 = ++tot;
    if (l == r) { return u2; }
    int mid = (l + r) >> 1;
    if (pos <= mid)
      lc[u2] = insert(lc[u], pos, val, l, mid);
    else
      rc[u2] = insert(rc[u], pos, val, mid + 1, r);
  }
} // namespace seg

int p[N], a[N], ans[N];
int rt[N];

int main() {
  scanf("%d", &n);
  FOR(i, 1, n) scanf("%d", &p[i]);

  int las = -1;
  int x = 0, y = 0, z = 0;
  int xlas = -1, ylas = -1;
  FOR(i, 1, n) {
    if (p[i] >= las)
      a[i] = 2, las = p[i], ++z;
    else
      a[i] = 1;
  }

  ROF(i, n, 1) {
    rt[i] = seg::insert(rt[i + 1], p[i], a[i]); //在p[i]位置插入一个权值为a[i]的元素
  }
  FOR(i, 1, n) { //确定第i位是否为0
    if (a[i] == 2) --z;
    if (p[i] < xlas) { //直接放
      ans[i] = 0;
    } else {
      int c = x + (xlas <= p[i]) - y + z; //如果这一位给X
      if (seg::check(rt[i + 1], ylas,
              c)) { //能否选择一个上升序列，首元素大于等于ylas，权值恰为c
        ans[i] = 0;
        if (xlas <= p[i]) ++x, xlas = p[i];
      } else { //检查这一位能否是1
        c = x - (y + (ylas <= p[i])) + z;
        if (seg::check(rt[i + 1], max(ylas, p[i]), c)) {
          ans[i] = 1;
          if (ylas <= p[i]) ++y, ylas = p[i];
        } else {
          puts("-1");
          return 0;
        }
      }
    }
  }
  FOR(i, 1, n) printf("%d", ans[i]);
  puts("");
  return 0;
}
