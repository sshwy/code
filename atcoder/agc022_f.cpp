#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 52, P = 1e9 + 7;

int n;
int C[N][N], f[N][N];
// f[i,j]表示一共i个点，最后一层有j个点有奇数个儿子的方案数

int main() {
  scanf("%d", &n);
  C[0][0] = 1;
  FOR(i, 1, n) {
    C[i][0] = 1;
    FOR(j, 1, i) {
      C[i][j] = (C[i - 1][j - 1] + C[i - 1][j]) % P;
      // printf("C[%d,%d]=%d\n",i,j,C[i][j]);
    }
  }
  f[1][0] = f[1][1] = n;
  FOR(i, 1, n) {
    FOR(j, 0, i) {
      FOR(k, max(j, 1), n - i) {   //枚举下一层一共有k个点
        if ((k - j) & 1) continue; //根据状态定义，k-j必须是偶数
        int t = k - j >> 1;
        //假设下一层是叶子结点，那么可以理解为有(k-j)/2个点是-1，(k+j)/2个点是1
        FOR(x, 0, k) { //枚举下一层实际上有x个点是-1
          //这样就要求|x-(k-j)/2|个点需要从-1改成1（或者从1改成-1）
          //相当于下一层有|x-(k-j)/2|个奇数儿子结点
          f[i + k][abs(x - t)] =
              (f[i + k][abs(x - t)] + 1ll * f[i][j] * C[n - i][k] % P * C[k][x]) % P;
          // printf("(%d,%d) <- (%d,%d)\n",i+k,abs(x-t),i,j);
        }
      }
      // printf("f[%d,%d]=%d\n",i,j,f[i][j]);
    }
  }
  printf("%d\n", f[n][0]);
  return 0;
}
