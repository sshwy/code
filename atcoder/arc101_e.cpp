// by Sshwy
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 5005, P = 1e9 + 7;

int n;
vector<int> T[N];

int sz[N];
int f[N][N]
     [2]; // f[i,j,0/1]表示第i个点的子树，i所在的连通块大小为j，被割掉的边的奇偶性
          // 的容斥贡献。
//转移的时侯在割边时才统计与i连通的部分的贡献。
int g[N]; // g[i]表示有i个点的配对方案
int t[N][2];

void add(int &x, int y) { x = (x + y) % P; }
void dfs(int u, int p) {
  for (int v : T[u])
    if (v != p) dfs(v, u);

  sz[u] = 1;
  f[u][1][0] = 1; //方案数为1

  for (int v : T[u])
    if (v != p) {
      memset(t, 0, sizeof(t[0]) * (sz[u] + sz[v] + 1));
      //不割边
      FOR(i, 1, sz[u]) {
        FOR(j, 1, sz[v]) {
          add(t[i + j][0], 1ll * f[u][i][0] * f[v][j][0] % P);
          add(t[i + j][0], 1ll * f[u][i][1] * f[v][j][1] % P);
          add(t[i + j][1], 1ll * f[u][i][0] * f[v][j][1] % P);
          add(t[i + j][1], 1ll * f[u][i][1] * f[v][j][0] % P);
        }
      }
      //割边
      FOR(i, 1, sz[u]) {
        FOR(j, 1, sz[v]) {
          add(t[i][0], 1ll * f[u][i][0] * f[v][j][1] % P * g[j] % P);
          add(t[i][0], 1ll * f[u][i][1] * f[v][j][0] % P * g[j] % P);
          add(t[i][1], 1ll * f[u][i][1] * f[v][j][1] % P * g[j] % P);
          add(t[i][1], 1ll * f[u][i][0] * f[v][j][0] % P * g[j] % P);
        }
      }
      memcpy(f[u], t, sizeof(t[0]) * (sz[u] + sz[v] + 1));
      sz[u] += sz[v];
    }
}
int main() {
  scanf("%d", &n);
  FOR(i, 1, n - 1) {
    int u, v;
    scanf("%d%d", &u, &v);
    T[u].pb(v), T[v].pb(u);
  }

  g[2] = 1;
  FOR(i, 2, n / 2) g[i * 2] = g[i * 2 - 2] * (i * 2 - 1ll) % P;

  dfs(1, 0);
  int ans = 0;
  FOR(i, 1, n) {
    if (f[1][i][0]) add(ans, f[1][i][0] * 1ll * g[i] % P);
    if (f[1][i][1]) add(ans, -f[1][i][1] * 1ll * g[i] % P);
  }
  add(ans, P);
  printf("%d\n", ans);

  return 0;
}
