// by Sshwy
//#define DEBUGGER
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (long long i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (long long i = (a); i >= (b); --i)

#ifdef DEBUGGER

#define log_prefix()                                                \
  {                                                                 \
    fprintf(stderr, "\033[37mLine %-3d [%dms]:\033[0m  ", __LINE__, \
        (int)clock() / 1000);                                       \
  }
#define ilog(...) \
  { fprintf(stderr, __VA_ARGS__); }
#define llog(...)      \
  {                    \
    log_prefix();      \
    ilog(__VA_ARGS__); \
  }
#define log(...)           \
  {                        \
    log_prefix();          \
    ilog(__VA_ARGS__);     \
    fprintf(stderr, "\n"); \
  }
#define red(...)       \
  {                    \
    log_prefix();      \
    ilog("\033[31m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define green(...)     \
  {                    \
    log_prefix();      \
    ilog("\033[32m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define blue(...)      \
  {                    \
    log_prefix();      \
    ilog("\033[34m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }

#else

#define log_prefix() ;
#define ilog(...) ;
#define llog(...) ;
#define log(...) ;
#define red(...) ;
#define green(...) ;
#define blue(...) ;

#endif

namespace RA {
  long long r(long long p) { return 1ll * rand() * rand() % p; }
  long long r(long long L, long long R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/

long long n;

string calc(long long x, long long y, long long k) { // 2^0,2^1,...,2^k
  if (k == -1) return "";
  long long f1 = x < 0, f2 = y < 0;
  x = abs(x), y = abs(y);
  string s;
  if (x > y)
    s = 'R' + calc(x - (1 << k), y, k - 1);
  else
    s = 'U' + calc(x, y - (1 << k), k - 1);
  if (f1)
    for (auto &c : s) {
      if (c == 'L')
        c = 'R';
      else if (c == 'R')
        c = 'L';
    }
  if (f2)
    for (auto &c : s) {
      if (c == 'U')
        c = 'D';
      else if (c == 'D')
        c = 'U';
    }
  return s;
}

void check(string s, vector<long long> d, pair<int, int> p) {
  assert(s.size() == d.size());
  long long px = 0, py = 0;
  for (long long i = 0; i < s.size(); i++) {
    if (s[i] == 'L') px -= d[i];
    if (s[i] == 'R') px += d[i];
    if (s[i] == 'U') py += d[i];
    if (s[i] == 'D') py -= d[i];
    // printf("%d %d\n",px,py);
  }
  assert(px == p.fi && py == p.se);
}
signed main() {
  long long n;
  cin >> n;
  vector<pair<int, int>> v;
  FOR(i, 1, n) {
    long long x, y;
    cin >> x >> y;
    if (v.size() && abs(v.back().fi + v.back().se) % 2 != abs(x + y) % 2) {
      cout << -1 << endl;
      return 0;
    }
    v.pb({x, y});
  }

  vector<long long> d;

  if (abs(v.back().fi + v.back().se) % 2 == 0) {
    d.pb(1);
    ROF(i, 30, 0) d.pb(1ll << i);
    cout << d.size() << endl;
    for (auto x : d) cout << x << " ";
    cout << endl;
    for (auto p : v) {
      auto ans = ('R' + calc(p.fi - 1, p.se, 30));
      cout << ans << endl;
      check(ans, d, p);
    }
  } else {
    ROF(i, 30, 0) d.pb(1ll << i);
    cout << d.size() << endl;
    for (auto x : d) cout << x << " ";
    cout << endl;
    for (auto p : v) {
      auto ans = calc(p.fi, p.se, 30);
      cout << ans << endl;
      check(ans, d, p);
    }
  }
  return 0;
}
