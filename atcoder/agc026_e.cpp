#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 3050 * 2;

int n;
char s[N];
string a[N];
int la;

bool del_a(string &s) {
  for (string::iterator it = s.begin(); it != s.end(); it++) {
    if (*it == 'a') return s.erase(it), 1;
  }
  return 0;
}
bool del_b(string &s) {
  for (string::iterator it = s.begin(); it != s.end(); it++) {
    if (*it == 'b') return s.erase(it), 1;
  }
  return 0;
}
string calc(string s) {
  // cout<<"calc "<<s<<endl;
  if (s[0] == 'a') {
    string res = "";
    int las_b = -1;
    int pos_a = -1, pos_b = -1;
    while (pos_b < (int)s.size()) {
      ++pos_b;
      while (pos_b < (int)s.size() && s[pos_b] == 'a') ++pos_b;
      ++pos_a;
      while (pos_a < (int)s.size() && s[pos_a] == 'b') ++pos_a;
      if (pos_b == s.size()) break;
      // printf("%d %d\n",pos_a,pos_b);
      if (pos_a > las_b) res += "ab", las_b = pos_b;
    }
    return res;
  } else {
    string res = s;
    while (s != "") {
      del_a(s);
      del_b(s);
      res = max(res, s);
    }
    // cout<<"res="<<res<<endl;
    return res;
  }
}
int main() {
  scanf("%d", &n);
  scanf("%s", s + 1);
  int ca = 0, cb = 0;
  string t = "";
  FOR(i, 1, n * 2) {
    if (s[i] == 'a')
      ca++;
    else
      cb++;
    t.pb(s[i]);
    if (ca == cb) a[++la] = calc(t), t = "";
  }
  assert(t == "");
  string ans = "";
  ROF(i, la, 1) ans = max(ans, a[i] + ans);
  cout << ans;
  return 0;
}
