// by Sshwy
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 17, M = 17, P = 1e9 + 7;
int pw(int a, int m) {
  int res = 1;
  while (m) m & 1 ? res = 1ll * res * a % P : 0, a = 1ll * a * a % P, m >>= 1;
  return res;
}

int n, m, nn;
int a[M];
int f[1 << N];
// f[mask]表示mask集合对应的区间的最小值是a中的元素

int fac[(1 << N) + 1], fnv[(1 << N) + 1];

int binom(int n, int m) {
  if (n < 0 || m < 0 || n < m) return 0;
  return fac[n] * 1ll * fnv[m] % P * fnv[n - m] % P;
}
void add(int &x, int y) { x = (x + y) % P; }
int sig(int x) { return 1 - 2 * (x & 1); }
int main() {
  scanf("%d%d", &n, &m);
  nn = 1 << n;

  fac[0] = 1;
  FOR(i, 1, nn) fac[i] = 1ll * fac[i - 1] * i % P;
  fnv[nn] = pw(fac[nn], P - 2);
  ROF(i, nn, 1) fnv[i - 1] = 1ll * fnv[i] * i % P;

  FOR(i, 1, m) scanf("%d", &a[i]);

  f[0] = 1; //初始时

  ROF(i, m, 1) {
    ROF(mask, nn - 1, 0) { //滚动数组
      int sum = mask; // sum表示已经使用过的数字的个数。恰好等于mask
      FOR(j, 0, n - 1) if (!(mask >> j & 1)) {
        add(f[mask | (1 << j)], binom(nn - sum - a[i], (1 << j) - 1) * 1ll *
                                    fac[1 << j] % P * f[mask] % P);
      }
    }
  }

  int ans = 0;

  FOR(mask, 0, nn - 1)
  add(ans,
      sig(__builtin_popcount(mask)) * 1ll * f[mask] % P * fac[nn - mask - 1] % P);

  ans = ans * 1ll * nn % P;

  add(ans, P);

  printf("%d\n", ans);
  return 0;
}
