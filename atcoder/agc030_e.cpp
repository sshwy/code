#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <deque>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 5050;

int n;
char s[N], t[N];

deque<int> A, B;

int main() {
  scanf("%d", &n);
  scanf("%s", s + 1);
  scanf("%s", t + 1);
  FOR(i, 1, n) s[i] -= '0', t[i] -= '0';

  s[0] = !s[1], t[0] = !t[1];
  s[n + 1] = !s[n], t[n + 1] = !t[n];

  FOR(i, 1, n + 1) if (s[i - 1] != s[i]) A.push_back(i);
  FOR(i, 1, n + 1) if (t[i - 1] != t[i]) B.push_back(i);
  if (s[1] != t[1]) A.push_front(1);
  for (int i = 0; i <= n + 1; i += 2) A.push_front(1), A.push_front(1);
  for (int i = 0; i <= n + 1; i += 2) A.push_back(n + 1), A.push_back(n + 1);
  int ans = 1e9;
  for (int i = 0; i + B.size() <= A.size(); i += 2) {
    int tot = 0;
    for (int j = 0; j < i; j++) tot += abs(A[j] - 1); //全放左边
    for (int j = 0; j < B.size(); j++) { tot += abs(A[i + j] - B[j]); }
    for (int j = i + B.size(); j < A.size(); j++)
      tot += abs(A[j] - (n + 1)); //全放右边
    ans = min(ans, tot);
  }
  printf("%d\n", ans);
  return 0;
}
