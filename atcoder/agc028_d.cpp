#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 350 * 2, P = 1e9 + 7;
typedef long long LL;

int n, K;
LL f[N][N]; // f[i,j]表示连通块(i,j)的出现次数
LL g[N], ans;
int A[N], B[N];
int use[N];

int main() {
  scanf("%d%d", &n, &K);
  g[0] = 1;
  FOR(i, 1, n) g[i * 2] = g[(i - 1) * 2] * (i * 2ll - 1) % P;
  FOR(i, 1, K) {
    scanf("%d%d", &A[i], &B[i]);
    use[A[i]] = use[B[i]] = 1;
  }
  FOR(len, 1, n * 2 - 1) {
    FOR(i, 1, n * 2 - len) {
      int j = i + len;
      int x = 0, y = 0;
      FOR(k, 1, i - 1) if (!use[k])++ y;
      FOR(k, i, j) if (!use[k])++ x;
      FOR(k, j + 1, n * 2) if (!use[k])++ y;
      FOR(k, 1, K)
      if ((i <= A[k] && A[k] <= j) ^ (i <= B[k] && B[k] <= j)) x = 1;
      if ((x & 1) || (y & 1)) continue;
      f[i][j] = g[x];
      FOR(k, i, j - 1) {
        if (!use[k]) --x;
        if (x & 1) continue;
        f[i][j] = (f[i][j] - f[i][k] * g[x]) % P;
      }
      if (f[i][j]) ans = (ans + f[i][j] * g[y]) % P;
    }
  }
  ans = (ans + P) % P;
  printf("%lld\n", ans);
  return 0;
}
