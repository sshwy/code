#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
#define int lld
int pw(int a, int m, int p) {
  int res = 1;
  while (m) m & 1 ? res = 1ll * res * a % p : 0, a = 1ll * a * a % p, m >>= 1;
  return res;
}
const int N = 20, NN = 1 << 18, P = 998244353;

int n, nn;
int p[NN], s;
int c[NN];

void fwt(int *f, int len) {
  for (int j = 1; j < len; j <<= 1)
    for (int i = 0; i < len; i += j << 1)
      for (int k = i, u, v; k < i + j; k++)
        u = f[k], v = f[k + j], f[k] = (u + v) % P, f[k + j] = (u - v + P) % P;
}
void ifwt(int *f, int len) {
  for (int j = 1; j < len; j <<= 1)
    for (int i = 0; i < len; i += j << 1)
      for (int k = i, u, v; k < i + j; k++)
        u = f[k], v = f[k + j], f[k] = (u + v) % P, f[k + j] = (u - v + P) % P;
  int ilen = pw(len, P - 2, P);
  FOR(i, 0, len - 1) f[i] = 1ll * f[i] * ilen % P;
}
signed main() {
  scanf("%lld", &n);
  nn = 1 << n;
  FOR(i, 0, nn - 1) scanf("%lld", &p[i]), s = (s + p[i]) % P;
  s = pw(s, P - 2, P);
  FOR(i, 0, nn - 1) p[i] = 1ll * p[i] * s % P;
  p[0]--;
  c[0] = nn - 1;
  FOR(i, 1, nn - 1) c[i] = P - 1;
  fwt(p, nn), fwt(c, nn);
  FOR(i, 0, nn - 1) c[i] = 1ll * c[i] * pw(p[i], P - 2, P) % P;
  ifwt(c, nn);
  FOR(i, 0, nn - 1) printf("%lld\n", (c[i] - c[0] + P) % P);
  return 0;
}
