#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 1e5 + 5;

struct P {
  int n;
  int a[N];
  P(int _n) {
    n = _n;
    FOR(i, 1, n) a[i] = i;
  }
  P() {}
  void read() { FOR(i, 1, n) scanf("%d", &a[i]); }
  int &operator[](int x) { return a[x]; }
  P operator*(P b) {
    P res;
    res.n = n;
    FOR(i, 1, n) res[i] = a[b[i]];
    return res;
  }
  P operator*=(P b) {
    *this = *this * b;
    return *this;
  }
  void print() { FOR(i, 1, n) printf("%d%c", a[i], " \n"[i == n]); }
};

P inv(P &a) {
  P res;
  res.n = a.n;
  FOR(i, 1, a.n) res[a[i]] = i;
  return res;
}
P pw(P a, int m) {
  P res(a.n);
  while (m) m & 1 ? res = res * a : 0, a = a * a, m >>= 1;
  return res;
}

int n, k;

int main() {
  scanf("%d%d", &n, &k);
  P p(n), q(n);
  p.read();
  q.read();
  P ip = inv(p), iq = inv(q);

  P A = q * ip * iq * p;
  A = pw(A, (k - 1) / 6);
  if (k % 6 == 4)
    A *= q;
  else if (k % 6 == 5)
    A *= q * ip;
  else if (k % 6 == 0)
    A *= q * ip;

  P B = k % 6 == 0   ? iq * p
        : k % 6 == 1 ? p
        : k % 6 == 2 ? q
        : k % 6 == 3 ? q * ip
        : k % 6 == 4 ? ip
                     : iq;

  P ans = A * B * inv(A);
  ans.print();
  return 0;
}
