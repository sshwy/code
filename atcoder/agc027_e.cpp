#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
typedef long long LL;
const int N = 1e5 + 5, P = 1e9 + 7;

int n;
char S[N];
LL s[N];
int app_a[N], app_b[N]; //在当前状态下加一个a后转移到的状态
int nex[N][3]; //下一个，前缀和%3=i的位置（不算当前的位置）
int nex_xx[N]; //下一个连续相同字符的位置， nex_xx[i] 和nex_xx[i]+1相同
LL f[N];

int main() {
  scanf("%s", S + 1);
  n = strlen(S + 1);
  FOR(i, 1, n) s[i] = S[i] == 'a' ? 1 : 2;
  FOR(i, 2, n) s[i] += s[i - 1];
  nex[n][0] = nex[n][1] = nex[n][2] = n + 1;
  nex[n + 1][0] = nex[n + 1][1] = nex[n + 1][2] = n + 1;
  ROF(i, n - 1, 0) {
    FOR(j, 0, 2) nex[i][j] = s[i + 1] % 3 == j ? i + 1 : nex[i + 1][j];
  }
  nex_xx[n] = nex_xx[n + 1] = n + 1;
  ROF(i, n - 1, 0) {
    nex_xx[i] = s[i + 1] - s[i] == s[i + 2] - s[i + 1] ? i + 1 : nex_xx[i + 1];
  }
  FOR(i, 0, n) {
    app_a[i] = s[i + 1] - s[i] == 1 ? i + 1 : nex[nex_xx[i]][(s[i] + 1) % 3];
    app_b[i] = s[i + 1] - s[i] == 2 ? i + 1 : nex[nex_xx[i]][(s[i] + 2) % 3];
  }
  f[0] = 1;
  FOR(i, 0, n) {
    f[app_a[i]] = (f[app_a[i]] + f[i]) % P;
    f[app_b[i]] = (f[app_b[i]] + f[i]) % P;
  }
  LL ans = 0;
  FOR(i, 1, n) if (s[i] % 3 == s[n] % 3) ans = (ans + f[i]) % P;
  FOR(i, 1, n) printf("%d%c", f[i], " \n"[i == n]);
  if (nex_xx[0] == n + 1)
    puts("1");
  else
    printf("%lld\n", ans);
  return 0;
}
