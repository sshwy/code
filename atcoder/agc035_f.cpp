#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 5e5 + 5, P = 998244353;
int pw(int a, int m) {
  int res = 1;
  while (m) m & 1 ? res = 1ll * res * a % P : 0, a = 1ll * a * a % P, m >>= 1;
  return res;
}
int fac[N], fnv[N];

int n, m;
int binom(int n, int m) {
  if (n < m) return 0;
  return fac[n] * 1ll * fnv[m] % P * fnv[n - m] % P;
}
int sig(int x) { return 1 - 2 * (x & 1); }

int main() {
  scanf("%d%d", &n, &m);
  fac[0] = 1;
  if (n < m) swap(n, m);
  FOR(i, 1, n) fac[i] = 1ll * fac[i - 1] * i % P;
  fnv[n] = pw(fac[n], P - 2);
  ROF(i, n, 1) fnv[i - 1] = fnv[i] * 1ll * i % P;
  int ans = 0;
  FOR(i, 0, min(n, m)) {
    ans = (ans + binom(n, i) * 1ll * binom(m, i) % P * fac[i] % P * pw(m + 1, n - i) %
                     P * pw(n + 1, m - i) % P * sig(i)) %
          P;
  }
  ans = (ans + P) % P;
  printf("%d\n", ans);
  return 0;
}
