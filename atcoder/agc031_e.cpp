#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
typedef long long LL;
const LL LLINF = 0x3f3f3f3f3f3f3f3f;
const int N = 400, INF = 0x3f3f3f3f;

int s, t;
struct qxx {
  int nex, t, v;
  LL c;
};
qxx e[N * N * 10];
LL h[N], hh[N], le = 1;
void add_path(int f, int t, int v, LL c) {
  e[++le] = (qxx){h[f], t, v, c}, h[f] = le;
}
void add_flow(int f, int t, int v, LL c) {
  add_path(f, t, v, c), add_path(t, f, 0, -c);
}
#define FORe(i, _u, _v, _w, _c)                                            \
  for (LL i = h[_u], _v, _w, _c; _v = e[i].t, _w = e[i].v, _c = e[i].c, i; \
       i = e[i].nex)
#define FORflowe(i, _u, _v, _w, _c)                                          \
  for (LL &i = hh[_u], _v, _w, _c; _v = e[i].t, _w = e[i].v, _c = e[i].c, i; \
       i = e[i].nex)

int n, m;
int x[N], y[N];
LL val[N], ans = -LLINF;
int xL[N], xR[N], yL[N], yR[N];

vector<pii> L, R, U, D;

queue<int> q;
bool vising[N];
LL d[N];
bool spfa() {
  memset(d, 0x3f, sizeof(d));
  q.push(s), d[s] = 0;
  while (!q.empty()) {
    int u = q.front();
    q.pop();
    vising[u] = 0;
    FORe(i, u, v, w, c) {
      if (!w || d[v] <= d[u] + c) continue;
      d[v] = d[u] + c;
      if (!vising[v]) q.push(v), vising[v] = 1;
    }
  }
  return d[t] != LLINF;
}
LL mincost;
bool vis[N];
int dfs(int u, int flow) {
  vis[u] = 1;
  if (u == t) return flow;
  int rest = flow;
  FORflowe(i, u, v, w, c) {
    if (!rest) break;
    if (vis[v] && v != t || !w || d[v] != d[u] + c) continue;
    int k = dfs(v, min(w, 1ll * rest));
    if (k)
      mincost += k * c, e[i].v -= k, e[i ^ 1].v += k, rest -= k;
    else
      d[v] = 0;
  }
  return flow - rest;
}
LL go() {
  int maxflow = 0;
  while (spfa()) {
    memcpy(hh, h, sizeof(h));
    for (int i; memset(vis, 0, sizeof(vis)), (i = dfs(s, INF)) && vis[t];)
      maxflow += i;
  }
  return mincost;
}
void init() {
  memset(h, 0, sizeof(h));
  le = 1, mincost = 0;
}
void calc(int k) {
  FOR(i, 0, n) xL[i] = yL[i] = -1, xR[i] = yR[i] = INF;
  xR[k + 1] = INF;
  yR[k + 1] = INF;
  for (pii x : L) xL[x.se + 1] = max(xL[x.se + 1], x.fi + 1); // x[b+1] >= a+1
  for (pii x : R)
    if (k - x.se >= 0) xR[k - x.se] = min(xR[k - x.se], x.fi - 1); // x[k-b] <= a-1
  for (pii x : D) yL[x.se + 1] = max(yL[x.se + 1], x.fi + 1);
  for (pii x : U)
    if (k - x.se >= 0) yR[k - x.se] = min(yR[k - x.se], x.fi - 1); // BUG#1:忘判>0
  FOR(i, 1, k) xL[i] = max(xL[i], xL[i - 1]);
  ROF(i, k, 1) xR[i] = min(xR[i], xR[i + 1]);
  FOR(i, 1, k) yL[i] = max(yL[i], yL[i - 1]);
  ROF(i, k, 1) yR[i] = min(yR[i], yR[i + 1]);
  // item: 1 - n, n+1 - 2n
  // x : 2n+1 - 2n+k
  // y : 2n+k+1 - 2n+2k
  // s : 0
  // t : 2n+2k+1
  init();
  s = 0, t = 2 * n + 2 * k + 1;
  FOR(i, 1, k) add_flow(s, 2 * n + i, 1, 0);
  FOR(i, 1, k)
  FOR(j, 1, n)
  if (xL[i] <= x[j] && x[j] <= xR[i]) add_flow(2 * n + i, j, INF, 0);
  FOR(i, 1, n) add_flow(i, i + n, 1, -val[i]);
  FOR(i, 1, k)
  FOR(j, 1, n)
  if (yL[i] <= y[j] && y[j] <= yR[i]) add_flow(j + n, 2 * n + k + i, INF, 0);
  FOR(i, 1, k) add_flow(2 * n + k + i, t, 1, 0);

  LL res = go();
  ans = max(ans, -res);
}

int main() {
  scanf("%d", &n);
  FOR(i, 1, n) scanf("%d%d%lld", &x[i], &y[i], &val[i]);
  scanf("%d", &m);
  FOR(i, 1, m) {
    char t[10];
    int a, b;
    scanf("%s%d%d", t, &a, &b);
    if (t[0] == 'L')
      L.pb({a, b});
    else if (t[0] == 'R')
      R.pb({a, b});
    else if (t[0] == 'U')
      U.pb({a, b});
    else
      D.pb({a, b});
  }
  FOR(i, 1, n) calc(i);
  printf("%lld\n", ans);
  return 0;
}
