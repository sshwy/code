#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 1e5 + 5;

int n;
int a[N];

bool dfs(int x) {
  if (a[x + 1] <= x) {
    int cnt = 0, pos = x + 1;
    while (a[pos] == x) ++cnt, ++pos;
    int c2 = a[x] - x;
    if ((cnt & 1) || (c2 & 1)) return 1;
    return 0;
  }
  return dfs(x + 1);
}
int main() {
  scanf("%d", &n);
  FOR(i, 1, n) scanf("%d", &a[i]);
  sort(a + 1, a + n + 1);
  reverse(a + 1, a + n + 1);
  int ans = dfs(1);
  puts(ans ? "First" : "Second");
  return 0;
}
