#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 305, P = 1e9 + 7;

int n, tot;
int typ[N * 2];     // 1:已确定；0：(-1,-1)；-1：(-1,x)
int f[N * 2][N][N]; //

int main() {
  scanf("%d", &n);
  FOR(i, 1, n) {
    int x, y;
    scanf("%d%d", &x, &y);
    if (x != -1 && y != -1)
      typ[x] = typ[y] = 1;
    else if (x == -1 && y == -1)
      ++tot;
    else if (x != -1)
      typ[x] = -1;
    else
      typ[y] = -1;
  }
  f[n * 2 + 1][0][0] = 1;
  ROF(i, n * 2, 0) {
    if (typ[i] == 1)
      memcpy(f[i], f[i + 1], sizeof(f[i + 1]));
    else if (typ[i] == 0) {
      FOR(j, 0, n) FOR(k, 0, n) {
        f[i][j][k + 1] = (f[i][j][k + 1] + f[i + 1][j][k]) % P; //添加一个普通括号
        if (j)
          f[i][j - 1][k] =
              (f[i][j - 1][k] + f[i + 1][j][k] * 1ll * j) % P; //匹配一个特殊括号
        if (k)
          f[i][j][k - 1] = (f[i][j][k - 1] + f[i + 1][j][k]) % P; //匹配一个普通括号
      }
    } else {
      FOR(j, 0, n) FOR(k, 0, n) {
        f[i][j + 1][k] = (f[i][j + 1][k] + f[i + 1][j][k]) % P; //添加一个特殊括号
        if (k)
          f[i][j][k - 1] = (f[i][j][k - 1] + f[i + 1][j][k]) % P; //匹配一个普通括号
      }
    }
  }
  int ans = f[1][0][0];
  FOR(i, 1, tot) ans = 1ll * ans * i % P;
  printf("%d\n", ans);
  return 0;
}
