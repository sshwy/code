#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 2e5 + 5, M = 2e6 + 5, INF = 0x3f3f3f3f;

struct qxx {
  int nex, t, v;
};
qxx e[M];
int hh[M], h[M], le = 1;
void add_path(int f, int t, int v) { e[++le] = (qxx){h[f], t, v}, h[f] = le; }
void add_flow(int f, int t, int v) { add_path(f, t, v), add_path(t, f, 0); }
#define FORe(i, _u, _v, _w) \
  for (int i = h[_u], _v, _w; _v = e[i].t, _w = e[i].v, i; i = e[i].nex)
#define FORflowe(i, _u, _v, _w) \
  for (int &i = hh[_u], _v, _w; _v = e[i].t, _w = e[i].v, i; i = e[i].nex)

int s, t;
int n;
int dep[N];
queue<int> q;
bool bfs() {
  memset(dep, 0, sizeof(dep));
  q.push(s), dep[s] = 1;
  while (!q.empty()) {
    int u = q.front();
    q.pop();
    FORe(i, u, v, w) if (w && !dep[v]) dep[v] = dep[u] + 1, q.push(v);
  }
  return !!dep[t];
}
int dfs(int u, int flow) {
  if (u == t) return flow;
  int rest = flow;
  FORflowe(i, u, v, w) {
    if (!w || dep[v] != dep[u] + 1) continue;
    int k = dfs(v, min(w, rest));
    e[i].v -= k, e[i ^ 1].v += k, rest -= k;
    if (!rest) break;
  }
  return flow - rest;
}
int dinic() {
  int maxflow = 0;
  while (bfs()) {
    memcpy(hh, h, sizeof(h));
    for (int i; (i = dfs(s, INF));) maxflow += i;
  }
  return maxflow;
}

vector<int> c[N];
vector<pii> g[N];
pii ans[N];
int p[N], vis[N], use[N], la;
void dfs(int u) {
  vis[u] = 1;
  for (pii v : g[u])
    if (!vis[v.fi] && !use[v.se]) {
      use[v.se] = 1;
      ans[v.se] = {u, v.fi};
      ++la;
      dfs(v.fi);
    }
}

int main() {
  scanf("%d", &n);
  s = 0, t = n * 2;
  FOR(i, 1, n - 1) {
    int k, x;
    scanf("%d", &k);
    FOR(j, 1, k) {
      scanf("%d", &x);
      c[i].pb(x);
      if (x != 1) add_flow(x, i + n, 1); // 1是根结点
    }
  }
  FOR(i, 2, n) add_flow(s, i, 1);
  FOR(i, 1, n - 1) add_flow(i + n, t, 1);
  int tot = dinic();
  if (tot < n - 1) return puts("-1"), 0;
  assert(tot == n - 1);
  FOR(i, 2, n) { FORe(j, i, v, w) if ((j & 1) == 0 && !w) p[v - n] = i; }
  // FOR(i,1,n-1)printf("p[%d]=%d\n",i,p[i]);
  FOR(i, 1, n - 1) {
    for (int x : c[i]) {
      if (x != p[i]) g[x].pb({p[i], i}); //,g[p[i]].pb(x);
    }
  }
  dfs(1);
  if (la != n - 1) return puts("-1"), 0;
  FOR(i, 1, n - 1) printf("%d %d\n", ans[i].fi, ans[i].se);
  return 0;
}
