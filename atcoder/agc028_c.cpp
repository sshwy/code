#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
typedef long long LL;
const int N = 1e5 + 5;

int n;
LL a[N], b[N], sa, sb, ans;

multiset<int> A, B;
LL tot;

int main() {
  scanf("%d", &n);
  FOR(i, 1, n) scanf("%lld%lld", &a[i], &b[i]), sa += a[i], sb += b[i];
  ans = min(sa, sb);
  FOR(i, 2, n) A.insert(a[i]), A.insert(b[i]), tot += a[i], tot += b[i];
  FOR(i, 1, n) {
    int cur = *A.rbegin();
    tot -= cur;
    A.erase(A.find(cur));
    B.insert(cur);
  }
  ans = min(a[1] + b[1] + tot, ans);
  FOR(i, 2, n) {
    int cur;
    B.insert(a[i - 1]);
    B.insert(b[i - 1]);
    cur = *B.begin(), B.erase(B.find(cur)), A.insert(cur), tot += cur;
    cur = *B.begin(), B.erase(B.find(cur)), A.insert(cur), tot += cur;
    cur = *A.rbegin(), A.erase(A.find(cur)), tot -= cur, B.insert(cur);
    cur = *A.rbegin(), A.erase(A.find(cur)), tot -= cur, B.insert(cur);
    if (A.find(a[i]) != A.end())
      tot -= a[i], A.erase(A.find(a[i])), cur = *B.begin(), B.erase(B.find(cur)),
                                          tot += cur, A.insert(cur);
    else
      B.erase(B.find(a[i]));
    if (A.find(b[i]) != A.end())
      tot -= b[i], A.erase(A.find(b[i])), cur = *B.begin(), B.erase(B.find(cur)),
                                          tot += cur, A.insert(cur);
    else
      B.erase(B.find(b[i]));
    ans = min(ans, a[i] + b[i] + tot);
  }
  printf("%lld\n", ans);
  return 0;
}
