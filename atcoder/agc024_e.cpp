#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 500;

int n, K, P;
long long f[N][N], C[N][N];
// f[i,j]表示i个点，根结点的权值为j的树的个数（默认根结点的id=0）

int main() {
  scanf("%d%d%d", &n, &K, &P);
  FOR(i, 0, K) f[1][i] = 1;
  ROF(i, K, 0) f[1][i] = (f[1][i] + f[1][i + 1]) % P;
  C[0][0] = 1;
  FOR(i, 1, n + 1) {
    C[i][0] = 1;
    FOR(j, 1, i) C[i][j] = (C[i - 1][j - 1] + C[i - 1][j]) % P;
  }
  FOR(i, 2, n + 1) {
    ROF(j, K, 0) {
      FOR(k, 1, i - 1) {
        long long s = f[k][j + 1];
        f[i][j] =
            (f[i][j] + (f[i - k][j] - f[i - k][j + 1]) * C[i - 2][k - 1] % P * s) % P;
      }
      f[i][j] = (f[i][j] + f[i][j + 1]) % P;
    }
  }
  printf("%lld\n", (f[n + 1][0] - f[n + 1][1] + P) % P);
  return 0;
}
