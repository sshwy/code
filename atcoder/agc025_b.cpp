#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (lld i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (lld i = (a); i >= (b); --i)
/******************heading******************/
const lld N = 3e5 + 5, P = 998244353;
lld n, a, b;
lld fac[N], fnv[N];
lld k;
lld gcd(lld a, lld b) { return b ? gcd(b, a % b) : a; }
lld exgcd(lld a, lld b, lld &x, lld &y) {
  if (!b) return x = 1, y = 0, a;
  lld t = exgcd(b, a % b, y, x);
  return y -= (a / b) * x, t;
}
lld inv(lld a) {
  lld x, y;
  exgcd(a, P, x, y);
  return (x % P + P) % P;
}
lld binom(lld a, lld b) {
  if (a < b) return 0;
  return 1ll * fac[a] * fnv[b] % P * fnv[a - b] % P;
}

int main() {
  cin >> n >> a >> b >> k;
  lld g = gcd(a, b);
  if (k % g) return puts("0"), 0;
  if (!k) return puts("1"), 0;

  fac[0] = 1;
  FOR(i, 1, n) fac[i] = 1ll * fac[i - 1] * i % P;
  fnv[n] = inv(fac[n]);
  ROF(i, n, 1) fnv[i - 1] = 1ll * fnv[i] * i % P;

  a /= g, b /= g, k /= g;
  lld x0, y0, ans = 0;
  exgcd(a, b, x0, y0);
  x0 *= k, y0 *= k;
  lld lt = ceil(-1.0 * x0 / b), rt = floor(1.0 * y0 / a);
  FOR(t, lt, rt) {
    lld x = x0 + b * t;
    lld y = y0 - a * t;
    if (x > n || y > n) continue;
    ans = (ans + 1ll * binom(n, x) * binom(n, y) % P) % P;
  }
  cout << ans << endl;
  return 0;
}
