#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 1e5 + 5;

int n, s;
long long p[N], x[N], ans[N], tot, la;

int main() {
  scanf("%d%d", &n, &s);
  FOR(i, 1, n) scanf("%lld%lld", &x[i], &p[i]);
  int l = 1, r = n;
  while (l <= r) {
    if (s <= x[l])
      while (l <= r) ans[++la] = r--; //倒过来的
    else if (x[r] <= s)
      while (l <= r) ans[++la] = l++;
    else {
      if (p[l] >= p[r]) {
        p[l] += p[r];
        ans[++la] = r--;
      } else {
        p[r] += p[l];
        ans[++la] = l++;
      }
    }
  }
  assert(la == n);
  reverse(ans + 1, ans + n + 1);
  ans[0] = 0, x[0] = s;
  // FOR(i,0,n)printf("%d%c",x[ans[i]]," \n"[i==n]);
  FOR(i, 1, n) tot += abs(x[ans[i - 1]] - x[ans[i]]);
  printf("%lld\n", tot);
  return 0;
}
