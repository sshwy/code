#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 3005, INF = 0x3f3f3f3f;

int n;
int a[N];
vector<int> g[N];

bool dfs(int u, int p = 0) {
  int min_au = INF;
  for (int v : g[u])
    if (v != p) {
      if (dfs(v, u))
        ;
      else
        min_au = min(min_au, a[v]);
    }
  if (min_au < a[u]) return 1; // win
  return 0;                    // lose
}
int main() {
  scanf("%d", &n);
  FOR(i, 1, n) scanf("%d", &a[i]);
  FOR(i, 1, n - 1) {
    int x, y;
    scanf("%d%d", &x, &y);
    g[x].pb(y), g[y].pb(x);
  }
  FOR(i, 1, n) if (dfs(i)) printf("%d ", i);
  return 0;
}
