#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int P = 1e9 + 7, N = 3005, I2 = 500000004;

int n, q;
int a[N];
int f[N][N];

int main() {
  scanf("%d%d", &n, &q);
  FOR(i, 1, n) scanf("%d", &a[i]);
  FOR(i, 1, n) FOR(j, 1, n) {
    if (a[i] > a[j]) f[i][j] = 1;
  }
  FOR(_, 1, q) {
    int x, y;
    scanf("%d%d", &x, &y);
    if (x > y) swap(x, y);
    f[x][y] = (1ll * f[x][y] * I2 + 1ll * f[y][x] * I2) % P;
    f[y][x] = f[x][y];
    FOR(i, 1, n) {
      if (i == x || i == y) continue;
      f[x][i] = (1ll * f[x][i] * I2 + 1ll * f[y][i] * I2) % P;
      f[y][i] = f[x][i];
      f[i][x] = (1ll * f[i][x] * I2 + 1ll * f[i][y] * I2) % P;
      f[i][y] = f[i][x];
    }
  }
  int ans = 0;
  FOR(i, 1, n) FOR(j, i + 1, n) ans = (ans + f[i][j]) % P;
  FOR(i, 1, q) ans = ans * 2 % P;
  printf("%d\n", ans);
  return 0;
}
