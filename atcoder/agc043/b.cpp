// by Sshwy
//#define DEBUGGER
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

#ifdef DEBUGGER

#define log_prefix()                                                \
  {                                                                 \
    fprintf(stderr, "\033[37mLine %-3d [%dms]:\033[0m  ", __LINE__, \
        (int)clock() / 1000);                                       \
  }
#define ilog(...) \
  { fprintf(stderr, __VA_ARGS__); }
#define llog(...)      \
  {                    \
    log_prefix();      \
    ilog(__VA_ARGS__); \
  }
#define log(...)           \
  {                        \
    log_prefix();          \
    ilog(__VA_ARGS__);     \
    fprintf(stderr, "\n"); \
  }
#define red(...)       \
  {                    \
    log_prefix();      \
    ilog("\033[31m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define green(...)     \
  {                    \
    log_prefix();      \
    ilog("\033[32m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define blue(...)      \
  {                    \
    log_prefix();      \
    ilog("\033[34m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }

#else

#define log_prefix() ;
#define ilog(...) ;
#define llog(...) ;
#define log(...) ;
#define red(...) ;
#define green(...) ;
#define blue(...) ;

#endif

namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 1e6 + 5;
int n;
char a[N];
int b[N];

int binom(int n, int m) {
  if ((n & m) == m) return 1;
  return 0;
}

int main() {
  scanf("%d", &n);
  scanf("%s", a + 1);
  FOR(i, 1, n - 1) b[i] = abs(a[i] - a[i + 1]);
  --n;
  if (n == 1) {
    printf("%d\n", b[1]);
    return 0;
  }

  bool flag = 1;
  FOR(i, 1, n) if (b[i] == 1) flag = 0;
  if (flag) { //只有0，2
    int res = 0;
    FOR(i, 1, n) {
      int coef = binom(n - 1, i - 1);
      res ^= coef * b[i];
    }
    printf("%d\n", res);
    return 0;
  }
  //有1，则答案不可能为2。所以所有的2都可以变成0
  FOR(i, 1, n) if (b[i] == 2) b[i] = 0;
  int res = 0;
  FOR(i, 1, n) {
    int coef = binom(n - 1, i - 1);
    res ^= coef * b[i];
  }
  printf("%d\n", res);
  return 0;
}
