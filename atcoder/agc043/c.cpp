// by Sshwy
//#define DEBUGGER
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

#ifdef DEBUGGER

#define log_prefix()                                                \
  {                                                                 \
    fprintf(stderr, "\033[37mLine %-3d [%dms]:\033[0m  ", __LINE__, \
        (int)clock() / 1000);                                       \
  }
#define ilog(...) \
  { fprintf(stderr, __VA_ARGS__); }
#define llog(...)      \
  {                    \
    log_prefix();      \
    ilog(__VA_ARGS__); \
  }
#define log(...)           \
  {                        \
    log_prefix();          \
    ilog(__VA_ARGS__);     \
    fprintf(stderr, "\n"); \
  }
#define red(...)       \
  {                    \
    log_prefix();      \
    ilog("\033[31m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define green(...)     \
  {                    \
    log_prefix();      \
    ilog("\033[32m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define blue(...)      \
  {                    \
    log_prefix();      \
    ilog("\033[34m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }

#else

#define log_prefix() ;
#define ilog(...) ;
#define llog(...) ;
#define log(...) ;
#define red(...) ;
#define green(...) ;
#define blue(...) ;

#endif

namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 1e5 + 5, P = 998244353;
int pw(int a, int m) {
  int res = 1;
  while (m) m & 1 ? res = 1ll * res * a % P : 0, a = 1ll * a * a % P, m >>= 1;
  return res;
}
int n, m;
vector<int> g[N];
bool vis[N];
long long s[4][2];

int main() {
  scanf("%d", &n);
  const int C = pw(10, 18);
  long long c1 = 1, c0 = 1;
  FOR(id, 1, 3) {
    scanf("%d", &m);
    FOR(i, 1, n) g[i].clear();
    FOR(i, 1, m) {
      int u, v;
      scanf("%d%d", &u, &v);
      if (u > v) swap(u, v);
      g[u].pb(v);
    }
    memset(vis, 0, sizeof(vis));
    ROF(u, n, 1) {
      vis[u] = 1;
      for (int v : g[u])
        if (vis[v]) vis[u] = 0;
      if (vis[u]) {
        s[id][1] = (s[id][1] + pw(C, u) % P) % P;
        printf("s[%d][%d]+= C^%d\n", id, 1, u);
      }
    }
    memset(vis, 0, sizeof(vis));
    ROF(u, n - 1, 1) {
      vis[u] = 1;
      for (int v : g[u])
        if (vis[v]) vis[u] = 0;
      if (vis[u]) {
        s[id][0] = (s[id][0] + pw(C, u) % P) % P;
        printf("s[%d][%d]+= C^%d\n", id, 0, u);
      }
    }
    printf("s[%d][%d]=%lld\n", id, 0, s[id][0]);
    printf("s[%d][%d]=%lld\n", id, 1, s[id][1]);
  }

  long long x = (s[1][1] * s[2][1] + s[1][0] * s[2][0]) % P,
            y = (s[1][1] * s[2][0] + s[1][0] * s[2][1]) % P;
  long long X = (x * s[3][1] + y * s[3][0]) % P;
  printf("%lld\n", X);

  return 0;
}
