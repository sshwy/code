// by Sshwy
//#define DEBUGGER
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

#ifdef DEBUGGER

#define log_prefix()                                                \
  {                                                                 \
    fprintf(stderr, "\033[37mLine %-3d [%dms]:\033[0m  ", __LINE__, \
        (int)clock() / 1000);                                       \
  }
#define ilog(...) \
  { fprintf(stderr, __VA_ARGS__); }
#define llog(...)      \
  {                    \
    log_prefix();      \
    ilog(__VA_ARGS__); \
  }
#define log(...)           \
  {                        \
    log_prefix();          \
    ilog(__VA_ARGS__);     \
    fprintf(stderr, "\n"); \
  }
#define red(...)       \
  {                    \
    log_prefix();      \
    ilog("\033[31m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define green(...)     \
  {                    \
    log_prefix();      \
    ilog("\033[32m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define blue(...)      \
  {                    \
    log_prefix();      \
    ilog("\033[34m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }

#else

#define log_prefix() ;
#define ilog(...) ;
#define llog(...) ;
#define log(...) ;
#define red(...) ;
#define green(...) ;
#define blue(...) ;

#endif

namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 105;
int n, m;
char a[N][N];
int f[N][N];

int main() {
  cin >> n >> m;
  FOR(i, 1, n) cin >> a[i] + 1;
  FOR(i, 1, n) FOR(j, 1, m) {
    if (i == 1 && j == 1)
      f[i][j] = 1;
    else if (i == 1)
      f[i][j] = f[i][j - 1] + (a[i][j] != a[i][j - 1]);
    else if (j == 1)
      f[i][j] = f[i - 1][j] + (a[i - 1][j] != a[i][j]);
    else
      f[i][j] = min(f[i - 1][j] + (a[i - 1][j] != a[i][j]),
          f[i][j - 1] + (a[i][j - 1] != a[i][j]));
  }
  if (a[1][1] == '.' && a[n][m] == '.') {
    cout << f[n][m] / 2;
  } else if (a[1][1] == '.' && a[n][m] == '#') {
    cout << (f[n][m] + 1) / 2;
  } else if (a[1][1] == '#' && a[n][m] == '.') {
    cout << (f[n][m] + 1) / 2;
  } else {
    cout << (f[n][m] + 2) / 2;
  }
  return 0;
}
