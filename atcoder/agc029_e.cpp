#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 2e5 + 5;

int n;
vector<int> g[N];
map<int, int> q[N];

int Q(int u, int x, int p) {
  if (q[u].count(x)) return q[u][x];
  int res = 0;
  for (int v : g[u])
    if (v != p) {
      if (v <= x) res += Q(v, x, u) + 1;
    }
  q[u][x] = res;
  return res;
}
int ans[N], m[N];
void dfs(int u, int p) {
  if (u > m[p]) {
    ans[u] = ans[p] + Q(u, m[u], p) + 1;
  } else {
    ans[u] = ans[p] + Q(u, m[u], p) - Q(u, m[p], p);
  }
  for (int v : g[u])
    if (v != p) {
      m[v] = max(m[u], u);
      dfs(v, u);
    }
}

int main() {
  scanf("%d", &n);
  FOR(i, 1, n - 1) {
    int a, b;
    scanf("%d%d", &a, &b);
    g[a].pb(b), g[b].pb(a);
  }
  dfs(1, 0);
  FOR(i, 2, n) printf("%d%c", ans[i] - 1, " \n"[i == n]);
  return 0;
}
