#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 3e5 + 5, P = 1e9 + 7;

int n;
char s[N];

int Q[N][2];
vector<int> S = {0, 1, 2, 4, 3, 5, 6, 7}; // 7

int f[N][8];

int main() {
  scanf("%s", s + 1);
  n = strlen(s + 1);
  Q[0][1] = 2;
  Q[0][0] = 1;
  Q[2][1] = 3;
  Q[2][0] = 4;
  Q[1][1] = 5;
  Q[1][0] = 6;
  Q[6][1] = 1;
  Q[6][0] = 1;
  Q[5][1] = 2;
  Q[5][0] = 1;
  Q[4][1] = 2;
  Q[4][0] = 7;
  Q[3][1] = 3;
  Q[3][0] = 3;
  Q[7][1] = 4;
  Q[7][0] = 4;

  f[0][0] = 1;
  FOR(i, 0, n) {
    if (s[i + 1] != '1') {
      for (int x : S) { (f[i + 1][Q[x][0]] += f[i][x]) %= P; }
    }
    if (s[i + 1] != '0') {
      for (int x : S) { (f[i + 1][Q[x][1]] += f[i][x]) %= P; }
    }
  }
  printf("%d\n", (f[n][2] + f[n][3]) % P);
  return 0;
}
