#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
typedef long long LL;
const int SZ = 1e6 + 5, N = 505;

int n;

bool bp[SZ];
LL pn[SZ], lp;
void sieve() {
  bp[0] = bp[1] = 1;
  FOR(i, 2, SZ - 1) {
    if (!bp[i]) pn[++lp] = i;
    if (lp >= 1000) break;
    FOR(j, 1, lp) {
      int pj = pn[j];
      if (i * pj >= SZ) break;
      bp[i * pj] = 1;
      if (i % pj == 0) break;
    }
  }
}
LL gcd(LL a, LL b) { return b ? gcd(b, a % b) : a; }
LL lcm(LL a, LL b) {
  if (a == 0) a = 1;
  if (b == 0) b = 1;
  return a / gcd(a, b) * b;
}
LL lcm(LL a, LL b, LL c, LL d) { return lcm(lcm(a, b), lcm(c, d)); }
LL a[N][N], b[N][N];

int main() {
  scanf("%d", &n);
  if (n == 2) return puts("4 7\n23 10"), 0;
  sieve();
  int pos = 0;
  for (int i = 1; i <= n; i += 2) a[i][1] = pn[++pos];
  for (int i = n & 1 ? 1 : 2; i <= n; i += 2) a[n][i] = pn[++pos];
  for (int i = 1; i <= n; i += 2) b[1][i] = pn[++pos];
  for (int i = 1; i <= n; i += 2) b[i][1] = pn[++pos];
  ROF(i, n - 1, 1) FOR(j, 2, n) a[i][j] = a[i + 1][j - 1];
  FOR(i, 2, n) FOR(j, 2, n) b[i][j] = b[i - 1][j - 1];
  FOR(i, 1, n) FOR(j, 1, n) a[i][j] *= b[i][j];
  FOR(i, 1, n) FOR(j, 1, n) if (!a[i][j]) {
    a[i][j] = lcm(a[i - 1][j], a[i + 1][j], a[i][j - 1], a[i][j + 1]) + 1;
  }
  FOR(i, 1, n) FOR(j, 1, n) printf("%lld%c", a[i][j], " \n"[j == n]);
  return 0;
}
