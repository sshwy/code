#include <cstdio>
#include <cstdlib>
#include <ctime>
using namespace std;

int main() {
  system("g++ agc026_e.cpp -o .usr2");
  system("g++ a2.cpp -o .std2");
  system("g++ g2.cpp -o .gen2");
  int t = 0;
  while (++t) {
    system("./.gen2 > .fin2");
    system("./.usr2 < .fin2 > .fout2");
    system("./.std2 < .fin2 > .fstd2");
    if (system("diff -w .fout2 .fstd2")) break;
    printf("AC#%d\n", t);
  }
  printf("WA#%d\n", t);
  return 0;
}
