#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 2e6 + 6, SZ = 5e6 + 6;

int n, m, k;
char s[N], t[N];

struct node {
  node *pre, *nex;
  int x, y, c; //(x,y) x c
  node() { nex = NULL, pre = NULL, x = y = c = 0; }
  node(node *_pre, node *_nex, int _x, int _y, int _c) {
    pre = _pre, nex = _nex, x = _x, y = _y, c = _c;
  }
} BIN[SZ];
int lb;

node *head = &(BIN[0] = node(NULL, NULL, 0, 0, 1));

node *insert_pre(node *u, int x, int y) { //在u前面插入(x,y)
  if (u == head) {
    BIN[++lb] = node(NULL, u, x, y, 1);
    head = BIN + lb;
  } else {
    if (u->pre != NULL && u->pre->x == x && u->pre->y == y) {
      ++u->pre->c;
      return u->pre;
    }
    BIN[++lb] = node(u->pre, u, x, y, 1);
    if (u->pre != NULL) u->pre->nex = BIN + lb;
  }
  u->pre = BIN + lb;
  return BIN + lb;
}
void push_front(int x, int y) { //插入一个二元组
  int cnt = k - 1;              //剩余进位次数
  if (x && y) {                 //要进位
    // x,y表示进位的状态
    for (node *cur = head; cur != NULL; cur = cur->nex) {
      if (x && y) {
        if (cur->x && cur->y) { //(1,1) + (1,1)
          puts("ERROR");        //倒序处理的，不可能追上
          exit(1);
        } else if (cur->x) { //(1,0) + (1,1)
          if (cur->c == 1) {
            cur->x = 0;
            cur->y = 1;
            x = 1, y = 0;
          } else {
            node *add = insert_pre(cur, 0, 1);
            --cur->c; //拆一个出来，个数减一
            x = 1, y = 0;
            cur = add;
          }
        } else if (cur->y) { //(0,1) + (1,1);
          if (cur->c == 1) {
            cur->x = 1;
            cur->y = 0;
            x = 0, y = 1;
          } else {
            node *add = insert_pre(cur, 1, 0);
            --cur->c; //拆一个出来，个数减一
            x = 0, y = 1;
            cur = add;
          }
        } else {     //放下来 or 不放？ (0,0) + (1,1)
          if (cnt) { //不放，继续进位
            --cnt;
            if (cur->c == 1) {
              cur->x = 0;
              cur->y = 0;
              x = 1, y = 1;
            } else {
              node *add = insert_pre(cur, 0, 0);
              --cur->c; //拆一个出来，个数减一
              x = 1, y = 1;
              cur = add;
            }
          } else { //放下来
            if (cur->c == 1) {
              cur->x = 1;
              cur->y = 1;
              x = 0, y = 0;
            } else {
              node *add = insert_pre(cur, 1, 1);
              --cur->c; //拆一个出来，个数减一
              x = 0, y = 0;
              cur = add;
            }
          }
        }
      } else if (x) {
        if (cur->x && cur->y) { //(1,1) + (1,0)
          puts("ERROR");        //倒序处理的，不可能追上
          exit(1);
        } else if (cur->x) { //(1,0) + (1,0)
          cur->x = 0;
          cur->y = 0;
          x = 1, y = 0;
        } else if (cur->y) { //(0,1) + (1,0)
          if (cnt) {
            --cnt;
            if (cur->c == 1) {
              cur->x = 0;
              cur->y = 0;
              x = 1, y = 1;
            } else {
              node *add = insert_pre(cur, 0, 0);
              --cur->c; //拆一个出来，个数减一
              x = 1, y = 1;
              cur = add;
            }
          } else {
            if (cur->c == 1) {
              cur->x = 1;
              cur->y = 1;
              x = 0, y = 0;
            } else {
              node *add = insert_pre(cur, 1, 1);
              --cur->c; //拆一个出来，个数减一
              x = 0, y = 0;
              cur = add;
            }
          }
        } else { // (0,0) + (1,0)
          if (cur->c == 1) {
            cur->x = 1;
            cur->y = 0;
            x = 0, y = 0;
          } else {
            insert_pre(cur, 1, 0);
            --cur->c;     //拆一个出来，个数减一
            x = 0, y = 0; // cur=BIN+lb;
          }
        }
      } else if (y) {
        if (cur->x && cur->y) { //(1,1) + (1,0)
          puts("ERROR");        //倒序处理的，不可能追上
          exit(1);
        } else if (cur->x) { //(1,0) + (0,1)
          if (cnt) {
            --cnt;
            if (cur->c == 1) {
              cur->x = 0;
              cur->y = 0;
              x = 1, y = 1;
            } else {
              node *add = insert_pre(cur, 0, 0);
              --cur->c; //拆一个出来，个数减一
              x = 1, y = 1;
              cur = add;
            }
          } else {
            if (cur->c == 1) {
              cur->x = 1;
              cur->y = 1;
              x = 0, y = 0;
            } else {
              node *add = insert_pre(cur, 1, 1);
              --cur->c; //拆一个出来，个数减一
              x = 0, y = 0;
              cur = add;
            }
          }
        } else if (cur->y) { //(0,1) + (0,1)
          cur->x = 0;
          cur->y = 0;
          x = 0, y = 1;
        } else { //(0,0) + (0,1)
          if (cur->c == 1) {
            cur->x = 0;
            cur->y = 1;
            x = 0, y = 0;
          } else {
            insert_pre(cur, 0, 1);
            --cur->c;     //拆一个出来，个数减一
            x = 0, y = 0; // cur=BIN+lb;
          }
        }
      } else
        break;
      if (cur->nex == NULL) { //高位补0
        BIN[++lb] = node(cur, NULL, 0, 0, 1);
        cur->nex = BIN + lb;
      }
    }
    //记得插入一个00
    insert_pre(head, 0, 0);
  } else {
    if (head->x == x && head->y == y)
      head->c++;
    else
      insert_pre(head, x, y);
  }
}

int ls, lt;
void getans(node *cur) {
  if (cur == NULL) return;
  getans(cur->nex);
  FOR(_, 1, cur->c) s[ls++] = cur->x + '0';
  FOR(_, 1, cur->c) t[lt++] = cur->y + '0';
}

int main() {
  // freopen(".fin","r",stdin);
  scanf("%d%d%d", &n, &m, &k);
  scanf("%s", s + 1);
  scanf("%s", t + 1);
  reverse(s + 1, s + n + 1);
  reverse(t + 1, t + m + 1);
  FOR(i, 1, n) s[i] -= '0';
  FOR(i, 1, m) t[i] -= '0';
  ROF(i, max(n, m), 1) { //依次处理进位
    push_front(s[i], t[i]);
  }
  getans(head);
  s[ls] = 0, t[lt] = 0;
  char *S = s, *T = t;
  while (S[0] == '0') ++S;
  while (T[0] == '0') ++T;
  printf("%s\n%s\n", S, T);
  return 0;
}
