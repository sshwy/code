#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 2e5 + 5;

int n;
int p[N], x;
struct node {
  long long u, c[2];
  bool operator<(const node &d) const {
    return c[0] * d.c[1] == c[1] * d.c[0] ? u < d.u : c[0] * d.c[1] < c[1] * d.c[0];
  }
} a[N];

set<node> s;

int f[N];
void init(int lim) { FOR(i, 0, n) f[i] = i; }
int get(int u) { return f[u] == u ? u : f[u] = get(f[u]); }

long long ans;

int main() {
  scanf("%d", &n);
  FOR(i, 2, n) scanf("%d", &p[i]);
  FOR(i, 1, n) scanf("%d", &x), a[i].c[x]++;
  FOR(i, 1, n) a[i].u = i;
  FOR(i, 1, n) s.insert(a[i]);
  init(n);
  FOR(i, 1, n - 1) {
    node u = *s.rbegin();
    s.erase(u);
    if (get(p[u.u]) == 0) {
      --i;
      continue;
    }
    // printf("(%d,C0=%d,C1=%d)\n",u.u,u.c[0],u.c[1]);
    int fa = get(p[u.u]);
    node v = a[fa];
    // u合并到v上
    s.erase(v);
    a[fa] = {v.u, v.c[0] + u.c[0], v.c[1] + u.c[1]};
    f[u.u] = v.u; // merge
    ans += v.c[1] * 1ll * u.c[0];
    s.insert(a[fa]);
  }
  assert(s.size() == 1);
  printf("%lld\n", ans);
  return 0;
}
