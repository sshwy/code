#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 2030;

int n, m;
vector<int> g[N];
vector<pii> path[N], g_lim[N];
int dep[N], fa[N];
int u[N], v[N], w[N];

void dfs(int u, int p) {
  dep[u] = dep[p] + 1, fa[u] = p;
  for (int v : g[u])
    if (v != p) dfs(v, u);
}
int lca(int x, int y) {
  while (x != y) {
    if (dep[x] < dep[y]) swap(x, y);
    x = fa[x];
  }
  return x;
}
int pass[N][N], ans;
int mtc[N][2]; //这个点选择的两条链
void solve(int u, int p) {
  mtc[u][0] = mtc[u][1] = -1;
  FOR(i, 1, m) pass[u][i] = -1;
  for (int v : g[u])
    if (v != p) {
      solve(v, u);
      FOR(i, 1, m) if (pass[v][i] != -1) pass[u][i] = pass[v][i];
    }
  for (pii p : path[u]) pass[u][p.fi] = p.se; //继承的同时，ban掉一些以u为lca的边
  int cnt = 0;                                //有多少边经过u的父边
  FOR(i, 1, m) cnt += pass[u][i] != -1;
  if (cnt < 2) {
    ans += cnt;
  } else {
    ans += 2;
    for (int v : g[u])
      if (v != p) {
        if (mtc[v][0] != -1 && mtc[v][1] != -1 && pass[u][mtc[v][0]] != -1 &&
            pass[u][mtc[v][1]] != -1) { //两个都继承上来
          mtc[u][0] = mtc[v][0];
          mtc[u][1] = mtc[v][1]; //直接采用儿子的限制
          return;
        }
      }
    int x = -1, y = -1;
    FOR(i, 1, m) if (pass[u][i] != -1) x == -1 ? x = i : y = i;
    mtc[u][0] = x, mtc[u][1] = y;
    g_lim[x].pb({y, pass[u][x] ^ pass[u][y] ^ 1});
    g_lim[y].pb({x, pass[u][x] ^ pass[u][y] ^ 1});
  }
}
int rv[N], vis[N];
void dfs2(int u, int col) {
  if (vis[u]) return;
  rv[u] = col;
  vis[u] = 1;
  for (pii p : g_lim[u]) dfs2(p.fi, p.se ^ col);
}
int main() {
  scanf("%d%d", &n, &m);
  FOR(i, 1, n - 1) {
    int a, b;
    scanf("%d%d", &a, &b);
    g[a].pb(b), g[b].pb(a);
  }
  dfs(1, 0);
  FOR(i, 1, m) {
    scanf("%d %d", &u[i], &v[i]);
    w[i] = lca(u[i], v[i]);
    if (u[i] != w[i]) path[u[i]].pb({i, 0}); // 0:从u出发的边
    if (v[i] != w[i]) path[v[i]].pb({i, 1}); // 1:到达u的边
    path[w[i]].pb({i, -1});                  //经过u的边
  }
  solve(1, 0);
  FOR(i, 1, m) if (!vis[i]) dfs2(i, 0);
  printf("%d\n", ans);
  FOR(i, 1, m) {
    if (rv[i])
      printf("%d %d\n", v[i], u[i]);
    else
      printf("%d %d\n", u[i], v[i]);
  }
  return 0;
}
