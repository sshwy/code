#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
namespace IO {
  char nc() {
    static char bf[100000], *p1 = bf, *p2 = bf;
    return p1 == p2 && (p2 = (p1 = bf) + fread(bf, 1, 100000, stdin), p1 == p2)
               ? EOF
               : *p1++;
  }
  int rd() {
    int res = 0;
    char c = getchar();
    while (!isdigit(c)) c = getchar();
    while (isdigit(c)) res = res * 10 + c - '0', c = getchar();
    return res;
  }
} // namespace IO
/******************heading******************/
/*
 * 随机化决策
 * 控制深度（更新答案之间的间隙时间限制）
 * 游戏状态封装
 * BFS 容错
 */
const int dir[8][2] = {
    {0, 1}, {0, -1}, {1, 0}, {-1, 0}, {1, 1}, {1, -1}, {-1, 1}, {-1, -1}};
const char mv[9] = "lhjknbuy";
// char ans[1800]; int ans_tot;

int n, m; // 22,79

char a[25][85];
bool vis[25][85];
void chessbroad_print(int cx, int cy) {
  FOR(i, 1, n) FOR(j, 1, m) {
    char str[5] = {a[i][j] + '0', 0};
    printf("%s%s", i == cx && j == cy ? "\033[31m@\033[0m" : (vis[i][j] ? " " : str),
        (j == m ? "\n" : ""));
  }
}

struct game {     //{{{
  char ans[1800]; // current ans
  int tot, len;   // current len
  int sx, sy;
  game() {
    memset(ans, 0, sizeof(ans));
    tot = 0, len = 1;
  }
  void print() {
    printf("\033[32m%.2lf%%\033[0m , move = ", tot * 100.0 / (n * m));
    FOR(i, 1, len - 1) { printf("%c", mv[ans[i]]); }
    puts("");
  }
  bool operator<(game g) const { return tot < g.tot; }
  bool operator==(game g) const {
    if (tot != g.tot) return 0;
    // if(len!=g.len)return 0;
    // FOR(i,1,len-1)if(ans[i]!=g.ans[i])return 0;
    return 1;
  }
}; //}}}
game dfs_g;
/*
 * DFS的时侯存答案用的
 */

int sx, sy;
int dfs_cnt, last_time, dfs_lim;
/*
 * dfs_cnt: 递归次数
 * last_time: 上次更新答案的时间戳
 * dfs_lim: 递归次数限制，-1表示无限制
 */
char tmp[1800];
int tmptot, tmplen;

void print() {
  printf("\033[32m%.2lf%%\033[0m , move = ", tmptot * 100.0 / (n * m));
  FOR(i, 1, tmplen - 1) printf("%c", mv[tmp[i]]);
  puts("");
}
bool check(int x, int y) { return !(x < 1 || x > n || y < 1 || y > m || vis[x][y]); }
void make_choise(int x, int y, int *cho) {
  //把决策排序
  FOR(i, 0, 7) cho[i] = i;
  random_shuffle(cho, cho + 8);
}
void dfs(int x, int y, int k, int tot) { //{{{
  // FOR(i,1,k)printf(" ");
  // printf("\033[33mdfs(%d,%d,%d,%d)\033[0m\n",x,y,k,tot);
  dfs_cnt++;
  // if(~dfs_lim&&dfs_cnt-last_time>dfs_lim)return;
  if (~dfs_lim && dfs_cnt > dfs_lim) return;
  if (tot > dfs_g.tot) {
    tmplen = k, tmptot = tot;
    last_time = dfs_cnt;
    dfs_g.tot = tot, dfs_g.len = k, dfs_g.sx = x, dfs_g.sy = y;
    FOR(i, 0, k) dfs_g.ans[i] = tmp[i];
  }
  // print();
  // chessbroad_print(x,y);
  int cho[8];
  make_choise(x, y, cho);
  FOR(__, 0, 7) {
    int _ = cho[__];
    int nx = x + dir[_][0], ny = y + dir[_][1], step = a[nx][ny];
    if (!check(nx, ny)) continue;
    bool fl = 1;
    FOR(i, 1, step) {
      if (!check(x + dir[_][0] * i, y + dir[_][1] * i)) {
        fl = 0;
        break;
      }
    }
    if (!fl) continue;
    FOR(i, 1, step) vis[x + dir[_][0] * i][y + dir[_][1] * i] = 1;
    tmp[k] = _;
    dfs(x + dir[_][0] * step, y + dir[_][1] * step, k + 1, tot + step);
    FOR(i, 1, step) vis[x + dir[_][0] * i][y + dir[_][1] * i] = 0;
    if (~dfs_lim && dfs_cnt > dfs_lim) return;
  }
} //}}}
void go(int lim, int sx, int sy) { //{{{
  dfs_cnt = 0, dfs_lim = lim, last_time = 0;
  vis[sx][sy] = 1;
  dfs(sx, sy, dfs_g.len, dfs_g.tot);
} //}}}
void apply(game g, int &x, int &y) { //{{{
  memset(vis, 0, sizeof vis);
  // printf("apply:\n");
  // g.print();
  // printf("apply:x=%d,y=%d\n",x,y);
  memset(vis, 0, sizeof(vis));
  FOR(__, 1, g.len - 1) {
    int _ = g.ans[__];
    int l = a[x + dir[_][0]][y + dir[_][1]];
    FOR(i, 1, l) { vis[x + dir[_][0] * i][y + dir[_][1] * i] = 1; }
    x += dir[_][0] * l, y += dir[_][1] * l;
    // printf("apply:x=%d,y=%d\n",x,y);
  }
  // chessbroad_print(x,y);
} //}}}
void update(game &g, int lim) { //{{{
  game g1 = g;
  int x = sx, y = sy;
  // 获取局面
  apply(g1, x, y);
  assert(x == g1.sx);
  assert(y == g1.sy);
  dfs_g = g1;
  // 更新方案
  memset(tmp, 0, sizeof(tmp));
  tmplen = g1.len, tmptot = g1.tot;
  FOR(i, 1, g1.len - 1) tmp[i] = g1.ans[i];
  // 继续搜索
  go(lim, x, y);
  // 如果方案更优就更新
  if (dfs_g.tot > g.tot) g = dfs_g;
} //}}}
void read() {
  scanf("%d%d", &n, &m);
  FOR(i, 1, n) scanf("%s", a[i] + 1);
  FOR(i, 1, n) FOR(j, 1, m) if (a[i][j] != '@') a[i][j] -= '0';
}
void getstart(int &x, int &y) {
  FOR(i, 1, n) FOR(j, 1, m) if (a[i][j] == '@') return x = i, y = j, void();
}
game gstart, gans;

priority_queue<game> q;

void bfs(game gs, int lim) {
  int bfs_cnt = 0;
  while (q.size()) q.pop();
  q.push(gs);
  while (!q.empty()) {
    game cur = q.top();
    q.pop();
    // cur.print();
    if (gans < cur) gans = cur, gans.print();
    if (~lim && ++bfs_cnt > lim) break;
    FOR(i, 1, 15) {
      game x = cur;
      update(x, 35);
      if (x == cur) {
        // puts("Get the same!");
        continue;
      }
      q.push(x);
    }
  }
}

int main() {
  freopen("greed.in", "r", stdin);
  srand(clock());
  read();
  getstart(sx, sy);
  printf("sx=%d,sy=%d\n", sx, sy);
  gstart.sx = sx, gstart.sy = sy;
  gans = gstart;
  FOR(i, 1, 10) {
    printf("\033[31m Attempted #%d\n\033[0m", i);
    bfs(gstart, 500000);
  }
  printf("Final:\n");
  gans.print();
  // chessbroad_print(g.sx,g.sy);
  // printf("\033[31m%.2lf%%\033[0m,move = %s\n",ans_tot*100.0/(n*m),ans+1);
  return 0;
}
