#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
namespace IO {
  char nc() {
    static char bf[100000], *p1 = bf, *p2 = bf;
    return p1 == p2 && (p2 = (p1 = bf) + fread(bf, 1, 100000, stdin), p1 == p2)
               ? EOF
               : *p1++;
  }
  int rd() {
    int res = 0;
    char c = getchar();
    while (!isdigit(c)) c = getchar();
    while (isdigit(c)) res = res * 10 + c - '0', c = getchar();
    return res;
  }
} // namespace IO
/******************heading******************/
/*
 * 随机化决策
 */
int n, m; // 22,79
char a[200][200];
bool vis[200][200];

int dir[8][2] = {
    {0, 1}, {0, -1}, {1, 0}, {-1, 0}, {1, 1}, {1, -1}, {-1, 1}, {-1, -1}};
char mv[9] = "lhjknbuy";
char ans[40000], la;
int atot;
bool check(int x, int y) { return !(x < 1 || x > n || y < 1 || y > m || vis[x][y]); }

void make_choise(int x, int y, int *cho) {
  FOR(i, 0, 7) cho[i] = i;
  random_shuffle(cho, cho + 8);
}
void dfs(int x, int y, int k, int tot) {
  // printf("dfs(%d,%d)\n",x,y);
  if (tot > atot) {
    atot = tot;
    ans[k] = 0;
    printf("\033[stot=%.2lf%%,move= %s\033[u", atot * 100.0 / (n * m), ans + 1);
  }
  int cho[8];
  make_choise(x, y, cho);
  FOR(__, 0, 7) {
    // FOR(_,0,7){
    int _ = cho[__];
    int nx = x + dir[_][0], ny = y + dir[_][1];
    if (!check(nx, ny)) continue;
    // printf("nx=%d,ny=%d\n",nx,ny);
    bool fl = 1;
    FOR(i, 1, a[nx][ny]) {
      if (!check(x + dir[_][0] * i, y + dir[_][1] * i)) {
        fl = 0;
        break;
      }
    }
    if (!fl) continue;
    FOR(i, 1, a[nx][ny]) { vis[x + dir[_][0] * i][y + dir[_][1] * i] = 1; }
    ans[k] = mv[_];
    dfs(x + dir[_][0] * a[nx][ny], y + dir[_][1] * a[nx][ny], k + 1, tot + a[nx][ny]);
    FOR(i, 1, a[nx][ny]) { vis[x + dir[_][0] * i][y + dir[_][1] * i] = 0; }
  }
}
void go(int sx, int sy) {
  vis[sx][sy] = 1;
  FOR(i, 1, n) FOR(j, 1, m) a[i][j] -= '0';
  dfs(sx, sy, 1, 1);
}
int main() {
  freopen("greed.in", "r", stdin);
  srand(clock());
  scanf("%d%d", &n, &m);
  FOR(i, 1, n) { scanf("%s", a[i] + 1); }
  FOR(i, 1, n) FOR(j, 1, m) {
    if (a[i][j] == '@') {
      go(i, j);
      return 0;
    }
  }
  puts("\033[31mCan't find start point!\033[0m");
  return 0;
}
