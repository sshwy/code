#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
namespace IO {
  char nc() {
    static char bf[100000], *p1 = bf, *p2 = bf;
    return p1 == p2 && (p2 = (p1 = bf) + fread(bf, 1, 100000, stdin), p1 == p2)
               ? EOF
               : *p1++;
  }
  int rd() {
    int res = 0;
    char c = getchar();
    while (!isdigit(c)) c = getchar();
    while (isdigit(c)) res = res * 10 + c - '0', c = getchar();
    return res;
  }
} // namespace IO
/******************heading******************/
/*
 * 随机化决策
 * 控制深度（更新答案之间的间隙时间限制）
 */
const int dir[8][2] = {
    {0, 1}, {0, -1}, {1, 0}, {-1, 0}, {1, 1}, {1, -1}, {-1, 1}, {-1, -1}};
const char mv[9] = "lhjknbuy";

char ans[1800], la;
int ans_tot;

struct game {
  int n, m; // 22,79
  char a[25][85];
  bool vis[25][85];

  char tmp[1800], la;
  int atot, dfs_cnt, dfs_lim, a_time;
  bool check(int x, int y) {
    return !(x < 1 || x > n || y < 1 || y > m || vis[x][y]);
  }

  game() {
    memset(a, 0, sizeof(a));
    memset(vis, 0, sizeof(vis));
    la = atot = 0;
  }
  void make_choise(int x, int y, int *cho) {
    FOR(i, 0, 7) cho[i] = i;
    random_shuffle(cho, cho + 8);
  }
  void dfs(int x, int y, int k, int tot) {
    dfs_cnt++;
    if (~dfs_lim && dfs_cnt - a_time > dfs_lim) return;
    if (tot > atot) {
      atot = tot, a_time = dfs_cnt;
      tmp[k] = 0;
      printf("\033[32m%.2lf%%\033[0m,move = %s\n", atot * 100.0 / (n * m), tmp + 1);
      if (tot > ans_tot) {
        ans_tot = tot;
        FOR(i, 0, k) ans[i] = tmp[i];
      }
    }
    int cho[8];
    make_choise(x, y, cho);
    FOR(__, 0, 7) {
      int _ = cho[__];
      int nx = x + dir[_][0], ny = y + dir[_][1];
      if (!check(nx, ny)) continue;
      bool fl = 1;
      FOR(i, 1, a[nx][ny]) {
        if (!check(x + dir[_][0] * i, y + dir[_][1] * i)) {
          fl = 0;
          break;
        }
      }
      if (!fl) continue;
      FOR(i, 1, a[nx][ny]) { vis[x + dir[_][0] * i][y + dir[_][1] * i] = 1; }
      tmp[k] = mv[_];
      dfs(x + dir[_][0] * a[nx][ny], y + dir[_][1] * a[nx][ny], k + 1,
          tot + a[nx][ny]);
      FOR(i, 1, a[nx][ny]) { vis[x + dir[_][0] * i][y + dir[_][1] * i] = 0; }
      if (~dfs_lim && dfs_cnt > dfs_lim) return;
    }
  }
  void go() {
    dfs_cnt = 0, dfs_lim = -1, a_time = 0;
    atot = 0;
    memset(tmp, 0, sizeof(tmp));
    FOR(i, 1, n) FOR(j, 1, m) {
      if (a[i][j] == '@') {
        vis[i][j] = 1;
        dfs(i, j, 1, 1);
        return;
      }
    }
  }
  void go(int lim) { //限制次数
    dfs_cnt = 0, dfs_lim = lim, a_time = 0;
    atot = 0;
    memset(tmp, 0, sizeof(tmp));
    FOR(i, 1, n) FOR(j, 1, m) {
      if (a[i][j] == '@') {
        vis[i][j] = 1;
        dfs(i, j, 1, 1);
        return;
      }
    }
  }
  void read() {
    scanf("%d%d", &n, &m);
    FOR(i, 1, n) scanf("%s", a[i] + 1);
    FOR(i, 1, n) FOR(j, 1, m) if (a[i][j] != '@') a[i][j] -= '0';
  }
  void print() { printf("\n%.2lf%%,move = %s\n", atot * 100.0 / (n * m), tmp + 1); }
} g;

int main() {
  freopen("greed.in", "r", stdin);
  srand(clock());
  g.read();
  FOR(i, 1, 100) {
    printf("\033[31m Attempted #%d\n\033[0m", i);
    g.go(3000000);
  }
  printf(
      "\033[31m%.2lf%%\033[0m,move = %s\n", ans_tot * 100.0 / (g.n * g.m), ans + 1);
  return 0;
}
