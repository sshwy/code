// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

void go() {
  int a, b, c, d;
  int A = 0, B1 = 0, B2 = 0;
  scanf("%d%d%d%d", &a, &b, &c, &d);
  if (c != 0) {
    if (a == 0) {
      swap(a, c), swap(b, d), a = -a, b = -b, ++B1;
    } else if (d == 0) {
      swap(a, b), swap(c, d), b = -b, d = -d, ++B2;
    } else {
      swap(a, c), swap(b, d), a = -a, b = -b, ++B1;
      swap(a, b), swap(c, d), b = -b, d = -d, ++B2;
    }
  }
  if (a == -1) a = -a, b = -b, c = -c, d = -d, B2 += 2;
  A = b;
  printf("3\nB %d\nA %d\nB %d\n", B1, A, B2);
}

int main() {
  int t;
  scanf("%d", &t);
  FOR(i, 1, t) go();
  return 0;
}
