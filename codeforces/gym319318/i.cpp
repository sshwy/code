// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

const int N = 200;

char s[N], t[N];

int main() {
  scanf("%s%s", s, t);
  int ls = strlen(s);
  int lt = strlen(t);

  int cur = 0;
  FOR(i, 0, ls - 1) {
    if (s[i] == t[cur]) ++cur;
    if (cur == lt) break;
  }
  if (cur == lt)
    puts("Yes");
  else
    puts("No");
  return 0;
}
