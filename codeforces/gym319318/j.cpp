// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

const int N = 5e7 + 5;

bool co[N];
int cnt[N];
int pn[N], lp;

void sieve() {
  co[0] = co[1] = true;
  FOR(i, 2, N - 1) {
    if (!co[i]) pn[++lp] = i;
    FOR(j, 1, lp) {
      if (i * pn[j] >= N) break;
      co[i * pn[j]] = true;
      if (i % pn[j] == 0) break;
    }
  }
  FOR(i, 1, N - 1) cnt[i] = cnt[i - 1] + !co[i];
}

int main() {
  sieve();
  int l, k;
  scanf("%d%d", &l, &k);
  if (l + k * 2 - 1 >= N) {
    puts("No");
  } else {
    if (cnt[l + k * 2 - 1] - cnt[l - 1] > k) {
      puts("Yes");
    } else {
      puts("No");
    }
  }
  return 0;
}
