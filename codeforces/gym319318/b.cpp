// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

int n, m, k;

int main() {
  scanf("%d%d%d", &n, &m, &k);
  double ans = 0;
  if (k <= m) {
    ans += m - k + 1;
    k = m + 1;
  }
  if (k <= n) {
    if (n < 5e7) {
      FOR(i, k, n) {
        double t = m * 1.0 / i;
        ans += t;
        // printf("t %lf\n", t);
      }
    } else {
      ans += m * (log(n) - log(k - 1));
    }
  }
  printf("%.9lf\n", ans);
  return 0;
}
