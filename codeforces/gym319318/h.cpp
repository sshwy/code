#include <bits/stdc++.h>
using namespace std;
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
int n, m, ans;
void end() { printf("%d\n", ans); }
void go() {
  scanf("%d%d", &n, &m);
  m = n / m;
  ans = m + 1;
  if (n == m || m == 1) return end();
  ans = 3;
  return end();
}
int main() {
  int t;
  scanf("%d", &t);
  FOR(i, 1, t) go();
  return 0;
}
