// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define x first
#define y second
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

typedef long long LL;
typedef pair<LL, LL> pt;

const int N = 2e5 + 5;
const LL INF = 4e18;
const pt pINF(INF, INF);

int n;
LL ans;
pt A[N], a[N];

pt min(pt x, pt y) { return pt(min(x.x, y.x), min(x.y, y.y)); }
void getMax(LL &x, LL y) { x = max(x, y); }

void work1(int l, int mid, int r) {
  int j = mid + 1;
  ROF(i, mid, l) {
    if (a[i].x > a[j].x || a[i].y > a[j].y) continue;
    while (j < r && a[j + 1].x >= a[i].x && a[j + 1].y >= a[i].y) ++j;
    getMax(ans, a[i].x * a[i].y * (j - i + 1));
  }
}
int q[N], ql, qr;
void work2(int l, int mid, int r) {
  int lj = mid + 1, rj = mid;
  ql = 1, qr = 0;
  ROF(i, mid, l) {
#define X(j) (a[j].y)
#define Y(j) ((a[j].y) * j)
#define Vec(j1, j2) pt(X(j2) - X(j1), Y(j2) - Y(j1))
#define Det(p1, p2) (p1.x * p2.y - p2.x * p1.y)
#define Cross(j1, j2, j3) Det(Vec(j1, j2), Vec(j2, j3))
    auto add = [&](int j) {
      while (ql < qr && Cross(q[qr - 1], q[qr], j) <= 0) --qr;
      q[++qr] = j;
    };
    auto del = [&](int j) {
      while (ql <= qr && q[ql] <= j) ++ql;
    };
    auto eval = [&](int i, int j) {
      LL res = Y(j) - X(j) * (i - 1);
      return res;
    };
    while (rj < r && a[i].x <= a[rj + 1].x) add(++rj);
    while (lj <= rj && a[i].y < a[lj].y) del(lj++);

    while (ql < qr && eval(i, q[qr]) < eval(i, q[qr - 1])) { --qr; }
    if (ql <= qr) { getMax(ans, eval(i, q[qr]) * a[i].x); }
  }
}
void work(int l, int mid, int r) {
  work1(l, mid, r);
  work2(l, mid, r);
}
void solve(int l, int r) {
  if (l == r) {
    ans = max(ans, A[l].x * A[l].y);
    return;
  }
  int mid = (l + r) >> 1;
  solve(l, mid), solve(mid + 1, r);

  ROF(i, mid, l) a[i] = min(A[i], i < mid ? a[i + 1] : pINF);
  FOR(i, mid + 1, r) a[i] = min(A[i], i - 1 > mid ? a[i - 1] : pINF);

  work(l, mid, r);
  reverse(a + l, a + r + 1);
  work(l, l + r - mid - 1, r);
}
int main() {
  scanf("%d", &n);
  FOR(i, 1, n) {
    int x, y;
    scanf("%d%d", &x, &y);
    A[i] = {x, y};
  }

  solve(1, n);

  printf("%lld\n", ans);
  return 0;
}
