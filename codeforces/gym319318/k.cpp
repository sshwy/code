// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

const int N = 1e5 + 5;

int n, dep[N], fa[N];
vector<int> g[N];

void dfs(int u, int p) {
  dep[u] = dep[p] + 1;
  fa[u] = p;
  for (int v : g[u])
    if (v != p) { dfs(v, u); }
}
int main() {
  scanf("%d", &n);
  FOR(i, 1, n - 1) {
    int u, v;
    scanf("%d%d", &u, &v);
    g[u].pb(v);
    g[v].pb(u);
  }
  dfs(1, 0);
  int a, b;
  scanf("%d%d", &a, &b);

  int len = 0;
  if (dep[a] < dep[b]) swap(a, b);
  while (dep[a] > dep[b]) a = fa[a], ++len;
  while (a != b) a = fa[a], b = fa[b], len += 2;

  if (len & 1)
    puts("Yes");
  else
    puts("No");

  return 0;
}
