// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

const int N = 2e5 + 5;

int n, x[N], y[N];

long long ans;
int main() {
  scanf("%d", &n);
  FOR(i, 1, n) scanf("%d%d", x + i, y + i);
  FOR(i, 1, n) {
    int mx = x[i], my = y[i];
    FOR(j, i, n) {
      mx = min(mx, x[j]);
      my = min(my, y[j]);
      ans = max(ans, 1ll * mx * my * (j - i + 1));
    }
  }
  printf("%lld\n", ans);
  return 0;
}
