// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

const int W = 1e7 + 5;

bool co[W];
int pn[W], lp;
void sieve() {
  co[0] = co[1] = true;
  FOR(i, 2, W - 1) {
    if (co[i] == false) pn[++lp] = i;
    FOR(j, 1, lp) {
      if (i * pn[j] >= W) break;
      co[i * pn[j]] = true;
      if (i % pn[j] == 0) break;
    }
  }
}
int c(int n, int x) {
  // int res = 0;
  // FOR(i, 1, n) {
  //   int j = i;
  //   while(j % x == 0) ++res, j /= x;
  // }
  // return res;
  int res = 0;
  while (n) res += n / x, n /= x;
  return res;
}
int calc(int x, int l, int r) {
  int res = c(r, x) - c(l - 1, x);
  // printf("calc(%d, %d, %d) = %d\n", x, l, r, res);
  return res;
}

void go() {
  int l1, r1, l2, r2;
  scanf("%d%d%d%d", &l1, &r1, &l2, &r2);
  FOR(i, 1, lp) {
    if (pn[i] > r1 && pn[i] > r2) break;
    if (calc(pn[i], l1, r1) > calc(pn[i], l2, r2)) {
      puts("No");
      return;
    }
  }
  puts("Yes");
}

int main() {
  sieve();

  int t;
  scanf("%d", &t);
  FOR(i, 1, t) go();
  return 0;
}
