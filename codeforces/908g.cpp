// by Sshwy
//#define DEBUGGER
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

#ifdef DEBUGGER

#define log_prefix()                                                \
  {                                                                 \
    fprintf(stderr, "\033[37mLine %-3d [%dms]:\033[0m  ", __LINE__, \
        (int)clock() / 1000);                                       \
  }
#define ilog(...) \
  { fprintf(stderr, __VA_ARGS__); }
#define llog(...)      \
  {                    \
    log_prefix();      \
    ilog(__VA_ARGS__); \
  }
#define log(...)           \
  {                        \
    log_prefix();          \
    ilog(__VA_ARGS__);     \
    fprintf(stderr, "\n"); \
  }
#define red(...)       \
  {                    \
    log_prefix();      \
    ilog("\033[31m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define green(...)     \
  {                    \
    log_prefix();      \
    ilog("\033[32m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define blue(...)      \
  {                    \
    log_prefix();      \
    ilog("\033[34m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }

#else

#define log_prefix() ;
#define ilog(...) ;
#define llog(...) ;
#define log(...) ;
#define red(...) ;
#define green(...) ;
#define blue(...) ;

#endif

namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 1000, P = 1e9 + 7;
int n, f[N][N][2], g[N][N][2], m10[N];
// f: 个数
// g: 贡献
char a[N];

void add(int &x, int y) { x = (x + y % P) % P; }
int calc(int d) {
  // printf("\033[32mcalc(%d)\033[0m\n",d);
  memset(f, 0, sizeof(f));
  memset(g, 0, sizeof(g));
  f[0][0][1] = 1;
  FOR(i, 1, n) {
    FOR(j, 0, i) { // f[i,j]
      // f[i-1]没有达到上限
      FOR(k, 0, 9) {
        if (k < d) {
          add(f[i][j][0], f[i - 1][j][0]);
          add(g[i][j][0], g[i - 1][j][0]);
        } else {
          if (j > 0) {
            add(f[i][j][0], f[i - 1][j - 1][0]);
            add(g[i][j][0],
                (g[i - 1][j - 1][0] + f[i - 1][j - 1][0] * 1ll * m10[j - 1]) % P);
          }
        }
      }
      // f[i-1]达到上限
      FOR(k, 0, a[i] - 1) { //第i个数填k，没有到达上限
        if (k < d) {
          add(f[i][j][0], f[i - 1][j][1]);
          add(g[i][j][0], g[i - 1][j][1]);
        } else {
          if (j > 0) {
            add(f[i][j][0], f[i - 1][j - 1][1]);
            add(g[i][j][0],
                (g[i - 1][j - 1][1] + f[i - 1][j - 1][1] * 1ll * m10[j - 1]) % P);
          }
        }
      }
      //第i个数填a[i]，也达到上限
      if (a[i] < d) {
        add(f[i][j][1], f[i - 1][j][1]);
        add(g[i][j][1], g[i - 1][j][1]);
      } else {
        if (j > 0) {
          add(f[i][j][1], f[i - 1][j - 1][1]);
          add(g[i][j][1],
              (g[i - 1][j - 1][1] + f[i - 1][j - 1][1] * 1ll * m10[j - 1]) % P);
        }
      }
      // printf("g[长度为%d,>=d有%d位,是否达到上界%d]的方案数=%d\n",i,j,0,g[i][j][0]);
      // printf("g[长度为%d,>=d有%d位,是否达到上界%d]的方案数=%d\n",i,j,1,g[i][j][1]);
    }
  }
  int res = 0;
  FOR(i, 0, n) add(res, g[n][i][0] + g[n][i][1]);
  // printf("res %d\n",res);
  return res;
}

int main() {
  scanf("%s", a + 1);
  n = strlen(a + 1);

  m10[0] = 1;
  FOR(i, 1, n) m10[i] = m10[i - 1] * 10ll % P;
  FOR(i, 1, n) a[i] -= '0';

  int ans = 0;
  FOR(i, 1, 9) add(ans, calc(i));
  printf("%d\n", ans);

  return 0;
}
