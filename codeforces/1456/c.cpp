// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

const int N = 5e5 + 5;
int n, k;
int a[N], good[N], bad[N], lg, lb;
long long c[N], d[N];

int main() {
  scanf("%d%d", &n, &k);
  ++k;
  FOR(i, 0, n - 1) scanf("%d", a + i);
  sort(a, a + n);
  FOR(i, 0, n - 1) if (a[i] > 0) good[++lg] = a[i];
  else bad[++lb] = a[i];

  long long ans = 0, sum = 0, s2 = 0;
  FOR(i, 1, lg) s2 += good[i] * 1ll * (lg - i), sum += good[i];

  FOR(i, 1, lb) {
    c[i] = c[i - 1] + bad[i];
    d[i] = d[i - 1] + bad[i] * 1ll * i;
  }
  long long cur = 0;
  FOR(i, 1, lb) {
    cur += ((i - 1) / k) * 1ll * bad[i];
    ans = max(ans, cur + (d[lb] - d[i]) +
                       (c[lb] - c[i]) * (((i - 1) / k + 1) - (i + 1)) + s2 +
                       sum * (lb - i + (i - 1) / k + 1));
  }
  printf("%lld\n", ans);
  return 0;
}
