// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

const int N = 1e5 + 5;
int n, a[N];

int main() {
  scanf("%d", &n);
  FOR(i, 1, n) scanf("%d", a + i);
  FOR(i, 2, n - 1) {
    if ((a[i - 1] ^ a[i]) > a[i + 1]) {
      puts("1");
      return 0;
    }
    if (a[i - 1] > (a[i] ^ a[i + 1])) {
      puts("1");
      return 0;
    }
  }
  int ans = n + 5;
  FOR(i, 1, n) {
    int L = 0;
    FOR(j, i, n) {
      L ^= a[j];
      int R = 0;
      FOR(k, j + 1, n) {
        R ^= a[k];
        if (L > R) ans = min(ans, j - i + k - j - 1);
      }
    }
  }
  if (ans == n + 5) ans = -1;
  printf("%d\n", ans);
  return 0;
}
