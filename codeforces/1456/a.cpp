// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

const int N = 1e5 + 5;
char a[N];
int b[N + N];

void go() {
  int n, p, k, x, y;
  scanf("%d %d %d", &n, &p, &k);
  scanf("%s", a + 1);
  scanf("%d%d", &x, &y);
  fill(b + 1, b + n + n + 1, 0);
  ROF(i, n, 1) b[i] = b[i + k] + (a[i] == '0');
  long long ans = 1e18;
  FOR(i, 1, n) { // start from i
    if (i + p - 1 > n) break;
    ans = min(ans, b[i + p - 1] * 1ll * x + (i - 1) * 1ll * y);
  }
  printf("%lld\n", ans);
}
int main() {
  int t;
  scanf("%d", &t);
  FOR(i, 1, t) go();
  return 0;
}
