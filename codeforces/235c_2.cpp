#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
namespace IO {
  char nc() {
    static char bf[100000], *p1 = bf, *p2 = bf;
    return p1 == p2 && (p2 = (p1 = bf) + fread(bf, 1, 100000, stdin), p1 == p2)
               ? EOF
               : *p1++;
  }
  int rd() {
    int res = 0;
    char c = getchar();
    while (!isdigit(c)) c = getchar();
    while (isdigit(c)) res = res * 10 + c - '0', c = getchar();
    return res;
  }
} // namespace IO
/******************heading******************/
const int SZ = 1000;

char s[SZ];
int ls;

void query(char *x) {
  int lx = strlen(x + 1), res = 0;
  FOR(i, 1, ls - lx + 1) {
    bool f = 0;
    FOR(j, 1, lx) {
      bool ff = 1;
      FOR(k, 0, lx - 1) { ff &= s[i + k] == x[(j + k - 1) % lx + 1]; }
      f |= ff;
    }
    res += f;
  }
  printf("%d\n", res);
}

char x[SZ];
int n;

int main() {
  scanf("%s", s + 1);
  ls = strlen(s + 1);
  scanf("%d", &n);
  FOR(i, 1, n) {
    scanf("%s", x + 1);
    query(x);
  }
  return 0;
}
