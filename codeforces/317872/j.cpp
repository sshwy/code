// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

const int N = 2e5 + 5;
int n;
long long ans1, ans2, cnt0, tot0, mx1, mx2;

int main() {
  scanf("%d", &n);
  FOR(i, 1, n) {
    int a, b;
    scanf("%d%d", &a, &b);

    if (b) {
      int t = b ? a - b + 1 : 2e9;
      if (t > mx1)
        mx2 = mx1, mx1 = t;
      else if (t > mx2)
        mx2 = t;
      ans2 += a;
    } else {
      cnt0++;
      tot0 += a;
    }
  }
  ans1 = mx1;
  if (cnt0 == 1) {
    ans2 += tot0;
    ans1++;
  }
  if (cnt0 == 0) { ans1 = min(ans1, mx2 + 1); }
  printf("%lld %lld\n", ans1, ans2);
  return 0;
}
