// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

const int N = 5005;

int n;
long long s = 0, a[N];
double p[N], ans;

int main() {
  scanf("%d", &n);
  FOR(i, 1, n) {
    scanf("%lld", a + i);
    s += a[i];
  }
  sort(a + 1, a + n + 1);

  FOR(i, 1, n) {
    if ((n - i) & 1) { // gsb
      ROF(j, i, 1) { p[j] = p[j - 1] * (j - 1) / i + p[j] * (i - j) / i; }
    } else {
      p[i] = 1;
    }
  }

  FOR(i, 1, n) ans += p[i] * a[i];

  printf("%.9lf %.9lf\n", ans, s - ans);
  return 0;
}
