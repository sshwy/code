// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

const int N = 2e5 + 5;

int n;
int a[N], b[N], c[N], d[N];

bool check(double v) {
  long long p = 0, q = 0;
  FOR(i, 1, n) {
    double pi = d[i] - b[i], qi = c[i] - a[i];
    if (qi < 0) {
      qi = -qi, pi = -pi;
      swap(a[i], c[i]);
      swap(b[i], d[i]);
    }

    if (v * qi > pi) {
      p += b[i];
      q += a[i];
    } else {
      p += d[i];
      q += c[i];
    }
  }
  return v > 1.0 * p / q;
}
int main() {
  scanf("%d", &n);
  double l = 0, r = 0;
  FOR(i, 1, n) {
    scanf("%d%d%d%d", a + i, b + i, c + i, d + i);
    r = max(r, 1.0 * b[i] / a[i]);
    r = max(r, 1.0 * d[i] / c[i]);
  }

  const double eps = 1e-11;
  FOR(_, 1, 500) {
    // while(l + eps < r)  {
    double mid = (l + r) / 2;
    if (check(mid)) {
      r = mid;
    } else {
      l = mid;
    }
  }

  printf("%.9lf\n", l);
  return 0;
}
