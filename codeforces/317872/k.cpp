// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

const int N = 2e5 + 5;

int n, m;
pair<int, int> a[N], b[N];
void go() {
  scanf("%d%d", &n, &m);
  int la = 0, lb = 0;
  FOR(i, 1, n) {
    int ai, bi;
    scanf("%d%d", &ai, &bi);
    bi = min(bi, m);
    if (bi > ai)
      a[++la] = {ai, bi - ai};
    else
      b[++lb] = {bi, ai - bi};
  }
  sort(a + 1, a + la + 1);
  long long cur = m;
  FOR(i, 1, la) {
    if (a[i].first > cur) return puts("NO"), void();
    cur += a[i].second;
  }
  long long cur2 = 0, m2 = 0;
  sort(b + 1, b + lb + 1);
  FOR(i, 1, lb) {
    if (b[i].first > cur2) {
      m2 += b[i].first - cur2;
      cur2 = b[i].first;
    }
    cur2 += b[i].second;
  }
  if (cur2 > cur) return puts("NO"), void();
  puts("YES");
}

int main() {
  int t;
  scanf("%d", &t);
  FOR(i, 1, t) go();
  return 0;
}
