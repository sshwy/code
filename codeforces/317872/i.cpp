// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

int n, k;

#define x first
#define y second

vector<pair<int, int>> s;

void work(int n) {
  vector<pair<int, int>> v;
  if (n & 1) {
    ROF(i, n / 2 - 1, 0) { v.pb({1 + i, n - i}); }
    FOR(i, 1, n) {
      s.insert(s.end(), v.begin(), v.end());
      for (auto &e : v) e.x = e.x % n + 1, e.y = e.y % n + 1;
    }
  } else {
    v.pb({n / 2, n});
    ROF(i, (n - 1) / 2 - 1, 0) { v.pb({1 + i, n - 1 - i}); }
    FOR(i, 1, n - 1) {
      s.insert(s.end(), v.begin(), v.end());
      for (auto &e : v) e.x = e.x % (n - 1) + 1, e.y = e.y % (n - 1) + 1;
      v[0].y = n;
    }
  }
}
int main() {
  scanf("%d%d", &n, &k);
  work(n);
  int r = s.size() / k + !!(s.size() % k);
  printf("%d\n", r);
  FOR(i, 0, r - 1) {
    int l = i * k, r = min(int(s.size()), (i + 1) * k);
    printf("%d\n", r - l);
    FOR(j, l, r - 1) printf("%d %d\n", s[j].x, s[j].y);
  }
  return 0;
}
