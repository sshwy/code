// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

const int N = 5e5 + 5;

int n, L[N], R[N];
vector<int> g[N];
bool ok[N];

pair<int, int> V[10];

void pushup(int u, int p) {
  int lv = 0;

  ok[u] = true;
  L[u] = R[u] = u;

  for (int v : g[u])
    if (v != p) {
      ok[u] &= ok[v];
      L[u] = min(L[u], L[v]);
      R[u] = max(R[u], R[v]);

      V[++lv] = {L[v], R[v]};
    }

  if (!ok[u]) return;

  V[++lv] = {u, u};

  if (lv > 3)
    ok[u] = false;
  else {
    sort(V + 1, V + lv + 1);
    ok[u] = true;
    FOR(i, 2, lv) if (V[i - 1].second >= V[i].first) {
      ok[u] = false;
      break;
    }
    if (lv == 3 && V[2] != make_pair(u, u)) { ok[u] = false; }
  }
}
void dfs(int u, int p) {
  for (int v : g[u])
    if (v != p) { dfs(v, u); }
  pushup(u, p);
}
vector<int> ans;
void dfs2(int u, int p) {
  if (ok[u]) ans.pb(u);
  for (int v : g[u])
    if (v != p) {
      int Lu = L[u], Ru = R[u];
      int Lv = L[v], Rv = R[v];
      int OKu = ok[u], OKv = ok[v];

      pushup(u, v);
      pushup(v, 0);

      dfs2(v, u);

      L[u] = Lu, R[u] = Ru;
      L[v] = Lv, R[v] = Rv;
      ok[u] = OKu, ok[v] = OKv;
    }
}
int dg[N];
int main() {
  scanf("%d", &n);
  FOR(i, 1, n - 1) {
    int u, v;
    scanf("%d%d", &u, &v);
    g[u].pb(v);
    g[v].pb(u);
    dg[u]++;
    dg[v]++;
  }
  FOR(i, 1, n) if (dg[i] > 3) {
    puts("-1");
    return 0;
  }
  FOR(i, 1, n) L[i] = R[i] = i;

  dfs(1, 0);
  dfs2(1, 0);

  sort(ans.begin(), ans.end());

  if (ans.size()) {
    FOR(i, 0, ans.size() - 1) printf("%d%c", ans[i], " \n"[i + 1 == ans.size()]);
  } else {
    puts("-1");
  }
  return 0;
}
