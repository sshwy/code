#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
namespace IO {
  char nc() {
    static char bf[100000], *p1 = bf, *p2 = bf;
    return p1 == p2 && (p2 = (p1 = bf) + fread(bf, 1, 100000, stdin), p1 == p2)
               ? EOF
               : *p1++;
  }
  int rd() {
    int res = 0;
    char c = getchar();
    while (!isdigit(c)) c = getchar();
    while (isdigit(c)) res = res * 10 + c - '0', c = getchar();
    return res;
  }
} // namespace IO
/******************heading******************/
#define int lld
const int N = 50;

int n, m;
int ad[N];
int components, isolated;
bool vis[N];

bool has_odd_cycle;
int col[N];

void dfs(int u) {
  if (vis[u]) return;
  vis[u] = 1;
  for (int adu = ad[u]; adu; adu -= adu & -adu) {
    const int v = __builtin_ctzll(adu);
    if (u == v) continue;
    if (!vis[v]) col[v] = !col[u], dfs(v);
    if (col[v] == col[u]) has_odd_cycle = 1;
  }
}
map<int, int> f;
int dp(int mask) {
  if (!mask) return 1ll;
  if (f.count(mask)) return f[mask];
  int u = __builtin_ctzll(mask);
  return f[mask] = dp(mask - (1ll << u)) + dp(mask - (mask & ad[u]));
}
signed main() {
  ios::sync_with_stdio(false);
  cin.tie(0);
  cout.tie(0);
  cin >> n >> m;
  // scanf("%lld%lld",&n,&m);
  FOR(i, 1, m) {
    int x, y;
    cin >> x >> y;
    // scanf("%lld%lld",&x,&y);
    --x, --y;
    ad[x] |= 1ll << y;
    ad[y] |= 1ll << x;
  }
  FOR(i, 0, n - 1) {
    if (!ad[i]) isolated++;
    ad[i] |= 1ll << i;
  }
  FOR(i, 0, n - 1) {
    if (!vis[i]) components++, dfs(i);
  }
  int ans = 1ll << n;
  ans -= dp((1ll << n) - 1) * 2;
  ans -= 1ll << components;
  ans += 1ll << isolated + 1;
  if (!has_odd_cycle) ans += 1ll << components;
  if (!m) ans -= 1ll << n;
  cout << ans;
  // printf("%lld\n",ans);
  return 0;
}
// ans = f(012)-f(01)-f(12)-f(02)+f(0)+f(1)+f(2)-f()
// f(012)=2^n
// f(01)=f(12)
// f(02)=2^c
// f(0)=f(2)=2^i
// f(1)=![has unbipar]*2^c
// f()=[m==0]*2^n
// f表示边权属于该集合的方案数
