#include <bits/stdc++.h>
#define INF 999999999
using namespace std;
int n, l[16], tot;
int f[16][1 << 16];
bool vis[16][1 << 16];
string a[16];

struct data {
  int v, s;
};
queue<data> q;

int csp(string a, string b) {
  // cout<<"csp:"<<a<<","<<b<<":";
  int la = a.size(), lb = b.size();
  int l = min(la, lb);
  for (int i = l; i > 0; i--) {
    bool f = true;
    for (int j = 0; j < i; j++)
      if (a[la - i + j] != b[j]) {
        f = false;
        break;
      }
    if (f) {
      // cout<<i<<endl;
      return i;
    }
  }
  // cout<<0<<endl;
  return 0;
}
int main() {
  freopen("gym-101303Jin.cpp", "r", stdin);
  scanf("%d", &n);
  tot = n;
  for (int i = 1; i <= n; i++) {
    scanf("%d", &l[i]); // length
    for (int j = 1; j <= l[i]; j++) {
      char c;
      cin >> c;
      a[i].push_back(c);
    }
    for (int j = 1; j < i; j++) {
      if (a[j] == "")
        ;
      else if (a[i].find(a[j]) != string::npos)
        a[j] = "", tot--;
      else if (a[j].find(a[i]) != string::npos) {
        a[i] = "", tot--;
        break;
      }
    }
  }
  sort(a + 1, a + n + 1);
  reverse(a + 1, a + n + 1);

  int mxt = 1 << tot;
  for (int i = 0; i <= tot; i++)
    for (int j = 0; j < mxt; j++) f[i][j] = INF;

  f[0][0] = 0, vis[0][0] = 1, a[0] = "";
  q.push((data){0, 0});

  while (!q.empty()) {
    data k = q.front();
    q.pop();
    // printf("k:%d,%d\n",k.v,k.s);
    int nex = (~k.s) ^ ((~(k.s >> tot)) << tot), x;
    while (nex != 0) {
      x = nex & -nex, nex -= x;
      int j = (int)(log(x) / log(2)) + 1;
      int t = f[k.v][k.s] + a[j].size() - csp(a[k.v], a[j]);
      int ns = k.s + x;
      // printf("\t\tx:%d,j:%d,t:%d\n",x,j,t);
      if (f[j][ns] > t) {
        f[j][ns] = t;
        // printf("\tf[%d][%d]:%d\n",j,ns,t);
        if (vis[j][ns] == 0) {
          q.push((data){j, ns});
          vis[j][ns] = 1;
        }
      }
    }
  }
  int mn = INF;
  // printf("f:");
  for (int i = 1; i <= tot; i++) {
    // printf("%d ",f[i][mxt-1]);
    mn = min(mn, f[i][mxt - 1]);
  }
  // printf("\n");
  printf("%d", mn);
  return 0;
}
