// by Sshwy
//#define DEBUGGER
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

#ifdef DEBUGGER

#define log_prefix()                                                \
  {                                                                 \
    fprintf(stderr, "\033[37mLine %-3d [%dms]:\033[0m  ", __LINE__, \
        (int)clock() / 1000);                                       \
  }
#define ilog(...) \
  { fprintf(stderr, __VA_ARGS__); }
#define llog(...)      \
  {                    \
    log_prefix();      \
    ilog(__VA_ARGS__); \
  }
#define log(...)           \
  {                        \
    log_prefix();          \
    ilog(__VA_ARGS__);     \
    fprintf(stderr, "\n"); \
  }
#define red(...)       \
  {                    \
    log_prefix();      \
    ilog("\033[31m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define green(...)     \
  {                    \
    log_prefix();      \
    ilog("\033[32m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define blue(...)      \
  {                    \
    log_prefix();      \
    ilog("\033[34m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }

#else

#define log_prefix() ;
#define ilog(...) ;
#define llog(...) ;
#define log(...) ;
#define red(...) ;
#define green(...) ;
#define blue(...) ;

#endif

namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 1e5 + 5;
int n, m, a[N], b[N], ans[N];
long long sum;
pair<int, int> p[N];

int main() {
  cin >> n >> m;
  FOR(i, 1, m) cin >> a[i], p[i] = {a[i], i}, sum += a[i];
  if (sum < n) {
    cout << -1 << endl;
    return 0;
  }
  FOR(i, 1, m) {
    b[i] = i;
    if (b[i] + p[i].fi - 1 > n) {
      cout << -1 << endl;
      return 0;
    }
  }
  // FOR(i,1,m)printf("%d%c",p[i].fi," \n"[i==m]);
  // FOR(i,1,m)printf("%d%c",b[i]," \n"[i==m]);
  int las = n;
  ROF(i, m, 1) {
    if (b[i] + p[i].fi - 1 < las) {
      b[i] = las - p[i].fi + 1;
      las = b[i] - 1;
    } else
      break;
  }
  FOR(i, 1, m) ans[p[i].se] = b[i];
  FOR(i, 1, m) printf("%d%c", ans[i], " \n"[i == m]);
  return 0;
}
