// by Sshwy
//#define DEBUGGER
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

#ifdef DEBUGGER

#define log_prefix()                                                \
  {                                                                 \
    fprintf(stderr, "\033[37mLine %-3d [%dms]:\033[0m  ", __LINE__, \
        (int)clock() / 1000);                                       \
  }
#define ilog(...) \
  { fprintf(stderr, __VA_ARGS__); }
#define llog(...)      \
  {                    \
    log_prefix();      \
    ilog(__VA_ARGS__); \
  }
#define log(...)           \
  {                        \
    log_prefix();          \
    ilog(__VA_ARGS__);     \
    fprintf(stderr, "\n"); \
  }
#define red(...)       \
  {                    \
    log_prefix();      \
    ilog("\033[31m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define green(...)     \
  {                    \
    log_prefix();      \
    ilog("\033[32m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define blue(...)      \
  {                    \
    log_prefix();      \
    ilog("\033[34m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }

#else

#define log_prefix() ;
#define ilog(...) ;
#define llog(...) ;
#define log(...) ;
#define red(...) ;
#define green(...) ;
#define blue(...) ;

#endif

namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 2e5 + 5;
typedef pair<int, int> pii;

void go() {
  string s;
  vector<pii> v, v2, ans;
  int c[30] = {0};

  cin >> s;
  for (char &c : s) c -= 'a';

  int las = 0;
  for (int i = 1; i < s.size(); i++) {
    if (s[i] == s[i - 1]) {
      c[s[i]]++;
      v.push_back({i - las, s[i]});
      las = i;
    }
  }
  int rest = s.size() - las;

  int tot = v.size(), psum = 0, z = 0;
  for (auto x : v) {
    if (z) x.fi += z;
    if (v2.empty()) {
      v2.push_back(x), psum += x.fi, z = 0;
    } else {
      int x1 = v2.back().se, x2 = x.se;
      if (x1 == x2) {
        v2.push_back(x), psum += x.fi, z = 0;
      } else { //可以删
        c[x1]--, c[x2]--;
        int mx = *max_element(c, c + 26);
        bool flag = tot - 2 > 1 && mx * 2 > tot - 2;
        ++c[x1], ++c[x2];

        if (flag == 0) {
          c[x1]--, c[x2]--, tot -= 2;
          ans.pb({psum + 1, psum + x.fi});
          psum -= v2.back().fi;
          z = v2.back().fi;
          v2.pop_back();
        } else {
          v2.push_back(x), psum += x.fi, z = 0;
        }
      }
    }
  }

  // for(auto x:v)printf("(%d,%d) ",x.fi,x.se); puts("");

  if (z) rest += z;
  v = v2, tot = v.size(), psum = 0, z = 0;
  v2.clear();

  // for(auto x:v)printf("(%d,%d) ",x.fi,x.se); puts("");

  int X = max_element(c, c + 26) - c;
  for (auto x : v) {
    if (z) x.fi += z;
    if (v2.empty()) {
      v2.push_back(x), psum += x.fi, z = 0;
    } else {
      int x1 = v2.back().se, x2 = x.se;
      if (x1 == x2 || x1 != X && x2 != X) {
        v2.push_back(x), psum += x.fi, z = 0;
      } else { //可以删
        ans.pb({psum + 1, psum + x.fi});
        psum -= v2.back().fi;
        z = v2.back().fi;
        v2.pop_back();
      }
    }
  }
  if (z) rest += z;

  v = v2;
  for (auto x : v) ans.pb({1, x.fi});

  ans.pb({1, rest});

  cout << ans.size() << endl;
  for (auto x : ans) cout << x.fi << " " << x.se << endl;
}
int main() {
  int t;
  cin >> t;
  FOR(i, 1, t) go();
  return 0;
}
