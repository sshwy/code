// by Sshwy
//#define DEBUGGER
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

#ifdef DEBUGGER

#define log_prefix()                                                \
  {                                                                 \
    fprintf(stderr, "\033[37mLine %-3d [%dms]:\033[0m  ", __LINE__, \
        (int)clock() / 1000);                                       \
  }
#define ilog(...) \
  { fprintf(stderr, __VA_ARGS__); }
#define llog(...)      \
  {                    \
    log_prefix();      \
    ilog(__VA_ARGS__); \
  }
#define log(...)           \
  {                        \
    log_prefix();          \
    ilog(__VA_ARGS__);     \
    fprintf(stderr, "\n"); \
  }
#define red(...)       \
  {                    \
    log_prefix();      \
    ilog("\033[31m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define green(...)     \
  {                    \
    log_prefix();      \
    ilog("\033[32m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define blue(...)      \
  {                    \
    log_prefix();      \
    ilog("\033[34m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }

#else

#define log_prefix() ;
#define ilog(...) ;
#define llog(...) ;
#define log(...) ;
#define red(...) ;
#define green(...) ;
#define blue(...) ;

#endif

namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 100;
int f[N]; //最高位为i结束的序列的个数。
void go() {
  fill(f, f + N, 0);
  int d, m, n, s = 0;
  cin >> d >> m;
  f[0] = 1; // a[1]=1, n=1的情况
  s = f[0];
  FOR(i, 1, N - 1) {
    // a[i+1]的最高位是1<<i。则i+1有1<<i种可能
    if (d < (1 << i)) {
      n = i - 1;
      break;
    }
    int coef = min((1 << i + 1) - 1, d) - (1 << i) + 1;
    f[i] = s * 1ll * coef % m;
    f[i] = (f[i] + coef) % m; //新开一个序列
    // printf("coef %d\n",coef); printf("f %d %d\n",i,f[i]);
    s = (s + f[i]) % m;
  }
  int ans = 0;
  FOR(i, 0, n) ans = (ans + f[i]) % m;
  printf("%d\n", ans);
}
int main() {
  int t;
  cin >> t;
  FOR(i, 1, t) go();
  return 0;
}
