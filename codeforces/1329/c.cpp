// by Sshwy
//#define DEBUGGER
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

#ifdef DEBUGGER

#define log_prefix()                                                \
  {                                                                 \
    fprintf(stderr, "\033[37mLine %-3d [%dms]:\033[0m  ", __LINE__, \
        (int)clock() / 1000);                                       \
  }
#define ilog(...) \
  { fprintf(stderr, __VA_ARGS__); }
#define llog(...)      \
  {                    \
    log_prefix();      \
    ilog(__VA_ARGS__); \
  }
#define log(...)           \
  {                        \
    log_prefix();          \
    ilog(__VA_ARGS__);     \
    fprintf(stderr, "\n"); \
  }
#define red(...)       \
  {                    \
    log_prefix();      \
    ilog("\033[31m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define green(...)     \
  {                    \
    log_prefix();      \
    ilog("\033[32m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define blue(...)      \
  {                    \
    log_prefix();      \
    ilog("\033[34m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }

#else

#define log_prefix() ;
#define ilog(...) ;
#define llog(...) ;
#define log(...) ;
#define red(...) ;
#define green(...) ;
#define blue(...) ;

#endif

namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
#include <stack>
const int N = (1 << 21) + 5;
int a[N], dep[N], ban[N];
int n, m, lim, ans[N], la;

bool f(int u) {
  int lu = u << 1;
  int ru = u << 1 | 1;
  if (ban[u] == 1) return 0;
  if (a[lu] == 0 && a[ru] == 0) {
    if (u < (1 << m)) {
      ban[u] = 1;
      return 0;
    } else {
      a[u] = 0;
      return 1;
    }
  } else {
    if (a[lu] > a[ru]) {
      int t = a[lu];
      if (f(lu)) {
        a[u] = t;
        return 1;
      } else {
        ban[u] = 1;
        return 0;
      }
    } else {
      int t = a[ru];
      if (f(ru)) {
        a[u] = t;
        return 1;
      } else {
        ban[u] = 1;
        return 0;
      }
    }
  }
}
void work(int u) {
  if (a[u] == 0) return;
  if (ban[u] == 0)
    while (f(u)) ans[++la] = u;
  work(u << 1);
  work(u << 1 | 1);
}
void go() {
  cin >> n >> m;
  lim = (1 << n) - 1;
  fill(a, a + (lim + 1) * 2, 0);
  fill(ban, ban + (lim + 1), 0);
  FOR(i, 1, lim) cin >> a[i];
  dep[1] = 1;
  FOR(i, 2, lim) dep[i] = dep[i / 2] + 1;
  la = 0;
  work(1);
  long long sum = 0;
  FOR(i, 1, (1 << m) - 1) sum += a[i];
  cout << sum << endl;
  FOR(i, 1, la) cout << ans[i] << " \n"[i == la];
}
int main() {
  ios::sync_with_stdio(false);
  cin.tie(0);
  cout.tie(0);
  int t;
  cin >> t;
  FOR(i, 1, t) go();
  return 0;
}
