#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/

int n;
int a[1000], b[1000], lb;
int A, B;

int main() {
  scanf("%d", &n);
  FOR(i, 1, n) {
    int x;
    scanf("%d", &x);
    FOR(i, 1, x) scanf("%d", &a[i]);
    for (int i = 1, j = x; i < j; i++, j--) A += a[i], B += a[j];
    if (x & 1) b[++lb] = a[x / 2 + 1];
  }
  sort(b + 1, b + lb + 1);
  reverse(b + 1, b + lb + 1);
  FOR(i, 1, lb) i & 1 ? A += b[i] : B += b[i];
  printf("%d %d\n", A, B);
  return 0;
}
