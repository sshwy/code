#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
#define LL long long
const int N = 22, SZ = (1 << 21) + 1;

int n, nn;
char s[SZ];
LL a[SZ], b[SZ];

void fwt(LL *f, int len, int tag) {
  for (int j = 1; j < len; j <<= 1)
    for (int i = 0; i < len; i += j << 1)
      for (int k = i; k < i + j; k++) f[k + j] += f[k] * tag;
}
int main() {
  scanf("%d", &n);
  nn = 1 << n;
  scanf("%s", s);
  FOR(i, 0, nn - 1) a[i] = s[i] - '0';
  scanf("%s", s);
  FOR(i, 0, nn - 1) b[i] = s[i] - '0';

  FOR(i, 0, nn - 1) a[i] <<= __builtin_popcount(i) * 2;
  FOR(i, 0, nn - 1) b[i] <<= __builtin_popcount(i) * 2;

  fwt(a, nn, 1), fwt(b, nn, 1);
  FOR(i, 0, nn - 1) a[i] *= b[i];
  fwt(a, nn, -1);

  FOR(i, 0, nn - 1) a[i] >>= __builtin_popcount(i) * 2, a[i] &= 3;
  FOR(i, 0, nn - 1) printf("%lld", a[i]);
  return 0;
}
