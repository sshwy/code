#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int M = 5e4 + 5, SZ = 1 << 23;

int n, m, k;
int A[M], B[M], C[M], D[M];
struct segment {
  int l, r, h, typ;
} a[M * 2];
int la;
bool cmp(segment x, segment y) { return x.h < y.h; }

int lb;
int lc[SZ], rc[SZ], mn[SZ], mn_cnt[SZ], tag[SZ];
int new_node(int l, int r) {
  ++lb, lc[lb] = rc[lb] = 0, mn[lb] = 0, mn_cnt[lb] = r - l + 1, tag[lb] = 0;
  return lb;
}
void init(int l = 1, int r = 1e9) {
  lb = 0;
  new_node(l, r);
}
void node_add(int u, int v) { mn[u] += v, tag[u] += v; }
void pushdown(int u, int l, int r) {
  int mid = (l + r) >> 1;
  if (!lc[u]) lc[u] = new_node(l, mid);
  if (!rc[u]) rc[u] = new_node(mid + 1, r);
  if (tag[u] == 0) return;
  node_add(lc[u], tag[u]), node_add(rc[u], tag[u]);
  tag[u] = 0;
}
void pushup(int u) {
  if (mn[lc[u]] < mn[rc[u]])
    mn[u] = mn[lc[u]], mn_cnt[u] = mn_cnt[lc[u]];
  else if (mn[rc[u]] < mn[lc[u]])
    mn[u] = mn[rc[u]], mn_cnt[u] = mn_cnt[rc[u]];
  else
    mn[u] = mn[lc[u]], mn_cnt[u] = mn_cnt[lc[u]] + mn_cnt[rc[u]];
}
void add(int L, int R, int v, int u = 1, int l = 1, int r = 1e9) {
  if (L <= l && r <= R) return node_add(u, v);
  int mid = (l + r) >> 1;
  pushdown(u, l, r);
  if (L <= mid) add(L, R, v, lc[u], l, mid);
  if (mid < R) add(L, R, v, rc[u], mid + 1, r);
  pushup(u);
}
int query() {
  if (mn[1] > 0) return 1e9;
  return 1e9 - mn_cnt[1];
}

int work() { //返回奇偶性即可
  int pos = 1, res = 0;
  init();
  while (pos <= la) {
    int nex = pos;
    while (nex <= la && a[nex].h == a[pos].h) {
      add(a[nex].l, a[nex].r, a[nex].typ), ++nex;
    }
    res = res ^ (((a[nex].h - a[pos].h) & 1) * query());
    pos = nex;
  }
  return res;
}
int main() {
  cin >> n >> m >> k;
  FOR(i, 1, m) cin >> A[i] >> B[i] >> C[i] >> D[i];
  sort(a + 1, a + m + 1, cmp);
  int val = 0, ans = 0;
  while (k) {
    la = 0;
    FOR(i, 1, m) {
      a[++la] = (segment){B[i], D[i], A[i], 1};
      a[++la] = (segment){B[i], D[i], C[i] + 1, -1};
    }
    sort(a + 1, a + la + 1, cmp);
    int t = work();
    if (t & 1) ans = ans ^ (1 << (val - 1)) ^ (1 << val);

    k >>= 1;
    ++val;
    FOR(i, 1, m) {
      A[i] = (A[i] + 1) / 2;
      B[i] = (B[i] + 1) / 2;
      C[i] = (C[i]) / 2;
      D[i] = (D[i]) / 2;
    }
    int pos = 0;
    FOR(i, 1, m) {
      if (A[i] <= C[i] && B[i] <= D[i])
        ++pos, A[pos] = A[i], B[pos] = B[i], C[pos] = C[i], D[pos] = D[i];
    }
    m = pos;
  }
  puts(ans ? "Hamed" : "Malek");
  return 0;
}
