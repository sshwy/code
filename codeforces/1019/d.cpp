// by Sshwy
//#define DEBUGGER
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

#ifdef DEBUGGER

#define log_prefix()                                                \
  {                                                                 \
    fprintf(stderr, "\033[37mLine %-3d [%dms]:\033[0m  ", __LINE__, \
        (int)clock() / 1000);                                       \
  }
#define ilog(...) \
  { fprintf(stderr, __VA_ARGS__); }
#define llog(...)      \
  {                    \
    log_prefix();      \
    ilog(__VA_ARGS__); \
  }
#define log(...)           \
  {                        \
    log_prefix();          \
    ilog(__VA_ARGS__);     \
    fprintf(stderr, "\n"); \
  }
#define red(...)       \
  {                    \
    log_prefix();      \
    ilog("\033[31m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define green(...)     \
  {                    \
    log_prefix();      \
    ilog("\033[32m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define blue(...)      \
  {                    \
    log_prefix();      \
    ilog("\033[34m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }

#else

#define log_prefix() ;
#define ilog(...) ;
#define llog(...) ;
#define log(...) ;
#define red(...) ;
#define green(...) ;
#define blue(...) ;

#endif

namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 3000;
int n;
long long s;
typedef pair<int, int> point;
point p[N], a[N];
vector<point> v;
int pos[N], id[N];

point operator-(point x, point y) { return point(x.fi - y.fi, x.se - y.se); }
long long det(point x, point y) { return x.fi * 1ll * y.se - x.se * 1ll * y.fi; }
int main() {
  cin >> n >> s;
  s *= 2;
  FOR(i, 1, n) cin >> p[i].fi >> p[i].se;
  sort(p + 1, p + n + 1);
  FOR(i, 1, n) FOR(j, i + 1, n) {
    if (p[i].fi == p[j].fi && p[i].se > p[j].se)
      v.pb({j, i});
    else
      v.pb({i, j});
  }
  auto cmp = [](pair<int, int> x, pair<int, int> y) {
    return det(p[x.se] - p[x.fi], p[y.se] - p[y.fi]) > 0;
  };
  sort(v.begin(), v.end(), cmp);
  FOR(i, 1, n) a[i] = p[i], pos[i] = i, id[i] = i;
  for (auto e : v) {
    int u = e.fi, v = e.se;
    auto find = [](point x, point y, long long area) {
      int l = 1, r = n;
      // FOR(i,1,n)printf("%lld%c",det(y-x, a[i]-x)," \n"[i==n]);
      while (l < r) {
        int mid = (l + r) >> 1;
        if (det(y - x, a[mid] - x) < area)
          l = mid + 1;
        else
          r = mid;
      }
      if (det(y - x, a[l] - x) == area) {
        cout << "Yes" << endl
             << x.fi << " " << x.se << endl
             << y.fi << " " << y.se << endl
             << a[l].fi << " " << a[l].se << endl;
        return true;
      }
      return false;
    };
    if (find(p[u], p[v], s) || find(p[u], p[v], -s)) return 0;
    assert(a[pos[u]] == p[u]);
    assert(a[pos[v]] == p[v]);

    swap(id[pos[u]], id[pos[v]]);
    swap(a[pos[u]], a[pos[v]]);
    swap(pos[u], pos[v]);
  }
  cout << "No" << endl;
  return 0;
}
