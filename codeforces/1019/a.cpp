// by Sshwy
//#define DEBUGGER
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

#ifdef DEBUGGER

#define log_prefix()                                                \
  {                                                                 \
    fprintf(stderr, "\033[37mLine %-3d [%dms]:\033[0m  ", __LINE__, \
        (int)clock() / 1000);                                       \
  }
#define ilog(...) \
  { fprintf(stderr, __VA_ARGS__); }
#define llog(...)      \
  {                    \
    log_prefix();      \
    ilog(__VA_ARGS__); \
  }
#define log(...)           \
  {                        \
    log_prefix();          \
    ilog(__VA_ARGS__);     \
    fprintf(stderr, "\n"); \
  }
#define red(...)       \
  {                    \
    log_prefix();      \
    ilog("\033[31m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define green(...)     \
  {                    \
    log_prefix();      \
    ilog("\033[32m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define blue(...)      \
  {                    \
    log_prefix();      \
    ilog("\033[34m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }

#else

#define log_prefix() ;
#define ilog(...) ;
#define llog(...) ;
#define log(...) ;
#define red(...) ;
#define green(...) ;
#define blue(...) ;

#endif

namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 5000;
int n, m;
vector<int> a[N];
bool vis[N];
int main() {
  cin >> n >> m;
  FOR(i, 1, n) {
    int p, c;
    cin >> p >> c;
    a[p].pb(c);
  }
  FOR(i, 1, m) sort(a[i].begin(), a[i].end());
  long long ans = 1ll << 62;
  ROF(x, n, 1) { //拿到x票
    // printf("x %d\n",x);
    long long tot = 0;
    int cx = a[1].size();
    vector<int> v;
    FOR(i, 2, m) {
      if (int(a[i].size()) >= x) {
        FOR(j, 0, (int(a[i].size()) - x)) { tot += a[i][j], ++cx; }
      }
      // printf("a[%d]:%d\n",i,a[i].size());
      FOR(j, max(int(a[i].size()) - x + 1, 0), int(a[i].size()) - 1) {
        v.pb(a[i][j]);
      }
    }
    // puts("GG");
    // printf("cx %d\n",cx);
    if (vis[cx]) continue;
    if (cx >= x)
      ans = min(ans, tot), vis[cx] = 1;
    else {
      sort(v.begin(), v.end());
      // printf("v.size() %d\n",v.size());
      // printf("%d\n",x-cx-1);
      int lim = x - cx - 1;
      FOR(j, 0, lim) {
        tot += v[j], ++cx;
        // printf("v[%d]=%d\n",j,v[j]);
      }
      // printf("cx %d\n",cx);
      assert(x == cx);
      ans = min(ans, tot), vis[cx] = 1;
    }
  }
  cout << ans << endl;
  return 0;
}
