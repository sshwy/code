#include <cstdio>
#include <cstdlib>
#include <ctime>
using namespace std;

int main() {
  system("g++ e.cpp -O2 -o .usr");
  system("g++ e2.cpp -O2 -o .std");
  system("g++ e_gen.cpp -O2 -o .gen");
  int t = 0;
  while (++t) {
    system("./.gen > .fin");
    if (system("./.usr < .fin > .fout")) break;
    system("./.std < .fin > .fstd");
    if (system("diff -w .fout .fstd")) break;
    printf("AC#%d\n", t);
  }
  printf("WA#%d\n", t);
  return 0;
}
