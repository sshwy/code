// by Sshwy
//#define DEBUGGER
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

#ifdef DEBUGGER

#define log_prefix()                                                \
  {                                                                 \
    fprintf(stderr, "\033[37mLine %-3d [%dms]:\033[0m  ", __LINE__, \
        (int)clock() / 1000);                                       \
  }
#define ilog(...) \
  { fprintf(stderr, __VA_ARGS__); }
#define llog(...)      \
  {                    \
    log_prefix();      \
    ilog(__VA_ARGS__); \
  }
#define log(...)           \
  {                        \
    log_prefix();          \
    ilog(__VA_ARGS__);     \
    fprintf(stderr, "\n"); \
  }
#define red(...)       \
  {                    \
    log_prefix();      \
    ilog("\033[31m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define green(...)     \
  {                    \
    log_prefix();      \
    ilog("\033[32m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define blue(...)      \
  {                    \
    log_prefix();      \
    ilog("\033[34m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }

#else

#define log_prefix() ;
#define ilog(...) ;
#define llog(...) ;
#define log(...) ;
#define red(...) ;
#define green(...) ;
#define blue(...) ;

#endif

namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
namespace IO { // require C++11
  const int _BS = 200000;
  char _ob[_BS], *_z = _ob, _ib[_BS], *_x = _ib, *_y = _ib;
  // Input
  char _nc() {
    return _x == _y && (_y = (_x = _ib) + fread(_ib, 1, _BS, stdin), _x == _y)
               ? EOF
               : *_x++;
  }
  inline void rd(char &c) { c = _nc(); }
  void rd(char *s) {
    char c = _nc();
    while (!isblank(c) && isprint(c) && c != EOF) *s++ = c, c = _nc();
    *s = 0;
  }
  template <class T> void rd(T &res) { // unsigned/signed int/long long
    res = 0;
    char c = _nc(), flag = 0;
    while (!isdigit(c) && c != '-') c = _nc();
    if (c == '-') flag = 1, c = _nc();
    while (isdigit(c)) res = res * 10 + c - '0', c = _nc();
    if (flag) res = -res;
  }
  template <class T, class... A> void rd(T &hd, A &...rs) { rd(hd), rd(rs...); }
  // Output
  inline void flush() { fwrite(_ob, 1, _z - _ob, stdout), _z = _ob; }
  inline void wrch(char x) {
    if (_z == _ob + _BS - 1) flush();
    *_z = x, ++_z;
  }
  void wr(char *s) {
    while (*s) wrch(*s), ++s;
  }
  void wr(const char *s) {
    while (*s) wrch(*s), ++s;
  }
  template <class T> void wr(T x) { // unsigned/signed int/long long
    if (x == 0) return wrch('0'), void();
    if (x < 0) wrch('-'), x = -x;
    int _t[30], _lt = 0;
    while (x) _t[++_lt] = x % 10, x /= 10;
    while (_lt > 0) wrch(_t[_lt] + '0'), --_lt;
  }
  inline void wr(char x) { wrch(x); }
  template <class T, class... A> void wr(T hd, A... rs) { wr(hd), wr(rs...); }
  template <class... A> void wrln(A... ar) { wr(ar...), wrch('\n'); }
  struct _Flusher {
    ~_Flusher() { flush(); }
  } _flusher;
} // namespace IO
/******************heading******************/
using IO::rd;
using IO::wr;
using IO::wrln;

typedef long long gt;
typedef pair<gt, gt> point;
typedef vector<point> convex;

point operator+(const point x, const point y) {
  return point(x.fi + y.fi, x.se + y.se);
}
point operator-(const point x, const point y) {
  return point(x.fi - y.fi, x.se - y.se);
}
__int128 det(const point x, const point y) {
  return __int128(x.fi) * y.se - __int128(x.se) * y.fi;
}

void make_up_convex(vector<point> &S) { // 上凸壳 andrew
  sort(S.begin(), S.end());             // sort by x-axis
  convex C;
  for (auto p : S) {
    while (C.size() >= 2 && det(C.back() - *(C.end() - 2), p - *(C.end() - 2)) >= 0) {
      C.pop_back();
    }
    C.push_back(p);
  }
  S = C;
}

convex convex_minkowski_sum(
    convex x, convex y) { // 上凸壳的闵可夫斯基和（不是凸包！）
  assert(!x.empty() && !y.empty());
  convex res;
  res.push_back(x[0] + y[0]);
  const int sx = x.size(), sy = y.size();
  int cx = 0, cy = 0;
  while (cx + 1 < sx && cy + 1 < sy) {
    if (det((x[cx + 1] - x[cx]), (y[cy + 1] - y[cy])) <= 0)
      res.push_back(x[++cx] + y[cy]);
    else
      res.push_back(x[cx] + y[++cy]);
  }
  while (cx + 1 < sx) res.push_back(x[++cx] + y[cy]);
  while (cy + 1 < sy) res.push_back(x[cx] + y[++cy]);
  return res;
}

const int N = 5e5 + 5;
int n, real_n, m;

struct qxx {
  int nex, t, k, b;
} e[N * 2];
int h[N], le = 1;
void add_path(int f, int t, int k, int b) { e[++le] = {h[f], t, k, b}, h[f] = le; }

int cur[N], dg[N], sz[N], Cut[N * 2];
void expand(int u) { // transfer to binary tree
  if (dg[cur[u]] < 2)
    dg[cur[u]]++;
  else {
    ++n;
    add_path(cur[u], n, 0, 0);
    add_path(n, cur[u], 0, 0);
    cur[u] = n;
    dg[cur[u]]++;
  }
}

void Size(int u, int p) {
  sz[u] = 1;
  for (int i = h[u]; i; i = e[i].nex) {
    int v = e[i].t;
    if (!Cut[i] && v != p) Size(v, u), sz[u] += sz[v];
  }
}
int Core_edge(int u, int p, int T) {
  int res = -1;
  auto upd = [&T](int &x, int y) {
    if (y == -1) return;
    if (x == -1 || max(T - sz[e[x].t], sz[e[x].t]) > max(T - sz[e[y].t], sz[e[y].t]))
      x = y;
  };
  for (int i = h[u]; i; i = e[i].nex) {
    int v = e[i].t;
    if (!Cut[i] && v != p) {
      upd(res, i);
      int x = Core_edge(v, u, T);
      upd(res, x);
    }
  }
  return res;
}
void dfs(int u, int p, vector<point> &S, gt cur_k = 0, gt cur_b = 0) {
  S.pb({cur_k, cur_b});
  for (int i = h[u]; i; i = e[i].nex) {
    int v = e[i].t;
    if (!Cut[i] && v != p) { dfs(v, u, S, cur_k + e[i].k, cur_b + e[i].b); }
  }
}

convex Total;
void Solve(int u, int p) { // centroid decomposition
  Size(u, p);
  int ce = Core_edge(u, p, sz[u]);

  if (ce == -1) return;

  int x = e[ce ^ 1].t, y = e[ce].t;
  vector<point> A, B;
  dfs(x, y, A, e[ce].k, e[ce].b);
  dfs(y, x, B, 0, 0);

  make_up_convex(A);
  make_up_convex(B);
  auto C = convex_minkowski_sum(A, B);

  Total.insert(Total.end(), C.begin(), C.end());
  // make_up_convex(Total); 边insert边make，你就T了

  Cut[ce] = Cut[ce ^ 1] = 1;

  Solve(x, y);
  Solve(y, x);
}
int main() {
  rd(real_n, m);
  n = real_n;
  FOR(i, 1, n) cur[i] = i;
  FOR(i, 1, real_n - 1) {
    int u, v, a, b;
    rd(u, v, a, b);
    expand(u);
    expand(v);
    add_path(cur[u], cur[v], a, b);
    add_path(cur[v], cur[u], a, b);
  }
  Solve(1, 0);

  make_up_convex(Total);
  sort(Total.begin(), Total.end()); // sort by x-axis

  if (Total.empty()) Total.pb({0, 0});

  auto cur = Total.begin();
  auto f = [](point p, int x) { return x * p.fi + p.se; };
  vector<long long> ans;
  FOR(i, 0, m - 1) {
    while ((cur + 1) != Total.end() && f(*cur, i) <= f(*(cur + 1), i)) ++cur;
    ans.pb(f(*cur, i));
  }
  for (auto x : ans) wr(x, ' ');
  wr('\n');
  return 0;
}

// 一条路径的权值是一个关于t的一次函数。
// 考虑边分治。我们只关心经过当前分治边的直径。
// 由于参数是非负的。因此直径肯定是两端都取到叶子。
// 那么相当于我们把左右两边的子树的叶子各选一个，加起来就得到了这条直径的一次函数。
// 但这样总共有O(n^2)条直线。所以不能直接枚举。
// 我们只用考虑这些直线形成的一个半平面交（下凸）。
// 而两边的半平面交的和，不好算。
// 考虑对偶成凸包。那么两个一次函数的和就是两个点的和。
// 则凸包的合并就是闵可夫斯基和。可以O(n)做！
// 这个合并后的凸包的大小也是O(n)的。
// 对分治后每个子树都做一次，就会得到一共nlogn个凸包。
// 把他们都合并（注意，这次不是闵可夫斯基和，是凸包的合并）就得到了所有直径的凸包。
