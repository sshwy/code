// by Sshwy
//#define DEBUGGER
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

#ifdef DEBUGGER

#define log_prefix()                                                \
  {                                                                 \
    fprintf(stderr, "\033[37mLine %-3d [%dms]:\033[0m  ", __LINE__, \
        (int)clock() / 1000);                                       \
  }
#define ilog(...) \
  { fprintf(stderr, __VA_ARGS__); }
#define llog(...)      \
  {                    \
    log_prefix();      \
    ilog(__VA_ARGS__); \
  }
#define log(...)           \
  {                        \
    log_prefix();          \
    ilog(__VA_ARGS__);     \
    fprintf(stderr, "\n"); \
  }
#define red(...)       \
  {                    \
    log_prefix();      \
    ilog("\033[31m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define green(...)     \
  {                    \
    log_prefix();      \
    ilog("\033[32m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define blue(...)      \
  {                    \
    log_prefix();      \
    ilog("\033[34m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }

#else

#define log_prefix() ;
#define ilog(...) ;
#define llog(...) ;
#define log(...) ;
#define red(...) ;
#define green(...) ;
#define blue(...) ;

#endif

namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 2e6 + 5, M = N;
int n, m;

struct qxx {
  int nex, t;
} e[M];
int h[N], le;
void add_path(int f, int t) { e[++le] = {h[f], t}, h[f] = le; }

int use[N], totuse, ban[N], g[N], tag[N];
vector<int> a2;

void dfs(int u) {
  assert(use[u] == 0);
  int id = ++totuse;
  use[u] = id;
  // printf("use %d %d\n",u,id);
  a2.pb(u);
  for (int i = h[u]; i; i = e[i].nex) {
    int v = e[i].t;
    if (!use[v]) use[v] = id;
  }
  for (int i = h[u]; i; i = e[i].nex) {
    int v = e[i].t;
    if (use[v] == id && !tag[v]) {
      tag[v] = 1;
      for (int j = h[v]; j; j = e[j].nex) {
        int z = e[j].t;
        if (!use[z]) dfs(z);
      }
    }
  }
}
int main() {
  scanf("%d%d", &n, &m);
  FOR(i, 1, m) {
    int a, b;
    scanf("%d%d", &a, &b);
    add_path(a, b);
  }
  FOR(i, 1, n) add_path(n + 1, i);
  add_path(n + 2, n + 1);
  dfs(n + 2);
  reverse(a2.begin(), a2.end());
  vector<int> a;
  for (auto x : a2) {
    if (ban[x]) continue;
    if (x <= n) a.pb(x);
    ban[x] = 1;
    // printf("%d:\n",x);
    for (int i = h[x]; i; i = e[i].nex) {
      const int v = e[i].t;
      // assert(ban[v]==0);
      ban[v] = 1;
      // printf("ban %d\n",v);
    }
  }
  cout << a.size() << endl;
  for (auto x : a) cout << x << " ";
  cout << endl;
  return 0;
}
