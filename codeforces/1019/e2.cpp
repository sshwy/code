#include <algorithm>
#include <cstdio>
#include <vector>
using namespace std;
const int N = 1e5 + 5;
typedef long long LL;
struct point {
  LL x, y;
  inline bool operator<(const point &rhs) const {
    return x == rhs.x ? y > rhs.y : x < rhs.x;
  }
  inline point operator+(const point &r) const { return (point){x + r.x, y + r.y}; }
  inline point operator-(const point &r) const { return (point){x - r.x, y - r.y}; }
} sta[N];
int top;
struct hull {
  inline bool cross(point a, point b) {
    return (long double)a.x * b.y - 1e-8 > (long double)a.y * b.x;
  }
  std::vector<point> pt;
  inline bool empty() const { return pt.empty(); }
  inline void add(int x, LL y) {
    for (auto &i : pt) i.x += x, i.y += y;
  }
  inline void insert(const point &nw) {
    while (pt.size() > 1) {
      const point &pre = pt.back(), &ppre = pt[(int)pt.size() - 2];
      if (cross(nw - ppre, pre - ppre) == 0)
        pt.pop_back();
      else
        break;
    }
    pt.push_back(nw);
  }
  inline void insert(LL x, LL y) { insert((point){x, y}); }
  hull operator*(const hull &rhs) {
    if (rhs.empty()) return *this;
    if (empty()) return rhs;
    hull c;
    int i1 = 0, i2 = 0;
    for (; i1 != (int)pt.size() - 1 || i2 != (int)rhs.pt.size() - 1;) {
      c.insert(pt[i1] + rhs.pt[i2]);
      const point &nw = c.pt.back();
      if (i1 == (int)pt.size() - 1)
        ++i2;
      else if (i2 == (int)rhs.pt.size() - 1)
        ++i1;
      else {
        point _i1 = pt[i1 + 1] + rhs.pt[i2], _i2 = pt[i1] + rhs.pt[i2 + 1];
        if (cross(_i1 - nw, _i2 - nw) == 0)
          ++i1;
        else
          ++i2;
      }
    }
    c.insert(pt[i1] + rhs.pt[i2]);
    return c;
  }
  void done(int m) {
    int it = 0;
    if (empty()) {
      while (m--) putchar('0'), putchar(' ');
    }
    for (int i = 0; i < m; ++i) {
      while (it + 1 < (int)pt.size() &&
             i * pt[it].x + pt[it].y <= i * pt[it + 1].x + pt[it + 1].y)
        ++it;
      printf("%lld ", i * pt[it].x + pt[it].y);
    }
  }
  inline void output() {
    for (auto it : pt) printf("(%lld,%lld) ", it.x, it.y);
    putchar('\n');
  }
};
vector<hull> A;
void merge(hull &a, hull &b) {
  hull c;
  auto i1 = a.pt.begin(), i2 = b.pt.begin();
  while (i1 != a.pt.end() && i2 != b.pt.end())
    if (i1->x < i2->x || i1->x == i2->x && i1->y > i2->y)
      c.insert(*i1++);
    else
      c.insert(*i2++);
  while (i1 != a.pt.end()) c.insert(*i1++);
  while (i2 != b.pt.end()) c.insert(*i2++);
  a = c;
}
struct edge {
  int to, nxt, a, b;
} e[N << 1];
int n, m, nod, rt, sz[N], mxd[N], all, head[N], cnt;
bool vis[N];
void get_centroid(int now, int pre) {
  mxd[now] = 0, sz[now] = 1;
  for (int i = head[now]; i; i = e[i].nxt)
    if (!vis[e[i].to] && e[i].to != pre) {
      get_centroid(e[i].to, now), sz[now] += sz[e[i].to];
      mxd[now] = max(mxd[now], sz[e[i].to]);
    }
  mxd[now] = max(mxd[now], all - sz[now]);
  if (!rt || mxd[now] < mxd[rt]) rt = now;
}
void dfs(int now, int pre, LL a, LL b) {
  bool x = 0;
  for (int i = head[now]; i; i = e[i].nxt)
    if (e[i].to != pre && !vis[e[i].to]) {
      x = 1;
      dfs(e[i].to, now, a + e[i].a, b + e[i].b);
    }
  if (!x) sta[++top] = (point){a, b};
}
void mink(vector<hull> &A, int l, int r, hull &x) {
  if (l == r)
    x = A[l];
  else {
    hull R;
    const int mid = l + r >> 1;
    mink(A, l, mid, x), mink(A, mid + 1, r, R);
    ::A.push_back(x * R);
    merge(x, R);
  }
}
void work(int now) {
  vector<hull> B;
  for (int i = head[now]; i; i = e[i].nxt)
    if (!vis[e[i].to]) {
      top = 0;
      dfs(e[i].to, now, e[i].a, e[i].b);
      sort(sta + 1, sta + top + 1);
      hull x;
      for (int i = 1; i <= top; ++i) x.insert(sta[i]);
      B.push_back(x);
    }
  if (B.empty()) return;
  hull k;
  mink(B, 0, (int)B.size() - 1, k);
  A.push_back(k);
}
void solve(int now) {
  vis[now] = 1;
  work(now);
  int sm = all;
  for (int i = head[now]; i; i = e[i].nxt)
    if (!vis[e[i].to]) {
      all = sz[now] > sz[e[i].to] ? sz[e[i].to] : sm - sz[now];
      rt = 0;
      get_centroid(e[i].to, now);
      solve(rt);
    }
}
void final(vector<hull> &A, int l, int r, hull &x) {
  if (l == r)
    x = A[l];
  else {
    const int mid = l + r >> 1;
    hull R;
    final(A, l, mid, x), final(A, mid + 1, r, R);
    merge(x, R);
  }
}
int main() {
  scanf("%d%d", &n, &m), nod = n;
  for (int i = 1; i < n; ++i) {
    int u, v, a, b;
    scanf("%d%d%d%d", &u, &v, &a, &b);
    e[++cnt] = (edge){v, head[u], a, b}, head[u] = cnt;
    e[++cnt] = (edge){u, head[v], a, b}, head[v] = cnt;
  }
  all = n, rt = 0;
  get_centroid(1, 0);
  solve(rt);
  hull k;
  if (!A.empty()) final(A, 0, (int)A.size() - 1, k);
  k.done(m);
  return 0;
}
