// by Sshwy
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 1e5 + 5;

int n;
vector<int> g[N];
int fa[N][20], dep[N];

int lca(int u, int v) {
  if (dep[u] < dep[v]) swap(u, v);
  ROF(j, 19, 0) if (dep[u] - (1 << j) >= dep[v]) u = fa[u][j];
  if (u == v) return u;
  ROF(j, 19, 0) if (fa[u][j] != fa[v][j]) u = fa[u][j], v = fa[v][j];
  return fa[u][0];
}
int dist(int u, int v) {
  int z = lca(u, v);
  // printf("dist(%d,%d)=%d\n",u,v, dep[u]+dep[v]-dep[z]*2);
  return dep[u] + dep[v] - dep[z] * 2;
}
void dfs(int u, int p) {
  dep[u] = dep[p] + 1;
  fa[u][0] = p;
  FOR(j, 1, 19) {
    fa[u][j] = fa[fa[u][j - 1]][j - 1];
    if (!fa[u][j]) break;
  }
  for (int v : g[u])
    if (v != p) { dfs(v, u); }
}
vector<int> v;
void go() {
  v.clear();
  int x, y, a, b, k;
  scanf("%d%d%d%d%d", &x, &y, &a, &b, &k);
  // printf("%d %d %d %d %d\n",x,y,a,b,k);
  int c = lca(a, b);
  // printf("c=%d\n",c);
  v.pb(dist(a, b));
  int clen = dist(x, y); //环长
  int ax = dist(a, x), by = dist(b, y), ay = dist(a, y), bx = dist(b, x);
  v.pb(ax + 1 + by);
  v.pb(ax + clen + by);
  v.pb(ay + 1 + bx);
  v.pb(ay + clen + bx);
  for (int x : v) {
    if (x <= k && x % 2 == k % 2) return puts("YES"), void();
  }
  puts("NO");
}
int main() {
  scanf("%d", &n);
  FOR(i, 1, n - 1) {
    int u, v;
    scanf("%d%d", &u, &v);
    g[u].pb(v);
    g[v].pb(u);
  }
  dfs(1, 0);
  int q;
  scanf("%d", &q);
  FOR(i, 1, q) { go(); }
  return 0;
}
