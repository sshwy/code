// by Sshwy
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 2e5 + 5;

int n;
char s[N];
int p[N];

void go() {
  scanf("%d", &n);
  scanf("%s", s + 1);
  s[n] = 0;
  FOR(i, 1, n) p[i] = n - i + 1;
  FOR(i, 1, n - 1) {
    if (s[i] == '<') {
      int j = i;
      while (s[j] == '<') ++j;
      int nex = j;
      while (i < j) swap(p[i], p[j]), ++i, --j;
      i = nex;
    }
  }
  FOR(i, 1, n) printf("%d%c", p[i], " \n"[i == n]);
  FOR(i, 1, n) p[i] = i;
  FOR(i, 1, n - 1) {
    if (s[i] == '>') {
      int j = i;
      while (s[j] == '>') ++j;
      int nex = j;
      while (i < j) swap(p[i], p[j]), ++i, --j;
      i = nex;
    }
  }
  FOR(i, 1, n) printf("%d%c", p[i], " \n"[i == n]);
}
int main() {
  int t;
  scanf("%d", &t);
  FOR(i, 1, t) go();
  return 0;
}
