// by Sshwy
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 60, M = 2e4 + 5;

int n, m, k, ans;
int f[N][M], a[N][M], g[N][M], h[N][M];

int w(int i, int l, int r) { return a[i][r] - a[i][l - 1]; }
int F(int i, int l) { return f[i - 1][l] - a[i][l - 1]; }
int G(int i, int l) { return f[i - 1][l] + a[i][l + k - 1]; }

int que[M], ql, qr;

int main() {
  scanf("%d%d%d", &n, &m, &k);
  FOR(i, 1, n) FOR(j, 1, m) scanf("%d", &a[i][j]);
  FOR(i, 1, n) FOR(j, 1, m) a[i][j] += a[i][j - 1];

  FOR(j, 1, m - k + 1) f[1][j] = w(1, j, j + k - 1);
  FOR(i, 2, n) {
    FOR(j, 1, m - k + 1) h[i][j] = g[i][j] = f[i - 1][j] + w(i, j, j + k - 1);
    FOR(j, 1, m - k + 1) g[i][j] = max(g[i][j], g[i][j - 1]);
    ROF(j, m - k + 1, 1) h[i][j] = max(h[i][j], h[i][j + 1]);
    FOR(j, 1, m - k + 1) {
      f[i][j] = max(f[i][j], g[i][j - k] + w(i, j, j + k - 1));
      f[i][j] = max(f[i][j], h[i][j + k] + w(i, j, j + k - 1));
    }
    ql = 1, qr = 0;
    FOR(j, 1, m - k + 1) {
      while (ql <= qr && j - k + 1 > que[ql]) ++ql;
      while (ql <= qr && F(i, que[qr]) <= F(i, j)) --qr;
      que[++qr] = j;
      f[i][j] = max(f[i][j], F(i, que[ql]) + a[i][j + k - 1]);
    }
    ql = 1, qr = 0;
    ROF(j, m - k + 1, 1) {
      while (ql <= qr && j + k - 1 < que[ql]) ++ql;
      while (ql <= qr && G(i, que[qr]) <= G(i, j)) --qr;
      que[++qr] = j;
      f[i][j] = max(f[i][j], G(i, que[ql]) - a[i][j - 1]);
    }
  }
  FOR(j, 1, m - k + 1) ans = max(ans, f[n][j]);
  printf("%d\n", ans);

  return 0;
}
