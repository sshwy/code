// by Sshwy
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
#define int long long
const int N = 1020;

int n, m;
struct p3 {
  int a, b, c;
} a[N];

void go() {
  cin >> n >> m;
  FOR(i, 1, n) cin >> a[i].a >> a[i].b >> a[i].c;
  sort(a + 1, a + n + 1, [](p3 x, p3 y) { return x.a < y.a; });
  // FOR(i,1,n)printf("%d %d %d\n",a[i].a,a[i].b,a[i].c);
  int L = m, R = m; //可行的温度区间
  int pos = 0, las = 0;
  while (pos < n) {
    int cur = a[++pos].a;
    L -= cur - las;
    R += cur - las;
    L = max(L, a[pos].b);
    R = min(R, a[pos].c);
    // printf("pos=%d, L=%d,R=%d\n",pos,L,R);
    while (pos < n && a[pos + 1].a == cur) {
      ++pos;
      L = max(L, a[pos].b);
      R = min(R, a[pos].c);
      // printf("pos=%d, L=%d,R=%d\n",pos,L,R);
    }
    if (L > R) {
      cout << "NO" << endl;
      return;
    }
    las = a[pos].a;
  }
  cout << "YES" << endl;
}
signed main() {
  // ios::sync_with_stdio(false);
  // cin.tie(0);
  // cout.tie(0);
  int t;
  cin >> t;
  FOR(i, 1, t) go();
  return 0;
}
