// by Sshwy
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/

bool check(string s) {
  if (s.size() == 0) return 1;
  int l = 0, r = s.size() - 1;
  while (l < r) {
    if (s[l] != s[r]) return 0;
    ++l, --r;
  }
  return 1;
}
bool check(string s, string t) {
  reverse(t.begin(), t.end());
  return s == t;
}

int n, m;
string s[200];
string p = "";
vector<string> v, v2;

int main() {
  cin >> n >> m;
  FOR(i, 1, n) cin >> s[i];
  FOR(i, 1, n) if (check(s[i])) p = s[i];
  FOR(i, 1, n) FOR(j, i + 1, n) if (check(s[i], s[j])) v.pb(s[i]);
  int len = p.size() + v.size() * m * 2;
  cout << len << endl;
  v2 = v;
  reverse(v2.begin(), v2.end());
  for (string s : v2) {
    string s1 = s;
    reverse(s1.begin(), s1.end());
    cout << s1;
  }
  cout << p;
  for (string s : v) {
    string s1 = s;
    cout << s1;
  }
  return 0;
}
