#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 5e5 + 5, M = N * 2, INF = 0x3f3f3f3f;

int n;

struct qxx {
  int nex, t, v;
};
qxx e[M];
int h[N], le;
void add_path(int f, int t, int v) { e[++le] = (qxx){h[f], t, v}, h[f] = le; }
#define FORe(i, u, v) for (int i = h[u], v; v = e[i].t, i; i = e[i].nex)
#define FORew(i, u, v, w) \
  for (int i = h[u], v, w; v = e[i].t, w = e[i].v, i; i = e[i].nex)

int sz[N], msk[N];
int ans[N], dep[N];
bool big[N];

int c[1 << 23];
int dfs1(int u, int p, int s) {
  msk[u] = s, dep[u] = dep[p] + 1;
  FORew(i, u, v, w) sz[u] += dfs1(v, u, s ^ (1 << w));
  // printf("msk[%d]=%d\n",u,msk[u]);
  return ++sz[u];
}

void dfsadd(int u) {
  if (c[msk[u]] < dep[u]) c[msk[u]] = dep[u];
  FORe(i, u, v) if (!big[v]) dfsadd(v);
}
void dfsdel(int u) {
  c[msk[u]] = -INF;
  FORe(i, u, v) dfsdel(v);
}
void updans(int u, int root) {
  // printf("updans(%d,%d)\n",u,root);
  int t = c[msk[u]];
  FOR(i, 0, 21) { t = max(t, c[msk[u] ^ (1 << i)]); }
  ans[root] = max(ans[root], t + dep[u] - dep[root] * 2);
}
void dfsupdans(int u, int root) {
  updans(u, root);
  FORe(i, u, v) dfsupdans(v, root);
}
void dfs(int u, int keep = 0) {
  // printf("dfs(%d,%d)\n",u,keep);
  // printf("dep[%d]=%d\n",u,dep[u]);
  int mx = 0, hvs = 0;
  FORe(i, u, v) if (sz[v] > mx) mx = sz[v], hvs = v;
  FORe(i, u, v) if (v != hvs) dfs(v, 0), ans[u] = max(ans[u], ans[v]);
  if (hvs) dfs(hvs, 1), ans[u] = max(ans[u], ans[hvs]);
  FORe(i, u, v) if (v != hvs) {
    dfsupdans(v, u);
    dfsadd(v);
  }
  updans(u, u);
  c[msk[u]] = max(c[msk[u]], dep[u]);
  if (keep) return;
  dfsdel(u);
}
int main() {
  scanf("%d", &n);
  memset(c, -0x3f, sizeof(c));
  FOR(i, 2, n) {
    int p;
    char cx[5];
    scanf("%d%s", &p, cx);
    add_path(p, i, cx[0] - 'a');
  }
  dfs1(1, 0, 0);
  dfs(1);
  FOR(i, 1, n) printf("%d ", ans[i]);
  return 0;
}
