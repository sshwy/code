// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
#define for_subset(j, s) for (int j = s; j; j = (j - 1) & (s))

const int N = 15, INF = 0x3f3f3f3f;
void getMin(int &x, int y) { x = min(x, y); }

int n, nn, a[N + 1], b[1 << N];
int f[1 << N][N + 1],
    h[1 << N][N + 1]; // t[s, j] 表示这个 f[s, j] 的状态转移过来时的权值

struct Trans {
  int t, nexj;
  void print(int j) {
    for (int i = t; i; i -= i & -i) {
      int x = __builtin_ctz(i);
      if (x != j) printf("%d %d\n", x + 1, j + 1);
    }
  }
} g[1 << N][N + 1];

void work(pair<int, int> e) {
  int t = e.second, ct = __builtin_popcount(t) - 1;
  Trans trans = {t, -1};
  FOR(i, 0, n - 1) if (t >> i & 1) if (f[t][i] > ct && h[t][i] < e.first) {
    f[t][i] = ct;
    g[t][i] = trans;
    h[t][i] = e.first;
  };
  for_subset(s, (nn - 1) - t) {
    assert(!(s & t));
    int minVal = INF;
    Trans minTrans = trans;
    FOR(j, 0, n - 1) {
      if (t >> j & 1) {
        if (f[s | t][j] > minVal + ct && h[s | t][j] < e.first) {
          f[s | t][j] = minVal + ct;
          g[s | t][j] = minTrans;
          h[s | t][j] = e.first;
        }
      }
      if (s >> j & 1) {
        if (minVal > f[s][j]) {
          minVal = f[s][j];
          minTrans.nexj = j;
        }
      }
    }
  }
}

vector<pair<int, int>> v;

void print(int s, int j) {
  // printf("print %d %d : %d\n", s, j, b[g[s][j].t]);
  if (j == -1) return;
  // assert((g[s][j].t & s) == g[s][j].t);
  print(s - g[s][j].t, g[s][j].nexj);
  g[s][j].print(j);
}
void go() {
  scanf("%d", &n);
  nn = 1 << n;
  FOR(i, 1, n) scanf("%d", a + i), b[1 << (i - 1)] = a[i];
  FOR(i, 1, nn - 1) b[i] = b[i - (i & -i)] + b[i & -i];

  v.clear();
  FOR(i, 1, nn - 1) v.pb({b[i], i});
  sort(v.begin(), v.end());

  FOR(i, 0, nn - 1) memset(f[i], 0x3f, sizeof(f[i]));
  FOR(i, 0, nn - 1) memset(h[i], 0, sizeof(h[i]));

  for (auto e : v) work(e);

  int ansj = min_element(f[nn - 1], f[nn - 1] + n) - f[nn - 1];
  printf("%d\n", f[nn - 1][ansj]);
  print(nn - 1, ansj);
}

int main() {
  int t;
  scanf("%d", &t);
  FOR(i, 1, t) go();
  return 0;
}
