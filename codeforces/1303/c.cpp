// by Sshwy
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/

string s;

char ans[1000];
bool use[500];
int pos;

void go() {
  cin >> s;
  int pos = 500, L = 500, R = 500;
  memset(ans, 0, sizeof(ans));
  FOR(i, 'a', 'z') use[i] = 0;
  for (char c : s) {
    if (ans[pos] == 0) {
      ans[pos] = c;
      use[c] = 1;
    } else if (ans[pos - 1] == c)
      --pos;
    else if (ans[pos + 1] == c)
      ++pos;
    else if (use[c]) {
      cout << "NO" << endl;
      return;
    } else if (ans[pos - 1] == 0)
      ans[--pos] = c, L = min(L, pos), use[c] = 1;
    else if (ans[pos + 1] == 0)
      ans[++pos] = c, R = max(R, pos), use[c] = 1;
    else {
      cout << "NO" << endl;
      return;
    }
  }
  FOR(i, 'a', 'z') if (use[i] == 0) ans[++R] = i;
  cout << "YES" << endl;
  FOR(i, L, R) cout << ans[i];
  cout << endl;
}
int main() {
  int t;
  cin >> t;
  FOR(i, 1, t) go();
  return 0;
}
