// by Sshwy
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
#define int long long
const int M = 1e5 + 5;

long long n;
int m;
int a[M];
int cnt[100];

void go() {
  cin >> n >> m;
  long long sum = 0;
  memset(cnt, 0, sizeof(cnt));
  FOR(i, 1, m) {
    cin >> a[i];
    int c = 0;
    while ((1 << c) < a[i]) ++c;
    assert((1 << c) == a[i]);
    sum += a[i];
    cnt[c]++;
  }
  if (sum < n) {
    cout << -1 << endl;
    return;
  }
  int ans = 0;
  FOR(i, 0, 60) {
    cnt[i] += cnt[i - 1] / 2;
    if (n >> i & 1) {
      if (cnt[i])
        --cnt[i];
      else {
        int k = i;
        while (cnt[k] == 0) ++k;
        cnt[k]--;
        FOR(j, i, k - 1) cnt[j]++;
        ans += k - i;
      }
    }
  }
  cout << ans << endl;
}
signed main() {
  ios::sync_with_stdio(false);
  cin.tie(0);
  cout.tie(0);

  int t;
  cin >> t;
  FOR(i, 1, t) go();
  return 0;
}
