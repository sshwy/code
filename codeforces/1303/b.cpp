// by Sshwy
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
#define int long long

long long n, g, b;

void go() {
  cin >> n >> g >> b;
  int x = (n + 1) / 2;
  int cg = x / g + !!(x % g);
  int pos = (cg - 1) * (b + g) + (x % g == 0 ? g : x % g);
  n = n - x - (cg - 1) * b;
  if (n <= 0) {
    cout << pos << endl;
  } else {
    cout << pos + n << endl;
  }
}

signed main() {
  int t;
  cin >> t;
  FOR(i, 1, t) go();
  return 0;
}
