// by Sshwy
//#define DEBUGGER
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

#ifdef DEBUGGER

#define log_prefix()                                                \
  {                                                                 \
    fprintf(stderr, "\033[37mLine %-3d [%dms]:\033[0m  ", __LINE__, \
        (int)clock() / 1000);                                       \
  }
#define ilog(...) \
  { fprintf(stderr, __VA_ARGS__); }
#define llog(...)      \
  {                    \
    log_prefix();      \
    ilog(__VA_ARGS__); \
  }
#define log(...)           \
  {                        \
    log_prefix();          \
    ilog(__VA_ARGS__);     \
    fprintf(stderr, "\n"); \
  }
#define red(...)       \
  {                    \
    log_prefix();      \
    ilog("\033[31m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define green(...)     \
  {                    \
    log_prefix();      \
    ilog("\033[32m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define blue(...)      \
  {                    \
    log_prefix();      \
    ilog("\033[34m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }

#else

#define log_prefix() ;
#define ilog(...) ;
#define llog(...) ;
#define log(...) ;
#define red(...) ;
#define green(...) ;
#define blue(...) ;

#endif

namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
typedef long long LL;
typedef pair<LL, LL> atom;

const int N = 2e5 + 5, SZ = N << 2;
int n, sz[N], Cut[N], sm[N];
vector<int> g[N];
LL a[N], ans = 0;

namespace seg {
  int mx[SZ], mn[SZ], tim[SZ], totim;
  atom tag[SZ];
  inline void cls(int u) {
    if (tim[u] < totim) tag[u] = {0, 0}, tim[u] = totim;
  }
  void insert(LL k, LL b, int u = 1, int l = 1, int r = n) {
    cls(u);
    if (k * l + b >= tag[u].fi * l + tag[u].se &&
        k * r + b >= tag[u].fi * r + tag[u].se)
      return tag[u] = {k, b}, void();
    if (l == r || (k * l + b < tag[u].fi * l + tag[u].se &&
                      k * r + b < tag[u].fi * r + tag[u].se))
      return; // !!!
    int mid = (l + r) >> 1;
    insert(k, b, u << 1, l, mid), insert(k, b, u << 1 | 1, mid + 1, r);
  }
  LL query(LL x, int u = 1, int l = 1, int r = n) {
    cls(u);
    if (l == r) return tag[u].fi * x + tag[u].se;
    int mid = (l + r) >> 1;
    return max(tag[u].fi * x + tag[u].se,
        x <= mid ? query(x, u << 1, l, mid) : query(x, u << 1 | 1, mid + 1, r));
  }
  void clear() { ++totim; }
} // namespace seg

void Size(int u, int p) {
  sz[u] = 1, sm[u] = 0;
  for (int v : g[u])
    if (v != p && !Cut[v]) Size(v, u), sz[u] += sz[v], sm[u] = max(sm[u], sz[v]);
}
int Core(int u, int p, int T) {
  int c = u;
  for (int v : g[u])
    if (v != p && !Cut[v]) {
      int x = Core(v, u, T);
      if (max(sm[c], T - sz[c]) > max(sm[x], T - sz[x])) c = x;
    }
  return c;
}

void dfs(
    int u, int p, vector<atom> &v1, vector<atom> &v2, int dep, LL x1, LL x2, LL x3) {
  x1 += dep * a[u], x2 += a[u], x3 += x2, v1.pb({x3, dep}), v2.pb({x1, x2});
  for (int v : g[u])
    if (v != p && !Cut[v]) dfs(v, u, v1, v2, dep + 1, x1, x2, x3);
}
void Solve(int u, int p) {
  Size(u, p);
  int core = Core(u, p, sz[u]);

  vector<vector<atom>> v1, v2;
  for (int v : g[core])
    if (!Cut[v]) {
      vector<atom> t1, t2;
      dfs(v, core, t1, t2, 1, 0, 0, 0), v1.pb(t1), v2.pb(t2);
    }

  seg::clear();
  for (int i = 0; i < int(v1.size()); ++i) {
    for (auto &x : v1[i])
      ans = max(ans, seg::query(x.se + 1) + x.fi + a[core] * (x.se + 1));
    for (auto &x : v2[i]) seg::insert(x.se, x.fi); // kx+b
  }
  seg::clear();
  for (int i = int(v1.size()) - 1; i >= 0; --i) {
    for (auto &x : v1[i])
      ans = max(ans, seg::query(x.se + 1) + x.fi + a[core] * (x.se + 1));
    for (auto &x : v2[i]) seg::insert(x.se, x.fi); // kx+b
  }

  Cut[core] = 1;
  for (int v : g[core])
    if (!Cut[v]) Solve(v, core);
}
int main() {
  cin >> n;
  FOR(i, 1, n - 1) {
    int u, v;
    cin >> u >> v;
    g[u].pb(v);
    g[v].pb(u);
  }
  FOR(i, 1, n) { cin >> a[i]; }
  Solve(1, 0);
  if (n == 2) ans = max(a[1] * 2 + a[2], a[1] + a[2] * 2);
  cout << ans << endl;
  return 0;
}
