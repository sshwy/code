#include <cstdio>
#include <cstring>

#define Int register int
#define int LL

typedef long long LL;

const int MAXN = 1.5e5;
int n, Cnte, Head[MAXN + 5], Val[MAXN + 5], MaxDepth, Depth[MAXN + 5];
int Cnt, v1[MAXN + 5], v2[MAXN + 5], s[MAXN + 5], l[MAXN + 5], bel[MAXN + 5], Root;
int Siz[MAXN + 5], Wt[MAXN + 5];
bool Vis[MAXN + 5];
LL Ans;

struct Edge {
  int To, nxt;
} Graph[MAXN * 2 + 5];

template <typename Tp> inline Tp Max(const Tp a, const Tp b) { return a < b ? b : a; }

class LiChaoSegmentTree {
private:
  int Cnt, Tag[MAXN << 2];
  struct Function {
    LL k, b;
    inline LL Val(const int x) { return k * x + b; }
  } Line[MAXN << 2];

public:
  inline void Clear(const int i, const int l, const int r) {
    Tag[i] = Cnt = 0;
    if (l == r) return;
    int mid = l + r >> 1;
    Clear(i << 1, l, mid), Clear(i << 1 | 1, mid + 1, r);
  }
  inline void Insert(const LL k, const LL b, const int r) {
    Line[++Cnt] = {k, b};
    Update(1, 1, r, Cnt);
  }
  inline void Update(const int i, const int l, const int r, int id) {
    if (Line[Tag[i]].Val(l) <= Line[id].Val(l) &&
        Line[Tag[i]].Val(r) <= Line[id].Val(r)) {
      Tag[i] = id;
      return;
    }
    if (Line[Tag[i]].Val(l) >= Line[id].Val(l) &&
        Line[Tag[i]].Val(r) >= Line[id].Val(r))
      return;
    int mid = l + r >> 1;
    if (Line[Tag[i]].Val(mid) < Line[id].Val(mid)) id ^= Tag[i] ^= id ^= Tag[i];
    if (Line[id].k < Line[Tag[i]].k)
      Update(i << 1, l, mid, id);
    else
      Update(i << 1 | 1, mid + 1, r, id);
  }
  inline LL Query(const int i, const int l, const int r, const int x) {
    if (l == r) return Line[Tag[i]].Val(l);
    int mid = l + r >> 1;
    LL ret = Line[Tag[i]].Val(x);
    if (x <= mid)
      ret = Max(ret, Query(i << 1, l, mid, x));
    else
      ret = Max(ret, Query(i << 1 | 1, mid + 1, r, x));
    return ret;
  }
} LCST;

inline void Link(const int s, const int t) {
  Graph[++Cnte] = {t, Head[s]};
  Head[s] = Cnte;
}

inline void FindCent(const int u, const int f, const int s, int &rt) {
  Siz[u] = 1, Wt[u] = 0;
  for (Int i = Head[u], v; i; i = Graph[i].nxt) {
    if (!Vis[v = Graph[i].To] && v ^ f) {
      FindCent(v, u, s, rt), Siz[u] += Siz[v];
      Wt[u] = Max(Wt[u], Siz[v]);
    }
  }
  Wt[u] = Max(Wt[u], s - Siz[u]);
  if (!rt || Wt[rt] > Wt[u]) rt = u;
}

inline void Collect(
    const int u, const int f, const LL vl1, const LL vl2, const LL sm, int fr) {
  if (!fr && f) fr = u;
  Siz[u] = 1;
  MaxDepth = Max(MaxDepth, Depth[u] = Depth[f] + 1);
  bool lf = true;
  for (Int i = Head[u], v; i; i = Graph[i].nxt) {
    if (!Vis[v = Graph[i].To] && v ^ f) {
      lf = false;
      Collect(
          v, u, vl1 + sm + Val[v], vl2 + 1LL * Val[v] * Depth[u], sm + Val[v], fr);
      Siz[u] += Siz[v];
    }
  }
  if (lf) {
    ++Cnt;
    v1[Cnt] = vl1, v2[Cnt] = vl2, s[Cnt] = sm - Val[Root], l[Cnt] = Depth[u],
    bel[Cnt] = fr;
  }
}

inline void Solve(const int u) {
  Vis[Root = u] = true;
  MaxDepth = Cnt = 0;
  Collect(u, 0, Val[u], 0, Val[u], 0);
  v1[++Cnt] = Val[u], l[Cnt] = 1, v2[Cnt] = s[Cnt] = bel[Cnt] = 0;
  LCST.Clear(1, 1, MaxDepth);
  bel[Cnt + 1] = -1;
  for (Int i = 1, j; i <= Cnt;) {
    j = i;
    while (bel[i] == bel[j])
      Ans = Max(Ans, LCST.Query(1, 1, MaxDepth, l[j]) + v1[j]), ++j;
    j = i;
    while (bel[i] == bel[j]) LCST.Insert(s[j], v2[j], MaxDepth), ++j;
    i = j;
  }
  LCST.Clear(1, 1, MaxDepth);
  bel[0] = -1;
  for (Int i = Cnt, j; i;) {
    j = i;
    while (bel[i] == bel[j])
      Ans = Max(Ans, LCST.Query(1, 1, MaxDepth, l[j]) + v1[j]), --j;
    j = i;
    while (bel[i] == bel[j]) LCST.Insert(s[j], v2[j], MaxDepth), --j;
    i = j;
  }
  for (Int i = Head[u], v, rt; i; i = Graph[i].nxt) {
    if (!Vis[v = Graph[i].To] && Siz[v] > 1) {
      FindCent(v, u, Siz[v], rt = 0);
      Solve(rt);
    }
  }
}

inline void Work() {
  scanf("%lld", &n);
  for (Int i = 1, u, v; i ^ n; ++i) {
    scanf("%lld %lld", &u, &v);
    Link(u, v), Link(v, u);
  }
  for (Int i = 1; i <= n; ++i) scanf("%lld", &Val[i]);
  int rt = 0;
  FindCent(1, 0, n, rt);
  Solve(rt);
  printf("%lld\n", Ans);
}

signed main() {
  Work();
  return 0;
}
