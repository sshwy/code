// by Sshwy
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 1000;

char s[N], t[N];
bool vis[N];
int ls, lt;

int nex[N][30];
int f[N][N];

bool check(int sp) {
  FOR(i, 0, lt) FOR(j, 0, lt) f[i][j] = ls + 1;
  f[0][0] = 0;
  int lim = lt - sp;
  FOR(i, 0, sp) FOR(j, 0, lim) {
    int pos = f[i][j];
    FOR(k, i + 1, i + 1) {
      pos = nex[pos][t[k]];
      f[k][j] = min(f[k][j], pos);
      if (pos == ls + 1) break;
    }
    pos = f[i][j];
    FOR(k, j + 1, j + 1) {
      pos = nex[pos][t[k + sp]];
      f[i][k] = min(f[i][k], pos);
      if (pos == ls + 1) break;
    }
  }
  return f[sp][lim] <= ls;
}
void go() {
  scanf("%s", s + 1);
  scanf("%s", t + 1);
  ls = strlen(s + 1);
  lt = strlen(t + 1);
  FOR(i, 1, ls) s[i] -= 'a';
  FOR(i, 1, lt) t[i] -= 'a';

  bool fl = []() {
    int pos = 0;
    FOR(i, 1, lt) {
      while (++pos <= ls && s[pos] != t[i])
        ;
      if (pos > ls) return 0;
    }
    return 1;
  }();
  if (fl) {
    puts("YES");
    return;
  }

  FOR(i, 0, 25) nex[ls][i] = nex[ls + 1][i] = ls + 1;
  ROF(i, ls - 1, 0) {
    FOR(j, 0, 25) { nex[i][j] = s[i + 1] == j ? i + 1 : nex[i + 1][j]; }
  }

  FOR(sp, 1, lt - 1) {
    if (check(sp)) {
      puts("YES");
      return;
    }
  }
  puts("NO");
}
int main() {
  int t;
  cin >> t;
  FOR(i, 1, t) go();
  return 0;
}
