#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
namespace IO {
  char nc() {
    static char bf[100000], *p1 = bf, *p2 = bf;
    return p1 == p2 && (p2 = (p1 = bf) + fread(bf, 1, 100000, stdin), p1 == p2)
               ? EOF
               : *p1++;
  }
  int rd() {
    int res = 0;
    char c = getchar();
    while (!isdigit(c)) c = getchar();
    while (isdigit(c)) res = res * 10 + c - '0', c = getchar();
    return res;
  }
} // namespace IO
/******************heading******************/

const int SZ = 2e6 + 500, ALP = 26, INF = 0x3f3f3f3f;

struct SAM {
  int tot, last;
  int tr[SZ][ALP], fail[SZ];
  int len[SZ], cnt[SZ], end[SZ]; // cnt:状态出现次数;end：状态的结尾位置
  int s[SZ], ls;
  SAM() { tot = last = 1, len[1] = 0, fail[1] = 0; }
  void print_node(int u) { //输出每个结点的状态
    printf("u=%d,cnt=%d,len=%d,fail=%d, ", u, cnt[u], len[u], fail[u]);
    FOR(i, end[u] - len[u] + 1, end[u]) putchar(s[i]);
    puts("");
  }
  void insert(char x) {
    s[++ls] = x;
    x -= 'a';
    int u = ++tot, p = last;
    len[u] = len[last] + 1, last = u;
    cnt[u] = 1, end[u] = ls; // u 的卫星信息
    while (p && tr[p][x] == 0) tr[p][x] = u, p = fail[p];
    if (!p)
      fail[u] = 1;
    else {
      int q = tr[p][x];
      if (len[q] == len[p] + 1)
        fail[u] = q;
      else {
        int cq = ++tot;
        len[cq] = len[p] + 1, fail[cq] = fail[q];
        end[cq] = end[q]; //如果需要，更新的cq的卫星信息
        memcpy(tr[cq], tr[q], sizeof(tr[q]));
        fail[q] = fail[u] = cq;
        while (p && tr[p][x] == q) tr[p][x] = cq, p = fail[p];
      }
    }
  }
  int a[SZ], bin[SZ], tim[SZ]; // tim：每个结点的时间戳
  void count() {               //桶排，统计cnt
    FOR(i, 1, tot) bin[len[i]]++;
    FOR(i, 1, tot) bin[i] += bin[i - 1];
    FOR(i, 1, tot) a[bin[len[i]]--] = i;
    ROF(i, tot, 1) cnt[fail[a[i]]] += cnt[a[i]];
  }
  void make(char *s, int l) { FOR(i, 1, l) insert(s[i]); }
  int cnt2[SZ];
  void go(char *s, int l) {
    int u = 1, ans = INF;
    FOR(i, 1, l) {
      char c = s[i] - 'a';
      while (u && !tr[u][c]) u = fail[u];
      if (!u) u = 1;
      u = tr[u][c];
      cnt2[u]++;
    }
    ROF(i, tot, 1) { cnt[fail[a[i]]] += cnt[a[i]]; }
    u = 1;
    FOR(i, 1, l) {
      char c = s[i] - 'a';
      while (u && !tr[u][c]) u = fail[u];
      if (!u) u = 1;
      u = tr[u][c];
      if (cnt[u] == 1 && cnt2[u] == 1) ans = min(ans, len[fail[u]] + 1);
    }
    printf("%d", ans == INF ? -1 : ans);
  }
} sam;

char s1[SZ], s2[SZ];
int l1, l2;

int main() {
  scanf("%s", s1 + 1);
  scanf("%s", s2 + 1);
  l1 = strlen(s1 + 1), l2 = strlen(s2 + 1);
  sam.make(s1, l1);
  sam.count();
  sam.go(s2, l2);
  return 0;
}
