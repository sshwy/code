// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

const int N = 100, INF = 0x3f3f3f3f;

int n, a[N], m, m2;
int f[N][N][N * N];
int ans[N * N];

void getMin(int &x, int y) { x = min(x, y); }
void getMin(int &x, long long y) { x = min((long long)(x), y); }
int q[N], ql, qr;
void calc1(int cur) {           // a[cur] >= j
  FOR(i, a[cur], m2 + a[cur]) { // i = x + a[cur]
    ql = 1, qr = 0;
    int x = i - a[cur];
#define Y(p) (2ll * f[cur - 1][p][x] + p * p + 3 * p + 2)
    q[++qr] = 0;
    FOR(j, 1, a[cur]) {
      if (ql < qr) assert(q[ql + 1] > q[ql]);
      while (ql < qr && (Y(q[ql + 1]) - Y(q[ql])) <= 2 * j * (q[ql + 1] - q[ql]))
        ++ql;
      int p = q[ql];
      long long s = -2 * j * p + Y(p) + j * j - 3 * j;
      assert(s % 2 == 0);
      getMin(f[cur][j][i - j], s / 2);
      while (ql < qr && (Y(q[qr]) - Y(q[qr - 1])) * (j - q[qr]) >=
                            (Y(j) - Y(q[qr])) * (q[qr] - q[qr - 1]))
        --qr;
      q[++qr] = j;
    }
  }
}
void calc2(int cur) {            // a[cur] < j
  FOR(i, -a[cur], m2 - a[cur]) { // i = x - a[cur]
    ql = 1, qr = 0;
    int x = i + a[cur];
#define Y(p) (2ll * f[cur - 1][p][x] + p * p + 3 * p + 2)
    FOR(j, 0, a[cur]) {
      while (ql < qr && (Y(q[qr]) - Y(q[qr - 1])) * (j - q[qr]) >=
                            (Y(j) - Y(q[qr])) * (q[qr] - q[qr - 1]))
        --qr;
      q[++qr] = j;
    }
    // printf("init: "); FOR(i,ql,qr)printf("%d%c",q[i]," \n"[i==qr]);
    int limJ = min(m, m2 - i);
    FOR(j, a[cur] + 1, limJ) {
      if (ql < qr) assert(q[ql + 1] > q[ql]);
      while (ql < qr && (Y(q[ql + 1]) - Y(q[ql])) <= 2 * j * (q[ql + 1] - q[ql]))
        ++ql;
      int p = q[ql];
      long long s = -2 * j * p + Y(p) + j * j - 3 * j;
      assert(s % 2 == 0);
      getMin(f[cur][j][i + j], s / 2);
      while (ql < qr && (Y(q[qr]) - Y(q[qr - 1])) * (j - q[qr]) >=
                            (Y(j) - Y(q[qr])) * (q[qr] - q[qr - 1]))
        --qr;
      q[++qr] = j;
    }
  }
}
void calc(int cur) {
  calc1(cur);
  calc2(cur);
}
void go() {
  memset(f, 0x3f, sizeof(f));
  memset(ans, 0x3f, sizeof(ans));
  f[0][0][0] = 0;
  FOR(i, 1, n) calc(i);
  FOR(k, 0, m2) {
    if (k > 0) getMin(ans[k], ans[k - 1]);
    FOR(j, 0, m) if (f[n][j][k] < INF) {
      getMin(ans[k], f[n][j][k] + (m - j) * (m - j - 1) / 2);
    }
  }
}
int main() {
  scanf("%d", &m);
  int tot = 0;
  FOR(i, 1, m) {
    int x;
    scanf("%d", &x);
    if (x)
      a[++n] = i;
    else
      ++tot;
  }
  m2 = m * (m - 1) / 2;
  go();
  FOR(k, 0, m2) { printf("%d%c", tot * (tot - 1) / 2 - ans[k], " \n"[k == m2]); }
  return 0;
}
