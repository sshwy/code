// by Sshwy
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

template <const int SZ, class T = int, const T initValue = 0, const int LogSZ = 30>
class fenwick {
private:
  T c[SZ], sum;

public:
  fenwick() { fill(c, c + SZ, initValue), sum = initValue; }
  T &operator[](int x) { return c[x]; }
  void add(int pos, T v) {
    for (int i = pos; i < SZ; i += i & -i) c[i] = c[i] + v;
    sum = sum + v;
  }
  void add(int pos, T v, T addFunc(T, T)) {
    for (int i = pos; i < SZ; i += i & -i) c[i] = addFunc(c[i], v);
    sum = addFunc(sum, v);
  }
  T prefixSum(int pos) {
    T res = initValue;
    for (int i = pos; i > 0; i -= i & -i) res += c[i];
    return res;
  }
  T prefixSum(int pos, T addFunc(T, T)) {
    T res = initValue;
    for (int i = pos; i > 0; i -= i & -i) res = addFunc(res, c[i]);
    return res;
  }
  T suffixSum(int pos) { return sum - prefixSum(pos - 1); }
  int lowerBound(const T sum) {
    int pos = 0;
    T pre = initValue;
    ROF(j, LogSZ, 0)
    if (pos + (1 << j) < SZ && pre + c[pos + (1 << j)] < sum)
      pre = pre + c[pos + (1 << j)], pos += 1 << j;
    return pos + 1;
  }
  int lowerBound(
      const T sum, bool cmp(T, T)) { // lowerBound: cmp = <,  upperBound: cmp = <=
    int pos = 0;
    T pre = initValue;
    ROF(j, LogSZ, 0)
    if (pos + (1 << j) < SZ && cmp(pre + c[pos + (1 << j)], sum))
      pre = pre + c[pos + (1 << j)], pos += 1 << j;
    return pos + 1;
  }
};
const int N = 1e6 + 5;
fenwick<N, int> f;
int n, q;
int main() {
  scanf("%d%d", &n, &q);
  FOR(i, 1, n) {
    int x;
    scanf("%d", &x);
    f.add(x, 1);
  }
  int tot = n;
  FOR(i, 1, q) {
    int k;
    scanf("%d", &k);
    if (1 <= k && k <= n) {
      f.add(k, 1);
      ++tot;
    } else {
      k = abs(k);
      int v = f.lowerBound(k);
      f.add(v, -1);
      --tot;
    }
  }
  if (tot == 0) return puts("0"), 0;
  int ans = f.lowerBound(1);
  printf("%d\n", ans);
  return 0;
}
