// by Sshwy
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA
const int N = 5e3 + 5;
int n, m;
int n1, n2, n3;
vector<int> g[N];
vector<int> cv[4];
int vis[N], c[4];
bool dfs(int u, int col) {
  vis[u] = col;
  c[col]++;
  cv[col].pb(u);
  for (int v : g[u]) {
    if (vis[v]) {
      if (vis[v] == vis[u]) return 1;
    } else {
      if (dfs(v, col ^ 1)) return 1;
    }
  }
  return 0;
}
bool f[N];
int h[N], ans[N], use[N];
int main() {
  scanf("%d%d", &n, &m);
  scanf("%d%d%d", &n1, &n2, &n3);
  FOR(i, 1, m) {
    int u, v;
    scanf("%d%d", &u, &v);
    g[u].pb(v);
    g[v].pb(u);
  }
  vector<int> v;
  vector<pair<vector<int>, vector<int>>> vs;
  int tot = 0;
  FOR(i, 1, n) {
    if (!vis[i]) {
      c[2] = c[3] = 0;
      cv[2].clear();
      cv[3].clear();
      if (dfs(i, 2)) return puts("NO"), 0;
      tot += min(c[2], c[3]);
      v.pb(abs(c[2] - c[3]));
      if (c[2] < c[3])
        vs.pb({cv[2], cv[3]});
      else
        vs.pb({cv[3], cv[2]});
    }
  }
  f[0] = 1;
  for (int j = 0; j < v.size(); ++j) {
    int x = v[j];
    ROF(i, n - x, 0) if (f[i] && !f[i + x]) f[i + x] |= f[i], h[i + x] = j;
  }
  if (n2 < tot || !f[n2 - tot]) return puts("NO"), 0;
  for (int i = n2 - tot; i; i -= v[h[i]]) {
    int j = h[i];
    for (auto u : vs[j].second) ans[u] = 2;
    for (auto u : vs[j].first) {
      if (n1)
        ans[u] = 1, --n1;
      else
        ans[u] = 3, --n3;
    }
    use[j] = 1;
  }
  for (int j = 0; j < v.size(); ++j)
    if (!use[j]) {
      for (auto u : vs[j].first) ans[u] = 2;
      for (auto u : vs[j].second) {
        if (n1)
          ans[u] = 1, --n1;
        else
          ans[u] = 3, --n3;
      }
      use[j] = 1;
    }
  puts("YES");
  FOR(i, 1, n) printf("%d", ans[i]);
  puts("");

  return 0;
}
