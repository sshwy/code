// by Sshwy
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA
const int N = 2e5 + 5;
char s[N];
void go() {
  scanf("%s", s + 1);
  int ls = strlen(s + 1);
  int c[4] = {0};
  FOR(i, 1, ls) s[i] -= '0';
  int pos = 0;
  while (pos <= ls && (!c[1] || !c[2] || !c[3])) ++pos, c[s[pos]]++;
  if (pos > ls) return puts("0"), void();
  int ans = pos;
  FOR(i, 1, ls) {
    c[s[i]]--;
    while (pos <= ls && (!c[1] || !c[2] || !c[3])) ++pos, c[s[pos]]++;
    if (pos > ls) break;
    ans = min(ans, pos - i);
  }
  printf("%d\n", ans);
}
int main() {
  int t;
  scanf("%d", &t);
  FOR(i, 1, t) go();
  return 0;
}
