// by Sshwy
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA
int n;
double pi = acos(0) * 2, x;
double calc(double t) { // t/n
  double y = sin(pi - pi / 4 - pi * t / n) / sin(pi / 4) * x;
  return y;
}
void go() {
  scanf("%d", &n);
  x = 1 / sin(pi / (n * 2)) / 2; // radius
  double ans = 0;
  FOR(t, 1, n) if (pi - pi / 4 - pi * t / n > 0) ans = max(ans, calc(t));
  ans = ans * sqrt(2);
  printf("%.9lf\n", ans);
}
int main() {
  int t;
  scanf("%d", &t);
  FOR(i, 1, t) go();
  return 0;
}
