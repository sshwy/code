// by Sshwy
//#define DEBUGGER
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

#ifdef DEBUGGER

#define log_prefix()                                                \
  {                                                                 \
    fprintf(stderr, "\033[37mLine %-3d [%dms]:\033[0m  ", __LINE__, \
        (int)clock() / 1000);                                       \
  }
#define ilog(...) \
  { fprintf(stderr, __VA_ARGS__); }
#define llog(...)      \
  {                    \
    log_prefix();      \
    ilog(__VA_ARGS__); \
  }
#define log(...)           \
  {                        \
    log_prefix();          \
    ilog(__VA_ARGS__);     \
    fprintf(stderr, "\n"); \
  }
#define red(...)       \
  {                    \
    log_prefix();      \
    ilog("\033[31m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define green(...)     \
  {                    \
    log_prefix();      \
    ilog("\033[32m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define blue(...)      \
  {                    \
    log_prefix();      \
    ilog("\033[34m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }

#else

#define log_prefix() ;
#define ilog(...) ;
#define llog(...) ;
#define log(...) ;
#define red(...) ;
#define green(...) ;
#define blue(...) ;

#endif

namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 4e3 + 5;
int n, m;
int l[N], s[N], c[N];

int f[N][N];

int main() {
  scanf("%d%d", &n, &m);
  FOR(i, 1, n) scanf("%d", &l[i]);
  FOR(i, 1, n) scanf("%d", &s[i]);
  FOR(i, 1, n + m) scanf("%d", &c[i]);

  memset(f, -0x3f, sizeof(f));
  FOR(i, 1, n + m) f[i][0] = 0;
  ROF(i, n, 1) {
    ROF(j, n, 1) f[l[i]][j] = max(f[l[i]][j], f[l[i]][j - 1] - s[i] + c[l[i]]);
    int lim = n;
    FOR(j, l[i], n + m) {
      FOR(k, 0, lim)
      f[j + 1][k / 2] = max(f[j + 1][k / 2], f[j][k] + c[j + 1] * (k / 2));
      lim /= 2;
    }
  }

  printf("%d\n", f[n + m][0]);

  return 0;
}
// 性质：在确定了选人的方案后，我们按照任意顺序把这些人加进去，代价和不变。
// f[i,j]表示ag值最高为i，且ag值为i的有j个人的最大价值
// 倒着加人进去。这就意味着我们每次加的人要是最大的ag值
// 加入一个人i，则我们只关心ag值大于等于l[i]的部分
// 首先可以对f[l[i],j]做一个背包转移
// 然后可以考虑把最高ag值的人往上进位
