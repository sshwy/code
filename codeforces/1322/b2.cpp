// by Sshwy
//#define DEBUGGER
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

#ifdef DEBUGGER

#define log_prefix()                                                \
  {                                                                 \
    fprintf(stderr, "\033[37mLine %-3d [%dms]:\033[0m  ", __LINE__, \
        (int)clock() / 1000);                                       \
  }
#define ilog(...) \
  { fprintf(stderr, __VA_ARGS__); }
#define llog(...)      \
  {                    \
    log_prefix();      \
    ilog(__VA_ARGS__); \
  }
#define log(...)           \
  {                        \
    log_prefix();          \
    ilog(__VA_ARGS__);     \
    fprintf(stderr, "\n"); \
  }
#define red(...)       \
  {                    \
    log_prefix();      \
    ilog("\033[31m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define green(...)     \
  {                    \
    log_prefix();      \
    ilog("\033[32m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define blue(...)      \
  {                    \
    log_prefix();      \
    ilog("\033[34m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }

#else

#define log_prefix() ;
#define ilog(...) ;
#define llog(...) ;
#define log(...) ;
#define red(...) ;
#define green(...) ;
#define blue(...) ;

#endif

namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 4e5 + 5;
int n;
int a[N];

int calc(int k) {
  // printf("calc(%d)\n",k);
  FOR(i, 1, n) a[i] &= (1 << (k + 1)) - 1;
  // FOR(i,1,n)printf("%d%c",a[i]," \n"[i==n]);
  sort(a + 1, a + n + 1);
  int L = n + 1, R = n, lim = 1 << (k + 1), l2 = 1 << k;
  int res = 0;
  FOR(i, 1, n) { // 1<<k <= a[j]+a[i] < 1<<(k+1)
    while (R >= 1 && a[R] + a[i] >= lim) --R;
    while (L > 1 && a[L - 1] + a[i] >= l2) --L;
    // printf("i=%d,L=%d,R=%d\n",i,L,R);
    if (L <= R && L < i) res += min(R, i - 1) - L + 1;
  }
  // printf("res=%d\n",res);
  return res & 1;
}
int ans = 0;
int main() {
  scanf("%d", &n);
  FOR(i, 1, n) scanf("%d", &a[i]);
  FOR(i, 1, n) FOR(j, i + 1, n) ans ^= a[i] + a[j];
  printf("%d\n", ans);
  return 0;
}
