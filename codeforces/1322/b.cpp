// by Sshwy
//#define DEBUGGER
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

#ifdef DEBUGGER

#define log_prefix()                                                \
  {                                                                 \
    fprintf(stderr, "\033[37mLine %-3d [%dms]:\033[0m  ", __LINE__, \
        (int)clock() / 1000);                                       \
  }
#define ilog(...) \
  { fprintf(stderr, __VA_ARGS__); }
#define llog(...)      \
  {                    \
    log_prefix();      \
    ilog(__VA_ARGS__); \
  }
#define log(...)           \
  {                        \
    log_prefix();          \
    ilog(__VA_ARGS__);     \
    fprintf(stderr, "\n"); \
  }
#define red(...)       \
  {                    \
    log_prefix();      \
    ilog("\033[31m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define green(...)     \
  {                    \
    log_prefix();      \
    ilog("\033[32m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define blue(...)      \
  {                    \
    log_prefix();      \
    ilog("\033[34m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }

#else

#define log_prefix() ;
#define ilog(...) ;
#define llog(...) ;
#define log(...) ;
#define red(...) ;
#define green(...) ;
#define blue(...) ;

#endif

namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 4e5 + 5;
int n;
int a[N];
int ans = 0;

int calc(int k) {
  FOR(i, 1, n) a[i] &= (1 << (k + 1)) - 1;
  sort(a + 1, a + n + 1);
  int res = 0, L, R;
  FOR(i, 1, n) { // 1<<k <= a[j]+a[i] < 1<<(k+1)
    int L1, R1, L2, R2;
    L1 = lower_bound(a + 1, a + n + 1, (1 << k) - a[i]) - a;
    R1 = lower_bound(a + 1, a + n + 1, (1 << k + 1) - a[i]) - a - 1;
    L2 = lower_bound(a + 1, a + n + 1, (1 << k + 1) + (1 << k) - a[i]) - a;
    R2 = lower_bound(a + 1, a + n + 1, (1 << k + 1) + (1 << k + 1) - a[i]) - a - 1;
    if (L1 > R2 || L1 >= i) {
      if (L2 <= R2 && L2 < i) res += min(R2, i - 1) - L2 + 1;
    } else if (L2 > R2 || L2 >= i) {
      if (L1 <= R1 && L1 < i) res += min(R1, i - 1) - L1 + 1;
    } else if (R2 < L1 || R1 < L2) {
      if (L2 <= R2 && L2 < i) res += min(R2, i - 1) - L2 + 1;
      if (L1 <= R1 && L1 < i) res += min(R1, i - 1) - L1 + 1;
    } else {
      L = min(L1, L2), R = max(R1, R2);
      if (L <= R && L < i) res += min(R, i - 1) - L + 1;
    }
  }
  // printf("res=%d\n",res);
  return res & 1;
}
int main() {
  scanf("%d", &n);
  FOR(i, 1, n) scanf("%d", &a[i]);
  ROF(k, 28, 0) if (calc(k)) ans |= 1 << k;
  printf("%d\n", ans);
  return 0;
}
