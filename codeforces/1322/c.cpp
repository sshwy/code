// by Sshwy
//#define DEBUGGER
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

#ifdef DEBUGGER

#define log_prefix()                                                \
  {                                                                 \
    fprintf(stderr, "\033[37mLine %-3d [%dms]:\033[0m  ", __LINE__, \
        (int)clock() / 1000);                                       \
  }
#define ilog(...) \
  { fprintf(stderr, __VA_ARGS__); }
#define llog(...)      \
  {                    \
    log_prefix();      \
    ilog(__VA_ARGS__); \
  }
#define log(...)           \
  {                        \
    log_prefix();          \
    ilog(__VA_ARGS__);     \
    fprintf(stderr, "\n"); \
  }
#define red(...)       \
  {                    \
    log_prefix();      \
    ilog("\033[31m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define green(...)     \
  {                    \
    log_prefix();      \
    ilog("\033[32m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define blue(...)      \
  {                    \
    log_prefix();      \
    ilog("\033[34m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }

#else

#define log_prefix() ;
#define ilog(...) ;
#define llog(...) ;
#define log(...) ;
#define red(...) ;
#define green(...) ;
#define blue(...) ;

#endif

namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
typedef long long LL;
const int N = 5e5 + 5;
int n, m;
LL c[N], b[N];
vector<int> g[N];

LL gcd(LL a, LL b) { return b ? gcd(b, a % b) : a; }

map<pair<int, int>, LL> mp;

int Mod[] = {(int)1e9 + 7, (int)1e9 + 9};
int Base[] = {17, 19};
int Hash(int k, int u) {
  sort(g[u].begin(), g[u].end());
  int res = 0;
  for (int x : g[u]) { res = (res * 1ll * Base[k] + x) % Mod[k]; }
  return res;
}
void go() {
  scanf("%d%d", &n, &m);
  FOR(i, 1, n) scanf("%lld", &c[i]), b[i] = 0;
  FOR(i, 1, n) g[i].clear();
  FOR(i, 1, m) {
    int u, v;
    scanf("%d%d", &u, &v);
    b[u] += c[v];
    g[v].pb(u);
  }
  LL res = 0;
  mp.clear();
  FOR(i, 1, n) if (b[i]) res = gcd(res, b[i]);
  FOR(i, 1, n) {
    if (c[i] && g[i].size()) {
      pair<int, int> p(Hash(0, i), Hash(1, i));
      if (!mp.count(p)) mp[p] = 0;
      mp[p] += c[i];
    }
  }
  for (pair<pair<int, int>, LL> p : mp) { res = gcd(res, p.se); }
  printf("%lld\n", res);
}
int main() {
  int t;
  scanf("%d", &t);
  FOR(i, 1, t) go();
  return 0;
}
