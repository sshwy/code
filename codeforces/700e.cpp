// by Sshwy
#define DEBUGGER
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

#ifdef DEBUGGER

#define log_prefix()                                                \
  {                                                                 \
    fprintf(stderr, "\033[37mLine %-3d [%dms]:\033[0m  ", __LINE__, \
        (int)clock() / 1000);                                       \
  }
#define ilog(...) \
  { fprintf(stderr, __VA_ARGS__); }
#define llog(...)      \
  {                    \
    log_prefix();      \
    ilog(__VA_ARGS__); \
  }
#define log(...)           \
  {                        \
    log_prefix();          \
    ilog(__VA_ARGS__);     \
    fprintf(stderr, "\n"); \
  }
#define red(...)       \
  {                    \
    log_prefix();      \
    ilog("\033[31m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define green(...)     \
  {                    \
    log_prefix();      \
    ilog("\033[32m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define blue(...)      \
  {                    \
    log_prefix();      \
    ilog("\033[34m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }

#else

#define log_prefix() ;
#define ilog(...) ;
#define llog(...) ;
#define log(...) ;
#define red(...) ;
#define green(...) ;
#define blue(...) ;

#endif

namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
int xxx;
const int N = 2e5 + 5, SZ = N * 2, ALP = 26;

int n;
char s[N];

namespace seg {
  int tot;
  const int SEG = SZ * 30;
  int lc[SEG], rc[SEG];
  void add(int pos, int &u, int l = 1, int r = n) {
    if (!u) u = ++tot;
    if (l == r) return;
    int mid = (l + r) >> 1;
    pos <= mid ? add(pos, lc[u], l, mid) : add(pos, rc[u], mid + 1, r);
  }
  int merge(int x, int y) {
    if (!x || !y) return x + y;
    int u = ++tot;
    lc[u] = merge(lc[x], lc[y]);
    rc[u] = merge(rc[x], rc[y]);
    return u;
  }
  int find(int u, int l = 1, int r = n) { //最后一次出现
    if (l == r) return l;
    int mid = (l + r) >> 1;
    if (rc[u]) return find(rc[u], mid + 1, r);
    assert(lc[u]);
    return find(lc[u], l, mid);
  }
  int prev(int u, int pos, int l = 1, int r = n) { // pos的前驱
    if (l == r) return 0;
    int mid = (l + r) >> 1;
    if (pos <= mid) return prev(lc[u], pos, l, mid);
    int x = prev(rc[u], pos, mid + 1, r);
    if (x) return x;
    if (!lc[u]) return 0;
    return find(lc[u], l, mid);
  }
  void print(int u, int l = 1, int r = n) {
    if (l == r) {
      ilog("%d ", l);
      return;
    }
    int mid = (l + r) >> 1;
    if (lc[u]) print(lc[u], l, mid);
    if (rc[u]) print(rc[u], mid + 1, r);
  }
} // namespace seg
int rt[SZ];

int tot = 1, last = 1;
int fail[SZ], tr[SZ][ALP], len[SZ], endpos[SZ], ls;
void insert(char c) {
  c -= 'a';
  int u = ++tot, p = last;
  len[u] = len[last] + 1;
  endpos[u] = ++ls;
  while (p && tr[p][c] == 0) tr[p][c] = u, p = fail[p];
  if (!p)
    fail[u] = 1;
  else {
    int q = tr[p][c];
    if (len[q] == len[p] + 1)
      fail[u] = q;
    else {
      int cq = ++tot;
      len[cq] = len[p] + 1;
      endpos[cq] = endpos[q];
      memcpy(tr[cq], tr[q], sizeof(tr[q]));
      fail[cq] = fail[q], fail[q] = fail[u] = cq;
      while (p && tr[p][c] == q) tr[p][c] = cq, p = fail[p];
    }
  }
  last = u;
}
int bin[SZ], a[SZ];
void qsort() {
  FOR(i, 1, tot) bin[len[i]]++;
  FOR(i, 1, tot) bin[i] += bin[i - 1];
  ROF(i, tot, 1) a[bin[len[i]]--] = i;
}

vector<int> g[SZ];
int stk[SZ], dep[SZ];
int f[SZ], ans;
bool check(int x, int y) { // x occurred twice in y
  ++xxx;
  int p1 = seg::find(rt[y]);
  int p2 = seg::prev(rt[x], p1);
  // if(p1-len[y]<=p2-len[x]){ green("x=%d,y=%d,p1=%d,p2=%d",x,y,p1,p2); } else{
  // red("x=%d,y=%d,p1=%d,p2=%d",x,y,p1,p2); }
  if (p1 - len[y] <= p2 - len[x]) return 1;
  return 0;
}
void dfs(int u, int p, int pos) {
  f[u] = 1;
  dep[u] = dep[p] + 1;
  stk[dep[u]] = u;
  while (pos < dep[u] - 1 && check(stk[pos + 1], u)) ++pos;
  f[u] = f[stk[pos]] + 1;
  ans = max(ans, f[u]);
  for (int v : g[u]) dfs(v, u, pos);
}

int main() {
  scanf("%d", &n);
  scanf("%s", s + 1);
  FOR(i, 1, n) {
    insert(s[i]);
    seg::add(i, rt[last]);
  }
  qsort();
  ROF(i, tot, 1) {
    int u = a[i];
    rt[fail[u]] = seg::merge(rt[fail[u]], rt[u]);
    g[fail[u]].pb(u);
  }
  // FOR(i,1,tot){
  //    llog("u=%-2d,len=%-2d,fail=%-2d ",i,len[i],fail[i]);
  //    FOR(j,1,endpos[i]-len[i])ilog(" ");
  //    FOR(j,endpos[i]-len[i]+1,endpos[i])ilog("%c",s[j]);
  //    ilog("\n");
  //    llog("seg: "); seg::print(rt[i]); ilog("\n");
  //}
  dfs(1, 0, 0);
  printf("%d\n", ans - 1);
  log("xxx=%d", xxx);
  return 0;
}
