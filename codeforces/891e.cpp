#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 5e3 + 5, P = 1e9 + 7;

int pw(int a, int m) {
  int res = 1;
  while (m) m & 1 ? res = 1ll * res * a % P : 0, a = 1ll * a * a % P, m >>= 1;
  return res;
}
int n, k;
int c[N];

int main() {
  scanf("%d%d", &n, &k);
  c[0] = 1;
  FOR(i, 1, n) {
    int a;
    scanf("%d", &a);
    ROF(j, i, 1) c[j] = (c[j] * 1ll * a - c[j - 1]) % P;
    c[0] = 1ll * c[0] * a % P;
  }
  FOR(i, 0, n) c[i] = (c[i] + P) % P;
  int in = pw(n, P - 2);
  int ans = 0, kd = 1, nx = 1;
  FOR(i, 0, min(n, k)) {
    ans = (ans + 1ll * kd * nx % P * c[i]) % P;
    kd = 1ll * kd * (k - i) % P;
    nx = 1ll * nx * in % P;
  }
  printf("%d\n", (c[0] - ans + P) % P);
  return 0;
}
