#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
namespace IO {
  char nc() {
    static char bf[100000], *p1 = bf, *p2 = bf;
    return p1 == p2 && (p2 = (p1 = bf) + fread(bf, 1, 100000, stdin), p1 == p2)
               ? EOF
               : *p1++;
  }
  int rd() {
    int res = 0;
    char c = getchar();
    while (!isdigit(c)) c = getchar();
    while (isdigit(c)) res = res * 10 + c - '0', c = getchar();
    return res;
  }
} // namespace IO
/******************heading******************/
using namespace RA;

void rstr3(int len) {
  FOR(i, 1, len) putchar(r('a', 'b'));
  FOR(i, 1, len) putchar(r('a', 'a'));
  FOR(i, 1, len) putchar(r('b', 'c'));
  puts("");
}
void rstr(int len) {
  FOR(i, 1, len) putchar(r('a', 'b'));
  puts("");
}

int main() {
  srand(clock());
  int n = r(5, 10);
  rstr3(n);
  int t = 10;
  printf("%d\n", t);
  FOR(i, 1, t) { rstr(r(1, n / 2)); }
  return 0;
}
