// by Sshwy
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int MTX = 5;

struct Matrix {
  int w, h;
  double c[MTX][MTX];
  double *operator[](int x) { return c[x]; }
  Matrix() {}
  Matrix(int _w) {
    w = h = _w;
    FOR(i, 1, h) FOR(j, 1, w) c[i][j] = 0;
    FOR(i, 1, h) c[i][i] = 1;
  }
  Matrix(int _h, int _w) {
    w = _w, h = _h;
    FOR(i, 1, h) FOR(j, 1, w) c[i][j] = 0;
  }
  Matrix(int _h, int _w, double _c[MTX][MTX]) {
    w = _w, h = _h;
    FOR(i, 1, h) FOR(j, 1, w) c[i][j] = _c[i][j];
  }
  Matrix operator*(Matrix &m) {
    assert(w == m.h);
    Matrix res(h, m.w);
    FOR(i, 1, h) FOR(k, 1, w) FOR(j, 1, m.w) {
      res[i][j] = res[i][j] + c[i][k] * m[k][j];
    }
    return res;
  }
  Matrix &operator*=(Matrix &m) {
    *this = *this * m;
    return *this;
  }
};

const int GE = 20;
bool gauss(double g[GE][GE], int lg) {
  for (int i = 1; i <= lg; i++) { // 消第 i 个元
    for (int j = i; j <= lg; j++)
      if (fabs(g[j][i]) > 1e-8) {
        for (int k = 0; k <= lg; k++) swap(g[j][k], g[i][k]);
        break;
      }
    if (fabs(g[i][i]) < 1e-8) return 0; // 无解
    for (int j = 1; j <= lg; j++) {
      if (i == j) continue;
      double rate = g[j][i] / g[i][i];
      for (int k = i; k <= lg; k++) g[j][k] -= g[i][k] * rate;
      g[j][0] -= g[i][0] * rate;
    }
  }
  FOR(i, 1, lg) g[i][0] /= g[i][i], g[i][i] = 1;
  return 1;
}
double g[GE][GE];
int lg;
Matrix calc(double w[][MTX * 2]) { // 1-4 in A, 5-8 in B
  lg = 0;
  memset(g, 0, sizeof(g));
  FOR(i, 1, 4) FOR(j, 1, 4) { // i to j+4
    int ij = (i - 1) * 4 + j;
    FOR(k, 1, 4) { // k in A
      int kj = (k - 1) * 4 + j;
      g[ij][kj] = w[i][k];
    }
    FOR(k, 5, 8) { // k in A
      g[ij][0] += w[i][k];
    }
    g[ij][ij] -= 1;
    g[ij][0] = -g[ij][0];
  }
  gauss(g, 16);
  double m[MTX][MTX];
  FOR(i, 1, 4) FOR(j, 1, 4) m[i][j] = g[(i - 1) * 4 + j][0];
  return Matrix(4, 4, m);
}
const int N = 5e4 + 50;
int n, m, q;
int tot_rocks[N], rocks[N][MTX * 2]; // rocks[i,j]:从i到i/4*4+j的点的rock数
Matrix a[N];

void rebuild_matrix(int id) {
  // id*4+0..7

  double w[5][MTX * 2];
  FOR(i, 0, 3) FOR(j, 0, 7) {
    w[i + 1][j + 1] = rocks[id * 4 + i][j] * 1.0 / tot_rocks[id * 4 + i];
  }
  a[id] = calc(w);
}
void add_path(int x, int y, int z) {
  tot_rocks[x] += z;
  int id = y - x / 4 * 4;
  assert(rocks[x][id] == 0);
  rocks[x][id] = z;
  rebuild_matrix(x / 4);
}
void rem_path(int x, int y) {
  int id = y - x / 4 * 4;
  assert(rocks[x][id]);
  tot_rocks[x] -= rocks[x][id];
  rocks[x][id] = 0;
  rebuild_matrix(x / 4);
}
void dfs(int u) {}
double query(int x, int y) {
  if (x / 4 > y / 4) return 0;
  Matrix A(4);
  FOR(i, x / 4, y / 4 - 1) A *= a[i];
}

void go(int id) {
  printf("Case #%d:", id);
  scanf("%d%d%d", &n, &m, &q);
  FOR(i, 1, m) {
    int x, y, z;
    scanf("%d%d%d", &x, &y, &z);
    add_path(x, y, z);
  }
  FOR(i, 1, q) {
    int e, x, y, z;
    scanf("%d%d%d", &e, &x, &y);
    if (e == 1) {
      scanf("%d", &z);
      add_path(x, y, z);
    } else if (e == 2) {
      rem_path(x, y);
    } else {
      printf(" %lf", query(x, y));
    }
  }
  puts("");
}
int main() {
  int t;
  scanf("%d", &t);
  FOR(i, 1, t) go(i);
  return 0;
}
