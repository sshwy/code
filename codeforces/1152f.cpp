// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

const int N = 1e5 + 5, K = 13, S = 1 << 5, P = 1e9 + 7;
void add(int &x, long long y) { x = (x + y) % P; }

template <const int SZ> struct Matrix {
  int n, m, c[SZ][SZ];
  Matrix() {}
  Matrix(int _n, int _m) {
    assert(0 <= _n && _n < SZ);
    assert(0 <= _m && _m < SZ);
    n = _n, m = _m;
    FOR(i, 1, n) FOR(j, 1, m) c[i][j] = 0;
  }
  int *operator[](int x) {
    assert(0 <= x && x < SZ);
    return c[x];
  }
  Matrix operator*(Matrix M) {
    assert(m == M.n);
    Matrix res(n, M.m);
    FOR(i, 1, n) FOR(j, 1, m) if (c[i][j]) FOR(k, 1, M.m) {
      add(res[i][k], 1ll * c[i][j] * M[j][k]);
    }
    return res;
  }
};

Matrix<K * S + 1> mtx(K *S - 1, K *S - 1), a(K *S - 1, K *S - 1);

int n, k, m, lim;

int _(int j, int s) { return j * lim + s + 1; }
void addTrans(int j1, int s1, int j2, int s2, int coef) {
  add(mtx[_(j1, s1)][_(j2, s2)], coef);
}

int f[K][S], g[K][S];

int main() {
  scanf("%d%d%d", &n, &k, &m);
  f[0][0] = 1;
  lim = 1 << m;

  FOR(j, 0, k) FOR(s, 0, lim - 1) {
    addTrans(j, s, j, (s << 1) & (lim - 1), 1);
    if (j < k) {
      addTrans(j, s, j + 1, (s << 1 | 1) & (lim - 1), __builtin_popcount(s) + 1);
    }
  }
  FOR(i, 1, K * S - 1) a[i][i] = 1;

  while (n) {
    if (n & 1) a = a * mtx;
    mtx = mtx * mtx;
    n >>= 1;
  }

  int ans = 0;
  FOR(s, 0, lim - 1) add(ans, a[_(0, 0)][_(k, s)]);
  printf("%d\n", ans);
  return 0;
}
