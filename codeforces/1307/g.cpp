// by Sshwy
//#define DEBUGGER
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

#ifdef DEBUGGER

#define log_prefix()                                                \
  {                                                                 \
    fprintf(stderr, "\033[37mLine %-3d [%dms]:\033[0m  ", __LINE__, \
        (int)clock() / 1000);                                       \
  }
#define ilog(...) \
  { fprintf(stderr, __VA_ARGS__); }
#define llog(...)      \
  {                    \
    log_prefix();      \
    ilog(__VA_ARGS__); \
  }
#define log(...)           \
  {                        \
    log_prefix();          \
    ilog(__VA_ARGS__);     \
    fprintf(stderr, "\n"); \
  }
#define red(...)       \
  {                    \
    log_prefix();      \
    ilog("\033[31m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define green(...)     \
  {                    \
    log_prefix();      \
    ilog("\033[32m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define blue(...)      \
  {                    \
    log_prefix();      \
    ilog("\033[34m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }

#else

#define log_prefix() ;
#define ilog(...) ;
#define llog(...) ;
#define log(...) ;
#define red(...) ;
#define green(...) ;
#define blue(...) ;

#endif

namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 52, M = N * (N - 1) * 2, INF = 0x3f3f3f3f;

struct qxx {
  int nex, t, v, c;
} e[M];
int h[N], le = 1;
void add_path(int f, int t, int v, int c) { e[++le] = {h[f], t, v, c}, h[f] = le; }
void add_flow(int f, int t, int v, int c) {
  add_path(f, t, v, c), add_path(t, f, 0, -c);
}

int s, t;
bool vis[N];
int incf[N], pre[N];
int d[N];
queue<int> q;
bool spfa() {
  memset(d, -0x3f, sizeof(d));
  d[s] = 0;
  incf[s] = INF, incf[t] = 0;
  q.push(s);
  while (!q.empty()) {
    int u = q.front();
    q.pop();
    vis[u] = 0;
    for (int i = h[u]; i; i = e[i].nex) {
      const int v = e[i].t, w = e[i].v, c = e[i].c;
      if (!w || d[v] >= d[u] + c) continue;
      d[v] = d[u] + c, incf[v] = min(w, incf[u]), pre[v] = i;
      if (!vis[v]) q.push(v), vis[v] = 1;
    }
  }
  return incf[t];
}
int maxcost = 0;
void update() {
  maxcost += d[t];
  for (int u = t; u != s; u = e[pre[u] ^ 1].t) {
    e[pre[u]].v--;
    e[pre[u] ^ 1].v++;
  }
}

int n, m;
int f[N], lf; // f[i]表示最大流为i时的最大费用

double F(int X, int x) { return (X - f[x]) * 1.0 / x; }
double query(int x) {
  int l = 1, r = lf;
  while (l < r) {
    int mid = (l + r) >> 1;
    if (F(x, mid + 1) - F(x, mid) < 0)
      l = mid + 1;
    else
      r = mid;
  }
  log("x=%d,l=%d,F=%.3lf", x, l, F(x, l));
  return F(x, l);
}
int main() {
  scanf("%d%d", &n, &m);
  s = 1, t = n;
  bool fl = 0;
  FOR(i, 1, m) {
    int u, v, w;
    scanf("%d%d%d", &u, &v, &w);
    if (u == 1 && v == 2 && w == 1) fl = 1;
    add_flow(u, v, 1, -w);
  }
  while (spfa()) { update(), f[++lf] = maxcost; }
  f[lf + 1] = -0x3f3f3f3f;

  llog("f: ");
  FOR(i, 1, lf) ilog("%d%c", f[i], " \n"[i == lf]);

  int q;
  scanf("%d", &q);
  FOR(i, 1, q) {
    int x;
    scanf("%d", &x);
    printf("%.10lf\n", query(x));
  }

  return 0;
}
// 设最短路长度>=L，求最小花费
// min  sum x(u,v)
// s.t.
//      d[n]-d[1]>=L
//      d[v]<=d[u]+w(u,v)+x(u,v)
//
// min  sum max(d[v]-d[u]-w(u,v),0) + max(L+d[1]-d[n],0)*infty
//
// max f[i] + i*L
