// by Sshwy
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 2e5 + 5, M = 4e5 + 5;

struct qxx {
  int nex, t;
};
qxx e[M];
int h[N], le = 1;
void add_path(int f, int t) { e[++le] = (qxx){h[f], t}, h[f] = le; }

int n, m, k;
int a[N];
int x[N], y[N];

int vis[N];
int q[N], ql, qr;
void bfs(int s, int *dist) {
  memset(vis, 0, sizeof(int) * (n + 1));
  q[ql = qr = 1] = s;
  dist[s] = 0;
  vis[s] = 1;
  while (ql <= qr) {
    int u = q[ql++];
    for (int i = h[u]; i; i = e[i].nex) {
      const int &v = e[i].t;
      if (vis[v]) continue;
      dist[v] = dist[u] + 1;
      q[++qr] = v, vis[v] = 1;
    }
  }
}
int f[N], g[N];

int main() {
  scanf("%d%d%d", &n, &m, &k);
  FOR(i, 1, k) scanf("%d", a + i);
  FOR(i, 1, m) {
    int x, y;
    scanf("%d%d", &x, &y);
    add_path(x, y), add_path(y, x);
  }
  bfs(1, x);
  bfs(n, y);

  // max{ min(x_a+y_b,x_b+y_a) }
  // max{ x_a+y_b: x_a-y_a <= x_b-y_b) }

  sort(a + 1, a + k + 1, [](int a, int b) { return x[a] - y[a] < x[b] - y[b]; });
  // FOR(i,1,k)printf("%d%c",a[i]," \n"[i==k]);
  // FOR(i,1,k)printf("%d%c",x[a[i]]," \n"[i==k]);
  // FOR(i,1,k)printf("%d%c",y[a[i]]," \n"[i==k]);
  f[1] = x[a[1]];
  FOR(i, 2, k) f[i] = max(f[i - 1], x[a[i]]);
  g[k] = y[a[k]];
  ROF(i, k - 1, 1) g[i] = max(g[i + 1], y[a[i]]);
  int mx = 0;
  FOR(i, 1, k - 1) mx = max(mx, f[i] + g[i + 1]);

  if (mx + 1 >= x[n])
    printf("%d\n", x[n]);
  else
    printf("%d\n", mx + 1);
  return 0;
}
