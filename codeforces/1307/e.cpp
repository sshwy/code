// by Sshwy
//#define DEBUGGER
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

#ifdef DEBUGGER

#define log_prefix()                                                \
  {                                                                 \
    fprintf(stderr, "\033[37mLine %-3d [%dms]:\033[0m  ", __LINE__, \
        (int)clock() / 1000);                                       \
  }
#define ilog(...) \
  { fprintf(stderr, __VA_ARGS__); }
#define llog(...)      \
  {                    \
    log_prefix();      \
    ilog(__VA_ARGS__); \
  }
#define log(...)           \
  {                        \
    log_prefix();          \
    ilog(__VA_ARGS__);     \
    fprintf(stderr, "\n"); \
  }
#define red(...)       \
  {                    \
    log_prefix();      \
    ilog("\033[31m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define green(...)     \
  {                    \
    log_prefix();      \
    ilog("\033[32m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define blue(...)      \
  {                    \
    log_prefix();      \
    ilog("\033[34m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }

#else

#define log_prefix() ;
#define ilog(...) ;
#define llog(...) ;
#define log(...) ;
#define red(...) ;
#define green(...) ;
#define blue(...) ;

#endif

namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 5005, P = 1e9 + 7;

int n, m;
int a[N];

int c1[N], c2[N];
int h[N][N], w[N][N]; // h[i,j]表示颜色为i的食量小于等于j的cow的个数

void calc_h(int i) { FOR(j, 1, n) h[i][j] = h[i][j - 1] + w[i][j]; }

int main() {
  scanf("%d%d", &n, &m);
  FOR(i, 1, n) scanf("%d", &a[i]);
  FOR(i, 1, m) {
    int x, y;
    scanf("%d%d", &x, &y);
    w[x][y]++;
  }
  FOR(i, 1, n) calc_h(i);

  long long a1 = 0, a2 = 0;

  FOR(i, 0, n) {
    fill(c1, c1 + n + 1, 0);
    fill(c2, c2 + n + 1, 0);
    FOR(j, 1, i) c1[a[j]]++;
    FOR(j, i + 1, n) c2[a[j]]++;

    long long t1 = 0, t2 = 1;

    if (i == 0) {
      FOR(s, 1, n) { // sweetness
        int x = h[s][c1[s]], y = h[s][c2[s]];
        if (!x && !y)
          continue;
        else if (x == 0) {
          t1++;
          t2 = t2 * y % P;
        } else if (y == 0) {
          t1++;
          t2 = t2 * x % P;
        } else {
          t1 += 2;
          long long c = (x * 1ll * y - min(x, y)) % P;
          t2 = t2 * c % P;
        }
      }
      if (t1 > a1)
        a1 = t1, a2 = t2;
      else if (t1 == a1)
        a2 = (a2 + t2) % P;
      log("i=%d,t1=%lld,t2=%lld", i, t1, t2);
    } else if (w[a[i]][c1[a[i]]]) {
      int &toggle = w[a[i]][c1[a[i]]];
      --toggle;
      calc_h(a[i]);
      c1[a[i]] = 0; //全被吃了
      ++t1;

      FOR(s, 1, n) { // sweetness
        int x = h[s][c1[s]], y = h[s][c2[s]];
        log("s=%d,x=%d,y=%d", s, x, y);
        if (!x && !y)
          continue;
        else if (x == 0) {
          t1++;
          t2 = t2 * y % P;
        } else if (y == 0) {
          t1++;
          t2 = t2 * x % P;
        } else if (x == 1 && y == 1) {
          t1++;
          t2 = t2 * 2 % P;
        } else {
          t1 += 2;
          long long c = (x * 1ll * y - min(x, y)) % P;
          t2 = t2 * c % P;
        }
      }
      if (t1 > a1)
        a1 = t1, a2 = t2;
      else if (t1 == a1)
        a2 = (a2 + t2) % P;
      log("i=%d,t1=%lld,t2=%lld", i, t1, t2);

      ++toggle;
      calc_h(a[i]);
    }
  }
  int A1 = a1, A2 = a2 % P;
  printf("%d %d\n", A1, A2);
  return 0;
}
