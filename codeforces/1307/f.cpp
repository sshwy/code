// by Sshwy
//#define DEBUGGER
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

#ifdef DEBUGGER

#define log_prefix()                                                \
  {                                                                 \
    fprintf(stderr, "\033[37mLine %-3d [%dms]:\033[0m  ", __LINE__, \
        (int)clock() / 1000);                                       \
  }
#define ilog(...) \
  { fprintf(stderr, __VA_ARGS__); }
#define llog(...)      \
  {                    \
    log_prefix();      \
    ilog(__VA_ARGS__); \
  }
#define log(...)           \
  {                        \
    log_prefix();          \
    ilog(__VA_ARGS__);     \
    fprintf(stderr, "\n"); \
  }
#define red(...)       \
  {                    \
    log_prefix();      \
    ilog("\033[31m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define green(...)     \
  {                    \
    log_prefix();      \
    ilog("\033[32m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define blue(...)      \
  {                    \
    log_prefix();      \
    ilog("\033[34m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }

#else

#define log_prefix() ;
#define ilog(...) ;
#define llog(...) ;
#define log(...) ;
#define red(...) ;
#define green(...) ;
#define blue(...) ;

#endif

namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 2e5 + 5;

int n, k, r;
int R[N];
vector<int> g[N * 2];

queue<int> q;
int d[N * 2];
int f[N * 2];
int get(int u) { return f[u] == u ? u : f[u] = get(f[u]); }
void merge(int u, int v) { f[get(u)] = get(v); }

int dep[N * 2], fa[N * 2][20];
void dfs(int u, int p) {
  dep[u] = dep[p] + 1;
  fa[u][0] = p;
  FOR(j, 1, 19) {
    fa[u][j] = fa[fa[u][j - 1]][j - 1];
    if (fa[u][j] == 0) break;
  }
  for (int v : g[u])
    if (v != p) dfs(v, u);
}

int lca(int x, int y) {
  if (dep[x] < dep[y]) swap(x, y);
  ROF(j, 19, 0) if (dep[x] - (1 << j) >= dep[y]) x = fa[x][j];
  if (x == y) return x;
  ROF(j, 19, 0) if (fa[x][j] != fa[y][j]) x = fa[x][j], y = fa[y][j];
  return fa[x][0];
}
int anc(int x, int y) {
  ROF(j, 19, 0) if (y >> j & 1) x = fa[x][j];
  return x;
}
bool query(int x, int y) {
  if (get(x) == get(y)) return 1;
  int z = lca(x, y), len = dep[x] + dep[y] - dep[z] * 2;
  if (len <= 2 * k) return 1;
  int X = dep[x] - dep[z] >= k ? anc(x, k) : anc(y, len - k);
  int Y = dep[y] - dep[z] >= k ? anc(y, k) : anc(x, len - k);
  if (get(X) == get(Y)) return 1;
  return 0;
}
int main() {
  scanf("%d%d%d", &n, &k, &r);
  FOR(i, 1, n - 1) {
    int x, y;
    scanf("%d%d", &x, &y);
    g[x].pb(n + i), g[n + i].pb(x);
    g[y].pb(n + i), g[n + i].pb(y);
  }
  FOR(i, 1, r) scanf("%d", R + i);

  fill(d, d + n + n, -1);
  FOR(i, 1, r) q.push(R[i]), d[R[i]] = 0;
  FOR(i, 1, n * 2) f[i] = i;

  while (!q.empty()) {
    int u = q.front();
    q.pop();
    if (d[u] < k)
      for (int v : g[u]) {
        merge(u, v);
        if (d[v] == -1) {
          d[v] = d[u] + 1;
          q.push(v);
        }
      }
  }

  dfs(1, 0);
  int q;
  scanf("%d", &q);
  FOR(i, 1, q) {
    int a, b;
    scanf("%d%d", &a, &b);
    puts(query(a, b) ? "YES" : "NO");
  }

  return 0;
}
