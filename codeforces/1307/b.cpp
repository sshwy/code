// by Sshwy
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/

int n, x;

void go() {
  cin >> n >> x;
  int mx = 0, a, tag = 0;
  FOR(i, 1, n) {
    cin >> a;
    if (a <= x)
      mx = max(mx, a);
    else
      tag = 1;
  }
  int ans = 2e9;
  if (tag) ans = min(ans, 2);
  if (mx) ans = min(ans, (x - 1) / mx + 1);
  cout << ans << endl;
}
int main() {
  int t;
  cin >> t;
  FOR(i, 1, t) go();
  return 0;
}
