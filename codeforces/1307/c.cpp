// by Sshwy
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 1e5 + 5;

char s[N];
int n;
long long ans;

long long calc(int A, int B) {
  int cA = 0;
  long long res = 0;
  FOR(i, 1, n) {
    if (s[i] == A) { cA++; }
    if (s[i] == B) { res += cA; }
  }
  return res;
}
int main() {
  cin >> s + 1;
  n = strlen(s + 1);
  {
    int c[30] = {0};
    FOR(i, 1, n) c[s[i] - 'a']++;
    ans = *max_element(c, c + 26);
    if (ans > 3) ans = ans * (ans - 1ll) / 2;
  }
  FOR(i, 'a', 'z') FOR(j, 'a', 'z') if (i != j) ans = max(ans, calc(i, j));
  cout << ans;
  return 0;
}
