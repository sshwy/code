// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

const int N = 3e5 + 5;

int n, L[N], R[N], totdfn;
size_t ans = 0;
set<pair<int, int>> s;
vector<int> g1[N], g2[N];

bool has_replaced;
pair<int, int> env_replaced;

void Add(int l, int r) {
  has_replaced = false;
  pair<int, int> pr(l, r);
  if (s.empty()) {
    s.insert(pr);
    return;
  }
  auto p = s.lower_bound(pr);
  if (p != s.end() && p->second <= r) return;
  if (p == s.begin()) {
    s.insert(pr);
    return;
  }
  p = prev(p);
  if (p->first <= l && r <= p->second) {
    has_replaced = true;
    env_replaced = *p;
    s.erase(p);
  }
  s.insert(pr);
}
void Sub(int l, int r) {
  pair<int, int> pr(l, r);
  s.erase(pr);
}

void dfs1(int u) {
  Add(L[u], R[u]);
  bool flag = has_replaced;
  pair<int, int> replaced = env_replaced;
  ans = max(ans, s.size());
  for (int v : g1[u]) dfs1(v);
  Sub(L[u], R[u]);
  if (flag) s.insert(replaced);
}

void dfs2(int u) {
  L[u] = ++totdfn;
  for (int v : g2[u]) dfs2(v);
  R[u] = totdfn;
}

void go() {
  ans = 0;
  totdfn = 0;
  s.clear();
  FOR(i, 1, n) g1[i].clear(), g2[i].clear();

  scanf("%d", &n);
  FOR(i, 2, n) {
    int u;
    scanf("%d", &u);
    g1[u].pb(i);
  }
  FOR(i, 2, n) {
    int u;
    scanf("%d", &u);
    g2[u].pb(i);
  }
  dfs2(1);
  dfs1(1);
  printf("%lu\n", ans);
}

int main() {
  int t;
  scanf("%d", &t);
  FOR(i, 1, t) go();
  return 0;
}
