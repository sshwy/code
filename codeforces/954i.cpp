// by Sshwy
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA
const int N = 2e5 + 5;
void get_next(int *t, int lt, int *nex) {
  nex[1] = 0;
  int pos = 0;
  FOR(i, 2, lt) {
    while (pos && t[pos + 1] != t[i]) pos = nex[pos];
    if (t[pos + 1] == t[i]) ++pos;
    nex[i] = pos;
  }
}
char s[N], t[N];
int s2[N], t2[N], nex[N], c[N];
int ls, lt;
int main() {
  scanf("%s%s", s + 1, t + 1);
  ls = strlen(s + 1), lt = strlen(t + 1);
  FOR(S, 0, (1 << 6) - 1) {
    FOR(i, 1, ls) s2[i] = S >> (s[i] - 'a') & 1;
    FOR(i, 1, lt) t2[i] = S >> (t[i] - 'a') & 1;
    get_next(t2, lt, nex);
    s2[ls + 1] = t2[lt + 1] = 10;
    int pos = 0;
    FOR(i, 1, ls) {
      while (pos && t2[pos + 1] != s2[i]) pos = nex[pos];
      if (t2[pos + 1] == s2[i]) ++pos;
      if (pos == lt) c[i]++;
    }
  }
  FOR(i, lt, ls) printf("%d%c", 6 - __builtin_ctz(c[i]), " \n"[i == ls]);
  return 0;
}
