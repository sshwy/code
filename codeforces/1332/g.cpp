// by Sshwy
#define DEBUGGER
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

#ifdef DEBUGGER

#define log_prefix()                                                \
  {                                                                 \
    fprintf(stderr, "\033[37mLine %-3d [%dms]:\033[0m  ", __LINE__, \
        (int)clock() / 1000);                                       \
  }
#define ilog(...) \
  { fprintf(stderr, __VA_ARGS__); }
#define llog(...)      \
  {                    \
    log_prefix();      \
    ilog(__VA_ARGS__); \
  }
#define log(...)           \
  {                        \
    log_prefix();          \
    ilog(__VA_ARGS__);     \
    fprintf(stderr, "\n"); \
  }
#define red(...)       \
  {                    \
    log_prefix();      \
    ilog("\033[31m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define green(...)     \
  {                    \
    log_prefix();      \
    ilog("\033[32m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define blue(...)      \
  {                    \
    log_prefix();      \
    ilog("\033[34m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }

#else

#define log_prefix() ;
#define ilog(...) ;
#define llog(...) ;
#define log(...) ;
#define red(...) ;
#define green(...) ;
#define blue(...) ;

#endif

namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 2e5 + 5;
int n, q, vis[N], s1[N], s2[N], l1, l2, a3[N], a4[N], a[N], p1[N], p2[N];
set<int> A[3]; //不在单调栈，不在第一个，不在第二个
vector<pair<int, int>> qry[N];
pair<int, int> ans[N];
struct atom {
  int a, b, c, d;
} at3[N], at4[N]; //以i为右端点的子序列方案

void add(int id, int x) {
  if (vis[x] == 0) A[0].erase(x);
  vis[x]++;
  A[id].erase(x);
}
void rem(int id, int x) {
  vis[x]--;
  if (vis[x] == 0) A[0].insert(x);
  A[id].insert(x);
}
int main() {
  ios::sync_with_stdio(false);
  cin.tie(0);
  cout.tie(0);

  cin >> n >> q;
  FOR(i, 1, n) cin >> a[i];
  FOR(i, 1, q) {
    int l, r;
    cin >> l >> r;
    qry[l].pb({r, i});
  }
  FOR(i, 1, n + 1) a3[i] = a4[i] = n + 1;
  FOR(j, 0, 2) FOR(i, 1, n) A[j].insert(i);
  ROF(i, n, 1) {
    while (l1 > 0 && a[i] > a[s1[l1]]) rem(1, s1[l1]), --l1;
    s1[++l1] = i, add(1, i);
    if (l1 > 1) p1[i] = a[i] < a[s1[l1 - 1]] ? s1[l1 - 1] : p1[s1[l1 - 1]];

    while (l2 > 0 && a[i] < a[s2[l2]]) rem(2, s2[l2]), --l2;
    s2[++l2] = i, add(2, i);
    if (l2 > 1) p2[i] = a[i] > a[s2[l2 - 1]] ? s2[l2 - 1] : p2[s2[l2 - 1]];

    auto find1 = [](int x) {
      int l = 1, r = l1, mid;
      while (l < r) mid = (l + r) >> 1, s1[mid] < x ? r = mid : l = mid + 1;
      // assert(s1[l]<x);
      return s1[l];
    };
    auto find2 = [](int x) {
      int l = 1, r = l2, mid;
      while (l < r) mid = (l + r) >> 1, s2[mid] < x ? r = mid : l = mid + 1;
      // assert(s2[l]<x);
      return s2[l];
    };
    if (p1[i] && p2[i]) { // a4
      int pos = max(p1[i], p2[i]);
      auto p = A[0].upper_bound(pos);
      if (p != A[0].end()) {
        a4[i] = min(a4[i], *p);
        if (at4[*p].a == 0) {
          int x = find1(*p), y = find2(*p);
          if (x > y) swap(x, y);
          at4[*p] = {i, x, y, *p};
        }
      }
    }
    if (p1[i]) { // a3
      int pos = p1[i];
      auto p = A[1].upper_bound(pos);
      if (p != A[1].end()) {
        a3[i] = min(a3[i], *p);
        if (at3[*p].a == 0) {
          int x = find1(*p);
          at3[*p] = {i, x, *p, 0};
        }
      }
    }
    if (p2[i]) { // a3
      int pos = p2[i];
      auto p = A[2].upper_bound(pos);
      if (p != A[2].end()) {
        a3[i] = min(a3[i], *p);
        if (at3[*p].a == 0) {
          int x = find2(*p);
          at3[*p] = {i, x, *p, 0};
        }
      }
    }

    a4[i] = min(a4[i], a4[i + 1]);
    a3[i] = min(a3[i], a3[i + 1]);

    for (auto p : qry[i]) {
      if (a4[i] <= p.fi) {
        ans[p.se] = {4, a4[i]};
      } else if (a3[i] <= p.fi) {
        ans[p.se] = {3, a3[i]};
      }
    }
  }
  // FOR(i,1,n)printf("%d%c",a3[i]," \n"[i==n]);
  // FOR(i,1,n)printf("%d%c",a4[i]," \n"[i==n]);

  FOR(i, 1, q) {
    cout << ans[i].fi << endl;
    if (ans[i].fi == 3) {
      auto x = at3[ans[i].se];
      cout << x.a << " " << x.b << " " << x.c << endl;
    } else if (ans[i].fi == 4) {
      auto x = at4[ans[i].se];
      cout << x.a << " " << x.b << " " << x.c << " " << x.d << endl;
    }
  }
  return 0;
}

// 长度大于4的一定有triple
// 因此我们考虑是否存在L3和L4。
// 首先，non-triple中不能有相邻相同元素。
// 对于i，求出它往后最短的L4。L3类似。
// 找到第一个比i大的数、第一个比i小的数的位置，x,y。
// 则第4个数一定在x,y后面。易证。
// 第4个数一定不在单调栈中。易证。
// 不在单调栈中的数，一定存在一个4。在它前面一定有比它大和比它小的数。
// 因此找一个，在x,y后面，不在单调栈中的数即可。
