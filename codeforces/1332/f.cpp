// by Sshwy
//#define DEBUGGER
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

#ifdef DEBUGGER

#define log_prefix()                                                \
  {                                                                 \
    fprintf(stderr, "\033[37mLine %-3d [%dms]:\033[0m  ", __LINE__, \
        (int)clock() / 1000);                                       \
  }
#define ilog(...) \
  { fprintf(stderr, __VA_ARGS__); }
#define llog(...)      \
  {                    \
    log_prefix();      \
    ilog(__VA_ARGS__); \
  }
#define log(...)           \
  {                        \
    log_prefix();          \
    ilog(__VA_ARGS__);     \
    fprintf(stderr, "\n"); \
  }
#define red(...)       \
  {                    \
    log_prefix();      \
    ilog("\033[31m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define green(...)     \
  {                    \
    log_prefix();      \
    ilog("\033[32m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define blue(...)      \
  {                    \
    log_prefix();      \
    ilog("\033[34m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }

#else

#define log_prefix() ;
#define ilog(...) ;
#define llog(...) ;
#define log(...) ;
#define red(...) ;
#define green(...) ;
#define blue(...) ;

#endif

namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 3e5 + 5, P = 998244353;
int n;
vector<int> g[N];
typedef long long LL;
LL f[N][2][2];

// f[u,x,y]:u为根的子树，u的父边选不选，u在不在独立集里，的方案数。
// f[u,1,1]：u在独立集里。且u的父边必选。儿子：如果不选边，则f[v,0,0/1]都可以。如果选边，则只有f[v,1,0]可以。而u的存在也是合法的。
// f[u,0,1]：u在独立集中，但u的父边不选。则至少有一个儿子要连他（因为u在子图中）。不然是不合法的。因此用上述的方案减掉所有儿子边都不选的方案即可。
// f[u,1,0]：u不在独立集中，u的父边必选。儿子：f[v,0/1,0/1]都可以。
// f[u,0,0]：u不在独立集中，u的父边不选。这时，u可以选择在子图中，或者不在子图中。
//若u不在子图中，则儿子的边都不能选。
//若u在子图中，则儿子的边至少选一个。
//至于儿子的方案，无所谓。你儿子：f[v,0/1,0/1]都可以。但是也不能让u成为孤立点。因此也要减掉都不选的方案。

void dfs(int u, int p) {
  for (int v : g[u])
    if (v != p) dfs(v, u);

  LL x1 = 1, x2 = 1, x3 = 1;
  for (int v : g[u])
    if (v != p) {
      LL c1 = (f[v][0][0] + f[v][0][1] + f[v][1][0]) % P;
      x1 = x1 * c1 % P;
      LL c2 = (f[v][0][0] + f[v][0][1]) % P;
      x2 = x2 * c2 % P;
      LL c3 = (f[v][0][0] + f[v][0][1] + f[v][1][0] + f[v][1][1]) % P;
      x3 = x3 * c3 % P;
    }
  f[u][1][1] = x1;
  f[u][0][1] = (x1 - x2 + P) % P;
  f[u][1][0] = x3;
  f[u][0][0] = x3;
  // printf("f %d %d %d : %lld\n",u,1,1,f[u][1][1]);
  // printf("f %d %d %d : %lld\n",u,0,1,f[u][0][1]);
  // printf("f %d %d %d : %lld\n",u,1,0,f[u][1][0]);
  // printf("f %d %d %d : %lld\n",u,0,0,f[u][0][0]);
}
int main() {
  scanf("%d", &n);
  FOR(i, 1, n - 1) {
    int u, v;
    scanf("%d%d", &u, &v);
    g[u].pb(v);
    g[v].pb(u);
  }
  dfs(1, 0);
  LL ans = (f[1][0][1] + f[1][0][0] - 1 + P) % P;
  printf("%lld\n", ans);
  return 0;
}
