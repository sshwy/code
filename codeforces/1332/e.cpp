// by Sshwy
//#define DEBUGGER
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

#ifdef DEBUGGER

#define log_prefix()                                                \
  {                                                                 \
    fprintf(stderr, "\033[37mLine %-3d [%dms]:\033[0m  ", __LINE__, \
        (int)clock() / 1000);                                       \
  }
#define ilog(...) \
  { fprintf(stderr, __VA_ARGS__); }
#define llog(...)      \
  {                    \
    log_prefix();      \
    ilog(__VA_ARGS__); \
  }
#define log(...)           \
  {                        \
    log_prefix();          \
    ilog(__VA_ARGS__);     \
    fprintf(stderr, "\n"); \
  }
#define red(...)       \
  {                    \
    log_prefix();      \
    ilog("\033[31m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define green(...)     \
  {                    \
    log_prefix();      \
    ilog("\033[32m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define blue(...)      \
  {                    \
    log_prefix();      \
    ilog("\033[34m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }

#else

#define log_prefix() ;
#define ilog(...) ;
#define llog(...) ;
#define log(...) ;
#define red(...) ;
#define green(...) ;
#define blue(...) ;

#endif

namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int MTX = 4, P = 998244353;
#define LL long long
struct Matrix {
  int w, h;
  LL c[MTX][MTX];
  Matrix(int _h = 0, int _w = 0) {
    w = _w, h = _h;
    FOR(i, 0, h) FOR(j, 1, w) c[i][j] = 0;
  }
  Matrix(int _h, int _w, LL _c[MTX][MTX]) {
    w = _w, h = _h;
    FOR(i, 1, h) FOR(j, 1, w) c[i][j] = _c[i][j];
  }
  Matrix(int _w) {
    w = h = _w;
    FOR(i, 1, h) FOR(j, 1, w) c[i][j] = 0;
    FOR(i, 1, h) c[i][i] = 1;
  }
  Matrix operator*(const Matrix &m) {
    assert(w == m.h);
    Matrix res(h, m.w);
    FOR(i, 1, h) FOR(k, 1, w) FOR(j, 1, m.w) {
      res.c[i][j] = (res.c[i][j] + c[i][k] * 1ll * m.c[k][j]) % P;
    }
    return res;
  }
};

Matrix pw(Matrix M, long long k) {
  Matrix res = M;
  --k;
  while (k) {
    if (k & 1) res = res * M;
    M = M * M;
    k >>= 1;
  }
  return res;
}
long long pw(long long a, long long m, long long P) {
  long long res = 1;
  while (m) m & 1 ? res = res * 1ll * a % P : 0, a = 1ll * a * a % P, m >>= 1;
  return res;
}
int main() {
  long long n, m, L, R;
  cin >> n >> m >> L >> R;

  if ((n * m) & 1) {
    cout << pw(R - L + 1, n * m, P) << endl;
    return 0;
  }

  long long x = R - L + 1;
  if (R % 2 && L % 2) --x;
  if (R % 2 == 0 && L % 2 == 0) ++x;
  x /= 2; //偶数个数
  long long y = R - L + 1 - x;
  // cout<<"x "<<x<<" y "<<y<<endl;
  long long mt[MTX][MTX] = {{0}, {0, x, y}, {0, y, x}};
  long long f[MTX][MTX] = {{0}};
  f[1][1] = x;
  f[2][1] = y;
  long long k = n * 1ll * m - 1;

  Matrix Mt(2, 2, mt), F(2, 1, f);

  assert(k > 0);

  Matrix X = pw(Mt, k);
  F = X * F;
  cout << F.c[1][1] << endl;
  return 0;
}
