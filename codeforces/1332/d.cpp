// by Sshwy
//#define DEBUGGER
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

#ifdef DEBUGGER

#define log_prefix()                                                \
  {                                                                 \
    fprintf(stderr, "\033[37mLine %-3d [%dms]:\033[0m  ", __LINE__, \
        (int)clock() / 1000);                                       \
  }
#define ilog(...) \
  { fprintf(stderr, __VA_ARGS__); }
#define llog(...)      \
  {                    \
    log_prefix();      \
    ilog(__VA_ARGS__); \
  }
#define log(...)           \
  {                        \
    log_prefix();          \
    ilog(__VA_ARGS__);     \
    fprintf(stderr, "\n"); \
  }
#define red(...)       \
  {                    \
    log_prefix();      \
    ilog("\033[31m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define green(...)     \
  {                    \
    log_prefix();      \
    ilog("\033[32m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define blue(...)      \
  {                    \
    log_prefix();      \
    ilog("\033[34m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }

#else

#define log_prefix() ;
#define ilog(...) ;
#define llog(...) ;
#define log(...) ;
#define red(...) ;
#define green(...) ;
#define blue(...) ;

#endif

namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/

int k;
int a[5][5], dp[5][5];

int main() {
  cin >> k;
  int n = 3, m = 4;
  a[1][1] = a[3][1] = a[3][2] = a[3][3] = (1 << 18) - 1;
  a[1][2] = a[1][3] = a[2][3] = a[3][4] = (1 << 17) - 1;
  a[2][1] = 1 << 17;
  a[2][2] = a[1][4] = 0;
  a[2][4] = (1 << 17) - 1 - k;
  cout << n << " " << m << endl;
  FOR(i, 1, n) FOR(j, 1, m) cout << a[i][j] << " \n"[j == m];
  // dp[1][1]=a[1][1];
  // FOR(i,1,n)FOR(j,1,m){
  //    if(i==1 && j==1);
  //    else dp[i][j]=max(dp[i-1][j]&a[i][j],dp[i][j-1]&a[i][j]);
  //}
  // cout<<(1<<17)-1-dp[n][m];
  return 0;
}
