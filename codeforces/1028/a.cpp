#include <bits/stdc++.h>
#define FOR(a, b, c) for (int a = b; a <= int(c); ++a)
#define ROF(a, b, c) for (int a = b; a >= int(c); --a)
#define pb push_back
using namespace std;
const int N = 120;
char A[N][N];
int main() {
  int n, m;
  cin >> n >> m;
  FOR(i, 1, n) cin >> (A[i] + 1);
  int a, b, c, d;
  FOR(i, 1, n) {
    bool fl = 0;
    FOR(j, 1, m) {
      if (A[i][j] == 'B') {
        a = i, b = j;
        fl = 1;
        break;
      }
    }
    if (fl) break;
  }
  FOR(i, 1, n) FOR(j, 1, m) {
    if (A[i][j] == 'B') { c = i, d = j; }
  }
  cout << (a + c) / 2 << " " << (b + d) / 2 << endl;
  return 0;
}