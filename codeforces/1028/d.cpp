#include <bits/stdc++.h>
#define FOR(a, b, c) for (int a = b; a <= int(c); ++a)
#define ROF(a, b, c) for (int a = b; a >= int(c); --a)
#define pb push_back
using namespace std;
const int N = 5e5 + 5, P = 1e9 + 7;
int n;
priority_queue<int, vector<int>, greater<int>> S;
priority_queue<int> B;
set<int> s;
int main() {
  scanf("%d", &n);
  int ans = 1;
  FOR(i, 1, n) {
    char op[10];
    int x;
    scanf("%s%d", op, &x);
    if (op[1] == 'C') {
      if (S.size() && x == S.top()) {
        S.pop();
        for (auto y : s) { B.push(y); }
        s.clear();
      } else if (B.size() && x == B.top()) {
        B.pop();
        for (auto y : s) { S.push(y); }
        s.clear();
      } else if (s.size() && s.find(x) != s.end()) {
        for (auto y : s) {
          if (y < x)
            B.push(y);
          else if (y > x)
            S.push(y);
        }
        s.clear();
        ans = ans * 2ll % P;
      } else {
        ans = 0;
      }
    } else {
      if (S.size() && x > S.top())
        S.push(x);
      else if (B.size() && x < B.top())
        B.push(x);
      else
        s.insert(x);
    }
  }
  ans = ans * 1ll * int(s.size() + 1) % P;
  printf("%d\n", ans);
  return 0;
}