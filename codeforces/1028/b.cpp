#include <bits/stdc++.h>
#define FOR(a, b, c) for (int a = b; a <= int(c); ++a)
#define ROF(a, b, c) for (int a = b; a >= int(c); --a)
#define pb push_back
using namespace std;
int n, m;

int main() {
  cin >> n >> m;
  int cnt = ceil(n / 9.0);
  string s(cnt, '9');
  string t = s + string(cnt - 1, '0') + "1";
  cout << s << endl << t << endl;
  return 0;
}