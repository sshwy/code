#include <bits/stdc++.h>
#define FOR(a, b, c) for (int a = b; a <= int(c); ++a)
#define ROF(a, b, c) for (int a = b; a >= int(c); --a)
#define pb push_back
using namespace std;

long long sq(long long x) { return x * x; }
typedef pair<long long, long long> point;
map<int, vector<point>> mp;
map<point, int> ans, ang;
point sym(point x, point y) {
  point z = {x.first + y.first, x.second + y.second};
  long long g = __gcd(z.first, z.second);
  z.first /= g;
  z.second /= g;
  return z;
}
void insert(int x, int y) {
  long long c = sq(x) + sq(y);
  point u(x, y);
  for (auto v : mp[c]) {
    auto z = sym(u, v);
    if (ans.count(z))
      ans[z]++;
    else
      ans[z] = 1;
  }
  mp[c].pb(u);
  long long g = __gcd(x, y);
  u.first /= g;
  u.second /= g;
  if (ang.count(u))
    ang[u]++;
  else
    ang[u] = 1;
}
void remove(int x, int y) {
  long long c = sq(x) + sq(y);
  point u(x, y);
  auto &V = mp[c];
  for (auto p = V.begin(); p != V.end(); ++p) {
    if (*p == u) {
      V.erase(p);
      break;
    }
  }
  for (auto v : V) {
    auto z = sym(u, v);
    if (ans.count(z))
      ans[z]--;
    else
      assert(0);
  }
  long long g = __gcd(x, y);
  u.first /= g;
  u.second /= g;
  if (ang.count(u))
    ang[u]--;
  else
    assert(0);
}
int qry(int x, int y) {
  int g = __gcd(x, y);
  x /= g;
  y /= g;
  return ans[{x, y}];
}
int main() {
  int q, tot = 0;
  scanf("%d", &q);
  FOR(i, 1, q) {
    int t, x, y;
    scanf("%d%d%d", &t, &x, &y);
    if (t == 1) {
      insert(x, y);
      ++tot;
    } else if (t == 2) {
      remove(x, y);
      --tot;
    } else {
      int t = qry(x, y);
      int g = __gcd(x, y);
      printf("%d\n", tot - 2 * t - ang[{x / g, y / g}]);
    }
  }
  return 0;
}