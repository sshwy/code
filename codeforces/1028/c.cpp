#include <bits/stdc++.h>
#define FOR(a, b, c) for (int a = b; a <= int(c); ++a)
#define ROF(a, b, c) for (int a = b; a >= int(c); --a)
#define pb push_back
using namespace std;
const int N = 2e5 + 5;
int n;
struct atom {
  int a, b, c, d;
} a[N];
struct pq {
  priority_queue<int> A, B;
  void push(int x) { A.push(x); }
  void remove(int x) { B.push(x); }
  int top() {
    while (A.size() && B.size() && A.top() == B.top()) {
      A.pop();
      B.pop();
    }
    assert(A.size());
    return A.top();
  }
} sx1, sx2, sy1, sy2;
int main() {
  scanf("%d", &n);
  FOR(i, 1, n) scanf("%d%d%d%d", &a[i].a, &a[i].b, &a[i].c, &a[i].d);
  FOR(i, 1, n) {
    sx1.push(a[i].a);
    sy1.push(a[i].b);
    sx2.push(-a[i].c);
    sy2.push(-a[i].d);
  }
  FOR(i, 1, n) {
    sx1.remove(a[i].a);
    sy1.remove(a[i].b);
    sx2.remove(-a[i].c);
    sy2.remove(-a[i].d);

    int x1 = sx1.top(), x2 = -sx2.top();
    if (x1 <= x2) {
      int y1 = sy1.top(), y2 = -sy2.top();
      if (y1 <= y2) {
        printf("%d %d\n", x1, y1);
        return 0;
      }
    }

    sx1.push(a[i].a);
    sy1.push(a[i].b);
    sx2.push(-a[i].c);
    sy2.push(-a[i].d);
  }
  return 0;
}