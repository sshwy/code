#include <bits/stdc++.h>
#define FOR(a, b, c) for (int a = b; a <= int(c); ++a)
#define ROF(a, b, c) for (int a = b; a >= int(c); --a)
#define pb push_back
using namespace std;
const int N = 6e6 + 5, Q = 2e6 + 5;
int n, q, f[N][8], g[20], a[N], t[1 << 8], ans[N];
vector<pair<int, int>> qry[Q];
vector<int> trans(int x) {
  vector<int> d, res;
  for (int i = 2; i * i <= x; ++i) {
    if (x % i) continue;
    int c = 0;
    while (x % i == 0) x /= i, c ^= 1;
    if (c) d.pb(i);
  }
  if (x > 1) d.pb(x);
  return d;
}
void getmax(int &x, int y) { x = max(x, y); }
int main() {
  scanf("%d%d", &n, &q);
  FOR(i, 1, n) scanf("%d", &a[i]);
  FOR(i, 1, q) {
    int l, r;
    scanf("%d%d", &l, &r);
    qry[r].pb({l, i});
  }
  // puts("GG");
  FOR(i, 1, n) {
    // printf("i %d\n",i);
    auto v = trans(a[i]);
    // printf("i %d\n",i);

    int lim = 1 << v.size();
    t[0] = 1;
    FOR(s, 0, lim - 1) {
      if (s) t[s] = t[s ^ (s & -s)] * v[__builtin_ctz(s)];
      int x = t[s];
      // printf("s %d x %d\n",s,x);
      int l1 = v.size() - __builtin_popcount(s);
      FOR(j, 0, 7) { getmax(g[j + l1], f[x][j]); }
    }

    FOR(s, 0, lim - 1) {
      int x = t[s];
      int l1 = v.size() - __builtin_popcount(s);
      getmax(f[x][l1], i);
    }

    for (auto qr : qry[i]) {
      FOR(j, 0, 19) if (qr.first <= g[j]) {
        ans[qr.second] = j;
        break;
      }
    }
  }
  FOR(i, 1, q) printf("%d\n", ans[i]);
  return 0;
}