#include <bits/stdc++.h>
#define FOR(a, b, c) for (int a = b; a <= int(c); ++a)
#define ROF(a, b, c) for (int a = b; a >= int(c); --a)
#define pb push_back
using namespace std;
const int N = 2e5 + 5;
int b[N], n;
long long a[N];
void YES() {
  cout << "YES" << endl;
  FOR(i, 1, n) cout << a[i] << " \n"[i == n];
  exit(0);
}
void NO() {
  cout << "NO" << endl;
  exit(0);
}
void start(int st) { // b[st] == a[st]
  a[st] = b[st];
  assert(a[st] > b[st - 1]);
  // printf("st %d a %lld\n",st,a[st]);
  for (int i = st - 1; i % n != st % n; --i) {
    if (i == 0) i += n;
    // printf("i %d\n",i);
    long long x = a[i % n + 1];
    // printf("x %lld\n",x);
    // a[i] = b[i] + k*x > b[i-1]
    // k*x > b[i-1] - b[i]
    a[i] = b[i] + ((b[i - 1] - b[i]) / x + 1) * x;
    assert(a[i] > b[i - 1]);
    if (a[i] > 1e18) NO();
  }
}
int main() {
  ios::sync_with_stdio(0);
  cin.tie(0);
  cout.tie(0);
  cin >> n;
  FOR(i, 1, n) cin >> b[i];
  if (*max_element(b + 1, b + n + 1) == 0) {
    FOR(i, 1, n) a[i] = 1;
    YES();
  } else if (*max_element(b + 1, b + n + 1) == *min_element(b + 1, b + n + 1)) {
    NO();
  } else {
    b[0] = b[n];
    b[n + 1] = b[1];
    int pos = min_element(b + 1, b + n + 1) - b;
    while (b[pos + 1] <= b[pos]) {
      ++pos;
      if (pos > n) pos -= n;
    }
    assert(b[pos + 1] > b[pos]);
    start(pos % n + 1);
    YES();
  }
  return 0;
}