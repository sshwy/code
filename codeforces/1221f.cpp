#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
namespace IO {
  char nc() {
    static char bf[100000], *p1 = bf, *p2 = bf;
    return p1 == p2 && (p2 = (p1 = bf) + fread(bf, 1, 100000, stdin), p1 == p2)
               ? EOF
               : *p1++;
  }
  int rd() {
    int res = 0;
    char c = getchar();
    while (!isdigit(c)) c = getchar();
    while (isdigit(c)) res = res * 10 + c - '0', c = getchar();
    return res;
  }
} // namespace IO
/******************heading******************/
#define int lld
const int N = 1e6 + 5, SZ = N << 2;

int n, maxl;

namespace DC {
  int dc[N * 4], ld;
  void add(int x) { dc[++ld] = x; }
  void work() {
    sort(dc + 1, dc + ld + 1), ld = unique(dc + 1, dc + ld + 1) - dc - 1;
  }
  int tr(int x) { return lower_bound(dc + 1, dc + ld + 1, x) - dc; }
} // namespace DC
struct data {
  int x, y, c;
  void read() {
    cin >> x >> y >> c;
    DC::add(x), DC::add(y);
    if (x > y) swap(x, y); // above y=x
  }
  void refresh() {
    x = DC::tr(x);
    y = DC::tr(y);
  }
  bool operator<(data d) const { return y < d.y; }
} d[N];

int s[SZ], sr[SZ], srp[SZ];
void nodeadd(int u, int v) { s[u] += v, sr[u] += v; }
void pushup(int u) {
  s[u] = s[u << 1] + s[u << 1 | 1];
  if (sr[u << 1 | 1] > sr[u << 1] + s[u << 1 | 1]) {
    sr[u] = sr[u << 1 | 1], srp[u] = srp[u << 1 | 1];
  } else {
    sr[u] = sr[u << 1] + s[u << 1 | 1], srp[u] = srp[u << 1];
  }
}
void build(int u = 1, int l = 1, int r = maxl) {
  if (l == r) return srp[u] = l, void();
  int mid = (l + r) >> 1;
  build(u << 1, l, mid), build(u << 1 | 1, mid + 1, r);
  pushup(u);
}
void add(int p, int v, int u = 1, int l = 1, int r = maxl) {
  if (l == r) return nodeadd(u, v);
  int mid = (l + r) >> 1;
  if (p <= mid)
    add(p, v, u << 1, l, mid);
  else
    add(p, v, u << 1 | 1, mid + 1, r);
  pushup(u);
}
int qrs, qrcs, qrp; // query,sum,current,pos
void query_right(int L, int R, int u, int l, int r) {
  if (R < l || r < L) return;
  if (L <= l && r <= R) {
    if (qrcs + sr[u] > qrs) { qrs = qrcs + sr[u], qrp = srp[u]; }
    qrcs += s[u];
    return;
  }
  int mid = (l + r) >> 1;
  query_right(L, R, u << 1 | 1, mid + 1, r), query_right(L, R, u << 1, l, mid);
}
void query(int L, int R) {
  qrs = qrcs = qrp = 0;
  query_right(L, R, 1, 1, maxl);
}
int ans, al = 1, ar = 1;

signed main() {
  ios::sync_with_stdio(false);
  cin.tie(0);
  cout.tie(0);
  cin >> n;
  FOR(i, 1, n) d[i].read();
  DC::add(2e9);
  DC::work();
  al = ar = DC::ld;
  FOR(i, 1, n) d[i].refresh();
  sort(d + 1, d + n + 1);
  maxl = d[n].y;
  build();
  int last = 0;
  FOR(i, 1, n) {
    const int x = d[i].x, y = d[i].y, c = d[i].c;
    FOR(j, last + 1, y - 1) { add(j, DC::dc[j] - DC::dc[j + 1]); }
    last = max(last, y - 1);
    add(x, c);
    if (i < n && d[i].y == d[i + 1].y) continue;
    query(1, y);
    if (ans < qrs) { ans = qrs, al = qrp, ar = y; }
  }
  cout << ans << '\n'
       << DC::dc[al] << ' ' << DC::dc[al] << ' ' << DC::dc[ar] << ' ' << DC::dc[ar];
  return 0;
}
/*
 * BUG#1: 线段树边界写成n，应该是maxl
 */
