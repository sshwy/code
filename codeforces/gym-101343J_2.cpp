#include <bits/stdc++.h>
#define INF 999999999
using namespace std;
int n, l[16], tot;
int f[16][1 << 16], c[16][16];
bool vis[16][1 << 16];
vector<int> a[16];

struct data {
  int v, s;
};
queue<data> q;

int csp(const vector<int> &a, const vector<int> &b) {
  // cout<<"csp:";
  int la = a.size(), lb = b.size();
  int l = min(la, lb);
  for (int i = l; i > 0; i--) {
    bool f = true;
    for (int j = 0; j < i; j++)
      if (a[la - i + j] != b[j]) {
        f = false;
        break;
      }
    if (f) {
      // cout<<i<<endl;
      return i;
    }
  }
  // cout<<0<<endl;
  return 0;
}
bool cmp(const vector<int> &a, const vector<int> &b) {
  if (a.size() == 0) return false;
  if (b.size() == 0) return true;
  for (int i = 0; i < a.size() && i < b.size(); i++) {
    if (a[i] != b[i]) return a[i] > b[i];
  }
  return true;
}

bool vfind(const vector<int> &a, const vector<int> &b) {
  // printf("vfind:\n");
  int la = a.size(), lb = b.size();
  for (int i = 0; i <= la - lb; i++) {
    bool f = true;
    for (int j = 0; j < b.size(); j++) {
      if (a[i + j] != b[j]) {
        f = false;
        break;
      }
    }
    if (f) return true;
  }
  return false;
}

int main() {
  // freopen("gym-101303Jin.cpp","r",stdin);
  scanf("%d", &n);
  tot = n;
  for (int i = 1; i <= n; i++) {
    scanf("%d", &l[i]); // length
    for (int j = 1; j <= l[i]; j++) {
      int cc;
      scanf("%d", &cc);
      a[i].push_back(cc);
    }
    for (int j = 1; j < i; j++) {
      if (a[j].size() == 0)
        ;
      else if (vfind(a[i], a[j]))
        a[j].clear(), tot--;
      else if (vfind(a[j], a[i])) {
        a[i].clear(), tot--;
        break;
      }
    }
  }
  sort(a + 1, a + n + 1, cmp);
  // reverse(a+1,a+n+1);

  int mxt = 1 << tot;
  for (int i = 0; i <= tot; i++)
    for (int j = 0; j < mxt; j++) f[i][j] = INF;

  f[0][0] = 0, vis[0][0] = 1, a[0].clear();
  q.push((data){0, 0});
  for (int i = 1; i <= tot; i++) {
    for (int j = 1; j <= tot; j++) {
      if (i != j) c[i][j] = csp(a[i], a[j]);
    }
  }

  for (int i = 0; i < mxt; i++) {    // status i
    for (int j = 1; j <= tot; j++) { // end with a[j]
      if ((i & (1 << (j - 1))) == 0) continue;
      int nex = (~i) ^ ((~(i >> tot)) << tot), x; //后k位取反其余的清零
    }
  }

  while (!q.empty()) {
    data k = q.front();
    q.pop();
    // printf("k:%d,%d\n",k.v,k.s);
    int nex = (~k.s) ^ ((~(k.s >> tot)) << tot), x;
    while (nex != 0) {
      x = nex & -nex, nex -= x;
      int j = (int)(log(x) / log(2)) + 1;
      int t = f[k.v][k.s] + a[j].size() - c[k.v][j];
      int ns = k.s + x;
      // printf("\t\tx:%d,j:%d,t:%d\n",x,j,t);
      if (f[j][ns] > t) {
        f[j][ns] = t;
        // printf("\tf[%d][%d]:%d\n",j,ns,t);
        if (vis[j][ns] == 0) {
          q.push((data){j, ns});
          vis[j][ns] = 1;
        }
      }
    }
  }
  int mn = INF;
  // printf("f:");
  for (int i = 1; i <= tot; i++) {
    // printf("%d ",f[i][mxt-1]);
    mn = min(mn, f[i][mxt - 1]);
  }
  // printf("\n");
  printf("%d", mn);
  return 0;
}
