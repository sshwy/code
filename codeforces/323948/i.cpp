// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

const int N = 1e5 + 5;

int n;
double a[N], b[N], f[N], g[N], h[N];
vector<int> G[N];

void dfs(int u, int p) {
  g[u] = 0;
  for (int v : G[u])
    if (v != p) {
      dfs(v, u);
      g[u] += f[v];
    }
  f[u] = max(0.0, b[u] + g[u]);
}
void dfs2(int u, int p) {
  h[u] = g[u] + b[u];

  for (int v : G[u])
    if (v != p) {
      // change u to v
      double fu = f[u], gu = g[u], fv = f[v], gv = g[v];
      g[u] -= f[v];
      f[u] = max(0.0, b[u] + g[u]);
      g[v] += f[u];
      f[v] = max(0.0, b[v] + g[v]);
      dfs2(v, u);
      f[u] = fu;
      f[v] = fv;
      g[u] = gu;
      g[v] = gv;
    }
}
bool check(double x) {
  FOR(i, 1, n) b[i] = a[i] - x;
  dfs(1, 0);
  dfs2(1, 0);
  FOR(i, 1, n) if (h[i] < 0) return false;
  return true;
}
int main() {
  scanf("%d", &n);
  FOR(i, 1, n) {
    int ai;
    scanf("%d", &ai);
    a[i] = ai;
  }
  FOR(i, 2, n) {
    int x;
    scanf("%d", &x);
    G[i].pb(x);
    G[x].pb(i);
  }
  double l = 0, r = 1e9;
  const double eps = 1e-7;
  while (l + eps < r) {
    double mid = (l + r) / 2;
    if (check(mid))
      l = mid;
    else
      r = mid;
  }
  printf("%.7lf\n", l);
  return 0;
}
