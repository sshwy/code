// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

using namespace std;

void getMin(int &x, int y) { x = min(x, y); }

const int N = 505;
int n, m, tot;
int a[N][N], b[N];

long long cnt[N * N];

int root;
int s[N], tp, lc[N], rc[N];
int sz[N];

void al(int u, int v) { lc[u] = v; }
void ar(int u, int v) { rc[u] = v; }

void build() {
  tp = 0;
  FOR(i, 0, m) lc[i] = rc[i] = sz[i] = 0;
  FOR(i, 1, m) {
    while (tp > 1 && b[s[tp - 1]] >= b[i]) ar(s[tp - 1], s[tp]), --tp;
    if (tp > 0 && b[s[tp]] >= b[i]) al(i, s[tp]), --tp;
    s[++tp] = i;
  }
  while (tp > 1) ar(s[tp - 1], s[tp]), --tp;
  root = s[1];
}

void dfs(int u) {
  if (!u) return;
  dfs(lc[u]);
  dfs(rc[u]);
  sz[u] = sz[lc[u]] + sz[rc[u]] + 1;
  cnt[b[u]] += 1ll * (sz[lc[u]] + 1) * (sz[rc[u]] + 1);
}
void work() {
  build();
  dfs(root);
}

int main() {
  scanf("%d%d", &n, &m);
  FOR(i, 1, n) FOR(j, 1, m) {
    int x, y;
    scanf("%d%d", &x, &y);
    a[x][y] = tot++;
  }
  FOR(i, 1, n) {
    memset(b, 0x3f, sizeof(b));
    FOR(j, i, n) {
      FOR(k, 1, m) getMin(b[k], a[j][k]);
      work();
    }
  }
  ROF(i, n * m, 1) cnt[i] += cnt[i + 1];
  FOR(i, 1, n * m) printf("%lld\n", cnt[i]);
  return 0;
}
