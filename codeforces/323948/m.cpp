// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

const int N = 1e6 + 5, P = 1e9 + 7;
int n, a[N], cnt[N];

void work(int x) {
  for (int i = 2; i * i <= x; i++) {
    if (x % i) continue;
    int e = 0;
    while (x % i == 0) x /= i, ++e;
    if (e & 1) cnt[i]++;
  }
  if (x > 1) cnt[x]++;
}
int main() {
  scanf("%d", &n);
  FOR(i, 1, n) {
    scanf("%d", a + i);
    work(a[i]);
  }
  int W = 1e6;
  int ans = 1;
  FOR(i, 2, W) if (cnt[i]) {
    int e = min(cnt[i], n - cnt[i]);
    FOR(j, 1, e) ans = ans * 1ll * i % P;
  }
  printf("%d\n", ans);
  return 0;
}
