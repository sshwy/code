// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

const int N = 1e5 + 5, P = 998244353, ALP = 26;

int n;
char s[N];
int cnt[N][ALP];
int b1[ALP][ALP], b2[ALP][ALP], b3[ALP][ALP];

int ans;
void add(int &x, long long y) { x = (x + y) % P; }

int main() {
  scanf("%s", s + 1);
  n = strlen(s + 1);

  FOR(i, 1, n) s[i] -= 'a';

  FOR(i, 1, n) {
    FOR(j, 0, ALP - 1) { cnt[i][j] = cnt[i - 1][j] + (s[i] == j); }
  }

  ROF(i, n, 1) {
    FOR(x, 0, ALP - 1) if (x != s[i]) {
      FOR(y, 0, ALP - 1) if (y != s[i] && y != x) {
        if (b3[x][y]) {
          // a s[i] xyxy = ? s[i] xyxy - x s[i] xyxy - y s[i] xyxy - s[i] s[i] xyxy
          long long t = 1ll * (i - 1) % P;
          t = (t - 1ll * cnt[i - 1][x]) % P;
          t = (t - 1ll * cnt[i - 1][y]) % P;
          t = (t - 1ll * cnt[i - 1][s[i]]) % P;
          t = 1ll * t * b3[x][y] % P;
          t = (t + P) % P;
          add(ans, t);
        }
      }
    }
    FOR(y, 0, ALP - 1) if (y != s[i]) {
      // b1: mo
      add(b1[s[i]][y], cnt[n][y] - cnt[i][y]);
    }
    FOR(y, 0, ALP - 1) if (y != s[i]) {
      // b2: omo
      add(b2[s[i]][y], b1[y][s[i]]);
    }
    FOR(y, 0, ALP - 1) if (y != s[i]) {
      // b3: momo
      add(b3[s[i]][y], b2[y][s[i]]);
    }
  }
  printf("%d\n", ans);
  return 0;
}
