#include <cstdio>
#include <cstdlib>
#include <ctime>
using namespace std;

int main() {
  system("g++ e.cpp -o .usr");
  system("g++ gen.cpp -o .gen");
  int t = 0;
  while (++t) {
    system("./.gen > .fin");
    if (system("./.usr < .fin > .fout")) break;
    if (system("./chk")) break;
    printf("AC#%d\n", t);
  }
  printf("WA#%d\n", t);
  return 0;
}
