// by Sshwy
//#define DEBUGGER
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

#ifdef DEBUGGER

#define log_prefix()                                                \
  {                                                                 \
    fprintf(stderr, "\033[37mLine %-3d [%dms]:\033[0m  ", __LINE__, \
        (int)clock() / 1000);                                       \
  }
#define ilog(...) \
  { fprintf(stderr, __VA_ARGS__); }
#define llog(...)      \
  {                    \
    log_prefix();      \
    ilog(__VA_ARGS__); \
  }
#define log(...)           \
  {                        \
    log_prefix();          \
    ilog(__VA_ARGS__);     \
    fprintf(stderr, "\n"); \
  }
#define red(...)       \
  {                    \
    log_prefix();      \
    ilog("\033[31m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define green(...)     \
  {                    \
    log_prefix();      \
    ilog("\033[32m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define blue(...)      \
  {                    \
    log_prefix();      \
    ilog("\033[34m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }

#else

#define log_prefix() ;
#define ilog(...) ;
#define llog(...) ;
#define log(...) ;
#define red(...) ;
#define green(...) ;
#define blue(...) ;

#endif

namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 2e5 + 5, W = 1 << 19, L = 19;
int n;
int a[N];
int fa[N];

int get(int u) { return fa[u] == u ? u : fa[u] = get(fa[u]); }
void merge(int u, int v) { fa[get(u)] = get(v); }

pair<int, int> c[W];
pair<int, int> b[N];
void push(pair<int, int> &p, int x) {
  if (p.fi != -1 && get(p.fi) == get(x))
    p.fi = a[p.fi] > a[x] ? p.fi : x;
  else if (p.se != -1 && get(p.se) == get(x))
    p.se = a[p.se] > a[x] ? p.se : x;
  else if (p.fi == -1 || a[x] > a[p.fi])
    p.fi = x;
  else if (p.se == -1 || a[x] > a[p.se])
    p.se = x;
}
void push(pair<int, int> &p, const pair<int, int> &q) {
  if (q.fi != -1) push(p, q.fi);
  if (q.se != -1) push(p, q.se);
}
long long ans = 0;
int main() {
  scanf("%d", &n);
  FOR(i, 1, n) scanf("%d", &a[i]);
  ++n; // a[n]==0
  FOR(i, 1, n) fa[i] = i;
  FOR(_, 1, L - 1) {
    red("_=%d", _);
    FOR(i, 0, W - 1) c[i] = {-1, -1};
    FOR(i, 1, n) push(c[a[i]], i);
    FOR(j, 0, L - 1) FOR(i, 0, W - 1) if (i >> j & 1) { push(c[i], c[i ^ (1 << j)]); }
    FOR(i, 1, n) b[i] = {-1, -1};
    FOR(i, 1, n) {
      pair<int, int> x = c[a[i] ^ (W - 1)];
      int y = x.fi != -1 && get(x.fi) != get(i)
                  ? x.fi
                  : (x.se != -1 && get(x.se) != get(i) ? x.se : -1);
      if (y != -1) { // a[i] -> y
        int id = get(i);
        if (b[id].fi == -1 || a[b[id].fi] + a[b[id].se] < a[i] + a[y]) b[id] = {i, y};
      }
    }
    bool fl = 0;
    FOR(i, 1, n) {
      if (b[i].fi != -1) {
        if (get(b[i].fi) != get(b[i].se)) {
          merge(b[i].fi, b[i].se), ans += a[b[i].fi] + a[b[i].se];
          fl = 1;
          // log("merge(%d,%d)",b[i].fi,b[i].se);
        }
      }
    }
    if (!fl) break;
  }
  FOR(i, 1, n) ans -= a[i];
  printf("%lld\n", ans);
  return 0;
}
