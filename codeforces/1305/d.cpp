// by Sshwy
//#define DEBUGGER
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

#ifdef DEBUGGER

#define log_prefix()                                                \
  {                                                                 \
    fprintf(stderr, "\033[37mLine %-3d [%dms]:\033[0m  ", __LINE__, \
        (int)clock() / 1000);                                       \
  }
#define ilog(...) \
  { fprintf(stderr, __VA_ARGS__); }
#define llog(...)      \
  {                    \
    log_prefix();      \
    ilog(__VA_ARGS__); \
  }
#define log(...)           \
  {                        \
    log_prefix();          \
    ilog(__VA_ARGS__);     \
    fprintf(stderr, "\n"); \
  }
#define red(...)       \
  {                    \
    log_prefix();      \
    ilog("\033[31m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define green(...)     \
  {                    \
    log_prefix();      \
    ilog("\033[32m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define blue(...)      \
  {                    \
    log_prefix();      \
    ilog("\033[34m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }

#else

#define log_prefix() ;
#define ilog(...) ;
#define llog(...) ;
#define log(...) ;
#define red(...) ;
#define green(...) ;
#define blue(...) ;

#endif

namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 2005;
int n;
vector<int> g[N];
bool cut[N];

bool find(int u, int p, int x) {
  if (u == x) return 1;
  for (int v : g[u])
    if (v != p) {
      if (find(v, u, x)) return 1;
    }
  return 0;
}
void Cut(int u, int p) {
  cut[u] = 1;
  for (int v : g[u])
    if (v != p) { Cut(v, u); }
}
int d[N];
void check(int x, int y) {
  int z;
  cout << "? " << x << " " << y << endl;
  cout.flush();
  cin >> z;
  for (int v : g[z]) {
    if (cut[v]) continue;
    if (find(v, z, x))
      Cut(v, z), d[z]--;
    else if (find(v, z, y))
      Cut(v, z), d[z]--;
  }
}
int main() {
  cin >> n;
  FOR(i, 1, n - 1) {
    int x, y;
    cin >> x >> y;
    g[x].pb(y);
    g[y].pb(x);
    d[x]++, d[y]++;
  }
  int p1, p2;
  while (1) {
    p1 = -1, p2 = -1;
    FOR(i, 1, n) if (!cut[i] && d[i] <= 1) {
      if (p1 == -1)
        p1 = i;
      else
        p2 = i;
    }
    if (p1 != -1 && p2 != -1)
      check(p1, p2);
    else {
      assert(p1 != -1);
      cout << "! " << p1 << endl;
      cout.flush();
      return 0;
    }
  }
  return 0;
}
