// by Sshwy
//#define DEBUGGER
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

#ifdef DEBUGGER

#define log_prefix()                                                  \
  {                                                                   \
    fprintf(stderr, "\033[37mLine %-3d [%lldms]:\033[0m  ", __LINE__, \
        (int)clock() / 1000);                                         \
  }
#define ilog(...) \
  { fprintf(stderr, __VA_ARGS__); }
#define llog(...)      \
  {                    \
    log_prefix();      \
    ilog(__VA_ARGS__); \
  }
#define log(...)           \
  {                        \
    log_prefix();          \
    ilog(__VA_ARGS__);     \
    fprintf(stderr, "\n"); \
  }
#define red(...)       \
  {                    \
    log_prefix();      \
    ilog("\033[31m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define green(...)     \
  {                    \
    log_prefix();      \
    ilog("\033[32m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define blue(...)      \
  {                    \
    log_prefix();      \
    ilog("\033[34m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }

#else

#define log_prefix() ;
#define ilog(...) ;
#define llog(...) ;
#define log(...) ;
#define red(...) ;
#define green(...) ;
#define blue(...) ;

#endif

namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
#define int long long
const int N = 2e5 + 5, W = 1e6 + 5;
int n;
int a[N];
int ans;
bool bp[W];
int pn[W], lp;
void sieve(int lim) {
  bp[0] = bp[1] = 1;
  FOR(i, 2, lim) {
    if (!bp[i]) pn[++lp] = i;
    FOR(j, 1, lp) {
      if (i * pn[j] > lim) break;
      bp[i * pn[j]] = 1;
      if (i % pn[j] == 0) break;
    }
  }
}
void update(int x) {
  if (x == 1) return;
  int tot = 0;
  FOR(i, 1, n) {
    tot += a[i] < x ? x - a[i] : min(a[i] % x, x - a[i] % x);
    if (tot >= ans) break;
  }
  if (tot < ans) ans = tot;
}
set<int> S;
void div(int x) {
  for (int i = 1; i <= lp && pn[i] * pn[i] <= x; i++) {
    if (x % pn[i]) continue;
    while (x % pn[i] == 0) x /= pn[i];
    S.insert(pn[i]);
  }
  if (x > 1) S.insert(x);
}
signed main() {
  sieve(1e6);
  scanf("%lld", &n);
  ans = n;
  FOR(i, 1, n) scanf("%lld", &a[i]);
  random_shuffle(a + 1, a + n + 1);
  for (int i = 1; i <= n && i <= 50; i++) {
    div(a[i]);
    if (a[i] > 1) div(a[i] - 1);
    div(a[i] + 1);
  }
  for (int x : S) update(x);
  printf("%lld\n", ans);
  return 0;
}
