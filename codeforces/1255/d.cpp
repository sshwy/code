#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/

char ch[] = "qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM1234567890";
int r, c, k;
char a[200][200];
char ans[200][200];

int tot;

pair<pair<int, int>, int> t[200 * 200];
int lt;

int pos;

void make(int cnt, char c) {
  int cur = 0;
  while (cur < cnt) {
    ++pos;
    assert(pos <= lt);
    ans[t[pos].fi.fi][t[pos].fi.se] = c;
    cur += t[pos].se;
  }
}
void go() {
  lt = pos = tot = 0;
  cin >> r >> c >> k;
  FOR(i, 1, r) {
    cin >> (a[i] + 1);
    FOR(j, 1, c) tot += a[i][j] == 'R';
  }
  int cnt = tot / k, more = tot % k;
  FOR(i, 1, r) {
    if (i & 1) {
      FOR(j, 1, c) { t[++lt] = {{i, j}, a[i][j] == 'R'}; }
    } else {
      ROF(j, c, 1) { t[++lt] = {{i, j}, a[i][j] == 'R'}; }
    }
  }
  FOR(i, 1, more) make(cnt + 1, ch[i - 1]);
  FOR(i, more + 1, k) make(cnt, ch[i - 1]);
  while (pos < lt) {
    ++pos;
    ans[t[pos].fi.fi][t[pos].fi.se] = ch[k - 1];
  }
  FOR(i, 1, r) ans[i][c + 1] = 0;
  FOR(i, 1, r) cout << (ans[i] + 1) << endl;
}
int main() {
  assert(strlen(ch) == 62);
  int t;
  cin >> t;
  FOR(i, 1, t) go();
  return 0;
}
