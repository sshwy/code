#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 1005;

int n, m;
int a[N];

void go() {
  cin >> n >> m;
  FOR(i, 1, n) cin >> a[i];
  if (m < n || n == 2) return cout << -1 << endl, void();
  int ans = 0;
  FOR(i, 1, n) ans += a[i];
  ans *= 2;
  cout << ans << endl;
  FOR(i, 1, n - 1) cout << i << " " << i + 1 << endl;
  cout << n << " " << 1 << endl;
}
int main() {
  int t;
  cin >> t;
  FOR(i, 1, t) go();
  return 0;
}
