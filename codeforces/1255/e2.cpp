#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 1e6 + 5;
typedef long long LL;
const LL INF = 1e18;

int n;
LL a[N], sum, ans = INF;

void calc(LL k) {
  LL res = 0;
  FOR(i, 1, n) {
    LL t = a[i] % k;
    res += min(t, k - t);
  }
  ans = min(ans, res);
}
int main() {
  ios::sync_with_stdio(false);
  cin.tie(0);
  cin >> n;
  FOR(i, 1, n) {
    cin >> a[i];
    a[i] += a[i - 1];
  }
  sum = a[n];
  for (LL i = 2; i * i <= sum; i++) {
    if (sum % i) continue;
    calc(i);
    while (sum % i == 0) sum /= i;
  }
  if (sum > 1) calc(sum);
  cout << (ans == INF ? -1 : ans) << endl;
  return 0;
}
