#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/

int n;
vector<int> L, R;
vector<pair<long long, int>> Lp, L1, L2;
vector<pair<long long, int>> Rp, R1, R2;

int get_sig(int a, int b, int c) {
  int sig;
  cout << 2 << " " << a << " " << b << " " << c << endl;
  cout.flush();
  cin >> sig;
  return sig;
}
long long get_aria(int a, int b, int c) {
  long long aria;
  cout << 1 << " " << a << " " << b << " " << c << endl;
  cout.flush();
  cin >> aria;
  return aria;
}
int main() {
  cin >> n;
  FOR(i, 3, n) {
    if (get_sig(1, 2, i) > 0)
      L.pb(i);
    else
      R.pb(i);
  }
  // Left
  int lmid = -1, rmid = -1;
  int mid = -1, midaria = 0;
  for (int x : L) {
    long long aria = get_aria(x, 1, 2);
    Lp.pb({aria, x});
    if (aria > midaria) midaria = aria, mid = x;
  }
  lmid = mid;
  for (pair<long long, int> x : Lp) {
    if (x.se == mid)
      ;
    else if (get_sig(1, mid, x.se) > 0)
      L1.pb({-x.fi, x.se});
    else
      L2.pb(x);
  }
  sort(L1.begin(), L1.end());
  sort(L2.begin(), L2.end());
  // Right
  mid = -1, midaria = 0;
  for (int x : R) {
    long long aria = get_aria(x, 1, 2);
    Rp.pb({aria, x});
    if (aria > midaria) midaria = aria, mid = x;
  }
  rmid = mid;
  for (pair<long long, int> x : Rp) {
    if (x.se == mid)
      ;
    else if (get_sig(1, mid, x.se) > 0)
      R1.pb({-x.fi, x.se});
    else
      R2.pb(x);
  }
  sort(R1.begin(), R1.end());
  sort(R2.begin(), R2.end());
  cout << 0 << " ";
  cout << 1 << " ";
  for (pair<long long, int> x : R2) cout << x.se << " ";
  if (~rmid) cout << rmid << " ";
  for (pair<long long, int> x : R1) cout << x.se << " ";
  cout << 2 << " ";
  for (pair<long long, int> x : L2) cout << x.se << " ";
  if (~lmid) cout << lmid << " ";
  for (pair<long long, int> x : L1) cout << x.se << " ";
  cout << endl;
  cout.flush();
  return 0;
}
