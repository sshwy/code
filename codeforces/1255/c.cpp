#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 1e5 + 5;

int n, c[N];
int ans[N], la;
int q[N][3];
bool vis[N];
map<pair<int, int>, vector<int>> mp;

void make(int x, int y) { // x<y
  // printf("make(%d,%d)\n",x,y);
  if (x > y) swap(x, y);
  for (int i : mp[{x, y}]) {
    if (vis[i]) continue;
    int z = q[i][0] == x || q[i][0] == y
                ? q[i][1] == x || q[i][1] == y
                      ? q[i][2] == x || q[i][2] == y ? 0 : q[i][2]
                      : q[i][1]
                : q[i][0];
    assert(z);
    ans[++la] = z;
    vis[i] = 1;
    // printf("z=%d\n",z);
    break;
  }
  if (la < n) make(ans[la - 1], ans[la]);
}
int main() {
  cin >> n;
  FOR(i, 1, n - 2) {
    int x, y, z;
    cin >> x >> y >> z;
    q[i][0] = x;
    q[i][1] = y;
    q[i][2] = z;
    sort(q[i], q[i] + 3);
    x = q[i][0];
    y = q[i][1];
    z = q[i][2];
    c[x]++;
    c[y]++;
    c[z]++;
    mp[{x, y}].pb(i);
    mp[{x, z}].pb(i);
    mp[{y, z}].pb(i);
  }
  int start = 0;
  FOR(i, 1, n) if (c[i] == 1) {
    start = i;
    break;
  }
  assert(start);
  // printf("start=%d\n",start);
  int secon = 0;
  FOR(i, 1, n) if (c[i] == 2 && mp.count({min(start, i), max(start, i)})) {
    secon = i;
    break;
  }
  // printf("secon=%d\n",secon);
  assert(secon);
  ans[++la] = start;
  ans[++la] = secon;
  make(start, secon);
  assert(la == n);
  FOR(i, 1, n) cout << ans[i] << " \n"[i == n];
  return 0;
}
