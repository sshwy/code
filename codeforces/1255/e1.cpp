#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
#define int long long
const int N = 1e5 + 5, INF = 0x3f3f3f3f;

int n, ans = INF;
int a[N], s;
int t[N];

int getans(int m) {
  if (m == 1) return 0;
  int res = 0;
  for (int i = 1, j = m; i < j; i++, j--) res += t[j] - t[i];
  return res;
}
int calc(int k) {
  int las = 0, cnt = 0, res = 0;
  FOR(i, 1, n) {
    if (a[i]) {
      ++cnt;
      t[cnt] = i;
      if (!las) las = i;
      if (cnt == k) res += getans(k), cnt = 0, las = 0;
    }
  }
  assert(las == 0);
  return res;
}
signed main() {
  cin >> n;
  FOR(i, 1, n) {
    cin >> a[i];
    s += a[i];
  }
  if (s == 1) return cout << -1, 0;
  ans = calc(s);
  for (int i = 2; i * i <= s; i++) {
    if (s % i) continue;
    ans = min(ans, calc(i));
    ans = min(ans, calc(s / i));
  }
  cout << ans;
  return 0;
}
