#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
namespace IO {
  char nc() {
    static char bf[100000], *p1 = bf, *p2 = bf;
    return p1 == p2 && (p2 = (p1 = bf) + fread(bf, 1, 100000, stdin), p1 == p2)
               ? EOF
               : *p1++;
  }
  int rd() {
    int res = 0;
    char c = getchar();
    while (!isdigit(c)) c = getchar();
    while (isdigit(c)) res = res * 10 + c - '0', c = getchar();
    return res;
  }
} // namespace IO
/******************heading******************/
const int SZ = 5e4 * 12 * 2 + 500, ALP = 27;

int L[11], R[11];
struct SAM {
  int tot, last;
  int tr[SZ][ALP], fail[SZ];
  int len[SZ], cnt[12][SZ], end[SZ]; // cnt:状态出现次数;end：状态的结尾位置
  int s[SZ], ls;
  SAM() { tot = last = 1, len[1] = 0, fail[1] = 0; }
  void print_node(int u) { //输出每个结点的状态
    printf("u=%d,len=%d,fail=%d, ", u, len[u], fail[u]);
    FOR(i, end[u] - len[u] + 1, end[u]) putchar(s[i]);
    puts("");
  }
  void insert(char x, int id) {
    s[++ls] = x;
    x -= 'a';
    int u = ++tot, p = last;
    len[u] = len[last] + 1, last = u;
    cnt[id][u] = 1, end[u] = ls; // u 的卫星信息
    while (p && tr[p][x] == 0) tr[p][x] = u, p = fail[p];
    if (!p)
      fail[u] = 1;
    else {
      int q = tr[p][x];
      if (len[q] == len[p] + 1)
        fail[u] = q;
      else {
        int cq = ++tot;
        len[cq] = len[p] + 1, fail[cq] = fail[q];
        end[cq] = end[q]; //如果需要，更新的cq的卫星信息
        memcpy(tr[cq], tr[q], sizeof(tr[q]));
        fail[q] = fail[u] = cq;
        while (p && tr[p][x] == q) tr[p][x] = cq, p = fail[p];
      }
    }
  }
  int a[SZ], bin[SZ], tim[SZ]; // tim：每个结点的时间戳
  void cat(char *t, int l, int id) { FOR(i, 1, l) insert(t[i], id); }
  int ans;
  void check(int u, int n) {
    if (!cnt[0][u]) return;
    bool f = 1;
    for (int i = 1; f && i <= n; i++) { f &= L[i] <= cnt[i][u] && cnt[i][u] <= R[i]; }
    if (f) ans += len[u] - len[fail[u]];
  }
  void count(int n) { //桶排，统计cnt
    FOR(i, 1, tot) bin[len[i]]++;
    FOR(i, 1, tot) bin[i] += bin[i - 1];
    FOR(i, 1, tot) a[bin[len[i]]--] = i;
    FOR(j, 0, n) ROF(i, tot, 1) cnt[j][fail[a[i]]] += cnt[j][a[i]];
    FOR(i, 1, tot) { check(i, n); }
    printf("%d", ans);
  }
} sam;

char s[SZ], p[11][SZ];
int ls, l[11], n;

int main() {
  scanf("%s", s + 1);
  ls = strlen(s + 1);
  sam.cat(s, ls, 0);
  sam.insert('{', 11);
  scanf("%d", &n);
  FOR(i, 1, n) {
    scanf("%s%d%d", p[i] + 1, &L[i], &R[i]);
    l[i] = strlen(p[i] + 1);
    sam.cat(p[i], l[i], i);
    sam.insert('{', 11);
  }
  sam.count(n);
  return 0;
}
/*
 * 'z'+1={
 */
