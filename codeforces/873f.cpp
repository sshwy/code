#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 4e5 + 5;

int n;
char s[N], a[N];

int len[N], fail[N], tr[N][26];
int cnt[N];
int last = 1, tot = 1;

void insert(char c) {
  c -= 'a';
  int u = ++tot, p = last;
  len[u] = len[last] + 1, last = u;
  while (p && tr[p][c] == 0) tr[p][c] = u, p = fail[p];
  if (!p)
    fail[u] = 1;
  else {
    int q = tr[p][c];
    if (len[q] == len[p] + 1)
      fail[u] = q;
    else {
      int cq = ++tot;
      len[cq] = len[p] + 1, fail[cq] = fail[q], fail[q] = fail[u] = cq;
      memcpy(tr[cq], tr[q], sizeof(tr[q]));
      while (p && tr[p][c] == q) tr[p][c] = cq, p = fail[p];
    }
  }
}
int bin[N], b[N];
void calc() {
  FOR(i, 1, tot) bin[len[i]]++;
  FOR(i, 1, tot) bin[i] += bin[i - 1];
  ROF(i, tot, 1) b[bin[len[i]]--] = i;
  ROF(i, tot, 1) {
    int u = b[i];
    cnt[fail[u]] += cnt[u];
  }
}

int main() {
  scanf("%d", &n);
  scanf("%s", s + 1);
  scanf("%s", a + 1);
  FOR(i, 1, n) {
    insert(s[i]);
    if (a[i] == '0') cnt[last]++;
  }
  calc();
  long long ans = 0;
  FOR(i, 1, tot) ans = max(ans, len[i] * 1ll * cnt[i]);
  printf("%lld\n", ans);
  return 0;
}
