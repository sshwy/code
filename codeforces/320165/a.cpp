// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

const int N = 20;
int n, nn, ans = -1;
int pos = 0, dir = 0;
bool vis[1 << N];
int a[1 << N], cnt;

int ask(int p) {
  p = (p % nn + nn) % nn;

  if (vis[p]) return a[p];
  ++cnt;
  if (cnt > 2 * n) assert(0);
  printf("? %d\n", p + 1);
  fflush(stdout);
  int x;
  scanf("%d", &x);
  vis[p] = true;
  a[p] = x;

  if (ans == -1 || a[ans] < a[p]) ans = p;
  if (a[ans] == nn) {
    printf("! %d\n", ans + 1);
    exit(0);
  }

  return x;
}
void answer(int p) {
  p = (p % nn + nn) % nn;
  printf("! %d\n", p + 1);
  exit(0);
}

int main() {
  srand(clock() + time(0));

  scanf("%d", &n);
  nn = 1 << n;

  int l = 0, r = nn - 1;
  int rootDir = ask(l) < ask(r);
  while (l <= r) {
    if (l == r) { answer(l); }
    if (l + 1 == r) {
      ask(l);
      answer(r);
    }
    if (l + 2 == r) {
      ask(l);
      ask(l + 1);
      answer(r);
    }
    int mid = (l + r) >> 1;
    int dir = ask(mid) < ask(mid + 1);
    if (dir == rootDir) {
      if (dir)
        l = mid + 2;
      else
        r = mid - 1;
    } else {
      if (dir) {
        if (ask(0) < ask(mid))
          l = mid + 2;
        else
          r = mid - 1;
      } else {
        if (ask(0) < ask(mid))
          r = mid - 1;
        else
          l = mid + 2;
      }
    }
  }
  assert(0);
  return 0;
}
