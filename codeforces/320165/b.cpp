// by Yao
#include <bits/stdc++.h>
using namespace std;
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

const int N = 1e6 + 5;

int n, a[N];

int main() {
  scanf("%d", &n);
  FOR(i, 1, n) scanf("%d", a + i);
  sort(a + 1, a + n + 1);
  double ans = a[n] - a[1];
  FOR(i, 1, n - 1) {
    double d = (a[i] - a[1]) / 2.0 + (a[n] - a[i + 1]) / 2.0;
    d = max(d, 0.0 + a[i] - a[1]);
    d = max(d, 0.0 + a[n] - a[i + 1]);
    ans = min(ans, d);
  }
  printf("%.1lf\n", ans);
  return 0;
}
