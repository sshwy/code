// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

int main() {
  printf("12\n8 9\n9 10\n9 11\n9 12\n4 5\n2 3\n1 2\n3 4\n6 7\n5 6\n7 8");
  return 0;
}
