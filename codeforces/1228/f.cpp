#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
namespace IO {
  char nc() {
    static char bf[100000], *p1 = bf, *p2 = bf;
    return p1 == p2 && (p2 = (p1 = bf) + fread(bf, 1, 100000, stdin), p1 == p2)
               ? EOF
               : *p1++;
  }
  int rd() {
    int res = 0;
    char c = getchar();
    while (!isdigit(c)) c = getchar();
    while (isdigit(c)) res = res * 10 + c - '0', c = getchar();
    return res;
  }
} // namespace IO
/******************heading******************/
const int N = 2e5 + 5, M = 3e5 + 5;

int n, m;
int d[N], c[N];

struct qxx {
  int nex, t;
};
qxx e[M];
int h[N], le;
void add_path(int f, int t) { e[++le] = (qxx){h[f], t}, h[f] = le; }

int c1[N];
int dfs(int u, int p, int dep) {
  if (d[u] == 1) {
    c1[dep]++;
    if (dep != n && dep != n - 1) return 0;
    return 1;
  }
  for (int i = h[u]; i; i = e[i].nex) {
    const int v = e[i].t;
    if (v == p) continue;
    if (!dfs(v, u, dep + 1)) return 0;
  }
  return 1;
}
int check1() {
  int root = 0, vv = 0;
  FOR(i, 1, n) {
    if (d[i] == 2) root = i;
    if (d[i] == 4) vv = i;
  }
  if (!root || !vv) return cout << 0, 0;
  if (!dfs(root, 0, 1)) { return cout << 0, 0; }
  if (c1[n - 1] != 2) return cout << 0, 0;
  if (c1[n] + c1[n - 1] != (1 << (n - 1))) return cout << 0, 0;
  cout << 1 << endl << vv;
  return 0;
}
queue<int> q;
int he[N];
bool dfs2(int u, int p, int x, int y) {
  if (d[u] == 1) return 1;
  int ans[10], la = 0;
  for (int i = h[u]; i; i = e[i].nex) {
    const int v = e[i].t;
    if (v == p) continue;
    if (v == x) continue;
    if (v == y) continue;
    ans[++la] = h[v];
  }
  if (la != 2) return 0;
  if (ans[1] != ans[2]) return 0;
  return 1;
}
int check2() {
  FOR(i, 1, n) {
    if (d[i] == 1) q.push(i), he[i] = 1;
  }
  while (!q.empty()) {
    int u = q.front();
    q.pop();
    for (int i = h[u]; i; i = e[i].nex) {
      const int v = e[i].t;
      if (he[v]) continue;
      he[v] = he[u] + 1;
      q.push(v);
    }
  }
  int ans[10], la = 0;
  FOR(i, 1, n) {
    c1[he[i]]++;
    if (he[i] == n - 1) ans[++la] = i;
  }
  if (c1[n - 1] != 2) return cout << 0, 0;
  if (dfs2(ans[1], ans[2], ans[1], ans[2]) && dfs2(ans[2], ans[1], ans[1], ans[2])) {
    cout << 2 << endl << ans[1] << ' ' << ans[2];
    return 0;
  }
  cout << 0;
  return 0;
}
int dfs3(int u, int p, int dep) {
  if (d[u] == 1) {
    c1[dep]++;
    if (dep != n) return 0;
    return 1;
  }
  for (int i = h[u]; i; i = e[i].nex) {
    const int v = e[i].t;
    if (v == p) continue;
    if (!dfs(v, u, dep + 1)) return 0;
  }
  return 1;
}
int check3() {
  int ans[10], la = 0;
  FOR(i, 1, n) if (d[i] == 2) ans[++la] = i;
  if (la != 2) return cout << 0, 0;
  if (dfs3(ans[1], 0, 1)) {
    cout << 1 << endl << ans[2];
    return 0;
  }
  if (dfs3(ans[2], 0, 1)) {
    cout << 1 << endl << ans[1];
    return 0;
  }
  cout << 0;
  return 0;
}
int main() {
  cin >> n;
  m = (1 << n) - 3;
  FOR(i, 1, m) {
    int a, b;
    cin >> a >> b;
    d[a]++, d[b]++;
    add_path(a, b);
    add_path(b, a);
  }
  FOR(i, 1, n) {
    if (d[i] > 4) return cout << 0, 0;
    c[d[i]]++;
  }
  if (c[4] > 1) return cout << 0, 0;
  if (n == 2) {
    if (!h[1]) {
      cout << "2\n2 3";
      return 0;
    }
    if (!h[2]) {
      cout << "2\n1 3";
      return 0;
    }
    if (!h[3]) {
      cout << "2\n1 2";
      return 0;
    }
    cout << 0;
    return 0;
  }
  if (c[4] == 1 && c[2] == 1) { // case1
    check1();
    return 0;
  }
  if (c[4] == 0 && c[2] == 0) { // case2
    check2();
    return 0;
  }
  if (c[4] == 0 && c[2] == 2) { // case3
    check3();
    return 0;
  }
  cout << 0;
  return 0;
}
