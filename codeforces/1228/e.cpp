#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
namespace IO {
  char nc() {
    static char bf[100000], *p1 = bf, *p2 = bf;
    return p1 == p2 && (p2 = (p1 = bf) + fread(bf, 1, 100000, stdin), p1 == p2)
               ? EOF
               : *p1++;
  }
  int rd() {
    int res = 0;
    char c = getchar();
    while (!isdigit(c)) c = getchar();
    while (isdigit(c)) res = res * 10 + c - '0', c = getchar();
    return res;
  }
} // namespace IO
/******************heading******************/
#define int lld
const int N = 500, P = 1e9 + 7;

int n, k, ans;
int fac[N], fnv[N];

int pw(int a, int m, int p) {
  int res = 1;
  while (m) m & 1 ? res = 1ll * res * a % p : 0, a = 1ll * a * a % p, m >>= 1;
  return res;
}
int calc2(int x, int y) { return pw(k - 1, n * n - (n - x) * (n - y), P); }
int calc(int x, int y) {
  int res = pw(k, x * y, P);
  return res;
}
void init(int k) {
  fac[0] = 1;
  FOR(i, 1, k) fac[i] = 1ll * fac[i - 1] * i % P;
  fnv[k] = pw(fac[k], P - 2, P);
  ROF(i, k, 1) fnv[i - 1] = 1ll * fnv[i] * i % P;
}
int binom(int n, int m) {
  if (n < m) return 0;
  return 1ll * fac[n] * fnv[m] % P * fnv[n - m] % P;
}
signed main() {
  cin >> n >> k;
  init(n);
  FOR(i, 0, n) FOR(j, 0, n) {
    int t = 1ll * calc2(i, j) * calc(n - i, n - j) % P;
    t = 1ll * t * binom(n, i) % P * binom(n, j) % P;
    if ((i + j) & 1)
      ans -= t;
    else
      ans += t;
    ans %= P;
  }
  ans = (ans + P) % P;
  cout << ans;
  return 0;
}
