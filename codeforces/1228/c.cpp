#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
namespace IO {
  char nc() {
    static char bf[100000], *p1 = bf, *p2 = bf;
    return p1 == p2 && (p2 = (p1 = bf) + fread(bf, 1, 100000, stdin), p1 == p2)
               ? EOF
               : *p1++;
  }
  int rd() {
    int res = 0;
    char c = getchar();
    while (!isdigit(c)) c = getchar();
    while (isdigit(c)) res = res * 10 + c - '0', c = getchar();
    return res;
  }
} // namespace IO
/******************heading******************/
#define int lld
const int P = 1e9 + 7;

int x, n;
int d[100], ld;
int ans = 1;

int pw(int a, int m, int p) {
  int res = 1;
  while (m) m & 1 ? res = 1ll * res * a % p : 0, a = 1ll * a * a % p, m >>= 1;
  return res;
}

void div(int m) {
  for (int i = 2; i * i <= m; i++) {
    if (m % i) continue;
    d[++ld] = i;
    while (m % i == 0) m /= i;
  }
  if (m > 1) d[++ld] = m;
}

void calc(int p) {
  int t = n, cnt = 0;
  while (t) {
    cnt += t / p;
    t /= p;
  }
  ans = 1ll * ans * pw(p, cnt, P) % P;
}
signed main() {
  cin >> x >> n;
  div(x);
  FOR(i, 1, ld) calc(d[i]);
  cout << ans << endl;
  return 0;
}
