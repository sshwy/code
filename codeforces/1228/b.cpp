#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
namespace IO {
  char nc() {
    static char bf[100000], *p1 = bf, *p2 = bf;
    return p1 == p2 && (p2 = (p1 = bf) + fread(bf, 1, 100000, stdin), p1 == p2)
               ? EOF
               : *p1++;
  }
  int rd() {
    int res = 0;
    char c = getchar();
    while (!isdigit(c)) c = getchar();
    while (isdigit(c)) res = res * 10 + c - '0', c = getchar();
    return res;
  }
} // namespace IO
/******************heading******************/
const int N = 2e3 + 5, P = 1e9 + 7;

int h, w, ans = 1;
int r[N], c[N];
int a[N][N];

int main() {
  cin >> h >> w;
  FOR(i, 1, h) cin >> r[i];
  FOR(i, 1, w) cin >> c[i];
  FOR(i, 1, h) {
    FOR(j, 1, r[i]) a[i][j] = 1;
    a[i][r[i] + 1] = 2;
  }
  FOR(j, 1, w) {
    FOR(i, 1, c[j]) {
      if (a[i][j] == 2) {
        cout << 0;
        return 0;
      }
      a[i][j] = 1;
    }
    if (a[c[j] + 1][j] == 1) {
      cout << 0;
      return 0;
    }
    a[c[j] + 1][j] = 2;
  }
  FOR(i, 1, h) {
    FOR(j, 1, w) {
      if (a[i][j]) continue;
      ans = ans * 2 % P;
    }
  }
  cout << ans << endl;
  return 0;
}
