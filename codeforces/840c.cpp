// by Sshwy
//#define DEBUGGER
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

#ifdef DEBUGGER

#define log_prefix()                                                \
  {                                                                 \
    fprintf(stderr, "\033[37mLine %-3d [%dms]:\033[0m  ", __LINE__, \
        (int)clock() / 1000);                                       \
  }
#define ilog(...) \
  { fprintf(stderr, __VA_ARGS__); }
#define llog(...)      \
  {                    \
    log_prefix();      \
    ilog(__VA_ARGS__); \
  }
#define log(...)           \
  {                        \
    log_prefix();          \
    ilog(__VA_ARGS__);     \
    fprintf(stderr, "\n"); \
  }
#define red(...)       \
  {                    \
    log_prefix();      \
    ilog("\033[31m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define green(...)     \
  {                    \
    log_prefix();      \
    ilog("\033[32m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define blue(...)      \
  {                    \
    log_prefix();      \
    ilog("\033[34m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }

#else

#define log_prefix() ;
#define ilog(...) ;
#define llog(...) ;
#define log(...) ;
#define red(...) ;
#define green(...) ;
#define blue(...) ;

#endif

namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 500, P = 1e9 + 7;
int n, a[N], c[N][N], fac[N], f[N], g[N], tot;
bool vis[N];
vector<int> v;
// f[i]:有i个相邻的方案数

bool square(long long x) {
  long long y = sqrt(x);
  while (y * y < x) ++y;
  while (y * y > x) --y;
  if (y * y == x) return 1;
  return 0;
}

int main() {
  cin >> n;
  FOR(i, 1, n) cin >> a[i];

  c[0][0] = 1;
  FOR(i, 1, n) {
    c[i][0] = 1;
    FOR(j, 1, i) c[i][j] = (c[i - 1][j - 1] + c[i - 1][j]) % P;
  }
  fac[0] = 1;
  FOR(i, 1, n) fac[i] = fac[i - 1] * 1ll * i % P;

  FOR(i, 1, n) if (!vis[i]) {
    int cnt = 1;
    FOR(j, i + 1, n)
    if (!vis[j] && square(a[i] * 1ll * a[j])) vis[j] = 1, ++cnt;
    v.pb(cnt);
  }
  f[0] = 1;
  auto add = [](int &x, long long y) { x = (x + y) % P; };
  // FOR(i,0,tot)cout<<f[i]<<" \n"[i==tot];
  for (int x : v) {
    // printf("x %d\n",x);
    FOR(i, 0, x - 1) {
      long long coef = c[x - 1][i] * 1ll * fac[x] % P;
      FOR(j, 0, tot) { // i+j个相邻的
        add(g[i + j],
            f[j] * coef % P * c[x - i + (tot + 1 - j - 1)][tot + 1 - j - 1]);
      }
    }
    tot += x;
    FOR(i, 0, tot) f[i] = g[i], g[i] = 0;
    // FOR(i,0,tot)cout<<f[i]<<" \n"[i==tot];
  }
  int ans = 0;
  FOR(i, 0, tot) {
    int coef = i % 2 ? -1 : 1;
    add(ans, coef * f[i]);
  }
  add(ans, P);
  cout << ans << endl;
  return 0;
}
