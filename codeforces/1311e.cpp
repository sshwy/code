// by Sshwy
//#define DEBUGGER
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

#ifdef DEBUGGER

#define log_prefix()                                                \
  {                                                                 \
    fprintf(stderr, "\033[37mLine %-3d [%dms]:\033[0m  ", __LINE__, \
        (int)clock() / 1000);                                       \
  }
#define ilog(...) \
  { fprintf(stderr, __VA_ARGS__); }
#define llog(...)      \
  {                    \
    log_prefix();      \
    ilog(__VA_ARGS__); \
  }
#define log(...)           \
  {                        \
    log_prefix();          \
    ilog(__VA_ARGS__);     \
    fprintf(stderr, "\n"); \
  }
#define red(...)       \
  {                    \
    log_prefix();      \
    ilog("\033[31m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define green(...)     \
  {                    \
    log_prefix();      \
    ilog("\033[32m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define blue(...)      \
  {                    \
    log_prefix();      \
    ilog("\033[34m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }

#else

#define log_prefix() ;
#define ilog(...) ;
#define llog(...) ;
#define log(...) ;
#define red(...) ;
#define green(...) ;
#define blue(...) ;

#endif

namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 5005;
int n, d;
vector<int> v[N];
int dep[N], p[N];
void go() {
  cin >> n >> d;
  FOR(i, 0, N - 1) v[i].clear();
  FOR(i, 1, n) dep[i] = 0;
  FOR(i, 1, n) p[i] = 0;
  int cur = 0;
  FOR(i, 1, n) {
    if (i > 1)
      dep[i] = dep[i / 2] + 1;
    else
      dep[i] = 0;
    d -= dep[i];
    cur = dep[i];
  }
  if (d < 0) return cout << "NO" << endl, void();
  FOR(i, 1, n) v[dep[i]].pb(i);
  ROF(i, n, 0) if (v[i].size() > 1) {
    if (d == 0) break;
    while (d > 0 && v[i].size() > 1) {
      if (d == 0) break;
      int x = v[i][1];
      v[i].erase(v[i].begin() + 1);
      if (cur + 1 - i <= d) {
        d -= cur + 1 - i;
        ++cur;
        v[cur].pb(x);
      } else {
        v[i + d].pb(x);
        d = 0;
      }
    }
  }
  if (d > 0) return cout << "NO" << endl, void();
  FOR(i, 1, cur) {
    auto pu = v[i].begin();
    auto pf = v[i - 1].begin();
    int cnt = 0;
    while (pu != v[i].end()) {
      assert(pf != v[i - 1].end());
      p[*pu] = *pf;
      ++cnt;
      ++pu;
      if (cnt == 2) ++pf, cnt = 0;
    }
  }
  cout << "YES" << endl;
  FOR(i, 2, n) cout << p[i] << " \n"[i == n];
}
int main() {
  int t;
  cin >> t;
  FOR(i, 1, t) go();
  return 0;
}
