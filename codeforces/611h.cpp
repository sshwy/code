// by Sshwy
//#define DEBUGGER
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

#ifdef DEBUGGER

#define log_prefix()                                                \
  {                                                                 \
    fprintf(stderr, "\033[37mLine %-3d [%dms]:\033[0m  ", __LINE__, \
        (int)clock() / 1000);                                       \
  }
#define ilog(...) \
  { fprintf(stderr, __VA_ARGS__); }
#define llog(...)      \
  {                    \
    log_prefix();      \
    ilog(__VA_ARGS__); \
  }
#define log(...)           \
  {                        \
    log_prefix();          \
    ilog(__VA_ARGS__);     \
    fprintf(stderr, "\n"); \
  }
#define red(...)       \
  {                    \
    log_prefix();      \
    ilog("\033[31m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define green(...)     \
  {                    \
    log_prefix();      \
    ilog("\033[32m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define blue(...)      \
  {                    \
    log_prefix();      \
    ilog("\033[34m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }

#else

#define log_prefix() ;
#define ilog(...) ;
#define llog(...) ;
#define log(...) ;
#define red(...) ;
#define green(...) ;
#define blue(...) ;

#endif

namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 5e4 + 5, M = N * 2, INF = 0x3f3f3f3f;

struct qxx {
  int nex, t, v;
};
qxx e[M];
int h[N], le = 1;
int hh[N];
void add_path(int f, int t, int v) { e[++le] = (qxx){h[f], t, v}, h[f] = le; }
void add_flow(int f, int t, int v) { add_path(f, t, v), add_path(t, f, 0); }
void add_bidir_flow(int f, int t, int v) { add_path(f, t, v), add_path(t, f, v); }
#define FORe(i, u, v, w) \
  for (int i = h[u], v, w; v = e[i].t, w = e[i].v, i; i = e[i].nex)
#define FORflowe(i, u, v, w) \
  for (int &i = hh[u], v, w; v = e[i].t, w = e[i].v, i; i = e[i].nex)

int s, t;
int dep[N];
queue<int> q;

bool bfs() {
  memset(dep, 0, sizeof(dep));
  dep[s] = 1, q.push(s);
  while (!q.empty()) {
    int u = q.front();
    q.pop();
    FORe(i, u, v, w) if (!dep[v] && w) dep[v] = dep[u] + 1, q.push(v);
  }
  return dep[t] != 0;
}
int dfs(int u, int flow) {
  if (u == t || !flow) return flow;
  int rest = flow;
  FORflowe(i, u, v, w) {
    if (!w || dep[v] != dep[u] + 1) continue;
    int k = dfs(v, min(rest, w));
    e[i].v -= k, e[i ^ 1].v += k, rest -= k;
    if (!rest) break;
  }
  return flow - rest;
}
int dinic() {
  int maxflow = 0;
  while (bfs()) {
    memcpy(hh, h, sizeof(h));
    for (int x; (x = dfs(s, INF));) maxflow += x;
  }
  return maxflow;
}

int n;

int main() {
  cin >> n;
  FOR(i, 1, n - 1) {
    string s, t;
    cin >> s >> t;
  }
  return 0;
}
