// by Sshwy
//#define DEBUGGER
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

#ifdef DEBUGGER

#define log_prefix()                                                \
  {                                                                 \
    fprintf(stderr, "\033[37mLine %-3d [%dms]:\033[0m  ", __LINE__, \
        (int)clock() / 1000);                                       \
  }
#define ilog(...) \
  { fprintf(stderr, __VA_ARGS__); }
#define llog(...)      \
  {                    \
    log_prefix();      \
    ilog(__VA_ARGS__); \
  }
#define log(...)           \
  {                        \
    log_prefix();          \
    ilog(__VA_ARGS__);     \
    fprintf(stderr, "\n"); \
  }
#define red(...)       \
  {                    \
    log_prefix();      \
    ilog("\033[31m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define green(...)     \
  {                    \
    log_prefix();      \
    ilog("\033[32m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define blue(...)      \
  {                    \
    log_prefix();      \
    ilog("\033[34m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }

#else

#define log_prefix() ;
#define ilog(...) ;
#define llog(...) ;
#define log(...) ;
#define red(...) ;
#define green(...) ;
#define blue(...) ;

#endif

namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 1e5 + 5, M = 3e5 + 5, INF = 1e9, SZ = N << 2;
int n, m, T;
int a[N];

typedef pair<int, int> pii;
vector<pii> qr[N];

int ans[M];
int D[N], ld;

#define Md ((l + r) >> 1)
#define lu (u << 1)
#define ru (u << 1 | 1)
namespace seg1 {
  int mx[SZ];
  void assign(int p, int v, int u = 1, int l = 1, int r = n) {
    (l == r ? (mx[u] = max(mx[u], v))
            : (p <= Md ? assign(p, v, lu, l, Md) : assign(p, v, ru, Md + 1, r),
                  mx[u] = max(mx[lu], mx[ru])));
  }
  int getMax(int L, int R, int u = 1, int l = 1, int r = n) {
    return (R < l || r < L)
               ? 0
               : (L <= l && r <= R
                         ? mx[u]
                         : max(getMax(L, R, lu, l, Md), getMax(L, R, ru, Md + 1, r)));
  }
} // namespace seg1
namespace seg2 {
  int mn[SZ];
  void setMin(int L, int R, int v, int u = 1, int l = 1, int r = n) {
    (R < l || r < L)
        ? 0
        : (L <= l && r <= R
                  ? (mn[u] = min(mn[u], v))
                  : (setMin(L, R, v, lu, l, Md), setMin(L, R, v, ru, Md + 1, r), 0));
  }
  int at(int p, int u = 1, int l = 1, int r = n) {
    return l == r ? mn[u]
                  : min(mn[u], p <= Md ? at(p, lu, l, Md) : at(p, ru, Md + 1, r));
  }
} // namespace seg2
void work() {
  fill(seg2::mn, seg2::mn + SZ, INF);
  fill(seg1::mx, seg1::mx + SZ, 0);
  FOR(i, 1, n) {
    int vi = lower_bound(D + 1, D + ld + 1, a[i]) - D;
    int j = seg1::getMax(vi, n);
    if (j) {
      seg2::setMin(1, j, a[j] - a[i]);
      while (j) {
        int v2 = lower_bound(D + 1, D + ld + 1, (a[i] + a[j] + 1) / 2) - D;
        --v2;
        j = vi <= v2 ? seg1::getMax(vi, v2) : 0;
        if (j) seg2::setMin(1, j, a[j] - a[i]);
      }
    }
    for (pii p : qr[i]) {
      int l = p.fi, id = p.se;
      ans[id] = min(ans[id], seg2::at(l));
    }
    seg1::assign(vi, i);
  }
}
int main() {
  scanf("%d", &n);
  T = max(1, (int)sqrt(n));
  FOR(i, 1, n) scanf("%d", a + i), D[++ld] = a[i];

  sort(D + 1, D + ld + 1);
  ld = unique(D + 1, D + ld + 1) - D - 1;

  scanf("%d", &m);
  FOR(i, 1, m) {
    int l, r;
    scanf("%d%d", &l, &r);
    qr[r].pb({l, i});
  }
  fill(ans, ans + M, INF);
  work();

  //取相反数
  FOR(i, 1, n) a[i] = INF - a[i];
  FOR(i, 1, ld) D[i] = INF - D[i];
  reverse(D + 1, D + ld + 1);

  work();

  FOR(i, 1, m) printf("%d\n", ans[i]);
  return 0;
}
