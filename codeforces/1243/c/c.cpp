#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/

long long n;

long long p[100], c[100], lp;
int main() {
  cin >> n;
  if (n == 1) return cout << 1, 0;
  for (long long i = 2; i * i <= n; i++) {
    if (n % i) continue;
    p[++lp] = i;
    while (n % i == 0) c[lp]++, n /= i;
  }
  if (n > 1) p[++lp] = n, c[lp] = 1;
  if (lp > 1)
    cout << 1 << endl;
  else if (c[1] == 1)
    cout << n << endl;
  else
    cout << p[1] << endl;
  return 0;
}
