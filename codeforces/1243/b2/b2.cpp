#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 1000;

int n;
char s[N], t[N];
int ans[N][2], la;
int c[500];
bool vis[N];

void go() {
  la = 0;
  cin >> n >> (s + 1) >> (t + 1);
  FOR(i, 1, n) vis[i] = 0;
  FOR(i, 'a', 'z') c[i] = 0;
  FOR(i, 1, n) c[s[i]]++, c[t[i]]++;
  FOR(i, 'a', 'z') if (c[i] & 1) return cout << "No" << endl, void();

  FOR(i, 1, n) if (s[i] == t[i]) vis[i] = 1;

  int fl = 0;
  do {
    fl = 0;
    FOR(i, 1, n) if (!vis[i]) {
      FOR(j, i + 1, n) if (!vis[i] && !vis[j]) {
        if (i != j && s[i] == s[j]) {
          ++la, ans[la][0] = i, ans[la][1] = j;
          swap(s[i], t[j]);
          assert(s[j] == t[j]);
          vis[j] = 1;
          fl = 1;
          if (s[i] == t[i]) {
            vis[i] = 1;
            break;
          }
        }
      }
    }
  } while (fl);
  FOR(i, 1, n) if (s[i] == t[i]) vis[i] = 1;
  do {
    fl = 0;
    FOR(i, 1, n) if (!vis[i]) FOR(j, i + 1, n) if (!vis[i] && !vis[j]) {
      if (i != j && t[i] == t[j]) {
        ++la, ans[la][0] = j, ans[la][1] = i;
        swap(s[j], t[i]);
        assert(s[j] == t[j]);
        vis[j] = 1;
        fl = 1;
        if (s[i] == t[i]) {
          vis[i] = 1;
          break;
        }
      }
    }
  } while (fl);
  FOR(i, 1, n) if (s[i] == t[i]) vis[i] = 1;
  do {
    fl = 0;
    FOR(i, 1, n) if (!vis[i]) {
      FOR(j, i + 1, n) if (i != j && s[j] == t[i]) {
        fl = 1;
        // s[i] -> t[i], s[j] -> t[j]
        ++la, ans[la][0] = i, ans[la][1] = i;
        swap(s[i], t[i]);
        ++la, ans[la][0] = j, ans[la][1] = i;
        swap(s[j], t[i]);
        assert(s[i] == t[i]);
        vis[i] = 1;
        break;
      }
      if (vis[i]) break;
    }
  } while (fl);
  FOR(i, 1, n) if (!vis[i]) exit(1);
  cout << "Yes" << endl;
  cout << la << endl;
  FOR(i, 1, la) cout << ans[i][0] << " " << ans[i][1] << endl;
}

int main() {
  ios::sync_with_stdio(false);
  int k;
  cin >> k;
  FOR(i, 1, k) go();
  return 0;
}
