#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 2000;

int k, n;
int a[N];

void go() {
  cin >> n;
  FOR(i, 1, n) cin >> a[i];
  int ans = 1;
  FOR(i, 1, n) {
    int cnt = 0;
    FOR(j, 1, n) cnt += a[j] >= i;
    if (cnt >= i) ans = i;
  }
  cout << ans << endl;
}
int main() {
  cin >> k;
  FOR(i, 1, k) go();
  return 0;
}
