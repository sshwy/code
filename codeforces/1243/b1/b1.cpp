#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 1e5 + 5;

int n;
char s[N], t[N];
int ans[N][2], la;
int c[500];
bool vis[N];

void go() {
  la = 0;
  cin >> n >> (s + 1) >> (t + 1);
  FOR(i, 1, n) vis[i] = 0;
  FOR(i, 'a', 'z') c[i] = 0;
  FOR(i, 1, n) c[s[i]]++, c[t[i]]++;
  FOR(i, 'a', 'z') if (c[i] & 1) return puts("No"), void();
  FOR(i, 1, n) if (s[i] == t[i]) vis[i] = 1;
  FOR(i, 1, n) if (!vis[i]) {
    int fl = 0;
    FOR(j, i + 1, n) if (!vis[j]) {
      if (s[i] == s[j] && t[i] == t[j]) vis[i] = vis[j] = 1;
      fl = 1;
      break;
    }
    if (fl) break;
  }
  FOR(i, 1, n) if (!vis[i]) return puts("No"), void();

  puts("Yes");
}

int main() {
  int k;
  cin >> k;
  FOR(i, 1, k) go();
  return 0;
}
