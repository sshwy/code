#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 100;

int n;
int a[N];
int f[N][N], g[N][N];

int main() {
  cin >> n;
  FOR(i, 1, n) cin >> a[i];
  FOR(i, 1, n) f[i][i] = g[i][i] = a[i];
  FOR(len, 1, n - 1) {
    FOR(i, 1, n - len) {
      int j = i + len;
      f[i][j] = max(g[i][j - 1], g[i + 1][j]);
      g[i][j] = min(f[i][j - 1], f[i + 1][j]);
    }
  }
  ROF(i, n, 1) FOR(j, 1, i) printf("%-2d%c", f[j][i], " \n"[j == i]);
  ROF(i, n, 1) FOR(j, 1, i) printf("%-2d%c", g[j][i], " \n"[j == i]);
  return 0;
}
