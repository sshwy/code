#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
namespace IO {
  char nc() {
    static char bf[100000], *p1 = bf, *p2 = bf;
    return p1 == p2 && (p2 = (p1 = bf) + fread(bf, 1, 100000, stdin), p1 == p2)
               ? EOF
               : *p1++;
  }
  int rd() {
    int res = 0;
    char c = getchar();
    while (!isdigit(c)) c = getchar();
    while (isdigit(c)) res = res * 10 + c - '0', c = getchar();
    return res;
  }
} // namespace IO
/******************heading******************/
const int SZ = 2 * 2.5e5 + 500, ALP = 26;
int n, k;

struct SAM {
  int tot, last;
  int tr[SZ][ALP], fail[SZ];
  int len[SZ], end[SZ]; // cnt:状态出现次数;end：状态的结尾位置
  // set<int> s[SZ];
  // int str[SZ],ls;
  int newnode(int l) {
    ++tot, len[tot] = l, fail[tot] = 0, end[tot] = 0;
    memset(tr[tot], 0, sizeof(tr[tot]));
    return tot;
  }
  void clear() { tot = 0, last = newnode(0); }
  SAM() { clear(); }
  // void print_node(int u){//输出每个结点的状态{{{
  //    printf("\033[34m");
  //    printf("u=%2d,len=%2d,fail=%2d, ",u,len[u],fail[u]);
  //    FOR(i,end[u]-len[u]+1,end[u])putchar(str[i]);
  //    puts("");
  //    for(map<int,int>::iterator i=cnt[u].begin();i!=cnt[u].end();++i){
  //        int u=i->first,v=i->second;
  //        printf("it exists in \033[0m%d\033[34m "
  //                "for \033[0m%d\033[34m times\n",u,v);
  //    }
  //    puts("\033[0m");
  //}//}}}
  void insert(char x) {
    // str[++ls]=x;
    x -= 'a';
    int p = last, u = 0, cq = 0, q = 0;
    if (!tr[p][x]) {
      u = newnode(len[last] + 1);
      // end[u]=ls;//u 的卫星信息
      while (p && tr[p][x] == 0) tr[p][x] = u, p = fail[p];
    }
    if (!p)
      fail[u] = 1;
    else {
      q = tr[p][x];
      if (len[q] == len[p] + 1)
        fail[u] = q;
      else {
        cq = newnode(len[p] + 1);
        fail[cq] = fail[q];
        // end[cq]=end[q];//如果需要，更新的cq的卫星信息
        memcpy(tr[cq], tr[q], sizeof(tr[q]));
        fail[q] = fail[u] = cq;
        while (p && tr[p][x] == q) tr[p][x] = cq, p = fail[p];
      }
    }
    last = u ? u : cq ? cq : q;
  }
  int a[SZ], bin[SZ]; // tim：每个结点的时间戳
  void insert(char *s) {
    last = 1;
    for (int i = 1; s[i]; i++) insert(s[i]);
  }
  int aend, al;
  void go(char *s) {
    // FOR(i,1,tot)bin[len[i]]++;
    // FOR(i,1,tot)bin[i]+=bin[i-1];
    // FOR(i,1,tot)a[bin[len[i]]--]=i;
    int u = 1, l = 0;
    al = aend = 0x3f3f3f3f;
    for (int i = 1; s[i]; i++) {
      int c = s[i] - 'a';
      if (!tr[u][c]) {
        if (l + 1 < al) { aend = i, al = l + 1; }
      }
      while (u && !tr[u][c]) u = fail[u];
      u ? u = 1 : 0, u = tr[u][c];
      l = min(len[u], l + 1);
    }
  }
} sam;

char s[SZ], t[SZ];

void go(int id) {
  // clear
  scanf("%d", &n);
  scanf("%s", s + 1);
  FOR(i, 1, n - 1) {
    scanf("%s", t + 1);
    sam.insert(t);
  }
  sam.go(s);
}
signed main() {
  int t;
  scanf("%d", &t);
  FOR(i, 1, t) go(i);
  return 0;
}
