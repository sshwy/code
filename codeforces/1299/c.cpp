// by Sshwy
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 1e6 + 5;

int n;
int b[N];
double a[N];
int w[N], nex[N];

bool trans() {
  bool res = 0;
  for (int i = 1; nex[i] <= n; i = nex[i]) {
    if (a[i] > a[nex[i]]) {
      res = 1;
      int pos = i;
      double sum = a[i] * w[i], avg = a[i], sw = w[i];
      while (nex[pos] <= n && a[nex[pos]] < avg) {
        pos = nex[pos];
        sum += a[pos] * w[pos];
        sw += w[pos];
        avg = sum / sw;
      }
      a[i] = avg;
      w[i] = sw;
      nex[i] = nex[pos];
    }
  }
  return res;
}
void print(int u) {
  FOR(i, 1, w[u]) printf("%.10lf\n", a[u]);
  if (nex[u] <= n) print(nex[u]);
}
int main() {
  scanf("%d", &n);
  FOR(i, 1, n) scanf("%d", &b[i]), a[i] = b[i];
  FOR(i, 1, n) w[i] = 1;
  FOR(i, 1, n + 1) nex[i] = i + 1;
  while (trans())
    ;
  print(1);
  return 0;
}
