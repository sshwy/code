// by Sshwy
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 3e5 + 5;

int n;
int X[N], Y[N];

long long sqr(int x) { return x * x; }
int main() {
  cin >> n;
  FOR(i, 0, n - 1) { cin >> X[i] >> Y[i]; }
  if (n & 1) return puts("NO"), 0;
  FOR(i, 0, n / 2 - 1) {
    int j = i + n / 2;
    if ((X[(i + 1) % n] - X[i]) * 1ll * (Y[(j + 1) % n] - Y[j]) !=
        (X[(j + 1) % n] - X[j]) * 1ll * (Y[(i + 1) % n] - Y[i]))
      return puts("NO"), 0;
    if (sqr(X[(i + 1) % n] - X[i]) + sqr(Y[(i + 1) % n] - Y[i]) !=
        sqr(X[(j + 1) % n] - X[j]) + sqr(Y[(j + 1) % n] - Y[j]))
      return puts("NO"), 0;
  }
  puts("YES");
  return 0;
}
