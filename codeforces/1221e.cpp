#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
namespace IO {
  char nc() {
    static char bf[100000], *p1 = bf, *p2 = bf;
    return p1 == p2 && (p2 = (p1 = bf) + fread(bf, 1, 100000, stdin), p1 == p2)
               ? EOF
               : *p1++;
  }
  int rd() {
    int res = 0;
    char c = getchar();
    while (!isdigit(c)) c = getchar();
    while (isdigit(c)) res = res * 10 + c - '0', c = getchar();
    return res;
  }
} // namespace IO
/******************heading******************/
const int N = 3e5 + 5;

int q, a, b;
char s[N];
int c[N], lc;

void go() {
  scanf("%d%d", &a, &b);
  scanf("%s", s + 1);
  lc = 1, c[lc] = 0;
  int x2 = 0, x3 = 0, x4 = 0, p4;
  for (int i = 1; s[i]; i++) {
    if (s[i] == '.')
      ++c[lc];
    else if (c[lc])
      ++lc, c[lc] = 0;
  }
  // printf("a=%d,b=%d\n",a,b);
  // FOR(i,1,lc)printf("%d%c",c[i]," \n"[i==lc]);
  FOR(i, 1, lc) {
    if (b <= c[i] && c[i] < a)
      x2++;
    else if (c[i] >= b * 2)
      x4++, p4 = c[i];
    else if (a <= c[i] && c[i] < b * 2)
      x3++;
  }
  if (x2 || x4 > 1) return puts("NO"), void();
  if (!x4) return puts(x3 & 1 ? "YES" : "NO"), void();
  FOR(i, 0, p4 - a) {
    bool f = 1;
    int j = p4 - a - i;
    if (b <= i && i < a || b <= j && j < a) f = 0;
    if (j >= b * 2 || i >= b * 2) f = 0;
    if (!f) continue;
    if ((x3 + (a <= i && i < b * 2) + (a <= j && j < b * 2) + 1) & 1) {
      return puts("YES"), void();
    }
  }
  return puts("NO"), void();
}
int main() {
  scanf("%d", &q);
  FOR(i, 1, q) go();
  return 0;
}
