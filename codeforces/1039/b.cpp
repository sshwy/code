#include <bits/stdc++.h>
#define FOR(a, b, c) for (int a = b; a <= int(c); ++a)
#define ROF(a, b, c) for (int a = b; a >= int(c); --a)
#define pb push_back
using namespace std;
const long long T = 50;
long long n, k;

bool qry(long long L, long long R) {
  assert(1 <= L && L <= R && R <= n);
  cout << L << " " << R << endl << flush;
  string res;
  cin >> res;
  return res == "Yes";
}
void dfs(long long L, long long R) { // currently in [L,R]
  if (R - L < T) {
    long long pos = rand() % (R - L + 1) + L;
    if (qry(pos, pos)) exit(0);
    dfs(max(L - k, 1ll), min(R + k, n));
  } else {
    long long mid = (L + R) >> 1;
    if (qry(L, mid))
      dfs(max(L - k, 1ll), min(mid + k, n));
    else
      dfs(max(mid + 1 - k, 1ll), min(R + k, n));
  }
}
int main() {
  srand(clock() + time(0));
  cin >> n >> k;
  dfs(1, n);
  return 0;
}