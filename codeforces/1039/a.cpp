#include <bits/stdc++.h>
#define FOR(a, b, c) for (int a = b; a <= int(c); ++a)
#define ROF(a, b, c) for (int a = b; a >= int(c); --a)
#define pb push_back
using namespace std;
const int N = 2e5 + 5;
long long n, a[N], x[N], t;
long long L[N], R[N], c[N];
int main() {
  cin >> n >> t;
  FOR(i, 1, n) cin >> a[i];
  FOR(i, 1, n) cin >> x[i];
  FOR(i, 1, n) if (x[i] < i) return cout << "No" << endl, 0;
  FOR(i, 2, n) if (x[i] < x[i - 1]) return cout << "No" << endl, 0;
  FOR(i, 1, n) R[i] = 0x3f3f3f3f3f3f3f3f;
  FOR(i, 1, n) L[i] = a[i] + t;
  a[n + 1] = 0x3f3f3f3f3f3f3f3f;
  FOR(i, 1, n) {
    c[i + 1]++;
    c[x[i] + 1]--;
    R[x[i]] = min(R[x[i]], a[x[i] + 1] + t - 1);
  }
  FOR(i, 1, n) c[i] += c[i - 1];
  FOR(i, 1, n) if (c[i]) { L[i - 1] = max(L[i - 1], a[i] + t); }
  FOR(i, 2, n) { L[i] = max(L[i], L[i - 1] + 1); }
  FOR(i, 1, n) if (L[i] > R[i]) return cout << "No" << endl, 0;
  cout << "Yes" << endl;
  FOR(i, 1, n) cout << L[i] << " \n"[i == n];
  return 0;
}