#include <bits/stdc++.h>
#define FOR(a, b, c) for (int a = b; a <= int(c); ++a)
#define ROF(a, b, c) for (int a = b; a >= int(c); --a)
#define pb push_back
using namespace std;
const int N = 5e5 + 5, P = 1e9 + 7;
int n, m, k;
long long c[N], pw[N], ans;
map<long long, vector<pair<int, int>>> mp;
void add(long long &x, long long y) { x = (x + y) % P; }
int f[N], g[N], tim;
void cls(int u) {
  if (g[u] < tim) { g[u] = tim, f[u] = u; }
}
int get(int u) {
  cls(u);
  return f[u] == u ? u : f[u] = get(f[u]);
}
int main() {
  ios::sync_with_stdio(false);
  cin.tie(0);
  cout.tie(0);
  cin >> n >> m >> k;
  pw[0] = 1;
  FOR(i, 1, n) pw[i] = pw[i - 1] * 2 % P;
  FOR(i, 1, n) cin >> c[i];
  FOR(i, 1, m) {
    int u, v;
    cin >> u >> v;
    mp[c[u] ^ c[v]].pb({u, v});
  }
  ans = pw[n];
  FOR(i, 1, k) ans = ans * 2 % P;
  ++tim;
  for (auto pr : mp) {
    if (pr.first == 0) { // all of them are invalid
      assert(0);
    } else {
      // pr里的放一起
      int cnt = n;
      for (auto e : pr.second) {
        int u = e.first, v = e.second;
        if (get(u) != get(v)) --cnt;
        f[get(u)] = get(v);
      }
      long long x1 = pw[cnt];
      ++tim;
      x1 = pw[n] - x1;
      // cout<<"val "<<pr.first<<" x1 "<<x1<<endl;
      add(ans, -x1);
    }
  }
  add(ans, P);
  cout << ans << endl;
  return 0;
}