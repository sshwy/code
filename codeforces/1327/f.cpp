// by Sshwy
//#define DEBUGGER
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

#ifdef DEBUGGER

#define log_prefix()                                                  \
  {                                                                   \
    fprintf(stderr, "\033[37mLine %-3d [%lldms]:\033[0m  ", __LINE__, \
        (int)clock() / 1000);                                         \
  }
#define ilog(...) \
  { fprintf(stderr, __VA_ARGS__); }
#define llog(...)      \
  {                    \
    log_prefix();      \
    ilog(__VA_ARGS__); \
  }
#define log(...)           \
  {                        \
    log_prefix();          \
    ilog(__VA_ARGS__);     \
    fprintf(stderr, "\n"); \
  }
#define red(...)       \
  {                    \
    log_prefix();      \
    ilog("\033[31m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define green(...)     \
  {                    \
    log_prefix();      \
    ilog("\033[32m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define blue(...)      \
  {                    \
    log_prefix();      \
    ilog("\033[34m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }

#else

#define log_prefix() ;
#define ilog(...) ;
#define llog(...) ;
#define log(...) ;
#define red(...) ;
#define green(...) ;
#define blue(...) ;

#endif

namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 5e5 + 5, P = 998244353;
#define int long long
int n, m, lb, lb2, vis[N], use[N];
struct atom {
  int a, b, c;
} a[N];
pair<int, int> b[N], b2[N];
long long f[N], c[N], sf[N];

int calc(int k) {
  // printf("\033[32mcalc(%lld)\033[0m\n",k);
  fill(vis, vis + N, 0);
  fill(f, f + N, 0);
  fill(c, c + N, 0);
  lb = 0;
  FOR(i, 1, m) {
    if (a[i].c >> k & 1) { // and出来是1
      vis[a[i].a]++, vis[a[i].b + 1]--;
    } else { //有一个是0
      b[++lb] = {a[i].a, a[i].b};
    }
  }
  FOR(i, 1, n) vis[i] += vis[i - 1]; // vis[i]>0，表示这个位置只能为1
  sort(b + 1, b + lb + 1, [](pair<int, int> x, pair<int, int> y) {
    return x.fi != y.fi ? x.fi < y.fi : x.se > y.se;
  });
  FOR(i, 1, lb) use[i] = 1;
  int mR = n + 1;
  ROF(i, lb, 1) {
    if (b[i].se >= mR) use[i] = 0;
    mR = min(mR, b[i].se);
  }
  lb2 = 0;
  FOR(i, 1, lb) if (use[i]) b2[++lb2] = b[i];
  FOR(i, 1, lb2) b[i] = b2[i];
  lb = lb2;

  f[0] = sf[0] = 1;

  int pos = 0, nex = 1;
  long long res = 0;

  while (nex <= lb && b[nex].fi <= 0) ++nex;
  if (nex > lb) res += f[0];

  FOR(i, 1, n) {
    if (vis[i]) {
      f[i] = 0;
    } else {
      while (pos < lb && b[pos + 1].se < i) ++pos;
      while (nex <= lb && b[nex].fi <= i) ++nex;
      // printf("i %lld pos %lld\n",i,pos);
      if (pos)
        f[i] = (sf[i - 1] - sf[b[pos].fi - 1] + P) % P;
      else
        f[i] = sf[i - 1];
      if (nex > lb) { res = (res + f[i]) % P; }
    }
    sf[i] = (sf[i - 1] + f[i]) % P;
    // printf("f[%lld] %lld vis %lld pos %d\n",i,f[i],vis[i],pos);
  }
  // printf("calc(%lld) %lld\n",k,res);
  return res;
}
signed main() {
  int k;
  scanf("%lld%lld%lld", &n, &k, &m);
  FOR(i, 1, m) {
    int l, r, x;
    scanf("%lld%lld%lld", &l, &r, &x);
    a[i] = {l, r, x};
  }
  long long ans = 1;
  FOR(i, 0, k - 1) ans = ans * calc(i) % P;
  int Ans = ans;
  printf("%lld\n", Ans);
  return 0;
}
