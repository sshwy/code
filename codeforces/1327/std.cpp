#include <algorithm>
#include <cstdio>
#include <cstring>
#include <iostream>
#define ll long long
using namespace std;
template <class T> inline void rd(T &x) {
  x = 0;
  char c = getchar();
  int f = 1;
  while (!isdigit(c)) {
    if (c == '-') f = -1;
    c = getchar();
  }
  while (isdigit(c)) x = x * 10 - '0' + c, c = getchar();
  x *= f;
}
const int N = 5e5 + 10, mod = 998244353;
struct cond {
  int l, r, x;
  cond(int l = 0, int r = 0, int x = 0)
      : l(l)
      , r(r)
      , x(x) {}
} A[N];
int n, m, k;
int vis[N], lim[N], f[N], g[N];
// int c[N];
// void I(int i,int x) { i++; for(;i<=n+1;i+=i&-i) c[i]=(c[i]+x)%mod; }
// int Q(int i) { i++; int ans=0; for(;i;i-=i&-i) ans=(ans+c[i])%mod; return
// ans; }
int main() {
  rd(n), rd(k), rd(m);
  for (int i = 1; i <= m; ++i) rd(A[i].l), rd(A[i].r), rd(A[i].x);
  int ans = 1;
  for (int d = 0; d < k; ++d) {
    for (int i = 1; i <= m; ++i) {
      if (A[i].x >> d & 1)
        vis[A[i].l]++, vis[A[i].r + 1]--;
      else
        lim[A[i].r + 1] = max(lim[A[i].r + 1], A[i].l);
    }
    // I(0,1);
    f[0] = g[0] = 1;
    int tot = 1;
    for (int i = 1; i <= n + 1; ++i) {
      lim[i] = max(lim[i - 1], lim[i]);
      vis[i] += vis[i - 1];
      if (vis[i])
        f[i] = 0;
      else {
        // f[i]=(tot-(lim[i]?Q(lim[i]-1):0));
        // I(i,f[i]);
        // tot=(tot+f[i])%mod;
        f[i] = (tot - (lim[i] ? g[lim[i] - 1] : 0)) % mod;
      }
      tot = (tot + f[i]) % mod;
      g[i] = (g[i - 1] + f[i]) % mod;
      printf("f[%d] %lld\n", i, f[i]);
    }
    ans = ans * (ll)f[n + 1] % mod;
    for (int i = 0; i <= n + 1; ++i) vis[i] = 0, lim[i] = 0; //,c[i]=0;
    printf("> %lld\n", f[n + 1]);
  }
  printf("%d", (ans + mod) % mod);
  return 0;
}
