// by Sshwy
//#define DEBUGGER
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

#ifdef DEBUGGER

#define log_prefix()                                                \
  {                                                                 \
    fprintf(stderr, "\033[37mLine %-3d [%dms]:\033[0m  ", __LINE__, \
        (int)clock() / 1000);                                       \
  }
#define ilog(...) \
  { fprintf(stderr, __VA_ARGS__); }
#define llog(...)      \
  {                    \
    log_prefix();      \
    ilog(__VA_ARGS__); \
  }
#define log(...)           \
  {                        \
    log_prefix();          \
    ilog(__VA_ARGS__);     \
    fprintf(stderr, "\n"); \
  }
#define red(...)       \
  {                    \
    log_prefix();      \
    ilog("\033[31m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define green(...)     \
  {                    \
    log_prefix();      \
    ilog("\033[32m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define blue(...)      \
  {                    \
    log_prefix();      \
    ilog("\033[34m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }

#else

#define log_prefix() ;
#define ilog(...) ;
#define llog(...) ;
#define log(...) ;
#define red(...) ;
#define green(...) ;
#define blue(...) ;

#endif

namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 2e5 + 5;
int n;
int p[N], c[N], vis[N];

int calc(int x) {
  vector<int> v;
  for (int u = p[x]; u != x; u = p[u]) v.pb(u), vis[u] = 1;
  v.pb(x), vis[x] = 1;
  for (int &z : v) z = c[z];

  int len = v.size();
  int res = len;
  auto check = [&](int k) {
    FOR(i, 0, k - 1) {
      bool flag = 1;
      for (int j = i; j < len; j += k) flag &= (v[i] == v[j]);
      if (flag) { return 1; }
    }
    return 0;
  };
  for (int i = 1; i * i <= len; ++i)
    if (len % i == 0) {
      if (check(i)) res = min(res, i);
      if (check(len / i)) res = min(res, len / i);
    }
  return res;
}
void go() {
  scanf("%d", &n);
  FOR(i, 1, n) scanf("%d", &p[i]);
  FOR(i, 1, n) scanf("%d", &c[i]);
  fill(vis, vis + N, 0);
  int ans = n;
  FOR(i, 1, n) if (!vis[i]) ans = min(ans, calc(i));
  printf("%d\n", ans);
}
int main() {
  int t;
  scanf("%d", &t);
  FOR(i, 1, t) go();
  return 0;
}
