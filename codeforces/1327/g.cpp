// by Sshwy
#define DEBUGGER
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

#ifdef DEBUGGER

#define log_prefix()                                                \
  {                                                                 \
    fprintf(stderr, "\033[37mLine %-3d [%dms]:\033[0m  ", __LINE__, \
        (int)clock() / 1000);                                       \
  }
#define ilog(...) \
  { fprintf(stderr, __VA_ARGS__); }
#define llog(...)      \
  {                    \
    log_prefix();      \
    ilog(__VA_ARGS__); \
  }
#define log(...)           \
  {                        \
    log_prefix();          \
    ilog(__VA_ARGS__);     \
    fprintf(stderr, "\n"); \
  }
#define red(...)       \
  {                    \
    log_prefix();      \
    ilog("\033[31m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define green(...)     \
  {                    \
    log_prefix();      \
    ilog("\033[32m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define blue(...)      \
  {                    \
    log_prefix();      \
    ilog("\033[34m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }

#else

#define log_prefix() ;
#define ilog(...) ;
#define llog(...) ;
#define log(...) ;
#define red(...) ;
#define green(...) ;
#define blue(...) ;

#endif

namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
namespace IO { // require C++11
  const int _BS = 200000;
  char _ob[_BS], *_z = _ob, _ib[_BS], *_x = _ib, *_y = _ib;
  // Input
  char _nc() {
    return _x == _y && (_y = (_x = _ib) + fread(_ib, 1, _BS, stdin), _x == _y)
               ? EOF
               : *_x++;
  }
  inline void rd(char &c) { c = _nc(); }
  void rd(char *s) {
    char c = _nc();
    while (!isblank(c) && isprint(c) && c != EOF) *s++ = c, c = _nc();
    *s = 0;
  }
  template <class T> void rd(T &res) { // unsigned/signed int/long long
    res = 0;
    char c = _nc(), flag = 0;
    while (!isdigit(c) && c != '-') c = _nc();
    if (c == '-') flag = 1, c = _nc();
    while (isdigit(c)) res = res * 10 + c - '0', c = _nc();
    if (flag) res = -res;
  }
  template <class T, class... Ar> void rd(T &hd, Ar &...rs) { rd(hd), rd(rs...); }
  // Output
  inline void flush() { fwrite(_ob, 1, _z - _ob, stdout), _z = _ob; }
  inline void wrch(char x) {
    if (_z == _ob + _BS - 1) flush();
    *_z = x, ++_z;
  }
  void wr(char *s) {
    while (*s) wrch(*s), ++s;
  }
  template <class T> void wr(T x) { // unsigned/signed int/long long
    if (x == 0) return wrch('0'), void();
    if (x < 0) wrch('-'), x = -x;
    int _t[30], _lt = 0;
    while (x) _t[++_lt] = x % 10, x /= 10;
    while (_lt > 0) wrch(_t[_lt] + '0'), --_lt;
  }
  inline void wr(char x) { wrch(x); }
  template <class T, class... Ar> void wr(T hd, Ar... rs) { wr(hd), wr(rs...); }
  template <class... Ar> void wrln(Ar... ar) { wr(ar...), wrch('\n'); }
  struct _Flusher {
    ~_Flusher() { flush(); }
  } _flusher;
} // namespace IO
const int SZ = 1020, ALP = 14, N = 4e5 + 5;
typedef long long LL;
int k;
int tot, tr[SZ][ALP], fail[SZ];
LL val[SZ], F[1 << ALP][SZ], G[1 << ALP][SZ];

void insert(char *s, int x) {
  // printf("insert(%s,%d)\n",s,x);
  int u = 0;
  while (*s) {
    int c = *s - 'a';
    if (!tr[u][c]) tr[u][c] = ++tot;
    u = tr[u][c];
    ++s;
  }
  val[u] += x;
}
queue<int> q;
void build() {
  FOR(i, 0, ALP - 1) if (tr[0][i]) q.push(tr[0][i]);
  while (!q.empty()) {
    int u = q.front();
    q.pop();
    val[u] += val[fail[u]];
    // printf("val %d %lld\n",u,val[u]);
    FOR(i, 0, ALP - 1) {
      if (tr[u][i])
        fail[tr[u][i]] = tr[fail[u]][i], q.push(tr[u][i]);
      else
        tr[u][i] = tr[fail[u]][i];
    }
  }
}

char s[N];

int main() {
  IO::rd(k);
  FOR(i, 1, k) {
    int x;
    IO::rd(s, x);
    insert(s, x);
  }
  build();
  // FOR(i,0,tot){
  //    FOR(j,0,ALP-1)if(tr[i][j]){
  //        printf("%d -> %d [label=\"%c\"];\n",i,tr[i][j],char(j+'a'));
  //    }
  //}

  IO::rd(s + 1);
  int n = strlen(s + 1), cqm = 0;
  FOR(i, 1, n) cqm += s[i] == '?';

  vector<vector<pair<int, LL>>> Go;

  for (int pos = 0, nex; pos <= n; pos = nex) {
    nex = pos + 1;
    while (nex <= n && s[nex] != '?') ++nex;
    vector<pair<int, LL>> go;
    FOR(i, 0, tot) {
      int u = i;
      LL v = 0;
      FOR(j, pos + 1, nex - 1) {
        u = tr[u][s[j] - 'a'];
        v += val[u];
      }
      go.pb({u, v});
    }
    assert(int(go.size()) == tot + 1);
    // for(auto p:go)printf("(%d,%lld) ",p.fi,p.se);
    // puts("");
    Go.pb(go);
  }

  LL lim = 1 << ALP;
  const LL INF = 0x3f3f3f3f3f3f3f3f;
  auto *f = F;
  auto *g = G;

  FOR(i, 0, lim - 1) fill(f[i], f[i] + tot + 1, -INF);
  f[0][0] = 0;

  for (auto p = Go.begin(); p != Go.end(); ++p) {
    FOR(i, 0, lim - 1) fill(g[i], g[i] + tot + 1, -INF);
    const auto &T = *p;
    FOR(j, 0, tot) {
      const auto x = T[j];
      // j => x.fi
      FOR(i, 0, lim - 1) {
        if (f[i][j] == -INF) continue;
        auto &y = g[i][x.fi];
        y = max(y, f[i][j] + x.se);
      }
    }
    swap(f, g);
    if ((p + 1) != Go.end()) {
      FOR(i, 0, lim - 1) fill(g[i], g[i] + tot + 1, -INF);
      FOR(i, 0, lim - 1) {
        FOR(j, 0, tot) {
          if (f[i][j] == -INF) continue;
          FOR(k, 0, ALP - 1) if (!(i >> k & 1)) {
            int ni = i ^ (1 << k), nj = tr[j][k];
            g[ni][nj] = max(g[ni][nj], f[i][j] + val[nj]);
          }
        }
      }
      swap(f, g);
    }
    // printf("%d\n",p-Go.begin());
    // FOR(i,0,lim-1)FOR(j,0,tot)if(f[i][j]!=-INF)printf("f[%d,%d]
    // %lld\n",i,j,f[i][j]);
  }

  LL ans = -INF;
  FOR(i, 0, lim - 1) FOR(j, 0, tot) ans = max(ans, f[i][j]);
  IO::wrln(ans);

  return 0;
}
