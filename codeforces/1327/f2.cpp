// by Sshwy
//#define DEBUGGER
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

#ifdef DEBUGGER

#define log_prefix()                                                \
  {                                                                 \
    fprintf(stderr, "\033[37mLine %-3d [%dms]:\033[0m  ", __LINE__, \
        (int)clock() / 1000);                                       \
  }
#define ilog(...) \
  { fprintf(stderr, __VA_ARGS__); }
#define llog(...)      \
  {                    \
    log_prefix();      \
    ilog(__VA_ARGS__); \
  }
#define log(...)           \
  {                        \
    log_prefix();          \
    ilog(__VA_ARGS__);     \
    fprintf(stderr, "\n"); \
  }
#define red(...)       \
  {                    \
    log_prefix();      \
    ilog("\033[31m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define green(...)     \
  {                    \
    log_prefix();      \
    ilog("\033[32m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define blue(...)      \
  {                    \
    log_prefix();      \
    ilog("\033[34m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }

#else

#define log_prefix() ;
#define ilog(...) ;
#define llog(...) ;
#define log(...) ;
#define red(...) ;
#define green(...) ;
#define blue(...) ;

#endif

namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 5e5 + 5, P = 998244353;
int n, m, lb, lb2, vis[N], use[N];
struct atom {
  int a, b, c;
} a[N];
pair<int, int> b[N], b2[N];
long long f[N], c[N];

int main() {
  int k;
  scanf("%d%d%d", &n, &k, &m);
  FOR(i, 1, m) {
    int l, r, x;
    scanf("%d%d%d", &l, &r, &x);
    a[i] = {l, r, x};
  }

  long long ans = 0;

  int lim = 1 << k;
  FOR(x1, 0, lim - 1)
  FOR(x2, 0, lim - 1)
  FOR(x3, 0, lim - 1)
  FOR(x4, 0, lim - 1)
  FOR(x5, 0, lim - 1)
  FOR(x6, 0, lim - 1) {
    int A[] = {0, x1, x2, x3, x4, x5, x6};
    bool flag = 1;
    FOR(i, 1, m) {
      int x = A[a[i].a];
      FOR(j, a[i].a, a[i].b) x &= A[j];
      if (x != a[i].c) flag = 0;
    }
    if (flag) ++ans;
  }

  int Ans = ans;
  printf("%d\n", Ans);
  return 0;
}
