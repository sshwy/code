// by Sshwy
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 3005;

vector<int> g[N];

int n;
double ans;
int dep[N], cir;
int a[N][2];

void dfs(int u, int p, int d) {
  dep[u] = d;
  a[u][0] ? a[u][1] = d : a[u][0] = d;
  for (int v : g[u])
    if (v != p) dep[v] ? cir = d - dep[v] + 1 : (dfs(v, u, d + 1), 0);
  dep[u] = 0;
}
void calc(int u) {
  memset(a, 0, sizeof(a));
  cir = 0;
  dfs(u, 0, 1);
  FOR(i, 1, n) {
    if (a[i][1])
      ans +=
          1.0 / a[i][0] + 1.0 / a[i][1] - 1.0 / ((a[i][0] + a[i][1] - 2 + cir) / 2.0);
    else
      ans += 1.0 / a[i][0];
  }
}
int main() {
  cin >> n;
  FOR(i, 1, n) {
    int u, v;
    cin >> u >> v;
    ++u, ++v;
    g[u].pb(v), g[v].pb(u);
  }
  FOR(i, 1, n) calc(i);
  printf("%.15lf", ans);
  return 0;
}
