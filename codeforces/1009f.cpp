#include <algorithm>
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (int)(b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (int)(b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 1e6 + 5;

struct qxx {
  int nex, t;
};
qxx e[N * 2];
int h[N], le = 1;
void add_path(int f, int t) { e[++le] = (qxx){h[f], t}, h[f] = le; }
#define FORe(i, u, v) for (int i = h[u], v; v = e[i].t, i; i = e[i].nex)

int n;

deque<int> *d[N];
long long ans;

int dep[N], hgh[N];
int a[N], b[N];
void dfs1(int u, int p) {
  dep[u] = dep[p] + 1;
  hgh[u] = 1;
  FORe(i, u, v) if (v != p) dfs1(v, u), hgh[u] = max(hgh[u], hgh[v] + 1);
}
void dfs2(int u, int p) {
  int mx = 0, son = -1;
  FORe(i, u, v) if (v != p && hgh[v] > mx) mx = hgh[v], son = v;
  if (son == -1) {
    d[u] = new deque<int>;
    d[u]->pb(1);
    a[u] = 0, b[u] = 1;
    return;
  }
  dfs2(son, u);
  FORe(i, u, v) if (v != p && v != son) dfs2(v, u);
  //继承 son
  d[u] = d[son];
  d[u]->push_front(1);
  a[u] = 0, b[u] = 1;
  if (b[son] > b[u])
    b[u] = b[son], a[u] = a[son] + 1;
  else if (b[son] == b[u])
    a[u] = min(a[u], a[son] + 1);
  //合并其他儿子的答案
  FORe(i, u, v) {
    if (v == p || v == son) continue;
    int lim = (int)d[v]->size() - 1;
    int mx2 = 0, ans = -1;
    FOR(i, 0, lim) {
      (*d[u])[i + 1] += (*d[v])[i];
      if ((*d[u])[i + 1] > mx2) mx2 = (*d[u])[i + 1], ans = i + 1;
    }
    if (mx2 > b[u])
      b[u] = mx2, a[u] = ans;
    else if (mx2 == b[u])
      a[u] = min(a[u], ans);
    delete (d[v]);
  }
}

int main() {
  scanf("%d", &n);
  FOR(i, 1, n - 1) {
    int x, y;
    scanf("%d%d", &x, &y);
    add_path(x, y), add_path(y, x);
  }
  dfs1(1, 0);
  dfs2(1, 0);
  FOR(i, 1, n) printf("%d\n", a[i]);
  return 0;
}
