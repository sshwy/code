#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 120;

int n, m;
long long a[N];
int v[N];
long long c[2];

int main() {
  scanf("%d%d", &n, &m);
  FOR(i, 1, n) scanf("%lld", &a[i]);

  FOR(s, 2, m * 2) { // s=a+b
    FOR(i, 1, n) v[i] = a[i] % s;
    sort(v + 1, v + n + 1);
    v[n + 1] = s - 1;
    int cur = 0;
    ROF(i, n + 1, 1) {
      int l = v[i - 1] + 1, r = v[i];
      l = max(l, s - v[i]);
      r = min(r, m);
      if (cur == 0) { //统计后手获胜的情况
        l = max(l, v[n] / 2 + 1);
      } else { //统计先手获胜的情况
        l = max(l, v[n - 1] / 2 + 1);
      }
      r = min(r, s - l), l = max(l, s - r);
      r = min(r, s - l), l = max(l, s - r);
      c[cur] += max(0, r - l + 1);
      cur ^= 1;
    }
  }
  long long x = (1ll * m * m - c[1] - c[0]) / 2;
  printf("%lld %lld %lld %lld\n", x, x, c[1], c[0]);
  return 0;
}
