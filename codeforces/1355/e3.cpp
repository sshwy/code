// by Sshwy
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA
const int N = 1e5 + 5;
int n;
long long A, R, M;
long long h[N];

long long calc(long long x) {
  long long X = 0, Y = 0;
  FOR(i, 1, n) if (h[i] < x) X += x - h[i];
  else Y += h[i] - x;
  long long tot = min(X, Y) * M;
  if (X < Y)
    tot += (Y - X) * R;
  else
    tot += (X - Y) * A;
  return tot;
}
int main() {
  scanf("%d%lld%lld%lld", &n, &A, &R, &M);
  M = min(M, A + R);
  FOR(i, 1, n) scanf("%lld", &h[i]);
  sort(h + 1, h + n + 1);
  long long l = 0, r = 1e9;
  while (l < r) {
    int mid = (l + r) >> 1;
    if (calc(mid + 1) - calc(mid) > 0)
      r = mid;
    else
      l = mid + 1;
  }
  printf("%lld\n", calc(l));
  return 0;
}
