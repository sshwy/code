// by Sshwy
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

int a, b, c, d;

int main() {
  scanf("%d%d%d%d", &a, &b, &c, &d);
  long long ans = 0;
  FOR(z, c, d) {
    // x+y>=z
    // y>=z-x
    int L = max(z - b, b), R = min(c - 1, z - a); // z-x bound
    if (L <= R) {
      // printf("L %d R %d\n",L,R);
      ans += (c - R + c - L) * 1ll * (R - L + 1) / 2;
    }
    L = max(z - b + 1, a), R = b;
    if (L <= R) {
      // printf("L %d R %d\n",L,R);
      ans += (R - L + 1) * 1ll * (c - b + 1);
    }
    // printf("z %d ans %lld\n",z,ans);
  }
  printf("%lld\n", ans);
  return 0;
}
