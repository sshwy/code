// by Sshwy
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

long long a, k;
long long calc(long long x) {
  long long mx = 0, mn = 9;
  while (x) mx = max(mx, x % 10), mn = min(mn, x % 10), x /= 10;
  return mx * mn;
}
void go() {
  scanf("%lld%lld", &a, &k);
  FOR(i, 1, k - 1) {
    a = a + calc(a);
    if (calc(a) == 0) break;
  }
  printf("%lld\n", a);
}
int main() {
  int t;
  scanf("%d", &t);
  FOR(i, 1, t) go();
  return 0;
}
