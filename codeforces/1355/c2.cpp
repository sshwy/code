// by Sshwy
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

int a, b, c, d;

int main() {
  cin >> a >> b >> c >> d;
  int ans = 0;
  FOR(x, a, b) FOR(y, b, c) FOR(z, c, d) if (x + y > z)++ ans;
  printf("%d\n", ans);
  return 0;
}
