// by Sshwy
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

int n, s;

int main() {
  scanf("%d%d", &n, &s);
  if (s == 1) return puts("NO"), 0;
  FOR(d, 2, s) {
    if (s % d) continue;
    if (s / d >= n) {
      puts("YES");
      FOR(i, 1, n - 1) printf("%d ", d);
      printf("%d\n", s - (n - 1) * d);
      puts("1");
      return 0;
    }
  }
  if (n * 2 <= s) {
    puts("YES");
    FOR(i, 1, n - 1) printf("2 ");
    printf("%d\n", s - (n - 1) * 2);
    puts("1");
    return 0;
  }
  puts("NO");
  return 0;
}
