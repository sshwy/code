// by Sshwy
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA
const int N = 1e5 + 5;
int n;
long long A, R, M;
long long h[N];

int main() {
  scanf("%d%lld%lld%lld", &n, &A, &R, &M);
  M = min(M, A + R);
  FOR(i, 1, n) scanf("%lld", &h[i]);
  sort(h + 1, h + n + 1);
  long long P = 0, Q = 0;
  FOR(i, 1, n) Q += h[i];
  long long ans = (Q - n * h[1]) * R;
  ans = min(ans, (h[n] * n - Q) * A);
  long long S = Q, sp = S / n;
  FOR(i, 1, n - 1) {
    P += h[i], Q -= h[i];
    long long L = h[i], R = h[i + 1];
    R = min(R, sp);
    if (L > R) break;
    long long k = i * M - R * n, b = R * S - P * M;
    ans = min(ans, L * k + b);
    ans = min(ans, R * k + b);
    printf("f(%lld)=%lld\n", L, L * k + b);
  }
  P = Q = 0;
  FOR(i, 1, n) P += h[i];
  ROF(i, n - 1, 1) {
    P -= h[i + 1], Q += h[i + 1];
    long long L = h[i], R = h[i + 1];
    L = max(L, sp + 1);
    if (L > R) break;
    long long k = A * n - (n - i) * M, b = Q * M - A * S;
    ans = min(ans, L * k + b);
    ans = min(ans, R * k + b);
  }
  printf("%lld\n", ans);
  return 0;
}
