// by Sshwy
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA
const int N = 3e5 + 5;
int n;
int a[N];
void go() {
  scanf("%d", &n);
  FOR(i, 1, n) scanf("%d", &a[i]);
  sort(a + 1, a + n + 1);
  int cur = 0;
  a[n + 1] = 0;
  int ans = 0;
  FOR(i, 1, n) {
    ++cur;
    if (a[i] != a[i + 1]) {
      ans += cur / a[i];
      cur %= a[i];
    }
  }
  printf("%d\n", ans);
}
int main() {
  int t;
  scanf("%d", &t);
  FOR(i, 1, t) go();
  return 0;
}
