#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 2e6 + 5;

int n;
char a[N];
int d[N], h[N];
vector<int> g[N];

bool check(int u) {
  if (d[u] != 3) return 0;
  int cnt = 0;
  for (int v : g[u]) {
    int fl = 0;
    for (int w : g[v]) {
      if (w != u) ++fl;
      if (fl) break;
    }
    cnt += !!fl;
  }
  return cnt >= 2;
}
void go() {
  scanf("%d", &n);
  FOR(i, 0, n * 4) d[i] = 0, g[i].clear();
  FOR(i, 1, n - 1) {
    int a, b;
    scanf("%d%d", &a, &b);
    g[a].pb(b);
    g[b].pb(a);
    d[a]++, d[b]++;
  }
  int n2 = n;
  scanf("%s", a + 1);
  FOR(i, 1, n2) if (a[i] == 'W') {
    ++n, d[i]++, d[n]++;
    g[i].pb(n), g[n].pb(i);
    d[n] += 2;
    ++n, d[n]++;
    g[n - 1].pb(n);
    g[n].pb(n - 1);
    ++n, d[n]++;
    g[n - 2].pb(n);
    g[n].pb(n - 2);
  }
  int cnt = 0;
  FOR(i, 1, n) {
    if (d[i] > 3) return puts("White"), void();
    if (d[i] == 3) ++cnt;
    if (cnt > 2) return puts("White"), void();
    if (check(i)) return puts("White"), void();
  }
  if (cnt < 2)
    return puts("Draw"), void();
  else if (n & 1)
    return puts("White"), void();
  puts("Draw");
}
int main() {
  int t;
  scanf("%d", &t);
  FOR(i, 1, t) go();
  return 0;
}
