// by Sshwy
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int M = 1e5 + 5;

long double fac[M], ans;
long double binom(int n, int m) { return fac[n] - fac[m] - fac[n - m]; }
int main() {
  int n, n2, m, k;
  scanf("%d%d%d", &n, &m, &k);
  n2 = n * n;
  fac[0] = log(1);
  FOR(i, 1, m) fac[i] = fac[i - 1] + log(i);

  FOR(i, 0, n) FOR(j, 0, n) {
    int s = (n - i) * (n - j);
    if (k + s - n2 >= 0)
      ans += exp(binom(n, i) + binom(n, j) + fac[k] - fac[k - n2 + s] +
                 fac[m - n2 + s] - fac[m]);
  }
  printf("%.15lf", (double)min(ans, (long double)1e99));
  return 0;
}
