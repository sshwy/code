// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

const int N = 3e5 + 5;

int fa[N], ans[N];
int n, m;

int get(int u) { return fa[u] == u ? u : fa[u] = get(fa[u]); }
int main() {
  scanf("%d%d", &n, &m);
  FOR(i, 0, n + 1) fa[i] = i;
  FOR(i, 1, m) {
    int l, r, x;
    scanf("%d%d%d", &l, &r, &x);
    for (int j = get(l); j < x; j = get(j)) {
      ans[j] = x;
      fa[get(j)] = j + 1;
    }
    for (int j = get(x + 1); j <= r; j = get(j)) {
      ans[j] = x;
      fa[get(j)] = j + 1;
    }
  }
  FOR(i, 1, n) printf("%d%c", ans[i], " \n"[i == n]);
  return 0;
}
