// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

int c[5], n;

int main() {
  scanf("%d", &n);
  FOR(i, 1, n) {
    int x;
    scanf("%d", &x);
    c[x]++;
  }
  int ans = 0;
  if (c[1] && c[2]) {
    int t = min(c[1], c[2]);
    ans += t;
    c[1] -= t;
    c[2] -= t;
    c[3] += t;
  }
  if (c[1]) {
    assert(c[2] == 0);
    int rest = c[1] % 3, round = c[1] / 3;
    ans += round * 2;
    c[3] += round;
    if (rest <= c[3]) {
      ans += rest;
      return printf("%d\n", ans), 0;
    } else {
      if (rest == 1) {
        if (c[4] < 2) return puts("-1"), 0;
        ans += 2;
        return printf("%d\n", ans), 0;
      } else if (rest == 2) {
        if (c[4] < 1) return puts("-1"), 0;
        ans += 2;
        return printf("%d\n", ans), 0;
      } else
        assert(0);
    }
  } else {
  }
  return 0;
}
