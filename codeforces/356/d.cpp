// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

const int N = 70105;

int n, s;
int a[N], b[N], c[N], son[N];
bool vis[N];
unsigned f[N / 32];

bool get(int pos) { return f[pos >> 5] >> (pos & 31) & 1; }
#define ls(x, y) ((y) ? (((x) & ((1u << (32 - (y))) - 1)) << (y)) : (x))
unsigned getChunk(int pos) {
  unsigned v = (f[pos >> 5] >> (pos & 31));
  if (pos & 31) v |= ls(f[(pos >> 5) + 1], 32 - (pos & 31));
  return v;
}
void bitorChunk(int pos, unsigned v) {
  f[pos >> 5] |= ls(v, pos & 31);
  if (pos & 31) f[(pos >> 5) + 1] |= v >> (32 - (pos & 31));
}

void getScheme(int x) {
  if (x == 0) return;
  assert(b[x] < 1e9);
  int i = b[x];
  assert(b[x - a[i]] < i);
  vis[i] = true;
  getScheme(x - a[i]);
}

bool cmp(int x, int y) {
  if (a[x] != a[y]) return a[x] < a[y];
  return vis[x] < vis[y];
}

int main() {
  scanf("%d%d", &n, &s);
  FOR(i, 1, n) { scanf("%d", a + i); }
  bitorChunk(0, 1);

  int maxI = max_element(a + 1, a + n + 1) - a;
  s -= a[maxI];
  vis[maxI] = true;

  FOR(i, 0, s) b[i] = 1e9;
  b[0] = 0;

  int lim = s / 32;
  FOR(i, 1, n) {
    if (i == maxI) continue;
    ROF(j, lim, 0) {
      unsigned v = getChunk(j * 32 + a[i]);
      if ((v | f[j]) > v) {
        FOR(k, 0, 31) {
          if (!(v >> k & 1) && (f[j] >> k & 1)) { b[j * 32 + k + a[i]] = i; }
        }
      }
      bitorChunk(j * 32 + a[i], f[j]);
    }
  }

  if (getChunk(s) & 1) {
    getScheme(s);
  } else {
    puts("-1");
    return 0;
  }

  FOR(i, 1, n) c[i] = i;
  sort(c + 1, c + n + 1, cmp);

  FOR(i, 2, n) {
    if (vis[c[i - 1]] == false) { son[c[i]] = c[i - 1]; }
  }

  FOR(i, 1, n) {
    if (son[i]) {
      printf("%d 1 %d\n", a[i] - a[son[i]], son[i]);
    } else {
      printf("%d 0\n", a[i]);
    }
  }

  return 0;
}
