// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

const int L = 1e6 + 5;

long long n, m, ls, lt, ans;
string s, t;
bool vis[L];

int main() {
  ios::sync_with_stdio(false);
  cin >> n >> m;
  cin >> s >> t;

  if (n < m) swap(n, m), swap(s, t);

  ls = s.size();
  lt = t.size();

  assert(ls <= lt);

  FOR(i, 0, ls - 1) {
    if (vis[i]) continue;
    string r, q;
    for (int j = i; !vis[j]; j = (j + ls) % lt) {
      vis[j] = true;
      r += t[j];
      q += j < ls ? s[j] : ' ';
    }
    int cnt[26] = {0};
    long long lr = r.size(), len = n % lr, loop = n / lr;
    FOR(i, 0, len - 1) cnt[r[i] - 'a']++;
    FOR(i, 0, lr - 1) {
      if (q[i] != ' ') { ans += cnt[q[i] - 'a']; }
      cnt[r[i] - 'a']--;
      cnt[r[(i + len) % lr] - 'a']++;
    }
    memset(cnt, 0, sizeof(cnt));
    FOR(i, 0, lr - 1) cnt[r[i] - 'a']++;
    FOR(i, 0, lr - 1) {
      if (q[i] != ' ') { ans += 1ll * cnt[q[i] - 'a'] * loop; }
    }
  }
  cout << n * ls - ans << endl;
  return 0;
}
