#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
namespace IO {
  char nc() {
    static char bf[100000], *p1 = bf, *p2 = bf;
    return p1 == p2 && (p2 = (p1 = bf) + fread(bf, 1, 100000, stdin), p1 == p2)
               ? EOF
               : *p1++;
  }
  int rd() {
    int res = 0;
    char c = getchar();
    while (!isdigit(c)) c = getchar();
    while (isdigit(c)) res = res * 10 + c - '0', c = getchar();
    return res;
  }
} // namespace IO
/******************heading******************/
const int SZ = 2e6 + 500, ALP = 26;

struct SAM {
  int tot, last;
  int tr[SZ][ALP], fail[SZ];
  int len[SZ], cnt[SZ], end[SZ]; // cnt:状态出现次数
  int s[SZ], ls;
  SAM() { tot = last = 1, len[1] = 0, fail[1] = 0; }
  void insert(char x) {
    s[++ls] = x;
    x -= 'a';
    int u = ++tot, p = last;
    len[u] = len[last] + 1, last = u;
    cnt[u] = 1, end[u] = ls;
    while (p && tr[p][x] == 0) tr[p][x] = u, p = fail[p];
    if (!p)
      fail[u] = 1;
    else {
      int q = tr[p][x];
      if (len[q] == len[p] + 1)
        fail[u] = q;
      else {
        int cq = ++tot;
        len[cq] = len[p] + 1, fail[cq] = fail[q];
        end[cq] = end[q];
        memcpy(tr[cq], tr[q], sizeof(tr[q]));
        fail[q] = fail[u] = cq;
        while (p && tr[p][x] == q) tr[p][x] = cq, p = fail[p];
      }
    }
  }
  int a[SZ], bin[SZ], tim[SZ]; // tim：每个结点的时间戳
  void qsort() {
    FOR(i, 1, tot) bin[len[i]]++;
    FOR(i, 1, tot) bin[i] += bin[i - 1];
    FOR(i, 1, tot) a[bin[len[i]]--] = i;
    ROF(i, tot, 1) cnt[fail[a[i]]] += cnt[a[i]];
    FOR(i, 1, tot) print_node(i);
  }
  void print_node(int u) {
    printf("u=%2d,cnt=%2d,len=%2d,fail=%2d, ", u, cnt[u], len[u], fail[u]);
    FOR(i, end[u] - len[u] + 1, end[u]) putchar(s[i]);
    puts("");
  }
  void query(const char *x, int ti) {
    int u = 1, lx = strlen(x + 1), res = 0, curlen = 0;
    FOR(i, 1, lx) {
      int c = x[i] - 'a';
      while (u && !tr[u][c]) u = fail[u], curlen = min(curlen, len[u]);
      if (!u) break;
      u = tr[u][c], curlen++;
    }
    if (!u) {
      puts("0");
      return;
    }
    FOR(i, 1, lx) {
      int c = x[i] - 'a';
      while (u && !tr[u][c]) u = fail[u], curlen = min(curlen, len[u]);
      if (!u) break;
      u = tr[u][c], curlen = min(lx, curlen + 1);
      while (fail[u] && len[fail[u]] >= curlen) u = fail[u];
      if (curlen >= lx && tim[u] < ti) res += cnt[u], tim[u] = ti;
    }
    printf("%d\n", res);
  }
} sam;

char s[SZ];
char x[SZ];
int n;

int main() {
  scanf("%s", s + 1);
  for (int i = 1; s[i]; i++) sam.insert(s[i]);
  sam.qsort();
  scanf("%d", &n);
  FOR(i, 1, n) {
    scanf("%s", x + 1);
    sam.query(x, i);
  }
  return 0;
}
