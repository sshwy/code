#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
namespace IO {
  char nc() {
    static char bf[100000], *p1 = bf, *p2 = bf;
    return p1 == p2 && (p2 = (p1 = bf) + fread(bf, 1, 100000, stdin), p1 == p2)
               ? EOF
               : *p1++;
  }
  int rd() {
    int res = 0;
    char c = getchar();
    while (!isdigit(c)) c = getchar();
    while (isdigit(c)) res = res * 10 + c - '0', c = getchar();
    return res;
  }
} // namespace IO
/******************heading******************/
#define int lld
const int N = 1e5 + 5, SZ = N << 2;

int a[N];

struct node {
  int s, m;
  void assign(int x) { s = m = x; }
  void mod(int x) { s %= x, m %= x; }
} t[SZ];

int n, m;
void pushup(int u) {
  t[u].s = t[u << 1].s + t[u << 1 | 1].s;
  t[u].m = max(t[u << 1].m, t[u << 1 | 1].m);
}
void build(int u = 1, int l = 1, int r = n) {
  if (l == r) return t[u].assign(a[l]), void();
  int mid = (l + r) >> 1;
  build(u << 1, l, mid), build(u << 1 | 1, mid + 1, r);
  pushup(u);
}
void mod(int L, int R, int v, int u = 1, int l = 1, int r = n) {
  if (R < l || r < L) return;
  if (t[u].m < v) return;
  if (l == r) return t[u].mod(v), void();
  int mid = (l + r) >> 1;
  mod(L, R, v, u << 1, l, mid), mod(L, R, v, u << 1 | 1, mid + 1, r);
  pushup(u);
}
void assign(int p, int v, int u = 1, int l = 1, int r = n) {
  if (l == r) return t[u].assign(v), void();
  int mid = (l + r) >> 1;
  if (p <= mid)
    assign(p, v, u << 1, l, mid);
  else
    assign(p, v, u << 1 | 1, mid + 1, r);
  pushup(u);
}
int query_sum(int L, int R, int u = 1, int l = 1, int r = n) {
  if (R < l || r < L) return 0;
  if (L <= l && r <= R) return t[u].s;
  int mid = (l + r) >> 1;
  return query_sum(L, R, u << 1, l, mid) + query_sum(L, R, u << 1 | 1, mid + 1, r);
}

signed main() {
  cin >> n >> m;
  FOR(i, 1, n) cin >> a[i];
  build();
  FOR(i, 1, m) {
    int typ, l, r, x, k;
    cin >> typ;
    if (typ == 1) {
      cin >> l >> r;
      cout << query_sum(l, r) << endl;
    } else if (typ == 2) {
      cin >> l >> r >> x;
      mod(l, r, x);
    } else {
      cin >> k >> x;
      assign(k, x);
    }
  }
  return 0;
}
