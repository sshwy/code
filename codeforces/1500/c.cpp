// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define L first
#define R second
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

const int N = 1505;

int n, m, totvis;
int a[N][N], b[N][N], up[N][N];
bool vis[N];         // 已经决定要 sort
bool ban[N], bad[N]; // 不允许 sort
bool cut[N];         // 作为区间右端点

vector<pair<int, int>> v;
vector<int> ans;

void nosol() {
  puts("-1");
  exit(0);
}
pair<int, int> t[N];
void work(int *f, int col) {
  FOR(i, 1, n) t[i] = {a[f[i]][col], i};
  sort(t + 1, t + n + 1);
  FOR(i, 1, n) t[i].second = f[t[i].second];
  FOR(i, 1, n) f[i] = t[i].second;
}
void goend() {
  FOR(i, 1, m) if (ban[i] && vis[i]) nosol();
  reverse(ans.begin(), ans.end());
  int row[N];
  FOR(i, 1, n) row[i] = i;
  for (auto c : ans) {
    work(row, c);
    // printf("after sort by col %d\n", c);
    // FOR(i, 1, n) printf("%d%c", row[i], " \n"[i == n]);
  }
  FOR(i, 1, n) FOR(j, 1, m) {
    if (a[row[i]][j] != b[i][j]) nosol();
  }
  printf("%lu\n", ans.size());
  for (unsigned i = 0; i < ans.size(); i++)
    printf("%d%c", ans[i], " \n"[i + 1 == ans.size()]);
  exit(0);
}
void go() {
  int good[N] = {0}, isR[N] = {0};
  FOR(i, 1, n) if (cut[i]) bad[i] = true;
  for (auto e : v) {
    int l = e.L, r = e.R;
    isR[r] = true;
    FOR(i, 1, m) if (!vis[i]) {
      if (up[r][i] <= l) {
        good[i]++;
        bad[r] = false;
      }
    }
  }
  bool end = false;
  FOR(i, 1, n) if (isR[i] && bad[i]) { // 没有出现单增
    end = true;
    FOR(j, 1, m) if (!vis[j]) ban[j] = true;
  }
  if (end) goend();
  FOR(i, 1, m) if (!vis[i] && good[i] == v.size()) {
    vis[i] = true, ++totvis;
    // printf("col(%d)\n", i);
    ans.pb(i);
    for (auto e : v) {
      int l = e.L, r = e.R;
      FOR(j, l, r - 1) if (b[j][i] < b[j + 1][i]) cut[j] = true;
    }
  }
  if (v.empty()) goend();
  v.clear();
  int las = 1;
  FOR(i, 1, n) if (cut[i]) {
    if (las < i) v.pb({las, i});
    las = i + 1;
  }
  if (totvis == n) goend();
}
int main() {
  scanf("%d%d", &n, &m);
  FOR(i, 1, n) {
    FOR(j, 1, m) { scanf("%d", &a[i][j]); }
  }
  FOR(i, 1, n) {
    FOR(j, 1, m) {
      scanf("%d", &b[i][j]);
      if (i == 1)
        up[i][j] = i;
      else {
        if (b[i - 1][j] <= b[i][j])
          up[i][j] = up[i - 1][j];
        else
          up[i][j] = i;
      }
    }
  }
  v.pb({1, n});
  cut[n] = true;
  while (1) go();
  return 0;
}
