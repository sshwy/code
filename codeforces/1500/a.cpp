// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

const int N = 2e5 + 5, A = 5e6 + 5;
int n;
int ans[10], la;

pair<int, int> p[A], a[N];
vector<int> pos[A], p2;
void end() {
  puts("YES");
  FOR(i, 1, 4) printf("%d%c", ans[i], " \n"[i == 4]);
  exit(0);
}
int main() {
  scanf("%d", &n);
  FOR(i, 1, n) {
    int x;
    scanf("%d", &x);
    a[i].first = x;
    a[i].second = i;
    pos[x].pb(i);
    if (pos[x].size() == 4) {
      ans[1] = pos[x][0];
      ans[2] = pos[x][1];
      ans[3] = pos[x][2];
      ans[4] = pos[x][3];
      end();
    } else if (pos[x].size() == 2) {
      p2.pb(x);
      if (p2.size() > 1) {
        int px = p2[0], py = p2[1];
        ans[1] = pos[px][0];
        ans[2] = pos[py][0];
        ans[3] = pos[px][1];
        ans[4] = pos[py][1];
        end();
      }
    }
  }
  sort(a + 1, a + n + 1);
  FOR(i, 1, n) {
    if (i > 1 && a[i].first == a[i - 1].first) continue;
    FOR(j, i + 1, n) {
      if (j > i + 1 && a[j].first == a[j - 1].first) continue;
      int s = a[i].first + a[j].first;
      if (p[s].first) {
        ans[1] = p[s].first;
        ans[2] = p[s].second;
        ans[3] = a[i].second;
        ans[4] = a[j].second;
        end();
      } else {
        p[s] = {a[i].second, a[j].second};
      }
    }
  }
  puts("NO");
  return 0;
}
