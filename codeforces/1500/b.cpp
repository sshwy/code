// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

const int N = 1e6 + 5;

int n, m;
long long k;
int a[N], b[N];
int pa[N * 2], pb[N * 2];

long long mul(
    long long a, long long b, long long p) { // 这个函数貌似目前只支持非负整数
  if (a <= 1000000000 && b <= 1000000000) return a * b % p;
  long long res = 0;
  while (b) {
    if (b & 1) res = (res + a) % p;
    a = a * 2 % p, b >>= 1;
  }
  return res;
}
long long exgcd(long long a, long long b, long long &x, long long &y) {
  if (!b) return x = 1, y = 0, a;
  long long t = exgcd(b, a % b, y, x);
  return y = y - (a / b) * x, t;
}
vector<pair<long long, long long>> vp;

pair<long long, long long> go(vector<pair<int, int>> v) {
  long long b = 0, a = 1; // x=0 mod 1
  for (auto p : v) {
    long long a1, b1, k, k1;
    a1 = p.second;
    b1 = p.first;
    long long g = exgcd(a, a1, k, k1), d = ((b1 - b) % a1 + a1) % a1;
    // printf("exgcd %lld %lld %lld %lld\n", a, a1, k, k1);
    if (d % g) return make_pair(-1, -1);
    k = mul(k, d / g, a1);
    // 然后合并方程
    b = b + a * k, a = a / g * a1, b = (b + a) % a;
  }
  return make_pair((b + a) % a, a);
}

void init() {
  FOR(i, 1, 2 * max(n, m)) if (pa[i] && pb[i]) {
    int A = (pa[i] - 1) % n;
    int B = (pb[i] - 1) % m;
    vector<pair<int, int>> v;
    v.pb({A, n});
    v.pb({B, m});
    auto p = go(v);
    if (p.first != -1 && p.second != -1) { vp.pb(p); }
  }
}
long long calc(long long d) {
  long long res = 0;
  for (auto p : vp) { res += (d - 1) / p.second + ((d - 1) % p.second >= p.first); }
  return res;
}
int main() {
  scanf("%d%d", &n, &m);
  scanf("%lld", &k);
  FOR(i, 1, n) scanf("%d", a + i), pa[a[i]] = i;
  FOR(j, 1, m) scanf("%d", b + j), pb[b[j]] = j;

  init();

  long long l = 1, r = 2e18;
  while (l < r) {
    long long mid = (l + r) >> 1;
    if (mid - calc(mid) < k)
      l = mid + 1;
    else
      r = mid;
  }
  printf("%lld\n", l);
  return 0;
}
