// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

const int SZ = 5e5 * 41;
int n, q;

int tot;
int lc[SZ], rc[SZ], tag[SZ], sz[SZ], rstat[SZ], lstat[SZ];

int newNode() {
  int u = ++tot;
  return u;
}
int root;

void applyXor(int u, int x, int dep) {
  if (dep == -1) return;
  if (!u) return;
  if (x >> dep & 1) {
    swap(lc[u], rc[u]);
    int l_to_r = lstat[u] & x;
    int r_to_l = rstat[u] & x;
    lstat[u] = (lstat[u] ^ l_to_r) | r_to_l;
    rstat[u] = (rstat[u] ^ r_to_l) | l_to_r;
  }
  tag[u] ^= x;
}

void pushup(int u, int dep) {
  sz[u] = sz[lc[u]] + sz[rc[u]];
  lstat[u] = lstat[lc[u]] | lstat[rc[u]] | ((!!lc[u]) << dep);
  rstat[u] = rstat[lc[u]] | rstat[rc[u]] | ((!!rc[u]) << dep);
  assert(sz[0] == 0);
}

void pushdown(int u, int dep) {
  if (tag[u]) {
    if (lc[u]) applyXor(lc[u], tag[u], dep - 1);
    if (rc[u]) applyXor(rc[u], tag[u], dep - 1);
    tag[u] = 0;
  }
}
void insert(int &u, int x, int dep = 19) {
  if (!u) u = newNode();
  if (dep == -1) {
    sz[u] = 1;
    return;
  }
  if (x >> dep & 1)
    insert(rc[u], x, dep - 1);
  else
    insert(lc[u], x, dep - 1);
  pushup(u, dep);
}

void split(int u, int k, int &x, int &y, int dep = 19, int val = 0) { // x: < k
  // printf("split u %d k %d dep %d val %d\n", u, k, dep, val);
  if (!u) {
    x = y = 0;
    return;
  }
  if (dep == -1) {
    if (val < k) {
      x = u, y = 0;
    } else {
      x = 0, y = u;
    }
    return;
  }
  pushdown(u, dep);
  if (val + (1 << dep) < k) { // lc belongs to x
    int tx, ty;
    split(rc[u], k, tx, ty, dep - 1, val + (1 << dep));
    x = u, lc[x] = lc[u], rc[x] = tx;
    y = newNode(), lc[y] = 0, rc[y] = ty;
  } else { // rc belongs to y
    int tx, ty;
    split(lc[u], k, tx, ty, dep - 1, val);
    y = u, lc[y] = ty, rc[y] = rc[u];
    x = newNode(), lc[x] = tx, rc[x] = 0;
  }
  pushup(x, dep), pushup(y, dep);
}

int merge(int x, int y, int dep = 19) {
  if (!x || !y) return x + y;
  if (dep == -1) {
    if (sz[x] || sz[y]) sz[x] = 1;
    return x;
  }
  pushdown(x, dep), pushdown(y, dep);
  lc[x] = merge(lc[x], lc[y], dep - 1);
  rc[x] = merge(rc[x], rc[y], dep - 1);
  pushup(x, dep);
  return x;
}

void applyAnd(int u, int x, int dep = 19) {
  if (!u || dep == -1) return;
  pushdown(u, dep);
  if (rstat[u] & (~x) & lstat[u]) {
    if (!(x >> dep & 1)) {
      lc[u] = merge(lc[u], rc[u], dep - 1);
      rc[u] = 0;
    }
    applyAnd(lc[u], x, dep - 1);
    applyAnd(rc[u], x, dep - 1);
  } else {
    int xor_x = rstat[u] & (~x);
    assert(dep > -1);
    applyXor(u, xor_x, dep);
  }
  pushup(u, dep);
}
void applyOr(int u, int x, int dep = 19) {
  if (!u || dep == -1) return;
  pushdown(u, dep);
  if (lstat[u] & x & rstat[u]) {
    if (x >> dep & 1) {
      rc[u] = merge(lc[u], rc[u], dep - 1);
      lc[u] = 0;
    }
    applyOr(lc[u], x, dep - 1);
    applyOr(rc[u], x, dep - 1);
  } else {
    int xor_x = lstat[u] & x;
    assert(dep > -1);
    applyXor(u, xor_x, dep);
  }
  pushup(u, dep);
}
void rangeApply(int t, int l, int r, int val) {
  int x, y, z;
  split(root, l, x, y);
  split(y, r + 1, y, z);
  if (t == 1) applyAnd(y, val);
  if (t == 2) applyOr(y, val);
  if (t == 3) applyXor(y, val, 19);
  root = merge(merge(x, y), z);
}
int rangeCount(int l, int r) {
  int x, y, z, res = 0;
  split(root, l, x, y);
  split(y, r + 1, y, z);
  res = sz[y];
  root = merge(merge(x, y), z);
  return res;
}

int main() {
  root = newNode();
  scanf("%d%d", &n, &q);
  FOR(i, 1, n) {
    int ai;
    scanf("%d", &ai);
    insert(root, ai);
  }
  FOR(i, 1, q) {
    int t, l, r, x;
    scanf("%d%d%d", &t, &l, &r);
    if (t == 4) {
      printf("%d\n", rangeCount(l, r));
    } else {
      scanf("%d", &x);
      rangeApply(t, l, r, x);
    }
  }

  return 0;
}
