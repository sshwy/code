// by Sshwy
//#define DEBUGGER
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

#ifdef DEBUGGER

#define log_prefix()                                                \
  {                                                                 \
    fprintf(stderr, "\033[37mLine %-3d [%dms]:\033[0m  ", __LINE__, \
        (int)clock() / 1000);                                       \
  }
#define ilog(...) \
  { fprintf(stderr, __VA_ARGS__); }
#define llog(...)      \
  {                    \
    log_prefix();      \
    ilog(__VA_ARGS__); \
  }
#define log(...)           \
  {                        \
    log_prefix();          \
    ilog(__VA_ARGS__);     \
    fprintf(stderr, "\n"); \
  }
#define red(...)       \
  {                    \
    log_prefix();      \
    ilog("\033[31m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define green(...)     \
  {                    \
    log_prefix();      \
    ilog("\033[32m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define blue(...)      \
  {                    \
    log_prefix();      \
    ilog("\033[34m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }

#else

#define log_prefix() ;
#define ilog(...) ;
#define llog(...) ;
#define log(...) ;
#define red(...) ;
#define green(...) ;
#define blue(...) ;

#endif

namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 1005;
int n;
int a[N], b[N][N], c[N];
pair<int, int> p[N];

int main() {
  cin >> n;
  FOR(i, 1, n) cin >> a[i];
  FOR(i, 1, n) p[i] = {a[i], i};
  sort(p + 1, p + n + 1, greater<pair<int, int>>());
  FOR(i, 1, n) c[p[i].se] = i;
  FOR(i, 1, n) {
    FOR(j, 0, p[i].fi - 1) { b[(i + j) % (n + 1)][i] = 1; }
  }
  // FOR(i,0,n)FOR(j,1,n)printf("%d%c",b[i][j]," \n"[j==n]);
  vector<string> ans;
  FOR(i, 0, n) {
    string s;
    bool flag = 0;
    FOR(j, 1, n) {
      s += char(b[i][c[j]] + '0');
      if (b[i][c[j]]) flag = 1;
    }
    if (flag) ans.pb(s);
  }
  cout << ans.size() << endl;
  for (auto x : ans) cout << x << endl;
  return 0;
}
