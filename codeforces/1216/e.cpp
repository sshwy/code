#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
namespace IO {
  char nc() {
    static char bf[100000], *p1 = bf, *p2 = bf;
    return p1 == p2 && (p2 = (p1 = bf) + fread(bf, 1, 100000, stdin), p1 == p2)
               ? EOF
               : *p1++;
  }
  int rd() {
    int res = 0;
    char c = getchar();
    while (!isdigit(c)) c = getchar();
    while (isdigit(c)) res = res * 10 + c - '0', c = getchar();
    return res;
  }
} // namespace IO
/******************heading******************/

lld q, k;

lld calc(lld x) { // 123...x的位数
  lld lim = 1, res = 0;
  FOR(len, 1, 20) {
    if (lim > x) break;
    lld nex = lim * 10 - 1;
    nex = min(nex, x);
    nex -= lim - 1;
    // cout<<"nex="<<nex<<endl;
    res += nex * len;
    ;

    lim *= 10;
  }
  return res;
}
lld calc2(lld x) { // 1 12 123 1234 ... 123..x 的位数
  lld lim = 1, res = 0;
  FOR(len, 1, 20) {
    if (lim > x) break;
    lld nex = lim * 10 - 1;
    nex = min(nex, x);
    // lim to nex;
    // cout<<"nex="<<nex<<endl;
    // cout<<"len="<<len<<endl;
    // cout<<"lim="<<lim<<endl;
    res += len * ((x << 1) - lim - nex + 2) * (nex - lim + 1) / 2;
    lim *= 10;
  }
  return res;
}
void go() {
  cin >> k;
  lld l = 1, r = 6e8;
  while (l < r) { //第一个大于等于k的位置
    lld mid = (l + r) >> 1;
    if (calc2(mid) >= k)
      r = mid;
    else
      l = mid + 1;
  }
  k -= calc2(l - 1);
  assert(k <= calc(l));
  lld l2 = 1, r2 = 6e8;
  while (l2 < r2) {
    lld mid = (l2 + r2) >> 1;
    if (calc(mid) >= k)
      r2 = mid;
    else
      l2 = mid + 1;
  }
  k -= calc(l2 - 1);
  lld b[50], lb = 0, y = l2;
  while (y) b[++lb] = y % 10, y /= 10;
  reverse(b + 1, b + lb + 1);
  cout << b[k] << endl;
}
int main() {
  // while(cin>>q)cout<<calc2(q)<<endl;
  cin >> q;
  FOR(i, 1, q) go();
  return 0;
}
