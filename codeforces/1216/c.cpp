#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
namespace IO {
  char nc() {
    static char bf[100000], *p1 = bf, *p2 = bf;
    return p1 == p2 && (p2 = (p1 = bf) + fread(bf, 1, 100000, stdin), p1 == p2)
               ? EOF
               : *p1++;
  }
  int rd() {
    int res = 0;
    char c = getchar();
    while (!isdigit(c)) c = getchar();
    while (isdigit(c)) res = res * 10 + c - '0', c = getchar();
    return res;
  }
} // namespace IO
/******************heading******************/

int x[10], y[10];
bool in(int a1, int a2, int b1, int b2) {
  int a = x[a1], b = y[a1], c = x[a2], d = y[a2];
  int e = x[b1], f = y[b1], g = x[b2], h = y[b2];
  if (a > c) swap(a, c);
  if (b > d) swap(b, d);
  if (e > g) swap(e, g);
  if (f > h) swap(f, h);
  return e <= a && c <= g && f <= b && d <= h;
}
bool out(int a1, int a2, int b1, int b2) {
  int a = x[a1], b = y[a1], c = x[a2], d = y[a2];
  int e = x[b1], f = y[b1], g = x[b2], h = y[b2];
  if (a > c) swap(a, c);
  if (b > d) swap(b, d);
  if (e > g) swap(e, g);
  if (f > h) swap(f, h);
  if (c < e || g < a || d < f || h < b) return 1;
  return 0;
}
bool point_in(int p, int a1, int a2) {
  int a = x[a1], b = y[a1], c = x[a2], d = y[a2];
  if (a > c) swap(a, c);
  if (b > d) swap(b, d);
  int e = x[p], f = y[p];
  return a <= e && e <= c && b <= f && f <= d;
}

int main() {
  cin >> x[1] >> y[1] >> x[2] >> y[2];
  cin >> x[3] >> y[3] >> x[4] >> y[4];
  cin >> x[5] >> y[5] >> x[6] >> y[6];
  x[7] = x[1], y[7] = y[2];
  x[8] = x[2], y[8] = y[1];
  if (in(1, 2, 3, 4) || in(1, 2, 5, 6)) {
    cout << "NO";
    return 0;
  } else if (out(3, 4, 5, 6)) {
    cout << "YES";
    return 0;
  } else {
    if ((point_in(1, 3, 4) || point_in(1, 5, 6)) &&
        (point_in(2, 3, 4) || point_in(2, 5, 6)) &&
        (point_in(7, 3, 4) || point_in(7, 5, 6)) &&
        (point_in(8, 3, 4) || point_in(8, 5, 6))) {
      cout << "NO";
      return 0;
    } else {
      cout << "YES";
      return 0;
    }
  }
  return 0;
}
