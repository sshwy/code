#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
namespace IO {
  char nc() {
    static char bf[100000], *p1 = bf, *p2 = bf;
    return p1 == p2 && (p2 = (p1 = bf) + fread(bf, 1, 100000, stdin), p1 == p2)
               ? EOF
               : *p1++;
  }
  int rd() {
    int res = 0;
    char c = getchar();
    while (!isdigit(c)) c = getchar();
    while (isdigit(c)) res = res * 10 + c - '0', c = getchar();
    return res;
  }
} // namespace IO
/******************heading******************/
const long long N = 3e5 + 50;

int q, n;
long long a[N], b[N];
long long f[N][3];

long long min(long long a, long long b) { return a < b ? a : b; }
void go() {
  cin >> n;
  FOR(i, 1, n) cin >> a[i] >> b[i];
  a[0] = -100;
  f[1][0] = 0;
  FOR(i, 1, n) {
    // f[i][0]
    if (a[i] == a[i - 1]) {
      f[i][0] = min(f[i - 1][1], f[i - 1][2]);
    } else if (a[i] == a[i - 1] + 1) {
      f[i][0] = min(f[i - 1][0], f[i - 1][2]);
    } else if (a[i] == a[i - 1] + 2) {
      f[i][0] = min(f[i - 1][0], f[i - 1][1]);
    } else {
      f[i][0] = min(min(f[i - 1][0], f[i - 1][1]), f[i - 1][2]);
    }
    // f[i][1]
    if (a[i] == a[i - 1]) {
      f[i][1] = min(f[i - 1][0], f[i - 1][2]) + b[i];
    } else if (a[i] + 1 == a[i - 1]) {
      f[i][1] = min(f[i - 1][1], f[i - 1][2]) + b[i];
    } else if (a[i] == a[i - 1] + 1) {
      f[i][1] = min(f[i - 1][0], f[i - 1][1]) + b[i];
    } else {
      f[i][1] = min(min(f[i - 1][0], f[i - 1][1]), f[i - 1][2]) + b[i];
    }
    // f[i][2]
    if (a[i] == a[i - 1]) {
      f[i][2] = min(f[i - 1][0], f[i - 1][1]) + b[i] * 2;
    } else if (a[i] + 1 == a[i - 1]) {
      f[i][2] = min(f[i - 1][0], f[i - 1][2]) + b[i] * 2;
    } else if (a[i] + 2 == a[i - 1]) {
      f[i][2] = min(f[i - 1][1], f[i - 1][2]) + b[i] * 2;
    } else {
      f[i][2] = min(min(f[i - 1][0], f[i - 1][1]), f[i - 1][2]) + b[i] * 2;
    }
  }
  cout << min(min(f[n][0], f[n][1]), f[n][2]) << endl;
}
int main() {
  ios::sync_with_stdio(false);
  cin.tie(0);
  cout.tie(0);
  cin >> q;
  FOR(i, 1, q) go();
  return 0;
}
