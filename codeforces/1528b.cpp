// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

const int P = 998244353, N = 1e6 + 5;

int n;
int f[N], g[N], d[N];

int main() {
  cin >> n;
  FOR(i, 1, n) {
    for (int j = i; j <= n; j += i) d[j]++;
  }
  FOR(i, 1, n) {
    f[i] = g[i - 1] + d[i];
    g[i] = (g[i - 1] + f[i]) % P;
  }
  printf("%d\n", f[n]);
  return 0;
}
