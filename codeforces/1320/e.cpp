// by Sshwy
//#define DEBUGGER
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

#ifdef DEBUGGER

#define log_prefix()                                                \
  {                                                                 \
    fprintf(stderr, "\033[37mLine %-3d [%dms]:\033[0m  ", __LINE__, \
        (int)clock() / 1000);                                       \
  }
#define ilog(...) \
  { fprintf(stderr, __VA_ARGS__); }
#define llog(...)      \
  {                    \
    log_prefix();      \
    ilog(__VA_ARGS__); \
  }
#define log(...)           \
  {                        \
    log_prefix();          \
    ilog(__VA_ARGS__);     \
    fprintf(stderr, "\n"); \
  }
#define red(...)       \
  {                    \
    log_prefix();      \
    ilog("\033[31m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define green(...)     \
  {                    \
    log_prefix();      \
    ilog("\033[32m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define blue(...)      \
  {                    \
    log_prefix();      \
    ilog("\033[34m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }

#else

#define log_prefix() ;
#define ilog(...) ;
#define llog(...) ;
#define log(...) ;
#define red(...) ;
#define green(...) ;
#define blue(...) ;

#endif

namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 5e5 + 5;

int n;
vector<int> g[N];
typedef pair<int, int> pii;
vector<pii> vt[N];
int dep[N], fa[N][20];
int dfn[N], totdfn;
void dfs(int u, int p) {
  dep[u] = dep[p] + 1, fa[u][0] = p, dfn[u] = ++totdfn;
  FOR(j, 1, 19) {
    fa[u][j] = fa[fa[u][j - 1]][j - 1];
    if (fa[u][j] == 0) break;
  }
  for (int v : g[u])
    if (v != p) dfs(v, u);
}
int lca(int x, int y) {
  if (dep[x] < dep[y]) swap(x, y);
  ROF(j, 19, 0) if (dep[x] - (1 << j) >= dep[y]) x = fa[x][j];
  if (x == y) return x;
  ROF(j, 19, 0) if (fa[x][j] != fa[y][j]) x = fa[x][j], y = fa[y][j];
  return fa[x][0];
}
int distance(int x, int y) { return dep[x] + dep[y] - dep[lca(x, y)] * 2; }

void VT_addpath(int u, int v, int w) {
  vt[u].pb({v, w});
  vt[v].pb({u, w});
}
int s[N], tp;
void build_VT(vector<int> &V) {
  for (int u : V) vt[u].clear();
  sort(V.begin(), V.end(), [](int x, int y) { return dfn[x] < dfn[y]; });
  V.resize(unique(V.begin(), V.end()) - V.begin());
  s[tp = 1] = lca(V.front(), V.back());
  vector<int> Vt;
  for (int u : V) {
    int z = lca(s[tp], u);
    if (u == z) continue;
    while (tp > 1 && dep[s[tp - 1]] >= dep[z])
      VT_addpath(s[tp], s[tp - 1], distance(s[tp], s[tp - 1])), --tp;
    if (s[tp] != z) VT_addpath(s[tp], z, distance(s[tp], z)), s[tp] = z, Vt.pb(z);
    s[++tp] = u;
  }
  while (tp > 1) VT_addpath(s[tp], s[tp - 1], distance(s[tp], s[tp - 1])), --tp;
  V.pb(s[1]);
  V.insert(V.end(), Vt.begin(), Vt.end());
  sort(V.begin(), V.end(), [](int x, int y) { return dfn[x] < dfn[y]; });
  V.resize(unique(V.begin(), V.end()) - V.begin());
}
int k, m;
struct virus {
  int v, s, dist, id;
  void reset() { dist = id = 1e9, s = 1, v = 0; }
  virus() { reset(); }
  int tim() const { return dist / s + !!(dist % s); }
  bool operator<(virus x) const {
    return (tim() != x.tim()) ? tim() > x.tim() : id > x.id;
  }
  bool operator>(virus x) const {
    return (tim() != x.tim()) ? tim() < x.tim() : id < x.id;
  }
  bool operator>=(virus x) const {
    return (tim() != x.tim()) ? tim() < x.tim() : id <= x.id;
  }
  virus walkto(int u, int d) const {
    virus res = *this;
    res.dist += d, res.v = u;
    return res;
  }
} vss[N];
int a[N];

priority_queue<virus> q;
virus t[N];
void go() {
  scanf("%d%d", &k, &m);
  FOR(i, 1, k)
  scanf("%d%d", &vss[i].v, &vss[i].s), vss[i].dist = 0, vss[i].id = i;
  FOR(i, 1, m) scanf("%d", &a[i]);

  vector<int> V;
  FOR(i, 1, k) V.pb(vss[i].v);
  FOR(i, 1, m) V.pb(a[i]);
  build_VT(V);
  for (int v : V) t[v].reset();

  FOR(i, 1, k) q.push(vss[i]), t[vss[i].v] = vss[i];
  while (!q.empty()) {
    virus vr = q.top();
    q.pop();
    int u = vr.v;
    log("(%d,%d,%d,%d)", vr.v, vr.dist, vr.s, vr.id);
    if (t[u] > vr) continue;
    for (pii p : vt[u]) {
      int v = p.fi, w = p.se;
      log("%d,%d", u, v);
      virus nv = vr.walkto(v, w);
      if (t[v] >= nv) continue;
      t[v] = nv;
      q.push(nv);
    }
  }
  FOR(i, 1, m) printf("%d%c", t[a[i]].id, " \n"[i == m]);

  for (int v : V) vt[v].clear();
}

int main() {
  scanf("%d", &n);
  FOR(i, 1, n - 1) {
    int x, y;
    scanf("%d%d", &x, &y);
    g[x].pb(y);
    g[y].pb(x);
  }
  dfs(1, 0);
  int q;
  scanf("%d", &q);
  FOR(i, 1, q) go();
  return 0;
}
