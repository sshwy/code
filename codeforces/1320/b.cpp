// by Sshwy
//#define DEBUGGER
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

#ifdef DEBUGGER

#define log_prefix()                                                \
  {                                                                 \
    fprintf(stderr, "\033[37mLine %-3d [%dms]:\033[0m  ", __LINE__, \
        (int)clock() / 1000);                                       \
  }
#define ilog(...) \
  { fprintf(stderr, __VA_ARGS__); }
#define llog(...)      \
  {                    \
    log_prefix();      \
    ilog(__VA_ARGS__); \
  }
#define log(...)           \
  {                        \
    log_prefix();          \
    ilog(__VA_ARGS__);     \
    fprintf(stderr, "\n"); \
  }
#define red(...)       \
  {                    \
    log_prefix();      \
    ilog("\033[31m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define green(...)     \
  {                    \
    log_prefix();      \
    ilog("\033[32m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define blue(...)      \
  {                    \
    log_prefix();      \
    ilog("\033[34m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }

#else

#define log_prefix() ;
#define ilog(...) ;
#define llog(...) ;
#define log(...) ;
#define red(...) ;
#define green(...) ;
#define blue(...) ;

#endif

namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 2e5 + 5, M = N;
vector<int> g[N];
int n, m, k;
int a[N];

int d[N], G[N];
queue<int> q;
void bfs(int x) {
  memset(d, 0x3f, sizeof(d));
  d[x] = 0;
  G[x] = 1;
  q.push(x);
  while (q.size()) {
    int u = q.front();
    q.pop();
    for (int v : g[u]) {
      if (d[v] > d[u] + 1) {
        d[v] = d[u] + 1;
        G[v]++;
        q.push(v);
      } else if (d[v] == d[u] + 1) {
        G[v]++;
      }
    }
  }
}

int main() {
  scanf("%d%d", &n, &m);
  FOR(i, 1, m) {
    int a, b;
    scanf("%d%d", &a, &b);
    g[b].pb(a);
  }
  scanf("%d", &k);
  FOR(i, 1, k) scanf("%d", &a[i]);
  bfs(a[k]);
  //最小：
  int res = 0;
  FOR(i, 1, k - 1) if (d[a[i]] - 1 != d[a[i + 1]]) res++;
  printf("%d ", res);
  res = 0;
  FOR(i, 1, k - 1) if (d[a[i]] - 1 != d[a[i + 1]] || G[a[i]] > 1) res++;
  printf("%d ", res);
  return 0;
}
