// by Sshwy
//#define DEBUGGER
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

#ifdef DEBUGGER

#define log_prefix()                                                  \
  {                                                                   \
    fprintf(stderr, "\033[37mLine %-3d [%lldms]:\033[0m  ", __LINE__, \
        (int)clock() / 1000);                                         \
  }
#define ilog(...) \
  { fprintf(stderr, __VA_ARGS__); }
#define llog(...)      \
  {                    \
    log_prefix();      \
    ilog(__VA_ARGS__); \
  }
#define log(...)           \
  {                        \
    log_prefix();          \
    ilog(__VA_ARGS__);     \
    fprintf(stderr, "\n"); \
  }
#define red(...)       \
  {                    \
    log_prefix();      \
    ilog("\033[31m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define green(...)     \
  {                    \
    log_prefix();      \
    ilog("\033[32m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define blue(...)      \
  {                    \
    log_prefix();      \
    ilog("\033[34m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }

#else

#define log_prefix() ;
#define ilog(...) ;
#define llog(...) ;
#define log(...) ;
#define red(...) ;
#define green(...) ;
#define blue(...) ;

#endif

namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
#define int long long
const int N = 2e5 + 5, X = 1e6 + 5;
int n, m, p;
typedef pair<int, int> pii;
typedef long long LL;
const LL INF = 1e16;

namespace seg {
  LL mx[X << 2], tag[X << 2];
  const int MXR = 1e6;
  void pushdown(int u) {
    if (tag[u]) {
      mx[u << 1] += tag[u];
      tag[u << 1] += tag[u];
      mx[u << 1 | 1] += tag[u];
      tag[u << 1 | 1] += tag[u];
      tag[u] = 0;
    }
  }
  void add(int L, int R, LL v, int u = 1, int l = 1, int r = MXR) {
    if (L <= l && r <= R) {
      tag[u] += v;
      mx[u] += v;
      return;
    }
    int mid = (l + r) >> 1;
    pushdown(u);
    if (L <= mid) add(L, R, v, u << 1, l, mid);
    if (mid < R) add(L, R, v, u << 1 | 1, mid + 1, r);
    mx[u] = max(mx[u << 1], mx[u << 1 | 1]);
  }
  LL query_max(int L, int R, int u = 1, int l = 1, int r = MXR) {
    if (L <= l && r <= R) return mx[u];
    int mid = (l + r) >> 1;
    pushdown(u);
    LL res = -INF;
    if (L <= mid) res = max(res, query_max(L, R, u << 1, l, mid));
    if (mid < R) res = max(res, query_max(L, R, u << 1 | 1, mid + 1, r));
    return res;
  }
} // namespace seg

pii a[N], b[N];
vector<pii> A[X];
LL B[X], mxb, mxa;

signed main() {
  scanf("%lld%lld%lld", &n, &m, &p);
  FOR(i, 1, n)
  scanf("%lld%lld", &a[i].fi, &a[i].se), mxa = max(mxa, 0ll + a[i].fi);
  FOR(i, 1, m)
  scanf("%lld%lld", &b[i].fi, &b[i].se), mxb = max(mxb, 0ll + b[i].fi);
  fill(B, B + X, -INF);
  FOR(i, 1, m) { B[b[i].fi] = max(B[b[i].fi], 0ll - b[i].se); }
  FOR(i, 1, mxb) seg::add(i, i, B[i]);
  // FOR(i,1,mxb)printf("%lld%c",B[i]," \n"[i==mxb]);

  FOR(i, 1, p) {
    int x, y, z;
    scanf("%lld%lld%lld", &x, &y, &z);
    A[x].pb({y, z});
  }
  sort(a + 1, a + n + 1);
  LL ans = -INF;
  int pos = 0;
  FOR(i, 1, mxa) {
    LL t = seg::query_max(1, mxb);
    while (pos < n && a[pos + 1].fi == i) ++pos, ans = max(ans, t - a[pos].se);
    // log("i=%lld,ans=%lld",i,ans);
    for (pii p : A[i]) {
      seg::add(p.fi + 1, mxb, p.se);
      // log("seg::add(%lld,%lld,%lld)",p.fi+1,mxb,p.se);
    }
  }
  printf("%lld\n", ans);
  return 0;
}
