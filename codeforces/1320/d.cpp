// by Sshwy
//#define DEBUGGER
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

#ifdef DEBUGGER

#define log_prefix()                                                \
  {                                                                 \
    fprintf(stderr, "\033[37mLine %-3d [%dms]:\033[0m  ", __LINE__, \
        (int)clock() / 1000);                                       \
  }
#define ilog(...) \
  { fprintf(stderr, __VA_ARGS__); }
#define llog(...)      \
  {                    \
    log_prefix();      \
    ilog(__VA_ARGS__); \
  }
#define log(...)           \
  {                        \
    log_prefix();          \
    ilog(__VA_ARGS__);     \
    fprintf(stderr, "\n"); \
  }
#define red(...)       \
  {                    \
    log_prefix();      \
    ilog("\033[31m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define green(...)     \
  {                    \
    log_prefix();      \
    ilog("\033[32m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define blue(...)      \
  {                    \
    log_prefix();      \
    ilog("\033[34m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }

#else

#define log_prefix() ;
#define ilog(...) ;
#define llog(...) ;
#define log(...) ;
#define red(...) ;
#define green(...) ;
#define blue(...) ;

#endif

namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 2e5 + 5, B1 = 17, P1 = 998244353, B2 = 19, P2 = 1e9 + 9;

int n;
char t[N];
int a[N];
int mp1[N], mp2[N];

struct hv {
  int len;            // 0的段数
  int L1, R1;         //开头结尾1的个数
  int L0, R0;         //第一段和最后一段0的个数
  int h1 = 0, h2 = 0; //除了第一段和最后一段之外的h1值
  hv() { L1 = R1 = L0 = R0 = 0, len = 0; }
  hv(int bit) {
    if (bit) {
      len = 0;
      L1 = R1 = 1;
      L0 = R0 = 0;
    } else {
      len = 1;
      L1 = R1 = 0;
      L0 = R0 = 1;
    }
  }
  void print() const {
    ilog("(len=%d, %d,%d, %d,%d, h1=%d,h2=%d)", len, L1, R1, L0, R0, h1, h2);
  }
  hv(int a, int b, int c, int d, int e, int f, int g) {
    len = a, L1 = b, R1 = c, L0 = d, R0 = e, h1 = f, h2 = g;
  }
  hv operator+(const hv x) const {
    llog("hv add: ");
    print();
    x.print();
    ilog(" = ");
    if (len == 0 && x.len == 0) {
      hv res = x;
      res.L1 = res.R1 = L1 ^ x.L1;
      res.print();
      ilog("\n");
      return res;
    } else if (len == 0) {
      hv res = x;
      res.L1 ^= R1;
      res.print();
      ilog("\n");
      return res;
    } else if (x.len == 0) {
      hv res = *this;
      res.R1 ^= x.L1;
      res.print();
      ilog("\n");
      return res;
    } else if ((R1 ^ x.L1) % 2) { //有1隔着
      int newh1 = ((h1 + (len == 1 ? 0 : R0 * 1ll * mp1[len] % P1)) % P1 +
                      (x.len == 1 ? 0
                                  : (x.L0 * 1ll * mp1[len + 1] % P1 +
                                        x.h1 * 1ll * mp1[len] % P1) %
                                        P1)) %
                  P1;
      int newh2 = ((h2 + (len == 1 ? 0 : R0 * 1ll * mp2[len] % P2)) % P2 +
                      (x.len == 1 ? 0
                                  : (x.L0 * 1ll * mp2[len + 1] % P2 +
                                        x.h2 * 1ll * mp2[len] % P2) %
                                        P2)) %
                  P2;
      if (len + x.len <= 2) newh1 = 0, newh2 = 0;
      hv res(len + x.len, L1, x.R1, L0, x.R0, newh1, newh2);
      res.print();
      ilog("\n");
      return res;
    } else if (len == 1) {
      hv res(x.len, L1, x.R1, L0 + x.L0, x.R0, x.h1, x.h2);
      if (res.len == 1) res.L0 = res.R0 = L0 + x.R0;
      res.print();
      ilog("\n");
      return res;
    } else if (x.len == 1) {
      hv res(len, L1, x.R1, L0, R0 + x.R0, h1, h2);
      if (res.len == 1) res.L0 = res.R0 = L0 + x.R0;
      res.print();
      ilog("\n");
      return res;
    } else {
      int newh1 = ((h1 + (R0 + x.L0) * 1ll * mp1[len] % P1) % P1 +
                      x.h1 * 1ll * mp1[len - 1] % P1) %
                  P1;
      int newh2 = ((h2 + (R0 + x.L0) * 1ll * mp2[len] % P2) % P2 +
                      x.h2 * 1ll * mp2[len - 1] % P2) %
                  P2;
      ilog("(newh1=%d,%d,%d)", newh1, R0 + x.L0, mp1[len]);
      if (len + x.len - 1 <= 2) newh1 = 0, newh2 = 0;
      hv res(len + x.len - 1, L1, x.R1, L0, x.R0, newh1, newh2);
      if (res.len == 1) res.L0 = res.R0 = L0 + x.R0;
      res.print();
      ilog("\n");
      return res;
    }
  }
  bool operator==(const hv &x) const {
    return len == x.len && h1 == x.h1 && h2 == x.h2 && (L1 & 1) == (x.L1 & 1) &&
           (R1 & 1) == (x.R1 & 1) && L0 == x.L0 && R0 == x.R0;
  }
};

namespace seg {
  hv h[N << 2];
  void build(int u = 1, int l = 1, int r = n) {
    if (l == r) {
      h[u] = hv(t[l] == '1');
      return;
    }
    int mid = (l + r) >> 1;
    build(u << 1, l, mid), build(u << 1 | 1, mid + 1, r);
    h[u] = h[u << 1] + h[u << 1 | 1];
    llog("h[%d](%d,%d)=", u, l, r);
    h[u].print();
    ilog("\n");
  }
  hv query(int L, int R, int u = 1, int l = 1, int r = n) {
    if (u == 1) {
      llog("query(%d,%d): ", L, R);
      FOR(i, L, R) ilog("%c", t[i]);
      ilog("\n");
    }
    if (L <= l && r <= R) return h[u];
    int mid = (l + r) >> 1;
    if (L <= mid && mid < R)
      return query(L, R, u << 1, l, mid) + query(L, R, u << 1 | 1, mid + 1, r);
    else if (L <= mid)
      return query(L, R, u << 1, l, mid);
    assert(mid < R);
    return query(L, R, u << 1 | 1, mid + 1, r);
  }
} // namespace seg
bool go() {
  int l1, l2, len;
  scanf("%d%d%d", &l1, &l2, &len);
  if (l1 == l2) return 1;
  if (a[l1 + len - 1] - a[l1 - 1] != a[l2 + len - 1] - a[l2 - 1]) return 0;
  hv X = seg::query(l1, l1 + len - 1), Y = seg::query(l2, l2 + len - 1);
  llog("X & Y: ");
  X.print(), Y.print();
  ilog("\n");
  if (X == Y) return 1;
  return 0;
}
int main() {
  scanf("%d", &n);
  scanf("%s", t + 1);
  FOR(i, 1, n) a[i] = t[i] == '1';
  FOR(i, 1, n) a[i] += a[i - 1];

  mp1[0] = 1, mp2[0] = 1;
  FOR(i, 1, n) {
    mp1[i] = mp1[i - 1] * 1ll * B1 % P1;
    mp2[i] = mp2[i - 1] * 1ll * B2 % P2;
  }

  seg::build();
  green("build down.");

  int q;
  scanf("%d", &q);
  FOR(i, 1, q) puts(go() ? "Yes" : "No");
  return 0;
}
