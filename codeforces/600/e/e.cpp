#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
#define int lld
const int N = 1e5 + 5, M = N * 2;

struct qxx {
  int nex, t;
};
qxx e[M];
int h[N], le;
void add_path(int f, int t) { e[++le] = (qxx){h[f], t}, h[f] = le; }

int n;
int col[N], sz[N];
int cnt[N], ans[N];
bool big[N];
int cnt_max, cnt_max_sum;

void add_col(int c) {
  cnt[c]++;
  if (cnt[c] > cnt_max)
    cnt_max = cnt[c], cnt_max_sum = c;
  else if (cnt[c] == cnt_max)
    cnt_max_sum += c;
}
void del_col(int c) { cnt[c]--; }

int szdfs(int u, int p = 0) {
  for (int i = h[u], v; v = e[i].t, i; i = e[i].nex)
    if (v != p) sz[u] += szdfs(v, u);
  return ++sz[u];
}
void adddfs(int u, int p = 0) {
  add_col(col[u]);
  for (int i = h[u], v; v = e[i].t, i; i = e[i].nex)
    if (v != p && !big[v]) adddfs(v, u);
}
void deldfs(int u, int p = 0) {
  del_col(col[u]);
  for (int i = h[u], v; v = e[i].t, i; i = e[i].nex)
    if (v != p) deldfs(v, u);
}
void dfs(int u, int p = 0, int keep = 0) {
  // printf("dfs(%d,%d,%d)\n",u,p,keep);
  int mx = 0, hvs = 0;
  for (int i = h[u], v; v = e[i].t, i; i = e[i].nex)
    if (v != p && sz[v] > mx) mx = sz[v], hvs = v;
  for (int i = h[u], v; v = e[i].t, i; i = e[i].nex)
    if (v != p && v != hvs) dfs(v, u, 0);
  if (hvs) dfs(hvs, u, 1);
  big[hvs] = 1, adddfs(u, p), big[hvs] = 0; // even hvs=0,no effect
  ans[u] = cnt_max_sum;
  if (keep) return;
  deldfs(u, p);
  cnt_max = cnt_max_sum = 0;
}

signed main() {
  // scanf("%d",&n);
  cin >> n;
  FOR(i, 1, n) cin >> col[i]; // scanf("%d",&col[i]);
  FOR(i, 1, n - 1) {
    int u, v;
    cin >> u >> v;
    // scanf("%d%d",&u,&v);
    add_path(u, v), add_path(v, u);
  }
  szdfs(1);
  dfs(1);
  FOR(i, 1, n) cout << ans[i] << ' '; // printf("%d ",ans[i]);
  return 0;
}
