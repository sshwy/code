// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

struct Edge {
  int u, v, w, ow;
  Edge(int _u, int _v, int _w) { u = _u, v = _v, w = ow = _w; }
  void reset() { w = ow; }
};

/**
 * Chu-Liu/Edmonds' algorithm
 * 计算有向图（允许重边、不允许自环）给定根的最小权外向生成树（最小树形图）
 * vector<Edge> buildFrom(n, r, ve): n 个点，边集是 ve，根是 r 的最小权外向生成树
 *   若无解则返回一个空的 vector
 *   要求 ve 非空
 */
template <const int N, const int M> struct DirectedMST {
  int nd[N], tnd[N], fa[N], pre[N], In[N], Time[M], totTime, onCir[N], totCir;
  vector<int> toggle[M];

  int get(int u) { return fa[u] == u ? u : fa[u] = get(fa[u]); }
  int getNode(int u) { return nd[u] == u ? u : nd[u] = getNode(nd[u]); }

  bool work(const int n, const int root, vector<Edge> &ve) {
    bool flag = false;
    fill(In, In + n + 1, -1);
    fill(onCir, onCir + n + 1, 0);
    totCir = 0;

    for (unsigned i = 0; i < ve.size(); i++) {
      int u = getNode(ve[i].u), v = getNode(ve[i].v);
      if (u == v) continue;
      if (In[v] == -1 || ve[In[v]].w > ve[i].w) In[v] = i;
    }

    FOR(i, 1, n) fa[i] = i;

    FOR(i, 1, n) if (i != root && getNode(i) == i) {
      if (In[i] == -1) return false;
      Edge e = ve[In[i]];
      int u = getNode(e.u), v = getNode(e.v);
      if (u == v) continue;
      if (get(u) == get(v)) {
        ++totCir;
        for (int z = u; z != -1; z = z == v ? -1 : getNode(ve[In[z]].u))
          onCir[z] = totCir, tnd[z] = v, Time[In[z]] = ++totTime; // assert(z);
        flag = true;
      } else {
        fa[get(u)] = get(v);
      }
    }

    for (unsigned i = 0; i < ve.size(); i++) {
      auto &e = ve[i];
      int u = getNode(e.u), v = getNode(e.v);
      if (u == v) continue;
      if (onCir[v] && onCir[v] == onCir[u]) continue;
      if (onCir[v]) toggle[i].push_back(In[v]), e.w -= ve[In[v]].w;
    }

    FOR(i, 1, n) if (onCir[i]) nd[i] = tnd[i]; // assert(getNode(i) == i);

    return flag;
  }
  vector<Edge> buildFrom(int n, int root, vector<Edge> ve) {
    assert(!ve.empty());
    vector<Edge> vt;
    FOR(i, 1, n) nd[i] = i;
    fill(Time, Time + ve.size() + 1, 0);
    totTime = 0;

    while (work(n, root, ve))
      ;

    FOR(i, 1, n) if (getNode(i) == i && i != root) {
      if (In[i] == -1) return vt; // empty
      Time[In[i]] = ++totTime;
    }
    vector<int> SortByTime(totTime + 1, -1);
    for (unsigned i = 0; i < ve.size(); i++)
      if (Time[i]) SortByTime[Time[i]] = i;

    ROF(i, totTime, 1) {
      int x = SortByTime[i];
      if (Time[x])
        for (int y : toggle[x]) Time[y] = 0;
    }

    for (unsigned i = 0; i < ve.size(); i++) {
      ve[i].reset();
      if (Time[i]) vt.push_back(ve[i]);
    }
    assert(vt.size() == n - 1);
    return vt;
  }
};

const int N = 55, M = N * N;

pair<int, int> SubstrMatchPrefix(string s, string t) { // len, pos on s
  int len = -1, pos = -1;
  for (unsigned i = 0; i < s.size(); i++) {
    unsigned j = 0;
    while (j < t.size() && i + j < s.size() && s[i + j] == t[j]) ++j;
    if ((int)(j) > len) len = j, pos = i;
  }
  return make_pair(len, pos);
}

int n;
string s[N];
int nd[N][N], tot, fa[N * N];
char ch[N * N];
pair<int, int> f[N][N];
DirectedMST<N, M> DMST;
vector<Edge> ve;
vector<int> g[N];

void ae(int u, int v, char c) {
  fa[v] = u;
  ch[v] = c;
}
void dfs(int u, int p) {
  if (p) {
    int len = f[p][u].first, pos = f[p][u].second;
    for (int i = 0; i < len; i++) { nd[u][i] = nd[p][pos + i]; }
    int las = pos + len - 1 < 0 ? fa[nd[p][0]] : nd[p][pos + len - 1];
    for (unsigned i = len; i < s[u].size(); i++) {
      ae(las, nd[u][i] = ++tot, s[u][i]);
      las = tot;
    }
  }
  for (int v : g[u]) dfs(v, u);
}
int main() {
  freopen("dictionary.in", "r", stdin);
  freopen("dictionary.out", "w", stdout);

  cin >> n;
  FOR(i, 1, n) cin >> s[i];

  int minVal = 1e9, root = -1;
  vector<Edge> minvt;

  if (n == 1) {
    root = 1;
  } else {
    FOR(i, 1, n) FOR(j, 1, n) {
      f[i][j] = SubstrMatchPrefix(s[i], s[j]);
      if (i != j) ve.pb(Edge(i, j, -f[i][j].first));
    }
    FOR(i, 1, n) {
      auto vt = DMST.buildFrom(n, i, ve);
      int sum = 0;
      for (auto e : vt) sum += e.w;
      if (sum < minVal) minVal = sum, minvt = vt, root = i;
    }
    assert(root != -1);
    for (auto e : minvt) g[e.u].pb(e.v);
  }

  ++tot;
  for (unsigned i = 0; i < s[root].size(); i++) {
    nd[root][i] = ++tot;
    if (i > 0)
      ae(nd[root][i - 1], nd[root][i], s[root][i]);
    else
      ae(1, nd[root][i], s[root][i]);
  }

  dfs(root, 0);

  printf("%d\n", tot);
  FOR(i, 1, tot) {
    if (fa[i])
      cout << fa[i] << " " << ch[i] << endl;
    else
      cout << '0' << endl;
  }
  return 0;
}
