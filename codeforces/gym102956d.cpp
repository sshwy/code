// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

const int N = 1e6 + 5;
int n, b[100];
long long a[N], f[N];

int main() {
  scanf("%d", &n);
  FOR(i, 1, n) scanf("%lld", &a[i]);

  FOR(i, 1, n) {
    FOR(j, 0, 40) {
      if (b[j]) f[i] = max(f[i], f[b[j]] + (a[b[j]] & a[i]));
      if (a[i] >> j & 1) b[j] = i;
    }
  }

  printf("%lld\n", *max_element(f + 1, f + n + 1));
  return 0;
}
