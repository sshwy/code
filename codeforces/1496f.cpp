// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

const int N = 450, P = 998244353;
int n, m;
int a[N], b[N], w[N][N], cnt[N][N];
vector<int> g[N];

int go(int u, int v) {
  FOR(i, 1, n) a[i] = w[i][u], b[i] = w[i][v];
  FOR(i, 1, n) cnt[a[i]][b[i]]++;

  int d = w[u][v];
  if (d) FOR(i, 0, d) if (cnt[i][d - i] != 1) {
      FOR(i, 1, n) cnt[a[i]][b[i]]--;
      return 0;
    }
  int res = 1;
  FOR(i, 1, n) if (a[i] + b[i] > d) {
    int t = 0;
    for (int x : g[i])
      if (w[x][i] == 1 && a[x] + 1 == a[i] && b[x] + 1 == b[i]) ++t;
    res = 1ll * res * t % P;
  }

  FOR(i, 1, n) cnt[a[i]][b[i]]--;

  return res;
}

int main() {
  scanf("%d%d", &n, &m);
  FOR(i, 1, n) FOR(j, 1, n) if (i != j) w[i][j] = n + 5;
  FOR(i, 1, m) {
    int x, y;
    scanf("%d%d", &x, &y);

    w[x][y] = w[y][x] = 1;
    g[x].pb(y);
    g[y].pb(x);
  }

  FOR(k, 1, n)
  FOR(i, 1, n) FOR(j, 1, n) w[i][j] = min(w[i][j], w[i][k] + w[k][j]);

  FOR(i, 1, n) FOR(j, 1, n) printf("%d%c", go(i, j), " \n"[j == n]);

  return 0;
}
