// by Sshwy
//#define DEBUGGER
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

#ifdef DEBUGGER

#define log_prefix()                                                \
  {                                                                 \
    fprintf(stderr, "\033[37mLine %-3d [%dms]:\033[0m  ", __LINE__, \
        (int)clock() / 1000);                                       \
  }
#define ilog(...) \
  { fprintf(stderr, __VA_ARGS__); }
#define llog(...)      \
  {                    \
    log_prefix();      \
    ilog(__VA_ARGS__); \
  }
#define log(...)           \
  {                        \
    log_prefix();          \
    ilog(__VA_ARGS__);     \
    fprintf(stderr, "\n"); \
  }
#define red(...)       \
  {                    \
    log_prefix();      \
    ilog("\033[31m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define green(...)     \
  {                    \
    log_prefix();      \
    ilog("\033[32m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define blue(...)      \
  {                    \
    log_prefix();      \
    ilog("\033[34m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }

#else

#define log_prefix() ;
#define ilog(...) ;
#define llog(...) ;
#define log(...) ;
#define red(...) ;
#define green(...) ;
#define blue(...) ;

#endif

namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 6e5 + 5;

int n, q;
char s[N];

int sa[20][N], rk[20][N], rk2[20][N], g[20][N]; // rk2:suf[i]在sa中的指针
int bin[N];
int tmp[N];
int sz;

void qsort(int j, int *A, int *B) {
  fill(bin, bin + sz + 1, 0);
  FOR(i, 1, n) bin[B[i]]++;
  FOR(i, 1, sz) bin[i] += bin[i - 1];
  ROF(i, n, 1) A[bin[B[tmp[i]]]--] = tmp[i];
}
void make() {
  sz = max(n, 26);
  FOR(i, 1, n) rk[0][i] = s[i] - 'a' + 1, tmp[i] = i;
  qsort(0, sa[0], rk[0]);
  FOR(i, 1, n) rk2[0][sa[0][i]] = i, g[0][i] = rk[0][sa[0][i]];
  for (int j = 0; (1 << j) < n; j++) {
    int len = 1 << j, *A = sa[j], *B = rk[j], *C = sa[j + 1], *D = rk[j + 1],
        *E = rk2[j + 1], *F = g[j + 1];
    int lt = 0, tot = 0;
    FOR(i, 1, n)
    if (A[i] + len > n) tmp[++lt] = A[i]; //没有第二段，即第二关键字都为0
    FOR(i, 1, n)
    if (A[i] - len >= 1) tmp[++lt] = A[i] - len; //如果存在第1段就按第一段排
    assert(lt == n);
    qsort(j + 1, C, B);
    FOR(i, 1, n) E[C[i]] = i;
    D[C[1]] = 1, tot = 1;
    FOR(i, 2, n)
    D[C[i]] =
        B[C[i - 1]] == B[C[i]] && B[C[i - 1] + len] == B[C[i] + len] ? tot : ++tot;
    FOR(i, 1, n) F[i] = D[C[i]];
  }
}

int Nex(int p, const int j, int pos) { //找到第一次匹配p的位置（排名相同）
  int p1 = rk2[j][p], *G = g[j];
  ROF(k, 19, 0)
  if (p1 - (1 << k) >= 1 && G[p1 - (1 << k)] == G[p1]) p1 -= (1 << k);
  ROF(k, 19, 0)
  if (p1 + (1 << k) <= n && G[p1 + (1 << k)] == G[p1] && sa[j][p1 + (1 << k)] < pos)
    p1 += 1 << k;
  if (pos <= sa[j][p1]) return sa[j][p1];
  if (G[p1 + 1] == G[p1]) return sa[j][p1 + 1];
  return -1;
}
int Pre(int p, const int j, int pos) {
  int p1 = rk2[j][p], *G = g[j];
  ROF(k, 19, 0)
  if (p1 + (1 << k) <= n && G[p1 + (1 << k)] == G[p1]) p1 += (1 << k);
  ROF(k, 19, 0)
  if (p1 - (1 << k) >= 1 && G[p1 - (1 << k)] == G[p1] && sa[j][p1 - (1 << k)] > pos)
    p1 -= 1 << k;
  if (pos >= sa[j][p1]) return sa[j][p1];
  if (G[p1 - 1] == G[p1]) return sa[j][p1 - 1];
  return -1;
}

struct atom { //等差数列
  int l, r, d, e;
  atom() { l = r = d = e = 0; }
  atom(int x) { l = r = x, d = 0, e = 1; }
  atom(int x, int y) { l = x, r = y, d = r - l, e = 1; }
  atom(int x, int y, int z) { l = x, r = y, d = z, e = 1; }

  bool have(int x) const {
    return e ? (l == r ? (l == x)
                       : ((x - l) % d == 0 && (l <= x && x <= r || r <= x && x <= l)))
             : 0;
  }
  int size() const { return e ? (l == r ? 1 : (r - l) / d + 1) : 0; }
  atom reverse() const { return e ? atom(r, l, -d) : atom(); }
  atom oppsite() const { return e ? atom(-l, -r, -d) : atom(); }
  atom operator+(const int x) const { return e ? atom(l + x, r + x, d) : atom(); }
  atom operator-(const int x) const { return e ? atom(l - x, r - x, d) : atom(); }
  atom operator&(const atom a) const { //等差数列交
    const atom *x = this, *y = &a;
    if (x->size() > y->size()) swap(x, y);
    if (x->size() == 0) {
      return atom();
    } else if (x->size() == 1) {
      return y->have(x->l) ? *x : atom();
    } else if (x->size() == 2) {
      int L = y->have(x->l), R = y->have(x->r);
      return (L && R) ? *x : (L ? atom(x->l) : (R ? atom(x->r) : atom()));
    } else {
      assert(d == a.d);
      return ((l - a.l) % d) ? atom() : atom(max(l, a.l), min(r, a.r), d);
    }
  }
};

int calc(int L, int R, int j) { // 2^j -> min(2^{j+1}-1,R-L)
  int mxl = min((1 << (j + 1)) - 1, R - L);
  //长度在[2^j,mxl]的最长的Border
  atom A, B;
  int pL, pR, p1, p2, p3, R1;
  pL = R - mxl + 1, pR = R - (1 << j) + 1;
  A = (p1 = Nex(L, j, pL), p1 == -1)
          ? atom()
          : ((p2 = Nex(L, j, p1 + 1), p2 == -1 || p2 > pR)
                    ? atom(p1)
                    : ((p3 = Pre(L, j, pR), p3 == -1 || p3 < pL)
                              ? atom(p1, p2)
                              : atom(p1, p3, p2 - p1)));
  pL = L, pR = L + mxl - (1 << j), R1 = R - (1 << j) + 1;
  B = (p1 = Nex(R1, j, pL), p1 == -1 || p1 > pR)
          ? atom()
          : ((p2 = Nex(R1, j, p1 + 1), p2 == -1 || p2 > pR)
                    ? atom(p1)
                    : ((p3 = Pre(R1, j, pR), p3 == -1 || p3 < pL)
                              ? atom(p1, p2)
                              : atom(p1, p3, p2 - p1)));
  A = A + ((1 << j) - L);
  A = A.oppsite();
  A = A + (R + 1);
  A = A.reverse();
  A = A & B;
  A = A + ((1 << j) - 1);
  if (A.size()) return A.r - L + 1;
  return -1;
}
void go() {
  int L, R;
  scanf("%d%d", &L, &R);
  ROF(j, 19, 0) {
    if ((1 << j) <= R - L) {
      int ans = calc(L, R, j);
      if (ans != -1) return printf("%d\n", ans), void();
    }
  }
  puts("0");
}

int main() {
  scanf("%d%d", &n, &q);
  scanf("%s", s + 1);

  make();

  FOR(i, 1, q) go();
  return 0;
}
