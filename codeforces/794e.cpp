#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 3e5 + 5;

int n, mx;
int a[N], b[N * 2 + 5], c[N * 2 + 5];
int ans[N];

int main() {
  scanf("%d", &n);
  FOR(i, 1, n) scanf("%d", &a[i]), mx = max(mx, a[i]);
  FOR(i, 1, n - 2) b[i + i + 2] = min(max(a[i], a[i + 2]), a[i + 1]);
  FOR(i, 1, n - 1) b[i + i + 1] = max(a[i], a[i + 1]);
  ROF(i, n, 1) {
    if (i < 2)
      ans[i] = mx;
    else if (i == n)
      ans[i] = b[n + 1];
    else if (i == n - 1)
      ans[i] = max(b[n], b[n + 2]);
    else
      ans[i] = max(ans[i + 2], max(b[i + 1], b[n * 2 - i + 1]));
    //[1,i],[n-i+1,n]
    // i+1,2n-i+1
    // i=n
  }
  FOR(k, 0, n - 1) printf("%d ", ans[n - k]);
  return 0;
}
