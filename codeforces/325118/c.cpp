// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

const int N = 505, K = 30;

int n, m, k;
int R[N][N], D[N][N];

int id(int x, int y) { return (x - 1) * m + (y - 1); }

unordered_map<int, int> dist[K];
set<pair<int, int>> vis;
queue<pair<int, int>> q;

bool relax(int u, int ku, int v, int w) {
  if (dist[ku + 1].find(v) == dist[ku + 1].end()) {
    dist[ku + 1][v] = w;
    return true;
  }
  if (dist[ku + 1][v] > w) {
    dist[ku + 1][v] = w;
    return true;
  }
  return false;
}

int work(int sx, int sy) {
  int res = 0x7fffffff;
  FOR(i, 0, K - 1) dist[i].clear();
  vis.clear();

  dist[0][id(sx, sy)] = 0;
  q.push({0, id(sx, sy)});
  vis.insert({0, id(sx, sy)});

  while (!q.empty()) {
    auto u = q.front();
    q.pop();
    vis.erase(u);

    int x = u.second / m + 1, y = u.second % m + 1, ku = u.first;
    int du = dist[ku][u.second];

    if (ku >= k / 2) {
      res = min(res, du * 2);
    } else {
      int dir[4][3] = {
          0, 1, R[x][y], 0, -1, R[x][y - 1], 1, 0, D[x][y], -1, 0, D[x - 1][y]};
      FOR(_, 0, 3) {
        int nx = x + dir[_][0], ny = y + dir[_][1], w = dir[_][2];
        if (nx < 1 || nx > n || ny < 1 || ny > m) continue;
        // printf("(%d, %d) => (%d, %d) w = %d\n", x, y, nx, ny, w);

        int v = id(nx, ny);
        if (relax(u.second, ku, v, du + w) && vis.find({ku + 1, v}) == vis.end()) {
          q.push(make_pair(ku + 1, v));
          vis.insert(make_pair(ku + 1, v));
        }
      }
    }
  }
  return res;
}
int main() {
  scanf("%d%d%d", &n, &m, &k);
  if (k & 1) {
    FOR(i, 1, n) FOR(j, 1, m) printf("-1%c", " \n"[j == m]);
    return 0;
  }
  FOR(i, 1, n) FOR(j, 1, m - 1) scanf("%d", &R[i][j]);
  FOR(i, 1, n - 1) FOR(j, 1, m) scanf("%d", &D[i][j]);
  FOR(i, 1, n) FOR(j, 1, m) {
    int x = work(i, j);
    printf("%d%c", x, " \n"[j == m]);
  }
  return 0;
}
