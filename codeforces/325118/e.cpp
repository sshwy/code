// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

typedef long long LL;
const int N = 2e5 + 5, W = 5e5 + 5;

int n;
int a[N];
LL pa[N], sa[N], ans;
LL pb[N], sb[N];

vector<pair<int, LL>> v;

void addQry(LL x) {
  // printf("addQry %lld\n", x);
  v.pb({1, x});
}
void addNum(LL x) {
  // printf("        addNum %lld\n", x);
  v.pb({0, x});
}

int c[W];
void add(int pos, int v) {
  for (int i = pos; i < W; i += i & -i) c[i] += v;
}
int pre(int pos) {
  int res = 0;
  for (int i = pos; i > 0; i -= i & -i) res += c[i];
  return res;
}

void work() {
  // puts("\nwork");
  int tot = 0;

  vector<LL> d;
  for (auto e : v) d.pb(e.second);
  sort(d.begin(), d.end());

  memset(c, 0, sizeof(int) * (d.size() + 3));

  for (auto e : v) {
    int v = lower_bound(d.begin(), d.end(), e.second) - d.begin() + 1;
    if (e.first == 0) {
      add(v, 1);
      tot++;
    } else {
      ans += tot - pre(v);
    }
  }
}

void go() {
  ans = 0;
  scanf("%d", &n);
  FOR(i, 1, n) scanf("%d", a + i);

  if (n == 1) return puts("1"), void();

  pa[0] = sa[n + 1] = 0;
  FOR(i, 1, n) pa[i] = pa[i - 1] + a[i];
  ROF(i, n, 1) sa[i] = sa[i + 1] + a[i];

  pb[0] = sb[n + 1] = 0;
  FOR(i, 1, n) pb[i] = a[i] - pb[i - 1];
  ROF(i, n, 1) sb[i] = a[i] - sb[i + 1];

  FOR(i, 1, n) {
    // cccppp
    if (pa[i] != sa[i + 1]) ++ans;
    // pccppp
    if (i >= 3 && i < n && pa[i] - a[1] * 2 < sa[i + 1]) ++ans;
    // cccppc
    if (i <= n - 3 && pa[i] + a[n] * 2 < sa[i + 1]) ++ans;
    // pccppc
    if (i >= 3 && i <= n - 3 && pa[i] + a[n] * 2 - a[1] * 2 < sa[i + 1]) ++ans;
  }

  printf("ans %lld\n", ans);

  // case 2: c * k pcpc..pc p * t
  v.clear();
  for (int i = 0; i <= n; i += 2) {
    // printf("i %d\n", i);
    addQry(pb[i] - sa[i + 1]);
    if (i >= 5) addQry(pb[i] - sa[i + 1] - a[1] * 2);
    if (i <= n - 3) addQry(pb[i] - sa[i + 1] + a[n] * 2);
    addNum(pb[i] - pa[i]);
  }
  work();

  printf("ans %lld\n", ans);

  v.clear();
  for (int i = 1; i <= n; i += 2) {
    addQry(pb[i] - sa[i + 1]);
    if (i >= 5) addQry(pb[i] - sa[i + 1] - a[1] * 2);
    if (i <= n - 3) addQry(pb[i] - sa[i + 1] + a[n] * 2);
    addNum(pb[i] - pa[i]);
  }
  work();

  // printf("ans %lld\n", ans);
  // case 3: cpcpc
  if (n % 2 && pb[n] != 0) ++ans;

  printf("%lld\n", ans);
}

int main() {
  int t;
  scanf("%d", &t);
  FOR(i, 1, t) go();
  return 0;
}
