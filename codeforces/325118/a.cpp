// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

int f(long long x) {
  if (!x) return 0;
  return x % 10 + f(x / 10);
}

int main() {
  int t;
  cin >> t;
  FOR(i, 1, t) {
    long long x;
    cin >> x;
    if (x % 2050)
      cout << -1 << endl;
    else
      cout << f(x / 2050) << endl;
  }
  return 0;
}
