// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

const int N = 200;

int n, m, b[N][N];
vector<pair<int, int>> v;
bool vis[N][N], ban[N][N];
int ans[N][N];

void go() {
  scanf("%d%d", &n, &m);
  v.clear();
  FOR(i, 1, n) {
    FOR(j, 1, m) {
      scanf("%d", &b[i][j]);
      v.pb({b[i][j], (i - 1) * m + (j - 1)});
      vis[i][j] = false;
      ban[i][j] = false;
    }
  }
  sort(v.begin(), v.end());
  FOR(i, 0, m - 1) {
    int t = v[i].second;
    int x = t / m + 1, y = t % m + 1;
    assert(b[x][y] == v[i].first);
    vis[x][y] = true;
    ban[x][i + 1] = true;
    ans[x][i + 1] = b[x][y];
  }
  FOR(i, 1, n) {
    FOR(j, 1, m) if (!vis[i][j]) {
      vis[i][j] = true;
      FOR(k, 1, m) if (!ban[i][k]) {
        ban[i][k] = true;
        ans[i][k] = b[i][j];
        break;
      }
    }
  }
  FOR(i, 1, n) { FOR(j, 1, m) printf("%d%c", ans[i][j], " \n"[j == m]); }
}

int main() {
  int t;
  scanf("%d", &t);
  FOR(i, 1, t) go();
  return 0;
}
