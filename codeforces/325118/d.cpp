// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

const int N = 505;
int n, p[N];
int a[N][N];

int main() {
  scanf("%d", &n);
  FOR(i, 1, n) { scanf("%d", &p[i]); }
  FOR(i, 1, n) a[i][i] = p[i];
  FOR(i, 1, n) {
    int one = -1;
    FOR(j, 0, n - i) if (a[i + j][1 + j] == 1) {
      one = j;
      break;
    }
    assert(one != -1);
    FOR(j, 0, one - 1) { a[i + j + 1][1 + j] = a[i + j][1 + j] - 1; }
    FOR(j, one + 1, n - i) { a[i + j][j] = a[i + j][1 + j] - 1; }
  }
  FOR(i, 1, n) { FOR(j, 0, n - i) a[i + j][1 + j] += i - 1; }
  FOR(i, 1, n) { FOR(j, 1, i) printf("%d%c", a[i][j], " \n"[j == i]); }
  return 0;
}
