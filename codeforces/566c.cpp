// by Sshwy
//#define DEBUGGER
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

#ifdef DEBUGGER

#define log_prefix()                                                \
  {                                                                 \
    fprintf(stderr, "\033[37mLine %-3d [%dms]:\033[0m  ", __LINE__, \
        (int)clock() / 1000);                                       \
  }
#define ilog(...) \
  { fprintf(stderr, __VA_ARGS__); }
#define llog(...)      \
  {                    \
    log_prefix();      \
    ilog(__VA_ARGS__); \
  }
#define log(...)           \
  {                        \
    log_prefix();          \
    ilog(__VA_ARGS__);     \
    fprintf(stderr, "\n"); \
  }
#define red(...)       \
  {                    \
    log_prefix();      \
    ilog("\033[31m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define green(...)     \
  {                    \
    log_prefix();      \
    ilog("\033[32m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define blue(...)      \
  {                    \
    log_prefix();      \
    ilog("\033[34m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }

#else

#define log_prefix() ;
#define ilog(...) ;
#define llog(...) ;
#define log(...) ;
#define red(...) ;
#define green(...) ;
#define blue(...) ;

#endif

namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 2e5 + 5;

struct qxx {
  int nex, t, v;
} e[N * 2];
int h[N], le = 1;
void add_path(int f, int t, int v) { e[++le] = {h[f], t, v}, h[f] = le; }

int n;
int a[N];

bool Cut[N];
int sz[N], sm[N];

void Size(int u, int p) {
  sz[u] = 1, sm[u] = 0;
  for (int i = h[u]; i; i = e[i].nex) {
    int v = e[i].t;
    if (!Cut[v] && v != p) { Size(v, u), sz[u] += sz[v], sm[u] = max(sm[u], sz[v]); }
  }
}
int Core(int u, int p, int T) {
  int res = u, mx = max(T - sz[u], sm[u]);
  for (int i = h[u]; i; i = e[i].nex) {
    int v = e[i].t;
    if (!Cut[v] && v != p) {
      int x = Core(v, u, T), y = max(T - sz[x], sm[x]);
      if (y < mx) res = x, mx = y;
    }
  }
  return res;
}
double f[N], g[N];
void calc(int u, int p, long long dist) {
  f[u] = pow(dist, 1.5) * a[u];
  for (int i = h[u]; i; i = e[i].nex) {
    const int v = e[i].t, w = e[i].v;
    if (v != p) {
      calc(v, u, dist + w);
      f[u] += f[v];
    }
  }
}
pair<int, double> Solve(int u, int p) {
  Size(u, p);
  int core = Core(u, p, sz[u]);

  calc(core, 0, 0);
  double x = f[core]; // core,x
  pair<int, double> res = {core, x};

  calc(core, 0, 1);
  memcpy(g, f, sizeof(double) * (n + 1));
  calc(core, 0, -1);

  Cut[core] = 1;
  for (int i = h[core]; i; i = e[i].nex) {
    int v = e[i].t;
    if (!Cut[v]) {
      double y = g[core] - g[v] + f[v];
      if (y < x) {
        pair<int, double> p = Solve(v, core);
        if (p.se < res.se) res = p;
        return res;
      }
    }
  }
  return res;
}

int main() {
  scanf("%d", &n);
  FOR(i, 1, n) scanf("%d", &a[i]);
  FOR(i, 1, n - 1) {
    int a, b, l;
    scanf("%d%d%d", &a, &b, &l);
    add_path(a, b, l), add_path(b, a, l);
  }
  pair<int, double> ans = Solve(1, 0);
  printf("%d %.10lf\n", ans.fi, ans.se);
  return 0;
}
