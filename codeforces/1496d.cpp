// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

const int N = 1e5 + 5;
int n, a[N], l[N], l1[N], l2[N], cnt[N], vis[N];

int main() {
  scanf("%d", &n);
  FOR(i, 1, n) {
    int x;
    scanf("%d", &x);
    a[x] = i;
  }
  FOR(i, 1, n) {
    int x = a[i];
    if (vis[x - 1]) {
      cnt[x]++;
      l[x] = max(l[x], l[x - 1] + 1);
      l1[x] = l[x - 1] + 1;
    }
    if (vis[x + 1]) {
      cnt[x]++;
      l[x] = max(l[x], l[x + 1] + 1);
      l2[x] = l[x + 1] + 1;
    }
    vis[x] = 1;
  }
  int mx1 = 0, mx2 = 0, mxi = 0;
  FOR(i, 1, n) {
    if (l[i] > mx1)
      mx1 = l[i], mx2 = 1, mxi = i;
    else if (l[i] == mx1)
      ++mx2;
  }
  if (mx2 > 1) return puts("0"), 0;
  if (cnt[mxi] == 1) return puts("0"), 0;
  if (l1[mxi] == l2[mxi] && l[mxi] % 2 == 0) return puts("1"), 0;
  puts("0");
  return 0;
}
