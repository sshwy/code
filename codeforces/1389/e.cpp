// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

int gcd(int a, int b) { return b ? gcd(b, a % b) : a; }
void go() {
  int m, d, w;
  scanf("%d%d%d", &m, &d, &w);
  int g = gcd(d - 1, w), lim = min(m, d);
  int w0 = w / g;
  int m0 = (lim - 1) % w0 + 1, m1 = (lim - 1) / w0;
  long long ans = 0;
  ans += min(w0, m0) * 1ll * (m1 + 1) * (m1 + 1);
  ans += max(0, w0 - m0) * 1ll * m1 * m1;
  ans -= lim;
  ans /= 2;
  // ans = 0;
  // FOR(i, 1, lim) FOR(j, i+1, lim) if(i*(d-1)%w == j*(d-1)%w) {
  //   printf("%d %d\n", i, j);
  //   ++ans;
  // }
  printf("%lld\n", ans);
}

int main() {
  int t;
  scanf("%d", &t);
  FOR(i, 1, t) go();
  return 0;
}
