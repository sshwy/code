// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

const int N = 2e5 + 5;

struct range {
  int l, r, t;
} a[N];
int d[N * 2], ld;
bool cmp(range x, range y) { return x.r < y.r; }

int main() {
  scanf("%d", &n);
  FOR(i, 1, n) {
    int l, r, t;
    scanf("%d%d%d", &l, &r, &t);
    d[++ld] = l;
    d[++ld] = r;
    a[i] = {l, r, t};
  }
  sort(d + 1, d + ld + 1);
  ld = unique(d + 1, d + ld + 1) - d - 1;
  FOR(i, 1, n) {
    a[i].l = lower_bound(d + 1, d + ld + 1, a[i].l) - d;
    a[i].r = lower_bound(d + 1, d + ld + 1, a[i].r) - d;
  }
  sort(a + 1, a + n + 1, cmp);

  int pos = 0;
  FOR(i, 1, ld) {}

  return 0;
}
