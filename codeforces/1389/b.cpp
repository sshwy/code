// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

const int N = 1e5 + 5, K = 10;
int n, k, z, a[N];
int f[N][K];

void go() {
  scanf("%d%d%d", &n, &k, &z);
  int ans = 0;
  FOR(i, 1, n) scanf("%d", a + i);
  FOR(i, 0, z) { // i left
    if (i * 2 <= k) {
      int rest = k - i * 2, s = 0, mx = -1;
      FOR(j, 1, rest + 1) {
        s += a[j];
        if (j < n) mx = max(mx, a[j] + a[j + 1]);
      }
      s += mx * i;
      ans = max(ans, s);
    }
  }
  printf("%d\n", ans);
}

int main() {
  int t;
  scanf("%d", &t);
  FOR(i, 1, t) go();
  return 0;
}
