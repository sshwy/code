// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

int main() {
  int t;
  scanf("%d", &t);
  FOR(i, 1, t) {
    int l, r;
    scanf("%d%d", &l, &r);
    if (l * 2 > r)
      puts("-1 -1");
    else
      printf("%d %d\n", l, l * 2);
  }
  return 0;
}
