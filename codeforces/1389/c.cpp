// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

const int L = 2e5 + 5;
char s[L];

void go() {
  scanf("%s", s);
  int n = strlen(s), ans = 0;
  FOR(x, 0, 9) {
    int len = 0;
    FOR(i, 0, n - 1) {
      if (s[i] - '0' == x) ++len;
    }
    ans = max(ans, len);
  }
  FOR(x, 0, 9) FOR(y, x + 1, 9) {
    int cur = -1, len = 0;
    FOR(i, 0, n - 1) {
      if (s[i] - '0' == x && cur != x) cur = x, ++len;
      if (s[i] - '0' == y && cur != y) cur = y, ++len;
    }
    ans = max(ans, len / 2 * 2);
  }
  printf("%d\n", n - ans);
}

int main() {
  int t;
  scanf("%d", &t);
  FOR(i, 1, t) go();
  return 0;
}
