// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

const int N = 2e5 + 5;
int n, k;
int l1, l2, r1, r2;

long long c1(int a, int b) { // 先做first步，然后可以获得second步的利润
  if (k <= b) return a + k;
  long long res = 0;
  if (a < b) {
    long long cnt = min(n, k / b);
    res += cnt * (a + b);
    k -= cnt * b;
    if (k == 0) return res;
    if (cnt < n)
      res += min(a + k, k * 2);
    else
      res += k * 2;
    return res;
  } else {
    res += a + b, k -= b;
    res += k * 2;
    return res;
  }
}
long long c2(int x) {
  if (k <= n * 1ll * x) return k;
  long long res = 0;
  res += n * 1ll * x;
  k -= n * 1ll * x;
  res += k * 2;
  return res;
}
void go() {
  long long ans = 0;
  scanf("%d%d", &n, &k);
  scanf("%d%d%d%d", &l1, &r1, &l2, &r2);
  if (l1 > l2) swap(l1, l2), swap(r1, r2);
  if (r1 <= l2) {
    ans = c1(l2 - r1, r2 - l1);
  } else if (r1 <= r2) {
    if (k - n * 1ll * (r1 - l2) <= 0) return puts("0"), void();
    k -= n * (r1 - l2);
    ans = c2(r2 - r1 + l2 - l1);
  } else {
    if (k - n * 1ll * (r2 - l2) <= 0) return puts("0"), void();
    k -= n * (r2 - l2);
    ans = c2(r1 - r2 + l2 - l1);
  }
  printf("%lld\n", ans);
}

int main() {
  int t;
  scanf("%d", &t);
  FOR(i, 1, t) go();
  return 0;
}
