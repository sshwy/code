// by Sshwy
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/

template <const int SZ, class T = int, const T initValue = 0, const int LogSZ = 30>
class fenwick {
private:
  T c[SZ], sum;

public:
  fenwick() { fill(c, c + SZ, initValue), sum = initValue; }
  T &operator[](int x) { return c[x]; }
  void add(int pos, T v) {
    for (int i = pos; i < SZ; i += i & -i) c[i] = c[i] + v;
    sum = sum + v;
  }
  void add(int pos, T v, T addFunc(T, T)) {
    for (int i = pos; i < SZ; i += i & -i) c[i] = addFunc(c[i], v);
    sum = addFunc(sum, v);
  }
  T prefixSum(int pos) {
    T res = initValue;
    for (int i = pos; i > 0; i -= i & -i) res += c[i];
    return res;
  }
  T prefixSum(int pos, T addFunc(T, T)) {
    T res = initValue;
    for (int i = pos; i > 0; i -= i & -i) res = addFunc(res, c[i]);
    return res;
  }
  T suffixSum(int pos) { return sum - prefixSum(pos - 1); }
  int lowerBound(const T sum) {
    int pos = 0;
    T pre = initValue;
    ROF(j, LogSZ, 0)
    if (pos + (1 << j) < SZ && pre + c[pos + (1 << j)] < sum)
      pre = pre + c[pos + (1 << j)], pos += 1 << j;
    return pos + 1;
  }
  int lowerBound(
      const T sum, bool cmp(T, T)) { // lowerBound: cmp = <,  upperBound: cmp = <=
    int pos = 0;
    T pre = initValue;
    ROF(j, LogSZ, 0)
    if (pos + (1 << j) < SZ && cmp(pre + c[pos + (1 << j)], sum))
      pre = pre + c[pos + (1 << j)], pos += 1 << j;
    return pos + 1;
  }
};

const int N = 2e5 + 5;
fenwick<N> Pos;
long long sum, rv;

long long f(long long n) {
  int x = n / 2;
  if (n % 2 == 1) {
    return (x + 1ll) * x;
  } else {
    return x * 1ll * x;
  }
}

int n;
int a[N], pos[N];
long long ans[N];

int main() {
  scanf("%d", &n);
  FOR(i, 1, n) {
    scanf("%d", &a[i]);
    pos[a[i]] = i;
  }
  Pos.add(pos[1], 1);
  ans[1] = 0;
  FOR(i, 2, n) {
    // printf("\033[32mi=%d\033[0m\n",i);
    if (i % 2 == 0) {
      int midPos = Pos.lowerBound(i / 2);
      if (pos[i] < midPos) {
        sum -= pos[i];
        sum += midPos;
      } else {
        sum += pos[i];
        sum -= midPos;
      }
    } else {
      int L = Pos.lowerBound((i - 1) / 2), R = Pos.lowerBound((i - 1) / 2 + 1);
      // printf("L=%d,R=%d\n",L,R);
      if (pos[i] < L) {
        sum -= pos[i];
        sum += L;
      } else if (R < pos[i]) {
        sum -= R;
        sum += pos[i];
      }
    }
    rv += Pos.suffixSum(pos[i]);
    Pos.add(pos[i], 1);
    // printf("sum=%d\n",sum);
    // printf("f(%d)=%lld\n",i,f(i));
    ans[i] = sum - f(i) + rv;
  }
  FOR(i, 1, n) printf("%lld%c", ans[i], " \n"[i == n]);
  return 0;
}
