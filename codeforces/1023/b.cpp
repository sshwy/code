#include <bits/stdc++.h>
#define FOR(a, b, c) for (int a = b; a <= int(c); ++a)
#define ROF(a, b, c) for (int a = b; a >= int(c); --a)
#define pb push_back
using namespace std;

int main() {
  long long n, k;
  cin >> n >> k;
  n = min(n, k - 1);
  if (n + (n - 1) < k) return cout << 0 << endl, 0;
  long long x = k - n;
  long long ans = (n - x + 1) / 2;
  cout << ans << endl;
  return 0;
}