#include <bits/stdc++.h>
#define FOR(a, b, c) for (int a = b; a <= int(c); ++a)
#define ROF(a, b, c) for (int a = b; a >= int(c); --a)
#define pb push_back
using namespace std;
const int N = 2e5 + 5;
int n, k;
char s[N];
vector<int> v;
bool use[N];

int main() {
  cin >> n >> k;
  cin >> (s + 1);
  FOR(i, 1, n) {
    if (s[i] == '(') {
      v.pb(i);
    } else {
      if (k) {
        assert(v.size());
        use[v.back()] = use[i] = 1;
        v.pop_back();
        k -= 2;
      }
    }
  }
  FOR(i, 1, n) if (use[i]) cout << s[i];
  return 0;
}