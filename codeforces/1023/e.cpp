#include <bits/stdc++.h>
#define FOR(a, b, c) for (int a = b; a <= int(c); ++a)
#define ROF(a, b, c) for (int a = b; a >= int(c); --a)
#define pb push_back
using namespace std;

int n;
string ans, rv;

bool qry(int a, int b, int c, int d) {
  assert(a <= c && b <= d && c - a + d - b >= n - 1);
  cout << "? " << a << " " << b << " " << c << " " << d << endl << flush;
  string res;
  cin >> res;
  return res == "YES";
}
int main() {
  cin >> n;
  int x = 1, y = 1;
  while (x != n || y != n) {
    assert(x <= n && y <= n);
    if (n - x + n - y == n - 1) break;
    if (x < n) { // down first
      if (qry(x + 1, y, n, n))
        ans.pb('D'), ++x;
      else
        ans.pb('R'), ++y;
    } else {
      assert(y < n && qry(x, y + 1, n, n));
      ans.pb('R'), ++y;
    }
  }
  if (x == n && y == n) cout << "! " << ans << endl << flush;
  int X = n, Y = n;
  // printf("x %d y %d\n",x,y);
  while (X > x || Y > y) {
    // Right first
    // printf("X %d Y %d\n",X,Y);
    if (y < Y) {
      if (qry(1, 1, X, Y - 1))
        rv.pb('R'), --Y;
      else
        rv.pb('D'), --X;
    } else {
      assert(x < X && qry(1, 1, X - 1, Y));
      rv.pb('D'), --X;
    }
  }
  assert(x == X && y == Y);
  reverse(rv.begin(), rv.end());
  ans += rv;
  cout << "! " << ans << endl << flush;
  return 0;
}