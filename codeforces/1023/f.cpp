#include <bits/stdc++.h>
#define FOR(a, b, c) for (int a = b; a <= int(c); ++a)
#define ROF(a, b, c) for (int a = b; a >= int(c); --a)
#define pb push_back
using namespace std;
const int N = 5e5 + 5;
int n, k, m;

int f[N];

int get(int u) { return f[u] == u ? u : f[u] = get(f[u]); }

bool vis[N];
int tag[N], my[N], use[N], fa[N], dep[N];

struct qxx {
  int nex, t, v;
} e[N * 2];
int h[N], le = 1;
void add_path(int f, int t, int v) { e[++le] = {h[f], t, v}, h[f] = le; }
struct atom {
  int a, b, c;
};
vector<atom> v;

void dfs(int u, int p) {
  // printf("dfs %d %d\n",u,p);
  fa[u] = p;
  dep[u] = dep[p] + 1;
  for (int i = h[u]; i; i = e[i].nex) {
    int v = e[i].t;
    if (v != p) {
      if (e[i].v == 1) {
        my[v] = 1;
        // printf("my %d\n",v);
      }
      dfs(v, u);
    }
  }
}
int main() {
  scanf("%d%d%d", &n, &k, &m);
  FOR(i, 1, n) { f[i] = i; }
  FOR(i, 1, k) {
    int x, y;
    scanf("%d%d", &x, &y);
    add_path(x, y, 1);
    add_path(y, x, 1);
    f[get(x)] = get(y);
  }
  FOR(i, 1, m) {
    int x, y, z;
    scanf("%d%d%d", &x, &y, &z);
    if (get(x) != get(y)) {
      add_path(x, y, 0);
      add_path(y, x, 0);
      f[get(x)] = get(y);
    } else {
      v.pb({x, y, z});
    }
  }
  FOR(i, 1, n) if (!dep[i]) dfs(i, 0);
  FOR(i, 0, n) f[i] = i;
  long long ans = 0;
  for (auto x : v) {
    int u = x.a, v = x.b, w = x.c;
    // printf("%d %d %d\n",u,v,w);
    while (1) {
      u = get(u), v = get(v);
      if (u == v) break;
      if (dep[u] < dep[v]) swap(u, v);
      // printf("use %d\n",u);
      use[u] = 1;
      if (my[u]) ans += w;
      f[u] = fa[u];
    }
  }
  FOR(i, 1, n) if (my[i] && use[i] == 0) {
    puts("-1");
    return 0;
  }
  printf("%lld\n", ans);
  return 0;
}