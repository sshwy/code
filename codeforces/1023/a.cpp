#include <bits/stdc++.h>
#define FOR(a, b, c) for (int a = b; a <= int(c); ++a)
#define ROF(a, b, c) for (int a = b; a >= int(c); --a)
#define pb push_back
using namespace std;
const int N = 2e5 + 5;
char s[N], t[N];
int n, m;
int main() {
  cin >> n >> m;
  cin >> (s + 1) >> (t + 1);
  bool x = 1, y = 1, z = 0;
  FOR(i, 1, n) {
    if (s[i] == '*') {
      z = 1;
      break;
    }
    if (s[i] != t[i]) {
      x = 0;
      break;
    }
  }
  FOR(i, 1, n) {
    if (s[n - i + 1] == '*') break;
    if (s[n - i + 1] != t[m - i + 1]) {
      y = 0;
      break;
    }
  }
  if (z == 0) {
    if (x && n == m)
      cout << "YES" << endl;
    else
      cout << "NO" << endl;
  } else {
    if (x && y && n - 1 <= m)
      cout << "YES" << endl;
    else
      cout << "NO" << endl;
  }
  return 0;
}