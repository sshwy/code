#include <bits/stdc++.h>
#define FOR(a, b, c) for (int a = b; a <= int(c); ++a)
#define ROF(a, b, c) for (int a = b; a >= int(c); --a)
#define pb push_back
using namespace std;
const int N = 2e5 + 5;
int n, q;
int a[N], b[N];
vector<int> v[N];

void add(int pos, int v) {
  for (int i = pos; i < N; i += i & -i) b[i] += v;
}
int pre(int pos) {
  int res = 0;
  for (int i = pos; i > 0; i -= i & -i) res += b[i];
  return res;
}
int qry(int l, int r) { return pre(r) - pre(l - 1); }

int main() {
  scanf("%d%d", &n, &q);
  FOR(i, 1, n) {
    scanf("%d", &a[i]);
    v[a[i]].pb(i);
  }
  FOR(i, 1, q) {
    if (v[i].size()) {
      int l = v[i].front();
      int r = v[i].back();
      if (qry(l, r) > 0) {
        puts("NO");
        return 0;
      }
      for (auto x : v[i]) add(x, 1);
    }
  }
  if (v[q].size() == 0) {
    if (v[0].size() == 0) {
      puts("NO");
      return 0;
    } else {
      a[v[0].back()] = q;
      v[0].pop_back();
    }
  }
  FOR(i, 2, n) if (a[i] == 0) a[i] = a[i - 1];
  ROF(i, n - 1, 1) if (a[i] == 0) a[i] = a[i + 1];
  FOR(i, 1, n) if (a[i] == 0) a[i] = q;
  puts("YES");
  FOR(i, 1, n) printf("%d%c", a[i], " \n"[i == n]);
  return 0;
}