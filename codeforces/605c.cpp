// by Sshwy
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 1e5 + 5;
const long double eps = 1e-15;

int n;
int p, q;
int a[N], b[N];

long double f(int i, long double A) { // a[i]*A+b[i]*B <= 1
  return (1 - a[i] * A) / b[i];
}
long double calc(long double A) { // Ap + Bq
  long double B = 1e18;
  FOR(i, 1, n) B = min(B, f(i, A));
  return A * p + B * q;
}
int main() {
  cin >> n >> p >> q;
  FOR(i, 1, n) cin >> a[i] >> b[i];

  long double l = 0, r = 1e10, mid;
  FOR(i, 1, n) r = min(r, (long double)1.0 / a[i]);
  while (r - l > eps) {
    mid = (r - l) / 3;
    // printf("mid=%.10Lf\n",mid);
    // printf("l=%.10Lf,r=%.10Lf\n",l,r);
    if (calc(l + mid) + eps > calc(r - mid)) {
      r -= mid;
    } else {
      l += mid;
    }
  }
  // printf("l=%.10Lf\n",l);

  printf("%.8lf\n", (double)calc(l));

  return 0;
}
