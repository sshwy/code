// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

using namespace RA;

void go() {
  int n = rnd(1, 8);
  printf("%d\n", n);
  int h[1000];
  FOR(i, 1, n) {
    if (i == 1)
      h[i] = 0;
    else
      h[i] = rnd(h[i - 1], n - 1);
    printf("%d%c", h[i], " \n"[i == n]);
  }
}
int main() {
  srand(clock() + time(0));
  int t = 1000;
  printf("%d\n", t);
  FOR(i, 1, t) go();
  return 0;
}
