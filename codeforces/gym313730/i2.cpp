// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

void go() {
  int n;
  scanf("%d", &n);
  int h[1000], a[1000];
  FOR(i, 1, n) scanf("%d", h + i);
  FOR(i, 1, n) a[i] = i;
  int ans = 0;
  do {
    int mx = a[1], mn = a[1];
    bool flag = true;
    FOR(i, 1, n) {
      mx = max(mx, a[i]);
      mn = min(mn, a[i]);
      if (mx - mn != h[i]) {
        flag = false;
        break;
      }
    }
    if (flag) ++ans;
  } while (next_permutation(a + 1, a + n + 1));
  printf("%d\n", ans);
}
int main() {
  int t;
  scanf("%d", &t);
  FOR(i, 1, t) go();
  return 0;
}
