// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

const int L = 2e6 + 5;
const char w[] = "harbin";
char s[L];
int stat[7];

int calc() {
  int cnt[30] = {0};
  for (int i = 0; s[i]; ++i) cnt[s[i] - 'a']++;
  int res = 0;
  FOR(i, 0, 5) if (cnt[w[i] - 'a']) res |= 1 << i;
  return res;
}
void go() {
  FOR(i, 1, 6) {
    scanf("%s", s);
    stat[i] = calc();
  }
  int p[7];
  FOR(i, 1, 6) p[i] = i;
  do {
    bool flag = true;
    FOR(i, 1, 6) { flag &= stat[p[i]] >> (i - 1) & 1; }
    if (flag) {
      puts("Yes");
      return;
    }
  } while (next_permutation(p + 1, p + 6 + 1));
  puts("No");
}
int main() {
  int t;
  scanf("%d", &t);
  FOR(i, 1, t) go();
  return 0;
}
