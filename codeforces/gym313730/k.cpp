// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

const int N = 1e6 + 5;
int w[N];
double s;
void go() {
  int n, k;
  scanf("%d%d", &n, &k);
  s = 0;
  FOR(i, 1, n) scanf("%d", w + i), s += w[i];
  FOR(i, 1, n) printf("%.9lf%c", (k / s + 1) * w[i], " \n"[i == n]);
}
int main() {
  int t;
  scanf("%d", &t);
  FOR(i, 1, t) go();
  return 0;
}
