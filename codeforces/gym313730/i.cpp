// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

const int N = 1e5 + 5, P = 1e9 + 7;
int n, h[N], pre[N], fac[N], fnv[N];

int pw(int a, int m) {
  int res = 1;
  while (m) m & 1 ? res = 1ll * res * a % P : 0, a = 1ll * a * a % P, m >>= 1;
  return res;
}
int binom(int a, int b) {
  // printf("binom %d %d\n", a,b);
  if (a < b) return 0;
  return fac[a] * 1ll * fnv[b] % P * fnv[a - b] % P;
}
int calc(int i) {
  if (i == n && h[i] != n - 1) return 0;
  // printf("i %d pre %d\n",i,pre[i]);
  if (i == 1) {
    if (h[i] == 0) return 1;
    return 0;
  }
  int j = pre[i];
  if (j == 0) return 0;
  // printf("> i %d pre %d\n",i, pre[i]);
  int res =
      binom(h[i] - j, i - j - 1) * 1ll * fac[i - j - 1] % P * 2ll % P * calc(j) % P;
  // printf("calc %d : %d\n",i, res);
  return res;
}
void go() {
  scanf("%d", &n);
  fac[0] = 1;
  FOR(i, 1, n) fac[i] = fac[i - 1] * 1ll * i % P;
  fnv[n] = pw(fac[n], P - 2);
  ROF(i, n, 1) fnv[i - 1] = fnv[i] * 1ll * i % P;

  FOR(i, 1, n) scanf("%d", h + i);
  FOR(i, 1, n) {
    if (i == 1)
      pre[i] = 0;
    else {
      if (h[i] == h[i - 1])
        pre[i] = pre[i - 1];
      else
        pre[i] = i - 1;
      if (h[i] < h[i - 1]) {
        puts("0");
        return;
      }
    }
    if (h[i] > n - 1) {
      puts("0");
      return;
    }
  }
  int ans = calc(n);
  printf("%d\n", ans);
}
int main() {
  int t;
  scanf("%d", &t);
  FOR(i, 1, t) go();
  return 0;
}
