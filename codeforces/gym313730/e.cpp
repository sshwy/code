// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

const int N = 1e6 + 5;

pair<int, long long> A[N];
int la;
struct atom {
  vector<int> v;
  int x, y, typ;
  void read() {
    scanf("%d", &typ);
    if (typ == 1) {
      int n;
      scanf("%d", &n);
      FOR(i, 1, n) {
        int x;
        scanf("%d", &x);
        v.pb(x);
      }
    } else {
      scanf("%d%d", &x, &y);
    }
  }
  void work(long long val) {
    for (auto x : v) A[++la] = make_pair(x, val);
  }
} s[N];
long long f[N];

void go() {
  int n;
  la = 0;
  scanf("%d", &n);
  FOR(i, 1, n) s[i].read();
  fill(f, f + n + 1, 0);
  f[n] = 1;
  ROF(i, n, 1) {
    if (s[i].typ == 2) {
      f[s[i].x] += f[i];
      f[s[i].y] += f[i];
    } else {
      s[i].work(f[i]);
    }
  }
  FOR(i, 1, n) s[i].v.clear();

  sort(A + 1, A + la + 1);
  long long tot = 0;
  FOR(i, 1, la) tot += A[i].second;
  int cur = -1;
  long long cnt = 0;
  long long ans = 0;
  FOR(i, 1, la) {
    if (cur != A[i].first) {
      if (cur != -1) {
        if (cnt * 2 > tot) ans = cnt * 2 - tot;
      }
      cur = A[i].first;
      cnt = A[i].second;
    } else {
      cnt += A[i].second;
    }
  }
  if (cur != -1) {
    if (cnt * 2 > tot) ans = cnt * 2 - tot;
  }
  ans = tot - ans;
  printf("%lld\n", ans);
}
int main() {
  int t;
  scanf("%d", &t);
  FOR(i, 1, t) go();
  return 0;
}
