// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

const int N = 1e9 + 5;
int n;

int main() {
  scanf("%d", &n);
  FOR(i, 1, n) {
    int x;
    scanf("%d", &x);
    if (x <= 5)
      printf("-1\n");
    else if (x % 2 == 0)
      printf("%d %d\n", 2, x - 2);
    else
      printf("%d %d\n", 3, x - 3);
  }
  return 0;
}
