#include <bits/stdc++.h>
#define ll long long
using namespace std;
inline ll read() {
  ll x = 0, f = 1;
  char ch = getchar();
  while (!isdigit(ch)) {
    if (ch == '-') f = -1;
    ch = getchar();
  };
  while (isdigit(ch)) {
    x = (x << 1) + (x << 3) + ch - 48;
    ch = getchar();
  };
  return x * f;
}
ll n, m, g[21][100201], Q[1400021], F[1400022], S[1400201];
inline void FWTxor(ll *a, int id, int lim) {
  for (int len = 1; len < lim; len <<= 1) {
    for (int i = 0; i < lim; i += len << 1) {
      ll *a1 = a + i, *a2 = a1 + len;
      for (int j = 0; j < len; j++) {
        ll x = a1[j], y = a2[j];
        a1[j] = (x + y) / (id == 1 ? 1 : 2);
        a2[j] = (x - y) / (id == 1 ? 1 : 2);
      }
    }
  }
}
inline int getch() { //读入单个数字
  char s = getchar();
  while (!isdigit(s)) s = getchar();
  return s - '0';
}
int main() {
  n = read(), m = read();
  for (int i = 1; i <= n; i++) {
    for (int j = 1; j <= m; j++) { g[i][j] = getch(); }
  }
  for (int i = 1; i <= m; i++) {
    for (int j = 1; j <= n; j++) {
      S[i] |= (1 << j - 1) * g[j][i]; //预处理S、
    }
  }
  for (int i = 0; i <= (1 << n); i++) { Q[i] = Q[i >> 1] + (i & 1); }
  for (int i = 0; i <= (1 << n); i++) {
    Q[i] = min(Q[i], n - Q[i]); //预处理Q
  }
  for (int i = 1; i <= m; i++) {
    F[S[i]]++; //预处理F
  }
  FWTxor(F, 1, 1 << n);
  FWTxor(Q, 1, 1 << n);
  for (int i = 0; i < (1 << n); i++) F[i] *= Q[i]; // FWT优化
  FWTxor(F, -1, 1 << n);
  ll x = 999999999;
  for (int i = 0; i < (1 << n); i++) x = min(F[i], x);
  cout << x << endl;
}
