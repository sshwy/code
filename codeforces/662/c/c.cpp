#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int M = 1e5 + 5, N = 20, P = 998244353;

int n, nn, m;
char s[M];
int a[M], f[1 << N], g[1 << N], h[1 << N];
/*
 * g:状态为i的个数
 *
 */

int pw(int a, int m, int p) {
  int res = 1;
  while (m) m & 1 ? res = 1ll * res * a % p : 0, a = 1ll * a * a % p, m >>= 1;
  return res;
}
void fwt(int *f, int len, int tag) {
  for (int j = 1; j < len; j <<= 1)
    for (int i = 0; i < len; i += j << 1)
      for (int k = i, x, y; k < i + j; k++)
        x = f[k], y = f[k + j], f[k] = (x + y) % P, f[k + j] = (x - y) % P;
  int ilen = pw(len, P - 2, P);
  if (tag == -1) FOR(i, 0, len - 1) f[i] = 1ll * f[i] * ilen % P;
  FOR(i, 0, len - 1) f[i] += f[i] < 0 ? P : 0;
}
int main() {
  scanf("%d%d", &n, &m);
  nn = 1 << n;
  FOR(i, 0, n - 1) {
    scanf("%s", s + 1);
    FOR(j, 1, m) a[j] |= (s[j] - '0') << i;
  }
  FOR(i, 1, m) g[a[i]]++;
  FOR(i, 0, nn - 1)
  f[i] = min(__builtin_popcount(i), __builtin_popcount(i ^ (nn - 1)));
  fwt(f, nn, 1);
  fwt(g, nn, 1);
  FOR(i, 0, nn - 1) f[i] = 1ll * f[i] * g[i] % P;
  fwt(f, nn, -1);
  int ans = 0x3f3f3f3f;
  FOR(i, 0, nn - 1) ans = min(ans, f[i]);
  printf("%d\n", ans);
  return 0;
}
