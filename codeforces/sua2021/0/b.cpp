// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

const int N = 2e5 + 5;
int n, k, a[N], vis[N], pos[N], ans[N], use[N], la;
typedef pair<int, int> pii;
pii c[N << 2];
pii operator+(pii x, pii y) { return min(x, y); }

void build(int u, int l, int r) {
  if (l == r) {
    c[u] = {a[l], l};
    return;
  }
  int mid = (l + r) >> 1;
  build(u << 1, l, mid);
  build(u << 1 | 1, mid + 1, r);
  c[u] = c[u << 1] + c[u << 1 | 1];
}
void assign(int pos, int v, int u, int l, int r) {
  if (l == r) {
    c[u].first = v;
    return;
  }
  int mid = (l + r) >> 1;
  if (pos <= mid)
    assign(pos, v, u << 1, l, mid);
  else
    assign(pos, v, u << 1 | 1, mid + 1, r);
  c[u] = c[u << 1] + c[u << 1 | 1];
}
pii getMin(int L, int R, int u, int l, int r) {
  if (L <= l && r <= R) return c[u];
  int mid = (l + r) >> 1;
  pii res(1e9, 1e9);
  if (L <= mid) res = res + getMin(L, R, u << 1, l, mid);
  if (mid < R) res = res + getMin(L, R, u << 1 | 1, mid + 1, r);
  return res;
}
int findMinPos(int L, int R) {
  pii p;
  do {
    p = getMin(L, R, 1, 1, n);
    if (vis[p.first])
      assign(p.second, n + 1, 1, 1, n);
    else
      break;
  } while (1);
  // printf("p: %d %d\n", p.first, p.second);
  return p.first > n ? -1 : p.second;
}

int main() {
  scanf("%d%d", &n, &k);
  FOR(i, 1, n) scanf("%d", a + i);
  ROF(i, n, 1) {
    if (!vis[a[i]]) {
      vis[a[i]] = true;
      pos[i] = true;
    }
  }
  build(1, 1, n);
  FOR(i, 1, n) vis[i] = false;
  int las = 0;
  FOR(i, 1, n) if (pos[i]) {
    // printf("pos %d\n", i);
    while (vis[a[i]] == false) {
      int p = findMinPos(las + 1, i);
      // printf("find %d %d = %d\n", las+1, i, p);
      if (p != -1 && a[p] <= a[i]) {
        use[p] = true;
        vis[a[p]] = true;
        las = p;
      } else
        break;
    }
  }
  FOR(i, 1, n) if (use[i]) ans[++la] = a[i];
  FOR(i, 1, la) printf("%d%c", ans[i], " \n"[i == la]);
  return 0;
}
