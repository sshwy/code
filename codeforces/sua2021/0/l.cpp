// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

const int N = 30;
char s[N];
int d[N];
int n;
bool check() {
  FOR(i, 0, n - 1) {
    int t = 0;
    FOR(j, 0, i) t = (t + d[j] * d[i - j]) % 10;
    if (t != s[i]) return false;
  }
  return true;
}
void dfs(int k) {
  if (k == -1) {
    if (check() == false) return;
    ROF(i, n, 0) printf("%d", d[i]);
    puts("");
    exit(0);
    return;
  }
  int t = 0;
  ROF(i, n - 1, k + 1) t = (t + d[i] * d[n + k - i]) % 10;
  t = (s[n + k] - t + 20) % 10;
  if (n == k) {
    if (t == 0) {
      d[k] = 0;
      dfs(k - 1);
    } else if (t == 1) {
      d[k] = 1;
      dfs(k - 1);
      d[k] = 9;
      dfs(k - 1);
    } else if (t == 4) {
      d[k] = 2;
      dfs(k - 1);
      d[k] = 8;
      dfs(k - 1);
    } else if (t == 9) {
      d[k] = 3;
      dfs(k - 1);
      d[k] = 7;
      dfs(k - 1);
    } else if (t == 6) {
      d[k] = 4;
      dfs(k - 1);
      d[k] = 6;
      dfs(k - 1);
    } else if (t == 5) {
      d[k] = 5;
      dfs(k - 1);
    } else {
      return;
    }
  } else {
    if (t % 2 == 1) return;
    FOR(i, 0, 9) if ((i * d[n] * 2) % 10 == t) {
      d[k] = i;
      dfs(k - 1);
    }
  }
}
int main() {
  scanf("%s", s);
  int ls = strlen(s);
  reverse(s, s + ls);
  FOR(i, 0, ls - 1) s[i] -= '0';
  if (ls % 2 == 0) return puts("-1"), 0;
  n = (ls - 1) / 2;
  dfs(n);
  puts("-1");
  return 0;
}
