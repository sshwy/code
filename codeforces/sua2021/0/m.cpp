// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

const int N = 2005;
const char DOT = '.', AS = '/', BS = '\\';
int n, m, fa[N * N * 2];
char a[N][N];

int p(int x, int y) {
  if (x < 1 || x > n || y < 1 || y > m) return 0;
  return (x - 1) * m + y;
}
vector<pair<int, int>> E;
void ae(int u, int v) {
  if (u == v) return;
  // printf("ae %d %d\n", u, v);
  E.pb({u, v});
}
int get(int u) { return fa[u] == u ? u : fa[u] = get(fa[u]); }
void merge(int u, int v) { fa[get(u)] = get(v); }
void init() {
  FOR(i, 1, n) FOR(j, 1, m) if (a[i][j] != DOT) {
    if (((i + j) % 2 == 0) != (a[i][j] == AS)) {
      ROF(i, n, 1) ROF(j, m, 1) swap(a[i + 1][j], a[i][j]);
      FOR(j, 1, m) merge(p(1, j), 0);
      ++n;
      return;
    }
  }
}
void addEdge(int x, int y) {
  // printf("addEdge %d %d\n", x, y);
  const int d[4][5] = {{x, y + 1, x - 1, y + 1, BS}, {x, y, x - 1, y - 1, AS},
      {x + 1, y, x + 1, y - 1, BS}, {x + 1, y + 1, x + 1, y + 1, AS}};
  FOR(i, 0, 3) {
    if (a[d[i][0]][d[i][1]] == d[i][4]) {
      ae(p(x, y), p(d[i][2], d[i][3]));
    } else {
      merge(p(x, y), p(d[i][2], d[i][3]));
    }
  }
}
void go() {
  FOR(i, 1, n) FOR(j, 1, m) if ((i + j) % 2 == 0) { addEdge(i, j); }
  int ans = 0;
  for (auto &e : E) {
    if (get(e.first) != get(e.second)) {
      // printf("u %d v %d\n", e.first, e.second);
      merge(e.first, e.second);
      ++ans;
    }
  }
  printf("%d\n", ans);
}
int main() {
  scanf("%d%d", &n, &m);
  FOR(i, 1, n) { scanf("%s", a[i] + 1); }
  FOR(i, 0, n * m * 2) fa[i] = i;
  init();
  go();
  return 0;
}
