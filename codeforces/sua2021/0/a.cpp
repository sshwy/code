// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

const int N = 1e5 + 5;

int n, a[N];
long long ans[N], sa[N], b[N], c[N], sz[N], T;
vector<pair<int, int>> g[N];

void dfs1(int u, int p) {
  for (auto e : g[u]) {
    int v = e.first, w = e.second;
    if (v == p) continue;
    dfs1(v, u);
    sa[u] += sa[v];
    sz[u] += sz[v];
    b[u] += b[v] + sa[v] * w;
    c[u] += c[v] + sz[v] * w;
  }
  sa[u] += a[u];
  sz[u] += 1;
}
void dfs2(int u, int p) {
  ans[u] = b[u] + c[u] * a[u];
  for (auto e : g[u]) {
    int v = e.first, w = e.second;
    if (v == p) continue;

    long long bu = b[u], cu = c[u];
    b[u] -= b[v] + sa[v] * w;
    c[u] -= c[v] + sz[v] * w;
    b[v] += b[u] + (T - sa[v]) * w;
    c[v] += c[u] + (n - sz[v]) * w;
    dfs2(v, u);
    b[u] = bu;
    c[u] = cu;
  }
}

int main() {
  scanf("%d", &n);
  FOR(i, 1, n) scanf("%d", a + i);
  FOR(i, 1, n - 1) {
    int u, v, w;
    scanf("%d%d%d", &u, &v, &w);
    g[u].pb({v, w});
    g[v].pb({u, w});
  }
  dfs1(1, 0);
  T = sa[1];
  dfs2(1, 0);
  FOR(i, 1, n) printf("%lld\n", ans[i]);
  return 0;
}
