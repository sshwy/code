// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

namespace FLOW {
  const int N = 5e4 + 5, M = N * 2, INF = 0x3f3f3f3f;

  struct qxx {
    int nex, t, v;
  };
  qxx e[M];
  int h[N], le = 1;
  int hh[N];
  void add_path(int f, int t, int v) { e[++le] = (qxx){h[f], t, v}, h[f] = le; }
  void add_flow(int f, int t, int v) { add_path(f, t, v), add_path(t, f, 0); }
  void add_bidir_flow(int f, int t, int v) { add_path(f, t, v), add_path(t, f, v); }
#define FORe(i, u, v, w) \
  for (int i = h[u], v, w; v = e[i].t, w = e[i].v, i; i = e[i].nex)
#define FORflowe(i, u, v, w) \
  for (int &i = hh[u], v, w; v = e[i].t, w = e[i].v, i; i = e[i].nex)

  int s, t;
  int dep[N];
  queue<int> q;

  bool bfs() {
    memset(dep, 0, sizeof(dep));
    dep[s] = 1, q.push(s);
    while (!q.empty()) {
      int u = q.front();
      q.pop();
      FORe(i, u, v, w) if (!dep[v] && w) dep[v] = dep[u] + 1, q.push(v);
    }
    return dep[t] != 0;
  }
  int dfs(int u, int flow) {
    if (u == t || !flow) return flow;
    int rest = flow;
    FORflowe(i, u, v, w) {
      if (!w || dep[v] != dep[u] + 1) continue;
      int k = dfs(v, min(rest, w));
      e[i].v -= k, e[i ^ 1].v += k, rest -= k;
      if (!rest) break;
    }
    return flow - rest;
  }
  int dinic() {
    int maxflow = 0;
    while (bfs()) {
      memcpy(hh, h, sizeof(h));
      for (int x; (x = dfs(s, INF));) maxflow += x;
    }
    return maxflow;
  }
} // namespace FLOW
const int N = 505, ALP = 30;

int n, m, vis[N];
char s[N][ALP];
vector<int> g[N];

bool check(int x, int y) {
  int cnt = 0;
  FOR(i, 0, m - 1) if (s[x][i] != s[y][i])++ cnt;
  return cnt == 2;
}
void ae(int u, int v) {
  // printf("ae %d %d\n", u, v);
  g[u].pb(v);
  g[v].pb(u);
}
int col[N];
void dfs(int u, int c) {
  vis[u] = true;
  col[u] = c;
  for (int v : g[u]) {
    if (!vis[v]) dfs(v, !c);
  }
}
void work(int s) { dfs(s, 0); }
int main() {
  scanf("%d", &n);
  FOR(i, 1, n) scanf("%s", s[i]);
  m = strlen(s[1]);
  FOR(i, 1, n) FOR(j, i + 1, n) {
    if (check(i, j)) ae(i, j);
  }
  FOR(i, 1, n) if (!vis[i]) { work(i); }
  FOR(u, 1, n) for (int v : g[u]) {
    if (col[u] == 0)
      FLOW::add_flow(u, v, 1);
    else
      FLOW::add_flow(v, u, 1);
  }
  FLOW::s = 0;
  FLOW::t = n + 1;
  FOR(u, 1, n) {
    if (col[u] == 0)
      FLOW::add_flow(FLOW::s, u, 1);
    else
      FLOW::add_flow(u, FLOW::t, 1);
  }
  int ans = FLOW::dinic();
  printf("%d\n", n - ans);
  return 0;
}
