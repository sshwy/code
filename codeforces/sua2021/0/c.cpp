// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

const int N = 1e5 + 5;

int n, m;
vector<int> g[N];

queue<int> q;
int d[N];
void bfs(int s) {
  q.push(s);
  d[s] = 1;
  while (!q.empty()) {
    int u = q.front();
    q.pop();
    for (int v : g[u]) {
      if (!d[v]) {
        d[v] = d[u] + 1;
        q.push(v);
      }
    }
  }
}
int main() {
  scanf("%d%d", &n, &m);
  FOR(i, 1, m) {
    int a, b;
    scanf("%d%d", &a, &b);
    g[a].pb(b);
    g[b].pb(a);
  }
  bfs(1);
  printf("%d\n", d[n] - d[1] - 1);
  return 0;
}
