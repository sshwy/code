// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

const int N = 60;
const long long INF = 2e18;

int n, m;
long long k;
int p[N], vis[N];
long long _g[N][N];

long long add(long long x, long long y) { return min(INF, x + y); }
long long mul(long long x, long long y) {
  if (!x || !y) return 0;
  if (INF / x < y) return INF;
  return x * y;
}

long long g(int a, int b) { // a numbers, b free
  if (a < b || b < 0 || a < 0) return 0;
  if (a == 0 && b == 0) return 1;
  if (_g[a][b]) return _g[a][b];
  if (b) {
    _g[a][b] = add(mul(g(a - 1, b - 1), b), mul(g(a - 1, b), a - b));
  } else {
    _g[a][b] = mul(g(a - 1, 1), a - 1);
  }
  // printf("g(%d,%d) %lld\n", a, b, _g[a][b]);
  return _g[a][b];
}

long long binom(int a, int b) {
  if (a < b || a < 0 || b < 0) return 0;
  long long res = 1;
  double lres = 0;
  FOR(i, 1, b) {
    res = res * (a - b + i) / i;
    lres += log(a - b + i) - log(i);
  }
  if (lres > log(INF)) return INF;
  return res;
}

bool ok(int cur) {
  int fix = 0, a = 0;
  FOR(i, 1, cur) if (p[i] == i)++ fix;
  FOR(i, 1, cur) if (vis[i] == false)++ a;
  long long t = mul(binom(n - cur - a, m - fix), g(n - cur - m + fix, a));
  // printf("t %lld\n", t);
  if (t >= k) return true;
  k -= t;
  return false;
}

int main() {
  scanf("%d%d%lld", &n, &m, &k);
  long long tot = mul(binom(n, m), g(n - m, 0));
  if (tot < k) return puts("-1"), 0;
  FOR(i, 1, n) {
    FOR(j, 1, n) if (!vis[j]) {
      vis[p[i]] = false;
      p[i] = j;
      vis[p[i]] = true;
      // printf("p[%d] %d\n", i, p[i]);
      if (ok(i)) { break; }
    }
  }
  if (k != 1)
    puts("-1");
  else
    FOR(i, 1, n) printf("%d%c", p[i], " \n"[i == n]);
  return 0;
}
