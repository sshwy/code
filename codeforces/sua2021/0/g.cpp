// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

const int N = 2e5 + 5, A = 1e5;

int n;
struct atom {
  int t, m;
} row[N], col[N];

int d[N], ld;
long long ans = 0, c1 = 0, c2 = 0;

void go(long long &c) {
  FOR(i, 1, A) if (col[i].m) {
    ++c;
    d[++ld] = col[i].t + col[i].m - i;
  }
  sort(d + 1, d + ld + 1);
  FOR(i, 1, A) if (row[i].m) {
    int t = upper_bound(d + 1, d + ld + 1, row[i].t - i) - d - 1;
    ans += t;
  }
}
int main() {
  scanf("%d", &n);
  FOR(i, 1, n) {
    char op[5];
    scanf("%s", op);
    int t, m, a;
    scanf("%d%d%d", &t, &m, &a);
    if (op[0] == 'h') {
      row[a] = {t, m};
    } else {
      col[a] = {t, m};
    }
  }

  go(c1);
  FOR(i, 1, A) swap(row[i], col[i]);
  ld = 0;
  go(c2);

  printf("%lld\n", c1 * c2 - ans);
  return 0;
}
