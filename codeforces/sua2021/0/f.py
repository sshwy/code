def fac(n):
    if n == 0:
        return 1
    return fac(n-1)*n

def cuopai(n):
    s = 0
    for i in range(n+1):
        if i%2 == 0:
            s += fac(n)/fac(i)
        else:
            s -= fac(n)/fac(i)
    return s

for i in range(22):
    print("{} : {}".format(i, cuopai(i)))
