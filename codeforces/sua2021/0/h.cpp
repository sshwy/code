// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define X first
#define Y second
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

const int N = 2005;
const double PI = 3.1415926536;
typedef pair<int, int> Point;

int n;
Point a[N];

Point line(Point x, Point y) {
  Point res(x.X - y.X, x.Y - y.Y);
  if (res.X < 0) res.X = -res.X, res.Y = -res.Y;
  if (res.X == 0) res.Y = abs(res.Y);
  return res;
}

Point operator+(Point x, Point y) { return Point(x.X + y.X, x.Y + y.Y); }

bool cmp(pair<Point, int> x, pair<Point, int> y) {
  if (x.first.X == 0) return false;
  if (y.first.X == 0) return true;
  return 1ll * x.first.Y * y.first.X < 1ll * x.first.X * y.first.Y;
}

double atan(Point x) {
  if (x.X == 0) return PI / 2;
  return atan(x.Y * 1.0 / x.X);
}

int cnt[N], nex[N][N], vis[N][N];
double w[N][N];
int ans = 0;

void go(int s, int i) {
  int u = s, v = i;
  double totAngle = 0;
  do {
    int x = nex[u][v];
    totAngle += w[u][v];
    v = u, u = x;
    cnt[u]++;
    vis[u][v] = true;
  } while (!(u == s && v == i));
  do {
    int x = nex[u][v];
    v = u, u = x;
    if (totAngle > 6)
      ans = max(ans, cnt[u]);
    else
      ans = max(ans, cnt[u] * 2);
    cnt[u] = 0;
  } while (!(u == s && v == i));
}
void pivot(int s) {
  FOR(i, 1, n) if (i != s) {
    if (vis[s][i]) continue;
    go(s, i);
  }
}

int main() {
  scanf("%d", &n);
  FOR(i, 1, n) scanf("%d%d", &a[i].X, &a[i].Y);
  FOR(i, 1, n) {
    vector<pair<Point, int>> v;
    FOR(j, 1, n) if (j != i) v.pb({line(a[i], a[j]), j});
    sort(v.begin(), v.end(), cmp);
    FOR(j, 0, n - 2) {
      int x = v[j].second, y = v[(j + 1) % v.size()].second;
      nex[i][x] = y;
      if (j < n - 2) {
        w[i][x] = abs(atan(line(a[i], a[y])) - atan(line(a[i], a[x])));
      } else {
        w[i][x] = PI - abs(atan(line(a[i], a[y])) - atan(line(a[i], a[x])));
      }
    }
  }
  FOR(i, 1, n) pivot(i);
  printf("%d\n", ans);
  return 0;
}
