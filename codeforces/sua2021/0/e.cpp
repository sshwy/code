// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

const int N = 1e5 + 5, P = 11092019;

char s[N];
int c[N];

int main() {
  scanf("%s", s + 1);
  int n = strlen(s + 1);
  FOR(i, 1, n) c[s[i] - 'a']++;
  int ans = 1;
  FOR(i, 0, 25) ans = ans * 1ll * (c[i] + 1) % P;
  printf("%d\n", ans);
  return 0;
}
