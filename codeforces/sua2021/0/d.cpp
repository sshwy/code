// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

int a, b;

int main() {
  scanf("%d%d", &a, &b);
  long long cur = 0;
  long long ans = a > b ? 1e18 : b - a;
  while (a > 1) {
    if (a % 2 == 1) ++cur, ++a;
    a /= 2, ++cur;
    if (a <= b) ans = min(ans, b - a + cur);
  }
  printf("%lld\n", ans);
  return 0;
}
