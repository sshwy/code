// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

int n;
long long d, mx, mn, mxb, mnb;

int main() {
  scanf("%d%lld", &n, &d);
  FOR(i, 1, n) {
    long long x;
    scanf("%lld", &x);
    long long a = x - d * (i - 1);
    long long b = x - d * (n - i + 1);
    if (i == 1)
      mx = mn = a, mxb = mnb = b;
    else {
      mx = max(mx, a);
      mn = min(mn, a);
      mxb = max(mxb, b);
      mnb = min(mnb, b);
    }
  }
  long long ans = min(mx - mn, mxb - mnb);
  printf("%lld", ans / 2);
  if (ans % 2)
    puts(".5");
  else
    puts(".0");
  return 0;
}
