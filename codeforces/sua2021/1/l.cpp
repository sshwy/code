// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

typedef pair<int, int> Point;
const int N = 1e6 + 5;

int n, h[N];
long long ans;

long long calc(Point x, Point y) {
  return (y.first - x.first) * 1ll * (y.second - x.second);
}

vector<Point> A, B;
void work(int la, int ra, int lb, int rb) {
  if (la > ra || lb > rb) return;
  int ma = (la + ra) >> 1;
  int opt = -1;
  FOR(i, lb, rb) {
    if (opt == -1 || calc(A[ma], B[i]) > calc(A[ma], B[opt])) opt = i;
  }
  ans = max(ans, calc(A[ma], B[opt]));
  work(la, ma - 1, lb, opt);
  work(ma + 1, ra, opt, rb);
}
void solve(int l, int r) {
  if (l == r) return;
  int mid = (l + r) >> 1;
  FOR(i, l, mid) {
    Point p(i, -h[i]);
    if (A.empty() || A.back().second >= p.second) A.pb(p);
  }
  FOR(i, mid + 1, r) {
    Point p(i, h[i]);
    while (B.size() && B.back().second <= p.second) B.pop_back();
    B.pb(p);
  }
  if (A.size() && B.size()) work(0, A.size() - 1, 0, B.size() - 1);
  A.clear(), B.clear();
  solve(l, mid);
  solve(mid + 1, r);
}
int main() {
  scanf("%d", &n);
  FOR(i, 1, n) scanf("%d", h + i);
  solve(1, n);
  printf("%lld\n", ans);
  return 0;
}
