// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

const int N = 1e5 + 5;

int n, k, key[N];
vector<int> g[N];
bool ok[N];

int ans;
void dfs(int u, int p) {
  if (key[u]) ok[u] = true;
  for (int v : g[u])
    if (v != p) {
      dfs(v, u);
      if (ok[v]) ok[u] = true;
    }
  if (ok[u]) ++ans;
}
int main() {
  scanf("%d%d", &n, &k);
  FOR(i, 1, n - 1) {
    int u, v, w;
    scanf("%d%d%d", &u, &v, &w);
    g[u].pb(v);
    g[v].pb(u);
  }
  FOR(i, 1, k) {
    int u;
    scanf("%d", &u);
    key[u] = true;
  }
  FOR(i, 1, n) if (key[i]) {
    dfs(i, 0);
    break;
  }
  printf("%d\n", ans);
  return 0;
}
