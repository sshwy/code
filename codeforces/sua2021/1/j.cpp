// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

const int N = 505;

struct Big {
  bitset<N> v, vid;
  void operator^=(Big &b) {
    v ^= b.v;
    vid ^= b.vid;
  }
  bool operator[](int x) { return v[x]; }
};
int n;
Big a[N], b[N];

void insert(Big x) {
  ROF(i, n, 1) if (x[i]) {
    if (b[i][i])
      x ^= b[i];
    else {
      b[i] = x;
      break;
    }
  }
}
int main() {
  scanf("%d", &n);
  FOR(i, 1, n) FOR(j, 1, n) {
    int x;
    scanf("%d", &x);
    a[i].v.set(j, x);
  }
  FOR(i, 1, n) a[i].vid.set(i, 1);
  FOR(i, 1, n) insert(a[i]);
  FOR(i, 1, n) if (!b[i][i]) return puts("-1"), 0;
  ROF(i, n, 1) ROF(j, n, i + 1) if (b[j][i]) b[j] ^= b[i];
  FOR(i, 1, n) {
    FOR(j, 1, n) if (b[i].vid[j]) printf("%d ", j);
    puts("");
  }
  return 0;
}
