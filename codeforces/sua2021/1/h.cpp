// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

const int N = 5e4 + 5, BASE = 30000;

const int SZ = 1 << 19, P = 998244353;
int pw(int a, int m) {
  if (m < 0) m += P - 1;
  int res = 1;
  while (m) m & 1 ? res = 1ll * res * a % P : 0, a = 1ll * a * a % P, m >>= 1;
  return res;
}
int tr[SZ];
int init(int n) { // n指多项式的长度，非度数
  int len = 1;
  while (len < n) len <<= 1;
  FOR(i, 0, len - 1) tr[i] = tr[i >> 1] >> 1 | ((i & 1) * (len / 2));
  return len;
}
void dft(int *f, int len, int tag) {
  FOR(i, 0, len - 1) if (i < tr[i]) swap(f[i], f[tr[i]]);
  FOR(i, 0, len - 1) f[i] = (f[i] + P) % P;
  for (int j = 1; j < len; j <<= 1)
    for (int i = 0, wn = pw(3, (P - 1) / (j << 1) * tag); i < len; i += j << 1)
      for (int k = i, u, v, w = 1; k < i + j; k++, w = 1ll * w * wn % P)
        u = f[k], v = f[k + j] * 1ll * w % P, f[k] = (u + v) % P,
        f[k + j] = (u - v + P) % P;
  if (tag == -1)
    for (int i = 0, ilen = pw(len, P - 2); i < len; i++) f[i] = 1ll * f[i] * ilen % P;
}

int a[SZ], la;
int b[SZ], lb;
int c[SZ], lc;

int main() {
  scanf("%d", &la);
  FOR(i, 1, la) {
    int x;
    scanf("%d", &x);
    a[x + BASE]++;
  }
  scanf("%d", &lb);
  FOR(i, 1, lb) {
    int x;
    scanf("%d", &x);
    b[x + BASE]++;
  }
  scanf("%d", &lc);
  FOR(i, 1, lc) {
    int x;
    scanf("%d", &x);
    c[x + BASE]++;
  }

  int len = init(BASE * 4 + 5);
  dft(a, len, 1), dft(c, len, 1);
  FOR(i, 0, len - 1) a[i] = a[i] * 1ll * c[i] % P;
  dft(a, len, -1);

  long long ans = 0;
  FOR(i, 0, BASE * 2) if (b[i]) { ans += a[i * 2] * 1ll * b[i]; }
  printf("%lld\n", ans);

  return 0;
}
