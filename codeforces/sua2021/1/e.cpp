// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

const int N = 1e6 + 5;
int n, a[N];

void no() {
  puts("NO");
  exit(0);
}
int main() {
  scanf("%d", &n);
  FOR(i, 1, n) scanf("%d", a + i);
  FOR(i, 1, n) if (a[i] > 2) no();
  if (a[1] > 1 || a[n] > 1) no();
  int dir = a[1] == 1 ? 1 : 0; // 1: diff, 0: same
  FOR(i, 2, n) {
    if (dir == 0) { // same
      if (a[i] == 0)
        ;
      else if (a[i] == 1)
        dir = 1;
      else if (a[i] == 2)
        no();
    } else {
      if (a[i] == 0)
        ;
      else if (a[i] == 1)
        dir = 0;
      else
        ;
    }
  }
  if (dir == 1) no();
  puts("YES");
  return 0;
}
