// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

int a[10], b[10];
int main() {
  FOR(i, 1, 6) cin >> a[i];
  FOR(i, 1, 6) cin >> b[i];
  int ans = 0;
  FOR(i, 1, 6) FOR(j, 1, 6) if (a[i] > b[j])++ ans;
  int g = __gcd(ans, 36);
  cout << ans / g << "/" << 36 / g << endl;
  return 0;
}
