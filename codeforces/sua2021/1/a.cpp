// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

int n, T;

struct line {
  int h, l, r;
  bool operator<(line l) { return h < l.h; }
};
vector<line> hor, ver, rhor, rver;

struct hwystat {
  int x, y, dx, dy, lx, rx, ly, ry;
  bool operator==(hwystat s) {
    return x == s.x && y == s.y && dx == s.dx && dy == s.dy;
  }
  void walk(int d) {
    x += dx * d;
    y += dy * d;
  }
  void setRangex(int l, int r) {
    lx = l;
    rx = r;
  }
  void setRangey(int l, int r) {
    ly = l;
    ry = r;
  }
  hwystat getNext() {
    // printf("dx %d dy %d\n", dx, dy);
    assert(dx == 0 || dy == 0);
    // printf("dx %d dy %d\n", dx, dy);
    if (dx == 1) {
      for (auto e : ver) {
        if (e.l <= y && y <= e.r && x < e.h && e.h < rx) {
          return {e.h, y, 0, 1, 0, 0, e.l, e.r};
        }
      }
      return {rx, y, -1, 0, rx, lx, 0, 0};
    }
    if (dx == -1) {
      for (auto e : rver) {
        if (e.l <= y && y <= e.r && x > e.h && e.h > rx) {
          return {e.h, y, 0, -1, 0, 0, e.r, e.l};
        }
      }
      return {rx, y, 1, 0, rx, lx, 0, 0};
    }
    if (dy == 1) {
      for (auto e : hor) {
        if (e.l <= x && x <= e.r && y < e.h && e.h < ry) {
          return {x, e.h, -1, 0, e.r, e.l, 0, 0};
        }
      }
      return {x, ry, 0, -1, 0, 0, ry, ly};
    }
    if (dy == -1) {
      for (auto e : rhor) {
        if (e.l <= x && x <= e.r && y > e.h && e.h > ry) {
          return {x, e.h, 1, 0, e.l, e.r, 0, 0};
        }
      }
      return {x, ry, 0, 1, 0, 0, ry, ly};
    }
    assert(0);
  }
} start, cur;
int dis(hwystat x1, hwystat x2) { return abs(x1.x - x2.x) + abs(x1.y - x2.y); }

void go() {
  while (T) {
    hwystat nex = cur.getNext();
    // printf("cur %d %d %d %d rx:(%d, %d), ry:(%d, %d)\n",
    //     cur.x, cur.y, cur.dx, cur.dy, cur.lx, cur.rx, cur.ly, cur.ry);
    int d = dis(cur, nex);
    if (d <= T) {
      cur = nex;
      T -= d;
    } else {
      cur.walk(T);
      T = 0;
    }
    if (start == cur) break;
  }
  // printf("final %d %d %d %d\n", cur.x, cur.y, cur.dx, cur.dy);
}
int main() {
  scanf("%d%d", &n, &T);
  int tt = T;
  FOR(i, 1, n) {
    int x1, y1, x2, y2;
    scanf("%d%d%d%d", &x1, &y1, &x2, &y2);
    if (x1 == x2)
      ver.pb({x1, min(y1, y2), max(y1, y2)});
    else
      hor.pb({y1, min(x1, x2), max(x1, x2)});
    if (i == 1) {
      cur.x = x1, cur.y = y1;
      if (x1 < x2)
        cur.dx = 1;
      else if (x1 == x2)
        cur.dx = 0;
      else
        cur.dx = -1;
      if (y1 < y2)
        cur.dy = 1;
      else if (y1 == y2)
        cur.dy = 0;
      else
        cur.dy = -1;
      if (x1 != x2)
        cur.setRangex(x1, x2);
      else
        cur.setRangey(y1, y2);
    }
  }
  sort(ver.begin(), ver.end());
  sort(hor.begin(), hor.end());
  rver = ver;
  rhor = hor;
  reverse(rver.begin(), rver.end());
  reverse(rhor.begin(), rhor.end());

  start = cur;
  go();
  if (T) {
    assert(start == cur);
    int per = tt - T;
    assert(per);
    T %= per;
    go();
  }
  printf("%d %d\n", cur.x, cur.y);
  return 0;
}
