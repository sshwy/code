// by Sshwy
//#define DEBUGGER
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

#ifdef DEBUGGER

#define log_prefix()                                                \
  {                                                                 \
    fprintf(stderr, "\033[37mLine %-3d [%dms]:\033[0m  ", __LINE__, \
        (int)clock() / 1000);                                       \
  }
#define ilog(...) \
  { fprintf(stderr, __VA_ARGS__); }
#define llog(...)      \
  {                    \
    log_prefix();      \
    ilog(__VA_ARGS__); \
  }
#define log(...)           \
  {                        \
    log_prefix();          \
    ilog(__VA_ARGS__);     \
    fprintf(stderr, "\n"); \
  }
#define red(...)       \
  {                    \
    log_prefix();      \
    ilog("\033[31m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define green(...)     \
  {                    \
    log_prefix();      \
    ilog("\033[32m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define blue(...)      \
  {                    \
    log_prefix();      \
    ilog("\033[34m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }

#else

#define log_prefix() ;
#define ilog(...) ;
#define llog(...) ;
#define log(...) ;
#define red(...) ;
#define green(...) ;
#define blue(...) ;

#endif

namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 2e5 + 5;

int n, q;
char s[N];

struct atom { //等差数列
  int l, r, d, e;
  atom() { l = r = d = e = 0; }
  atom(int x) { l = r = x, d = 0, e = 1; }
  atom(int x, int y) { l = x, r = y, d = r - l, e = 1; }
  atom(int x, int y, int z) { l = x, r = y, d = z, e = 1; }

  bool have(int x) const {
    return e ? (l == r ? (l == x)
                       : ((x - l) % d == 0 && (l <= x && x <= r || r <= x && x <= l)))
             : 0;
  }
  int size() const { return e ? (l == r ? 1 : (r - l) / d + 1) : 0; }
  atom reverse() const { return e ? atom(r, l, -d) : atom(); }
  atom oppsite() const { return e ? atom(-l, -r, -d) : atom(); }
  atom operator+(const int x) const { return e ? atom(l + x, r + x, d) : atom(); }
  atom operator-(const int x) const { return e ? atom(l - x, r - x, d) : atom(); }
  atom operator&(const atom a) const { //等差数列交
    const atom *x = this, *y = &a;
    if (x->size() > y->size()) swap(x, y);
    if (x->size() == 0) {
      return atom();
    } else if (x->size() == 1) {
      return y->have(x->l) ? *x : atom();
    } else if (x->size() == 2) {
      int L = y->have(x->l), R = y->have(x->r);
      return (L && R) ? *x : (L ? atom(x->l) : (R ? atom(x->r) : atom()));
    } else {
      assert(d == a.d);
      if ((l - a.l) % d) return atom();
      return atom(max(l, a.l), min(r, a.r), d);
    }
  }
  void print() {
    ilog("(%-2d,%-2d,%-2d,%-2d) ", l, r, d, e);
    if (size() == 0)
      return;
    else {
      for (int i = l; i != r; i += d) ilog("%d ", i);
      ilog("%d ", r);
    }
  }
};
int next_match(int p, int j, int pos) {
  int len = 1 << j, lim = n - len + 1;
  FOR(i, pos, lim) {
    bool fl = 1;
    FOR(j, 0, len - 1) {
      if (s[p + j] != s[i + j]) {
        fl = 0;
        break;
      }
    }
    if (fl) return i;
  }
  return -1;
}
int prev_match(int p, int j, int pos) {
  int len = 1 << j;
  ROF(i, pos, 1) {
    bool fl = 1;
    FOR(j, 0, len - 1) {
      if (s[p + j] != s[i + j]) {
        fl = 0;
        break;
      }
    }
    if (fl) return i;
  }
  return -1;
}
int calc(int L, int R, int j) { // 2^j -> min(2^{j+1}-1,R-L)
  // green("calc(%d,%d,%d)",L,R,j);
  // llog(""); FOR(i,L,R)ilog("%c",s[i]); ilog("\n");
  int mxl = min((1 << (j + 1)) - 1, R - L);
  // log("[%d,%d]",1<<j,mxl);
  //长度在[2^j,mxl]的最长的Border
  atom A, B;
  {
    // S(L,L+2^j-1]在S[R-mxl+1,R]中匹配的位置集合
    int pL = R - mxl + 1, pR = R - (1 << j) + 1;
    int p1 = next_match(L, j, pL);
    // log("next_match(%d,%d,%d)=%d",L,j,pL,p1);
    if (p1 == -1 || p1 > pR)
      A = atom();
    else {
      int p2 = next_match(L, j, p1 + 1);
      if (p2 == -1 || p2 > pR)
        A = atom(p1);
      else {
        int p3 = prev_match(L, j, pR);
        if (p3 == -1 || p3 < pL)
          A = atom(p1, p2);
        else
          A = atom(p1, p3, p2 - p1);
      }
    }
  }
  {
    int pL = L, pR = L + mxl - (1 << j);
    int p1 = next_match(R - (1 << j) + 1, j, pL);
    if (p1 == -1 || p1 > pR)
      B = atom();
    else {
      int p2 = next_match(R - (1 << j) + 1, j, p1 + 1);
      if (p2 == -1 || p2 > pR)
        B = atom(p1);
      else {
        int p3 = prev_match(R - (1 << j) + 1, j, pR);
        if (p3 == -1 || p3 < pL)
          B = atom(p1, p2);
        else
          B = atom(p1, p3, p2 - p1);
      }
    }
  }
  // llog("A: "); A.print(); ilog("\n");
  // llog("B: "); B.print(); ilog("\n");
  A = A + ((1 << j) - 1);
  A = A - (L - 1);
  A = A.oppsite();
  A = A + (R + 1);
  A = A.reverse();
  // llog("A: "); A.print(); ilog("\n");
  // llog("B: "); B.print(); ilog("\n");
  A = A & B;
  A = A + ((1 << j) - 1);
  // llog("merge: "); A.print(); ilog("\n");
  if (A.size()) return A.r - L + 1;
  return -1;
}
void go() {
  int L, R;
  scanf("%d%d", &L, &R);
  ROF(j, 19, 0) {
    if ((1 << j) <= R - L) {
      int ans = calc(L, R, j);
      if (ans != -1) {
        printf("%d\n", ans);
        return;
      }
    }
  }
  puts("0");
}
int main() {
  scanf("%d%d", &n, &q);
  scanf("%s", s + 1);
  FOR(i, 1, q) go();
  return 0;
}
