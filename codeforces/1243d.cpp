#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 1e5 + 5;

int n, m;
vector<int> G[N];

int f[N], g[N];
void init(int lim) { FOR(i, 0, lim) f[i] = i, g[i] = 1; }
int get(int u) { return f[u] == u ? u : f[u] = get(f[u]); }
void merge(int u, int v) {
  // printf("merge(%d,%d)\n",u,v);
  u = get(u), v = get(v);
  f[u] = v, g[v] += g[u];
}
bool find(int u, int v) { return get(u) == get(v); }

int t[N];
set<int> s, s2;

int main() {
  scanf("%d%d", &n, &m);
  FOR(i, 1, m) {
    int u, v;
    scanf("%d%d", &u, &v);
    if (u > v) swap(u, v);
    G[v].pb(u);
  }
  init(n);
  FOR(u, 1, n) {
    for (int x : s) t[x] = 0;
    for (int v : G[u])
      if (!find(u, v)) t[get(v)]++;
    s2.clear();
    for (int x : s) {
      x = get(x);
      // printf("x=%d,u=%d\n",x,u);
      if (t[x] < g[x])
        merge(x, u);
      else
        s2.insert(x);
    }
    s2.insert(get(u));
    s = s2;
    // printf("s.insert(%d)\n",get(u));
  }
  int ans = 0;
  FOR(i, 1, n) t[get(i)] = 0;
  FOR(i, 1, n) ans += !t[get(i)], t[get(i)] = 1;
  printf("%d\n", ans - 1);
  return 0;
}
