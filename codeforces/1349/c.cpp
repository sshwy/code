// by Sshwy
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA
const int N = 1005;
const long long INF = 1e18 + 1;
int n, m, t;
char a[N][N];
long long tim[N][N];
deque<int> q;
int main() {
  scanf("%d%d%d", &n, &m, &t);
  FOR(i, 1, n) { scanf("%s", a[i] + 1); }
  FOR(i, 1, n) FOR(j, 1, m) tim[i][j] = INF;
  FOR(i, 1, n) FOR(j, 1, m) {
    if (a[i][j] == a[i + 1][j] || a[i][j] == a[i - 1][j] || a[i][j] == a[i][j + 1] ||
        a[i][j] == a[i][j - 1]) {
      q.push_back((i - 1) * m + j - 1);
      tim[i][j] = 0;
    }
  }
  int dir[][2] = {0, 1, 0, -1, 1, 0, -1, 0};
  while (!q.empty()) {
    int cur = q.front();
    q.pop_front();
    int cx = cur / m + 1, cy = cur % m + 1;
    FOR(_, 0, 3) {
      int nx = cx + dir[_][0], ny = cy + dir[_][1];
      if (nx < 1 || nx > n || ny < 1 || ny > m) continue;
      if (tim[nx][ny] == INF) {
        if (a[nx][ny] == a[cx][cy]) {
          tim[nx][ny] = tim[cx][cy];
          q.push_front((nx - 1) * m + ny - 1);
        } else {
          tim[nx][ny] = tim[cx][cy] + 1;
          q.push_back((nx - 1) * m + ny - 1);
        }
      }
    }
  }
  FOR(i, 1, t) {
    int x, y;
    long long z;
    scanf("%d%d%lld", &x, &y, &z);
    if (z <= tim[x][y]) {
      printf("%c\n", a[x][y]);
    } else {
      long long d = tim[x][y] - z;
      if (d & 1)
        printf("%d\n", (a[x][y] - '0') ^ 1);
      else
        printf("%c\n", a[x][y]);
    }
  }
  return 0;
}
