// by Sshwy
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

int n;
map<int, vector<int>> s;
void div(int x) {
  for (int i = 2; i * i <= x; ++i) {
    if (x % i) continue;
    int c = 0;
    while (x % i == 0) x /= i, ++c;
    s[i].pb(c);
  }
  if (x > 1) s[x].pb(1);
}
int main() {
  scanf("%d", &n);
  FOR(i, 1, n) {
    int x;
    scanf("%d", &x);
    div(x);
  }
  long long ans = 1;
  for (auto pv : s) {
    auto &v = pv.second;
    long long coef = 1;
    if (v.size() + 2 <= n) {
      coef = 1;
    } else {
      while (v.size() < n) v.pb(0);
      sort(v.begin(), v.end());
      coef = 1;
      FOR(_, 1, v[1]) coef *= pv.first;
    }
    ans *= coef;
  }
  printf("%lld\n", ans);
  return 0;
}
