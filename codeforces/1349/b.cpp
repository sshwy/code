// by Sshwy
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA
const int N = 1e5 + 5;
int n, k;
int a[N];
int s[N], t[N], g[N];
void go() {
  scanf("%d%d", &n, &k);
  FOR(i, 1, n) scanf("%d", &a[i]);
  {
    bool flag = 0;
    FOR(i, 1, n) if (a[i] == k) {
      flag = 1;
      break;
    }
    if (flag == 0) return puts("no"), void();
  }
  if (n == 1) return puts("yes"), void();
  FOR(i, 2, n) if (a[i] >= k && a[i - 1] >= k) return puts("yes"), void();
  FOR(i, 3, n) if (a[i] >= k && a[i - 2] >= k) return puts("yes"), void();
  puts("no");
}
int main() {
  int t;
  scanf("%d", &t);
  FOR(i, 1, t) go();
  return 0;
}
