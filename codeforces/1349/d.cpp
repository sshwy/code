// by Sshwy
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA
const int N = 3e5 + 5, P = 998244353;
int pw(int a, int m) {
  int res = 1;
  while (m) m & 1 ? res = 1ll * res * a % P : 0, a = 1ll * a * a % P, m >>= 1;
  return res;
}
int n, a[N], s, is, in_1;
int A(int i) { return (s - i) * 1ll * is % P * in_1 % P; }
int B(int i) { return i * 1ll * is % P; }
int C(int i) { return ((1 - A(i) - B(i)) % P + P) % P; }
int f[N];
int main() {
  scanf("%d", &n);
  FOR(i, 1, n) scanf("%d", &a[i]), s += a[i];
  is = pw(s, P - 2);
  in_1 = pw(n - 1, P - 2);

  f[0] = pw(1 - C(0), P - 2);
  // printf("f[%d]=%d\n",0,f[0]);
  FOR(i, 1, s) {
    f[i] = (B(i) * 1ll * f[i - 1] + 1) % P * pw(A(i), P - 2) % P;
    // printf("f[%d]=%d\n",i,f[i]);
  }
  FOR(i, 1, s) f[i] = (f[i] + f[i - 1]) % P;
  int ans = 0;
  FOR(i, 1, n) ans = (0ll + ans + f[s - 1] - f[a[i] - 1]) % P;
  ans = (ans - f[s - 1] * 1ll * (n - 1) % P) % P;
  ans = ans * 1ll * pw(n, P - 2) % P;
  ans = (ans + P) % P;
  printf("%d\n", ans);
  return 0;
}
