#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
#include <bitset>
/******************heading******************/
const int N = 120;

int K;

int f[N][N], vis[N][N];
int mex(bitset<1000> bin) {
  FOR(i, 0, 999) if (bin[i] == 0) return i;
  return 1000;
}
int sg(int x, int y) {
  if (x == 1 || y == 1) return 1;
  if (vis[x][y]) return f[x][y];
  bitset<1000> bin;
  bin[0] = 1;
  FOR(k, 2, min(min(x, y), K)) {
    int s = 0;
    FOR(i, 0, k - 1) FOR(j, 0, k - 1) {
      if (i == 0 && j == 0) continue;
      s ^= sg(x - i, y - j);
    }
    bin[s] = 1;
  }
  vis[x][y] = 1;
  f[x][y] = mex(bin);
  return f[x][y];
}
int main() {
  int n;
  cin >> n >> K;
  FOR(i, 1, n) FOR(j, 1, n) printf("%d%c", sg(i, j), " \n"[j == n]);
  return 0;
}
