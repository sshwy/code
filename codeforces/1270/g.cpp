// by Sshwy
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 1e6 + 5;

vector<int> g[N];

int n;
int a[N], d[N];

queue<int> q;
void go() {
  scanf("%d", &n);
  FOR(i, 1, n) scanf("%d", &a[i]);
  FOR(i, 1, n) g[i].clear(), d[i] = 0;

  FOR(i, 1, n) g[i].pb(i - a[i]), d[i - a[i]]++;
  FOR(i, 1, n) if (d[i] == 0) q.push(i);

  while (!q.empty()) {
    int u = q.front();
    q.pop();
    for (int v : g[u]) {
      d[v]--;
      if (d[v] == 0) q.push(v);
    }
  }
  int s = 0;
  FOR(i, 1, n) s += d[i] != 0;
  printf("%d\n", s);
  FOR(i, 1, n) if (d[i] != 0) printf("%d ", i);
  puts("");
}
int main() {
  int t;
  scanf("%d", &t);
  FOR(i, 1, t) go();
  return 0;
}
