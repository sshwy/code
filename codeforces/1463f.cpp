// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

const int X = 22;

void getMax(int &x, int y) { x = max(x, y); }

int n, x, y;
int _f[2][1 << X], c[X * 3];
int *f = _f[0], *g = _f[1];

int main() {
  scanf("%d%d%d", &n, &x, &y);
  int m = x + y;
  FOR(i, 0, m - 1) c[i] = (n - 1) / m + (i <= (n - 1) % m);

  memset(_f, -0x3f, sizeof(_f));
  int _INF = _f[0][0];

  f[0] = 0;

  int lim = 1 << max(x, y);
  FOR(i, 0, m - 1) {
    memset(g, -0x3f, sizeof(_f[0]));
    FOR(j, 0, lim - 1) if (f[j] > _INF) {
      getMax(g[(j << 1) & (lim - 1)], f[j]);
      if (!(j >> (x - 1) & 1) && !(j >> (y - 1) & 1)) {
        getMax(g[((j << 1) & (lim - 1)) | 1], f[j] + c[i]);
      }
    }
    swap(f, g);
  }
  int ans = *max_element(f, f + lim);
  printf("%d\n", ans);
  return 0;
}
