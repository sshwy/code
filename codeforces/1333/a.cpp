// by Sshwy
//#define DEBUGGER
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

#ifdef DEBUGGER

#define log_prefix()                                                \
  {                                                                 \
    fprintf(stderr, "\033[37mLine %-3d [%dms]:\033[0m  ", __LINE__, \
        (int)clock() / 1000);                                       \
  }
#define ilog(...) \
  { fprintf(stderr, __VA_ARGS__); }
#define llog(...)      \
  {                    \
    log_prefix();      \
    ilog(__VA_ARGS__); \
  }
#define log(...)           \
  {                        \
    log_prefix();          \
    ilog(__VA_ARGS__);     \
    fprintf(stderr, "\n"); \
  }
#define red(...)       \
  {                    \
    log_prefix();      \
    ilog("\033[31m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define green(...)     \
  {                    \
    log_prefix();      \
    ilog("\033[32m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define blue(...)      \
  {                    \
    log_prefix();      \
    ilog("\033[34m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }

#else

#define log_prefix() ;
#define ilog(...) ;
#define llog(...) ;
#define log(...) ;
#define red(...) ;
#define green(...) ;
#define blue(...) ;

#endif

namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 1000;
char a[N][N];
void go() {
  int n, m;
  cin >> n >> m;
  if (n % 2 && m % 2) {
    FOR(i, 1, n) {
      FOR(j, 1, m) { a[i][j] = j % 2 ? 'B' : 'W'; }
    }
    FOR(i, 1, n) a[i][m] = i & 1 ? 'B' : 'W';
    FOR(i, 1, n) {
      FOR(j, 1, m) printf("%c", a[i][j]);
      puts("");
    }
  } else if (n % 2) {
    FOR(i, 1, n) {
      for (int j = 1; j < m; j += 2) {
        a[i][j] = 'B';
        a[i][j + 1] = 'W';
      }
    }
    a[1][1] = 'B';
    a[1][2] = 'B';
    a[2][1] = 'B';
    a[2][2] = 'W';
    FOR(i, 1, n) {
      FOR(j, 1, m) printf("%c", a[i][j]);
      puts("");
    }
  } else if (m % 2) {
    for (int i = 1; i < n; i += 2) {
      FOR(j, 1, m) {
        a[i][j] = 'B';
        a[i + 1][j] = 'W';
      }
    }
    a[1][1] = 'B';
    a[1][2] = 'B';
    a[2][1] = 'B';
    a[2][2] = 'W';
    FOR(i, 1, n) {
      FOR(j, 1, m) printf("%c", a[i][j]);
      puts("");
    }
  } else {
    for (int i = 1; i < n; i += 2) {
      FOR(j, 1, m) {
        a[i][j] = 'B';
        a[i + 1][j] = 'W';
      }
    }
    a[1][1] = 'B';
    a[1][2] = 'B';
    a[2][1] = 'B';
    a[2][2] = 'W';
    FOR(i, 1, n) {
      FOR(j, 1, m) printf("%c", a[i][j]);
      puts("");
    }
  }
}
int main() {
  int t;
  cin >> t;
  FOR(i, 1, t) go();
  return 0;
}
