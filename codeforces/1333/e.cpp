// by Sshwy
//#define DEBUGGER
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

#ifdef DEBUGGER

#define log_prefix()                                                \
  {                                                                 \
    fprintf(stderr, "\033[37mLine %-3d [%dms]:\033[0m  ", __LINE__, \
        (int)clock() / 1000);                                       \
  }
#define ilog(...) \
  { fprintf(stderr, __VA_ARGS__); }
#define llog(...)      \
  {                    \
    log_prefix();      \
    ilog(__VA_ARGS__); \
  }
#define log(...)           \
  {                        \
    log_prefix();          \
    ilog(__VA_ARGS__);     \
    fprintf(stderr, "\n"); \
  }
#define red(...)       \
  {                    \
    log_prefix();      \
    ilog("\033[31m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define green(...)     \
  {                    \
    log_prefix();      \
    ilog("\033[32m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define blue(...)      \
  {                    \
    log_prefix();      \
    ilog("\033[34m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }

#else

#define log_prefix() ;
#define ilog(...) ;
#define llog(...) ;
#define log(...) ;
#define red(...) ;
#define green(...) ;
#define blue(...) ;

#endif

namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/

int n;
int p[100000];
int vis[100000];
int a[666][666];

void dfs(int n, int x, int tag) {
  if (n == 3) {
    a[1][1] = x + 8, a[1][2] = x + 2, a[1][3] = x + 9;
    a[2][1] = x + 1, a[2][2] = x + 3, a[2][3] = x + 6;
    a[3][1] = x + 4, a[3][2] = x + 7, a[3][3] = x + 5;
    if (tag) FOR(i, 1, 3) FOR(j, i + 1, 3) swap(a[i][j], a[j][i]);
  } else {
    FOR(j, 1, n) a[n][j] = ++x;
    ROF(i, n - 1, 1) a[i][n] = ++x;
    if (tag) FOR(j, 1, n - 1) swap(a[n][j], a[j][n]);
    dfs(n - 1, x, tag ^ 1);
  }
}
int main() {
  cin >> n;
  if (n < 3) return cout << -1 << endl, 0;
  dfs(n, 0, 0);
  FOR(i, 1, n) FOR(j, 1, n) cout << a[i][j] << " \n"[j == n];
  return 0;
}
