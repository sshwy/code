// by Sshwy
//#define DEBUGGER
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

#ifdef DEBUGGER

#define log_prefix()                                                \
  {                                                                 \
    fprintf(stderr, "\033[37mLine %-3d [%dms]:\033[0m  ", __LINE__, \
        (int)clock() / 1000);                                       \
  }
#define ilog(...) \
  { fprintf(stderr, __VA_ARGS__); }
#define llog(...)      \
  {                    \
    log_prefix();      \
    ilog(__VA_ARGS__); \
  }
#define log(...)           \
  {                        \
    log_prefix();          \
    ilog(__VA_ARGS__);     \
    fprintf(stderr, "\n"); \
  }
#define red(...)       \
  {                    \
    log_prefix();      \
    ilog("\033[31m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define green(...)     \
  {                    \
    log_prefix();      \
    ilog("\033[32m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define blue(...)      \
  {                    \
    log_prefix();      \
    ilog("\033[34m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }

#else

#define log_prefix() ;
#define ilog(...) ;
#define llog(...) ;
#define log(...) ;
#define red(...) ;
#define green(...) ;
#define blue(...) ;

#endif

namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/

int n;
int p[100000];
int vis[100000];

int queen() {
  FOR(i, 0, n * n - 1) vis[i] = 0;
  int pos = min_element(p, p + n * n) - p, vun = 0;
  vis[pos] = 1;
  FOR(_, 1, n * n - 2) {
    int fl = 0, mn = -1;
    FOR(i, 0, n * n - 1) if (!vis[i]) {
      int a = pos / n, b = pos % n, c = i / n, d = i % n;
      if (a == c || b == d || a + b == c + d || a - b == c - d) {
        if (mn == -1 || p[mn] > p[i]) mn = i;
        fl = 1;
      }
    }
    if (fl) {
      pos = mn;
      vis[pos] = 1;
    } else {
      ++vun;
      mn = -1;
      FOR(i, 0, n * n - 1) if (!vis[i]) {
        if (mn == -1 || p[mn] > p[i]) mn = i;
      }
      pos = mn;
      vis[pos] = 1;
    }
  }
  return vun;
}
int rook() {
  FOR(i, 0, n * n - 1) vis[i] = 0;
  int pos = min_element(p, p + n * n) - p, vun = 0;
  vis[pos] = 1;
  FOR(_, 1, n * n - 2) {
    int fl = 0, mn = -1;
    FOR(i, 0, n * n - 1) if (!vis[i]) {
      int a = pos / n, b = pos % n, c = i / n, d = i % n;
      if (a == c || b == d) {
        if (mn == -1 || p[mn] > p[i]) mn = i;
        fl = 1;
      }
    }
    if (fl) {
      pos = mn;
      vis[pos] = 1;
    } else {
      ++vun;
      mn = -1;
      FOR(i, 0, n * n - 1) if (!vis[i]) {
        if (mn == -1 || p[mn] > p[i]) mn = i;
      }
      pos = mn;
      vis[pos] = 1;
    }
  }
  return vun;
}
int main() {
  cin >> n;
  FOR(i, 0, n * n - 1) p[i] = i;
  while (1) {
    random_shuffle(p, p + n * n);
    if (rook() < queen()) break;
  }
  FOR(i, 0, n * n - 1) printf("%d%c", p[i] + 1, " \n"[i % n == n - 1]);
  return 0;
}
