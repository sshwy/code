#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
namespace IO {
  char nc() {
    static char bf[100000], *p1 = bf, *p2 = bf;
    return p1 == p2 && (p2 = (p1 = bf) + fread(bf, 1, 100000, stdin), p1 == p2)
               ? EOF
               : *p1++;
  }
  int rd() {
    int res = 0;
    char c = getchar();
    while (!isdigit(c)) c = getchar();
    while (isdigit(c)) res = res * 10 + c - '0', c = getchar();
    return res;
  }
} // namespace IO
/******************heading******************/
#define int lld
const int N = 503;

int n;
int a[N][N], x[N];
int ans[N], vis[N];

signed main() {
  cin >> n;
  FOR(i, 1, n) FOR(j, 1, n) cin >> a[i][j];
  FOR(i, 1, n) cin >> x[i];
  reverse(x + 1, x + n + 1);
  FOR(_k, 1, n) {
    int k = x[_k];
    vis[k] = 1;
    FOR(i, 1, n) {
      FOR(j, 1, n) {
        a[i][j] = min(a[i][j], a[i][k] + a[k][j]);
        if (vis[i] && vis[j]) ans[_k] += a[i][j];
      }
    }
  }
  reverse(ans + 1, ans + n + 1);
  FOR(i, 1, n) cout << ans[i] << ' ';
  return 0;
}
