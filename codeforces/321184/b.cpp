// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

const int N = 1e6 + 5, SZ = N << 2, INF = 1e9;

int n, C, a[N], b[N];
int mx[SZ];

void pushup(int u) { mx[u] = max(mx[u << 1], mx[u << 1 | 1]); }

void build(int u, int l, int r) {
  if (l == r) {
    mx[u] = 0;
    return;
  }
  int mid = (l + r) >> 1;
  build(u << 1, l, mid);
  build(u << 1 | 1, mid + 1, r);
  pushup(u);
}

int lowerBound(int v, int u, int l, int r) {
  if (mx[u] < v) return -1;
  if (l == r) return l;
  int mid = (l + r) >> 1;
  int x = lowerBound(v, u << 1, l, mid);
  if (x != -1) return x;
  return lowerBound(v, u << 1 | 1, mid + 1, r);
}

void add(int pos, int v, int u, int l, int r) {
  if (l == r) {
    mx[u] += v;
    return;
  }
  int mid = (l + r) >> 1;
  if (pos <= mid)
    add(pos, v, u << 1, l, mid);
  else
    add(pos, v, u << 1 | 1, mid + 1, r);
  pushup(u);
}

void work1() {
  int ans = 0;

  build(1, 1, n);

  FOR(i, 1, n) {
    int pos = lowerBound(a[i], 1, 1, n);
    if (pos == -1) {
      ++ans;
      add(ans, C - a[i], 1, 1, n);
    } else {
      add(pos, -a[i], 1, 1, n);
    }
  }
  printf("%d ", ans);
}
multiset<int> s;
void work2() {
  s.clear();
  int ans = 0;
  FOR(i, 1, n) {
    auto p = s.lower_bound(a[i]);
    if (p == s.end()) {
      s.insert(C - a[i]);
    } else {
      int bin = *p - a[i];
      s.erase(p);
      s.insert(bin);
    }
  }
  printf("%lu\n", s.size());
}
void go() {
  scanf("%d%d", &n, &C);
  FOR(i, 1, n) { scanf("%d", a + i); }
  work1();
  work2();
}

int main() {
  int t;
  scanf("%d", &t);
  FOR(i, 1, t) go();
  return 0;
}
