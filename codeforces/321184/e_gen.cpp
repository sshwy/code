// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

using namespace RA;

void go() {
  int n = rnd(1, 10);
  printf("%d\n", n);
  FOR(i, 1, n) printf("%d%c", rnd(1, 100), " \n"[i == n]);
  int q = rnd(1, 10);
  printf("%d\n", q);
  FOR(i, 1, q) {
    int l = rnd(1, n), r = rnd(1, n);
    if (l > r) swap(l, r);
    int k = rnd(1, r - l + 1);
    printf("%d %d %d\n", l, r, k);
  }
}
int main() {
  srand(clock() + time(0));
  int t = 10;
  printf("%d\n", t);
  FOR(i, 1, t) go();
  return 0;
}
