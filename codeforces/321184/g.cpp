// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

const int N = 5000;
int sx, sy, tx, ty, vf, vp, vh;
int n, x[N], y[N], v[N];

double dist[N];
bool vis[N];

double sq(int x) { return 1.0 * x * x; }
double distance(int a, int b) { return sqrt(sq(x[a] - x[b]) + sq(y[a] - y[b])); }

void dijk() {
  FOR(i, 0, n + 1) dist[i] = 1e99;
  FOR(i, 0, n + 1) vis[i] = false;
  dist[0] = 0;
  FOR(i, 0, n + 1) {
    int u = -1;
    FOR(j, 0, n + 1) if (!vis[j]) {
      if (u == -1 || dist[u] > dist[j]) u = j;
    }
    assert(u != -1);
    vis[u] = true;
    // printf("dist %d %lf\n", u, dist[u]);
    if (vp >= v[u]) continue;
    FOR(j, 0, n + 1) if (!vis[j]) {
      double duj = distance(u, j);
      dist[j] = min(dist[j], dist[u] + duj / vh * vp / (v[u] - vp) + duj / vh);
    }
  }
}
void go() {
  scanf("%d%d%d%d", &sx, &sy, &tx, &ty);
  scanf("%d%d%d", &vf, &vp, &vh);
  scanf("%d", &n);
  FOR(i, 0, n) { scanf("%d%d%d", &x[i], &y[i], &v[i]); }
  x[n + 1] = tx, y[n + 1] = ty, v[n + 1] = 0;
  dijk();
  printf("%.12lf\n", dist[n + 1]);
}

int main() {
  int t;
  scanf("%d", &t);
  FOR(i, 1, t) go();
  return 0;
}
