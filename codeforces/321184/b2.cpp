// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

const int N = 1e6 + 6;

int t, n, C;
int a[N];
vector<int> v;

void work1() {
  v.clear();
  FOR(i, 1, n) {
    bool flag = false;
    for (auto &e : v)
      if (e >= a[i]) {
        e -= a[i];
        flag = true;
        break;
      }
    if (!flag) { v.pb(C - a[i]); }
  }
  printf("%lu ", v.size());
}
multiset<int> s;
void work2() {
  s.clear();
  int ans = 0;
  FOR(i, 1, n) {
    auto p = s.lower_bound(a[i]);
    if (p == s.end()) {
      ++ans;
      s.insert(C - a[i]);
    } else {
      int bin = *p - a[i];
      s.erase(p);
      s.insert(bin);
    }
  }
  printf("%d\n", ans);
}
void go() {
  scanf("%d%d", &n, &C);
  FOR(i, 1, n) scanf("%d", a + i);
  work1();
  work2();
}

int main() {
  scanf("%d", &t);
  FOR(i, 1, t) go();
  return 0;
}
