// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

const int N = 1e5 + 5, W = 1e6 + 5, SZ = W * 23;
typedef long long LL;

int n, a[N], b[N], q;
LL f2[N];

void go() {
  scanf("%d", &n);
  FOR(i, 1, n) scanf("%d", &a[i]);
  scanf("%d", &q);
  FOR(i, 1, q) {
    int l, r, k;
    scanf("%d%d%d", &l, &r, &k);
    FOR(j, l, r) b[j] = a[j];
    sort(b + l, b + r + 1);
    LL ans = f2[r - l + 1];
    FOR(j, 1, k) ans += b[r - j + 1];
    printf("%lld\n", ans);
  }
}
int main() {
  int t;
  scanf("%d", &t);
  FOR(i, 1, N - 1) f2[i] = f2[i - 1] + i * 1ll * i;
  FOR(i, 1, t) go();
  return 0;
}
