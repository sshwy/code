// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

const int N = 1e5 + 5, W = 1e6 + 5, SZ = W * 23;
typedef long long LL;

int tot, lc[SZ], rc[SZ], cnt[SZ];
LL sum[SZ];

int cp(int u) {
  int u2 = ++tot;
  assert(tot < SZ);
  cnt[u2] = cnt[u], sum[u2] = sum[u];
  lc[u2] = lc[u], rc[u2] = rc[u];
  return u2;
}
int modify(int u, int l, int r, int v) {
  int u2 = cp(u);
  ++cnt[u2];
  sum[u2] += v;
  if (l == r) { return u2; }
  int mid = (l + r) >> 1;
  if (v <= mid)
    lc[u2] = modify(lc[u], l, mid, v);
  else
    rc[u2] = modify(rc[u], mid + 1, r, v);
  // pushup(u2);
  return u2;
}
LL query(int lu, int ru, int k, int l, int r) {
  if (l == r) {
    return (sum[ru] - sum[lu]) / (cnt[ru] - cnt[lu]) * min(cnt[ru] - cnt[lu], k);
  }
  if (cnt[ru] - cnt[lu] <= k) { return sum[ru] - sum[lu]; }
  assert(lu || ru);
  int mid = (l + r) >> 1, rcnt = cnt[rc[ru]] - cnt[rc[lu]];
  LL rsum = sum[rc[ru]] - sum[rc[lu]];
  if (k <= rcnt)
    return query(rc[lu], rc[ru], k, mid + 1, r);
  else
    return query(lc[lu], lc[ru], k - rcnt, l, mid) + rsum;
}

int n, rt[N], q;
LL f2[N];

void init() { tot = 0; }

void go() {
  const int maxw = 1e6;
  scanf("%d", &n);

  rt[0] = 0;
  init();
  FOR(i, 1, n) {
    int x;
    scanf("%d", &x);
    rt[i] = modify(rt[i - 1], 1, maxw, x);
    // printf("rt[%d] = %d, sum = %lld\n", i, rt[i],  sum[rt[i]]);
  }
  scanf("%d", &q);
  FOR(i, 1, q) {
    int l, r, k;
    scanf("%d%d%d", &l, &r, &k);
    // printf("l %d r %d k %d\n", l, r, k);
    LL ans = query(rt[l - 1], rt[r], k, 1, maxw) + f2[r - l + 1];
    // printf("f2 %d = %lld\n", r-l+1, f2[r-l+1]);
    printf("%lld\n", ans);
  }
}
int main() {
  int t;
  scanf("%d", &t);
  FOR(i, 1, N - 1) f2[i] = f2[i - 1] + i * 1ll * i;
  FOR(i, 1, t) go();
  return 0;
}
