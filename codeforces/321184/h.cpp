// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace cg {
  typedef long double vtyp;
  const vtyp eps = 1e-8;
  bool isZero(vtyp x) { return -eps < x && x < eps; }
  bool equal(vtyp x, vtyp y) { return isZero(x - y); }
  bool lt(vtyp x, vtyp y) { return !equal(x, y) && x < y; }
  bool gt(vtyp x, vtyp y) { return !equal(x, y) && x > y; }
  struct vec {
    vtyp x, y;
    vec() { x = y = 0; }
    vec(vtyp _x, vtyp _y) { x = _x, y = _y; }
    vec operator+(const vec V) const { return vec(x + V.x, y + V.y); }
    vec operator-() const { return vec(-x, -y); }
    vec operator-(const vec V) const { return *this + (-V); }
    vec operator*(const vtyp a) const { return vec(x * a, y * a); }
    friend vec operator*(const vtyp a, const vec v) { return v * a; }
    vec operator/(const vtyp a) const { return vec(x / a, y / a); }
    operator bool() const { return !(isZero(x) && isZero(y)); }
    bool operator==(const vec V) const { return bool(*this - V) == 0; }
    vtyp length() { return sqrt(x * x + y * y); }
    /**
     * 方向角，单位 rad
     */
    vtyp ang() const { return atan2(y, x); }
    /**
     * 方向向量
     * @return 0向量或者一个单位向量
     */
    vec dir() const {
      if (*this) {
        vtyp ang = atan2(y, x);
        return vec(cos(ang), sin(ang));
      } else
        return vec(0, 0);
    }
    // void read(){ scanf("%Lf%Lf",&x,&y); }
  };
  typedef vec point;

  struct line {
    point p1, p2;
    line() {}
    line(point _p1, point _p2) { p1 = _p1, p2 = _p2; }
  };
  typedef line segment;

  istream &operator>>(istream &in, vec &v) { return in >> v.x >> v.y, in; }
  ostream &operator<<(ostream &out, const vec &v) {
    return out << v.x << " " << v.y, out;
  }
  ifstream &operator>>(ifstream &in, vec &v) { return in >> v.x >> v.y, in; }
  ofstream &operator<<(ofstream &out, const vec &v) {
    return out << v.x << " " << v.y, out;
  }
  /**
   * 点积
   * a dot b == |a||b|cos theta
   */
  vtyp dot(const vec a, const vec b) { return a.x * b.x + a.y * b.y; }
  /**
   * 叉积
   * 两个向量围成的有向面积
   */
  vtyp det(const vec a, const vec b) { return a.x * b.y - a.y * b.x; }

  /**
   * 投影
   * @param L 直线
   * @param p 要求投影的点
   * @return p 在 L 上的投影坐标（即垂足）
   */
  point projection(line L, point p) {
    vec d = L.p2 - L.p1;
    return L.p1 + (dot(d, p - L.p1) / d.length()) * d.dir();
  }
  /**
   * 对称点
   * @param L 直线
   * @param p 点
   * @return p 关于直线 L 的对称点
   */
  point reflection(line L, point p) {
    point o = projection(L, p);
    return vtyp(2) * (o - p) + p;
  }

  /**
   * 判断向量是否平行
   */
  bool parallel(vec a, vec b) { return isZero(det(a, b)); }
  /**
   * 判断直线是否平行
   */
  bool parallel(line a, line b) { return parallel(a.p2 - a.p1, b.p2 - b.p1); }
  /**
   * 判断向量是否垂直
   */
  bool orthogonal(vec a, vec b) { return isZero(dot(a, b)); }
  /**
   * 判断直线是否垂直
   */
  bool orthogonal(line a, line b) { return orthogonal(a.p2 - a.p1, b.p2 - b.p1); }
  /**
   * 判断点 p 是否在直线L上
   */
  bool online(line L, point p) { return parallel(L.p2 - L.p1, p - L.p1); }
  /**
   * 判断两直线是否重合
   */
  bool coincident(line a, line b) { return online(a, b.p1) && online(a, b.p2); }
  /**
   * 判断点 p 是否与有向线段共线且在反向延长线上
   */
  bool online_back(segment sl, point p) {
    vec a = sl.p2 - sl.p1, b = p - sl.p1;
    return parallel(a, b) && lt(dot(a, b), 0);
  }
  /**
   * 判断点 p 是否与有向线段共线且在正向延长线上
   */
  bool online_front(segment sl, point p) {
    vec a = sl.p1 - sl.p2, b = p - sl.p2; // 倒过来
    return parallel(a, b) && lt(dot(a, b), 0);
  }
  /**
   * 判断点 p 是否在线段上（含端点）
   */
  bool on_segment(segment sl, point p) {
    return online(sl, p) && !online_back(sl, p) && !online_front(sl, p);
  }
  /**
   * 两条直线的交点
   * 需确保两条直线不平行
   */
  point intersection(line a, line b) {
    assert(!parallel(a, b));
    vtyp x = det(a.p1 - b.p1, b.p2 - b.p1);
    vtyp y = det(b.p2 - b.p1, a.p2 - b.p1);
    return a.p1 + (a.p2 - a.p1) * x / (x + y);
  }
  /**
   * 判断两个线段是否相交（含边界）
   */
  bool check_segment_intersection(segment a, segment b) {
    if (cg::coincident(a, b)) {
      if (on_segment(a, b.p1) || on_segment(a, b.p2) || on_segment(b, a.p1) ||
          on_segment(b, a.p2))
        return true;
      else
        return false;
    } else if (cg::parallel(a, b)) {
      return false;
    } else {
      point o = cg::intersection(a, b);
      if (cg::on_segment(a, o) && cg::on_segment(b, o))
        return true;
      else
        return false;
    }
  }
  /**
   * 两个点的距离
   */
  vtyp distance(point a, point b) { return (b - a).length(); }
  /**
   * 两个线段的距离
   */
  vtyp distance(segment a, segment b) {
    if (check_segment_intersection(a, b)) return 0;
    vtyp res = distance(a.p1, b.p1);
    res = min(res, distance(a.p1, b.p2));
    res = min(res, distance(a.p2, b.p1));
    res = min(res, distance(a.p2, b.p2));
    point o;
    if (o = projection(b, a.p1), on_segment(b, o)) res = min(res, distance(a.p1, o));
    if (o = projection(b, a.p2), on_segment(b, o)) res = min(res, distance(a.p2, o));
    if (o = projection(a, b.p1), on_segment(a, o)) res = min(res, distance(b.p1, o));
    if (o = projection(a, b.p2), on_segment(a, o)) res = min(res, distance(b.p2, o));
    return res;
  }
  /**
   * 求简单多边形面积
   * @param g 多边形顶点集
   */
  vtyp area(vector<point> g) {
    vtyp res = 0;
    for (unsigned i = 0; i < g.size(); i++) {
      res += det(g[i], g[(i + 1) % g.size()]);
    }
    res /= 2;
    return abs(res);
  }
  /**
   * 判断点p与多边形的包含关系
   * @param g 多边形顶点集
   * @return 0 表示在多边形外，1 表示在边上，2表示在多边形内
   */
  int polygon_point_containment(vector<point> g, point p) {
    line L(vec(p.x - 1, p.y), p); // 水平方向的射线
    int cnt = 0;
    for (unsigned i = 0; i < g.size(); i++) {
      int j = (i + 1) % g.size();
      line e(g[i], g[j]);
      if (on_segment(e, p)) return 1;
      if (parallel(L, e)) {
        // do nothing.
      } else if (online_front(L, g[i])) {
        if (g[i].y > g[j].y) ++cnt;
      } else if (online_front(L, g[j])) {
        if (g[j].y > g[i].y) ++cnt;
      } else {
        point o = intersection(L, e);
        if (on_segment(e, o) && online_front(L, o)) ++cnt;
      }
    }
    if (cnt % 2) return 2;
    return 0;
  }
} // namespace cg
using cg::line;
using cg::point;
using cg::segment;

typedef pair<long double, long double> range;
const int N = 505;
int n, m;

point a[N];
vector<point> v;
segment b[N];

const line Xaxis(point(0, 0), point(100, 0));

vector<range> vrng;

void work(point e, segment s) {
  vector<point> key;

  if (!parallel(Xaxis, line(e, s.p1))) {
    point p = cg::intersection(Xaxis, line(e, s.p1));
    key.pb(p);
  }

  if (!parallel(Xaxis, line(e, s.p2))) {
    point p = cg::intersection(Xaxis, line(e, s.p2));
    key.pb(p);
  }

  if (key.empty()) return;
  if (key.size() == 1) {
    if (check_segment_intersection(s, segment(point(key[0].x - 10, 0), e))) {
      vrng.pb({-1e50, key[0].x});
    }
    if (check_segment_intersection(s, segment(point(key[0].x + 10, 0), e))) {
      vrng.pb({key[0].x, 1e50});
    }
    return;
  }

  if (cg::equal(key[0].x, key[1].x)) return;

  assert(key.size() == 2);
  if (key[0].x > key[1].x) swap(key[0], key[1]);

  if (check_segment_intersection(s, segment(point(key[0].x - 10, 0), e))) {
    vrng.pb({-1e50, key[0].x});
  }
  if (check_segment_intersection(s, segment(point(key[1].x + 10, 0), e))) {
    vrng.pb({key[1].x, 1e50});
  }
  if (check_segment_intersection(
          s, segment(point((key[0].x + key[1].x) / 2.0, 0), e))) {
    vrng.pb({key[0].x, key[1].x});
  }
}

long double ans = 0;
vector<range> rgs;
void add(range x) { rgs.pb(x); }

template <class T> int findIndex(vector<T> v, T val) {
  return lower_bound(v.begin(), v.end(), val) - v.begin();
}

void calc(int totStar) {
  sort(rgs.begin(), rgs.end());
  vector<long double> D;
  for (auto x : rgs) D.pb(x.first), D.pb(x.second);
  sort(D.begin(), D.end());
  vector<pair<int, int>> vss;
  for (auto x : rgs) {
    vss.pb({findIndex(D, x.first), 1});
    vss.pb({findIndex(D, x.second), -1});
  }
  sort(vss.begin(), vss.end());
  int cur = 0;
  for (unsigned i = 0; i < vss.size(); ++i) {
    cur += vss[i].second;
    if (i + 1 < vss.size() && cur == totStar) {
      ans += D[vss[i + 1].first] - D[vss[i].first];
    }
  }
}
void init() {
  rgs.clear();
  ans = 0;
}
void go() {
  init();
  scanf("%d%d", &n, &m);
  FOR(i, 1, n) {
    int x, y;
    scanf("%d%d", &x, &y);
    a[i] = point(x, y);
  }
  FOR(i, 1, m) {
    int t[4];
    FOR(j, 0, 3) scanf("%d", t + j);
    b[i] = segment(point(t[0], t[1]), point(t[2], t[3]));
  }
  v.clear();
  FOR(i, 1, n) {
    bool flag = true;
    FOR(j, 1, m) {
      if (cg::on_segment(b[j], a[i])) {
        flag = false;
        add({-1e50, 1e50});
        break;
      }
    }
    if (flag) v.pb(a[i]);
  }
  for (point e : v) {
    vrng.clear();
    FOR(j, 1, m) work(e, b[j]);
    if (vrng.empty()) { continue; }
    sort(vrng.begin(), vrng.end());
    range cur = vrng[0];
    for (auto x : vrng) {
      if (cur.second < x.first) {
        add(cur);
        cur = x;
      } else {
        cur.second = max(cur.second, x.second);
      }
    }
    add(cur);
  }
  calc(n);
  if (ans > 1e9)
    puts("-1");
  else {
    double Ans = ans;
    printf("%.9lf\n", Ans);
  }
}
int main() {
  int t;
  scanf("%d", &t);
  FOR(i, 1, t) go();
  return 0;
}
