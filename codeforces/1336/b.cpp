// by Sshwy
//#define DEBUGGER
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

#ifdef DEBUGGER

#define log_prefix()                                                \
  {                                                                 \
    fprintf(stderr, "\033[37mLine %-3d [%dms]:\033[0m  ", __LINE__, \
        (int)clock() / 1000);                                       \
  }
#define ilog(...) \
  { fprintf(stderr, __VA_ARGS__); }
#define llog(...)      \
  {                    \
    log_prefix();      \
    ilog(__VA_ARGS__); \
  }
#define log(...)           \
  {                        \
    log_prefix();          \
    ilog(__VA_ARGS__);     \
    fprintf(stderr, "\n"); \
  }
#define red(...)       \
  {                    \
    log_prefix();      \
    ilog("\033[31m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define green(...)     \
  {                    \
    log_prefix();      \
    ilog("\033[32m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define blue(...)      \
  {                    \
    log_prefix();      \
    ilog("\033[34m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }

#else

#define log_prefix() ;
#define ilog(...) ;
#define llog(...) ;
#define log(...) ;
#define red(...) ;
#define green(...) ;
#define blue(...) ;

#endif

namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 1e5 + 5;
int la, lb, lc;
int a[N], b[N], c[N];

void go() {
  cin >> la >> lb >> lc;
  // cerr<<endl;
  // printf("%d %d %d\n",la,lb,lc);
  FOR(i, 1, la) cin >> a[i];
  FOR(i, 1, lb) cin >> b[i];
  FOR(i, 1, lc) cin >> c[i];
  sort(a + 1, a + la + 1);
  sort(b + 1, b + lb + 1);
  sort(c + 1, c + lc + 1);
  a[0] = a[1], a[la + 1] = a[la];
  b[0] = b[1], b[lb + 1] = b[lb];
  c[0] = c[1], c[lc + 1] = c[lc];
  auto La = [&a, &la](int x) { return *(upper_bound(a + 1, a + la + 1, x) - 1); };
  auto Ra = [&a, &la](int x) { return *lower_bound(a + 1, a + la + 1, x); };
  auto Lb = [&b, &lb](int x) { return *(upper_bound(b + 1, b + lb + 1, x) - 1); };
  auto Rb = [&b, &lb](int x) { return *lower_bound(b + 1, b + lb + 1, x); };
  auto Lc = [&c, &lc](int x) { return *(upper_bound(c + 1, c + lc + 1, x) - 1); };
  auto Rc = [&c, &lc](int x) { return *lower_bound(c + 1, c + lc + 1, x); };

  auto calc = [](int x, int y, int z) {
    auto sq = [](int x) { return x * 1ll * x; };
    return sq(x - y) + sq(x - z) + sq(y - z);
  };
  long long ans = (1ll << 63) - 1;

  FOR(i, 1, la) {
    ans = min(ans, calc(a[i], Lb(a[i]), Rc(a[i])));
    ans = min(ans, calc(a[i], Rb(a[i]), Lc(a[i])));
  }
  FOR(i, 1, lb) {
    ans = min(ans, calc(b[i], La(b[i]), Rc(b[i])));
    ans = min(ans, calc(b[i], Ra(b[i]), Lc(b[i])));
  }
  FOR(i, 1, lc) {
    ans = min(ans, calc(c[i], La(c[i]), Rb(c[i])));
    ans = min(ans, calc(c[i], Ra(c[i]), Lb(c[i])));
  }
  cout << ans << endl;
}
int main() {
  int t;
  cin >> t;
  FOR(i, 1, t) go();
  return 0;
}
