// by Sshwy
//#define DEBUGGER
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

#ifdef DEBUGGER

#define log_prefix()                                                \
  {                                                                 \
    fprintf(stderr, "\033[37mLine %-3d [%dms]:\033[0m  ", __LINE__, \
        (int)clock() / 1000);                                       \
  }
#define ilog(...) \
  { fprintf(stderr, __VA_ARGS__); }
#define llog(...)      \
  {                    \
    log_prefix();      \
    ilog(__VA_ARGS__); \
  }
#define log(...)           \
  {                        \
    log_prefix();          \
    ilog(__VA_ARGS__);     \
    fprintf(stderr, "\n"); \
  }
#define red(...)       \
  {                    \
    log_prefix();      \
    ilog("\033[31m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define green(...)     \
  {                    \
    log_prefix();      \
    ilog("\033[32m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define blue(...)      \
  {                    \
    log_prefix();      \
    ilog("\033[34m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }

#else

#define log_prefix() ;
#define ilog(...) ;
#define llog(...) ;
#define log(...) ;
#define red(...) ;
#define green(...) ;
#define blue(...) ;

#endif

namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 2e5 + 5;
int n, k, fa[N], sz[N], val[N], dep[N], cnt[N], vis[N], inq[N];
vector<int> g[N];

priority_queue<pair<int, int>> q; // 小根堆

void dfs(int u, int p) {
  fa[u] = p;
  sz[u] = 1;
  if (p) dep[u] = dep[p] + 1;
  for (int v : g[u])
    if (v != p) dfs(v, u), sz[u] += sz[v];
  q.push({dep[u] - (sz[u] - 1), u});
}
void dfs2(int u, int p) {
  cnt[u] = vis[u];
  for (int v : g[u])
    if (v != p) dfs2(v, u), cnt[u] += cnt[v];
}
int main() {
  scanf("%d%d", &n, &k);
  FOR(i, 1, n - 1) {
    int u, v;
    scanf("%d%d", &u, &v);
    g[u].pb(v), g[v].pb(u);
  }
  dfs(1, 0);
  // int tot=0;
  FOR(_, 1, k) {
    auto u = q.top();
    q.pop();
    // remove u.se
    // fprintf(stderr,"re %d\n",u.se);
    vis[u.se] = 1;
  }
  dfs2(1, 0);
  long long ans = 0;
  FOR(i, 1, n) if (!vis[i]) ans += cnt[i];
  printf("%lld\n", ans);
  // fprintf(stderr,"tot %d\n",tot);

  return 0;
}
