// by Sshwy
//#define DEBUGGER
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

#ifdef DEBUGGER

#define log_prefix()                                                \
  {                                                                 \
    fprintf(stderr, "\033[37mLine %-3d [%dms]:\033[0m  ", __LINE__, \
        (int)clock() / 1000);                                       \
  }
#define ilog(...) \
  { fprintf(stderr, __VA_ARGS__); }
#define llog(...)      \
  {                    \
    log_prefix();      \
    ilog(__VA_ARGS__); \
  }
#define log(...)           \
  {                        \
    log_prefix();          \
    ilog(__VA_ARGS__);     \
    fprintf(stderr, "\n"); \
  }
#define red(...)       \
  {                    \
    log_prefix();      \
    ilog("\033[31m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define green(...)     \
  {                    \
    log_prefix();      \
    ilog("\033[32m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define blue(...)      \
  {                    \
    log_prefix();      \
    ilog("\033[34m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }

#else

#define log_prefix() ;
#define ilog(...) ;
#define llog(...) ;
#define log(...) ;
#define red(...) ;
#define green(...) ;
#define blue(...) ;

#endif

namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 5000, P = 998244353;
int n, m;
char s[N], t[N];

int f[N][N];

int main() {
  cin >> (s + 1);
  n = strlen(s + 1);
  cin >> (t + 1);
  m = strlen(t + 1);

  auto add = [](int &x, int y) { x = (x + y) % P; };
  FOR(i, 1, n) if (i > m || t[i] == s[1]) f[i][i] = 1;
  FOR(d, 0, n - 2) {
    FOR(i, 1, n - d) {
      int j = i + d;
      int pos = d + 2;
      if (i > 1 && (i - 1 > m || s[pos] == t[i - 1])) {
        add(f[i - 1][j], f[i][j]);
        // printf("(%d,%d) <= (%d,%d):%d\n",i-1,j,i,j,f[i][j]);
      }
      if (j < n && (j + 1 > m || s[pos] == t[j + 1])) {
        add(f[i][j + 1], f[i][j]);
        // printf("(%d,%d) <= (%d,%d):%d\n",i,j+1,i,j,f[i][j]);
      }
    }
  }
  int ans = 0;
  FOR(i, m, n) add(ans, f[1][i]); //匹配到T
  ans = ans * 2ll % P;
  printf("%d\n", ans);
  return 0;
}
