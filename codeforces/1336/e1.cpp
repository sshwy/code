// by Sshwy
//#define DEBUGGER
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

#ifdef DEBUGGER

#define log_prefix()                                                \
  {                                                                 \
    fprintf(stderr, "\033[37mLine %-3d [%dms]:\033[0m  ", __LINE__, \
        (int)clock() / 1000);                                       \
  }
#define ilog(...) \
  { fprintf(stderr, __VA_ARGS__); }
#define llog(...)      \
  {                    \
    log_prefix();      \
    ilog(__VA_ARGS__); \
  }
#define log(...)           \
  {                        \
    log_prefix();          \
    ilog(__VA_ARGS__);     \
    fprintf(stderr, "\n"); \
  }
#define red(...)       \
  {                    \
    log_prefix();      \
    ilog("\033[31m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define green(...)     \
  {                    \
    log_prefix();      \
    ilog("\033[32m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define blue(...)      \
  {                    \
    log_prefix();      \
    ilog("\033[34m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }

#else

#define log_prefix() ;
#define ilog(...) ;
#define llog(...) ;
#define log(...) ;
#define red(...) ;
#define green(...) ;
#define blue(...) ;

#endif

namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 2e5 + 5, P = 998244353, T = 24, SZ = 1 << T;
int n, m, k;
long long a[N], c[SZ], p[100];

long long b[100];
void insert(long long x) {
  ROF(i, 62, 0) {
    if (x >> i & 1) {
      if (!b[i]) return b[i] = x, void();
      x ^= b[i];
    }
  }
  assert(x == 0);
}
vector<long long> B;
void work() {
  ROF(i, 62, 0) if (b[i]) { FOR(j, i + 1, 62) if (b[j] >> i & 1) b[j] ^= b[i]; }
  FOR(i, 0, 62) if (b[i]) B.pb(b[i]), ++k;
}

long long pw(long long a, long long m) {
  long long res = 1;
  while (m) m & 1 ? res = res * a % P : 0, a = a * a % P, m >>= 1;
  return res;
}
void solve1() {
  // for(auto x:B)printf("%d ",x); puts("");
  int lim = 1 << k;
  FOR(i, 0, k - 1) c[1 << i] = B[i];
  FOR(i, 1, lim - 1) { c[i] = c[i ^ (i & -i)] ^ c[i & -i]; }
  FOR(i, 0, lim - 1) { p[__builtin_popcountll(c[i])]++; }
  long long coef = pw(2, n - k);
  FOR(i, 0, m) p[i] = p[i] * coef % P;
  FOR(i, 0, m) printf("%lld%c", p[i], " \n"[i == m]);
}
long long f[40][40][1 << (35 - T)];
long long trans(long long x) { //去掉key bots
  long long res = 0;
  ROF(i, m - 1, 0) if (b[i] == 0) res = res << 1 | (x >> i & 1);
  return res;
}
void solve2() {   // k>T
  f[0][0][0] = 1; //什么都不选的方案数是1
  long long lim = (1ll << (m - k));
  auto add = [](long long &x, long long y) { x = (x + y) % P; };
  FOR(i, 0, k - 1) { //线性基中前i个数
    long long c = trans(B[i]);
    FOR(j, 0, i) { //选了j个数（key bits有j个
      FOR(l, 0, lim - 1) {
        if (f[i][j][l]) {
          //第i+1个数不选
          add(f[i + 1][j][l], f[i][j][l]);
          //第i+1个数选
          add(f[i + 1][j + 1][l ^ c], f[i][j][l]);
        }
      }
    }
  }
  FOR(j, 0, k) FOR(l, 0, lim - 1) if (f[k][j][l]) {
    add(p[j + __builtin_popcount(l)], f[k][j][l]);
  }
  long long coef = pw(2, n - k);
  FOR(i, 0, m) p[i] = p[i] * coef % P;
  FOR(i, 0, m) printf("%lld%c", p[i], " \n"[i == m]);
}
int main() {
  scanf("%d%d", &n, &m);
  FOR(i, 1, n) scanf("%lld", &a[i]), insert(a[i]);
  work();

  if (k <= T)
    solve1();
  else
    solve2();
  return 0;
}
