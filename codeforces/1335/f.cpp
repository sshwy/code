// by Sshwy
//#define DEBUGGER
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

#ifdef DEBUGGER

#define log_prefix()                                                \
  {                                                                 \
    fprintf(stderr, "\033[37mLine %-3d [%dms]:\033[0m  ", __LINE__, \
        (int)clock() / 1000);                                       \
  }
#define ilog(...) \
  { fprintf(stderr, __VA_ARGS__); }
#define llog(...)      \
  {                    \
    log_prefix();      \
    ilog(__VA_ARGS__); \
  }
#define log(...)           \
  {                        \
    log_prefix();          \
    ilog(__VA_ARGS__);     \
    fprintf(stderr, "\n"); \
  }
#define red(...)       \
  {                    \
    log_prefix();      \
    ilog("\033[31m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define green(...)     \
  {                    \
    log_prefix();      \
    ilog("\033[32m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define blue(...)      \
  {                    \
    log_prefix();      \
    ilog("\033[34m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }

#else

#define log_prefix() ;
#define ilog(...) ;
#define llog(...) ;
#define log(...) ;
#define red(...) ;
#define green(...) ;
#define blue(...) ;

#endif

namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int NM = 1e6 + 5;
int n, m;
vector<string> c, s;
vector<vector<int>> vis, id;
int pre[NM], cnt[NM][2];

int P(int x, int y) { return x * m + y; }
int X(int p) { return p / m; }
int Y(int p) { return p % m; }

int dfs(int x, int y, int tim) {
  vis[x][y] = tim;
  int nx = x, ny = y;
  if (s[x][y] == 'U')
    --nx;
  else if (s[x][y] == 'R')
    ++ny;
  else if (s[x][y] == 'D')
    ++nx;
  else
    --ny;
  if (vis[nx][ny]) {
    if (vis[nx][ny] == tim) {
      id[x][y] = P(x, y);
      pre[P(nx, ny)] = P(x, y);
      return P(nx, ny);
    } else {
      id[x][y] = pre[id[nx][ny]]; //环上的pre
      return -1;
    }
  } else {
    int t = dfs(nx, ny, tim);
    if (t == -1) {
      id[x][y] = pre[id[nx][ny]]; //环上的pre
      return -1;
    } else {
      id[x][y] = P(x, y);
      pre[P(nx, ny)] = P(x, y);
      if (t == P(x, y)) return -1; //到环的起点了
      return t;
    }
  }
}
void go() {
  cin >> n >> m;
  c = vector<string>(n), s = vector<string>(n);
  vis = vector<vector<int>>(n, vector<int>(m, 0));
  id = vector<vector<int>>(n, vector<int>(m, -1));
  for (auto &x : c) cin >> x;
  for (auto &x : s) cin >> x;
  int t = 0;
  FOR(i, 0, n - 1) FOR(j, 0, m - 1) {
    if (!vis[i][j]) { dfs(i, j, ++t); }
    // printf("(%d,%d)%c",X(id[i][j]),Y(id[i][j])," \n"[j==m-1]);
  }
  FOR(i, P(0, 0), P(n - 1, m - 1)) cnt[i][0] = cnt[i][1] = 0;
  FOR(i, 0, n - 1) FOR(j, 0, m - 1) {
    if (c[i][j] == '0')
      cnt[id[i][j]][0]++;
    else
      cnt[id[i][j]][1]++;
  }
  int ans = 0, a2 = 0;
  FOR(i, 0, n - 1) FOR(j, 0, m - 1) {
    int x = P(i, j);
    if (cnt[x][0] || cnt[x][1]) ++ans;
    if (cnt[x][0]) ++a2;
  }
  cout << ans << " " << a2 << endl;
}
int main() {
  ios::sync_with_stdio(0);
  cin.tie(0);
  cout.tie(0);
  int t;
  cin >> t;
  FOR(i, 1, t) go();
  return 0;
}
