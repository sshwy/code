// by Sshwy
//#define DEBUGGER
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

#ifdef DEBUGGER

#define log_prefix()                                                \
  {                                                                 \
    fprintf(stderr, "\033[37mLine %-3d [%dms]:\033[0m  ", __LINE__, \
        (int)clock() / 1000);                                       \
  }
#define ilog(...) \
  { fprintf(stderr, __VA_ARGS__); }
#define llog(...)      \
  {                    \
    log_prefix();      \
    ilog(__VA_ARGS__); \
  }
#define log(...)           \
  {                        \
    log_prefix();          \
    ilog(__VA_ARGS__);     \
    fprintf(stderr, "\n"); \
  }
#define red(...)       \
  {                    \
    log_prefix();      \
    ilog("\033[31m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define green(...)     \
  {                    \
    log_prefix();      \
    ilog("\033[32m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define blue(...)      \
  {                    \
    log_prefix();      \
    ilog("\033[34m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }

#else

#define log_prefix() ;
#define ilog(...) ;
#define llog(...) ;
#define log(...) ;
#define red(...) ;
#define green(...) ;
#define blue(...) ;

#endif

namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 2e5 + 5, W = 205;
int a[N], n;
int c[N], L[N], R[N];

void go() {
  cin >> n;
  FOR(i, 1, n) cin >> a[i];
  fill(c, c + 200 + 1, 0);
  FOR(i, 1, n) c[a[i]]++;
  int ans = *max_element(c, c + 200 + 1), lim = n / 2;
  FOR(i, 1, 200) {
    fill(L, L + n + 1, 0);
    fill(R, R + n + 1, 0);
    {
      int x = 0;
      FOR(j, 1, n) if (a[j] == i) L[++x] = j;
    }
    {
      int x = 0;
      ROF(j, n, 1) if (a[j] == i) R[++x] = j;
    }
    fill(c, c + 200 + 1, 0);
    int cl = -1, cr = -1, mx = 0;
    FOR(x, 1, lim) {
      if (L[x] && R[x] && L[x] + 1 <= R[x] - 1) {
        cl = L[x] + 1, cr = R[x] - 1;
      } else
        break;
    }
    if (cl == -1) continue;
    FOR(j, cl, cr) mx = max(mx, ++c[a[j]]);
    ROF(x, lim, 1) {
      if (L[x] && R[x] && L[x] + 1 <= R[x] - 1) {
        int l = L[x] + 1, r = R[x] - 1;
        while (l < cl) mx = max(mx, ++c[a[--cl]]);
        while (cr < r) mx = max(mx, ++c[a[++cr]]);
        ans = max(ans, x * 2 + mx);
      }
    }
  }
  cout << ans << endl;
}
int main() {
  ios::sync_with_stdio(false);
  int t;
  cin >> t;
  FOR(i, 1, t) go();
  return 0;
}
