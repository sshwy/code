// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

const int N = 2005;

struct Point {
  int x, y, w, id;
} a[N];
int n, fa[N], p[N], la;
vector<pair<int, int>> ans;

int get(int u) { return fa[u] == u ? u : fa[u] = get(fa[u]); }
void merge(int u, int v) {
  u = get(u), v = get(v);
  if (u == v) return;
  fa[u] = v;
}

bool cmp1(Point p, Point q) { return p.x < q.x; }
bool cmp2(Point p, Point q) { return 1ll * p.x * q.y - 1ll * q.x * p.y < 0; }

int main() {
  scanf("%d", &n);
  FOR(i, 1, n) {
    int x, y, w;
    scanf("%d%d%d", &x, &y, &w);
    if (w != i) a[++la] = {x, y, w, i};
  }
  if (!la) return puts("0"), 0;
  sort(a + 1, a + la + 1, cmp1);
  FOR(i, 2, la) a[i].x -= a[1].x, a[i].y -= a[1].y;
  a[1].x = a[1].y = 0;
  sort(a + 2, a + la + 1, cmp2);
  FOR(i, 1, n) fa[i] = i;
  FOR(i, 1, la) merge(a[i].w, a[i].id);
  FOR(i, 2, la - 1) {
    if (get(a[i].w) != get(a[i + 1].w)) {
      ans.pb({a[i].id, a[i + 1].id});
      swap(a[i].w, a[i + 1].w);
      merge(a[i].w, a[i + 1].w);
    }
  }
  FOR(i, 1, la) { p[a[i].id] = a[i].w; }
  int x = a[1].id;
  while (p[x] != x) {
    ans.pb({x, p[x]});
    swap(p[x], p[p[x]]);
  }
  printf("%lu\n", ans.size());
  for (auto pr : ans) printf("%d %d\n", pr.first, pr.second);
  return 0;
}
