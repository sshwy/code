// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

const int N = 2e5 + 5, N2 = 1005;

struct dsu {
  int fa[N], sz[N];
  void init(int n) { FOR(i, 0, n) fa[i] = i, sz[i] = 1; }
  int get(int u) { return fa[u] == u ? u : fa[u] = get(fa[u]); }
  int getSize(int u) { return sz[get(u)]; }
  void mergeTo(int u, int v) {
    u = get(u), v = get(v);
    fa[u] = v;
    sz[v] += sz[u];
  }
} d, d2, d3, d4;

int n, m, sxor;

struct Edge {
  int u, v, w;
} E[N];
bool cmp(Edge x, Edge y) { return x.w < y.w; }
vector<int> s, s2;
vector<int> to[N];
int cnt[N];
bool has[N2][N2];
void work1() {
  FOR(i, 1, m) {
    int u = E[i].u, v = E[i].v;
    if (u > v) swap(u, v);
    to[v].pb(u);
  }
  d4.init(n);
  FOR(v, 1, n) {
    for (int u : s) cnt[u] = 0;
    for (int u : to[v]) cnt[d4.get(u)]++;
    for (int u : s) {
      if (cnt[u] == d4.getSize(u)) {
        s2.pb(u);
      } else {
        d4.mergeTo(u, v);
      }
    }
    s2.pb(v);
    s = s2;
    s2.clear();
  }
  long long ans = 0;
  FOR(i, 1, m) {
    int u = E[i].u, v = E[i].v, w = E[i].w;
    if (d4.get(u) != d4.get(v)) {
      ans += w;
      d4.mergeTo(u, v);
    }
  }
  printf("%lld\n", ans);
}

vector<int> g[N2];

int fa[N2], val[N2], dep[N2];
void dfs(int u, int p) {
  fa[u] = p;
  dep[u] = dep[p] + 1;
  for (int v : g[u])
    if (v != p) { dfs(v, u); }
}

int main() {
  scanf("%d%d", &n, &m);
  FOR(i, 1, m) {
    int u, v, w;
    scanf("%d%d%d", &u, &v, &w);
    E[i] = {u, v, w};
    sxor ^= w;
  }
  sort(E + 1, E + m + 1, cmp);
  if (1ll * n * (n - 1) / 2 - m >= n) {
    work1();
    return 0;
  }
  assert(n < 1000);
  d.init(n);
  FOR(i, 1, m) {
    int u = E[i].u, v = E[i].v;
    has[u][v] = has[v][u] = true;
  }
  FOR(i, 1, n) FOR(j, i + 1, n) if (!has[i][j]) {
    if (d.get(i) == d.get(j)) {
      work1();
      return 0;
    }
    d.mergeTo(i, j);
    g[i].pb(j);
    g[j].pb(i);
  }
  d2.init(n);
  d3.init(n);
  FOR(i, 1, n) val[i] = 2e9;
  long long ans = 0;
  FOR(i, 1, m) {
    int u = E[i].u, v = E[i].v, w = E[i].w;
    if (d.get(u) == d.get(v)) {
    } else {
      if (d3.get(d.get(u)) != d3.get(d.get(v))) {
        ans += w;
        d3.mergeTo(d.get(u), d.get(v));
        g[u].pb(v);
        g[v].pb(u);
      }
    }
  }
  dfs(1, 0);
  FOR(i, 2, n) {
    if (has[fa[i]][i]) { d2.mergeTo(i, fa[i]); }
  }
  int tw = -1;
  FOR(i, 1, m) {
    int u = E[i].u, v = E[i].v, w = E[i].w;
    if (d2.get(u) != d2.get(v)) {
      tw = w;
      break;
    }
  }
  if (tw != -1)
    ans += min(sxor, tw);
  else
    ans += sxor;
  printf("%lld\n", ans);
  return 0;
}
