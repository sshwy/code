// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

typedef long long LL;
const LL INF = 2e18;

LL pls(LL x, LL y) { return min(x + y, INF); }

const int N = 2e5 + 5;

LL f[N], sf[N], n, k;
int ans[N];

void work(LL cur, LL k) {
  if (cur >= n) return;
  // printf("work %lld %lld\n", cur, k);
  int rest = n - cur + 1;
  FOR(i, 1, rest) {
    // printf("f[%d] = %lld\n", rest - i, f[rest - i]);
    // printf("k %lld\n", k);
    if (f[rest - i] >= k) {
      reverse(ans + cur, ans + cur + i);
      work(cur + i, k);
      return;
    } else {
      k -= f[rest - i];
    }
  }
}
void go() {
  scanf("%lld%lld", &n, &k);
  if (f[n] < k) {
    puts("-1");
    return;
  }
  FOR(i, 1, n) ans[i] = i;
  work(1, k);
  FOR(i, 1, n) printf("%d%c", ans[i], " \n"[i == n]);
}

void init() {
  f[0] = 1, sf[0] = 1;
  f[1] = 1, sf[1] = 2;
  FOR(i, 2, 1e5) {
    f[i] = sf[i - 1];
    sf[i] = pls(f[i], sf[i - 1]);
  }
}
int main() {
  init();
  int t;
  scanf("%d", &t);
  FOR(i, 1, t) go();
  return 0;
}
