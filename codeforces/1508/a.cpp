// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA
const int N = 3e5 + 5;
int n, c[3][2];
char s[3][N];

char ans[N];
int la;

void work(int x, int y) { // 1
  int p1[N] = {0}, p2[N] = {0};
  int l1 = 0, l2 = 0;
  FOR(i, 1, n * 2) {
    if (s[x][i] == '1') p1[++l1] = i;
    if (s[y][i] == '1') p2[++l2] = i;
  }
  assert(l1 >= n && l2 >= n);
  p1[0] = p2[0] = 0;
  FOR(i, 1, n) {
    FOR(j, p1[i - 1] + 1, p1[i] - 1) ans[++la] = s[x][j];
    FOR(j, p2[i - 1] + 1, p2[i] - 1) ans[++la] = s[y][j];
    ans[++la] = '1';
  }
  FOR(j, p1[n] + 1, n * 2) ans[++la] = s[x][j];
  FOR(j, p2[n] + 1, n * 2) ans[++la] = s[y][j];
}
void bitrev(char *bg, char *ed) {
  for (char *it = bg; it != ed; ++it) *it = *it == '0' ? '1' : '0';
}
void go() {
  scanf("%d", &n);
  memset(c, 0, sizeof(c));
  la = 0;
  FOR(i, 0, 2) {
    scanf("%s", s[i] + 1);
    FOR(j, 1, n * 2) c[i][s[i][j] - '0']++;
  }
  FOR(i, 0, 2) FOR(j, i + 1, 2) {
    if (c[i][1] >= n && c[j][1] >= n) {
      work(i, j);
      assert(la <= n * 3);
      ans[la + 1] = 0;
      printf("%s\n", ans + 1);
      return;
    }
    if (c[i][0] >= n && c[j][0] >= n) {
      bitrev(s[i] + 1, s[i] + n * 2 + 1);
      bitrev(s[j] + 1, s[j] + n * 2 + 1);
      work(i, j);
      assert(la <= n * 3);
      ans[la + 1] = 0;
      bitrev(ans + 1, ans + la + 1);
      printf("%s\n", ans + 1);
      return;
    }
  }
}

int main() {
  int t;
  scanf("%d", &t);
  FOR(i, 1, t) go();
  return 0;
}
