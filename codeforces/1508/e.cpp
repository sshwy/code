// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

const int N = 3e5 + 5;
int a[N], n;
vector<int> g[N];

bool cmp(int x, int y) { return a[x] < a[y]; }

int dfn[N], totdfn, pst[N], totpst, dep[N], fa[N], nd[N], anc[N][20], dfnNd[N];

void dfs(int u, int p) {
  anc[u][0] = fa[u] = p;
  FOR(j, 1, 19) { anc[u][j] = anc[anc[u][j - 1]][j - 1]; }
  if (p) dep[u] = dep[p] + 1;
  dfn[u] = ++totdfn;
  dfnNd[totdfn] = u;
  for (int v : g[u]) dfs(v, u);
  pst[u] = ++totpst;
}
int kthAnc(int u, int k) {
  ROF(j, 19, 0) if (k >> j & 1) u = anc[u][j];
  return u;
}
void work(int v) {
  long long ans = 0, t = 0;
  int pst_v = -1;
  FOR(i, 1, n) if (pst[i] == v) {
    pst_v = i;
    break;
  }
  assert(pst_v != -1);
  bool flag = false;
  for (int u = pst_v; u; u = fa[u]) {
    if (a[u] == v) flag = true;
  }
  if (flag == false) return puts("NO"), void();
  for (int u = nd[v]; fa[u]; u = fa[u]) {
    if (a[u] > a[fa[u]]) return puts("NO"), void();
    ++t;
    swap(a[u], a[fa[u]]);
    swap(nd[a[u]], nd[a[fa[u]]]);
  }
  assert(a[1] == v);

  FOR(i, 1, v - 1) ans += dep[nd[i]], assert(pst[nd[i]] == i);
  FOR(i, v, n) {
    int u = dfnNd[i];
    int cur = kthAnc(u, max(dep[u] - (i - v), 0));
    if (a[cur] != i) return puts("NO"), void();
  }
  puts("YES");
  printf("%lld\n", ans + t);
  FOR(i, 1, n) printf("%d%c", dfn[i], " \n"[i == n]);
}

int main() {
  scanf("%d", &n);
  FOR(i, 1, n) scanf("%d", a + i), nd[a[i]] = i;
  FOR(i, 1, n - 1) {
    int u, v;
    scanf("%d%d", &u, &v);
    g[u].pb(v);
  }
  FOR(i, 1, n) { sort(g[i].begin(), g[i].end(), cmp); }
  dfs(1, 0);
  FOR(i, 1, n) {
    if (pst[nd[i]] != i) {
      work(i);
      return 0;
    }
  }
  puts("YES");
  long long tot = 0;
  FOR(i, 1, n) tot += dep[i];
  printf("%lld\n", tot);
  FOR(i, 1, n) printf("%d%c", dfn[i], " \n"[i == n]);
  return 0;
}
