// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA
const int N = 100;
int a[N], b[N], la;
void go() {
  long long p, q;
  scanf("%lld%lld", &p, &q);
  if (p % q) return printf("%lld\n", p), void();
  la = 0;
  for (int i = 2; i * i <= q; i++)
    if (q % i == 0) {
      a[++la] = i, b[la] = 0;
      while (q % i == 0) q /= i, b[la]++;
    }
  if (q > 1) a[++la] = q, b[la] = 1;
  long long ans = 1;
  FOR(i, 1, la) {
    long long x = p;
    while (x % a[i] == 0) x /= a[i];
    FOR(j, 1, b[i] - 1) x *= a[i];
    ans = max(ans, x);
  }
  printf("%lld\n", ans);
}

int main() {
  int t;
  scanf("%d", &t);
  FOR(i, 1, t) go();
  return 0;
}
