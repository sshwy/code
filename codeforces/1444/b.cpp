// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA
const int N = 3e5 + 5, P = 998244353;
int pw(int a, int m) {
  int res = 1;
  while (m) m & 1 ? res = 1ll * res * a % P : 0, a = 1ll * a * a % P, m >>= 1;
  return res;
}
int n, a[N];

int main() {
  scanf("%d", &n);
  FOR(i, 1, n * 2) scanf("%d", a + i);
  sort(a + 1, a + 2 * n + 1);
  int ans = 1, q = 1, s = 0;
  FOR(i, n + 1, n * 2) ans = ans * 1ll * i % P;
  FOR(i, 1, n) q = q * 1ll * i % P;
  q = pw(q, P - 2);
  ans = ans * 1ll * q % P;
  // printf("ans %d\n",ans);
  FOR(i, 1, n) s = (s - a[i] % P + P) % P;
  FOR(i, n + 1, n * 2) s = (s + a[i]) % P;
  ans = ans * 1ll * s % P;
  printf("%d\n", ans);
  return 0;
}
