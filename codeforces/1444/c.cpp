// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA
const int N = 5e5 + 5;

struct dsu {
  int f[N], g[N], w[N];
  void init(int n) { FOR(i, 0, n) f[i] = i, g[i] = 1, w[i] = 0; }
  stack<pair<int *, int>> s;
  int get(int u) { return f[u] == u ? u : get(f[u]); }
  bool find(int u, int v) { return get(u) == get(v); }
  int val(int u) {
    int res = 0;
    while (f[u] != u) res ^= w[u], u = f[u];
    return res;
  }
  void merge(int u, int v) {
    int vu = val(u), vv = val(v);
    u = get(u), v = get(v);
    if (u == v) return;
    if (g[u] > g[v]) swap(u, v), swap(vu, vv);
    s.push({f + u, f[u]}), f[u] = v;
    s.push({w + u, w[u]}), w[u] = vu ^ vv ^ 1;
    s.push({g + v, g[v]}), g[v] += g[u];
  }
  void undo(unsigned tag) {
    while (s.size() > tag) *s.top().first = s.top().second, s.pop();
  }
} d;

int n, m, k;
int a[N];
bool ban[N];
vector<pair<int, int>> E;
bool cmp(const pair<int, int> x, const pair<int, int> y) {
  return make_pair(a[x.first], a[x.second]) < make_pair(a[y.first], a[y.second]);
}
bool eq(const pair<int, int> x, const pair<int, int> y) {
  return make_pair(a[x.first], a[x.second]) == make_pair(a[y.first], a[y.second]);
}
int tot, tb;
void check(int l, int r) {
  if (ban[a[E[l].first]] || ban[a[E[l].second]]) return;
  unsigned tag = d.s.size();
  bool flag = 0;
  FOR(i, l, r) {
    int u = E[i].first, v = E[i].second;
    if (d.get(u) == d.get(v)) {
      if (d.val(u) == d.val(v)) {
        flag = 1;
        break;
      }
    } else {
      d.merge(u, v);
    }
  }
  d.undo(tag);
  if (flag) tot++;
}
int main() {
  scanf("%d%d%d", &n, &m, &k);
  d.init(n);
  FOR(i, 1, n) scanf("%d", a + i);
  FOR(i, 1, m) {
    int u, v;
    scanf("%d%d", &u, &v);
    if (ban[a[u]] || ban[a[v]]) continue;
    if (a[u] == a[v]) {
      if (d.get(u) == d.get(v)) {
        if (d.val(u) == d.val(v)) {
          ban[a[u]] = 1;
          tb++;
        }
      } else {
        d.merge(u, v);
      }
    } else {
      if (a[u] > a[v]) swap(u, v);
      E.push_back({u, v});
    }
  }
  sort(E.begin(), E.end(), cmp);
  int L = 0;
  while (L < E.size()) {
    int R = L;
    while (R + 1 < E.size() && eq(E[R + 1], E[L])) ++R;
    check(L, R);
    L = R + 1;
  }
  long long ans = tb * (tb - 1ll) / 2 + tb * 1ll * (k - 1) + tot;
  ans = k * (k - 1ll) / 2 - ans;
  printf("%lld\n", ans);
  return 0;
}
