#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 5e5 + 5, SZ = 5e6 + 5;

int n, q;
namespace seg {
  int tot;
  int lc[SZ], rc[SZ], sz[SZ];
  void insert(int &u, int val, int l = 1, int r = n) {
    if (!u) u = ++tot;
    if (l == r) return sz[u] = 1, void();
    int mid = (l + r) >> 1;
    if (val <= mid)
      insert(lc[u], val, l, mid);
    else
      insert(rc[u], val, mid + 1, r);
    sz[u] = sz[lc[u]] + sz[rc[u]];
  }
  void merge(int &x, int &y, int l = 1, int r = n) { // merge y to x
    if (!y) return;
    if (!x) return x = y, void();
    if (l == r) return sz[x] += sz[y], void();
    int mid = (l + r) >> 1;
    merge(lc[x], lc[y], l, mid), merge(rc[x], rc[y], mid + 1, r);
    sz[x] = sz[lc[x]] + sz[rc[x]];
  }
  int query(int u, int L, int R, int l = 1, int r = n) {
    if (!u) return 0;
    if (r < L || R < l) return 0;
    if (L <= l && r <= R) return sz[u];
    int mid = (l + r) >> 1;
    return query(lc[u], L, R, l, mid) + query(rc[u], L, R, mid + 1, r);
  }
} // namespace seg

int rt[N];
int len[N], fail[N], tr[N][26];

int tot = 1, last = 1;
int pos[N];
void insert(char c, int id) {
  c -= 'a';
  int u = 0, p = last, q = 0, cq = 0;
  if (!tr[p][c]) {
    u = ++tot;
    len[u] = len[last] + 1;
    while (p && tr[p][c] == 0) tr[p][c] = u, p = fail[p];
  }
  if (!p)
    fail[u] = 1;
  else {
    q = tr[p][c];
    if (len[q] == len[p] + 1)
      fail[u] = q;
    else {
      cq = ++tot;
      len[cq] = len[p] + 1, fail[cq] = fail[q], fail[q] = fail[u] = cq;
      memcpy(tr[cq], tr[q], sizeof(tr[q]));
      while (p && tr[p][c] == q) tr[p][c] = cq, p = fail[p];
    }
  }
  last = u ? u : cq ? cq : q;
  seg::insert(rt[last], id);
}
int bin[N], b[N];
void qsort() {
  FOR(i, 1, tot) bin[len[i]]++;
  FOR(i, 1, tot) bin[i] += bin[i - 1];
  ROF(i, tot, 1) b[bin[len[i]]--] = i;
}

char s[N];
struct qry {
  int l, r, id;
};
vector<qry> qrys[N];
int ans[N];

int main() {
  scanf("%d%d", &n, &q);
  FOR(i, 1, n) {
    last = 1;
    scanf("%s", s + 1);
    int ls = strlen(s + 1);
    FOR(j, 1, ls) insert(s[j], i);
    pos[i] = last;
  }
  FOR(i, 1, q) {
    int l, r, k;
    scanf("%d%d%d", &l, &r, &k);
    qrys[pos[k]].pb({l, r, i});
  }
  qsort();
  ROF(i, tot, 1) {
    int u = b[i];
    for (qry x : qrys[u]) ans[x.id] = seg::query(rt[u], x.l, x.r);
    seg::merge(rt[fail[u]], rt[u]);
  }
  FOR(i, 1, q) { printf("%d\n", ans[i]); }
  return 0;
}
