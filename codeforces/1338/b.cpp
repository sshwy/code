// by Sshwy
//#define DEBUGGER
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

#ifdef DEBUGGER

#define log_prefix()                                                \
  {                                                                 \
    fprintf(stderr, "\033[37mLine %-3d [%dms]:\033[0m  ", __LINE__, \
        (int)clock() / 1000);                                       \
  }
#define ilog(...) \
  { fprintf(stderr, __VA_ARGS__); }
#define llog(...)      \
  {                    \
    log_prefix();      \
    ilog(__VA_ARGS__); \
  }
#define log(...)           \
  {                        \
    log_prefix();          \
    ilog(__VA_ARGS__);     \
    fprintf(stderr, "\n"); \
  }
#define red(...)       \
  {                    \
    log_prefix();      \
    ilog("\033[31m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define green(...)     \
  {                    \
    log_prefix();      \
    ilog("\033[32m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define blue(...)      \
  {                    \
    log_prefix();      \
    ilog("\033[34m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }

#else

#define log_prefix() ;
#define ilog(...) ;
#define llog(...) ;
#define log(...) ;
#define red(...) ;
#define green(...) ;
#define blue(...) ;

#endif

namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 1e5 + 5;
int n, dg[N], sz[N], dep[N], flag[2], f[N];
vector<int> g[N];

void dfs(int u, int p) {
  // printf("dfs %d %d\n",u,p);
  dep[u] = dep[p] + 1, sz[u] = 1;
  if (dg[u] == 1) flag[dep[u] & 1] = 1;
  for (int v : g[u])
    if (v != p) dfs(v, u), sz[u] += sz[v];
  int s0 = 0;
  for (int v : g[u])
    if (v != p) {
      if (sz[v] == 1)
        s0 = 1;
      else
        f[u] += f[v] + 1;
    }
  f[u] += s0;
}
int main() {
  scanf("%d", &n);
  FOR(i, 1, n - 1) {
    int a, b;
    scanf("%d%d", &a, &b);
    g[a].pb(b), g[b].pb(a);
    dg[a]++, dg[b]++;
  }
  int root = max_element(dg + 1, dg + n + 1) - dg;
  dfs(root, 0);
  if (flag[0] & flag[1])
    printf("3 ");
  else
    printf("1 ");
  printf("%d\n", f[root]);
  return 0;
}
