// by Sshwy
//#define DEBUGGER
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

#ifdef DEBUGGER

#define log_prefix()                                                \
  {                                                                 \
    fprintf(stderr, "\033[37mLine %-3d [%dms]:\033[0m  ", __LINE__, \
        (int)clock() / 1000);                                       \
  }
#define ilog(...) \
  { fprintf(stderr, __VA_ARGS__); }
#define llog(...)      \
  {                    \
    log_prefix();      \
    ilog(__VA_ARGS__); \
  }
#define log(...)           \
  {                        \
    log_prefix();          \
    ilog(__VA_ARGS__);     \
    fprintf(stderr, "\n"); \
  }
#define red(...)       \
  {                    \
    log_prefix();      \
    ilog("\033[31m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define green(...)     \
  {                    \
    log_prefix();      \
    ilog("\033[32m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define blue(...)      \
  {                    \
    log_prefix();      \
    ilog("\033[34m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }

#else

#define log_prefix() ;
#define ilog(...) ;
#define llog(...) ;
#define log(...) ;
#define red(...) ;
#define green(...) ;
#define blue(...) ;

#endif

namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/

long long calc(long long L, long long R, long long pos) {
  if (L == R) return L;
  long long step = (R - L + 1) / 4;
  if (pos <= step) return calc(L, L + step - 1, pos);
  if (pos <= step * 2) return calc(L + step * 2, L + step * 3 - 1, pos - step);
  if (pos <= step * 3) return calc(L + step * 3, R, pos - step * 2);
  return calc(L + step, L + step * 2 - 1, pos - step * 3);
}
void go() {
  long long n, x;
  scanf("%lld", &n);
  x = n / 3 - (n % 3 == 0) + 1;
  long long a = 0, pre = 0;
  while (pre + (1ll << ((a)*2)) < x) pre += (1ll << (a * 2)), ++a;

  long long A = x - pre + (1ll << (a * 2)) - 1;
  long long B = calc((1ll << (a * 2)) * 2, (1ll << (a * 2)) * 3 - 1, x - pre);
  long long C = A ^ B;
  long long ans[] = {C, A, B};
  // printf("AB: %lld %lld\n",A,B);
  printf("%lld\n", ans[n % 3]);
}
int main() {
  int t;
  scanf("%d", &t);
  FOR(i, 1, t) go();
  return 0;
}
