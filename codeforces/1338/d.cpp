// by Sshwy
//#define DEBUGGER
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

#ifdef DEBUGGER

#define log_prefix()                                                \
  {                                                                 \
    fprintf(stderr, "\033[37mLine %-3d [%dms]:\033[0m  ", __LINE__, \
        (int)clock() / 1000);                                       \
  }
#define ilog(...) \
  { fprintf(stderr, __VA_ARGS__); }
#define llog(...)      \
  {                    \
    log_prefix();      \
    ilog(__VA_ARGS__); \
  }
#define log(...)           \
  {                        \
    log_prefix();          \
    ilog(__VA_ARGS__);     \
    fprintf(stderr, "\n"); \
  }
#define red(...)       \
  {                    \
    log_prefix();      \
    ilog("\033[31m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define green(...)     \
  {                    \
    log_prefix();      \
    ilog("\033[32m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define blue(...)      \
  {                    \
    log_prefix();      \
    ilog("\033[34m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }

#else

#define log_prefix() ;
#define ilog(...) ;
#define llog(...) ;
#define log(...) ;
#define red(...) ;
#define green(...) ;
#define blue(...) ;

#endif

namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 1e5 + 5;

int n, f[N][2];
vector<int> g[N];

void dfs1(int u, int p) {
  for (int v : g[u])
    if (v != p) dfs1(v, u);
  f[u][1] = 1;
  int cnt = int(g[u].size()) - !!(p);
  for (int v : g[u])
    if (v != p) {
      f[u][1] = max(f[u][1], f[v][0] + 1);
      f[u][0] = max(f[u][0], max(f[v][0], f[v][1]) + cnt - 1);
    }
}
int ans;
void dfs2(int u, int p) {
  ans = max(ans, f[u][0]);
  ans = max(ans, f[u][1]);

  pair<int, int> pv[2][2] = {{{0, 0}}};
  int cnt = int(g[u].size()) - 1;
  for (int v : g[u]) {
    if (pv[0][0].fi == 0 || pv[0][0].se < f[v][0] + 1)
      pv[0][1] = pv[0][0], pv[0][0] = {v, f[v][0] + 1};
    else if (pv[0][1].fi == 0 || pv[0][1].se < f[v][0] + 1)
      pv[0][1] = {v, f[v][0] + 1};
    if (pv[1][0].fi == 0 || pv[1][0].se < max(f[v][0], f[v][1]) + cnt - 1)
      pv[1][1] = pv[1][0], pv[1][0] = {v, max(f[v][0], f[v][1]) + cnt - 1};
    else if (pv[1][1].fi == 0 || pv[1][1].se < max(f[v][0], f[v][1]) + cnt - 1)
      pv[1][1] = {v, max(f[v][0], f[v][1]) + cnt - 1};
  }
  for (int v : g[u])
    if (v != p) { // u->v
      int fu[] = {f[u][0], f[u][1]};
      int fv[] = {f[v][0], f[v][1]};
      if (cnt) {
        f[u][1] = pv[0][0].fi != v ? pv[0][0].se : pv[0][1].se;
        f[u][0] = pv[1][0].fi != v ? pv[1][0].se : pv[1][1].se;
      } else {
        f[u][1] = 1;
        f[u][0] = 0;
      }
      f[v][1] = max(f[v][1], f[u][0] + 1);
      ++f[v][0];
      f[v][0] = max(f[v][0], max(f[u][0], f[u][1]) + int(g[v].size()) - 1);

      dfs2(v, u);
      f[u][0] = fu[0], f[u][1] = fu[1];
      f[v][0] = fv[0], f[v][1] = fv[1];
    }
}

int main() {
  cin >> n;
  FOR(i, 1, n - 1) {
    int a, b;
    cin >> a >> b;
    g[a].pb(b), g[b].pb(a);
  }
  dfs1(1, 0);
  dfs2(1, 0);
  cout << ans << endl;
  return 0;
}
