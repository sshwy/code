#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
int pw(int a, int m, int p) {
  int res = 1;
  while (m) m & 1 ? res = 1ll * res * a % p : 0, a = 1ll * a * a % p, m >>= 1;
  return res;
}
const int P = 1e9 + 7, N = 2e5 + 5;

int fac[N], fnv[N];

int S(int n, int m) {
  int res = 0;
  FOR(k, 0, m)
  res = (res + (1ll - 2 * (k & 1)) * fac[m] % P * fnv[k] % P * fnv[m - k] % P *
                   pw(m - k, n, P)) %
        P;
  res = 1ll * res * fnv[m] % P;
  return res;
}
int n, k, x;
int sum;

int main() {
  scanf("%d%d", &n, &k);
  FOR(i, 1, n) scanf("%d", &x), sum = (sum + x) % P;

  fac[0] = 1;
  FOR(i, 1, n) fac[i] = 1ll * fac[i - 1] * i % P;
  fnv[n] = pw(fac[n], P - 2, P);
  ROF(i, n, 1) fnv[i - 1] = fnv[i] * 1ll * i % P;

  printf("%lld\n",
      (P + 1ll * sum * (S(n, k) + 1ll * (n - 1) * S(n - 1, k) % P) % P) % P);
  return 0;
}
