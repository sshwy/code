// by Sshwy
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int P = 1e9 + 7, SZ = 11000, SSZ = 110;

struct equation {
  int c[SSZ] = {};
  int &operator[](int x) { return c[x]; }
  equation operator+(equation e) {
    equation res;
    FOR(i, 0, SSZ - 1) res[i] = (c[i] + e[i]) % P;
    return res;
  }
  equation operator-(equation e) {
    equation res;
    FOR(i, 0, SSZ - 1) res[i] = (c[i] - e[i] + P) % P;
    return res;
  }
  equation operator-=(equation e) { return *this = *this - e, *this; }
  equation operator-(int x) {
    equation res = *this;
    res[0] = (res[0] - 1 + P) % P;
    return res;
  }
  equation operator*(int x) {
    equation res = *this;
    FOR(i, 0, SSZ - 1) if (res[i]) res[i] = res[i] * 1ll * x % P;
    return res;
  }
  equation operator*=(int x) { return *this = *this * x, *this; }
} EQ[SZ], G[SZ];
int lg;

int pw(int a, int m) {
  int res = 1;
  while (m) m & 1 ? res = 1ll * res * a % P : 0, a = 1ll * a * a % P, m >>= 1;
  return res;
}
int R, a[5], p[5];

inline int I(int x, int y) { return x += R, y += R, x * (2 * R + 1) + y; }
inline equation &F(int x, int y) { return EQ[I(x, y)]; }

bool check(int x, int y) { return x * x + y * y <= R * R; }
int tot;
void calc_main(int x, int y) { //第x行的主元编号为x+R+1
  if (!check(x, y)) return;
  if (!check(x - 1, y)) { //在边界上
    F(x, y)[++tot] = 1;
  }
}
void work(int x, int y) { //用F(x,y)计算F(x+1,y)
  if (!check(x, y)) return;
  F(x + 1, y) = F(x, y) - 1;
  if (check(x - 1, y)) F(x + 1, y) -= F(x - 1, y) * p[1];
  if (check(x, y - 1)) F(x + 1, y) -= F(x, y - 1) * p[2];
  if (check(x, y + 1)) F(x + 1, y) -= F(x, y + 1) * p[4];
  F(x + 1, y) *= pw(p[3], P - 2);
  if (!check(x + 1, y)) { // F(x+1,y)=0
    G[++lg] = F(x + 1, y);
  }
}

void gauss(equation c[], int n) {
  FOR(i, 1, n) {
    FOR(j, i, n) if (c[j][i]) {
      FOR(k, 0, n) swap(c[j][k], c[i][k]);
      break;
    }
    FOR(j, 1, n) {
      if (i == j) continue;
      int rate = c[j][i] * 1ll * pw(c[i][i], P - 2) % P;
      FOR(k, i, n) c[j][k] = (c[j][k] - 1ll * c[i][k] * rate) % P;
      c[j][0] = (c[j][0] - 1ll * c[i][0] * rate) % P;
    }
  }
  FOR(i, 1, n) c[i][0] = c[i][0] * 1ll * pw(c[i][i], P - 2) % P, c[i][i] = 1;
  FOR(i, 1, n) c[i][0] = (c[i][0] + P) % P;
}

int main() {
  scanf("%d%d%d%d%d", &R, &a[1], &a[2], &a[3], &a[4]);
  int s = a[1] + a[2] + a[3] + a[4];
  s = pw(s, P - 2);
  FOR(i, 1, 4) p[i] = a[i] * 1ll * s % P;

  FOR(i, -R, R) FOR(j, -R, R) { calc_main(i, j); }
  FOR(i, -R, R) FOR(j, -R, R) { work(i, j); }
  assert(lg == R * 2 + 1);

  FOR(i, 1, lg) G[i][0] = P - G[i][0];
  gauss(G, lg);

  int A[SZ], ans = F(0, 0)[0];
  FOR(i, 1, lg) A[i] = G[i][0];
  FOR(i, 1, lg) ans = (ans + F(0, 0)[i] * 1ll * A[i]) % P;

  printf("%d\n", ans);

  return 0;
}
