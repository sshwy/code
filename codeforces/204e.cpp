#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
namespace IO {
  char nc() {
    static char bf[100000], *p1 = bf, *p2 = bf;
    return p1 == p2 && (p2 = (p1 = bf) + fread(bf, 1, 100000, stdin), p1 == p2)
               ? EOF
               : *p1++;
  }
  int rd() {
    int res = 0;
    char c = getchar();
    while (!isdigit(c)) c = getchar();
    while (isdigit(c)) res = res * 10 + c - '0', c = getchar();
    return res;
  }
} // namespace IO
/******************heading******************/
const int SZ = 2e5 + 500, ALP = 26;
int n, k;

void mapinsert(map<int, int> &a, map<int, int> &b) {
  for (map<int, int>::iterator i = b.begin(); i != b.end(); ++i) {
    int u = i->first, v = i->second;
    if (a.count(u))
      a[u] += v;
    else
      a[u] = v;
  }
}
struct SAM {
  int tot, last;
  int tr[SZ][ALP], fail[SZ];
  int len[SZ], end[SZ]; // cnt:状态出现次数;end：状态的结尾位置
  // set<int> s[SZ];
  map<int, int> cnt[SZ];
  int str[SZ], ls;
  SAM() { tot = last = 1, len[1] = 0, fail[1] = 0; }
  // void print_node(int u){//输出每个结点的状态
  //    printf("\033[34m");
  //    printf("u=%2d,len=%2d,fail=%2d, ",u,len[u],fail[u]);
  //    FOR(i,end[u]-len[u]+1,end[u])putchar(str[i]);
  //    puts("");
  //    for(map<int,int>::iterator i=cnt[u].begin();i!=cnt[u].end();++i){
  //        int u=i->first,v=i->second;
  //        printf("it exists in \033[0m%d\033[34m "
  //                "for \033[0m%d\033[34m times\n",u,v);
  //    }
  //    puts("\033[0m");
  //}
  void insert(char x, int id) {
    str[++ls] = x;
    x -= 'a';
    int p = last, u = 0, cq = 0, q = 0;
    if (!tr[p][x]) {
      u = ++tot;
      len[u] = len[last] + 1;
      end[u] = ls; // u 的卫星信息
      while (p && tr[p][x] == 0) tr[p][x] = u, p = fail[p];
    }
    if (!p)
      fail[u] = 1;
    else {
      q = tr[p][x];
      if (len[q] == len[p] + 1)
        fail[u] = q;
      else {
        cq = ++tot;
        len[cq] = len[p] + 1, fail[cq] = fail[q];
        end[cq] = end[q]; //如果需要，更新的cq的卫星信息
        memcpy(tr[cq], tr[q], sizeof(tr[q]));
        fail[q] = fail[u] = cq;
        while (p && tr[p][x] == q) tr[p][x] = cq, p = fail[p];
      }
    }
    last = u ? u : cq ? cq : q;
    /*
     * 三种情况：
     * 1. 新建一个u结点，所以last=u
     * 2. 没有新建分支，但是要从q中分一个状态出来（包含类），则last=cq
     * 3. 刚好在q的状态，则last=q
     * 然后再更新last的卫星信息
     */
    // s[last].insert(id);
    cnt[last][id]++;
  }
  int a[SZ], bin[SZ], tim[SZ]; // tim：每个结点的时间戳
  void insert(char *s, int id) {
    last = 1;
    for (int i = 1; s[i]; i++) insert(s[i], id);
  }
  lld ans[SZ];
  void go() {
    FOR(i, 1, tot) bin[len[i]]++;
    FOR(i, 1, tot) bin[i] += bin[i - 1];
    FOR(i, 1, tot) a[bin[len[i]]--] = i;
    ROF(i, tot, 2) {
      int u = a[i];
      // s[fail[u]].insert(s[u].begin(),s[u].end());
      mapinsert(cnt[fail[u]], cnt[u]);
      // printf("u=%d, ",u);
      // printf("len=%d,len[fail]=%d, ",len[u],len[fail[u]]);
      // printset(s[u]);
      if (cnt[u].size() >= k) {
        int t = len[u] - len[fail[u]];
        for (map<int, int>::iterator i = cnt[u].begin(); i != cnt[u].end(); ++i) {
          int x = i->first, y = i->se;
          ans[x] += 1ll * t * y;
        }
      }
    }
    // FOR(i,1,tot)print_node(i);
    FOR(i, 1, n) cout << ans[i] << ' ';
    // FOR(i,1,n)printf("%d ",ans[i]);
  }
} sam;

char s[SZ];

signed main() {
  cin >> n >> k;
  // scanf("%d%d",&n,&k);
  FOR(i, 1, n) {
    cin >> s + 1;
    // scanf("%s",s+1);
    sam.insert(s, i);
  }
  sam.go();
  return 0;
}
