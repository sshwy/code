// by Sshwy
//#define DEBUGGER
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

#ifdef DEBUGGER

#define log_prefix()                                                \
  {                                                                 \
    fprintf(stderr, "\033[37mLine %-3d [%dms]:\033[0m  ", __LINE__, \
        (int)clock() / 1000);                                       \
  }
#define ilog(...) \
  { fprintf(stderr, __VA_ARGS__); }
#define llog(...)      \
  {                    \
    log_prefix();      \
    ilog(__VA_ARGS__); \
  }
#define log(...)           \
  {                        \
    log_prefix();          \
    ilog(__VA_ARGS__);     \
    fprintf(stderr, "\n"); \
  }
#define red(...)       \
  {                    \
    log_prefix();      \
    ilog("\033[31m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define green(...)     \
  {                    \
    log_prefix();      \
    ilog("\033[32m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define blue(...)      \
  {                    \
    log_prefix();      \
    ilog("\033[34m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }

#else

#define log_prefix() ;
#define ilog(...) ;
#define llog(...) ;
#define log(...) ;
#define red(...) ;
#define green(...) ;
#define blue(...) ;

#endif

namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 2e5 + 5;

vector<int> g[N];
int dep[N], fa[N][20], dfn[N], totdfn;
void dfs(int u, int p) {
  fa[u][0] = p;
  dep[u] = dep[p] + 1;
  dfn[u] = ++totdfn;
  FOR(j, 1, 19) {
    fa[u][j] = fa[fa[u][j - 1]][j - 1];
    if (fa[u][j] == 0) break;
  }
  for (int v : g[u])
    if (v != p) dfs(v, u);
}
int lca(int u, int v) {
  if (dep[u] < dep[v]) swap(u, v);
  ROF(j, 19, 0) if (dep[u] - (1 << j) >= dep[v]) u = fa[u][j];
  if (u == v) return u;
  ROF(j, 19, 0) if (fa[u][j] != fa[v][j]) u = fa[u][j], v = fa[v][j];
  return fa[u][0];
}
bool check(vector<int> &v, int z) { // z lca
  // for(int x:v)printf("%d ",x); printf("z=%d\n",z);
  for (int &x : v)
    if (dep[x] > dep[z]) x = fa[x][0];
  // v的点是否构成，z为lca的路径
  int las = -1, flag = 0;
  for (int x : v) {
    if (las != -1) {
      int y = lca(las, x);
      if (y == las) { // x是las的儿子
        las = x;
      } else {
        return 0;
      }
    } else {
      las = x;
    }
  }
  return 1;
}
string query(vector<int> &v) {
  if (v.size() <= 1) return "YES";
  sort(v.begin(), v.end(), [](int x, int y) { return dep[x] < dep[y]; });
  int z = v[0];
  for (int j = 1; j < v.size(); j++) z = lca(z, v[j]);
  if (check(v, z)) return "YES";
  return "NO";
}
int main() {
  int n, m;
  cin >> n >> m;
  FOR(i, 1, n - 1) {
    int u, v;
    cin >> u >> v;
    g[u].pb(v);
    g[v].pb(u);
  }
  dfs(1, 0);
  FOR(i, 1, m) {
    int k;
    vector<int> v;
    cin >> k;
    FOR(i, 1, k) {
      int x;
      cin >> x;
      v.pb(x);
    }
    cout << query(v) << endl;
  }
  return 0;
}
