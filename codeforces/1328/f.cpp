// by Sshwy
//#define DEBUGGER
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

#ifdef DEBUGGER

#define log_prefix()                                                \
  {                                                                 \
    fprintf(stderr, "\033[37mLine %-3d [%dms]:\033[0m  ", __LINE__, \
        (int)clock() / 1000);                                       \
  }
#define ilog(...) \
  { fprintf(stderr, __VA_ARGS__); }
#define llog(...)      \
  {                    \
    log_prefix();      \
    ilog(__VA_ARGS__); \
  }
#define log(...)           \
  {                        \
    log_prefix();          \
    ilog(__VA_ARGS__);     \
    fprintf(stderr, "\n"); \
  }
#define red(...)       \
  {                    \
    log_prefix();      \
    ilog("\033[31m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define green(...)     \
  {                    \
    log_prefix();      \
    ilog("\033[32m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define blue(...)      \
  {                    \
    log_prefix();      \
    ilog("\033[34m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }

#else

#define log_prefix() ;
#define ilog(...) ;
#define llog(...) ;
#define log(...) ;
#define red(...) ;
#define green(...) ;
#define blue(...) ;

#endif

namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 2e5 + 5;
int n, k;
long long a[N], pre[N], suf[N];

int main() {
  cin >> n >> k;
  FOR(i, 1, n) { cin >> a[i]; }
  sort(a + 1, a + n + 1);
  a[n + 1] = -2;
  int las = -1, cnt = 0, maxcnt = 0;
  FOR(i, 1, n + 1) {
    if (a[i] == las)
      ++cnt;
    else {
      maxcnt = max(maxcnt, cnt);
      cnt = 1;
      las = a[i];
    }
  }
  if (maxcnt >= k) {
    cout << 0 << endl;
    return 0;
  }
  long long minMoves = 0, maxMoves = 0, ans = 1e18;
  FOR(i, 1, n) pre[i] = pre[i - 1] + a[i];
  ROF(i, n, 1) suf[i] = suf[i + 1] + a[i];
  FOR(i, 1, n) { //以a[i]作为eq值
    int L = i, R = i;
    while (R < n && a[R + 1] == a[L]) ++R;
    // a[L..R] equal
    maxMoves = suf[R + 1] - (n - R) * (a[i] + 1);
    minMoves = (L - 1) * (a[i] - 1) - pre[L - 1];
    long long rest = k - (R - L + 1);
    ans = min(ans, maxMoves + minMoves + rest);
    if (L > rest) ans = min(ans, minMoves + rest);
    if (n - R >= rest) ans = min(ans, maxMoves + rest);
    i = R;
  }
  cout << ans << endl;
  return 0;
}
