// by Sshwy
//#define DEBUGGER
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

#ifdef DEBUGGER

#define log_prefix()                                                \
  {                                                                 \
    fprintf(stderr, "\033[37mLine %-3d [%dms]:\033[0m  ", __LINE__, \
        (int)clock() / 1000);                                       \
  }
#define ilog(...) \
  { fprintf(stderr, __VA_ARGS__); }
#define llog(...)      \
  {                    \
    log_prefix();      \
    ilog(__VA_ARGS__); \
  }
#define log(...)           \
  {                        \
    log_prefix();          \
    ilog(__VA_ARGS__);     \
    fprintf(stderr, "\n"); \
  }
#define red(...)       \
  {                    \
    log_prefix();      \
    ilog("\033[31m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define green(...)     \
  {                    \
    log_prefix();      \
    ilog("\033[32m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define blue(...)      \
  {                    \
    log_prefix();      \
    ilog("\033[34m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }

#else

#define log_prefix() ;
#define ilog(...) ;
#define llog(...) ;
#define log(...) ;
#define red(...) ;
#define green(...) ;
#define blue(...) ;

#endif

namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 2e5 + 5;
int a[N], fa[N], c[N], b[N];
int get(int u) { return fa[u] == u ? u : fa[u] = get(fa[u]); }
void go() {
  int n, lb = 0;
  cin >> n;
  FOR(i, 1, n) cin >> a[i];

  int st = -1;
  FOR(i, 2, n) if (a[i] == a[i - 1]) {
    st = i;
    break;
  }
  if (st != -1) {
    FOR(i, st, n) b[++lb] = a[i];
    FOR(i, 1, st - 1) b[++lb] = a[i];
  } else {
    FOR(i, 1, n) b[i] = a[i];
  }
  if (st != -1) assert(lb == n);

  vector<int> v, s;
  FOR(i, 1, n) fa[i] = i, c[i] = 0;
  FOR(i, 1, n) {
    if (!v.size())
      v.pb(b[i]);
    else if (v.back() != b[i])
      v.pb(b[i]);
    else
      fa[i] = get(fa[i - 1]); // merge
  }
  if (v.size() > 1 && v.front() == v.back()) {
    if (v.size() % 2 == 1 || v.size() == 2) { v.pop_back(), fa[get(n)] = get(1); }
  }
  FOR(i, 1, n) if (get(i) == i) s.pb(i);
  assert(s.size() == v.size());
  int ans = 0;
  if (v.size() == 1)
    ans = 1;
  else if (v.size() % 2 == 1)
    ans = 3;
  else
    ans = 2;
  int col = 1;
  for (int i = 0; i < s.size(); i++) {
    if (i > 0) { // change color
      if (ans == 3 && i + 1 == s.size())
        col = 3;
      else {
        if (col == 1)
          col = 2;
        else
          col = 1;
      }
    }
    c[s[i]] = col;
  }
  vector<int> ansc;
  if (st == -1)
    FOR(i, 1, n) ansc.pb(c[get(i)]);
  else {
    FOR(i, n - st + 2, n) ansc.pb(c[get(i)]);
    FOR(i, 1, n - st + 1) ansc.pb(c[get(i)]);
  }
  cout << ans << endl;
  for (int x : ansc) cout << x << " ";
  cout << endl;
}
int main() {
  int q;
  cin >> q;
  FOR(i, 1, q) go();
  return 0;
}
