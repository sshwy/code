// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA
const int N = 1e5 + 5;
int a[7], n, b[N];
pair<int, int> c[N * 6];
int lc;

int cnt[N], tot;
void add(int x) {
  if (cnt[x] == 0) ++tot;
  cnt[x]++;
}
void del(int x) {
  cnt[x]--;
  if (cnt[x] == 0) --tot;
}

int main() {
  FOR(i, 1, 6) scanf("%d", a + i);
  scanf("%d", &n);
  FOR(i, 1, n) scanf("%d", b + i);
  FOR(i, 1, n) FOR(j, 1, 6) c[++lc] = {b[i] - a[j], i};
  sort(c + 1, c + lc + 1);
  int r = 0, ans = 2e9 + 20;
  FOR(i, 1, lc) {
    if (i > 1) { del(c[i - 1].second); }
    while (tot < n) {
      if (r == lc)
        break;
      else
        add(c[++r].second);
    }
    if (tot < n) break;
    ans = min(ans, c[r].first - c[i].first);
  }
  printf("%d\n", ans);
  return 0;
}
