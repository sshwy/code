// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA
const int N = 1e5 + 5;
int n, ans[N];
vector<pair<int, int>> v; // (x,pos)
int cur;
int f[N];
int get(int i) { return f[i] == i ? i : f[i] = get(f[i]); }
int main() {
  scanf("%d", &n);
  FOR(i, 1, n + 1) f[i] = i;
  FOR(i, 1, n * 2) {
    char op[5];
    int x;
    scanf("%s", op);
    if (op[0] == '+') {
      ++cur;
    } else {
      scanf("%d", &x);
      while (v.size() && v.back().first <= x) v.pop_back();
      int pos = v.size() ? v.back().second + 1 : 1;
      pos = get(pos);
      if (pos <= cur) {
        ans[pos] = x;
        f[pos] = get(pos + 1);
        v.pb({x, cur});
      } else {
        puts("NO");
        return 0;
      }
    }
  }
  puts("YES");
  FOR(i, 1, n) printf("%d%c", ans[i], " \n"[i == n]);
  return 0;
}
