#include <bits/stdc++.h>
using namespace std;

using ll = long long;
using ld = long double;
using db = double;
using str = string; // yay python!

using pi = pair<int, int>;
using pl = pair<ll, ll>;
using pd = pair<db, db>;

using vi = vector<int>;
using vb = vector<bool>;
using vl = vector<ll>;
using vd = vector<db>;
using vs = vector<str>;
using vpi = vector<pi>;
using vpl = vector<pl>;
using vpd = vector<pd>;

#define tcT template <class T
#define tcTU tcT, class U
// ^ lol this makes everything look weird but I'll try it
tcT > using V = vector<T>;
tcT, size_t SZ > using AR = array<T, SZ>;
tcT > using PR = pair<T, T>;

// pairs
#define mp make_pair
#define f first
#define s second

// vectors
#define sz(x) (int)(x).size()
#define all(x) begin(x), end(x)
#define rall(x) (x).rbegin(), (x).rend()
#define sor(x) sort(all(x))
#define rsz resize
#define ins insert
#define ft front()
#define bk back()
#define pf push_front
#define pb push_back
#define eb emplace_back
#define lb lower_bound
#define ub upper_bound

// loops
#define FOR(i, a, b) for (int i = (a); i < (b); ++i)
#define F0R(i, a) FOR(i, 0, a)
#define ROF(i, a, b) for (int i = (b)-1; i >= (a); --i)
#define R0F(i, a) ROF(i, 0, a)
#define trav(a, x) for (auto &a : x)

const int MOD = 1e9 + 7; // 998244353;
const int MX = 1 << 19;
const ll INF = 1e18; // not too close to LLONG_MAX
const ld PI = acos((ld)-1);
const int dx[4] = {1, 0, -1, 0}, dy[4] = {0, 1, 0, -1}; // for every grid problem!!
mt19937 rng((uint32_t)chrono::steady_clock::now().time_since_epoch().count());
template <class T> using pqg = priority_queue<T, vector<T>, greater<T>>;

// helper funcs
constexpr int pct(int x) { return __builtin_popcount(x); }  // # of bits set
constexpr int bits(int x) { return 31 - __builtin_clz(x); } // floor(log2(x))
ll cdiv(ll a, ll b) {
  return a / b + ((a ^ b) > 0 && a % b);
} // divide a by b rounded up
ll fdiv(ll a, ll b) {
  return a / b - ((a ^ b) < 0 && a % b);
} // divide a by b rounded down

tcT > bool ckmin(T &a, const T &b) {
  return b < a ? a = b, 1 : 0;
} // set a = min(a,b)
tcT > bool ckmax(T &a, const T &b) { return a < b ? a = b, 1 : 0; }

tcTU > T fstTrue(T lo, T hi, U f) {
  hi++;
  assert(lo <= hi); // assuming f is increasing
  while (lo < hi) { // find first index such that f is true
    T mid = lo + (hi - lo) / 2;
    f(mid) ? hi = mid : lo = mid + 1;
  }
  return lo;
}
tcTU > T lstTrue(T lo, T hi, U f) {
  lo--;
  assert(lo <= hi); // assuming f is decreasing
  while (lo < hi) { // find first index such that f is true
    T mid = lo + (hi - lo + 1) / 2;
    f(mid) ? lo = mid : hi = mid - 1;
  }
  return lo;
}
tcT > void remDup(vector<T> &v) { // sort and remove duplicates
  sort(all(v));
  v.erase(unique(all(v)), end(v));
}
tcTU > void erase(T &t, const U &u) { // don't erase
  auto it = t.find(u);
  assert(it != end(t));
  t.erase(u);
} // element that doesn't exist from (multi)set

// INPUT
#define tcTUU tcT, class... U
tcT > void re(complex<T> &c);
tcTU > void re(pair<T, U> &p);
tcT > void re(vector<T> &v);
tcT, size_t SZ > void re(AR<T, SZ> &a);

tcT > void re(T &x) { cin >> x; }
void re(db &d) {
  str t;
  re(t);
  d = stod(t);
}
void re(ld &d) {
  str t;
  re(t);
  d = stold(t);
}
tcTUU > void re(T &t, U &...u) {
  re(t);
  re(u...);
}

tcT > void re(complex<T> &c) {
  T a, b;
  re(a, b);
  c = {a, b};
}
tcTU > void re(pair<T, U> &p) { re(p.f, p.s); }
tcT > void re(vector<T> &x) { trav(a, x) re(a); }
tcT, size_t SZ > void re(AR<T, SZ> &x) { trav(a, x) re(a); }
tcT > void rv(int &n, vector<T> &x) {
  re(n);
  x.rsz(n);
  trav(a, x) re(a);
}

// TO_STRING
#define ts to_string
str ts(char c) { return str(1, c); }
str ts(const char *s) { return (str)s; }
str ts(str s) { return s; }
str ts(bool b) {
#ifdef LOCAL
  return b ? "true" : "false";
#else
  return ts((int)b);
#endif
}
tcT > str ts(complex<T> c) {
  stringstream ss;
  ss << c;
  return ss.str();
}
str ts(vector<bool> v) {
  str res = "{";
  F0R(i, sz(v)) res += char('0' + v[i]);
  res += "}";
  return res;
}
template <size_t SZ> str ts(bitset<SZ> b) {
  str res = "";
  F0R(i, SZ) res += char('0' + b[i]);
  return res;
}
tcTU > str ts(pair<T, U> p);
tcT > str ts(T v) { // containers with begin(), end()
#ifdef LOCAL
  bool fst = 1;
  str res = "{";
  for (const auto &x : v) {
    if (!fst) res += ", ";
    fst = 0;
    res += ts(x);
  }
  res += "}";
  return res;
#else
  bool fst = 1;
  str res = "";
  for (const auto &x : v) {
    if (!fst) res += " ";
    fst = 0;
    res += ts(x);
  }
  return res;

#endif
}
tcTU > str ts(pair<T, U> p) {
#ifdef LOCAL
  return "(" + ts(p.f) + ", " + ts(p.s) + ")";
#else
  return ts(p.f) + " " + ts(p.s);
#endif
}

// OUTPUT
tcT > void pr(T x) { cout << ts(x); }
tcTUU > void pr(const T &t, const U &...u) {
  pr(t);
  pr(u...);
}
void ps() { pr("\n"); } // print w/ spaces
tcTUU > void ps(const T &t, const U &...u) {
  pr(t);
  if (sizeof...(u)) pr(" ");
  ps(u...);
}

// DEBUG
void DBG() { cerr << "]" << endl; }
tcTUU > void DBG(const T &t, const U &...u) {
  cerr << ts(t);
  if (sizeof...(u)) cerr << ", ";
  DBG(u...);
}
#ifdef LOCAL // compile with -DLOCAL, chk -> fake assert
#define dbg(...) \
  cerr << "Line(" << __LINE__ << ") -> [" << #__VA_ARGS__ << "]: [", DBG(__VA_ARGS__)
#define chk(...)                                                    \
  if (!(__VA_ARGS__))                                               \
    cerr << "Line(" << __LINE__ << ") -> function(" << __FUNCTION__ \
         << ") -> CHK FAILED: (" << #__VA_ARGS__ << ")"             \
         << "\n",                                                   \
        exit(0);
#else
#define dbg(...) 0
#define chk(...) 0
#endif

// FILE I/O
void setIn(str s) { freopen(s.c_str(), "r", stdin); }
void setOut(str s) { freopen(s.c_str(), "w", stdout); }
void unsyncIO() { cin.tie(0)->sync_with_stdio(0); }
void setIO(str s = "") {
  unsyncIO();
  // cin.exceptions(cin.failbit);
  // throws exception when do smth illegal
  // ex. try to read letter into int
  if (sz(s)) { setIn(s + ".in"), setOut(s + ".out"); } // for USACO
}

/**
 * Description: Calculates longest path in tree. The vertex furthest
 * from 0 must be an endpoint of the diameter.
 * Source: own
 * Verification:
 * http://www.spoj.com/problems/PT07Z/
 * https://codeforces.com/contest/1182/problem/D
 */

template <int SZ> struct TreeDiameter {
  int N, par[SZ], dist[SZ], diaLen;
  vi adj[SZ], dia, center;
  void ae(int a, int b) { adj[a].pb(b), adj[b].pb(a); }
  void dfs(int x) {
    trav(y, adj[x]) if (y != par[x]) {
      par[y] = x;
      dist[y] = dist[x] + 1;
      dfs(y);
    }
  }
  void genDist(int x) {
    par[x] = -1;
    dist[x] = 0;
    dfs(x);
  }
  void init(int _N) {
    N = _N;
    dia = {0, 0};
    // dbg("HA",N);
    genDist(0);
    F0R(i, N) if (dist[i] > dist[dia[0]]) dia[0] = i;
    genDist(dia[0]);
    F0R(i, N) if (dist[i] > dist[dia[1]]) dia[1] = i;
    diaLen = dist[dia[1]];
    int cen = dia[1];
    F0R(i, diaLen / 2) cen = par[cen];
    center = {cen};
    if (diaLen & 1) center.pb(par[cen]);
  }
};

TreeDiameter<MX> T;

/**
 * Description: 1D range increment and sum query.
 * Source: USACO Counting Haybales
 * Verification: SPOJ Horrible
 */

template <class T, int SZ> struct LazySeg {
  static_assert(pct(SZ) == 1); // SZ must be power of 2
  T comb(T a, T b) { return {max(a.f, b.f), max(a.s, b.s)}; }
  T seg[2 * SZ];
  bool lazy[2 * SZ];
  LazySeg() { F0R(i, 2 * SZ) seg[i] = {0, 0}, lazy[i] = 0; }
  void push(int ind, int L, int R) { /// modify values for current node
    if (!lazy[ind]) return;
    seg[ind] = {seg[ind].s, seg[ind].f};
    if (L != R) F0R(i, 2) lazy[2 * ind + i] ^= lazy[ind]; /// prop to children
    lazy[ind] = 0;
  } // recalc values for current node
  void pull(int ind) { seg[ind] = comb(seg[2 * ind], seg[2 * ind + 1]); }
  void build(vi v) {
    F0R(i, sz(v)) seg[SZ + i] = {v[i], 0};
    ROF(i, 1, SZ) pull(i);
  }
  void upd(int lo, int hi, bool inc, int ind = 1, int L = 0, int R = SZ - 1) {
    push(ind, L, R);
    if (hi < L || R < lo) return;
    if (lo <= L && R <= hi) {
      lazy[ind] = inc;
      push(ind, L, R);
      return;
    }
    int M = (L + R) / 2;
    upd(lo, hi, inc, 2 * ind, L, M);
    upd(lo, hi, inc, 2 * ind + 1, M + 1, R);
    pull(ind);
  }
  // T query(int lo, int hi, int ind=1, int L=0, int R=SZ-1) {
  // 	push(ind,L,R); if (lo > R || L > hi) return ;
  // 	if (lo <= L && R <= hi) return seg[ind];
  // 	int M = (L+R)/2;
  // 	return comb(query(lo,hi,2*ind,L,M),query(lo,hi,2*ind+1,M+1,R));
  // }
};

LazySeg<pi, 1 << 19> LS;

/**
 * Description: Euler Tour LCA. Compress takes a subset $S$ of nodes
 * and computes the minimal subtree that contains all the nodes
 * pairwise LCAs and compressing edges. Returns a list of
 * \texttt{(par, orig\_index)} representing a tree rooted at 0.
 * The root points to itself.
 * Time: O(N\log N) build, O(1) LCA, O(|S|\log |S|) compress
 * Source: USACO, Simon Lindholm (KACTL)
 * Verification: USACO Debug the Bugs
 * https://codeforces.com/contest/1320/problem/E
 */

// #include "../../data-structures/Static Range Queries (9.1)/RMQ (9.1).h"

struct LCA {
  int N;
  vpi adj[MX];
  vi depth, pos, par, rev; // rev is for compress
  vi tmp;                  // RMQ<pi> r;
  vi st, en;
  void init(int _N) {
    N = _N;
    depth = pos = par = rev = vi(N);
    st = en = vi(N);
  }
  void ae(int x, int y, int label) {
    adj[x].pb({y, label});
    adj[y].pb({x, label});
  }
  void dfs(int x) {
    pos[x] = sz(tmp);
    tmp.pb(depth[x]);
    trav(y, adj[x]) if (y.f != par[x]) {
      depth[y.f] = depth[par[y.f] = x] + 1;
      st[y.s] = sz(tmp);
      dfs(y.f);
      en[y.s] = sz(tmp) - 1;
    }
  }
  void gen(int R = 0) {
    par[R] = R;
    dfs(R);
  }
};
LCA L;

int N, M;
V<pair<pi, int>> ed;
vi roads;
vi ans;

void flip(int ind) { LS.upd(L.st[ind], L.en[ind], 1); }

void go(int x) {
  LS = LazySeg<pi, 1 << 19>();
  L = LCA();
  L.init(N);
  F0R(i, N - 1) L.ae(ed[i].f.f, ed[i].f.s, i);
  L.gen(x);
  LS.build(L.tmp);
  F0R(i, N - 1) if (ed[i].s) { flip(i); }
  F0R(i, M) {
    flip(roads[i]);
    ckmax(ans[i], LS.seg[1].f);
  }
  dbg(ans);
}

int main() {
  setIO();
  re(N); // OK
  F0R(i, N - 1) {
    int u, v, t;
    re(u, v, t);
    --u, --v;
    ed.pb({{u, v}, t});
    T.ae(u, v);
  }
  T.init(N);
  rv(M, roads);
  trav(t, roads)-- t;
  ans = vi(M, -1);
  // swap(T.dia[0],T.dia[1]);
  dbg("???", T.dia, T.diaLen);
  go(T.dia[0]);
  go(T.dia[1]);
  trav(t, ans) ps(t);
  // roads.pb();
  // ps(T.dia,T.diaLen);
  // exit(0);

  // you should actually read the stuff at the bottom
}

/* stuff you should look for
 * int overflow, array bounds
 * special cases (n=1?)
 * do smth instead of nothing and stay organized
 * WRITE STUFF DOWN
 * DON'T GET STUCK ON ONE APPROACH
 */
