// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA
const int N = 1e6 + 5;

void go() {
  long long a, b, c, d;
  scanf("%lld%lld%lld%lld", &a, &b, &c, &d);
  if (b * c < a) return puts("-1"), void();
  long long e = a / b; // t+1,...,t+e，造成的damage都是负的
  //第t+i时刻的伤害是 a-b*i
  // 在t，t+d,...,t+kd 时刻都用技能。要求t+kd<=t+e，防止第一个技能它退化？
  // 也不一定。有可能第一个技能的退化能换来更多技能的使用
  // 设k=e/d
  // 每延长1的时间，怪物回血量是k*b？
  // 减掉的血量最多是a*k
  // 所以要尽量减少回血
  // 那么如果第一次不用技能反而不优
  // 与其让第一个技能被完全施展，不如不用第一个技能
  long long k = e / d; // t,t+d,..,t+kd都用技能
  e = k * d;
  long long ans = (k + 1) * a; // 伤害
  ans -= b * e * (k + 1);
  ans += b * d * ((k + 1) * k / 2);
  printf("%lld\n", ans);
}
int main() {
  int t;
  scanf("%d", &t);
  FOR(i, 1, t) go();
  return 0;
}
