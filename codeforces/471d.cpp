// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

const int N = 2e5 + 5;
int n, m;
int a[N], b[N], nex[N];

void getNext(int *s, int ls) {
  nex[1] = 0;
  int j = 0;
  FOR(i, 2, ls) {
    while (j && s[i] != s[j + 1]) j = nex[j];
    if (s[i] == s[j + 1])
      nex[i] = ++j;
    else
      nex[i] = 0;
  }
}
int main() {
  scanf("%d%d", &n, &m);
  if (m == 1) return printf("%d\n", n), 0;
  FOR(i, 1, n) scanf("%d", &a[i]);
  FOR(i, 1, m) scanf("%d", &b[i]);

  FOR(i, 1, n) a[i] -= a[i + 1];
  FOR(i, 1, m) b[i] -= b[i + 1];
  --n, --m;

  b[m + 1] = -2e9;
  getNext(b, m);
  int j = 0;

  int ans = 0;
  FOR(i, 1, n) {
    while (j && a[i] != b[j + 1]) j = nex[j];
    if (a[i] == b[j + 1])
      ++j;
    else
      j = 0;
    if (j == m) ++ans;
  }
  printf("%d\n", ans);
  return 0;
}
