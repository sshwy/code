// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

const int N = 1e6 + 5, P = 998244353;

int pw(int a, int m) {
  int res = 1;
  while (m) m & 1 ? res = 1ll * res * a % P : 0, a = 1ll * a * a % P, m >>= 1;
  return res;
}

int n;

int main() {
  scanf("%d", &n);
  if (n & 1) return puts("0"), 0;
  int m = n / 2;
  int coef = 1;
  FOR(i, 1, m) coef = 1ll * coef * (2 * i - 1) % P;
  int ans = 1ll * pw(m, m - 2) * pw(4, m - 1) % P * coef % P;
  printf("%d\n", ans);
  return 0;
}
