// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (long long i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (long long i = (a); i >= (b); --i)

const long long N = 1e6 + 5;

bool ans[N], flag = true;
long long n, m, c[N], d[N], d2[N], L[N], R[N];

struct Limit {
  long long l, r, x;
} a[N];

void work(long long j) {
  FOR(i, 1, n) c[i] = 0;
  FOR(i, 1, m) if (a[i].x >> j & 1) {
    c[a[i].l]++;
    c[a[i].r + 1]--;
  }
  FOR(i, 1, n) c[i] += c[i - 1];

  L[0] = 0;
  FOR(i, 1, n) L[i] = c[i] == 1 ? i : L[i - 1];
  R[n + 1] = n + 1;
  ROF(i, n, 1) R[i] = c[i] == 1 ? i : R[i + 1];

  FOR(i, 1, n) d[i] = !!c[i] + d[i - 1];
  FOR(i, 1, n) d2[i] = (c[i] > 1) + d2[i - 1];

  long long minR = n + 1, maxL = 0, bad = 0, badId = 0, worst = 0;

  FOR(i, 1, m) if (!(a[i].x >> j & 1)) {
    long long l = a[i].l, r = a[i].r;
    if (d2[r] - d2[l - 1] == r - l + 1) {
      worst++;
      badId = i;
    } else if (d[r] - d[l - 1] == r - l + 1) {
      minR = min(minR, a[i].r);
      maxL = max(maxL, a[i].l);
      bad++;
      badId = i;
    }
  }
  if (worst > 1) {
    FOR(i, 1, m) ans[i] = false;
    flag = false;
    return;
  }
  if (worst == 1) {
    if (bad == 0) {
      FOR(i, 1, m) if (i != badId) ans[i] = false;
      return;
    } else {
      FOR(i, 1, m) ans[i] = false;
      flag = false;
      return;
    }
  }
  // worst == 0
  if (bad == 0) return;
  FOR(i, 1, m) {
    long long l = a[i].l, r = a[i].r;
    if (a[i].x >> j & 1) {
      if (R[l] > r || L[r] < l)
        ans[i] = false;
      else {
        long long x = R[l], y = L[r];
        if (minR < x || y < maxL) { ans[i] = false; }
      }
    } else {
      if (bad > 1 || (bad == 1 && i != badId)) {
        // printf("type 2 j %lld i %lld\n", j, i);
        ans[i] = false;
      }
    }
  }
}

signed main() {
  scanf("%lld%lld", &n, &m);
  FOR(i, 1, m) {
    long long l, r, x;
    scanf("%lld%lld%lld", &l, &r, &x);
    a[i] = {l, r, x};
  }

  FOR(i, 1, m) ans[i] = true;
  FOR(j, 0, 60) if (flag) work(j);

  FOR(i, 1, m) putchar(ans[i] + '0');
  puts("");
  return 0;
}
