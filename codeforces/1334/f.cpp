// by Sshwy
//#define DEBUGGER
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

#ifdef DEBUGGER

#define log_prefix()                                                \
  {                                                                 \
    fprintf(stderr, "\033[37mLine %-3d [%dms]:\033[0m  ", __LINE__, \
        (int)clock() / 1000);                                       \
  }
#define ilog(...) \
  { fprintf(stderr, __VA_ARGS__); }
#define llog(...)      \
  {                    \
    log_prefix();      \
    ilog(__VA_ARGS__); \
  }
#define log(...)           \
  {                        \
    log_prefix();          \
    ilog(__VA_ARGS__);     \
    fprintf(stderr, "\n"); \
  }
#define red(...)       \
  {                    \
    log_prefix();      \
    ilog("\033[31m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define green(...)     \
  {                    \
    log_prefix();      \
    ilog("\033[32m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define blue(...)      \
  {                    \
    log_prefix();      \
    ilog("\033[34m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }

#else

#define log_prefix() ;
#define ilog(...) ;
#define llog(...) ;
#define log(...) ;
#define red(...) ;
#define green(...) ;
#define blue(...) ;

#endif

namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/

const int N = 5e5 + 5, SZ = N * 4;
const long long INF = 1ll << 60;
int n, a[N], p[N], m, b[N];

long long val[SZ], tag[SZ];
void build(int u = 1, int l = 0, int r = m) {
  if (l == r) {
    val[u] = l == 0 ? 0 : INF;
    return;
  }
  int mid = (l + r) >> 1;
  build(u << 1, l, mid), build(u << 1 | 1, mid + 1, r);
}
void nodeadd(int u, long long v) {
  tag[u] += v;
  val[u] += v;
}
void pushdown(int u) {
  if (tag[u]) nodeadd(u << 1, tag[u]), nodeadd(u << 1 | 1, tag[u]), tag[u] = 0;
}
void add(int L, int R, long long v, int u = 1, int l = 0, int r = m) {
  if (L > R || R < l || r < L) return;
  if (L <= l && r <= R) {
    nodeadd(u, v);
    return;
  }
  int mid = (l + r) >> 1;
  pushdown(u);
  add(L, R, v, u << 1, l, mid), add(L, R, v, u << 1 | 1, mid + 1, r);
  // if(u==1)printf("add %d %d %lld\n",L,R,v);
}
void assign(int pos, long long v, int u = 1, int l = 0, int r = m) {
  if (l == r) {
    val[u] = v, tag[u] = 0;
    return;
  }
  int mid = (l + r) >> 1;
  pushdown(u);
  if (pos <= mid)
    assign(pos, v, u << 1, l, mid);
  else
    assign(pos, v, u << 1 | 1, mid + 1, r);
  // if(u==1)printf("assign %d %d\n",pos,v);
}
long long query(int pos, int u = 1, int l = 0, int r = m) {
  if (l == r) return val[u];
  int mid = (l + r) >> 1;
  pushdown(u);
  if (pos <= mid)
    return query(pos, u << 1, l, mid);
  else
    return query(pos, u << 1 | 1, mid + 1, r);
}

int main() {
  scanf("%d", &n);
  FOR(i, 1, n) scanf("%d", &a[i]);
  FOR(i, 1, n) scanf("%d", &p[i]);
  scanf("%d", &m);
  FOR(i, 1, m) scanf("%d", &b[i]);

  build(); // f[0]=0,f[i]=INF

  FOR(i, 1, n) { // a[i]
    int pos = lower_bound(b + 1, b + m + 1, a[i]) - b;
    if (b[pos] == a[i]) {
      // printf("i %d %d\n",i,a[i]);
      add(pos + 1, m, min(p[i], 0));
      long long fpos = query(pos), fpos_1 = query(pos - 1);
      // printf("f %d %lld\n",pos,fpos);
      // printf("f %d %lld\n",pos-1,fpos_1);
      assign(pos, min(fpos + min(p[i], 0), fpos_1));
      add(0, pos - 1, p[i]);
    } else {
      add(pos, m, min(p[i], 0));
      add(0, pos - 1, p[i]);
    }
  }

  long long ans = query(m);
  if (ans > (1ll << 55))
    puts("NO");
  else
    printf("YES\n%lld\n", ans);
  return 0;
}

// f[i,j]: 前i个数的前缀最大值为b[j]时的最小代价。
// 考虑a[i+1]：
//   - a[i+1]<b[j]。则你可以删掉a[i+1]也可以不删。f[i,j]+min(p[i+1],0) ->
//   f[i+1,j]
//   - a[i+1]==b[j]。
//       则可以保留或删除a[i+1]（保留并不影响f的值），则代价是min(p[i+1],0)，转移到f[i+1,j].
//       f[i,j]+min(p[i+1],0) -> f[i+1,j]
//       也可以强制作为前缀最大值里的数，也就是从f[i,j-1]转移过来。f[i,j-1]->f[i+1,j]
//   - a[i+1]>b[j]。
//       选：若a[i+1]==b[j+1]则可以转移到f[i+1,j+1]。f[i,j] -> f[i+1,j+1]
//       否则不合法。 不选：转移到f[i+1,j] f[i,j]+p[i+1] -> f[i+1,j].
