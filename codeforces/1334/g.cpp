// by Sshwy
//#define DEBUGGER
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

#ifdef DEBUGGER

#define log_prefix()                                                \
  {                                                                 \
    fprintf(stderr, "\033[37mLine %-3d [%dms]:\033[0m  ", __LINE__, \
        (int)clock() / 1000);                                       \
  }
#define ilog(...) \
  { fprintf(stderr, __VA_ARGS__); }
#define llog(...)      \
  {                    \
    log_prefix();      \
    ilog(__VA_ARGS__); \
  }
#define log(...)           \
  {                        \
    log_prefix();          \
    ilog(__VA_ARGS__);     \
    fprintf(stderr, "\n"); \
  }
#define red(...)       \
  {                    \
    log_prefix();      \
    ilog("\033[31m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define green(...)     \
  {                    \
    log_prefix();      \
    ilog("\033[32m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define blue(...)      \
  {                    \
    log_prefix();      \
    ilog("\033[34m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }

#else

#define log_prefix() ;
#define ilog(...) ;
#define llog(...) ;
#define log(...) ;
#define red(...) ;
#define green(...) ;
#define blue(...) ;

#endif

namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int LSZ = 19, SZ = 1 << LSZ, P = 998244353;
typedef vector<int> poly;

int pw(int a, int m) {
  int res = 1;
  while (m) m & 1 ? res = 1ll * res * a % P : 0, a = 1ll * a * a % P, m >>= 1;
  return res;
}

int tr[SZ];
int init(int n) {
  int len = 1;
  while (len < n) len <<= 1;
  FOR(i, 0, len - 1) tr[i] = tr[i >> 1] >> 1 | ((i & 1) * (len >> 1));
  return len;
}
void dft(poly &f, int len, int tag) {
  assert(f.size() <= len);
  f.resize(len, 0);
  for (int i = 0; i < len; ++i)
    if (i < tr[i]) swap(f[i], f[tr[i]]);
  for (int j = 1; j < len; j <<= 1)
    for (int i = 0, wn = pw(3, (P - 1) + (P - 1) / (j << 1) * tag); i < len;
         i += j << 1)
      for (int k = i, u, v, w = 1; k < i + j; k++, w = w * 1ll * wn % P)
        u = f[k], v = f[k + j] * 1ll * w % P, f[k] = (u + v) % P,
        f[k + j] = (u - v + P) % P;
  if (tag == -1)
    for (int i = 0, il = pw(len, P - 2); i < len; ++i) f[i] = f[i] * 1ll * il % P;
}

poly operator+(const poly &a, const poly &b) {
  poly res(a);
  res.resize(max(a.size(), b.size()), 0);
  for (int i = 0; i < b.size(); ++i) res[i] = (res[i] + b[i]) % P;
  return res;
}
poly operator*(const poly &a, const poly &b) {
  poly A = a, B = b;
  int len = init(A.size() + B.size());
  poly C(len, 0);
  dft(A, len, 1), dft(B, len, 1);
  FOR(i, 0, len - 1) C[i] = A[i] * 1ll * B[i] % P;
  dft(C, len, -1);
  // for(auto x:C)cout<<x<<" "; cout<<"P"<<endl;
  return C;
}
const int N = 2e5 + 5;
int p[30], ls, lt, val[30];
char s[N], t[N];

int main() {
  FOR(i, 1, 26) scanf("%d", &p[i]);
  FOR(i, 1, 26) val[i] = rand() % P;
  scanf("%s%s", s + 1, t + 1);
  ls = strlen(s + 1), lt = strlen(t + 1);
  poly vs, vt(1, 0);
  for (int i = 1; s[i]; ++i) vs.pb(s[i] - 'a' + 1);
  for (int i = 1; t[i]; ++i) vt.pb(t[i] - 'a' + 1);

  // for(auto x:vs)cout<<x<<" "; cout<<endl;
  // for(auto x:vs)cout<<p[x]<<" "; cout<<endl;

  reverse(vs.begin(), vs.end());
  vs.pb(0);
  poly vp = vs;
  FOR(i, 0, ls - 1) vs[i] = val[vs[i]];
  FOR(i, 0, ls - 1) vp[i] = val[p[vp[i]]];
  FOR(i, 1, lt) vt[i] = val[vt[i]];

  // for(auto x:vt)cout<<x<<" "; cout<<endl;

  poly S(vs.size(), 0), T(vt.size(), 0), f, g;

  auto sq = [](int x) { return x * 1ll * x % P; };

  FOR(i, 0, ls - 1) S[i] = sq(vs[i]) * sq(vp[i]) % P;
  FOR(i, 1, lt) T[i] = 1;
  f = S * T;
  g = g + f;

  FOR(i, 0, ls - 1)
  S[i] = (P - 2ll) * vs[i] % P * vp[i] % P * (vs[i] + vp[i]) % P;
  FOR(i, 1, lt) T[i] = vt[i];
  f = S * T;
  g = g + f;

  FOR(i, 0, ls - 1)
  S[i] = ((4ll * vs[i] % P * vp[i] % P + sq(vp[i])) % P + sq(vs[i])) % P;
  FOR(i, 1, lt) T[i] = sq(vt[i]);
  f = S * T;
  g = g + f;

  FOR(i, 0, ls - 1) S[i] = (P - 2ll) * (vp[i] + vs[i]) % P;
  FOR(i, 1, lt) T[i] = sq(vt[i]) * vt[i] % P;
  f = S * T;
  g = g + f;

  FOR(i, 0, ls - 1) S[i] = 1;
  FOR(i, 1, lt) T[i] = sq(vt[i]) * sq(vt[i]) % P;
  f = S * T;
  g = g + f;

  // for(auto gx:g)cout<<gx<<" "; cout<<endl;

  poly ans;
  FOR(i, 0, lt - ls) ans.pb(g[ls + i] == 0);

  for (auto x : ans) printf("%d", x);
  puts("");

  return 0;
}
