#include <bits/stdc++.h>
#define FOR(a, b, c) for (int a = (int)(b); a <= (int)(c); a++)
#define ROF(a, b, c) for (int a = (int)(b); a >= (int)(c); a--)
using namespace std;
const int N = 105;
int n, q;
struct WaterFill {
  int t, x, y;
  int fillTime(int _x, int _y) { return t + abs(x - _x) + abs(y - _y); }
} A[N];

int main() {
  scanf("%d%d", &n, &q);
  FOR(i, 1, n) { scanf("%d%d%d", &A[i].t, &A[i].x, &A[i].y); }
  FOR(i, 1, q) {
    int a, b, c, d;
    scanf("%d%d%d%d", &a, &b, &c, &d);
    int ans = 1e9;
    FOR(j, 1, n) {
      ans = min(ans, A[j].fillTime(a, b) - 1);
      ans = min(ans, A[j].fillTime(c, d) - abs(a - c) - abs(b - d) - 1);
    }
    printf("%d\n", ans);
  }
  return 0;
}
