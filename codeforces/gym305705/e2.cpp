// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

const int N = 5005, P = 1e9 + 7;

int n, m;
int f[N][N];

void add(int &x, int y) { x = (x + y) % P; }
int main() {
  cin >> n >> m;
  f[1][1] = 1;
  FOR(i, 2, n) {
    FOR(j, 1, m) { add(f[i][1], f[i - 1][j]); }
    FOR(j, 2, m) {
      add(f[i][j], f[i - 1][j - 1]);
      add(f[i][j], f[i - 1][m]);
    }
  }
  printf("%d\n", f[n][m]);
  return 0;
}
