// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA
const int N = 1e5 + 5, P = 1e9 + 7;
int n, m;
vector<int> g[N];

int he[N];
int cnt[N];
void dfs(int u, int p) {
  he[u] = 1;
  for (auto v : g[u])
    if (v != p) dfs(v, u), he[u] = max(he[u], he[v] + 1);
}
int b[N], lb;
int main() {
  scanf("%d%d", &n, &m);
  FOR(i, 1, n - 1) {
    int u, v;
    scanf("%d%d", &u, &v);
    g[u].pb(v);
    g[v].pb(u);
  }
  dfs(1, 0);
  FOR(i, 1, n) cnt[he[i]]++;
  int pos = 1;
  FOR(i, 1, m) {
    char op[5];
    int k;
    scanf("%s", op);
    if (op[0] == 'A') {
      scanf("%d", &k);
      if (pos <= 1)
        b[++lb] = k, --pos;
      else
        cnt[pos - 1] = cnt[pos] * 1ll * k % P, --pos;
    } else {
      if (lb) --lb;
      ++pos;
    }
    // if(pos>0)printf("%d\n",cnt[pos]);
  }
  int ans = 0;
  if (lb) {
    FOR(i, 1, n) ans = (ans + cnt[i]) % P;
    FOR(i, 1, lb) {
      cnt[1] = cnt[1] * 1ll * b[i] % P;
      ans = (ans + cnt[1]) % P;
    }
  } else {
    FOR(i, pos, n) ans = (ans + cnt[i]) % P;
  }
  printf("%d\n", ans);
  return 0;
}
