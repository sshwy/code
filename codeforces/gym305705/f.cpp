#include <bits/stdc++.h>
#define FOR(a, b, c) for (int a = (int)(b); a <= (int)(c); a++)
#define ROF(a, b, c) for (int a = (int)(b); a >= (int)(c); a--)
using namespace std;

const int N = 1e5 + 5, M = 2e5 + 5;
int n, m;

struct Range {
  int l, r;
} a[N];
enum NodeType { _range, _path };
struct Node {
  NodeType typ;
  int l, r;
  long long dist;
} t[N << 2];

Node merge(Node x, Node y) {
  if (x.typ == _range && y.typ == _range) {
    if (x.r < y.l) return Node{_path, x.r, y.l, 0ll + y.l - x.r};
    if (y.r < x.l) return Node{_path, x.l, y.r, 0ll + x.l - y.r};
    return Node{_range, max(x.l, y.l), min(x.r, y.r), 0};
  } else if (x.typ == _range) {
    if (y.l < x.l) return Node{_path, x.l, y.r, x.l - y.l + y.dist};
    if (y.l > x.r) return Node{_path, x.r, y.r, y.l - x.r + y.dist};
    return y;
  } else if (y.typ == _range) {
    if (x.r < y.l) return Node{_path, x.l, y.l, x.dist + y.l - x.r};
    if (x.r > y.r) return Node{_path, x.l, y.r, x.dist + x.r - y.r};
    return x;
  } else
    return Node{_path, x.l, y.r, x.dist + y.dist + abs(x.r - y.l)};
}

void pushup(int u) { t[u] = merge(t[u << 1], t[u << 1 | 1]); }
void build(int u, int l, int r) {
  if (l == r) {
    t[u] = Node{_range, a[l].l, a[l].r, 0};
    return;
  }
  int mid = (l + r) >> 1;
  build(u << 1, l, mid);
  build(u << 1 | 1, mid + 1, r);
  pushup(u);
}
Node qry(int L, int R, int u = 1, int l = 1, int r = n) {
  if (L <= l && r <= R) return t[u];
  int mid = (l + r) >> 1;
  if (R <= mid) return qry(L, R, u << 1, l, mid);
  if (mid < L) return qry(L, R, u << 1 | 1, mid + 1, r);
  return merge(qry(L, R, u << 1, l, mid), qry(L, R, u << 1 | 1, mid + 1, r));
}
void assign(int pos, Node v, int u = 1, int l = 1, int r = n) {
  if (l == r) {
    t[u] = v;
    return;
  }
  int mid = (l + r) >> 1;
  if (pos <= mid)
    assign(pos, v, u << 1, l, mid);
  else
    assign(pos, v, u << 1 | 1, mid + 1, r);
  pushup(u);
}

int main() {
  scanf("%d%d", &n, &m);
  FOR(i, 1, n) {
    int l, r;
    scanf("%d%d", &l, &r);
    a[i].l = l;
    a[i].r = r;
  }
  build(1, 1, n);
  FOR(i, 1, m) {
    char op[10];
    scanf("%s", op);
    if (op[0] == 'M') {
      int k, u, v;
      scanf("%d%d%d", &k, &u, &v);
      assign(k, Node{_range, u, v, 0});
    } else {
      int p, q;
      scanf("%d%d", &p, &q);
      Node ans = qry(p, q);
      if (ans.typ == _range)
        puts("0");
      else
        printf("%lld\n", ans.dist);
    }
  }
  return 0;
}
