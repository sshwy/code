// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA
const int N = 500;
int n, m, ans;
string s[N];
int main() {
  cin >> n >> m;
  FOR(i, 0, n - 1) cin >> s[i];
  FOR(i, 1, n - 2)
  FOR(j, 1, m - 2)
  if (s[i][j] == '.' && s[i - 1][j] == '.' && s[i + 1][j] == '.' &&
      s[i][j - 1] == '.' && s[i][j + 1] == '.') {
    ++ans;
  }
  cout << ans << endl;
  return 0;
}
