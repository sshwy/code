// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA
const int N = 1e5 + 5;
int n, k;
int a[N];
int main() {
  scanf("%d%d", &n, &k);
  long long ans = 0, mx = 0;
  FOR(i, 1, n) scanf("%d", a + i), ans += a[i], mx = max(mx, 0ll + a[i]);
  ans += mx * (k - 1);
  printf("%lld\n", ans);
  return 0;
}
