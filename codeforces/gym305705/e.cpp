#include <bits/stdc++.h>
#define int long long
#define FOR(a, b, c) for (int a = (int)(b); a <= (int)(c); a++)
#define ROF(a, b, c) for (int a = (int)(b); a >= (int)(c); a--)
using namespace std;
const int N = 1e6 + 5, P = 1e9 + 7;
int n, m;
int f[N][4], g[N][4], h[N][4];
void add(int &x, int y) { x = (x + y) % P; }
void sub(int &x, int y) { x = (x - y + P) % P; }
signed main() {
  scanf("%lld%lld", &n, &m);
  if (m == 1) {
    puts("1");
    return 0;
  }
  f[1][1] = 1;
  g[1][1] = 1;
  h[1][1] = 1;
  FOR(i, 2, n) {
    if (i > m) FOR(j, 0, 3) add(f[i][0], f[i - m][j]);
    if (i == m) f[i][0] = 1;
    for (auto j : {0, 1, 2, 3})
      add(f[i][1], g[i - 1][j]), sub(f[i][1], g[max(0ll, i - m)][j]);
    for (auto j : {0, 2})
      add(f[i][2], g[i - 1][j]), sub(f[i][2], g[max(0ll, i - m)][j]);
    for (auto j : {0, 2}) {
      add(f[i][3], h[i - 1][j]);
      sub(f[i][3], h[max(0ll, i - m)][j]);
      add(f[i][3], g[i - 1][j] * 1ll * (m - i - 1) % P);
      sub(f[i][3], g[max(0ll, i - m)][j] * 1ll * (m - i - 1) % P);
    }
    if (i < m) add(f[i][1], 1);
    FOR(j, 0, 3) {
      g[i][j] = f[i][j];
      add(g[i][j], g[i - 1][j]);
      h[i][j] = f[i][j] * 1ll * i % P;
      add(h[i][j], h[i - 1][j]);
      // printf("f[%lld,%lld] = %lld\n",i,j,f[i][j]);
    }
  }
  int ans = f[n][0];
  add(ans, f[n][2]);
  ans = (ans + P) % P;
  printf("%lld\n", ans);
  return 0;
}
