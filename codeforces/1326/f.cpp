// by Sshwy
//#define DEBUGGER
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

#ifdef DEBUGGER

#define log_prefix()                                                \
  {                                                                 \
    fprintf(stderr, "\033[37mLine %-3d [%dms]:\033[0m  ", __LINE__, \
        (int)clock() / 1000);                                       \
  }
#define ilog(...) \
  { fprintf(stderr, __VA_ARGS__); }
#define llog(...)      \
  {                    \
    log_prefix();      \
    ilog(__VA_ARGS__); \
  }
#define log(...)           \
  {                        \
    log_prefix();          \
    ilog(__VA_ARGS__);     \
    fprintf(stderr, "\n"); \
  }
#define red(...)       \
  {                    \
    log_prefix();      \
    ilog("\033[31m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define green(...)     \
  {                    \
    log_prefix();      \
    ilog("\033[32m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define blue(...)      \
  {                    \
    log_prefix();      \
    ilog("\033[34m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }

#else

#define log_prefix() ;
#define ilog(...) ;
#define llog(...) ;
#define log(...) ;
#define red(...) ;
#define green(...) ;
#define blue(...) ;

#endif

namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 18, NN = 1 << 18;
typedef long long LL;
int n, nn, g[N];
vector<vector<int>> part;
map<vector<int>, LL> part_ans;

void dfs(int s, int las, vector<int> &cur) {
  if (s == 0) part.push_back(cur);
  FOR(i, las, s) {
    cur.push_back(i);
    dfs(s - i, i, cur);
    cur.pop_back();
  }
}

int main() {
  scanf("%d", &n);
  FOR(i, 0, n - 1) {
    FOR(j, 0, n - 1) {
      int x;
      scanf("%1d", &x);
      if (x) g[i] |= 1 << j;
    }
  }

  vector<int> tmp;
  dfs(n, 1, tmp);

  nn = 1 << n;

  vector<vector<LL>> f(n + 1, vector<LL>(nn, 0)), h(n, vector<LL>(nn, 0));
  FOR(i, 0, n - 1) h[i][1 << i] = 1;
  FOR(i, 0, nn - 1) {
    if (__builtin_popcount(i) > 1) {
      FOR(j, 0, n - 1)
      if (i >> j & 1) FOR(k, 0, n - 1) if (i >> k & 1 && j != k && g[j] >> k & 1) {
          h[j][i] += h[k][i ^ (1 << j)];
        }
    }
  }

  auto fwt = [](vector<LL> &f, int n) {
    FOR(i, 0, n - 1)
    FOR(j, 0, (1 << n) - 1) if (j >> i & 1) f[j] += f[j ^ (1 << i)];
  };
  auto ifwt = [](vector<LL> &f, int n) {
    FOR(i, 0, n - 1)
    FOR(j, 0, (1 << n) - 1) if (j >> i & 1) f[j] -= f[j ^ (1 << i)];
  };

  FOR(i, 0, nn - 1) { FOR(j, 0, n - 1) f[__builtin_popcount(i)][i] += h[j][i]; }
  FOR(i, 1, n) fwt(f[i], n);

  for (auto p : part) {
    vector<LL> v;
    for (auto x : p) {
      if (!v.size())
        v = f[x];
      else
        FOR(i, 0, nn - 1) v[i] *= f[x][i];
    }
    ifwt(v, n);
    part_ans[p] = v[nn - 1];
  }

  auto calc = [](int mask) {
    int las = 1;
    vector<int> x;
    FOR(i, 0, n - 1) {
      if (mask >> i & 1) {
        ++las;
      } else {
        x.pb(las);
        las = 1;
      }
    }
    sort(x.begin(), x.end());
    return x;
  };

  int n2 = 1 << (n - 1);
  vector<LL> ans(n2, 0);
  FOR(i, 0, n2 - 1) ans[i] = part_ans[calc(i)];

  FOR(i, 0, n - 2)
  FOR(j, 0, n2 - 1) if (j >> i & 1) ans[j ^ (1 << i)] -= ans[j];
  FOR(i, 0, n2 - 1) printf("%lld%c", ans[i], " \n"[i == n2 - 1]);

  return 0;
}

//长度为n的排列，每个二进制串的出现次数。
// 1表示必须有边，0表示没有边。
//容斥：0无限制。则g(S)=f(T) T是S的超集。最后容斥回去即可。考虑g(S)。
//相当于，一段连续的1是一条路径。若干条路径组成串。
//如果确定了路径长度的可重集合，那么我们可以计算出方案数。
//设h(x,S)表示在集合S中选出长度为|S|的，以x结尾的有向路径的方案数。
//枚举集合，可以或卷积计算路径选择方案数。（不用限制不相交。因为如果你选了同一个点，那么总的被选点数就达不到n了）
