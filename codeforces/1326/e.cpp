// by Sshwy
//#define DEBUGGER
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

#ifdef DEBUGGER

#define log_prefix()                                                \
  {                                                                 \
    fprintf(stderr, "\033[37mLine %-3d [%dms]:\033[0m  ", __LINE__, \
        (int)clock() / 1000);                                       \
  }
#define ilog(...) \
  { fprintf(stderr, __VA_ARGS__); }
#define llog(...)      \
  {                    \
    log_prefix();      \
    ilog(__VA_ARGS__); \
  }
#define log(...)           \
  {                        \
    log_prefix();          \
    ilog(__VA_ARGS__);     \
    fprintf(stderr, "\n"); \
  }
#define red(...)       \
  {                    \
    log_prefix();      \
    ilog("\033[31m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define green(...)     \
  {                    \
    log_prefix();      \
    ilog("\033[32m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define blue(...)      \
  {                    \
    log_prefix();      \
    ilog("\033[34m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }

#else

#define log_prefix() ;
#define ilog(...) ;
#define llog(...) ;
#define log(...) ;
#define red(...) ;
#define green(...) ;
#define blue(...) ;

#endif

namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 3e5 + 5, SZ = N << 2;
int n, p[N], q[N], ans[N], a[N], c[N];

struct seg {
  int mx[SZ], tag[SZ];
  void pushdown(int u) {
    if (tag[u]) {
      mx[u << 1] += tag[u], tag[u << 1] += tag[u];
      mx[u << 1 | 1] += tag[u], tag[u << 1 | 1] += tag[u];
      tag[u] = 0;
    }
  }
  void add(int L, int R, int v, int u = 1, int l = 1, int r = n) {
    // if(u==1)printf("add %d %d %d\n",L,R,v);
    if (R < l || r < L) return;
    if (L <= l && r <= R) return mx[u] += v, tag[u] += v, void();
    int mid = (l + r) >> 1;
    pushdown(u);
    add(L, R, v, u << 1, l, mid), add(L, R, v, u << 1 | 1, mid + 1, r);
    mx[u] = max(mx[u << 1], mx[u << 1 | 1]);
  }
  void assign(int pos, int v, int u = 1, int l = 1, int r = n) {
    // if(u==1)printf("assign %d %d\n",pos,v);
    if (l == r) {
      mx[u] = v;
      return;
    }
    int mid = (l + r) >> 1;
    pushdown(u);
    if (pos <= mid)
      assign(pos, v, u << 1, l, mid);
    else
      assign(pos, v, u << 1 | 1, mid + 1, r);
    mx[u] = max(mx[u << 1], mx[u << 1 | 1]);
  }
  int query_max(int L, int R, int u = 1, int l = 1, int r = n) {
    // if(u==1)printf("query_max %d %d %d\n",L,R,mx[u]);
    if (R < l || r < L) return -1e9;
    if (L <= l && r <= R) return mx[u];
    int mid = (l + r) >> 1;
    pushdown(u);
    return max(
        query_max(L, R, u << 1, l, mid), query_max(L, R, u << 1 | 1, mid + 1, r));
  }
} X;

priority_queue<int> A, B;

int main() {
  cin >> n;
  FOR(i, 1, n) cin >> p[i];
  FOR(i, 1, n) a[p[i]] = i;
  FOR(i, 1, n) cin >> q[i];

  ans[1] = n;
  X.add(1, a[n], 1);

  FOR(i, 1, n - 1) {
    int u = q[i]; // bomb
    X.add(1, u, -1);
    int x = ans[i];
    while (X.query_max(1, n) <= 0) {
      --x;
      X.add(1, a[x], 1);
    }
    ans[i + 1] = x;
    // printf("ans[%d]=%d\n",i+1,x);
  }

  FOR(i, 1, n) cout << ans[i] << " \n"[i == n];
  return 0;
}
// 考虑答案是否小于x。
// 对于最靠右的>=x的数，它得被bomb掉。
// 然后第二靠右的>=x的数也得bomb掉。
