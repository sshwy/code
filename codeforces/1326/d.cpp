// by Sshwy
//#define DEBUGGER
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

#ifdef DEBUGGER

#define log_prefix()                                                \
  {                                                                 \
    fprintf(stderr, "\033[37mLine %-3d [%dms]:\033[0m  ", __LINE__, \
        (int)clock() / 1000);                                       \
  }
#define ilog(...) \
  { fprintf(stderr, __VA_ARGS__); }
#define llog(...)      \
  {                    \
    log_prefix();      \
    ilog(__VA_ARGS__); \
  }
#define log(...)           \
  {                        \
    log_prefix();          \
    ilog(__VA_ARGS__);     \
    fprintf(stderr, "\n"); \
  }
#define red(...)       \
  {                    \
    log_prefix();      \
    ilog("\033[31m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define green(...)     \
  {                    \
    log_prefix();      \
    ilog("\033[32m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define blue(...)      \
  {                    \
    log_prefix();      \
    ilog("\033[34m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }

#else

#define log_prefix() ;
#define ilog(...) ;
#define llog(...) ;
#define log(...) ;
#define red(...) ;
#define green(...) ;
#define blue(...) ;

#endif

namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int SZ = 1e6 + 5, ALP = 26;

struct PAM {
  int tot, last;
  int len[SZ], tr[SZ][ALP], fail[SZ];
  int s[SZ], ls;
  int newnode(int l) {
    return ++tot, len[tot] = l, fail[tot] = 0, fill(tr[tot], tr[tot] + ALP, 0), tot;
  }
  void clear() {
    tot = -1, newnode(0), newnode(-1);
    fail[0] = 1, last = 0;
    s[ls = 0] = -1;
  }
  PAM() { clear(); }
  int getfail(int u) {
    while (s[ls - len[u] - 1] != s[ls]) u = fail[u];
    return u;
  }
  void insert(char c) {
    s[++ls] = (c -= 'a');
    int cur = getfail(last);
    if (!tr[cur][c]) {
      int u = newnode(len[cur] + 2);
      fail[u] = tr[getfail(fail[cur])][c];
      tr[cur][c] = u;
    }
    last = tr[cur][c];
  }
} pam;
const int N = 1e6 + 5;

char s[N];
int n, x;

bool check_pal() {
  for (int i = 1, j = n; i < j; i++, j--)
    if (s[i] != s[j]) return 0;
  return 1;
}

void go() {
  cin >> (s + 1);
  n = strlen(s + 1);
  if (check_pal()) {
    cout << (s + 1) << endl;
    return;
  }
  x = 0; //前后缀匹配的长度
  while (x < n && s[x + 1] == s[n - x]) ++x;

  int l1 = 0, L1 = -1, R1 = -1;
  pam.clear();
  FOR(i, 1, n) {
    pam.insert(s[i]);
    if (n - i <= x) {
      int u = pam.last;
      while ((n - i) * 2 + pam.len[u] > n) u = pam.fail[u];
      int len = pam.len[u];
      int llen = (n - i) * 2 + len;
      if (llen > l1) l1 = llen, L1 = i - len + 1, R1 = i;
    }
  }
  pam.clear();
  int l2 = 0, L2 = -1, R2 = -1;
  ROF(i, n, 1) {
    pam.insert(s[i]);
    if (i - 1 <= x) {
      int u = pam.last;
      while ((i - 1) * 2 + pam.len[u] > n) u = pam.fail[u];
      int len = pam.len[u];
      int llen = (i - 1) * 2 + len;
      if (llen > l2) l2 = llen, L2 = i, R2 = i + len - 1;
    }
  }
  if (l1 > l2) {
    int y = n - R1;
    FOR(i, 1, y) cout << s[i];
    FOR(i, L1, n) cout << s[i];
    cout << endl;
  } else {
    int y = L2 - 1;
    FOR(i, 1, R2) cout << s[i];
    FOR(i, n - y + 1, n) cout << s[i];
    cout << endl;
  }
}
int main() {
  ios::sync_with_stdio(false);
  int t;
  cin >> t;
  FOR(i, 1, t) go();
  return 0;
}
