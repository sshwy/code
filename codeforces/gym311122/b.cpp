// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

bool check(int x, int d) {
  if (d > 0) {
    if (x >= 100) return true;
    while (x > 0) {
      if (x / 10 == d || x % 10 == d) return true;
      x -= d;
    }
    return false;
  } else {
    if (x % 10 == 0) return true;
    if (100 + (x % 10) > x) return false;
    return true;
  }
}
void go() {
  int q, d;
  scanf("%d%d", &q, &d);
  FOR(i, 1, q) {
    int x;
    scanf("%d", &x);
    puts(check(x, d) ? "YES" : "NO");
  }
}
int main() {
  int t;
  scanf("%d", &t);
  FOR(i, 1, t) go();
  return 0;
}
