// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

using namespace RA;

int a[1000];
long long d[2000];
void go() {
  int n = rnd(1, 100);
  FOR(i, 1, n) a[i] = rnd(1, 1e9);
  sort(a + 1, a + n + 1);
  n = unique(a + 1, a + n + 1) - a - 1;
  FOR(i, 1, n) a[i + n] = -a[i];
  random_shuffle(a + 1, a + n * 2 + 1);
  cout << n << endl;
  FOR(i, 1, n * 2) {
    d[i] = 0;
    FOR(j, 1, n * 2) d[i] += abs(a[i] - a[j]);
    cout << d[i] << " \n"[i == n * 2];
  }
}
int main() {
  srand(clock() + time(0));
  int t = 1000;
  cout << t << endl;
  FOR(i, 1, t) go();
  return 0;
}
