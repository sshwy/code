// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

const int N = 2e5 + 5;
int n;
long long d[N], b[N], s[N], a[N];

bool go() {
  scanf("%d", &n);
  FOR(i, 1, n * 2) scanf("%lld", d + i);
  sort(d + 1, d + n * 2 + 1);
  FOR(i, 1, n) if (d[i * 2] != d[i * 2 - 1] || d[i * 2] == 0) return false;
  FOR(i, 1, n) {
    b[i] = d[i * 2];
    if (b[i] % 2) return false;
    b[i] /= 2;
  }
  ROF(i, n, 1) {
    if (i == n) {
      if (b[i] % n) return false;
      a[i] = b[i] / n;
      s[i] = a[i];
    } else {
      b[i] -= s[i + 1];
      if (b[i] % i) return false;
      a[i] = b[i] / i;
      s[i] = a[i] + s[i + 1];
    }
  }
  FOR(i, 1, n) if (a[i] <= 0) return false;
  FOR(i, 2, n) if (a[i] <= a[i - 1]) return false;
  // printf("a: "); FOR(i,1,n)printf("%lld%c", a[i]," \n"[i==n]);
  return true;
}
int main() {
  int t;
  scanf("%d", &t);
  FOR(i, 1, t) puts(go() ? "YES" : "NO");
  return 0;
}
