// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

void go() {
  int n;
  cin >> n;
  int cnt[1000] = {0};
  FOR(i, 1, n) {
    int x;
    cin >> x;
    cnt[x]++;
  }
  cout << *max_element(cnt, cnt + 1000) << endl;
}

int main() {
  int t;
  cin >> t;
  FOR(i, 1, t) go();
  return 0;
}
