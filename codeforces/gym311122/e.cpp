// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

const int N = 2e5 + 5, SZ = N * 4;

int n, q;
char s[N], f[N];
int cnt[SZ][2], typ[SZ], tag[SZ];

void pushup(int u) {
  FOR(i, 0, 1) cnt[u][i] = cnt[u << 1][i] + cnt[u << 1 | 1][i];
  if (typ[u << 1] == typ[u << 1 | 1])
    typ[u] = typ[u << 1];
  else
    typ[u] = -1;
}
void build(int u, int l, int r) {
  tag[u] = 0;
  if (l == r) {
    cnt[u][0] = f[l] == '0';
    cnt[u][1] = f[l] == '1';
    typ[u] = f[l] == '1';
    return;
  }
  int mid = (l + r) >> 1;
  build(u << 1, l, mid);
  build(u << 1 | 1, mid + 1, r);
  pushup(u);
  assert(cnt[u][0] + cnt[u][1] == r - l + 1);
}

void add(pair<int, int> &x, pair<int, int> y) {
  x.first += y.first;
  x.second += y.second;
}
void cover(int u, int c) {
  if (c == 0) {
    cnt[u][0] += cnt[u][1];
    cnt[u][1] = 0;
    typ[u] = 0;
    tag[u] = c + 1;
  } else {
    cnt[u][1] += cnt[u][0];
    cnt[u][0] = 0;
    typ[u] = 1;
    tag[u] = c + 1;
  }
}
void pushdown(int u) {
  if (tag[u]) {
    // printf("pushdown %d\n",u);
    cover(u << 1, tag[u] - 1);
    cover(u << 1 | 1, tag[u] - 1);
    tag[u] = 0;
  }
}
pair<int, int> qry(int L, int R, int u, int l, int r) {
  // printf("u %d %d %d\n",u,l,r);
  assert(cnt[u][0] + cnt[u][1] == r - l + 1);
  if (L <= l && r <= R) return make_pair(cnt[u][0], cnt[u][1]);
  int mid = (l + r) >> 1;
  pushdown(u);
  pair<int, int> res(0, 0);
  if (L <= mid) add(res, qry(L, R, u << 1, l, mid));
  if (mid < R) add(res, qry(L, R, u << 1 | 1, mid + 1, r));
  pushup(u);
  return res;
}
void assign(int L, int R, int v, int u, int l, int r) {
  assert(cnt[u][0] + cnt[u][1] == r - l + 1);
  if (L <= l && r <= R) {
    cover(u, v);
    return;
  }
  int mid = (l + r) >> 1;
  pushdown(u);
  if (L <= mid) assign(L, R, v, u << 1, l, mid);
  if (mid < R) assign(L, R, v, u << 1 | 1, mid + 1, r);
  pushup(u);
}

bool check(int u, int l, int r) {
  if (l == r) {
    if (s[l] == '1')
      return cnt[u][1];
    else
      return cnt[u][0];
  }
  int mid = (l + r) >> 1;
  pushdown(u);
  return check(u << 1, l, mid) && check(u << 1 | 1, mid + 1, r);
}

int L[N], R[N];

bool go() {
  scanf("%d%d", &n, &q);
  scanf("%s", s + 1);
  scanf("%s", f + 1);
  FOR(i, 1, q) scanf("%d%d", L + i, R + i);
  build(1, 1, n);
  ROF(i, q, 1) {
    int cl = L[i], cr = R[i];
    pair<int, int> p = qry(cl, cr, 1, 1, n);
    // printf("qry %d %d : c0 %d c1 %d\n",cl,cr, p.first,p.second);
    if (p.first < p.second) {
      // printf("assign %d %d %d\n",cl,cr,1);
      assign(cl, cr, 1, 1, 1, n);
    } else if (p.first > p.second) {
      // printf("assign %d %d %d\n",cl,cr,0);
      assign(cl, cr, 0, 1, 1, n);
    } else {
      return false;
    }
  }
  if (check(1, 1, n)) return true;
  return false;
}
int main() {
  int t;
  scanf("%d", &t);
  FOR(i, 1, t) puts(go() ? "YES" : "NO");
  return 0;
}
