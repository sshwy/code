// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

const int N = 2e5 + 5;
long long gcd(long long a, long long b) { return b ? gcd(b, a % b) : a; }
int n;
long long K, a[N];

void go() {
  scanf("%d%lld", &n, &K);
  FOR(i, 1, n) { scanf("%lld", a + i); }
  long long g = 0;
  FOR(i, 2, n) { g = gcd(g, abs(a[i] - a[1])); }
  if (abs(K - a[1]) % g)
    puts("NO");
  else
    puts("YES");
}
int main() {
  int t;
  scanf("%d", &t);
  FOR(i, 1, t) go();
  return 0;
}
