// by Sshwy
//#define DEBUGGER
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

#ifdef DEBUGGER

#define log_prefix()                                                \
  {                                                                 \
    fprintf(stderr, "\033[37mLine %-3d [%dms]:\033[0m  ", __LINE__, \
        (int)clock() / 1000);                                       \
  }
#define ilog(...) \
  { fprintf(stderr, __VA_ARGS__); }
#define llog(...)      \
  {                    \
    log_prefix();      \
    ilog(__VA_ARGS__); \
  }
#define log(...)           \
  {                        \
    log_prefix();          \
    ilog(__VA_ARGS__);     \
    fprintf(stderr, "\n"); \
  }
#define red(...)       \
  {                    \
    log_prefix();      \
    ilog("\033[31m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define green(...)     \
  {                    \
    log_prefix();      \
    ilog("\033[32m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define blue(...)      \
  {                    \
    log_prefix();      \
    ilog("\033[34m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }

#else

#define log_prefix() ;
#define ilog(...) ;
#define llog(...) ;
#define log(...) ;
#define red(...) ;
#define green(...) ;
#define blue(...) ;

#endif

namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 205;
int n, m;
int a[N][N], b[N][N];

int main() {
  cin >> n >> m;
  FOR(i, 1, n) FOR(j, 1, m) scanf("%d", &a[i][j]);
  if (n == 1) {
    cout << "YES" << endl;
    cout << "0" << endl;
    FOR(i, 1, m) cout << a[1][i];
    cout << endl;
    return 0;
  }
  //第一行全0
  {
    FOR(i, 1, n) FOR(j, 1, m) b[i][j] = a[i][j];
    string s(m, '0'), t(n, '0');
    FOR(j, 1, m) if (b[1][j]) {
      FOR(i, 1, n) b[i][j] ^= 1;
      s[j - 1] = '1';
    }
    int cur = 0, flag = 1;
    FOR(i, 1, n) {
      int f01 = 0, f10 = 0;
      FOR(j, 1, m - 1) {
        if (b[i][j] < b[i][j + 1]) f01 = 1;
        if (b[i][j] > b[i][j + 1]) f10 = 1;
      }
      if (f01 && f10)
        flag = 0;
      else if (f01) {
        if (cur == 0)
          cur = 1;
        else
          flag = 0;
      } else if (f10) {
        t[i - 1] = '1';
        if (cur == 0)
          cur = 1;
        else
          flag = 0;
      } else {
        if (b[i][1] != cur) t[i - 1] = '1';
      }
    }
    if (flag) {
      cout << "YES" << endl << t << endl << s << endl;
      return 0;
    }
  }
  {
    FOR(i, 1, n) FOR(j, 1, m) b[i][j] = a[i][j];
    string s(m, '0'), t(n, '0');
    FOR(j, 1, m) if (!b[1][j]) {
      FOR(i, 1, n) b[i][j] ^= 1;
      s[j - 1] = '1';
    }
    int cur = 0, flag = 1;
    FOR(i, 1, n) {
      int f01 = 0, f10 = 0;
      FOR(j, 1, m - 1) {
        if (b[i][j] < b[i][j + 1]) f01 = 1;
        if (b[i][j] > b[i][j + 1]) f10 = 1;
      }
      if (f01 && f10)
        flag = 0;
      else if (f01) {
        if (cur == 0)
          cur = 1;
        else
          flag = 0;
      } else if (f10) {
        t[i - 1] = '1';
        if (cur == 0)
          cur = 1;
        else
          flag = 0;
      } else {
        if (b[i][1] != cur) t[i - 1] = '1';
      }
    }
    if (flag) {
      cout << "YES" << endl << t << endl << s << endl;
      return 0;
    }
  }
  //最后一行全1
  {
    FOR(i, 1, n) FOR(j, 1, m) b[i][j] = a[i][j];
    string s(m, '0'), t(n, '0');
    FOR(j, 1, m) if (b[n][j]) {
      FOR(i, 1, n) b[i][j] ^= 1;
      s[j - 1] = '1';
    }
    int cur = 0, flag = 1;
    FOR(i, 1, n) {
      int f01 = 0, f10 = 0;
      FOR(j, 1, m - 1) {
        if (b[i][j] < b[i][j + 1]) f01 = 1;
        if (b[i][j] > b[i][j + 1]) f10 = 1;
      }
      if (f01 && f10)
        flag = 0;
      else if (f01) {
        if (cur == 0)
          cur = 1;
        else
          flag = 0;
      } else if (f10) {
        t[i - 1] = '1';
        if (cur == 0)
          cur = 1;
        else
          flag = 0;
      } else {
        if (b[i][1] != cur) t[i - 1] = '1';
      }
    }
    if (flag) {
      cout << "YES" << endl << t << endl << s << endl;
      return 0;
    }
  }
  {
    FOR(i, 1, n) FOR(j, 1, m) b[i][j] = a[i][j];
    string s(m, '0'), t(n, '0');
    FOR(j, 1, m) if (!b[n][j]) {
      FOR(i, 1, n) b[i][j] ^= 1;
      s[j - 1] = '1';
    }
    int cur = 0, flag = 1;
    FOR(i, 1, n) {
      int f01 = 0, f10 = 0;
      FOR(j, 1, m - 1) {
        if (b[i][j] < b[i][j + 1]) f01 = 1;
        if (b[i][j] > b[i][j + 1]) f10 = 1;
      }
      if (f01 && f10)
        flag = 0;
      else if (f01) {
        if (cur == 0)
          cur = 1;
        else
          flag = 0;
      } else if (f10) {
        t[i - 1] = '1';
        if (cur == 0)
          cur = 1;
        else
          flag = 0;
      } else {
        if (b[i][1] != cur) t[i - 1] = '1';
      }
    }
    if (flag) {
      cout << "YES" << endl << t << endl << s << endl;
      return 0;
    }
  }
  cout << "NO" << endl;
  return 0;
}
