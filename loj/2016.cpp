// by Sshwy
//#define DEBUGGER
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

#ifdef DEBUGGER

#define log_prefix()                                                \
  {                                                                 \
    fprintf(stderr, "\033[37mLine %-3d [%dms]:\033[0m  ", __LINE__, \
        (int)clock() / 1000);                                       \
  }
#define ilog(...) \
  { fprintf(stderr, __VA_ARGS__); }
#define llog(...)      \
  {                    \
    log_prefix();      \
    ilog(__VA_ARGS__); \
  }
#define log(...)           \
  {                        \
    log_prefix();          \
    ilog(__VA_ARGS__);     \
    fprintf(stderr, "\n"); \
  }
#define red(...)       \
  {                    \
    log_prefix();      \
    ilog("\033[31m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define green(...)     \
  {                    \
    log_prefix();      \
    ilog("\033[32m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define blue(...)      \
  {                    \
    log_prefix();      \
    ilog("\033[34m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }

#else

#define log_prefix() ;
#define ilog(...) ;
#define llog(...) ;
#define log(...) ;
#define red(...) ;
#define green(...) ;
#define blue(...) ;

#endif

namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 2e5 + 5, LW = 18, W = 1 << LW, SZ = W << 2;
int n, m;
int a[N];
int mx[SZ];
void insert(int pos, int v, int u = 1, int l = 0, int r = W - 1) {
  if (l == r) return mx[u] = v, void();
  int mid = (l + r) >> 1;
  if (pos <= mid)
    insert(pos, v, u << 1, l, mid);
  else
    insert(pos, v, u << 1 | 1, mid + 1, r);
  mx[u] = max(mx[u << 1], mx[u << 1 | 1]);
}
int qry_max(int L, int R, int u = 1, int l = 0, int r = W - 1) {
  if (R < l || r < L || L > R) return 0;
  if (L <= l && r <= R) return mx[u];
  int mid = (l + r) >> 1;
  return max(qry_max(L, R, u << 1, l, mid), qry_max(L, R, u << 1 | 1, mid + 1, r));
}
int qry(int b, int x, int L) {
  int res = 0, k = LW, l = 0, r = W - 1;
  while (k) {
    --k;
    int mid = (l + r) >> 1;
    if (b >> k & 1) {
      if (qry_max(l - x, mid - x) >= L) {
        r = mid;
        res ^= (1 << k);
      } else
        l = mid + 1;
    } else {
      if (qry_max(mid + 1 - x, r - x) >= L) {
        l = mid + 1;
        res ^= (1 << k);
      } else
        r = mid;
    }
  }
  return res;
}
struct atom {
  int a, b, c, d;
};
vector<atom> qr[N];
int ans[N];
int main() {
  scanf("%d%d", &n, &m);
  FOR(i, 1, n) scanf("%d", &a[i]);
  FOR(i, 1, m) {
    int b, x, l, r;
    scanf("%d%d%d%d", &b, &x, &l, &r);
    qr[r].pb({b, x, l, i});
  }
  FOR(i, 1, n) {
    insert(a[i], i);
    for (atom x : qr[i]) { ans[x.d] = qry(x.a, x.b, x.c); }
  }
  FOR(i, 1, m) printf("%d\n", ans[i]);
  return 0;
}

// 离线
// 按r排序
// 考虑r=i的所有询问。相当于，序列中插入一个a[i]后排序。
// 然后我们从大到小考虑每一位能否是1
// 那么就要求区间中是否存在[l,r]中的数
// 即区间最大值
// 可以log做
// 那么一次查询的复杂度就是log^2的
