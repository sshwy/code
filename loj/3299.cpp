// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

const int N = 2e6 + 5;

struct atom {
  int op, t, x, y;
} a[N];
int d[N], ld;

int A[N], a2[N], B[N], b2[N], totB;

void add(int *f, int pos, int v) {
  assert(pos > 0);
  for (int i = pos; i < N; i += i & -i) f[i] += v;
}
int pre(int *f, int pos) {
  int res = 0;
  for (int i = pos; i; i -= i & -i) res += f[i];
  return res;
}

void work(atom _) {
  int v = _.op == 1 ? _.y : -_.y;
  if (_.t == 0) {
    add(A, _.x, v); // ice
    a2[_.x] += v;
  } else {
    add(B, _.x, v); // fire
    b2[_.x] += v;
    totB += v;
  }
}
int maxJ = 0;
void getAns() {
  int res = 0, r1 = -1;
  int cur = 0;
  int curA = 0, curB = 0;
  ROF(j, 20, 0) { // f(A) <= f(B)
    int nex = (1 << j) + cur;
    if (nex >= N) continue;
    int nexA = curA + A[nex], nexB = curB + B[nex];
    if (nexA <= totB - nexB) cur = nex, curA = nexA, curB = nexB;
  }
  if (min(curA, totB - curB) >= res) res = min(curA, totB - curB), r1 = cur;
  if (cur + 1 < N) {
    ++cur, curA += a2[cur], curB += b2[cur];
    if (min(curA, totB - curB) >= res) res = min(curA, totB - curB), r1 = cur;
  }
  cur = 0, curA = 0, curB = 0;
  ROF(j, 20, 0) { // f(A) <= f(B)
    int nex = (1 << j) + cur;
    if (nex >= N) continue;
    int nexA = curA + A[nex], nexB = curB + B[nex];
    if (nexA <= res && nexA <= totB - nexB) cur = nex, curA = nexA, curB = nexB;
  }
  if (curA == res) r1 = max(r1, cur);
  cur = 0, curA = 0, curB = 0;
  ROF(j, 20, 0) { // f(A) <= f(B)
    int nex = (1 << j) + cur;
    if (nex >= N) continue;
    int nexB = curB + B[nex];
    if (totB - nexB >= res) cur = nex, curB = nexB;
  }
  if (totB - curB == res) r1 = max(r1, cur);

  if (res == 0)
    puts("Peace");
  else {
    assert(r1 < ld);
    printf("%d %d\n", d[r1 + 1] - 1, res * 2);
  }
}
int n;
void go() {
  FOR(i, 1, n) {
    work(a[i]);
    getAns();
  }
}
int main() {
  scanf("%d", &n);
  FOR(i, 1, n) {
    int op, t, x, y, k;
    scanf("%d", &op);
    if (op == 1) {
      scanf("%d%d%d", &t, &x, &y);
      if (t == 1) ++x;
      a[i] = (atom){op, t, x, y};
      d[++ld] = x;
    } else {
      scanf("%d", &k);
      a[i].op = op;
      a[i].t = k;
    }
  }
  sort(d + 1, d + ld + 1);
  ld = unique(d + 1, d + ld + 1) - d - 1;
  FOR(i, 1, n) {
    if (a[i].op == 1) {
      a[i].x = lower_bound(d + 1, d + ld + 1, a[i].x) - d;
    } else {
      a[i] = a[a[i].t];
      a[i].op = 2;
    }
  }
  go();
  return 0;
}
