#include <bits/stdc++.h>
//#define int long long
#define FOR(a, b, c) for (int a = b; a <= (int)(c); ++a)
#define ROF(a, b, c) for (int a = b; a >= (int)(c); --a)
#define pb push_back
using namespace std;
const int N = 1005, P = 998244353;
int n, k, c, c0, c1, d0, d1, tots;
int b[N], s[N], typ[N], hate[N];

#define poly vector
template <class T> poly<T> operator+(const poly<T> x, const poly<T> y) {
  poly<T> res(x);
  res.resize(max(res.size(), y.size()));
  for (unsigned i = 0; i < y.size(); ++i) { res[i] = res[i] + y[i]; }
  return res;
}
template <class T> poly<T> operator*(const poly<T> x, const poly<T> y) {
  // puts("GGG1");
  poly<T> res(x.size() + y.size());
  for (unsigned i = 0; i < x.size(); ++i) {
    for (unsigned j = 0; j < y.size(); ++j) {
      assert(i + j < res.size());
      // printf("1 i %u j %u\n",i,j);
      auto t = x[i] * y[j];
      // printf("2 i %u j %u\n",i,j);
      res[i + j] = res[i + j] + t;
      // printf("3 i %u j %u\n",i,j);
    }
  }
  // puts("ggg1");
  return res;
}
template <class T> const poly<T> &operator*=(poly<T> &x, const poly<T> y) {
  x = x * y;
  return x;
}

poly<int> operator+(const poly<int> x, const poly<int> y) {
  poly<int> res(x);
  res.resize(max(res.size(), y.size()));
  for (unsigned i = 0; i < y.size(); ++i) { res[i] = (res[i] + y[i]) % P; }
  return res;
}

poly<int> operator*(const poly<int> x, const poly<int> y) {
  poly<int> res(x.size() + y.size());
  for (unsigned i = 0; i < x.size(); ++i) {
    for (unsigned j = 0; j < y.size(); ++j) {
      assert(i + j < res.size());
      res[i + j] = (res[i + j] + x[i] * 1ll * y[j] % P) % P;
    }
  }
  return res;
}

vector<int> B[N];
vector<int> px, py;
poly<poly<int>> prefer;
poly<int> X, Y;

void work(vector<int> &v) {
  bool flag = 0;
  for (auto i : v)
    if (typ[i] == 1) flag = 1;
  // printf("work flag %d\n",flag);
  if (flag == 0) {
    int sum = 0;
    for (auto i : v) sum += s[i], py.pb(s[i]);
    px.pb(sum);
  } else {
    int sum = 0;
    poly<int> As(1, 1), Bs(1, 1);
    for (auto i : v) {
      sum += s[i];
      if (typ[i] == 0)
        py.pb(s[i]);
      else {
        poly<int> pA(s[i] + 1, 0), pB(s[i] + 1, 0);
        if (hate[i] == 0) {
          pA[s[i]] = 1;
          pB[0] = 1, pB[s[i]] = 1;
        } else if (hate[i] == 1) {
          pA[0] = 1;
          pB[0] = 1, pB[s[i]] = 1;
        } else if (hate[i] == 2) {
          pA[0] = 1, pA[s[i]] = 1;
          pB[s[i]] = 1;
        } else {
          pA[0] = 1, pA[s[i]] = 1;
          pB[0] = 1;
        }
        As *= pA;
        Bs *= pB;
      }
    }
    /*cout<<"As: ";
    for(auto x:As)cout<<x<<" ";
    cout<<endl;
    cout<<"Bs: ";
    for(auto x:Bs)cout<<x<<" ";
    cout<<endl;*/
    poly<poly<int>> p(sum + 1);
    p[0] = As;
    p[sum] = Bs;
    // puts("GG1");
    prefer *= p;
    // puts("GG1");
  }
}
void init() {
  prefer.clear();
  prefer.resize(1, poly<int>(1, 1));
  px.clear();
  py.clear();
  FOR(i, 1, n) B[i].clear();
  X.clear();
  X.resize(1, 1);
  Y.clear();
  Y.resize(1, 1);
  tots = 0;
}
void go() {
  init();
  scanf("%d%d%d%d%d%d", &n, &c, &c0, &c1, &d0, &d1);
  FOR(i, 1, n) {
    scanf("%d%d", &b[i], &s[i]);
    typ[i] = 0;
    B[b[i]].pb(i);
    tots += s[i];
  }
  scanf("%d", &k);
  FOR(i, 1, k) {
    int x, p;
    scanf("%d%d", &x, &p);
    typ[x] = 1;
    hate[x] = p;
  }
  FOR(i, 0, c) if (B[i].size()) { work(B[i]); }
  for (auto s : px) {
    // printf("px %d\n",s);
    poly<int> t(s + 1);
    t[0] = t[s] = 1;
    X *= t;
  }
  for (auto s : py) {
    poly<int> t(s + 1);
    t[0] = t[s] = 1;
    Y *= t;
  }
  /*printf("X: ");
  for(auto x:X)printf("%d ",x);
  puts("");
  printf("Y: ");
  for(auto x:Y)printf("%d ",x);
  puts("");*/
  int ans = 0;
  /*for(int i=max(0,tots-c0);i<prefer.size() && i<=c1;++i){
    for(int j=max(0,tots-d0);j<prefer[i].size() && j<=d1;++j){
      ans=(ans+prefer[i][j])%P;
    }
  }*/
  for (unsigned i = 1; i < X.size(); i++) X[i] = (X[i] + X[i - 1]) % P;
  for (unsigned i = 1; i < Y.size(); i++) Y[i] = (Y[i] + Y[i - 1]) % P;
  // printf("prefer:\n");
  for (int i = 0; i < prefer.size(); ++i) {
    // printf("x^%d: ",i);
    for (int j = 0; j < prefer[i].size(); ++j) {
      assert(prefer[i][j] < P);
      // printf("%d ",prefer[i][j]);
      long long coef = prefer[i][j];

      int L = tots - c0 - i, R = c1 - i;
      // printf("(Lx %d Rx %d) ",L,R);
      L = max(L, 0);
      R = min(R, (int)(X.size()) - 1);
      if (L <= R)
        coef = coef * (X[R] - (L == 0 ? 0 : X[L - 1])) % P;
      else
        coef = 0;

      L = tots - d0 - j, R = d1 - j;
      // printf("(Ly %d Ry %d) ",L,R);
      L = max(L, 0);
      R = min(R, (int)(Y.size()) - 1);
      if (L <= R)
        coef = coef * (Y[R] - (L == 0 ? 0 : Y[L - 1])) % P;
      else
        coef = 0;

      ans = (ans + coef) % P;
    }
    // puts("");
  }
  ans = (ans + P) % P;
  printf("%d\n", ans);
}
signed main() {
  int t;
  scanf("%d", &t);
  FOR(i, 1, t) go();
  return 0;
}