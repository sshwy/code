// by Sshwy
//#define DEBUGGER
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

#ifdef DEBUGGER

#define log_prefix()                                                \
  {                                                                 \
    fprintf(stderr, "\033[37mLine %-3d [%dms]:\033[0m  ", __LINE__, \
        (int)clock() / 1000);                                       \
  }
#define ilog(...) \
  { fprintf(stderr, __VA_ARGS__); }
#define llog(...)      \
  {                    \
    log_prefix();      \
    ilog(__VA_ARGS__); \
  }
#define log(...)           \
  {                        \
    log_prefix();          \
    ilog(__VA_ARGS__);     \
    fprintf(stderr, "\n"); \
  }
#define red(...)       \
  {                    \
    log_prefix();      \
    ilog("\033[31m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define green(...)     \
  {                    \
    log_prefix();      \
    ilog("\033[32m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define blue(...)      \
  {                    \
    log_prefix();      \
    ilog("\033[34m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }

#else

#define log_prefix() ;
#define ilog(...) ;
#define llog(...) ;
#define log(...) ;
#define red(...) ;
#define green(...) ;
#define blue(...) ;

#endif

namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 105, M = 5e4 + 5, P = 998244353;
int n, s[N], p[N], m;
int _F[M * 2], _G[M * 2], _t[M * 2];
int *F = _F + M, *G = _G + M, *t = _t + M;
int pw(int a, int m) {
  int res = 1;
  while (m) m & 1 ? res = 1ll * res * a % P : 0, a = 1ll * a * a % P, m >>= 1;
  return res;
}
void add(int &x, long long y) { x = (x + y) % P; }
int main() {
  scanf("%d", &n);
  FOR(i, 1, n) scanf("%d", &s[i]);
  FOR(i, 1, n) scanf("%d", &p[i]), m += p[i];

  F[0] = 1;
  FOR(i, 1, n) {
    long long coef = s[i] == 1 ? -1 : 1;
    FOR(j, -m, m)
    if (F[j]) add(t[j + p[i]], F[j]), add(t[j - p[i]], F[j] * coef);
    FOR(j, -m, m) F[j] = t[j], t[j] = 0;
  }
  G[0] = 1;
  FOR(i, 1, n) {
    FOR(j, -m, m) if (G[j]) add(t[j + p[i]], G[j]), add(t[j - p[i]], G[j]);
    FOR(j, -m, m) G[j] = t[j], t[j] = 0;
  }
  int f1 = F[m], f2 = 0, g1 = G[m], g2 = 0;
  FOR(i, -m, m - 1) add(f2, F[i] * 1ll * m % P * pw(i - m, P - 2));
  FOR(i, -m, m - 1) add(g2, G[i] * 1ll * m % P * pw(i - m, P - 2));
  int ans = (f2 * 1ll * g1 - f1 * 1ll * g2) % P * pw(g1 * 1ll * g1 % P, P - 2) % P;
  add(ans, P);
  printf("%d\n", ans);
  return 0;
}
