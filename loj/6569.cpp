#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int SZ = 1 << 19, P = 998244353;
int pw(int a, int m) {
  if (m < 0) m += P - 1;
  int res = 1;
  while (m) m & 1 ? res = 1ll * res * a % P : 0, a = 1ll * a * a % P, m >>= 1;
  return res;
}
int tr[SZ];
int init(int n) { // n指多项式的长度，非度数
  int len = 1;
  while (len < n) len <<= 1;
  FOR(i, 0, len - 1) tr[i] = tr[i >> 1] >> 1 | ((i & 1) * (len / 2));
  return len;
}
int dft_w[2][19][SZ];
void init_G() {
  FOR(j2, 0, 18) {
    int j = 1 << j2, wn = pw(3, (P - 1) / (j << 1)), *W = dft_w[0][j2];
    W[0] = 1;
    FOR(i, 1, j - 1) W[i] = W[i - 1] * 1ll * wn % P;
    wn = pw(3, -(P - 1) / (j << 1)), W = dft_w[1][j2];
    W[0] = 1;
    FOR(i, 1, j - 1) W[i] = W[i - 1] * 1ll * wn % P;
  }
}
void dft(int *f, int len, int tag) {
  FOR(i, 0, len - 1) if (i < tr[i]) swap(f[i], f[tr[i]]);
  FOR(i, 0, len - 1) f[i] = (f[i] + P) % P;
  for (int j = 1, j2 = 0; j < len; j <<= 1, ++j2)
    for (int i = 0, *W = dft_w[tag == -1][j2] /*pw(3,(P-1)/(j<<1)*tag)*/; i < len;
         i += j << 1)
      for (int k = i, u, v, w = 1; k < i + j; k++, w = W[k - i] /*1ll*w*wn%P*/)
        u = f[k], v = f[k + j] * 1ll * w % P, f[k] = u + v, f[k] -= f[k] >= P ? P : 0,
        f[k + j] = u - v, f[k + j] += f[k + j] < 0 ? P : 0;
  if (tag == -1)
    for (int i = 0, ilen = pw(len, P - 2); i < len; i++) f[i] = 1ll * f[i] * ilen % P;
}
int inv_h[SZ];
void inv(const int *f, int *g, int n) { // f*g=1 mod x^n
  if (n == 1) return g[0] = pw(f[0], P - 2), void();
  inv(f, g, (n + 1) / 2);

  int len = init(2 * n);
  FOR(i, 0, len - 1) inv_h[i] = 0;
  FOR(i, (n + 1) / 2, len - 1) g[i] = 0;

  FOR(i, 0, n - 1) inv_h[i] = f[i];
  dft(inv_h, len, 1), dft(g, len, 1);
  FOR(i, 0, len - 1) inv_h[i] = 1ll * inv_h[i] * g[i] % P * g[i] % P;
  FOR(i, 0, len - 1) g[i] = (g[i] * 2ll - inv_h[i] + P) % P;
  dft(g, len, -1);

  // FOR(i,n,len-1)g[i]=0;
}
int ln_h[SZ];
void ln(const int *f, int *g, int n) { // ln f=g mod x^n ,f[0]=1
  inv(f, g, n);

  ln_h[n - 1] = 0;
  FOR(i, 1, n - 1) ln_h[i - 1] = f[i] * 1ll * i % P;

  int len = init(n * 2);
  FOR(i, n, len - 1) ln_h[i] = g[i] = 0;
  dft(ln_h, len, 1), dft(g, len, 1);
  FOR(i, 0, len - 1) g[i] = 1ll * g[i] * ln_h[i] % P;
  dft(g, len, -1);

  ROF(i, n - 1, 1) g[i] = g[i - 1] * 1ll * pw(i, P - 2) % P;
  g[0] = 0; // f[0]=1
}
int exp_t[SZ], exp_h[SZ];
void exp(const int *f, int *g, int n) { // f[0]==0
  if (n == 1) return g[0] = 1, void();
  exp(f, g, (n + 1) / 2);
  FOR(i, (n + 1) / 2, n - 1) g[i] = 0;

  ln(g, exp_h, n);
  FOR(i, 0, n - 1) exp_t[i] = (f[i] - exp_h[i] + P) % P;
  exp_t[0] = (exp_t[0] + 1) % P;

  int len = init(n * 2);
  FOR(i, n, len) g[i] = exp_t[i] = 0;
  dft(g, len, 1), dft(exp_t, len, 1);
  FOR(i, 0, len - 1) g[i] = g[i] * 1ll * exp_t[i] % P;
  dft(g, len, -1);
}
// make sure to init_G before dft!

int F0[SZ], A[SZ], B[SZ], C[SZ], D[SZ], E[SZ], G[SZ];
void solve(int *f, int n) { // get f mod n
  if (n == 1) return f[0] = 0, void();
  solve(f, (n + 1) / 2);
  // printf("f0:"); FOR(i,0,(n+1)/2-1)printf("%d%c",f[i]," \n"[i==(n+1)/2-1]);

  memcpy(F0, f, sizeof(int) * n);
  // B=2-2F0
  FOR(i, 0, n - 1) B[i] = (P - 2ll * F0[i] % P) % P;
  B[0] = (B[0] + 2) % P;
  // printf("B: ");FOR(i,0,n-1)printf("%d%c",B[i]," \n"[i==n-1]);
  FOR(i, 0, n - 1) B[i] = (B[i] + P) % P;
  // C=1/B
  inv(B, C, n);
  int len = init(n * 2);
  dft(F0, len, 1);
  // A=2*F0-F0^2
  FOR(i, 0, len - 1) A[i] = (2ll * F0[i] - F0[i] * 1ll * F0[i]) % P;
  FOR(i, 0, len - 1) A[i] = (A[i] + P) % P;
  dft(A, len, -1), dft(F0, len, -1);

  len = init(n * 3);
  FOR(i, n, len - 1) A[i] = 0;
  FOR(i, n, len - 1) C[i] = 0;
  // D=A*C=A/B
  // E=2*A/B^2=2*A*C*C +1
  dft(A, len, 1), dft(C, len, 1);
  FOR(i, 0, len - 1) D[i] = 1ll * A[i] * C[i] % P;
  FOR(i, 0, len - 1) E[i] = 1ll * A[i] * C[i] % P * C[i] % P * 2ll % P;
  dft(D, len, -1), dft(E, len, -1);
  E[0] = (E[0] + 1) % P;

  // G=F0 - x exp D
  exp(D, G, n);
  ROF(i, n - 1, 1) G[i] = G[i - 1];
  G[0] = 0;
  FOR(i, 0, n - 1) G[i] = (F0[i] - G[i] + P) % P;

  // D =1 - E*F0
  len = init(n * 2);
  memcpy(B, F0, sizeof(int) * n);
  FOR(i, n, len - 1) E[i] = 0;
  FOR(i, n, len - 1) B[i] = 0;
  dft(E, len, 1), dft(B, len, 1);
  FOR(i, 0, len - 1) D[i] = 1ll * E[i] * B[i] % P;
  dft(D, len, -1);
  FOR(i, 0, n - 1) D[i] = (P - D[i]) % P;
  D[0] = (D[0] + 1) % P;

  // E = 1/D
  inv(D, E, n);
  len = init(n * 2);
  // G=F0-G*E
  FOR(i, n, len - 1) G[i] = 0;
  FOR(i, n, len - 1) E[i] = 0;
  dft(G, len, 1), dft(E, len, 1);
  FOR(i, 0, len - 1) G[i] = 1ll * G[i] * E[i] % P;
  dft(G, len, -1);
  FOR(i, 0, n - 1) G[i] = (F0[i] - G[i] + P) % P;
  memcpy(f, G, sizeof(int) * n);
  // printf("f :"); FOR(i,0,n-1)printf("%d%c",f[i]," \n"[i==n-1]);
}

int f[SZ], fac[SZ];
int main() {
  init_G();
  int n = 131072;
  solve(f, n);
  fac[0] = 1;
  FOR(i, 1, n - 1) fac[i] = 1ll * fac[i - 1] * i % P;
  int q;
  scanf("%d", &q);
  FOR(i, 1, q) {
    int x;
    scanf("%d", &x);
    printf("%lld\n", f[x] * 1ll * fac[x - 1] % P);
  }
  return 0;
}
// BUG#1:EGF忘乘阶乘！
