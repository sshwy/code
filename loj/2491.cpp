#include <bits/stdc++.h>
#define FOR(a, b, c) for (int a = b; a <= int(c); ++a)
#define ROF(a, b, c) for (int a = b; a >= int(c); --a)
#define pb push_back
using namespace std;
const int N = 3e5 + 5, P = 998244353;
vector<int> g[N];
int n, m, fa[N][20], f[N][52], dep[N];

void dfs(int u, int p) {
  fa[u][0] = p;
  if (p) dep[u] = dep[p] + 1;
  FOR(j, 1, 19) {
    fa[u][j] = fa[fa[u][j - 1]][j - 1];
    if (fa[u][j] == 0) break;
  }
  for (int v : g[u])
    if (v != p) dfs(v, u);
}
int lca(int u, int v) {
  if (dep[u] < dep[v]) swap(u, v);
  ROF(j, 19, 0) if (dep[u] - (1 << j) >= dep[v]) u = fa[u][j];
  if (u == v) return u;
  ROF(j, 19, 0) if (fa[u][j] != fa[v][j]) u = fa[u][j], v = fa[v][j];
  return fa[u][0];
}
int main() {
  scanf("%d", &n);
  FOR(i, 1, n - 1) {
    int u, v;
    scanf("%d%d", &u, &v);
    g[u].pb(v);
    g[v].pb(u);
  }
  dfs(1, 0);
  FOR(i, 1, n) {
    long long cur = 1;
    FOR(j, 1, 50) {
      cur = cur * i % P;
      f[i][j] = (f[i - 1][j] + cur) % P;
    }
  }
  scanf("%d", &m);
  FOR(i, 1, m) {
    int u, v, k;
    scanf("%d%d%d", &u, &v, &k);
    int z = lca(u, v);
    // printf("u %d v %d z %d\n",u,v,z);
    int ans =
        (0ll + f[dep[u]][k] + f[dep[v]][k] - f[dep[z]][k] - f[dep[z] - 1][k]) % P;
    ans = (ans + P) % P;
    printf("%d\n", ans);
  }
  return 0;
}