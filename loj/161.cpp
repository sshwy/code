#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
namespace IO {
  char nc() { // 据传为 Manchery 大神所作
    static char bf[100000], *p1 = bf, *p2 = bf;
    return p1 == p2 && (p2 = (p1 = bf) + fread(bf, 1, 100000, stdin), p1 == p2)
               ? EOF
               : *p1++;
  }
  int rd() {
    int res = 0;
    char c = getchar();
    while (!isdigit(c)) c = getchar();
    while (isdigit(c)) res = res * 10 + c - '0', c = getchar();
    return res;
  }
} // namespace IO
/******************heading******************/
const int N = 5e6 + 5, P = 1e9 + 7;
int n, a[N];
int fac[N], inv[N], b[N];

int pw(int a, int m) {
  int res = 1;
  while (m) m & 1 ? res = 1ll * res * a % P : 0, a = 1ll * a * a % P, m >>= 1;
  return res;
}

int main() {
  n = IO::rd();                 // scanf("%d",&n);
  FOR(i, 1, n) a[i] = IO::rd(); // scanf("%d",&a[i]);
  fac[0] = 1;
  FOR(i, 1, n) fac[i] = 1ll * fac[i - 1] * a[i] % P;
  inv[n] = pw(fac[n], P - 2);
  ROF(i, n, 1) inv[i - 1] = 1ll * inv[i] * a[i] % P;
  // FOR(i,1,n)assert(1ll*fac[i]*inv[i]%P==1);
  b[0] = 1;
  FOR(i, 1, n) b[i] = 1ll * b[i - 1] * 998244353 % P;
  int ans = 0;
  FOR(i, 1, n) ans = (ans + 1ll * inv[i] * fac[i - 1] % P * b[n - i]) % P;
  // FOR(i,0,n)printf("%d ",fac[i]);  puts("");
  // FOR(i,0,n)printf("%d ",inv[i]);  puts("");
  printf("%d\n", ans);
  return 0;
}
