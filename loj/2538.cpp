// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

const int N = 5005, P = 998244353;

int pw(int a, int m) {
  int res = 1;
  while (m) m & 1 ? res = 1ll * res * a % P : 0, a = 1ll * a * a % P, m >>= 1;
  return res;
}

int n, m, k;
int a[N], b[N];
int f[N][N], g[N][N], fac[N], fnv[N];

int binom(int x, int y) {
  if (x < y) return 0;
  return 1ll * fac[x] * fnv[y] % P * fnv[x - y] % P;
}

int calc(int x) {
  int res = 0;
  FOR(i, 1, n) { res = (res + 1ll * binom(i - 1, x - 1) * b[i]) % P; }
  return res;
}

int calc2(int x) {
  if (k == 1) return binom(n, x);
  int res = 0;
  FOR(i, 1, n) {
    res = (res + 1ll * f[i - 1][k - 2] * a[i] % P * binom(n - i, x - k + 1)) % P;
  }
  return res;
}

void go() {
  scanf("%d%d%d", &n, &m, &k);
  FOR(i, 1, n) scanf("%d", a + i);
  FOR(i, 1, n) scanf("%d", b + i);

  sort(a + 1, a + n + 1);
  sort(b + 1, b + n + 1);

  reverse(a + 1, a + n + 1);

  FOR(i, 0, n) FOR(j, 0, n) f[i][j] = g[i][j] = 0;

  f[0][0] = 1;
  FOR(i, 1, n) {
    f[i][0] = 1;
    FOR(j, 1, i) { f[i][j] = (f[i - 1][j] + 1ll * f[i - 1][j - 1] * a[i]) % P; }
  }

  FOR(i, m - k + 1, n) {
    FOR(j, m - k + 1, i) {
      g[i][j] =
          (1ll * binom(i - 1, j - 1) * b[i] + g[i - 1][j] + g[i - 1][j - 1]) % P;
    }
  }

  int ans = 0;
  FOR(i, 0, k - 1) {
    if (m - i > n || i > n) continue;
    ans = (ans + 1ll * f[n][i] * g[n][m - i]) % P;
  }
  FOR(i, k, m - 1) {
    if (m - i > n || i > n) continue;
    ans = (ans + 1ll * calc2(i) * calc(m - i)) % P;
  }
  printf("%d\n", ans);
}

int main() {
  fac[0] = 1;
  FOR(i, 1, N - 1) fac[i] = 1ll * fac[i - 1] * i % P;
  fnv[N - 1] = pw(fac[N - 1], P - 2);
  ROF(i, N - 1, 1) fnv[i - 1] = 1ll * fnv[i] * i % P;

  int t;
  scanf("%d", &t);
  FOR(i, 1, t) go();
  return 0;
}
