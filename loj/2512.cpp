#include <bits/stdc++.h>
#define FOR(a, b, c) for (int a = b; a <= int(c); ++a)
#define ROF(a, b, c) for (int a = b; a >= int(c); --a)
#define pb push_back
using namespace std;
const int N = 2e5 + 5, SZ = N << 2, P = 1e9 + 7, I6 = (P + 1) / 6;
int n, m;

long long s1(int L, int R) {
  if (L > R) return 0;
  return R - L + 1;
}
long long s2(int L, int R) {
  if (L > R) return 0;
  return (R - L + 1ll) * (L + R) / 2 % P;
}
long long s3(int L, int R) {
  if (L > R) return 0;
  return (R * (R + 1ll) % P * (R + 2) % P - (L - 1ll) * L % P * (L + 1) % P + P) *
         I6 % P;
}
struct vt {
  int a, b, c;
};
vt operator+(vt x, vt y) {
  return {(x.a + y.a) % P, (x.b + y.b) % P, (x.c + y.c) % P};
}
void add(int &x, long long y) { x = (x + y) % P; }
struct seg {
  vt v[SZ];
  int tag[SZ];
  void add(int &x, long long y) { x = (x + y) % P; }
  void nodeadd(int u, int l, int r, int val) {
    add(v[u].a, s1(l, r) * val);
    add(v[u].b, s2(l, r) * val);
    add(v[u].c, s3(l, r) * val);
    add(tag[u], val);
  }
  void pushdown(int u, int l, int r) {
    if (tag[u]) {
      int mid = (l + r) >> 1;
      nodeadd(u << 1, l, mid, tag[u]);
      nodeadd(u << 1 | 1, mid + 1, r, tag[u]);
      tag[u] = 0;
    }
  }
  void add(int L, int R, int val, int u = 1, int l = 1, int r = n) {
    if (R < l || r < L || L > R) return;
    if (L <= l && r <= R) return nodeadd(u, l, r, val), void();
    int mid = (l + r) >> 1;
    pushdown(u, l, r);
    add(L, R, val, u << 1, l, mid);
    add(L, R, val, u << 1 | 1, mid + 1, r);
    v[u] = v[u << 1] + v[u << 1 | 1];
  }
  vt qry(int L, int R, int u = 1, int l = 1, int r = n) {
    if (R < l || r < L || L > R) return {0, 0};
    if (L <= l && r <= R) return v[u];
    int mid = (l + r) >> 1;
    pushdown(u, l, r);
    return qry(L, R, u << 1, l, mid) + qry(L, R, u << 1 | 1, mid + 1, r);
  }
} s[2];
int sum;

int calc(int x) {
  long long tot = sum * s2(1, x) % P;
  auto y = s[0].qry(n - x + 2, n);
  tot -= (y.c - (y.b * 1ll * (n - x + 1) % P - (y.a * s2(1, n - x)))) % P;
  y = s[1].qry(n - x + 2, n);
  tot -= (y.c - (y.b * 1ll * (n - x + 1) % P - (y.a * s2(1, n - x)))) % P;
  tot = (tot % P + P) % P;
  return tot;
}
int main() {
  scanf("%d%d", &n, &m);
  FOR(i, 1, n) {
    int x;
    scanf("%d", &x);
    s[0].add(i, i, x);
    s[1].add(n - i + 1, n - i + 1, x);
    add(sum, x);
  }
  FOR(i, 1, m) {
    int op, a, b, c;
    scanf("%d%d%d", &op, &a, &b);
    if (a > b) swap(a, b);
    if (op == 1) {
      scanf("%d", &c);
      s[0].add(a, b, c);
      s[1].add(n - b + 1, n - a + 1, c);
      add(sum, (b - a + 1ll) * c);
    } else {
      int ans = (calc(b) - calc(a - 1) + P) % P;
      printf("%d\n", ans);
    }
  }
  return 0;
}