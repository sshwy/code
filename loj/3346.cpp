// by Yao
#include "swap.h"
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

const int N = 1e5 + 5;

struct qxx {
  int nex, t, v;
} e[N * 2];
int h[N], le = 1;
void ae(int u, int v, int w) { e[++le] = {h[u], v, w}, h[u] = le; }

int fa[N], a[N], anc[N][20], b[N][20], c[N][20], dep[N];
bool vis[N];

int get(int u) { return fa[u] == u ? u : fa[u] = get(fa[u]); }
void dfs(int u, int p, int wp) {
  b[u][0] = wp, c[u][0] = a[u], anc[u][0] = p, dep[u] = dep[p] + 1;
  FOR(j, 1, 19) {
    anc[u][j] = anc[anc[u][j - 1]][j - 1];
    b[u][j] = max(b[u][j - 1], b[anc[u][j - 1]][j - 1]);
    c[u][j] = min(c[u][j - 1], c[anc[u][j - 1]][j - 1]);
    if (anc[u][j] == 0) break;
  }
  for (int i = h[u]; i; i = e[i].nex) {
    int v = e[i].t, w = e[i].v;
    if (v != p) dfs(v, u, w);
  }
}

void init(int n, int m, std::vector<int> U, std::vector<int> V, std::vector<int> W) {
  FOR(i, 1, n) a[i] = 1e9 + 1;
  memset(c, 0x3f, sizeof(c));
  vector<pair<int, int>> edges;
  for (unsigned i = 0; i < W.size(); i++) { edges.push_back({W[i], i}); }
  FOR(i, 1, n) fa[i] = i;
  sort(edges.begin(), edges.end());
  for (auto e : edges) {
    int id = e.second;
    int u = U[id] + 1, v = V[id] + 1, w = W[id];
    if (get(u) == get(v)) {
      a[u] = min(a[u], w);
      a[v] = min(a[v], w);
    } else {
      fa[get(u)] = get(v);
      ae(u, v, w);
      ae(v, u, w);
    }
  }
  FOR(i, 1, n) if (!vis[i]) dfs(i, 0, 0);
}

int getMinimumFuelCapacity(int x, int y) {
  ++x, ++y;
  if (get(x) != get(y)) return -1;
  if (dep[x] < dep[y]) swap(x, y);
  int max_path = 0, min_extra = min(a[x], a[y]);
  ROF(j, 19, 0) {
    if (dep[x] - (1 << j) >= dep[y]) {
      max_path = max(max_path, b[x][j]);
      min_extra = min(min_extra, c[x][j]);
      x = anc[x][j];
    }
  }
  if (x == y) {
    min_extra = min(min_extra, a[x]);
    if (anc[x][0]) { min_extra = min(min_extra, b[x][0]); }
    int ans = max(max_path, min_extra);
    return ans > 1e9 ? -1 : ans;
  }
  ROF(j, 19, 0) {
    if (anc[x][j] == anc[y][j]) continue;
    max_path = max(max_path, b[x][j]);
    min_extra = min(min_extra, c[x][j]);
    max_path = max(max_path, b[y][j]);
    min_extra = min(min_extra, c[y][j]);
    x = anc[x][j];
    y = anc[y][j];
  }
  max_path = max(max_path, b[x][0]);
  min_extra = min(min_extra, a[x]);
  max_path = max(max_path, b[y][0]);
  min_extra = min(min_extra, a[y]);
  int z = anc[x][0];
  min_extra = min(min_extra, a[z]);
  if (anc[z][0]) { min_extra = min(min_extra, b[z][0]); }
  int ans = max(max_path, min_extra);
  return ans > 1e9 ? -1 : ans;
}
