#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
namespace IO {
  char bf[1 << 25], *p1 = bf, *p2 = bf;
  char nc() {
    return p1 == p2 && (p2 = (p1 = bf) + fread(bf, 1, 1 << 25, stdin), p1 == p2)
               ? EOF
               : *p1++;
  }
  int rd() {
    int res = 0;
    char c = getchar();
    while (!isdigit(c)) c = getchar();
    while (isdigit(c)) res = res * 10 + c - '0', c = getchar();
    return res;
  }
} // namespace IO
using namespace IO;
const int N = 21, NN = 1 << 20, P = 1e9 + 9;

int n, nn;
int f[NN], g[NN];
int F[N][NN], G[N][NN], H[N][NN];

void fwt(int *f, int len) {
  for (int j = 1; j < len; j <<= 1)
    for (int i = 0; i < len; i += j << 1)
      for (int k = i; k < i + j; k++)
        f[k + j] = f[k + j] + f[k] >= P ? f[k + j] + f[k] - P : f[k + j] + f[k];
}
void ifwt(int *f, int len) {
  for (int j = 1; j < len; j <<= 1)
    for (int i = 0; i < len; i += j << 1)
      for (int k = i; k < i + j; k++)
        f[k + j] = f[k + j] - f[k] < 0 ? f[k + j] - f[k] + P : f[k + j] - f[k];
}
int c[NN];
int main() {
  n = rd();
  // scanf("%d",&n);
  nn = 1 << n;
  FOR(i, 0, nn - 1) f[i] = rd(); // scanf("%d",&f[i]);
  FOR(i, 0, nn - 1) g[i] = rd(); // scanf("%d",&g[i]);
  FOR(i, 0, nn - 1) c[i] = c[i >> 1] + (i & 1);
  FOR(i, 0, nn - 1) {
    int &x = F[c[i]][i];
    x += f[i], x -= x >= P ? P : 0;
  }
  FOR(i, 0, nn - 1) {
    int &x = G[c[i]][i];
    x += g[i], x -= x >= P ? P : 0;
  }
  FOR(i, 0, n) fwt(F[i], nn);
  FOR(i, 0, n) fwt(G[i], nn);
  FOR(i, 0, n) {
    FOR(k, 0, i) {
      FOR(j, 0, nn - 1) { H[i][j] = (H[i][j] + 1ll * F[k][j] * G[i - k][j]) % P; }
    }
  }
  FOR(i, 0, n) ifwt(H[i], nn);
  FOR(i, 0, nn - 1) {
    int x = H[c[i]][i];
    x += x < 0 ? P : 0;
    printf("%d ", x);
  }
  return 0;
}
