#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <climits>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <unordered_map>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
template <class Tp> void read(Tp &x) {
  static char ch;
  static bool neg;
  for (ch = neg = 0; !isdigit(ch); neg |= (ch == '-'), ch = getchar())
    ;
  for (x = 0; isdigit(ch); (x *= 10) += (ch ^ 48), ch = getchar())
    ;
  neg && (x = -x);
}
typedef __int128 LLL;
typedef long long LL;
LL I2, I6;
const int P = 998244353, SZ = 4e6 + 5;
int LIM = 4e6;

bool bp[SZ];
int pn[SZ], lp;
LL phi[SZ];   // prefix sum
LL phi_d[SZ]; // prefix sum
void sieve() {
  bp[0] = bp[1] = 1;
  phi[1] = 1, phi_d[1] = 1;
  FOR(i, 2, LIM) {
    if (!bp[i]) pn[++lp] = i, phi[i] = i - 1, phi_d[i] = (i - 1ll) * i % P;
    FOR(j, 1, lp) {
      int pj = pn[j];
      if (i * pj > LIM) break;
      bp[i * pj] = 1;
      if (i % pj == 0) {
        phi[i * pj] = phi[i] * pj % P;
        phi_d[i * pj] = phi_d[i] * pj % P * pj % P;
        break;
      } else {
        phi[i * pj] = phi[i] * phi[pj] % P;
        phi_d[i * pj] = phi_d[i] * phi_d[pj] % P;
      }
    }
  }
  FOR(i, 2, LIM) phi[i] = (phi[i] + phi[i - 1]) % P;
  FOR(i, 2, LIM) phi_d[i] = (phi_d[i] + phi_d[i - 1]) % P;
}

LL Sid(LL x) {
  x %= P;
  return x * (x + 1) % P * I2 % P;
}
LL Sid2(LL x) {
  x %= P;
  return x * (x + 1) % P * (x * 2 + 1) % P * I6 % P;
}
unordered_map<LL, LL> m_sphi, m_sphi_d;
LL Sphi(LL x) {
  if (x <= LIM) return phi[x];
  if (m_sphi.count(x)) return m_sphi[x];
  LL l = 2, r;
  LL res = Sid(x);
  while (l <= x) {
    r = x / (x / l);
    res = (res - Sphi(x / l) * (r - l + 1)) % P;
    l = r + 1;
  }
  if (res < 0) res += P;
  return m_sphi[x] = res;
}
LL Sphi_d(LL x) {
  if (x <= LIM) return phi_d[x];
  if (m_sphi_d.count(x)) return m_sphi_d[x];
  LL l = 2, r;
  LL res = Sid2(x);
  while (l <= x) {
    r = x / (x / l);
    res = (res - Sphi_d(x / l) * (Sid(r) - Sid(l - 1))) % P;
    l = r + 1;
  }
  if (res < 0) res += P;
  return m_sphi_d[x] = res;
}

LL solve1(LL x) {
  LL res = 0;
  LL l = 1, r;
  while (l <= x) {
    r = x / (x / l);
    res = (res + Sid(x / l) * (Sphi(r) - Sphi(l - 1))) % P;
    l = r + 1;
  }
  l = 1;
  while (l <= x) {
    r = x / (x / l);
    res = (res + Sid2(x / l) * (Sphi_d(r) - Sphi_d(l - 1))) % P;
    l = r + 1;
  }
  res = res * 3 % P;
  return res;
}
LL solve2(LL x, LLL n) {
  LL res = 0;
  for (LL T = 1; T * T <= x; T++) {
    if (x % T) continue;
    res = (res + ((n / T) % P * (Sphi(T) - Sphi(T - 1))) % P) % P;
    if (T * T != x)
      res = (res + ((n / (x / T)) % P * (Sphi(x / T) - Sphi(x / T - 1))) % P) % P;
  }
  res = (res + x) % P;
  return res;
}

LLL n, n3_LLL, delta;
LL n3;

LL pw(LL a, LL m) {
  LL res = 1;
  while (m) m & 1 ? res = res * a % P : 0, a = a * a % P, m >>= 1;
  return res;
}
int main() {
  I2 = pw(2, P - 2);
  I6 = pw(6, P - 2);
  read(n);
  sieve();

  n3_LLL = (LLL)pow((long double)n, 1.0 / 3);
  while (n3_LLL * n3_LLL * n3_LLL <= n) ++n3_LLL;
  while (n3_LLL * n3_LLL * n3_LLL > n) --n3_LLL;
  assert(n3_LLL <= LLONG_MAX);
  n3 = n3_LLL;

  delta = n - n3_LLL * n3_LLL * n3_LLL;
  LL x1 = solve1(n3 - 1);
  LL x2 = solve2(n3, delta);
  LL x3 = Sid(n3 - 1);
  LL ans = (x1 + x2 + x3 + P + P) % P;
  printf("%lld\n", ans);
  return 0;
}
