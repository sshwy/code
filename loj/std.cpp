#include <bits/stdc++.h>
#define ll long long
#define rt register int
#define r in
#include <sys/mman.h>
using namespace std;
struct ano {
  char *s;
  ano()
      : s((char *)mmap(0, 1 << 24, 1, 2, 0, 0)) {}
  operator int() {
    int x = 0;
    while (*s < 48) ++s;
    while (*s > 32) x = x * 10 + *s++ - 48;
    return x;
  }
} in;
char obuf[1 << 22], *ooh = obuf;
inline void write(int x) {
  static int buf[30], cnt;
  if (x == 0)
    *ooh++ = '0';
  else {
    for (cnt = 0; x; x /= 10) buf[++cnt] = x % 10 + 48;
    while (cnt) *ooh++ = buf[cnt--];
  }
}
inline void flush() { fwrite(obuf, 1, ooh - obuf, stdout); }
int n, m, x, y, z, k, cnt, lg;
int L[300010], hzmin[300010], to[300010][20];
ll qz[300010];
ll calc(int x, int y) {
  if (!x) return 0;
  int bs = 1;
  y = L[y];
  if (y > x) {
    for (rt i = lg; i >= 0; i--)
      if (to[y][i] > x) y = to[y][i], bs += (1 << i);
    bs++;
    y = to[y][0];
  }
  return bs * x + qz[y];
}
int Gcd(int x, int y) { return !y ? x : Gcd(y, x % y); }
int main() {
  n = r;
  for (rt i = 2; i <= n; i++) L[i] = r;
  hzmin[n + 1] = L[n];
  for (rt i = n; i >= 2; i--) hzmin[i] = min(hzmin[i + 1], L[i]);
  hzmin[1] = hzmin[2] = 1;
  for (rt i = 1; i <= n; i++) to[i][0] = hzmin[i];
  for (rt i = 1; i <= n; i++)
    for (rt j = 1; to[i][j - 1] > 1 && j <= 18; j++)
      lg = j, to[i][j] = to[to[i][j - 1]][j - 1];
  qz[1] = 0;
  for (rt i = 2; i <= n; i++) qz[i] = i - 1 + qz[hzmin[i]];
  for (rt T = r; T; T--) {
    int lef = r, rig = r, x = r;
    ll ans = calc(rig, x) - calc(lef - 1, x);
    int len = rig - lef + 1;
    const int gcd = Gcd(len, ans % len);
    write(ans / gcd);
    *ooh++ = '/';
    write(len / gcd);
    *ooh++ = '\n';
  }
  return flush(), 0;
}
