#include <bits/stdc++.h>
#define FOR(a, b, c) for (int a = b; a <= int(c); ++a)
#define ROF(a, b, c) for (int a = b; a >= int(c); --a)
#define pb push_back
using namespace std;
const int N = 5e5 + 5, SZ = N * 40;
typedef unsigned int vt;
int n, k, tot, sz[SZ], b[N], ch[SZ][2], rt[N];
vt a[N];

int cp(int u) {
  ++tot, sz[tot] = sz[u], ch[tot][0] = ch[u][0], ch[tot][1] = ch[u][1];
  return tot;
}
int insert(int u, vt x, int d = 31) {
  if (d == -1) {
    int u2 = cp(u);
    sz[u2]++;
    return u2;
  }
  int u2 = cp(u);
  ch[u2][x >> d & 1] = insert(ch[u2][x >> d & 1], x, d - 1);
  sz[u2]++;
  return u2;
}
vt xor_kth(int u, vt x, int rank, int d = 31) {
  if (d == -1) return 0;
  assert(sz[u]);
  vt xd = x >> d & 1;
  if (rank <= sz[ch[u][xd]]) {
    return xor_kth(ch[u][xd], x, rank, d - 1) ^ (xd << d);
  } else {
    return xor_kth(ch[u][xd ^ 1], x, rank - sz[ch[u][xd]], d - 1) ^ ((xd ^ 1) << d);
  }
}
priority_queue<pair<vt, int>> q;
int main() {
  scanf("%d%d", &n, &k);
  rt[0] = insert(rt[0], 0);
  FOR(i, 1, n) {
    scanf("%u", &a[i]);
    a[i] ^= a[i - 1];
    rt[i] = insert(rt[i - 1], a[i]);
  }
  FOR(i, 1, n) {
    vt x = a[i] ^ xor_kth(rt[i - 1], a[i], sz[rt[i - 1]]);
    q.push({x, i});
    // printf("push %u %d\n",x,i);
    b[i] = sz[rt[i - 1]];
  }
  unsigned long long ans = 0;
  FOR(_, 1, k) {
    assert(q.size());
    auto u = q.top();
    q.pop();
    // printf("get %u %d\n",u.first, u.second);
    ans += u.first;
    int id = u.second;
    --b[id];
    if (b[id]) {
      vt x = a[id] ^ xor_kth(rt[id - 1], a[id], b[id]);
      q.push({x, id});
      // printf("push %u %d\n",x,id);
    }
  }
  printf("%llu\n", ans);
  return 0;
}

/*
w(l,r)为区间异或和
求权值最大的k个区间的权值和
即权值最大的k个pair的权值和
考虑二分第k大的权值和，那么要先判定个数再求和。
二分后，考虑枚举右端点。则变成x异或一个集合里的数大于等于V的个数
Trie
可以在Trie上log查询
则二分的复杂度是nlog^2的
*/