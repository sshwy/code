#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 18, P = 998244353;
int pw(int a, int m) {
  int res = 1;
  while (m) m & 1 ? res = 1ll * res * a % P : 0, a = 1ll * a * a % P, m >>= 1;
  return res;
}

vector<int> g[N];
int n, q, r;
int nn;
int a[N], b[N], d[N], ans[1 << N];

void dfs(int u, int p, int s) {
  if (s >> u & 1) {
    a[u] = 0, b[u] = 0;
    if (p == -1) ans[s] = 0;
    return;
  }
  int A = 0, B = 0;
  for (int v : g[u])
    if (v != p) {
      dfs(v, u, s);
      A = (A + a[v]) % P, B = (B + b[v]) % P;
    }
  int x = pw((d[u] - A + P) % P, P - 2);
  a[u] = x;
  b[u] = 1ll * (B + d[u]) % P * x % P;
  if (p == -1) ans[s] = b[u];
}
int main() {
  scanf("%d%d%d", &n, &q, &r);
  --r;
  FOR(i, 1, n - 1) {
    int x, y;
    scanf("%d%d", &x, &y);
    --x, --y;
    g[x].pb(y), g[y].pb(x);
    d[x]++, d[y]++;
  }
  nn = 1 << n;
  FOR(i, 0, nn - 1) dfs(r, -1, i);
  FOR(i, 0, n - 1)
  FOR(j, 0, nn - 1) if (j >> i & 1) ans[j] = (P + ans[j] - ans[j ^ (1 << i)]) % P;
  FOR(j, 0, nn - 1) {
    if (!(__builtin_popcount(j) & 1)) ans[j] = (P - ans[j]) % P;
  }
  FOR(i, 1, q) {
    int k, s = 0, x;
    scanf("%d", &k);
    FOR(i, 1, k) scanf("%d", &x), --x, s |= 1 << x;
    printf("%d\n", ans[s]);
  }
  return 0;
}
