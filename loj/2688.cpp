// by Sshwy
//#define DEBUGGER
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

#ifdef DEBUGGER

#define log_prefix()                                                \
  {                                                                 \
    fprintf(stderr, "\033[37mLine %-3d [%dms]:\033[0m  ", __LINE__, \
        (int)clock() / 1000);                                       \
  }
#define ilog(...) \
  { fprintf(stderr, __VA_ARGS__); }
#define llog(...)      \
  {                    \
    log_prefix();      \
    ilog(__VA_ARGS__); \
  }
#define log(...)           \
  {                        \
    log_prefix();          \
    ilog(__VA_ARGS__);     \
    fprintf(stderr, "\n"); \
  }
#define red(...)       \
  {                    \
    log_prefix();      \
    ilog("\033[31m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define green(...)     \
  {                    \
    log_prefix();      \
    ilog("\033[32m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define blue(...)      \
  {                    \
    log_prefix();      \
    ilog("\033[34m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }

#else

#define log_prefix() ;
#define ilog(...) ;
#define llog(...) ;
#define log(...) ;
#define red(...) ;
#define green(...) ;
#define blue(...) ;

#endif

namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
#include <functional>
const int N = 53, M = 4005;

int n, m, ld;
struct atom {
  int a, b, c;
};
vector<atom> v;
vector<int> D;

long long f[N][N][M], cost_cnt[M][N];
atom fx[N][N][M]; //

int main() {
  scanf("%d%d", &n, &m);
  FOR(i, 1, m) {
    int a, b, c;
    scanf("%d%d%d", &a, &b, &c);
    v.pb({a, b, c});
    D.pb(c);
  }
  sort(D.begin(), D.end());
  D.erase(unique(D.begin(), D.end()), D.end());
  ld = D.size();
  for (auto &x : v) x.c = lower_bound(D.begin(), D.end(), x.c) - D.begin() + 1;
  sort(v.begin(), v.end(), [](atom x, atom y) { return x.c < y.c; });

  FOR(len, 0, n - 1) {
    FOR(i, 1, n - len) {
      int j = i + len;
      vector<atom> s;
      for (auto x : v)
        if (i <= x.a && x.b <= j) s.pb(x);
      FOR(x, i, j) cost_cnt[ld + 1][x] = 0;
      ROF(k, ld, 1) {
        FOR(x, i, j) { cost_cnt[k][x] = cost_cnt[k + 1][x]; }
        while (s.size() && s.back().c >= k) {
          FOR(x, s.back().a, s.back().b) { cost_cnt[k][x] += 1; }
          s.pop_back();
        }
      }
      assert(!s.size());
      FOR(x, i, j) {
        long long mxl = 0, mxr = 0;
        int pl = -1, pr = -1;
        ROF(k, ld, 1) { // f[i,j,k],x是分界
          if (mxl < f[i][x - 1][k]) { mxl = f[i][x - 1][k], pl = k; }
          if (mxr < f[x + 1][j][k]) { mxr = f[x + 1][j][k], pr = k; }
          if (f[i][j][k] < mxl + mxr + cost_cnt[k][x] * D[k - 1]) {
            f[i][j][k] = mxl + mxr + cost_cnt[k][x] * D[k - 1];
            fx[i][j][k] = {x, pl, pr};
          }
        }
      }
      // FOR(k,1,ld)if(f[i][j][k])printf("f[%d,%d,%d]=%lld\n",i,j,k,f[i][j][k]);
    }
  }
  int x = 1;
  FOR(k, 2, ld) if (f[1][n][k] > f[1][n][x]) x = k;
  // FOR(i,1,ld)printf("%d%c",D[i-1]," \n"[i==ld]);
  printf("%lld\n", f[1][n][x]);
  function<void(int, int, int)> print;
  print = [&print](int l, int r, int k) {
    // fprintf(stderr,"print(%d,%d,%d)\n",l,r,k);
    auto cur = fx[l][r][k];
    if (cur.b != -1) {
      print(l, cur.a - 1, cur.b);
    } else {
      FOR(i, l, cur.a - 1) printf("%d ", D[k - 1]);
    }
    printf("%d ", D[k - 1]);
    if (cur.c != -1) {
      print(cur.a + 1, r, cur.c);
    } else {
      FOR(i, cur.a + 1, r) printf("%d ", D[k - 1]);
    }
  };
  print(1, n, x);
  return 0;
}
