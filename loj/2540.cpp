// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

const int N = 20, P = 998244353;

int pw(int a, int m) {
  int res = 1;
  while (m) m & 1 ? res = 1ll * res * a % P : 0, a = 1ll * a * a % P, m >>= 1;
  return res;
}

int n, m;
int adj[N];
int f[1 << N], g[1 << N];
int fac[N + 1], fnv[N + 1];

int main() {
  fac[0] = 1;
  FOR(i, 1, N) fac[i] = 1ll * fac[i - 1] * i % P;
  fnv[N] = pw(fac[N], P - 2);
  ROF(i, N, 1) fnv[i - 1] = 1ll * fnv[i] * i % P;

  scanf("%d%d", &n, &m);
  FOR(i, 1, m) {
    int u, v;
    scanf("%d%d", &u, &v);
    --u, --v;
    adj[u] |= 1 << v;
    adj[v] |= 1 << u;
  }
  FOR(i, 0, n - 1) adj[i] |= 1 << i;

  int nn = 1 << n;
  FOR(i, 0, nn - 1) {
    FOR(j, 0, n - 1) if (!(i >> j & 1)) {
      int k = i | adj[j];
      g[k] = max(g[k], g[i] + 1);
    }
  }
  f[0] = 1;
  FOR(i, 0, nn - 1) {
    int popI = __builtin_popcount(i);
    FOR(j, 0, n - 1) if (!(i >> j & 1)) {
      int k = i | adj[j], popK = __builtin_popcount(k);
      if (g[k] == g[i] + 1) {
        int rest = n - popI - 1, x = popK - popI - 1;
        assert(rest >= 0 && x >= 0);
        f[k] = (f[k] + 1ll * fac[rest] * fnv[rest - x] % P * f[i]) % P;
      }
    }
  }

  int ans = 1ll * f[nn - 1] * fnv[n] % P;
  printf("%d\n", ans);
  return 0;
}
