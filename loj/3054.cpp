// by Sshwy
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA
const int N = 1e3 + 5;
typedef pair<int, int> point;
point operator-(point x, point y) { return {x.first - y.first, x.second - y.second}; }
point operator+(point x, point y) { return {x.first + y.first, x.second + y.second}; }
point slope(point x) {
  if (x.first < 0) x = (point){0, 0} - x;
  int t = __gcd(x.first, x.second);
  return {x.first / t, x.second / t};
}
int n;
point a[N];

map<point, vector<point>> mp; // slope, mid points

int main() {
  scanf("%d", &n);
  FOR(i, 1, n) scanf("%d%d", &a[i].first, &a[i].second);

  FOR(i, 1, n) FOR(j, i + 1, n) {
    auto k = slope(a[i] - a[j]);
    auto s = a[i] + a[j];
    mp[k].pb(s);
  }
  for (auto &pr : mp) {
    sort(pr.second.begin(), pr.second.end()); // sort by x
  }

  FOR(center, 1, n) { point D = a[center]; }

  return 0;
}
