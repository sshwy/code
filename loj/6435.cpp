// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

const int N = 3e5 + 5;

int n, L[N], a[N], f[N][20];
long long g[N][20];

void init() {
  FOR(i, 1, n) a[i] = L[i];
  ROF(i, n - 1, 1) a[i] = min(a[i], a[i + 1]);
  FOR(i, 1, n) { f[i][0] = a[i], g[i][0] = i - f[i][0]; }
  FOR(j, 1, 19) {
    FOR(i, 1, n) {
      int x = f[i][j - 1];
      f[i][j] = f[x][j - 1];
      g[i][j] = g[i][j - 1] + g[x][j - 1] + (x - f[x][j - 1]) * (1ll << (j - 1));
    }
  }
}
long long qry_2(int l, int x) {
  if (l >= x) return 0;
  long long res = 0, steps = 0;
  ROF(j, 19, 0) if (f[x][j] > l) {
    res += g[x][j] + steps * (x - f[x][j]);
    x = f[x][j];
    steps += 1 << j;
  }
  res += (steps + 1) * (x - l);
  return res;
}
long long qry(int l, int x) {
  if (l >= x) return 0;
  long long res = qry_2(l, L[x]);
  res += max(x - l, 0);
  return res;
}

long long gcd(long long A, long long B) { return A ? gcd(B % A, A) : B; }

int main() {
  scanf("%d", &n);
  FOR(i, 2, n) scanf("%d", L + i);
  L[1] = 1;

  init();

  int q;
  scanf("%d", &q);
  FOR(i, 1, q) {
    int l, r, x;
    scanf("%d%d%d", &l, &r, &x);
    long long A = qry(l, x) - qry(r + 1, x);
    long long B = r - l + 1;
    long long g = gcd(A, B);
    printf("%lld/%lld\n", A / g, B / g);
  }
  return 0;
}
