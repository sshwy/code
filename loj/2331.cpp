// by Sshwy
//#define DEBUGGER
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

#ifdef DEBUGGER

#define log_prefix()                                                \
  {                                                                 \
    fprintf(stderr, "\033[37mLine %-3d [%dms]:\033[0m  ", __LINE__, \
        (int)clock() / 1000);                                       \
  }
#define ilog(...) \
  { fprintf(stderr, __VA_ARGS__); }
#define llog(...)      \
  {                    \
    log_prefix();      \
    ilog(__VA_ARGS__); \
  }
#define log(...)           \
  {                        \
    log_prefix();          \
    ilog(__VA_ARGS__);     \
    fprintf(stderr, "\n"); \
  }
#define red(...)       \
  {                    \
    log_prefix();      \
    ilog("\033[31m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define green(...)     \
  {                    \
    log_prefix();      \
    ilog("\033[32m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define blue(...)      \
  {                    \
    log_prefix();      \
    ilog("\033[34m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }

#else

#define log_prefix() ;
#define ilog(...) ;
#define llog(...) ;
#define log(...) ;
#define red(...) ;
#define green(...) ;
#define blue(...) ;

#endif

namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 2005, P = 998244353;
int pw(int a, int m) {
  int res = 1;
  while (m) m & 1 ? res = 1ll * res * a % P : 0, a = 1ll * a * a % P, m >>= 1;
  return res;
}
int n, m, k;

typedef pair<int, int> pii;
struct atom {
  int a, b, c;
} a[N];
int w[N], lw, up[N], f[N];
// f[i]表示，最后放的位置是i的方案数。
//转移的时候，枚举上一个放的位置，并且要求右端点小于等于i的限制都被满足。

void work(vector<pii> &lim) {
  sort(lim.begin(), lim.end(),
      [](pii x, pii y) { return x.fi != y.fi ? x.fi > y.fi : x.se < y.se; });
  int R = 1e9;
  for (auto x = lim.begin(); x != lim.end(); ++x) {
    if (x->se > R)
      lim.erase(x), --x;
    else
      R = x->se;
  }
  reverse(lim.begin(), lim.end());
}
int calc(vector<pii> &lim, vector<int> pos) {

  work(lim);
  pos.pb(lw + 1);
  sort(pos.begin(), pos.end());
  fill(f, f + int(pos.size()) + 1, 0);
  f[0] = 1;

  auto plim = lim.begin();
  for (int x = 1; x <= int(pos.size()); ++x) {
    int px = pos[x - 1];
    while (plim != lim.end() && plim->se < px) ++plim;
    long long coef = (pw(up[px], w[px]) - pw(up[px] - 1, w[px]) + P) % P, L = 0;
    if (plim != lim.begin()) L = (plim - 1)->fi;
    long long c2 = coef;
    // printf("L %d R %d\n",L,R);
    for (int y = x - 1; y >= 0; y--) {
      int py = pos[y - 1];
      if (L <= py) {
        // printf("  py %d f[%d] %d\n",py,y,c2);
        f[x] = (f[x] + f[y] * c2) % P;
      }
      c2 = c2 * pw(up[py] - 1, w[py]) % P; //不能达到上界
    }
    // printf("coef %lld px %d fx %d\n",coef,px,f[x]);
  }
  return f[pos.size()];
}
void go() {
  scanf("%d%d%d", &n, &m, &k);
  vector<int> v = {1, n + 1};
  map<int, pair<vector<pii>, vector<int>>> s;
  FOR(i, 1, m) {
    int l, r, x;
    scanf("%d%d%d", &l, &r, &x);
    a[i] = {l, r, x};
    v.pb(l);
    v.pb(r + 1);
  }
  sort(v.begin(), v.end());
  v.erase(unique(v.begin(), v.end()), v.end());
  lw = v.size() - 1;
  FOR(i, 1, lw) { w[i] = v[i] - v[i - 1]; }
  w[lw + 1] = 1, up[lw + 1] = 1;
  FOR(i, 1, m) {
    a[i].a = upper_bound(v.begin(), v.end(), a[i].a) - v.begin();
    a[i].b = upper_bound(v.begin(), v.end(), a[i].b) - v.begin();
    s[a[i].c].fi.pb({a[i].a, a[i].b});
  }
  fill(up + 1, up + lw + 1, k);
  FOR(i, 1, m) { FOR(j, a[i].a, a[i].b) up[j] = min(up[j], a[i].c); }
  FOR(i, 1, lw) s[up[i]].se.pb(i);

  int ans = 1;
  for (auto &x : s) ans = 1ll * ans * calc(x.se.fi, x.se.se) % P;
  printf("%d\n", ans);
}
int main() {
  int t;
  scanf("%d", &t);
  FOR(i, 1, t) { go(); }
  return 0;
}

// 每个点有一个上限up[i]，不能超过上限。
// 限制(l,r,x)由up[i]=x的点满足。
// 因此对每种限制DP，然后乘起来即可
