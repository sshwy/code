#include <bits/stdc++.h>
#define FOR(a, b, c) for (int a = b; a <= int(c); ++a)
#define ROF(a, b, c) for (int a = b; a >= int(c); --a)
#define pb push_back
using namespace std;
const int N = 1e5 + 5;
int n, m, vis[N], dg[N], fa[N], use[N], dep[N], f[N];
vector<int> g[N];
vector<pair<int, int>> E;
int get(int u) { return f[u] == u ? u : f[u] = get(f[u]); }
bool dfs(int u, int col) {
  vis[u] = col;
  for (auto v : g[u]) {
    if (vis[v]) {
      if (vis[v] == vis[u]) return 1;
    } else {
      if (dfs(v, col ^ 1)) return 1;
    }
  }
  return 0;
}
priority_queue<pair<int, int>> A, B;
void dfs2(int u, int p) {
  // printf("dfs2 %d %d\n",u,p);
  fa[u] = p;
  dep[u] = dep[p] + 1;
  vis[u] = 1;
  for (int v : g[u])
    if (v != p) { dfs2(v, u); }
}
queue<int> q;
void go() {
  scanf("%d%d", &n, &m);
  // printf("n %d m %d\n",n,m);
  FOR(i, 1, n) g[i].clear();
  E.clear();
  fill(vis, vis + n + 1, 0);
  fill(dg, dg + n + 1, 0);
  while (A.size()) A.pop();
  while (B.size()) B.pop();
  FOR(i, 1, m) {
    int x, y;
    scanf("%d%d", &x, &y);
    g[x].pb(y);
    g[y].pb(x);
    dg[x]++, dg[y]++;
    A.push({min(x, y), max(x, y)});
  }
  FOR(i, 1, n) if (!vis[i] && dfs(i, 2)) {
    puts("NO");
    return;
  }
  // puts("GG");
  FOR(i, 1, n) if (dg[i] == 1) q.push(i);
  while (!q.empty()) {
    int u = q.front();
    q.pop();
    // printf("u %d\n",u);
    for (auto v : g[u]) {
      if (dg[v]) {
        B.push({min(u, v), max(u, v)});
        dg[v]--, dg[u]--;
        if (dg[v] == 1) q.push(v);
      }
    }
    assert(dg[u] == 0);
  }
  // puts("GG");
  FOR(i, 1, n) g[i].clear();
  FOR(i, 1, n) f[i] = i;
  fill(dg, dg + n + 1, 0);
  while (1) {
    while (A.size() && B.size() && A.top() == B.top()) A.pop(), B.pop();
    if (A.empty()) break;
    auto e = A.top();
    A.pop();
    int u = e.first, v = e.second;
    // printf("e %d %d\n",u,v);
    dg[u]++, dg[v]++;
    if (get(u) == get(v)) {
      E.pb({u, v});
    } else {
      g[u].pb(v);
      g[v].pb(u);
      f[get(u)] = get(v);
    }
  }
  // puts("GG");
  FOR(i, 1, n) if (dg[i] > 3) {
    puts("NO");
    return;
  }
  FOR(i, 1, n) f[i] = i;
  fill(vis, vis + n + 1, 0);
  fill(use, use + n + 1, 0);
  fill(fa, fa + n + 1, 0);
  FOR(i, 1, n) if (!vis[i]) dfs2(i, 0);
  // puts("GG2");
  for (auto e : E) {
    int x = e.first, y = e.second;
    while (1) {
      x = get(x), y = get(y);
      if (x == y) break;
      if (dep[x] < dep[y]) swap(x, y);
      use[x] = 1;
      f[get(x)] = fa[x];
    }
  }
  // puts("GG2");
  FOR(i, 1, n) if (fa[i] != 0 && use[i] == 0) {
    puts("NO");
    return;
  }
  FOR(i, 1, n) if (fa[i] != 0 && dg[i] == 3 && dg[fa[i]] == 3) {
    puts("NO");
    return;
  }
  for (auto e : E) {
    if (dg[e.first] == 3 && dg[e.second] == 3) {
      puts("NO");
      return;
    }
  }
  puts("YES");
}
int main() {
  int t;
  scanf("%d", &t);
  FOR(i, 1, t) go();
  return 0;
}