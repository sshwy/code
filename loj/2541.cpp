// by Sshwy
//#define DEBUGGER
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

#ifdef DEBUGGER

#define log_prefix()                                                \
  {                                                                 \
    fprintf(stderr, "\033[37mLine %-3d [%dms]:\033[0m  ", __LINE__, \
        (int)clock() / 1000);                                       \
  }
#define ilog(...) \
  { fprintf(stderr, __VA_ARGS__); }
#define llog(...)      \
  {                    \
    log_prefix();      \
    ilog(__VA_ARGS__); \
  }
#define log(...)           \
  {                        \
    log_prefix();          \
    ilog(__VA_ARGS__);     \
    fprintf(stderr, "\n"); \
  }
#define red(...)       \
  {                    \
    log_prefix();      \
    ilog("\033[31m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define green(...)     \
  {                    \
    log_prefix();      \
    ilog("\033[32m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define blue(...)      \
  {                    \
    log_prefix();      \
    ilog("\033[34m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }

#else

#define log_prefix() ;
#define ilog(...) ;
#define llog(...) ;
#define log(...) ;
#define red(...) ;
#define green(...) ;
#define blue(...) ;

#endif

namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/

const int LSZ = 19, SZ = 1 << LSZ, P = 998244353;
typedef vector<int> poly;

int pw(int a, int m) {
  int res = 1;
  while (m) m & 1 ? res = 1ll * res * a % P : 0, a = 1ll * a * a % P, m >>= 1;
  return res;
}

int tr[SZ];
int init(int n) {
  int len = 1;
  while (len < n) len <<= 1;
  FOR(i, 0, len - 1) tr[i] = tr[i >> 1] >> 1 | ((i & 1) * (len >> 1));
  return len;
}
void dft(poly &f, int len, int tag) {
  assert(f.size() <= len);
  f.resize(len, 0);
  for (int i = 0; i < len; ++i)
    if (i < tr[i]) swap(f[i], f[tr[i]]);
  for (int j = 1; j < len; j <<= 1)
    for (int i = 0, wn = pw(3, (P - 1) + (P - 1) / (j << 1) * tag); i < len;
         i += j << 1)
      for (int k = i, u, v, w = 1; k < i + j; k++, w = w * 1ll * wn % P)
        u = f[k], v = f[k + j] * 1ll * w % P, f[k] = (u + v) % P,
        f[k + j] = (u - v + P) % P;
  if (tag == -1)
    for (int i = 0, il = pw(len, P - 2); i < len; ++i) f[i] = f[i] * 1ll * il % P;
}

poly operator+(const poly &a, const poly &b) {
  poly res(a);
  res.resize(max(a.size(), b.size()), 0);
  for (int i = 0; i < b.size(); ++i) res[i] = (res[i] + b[i]) % P;
  return res;
}
poly operator*(const poly &a, const poly &b) {
  poly A = a, B = b;
  int len = init(A.size() + B.size());
  poly C(len, 0);
  dft(A, len, 1), dft(B, len, 1);
  FOR(i, 0, len - 1) C[i] = A[i] * 1ll * B[i] % P;
  dft(C, len, -1);
  // for(auto x:C)cout<<x<<" "; cout<<"P"<<endl;
  return C;
}
const int N = 1e5 + 5;
int n, a[N], s;
poly f[N];

poly solve(int l, int r) {
  if (l == r) return f[l];
  int mid = (l + r) >> 1;
  return solve(l, mid) * solve(mid + 1, r);
}
int main() {
  scanf("%d", &n);
  FOR(i, 1, n) scanf("%d", &a[i]), s += a[i];
  FOR(i, 2, n) {
    f[i] = poly(a[i] + 1, 0);
    f[i][0] = P - 1;
    f[i][a[i]] = 1;
  }
  poly F = solve(2, n);
  long long is = pw(s, P - 2);
  int ans = 0;
  for (unsigned i = 0; i < F.size(); ++i) {
    long long coef = pw(((1 - i * is) % P + P) % P, P - 2);
    ans = (ans + coef * F[i]) % P;
  }
  ans = ans * 1ll * a[1] % P * is % P;
  printf("%d\n", ans);
  return 0;
}
