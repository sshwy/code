#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 1e5 + 5, P = 100003;
int pw(int a, int m) {
  int res = 1;
  while (m) m & 1 ? res = 1ll * res * a % P : 0, a = 1ll * a * a % P, m >>= 1;
  return res;
}

int n, k, x;
int a[N], f[N];

int main() {
  scanf("%d%d", &n, &k);
  FOR(i, 1, n) scanf("%d", &a[i]);

  f[n] = 1; //你随便按一个，都能变成按n-1次的情况
  ROF(i, n - 1, k + 1)
  f[i] = (n + (n - i) * 1ll * f[i + 1]) % P * pw(i, P - 2) % P;
  ROF(i, k, 1) f[i] = 1;
  f[0] = 0;

  ROF(i, n, 1) if (a[i]) {
    ++x;
    for (int j = 1; j * j <= i; j++) {
      if (i % j) continue;
      a[j] ^= 1;
      if (j * j < i) a[i / j] ^= 1;
    }
  }

  int ans = 0;
  FOR(i, 1, x) ans = (ans + f[i]) % P;
  FOR(i, 1, n) ans = 1ll * ans * i % P;

  printf("%d\n", ans);
  return 0;
}
