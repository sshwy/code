// by Sshwy
//#define DEBUGGER
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

#ifdef DEBUGGER

#define log_prefix()                                                \
  {                                                                 \
    fprintf(stderr, "\033[37mLine %-3d [%dms]:\033[0m  ", __LINE__, \
        (int)clock() / 1000);                                       \
  }
#define ilog(...) \
  { fprintf(stderr, __VA_ARGS__); }
#define llog(...)      \
  {                    \
    log_prefix();      \
    ilog(__VA_ARGS__); \
  }
#define log(...)           \
  {                        \
    log_prefix();          \
    ilog(__VA_ARGS__);     \
    fprintf(stderr, "\n"); \
  }
#define red(...)       \
  {                    \
    log_prefix();      \
    ilog("\033[31m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define green(...)     \
  {                    \
    log_prefix();      \
    ilog("\033[32m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define blue(...)      \
  {                    \
    log_prefix();      \
    ilog("\033[34m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }

#else

#define log_prefix() ;
#define ilog(...) ;
#define llog(...) ;
#define log(...) ;
#define red(...) ;
#define green(...) ;
#define blue(...) ;

#endif

namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 1e5 + 5, L = 5.1e5 + 5, SZ = L, ALP = 26;
int n;
char s[L];
int tr[SZ][ALP], e[SZ], tot = 1;
long long f[SZ];

void insert(char *s, int u = 1) {
  if (*s == 0) {
    ++e[u];
    return;
  }
  int c = *s - 'a';
  if (!tr[u][c]) tr[u][c] = ++tot;
  insert(s + 1, tr[u][c]);
}
namespace span {
  vector<int> g[L];
  int sz[L];
  void add_path(int u, int v) { g[u].pb(v); }
  long long dfs(int u) {
    long long res = 0;
    sz[u] = 1;
    for (int v : g[u]) res += dfs(v), sz[u] += sz[v];
    vector<int> V;
    for (int v : g[u]) V.pb(sz[v]);
    sort(V.begin(), V.end());
    long long pre = 0;
    for (auto x : V) res += pre, pre += x;
    ++res;
    return res;
  }
} // namespace span
void dfs(int u, int blk) {
  if (e[u]) span::add_path(blk, u), blk = u;
  FOR(i, 0, ALP - 1) if (tr[u][i]) dfs(tr[u][i], blk);
}
int main() {
  scanf("%d", &n);
  FOR(i, 1, n) {
    scanf("%s", s);
    reverse(s, s + strlen(s));
    insert(s);
  }
  dfs(1, 1);
  span::dfs(1);
  printf("%lld\n", span::dfs(1) - 1);
  return 0;
}
