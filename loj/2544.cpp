// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

const int N = 2e7 + 5, P = 1e9 + 7;

int pw(int a, int m) {
  int res = 1;
  while (m) m & 1 ? res = 1ll * res * a % P : 0, a = 1ll * a * a % P, m >>= 1;
  return res;
}

int L, R;

int typ[N];
// 0: undetermined
// 1: prime
// 2: "prime" in [L, R]
// 3: multiply of "prime"
// 4: coprime in [1, L - 1]
int pn[N], lp;

int main() {
  scanf("%d%d", &L, &R);
  int n = R - L + 1;
  if (L == 1) {
    int ans = 1;
    FOR(i, 1, n - 1) ans = 1ll * ans * i % P;
    int coef = 1ll * n * (n + 1) / 2 % P;
    ans = 1ll * ans * coef % P;
    printf("%d\n", ans);
    return 0;
  }

  FOR(i, 2, L - 1) {
    if (!typ[i]) typ[i] = 1, pn[++lp] = i;
    FOR(j, 1, lp) {
      if (1ll * i * pn[j] >= L) break;
      typ[i * pn[j]] = 4;
      if (i % pn[j] == 0) break;
    }
  }
  int cntPrime = lp;
  FOR(i, L, R) {
    if (!typ[i]) typ[i] = 2, pn[++lp] = i;
    if (typ[i] == 2 || typ[i] == 3) {
      FOR(j, 1, lp) {
        if (1ll * i * pn[j] > R) break;
        typ[i * pn[j]] = 3;
      }
    }
  }
  int k = lp - cntPrime;
  // printf("k %d\n", k);

  int ans = 1ll * k * pw(k + 1, P - 2) % P;
  FOR(i, 1, n + 1) ans = 1ll * ans * i % P;

  printf("%d\n", ans);
  return 0;
}
