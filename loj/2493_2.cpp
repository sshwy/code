#include <bits/stdc++.h>
#define FOR(a, b, c) for (int a = b; a <= int(c); ++a)
#define ROF(a, b, c) for (int a = b; a >= int(c); --a)
#define pb push_back
using namespace std;
const int N = 1e5 + 5;
int n, m, vis[N];
vector<int> g[N];
bool dfs(int u, int col) {
  vis[u] = col;
  for (auto v : g[u]) {
    if (vis[v]) {
      if (vis[v] == vis[u]) return 1;
    } else {
      if (dfs(v, col ^ 1)) return 1;
    }
  }
  return 0;
}
void go() {
  scanf("%d%d", &n, &m);
  FOR(i, 1, n) g[i].clear();
  fill(vis, vis + n + 1, 0);
  FOR(i, 1, m) {
    int x, y;
    scanf("%d%d", &x, &y);
    g[x].pb(y);
    g[y].pb(x);
  }
  FOR(i, 1, n) if (!vis[i] && dfs(i, 2)) {
    puts("NO");
    return;
  }
  puts("YES");
}
int main() {
  int t;
  scanf("%d", &t);
  FOR(i, 1, t) go();
  return 0;
}