// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

const int N = 1e5 + 5, P = 998244353;

int pw(int a, int m) {
  int res = 1;
  while (m) m & 1 ? res = 1ll * res * a % P : 0, a = 1ll * a * a % P, m >>= 1;
  return res;
}
void add(int &x, int y) { x = (x + y) % P; }

int n, k;
int b[N], fac[N], fnv[N], ans[N];
pair<int, int> a[N];

int binom(int x, int y) {
  if (x < y) return 0;
  return 1ll * fac[x] * fnv[y] % P * fnv[x - y] % P;
}
void go1() {
  int pos = 0, p2 = 1;
  b[0] = -1;
  FOR(i, 1, n) {
    while (pos < n && b[pos + 1] * 2 < b[i]) ++pos;
    while (p2 <= n && b[p2] < b[i]) ++p2;
    assert(p2 <= n);
    add(ans[a[i].second], binom(n - p2 + pos, k));
    if (b[i] == 0) add(ans[a[i].second], binom(n - p2 + pos, k - 1));
  }
}
void go2() {
  FOR(i, 1, n) if (b[i]) {
    int nrk = n - ((lower_bound(b + 1, b + n + 1, b[i] * 2) - b) - 1) + 1;
    int rk = n - ((lower_bound(b + 1, b + n + 1, b[i]) - b) - 1);
  }
}
int main() {
  fac[0] = 1;
  FOR(i, 1, N - 1) fac[i] = 1ll * fac[i] * i % P;
  fnv[N - 1] = pw(fac[N - 1], P - 2);
  ROF(i, N - 1, 1) fnv[i - 1] = 1ll * fnv[i] * i % P;

  scanf("%d%d", &n, &k);
  FOR(i, 1, n) scanf("%d", &a[i].first), a[i].second = i;

  sort(a + 1, a + n + 1);
  FOR(i, 1, n) b[i] = a[i].first;

  go1();
  go2();
  return 0;
}
