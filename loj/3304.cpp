#include <bits/stdc++.h>
#define FOR(a, b, c) for (int a = (int)(b); a <= (int)(c); a++)
#define int long long
using namespace std;

const int N = 35, W = 2e5 + 5, P = 998244353;
int pw(int a, int m) {
  int res = 1;
  while (m) m & 1 ? res = 1ll * res * a % P : 0, a = 1ll * a * a % P, m >>= 1;
  return res;
}

struct poly {
  int f[2];
  poly() { f[0] = f[1] = 0; }
  poly operator+(const poly p) const {
    poly res = *this;
    res.f[0] = (res.f[0] + p.f[0]) % P;
    res.f[1] = (res.f[1] + p.f[1]) % P;
    return res;
  }
  poly operator-(const poly p) const {
    poly res = *this;
    res.f[0] = (res.f[0] - p.f[0] + P) % P;
    res.f[1] = (res.f[1] - p.f[1] + P) % P;
    return res;
  }
  poly operator*(const poly p) const {
    poly res;
    FOR(i, 0, 1) FOR(j, 0, 1) if (i + j < 2) {
      res.f[i + j] = (res.f[i + j] + 1ll * f[i] * p.f[j]) % P;
    }
    return res;
  }
  poly inv() {
    poly res;
    res.f[0] = pw(f[0], P - 2);
    res.f[1] = (P - res.f[0] * 1ll * res.f[0] % P * f[1] % P) % P;
    return res;
  }
  // void print(){ printf("[%lld,%lld]", f[0], f[1]); }
};
poly F(int val) {
  poly p;
  p.f[0] = 1;
  p.f[1] = val;
  return p;
}

poly det(poly g[N][N], int n) {
  // FOR(i,1,n) { FOR(j,1,n){ g[i][j].print(); } putchar('\n'); }
  int sig = 1, shift = 0;
  FOR(i, 1, n) {
    while (g[i][i].f[0] == 0) {
      FOR(j, i + 1, n) if (g[j][i].f[0]) {
        FOR(k, i, n) swap(g[i][k], g[j][k]);
        sig = -sig;
        break;
      }
      if (g[i][i].f[0] == 0) {
        if (shift) return poly(); // det = 0
        FOR(j, i, n) g[j][i].f[0] = g[j][i].f[1], g[j][i].f[1] = 0;
        ++shift;
      }
    }
    poly iv = g[i][i].inv();
    FOR(j, i + 1, n) {
      poly coef = g[j][i] * iv;
      FOR(k, i, n) g[j][k] = g[j][k] - g[i][k] * coef;
    }
  }
  poly res;
  res.f[0] = (P + sig) % P;
  FOR(i, 1, n) res = res * g[i][i];
  if (shift) res.f[1] = res.f[0], res.f[0] = 0;
  return res;
}

int n, m;
int w[N][N], cnt[W];
int ans = 0;
bool bp[W];
int pn[W], lp, phi[W];

void go(int g) { // L = D - A
  // printf("go %lld\n", g);
  poly L[N][N];
  FOR(i, 1, n) FOR(j, 1, n) if (w[i][j] && w[i][j] % g == 0) {
    poly fw = F(w[i][j]);
    L[i][i] = L[i][i] + fw;
    L[i][j] = L[i][j] - fw;
  }
  poly p = det(L, n - 1);
  // printf("p: "); p.print(); puts("");
  ans = (ans + p.f[1] * 1ll * phi[g]) % P;
  // printf("i %lld det %lld\n", g, p.f[1]);
}
void sieve() {
  bp[0] = bp[1] = true;
  phi[1] = 1;
  FOR(i, 2, W - 1) {
    if (bp[i] == false) pn[++lp] = i, phi[i] = i - 1;
    FOR(j, 1, lp) {
      if (i * pn[j] >= W) break;
      bp[i * pn[j]] = true;
      if (i % pn[j] == 0) {
        phi[i * pn[j]] = phi[i] * pn[j];
        break;
      } else {
        phi[i * pn[j]] = phi[i] * (pn[j] - 1);
      }
    }
  }
}
signed main() {
  scanf("%lld%lld", &n, &m);
  FOR(i, 1, m) {
    int u, v, val;
    scanf("%lld%lld%lld", &u, &v, &val);
    w[u][v] = w[v][u] = val;
    for (int i = 1; i * i <= val; ++i) {
      if (val % i) continue;
      cnt[i]++;
      if (i * i < val) cnt[val / i]++;
    }
  }
  sieve();
  FOR(i, 1, W - 1) if (cnt[i] >= n - 1) go(i);
  printf("%lld\n", ans);
  return 0;
}
