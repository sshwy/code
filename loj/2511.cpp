#include <bits/stdc++.h>
#define FOR(a, b, c) for (int a = b; a <= int(c); ++a)
#define ROF(a, b, c) for (int a = b; a >= int(c); --a)
#define pb push_back
using namespace std;

int s, t;
string name;
int f[20][1000][1000];
bool vis[20][1000][1000];
// f[i,j,k]：m=j,n=k是否在第i次询问时恰好知道（之前都不知道）
int F(int i, int j, int k) {
  assert(j <= k);
  if (i == 0) {
    return 0;
    if ((name == "Alice") == (i & 1)) {
      int prod = j * k, cnt = 0;
      for (int x = s; x * x <= prod; ++x) {
        if (prod % x) continue;
        ++cnt;
      }
      if (cnt == 1) return 1;
      return 0;
    } else {
      if (j + k == s + s) return 1;
      return 0;
    }
  }
  if (vis[i][j][k]) return f[i][j][k];
  vis[i][j][k] = 1;
  if (i >= 2 && F(i - 2, j, k) == 1) return f[i][j][k] = 1;
  if ((name == "Alice") == (i & 1)) {
    int prod = j * k;
    bool flag = 1;
    for (int x = s; x * x <= prod; ++x) {
      if (prod % x) continue;
      int y = prod / x;
      if (x == j) {
        if (F(i - 2, j, k) != 0) flag = 0;
      } else {
        if (F(i - 2, x, y) == 0) flag = 0;
      }
    }
    if (flag) return f[i][j][k] = 1;
  } else {
    int sum = j + k;
    bool flag = 1;
    for (int x = s; x + x <= sum; ++x) {
      int y = sum - x;
      if (x == j) {
        if (F(i - 1, j, k) != 0) flag = 0;
      } else {
        if (F(i - 1, x, y) == 0) flag = 0;
      }
    }
    if (flag) return f[i][j][k] = 1;
  }
  return f[i][j][k] = 0;
}
int G(int i, int j, int k) {
  if ((name == "Alice") == (i & 1)) {
    int prod = j * k;
    bool flag = 1;
    for (int x = s; x * x <= prod; ++x) {
      if (prod % x) continue;
      int y = prod / x;
      if (x != j && F(i - 1, x, y)) flag = 0;
    }
    if (flag) return 1;
  } else {
    int sum = j + k;
    bool flag = 1;
    for (int x = s; x + x <= sum; ++x) {
      int y = sum - x;
      if (x != j && F(i - 1, x, y)) flag = 0;
    }
    if (flag) return 1;
  }
  return 0;
}
int main() {
  cin >> s >> name >> t;
  for (int sum = s + s;; ++sum) {
    for (int m = s; m + m <= sum; ++m) {
      int n = sum - m;
      printf("search %d %d\n", m, n);
      bool flag = 1;
      FOR(i, 0, t) {
        // printf("F(%d,%d,%d) %d\n",i,m,n,F(i,m,n));
        if (F(i, m, n)) {
          flag = 0;
          break;
        }
      }
      if (flag && F(t + 1, m, n) && G(t + 2, m, n)) {
        printf("%d %d\n", m, n);
        return 0;
      }
    }
  }
  return 0;
}