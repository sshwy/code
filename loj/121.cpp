#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <stack>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
namespace IO {
  char nc() {
    static char bf[100000], *p1 = bf, *p2 = bf;
    return p1 == p2 && (p2 = (p1 = bf) + fread(bf, 1, 100000, stdin), p1 == p2)
               ? EOF
               : *p1++;
  }
  int rd() {
    int res = 0;
    char c = getchar();
    while (!isdigit(c)) c = getchar();
    while (isdigit(c)) res = res * 10 + c - '0', c = getchar();
    return res;
  }
} // namespace IO
/******************heading******************/
const int N = 5300, M = 5e5 + 5, SZ = M << 2;

struct data {
  int op, u, v, tim;
  bool operator<(data d) const {
    return u != d.u ? u < d.u : v != d.v ? v < d.v : tim < d.tim;
  }
} d[M];
int ld;

int n, m;

struct dsu {
  int f[M], g[M];
  void init(int n) { FOR(i, 0, n) f[i] = i, g[i] = 1; }
  dsu() {}
  dsu(int n) { init(n); }
  typedef pair<pair<int *, int>, int> ppi; // f[],val,time
  stack<ppi> s;
  int current_time() { return s.empty() ? 0 : s.top().se; }
  int get(int u) { return f[u] == u ? u : get(f[u]); }

  void merge(int u, int v) {
    int cur = current_time() + 1;
    u = get(u), v = get(v);
    if (u == v) return;
    if (g[u] == g[v]) {
      s.push(mk(mk(f + u, f[u]), cur)), f[u] = v;
      s.push(mk(mk(g + v, g[v]), cur)), g[v]++;
    } else {
      if (g[u] > g[v]) swap(u, v);
      s.push(mk(mk(f + u, f[u]), cur)), f[u] = v;
    }
  }
  void undo() {
    int cur = current_time();
    while (current_time() == cur) {
      ppi u = s.top();
      s.pop();
      *u.fi.fi = u.fi.se;
    }
  }
  void undo(int untiltime) {
    while (current_time() > untiltime) undo();
  }
  bool find(int u, int v) { return get(u) == get(v); }
} D;

struct node {
  vector<pii> e;
  pii q;
  node() { e.clear(), q = mk(0, 0); }
  void addedge(pii edge) { e.pb(edge); }
  void addqry(pii qry) { q = qry; }
} t[SZ];

void addedge(pii edge, int L, int R, int u = 1, int l = 1, int r = m) {
  // printf("addedge( (%d,%d) ,L=%d,R=%d,%d,l=%d,r=%d)\n",edge.fi,edge.se,
  //        L,R,u,l,r);
  if (R < l || r < L) return;
  if (L <= l && r <= R) return t[u].addedge(edge);
  int mid = (l + r) >> 1;
  addedge(edge, L, R, u << 1, l, mid), addedge(edge, L, R, u << 1 | 1, mid + 1, r);
}
void addquery(pii qry, int p, int u = 1, int l = 1, int r = m) {
  if (l == r) return t[u].addqry(qry);
  int mid = (l + r) >> 1;
  if (p <= mid)
    addquery(qry, p, u << 1, l, mid);
  else
    addquery(qry, p, u << 1 | 1, mid + 1, r);
}

void solve(int u = 1, int l = 1, int r = m) {
  // printf("solve(%d,%d,%d)\n",u,l,r);
  int cur = D.current_time();
  const vector<pii> &eg = t[u].e;
  const pii &q = t[u].q;
  for (unsigned int i = 0; i < eg.size(); i++) { D.merge(eg[i].fi, eg[i].se); }
  if (l == r) {
    if (q.fi && q.se) puts(D.find(q.fi, q.se) ? "Y" : "N");
  } else {
    int mid = (l + r) >> 1;
    solve(u << 1, l, mid);
    solve(u << 1 | 1, mid + 1, r);
  }
  D.undo(cur);
}

int main() {
  scanf("%d%d", &n, &m);
  D.init(n);
  FOR(i, 1, m) {
    int op, x, y;
    scanf("%d%d%d", &op, &x, &y);
    if (x > y) swap(x, y);
    if (op != 2)
      d[++ld] = (data){op, x, y, i};
    else
      addquery(mk(x, y), i);
  }
  sort(d + 1, d + ld + 1);
  // FOR(i,1,ld)
  //   printf("(%d,%d), op=%d, tim=%d\n",d[i].u,d[i].v,d[i].op,d[i].tim);

  FOR(i, 1, ld) {
    assert(d[i].op == 0);
    if (i < m && d[i].u == d[i + 1].u && d[i].v == d[i + 1].v) {
      assert(d[i + 1].op == 1);
      addedge(mk(d[i].u, d[i].v), d[i].tim, d[i + 1].tim - 1);
      ++i;
    } else if (i < m) {
      addedge(mk(d[i].u, d[i].v), d[i].tim, m);
    } else {
      addedge(mk(d[i].u, d[i].v), m, m);
    }
  }
  // puts("EOJ C");
  solve();
  return 0;
}
