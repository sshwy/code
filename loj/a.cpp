#include <bits/stdc++.h>

namespace IO {
  char gc() {
#ifdef FREAD
    static char buf[1 << 21], *P1 = buf, *P2 = buf;
    if (P1 == P2) {
      P1 = buf;
      P2 = buf + fread(buf, 1, 1 << 21, stdin);
      if (P1 == P2) return EOF;
    }
    return *(P1++);
#else
    return getchar();
#endif
  }
  template <typename Tp> bool get1(Tp &x) {
    bool neg = 0;
    char c = gc();
    while (c != EOF && (c < '0' || c > '9') && c != '-') c = gc();
    if (c == '-') c = gc(), neg = 1;
    if (c == EOF) return false;
    x = 0;
    for (; c >= '0' && c <= '9'; c = gc()) x = x * 10 + c - '0';
    if (neg) x = -x;
    return true;
  }
  template <typename Tp> void printendl(Tp x) {
    if (x < 0) putchar('-'), x = -x;
    static short a[40], sz;
    sz = 0;
    while (x > 0) a[sz++] = x % 10, x /= 10;
    if (sz == 0) putchar('0');
    for (int i = sz - 1; i >= 0; i--) putchar('0' + a[i]);
    puts("");
  }
} // namespace IO
using IO::get1;
using IO::printendl;
#define get2(x, y) get1(x) && get1(y)
#define get3(x, y, z) get2(x, y) && get1(z)
#define get4(x, y, z, w) get3(x, y, z) && get1(w)
#define pb push_back
#define mp std::make_pair
#define ff first
#define ss second
typedef long long LL;
typedef unsigned long long uLL;
typedef std::pair<int, int> pii;
const int inf = 0x3f3f3f3f;
const LL Linf = 1ll << 61;

const int maxn = 8111;

int n, mod;
int qpow(int x, int y) {
  int ret = 1;
  while (y) {
    if (y & 1) ret = 1ll * ret * x % mod;
    x = 1ll * x * x % mod;
    y >>= 1;
  }
  return ret;
}

int c[maxn][maxn], fac[maxn], pr[maxn];
bool isp[maxn];
void prework() {
  fac[0] = 1;
  for (int i = 1; i < maxn; i++) fac[i] = 1ll * i * fac[i - 1] % (mod - 1);
  for (int i = 0; i < maxn; i++) {
    c[i][0] = 1;
    for (int j = 1; j <= i; j++) {
      c[i][j] = c[i - 1][j] + c[i - 1][j - 1];
      if (c[i][j] >= (mod - 1)) c[i][j] -= (mod - 1);
    }
  }
  for (int i = 2; i <= n; i++) isp[i] = 1;
  for (int i = 2; i <= n; i++)
    if (isp[i])
      for (int j = i + i; j <= n; j += i) isp[j] = 0;
  for (int i = 2; i <= n; i++)
    if (isp[i]) {
      int now = i;
      while (now <= n) {
        pr[now] = i;
        now *= i;
      }
    }
}

int dp[maxn];
int calc(int x) {
  dp[0] = mod - 2;
  int sz = n / x;
  for (int i = 1; i <= sz; i++) {
    dp[i] = 0;
    for (int j = 1; j <= i; j++)
      dp[i] = (dp[i] + 1ll * (mod - 1 - dp[i - j]) * c[i * x - 1][j * x - 1] %
                           (mod - 1) * fac[j * x - 1]) %
              (mod - 1);
  }
  int sum = 0;
  for (int i = 1; i <= sz; i++)
    sum = (sum + 1ll * dp[i] * c[n][i * x] % (mod - 1) * fac[n - i * x]) % (mod - 1);
  printf("calc %d %d\n", x, sum);
  return sum;
}

int main() {
  get2(n, mod);
  prework();

  int ans = 1;
  for (int i = 2; i <= n; i++)
    if (pr[i]) ans = 1ll * ans * qpow(pr[i], calc(i)) % mod;
  printendl(ans);
  return 0;
}