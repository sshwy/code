// by Sshwy
//#define DEBUGGER
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

#ifdef DEBUGGER

#define log_prefix()                                                \
  {                                                                 \
    fprintf(stderr, "\033[37mLine %-3d [%dms]:\033[0m  ", __LINE__, \
        (int)clock() / 1000);                                       \
  }
#define ilog(...) \
  { fprintf(stderr, __VA_ARGS__); }
#define llog(...)      \
  {                    \
    log_prefix();      \
    ilog(__VA_ARGS__); \
  }
#define log(...)           \
  {                        \
    log_prefix();          \
    ilog(__VA_ARGS__);     \
    fprintf(stderr, "\n"); \
  }
#define red(...)       \
  {                    \
    log_prefix();      \
    ilog("\033[31m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define green(...)     \
  {                    \
    log_prefix();      \
    ilog("\033[32m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define blue(...)      \
  {                    \
    log_prefix();      \
    ilog("\033[34m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }

#else

#define log_prefix() ;
#define ilog(...) ;
#define llog(...) ;
#define log(...) ;
#define red(...) ;
#define green(...) ;
#define blue(...) ;

#endif

namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int C = 10, N = 105, M = 15, MM = 1 << 13, P = 1e9 + 7;
int n, m, c, q, tot = 1;
char s[2][C];
int nex[2][C];
int _f[MM][C][C], _g[MM][C][C];
int (*f)[C][C] = _f, (*g)[C][C] = _g;

void get_next(char *s, int *nex) {
  nex[0] = 0, nex[1] = 0;
  int i = 1, j = 0;
  while (i <= c) {
    if (j == 0 || s[i] == s[j])
      ++i, ++j, nex[i] = j;
    else
      j = nex[j];
  }
  // FOR(i,1,c)printf("%c%c",s[i]," \n"[i==c]);
  // FOR(i,1,c+1)printf("%d%c",nex[i]," \n"[i==c+1]);
}
int tr(int p, char c, char *s, int *nex) {
  ++p;
  while (p && s[p] != c) p = nex[p];
  return p;
}
void add(int &x, int y) { x = (x + y) % P; }
void go() {
  scanf("%s%s", s[0] + 1, s[1] + 1);
  get_next(s[0], nex[0]);
  get_next(s[1], nex[1]);

  // calculate invalid solution
  // init (1,0) cell
  memset(f, 0, sizeof(_f));
  f[0][0][0] = 1;
  FOR(i, 1, n) {
    FOR(j, 0, m) {
      memset(g, 0, sizeof(_f));
      FOR(S, 0, (1 << (m + 1)) - 1) {
        FOR(x, 0, c) FOR(y, 0, c) {
          if (f[S][x][y]) {
            // printf("f[%d,%d,%d,%d,%d] = %d\n", i,j,S,x,y,f[S][x][y]);
            if (j < m) { //(i,j) -> (i,j+1)
              const string sigma = "WBX";
              for (char e : sigma) {
                assert(e == 'W' || e == 'B' || e == 'X');
                int nx = tr(x, e, s[0], nex[0]);
                int ny = tr(y, e, s[1], nex[1]);
                if (ny == c && (S >> (j + 1) & 1)) continue;
                int nS = S & (~(1 << (j + 1)));
                if (nx == c) nS |= (1 << (j + 1));
                add(g[nS][nx][ny], f[S][x][y]);
              }
            } else { //(i,m) -> (i+1,0)
              add(g[S][0][0], f[S][x][y]);
            }
          }
        }
      }
      swap(f, g);
    }
  }
  int ans = 0;
  FOR(S, 0, (1 << (m + 1)) - 1) add(ans, f[S][0][0]);
  // printf("ans: %d\n",ans);
  printf("%d\n", (tot - ans + P) % P);
}
int main() {
  scanf("%d%d%d%d", &n, &m, &c, &q);
  FOR(i, 1, n) FOR(j, 1, m) tot = tot * 3ll % P;
  FOR(i, 1, q) { go(); }
  return 0;
}
