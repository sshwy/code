#include <bits/stdc++.h>
#define FOR(a, b, c) for (int a = b; a <= int(c); ++a)
#define ROF(a, b, c) for (int a = b; a >= int(c); --a)
#define pb push_back
using namespace std;
const int N = 2e5 + 5;
int n, a[N], id[N];
vector<int> g[N];
multiset<int> s[N];
void merge(int u, int v) {
  // printf("merge %d %d\n",u,v);
  if (s[id[u]].size() > s[id[v]].size()) swap(u, v);
  vector<int> V;
  auto pu = s[id[u]].begin(), pv = s[id[v]].begin();
  while (pu != s[id[u]].end() && pv != s[id[v]].end()) {
    V.pb(min(*pu, *pv));
    ++pu, ++pv;
  }
  for (auto x : V) {
    assert(s[id[v]].size());
    s[id[v]].erase(s[id[v]].begin());
  }
  for (auto x : V) s[id[v]].insert(x);
  id[u] = id[v];
}
void dfs(int u) {
  for (auto v : g[u]) {
    dfs(v);
    merge(u, v);
    // for(auto x:s[id[u]])printf("%d ",x);
    // puts("");
  }
  s[id[u]].insert(-a[u]);
}
int main() {
  scanf("%d", &n);
  FOR(i, 1, n) scanf("%d", &a[i]);
  FOR(i, 2, n) {
    int x;
    scanf("%d", &x);
    g[x].pb(i);
  }
  FOR(i, 1, n) { id[i] = i; }
  dfs(1);
  long long ans = 0;
  for (auto x : s[id[1]]) ans -= x;
  printf("%lld\n", ans);
  return 0;
}