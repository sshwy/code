#include <bits/stdc++.h>
#define FOR(a, b, c) for (int a = b; a <= int(c); ++a)
#define ROF(a, b, c) for (int a = b; a >= int(c); --a)
#define pb push_back
using namespace std;
const int N = 6e5 + 5, X = 150002, SZ = N << 2, INF = 1e9;

namespace seg {
  typedef pair<int, int> vt;
  vt v[SZ];
  int tag[SZ];
  vt operator+(vt a, vt b) {
    if (a.first < b.first)
      return a;
    else if (a.first > b.first)
      return b;
    return {a.first, a.second + b.second};
  }
  void nodeadd(int u, int val) {
    v[u].first += val;
    tag[u] += val;
  }
  void pushdown(int u) {
    if (tag[u]) nodeadd(u << 1, tag[u]), nodeadd(u << 1 | 1, tag[u]), tag[u] = 0;
  }
  void build(int u = 1, int l = 1, int r = N - 1) {
    if (l == r) return v[u] = {0, 1}, void();
    int mid = (l + r) >> 1;
    build(u << 1, l, mid), build(u << 1 | 1, mid + 1, r);
    v[u] = v[u << 1] + v[u << 1 | 1];
  }
  void add(int L, int R, int val, int u = 1, int l = 1, int r = N - 1) {
    if (R < l || r < L || L > R) return;
    // if(u==1)printf("add %d %d %d\n",L,R,val);
    if (L <= l && r <= R) return nodeadd(u, val), void();
    int mid = (l + r) >> 1;
    pushdown(u);
    add(L, R, val, u << 1, l, mid);
    add(L, R, val, u << 1 | 1, mid + 1, r);
    v[u] = v[u << 1] + v[u << 1 | 1];
  }
  vt qry(int L, int R, int u = 1, int l = 1, int r = N - 1) {
    if (R < l || r < L || L > R) return {INF, 0};
    if (L <= l && r <= R) return v[u];
    int mid = (l + r) >> 1;
    pushdown(u);
    return qry(L, R, u << 1, l, mid) + qry(L, R, u << 1 | 1, mid + 1, r);
  }
  int count_zero(int L, int R) {
    // printf("qry %d %d\n",L,R);
    auto x = qry(L, R);
    // printf("{%d,%d}\n",x.first,x.second);
    if (x.first == 0)
      return x.second;
    else
      return 0;
  }
} // namespace seg
int n, m;
int _c[N], a[N], tag;
int *c = _c + X;
int main() {
  scanf("%d%d", &n, &m);
  FOR(i, 1, n) {
    int x;
    scanf("%d", &x);
    c[x]++;
    a[i] = x;
  }
  seg::build();
  auto base = []() { return c - _c; };
  FOR(i, 1, n) { seg::add(base() + i - c[i] + 1, base() + i, 1); }
  FOR(i, 1, m) {
    int p, x;
    scanf("%d%d", &p, &x);
    if (p == 0) {
      if (x == -1) {
        --tag;
        ++c;
        seg::add(base() + n - c[n] + 1, base() + n, 1);
      } else {
        seg::add(base() + n - c[n] + 1, base() + n, -1);
        --c;
        ++tag;
      }
    } else {
      int val = a[p] + tag;
      assert(c[val]);
      if (val <= n) {
        seg::add(base() + val - c[val] + 1, base() + val - c[val] + 1, -1);
      }
      c[val]--;

      val = x;
      c[val]++;
      if (val <= n) {
        seg::add(base() + val - c[val] + 1, base() + val - c[val] + 1, 1);
      }
      a[p] = val - tag;
    }
    int ans = seg::count_zero(base() + 1, base() + n);
    printf("%d\n", ans);
  }
  return 0;
}