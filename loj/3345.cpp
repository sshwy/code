// by Yao
#include "paint.h"
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

int minimumInstructions(
    int N, int M, int K, vector<int> C, vector<int> A, vector<vector<int>> B) {
  vector<vector<int>> v(K, vector<int>());
  vector<int> f(666, 0), wall(M, N), val(M, 0), ok(N, false);
  // f[i, j] v[C[i]][j] do i
  FOR(i, 0, M - 1) for (int c : B[i]) v[c].pb(i);
  ROF(i, N - 1, 0) {
    f.clear(), f.resize(666, 0);

    int exJ = v[C[i]].size();
    FOR(j, 0, exJ - 1) {
      f[j] = 1;
      int x = v[C[i]][j];
      if (wall[(x + 1) % M] == i + 1) { f[j] = max(f[j], val[(x + 1) % M] + 1); }
      // printf("f[%d, %d] = %d\n", i + 1, v[C[i]][j] + 1, f[j]);
      if (f[j] >= M) ok[i] = true;
    }

    FOR(j, 0, exJ - 1) {
      int x = v[C[i]][j];
      wall[x] = i;
      val[x] = f[j];
    }
  }
  int pos = N, ans = 0, las = N;
  ROF(i, N - 1, 0) if (ok[i]) {
    // printf("ok %d\n", i);
    if (i < las - M) return -1;
    if (i < pos - M) {
      if (las == pos) return -1;
      pos = las;
      ++ans;
    }
    las = i;
  }
  if (las != 0) return -1;
  ++ans;
  return ans;
}
