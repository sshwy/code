// by Sshwy
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA
const int N = 1e5 + 5;
int n, k;

double f[N];
int g[N];
int q[N], ql, qr;
double w(int l, int r) { return (r - l) * 1.0 / r; }
pair<int, double> calc(double c) {
  f[0] = 0;
  g[0] = 0;
  ql = 1, qr = 0;
  q[++qr] = 0;

  FOR(i, 1, n) {
#define X(k) (k)
#define Y(k) (f[k])
#define K(i) (1.0 / (i))
#define slope(x, y) ((0.0 + Y(x) - Y(y)) / (X(x) - X(y)))
    while (ql < qr && slope(q[ql], q[ql + 1]) > K(i)) ++ql;
    int k = q[ql];
    f[i] = -K(i) * X(k) + Y(k) + 1 + c;
    g[i] = g[k] + 1;
    while (ql < qr && slope(q[qr], q[qr - 1]) < slope(q[qr], i)) --qr;
    q[++qr] = i;
  }
  return {g[n], f[n]};
}

int main() {
  scanf("%d%d", &n, &k);
  calc(0);
  double l = -1, r = 0;
  const double eps = 1e-15;
  while (l + eps < r) {
    double mid = l + (r - l) / 2;
    auto p = calc(mid);
    if (p.first <= k)
      l = mid;
    else
      r = mid;
  }
  double ans = calc(l).second - k * l;
  printf("%.10lf\n", ans);
  return 0;
}
