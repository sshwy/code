#include <bits/stdc++.h>
#define FOR(a, b, c) for (int a = b; a <= int(c); ++a)
#define ROF(a, b, c) for (int a = b; a >= int(c); --a)
#define pb push_back
using namespace std;
const int N = 2000, P = 1e9 + 7;
int n, z, m, k, p[N], a[N][N], inv[N];
int pw(int a, int m) {
  int res = 1;
  while (m) m & 1 ? res = 1ll * res * a % P : 0, a = 1ll * a * a % P, m >>= 1;
  return res;
}
void init() {
  int im = pw(m, P - 2);
  long long cur = pw(m, k) * 1ll * pw(pw(m + 1, P - 2), k) % P;
  p[0] = cur;
  FOR(i, 1, n) {
    cur = cur * (k - i + 1ll) % P * inv[i] % P * im % P;
    if (i <= k)
      p[i] = cur;
    else
      p[i] = 0;
  }
}
void add(int &x, long long y) { x = (x + y) % P; }
// long long XXX;
bool Gauss(int g[N][N], int lg) {
  for (int i = 1; i <= lg; i++) { // 消第 i 个元
    if (g[i][i] == 0) return 0;
    long long t = pw(g[i][i], P - 2);
    for (int j = 1; j <= lg; j++) {
      if (i == j) continue;
      if (g[j][i] == 0) continue;
      long long rate = g[j][i] * t % P;
      for (int k = i; k <= lg && k <= i + 1; k++) { add(g[j][k], -g[i][k] * rate); }
      add(g[j][0], -g[i][0] * rate);
    }
  }
  return 1;
}
void go() {
  // XXX=0;
  scanf("%d%d%d%d", &n, &z, &m, &k);
  if (z == 0) return puts("0"), void();
  if (k == 0) return puts("-1"), void();
  if (m == 0) {
    if (k <= 1)
      return puts("-1"), void();
    else {
      int ans = 0;
      while (z > 0) {
        if (z < n) ++z;
        z -= k;
        ++ans;
      }
      return printf("%d\n", ans), void();
    }
  }
  init();
  FOR(i, 1, n) fill(a[i], a[i] + n + 1, 0);
  long long c2 = pw(m + 1, P - 2);
  long long c1 = m * 1ll * c2 % P;
  FOR(i, 1, n - 1) {
    FOR(j, 0, i - 1) { add(a[i][i - j], p[j] * c1); }
    FOR(j, 0, i) { add(a[i][i + 1 - j], p[j] * c2); }
    add(a[i][i], P - 1);
    add(a[i][0], 1); // constant value
  }
  FOR(j, 0, n - 1) { add(a[n][n - j], p[j]); }
  add(a[n][n], P - 1);
  add(a[n][0], 1);

  // FOR(i,1,n)FOR(j,0,n)printf("%d%c",a[i][j]," \n"[j==n]);
  // printf("%d %d %d %d\n",n,z,m,k);
  if (Gauss(a, n) == 0) {
    puts("-1");
    return;
  }
  // FOR(i,1,n)FOR(j,0,n)printf("%d%c",a[i][j]," \n"[j==n]);

  int ans = (P - a[z][0]) * 1ll * pw(a[z][z], P - 2) % P;
  add(ans, P);
  // printf("XXX %lld\n",XXX);
  printf("%d\n", ans);
}
int main() {
  FOR(i, 1, 1500) inv[i] = pw(i, P - 2);
  int t;
  scanf("%d", &t);
  FOR(i, 1, t) go();
  return 0;
}