// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

const int N = 5005;
int n, a[N];

int see[N][N], f[N][N];

int main() {
  scanf("%d", &n);
  FOR(i, 1, n) scanf("%d", a + i);
  FOR(i, 1, n) {
    see[i][i] = i;
    long long p = 1, q = 0; // infty
    ROF(j, i - 1, 1) {
      long long y = a[i] - a[j], x = i - j;
      if (q >= 0 && y * q < p * x || q < 0 && y * q > p * x) {
        p = y, q = x;
        see[j][i] = j;
      } else {
        see[j][i] = see[j + 1][i];
      }
    }
  }
  int ans = 0;
  FOR(len, 0, n - 1) {
    FOR(i, 1, n - len) {
      int j = i + len;
      // printf("see[%d, %d] = %d\n", i, j, see[i][j]);
      if (len <= 1)
        f[i][j] = 1;
      else {
        int k = see[i][j];
        assert(k < j);
        f[i][j] = min(f[i][k - 1], f[i][k]) + f[k + 1][j];
      }
      // printf("f[%d, %d] = %d\n", i, j, f[i][j]);
      ans ^= f[i][j];
    }
  }
  printf("%d\n", ans);
  return 0;
}
