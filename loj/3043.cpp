// by Sshwy
//#define DEBUGGER
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

#ifdef DEBUGGER

#define log_prefix()                                                \
  {                                                                 \
    fprintf(stderr, "\033[37mLine %-3d [%dms]:\033[0m  ", __LINE__, \
        (int)clock() / 1000);                                       \
  }
#define ilog(...) \
  { fprintf(stderr, __VA_ARGS__); }
#define llog(...)      \
  {                    \
    log_prefix();      \
    ilog(__VA_ARGS__); \
  }
#define log(...)           \
  {                        \
    log_prefix();          \
    ilog(__VA_ARGS__);     \
    fprintf(stderr, "\n"); \
  }
#define red(...)       \
  {                    \
    log_prefix();      \
    ilog("\033[31m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define green(...)     \
  {                    \
    log_prefix();      \
    ilog("\033[32m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define blue(...)      \
  {                    \
    log_prefix();      \
    ilog("\033[34m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }

#else

#define log_prefix() ;
#define ilog(...) ;
#define llog(...) ;
#define log(...) ;
#define red(...) ;
#define green(...) ;
#define blue(...) ;

#endif

namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const long long N = 2e5 + 5, SZ = N << 2, P = 998244353, I2 = (P + 1) / 2;
//因为有可能在叶子结点pushup，所以空间开2倍（写法问题）
int n, m;
inline void add(int &x, int y) { x = (x + y) % P; }

int f[SZ]; //结点tag值为1的概率
int g[SZ]; //祖先路径上存在tag值为1的结点的概率
int pls[SZ], mul[SZ], s[SZ];

void node_add(int u, int v) {
  add(g[u], v);
  add(pls[u], v);
}
void node_mul(int u, long long v) {
  g[u] = g[u] * v % P;
  mul[u] = mul[u] * v % P;
  pls[u] = pls[u] * v % P;
}
void pushdown(int u, int l, int r) {
  if (mul[u] != 1) node_mul(u << 1, mul[u]), node_mul(u << 1 | 1, mul[u]), mul[u] = 1;
  if (pls[u]) node_add(u << 1, pls[u]), node_add(u << 1 | 1, pls[u]), pls[u] = 0;
}
void pushup(int u) { s[u] = ((s[u << 1] + s[u << 1 | 1]) % P + f[u]) % P; }
void modify(int L, int R, int u = 1, int l = 1, int r = n) {
  if (R < l || r < L) { // grey
    f[u] = (f[u] + g[u]) * I2 % P;
    pushup(u);
    return;
  }
  if (L <= l && r <= R) { // black
    f[u] = (f[u] + 1) * I2 % P;
    node_add(u, 1), node_mul(u, I2);
    //子树的点，在这次操作后，祖先的tag中都一定会有一个1。
    //因此z in T[u], g[z]=(g[z]+1)*I2%P;
    pushup(u);
    return;
  }
  // red
  int mid = (l + r) >> 1;
  pushdown(u, l, r);
  f[u] = f[u] * I2 % P;
  g[u] = g[u] * I2 % P;
  modify(L, R, u << 1, l, mid), modify(L, R, u << 1 | 1, mid + 1, r);
  pushup(u);
}

int main() {
  fill(mul, mul + SZ, 1);
  scanf("%d%d", &n, &m);
  int coef = 1;
  FOR(i, 1, m) {
    int op, l, r;
    scanf("%d", &op);
    if (op == 1) {
      scanf("%d%d", &l, &r);
      modify(l, r);
      coef = coef * 2ll % P;
    } else {
      int ans = s[1];
      ans = ans * 1ll * coef % P;
      printf("%d\n", ans);
    }
  }
  return 0;
}
