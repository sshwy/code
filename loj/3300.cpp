// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

const int M = 1005;
typedef long long LL;

LL n, z, p, m, a[M], s[M][M];

LL pw(LL a, LL m) {
  LL res = 1;
  while (m) m & 1 ? res = 1ll * res * a % p : 0, a = 1ll * a * a % p, m >>= 1;
  return res;
}
int main() {
  scanf("%lld%lld%lld%lld", &n, &z, &p, &m);
  FOR(i, 0, m) scanf("%lld", a + i);
  s[0][0] = 1;
  FOR(i, 1, m) {
    s[i][0] = 0;
    FOR(j, 1, i) s[i][j] = (s[i - 1][j - 1] + s[i - 1][j] * j) % p;
  }
  LL ans = 0;
  FOR(j, 0, m) {
    LL coef = 1, sum = 0;
    FOR(i, 0, j) {
      sum = (sum + coef * s[j][i] % p * pw(z + 1, n - i) % p) % p;
      coef = coef * (n - i) % p;
      coef = coef * z % p;
    }
    ans = (ans + sum * a[j]) % p;
  }
  printf("%lld\n", ans);
  return 0;
}
