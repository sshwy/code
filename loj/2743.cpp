// by Sshwy
//#define DEBUGGER
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

#ifdef DEBUGGER

#define log_prefix()                                                \
  {                                                                 \
    fprintf(stderr, "\033[37mLine %-3d [%dms]:\033[0m  ", __LINE__, \
        (int)clock() / 1000);                                       \
  }
#define ilog(...) \
  { fprintf(stderr, __VA_ARGS__); }
#define llog(...)      \
  {                    \
    log_prefix();      \
    ilog(__VA_ARGS__); \
  }
#define log(...)           \
  {                        \
    log_prefix();          \
    ilog(__VA_ARGS__);     \
    fprintf(stderr, "\n"); \
  }
#define red(...)       \
  {                    \
    log_prefix();      \
    ilog("\033[31m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define green(...)     \
  {                    \
    log_prefix();      \
    ilog("\033[32m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define blue(...)      \
  {                    \
    log_prefix();      \
    ilog("\033[34m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }

#else

#define log_prefix() ;
#define ilog(...) ;
#define llog(...) ;
#define log(...) ;
#define red(...) ;
#define green(...) ;
#define blue(...) ;

#endif

namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 105, W = 2005, P = 1e9 + 7;
int n, L, a[N];
long long F[N][W][3], G[N][W][3];
void add(long long &x, long long y) { x = (x + y) % P; }
int _(int x) { return x > L ? L + 1 : x; }
int main() {
  scanf("%d%d", &n, &L);
  FOR(i, 1, n) scanf("%d", &a[i]);
  if (n == 1) return puts("1"), 0;
  sort(a + 1, a + n + 1);
  ROF(i, n, 1) a[i] -= a[i - 1];
  // FOR(i,1,n)printf("%d%c",a[i]," \n"[i==n]);

  auto *f = F;
  auto *g = G;

  f[0][0][0] = 1;
  ROF(i, n, 1) {
    FOR(j, 0, n) FOR(k, 0, L) FOR(h, 0, 2) g[j][k][h] = 0;
    FOR(j, 0, n) {
      FOR(k, 0, L) { // f[j][k]
        FOR(h, 0, 2) {
          if (f[j][k][h] == 0) continue;
          //作为一个新的非边界段插入
          add(g[j + 1][_(k + (2 * (j + 1) - h) * a[i])][h], f[j][k][h] * (j + 1 - h));
          //作为一个新的边界段插入
          if (h < 2)
            add(g[j + 1][_(k + (2 * j + 1 - h) * a[i])][h + 1], f[j][k][h] * (2 - h));
          //合并两个段
          if (j > 1)
            add(g[j - 1][_(k + (2 * (j - 1) - h) * a[i])][h], f[j][k][h] * (j - 1));
          //附在一个段上
          if (j > 0)
            add(g[j][_(k + (2 * j - h) * a[i])][h], f[j][k][h] * (2 * j - h));
          //附在一个段上并钦定为边界
          if (h < 2 && j > 0)
            add(g[j][_(k + (2 * j - 1 - h) * a[i])][h + 1], f[j][k][h] * (2 - h));
        }
      }
    }
    swap(f, g);
    // printf("i %d %d\n",i,a[i]);
    // FOR(j,0,n)FOR(k,0,L)FOR(h,0,2)if(f[j][k][h])printf("f %d %d %d:
    // %lld\n",j,k,h,f[j][k][h]);
  }
  long long ans = 0;
  FOR(i, 0, L) add(ans, f[1][i][2]);
  printf("%lld\n", ans);
  return 0;
}
