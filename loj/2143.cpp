// by Sshwy
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/

int n, P, k, r;

typedef vector<long long> V;

V operator*(V a, V b) {
  V res(a.size());
  FOR(i, 0, (int)a.size() - 1) {
    FOR(j, 0, (int)b.size() - 1) {
      int x = (i + j) % k;
      res[x] = (res[x] + a[i] * 1ll * b[j] % P) % P;
    }
  }
  return res;
}
V pw(V f, long long m) {
  if (m == 1) return f;
  V res = f;
  --m;
  while (m) { m & 1 ? res = res * f, 0 : 0, f = f * f, m >>= 1; }
  return res;
}

int main() {
  scanf("%d%d%d%d", &n, &P, &k, &r);

  V f(k);

  if (k == 1)
    f[0] = 2;
  else
    f[0] = 1, f[1] = 1;

  f = pw(f, n * 1ll * k);

  printf("%lld\n", f[r]);

  return 0;
}
