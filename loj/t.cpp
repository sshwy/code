#include <bits/stdc++.h>
#define For(i, l, r) for (register int i = (l), i##end = (int)(r); i <= i##end; ++i)
#define Fordown(i, r, l) \
  for (register int i = (r), i##end = (int)(l); i >= i##end; --i)
#define Set(a, v) memset(a, v, sizeof(a))
using namespace std;

inline bool chkmin(int &a, int b) { return b < a ? a = b, 1 : 0; }
inline bool chkmax(int &a, int b) { return b > a ? a = b, 1 : 0; }

inline int read() {
  int x = 0, fh = 1;
  char ch = getchar();
  for (; !isdigit(ch); ch = getchar())
    if (ch == '-') fh = -1;
  for (; isdigit(ch); ch = getchar()) x = (x << 1) + (x << 3) + (ch ^ 48);
  return x * fh;
}

void File() {
#ifdef zjp_shadow
  freopen("2540.in", "r", stdin);
  freopen("2540.out", "w", stdout);
#endif
}

const int N = 20;
int n, m, Con[N];

typedef long long ll;
const int Mod = 998244353;

ll fpm(ll x, int power) {
  ll inv = 1;
  for (; power; power >>= 1, (x *= x) %= Mod)
    if (power & 1) (inv *= x) %= Mod;
  return inv;
}

ll fac[N + 5], ifac[N + 5];
void Init(int maxn) {
  fac[0] = ifac[0] = 1;
  For(i, 1, maxn) fac[i] = fac[i - 1] * i % Mod;
  ifac[maxn] = fpm(fac[maxn], Mod - 2);
  Fordown(i, maxn - 1, 1) ifac[i] = ifac[i + 1] * (i + 1) % Mod;
}

int dp[1 << N], bit[1 << N], MaxSize[1 << N];
;

inline int A(int n, int m) {
  if (m > n || n < 0 || m < 0) return 0;
  return fac[n] * ifac[n - m] % Mod;
}

int main() {
  File();

  n = read();
  m = read();
  Init(n);
  For(i, 1, m) {
    int u = read() - 1, v = read() - 1;
    Con[u] |= (1 << v);
    Con[v] |= (1 << u);
  }

  For(i, 0, n - 1) Con[i] |= (1 << i);

  dp[0] = 1;
  int maxsta = (1 << n) - 1;
  For(i, 0, maxsta) bit[i] = bit[i >> 1] + (i & 1);

  For(i, 0, maxsta) if (dp[i]) {
    For(j, 0, n - 1) if (!((1 << j) & i)) {
      register int Next = i | Con[j];
      if (chkmax(MaxSize[Next], MaxSize[i] + 1)) dp[Next] = 0;
      if (MaxSize[Next] == MaxSize[i] + 1)
        (dp[Next] += 1ll * dp[i] * A(n - bit[i] - 1, bit[Con[j] ^ (Con[j] & i)] - 1) %
                     Mod) %= Mod;
    }
  }

  printf("%lld\n", 1ll * dp[maxsta] * ifac[n] % Mod);
  return 0;
}
