// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

const int N = 3e5 + 5, P = 998244353, SZ = N * 20;

int pw(int a, int m) {
  int res = 1;
  while (m) m & 1 ? res = 1ll * res * a % P : 0, a = 1ll * a * a % P, m >>= 1;
  return res;
}

int n;

int val[SZ], tmul[SZ], ls[SZ], rs[SZ];
int tot;

struct SegmentTree {
  int root;
  int newNode() {
    return ++tot, tmul[tot] = 1, val[tot] = ls[tot] = rs[tot] = 0, tot;
  }
  void pushup(int u) { val[u] = (val[ls[u]] + val[rs[u]]) % P; }
  void nodeMul(int u, int v) {
    val[u] = 1ll * val[u] * v % P;
    tmul[u] = 1ll * tmul[u] * v % P;
  }
  void pushdown(int u) {
    if (tmul[u] != 1) {
      if (ls[u]) nodeMul(ls[u], tmul[u]);
      if (rs[u]) nodeMul(rs[u], tmul[u]);
      tmul[u] = 1;
    }
  }
  void _set(int pos, int v, int &u, int l, int r) {
    if (!u) u = newNode();
    if (l == r) return val[u] = v, void();
    int mid = (l + r) >> 1;
    pushdown(u);
    if (pos <= mid)
      _set(pos, v, ls[u], l, mid);
    else
      _set(pos, v, rs[u], mid + 1, r);
    pushup(u);
  }
  void set(int pos, int v) { _set(pos, v, root, 1, n); }
  void inherit(SegmentTree s) { root = s.root; }
  int _merge(int x, int y, int pr, int sx, int sy) {
    if (!x && !y) return 0;
    if (!x) {
      nodeMul(y, sx);
      return y;
    }
    if (!y) {
      nodeMul(x, sy);
      return x;
    }
    pushdown(x), pushdown(y);
    int lx = val[ls[x]], rx = val[rs[x]];
    int ly = val[ls[y]], ry = val[rs[y]];
    ls[x] = _merge(ls[x], ls[y], pr, (sx + 1ll * rx * (P + 1 - pr)) % P,
        (sy + 1ll * ry * (P + 1 - pr)) % P);
    rs[x] =
        _merge(rs[x], rs[y], pr, (sx + 1ll * lx * pr) % P, (sy + 1ll * ly * pr) % P);
    pushup(x);
    return x;
  }
  void merge(SegmentTree s, int pr) { root = _merge(root, s.root, pr, 0, 0); }
  int _get(int pos, int u, int l, int r) {
    if (!u) return 0;
    if (l == r) return val[u];
    int mid = (l + r) >> 1;
    pushdown(u);
    if (pos <= mid)
      return _get(pos, ls[u], l, mid);
    else
      return _get(pos, rs[u], mid + 1, r);
  }
  int get(int pos) { return _get(pos, root, 1, n); }
} s[N];

int fa[N], a[N], p[N], ie4;
bool hasSon[N];

vector<int> g[N], sortedA;

void dfs(int u) {
  if (!hasSon[u]) {
    s[u].set(
        lower_bound(sortedA.begin(), sortedA.end(), a[u]) - sortedA.begin() + 1, 1);
    return;
  }
  for (int v : g[u]) dfs(v);
  s[u].inherit(s[g[u][0]]);
  if (g[u].size() > 1) s[u].merge(s[g[u][1]], p[u]);
}

int main() {
  ie4 = pw(10000, P - 2);
  scanf("%d", &n);
  FOR(i, 1, n) {
    scanf("%d", &fa[i]);
    g[fa[i]].pb(i);
    hasSon[fa[i]] = true;
  }
  FOR(i, 1, n) {
    if (hasSon[i])
      scanf("%d", p + i), p[i] = 1ll * p[i] * ie4 % P;
    else
      scanf("%d", a + i), sortedA.pb(a[i]);
  }
  sort(sortedA.begin(), sortedA.end());
  dfs(1);
  int ans = 0;
  for (unsigned i = 0; i < sortedA.size(); i++) {
    int pr = s[1].get(i + 1);
    // printf("val %d pr %d\n", sortedA[i], pr);
    int t = 1ll * pr * pr % P * (i + 1) % P * sortedA[i] % P;
    ans = (ans + t) % P;
  }
  printf("%d\n", ans);
  return 0;
}
