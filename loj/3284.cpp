#include <bits/stdc++.h>
#define FOR(a, b, c) for (int a = b; a <= (int)(c); ++a)
#define ROF(a, b, c) for (int a = b; a >= (int)(c); --a)
#define pb push_back
using namespace std;
const int N = 7505;
int n, m;
int vis[N], c[N][N];
int g[N], fac[N];
int pw(int a, int m, int p) {
  int res = 1;
  while (m) m & 1 ? res = 1ll * res * a % p : 0, a = 1ll * a * a % p, m >>= 1;
  return res;
}
signed main() {
  cin >> n >> m;
  int ans = 1;
  c[0][0] = 1, fac[0] = 1;
  FOR(i, 1, n) {
    c[i][0] = 1;
    fac[i] = fac[i - 1] * 1ll * i % (m - 1);
    FOR(j, 1, i) c[i][j] = (c[i - 1][j - 1] + c[i - 1][j]) % (m - 1);
  }
  FOR(i, 2, n) {
    if (vis[i]) continue;
    int sum = 0;
    for (int s = i; s <= n; s += i) vis[s] = 1;
    for (int s = i; s <= n; s *= i) {
      auto calc = [](int s) {
        int lim = n / s;
        g[0] = -1;
        FOR(i, 1, lim) {
          g[i] = 0;
          FOR(j, 1, i) {
            int t = fac[s * j - 1] * 1ll * c[s * i - 1][s * j - 1] % (m - 1) *
                    g[i - j] % (m - 1);
            // printf("t %d\n",t);
            g[i] = (g[i] - t) % (m - 1);
          }
          // printf("g %d %d\n",i,g[i]);
        }
        int res = 0;
        FOR(i, 1, lim) {
          int t = c[n][s * i] * 1ll * fac[n - s * i] % (m - 1) * g[i] % (m - 1);
          res = (res + t) % (m - 1);
        }
        res = (res + m - 1) % (m - 1);
        // printf("calc %lld : %lld\n",s,res);
        return res;
      };
      sum = (sum + calc(s)) % (m - 1);
    }
    // printf("i %d sum %d\n",i,sum);
    ans = (ans * 1ll * pw(i, sum, m)) % m;
  }
  cout << ans << endl;
  return 0;
}