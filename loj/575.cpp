#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
namespace NTT {
  const int SZ = (1 << 20) + 5, P = 998244353;
  int pw(int a, int m) {
    assert(m > 0);
    a = (a + P) % P;
    int res = 1;
    while (m) m & 1 ? res = 1ll * res * a % P : 0, a = 1ll * a * a % P, m >>= 1;
    return res;
  }
  int tr[SZ];
  void dft(int *f, int len, int tag) {
    assert((len & -len) == len);
    FOR(i, 0, len - 1) if (i < tr[i]) swap(f[i], f[tr[i]]);
    FOR(i, 0, len - 1) f[i] = (f[i] + P) % P;
    for (int j = 1; j < len; j <<= 1) {
      int wn = pw(3, (P - 1) / (j << 1) * tag + P - 1); // BUG#1:j<<1写成1<<j
      for (int i = 0; i < len; i += j << 1) {
        int w = 1, u, v;
        for (int k = i; k < i + j; k++, w = 1ll * w * wn % P) {
          u = f[k], v = 1ll * w * f[k + j] % P;
          f[k] = u + v, f[k + j] = u - v;
          f[k] -= f[k] >= P ? P : 0,
              f[k + j] +=
              f[k + j] < 0 ? P : 0; // BUG#3:这种写法必须保证初始时f[]都是非负的
        }
      }
    }
    if (tag == -1) {
      int ilen = pw(len, P - 2);
      FOR(i, 0, len - 1) f[i] = 1ll * f[i] * ilen % P;
    }
  }
  int init(int l) {
    int d = 0, len = 1;
    while (len < (l << 1)) len <<= 1, ++d;
    FOR(i, 1, len - 1) tr[i] = (tr[i >> 1] >> 1) | ((i & 1) << (d - 1));
    return len;
  }
} // namespace NTT
typedef int dft[NTT::SZ];
int pw(int a, int m, int p) {
  int res = 1;
  while (m) m & 1 ? res = 1ll * res * a % p : 0, a = 1ll * a * a % p, m >>= 1;
  return res;
}
const int N = 1e5 + 5, P = 998244353;

char s[N];
int cnt[N], n;
int f[N], fac[N], fnv[N];

int sig(int k) { return 1 - 2 * (abs(k) & 1); } //(-1)^k or (-1)^(-k)
int F(int j) { return (s[j] == '>') * sig(cnt[j]) * f[j]; }
int G(int x) { return fnv[x]; }

int a[N], b[N], c[N];
dft A, B, C;

void solve(int l, int r) {
  if (l == r) return l == 0 ? f[0] = 1 : 0, void();
  int mid = (l + r) >> 1;
  solve(l, mid);
  int len = NTT::init((r - l));
  FOR(i, 0, mid - l) A[i] = F(i + l);
  FOR(i, mid - l + 1, len - 1)
  A[i] = 0; // BUG#2:清空的时侯要把高位也清空。不然会循环卷积
  FOR(i, 0, r - l - 1) B[i] = G(i + 1);

  NTT::dft(A, len, 1);
  NTT::dft(B, len, 1);
  FOR(i, 0, len - 1) C[i] = 1ll * A[i] * B[i] % P;
  NTT::dft(C, len, -1);
  FOR(i, mid + 1, r) f[i] = (f[i] + C[i - 1 - l] * sig(cnt[i - 1])) % P;

  solve(mid + 1, r);
}
signed main() {
  scanf("%s", s + 1);
  int n = strlen(s + 1) + 1;
  s[0] = '>';
  fac[0] = 1;
  FOR(i, 1, n) fac[i] = 1ll * fac[i - 1] * i % P;
  fnv[n] = pw(fac[n], P - 2, P);
  ROF(i, n, 1) fnv[i - 1] = 1ll * fnv[i] * i % P;
  FOR(i, 1, n - 1) cnt[i] = cnt[i - 1] + (s[i] == '>');
  solve(0, n);
  int ans = 1ll * fac[n] * f[n] % P;
  ans = (ans + P) % P;
  printf("%d\n", ans);
  return 0;
}
