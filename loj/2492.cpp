#include <bits/stdc++.h>
#define FOR(a, b, c) for (int a = b; a <= int(c); ++a)
#define ROF(a, b, c) for (int a = b; a >= int(c); --a)
#define pb push_back
using namespace std;
const int N = 1e5 + 5;

void add(long long f[N], int pos, long long v) {
  for (int i = pos; i < N; i += i & -i) f[i] += v;
}
long long pre(long long f[N], int pos) {
  int res = 0;
  for (int i = pos; i > 0; i -= i & -i) res += f[i];
  return res;
}
int n, a[N];

namespace d1 {
  long long c[N];
  set<int> s;
  long long calc(set<int>::iterator p) {
    auto R = p;
    ++R;
    auto L = s.end();
    if (p != s.begin()) L = p, --L;

    int l = L == s.end() ? (*p - 1) : (*p - *L - 1);
    int r = R == s.end() ? (n - *p) : (*R - *p - 1);
    return (l + 1) * 1ll * (r + 1);
  }
  void insert(int pos) { // insert a 1
    assert(s.find(pos) == s.end());

    auto R = s.lower_bound(pos);
    auto L = s.end();
    if (R != s.begin()) L = R, --L;

    if (R != s.end()) add(c, *R, -calc(R));
    if (L != s.end()) add(c, *L, -calc(L));
    auto pr = s.insert(pos);
    auto p = pr.first;
    if (R != s.end()) add(c, *R, calc(R));
    if (L != s.end()) add(c, *L, calc(L));
    add(c, *p, calc(p));
  }
  void remove(int pos) {
    assert(s.find(pos) != s.end());
    auto p = s.find(pos);

    auto R = p;
    ++R;
    auto L = s.end();
    if (p != s.begin()) L = p, --L;

    add(c, *p, -calc(p));
    if (R != s.end()) add(c, *R, -calc(R));
    if (L != s.end()) add(c, *L, -calc(L));
    s.erase(p);
    if (R != s.end()) add(c, *R, calc(R));
    if (L != s.end()) add(c, *L, calc(L));
  }
  long long qry(int l, int r) {
    auto L = s.lower_bound(l);
    if (L == s.end() || *L > r) return 0;
    auto R = s.upper_bound(r);
    --R;
    if (*L == *R) return (*L - l + 1) * 1ll * (r - *L + 1);
    assert(*L < *R);
    ++L;
    if (*L == *R) {
      --L;
      return (*L - l + 1) * 1ll * (*R - *L) + (*R - *L) * 1ll * (r - *R + 1);
    }
    --L;
    auto L1 = L, R1 = R;
    ++L1, --R1;
    assert(*L1 <= *R1);
    return pre(c, *R1) - pre(c, *L1 - 1) + (*L - l + 1) * 1ll * (*L1 - *L) +
           (r - *R + 1) * 1ll * (*R - *R1);
  }
} // namespace d1

namespace d3 {
  long long c[N];
  set<int> s;
  long long calc(int x, int y) {
    int x0 = x / 2, x1 = x - x0;
    int y0 = y / 2, y1 = y - y0;
    return (x0 + 1) * 1ll * y1 + x1 * 1ll * (y0 + 1);
  }
  long long calc(set<int>::iterator p) {
    auto R = p;
    ++R;
    auto L = s.end();
    if (p != s.begin()) L = p, --L;

    int l = L == s.end() ? (*p - 1) : (*p - *L - 1);
    int r = R == s.end() ? (n - *p) : (*R - *p - 1);
    return calc(l, r);
  }
  void insert(int pos) { // insert a 1
    assert(s.find(pos) == s.end());

    auto R = s.lower_bound(pos);
    auto L = s.end();
    if (R != s.begin()) L = R, --L;

    if (R != s.end()) add(c, *R, -calc(R));
    if (L != s.end()) add(c, *L, -calc(L));
    auto pr = s.insert(pos);
    auto p = pr.first;
    if (R != s.end()) add(c, *R, calc(R));
    if (L != s.end()) add(c, *L, calc(L));
    add(c, *p, calc(p));
  }
  void remove(int pos) {
    assert(s.find(pos) != s.end());
    auto p = s.find(pos);

    auto R = p;
    ++R;
    auto L = s.end();
    if (p != s.begin()) L = p, --L;

    add(c, *p, -calc(p));
    if (R != s.end()) add(c, *R, -calc(R));
    if (L != s.end()) add(c, *L, -calc(L));
    s.erase(p);
    if (R != s.end()) add(c, *R, calc(R));
    if (L != s.end()) add(c, *L, calc(L));
  }
  long long qry(int l, int r) {
    auto L = s.lower_bound(l);
    if (L == s.end() || *L > r) return 0;
    auto R = s.upper_bound(r);
    --R;
    if (*L == *R) return calc(*L - l, r - *L);
    assert(*L < *R);
    ++L;
    if (*L == *R) {
      --L;
      return calc(*L - l, *R - *L - 1) + calc(*R - *L - 1, r - *R);
    }
    --L;
    auto L1 = L, R1 = R;
    ++L1, --R1;
    assert(*L1 <= *R1);
    return pre(c, *R1) - pre(c, *L1 - 1) + calc(*L - l, *L1 - *L - 1) +
           calc(r - *R, *R - *R1 - 1);
  }
} // namespace d3

namespace d2 {
  long long c[N];
  set<int> s;
  long long calc(int x) {
    int x0 = x / 2, x1 = x - x0;
    return x0 * (x0 + 1ll) / 2 + x1 * (x1 + 1ll) / 2;
  }
  long long calc(set<int>::iterator p) {
    auto R = p;
    ++R;

    int r = R == s.end() ? (n - *p) : (*R - *p - 1);
    return calc(r);
  }
  void insert(int pos) { // insert a 1
    assert(s.find(pos) == s.end());

    auto R = s.lower_bound(pos);
    auto L = s.end();
    if (R != s.begin()) L = R, --L;

    if (L != s.end()) add(c, *L, -calc(L));
    auto pr = s.insert(pos);
    auto p = pr.first;
    if (L != s.end()) add(c, *L, calc(L));
    add(c, *p, calc(p));
  }
  void remove(int pos) {
    assert(s.find(pos) != s.end());
    auto p = s.find(pos);

    auto R = p;
    ++R;
    auto L = s.end();
    if (p != s.begin()) L = p, --L;

    add(c, *p, -calc(p));
    if (L != s.end()) add(c, *L, -calc(L));
    s.erase(p);
    if (L != s.end()) add(c, *L, calc(L));
  }
  long long qry(int l, int r) {
    auto L = s.lower_bound(l);
    if (L == s.end() || *L > r) return calc(r - l + 1);
    auto R = s.upper_bound(r);
    --R;
    if (*L == *R) return calc(*L - l) + calc(r - *L);
    assert(*L < *R);
    auto R1 = R;
    --R1;
    assert(*L <= *R1);
    return pre(c, *R1) - pre(c, *L - 1) + calc(*L - l) + calc(r - *R);
  }
} // namespace d2

namespace d4 {
  long long c[N];
  set<int> s;
  void insert(int pos) { // insert a 1
    assert(s.find(pos) == s.end());

    auto pr = s.insert(pos);
    auto p = pr.first;
    add(c, *p, 1);
  }
  void remove(int pos) {
    assert(s.find(pos) != s.end());
    auto p = s.find(pos);

    add(c, *p, -1);
    s.erase(p);
  }
  long long qry(int l, int r) {
    auto L = s.lower_bound(l);
    if (L == s.end() || *L > r) return 0;
    auto R = s.upper_bound(r);
    --R;
    return pre(c, *R) - pre(c, *L - 1);
  }
} // namespace d4

namespace d5 {
  long long c[N];
  set<int> s;
  long long calc(set<int>::iterator p) {
    auto R = p;
    ++R;
    auto L = s.end();
    if (p != s.begin()) L = p, --L;

    int l = L == s.end() ? (*p - 1) : (*p - *L - 1);
    int r = R == s.end() ? (n - *p) : (*R - *p - 1);
    return !!l + !!r;
  }
  void insert(int pos) { // insert a 1
    assert(s.find(pos) == s.end());

    auto R = s.lower_bound(pos);
    auto L = s.end();
    if (R != s.begin()) L = R, --L;

    if (R != s.end()) add(c, *R, -calc(R));
    if (L != s.end()) add(c, *L, -calc(L));
    auto pr = s.insert(pos);
    auto p = pr.first;
    if (R != s.end()) add(c, *R, calc(R));
    if (L != s.end()) add(c, *L, calc(L));
    add(c, *p, calc(p));
  }
  void remove(int pos) {
    assert(s.find(pos) != s.end());
    auto p = s.find(pos);

    auto R = p;
    ++R;
    auto L = s.end();
    if (p != s.begin()) L = p, --L;

    add(c, *p, -calc(p));
    if (R != s.end()) add(c, *R, -calc(R));
    if (L != s.end()) add(c, *L, -calc(L));
    s.erase(p);
    if (R != s.end()) add(c, *R, calc(R));
    if (L != s.end()) add(c, *L, calc(L));
  }
  long long qry(int l, int r) {
    auto L = s.lower_bound(l);
    if (L == s.end() || *L > r) return 0;
    auto R = s.upper_bound(r);
    --R;
    if (*L == *R) return !!(*L - l) + !!(r - *L);
    assert(*L < *R);
    ++L;
    if (*L == *R) {
      --L;
      return !!(*L - l) + !!(*R - *L - 1) + !!(*R - *L - 1) + !!(r - *R);
    }
    --L;
    auto L1 = L, R1 = R;
    ++L1, --R1;
    assert(*L1 <= *R1);
    return pre(c, *R1) - pre(c, *L1 - 1) + !!(*L - l) + !!(*L1 - *L - 1) +
           !!(r - *R) + !!(*R - *R1 - 1);
  }
} // namespace d5

int main() {
  scanf("%d", &n);
  FOR(i, 1, n) scanf("%d", &a[i]);
  FOR(i, 1, n) {
    if (a[i] == 1) {
      d1::insert(i);
      d4::insert(i);
      d5::insert(i);
    } else {
      d3::insert(i);
      d2::insert(i);
    }
  }
  int m;
  scanf("%d", &m);
  FOR(i, 1, m) {
    int typ, x, y;
    scanf("%d", &typ);
    if (typ == 1) {
      scanf("%d", &y);
      if (a[y] == 0) {
        d1::insert(y);
        d4::insert(y);
        d5::insert(y);
        d3::remove(y);
        d2::remove(y);
      } else {
        d1::remove(y);
        d4::remove(y);
        d5::remove(y);
        d3::insert(y);
        d2::insert(y);
      }
      a[y] ^= 1;
    } else {
      scanf("%d%d", &x, &y);
      long long ans = (y - x + 1ll) * (y - x + 2ll) / 2;
      // printf("ans %d\n",ans);
      ans -= d1::qry(x, y);
      // printf("ans %d\n",ans);
      ans -= d2::qry(x, y);
      // printf("ans %d\n",ans);
      ans -= d3::qry(x, y);
      // printf("ans %d\n",ans);
      ans += d4::qry(x, y);
      // printf("ans %d\n",ans);
      ans += d5::qry(x, y);
      printf("%lld\n", ans);
    }
  }
  return 0;
}
/*
1. 有偶数个1
2. 有奇数个1，则至少3个1和至少两个0

不合法：
1. 只有1个1
2. 有奇数个1，没有0
3. 有奇数个1，1个0
4. 减去只有一个1，没有0
5. 减去只有一个1，1个0
*/