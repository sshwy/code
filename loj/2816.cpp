// by Sshwy
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA
const int N = 2e5 + 5, M = 2e5 + 5;
int n, m, q;

int fa[N + M];
int get(int u) { return fa[u] == u ? u : fa[u] = get(fa[u]); }
int main() {
  scanf("%d%d%d", &n, &m, &q);
  FOR(i, 1, n + m) fa[i] = i;
  FOR(i, 1, q) {
    int x, y;
    scanf("%d%d", &x, &y);
    fa[get(x)] = get(y + n);
  }
  int ans = 0;
  FOR(i, 1, n + m) if (get(i) == i)++ ans;
  printf("%d\n", ans - 1);
  return 0;
}
