// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

const int N = 15, P = 1e9 + 7, I2 = (P + 1) / 2, I4 = I2 / 2;

int n, nn, m;

struct Atom {
  int typ, a1, b1, a2, b2;
};
vector<Atom> E;

void add(int &x, long long y) { x = (x + y) % P; }
unordered_map<int, int> f;

int F(int L, int R) {
  if (L == 0 && R == 0) return 1;
  if (f.find(L << n | R) != f.end()) return f[L << n | R];
  int fx = 0;
  for (auto e : E) {
    if (e.typ == 1) {
      if ((R & e.b1) && (e.a1 & L & -L)) {
        add(fx, 1ll * F(L ^ e.a1, R ^ e.b1) * I2 % P);
      }
    } else {
      int coef = e.typ == 2 ? I4 : P - I4;
      if ((R & e.b1) && (R & e.b2) && (e.a1 & L & -L) && (e.a2 & L)) {
        add(fx, 1ll * F(L ^ e.a1 ^ e.a2, R ^ e.b1 ^ e.b2) * coef % P);
      }
    }
  }
  f[L << n | R] = fx;
  return fx;
}

int main() {
  scanf("%d%d", &n, &m);
  nn = 1 << n;
  FOR(i, 1, m) {
    int t, a1, b1, a2, b2;
    scanf("%d", &t);
    if (t == 0) {
      scanf("%d%d", &a1, &b1);
      --a1, --b1;
      E.pb({1, 1 << a1, 1 << b1, -1, -1});
    } else {
      scanf("%d%d%d%d", &a1, &b1, &a2, &b2);
      --a1, --b1, --a2, --b2;
      E.pb({1, 1 << a1, 1 << b1, -1, -1});
      E.pb({1, 1 << a2, 1 << b2, -1, -1});
      if (a1 != a2 && b1 != b2) {
        if (a1 > a2) swap(a1, a2), swap(b1, b2);
        if (t == 1)
          E.pb({2, 1 << a1, 1 << b1, 1 << a2, 1 << b2});
        else
          E.pb({3, 1 << a1, 1 << b1, 1 << a2, 1 << b2});
      }
    }
  }
  int ans = 1ll * F(nn - 1, nn - 1) * nn % P;
  printf("%d\n", ans);
  return 0;
}
