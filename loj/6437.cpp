// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

typedef double db;
const int N = 205;

int n, m;

const db eps = 1e-5;

struct Vec {
  db x, y;
  Vec() { x = 0, y = 0; }
  Vec(db _x, db _y) { x = _x, y = _y; }
  void read() {
    double _x, _y;
    scanf("%lf%lf", &_x, &_y);
    x = _x, y = _y;
  }
  db dot(const Vec v) const { return x * v.x + y * v.y; }
  db length() { return sqrt(x * x + y * y); }
  Vec operator-(Vec v) { return Vec(x - v.x, y - v.y); }
  Vec operator+(Vec v) { return Vec(x + v.x, y + v.y); }
  Vec operator*(db v) { return Vec(x * v, y * v); }
  bool operator==(Vec v) { return abs(x - v.x) < eps && abs(y - v.y) < eps; }
  Vec dir() { return *this * (1 / length()); }
};
typedef Vec Point;

Point enemy[N], polygon[N], O;

struct Line {
  Point p1, p2;
  Line(Point _p1, Point _p2) { p1 = _p1, p2 = _p2; }
};

Point projection(Point p, Line L) {
  // a dot b  / |b| = |a| * cos theta
  Vec vL = L.p2 - L.p1;
  return vL.dir() * ((p - L.p1).dot(vL) / vL.length()) + L.p1;
}

double getAngle(Point p1, Point p2) {
  double delta = atan2(p2.y, p2.x) - atan2(p1.y, p1.x);
  if (delta < -M_PI) delta += 2 * M_PI;
  if (delta > M_PI) delta -= 2 * M_PI;
  return delta;
}
double calc(db r) {
  double tot_e = 0;
  FOR(i, 0, m - 1) {
    Point p1 = polygon[i], p2 = polygon[(i + 1) % m];
    Line L(p1, p2);

    if (p1 == O || p2 == O || projection(O, L) == O) continue;
    db ang = getAngle(p1, p2);
    db percent = 1;

    Point q = projection(O, L);

    db sq_q = (q.x * q.x + q.y * q.y);
    if (sq_q <= r * r) {
      db shift = sqrt(r * r - sq_q);
      Vec v = (L.p1 - L.p2).dir();
      Point q1 = q + v * shift, q2 = q - v * shift;
      if ((p1 - p2).dir() == v) swap(p1, p2);
      if ((q1 - q2).dir() == v) swap(q1, q2);
      Point pl = (p1 - q1).dir() == v ? p1 : q1;
      Point pr = (q2 - p2).dir() == v ? p2 : q2;
      if ((pr - pl).dir() == v) { percent = 1 - abs(getAngle(pl, pr)) / abs(ang); }
    }

    tot_e += percent * ang / (2 * M_PI);
  }
  return tot_e;
}

int main() {
  scanf("%d%d", &n, &m);
  FOR(i, 1, n) enemy[i].read();
  FOR(i, 0, m - 1) polygon[i].read();
  double ans = 0;
  bool useO = true;
  FOR(i, 0, m - 1) {
    Line L(polygon[i], polygon[(i + 1) % m]);
    if (O == polygon[i]) { useO = false; }
    if (projection(O, L) == O && (L.p1 - O).dir() + (L.p2 - O).dir() == O) {
      useO = false;
    }
  }
  FOR(i, 1, n) {
    if (enemy[i] == O && !useO) {
      // do nothing
    } else {
      ans += calc((enemy[i] - O).length());
    }
  }
  printf("%.5lf\n", ans);
  return 0;
}
