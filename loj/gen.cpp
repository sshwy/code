// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

using namespace RA;

int main() {
  srand(clock() + time(0));
  int n = rnd(3, 8);
  printf("%d\n", n);
  FOR(i, 1, n - 1) printf("%d%c", rnd(1, i), " \n"[i == n - 1]);
  int q = rnd(1, 20);
  printf("%d\n", q);
  FOR(i, 1, q) {
    int t[] = {rnd(1, n - 2), rnd(1, n - 2), rnd(1, n - 2)};
    sort(t, t + 3);
    FOR(j, 0, 2) t[j] += j;
    assert(t[0] < t[1] && t[1] < t[2]);
    printf("%d %d %d\n", t[0], t[1], t[2]);
  }
  return 0;
}
