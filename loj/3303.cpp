// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

const int N = 5.5e5, SZ = N * 25;
int n, a[N], p[N], dg[N];
int q[N], ql, qr, rt[N], tot, lc[SZ], rc[SZ], cnt[SZ], val[SZ];

void pushup(int u) {
  cnt[u] = cnt[lc[u]] ^ cnt[rc[u]];
  val[u] = ((val[lc[u]] ^ val[rc[u]]) << 1) ^ (cnt[rc[u]] & 1);
  assert(cnt[0] == 0 && val[0] == 0);
}
void insert(int &u, int v, int dep = 0) {
  if (!u) u = ++tot;
  if (dep == 21) { // leaf
    cnt[u]++;
    return;
  }
  if (v >> dep & 1)
    insert(rc[u], v, dep + 1);
  else
    insert(lc[u], v, dep + 1);
  pushup(u);
  // printf("insert: u %d v %d valu %d lcu %d rcu %d\n", u, v, val[u], lc[u],
  // rc[u]);
}
void inc(int u) {
  if (!u) return;
  swap(lc[u], rc[u]);
  inc(lc[u]);
  pushup(u);
}
void merge(int &u, int v) {
  if (!v) return;
  if (!u) {
    u = v;
    return;
  }
  merge(lc[u], lc[v]);
  merge(rc[u], rc[v]);
  cnt[u] ^= cnt[v];
  val[u] ^= val[v];
}

int main() {
  scanf("%d", &n);
  FOR(i, 1, n) scanf("%d", a + i);
  FOR(i, 2, n) scanf("%d", p + i), dg[p[i]]++;
  FOR(i, 1, n) if (dg[i] == 0) q[++qr] = i;
  long long ans = 0;
  while (ql < qr) {
    int u = q[++ql];
    insert(rt[u], a[u]);
    // printf("insert rt[%d] %d\n", u, a[u]);
    ans += val[rt[u]];
    // printf("u %d val %d\n", u, val[rt[u]]);
    inc(rt[u]);
    merge(rt[p[u]], rt[u]);
    --dg[p[u]];
    if (p[u] && dg[p[u]] == 0) q[++qr] = p[u];
  }
  printf("%lld\n", ans);
  return 0;
}
