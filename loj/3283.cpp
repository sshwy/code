// by Sshwy
//#define DEBUGGER
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

#ifdef DEBUGGER

#define log_prefix()                                                \
  {                                                                 \
    fprintf(stderr, "\033[37mLine %-3d [%dms]:\033[0m  ", __LINE__, \
        (int)clock() / 1000);                                       \
  }
#define ilog(...) \
  { fprintf(stderr, __VA_ARGS__); }
#define llog(...)      \
  {                    \
    log_prefix();      \
    ilog(__VA_ARGS__); \
  }
#define log(...)           \
  {                        \
    log_prefix();          \
    ilog(__VA_ARGS__);     \
    fprintf(stderr, "\n"); \
  }
#define red(...)       \
  {                    \
    log_prefix();      \
    ilog("\033[31m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define green(...)     \
  {                    \
    log_prefix();      \
    ilog("\033[32m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define blue(...)      \
  {                    \
    log_prefix();      \
    ilog("\033[34m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }

#else

#define log_prefix() ;
#define ilog(...) ;
#define llog(...) ;
#define log(...) ;
#define red(...) ;
#define green(...) ;
#define blue(...) ;

#endif

namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 2005, P = 1e9 + 7;
int n;
char a[N][N];
int f[N][N], g[N][N], c[N], L[N][N];

int main() {
  scanf("%d", &n);
  FOR(i, 1, n) scanf("%s", a[i] + 1);

  FOR(i, 1, n) FOR(j, 1, n) c[j] += a[i][j] == '.';
  FOR(j, 1, n) c[j] += c[j - 1];
  // FOR(j,1,n)printf("%d%c",c[j]," \n"[j==n]);
  FOR(i, 1, n) FOR(j, 1, n) L[i][j] = a[i][j] == 'W' ? L[i][j - 1] : j;

  f[0][0] = 1;
  FOR(i, 0, n) g[i][0] = 1;

  auto add = [](int &x, long long y) { x = (x + y % P) % P; };
  auto pw = [](int a, int m) {
    int res = 1;
    while (m) m & 1 ? res = 1ll * res * a % P : 0, a = 1ll * a * a % P, m >>= 1;
    return res;
  };

  FOR(j, 1, n) FOR(i, 1, n) { // f[i,j]
    if (a[i][j] == '.') {
      if (i == 1) {
        add(f[i][j], pw(2, c[j] - 1));
      } else {
        if (j > 1 && a[i - 1][j] == '.') {
          int pos = j - 1;
          add(f[i][j], g[i - 1][pos] * 1ll * pw(2, c[j] - c[pos] - 2));
        }
        // printf("1 f %d %d %d\n",i,j,f[i][j]);
        if (L[i][j - 1] > 0) {
          int pos = L[i][j - 1];
          add(f[i][j], f[i][pos] * 1ll * pw(2, c[j] - c[pos]));
        }
        // printf("2 f %d %d %d\n",i,j,f[i][j]);
        FOR(k, L[i][j - 1] + 1, j - 1) {
          if (a[i - 1][k] == '.')
            add(f[i][j], g[i - 1][k - 1] * 1ll * pw(2, c[j] - c[k - 1] - 2));
        }
        if (j == 1) {
          if (a[i - 1][1] == '.') {
            add(f[i][j], pw(2, c[j] - 2));
            // puts("G2");
          }
        }
      }
    }
    g[i][j] = (f[i][j] + g[i - 1][j]) % P;
    // printf("f %d %d %d\n",i,j,f[i][j]);
  }

  int ans = 0;
  FOR(j, 0, n - 1) if (a[n][j + 1] != 'W') {
    // printf("g %d %d %d\n",n,j,g[n][j]);
    add(ans, g[n][j] * 1ll * pw(2, c[n] - c[j] - 1));
    // printf("ans %d\n",ans);
  }
  add(ans, g[n][n]);
  printf("%d\n", ans);
  return 0;
}
