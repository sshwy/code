// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

bool query(string s) {
  printf("? %s\n", s.c_str());
  fflush(stdout);
  int res;
  scanf("%d", &res);
  return res == 1;
}
void answer(string s) {
  printf("! %s\n", s.c_str());
  fflush(stdout);
  exit(0);
}

int L, R;
double k;

int cnt[3];
char ch[] = {'a', 'b', 'c'};

int count(char c) {
  int l = 0, r = R;
  while (l < r) {
    int mid = (l + r + 1) >> 1;
    if (query(string(mid, c)))
      l = mid;
    else
      r = mid - 1;
  }
  return l;
}

int detect(string p, string s, int tot, char c) {
  int pre = 0, step = 1;
  bool increase = false;
  while (step > 0) {
    // printf("pre %d step %d\n", pre, step);
    if (pre + step <= tot && query(p + string(pre + step, c) + s)) {
      step *= 2;
      increase = true;
    } else {
      if (increase) {
        step /= 2;
        pre += step;
        step /= 2;
      } else {
        step /= 2;
      }
      increase = false;
    }
  }
  return pre;
}
string work(string s, int tot, char c) {
  if (tot == 0) return s;
  string prefix;
  for (unsigned i = 0; i <= s.size(); i++) {
    string suffix = s.substr(i);
    int t = detect(prefix, suffix, tot, c);
    prefix += string(t, c);
    tot -= t;
    if (i < s.size()) prefix += s[i];
  }
  return prefix;
}
int main() {
  scanf("%d%d%lf", &L, &R, &k);
  FOR(i, 0, 2) {
    cnt[i] = count(ch[i]);
    // printf("cnt %c = %d\n", ch[i], cnt[i]);
  }
  if (cnt[0] < cnt[1]) swap(cnt[0], cnt[1]), swap(ch[0], ch[1]);
  if (cnt[0] < cnt[2]) swap(cnt[0], cnt[2]), swap(ch[0], ch[2]);
  if (cnt[1] < cnt[2]) swap(cnt[1], cnt[2]), swap(ch[1], ch[2]);

  string s(cnt[0], ch[0]);
  FOR(i, 1, 2) { s = work(s, cnt[i], ch[i]); }
  answer(s);
  return 0;
}
