// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

const int N = 5005;
int n, m;
double k;
char s_c[N], t_c[N];

bool check(string s, string t) {
  unsigned i = 0, j = 0;
  while (i < s.size() && j < t.size()) {
    if (s[i] == t[j])
      ++i, ++j;
    else
      ++i;
  }
  return j == t.size();
}

int main() {
  FILE *fin = fopen("fin.txt", "r");
  FILE *fmsg = fopen("log.txt", "w");

  fscanf(fin, "%d %d %lf", &n, &m, &k);
  fscanf(fin, "%s", s_c);
  fclose(fin);

  string s(s_c);

  printf("%d %d %lf\n", n, m, k);
  fflush(stdout);

  fprintf(fmsg, "%d %d %lf\n%s\n", n, m, k, s_c);
  while (1) {
    char op[5];
    scanf("%s%s", op, t_c);
    fprintf(fmsg, "%s %s\n", op, t_c);
    if (op[0] == '?') {
      string t(t_c);
      int x = check(s, t);
      printf("%d\n", x);
      fprintf(fmsg, "%d\n", x);
      fflush(stdout);
    } else {
      string t(t_c);
      if (s == t) {
        fprintf(stderr, "ok correct\n");
        fclose(fmsg);
        return 0;
      } else {
        fprintf(stderr, "wrong answer (s = %s, t = %s)\n", s.c_str(), t.c_str());
        fclose(fmsg);
        return 0;
      }
    }
  }

  return 0;
}
