// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

const int P = 998244353, SZ = 1 << 21;

int pw(int a, int m) {
  int res = 1;
  while (m) m & 1 ? res = 1ll * res * a % P : 0, a = 1ll * a * a % P, m >>= 1;
  return res;
}
void add(int &x, long long y) { x = (x % P + y % P + P) % P; }

void fmt_and(int *f, int n, int tag) {
  for (int i = 0; i < n; i++)
    for (int j = 0; j < (1 << n); j++)
      if (j >> i & 1) add(f[j ^ (1 << i)], f[j] * tag);
}
void fmt_or(int *f, int n, int tag) {
  for (int i = 0; i < n; i++)
    for (int j = 0; j < (1 << n); j++)
      if (j >> i & 1) add(f[j], f[j ^ (1 << i)] * tag);
}

long long n, m;
int k, f[SZ], g[SZ];

int main() {
  scanf("%lld%lld%d", &n, &m, &k);
  n %= P - 1;
  m %= P - 1;

  int logK = 0;
  while ((1 << logK) <= k) ++logK;

  FOR(i, 0, k) f[i] = 1;
  fmt_and(f, logK, 1);
  FOR(i, 0, (1 << logK) - 1) f[i] = pw(f[i], m);
  fmt_and(f, logK, -1);

  FOR(i, 0, k) g[i] = 1;
  fmt_or(g, logK, 1);
  FOR(i, 0, (1 << logK) - 1) g[i] = pw(g[i], n);

  int ans = 0;
  FOR(i, 0, k) add(ans, 1ll * f[i] * g[i]);
  printf("%d\n", ans);

  return 0;
}
