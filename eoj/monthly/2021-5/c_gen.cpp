// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

using namespace RA;

int main() {
  srand(clock() + time(0));
  int L = 100, R = 100, k = 1000;
  printf("%d %d %d\n", L, R, k);
  int n = rnd(L, R);
  FOR(i, 1, n) printf("%c", rnd('a', 'c'));
  puts("");
  return 0;
}
