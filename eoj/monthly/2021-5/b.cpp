// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

const int M = 1e5 + 5;

long long a[M], b[M];

pair<int, bool> solve(int l, int r, int h, long long n) {
  if (n == 0) return {0, false};
  assert(n > 0);
  if (l > r) return {0, false};
  // printf("solve %d %d h = %d, n = %lld\n", l, r, h, n);
  // FOR(i, l, r) printf("%lld%c", a[i], " \n"[i == r]);
  if (h == 0) return {1, n == 1};

  int lb = 0, mid;
  FOR(i, l, r) if (a[i] % 2 == 0) b[++lb] = a[i];
  mid = l + lb - 1;
  FOR(i, l, r) if (a[i] % 2) b[++lb] = a[i];
  assert(lb == r - l + 1);
  FOR(i, l, r) a[i] = b[i - l + 1], a[i] /= 2;

  // [l, mid], [mid + 1, r]

  if (n % 2) {
    auto pl = solve(l, mid, h - 1, n / 2 + 1);
    auto pr = solve(mid + 1, r, h - 1, n / 2 + 1);
    if (pl.second && pr.second) {
      return {pl.first + pr.first - 1, true};
    } else if (pl.second || pr.second) {
      return {pl.first + pr.first, true};
    } else {
      return {pl.first + pr.first, false};
    }
  } else {
    auto pl = solve(l, mid, h - 1, n / 2);
    auto pr = solve(mid + 1, r, h - 1, n / 2);
    pl.first += pr.first;
    pl.second &= pr.second;
    return pl;
  }
}

int h, m;
long long n;

long long bitrev(long long x) {
  long long rx = 0;
  FOR(i, 0, h - 1) rx |= (x >> i & 1) << (h - 1 - i);
  return rx;
}
int main() {
  scanf("%d%d%lld", &h, &m, &n);
  FOR(i, 1, m) scanf("%lld", a + i), a[i] = bitrev(a[i] - 1);
  sort(a + 1, a + m + 1);

  printf("%lld\n", solve(1, m, h, n % (1ll << h)).first + n / (1ll << h) * m);
  return 0;
}
