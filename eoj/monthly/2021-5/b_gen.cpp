// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

using namespace RA;

const int N = 1e5 + 5;
int p[N];

int main() {
  srand(clock() + time(0));
  int h = rnd(1, 4);
  FOR(i, 1, 1 << h) p[i] = i;
  random_shuffle(p + 1, p + (1 << h) + 1);
  int m = rnd(1, 1 << h);
  int n = rnd(1, 1e3);
  printf("%d %d %d\n", h, m, n);
  FOR(i, 1, m) printf("%d%c", p[i], " \n"[i == m]);

  return 0;
}
