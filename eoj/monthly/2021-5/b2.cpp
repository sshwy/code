// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

const int N = 1e5 + 5;
int h, n, m, ans;
int a[N], s[N], cnt[N];
int totToggle;

void push() {
  int u = 1;
  FOR(i, 1, h) {
    if (s[u]) {
      s[u] = !s[u];
      u = u * 2 + 1;
    } else {
      s[u] = !s[u];
      u = u * 2;
    }
  }
  cnt[u]++;
}
int calc(int stat) {
  FOR(i, 0, totToggle - 1) { s[i + 1] = stat >> i & 1; }
  FOR(i, 1, 1 << (h + 1)) cnt[i] = 0;
  FOR(i, 1, n) push();
  int res = 0;
  FOR(i, 1, m) { res += cnt[a[i] + totToggle]; }
  return res;
}
int main() {
  cin >> h >> m >> n;
  FOR(i, 1, m) cin >> a[i];

  totToggle = (1 << h) - 1;
  FOR(i, 0, (1 << totToggle) - 1) { ans = max(ans, calc(i)); }
  cout << ans << endl;
  return 0;
}
