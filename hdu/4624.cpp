#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <climits>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 52;

long long f[N][N * N][N][2];
int n, n2;

void dp() {
  f[0][0][0][0] = 1;
  FOR(i, 0, n) FOR(j, 0, i * (i + 1) / 2) FOR(k, 0, i) FOR(bit, 0, 1) {
    f[i + 1][j][0][bit ^ 1] += f[i][j][k][bit];
    if (j + k + 1 <= n2) f[i + 1][j + k + 1][k + 1][bit] += f[i][j][k][bit];
    if (f[i][j][k][bit])
      printf("f[%d,%d,%d,%d]=%lld\n", i, j, k, bit, f[i][j][k][bit]);
  }
}
int main() {
  cin >> n;
  n2 = n * (n + 1) / 2;
  dp();
  long double ans = 0;
  FOR(A, 0, n2 - 1) {
    long double x = n2 / (n2 - A + 0.0);
    long long cnt = 0;
    FOR(k, 0, n) cnt += f[n][A][k][0] - f[n][A][k][1];
    printf("A=%d,cnt=%lld\n", A, cnt);
    ans += x * cnt;
  }
  ans = -ans;
  printf("%.10Lf", ans);
  return 0;
}
