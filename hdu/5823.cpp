#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
#define int unsigned int
const int SZ = 1 << 19, N = 19, INF = 0x3f3f3f3f;

void fwt(int *f, int len) {
  for (int j = 1; j < len; j <<= 1)
    for (int i = 0; i < len; i += j << 1)
      for (int k = i; k < i + j; k++) f[k + j] += f[k];
}
void ifwt(int *f, int len) {
  for (int j = 1; j < len; j <<= 1)
    for (int i = 0; i < len; i += j << 1)
      for (int k = i; k < i + j; k++) f[k + j] -= f[k];
}

int n, nn;
int f[SZ], f1[SZ], ans[SZ];
int g[N], tot;

int pw(int a, int m) {
  int res = 1;
  while (m) m & 1 ? res = res * a : 0, a = a * a, m >>= 1;
  return res;
}
char s[50];
void go() {
  scanf("%d", &n);
  nn = 1 << n;
  FOR(i, 0, n - 1) {
    g[i] = 0;
    scanf("%s", s);
    FOR(j, 0, n - 1) { g[i] |= (s[j] - '0') << j; }
    g[i] |= 1 << i;
  }
  FOR(i, 0, nn - 1) {
    // printf("\33[31mi=%u\033[0m\n",i);
    int s = 0, fl = 1;

    FOR(j, 0, n - 1) {
      if (i >> j & 1) {
        if (s & g[j]) fl = 0;
        s |= 1 << j; // BUG#1:判独立集的时侯或成了g[j]，实际上只用加入j这个一个点即可
      }
    }
    f1[i] = fl;
  }
  // printf("f1: ");FOR(i,0,nn-1)printf("%u%c",f1[i]," \n"[i==nn-1]);
  fwt(f1, nn);
  FOR(i, 0, nn - 1) ans[i] = INF;
  FOR(i, 0, nn - 1) f[i] = 0;
  f[0] = 1, ans[0] = 0;
  // printf("f: ");FOR(i,0,nn-1)printf("%u%c",f[i]," \n"[i==nn-1]);
  FOR(i, 1, n) {
    fwt(f, nn);
    FOR(j, 0, nn - 1) f[j] *= f1[j];
    ifwt(f, nn);
    FOR(j, 0, nn - 1) f[j] = !!f[j];
    // printf("i=%u, ",i);
    // printf("f: ");FOR(i,0,nn-1)printf("%u%c",f[i]," \n"[i==nn-1]);
    FOR(j, 0, nn - 1) if (f[j]) ans[j] = min(ans[j], i);
  }
  tot = 0;
  // printf("ans: ");FOR(i,0,nn-1)printf("%u%c",ans[i]," \n"[i==nn-1]);
  FOR(i, 1, nn - 1) { tot += ans[i] * pw(233, i); }
  printf("%u\n", tot);
}
signed main() {
  int t;
  scanf("%d", &t);
  FOR(i, 1, t) go();
  return 0;
}
/*
 * BUG#2:数组开小
 */
