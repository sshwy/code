// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

long long C[20][20];
long long work(int *cnt, int rest, int d) {
  long long res = 0;
  FOR(i, cnt[d], 18) {
    int restI = rest - (i - cnt[d]);
    if (restI < 0) continue;
    int lim[10] = {0};
    bool ok = true;
    FOR(j, 0, 9) if (j != d) {
      lim[j] = i - cnt[j] - 1;
      if (lim[j] < 0) ok = false;
    }
    if (ok) {
      long long f[20][20] = {0};
      f[0][0] = 1;
      int cur = 0;
      FOR(j, 0, 9) if (j != d) {
        ++cur;
        ROF(t, restI, 0) {
          int maxK = min(lim[j], t);
          FOR(k, 0, maxK) { f[cur][t] += f[cur - 1][t - k] * C[t][k]; }
        }
      }
      res += f[cur][restI] * C[rest][i - cnt[d]];
    }
  }
  return res;
}
long long calc(long long x, int d) {
  if (x == 0) return 0;
  long long res = 0;

  int a[20], la = 0;
  while (x) a[++la] = x % 10, x /= 10;
  reverse(a + 1, a + la + 1);

  FOR(i, 1, la - 1) {
    FOR(j, 1, 9) {
      int cnt[10] = {0};
      cnt[j]++;
      res += work(cnt, i - 1, d);
    }
  }

  FOR(i, 1, la) {
    int maxJ = i == la ? a[i] : a[i] - 1;
    int minJ = i == 1 ? 1 : 0;
    FOR(j, minJ, maxJ) {
      int cnt[10] = {0};
      FOR(k, 1, i - 1) cnt[a[k]]++;
      cnt[j]++;
      res += work(cnt, la - i, d);
    }
  }

  return res;
}
void go() {
  long long l, r, d;
  scanf("%lld %lld %lld", &l, &r, &d);
  printf("%lld\n", calc(r, d) - calc(l - 1, d));
}

int main() {
  C[0][0] = 1;
  FOR(i, 1, 19) {
    C[i][0] = 1;
    FOR(j, 1, 19) { C[i][j] = C[i - 1][j - 1] + C[i - 1][j]; }
  }
  int t;
  scanf("%d", &t);
  FOR(i, 1, t) go();
  return 0;
}
