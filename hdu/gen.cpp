// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  long long rnd(long long p) { return 1ll * rand() * rand() % p; }
  long long rnd(long long L, long long R) { return rnd(R - L + 1) + L; }
} // namespace RA

using namespace RA;

void go() {
  long long lim = 1ll << rnd(0, 59);
  long long l = rnd(1, lim), r = rnd(1, lim), d = rnd(0, 9);
  if (l > r) swap(l, r);
  printf("%lld %lld %lld\n", l, r, d);
}
int main() {
  srand(clock() + time(0));
  int t = 100;
  cout << t << endl;
  FOR(i, 1, t) go();
  return 0;
}
