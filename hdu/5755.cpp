#include <cstdio>
#include <cstring>
#include <iostream>
#define FOR(a, b, c) for (int a = b; a <= c; a++)
using namespace std;
const int N = 32;
int n, m, nm;
int a[N][N];
int c[N * N][N * N], b[N * N];
int _(int x, int y) { return (x - 1) * m + y; }
void gauss() {
  FOR(i, 1, nm) {
    FOR(j, i, nm) if (c[j][i] > 0) {
      FOR(k, 1, nm) swap(c[i][k], c[j][k]);
      swap(b[i], b[j]);
      break;
    }
    FOR(j, 1, nm) {
      if (i == j) continue;
      //在模3意义下，1,2的逆元都是其本身
      int rate = c[j][i] * c[i][i] % 3;
      if (!rate) continue;
      FOR(k, i, nm) c[j][k] = (c[j][k] - c[i][k] * rate % 3 + 3) % 3;
      b[j] = (b[j] - b[i] * rate % 3 + 3) % 3;
    }
  }
}
void go() {
  scanf("%d%d", &n, &m);
  nm = n * m;
  FOR(i, 1, n) FOR(j, 1, m) scanf("%d", &a[i][j]);
  int cnt = 0; //方程数量
  memset(c, 0, sizeof(c));
  FOR(i, 1, n) FOR(j, 1, m) { //添加方程
    ++cnt;
    c[cnt][_(i, j)] = 2;
    if (i > 1) c[cnt][_(i - 1, j)] = 1;
    if (i < n) c[cnt][_(i + 1, j)] = 1;
    if (j > 1) c[cnt][_(i, j - 1)] = 1;
    if (j < m) c[cnt][_(i, j + 1)] = 1;
    b[cnt] = 3 - a[i][j];
  }
  gauss();
  FOR(i, 1, nm) b[i] = b[i] * c[i][i] % 3;
  int ans = 0;
  FOR(i, 1, n) FOR(j, 1, m) ans += b[_(i, j)];
  printf("%d\n", ans);
  FOR(i, 1, n) FOR(j, 1, m) {
    int __ = b[_(i, j)];
    FOR(k, 1, __) printf("%d %d\n", i, j);
  }
}
int main() {
  // freopen("tmpout","w",stdout);
  int t;
  scanf("%d", &t);
  while (t--) go();
  return 0;
}
