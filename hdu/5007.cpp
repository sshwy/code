// by Yao
#include <cstdio>
#include <string>
using namespace std;
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)

char s[1000000 + 5];

void mai() { puts("MAI MAI MAI!"); }
void sony() { puts("SONY DAFA IS GOOD!"); }

int main() {
  int n = fread(s, 1, 1000000, stdin);
  FOR(i, 0, n - 1) {
    if (i + 5 <= n && string(s + i, s + i + 5) == "Apple") mai();
    if (i + 6 <= n && string(s + i, s + i + 6) == "iPhone") mai();
    if (i + 4 <= n && string(s + i, s + i + 4) == "iPod") mai();
    if (i + 4 <= n && string(s + i, s + i + 4) == "iPad") mai();
    if (i + 4 <= n && string(s + i, s + i + 4) == "Sony") sony();
  }
  return 0;
}
