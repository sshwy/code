#include <algorithm>
#include <cstdio>
#include <cstring>
#define FOR(a, b, c) for (int a = b; a <= c; a++)
#define ROF(a, b, c) for (int a = b; a >= c; a--)
#define int long long
using namespace std;
const int N = 105, P = 1e9 + 7;

int f[N][10][4], ld;
char d[N];

void DP() {
  FOR(i, 0, 9) f[1][i][0] = 1;
  FOR(i, 2, N - 1) FOR(j, 0, 9) {
    f[i][j][0] = f[i - 1][j][0];
    FOR(k, 0, 9) {
      if (j <= k) f[i][j][1] += f[i - 1][k][1];
      if (j < k) f[i][j][1] += f[i - 1][k][0];
      if (j >= k) f[i][j][2] += f[i - 1][k][2];
      if (j > k) f[i][j][2] += f[i - 1][k][0];
      if (j >= k) f[i][j][3] += f[i - 1][k][3];
      if (j > k) f[i][j][3] += f[i - 1][k][1];
    }
    f[i][j][1] %= P;
    f[i][j][2] %= P;
    f[i][j][3] %= P;
  }
}

void go() {
  scanf("%s", d + 1);
  ld = strlen(d + 1);
  reverse(d + 1, d + ld + 1); //最高位是ld
  FOR(i, 1, ld) d[i] -= '0';
  int res = 0, sta = 0;
  FOR(i, 1, ld - 1) FOR(j, 1, 9) FOR(k, 0, 3) res += f[i][j][k]; //长度小于ld
  FOR(j, 1, d[ld] - 1)
  FOR(k, 0, 3) res += f[ld][j][k]; //长度等于ld的情况首位非0
  res %= P;
  ROF(i, ld - 1, 1) { // sta：0相同，1单增，2单减，3v
    int l = 0, r = d[i] - 1;
    if (sta == 1 || sta == 3) l = max(l, d[i + 1] + 0ll);
    FOR(j, l, r) {
      if (sta == 0 || sta == 2) { //相同的贡献
        if (j <= d[i + 1])
          FOR(k, 0, 3) res += f[i][j][k];
        else
          res += f[i][j][1] + f[i][j][0];
      } else
        res += f[i][j][1] + f[i][j][0];
    }
    res %= P;
    if (d[i] < d[i + 1]) { //更新状态
      if (sta == 1 || sta == 3) {
        sta = 4;
        break;
      } //上界不合法
      else
        sta = 2;
    } else if (d[i] > d[i + 1]) {
      if (sta == 2 || sta == 3)
        sta = 3;
      else
        sta = 1;
    }
  }
  if (sta != 4) res++;
  printf("%lld\n", res);
}
signed main() {
  int t;
  DP();
  scanf("%lld", &t);
  while (t--) go();
  return 0;
}
/*
 * BUG#1: L47少写f[i][j][0]
 */
