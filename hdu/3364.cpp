#include <algorithm>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define clr(a) memset(a, 0, sizeof(a))
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
/******************heading******************/
const int N = 52, M = 52;
int T;
int n, m, q;
lld a[N], b[N], c[N]; //状圧
/*void print(char *info){
    printf("%s\n",info);
    FOR(i,1,n){
        ROF(j,m,1)printf("%d ",a[i]>>j&1);
        printf("| %d\n",a[i]&1);
    }
}*/
void guass() {
  FOR(i, 1, m) {
    // print("Info");
    FOR(j, i + 1, n) if (a[j] > a[i]) swap(a[i], a[j]);
    if (a[i] == 0) {
      printf("%lld\n", 1ll << (m - i + 1));
      return;
    } else if (a[i] == 1) {
      printf("0\n");
      return;
    }
    ROF(j, m, 1) {
      if (!(a[i] >> j & 1)) continue;
      FOR(k, 1, n) if (i != k && (a[k] >> j & 1)) a[k] ^= a[i];
      b[j] ^= b[i];
      break;
    }
  }
  // print("Result");
  FOR(i, 1, n) if (a[i] == 1) {
    printf("0\n");
    return;
  }
  printf("1\n");
}
void go(int cas) {
  printf("Case %d:\n", cas);
  scanf("%d%d", &n, &m);
  clr(c);
  FOR(i, 1, m) {
    int k;
    scanf("%d", &k);
    FOR(j, 1, k) {
      int x;
      scanf("%d", &x);
      c[x] |= 1ll << i;
    }
  }
  scanf("%d", &q);
  FOR(i, 1, q) {
    int x;
    FOR(j, 1, n) {
      scanf("%d", &x);
      c[j] = c[j] / 2 * 2 + x;
    }
    memcpy(a, c, sizeof(c));
    guass();
  }
}
int main() {
  scanf("%d", &T);
  FOR(i, 1, T) go(i);
  return 0;
}
/*
 * 之前写了一个不带状圧的版本，然而当时只知道用第i个方程的第i元去消元
 * 如果写成“第i个方程的编号最小的元”就能达到预期效果了
 */
