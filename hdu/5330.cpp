#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
#define LL long long
const int SZ = 1e6 + 5, M = 12;

int n, m;

LL f1[SZ][M * 3], f2[SZ][M * 3], ans[M * 3];
LL (*f)[M * 3], (*g)[M * 3];
LL m3[M];
void print() {
  FOR(i, 0, m3[m] - 1) {
    FOR(k, 0, m * 2) {
      if (!f[i][k]) continue;
      printf("f[");
      ROF(j, m - 1, 0) printf("%lld", i / m3[j] % 3);
      printf(",%d]=%lld\n", k, f[i][k]);
    }
  }
}
void go() {
  f = f1, g = f2;
  scanf("%d%d", &n, &m);
  int lim = m3[m] - 1;
  FOR(i, 0, lim) FOR(k, 0, m * 2 + 1) f[i][k] = 0;
  FOR(i, 1, n) {
    int x = 0;
    char s[50];
    scanf("%s", s + 1);
    FOR(j, 1, m) x = x * 3 + s[j] - '0';
    ++f[x][0];
  }
  FOR(j, 0, m - 1) {
    FOR(i, 0, lim) {
      int x = i / m3[j] % 3;
      g[i][0] = f[i][0];
      FOR(k, 1, m * 2) {
        if (x == 0) {
          g[i][k] = f[i][k] + f[i + m3[j]][k - 1] + f[i + m3[j] * 2][k - 2];
        } else if (x == 1) {
          g[i][k] = f[i][k] + f[i - m3[j]][k - 1] + f[i + m3[j]][k - 1];
        } else {
          g[i][k] = f[i][k] + f[i - m3[j]][k - 1] + f[i - m3[j] * 2][k - 2];
        }
      }
    }
    swap(f, g);
  }
  // print();
  FOR(i, 0, m * 2) ans[i] = 0;
  FOR(i, 0, lim) FOR(k, 1, m * 2) ans[k] += f[i][k] * f[i][0];
  FOR(k, 1, m * 2) ans[k] /= 2;
  FOR(i, 0, lim) ans[0] += f[i][0] * (f[i][0] - 1ll) / 2;
  FOR(i, 0, m * 2) printf("%lld\n", ans[i]);
}
int main() {
  m3[0] = 1;
  FOR(i, 1, 11) m3[i] = m3[i - 1] * 3;
  int t;
  scanf("%d", &t);
  FOR(i, 1, t) go();
  return 0;
}
/*
 * 状圧DP。f[mask,k]表示距离状态mask为k的状态的个数。转移的时侯枚举某个维度上的距离然后转移
 * 和高维前缀和差不多
 * 最后算重的要除以2
 */
