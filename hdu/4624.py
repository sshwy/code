import numpy as np
from decimal import *

ctx=localcontext()
ctx.prec=50

N=55
N2=50*51//2

f = np.zeros((N,N*N,N,2),dtype=int)

print("GG")

f[0][0][0][0]=1;


for i in range(0,50+1):
    print("i=%d" % i)
    for j in range(0,i*(i+1)//2+1):
        for k in range(0,i+1):
            for bit in [0,1]:
                f[i+1][j][0][bit^1]+=f[i][j][k][bit]
                if j+k+1 <= N2 :
                    f[i+1][j+k+1][k+1][bit]+=f[i][j][k][bit]


for i in range(1,51):
    n = i
    n2 = n*(n+1)//2
    ans = Decimal('0')
    for A in range(0,n2):
        x = Decimal(n2)/Decimal(n2-A)
        cnt = Decimal('0')
        for k in range(0,n+1):
            cnt+=f[n][A][k][0]-f[n][A][k][1]
        ans += Decimal(x)*Decimal(cnt)
    print("if(n==%d)cout<<\"%s\";" % (i,round( -ans,15)))
