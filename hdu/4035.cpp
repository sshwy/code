//注：本题1e-8会WA
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 1e4 + 5;

int n;
vector<int> g[N];
int d[N];
double k[N], e[N];
double a[N], b[N], c[N], ans;

void dfs1(int u, int p) {
  for (int v : g[u])
    if (v != p) dfs1(v, u);
  if (abs(1.0 - k[u] - e[u]) < 1e-9) {
    a[u] = 0;
    b[u] = 0;
    c[u] = k[u];
    if (u == 1) {
      if (abs(1.0 - k[u]) < 1e-9)
        ans = 1e10; // impossible
      else
        ans = 0;
    }
  } else {
    double A = 0, B = 0, C = 0, D = 0;
    for (int v : g[u])
      if (v != p) A += a[v], B += b[v], C += c[v];
    D = d[u] / (1.0 - k[u] - e[u]);
    a[u] = 1.0 / (D - A);
    b[u] = (B + d[u]) / (D - A);
    c[u] = (D * k[u] + C) / (D - A);
    if (u == 1) {
      if (abs(D - k[1] * D - A - C) < 1e-9)
        ans = 1e10;
      else
        ans = (B + d[1]) / (D - k[1] * D - A - C);
    }
  }
}
void go(int id) {
  scanf("%d", &n);
  FOR(i, 1, n) g[i].clear(), d[i] = 0;
  FOR(i, 1, n - 1) {
    int x, y;
    scanf("%d%d", &x, &y);
    g[x].pb(y), g[y].pb(x);
    d[x]++, d[y]++;
  }
  FOR(i, 1, n) {
    int x, y;
    scanf("%d%d", &x, &y);
    k[i] = x / 100.0;
    e[i] = y / 100.0;
  }
  dfs1(1, 0);
  if (abs(ans) < 1e-9) ans = 0.0;
  if (ans > 9e9)
    printf("Case %d: impossible\n", id);
  else
    printf("Case %d: %.6lf\n", id, ans);
}
int main() {
  int t;
  scanf("%d", &t);
  FOR(i, 1, t) go(i);
  return 0;
}
