#include <bits/stdc++.h>
using namespace std;

const int N = 1000005;
int n, m;
int a[N];
int mx[1 << 20];

int lst_build(int l, int r, int rt) {
  if (l == r) return mx[rt] = a[l];
  int mid = (l + r) >> 1;
  lst_build(l, mid, rt << 1), lst_build(mid + 1, r, rt << 1 | 1);
  mx[rt] = max(mx[rt << 1], mx[rt << 1 | 1]);
}
int lst_upd(int p, int v, int l, int r, int rt) {
  if (p < l || p > r) return 0;
  if (p == l && p == r) return mx[rt] = max(mx[rt], v);
  int mid = (l + r) >> 1;
  lst_upd(p, v, l, mid, rt << 1), lst_upd(p, v, mid + 1, r, rt << 1 | 1);
  mx[rt] = max(mx[rt << 1], mx[rt << 1 | 1]);
}
int lst_max(int L, int R, int l, int r, int rt) {
  if (R < l || r < L) return -999999999;
  if (L <= l && r <= R) return mx[rt];
  int mid = (l + r) >> 1;
  return max(lst_max(L, R, l, mid, rt << 1), lst_max(L, R, mid + 1, r, rt << 1 | 1));
}
int main() {
  cin >> n >> m;                            // scanf("%d%d",&n,&m);
  for (int i = 1; i <= n; i++) cin >> a[i]; // scanf("%d",&a[i]);
  lst_build(1, n, 1);
  for (int i = 1, x, y; i <= m; i++) {
    char c;
    cin >> c >> x >> y;
    // scanf("%c %d %d",&c,&x,&y);
    if (c == 'Q')
      printf("%d\n", lst_max(x, y, 1, n, 1));
    else if (c == 'U')
      lst_upd(x, y, 1, n, 1);
  }
  return 0;
}
