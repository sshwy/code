#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int P = 998244353, N = 1 << 22;

int n, nn;

int pw(int a, int m, int p) {
  int res = 1;
  while (m) m & 1 ? res = 1ll * res * a % p : 0, a = 1ll * a * a % p, m >>= 1;
  return res;
}
void fwt(int *f, int len, int tag) {
  for (int j = 1; j < len; j <<= 1)
    for (int i = 0; i < len; i += j << 1)
      for (int k = i, x, y; k < i + j; k++)
        x = f[k], y = f[k + j], f[k] = x + y, f[k] >= P ? f[k] -= P : 0,
        f[k + j] = x - y + P, f[k + j] >= P ? f[k + j] -= P : 0;
  int ilen = pw(len, P - 2, P);
  if (tag == -1) FOR(i, 0, len - 1) f[i] = 1ll * f[i] * ilen % P;
  FOR(i, 0, len - 1) f[i] += f[i] < 0 ? P : 0;
}
int f[N];
void go() {
  int lim = n;
  while (lim != (lim & -lim)) lim += lim & -lim;
  lim = min(lim, nn - 1);
  memset(f, 0, sizeof(f));
  FOR(i, 0, n - 1) f[i] = 1;
  fwt(f, nn, 1);
  FOR(i, 0, nn - 1) if (f[i]) f[i] = pw(f[i], 3, P);
  fwt(f, nn, -1);
  // FOR(i,0,nn-1)if(f[i])printf("f[%d]=%d\n",i,f[i]);
  int tot = 0, t2 = 0;
  FOR(i, 0, lim) {
    if (!f[i]) continue;
    tot += 1ll * f[i] * i % P, tot %= P;
    t2 += 1ll * i * i % P * f[i] % P, t2 %= P;
  }
  tot = 1ll * tot * tot % P;
  int ans = 1ll * (tot - t2) * pw(2, P - 2, P) % P;
  ans = (ans + P) % P;
  printf("%d\n", ans);
}
int main() {
  nn = 1 << 22;
  while (~scanf("%d", &n)) go();
  return 0;
}
