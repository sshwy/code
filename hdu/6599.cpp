#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
namespace IO {
  char nc() {
    static char bf[100000], *p1 = bf, *p2 = bf;
    return p1 == p2 && (p2 = (p1 = bf) + fread(bf, 1, 100000, stdin), p1 == p2)
               ? EOF
               : *p1++;
  }
  int rd() {
    int res = 0;
    char c = getchar();
    while (!isdigit(c)) c = getchar();
    while (isdigit(c)) res = res * 10 + c - '0', c = getchar();
    return res;
  }
} // namespace IO
/******************heading******************/
const int SZ = 3e5 + 500, ALP = 26;

struct qxx {
  int nex, t;
};
qxx e[SZ];
int h[SZ], le;
void add_path(int f, int t) { e[++le] = (qxx){h[f], t}, h[f] = le; }

struct PAM {
  int tot, last;
  int len[SZ], tr[SZ][ALP], fail[SZ];
  int s[SZ], ls;
  int cnt[SZ];
  int newnode(int l) {
    ++tot, len[tot] = l, fail[tot] = 0, cnt[tot] = 0;
    FOR(i, 0, ALP - 1) tr[tot][i] = 0;
    return tot;
  }
  void clear() {
    tot = -1;
    newnode(0), newnode(-1);
    fail[0] = 1, last = 0;
    s[ls = 0] = -1;
  }
  PAM() { clear(); }
  int getfail(int u) {
    while (s[ls - len[u] - 1] != s[ls]) u = fail[u];
    return u;
  }
  void insert(char c) {
    // printf("insert(%c)\n",c);
    s[++ls] = (c -= 'a');
    int cur = getfail(last);
    if (!tr[cur][c]) {
      int u = newnode(len[cur] + 2);
      fail[u] = tr[getfail(fail[cur])][c];
      tr[cur][c] = u;
    }
    last = tr[cur][c];
    cnt[last]++;
  }
  void count() { //最后用来计算每个状态的出现次数
    ROF(i, tot, 0) cnt[fail[i]] += cnt[i];
    // ROF(i,tot,0) printf("cnt[%d]=%d,len[%d]=%d\n",i,cnt[i],i,len[i]);
  }
  void build_fail_tree() {
    memset(h, 0, sizeof(int) * (tot + 5));
    le = 0;
    FOR(i, 0, tot) {
      if (i != 1) {
        add_path(fail[i], i);
        // printf("add_path(%d,%d)\n",fail[i],i);
      }
    }
  }
  int bin[SZ], ans[SZ];
  void dfs(int u) {
    // printf("dfs(%d)\n",u); printf("cnt[%d]=%d\n",u,cnt[u]);
    bin[len[u]] = 1;
    if (bin[(len[u] + 1) / 2]) ans[len[u]] += cnt[u];
    for (int i = h[u]; i; i = e[i].nex) dfs(e[i].t);
    bin[len[u]] = 0;
  }
  void go() {
    build_fail_tree();
    memset(ans, 0, sizeof(int) * (ls + 5));
    dfs(1);
    FOR(i, 1, ls) printf("%d%c", ans[i], " \n"[i == ls]);
  }
} pam;
// pam 的状态数可能比原串长度小！
char s[SZ];

void go() {
  pam.clear();
  for (int i = 1; s[i]; i++) pam.insert(s[i]);
  pam.count();
  // puts("Wild.");
  pam.go();
}

int main() {
  while (~scanf("%s", s + 1)) go();
  return 0;
}
