g++ 6796.cpp -o .u
g++ std.cpp -o .v
g++ gen.cpp -o .w

while true; do
  ./.w > .i
  ./.u < .i > .x
  ./.v < .i > .y
  if diff -w .x .y; then
    echo AC
  else
    echo WA
    break
  fi
done
