#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
typedef unsigned int UI;
typedef unsigned long long ULL;
const int N = 1e6 + 5;

UI ans[N];
bool bp[N];
ULL phi_d[N]; // phi_d * 1
UI pn[N];
int lp;
UI g[N];

void sieve(int lim) {
  bp[0] = bp[1] = 1;
  phi_d[1] = 1;
  FOR(i, 2, lim) {
    if (!bp[i]) pn[++lp] = i, phi_d[i] = i * (i - 1ull);
    FOR(j, 1, lp) {
      int pj = pn[j];
      if (i * pj > lim) break;
      bp[i * pj] = 1;
      if (i % pj == 0) {
        phi_d[i * pj] = phi_d[i] * pj * pj;
        break;
      } else {
        phi_d[i * pj] = phi_d[i] * phi_d[pj];
      }
    }
  }
  ROF(i, lim, 1) {
    for (int j = i + i; j <= lim; j += i) phi_d[j] += phi_d[i];
  }
  FOR(i, 1, lim) g[i] = (((UI)i * (phi_d[i] - 1ull)) >> 1ull) + (UI)i;
  FOR(i, 2, lim) g[i] += g[i - 1];
  FOR(d, 1, lim) {
    for (int i = 0; i <= lim; i += d) {
      UI t = d * d * g[i / d];
      ans[i] += t;
      ans[i + d] -= t;
    }
  }
  FOR(i, 2, lim) ans[i] += ans[i - 1];
}

int main() {
  sieve(5e5);
  int t;
  scanf("%d", &t);
  FOR(i, 1, t) {
    int n;
    scanf("%d", &n);
    printf("Case #%d: %u\n", i, ans[n]);
  }
  return 0;
}
