#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 35, P = 1e9 + 7, X = 17;
int pw(int a, int m) {
  int res = 1;
  while (m) m & 1 ? res = 1ll * res * a % P : 0, a = 1ll * a * a % P, m >>= 1;
  return res;
}

int n;
long long m;
int p[N], inv[N];
int A[N], B[N], la, lb;
int f[1 << X][N], g[1 << X][N];
int fac[N];

int sig(int x) { return 1 - 2 * (x & 1); }
pair<long long, int> pa[1 << X], pb[1 << X];
int lpa, lpb;
void go() {
  scanf("%d", &n);
  m = 0;
  FOR(i, 0, n - 1) scanf("%d", &p[i]), m = m + p[i] + 1;
  la = n / 2, lb = n - la;
  FOR(i, 0, la - 1) A[i] = p[i];
  FOR(i, la, n - 1) B[i - la] = p[i];
  m /= 2;
  lpa = lpb = 0;

  FOR(i, 0, (1 << la) - 1) {
    f[i][n - 1] = sig(__builtin_popcount(i));
    long long s = m - 1;
    FOR(j, 0, la - 1) if (i >> j & 1) s = s - A[j];
    ROF(j, n - 2, 0)
    f[i][j] = f[i][j + 1] * 1ll * ((s - (n - 2 - j)) % P) % P * inv[n - 1 - j] %
              P; // BUG#1:s-(n-2-j)没有单独取模导致溢出！
    pa[++lpa] = {s, i};
  }
  FOR(i, 0, (1 << lb) - 1) {
    g[i][0] = sig(__builtin_popcount(i));
    long long s = 0;
    FOR(j, 0, lb - 1) if (i >> j & 1) s = s - B[j];
    FOR(j, 1, n - 1)
    g[i][j] = g[i][j - 1] * 1ll * ((s - (j - 1)) % P) % P * inv[j] % P;
    pb[++lpb] = {s, i};
  }
  int ans = 0;
  sort(pa + 1, pa + lpa + 1);
  sort(pb + 1, pb + lpb + 1);

  FOR(k, 0, n - 1) {
    int pos = lpb + 1, pre = 0;
    FOR(i, 1, lpa) {
      while (pos > 1 && pb[pos - 1].fi + pa[i].fi >= 0)
        --pos, pre = (pre + g[pb[pos].se][k]) % P;
      int cur = f[pa[i].se][k];
      ans = (ans + cur * 1ll * pre) % P;
    }
  }
  ans = (ans + P) % P;
  printf("%d\n", ans);
}
int main() {
  FOR(i, 1, N - 1) inv[i] = pw(i, P - 2);
  fac[0] = 1;
  FOR(i, 1, N - 1) fac[i] = 1ll * fac[i - 1] * i % P;

  int t;
  scanf("%d", &t);
  FOR(i, 1, t) go();
  return 0;
}
