// by Yao
#include <algorithm>
#include <iostream>
#include <string>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

int t;

int main() {
  string s;
  cin >> t;
  getline(cin, s);
  FOR(i, 1, t) {
    getline(cin, s);
    int las = 0;
    while (1) {
      int cur = s.find_first_of(' ', las);
      if (cur == string::npos) {
        reverse(s.begin() + las, s.end());
        break;
      }
      reverse(s.begin() + las, s.begin() + cur);
      las = cur + 1;
    }
    cout << s << endl;
  }
  return 0;
}
