#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/

int n;

long long b[70], lb;
long long p[70], lp;
bool insert(long long x) { //消成对角矩阵
  ROF(i, 63, 0) {
    if (!(x >> i & 1)) continue;
    if (!b[i]) {
      b[i] = x, lb++;
      ROF(j, i - 1, 0) if (b[i] >> j & 1) b[i] ^= b[j];  //消掉i的其他元
      FOR(j, i + 1, 63) if (b[j] >> i & 1) b[j] ^= b[i]; //消掉其他元的i
      return 1;
    }
    x ^= b[i];
  }
  return 0;
}
void print() {
  int len = 0;
  FOR(i, 0, 63) if (b[i]) len = i;
  FOR(i, 0, len) {
    ROF(j, len, 0) { printf("%lld", b[i] >> j & 1); }
    puts("");
  }
}
void work() {
  FOR(i, 0, 63) {
    if (b[i]) p[lp++] = b[i];
  }
}
void clear() {
  memset(b, 0, sizeof(b));
  lb = 0, lp = 0;
}
long long query(long long x) {
  long long res = 0;
  FOR(i, 0, 63) if (x >> i & 1) res ^= p[i];
  return res;
}
void go() {
  cin >> n;
  clear();
  bool fl = 0;
  FOR(i, 1, n) {
    long long x;
    cin >> x;
    if (!insert(x)) fl = 1;
  }
  work();
  long long lim = (1ll << lb) - 1;
  int q;
  scanf("%d", &q);
  FOR(i, 1, q) {
    long long x;
    cin >> x;
    if (fl) x--;
    if (x > lim)
      cout << -1 << endl;
    else
      cout << query(x) << endl;
  }
}
int main() {
  int t;
  cin >> t;
  FOR(i, 1, t) cout << "Case #" << i << ":" << endl, go();
  return 0;
}
