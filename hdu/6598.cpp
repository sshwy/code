#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
namespace IO {
  char nc() {
    static char bf[100000], *p1 = bf, *p2 = bf;
    return p1 == p2 && (p2 = (p1 = bf) + fread(bf, 1, 100000, stdin), p1 == p2)
               ? EOF
               : *p1++;
  }
  int rd() {
    int res = 0;
    char c = getchar();
    while (!isdigit(c)) c = getchar();
    while (isdigit(c)) res = res * 10 + c - '0', c = getchar();
    return res;
  }
} // namespace IO
/******************heading******************/
#define int lld
const int N = 2e6 + 6, INF = 0x3f3f3f3f;

struct qxx {
  int nex, t, v;
};
qxx e[N];
int h[N], cnt = 1;
void add_path(int f, int t, int v) { e[++cnt] = (qxx){h[f], t, v}, h[f] = cnt; }
void add_flow(int f, int t, int v) { add_path(f, t, v), add_path(t, f, 0); }

namespace DINIC {
  int s, t, maxflow, d[N];
  bool bfs() {
    memset(d, 0, sizeof(d));
    queue<int> q;
    q.push(s), d[s] = 1;
    while (q.size()) {
      int u = q.front();
      q.pop();
      for (int i = h[u]; i; i = e[i].nex) {
        const int &v = e[i].t, &w = e[i].v;
        if (!d[v] && w) d[v] = d[u] + 1, q.push(v);
      }
    }
    return d[t];
  }
  int dinic(int u, int flow) {
    if (u == t) return flow;
    int k, rest = flow;
    for (int i = h[u]; i && rest; i = e[i].nex) {
      const int &v = e[i].t, &w = e[i].v;
      if (!w || d[v] != d[u] + 1) continue;
      k = dinic(v, min(rest, w));
      if (k)
        e[i].v -= k, e[i ^ 1].v += k, rest -= k;
      else
        d[v] = 0;
    }
    return flow - rest;
  }
  void go() {
    while (bfs())
      for (int i; i = dinic(s, INF);) maxflow += i;
  }
  void clear() {
    memset(h, 0, sizeof(h)), cnt = 1;
    maxflow = 0;
  }
} // namespace DINIC
int n, m;
void go() {
  DINIC::clear();
  DINIC::s = 0, DINIC::t = n + 1;
  int tot = 0;
  FOR(i, 1, m) {
    int u, v, a, b, c;
    scanf("%lld%lld%lld%lld%lld", &u, &v, &a, &b, &c);
    tot += a + b + c;
    add_flow(DINIC::s, u, b + c);
    add_flow(DINIC::s, v, b + c);
    add_flow(u, DINIC::t, a + b);
    add_flow(v, DINIC::t, a + b);
    add_flow(u, v, a + c - b - b);
    add_flow(v, u, a + c - b - b);
  }
  DINIC::go();
  printf("%lld\n", tot - DINIC::maxflow / 2);
}
signed main() {
  while (~scanf("%lld%lld", &n, &m)) go();
  return 0;
}
