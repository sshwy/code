// by Yao
#include <iostream>
#include <string>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

int f(char c) {
  if ('A' <= c && c <= 'Z')
    return c - 'A' + 1;
  else
    return 'a' - c - 1;
}

int main() {
  int t;
  cin >> t;
  FOR(i, 1, t) {
    string s;
    int x;
    cin >> s >> x;
    cout << f(s[0]) + x << endl;
  }
  return 0;
}
