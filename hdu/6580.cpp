#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
namespace IO {
  char nc() {
    static char bf[100000], *p1 = bf, *p2 = bf;
    return p1 == p2 && (p2 = (p1 = bf) + fread(bf, 1, 100000, stdin), p1 == p2)
               ? EOF
               : *p1++;
  }
  int rd() {
    int res = 0;
    char c = getchar();
    while (!isdigit(c)) c = getchar();
    while (isdigit(c)) res = res * 10 + c - '0', c = getchar();
    return res;
  }
} // namespace IO
/******************heading******************/

int main() { return 0; }
/*
 * 当前走过的大概是长这样子的
 * ooooooooooo..........
 * ..........ooo........
 * ......oooooo.........
 * ..........o..........
 * .....ooooooooooo.....
 * ........ooo..........
 * ..........o..........
 * ..........oooO.......
 * ..........v..........
 * ..........v..........
 * ..........v..........
 * 下一个选择的牛奶有三种情况：
 * 1. 在当前行的当前方向的未走过的格子
 * 2. 在当前行另一边未走过的格子
 * 3. 在未走过的行上
 * 4. 在走过的行的未走过的格子上
 * 5. 在走过的格子上
 *
 * 5.在走过的格子上：直接开一个堆
 * 4.走过的行未走过的格子：每一行的对应方向延伸到的位置显然是一个牛奶。于是记录一个牛奶左边和右边第一瓶牛奶的下标。在吃它的时侯，就把沿途的牛奶标记为类型5。
 * 3.在未走过的行上：思路相似，沿途标记为类型5，对应行标记为类型4。
 *
 */
