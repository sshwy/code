// by Yao
#include <algorithm>
#include <iostream>
#include <string>
#include <vector>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

int n;

void go() {
  int ans = 0;
  vector<string> v;
  cin >> n;
  string s;
  FOR(i, 1, n) {
    cin >> s;
    v.pb(s);
  }
  int ls = s.size();
  FOR(i, 0, ls - 1) {
    FOR(j, 1, ls - i) {
      string t = s.substr(i, j), rt = t;
      reverse(rt.begin(), rt.end());
      bool flag = true;
      for (string e : v) {
        bool fl = false;
        for (unsigned k = 0; k + t.size() <= e.size(); ++k) {
          string es = e.substr(k, t.size());
          if (es == t || es == rt) {
            fl = true;
            break;
          }
        }
        if (fl == false) {
          flag = false;
          break;
        }
      }
      if (flag) {
        // cout << t << endl;
        ans = max(ans, j);
      }
    }
  }
  cout << ans << endl;
}

int main() {
  int t;
  cin >> t;
  FOR(i, 1, t) go();
  return 0;
}
