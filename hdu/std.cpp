#include <bits/stdc++.h>
#define ll long long
#define inf 0x3f3f3f3f
#define rep(i, a, b) for (int i = (a); i <= (b); i++)
#define dep(i, a, b) for (int i = (a); i >= (b); i--)
#define ls (rt << 1)
#define rs (rt << 1 | 1)
using namespace std;
const int maxn = 4e1 + 5;
// const double pi=acos(-1.0);
// const double eps=1e-9;
// const ll mo=1e9+7;
ll l, r, d;
int a[maxn];
template <typename T> inline void read(T &X) {
  X = 0;
  int w = 0;
  char ch = 0;
  while (!isdigit(ch)) {
    w |= ch == '-';
    ch = getchar();
  }
  while (isdigit(ch)) X = (X << 3) + (X << 1) + (ch ^ 48), ch = getchar();
  if (w) X = -X;
}
ll dp[22][22];
ll c[maxn][maxn];
int cnt[maxn];
ll dfs(int pos, int lmt, int qd) {
  if (pos == -1) {
    rep(i, 0, 9) {
      if (i != d && cnt[i] >= cnt[d]) return 0;
    }
    return 1;
  }
  if (!lmt && !qd) {
    ll ans = 0;
    int mx = cnt[d];
    rep(i, 0, 9) if (i != d) mx = max(mx, cnt[i] + 1);
    for (int num = mx; num <= cnt[d] + pos + 1; num++) { // cnt[d]
      memset(dp, 0, sizeof(dp));                         //
      dp[0][0] = 1;                                      //
      for (int i = 1; i <= 10; i++) {
        if (i - 1 == d) {
          rep(j, 0, 20) dp[i][j] = dp[i - 1][j];
          continue;
        }
        for (int j = 0; j <= cnt[d] + pos + 1 - num; j++) {
          for (int k = 0; k <= j && k <= num - cnt[i - 1] - 1; k++) {
            dp[i][j] += dp[i - 1][j - k] * c[pos + 1 - (j - k)][k];
          }
        }
      }
      ans += dp[10][cnt[d] + pos + 1 - num];
    }
    return ans;
  }
  int up = lmt ? a[pos] : 9;
  ll ans = 0;
  rep(i, 0, up) {
    if (!qd || i) cnt[i]++;
    ans += dfs(pos - 1, lmt && i == a[pos], qd && i == 0);
    if (!qd || i) cnt[i]--;
  }
  return ans;
}
ll cal(ll x) {
  int len = 0;
  while (x) {
    a[len++] = x % 10;
    x /= 10;
  }
  return dfs(len - 1, 1, 1);
}
void solve() {
  read(l);
  read(r);
  read(d);
  ll ans = cal(r) - cal(l - 1);
  printf("%lld\n", ans);
}
int main() {
  /*
  #ifdef ONLINE_JUDGE
  #else
      freopen("D:/Temp/in.txt", "r", stdin);
  #endif
  */
  // freopen("e://duipai//myout.txt","w",stdout);
  int T = 1, cas = 1;
  read(T);
  c[0][0] = 1;
  rep(i, 1, 25) {
    c[i][0] = 1;
    rep(j, 1, i) c[i][j] = c[i - 1][j - 1] + c[i - 1][j];
  }
  // cout << c[4][2] << endl;
  while (T--) { solve(); }
  return 0;
}
