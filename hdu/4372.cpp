#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int P = 1e9 + 7;
const int N = 2e3 + 5;

long long s[N][N], c[N][N];

int main() {
  s[0][0] = 1;
  FOR(i, 1, 2000) {
    s[i][0] = 0;
    c[i][0] = 1;
    FOR(j, 1, i - 1)
    s[i][j] = (s[i - 1][j - 1] + s[i - 1][j] * (i - 1)) % P,
    c[i][j] = (c[i - 1][j - 1] + c[i - 1][j]) % P;
    s[i][i] = 1;
    c[i][i] = 1;
  }
  int t, n, x, y;
  scanf("%d", &t);
  FOR(i, 1, t) {
    scanf("%d%d%d", &n, &x, &y);
    if (x + y - 2 > n - 1)
      puts("0");
    else
      printf("%lld\n", s[n - 1][x + y - 2] * c[x + y - 2][x - 1] % P);
  }
  return 0;
}
