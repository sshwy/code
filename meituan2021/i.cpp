// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

typedef long long LL;
const LL INF = 0x3f3f3f3f;
struct Frac {
  LL p, q;
  Frac() { p = 0, q = 1; }
  Frac(LL x) { p = x, q = 1; }
  Frac(LL _p, LL _q) { p = _p, q = _q; }
  static LL gcd(LL a, LL b) { return b ? gcd(b, a % b) : a; }
  Frac zip() {
    if (p == INF || q == INF) return *this;
    LL g = gcd(p, q);
    p /= g, q /= g;
    if (q < 0) q = -q, p = -p;
    return *this;
  }
  Frac operator+(Frac x) { return Frac(p * x.q + q * x.p, q * x.q).zip(); }
  Frac operator-() { return Frac(-p, q); }
  Frac operator-(Frac x) { return *this + (-x); }
  Frac operator*(Frac x) { return Frac(p * x.p, q * x.q).zip(); }
  Frac operator/(Frac x) { return Frac(p * x.q, q * x.p).zip(); }
  friend Frac operator/(LL x, Frac y) { return Frac(x * y.q, y.p).zip(); }
  Frac operator/(LL x) { return Frac(p, q * x).zip(); }
  bool operator==(Frac x) const {
    return (p == x.p && q == x.q) || (p * x.q == q * x.p);
  }
  bool operator<(const Frac x) const { return p * x.q < q * x.p; }
  bool operator>(const Frac x) const { return p * x.q > q * x.p; }
  bool operator<=(const Frac x) const { return p * x.q <= q * x.p; }
  bool operator>=(const Frac x) const { return p * x.q >= q * x.p; }
};

Frac xl, xr;

void upd(Frac o) {
  if (o < xl) xl = o;
  if (o > xr) xr = o;
}
Frac P(Frac a, int op_id, Frac b) {
  upd(a);
  upd(b);
  Frac res;
  switch (op_id) {
    case 0:
      res = a + b;
      break;
    case 1:
      res = a - b;
      break;
    case 2:
      res = a * b;
      break;
    case 3:
      res = a / b;
      break;
  }
  upd(res);
  return res;
}

Frac t1(Frac c[], int o[]) {
  xl = xr = c[0];
  return P(P(P(c[0], o[0], c[1]), o[1], c[2]), o[2], c[3]);
}
Frac t2(Frac c[], int o[]) {
  xl = xr = c[0];
  return P(P(c[0], o[0], P(c[1], o[1], c[2])), o[2], c[3]);
}
Frac t3(Frac c[], int o[]) {
  xl = xr = c[0];
  return P(P(c[0], o[0], c[1]), o[1], P(c[2], o[2], c[3]));
}
Frac t4(Frac c[], int o[]) {
  xl = xr = c[0];
  return P(c[0], o[0], P(P(c[1], o[1], c[2]), o[2], c[3]));
}
Frac t5(Frac c[], int o[]) {
  xl = xr = c[0];
  return P(c[0], o[0], P(c[1], o[1], P(c[2], o[2], c[3])));
}

typedef Frac (*TFunc)(Frac *, int *);
TFunc funcs[] = {t1, t2, t3, t4, t5};

vector<pair<Frac, Frac>> v[30000];
vector<Frac> vs;

void go(int id) {
  if (id % 10 == 0) fprintf(stderr, "go %d, vs.size=%lu\n", id, vs.size());

  Frac a[4];
  int b[4], p[4];

  FOR(i, 0, 3) scanf("%d", b + i);
  FOR(i, 0, 3) a[i] = Frac(b[i]);

  FOR(i, 0, 3) p[i] = i;
  do {
    Frac c[] = {a[p[0]], a[p[1]], a[p[2]], a[p[3]]};
    FOR(o0, 0, 3) FOR(o1, 0, 3) FOR(o2, 0, 3) {
      int o[] = {o0, o1, o2};
      FOR(i, 0, 4) {
        Frac result = funcs[i](c, o);
        if (result == Frac(24)) {
          if (xr == Frac(144)) {
            FOR(j, 0, 2) printf("%lld %d ", c[j].p, o[j]);
            printf("%lld, tid=%d\n", c[3].p, i);
            FOR(j, 0, 3) printf("%d%c", b[j], " \n"[j == 3]);
          }
          v[id].push_back(make_pair(xl, xr));
          vs.push_back(xl);
          vs.push_back(xr);
        }
      }
    }
  } while (next_permutation(p, p + 4));
}

int t;
bool isOk(Frac l, Frac r) {
  FOR(i, 1, t) if (v[i].size()) {
    bool flag = false;
    for (auto e : v[i]) {
      if (l <= e.first && e.second <= r) {
        flag = true;
        break;
      }
    }
    if (!flag) return false;
  }
  return true;
}

int main() {
  scanf("%d", &t);
  FOR(i, 1, t) go(i);
  puts("");

  Frac ans = Frac(INF);

  sort(vs.begin(), vs.end());
  vs.erase(unique(vs.begin(), vs.end()), vs.end());

  fprintf(stderr, "vs.size = %lu\n", vs.size());

  unsigned l = 0;
  for (unsigned r = 0; r < vs.size(); r++) {
    fprintf(stderr, "r = %u\n", r);
    if (isOk(vs[l], vs[r])) {
      while (l < r && isOk(vs[l + 1], vs[r])) ++l;
      if (vs[r] - vs[l] < ans) {
        ans = vs[r] - vs[l];
        fprintf(stderr, "%.10lf\n", ans.p * 1.0 / ans.q);
      }
    }
  }

  printf("%lld / %lld\n", ans.p, ans.q);
  printf("%.10lf\n", ans.p * 1.0 / ans.q);
  return 0;
}
