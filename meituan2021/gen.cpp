// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

using namespace RA;

int main() {
  int t = 0;
  FOR(a0, 1, 13) FOR(a1, a0, 13) FOR(a2, a1, 13) FOR(a3, a2, 13)++ t;

  printf("%d\n", t);
  FOR(a0, 1, 13) FOR(a1, a0, 13) FOR(a2, a1, 13) FOR(a3, a2, 13) {
    printf("%d %d %d %d\n", a0, a1, a2, a3);
  }
  return 0;
}
