// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

const int M = 105;

int n, m, K;
long long a[M];

bool work(long long a_max) {
  long long x = a_max, a_sum = 0;
  FOR(i, 1, m) {
    a[i] = x;
    x = max(x - K, 1ll);
    a_sum += a[i];
  }
  if (a_sum > n) return false;
  FOR(i, 1, m - 1) {
    if (a[i] - a[i + 1] < K) {
      int rest = n - a_sum;
      FOR(j, 0, min(rest - 1, i)) a[i - j]++;
      return true;
    }
  }
  int rest = n - a_sum;
  FOR(j, 0, min(rest - 1, m)) a[m - j]++;
  return true;
}

void go() {
  scanf("%d%d%d", &n, &m, &K);
  if (n < m) {
    puts("-1");
    return;
  }
  if (K == 0 && n % m) {
    puts("-1");
    return;
  }

  long long l = 1, r = n;
  while (l < r) {
    long long mid = (l + r + 1) / 2;
    if (work(mid))
      l = mid;
    else
      r = mid - 1;
  }

  long long ans = 0;
  work(l);
  // FOR(i, 1, m) printf("%lld%c", a[i], " \n"[i == m]);
  FOR(i, 1, m) ans += 1ll * a[i] * a[i];
  ans = ans * m;
  ans -= 1ll * n * n;
  printf("%lld\n", ans);
}

int main() {
  int t;
  scanf("%d", &t);
  FOR(i, 1, t) go();
  return 0;
}
