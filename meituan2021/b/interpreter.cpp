#include "testlib.h"
#include <bits/stdc++.h>
using namespace std;

const int KMaxLineNum = 100000;
const int KMemorySize = 100000;
const int KCostLimit = 3000000;
const int KTimeLimit = 200000;
const int KACost = 1;
const int KBCost = 100;
const int KValueMaxDepth = 3;
const int KExecuteNum = 100;
const double KErrorRate = 0.0001;

int A[KMemorySize], B[KMemorySize];
int ti_A[KMemorySize], ti, line_num, r;
double unchanged_prob[KTimeLimit + 1];

namespace random_util {
  mt19937 rng(2);
  uniform_real_distribution<> real_dis(0.0, 1.0);
  uniform_int_distribution<> int_dis(INT_MIN, INT_MAX);
  double getRandomReal() { return real_dis(rng); }
  int getRandomInt() { return int_dis(rng); }
} // namespace random_util

int readA(int where) {
  int gap = ti - ti_A[where];
  ti_A[where] = ti;
  if (random_util::getRandomReal() > unchanged_prob[gap]) {
    A[where] = random_util::getRandomInt();
  }
  return A[where];
}

void writeA(int where, int w) {
  ti_A[where] = ti;
  A[where] = w;
}

void _assertMemory(int where) {
  ensuref(0 <= where && where < KMemorySize, "Invalid memory index %d", where);
}

void _assertLineNumber(int where) {
  ensuref(0 <= where && where < line_num, "Invalid line number %d", where);
}

void _assertBias(int bias) {
  ensuref(
      -KMemorySize <= bias && bias <= KMemorySize, "Invalid memory bias %d", bias);
}

enum class ValueType { VCONST, REG, A_POS, B_POS };

struct Value {
  ValueType type;
  int cost;
  explicit Value(ValueType _type, int _cost)
      : type(_type)
      , cost(_cost) {}
  virtual int read() = 0;
  virtual void write(int w) = 0;
};

struct ValueConst : public Value {
  int w;
  explicit ValueConst(int _w)
      : Value(ValueType::VCONST, 0)
      , w(_w) {}
  virtual int read() { return w; }
  virtual void write(int w) { ensuref(false, "You cannot write into a constant"); }
};

struct ValueReg : public Value {
  explicit ValueReg()
      : Value(ValueType::REG, 0) {}
  virtual int read() { return r; }
  virtual void write(int w) { r = w; }
};

struct ValueAPos : public Value {
  Value *pos;
  int bias;
  explicit ValueAPos(int _bias, Value *_pos)
      : Value(ValueType::A_POS, _pos->cost + KACost)
      , bias(_bias)
      , pos(_pos) {}
  virtual int read() {
    int where = pos->read() + bias;
    _assertMemory(where);
    return readA(where);
  }
  virtual void write(int w) {
    int where = pos->read() + bias;
    _assertMemory(where);
    return writeA(where, w);
  }
};

struct ValueBPos : public Value {
  Value *pos;
  int bias;
  explicit ValueBPos(int _bias, Value *_pos)
      : Value(ValueType::B_POS, _pos->cost + KBCost)
      , bias(_bias)
      , pos(_pos) {}
  virtual int read() {
    int where = pos->read() + bias;
    _assertMemory(where);
    return B[where];
  }
  virtual void write(int w) {
    int where = pos->read() + bias;
    _assertMemory(where);
    B[where] = w;
  }
};

Value *stringToValue(const string &s, int depth = 0) {
  ensuref(depth < KValueMaxDepth, "The depth of a value should not exceed %d",
      KValueMaxDepth);
  ensuref(s.length() > 0, "Invalid value %s", s.c_str());
  if (s[0] == 'A' || s[0] == 'B') {
    int n = s.length();
    ensuref(s[1] == '[' && s[n - 1] == ']' && n >= 3, "Invalid value %s", s.c_str());
    Value *pos = nullptr;
    int bias = 0;
    int where = 2;
    while (where < n - 1 && s[where] != '@' && s[where] != '[') ++where;
    if (s[where] == '@') {
      bias = stoi(s.substr(2, where - 2));
      pos = stringToValue(s.substr(where + 1, (n - 1) - (where + 1)), depth + 1);
    } else {
      pos = stringToValue(s.substr(2, n - 3), depth + 1);
    }
    _assertBias(bias);
    if (s[0] == 'A') {
      return new ValueAPos(bias, pos);
    } else {
      return new ValueBPos(bias, pos);
    }
  } else if (s[0] == 'r') {
    ensuref(s.length() == 1, "Invalid value %s", s.c_str());
    return new ValueReg();
  } else {
    return new ValueConst(stoi(s));
  }
}

Value *readValue() {
  string s = ouf.readToken();
  return stringToValue(s);
}

enum class InstructionType { GOTO, MOVE, CALC, READ, WRITE, EXIT };

int pc, inp_pos, c;
vector<int> inp_array, ans_array, oup_array;

void clearState() {
  pc = 0;
  ti = 0;
  inp_pos = 0;
  c = 0;
  oup_array.clear();
  memset(ti_A, 0x00, sizeof ti_A);
  for (int i = 0; i < KMemorySize; ++i) {
    A[i] = random_util::getRandomInt();
    B[i] = random_util::getRandomInt();
  }
}

struct Instruction {
  InstructionType type;
  int cost;
  Instruction(InstructionType _type, int _cost)
      : type(_type)
      , cost(_cost) {}
  virtual void execute() const = 0;
};

struct InstGoto : public Instruction {
  Value *cond, *pos;
  InstGoto(Value *_cond, Value *_pos)
      : Instruction(InstructionType::GOTO, _cond->cost + _pos->cost)
      , cond(_cond)
      , pos(_pos) {}
  virtual void execute() const {
    int cond_w = cond->read();
    if (cond_w)
      pc = pos->read();
    else
      ++pc;
    _assertLineNumber(pc);
  }
};

InstGoto *readGoto() {
  auto *cond = readValue();
  auto *pos = readValue();
  return new InstGoto(cond, pos);
}

struct InstMove : public Instruction {
  Value *s, *t;
  InstMove(Value *_s, Value *_t)
      : Instruction(InstructionType::MOVE, _s->cost + _t->cost)
      , s(_s)
      , t(_t) {}
  virtual void execute() const {
    int w = s->read();
    t->write(w);
    ++pc;
    _assertLineNumber(pc);
  }
};

InstMove *readMove() {
  auto *s = readValue();
  auto *t = readValue();
  return new InstMove(s, t);
}

enum class Op {
  EQ,
  LEQ,
  LQ,
  NEQ, // Boolean operators
  PLUS,
  TIMES,
  DIV,
  MINUS,
  MOD // integer operators
};

const map<string, Op> op_name_map = {{"==", Op::EQ}, {"<=", Op::LEQ}, {"<", Op::LQ},
    {"!=", Op::NEQ}, {"+", Op::PLUS}, {"*", Op::TIMES}, {"/", Op::DIV},
    {"-", Op::MINUS}, {"%", Op::MOD}};

Op stringToOp(const string &s) {
  ensuref(op_name_map.count(s), "Invalid operator %s", s.c_str());
  return op_name_map.find(s)->second;
}

int applyOp(Op op, int a, int b) {
  switch (op) {
    case Op::EQ:
      return a == b ? 1 : 0;
    case Op::LEQ:
      return a <= b ? 1 : 0;
    case Op::LQ:
      return a < b ? 1 : 0;
    case Op::NEQ:
      return a != b ? 1 : 0;
    case Op::PLUS:
      return a + b;
    case Op::TIMES:
      return a * b;
    case Op::DIV:
      return a / b;
    case Op::MINUS:
      return a - b;
    case Op::MOD:
      return a % b;
  }
  assert(0);
  exit(0);
}

struct InstCalc : public Instruction {
  Op op;
  Value *s1, *s2, *t;
  InstCalc(Op _op, Value *_s1, Value *_s2, Value *_t)
      : Instruction(InstructionType::CALC, _s1->cost + _s2->cost + _t->cost)
      , op(_op)
      , s1(_s1)
      , s2(_s2)
      , t(_t) {}
  virtual void execute() const {
    int w1 = s1->read(), w2 = s2->read();
    t->write(applyOp(op, w1, w2));
    ++pc;
    _assertLineNumber(pc);
  }
};

InstCalc *readCalc() {
  string op_name = ouf.readToken();
  Op op = stringToOp(op_name);
  auto *s1 = readValue();
  auto *s2 = readValue();
  auto *t = readValue();
  return new InstCalc(op, s1, s2, t);
}

struct InstWrite : public Instruction {
  Value *s;
  InstWrite(Value *_s)
      : Instruction(InstructionType::WRITE, _s->cost)
      , s(_s) {}
  virtual void execute() const {
    int w = s->read();
    oup_array.push_back(w);
    ++pc;
    _assertLineNumber(pc);
  }
};

InstWrite *readWrite() {
  auto *s = readValue();
  return new InstWrite(s);
}

struct InstRead : public Instruction {
  Value *t;
  InstRead(Value *_t)
      : Instruction(InstructionType::READ, _t->cost)
      , t(_t) {}
  virtual void execute() const {
    ensuref(inp_pos < inp_array.size(), "Require too many inputs");
    t->write(inp_array[inp_pos]);
    ++inp_pos;
    ++pc;
    _assertLineNumber(pc);
  }
};

InstRead *readRead() {
  auto *t = readValue();
  return new InstRead(t);
}

struct InstExit : public Instruction {
  InstExit()
      : Instruction(InstructionType::EXIT, 0) {}
  virtual void execute() const {
    ++ti;
    pc = -1;
  }
};

InstExit *readExit() { return new InstExit(); }

vector<Instruction *> code;

enum class Result { TLE, WA, AC };

std::pair<Result, int> execute() {
  clearState();
  while (pc != -1) {
    c += code[pc]->cost;
    code[pc]->execute();
    ti++;
    if (ti > KTimeLimit) { return {Result::TLE, 0}; }
  }
  if (oup_array == ans_array)
    return {Result::AC, c};
  else
    return {Result::WA, 0};
}

Instruction *readInst() {
  string inst_type = ouf.readToken();
  if (inst_type == "Goto")
    return readGoto();
  else if (inst_type == "Move")
    return readMove();
  else if (inst_type == "Calc")
    return readCalc();
  else if (inst_type == "Read")
    return readRead();
  else if (inst_type == "Write")
    return readWrite();
  else if (inst_type == "Exit")
    return readExit();
  ensuref(false, "Invalid instruction type %s", inst_type.c_str());
  exit(0);
}

void init() {
  line_num = ouf.readInt(0, KMaxLineNum);
  unchanged_prob[0] = 1.0;
  for (int i = 1; i <= KTimeLimit; ++i) {
    unchanged_prob[i] = unchanged_prob[i - 1] * (1 - KErrorRate);
  }
  int inp_num = inf.readInt(0, KTimeLimit);
  for (int i = 0; i < inp_num; ++i) {
    int w = inf.readInt();
    inp_array.push_back(w);
  }
  int oup_num = ans.readInt(0, KTimeLimit);
  for (int i = 0; i < oup_num; ++i) {
    int w = ans.readInt();
    ans_array.push_back(w);
  }
  for (int i = 0; i < line_num; ++i) { code.push_back(readInst()); }
}

int main(int argc, char *argv[]) {
  registerTestlibCmd(argc, argv);
  init();
  int max_cost = 0;
  int ac_num = 0, tle_num = 0, wa_num = 0;
  for (int t = 0; t < KExecuteNum; ++t) {
    auto result = execute();
    if (result.first == Result::AC) {
      ++ac_num;
      max_cost = max(max_cost, result.second);
    } else if (result.first == Result::WA) {
      ++wa_num;
    } else if (result.first == Result::TLE) {
      ++tle_num;
    }
  }
  auto res = ac_num == KExecuteNum && max_cost <= KCostLimit ? _ok : _wa;
  quitf(res, "AC: %d/%d, WA: %d/%d, TLE: %d/%d, Max Cost: %d", ac_num, KExecuteNum,
      wa_num, KExecuteNum, tle_num, KExecuteNum, max_cost);
}
