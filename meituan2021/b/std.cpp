// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

int n, m, a[1000];

void Read(int &x) { scanf("%d", &x); }
int main() {
  Read(n);
  Read(m);
  FOR(i, 1, n) Read(a[i]);
  FOR(i, 1, m) {
    int b[5];
    Read(b[0]);
    Read(b[1]);
    Read(b[2]);
    if (b[0] == 1)
      a[b[1]] += b[2];
    else {
      b[4] = 0;
      FOR(j, b[1], b[2]) b[4] += a[j];
      printf("%d\n", b[4]);
    }
  }
  return 0;
}
