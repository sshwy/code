// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

string s;
vector<string> v;
int main() {
  while (getline(cin, s)) v.push_back(s);
  cout << v.size() << endl;
  for (auto e : v) cout << e << endl;
  return 0;
}
