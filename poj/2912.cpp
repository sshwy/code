#include <algorithm>
#include <cstdio>
#include <cstring>
using namespace std;
const int N = 505, M = 2e3 + 5;
int n, m;

struct data {
  int a, b, c; // b表示偏移量
  void read() {
    char x;
    scanf("%d%c%d", &a, &x, &c);
    if (x == '=')
      b = 0;
    else if (x == '<')
      b = 1;
    else
      b = 2;
  }
};
data d[M];

namespace DIS {
  int f[N * 3];
  void init(int k) {
    for (int i = 0; i <= k; i++) f[i] = i;
  }
  int gf(int k) { return f[k] == k ? k : f[k] = gf(f[k]); }
  void un(int u, int v) { f[gf(u)] = gf(v); }
  bool find(int u, int v) { return gf(u) == gf(v); }
} // namespace DIS
bool go() {
  if (!~scanf("%d%d", &n, &m)) return 0;
  for (int i = 1; i <= m; i++) d[i].read();
  int err[N] = {0};
  for (int i = 0; i < n; i++) { // i是judge
    DIS::init(n * 3);
    for (int j = 1; j <= m; j++) {
      int u = d[j].a, v = d[j].c, w = d[j].b;
      if (u == i || v == i) continue;
      DIS::un(u, v + w * n);
      DIS::un(u + n, v + ((w + 1) % 3) * n);
      DIS::un(u + n + n, v + ((w + 2) % 3) * n);
      if (DIS::find(u, u + n) || DIS::find(v, v + n)) {
        err[i] = j; //出现矛盾
        break;
      }
    }
  }
  int tot = 0, ans = 0, mx = 0;
  for (int i = 0; i < n; i++) {
    if (err[i])
      mx = max(mx, err[i]);
    else
      ++tot, ans = i;
  }
  if (tot > 1)
    puts("Can not determine");
  else if (tot < 1)
    puts("Impossible");
  else
    printf("Player %d can be determined to be the judge "
           "after %d lines\n",
        ans, mx);
  return 1;
}
int main() {
  while (go())
    ;
  return 0;
}
