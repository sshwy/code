#include <cstdio>
#include <cstring>
#include <iostream>
using namespace std;
const int N = 12;
int n, m;
int s[1 << N]; //每段连续的0为偶数个的数
long long f[N][1 << N];

int main() {
  while (~scanf("%d%d", &n, &m)) {
    if (n == 0 && m == 0) break;
    // init
    memset(f, 0, sizeof(f));
    int mxs = 1 << m; //状态上限
    //预处理每段连续的0为偶数个的数
    for (int i = 0, tmp; i < mxs; i++) {
      s[i] = 1, tmp = 0;
      for (int j = 0; j < m; j++) {
        if (((1 << j) & i) == 0)
          tmp++;
        else if (tmp % 2) {
          s[i] = 0;
          break;
        }
      }
      if (tmp % 2) s[i] = 0;
    }
    // DP
    f[0][0] = 1;
    for (int i = 1; i <= n; i++)      //枚举行
      for (int j = 0; j < mxs; j++)   //考虑f[i][j]
        for (int k = 0; k < mxs; k++) // f[i-1][k]
          if ((k & j) == 0 && s[k | j]) f[i][j] += f[i - 1][k];

    printf("%lld\n", f[n][0]);
  }
  return 0;
}
