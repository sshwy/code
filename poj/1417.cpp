#include <algorithm>
#include <cstdio>
#include <cstring>
#include <vector>
using namespace std;
const int N = 1e3 + 5;
int n, p, q;

namespace DIS {
  int f[N], r[N];
  void init(int k) {
    for (int i = 0; i <= k; i++) f[i] = i, r[i] = 0;
  }
  int gf(int u) {
    if (f[u] == u) return u;
    int root = gf(f[u]);
    r[u] = r[u] ^ r[f[u]];
    return f[u] = root;
  }
  void un(int u, int v, int k) {
    int gu = gf(u), gv = gf(v);
    f[gu] = gv, r[gu] = r[u] ^ r[v] ^ k;
  }
  bool find(int u, int v) { return gf(u) == gf(v); }
} // namespace DIS

int f[N][N], pre[N][N]; // f[i,j]:前i组选j人的方案数
bool go() {
  scanf("%d%d%d", &n, &p, &q);
  if (n == 0 && p == 0 && q == 0) return 0;
  DIS::init(p + q);
  for (int i = 1; i <= n; i++) {
    int x, y;
    char op[5];
    scanf("%d%d%s", &x, &y, op);
    if (op[0] == 'y')
      DIS::un(x, y, 0);
    else
      DIS::un(x, y, 1);
  }

  int a[N][2] = {0}; //以i为根的集合的好人与坏人个数
  int b[N][2], lb = 0;
  int c[N]; //表示根节点i在b数组中的下标
  vector<int> man[N][2];
  for (int i = 1; i <= p + q; i++) a[DIS::gf(i)][DIS::r[i]]++;
  for (int i = 1; i <= p + q; i++)
    if (a[i][0]) c[i] = ++lb, b[lb][0] = a[i][0], b[lb][1] = a[i][1];
  for (int i = 1; i <= lb; i++) man[i][0].clear(), man[i][1].clear();
  for (int i = 1; i <= p + q; i++) man[c[DIS::gf(i)]][DIS::r[i]].push_back(i);
  // for(int i=1;i<=lb;i++)printf("(%d,%d)\n",b[i][0],b[i][1]);

  f[0][0] = 1;
  for (int i = 1; i <= lb; i++) {
    for (int j = 0; j <= p; j++) {
      f[i][j] = pre[i][j] = 0;
      if (j >= b[i][0] && f[i - 1][j - b[i][0]])
        f[i][j] += f[i - 1][j - b[i][0]], pre[i][j] = j - b[i][0];
      if (j >= b[i][1] && f[i - 1][j - b[i][1]])
        f[i][j] += f[i - 1][j - b[i][1]], pre[i][j] = j - b[i][1];
      // printf("f[%d,%d]:%d\n",i,j,f[i][j]);
      // printf("pre[%d,%d]:%d\n",i,j,pre[i][j]);
    }
  }

  if (f[lb][p] != 1) return puts("no"), 1;

  vector<int> ans;
  ans.clear();
  int u = p, k = lb;
  while (k != 0) {
    int cur = pre[k][u], cnt = u - cur;
    if (cnt == b[k][0])
      ans.insert(ans.end(), man[k][0].begin(), man[k][0].end());
    else
      ans.insert(ans.end(), man[k][1].begin(), man[k][1].end());
    --k, u = cur;
  }
  sort(ans.begin(), ans.end());
  for (int i = 0; i < ans.size(); i++) printf("%d\n", ans[i]);
  puts("end");
  return 1;
}
int main() {
  while (go())
    ;
  return 0;
}
/*
 * BUG#1: 并查集初始化错了，n改成p+q
 * BUG#2: 并查集路径压缩的权值合并代码写错
 * BUG#3: 没有考虑好人人数为0的情况
 */
