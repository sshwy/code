// by Yao
#include <iostream>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

string s;
char c;

int main() {

  do {
    c = getchar();
    if (c != EOF) s += c;
  } while (c != EOF);

  for (unsigned i = 0; i < s.size(); i++) {
    if (i + 2 < s.size() && s[i] == 'y' && s[i + 1] == 'o' && s[i + 2] == 'u') {
      cout << "we";
      i += 2;
    } else {
      cout << s[i];
    }
  }
  return 0;
}
