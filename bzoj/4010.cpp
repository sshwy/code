#include <bits/stdc++.h>
using namespace std;
int D;
struct graph {
  int n, m;
  int id[100001], ans[100001], ant;
  vector<int> dpc[100001];
  bool vis[100001];
  void addpath(int f, int t) {
    dpc[f].push_back(t);
    id[t]++;
  }
  void tp() {
    priority_queue<int> q;
    int tot = 0;
    for (int i = 1; i <= n; i++)
      if (!id[i]) q.push(i); // csh
    while (!q.empty()) {
      int now = q.top();
      q.pop();
      ans[++ant] = now, tot++;
      for (int i = 0; i < dpc[now].size(); i++) {
        if (!(--id[dpc[now][i]])) q.push(dpc[now][i]);
      }
    }
    if (tot < n)
      printf("Impossible!");
    else
      for (int i = ant; i >= 1; i--) printf("%d ", ans[i]);
    printf("\n");
  }

  void clear() {
    for (int i = 1; i <= n; i++) {
      dpc[i].clear();
      id[i] = 0;
    }
    ant = 0;
  }
};
graph g;
int main() {
  scanf("%d", &D);
  while (D--) {
    scanf("%d%d", &g.n, &g.m);
    g.clear();
    for (int i = 1, x, y; i <= g.m; i++) {
      scanf("%d%d", &x, &y);
      g.addpath(y, x);
    }
    g.tp();
  }
  return 0;
}
