#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
namespace IO {
  char nc() {
    static char bf[100000], *p1 = bf, *p2 = bf;
    return p1 == p2 && (p2 = (p1 = bf) + fread(bf, 1, 100000, stdin), p1 == p2)
               ? EOF
               : *p1++;
  }
  int rd() {
    int res = 0;
    char c = getchar();
    while (!isdigit(c)) c = getchar();
    while (isdigit(c)) res = res * 10 + c - '0', c = getchar();
    return res;
  }
} // namespace IO
/******************heading******************/
const lld A = 1e7 + 5, PP = 1e9 + 7;

lld P = PP;

bool bp[A];
int pn[A], lp;
lld mu[A];

lld mul(lld a, lld b) {
  a %= P, b %= P;
  if (P <= 1000000000ll) return 1ll * a * b % P;
  if (P <= 1000000000000ll)
    return (((a * (b >> 20) % P) << 20) % P + a * (b & ((1 << 20) - 1))) % P;
  lld d = floor(a * (long double)b / P);
  lld res = (a * b - d * P) % P;
  if (res < 0) res += P;
  return res;
}
lld sqr(lld x) { return mul(x, x); }
lld cub(lld x) { return mul(mul(x, x), x); }
lld pw(lld a, lld m) {
  a %= P; //!!!
  lld res = 1;
  for (; m; a = sqr(a), m >>= 1)
    if (m & 1) res = mul(res, a);
  return res;
}
lld inv6() {
  if (P == PP) return 166666668ll;
  return 833333345000000041ll;
}

void sieve(int k) {
  bp[0] = bp[1] = 1, mu[1] = 1;
  FOR(i, 2, k) {
    if (!bp[i]) pn[++lp] = i, mu[i] = -1;
    FOR(j, 1, lp) {
      int pj = pn[j];
      if (i * pj > k) break;
      bp[i * pj] = 1;
      if (i % pj == 0) {
        mu[i * pj] = 0;
        break;
      }
      mu[i * pj] = -mu[i];
    }
  }
  FOR(i, 2, k) mu[i] += mu[i - 1];
}
lld s3(lld a) {
  lld pos = 1, res = 0;
  while (pos <= a) {
    lld nex = a / (a / pos), cur = a / pos;
    res = (res + mul(mu[nex] - mu[pos - 1], cub(cur))) % P;
    pos = nex + 1;
  }
  return (res + P) % P;
}
lld s2(lld a) {
  lld pos = 1, res = 0;
  while (pos <= a) {
    lld nex = a / (a / pos), cur = a / pos;
    res = (res + mul(mu[nex] - mu[pos - 1], sqr(cur))) % P;
    pos = nex + 1;
  }
  return (res + P) % P;
}
lld ans = 0, m;
lld f(lld n) { return ((pw(m - 1, n) + (1 - (n & 1) * 2) * (m - 1)) % P + P) % P; }
lld d[50], c[50], la;
void div(lld n) {
  la = 0;
  FOR(i, 1, lp) {
    if (1ll * pn[i] * pn[i] > n) break;
    if (n % pn[i]) continue;
    d[++la] = pn[i], c[la] = 0;
    while (n % pn[i] == 0) n /= pn[i], c[la]++;
  }
  if (n > 1) d[++la] = n, c[la] = 1;
  // FOR(i,1,la)printf("(%lld,%lld)%c",d[i],c[i]," \n"[i==la]);
}
lld n, a;
void calc(lld d, lld phi) {
  // printf("calc(d=%lld,phi(n/d)=%lld)\n",d,phi);
  assert(phi >= 0);
  ans = (ans + mul(f(d), phi)) % P;
}
void dfs(lld k, lld x, lld n, lld phi_nx) { // phi(n/x)
  // printf("dfs(%d,%lld,%lld,%lld)\n",k,x,n,phi_nx);
  if (k == la + 1) return calc(x, phi_nx);
  lld t = 1ll, t2 = d[k] - 1;
  FOR(i, 0, c[k]) {
    // printf("i=%d,dk=%d\n",i,d[k]);
    dfs(k + 1, x / t, n,
        i == 0   ? phi_nx
        : i == 1 ? phi_nx * (d[k] - 1)
                 : phi_nx * t2);
    t = t * d[k];
    if (i >= 1) t2 *= d[k];
  }
}
void go() {
  scanf("%lld%lld", &n, &a);
  if (n % PP == 0)
    P = PP * PP;
  else
    P = PP;
  ans = 0;
  m = mul((s3(a) + mul(s2(a), 3) + 2) % P, inv6()) % P;
  // printf("\033[31mn=%lld,m=%lld,a=%lld\033[0m\n",n,m,a);
  div(n);
  dfs(1, n, n, 1);
  if (n % PP == 0)
    ans = mul(ans / PP, pw(n / PP, P - 2)) % PP;
  else
    ans = mul(ans, pw(n, P - 2));
  printf("%lld\n", ans);
}
int main() {
  // freopen("t1.txt","w",stdout);
  int t;
  scanf("%d", &t);
  sieve(10000000);
  FOR(i, 1, t) go();
  return 0;
}
/*
 * BUG#1:模数不一定是质数所以不能费马小定理求逆元！
 */
