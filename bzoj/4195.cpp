#include <bits/stdc++.h>
#define N 2000002
#define SIZE 2000001
using namespace std;
int t, n, tot;
int q[N][2], qnt;
char nc() {
  static char buf[100000], *p1 = buf, *p2 = buf;
  return p1 == p2 && (p2 = (p1 = buf) + fread(buf, 1, 100000, stdin), p1 == p2)
             ? EOF
             : *p1++;
}
int rd() {
  int res = 0;
  char c = nc();
  while (!isdigit(c)) c = nc();
  while (isdigit(c)) res = res * 10 + c - '0', c = nc();
  return res;
}
struct my_hash_map {
  struct data {
    int u, v, nex;
  };
  data e[SIZE];
  int head[SIZE], cnt;
  int hash(int u) { return u % SIZE; }
  int &operator[](int u) {
    int hu = hash(u);
    for (int i = head[hu]; i; i = e[i].nex)
      if (e[i].u == u) return e[i].v;
    e[++cnt] = (data){u, ++tot, head[hu]};
    return head[hu] = cnt, e[cnt].v;
  }
  void clear() {
    for (int i = 0; i <= cnt; i++) e[i].u = e[i].v = e[i].nex = 0;
    memset(head, 0, sizeof(head));
    cnt = 0;
  }
};
my_hash_map h;
struct disjoint {
  int f[N];
  void clear(int k) {
    for (int i = 0; i <= k; i++) f[i] = i;
  }
  int gf(int k) { return f[k] == k ? k : f[k] = gf(f[k]); }
  int un(int a, int b) { f[gf(a)] = gf(b); }
  int find(int a, int b) { return gf(a) == gf(b); }
};
disjoint s;
int main() {
  // freopen("bzoj4195.in","r",stdin);
  t = rd(); // scanf("%d",&t);
  while (t--) {
    n = rd(); // scanf("%d",&n);
    h.clear();
    s.clear(n * 2);
    tot = qnt = 0;
    bool f = true;
    for (int i = 1, a, b, c; i <= n; i++) {
      a = rd(), b = rd(), c = rd(); // scanf("%d%d%d",&a,&b,&c);
      if (c == 1)
        s.un(h[a], h[b]);
      else
        ++qnt, q[qnt][0] = h[a], q[qnt][1] = h[b];
    }
    for (int i = 1; i <= qnt; i++)
      if (s.find(q[i][0], q[i][1])) {
        f = false;
        break;
      }
    if (f)
      puts("YES");
    else
      puts("NO");
  }
  return 0;
}
