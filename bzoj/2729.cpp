#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
namespace IO {
  char nc() {
    static char bf[100000], *p1 = bf, *p2 = bf;
    return p1 == p2 && (p2 = (p1 = bf) + fread(bf, 1, 100000, stdin), p1 == p2)
               ? EOF
               : *p1++;
  }
  int rd() {
    int res = 0;
    char c = getchar();
    while (!isdigit(c)) c = getchar();
    while (isdigit(c)) res = res * 10 + c - '0', c = getchar();
    return res;
  }
} // namespace IO
/******************heading******************/
#define R register /*{{{*/
#define Maxl 2000
#define Base 100000000
#define Bit 8

struct Bigint {
  long long a[Maxl];
  int len;
  Bigint() {
    len = 1;
    memset(a, 0, sizeof a);
  };
  void Scan() {
    R char str[Maxl];
    scanf("%s", str);
    strin(str);
  }
  void strin(R char *p) {
    R int tmpl = strlen(p);
    for (R int i = tmpl - 1; i >= 0; i -= Bit, ++len) {
      for (R int j = 0, rem = 1; j < Bit; ++j, rem *= 10) {
        (i >= j) ? a[len] += ((*(p + i - j)) - '0') * rem : 0;
      }
    }
    --len;
    return;
  }
  void Cls() {
    while (!a[len] && len != 1) --len;
    while (a[len + 1]) ++len;
    return;
  }
  void Print() {
    Cls();
    printf("%lld", a[len]);
    for (int i = len - 1; i > 0; --i) printf("%08lld", a[i]);
    return;
  }
  int operator=(const int B) {
    len = 1;
    if (B >= Base)
      a[len] = Base, a[++len] = B % Base;
    else
      a[len] = B;
    return B;
  }
  bool operator<(const Bigint &B) {
    if (len != B.len) return len < B.len;
    for (int i = len; i > 0; --i)
      if (a[i] != B.a[i]) return a[i] < B.a[i];
    return 0;
  }
  bool operator==(const Bigint &B) {
    if (len != B.len) return 0;
    for (int i = len; i > 0; --i)
      if (a[i] != B.a[i]) return 0;
    return 1;
  }
  bool operator<=(const Bigint &B) { return ((*this) < B) || ((*this) == B); }
  bool operator>(const Bigint &B) { return !((*this) <= B); }
  bool operator>=(const Bigint &B) { return !((*this) < B); }
  bool operator!=(const Bigint &B) { return !((*this) == B); }

  bool operator<(const int &B) { return (a[2] * Base + a[1] < B); }
  bool operator==(const int &B) { return (a[2] * Base + a[1] == B); }
  bool operator<=(const int &B) { return ((*this) < B) || ((*this) == B); }
  bool operator>(const int &B) { return !((*this) <= B); }
  bool operator>=(const int &B) { return !((*this) < B); }
  bool operator!=(const int &B) { return !((*this) == B); }

  Bigint operator-(const Bigint &B) {
    Bigint ret = *this;
    for (int i = 1; i <= len && i <= B.len; ++i) {
      if (ret.a[i] < B.a[i]) ret.a[i] += Base, --ret.a[i + 1];
      ret.a[i] -= B.a[i];
    }
    ret.Cls();
    return ret;
  }
  Bigint operator+(const Bigint &B) {
    Bigint ret = *this;
    for (int i = 1; i <= len && i <= B.len; ++i) {
      if (ret.a[i] + B.a[i] >= Base) ret.a[i] -= Base, ++ret.a[i + 1];
      ret.a[i] += B.a[i];
    }
    ret.Cls();
    return ret;
  }
  Bigint operator*(const int &B) {
    Bigint ret = *this;
    for (int i = 1; i <= len; ++i) ret.a[i] *= B;
    for (int i = 1; i <= len; ++i) ret.a[i + 1] += ret.a[i] / Base, ret.a[i] %= Base;
    ret.Cls();
    return ret;
  }
  Bigint operator*(const Bigint &B) {
    Bigint ret;
    for (int i = 1; i <= len; ++i)
      for (int j = 1; j <= B.len; ++j) ret.a[i + j - 1] += a[i] * B.a[j];
    for (int i = 1; i < len + B.len - 1; ++i)
      ret.a[i + 1] += ret.a[i] / Base, ret.a[i] %= Base;
    for (ret.len = len + B.len - 1; ret.a[ret.len] > Base; ++ret.len) {
      ret.a[ret.len + 1] += ret.a[ret.len] / Base, ret.a[ret.len] %= Base;
    }
    ret.Cls();
    return ret;
  }
  Bigint operator/(const int &B) {
    Bigint ret = *this;
    for (int i = ret.len; i > 0; --i)
      ret.a[i - 1] += (ret.a[i] % B) * Base, ret.a[i] /= B;
    ret.Cls();
    return ret;
  }
  friend Bigint gcd(R Bigint xx, R Bigint yy) {
    R Bigint ret;
    ret = 1;
    while (xx != yy) {
      while (!(xx.a[1] & 1) && !(yy.a[1] & 1))
        xx = xx / 2, yy = yy / 2, ret = ret * 2;
      while (!(xx.a[1] & 1)) xx = xx / 2;
      while (!(yy.a[1] & 1)) yy = yy / 2;
      if (xx < yy) { swap(xx, yy); }
      if (xx == yy) break;
      xx = xx - yy;
    }
    ret = ret * xx;
    return ret;
  }
}; /*}}}*/
Bigint solve(int n, int m) {
  Bigint a;
  a = 1;
  FOR(i, n + 3 - m + 1, n + 3) a = a * i;
  FOR(i, 1, n) a = a * i;
  a = a * (n);
  a = a * (n + 1);
  return a;
}
int main() {
  int n, m;
  scanf("%d%d", &n, &m);
  Bigint b, c;
  b = solve(n, m);
  c = 1;
  FOR(i, n - m + 4, n + 2) c = c * i;
  FOR(i, 1, n + 1) c = c * i;
  c = c * m, c = c * 2;
  // c=c*(n+m);
  // c=c*2;
  b = b + c;
  b.Print();
  return 0;
}
