// by Sshwy
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA
const int N = 2.5e5 + 5, SZ = N << 2;
int n, m, K;

struct qxx {
  int nex, t, v;
} e[N * 2];
int h[N], le = 1;
void add_path(int f, int t, int v) { e[++le] = {h[f], t, v}, h[f] = le; }

int L[N], R[N], totdfn, vis[N], ve[N * 3], fa[N][20], dep[N], f[N];
long long d[N], de[N * 3];
struct atom {
  int a, b, x, y, c;
};
vector<atom> E;
typedef pair<int, int> seg_atom; // the other end, the id in E
const seg_atom Seg_inf_max = {1e9, 1e9}, Seg_inf_min = {-1e9, -1e9};

void dfs(int u, int p) {
  L[u] = ++totdfn, dep[u] = dep[p] + 1, fa[u][0] = p;
  FOR(j, 1, 19) {
    fa[u][j] = fa[fa[u][j - 1]][j - 1];
    if (fa[u][j] == 0) break;
  }
  for (int i = h[u]; i; i = e[i].nex) {
    int v = e[i].t;
    if (v != p) dfs(v, u);
  }
  R[u] = totdfn;
}
int lca(int u, int v) {
  if (dep[u] < dep[v]) swap(u, v);
  ROF(j, 19, 0) if (dep[u] - (1 << j) >= dep[v]) u = fa[u][j];
  if (u == v) return u;
  ROF(j, 19, 0) if (fa[u][j] != fa[v][j]) u = fa[u][j], v = fa[v][j];
  return fa[u][0];
}

priority_queue<seg_atom> qmax[N];
priority_queue<seg_atom, vector<seg_atom>, greater<seg_atom>> qmin[N];
seg_atom mx[SZ], mn[SZ];

void pushup(int u) {
  mx[u] = max(mx[u << 1], mx[u << 1 | 1]);
  mn[u] = min(mn[u << 1], mn[u << 1 | 1]);
}
void build(int u, int l, int r) {
  mx[u] = Seg_inf_min;
  mn[u] = Seg_inf_max;
  if (l == r) {
    qmax[l].push(Seg_inf_min);
    qmin[l].push(Seg_inf_max);
    return;
  }
  int mid = (l + r) >> 1;
  build(u << 1, l, mid), build(u << 1 | 1, mid + 1, r);
}
void insert(int pos, seg_atom v, int u = 1, int l = 1, int r = totdfn) {
  if (l == r) {
    qmax[l].push(v);
    qmin[l].push(v);
    mx[u] = qmax[l].top();
    mn[u] = qmin[l].top();
    return;
  }
  int mid = (l + r) >> 1;
  if (pos <= mid)
    insert(pos, v, u << 1, l, mid);
  else
    insert(pos, v, u << 1 | 1, mid + 1, r);
  pushup(u);
}
void zip(int u, int l, int r) {
  if (l == r) {
    while (qmax[l].top() != Seg_inf_min && ve[qmax[l].top().second]) qmax[l].pop();
    while (qmin[l].top() != Seg_inf_max && ve[qmin[l].top().second]) qmin[l].pop();
    mx[u] = qmax[l].top();
    mn[u] = qmin[l].top();
    return;
  }
  if (mx[u] != Seg_inf_min && ve[mx[u].second] ||
      mn[u] != Seg_inf_max && ve[mn[u].second]) {
    int mid = (l + r) >> 1;
    zip(u << 1, l, mid);
    zip(u << 1 | 1, mid + 1, r);
    pushup(u);
  }
}
seg_atom qrymax(int LL, int RR, int u = 1, int l = 1, int r = totdfn) {
  zip(u, l, r);
  if (RR < l || r < LL || LL > RR) return Seg_inf_min;
  if (LL <= l && r <= RR) { return mx[u]; }
  int mid = (l + r) >> 1;
  return max(qrymax(LL, RR, u << 1, l, mid), qrymax(LL, RR, u << 1 | 1, mid + 1, r));
}
seg_atom qrymin(int LL, int RR, int u = 1, int l = 1, int r = totdfn) {
  zip(u, l, r);
  if (RR < l || r < LL || LL > RR) return Seg_inf_max;
  if (LL <= l && r <= RR) return mn[u];
  int mid = (l + r) >> 1;
  return min(qrymin(LL, RR, u << 1, l, mid), qrymin(LL, RR, u << 1 | 1, mid + 1, r));
}
typedef pair<long long, int> pli;
priority_queue<pli, vector<pli>, greater<pli>> q;
vector<int> elca[N];

void expand_node(int u) {
  seg_atom t;
  while (1) {
    while (elca[u].size() && ve[elca[u].back()]) elca[u].pop_back();
    if (elca[u].empty()) break;
    int e = elca[u].back();
    de[e] = d[u] + E[e].c;
    assert(ve[e] == 0);
    ve[e] = 1;
    q.push({de[e], e});
  }
  while (1) {
    t = qrymin(L[u], R[u]);
    if (t == Seg_inf_max) break;
    if (t.first < L[u]) { // cross u
      int e = t.second;
      de[e] = d[u] + E[e].c;
      assert(ve[e] == 0);
      ve[e] = 1;
      q.push({de[e], e});
    } else
      break;
  }
  while (1) {
    t = qrymax(L[u], R[u]);
    if (t == Seg_inf_min) break;
    if (t.first > R[u]) { // cross u
      int e = t.second;
      de[e] = d[u] + E[e].c;
      assert(ve[e] == 0);
      ve[e] = 1;
      q.push({de[e], e});
    } else
      break;
  }
}
int get(int u) { return f[u] == u ? u : f[u] = get(f[u]); }
void merge_to(int u, int v) { f[get(u)] = get(v); }
void expand_edge(int e) {
  int u = E[e].x, v = E[e].y;
  int z = lca(u, v);
  while (1) {
    u = get(u);
    if (dep[u] < dep[z]) break;
    assert(vis[u] == 0);
    vis[u] = 1;
    d[u] = de[e];
    q.push({d[u], -u});
    merge_to(u, fa[u][0]);
  }
  while (1) {
    v = get(v);
    if (dep[v] < dep[z]) break;
    assert(vis[v] == 0);
    vis[v] = 1;
    d[v] = de[e];
    q.push({d[v], -v});
    merge_to(v, fa[v][0]);
  }
}

int main() {
  scanf("%d%d%d", &n, &m, &K);
  FOR(i, 1, n - 1) {
    int x, y, t;
    scanf("%d%d%d", &x, &y, &t);
    add_path(x, y, t);
    add_path(y, x, t);

    E.pb({x, x, y, y, t});
    E.pb({y, y, x, x, t});
  }
  dfs(1, 0);
  build(1, 1, totdfn);
  FOR(i, 1, n) f[i] = i;
  FOR(i, 1, m) {
    int a, b, x, y, c;
    scanf("%d%d%d%d%d", &a, &b, &x, &y, &c);
    E.pb({x, y, a, b, c});
  }
  for (int i = 0; i < E.size(); ++i) {
    int z = lca(E[i].a, E[i].b);
    elca[z].pb(i);
    insert(L[E[i].a], {L[E[i].b], i});
    insert(L[E[i].b], {L[E[i].a], i});
  }
  memset(d, 0x3f, sizeof(d));
  memset(de, 0x3f, sizeof(de));

  d[K] = 0;
  q.push({d[K], -K});
  vis[K] = 1;
  merge_to(K, fa[K][0]);

  while (!q.empty()) {
    pli t = q.top();
    q.pop();
    if (t.second < 0) {
      int u = -t.second;
      expand_node(u);
    } else {
      int e = t.second;
      expand_edge(e);
    }
  }
  FOR(i, 1, n) printf("%lld\n", d[i]);
  return 0;
}
