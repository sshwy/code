#include <bits/stdc++.h>
#define endl '\n'
using namespace std;
typedef long long ll;
int read() {
  int out = 0, sgn = 1;
  char jp = getchar();
  while (jp != '-' && (jp < '0' || jp > '9')) jp = getchar();
  if (jp == '-') sgn = -1, jp = getchar();
  while (jp >= '0' && jp <= '9') out = out * 10 + jp - '0', jp = getchar();
  return out * sgn;
}
const int P = 1e9 + 7;
int add(int a, int b) { return a + b >= P ? a + b - P : a + b; }
void inc(int &a, int b) { a = add(a, b); }
int mul(int a, int b) { return 1LL * a * b % P; }
int fpow(int a, int b) {
  int res = 1;
  while (b) {
    if (b & 1) res = mul(res, a);
    a = mul(a, a);
    b >>= 1;
  }
  return res;
}
const int N = 500 + 10, M = 1e6 + 10;
int n, k, a[N], rt, ch[N][2];
pair<int, int> t[N];
set<int> s;
set<int>::iterator it;
int f[N][N], siz[N], tmp[N], fac[M], invfac[M];
int binom(int x, int y) {
  if (x < 0 || y < 0 || x < y) return 0;
  return mul(fac[x], mul(invfac[y], invfac[x - y]));
}
void dfs(int u, int fa) {
  f[u][0] = 1, siz[u] = 0;
  for (int dir = 0; dir < 2; ++dir) {
    int v = ch[u][dir];
    if (!v) continue;
    dfs(v, u);
    for (int i = 0; i <= k && i <= siz[u]; ++i)
      if (f[u][i])
        for (int j = 0; i + j <= k && j <= siz[v]; ++j)
          if (f[v][j]) inc(tmp[i + j], mul(f[u][i], f[v][j]));
    siz[u] += siz[v];
    for (int i = 0; i <= k && i <= siz[u]; ++i) f[u][i] = tmp[i], tmp[i] = 0;
  }
  int w = siz[u] + 1, h = a[u] - a[fa];
  for (int j = 0; j <= k && j <= siz[u] && j <= w; ++j)
    for (int i = 0; i + j <= k && i <= w - j && i <= h; ++i) {
      int c = mul(fac[i], mul(binom(w - j, i), binom(h, i)));
      inc(tmp[i + j], mul(c, f[u][j]));
    }
  ++siz[u];
  for (int i = 0; i <= k && i <= siz[u]; ++i) f[u][i] = tmp[i], tmp[i] = 0;
}
int main() {
  n = read(), k = read();
  int mx = n;
  for (int i = 1; i <= n; ++i) {
    a[i] = read();
    t[i] = make_pair(a[i], i);
    mx = max(mx, a[i]);
  }
  fac[0] = 1;
  for (int i = 1; i <= mx; ++i) fac[i] = mul(fac[i - 1], i);
  invfac[mx] = fpow(fac[mx], P - 2);
  for (int i = mx - 1; i >= 0; --i) invfac[i] = mul(invfac[i + 1], i + 1);
  sort(t + 1, t + 1 + n);
  rt = t[1].second, s.insert(t[1].second);
  for (int i = 2; i <= n; ++i) {
    int x = t[i].second;
    it = s.lower_bound(x);
    if (it != s.end() && !ch[*it][0])
      ch[*it][0] = x;
    else
      ch[*(--it)][1] = x;
    s.insert(x);
  }
  dfs(rt, 0);
  printf("%d\n", f[rt][k]);
  return 0;
}
