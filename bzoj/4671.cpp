#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int K = 650, N = 102;
typedef long long LL;

int k, n, m;
LL g[K];
LL f[N], fac[N];
char str[K];

int col[N];

LL b[N];
bool insert(LL x) {
  ROF(i, 60, 0) if (x >> i & 1) {
    if (!b[i]) return b[i] = x, 1;
    x ^= b[i];
  }
  return 0;
}
void calc(int cnt) {
  int pos = 0;
  LL mask = 0;
  FOR(i, 1, n) {
    FOR(j, i + 1, n) {
      ++pos;
      if (col[i] != col[j]) mask |= 1ll << pos; //不在同一个连通块中
    }
  }
  assert(pos == m);
  memset(b, 0, sizeof(b));
  int tot = 0;
  FOR(i, 1, k) {
    if (insert(g[i] & mask)) ++tot;
  }
  tot = k - tot;
  f[cnt] += 1ll << tot;
}
void dfs(int x, int y) {
  if (x == n) return calc(y), void();
  FOR(i, 1, y) {
    col[x + 1] = i;
    dfs(x + 1, y);
  }
  col[x + 1] = y + 1;
  dfs(x + 1, y + 1);
}

int main() {
  scanf("%d", &k);
  FOR(i, 1, k) {
    scanf("%s", str);
    m = strlen(str);
    n = sqrt(m * 2) + 1;
    FOR(j, 1, m) g[i] |= (str[j - 1] - '0' + 0ll) << j; // g[i][j]=g[i]>>j&1
    // BUG#1:不加0ll 爆读入了
  }
  dfs(0, 0);
  fac[0] = 1;
  FOR(i, 1, n) fac[i] = fac[i - 1] * i;
  LL ans = 0;
  // FOR(i,1,n)printf("%d%c",f[i]," \n"[i==n]);
  FOR(i, 1, n) ans += (1 - 2 * ((i - 1) & 1)) * fac[i - 1] * f[i];
  printf("%lld\n", ans);
  return 0;
}
