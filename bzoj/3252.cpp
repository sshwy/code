#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 2e5 + 5;

struct qxx {
  int nex, t;
};
qxx e[N * 2];
int h[N], le = 1;
void add_path(int f, int t) { e[++le] = (qxx){h[f], t}, h[f] = le; }
#define FORe(i, u, v) for (int i = h[u], v; v = e[i].t, i; i = e[i].nex)

int n, k;
long long a[N];

void dfs(int u, int p) {
  long long mx = 0, son = -1;
  FORe(i, u, v) if (v != p) dfs(v, u), a[v] > mx ? mx = a[v], son = v : 0;
  if (son != -1) a[u] += a[son], a[son] = 0;
}
int main() {
  scanf("%d%d", &n, &k);
  FOR(i, 1, n) scanf("%lld", &a[i]);
  FOR(i, 1, n - 1) {
    int x, y;
    scanf("%d%d", &x, &y);
    add_path(x, y), add_path(y, x);
  }
  dfs(1, 0);
  sort(a + 1, a + n + 1);
  reverse(a + 1, a + n + 1);
  FOR(i, 1, k) a[0] += a[i];
  printf("%lld\n", a[0]);
  return 0;
}
