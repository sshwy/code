#include <bits/stdc++.h>
using namespace std;

#define ll long long
#define lf long double
#define ull unsigned long long

void read(ll &x) {
  x = 0;
  ll f = 1;
  char ch = getchar();
  for (; !isdigit(ch); ch = getchar())
    if (ch == '-') f = -f;
  for (; isdigit(ch); ch = getchar()) x = x * 10 + ch - '0';
  x *= f;
}

void print(int x) {
  if (x < 0) putchar('-'), x = -x;
  if (!x) return;
  print(x / 10), putchar(x % 10 + 48);
}
void write(int x) {
  if (!x)
    putchar('0');
  else
    print(x);
  putchar('\n');
}

#define sqr(x) mul(x, x)
#define cul(x) mul(sqr(x), x)

const int maxn = 1e7 + 10;
const int MOD = 1e9 + 7;
const int MAXN = 1e4 + 10;

ll mod;

int pri[maxn], vis[maxn], mu[maxn], tot;

void sieve(int N) {
  mu[1] = 1;
  for (int i = 2; i <= N; i++) {
    if (!vis[i]) pri[++tot] = i, mu[i] = -1;
    for (int j = 1; j <= tot && i * pri[j] <= N; j++) {
      vis[i * pri[j]] = 1;
      if (i % pri[j] == 0) break;
      mu[i * pri[j]] = -mu[i];
    }
  }
  for (int i = 1; i <= N; i++) mu[i] += mu[i - 1];
}

ll mul(ll a, ll b) {
  lf res = (lf)a * b / mod;
  ll t = a * b, t2 = (t - (ull)res * mod + mod) % mod;
  return t2;
}

ll qpow(ll a, ll x) {
  ll res = 1;
  for (; x; x >>= 1, a = mul(a, a))
    if (x & 1) res = mul(res, a);
  return res;
}

ll a[MAXN], p[MAXN], cnt;

ll m, ans, inv6, nown;

void mobius(int n) {
  m = 0;
  int T = 1;
  while (T <= n) {
    int pre = T;
    T = n / (n / T);
    m = (m + 1ll * mul((mu[T] - mu[pre - 1]), cul(n / T))) % mod;
    m = (m + 1ll * mul((mu[T] - mu[pre - 1]), sqr(n / T)) * 3ll % mod) % mod;
    T++;
  }
  m = (m + 2) % mod;
  cout << "gg=" << m << endl;
  m = mul(m, inv6) % mod;
  cout << "m=" << m << endl;
}

void prepare(ll n) {
  cnt = 0;
  for (int i = 2; 1ll * i * i <= n; i++) {
    if (n % i) continue;
    a[++cnt] = i;
    p[cnt] = 0;
    while (n % i == 0) n /= i, p[cnt]++;
  }
  if (n != 1) a[++cnt] = n, p[cnt] = 1;
}

ll f(ll n) {
  ll Ans = qpow(m - 1, n);
  if (n & 1)
    Ans = (Ans + mod - m + 1) % mod;
  else
    Ans = (Ans + m - 1) % mod;
  return Ans;
}

void polya(int now, ll d, ll phi) {
  if (now == cnt + 1) {
    cout << "phi=" << phi << ",d=" << d << endl;
    return ans = (ans + mul(phi, f(nown / d))) % mod, cout << "ans=" << ans << endl,
           void();
  }
  polya(now + 1, d, phi);
  d *= a[now], phi *= a[now] - 1;
  polya(now + 1, d, phi);
  for (int i = 2; i <= p[now]; i++)
    d *= a[now], phi *= a[now], polya(now + 1, d, phi);
}

ll inn[20], ina[20], mx, inv[10];

void pre_inv() {
  inv[1] = 1;
  for (int i = 2; i <= 6; i++) inv[i] = mul(mod - mod / i, inv[mod % i]);
  inv6 = inv[6];
}

int main() {
  // freopen("t2.txt","w",stdout);
  int t;
  scanf("%d", &t);
  for (int i = 1; i <= t; i++) read(inn[i]), read(ina[i]), mx = max(mx, ina[i]);
  sieve(mx);
  for (int i = 1; i <= t; i++) {
    nown = inn[i];
    if (inn[i] % MOD)
      mod = MOD;
    else
      mod = 1ll * MOD * MOD;
    pre_inv();
    mobius(ina[i]);
    prepare(inn[i]);
    ans = 0;
    polya(1, 1, 1);
    if (inn[i] % MOD)
      ans = mul(ans, qpow(inn[i], mod - 2));
    else
      ans = mul(ans / MOD, qpow(inn[i] / MOD, MOD - 2)) % MOD;
    cout << "n=" << inn[i] << endl;
    write(ans);
  }
  return 0;
}
