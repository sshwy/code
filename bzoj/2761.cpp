#include <bits/stdc++.h>
#define SZ 100007
#define N 50001
using namespace std;
int t, n;
struct my_hash_map {
  struct data {
    int u, v, nex;
  };
  data e[SZ];
  int head[SZ], cnt;
  int hash(int u) { return (u % SZ + SZ) % SZ; }
  int &operator[](int u) {
    int hu = hash(u);
    for (int i = head[hu]; i; i = e[i].nex)
      if (e[i].u == u) return e[i].v;
    e[++cnt] = (data){u, 0, head[hu]};
    head[hu] = cnt;
    return e[cnt].v;
  }
  void clear() {
    memset(e, 0, sizeof(e));
    memset(head, 0, sizeof(head));
    cnt = 0;
  }
};
my_hash_map h;
int main() {
  scanf("%d", &t);
  while (t--) {
    h.clear();
    scanf("%d", &n);
    for (int i = 1, a; i <= n; i++) {
      scanf("%d", &a);
      if (h[a] == 0) {
        printf("%d ", a);
        h[a] = 1;
      }
    }
    printf("\n");
  }
  return 0;
}
