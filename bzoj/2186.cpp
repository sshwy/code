#include <cstdio>
using namespace std;
const int N = 1e7 + 5;

int t, r, n, m;
int vis[N], p[N / 10], lp;
void sieve(int k) {
  vis[0] = vis[1] = 1;
  for (int i = 2; i <= k; i++) {
    if (!vis[i]) p[++lp] = i;
    for (int j = 1; j <= lp; j++) {
      if (i * p[j] > k) break;
      vis[i * p[j]] = 1;
      if (i % p[j] == 0) break;
    }
  }
}
int fac[N], inv[N], fp[N]; // fac phi
int main() {
  scanf("%d%d", &t, &r);

  sieve(1e7);
  fac[0] = 1;
  for (int i = 1; i <= 10000000; i++) fac[i] = 1ll * i * fac[i - 1] % r;
  inv[1] = 1;
  for (int i = 2; i <= 10000000; i++) inv[i] = (1ll * (r - r / i) * inv[r % i] % r);
  fp[1] = 1;
  for (int i = 2; i <= 10000000; i++) {
    fp[i] = fp[i - 1];
    if (vis[i] == 0) fp[i] = 1ll * fp[i] * (i - 1) % r * inv[i] % r;
  }

  while (t--) {
    scanf("%d%d", &n, &m);
    printf("%lld\n", 1ll * fac[n] * fp[m] % r);
  }
  return 0;
}
