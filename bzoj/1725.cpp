#include <bits/stdc++.h>
#define P 100000000
using namespace std;
int n, m, cnt, tot;
int s[1 << 12];
int field[15];
int f[13][1 << 12];
int main() {
  scanf("%d%d", &n, &m);
  for (int i = 1; i <= n; i++) {
    for (int j = 1, a; j <= m; j++) {
      scanf("%d", &a);
      field[i] = field[i] << 1 | (!a);
    }
  }
  int maxi = 1 << m;
  for (int i = 0; i < maxi; i++) {
    if ((i & (i << 1)) == 0 && (i & (i >> 1)) == 0) { s[++cnt] = i; }
  }
  for (int i = 1; i <= cnt; i++) {
    if ((s[i] & field[1]) == 0) { f[1][s[i]] = 1; }
  }
  for (int i = 2; i <= n; i++) {
    for (int j = 1; j <= cnt; j++) {
      if ((s[j] & field[i]) == 0) {
        for (int k = 1; k <= cnt; k++) {
          if ((s[j] & s[k]) == 0) {
            f[i][s[j]] += f[i - 1][s[k]];
            f[i][s[j]] %= P;
          }
        }
      }
    }
  }
  for (int i = 1; i <= cnt; i++) { tot = (tot + f[n][s[i]]) % P; }
  printf("%d", tot);
  return 0;
}
