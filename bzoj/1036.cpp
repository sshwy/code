#include <bits/stdc++.h>
#define N 30001
using namespace std;
int n, q;
int w[N], wct;
bool vis[N];            // use in build_tree
int s[N * 4], m[N * 4]; // LST sum & max

struct data {
  int t, nex;
};
data e[N * 2];
int head[N], cnt;
void add_path(int f, int t) { e[++cnt] = (data){t, head[f]}, head[f] = cnt; }

struct node {
  int p, s, b;        // par,son,bro
  int hv, dp, tp, sz; // heavy,deep,top,size
  int iw;             // index in w[]
};
node t[30001];

int build_tree(int rt, int dp) {
  if (vis[rt]) return 0;
  vis[rt] = 1, t[rt].dp = dp;
  for (int i = head[rt]; i; i = e[i].nex) {
    if (!vis[e[i].t]) {
      t[e[i].t].p = rt, t[e[i].t].b = t[rt].s, t[rt].s = e[i].t;
      t[rt].sz += build_tree(e[i].t, dp + 1);
    }
  }
  return ++t[rt].sz;
}
int build_link(int rt, int tp) {
  t[rt].tp = tp, t[rt].iw = ++wct;
  if (!t[rt].s) return 0;
  for (int i = t[rt].s, mx = 0; i; i = t[i].b)
    if (t[i].sz > mx) mx = t[i].sz, t[rt].hv = i;
  build_link(t[rt].hv, tp);
  for (int i = t[rt].s, mx = 0; i; i = t[i].b)
    if (i != t[rt].hv) build_link(i, i);
}

void push_up(int rt) {
  s[rt] = s[rt << 1] + s[rt << 1 | 1], m[rt] = max(m[rt << 1], m[rt << 1 | 1]);
}
int build_lst(int l, int r, int rt) {
  if (l == r) return s[rt] = m[rt] = w[l];
  int mid = (l + r) >> 1;
  build_lst(l, mid, rt << 1), build_lst(mid + 1, r, rt << 1 | 1);
  push_up(rt);
}
int change(int p, int l, int r, int rt, int v) {
  if (p == l && p == r) return s[rt] = m[rt] = v;
  int mid = (l + r) >> 1;
  if (l <= p && p <= mid) change(p, l, mid, rt << 1, v);
  if (mid < p && p <= r) change(p, mid + 1, r, rt << 1 | 1, v);
  push_up(rt);
}
int query_sum(int L, int R, int l, int r, int rt) {
  if (L <= l && r <= R) return s[rt];
  int mid = (l + r) >> 1, su = 0;
  if (!(mid < L || R < l)) su += query_sum(L, R, l, mid, rt << 1);
  if (!(r < L || R < mid + 1)) su += query_sum(L, R, mid + 1, r, rt << 1 | 1);
  return su;
}
int query_max(int L, int R, int l, int r, int rt) {
  if (L <= l && r <= R) return m[rt];
  int mid = (l + r) >> 1, mx = (1 << 31);
  if (!(mid < L || R < l)) mx = max(mx, query_max(L, R, l, mid, rt << 1));
  if (!(r < L || R < mid + 1)) mx = max(mx, query_max(L, R, mid + 1, r, rt << 1 | 1));
  return mx;
}

int query_tree_max(int a, int b) {
  int res = (1 << 31);
  while (t[a].tp != t[b].tp) {
    if (t[t[a].tp].dp < t[t[b].tp].dp) a ^= b ^= a ^= b;
    res = max(res, query_max(t[t[a].tp].iw, t[a].iw, 1, n, 1));
    a = t[t[a].tp].p;
  }
  if (t[a].iw > t[b].iw) a ^= b ^= a ^= b;
  return max(res, query_max(t[a].iw, t[b].iw, 1, n, 1));
}
int query_tree_sum(int a, int b) {
  int res = 0;
  while (t[a].tp != t[b].tp) {
    if (t[t[a].tp].dp < t[t[b].tp].dp) a ^= b ^= a ^= b;
    res += query_sum(t[t[a].tp].iw, t[a].iw, 1, n, 1);
    a = t[t[a].tp].p;
  }
  if (t[a].iw > t[b].iw) a ^= b ^= a ^= b;
  return res + query_sum(t[a].iw, t[b].iw, 1, n, 1);
}
int main() {
  scanf("%d", &n);
  for (int i = 1, x, y; i < n; i++) {
    scanf("%d%d", &x, &y);
    add_path(x, y);
    add_path(y, x);
  }
  build_tree(1, 1), build_link(1, 1);
  for (int i = 1; i <= n; i++) scanf("%d", &w[t[i].iw]);
  build_lst(1, n, 1);

  scanf("%d", &q);
  for (int i = 1, x, y; i <= q; i++) {
    char str[10];
    scanf("%s%d%d", str, &x, &y);
    if (str[1] == 'H')
      change(t[x].iw, 1, n, 1, y);
    else if (str[1] == 'M')
      printf("%d\n", query_tree_max(x, y));
    else
      printf("%d\n", query_tree_sum(x, y));
  }
  return 0;
}
