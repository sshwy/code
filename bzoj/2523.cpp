#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
namespace IO {
  char nc() {
    static char bf[100000], *p1 = bf, *p2 = bf;
    return p1 == p2 && (p2 = (p1 = bf) + fread(bf, 1, 100000, stdin), p1 == p2)
               ? EOF
               : *p1++;
  }
  int rd() {
    int res = 0;
    char c = getchar();
    while (!isdigit(c)) c = getchar();
    while (isdigit(c)) res = res * 10 + c - '0', c = getchar();
    return res;
  }
} // namespace IO
/******************heading******************/
const int M = 3e4 + 5;
int calc(int a, int b, int c) {
  if (a == b) return 3;
  if (a == c) return 2;
  if (b == c) return 1;
  if (a > b && a > c) {
    int t = calc(abs(b - c), b, c);
    switch ((t - 1) % 3 + 1) {
      case 1:
        return t + 3;
      case 2:
        return t + 2;
      case 3:
        return t + 1;
    }
  }
  if (b > a && b > c) {
    int t = calc(a, abs(a - c), c);
    switch ((t - 1) % 3 + 1) {
      case 1:
        return t + 1;
      case 2:
        return t + 3;
      case 3:
        return t + 2;
    }
  }
  if (c > a && c > b) {
    int t = calc(a, b, abs(a - b));
    switch ((t - 1) % 3 + 1) {
      case 1:
        return t + 2;
      case 2:
        return t + 1;
      case 3:
        return t + 3;
    }
  }
  return 0;
}
int n, m, len;
struct data {
  int a, b, c;
} ans[M];
int go() {
  int n3 = (n - 1) % 3 + 1;
  len = 0;
  FOR(i, 1, m - 1) {
    if (n3 == 1 && calc(m, i, m - i) == n) ans[++len] = (data){m, i, m - i};
    if (n3 == 2 && calc(i, m, m - i) == n) ans[++len] = (data){i, m, m - i};
    if (n3 == 3 && calc(i, m - i, m) == n) ans[++len] = (data){i, m - i, m};
  }
  printf("%d\n", len);
  FOR(i, 1, len) printf("%d %d %d\n", ans[i].a, ans[i].b, ans[i].c);
  return 0;
}
int main() {
  while (scanf("%d%d", &n, &m) != EOF) {
    if (n == -1 && m == -1) return 0;
    go();
  }
  return 0;
}
