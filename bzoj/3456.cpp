#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int SZ = 1 << 19, P = 1004535809;
int pw(int a, int m) {
  if (m < 0) m += P - 1;
  int res = 1;
  while (m) m & 1 ? res = 1ll * res * a % P : 0, a = 1ll * a * a % P, m >>= 1;
  return res;
}
int tr[SZ];
int init(int n) { // n指多项式的长度，非度数
  int len = 1;
  while (len < n) len <<= 1;
  FOR(i, 0, len - 1) tr[i] = tr[i >> 1] >> 1 | ((i & 1) * (len / 2));
  return len;
}
void dft(int *f, int len, int tag) {
  FOR(i, 0, len - 1) if (i < tr[i]) swap(f[i], f[tr[i]]);
  FOR(i, 0, len - 1) f[i] = (f[i] + P) % P;
  for (int j = 1; j < len; j <<= 1)
    for (int i = 0, wn = pw(3, (P - 1) / (j << 1) * tag); i < len; i += j << 1)
      for (int k = i, u, v, w = 1; k < i + j; k++, w = 1ll * w * wn % P)
        u = f[k], v = f[k + j] * 1ll * w % P, f[k] = (u + v) % P,
        f[k + j] = (u - v + P) % P;
  if (tag == -1)
    for (int i = 0, ilen = pw(len, P - 2); i < len; i++) f[i] = 1ll * f[i] * ilen % P;
}
int inv_h[SZ];
void inv(int *f, int *g, int n) { // f*g=1 mod x^n
  if (n == 1) return g[0] = pw(f[0], P - 2), void();
  inv(f, g, (n + 1) / 2);

  int len = init(2 * (n));
  FOR(i, 0, len - 1) inv_h[i] = 0;
  FOR(i, (n + 1) / 2, len - 1) g[i] = 0;

  FOR(i, 0, n - 1) inv_h[i] = f[i];
  dft(inv_h, len, 1), dft(g, len, 1);
  FOR(i, 0, len - 1) inv_h[i] = 1ll * inv_h[i] * g[i] % P * g[i] % P;
  dft(g, len, -1), dft(inv_h, len, -1);

  FOR(i, 0, n - 1) g[i] = (g[i] * 2ll - inv_h[i] + P) % P;
  FOR(i, n, len - 1) g[i] = 0;
}

const int N = 2e5 + 5;
int n;
int fac[N], fnv[N];
int h[SZ], g[SZ], ig[SZ];

int G(int i) { return pw(2, (1ll * i * (i - 1) / 2) % (P - 1)); }
int main() {
  scanf("%d", &n);
  fac[0] = 1;
  FOR(i, 1, n) fac[i] = 1ll * fac[i - 1] * i % P;
  fnv[n] = pw(fac[n], P - 2);
  ROF(i, n, 1) fnv[i - 1] = 1ll * fnv[i] * i % P;

  FOR(i, 1, n) h[i] = G(i) * 1ll * fnv[i - 1] % P;
  FOR(i, 0, n) g[i] = G(i) * 1ll * fnv[i] % P;

  inv(g, ig, n + 1);

  int len = init((n + 1) * 2);
  dft(h, len, 1), dft(ig, len, 1);
  FOR(i, 0, len - 1) h[i] = 1ll * h[i] * ig[i] % P;
  dft(h, len, -1);

  int ans = h[n] * 1ll * fac[n - 1] % P;
  printf("%d\n", ans);
  return 0;
}
