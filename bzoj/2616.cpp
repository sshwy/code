// by Sshwy
//#define DEBUGGER
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

#ifdef DEBUGGER

#define log_prefix()                                                \
  {                                                                 \
    fprintf(stderr, "\033[37mLine %-3d [%dms]:\033[0m  ", __LINE__, \
        (int)clock() / 1000);                                       \
  }
#define ilog(...) \
  { fprintf(stderr, __VA_ARGS__); }
#define llog(...)      \
  {                    \
    log_prefix();      \
    ilog(__VA_ARGS__); \
  }
#define log(...)           \
  {                        \
    log_prefix();          \
    ilog(__VA_ARGS__);     \
    fprintf(stderr, "\n"); \
  }
#define red(...)       \
  {                    \
    log_prefix();      \
    ilog("\033[31m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define green(...)     \
  {                    \
    log_prefix();      \
    ilog("\033[32m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define blue(...)      \
  {                    \
    log_prefix();      \
    ilog("\033[34m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }

#else

#define log_prefix() ;
#define ilog(...) ;
#define llog(...) ;
#define log(...) ;
#define red(...) ;
#define green(...) ;
#define blue(...) ;

#endif

namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 505, P = 1000000007, H = 1e6 + 5;
int pw(int a, int m) {
  int res = 1;
  while (m) m & 1 ? res = 1ll * res * a % P : 0, a = 1ll * a * a % P, m >>= 1;
  return res;
}
int n, k;
int a[N], c[N][N], fac[H], fnv[H];

inline void add(int &x, int y) { x = (x + y) % P; }
vector<int> trans(vector<int> f, vector<int> g) {
  vector<int> t(f.size() + g.size() - 1, 0);
  for (int i = 0; i < f.size(); i++)
    for (int j = 0; j < g.size(); j++) { add(t[i + j], 1ll * f[i] * g[j] % P); }
  return t;
}
int binom(int a, int b) {
  if (a < b) return 0;
  return fac[a] * 1ll * fnv[b] % P * fnv[a - b] % P;
}
vector<int> calc(int l, int r, int bottom) {
  if (l == r) return {1, a[l] - bottom};

  int h = 1e9, len = r - l + 1;
  FOR(i, l, r) h = min(h, a[i]);
  vector<int> f = {1}, fh = {1, h - bottom};
  int las = 0;

  FOR(i, l, r) {
    if (a[i] == h) {
      if (las) f = trans(f, calc(las, i - 1, h));
      las = 0;
    } else {
      if (!las) las = i;
    }
  }
  if (las) f = trans(f, calc(las, r, h));

  vector<int> g(len + 1, 0);
  for (int x = 0; x <= len; x++) {
    for (int i = 0; i < f.size(); i++) {
      if (i > len || i > x) break;
      add(g[x], f[i] * 1ll * binom(len - i, x - i) % P * binom(h - bottom, x - i) %
                    P * fac[x - i] % P);
    }
  }
  return g;
}

int main() {
  scanf("%d%d", &n, &k);
  FOR(i, 1, n) scanf("%d", &a[i]);

  int LIM = 1e6;
  fac[0] = 1;
  FOR(i, 1, LIM) fac[i] = 1ll * fac[i - 1] * i % P;
  fnv[LIM] = pw(fac[LIM], P - 2);
  ROF(i, LIM, 1) fnv[i - 1] = fnv[i] * 1ll * i % P;

  printf("%d\n", calc(1, n, 0)[k]);
  return 0;
}
