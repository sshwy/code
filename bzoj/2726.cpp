#include <cstdio>
#define int long long
using namespace std;
const int N = 3e5 + 5;
int n, s;
int T[N], F[N];
int f[N];

int Y(int j) { return s * F[j + 1] + f[j] - T[j] * F[j + 1]; }
int M(int i) { return T[i]; }
int X(int j) { return F[j + 1]; }

int slope(int i, int j) { return (Y(i) - Y(j)) * 1.0 / (X(i) - X(j)); } //算斜率

int q[N], lq;
int find(int k) {         //二分查找最优决策，k:斜率
  if (lq == 0) return -1; //没有
  int l = 1, r = lq;
  while (l < r) {
    int mid = (l + r) >> 1;
    double kmid = mid == lq ? 1e99 : slope(q[mid], q[mid + 1]); // BUG#2:没有套q
    if (-k >= kmid)
      r = mid; // BUG#1:k没加负号
    else
      l = mid + 1;
  }
  return q[l];
}
void add(int j) { //添加一个决策到凸壳中
  while (lq >= 2 && slope(j, q[lq]) > slope(q[lq], q[lq - 1])) --lq;
  q[++lq] = j;
}

signed main() {
  scanf("%lld%lld", &n, &s);
  for (int i = 1; i <= n; i++) scanf("%lld%lld", &T[i], &F[i]);
  for (int i = 1; i <= n; i++) T[i] += T[i - 1];     //前缀和
  for (int i = n - 1; i >= 1; i--) F[i] += F[i + 1]; //后缀和

  for (int i = 0; i <= n; i++) {
    int j = find(M(i));                     //寻找最优决策
    if (j != -1) f[i] = M(i) * X(j) + Y(j); //找到最优决策就更新
    add(i);                                 //添加决策i
  }
  printf("%lld\n", f[n]);
  return 0;
}
