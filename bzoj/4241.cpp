// by Sshwy
//#define DEBUGGER
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

#ifdef DEBUGGER

#define log_prefix()                                                \
  {                                                                 \
    fprintf(stderr, "\033[37mLine %-3d [%dms]:\033[0m  ", __LINE__, \
        (int)clock() / 1000);                                       \
  }
#define ilog(...) \
  { fprintf(stderr, __VA_ARGS__); }
#define llog(...)      \
  {                    \
    log_prefix();      \
    ilog(__VA_ARGS__); \
  }
#define log(...)           \
  {                        \
    log_prefix();          \
    ilog(__VA_ARGS__);     \
    fprintf(stderr, "\n"); \
  }
#define red(...)       \
  {                    \
    log_prefix();      \
    ilog("\033[31m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define green(...)     \
  {                    \
    log_prefix();      \
    ilog("\033[32m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define blue(...)      \
  {                    \
    log_prefix();      \
    ilog("\033[34m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }

#else

#define log_prefix() ;
#define ilog(...) ;
#define llog(...) ;
#define log(...) ;
#define red(...) ;
#define green(...) ;
#define blue(...) ;

#endif

namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 1e5 + 5, SQRTN = 5000;
int n, q, T, lt;
int a[N];
int D[N], ld;
vector<int> S[N];
int c[N];
long long f[SQRTN][SQRTN];

int qc(int val, int l, int r) {
  return upper_bound(S[val].begin(), S[val].end(), r) -
         upper_bound(S[val].begin(), S[val].end(), l - 1);
}
long long go() {
  int l, r;
  scanf("%d%d", &l, &r);
  long long res = 0;
  if (l / T == r / T) {
    FOR(i, l, r) res = max(res, qc(a[i], l, r) * 1ll * D[a[i]]);
  } else {
    int x = l / T * T + T - 1;
    FOR(i, l, x) res = max(res, qc(a[i], l, r) * 1ll * D[a[i]]);
    l = x + 1;
    x = r / T * T;
    FOR(i, x, r) res = max(res, qc(a[i], l, r) * 1ll * D[a[i]]);
    r = x - 1;
    if (l <= r) res = max(res, f[l / T][r / T]);
  }
  return res;
}
int main() {
  scanf("%d%d", &n, &q);
  T = max(2, (int)sqrt(n) / 2);
  lt = n / T;

  FOR(i, 1, n) scanf("%d", &a[i]);
  FOR(i, 1, n) D[++ld] = a[i];

  sort(D + 1, D + ld + 1);
  ld = unique(D + 1, D + ld + 1) - D - 1;
  FOR(i, 1, n) a[i] = lower_bound(D + 1, D + ld + 1, a[i]) - D;

  FOR(i, 1, n) S[a[i]].pb(i);
  FOR(i, 0, lt) {
    fill(c, c + n + 1, 0);
    long long x = 0;
    FOR(j, i, lt) {
      int l = max(1, j * T), r = min(n, (j + 1) * T - 1);
      FOR(k, l, r) c[a[k]]++, x = max(x, c[a[k]] * 1ll * D[a[k]]);
      f[i][j] = x;
      log("f[%d,%d]=%lld", i, j, f[i][j]);
    }
  }

  FOR(i, 1, q) { printf("%lld\n", go()); }
  return 0;
}
