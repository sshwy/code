#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
namespace IO {
  char nc() {
    static char bf[100000], *p1 = bf, *p2 = bf;
    return p1 == p2 && (p2 = (p1 = bf) + fread(bf, 1, 100000, stdin), p1 == p2)
               ? EOF
               : *p1++;
  }
  int rd() {
    int res = 0;
    char c = getchar();
    while (!isdigit(c)) c = getchar();
    while (isdigit(c)) res = res * 10 + c - '0', c = getchar();
    return res;
  }
} // namespace IO
/******************heading******************/
//#define int lld
const int N = 2005, P = 1e8 + 7;

int n, k;
int gcd(int a, int b) { return b ? gcd(b, a % b) : a; }

int f[N][N];
int ans;
int pw(int a, int m, int p) {
  int res = 1;
  while (m) m & 1 ? res = 1ll * res * a % p : 0, a = 1ll * a * a % p, m >>= 1;
  return res;
}
int g[N];
void DP(int lim, int K) {
  f[0][0] = 1;
  FOR(i, 1, lim) {
    f[i][0] = 0;
    FOR(j, 0, K) {
      f[i][0] = (f[i][0] + f[i - 1][j]) % P;
      if (j > 0) {
        f[i][j] = f[i - 1][j - 1];
        // printf("f[%d,%d]=%d\n",i,j,f[i][j]);
      }
    }
    // printf("f[%d,%d]=%d\n",i,0,f[i][0]);
  }
  FOR(i, 1, lim) {
    g[i] = 0;
    FOR(j, 0, k) g[i] = (g[i] + f[i][j]) % P;
    int lj = min(k * 2, i - 1);
    FOR(j, k + 1, lj) { g[i] = (g[i] - f[i - j - 1][0] * (k * 2 - j + 1ll)) % P; }
    g[i] = (g[i] + P) % P;
    // printf("g[%d]=%d\n",i,g[i]);
  }
}
int calc(int d) {
  // printf("calc(%d)\n",d);
  if (n <= k) return pw(2, d, P);
  if (n > k && d <= k) return pw(2, d, P) - 1;
  // d是环数
  return g[d];
  return 0;
}
void go() {
  scanf("%d%d", &n, &k);
  ans = 0;
  DP(n, k);
  FOR(i, 1, n) { ans = (ans + calc(gcd(n, i))) % P; }
  ans = 1ll * ans * pw(n, P - 2, P) % P;
  printf("%d\n", ans);
}
signed main() {
  int t;
  scanf("%d", &t);
  FOR(i, 1, t) go();
  return 0;
}
/*
 * BUG#1: 取模不要用(a+=c)%=P的形式！如果是int类型可能在+=的时侯就溢出了
 */
