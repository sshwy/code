#include <algorithm>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <iostream>
#define rin(i, a, b) for (int i = (a); i <= (b); i++)
#define rec(i, a, b) for (int i = (a); i >= (b); i--)
#define trav(i, x) for (int i = head[(x)]; i; i = e[i].nxt)
using std::cin;
using std::cout;
using std::endl;
typedef long long LL;

inline int read() {
  int x = 0;
  char ch = getchar();
  while (ch < '0' || ch > '9') ch = getchar();
  while (ch >= '0' && ch <= '9') {
    x = (x << 3) + (x << 1) + ch - '0';
    ch = getchar();
  }
  return x;
}

const int MAXN = 2005;
const int MOD = 1e8 + 7;
int n, k;
LL f[MAXN][MAXN], g[MAXN][MAXN], cnt[MAXN];
int prm[MAXN], phi[MAXN], pcnt;
bool vis[MAXN];

inline LL qpow(LL x, LL y) {
  LL ret = 1, tt = x % MOD;
  while (y) {
    if (y & 1) ret = ret * tt % MOD;
    tt = tt * tt % MOD;
    y >>= 1;
  }
  return ret;
}

inline void pre_process1() {
  phi[1] = 1;
  rin(i, 2, n) {
    if (!vis[i]) {
      prm[++pcnt] = i;
      phi[i] = i - 1;
    }
    for (int j = 1; j <= pcnt && i * prm[j] <= n; j++) {
      vis[i * prm[j]] = 1;
      if (i % prm[j] == 0) {
        phi[i * prm[j]] = phi[i] * prm[j];
        break;
      }
      phi[i * prm[j]] = phi[i] * (prm[j] - 1);
    }
  }
}

inline void pre_process2() {
  f[0][0] = f[1][0] = f[1][1] = g[1][0] = 1;
  rin(i, 2, n) {
    f[i][0] = g[i][0] = 0;
    rin(j, 0, std::min(i - 1, k)) {
      f[i][0] += f[i - 1][j];
      if (f[i][0] >= MOD) f[i][0] -= MOD;
    }
    rin(j, 1, std::min(i, k)) f[i][j] = f[i - 1][j - 1];
    rin(j, 0, std::min(i - 2, k)) {
      g[i][0] += g[i - 1][j];
      if (g[i][0] >= MOD) g[i][0] -= MOD;
    }
    rin(j, 1, std::min(i - 1, k)) g[i][j] = g[i - 1][j - 1];
  }
  rin(i, 1, n) {
    cnt[i] = 0;
    rin(j, 0, std::min(i, k)) {
      cnt[i] += f[i][j];
      if (cnt[i] >= MOD) cnt[i] -= MOD;
    }
    rin(j, k + 1, std::min(i - 1, k << 1)) {
      cnt[i] -= g[i - j][0] * ((k << 1) - j + 1) % MOD;
      if (cnt[i] < 0) cnt[i] += MOD;
    }
  }
}

inline LL solve(int x) {
  if (x <= k) {
    if (k < n)
      return (qpow(2, x) - 1 + MOD) % MOD;
    else
      return qpow(2, x);
  } else
    return cnt[x];
}

int main() {
  int T = read();
  n = 2000;
  pre_process1();
  while (T--) {
    n = read(), k = read();
    pre_process2();
    int lim = sqrt(n);
    LL ans = 0;
    rin(i, 1, lim) {
      if (n % i) continue;
      ans = (ans + solve(i) * phi[n / i]) % MOD;
      if (i * i == n) continue;
      ans = (ans + solve(n / i) * phi[i]) % MOD;
    }
    printf("%lld\n", ans * qpow(n, MOD - 2) % MOD);
  }
  return 0;
}
