#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
namespace IO {
  char nc() {
    static char bf[100000], *p1 = bf, *p2 = bf;
    return p1 == p2 && (p2 = (p1 = bf) + fread(bf, 1, 100000, stdin), p1 == p2)
               ? EOF
               : *p1++;
  }
  int rd() {
    int res = 0;
    char c = getchar();
    while (!isdigit(c)) c = getchar();
    while (isdigit(c)) res = res * 10 + c - '0', c = getchar();
    return res;
  }
} // namespace IO
/******************heading******************/
#define int lld
const int N = 50500;

int n, m, T;
int a[N];

int gcd(int a, int b) { return b ? gcd(b, a % b) : a; }

struct data {
  int l, r, id, a;
} q[N];
bool cmp(data x, data y) { return x.l / T != y.l / T ? x.l < y.l : x.r < y.r; }
bool cmp2(data x, data y) { return x.id < y.id; }

int curl = 1, curr = 0, curtot;
int c[N];

void addl() { curtot -= --c[a[curl++]]; }
void addr() { curtot += c[a[++curr]]++; }
void subl() { curtot += c[a[--curl]]++; }
void subr() { curtot -= --c[a[curr--]]; }

signed main() {
  scanf("%lld%lld", &n, &m);
  T = sqrt(n);
  FOR(i, 1, n) scanf("%lld", &a[i]);
  FOR(i, 1, m) scanf("%lld%lld", &q[i].l, &q[i].r), q[i].id = i;
  sort(q + 1, q + m + 1, cmp);
  FOR(i, 1, m) {
    int ql = q[i].l, qr = q[i].r;
    if (ql == qr) {
      q[i].a = 0;
      continue;
    }
    while (curr < qr) addr();
    while (curl > ql) subl();
    while (curr > qr) subr();
    while (curl < ql) addl();
    q[i].a = curtot;
  }
  sort(q + 1, q + m + 1, cmp2);
  FOR(i, 1, m) {
    int x = q[i].a, y = (q[i].r - q[i].l) * (q[i].r - q[i].l + 1ll) / 2;
    if (!y)
      puts("0/1");
    else {
      int g = gcd(x, y);
      printf("%lld/%lld\n", x / g, y / g);
    }
  }
  return 0;
}
