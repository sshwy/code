#include <cmath>
#include <cstdio>
#include <iostream>
#define lld long long
using namespace std;
const lld MAXN = 1e6 + 10;
lld a, b, c, x1, x2, yy1, y2, x, y;
lld exgcd(lld a, lld b, lld &x, lld &y) {
  if (!b) return x = 1, y = 0, a;
  lld t = exgcd(b, a % b, y, x);
  return y = y - (a / b) * x, t;
}
lld min(lld a, lld b) { return a < b ? a : b; }
lld max(lld a, lld b) { return a > b ? a : b; }
int main() {
  cin >> a >> b >> c >> x1 >> x2 >> yy1 >> y2;
  c = -c;
  if (a == 0 && b == 0) {
    if (c == 0) {
      printf("%lld", (lld)(x2 - x1 + 1) * (y2 - yy1 + 1));
      return 0;
    } else {
      printf("0");
      return 0;
    }
  }
  if (a == 0) {
    if (c % b) {
      printf("0");
      return 0;
    }
    if (c / b >= yy1 && c / b <= y2) {
      printf("%lld", x2 - x1 + 1);
      return 0;
    } else {
      printf("0");
      return 0;
    }
  }
  if (b == 0) {
    if (c % a) {
      printf("0");
      return 0;
    }
    if (c / a >= x1 && c / a <= x2) {
      printf("%lld", y2 - yy1 + 1);
      return 0;
    } else {
      printf("0");
      return 0;
    }
  }
  lld r = exgcd(a, b, x, y);
  b = b / r;
  a = -a / r; //利用公式构造增量
  if (c % r) {
    printf("0");
    return 0;
  }
  x = x * c / r;
  y = y * c / r;
  lld xlower, xupper, ylower, yupper;
  if (b > 0)
    xlower = ceil((double)(x1 - x) / b), xupper = floor((double)(x2 - x) / b);
  if (b < 0)
    xlower = ceil((double)(x2 - x) / b), xupper = floor((double)(x1 - x) / b);
  if (a > 0)
    ylower = ceil((double)(yy1 - y) / a), yupper = floor((double)(y2 - y) / a);
  if (a < 0)
    ylower = ceil((double)(y2 - y) / a), yupper = floor((double)(yy1 - y) / a);
  lld ans = max(0, min(xupper, yupper) - max(xlower, ylower) + 1);
  printf("%lld", ans);

  return 0;
}
