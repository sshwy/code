#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int M = 50003, P = 1e9 + 7;

int n, m;
bool bp[M];
int pn[M], lp;
int f[1 << 16], nn = 1 << 16;

int pw(int a, int m, int p) {
  int res = 1;
  while (m) m & 1 ? res = 1ll * res * a % p : 0, a = 1ll * a * a % p, m >>= 1;
  return res;
}
void sieve(int k) {
  bp[0] = bp[1] = 1;
  FOR(i, 2, k) {
    if (!bp[i]) pn[++lp] = i;
    FOR(j, 1, lp) {
      int pj = pn[j];
      if (i * pj > k) break;
      bp[i * pj] = 1;
      if (i % pj == 0) break;
    }
  }
}

void fwt(int *f, int len, int tag) {
  for (int j = 1; j < len; j <<= 1)
    for (int i = 0; i < len; i += j << 1)
      for (int k = i, x, y; k < i + j; k++)
        x = f[k], y = f[k + j], f[k] = (x + y) % P, f[k + j] = (x - y) % P;
  int ilen = pw(len, P - 2, P);
  if (tag == -1) FOR(i, 0, len - 1) f[i] = 1ll * f[i] * ilen % P;
  FOR(i, 0, len - 1) f[i] += f[i] < 0 ? P : 0;
}
void go() {
  memset(f, 0, sizeof(f));
  FOR(i, 1, lp) {
    if (pn[i] > m) break;
    f[pn[i]] = 1;
  }
  // FOR(i,0,20)printf("f[%d]=%d\n",i,f[i]);
  fwt(f, nn, 1);
  // fwt(f,nn,-1);

  FOR(i, 0, nn - 1) f[i] = pw(f[i], n, P);
  fwt(f, nn, -1);
  int ans = 0;
  // FOR(i,1,nn-1)ans=(ans+f[i])%P;
  ans = f[0];
  printf("%d\n", ans);
}
int main() {
  sieve(50000);
  while (~scanf("%d%d", &n, &m)) go();
  return 0;
}
