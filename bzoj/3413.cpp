#include <algorithm>
#include <cstdio>
#include <cstring>
#define int long long
#define FOR(a, b, c) for (int a = b; a <= c; a++)
#define ROF(a, b, c) for (int a = b; a >= c; a--)
using namespace std;
const int N = 1e3 + 4, P = 1e9 + 7;

int f[N][10][10], m10[N], la;
char a[N];

void DP() {
  FOR(i, 0, 9) f[2][i][i] = 1;
  FOR(i, 3, N - 1) FOR(j, 0, 9) FOR(k, 0, 9) {
    if (j == k)
      f[i][j][k] = m10[i - 2];
    else
      FOR(x, 0, 9) f[i][j][k] += j == x ? m10[i - 3] : f[i - 1][k][x];
    f[i][j][k] %= P;
  }
}

int solve(int isleft) { //如果是左区间端点，那么不考虑上界
  scanf("%s", a + 1);
  la = strlen(a + 1);
  reverse(a + 1, a + la + 1);
  FOR(i, 1, la) a[i] -= '0';

  int res = 0, fl = 0;
  FOR(i, 2, la - 1) FOR(j, 1, 9) FOR(k, 0, 9) res = (res + f[i][j][k]) % P;
  FOR(j, 1, a[la] - 1) FOR(k, 0, 9) res = (res + f[la][j][k]) % P;
  ROF(i, la, 2) {
    if ((i < la && a[i] == a[i + 1]) || (i + 1 < la && a[i] == a[i + 2])) fl = 1;
    if (fl)
      res = (res + a[i - 1] * m10[i - 2]) % P;
    else
      FOR(j, 0, a[i - 1] - 1)
    res = (res + ((i < la && j == a[i + 1]) ? m10[i - 2] : f[i][a[i]][j])) % P;
  }
  if ((1 < la && a[1] == a[2]) || (2 < la && a[1] == a[3])) fl = 1;
  if (!isleft && fl) res = (res + 1) % P; //即上界也要算进去
  return res;
}

signed main() {
  m10[0] = 1;
  FOR(i, 1, N - 1) m10[i] = m10[i - 1] * 10 % P;
  DP();

  int a = solve(1), b = solve(0);
  printf("%lld\n", (b - a + P) % P);
  return 0;
}
/*
 * BUG#1:没判断最后一位的回文.L37
 * BUG#2:没有考虑三位运算符优先级（优先级小于算术加法）.L35
 */
