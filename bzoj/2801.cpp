// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

const int N = 5e5 + 5, M = 3e6 + 5;

struct qxx {
  int nex, t, v;
} e[M * 2];
int le, vis[N], h[N];

void add_path(int u, int v, int w) { e[++le] = {h[u], v, w}, h[u] = le; }

int n, m;
int c[N];
long long A[N], B[N], sa, sb;
long long Lx, Rx;

void bound(long long a, long long b, long long L, long long R) {
  // printf("bound %lld %lld %lld %lld\n", a, b, L, R);
  if (a < 0) a = -a, b = -b, L = -L, R = -R, swap(L, R);
  Lx = max(Lx, (long long)ceil(1.0 * (L - b) / a));
  Rx = min(Rx, (long long)floor(1.0 * (R - b) / a));
  // printf("Lx %lld Rx %lld\n", Lx, Rx);
}
void dfs(int u, long long a, long long b) {
  // printf("u %d a %lld b %lld\n", u, p, a, b);
  vis[u] = true;
  A[u] = a, B[u] = b;
  sa += a, sb += b;
  bound(a, b, 0, c[u]);
  for (int i = h[u]; i; i = e[i].nex) {
    int v = e[i].t, w = e[i].v;
    long long av = -a, bv = c[u] + c[v] - w - b;
    if (!vis[v])
      dfs(v, av, bv);
    else {
      long long aa = A[v] - av, bb = bv - B[v];
      if (aa == 0 && bb == 0) continue;
      if (aa == 0 || bb % aa) {
        Lx = LLONG_MAX;
        Rx = LLONG_MIN;
      }
      Lx = max(Lx, bb / aa);
      Rx = min(Rx, bb / aa);
    }
  }
}
long long ans_max, ans_min;
void work(int u) {
  Lx = LLONG_MIN;
  Rx = LLONG_MAX;
  sa = sb = 0;
  dfs(u, 1, 0);
  if (Lx > Rx) {
    puts("NIE");
    exit(0);
  }
  long long t[] = {sa * Lx + sb, sa * Rx + sb};
  if (t[0] > t[1]) swap(t[0], t[1]);
  ans_min += t[0];
  ans_max += t[1];
}

int main() {
  scanf("%d%d", &n, &m);
  FOR(i, 1, n) { scanf("%d", c + i); }
  FOR(i, 1, m) {
    int u, v, w;
    scanf("%d%d%d", &u, &v, &w);
    add_path(u, v, w);
    add_path(v, u, w);
  }
  FOR(i, 1, n) if (!vis[i]) { work(i); }
  printf("%lld %lld\n", ans_min, ans_max);
  return 0;
}
