#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
namespace IO {
  char nc() {
    static char bf[100000], *p1 = bf, *p2 = bf;
    return p1 == p2 && (p2 = (p1 = bf) + fread(bf, 1, 100000, stdin), p1 == p2)
               ? EOF
               : *p1++;
  }
  int rd() {
    int res = 0;
    char c = getchar();
    while (!isdigit(c)) c = getchar();
    while (isdigit(c)) res = res * 10 + c - '0', c = getchar();
    return res;
  }
} // namespace IO
/******************heading******************/
const int N = 1e6 + 6, M = 7e6 + 6, INF = 0x3f3f3f3f;

struct qxx {
  int nex, t, v;
};
qxx e[M];
int h[N], cnt = 1;
void add_path(int f, int t, int v) { e[++cnt] = (qxx){h[f], t, v}, h[f] = cnt; }
void add_flow(int f, int t, int v) { add_path(f, t, v), add_path(t, f, 0); }
void add_dual_flow(int f, int t, int v) { add_path(f, t, v), add_path(t, f, v); }

namespace DINIC {
  int s, t, maxflow, d[N];
  queue<int> q;
  bool bfs() {
    memset(d, 0, sizeof(d));
    q.push(s), d[s] = 1;
    while (!q.empty()) {
      int u = q.front();
      q.pop();
      for (int i = h[u]; i; i = e[i].nex) {
        const int &v = e[i].t, &w = e[i].v;
        if (!d[v] && w) d[v] = d[u] + 1, q.push(v);
      }
    }
    return d[t];
  }
  int dinic(int u, int flow) {
    if (u == t) return flow;
    int k, rest = flow;
    for (int i = h[u]; i && rest; i = e[i].nex) {
      const int &v = e[i].t, &w = e[i].v;
      if (!w || d[v] != d[u] + 1) continue;
      k = dinic(v, min(rest, w));
      if (k)
        e[i].v -= k, e[i ^ 1].v += k, rest -= k;
      else
        d[v] = 0;
    }
    return flow - rest;
  }
  void go() {
    while (bfs())
      for (int i; i = dinic(s, INF);) maxflow += i;
  }
} // namespace DINIC
int n, m;

int tr(int x, int y) { return x * m + y; }
int main() {
  scanf("%d%d", &n, &m);
  FOR(i, 1, n) FOR(j, 1, m - 1) {
    int x;
    scanf("%d", &x);
    add_dual_flow(tr(i, j), tr(i, j + 1), x);
  }
  FOR(i, 1, n - 1) FOR(j, 1, m) {
    int x;
    scanf("%d", &x);
    add_dual_flow(tr(i, j), tr(i + 1, j), x);
  }
  FOR(i, 1, n - 1) FOR(j, 1, m - 1) {
    int x;
    scanf("%d", &x);
    add_dual_flow(tr(i, j), tr(i + 1, j + 1), x);
  }
  DINIC::s = tr(1, 1), DINIC::t = tr(n, m);
  DINIC::go();
  printf("%d\n", DINIC::maxflow);
  return 0;
}
