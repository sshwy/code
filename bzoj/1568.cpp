#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
namespace IO {
  char nc() {
    static char bf[100000], *p1 = bf, *p2 = bf;
    return p1 == p2 && (p2 = (p1 = bf) + fread(bf, 1, 100000, stdin), p1 == p2)
               ? EOF
               : *p1++;
  }
  int rd() {
    int res = 0;
    char c = getchar();
    while (!isdigit(c)) c = getchar();
    while (isdigit(c)) res = res * 10 + c - '0', c = getchar();
    return res;
  }
} // namespace IO
/******************heading******************/
const int N = 1e5 + 5, SZ = 50005 << 2;

int n;

struct node {
  int l, r;
  double s, p;
  node() { s = p = l = r = 0; }
  int upd(double _s, double _p) {
    int a = _s < s;
    int b = _s + (r - l) * _p < s + (r - l) * p;
    if (a && b) return -1;    //完全没有更新的价值
    if (a || b) return 0;     //需要继续递归
    return s = _s, p = _p, 1; //整个区间都被更新了
  }
} t[SZ];

void build(int u = 1, int l = 1, int r = 50000) {
  t[u].l = l, t[u].r = r;
  if (l == r) return;
  int mid = (l + r) >> 1;
  build(u << 1, l, mid), build(u << 1 | 1, mid + 1, r);
}
void add(double s, double p, int u = 1) {
  const int l = t[u].l, r = t[u].r;
  if (l == r) return t[u].upd(s, p), void();
  int x = t[u].upd(s, p);
  if (x == 1 || x == -1) return;
  int mid = (l + r) >> 1;
  add(s, p, u << 1), add(s + (mid - l + 1) * p, p, u << 1 | 1);
}
double query(int p, int u = 1) {
  const int l = t[u].l, r = t[u].r;
  if (l == r) return t[u].s;
  int mid = (l + r) >> 1;
  double res = t[u].s + t[u].p * (p - l);
  if (p <= mid)
    return max(query(p, u << 1), res);
  else
    return max(query(p, u << 1 | 1), res);
}
int main() {
  scanf("%d", &n);
  build();
  FOR(i, 1, n) {
    char op[10];
    int t;
    double s, p;
    scanf("%s", op);
    if (op[0] == 'Q') {
      scanf("%d", &t);
      printf("%d\n", (int)query(t) / 100);
      // printf("%lf\n",query(t));
    } else {
      scanf("%lf%lf", &s, &p);
      add(s, p);
    }
  }
  return 0;
}
