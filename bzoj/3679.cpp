#include <cstdio>
#include <map>
#define int long long
#define FOR(a, b, c) for (int a = b; a <= c; a++)
using namespace std;

int n, L, R, tot;
int f[30][10][6000];

map<int, int> p;
int w[6000];

void check(int a, int b, int c, int d) {
  int s = 1;
  FOR(i, 1, a) s *= 2;
  if (s > n) return;
  FOR(i, 1, b) s *= 3;
  if (s > n) return;
  FOR(i, 1, c) s *= 5;
  if (s > n) return;
  FOR(i, 1, d) s *= 7;
  if (s > n) return;
  p[s] = ++tot, w[tot] = s;
}
void DP() {
  FOR(i, 1, 9) if (i <= n) f[1][i][p[i]] = 1; //初始化一位数
  FOR(i, 1, 19)
  FOR(j, 1, 9)
  FOR(k, 1, tot)
  if (f[i][j][k])
    FOR(x, 1, 9) if (w[k] * x <= n) f[i + 1][x][p[w[k] * x]] += f[i][j][k];
}
int calc(int m) { //没有取到m
  int d[30], ld = 0, res = 0;
  while (m) d[++ld] = m % 10, m /= 10; //分解10进制位
  for (int i = 1, j = ld; i < j; i++, j--) swap(d[i], d[j]);
  FOR(i, 1, ld - 1) FOR(j, 1, 9) FOR(k, 1, tot) res += f[i][j][k]; //长度小于ld
  int pre = 1; //存储之前的位的乘积
  FOR(i, 1, ld) {
    FOR(k, 1, tot)
    if (w[k] * pre <= n) FOR(j, 1, d[i] - 1) res += f[ld - i + 1][j][k];
    pre *= d[i];
    if (pre > n || pre == 0) break;
  }
  return res;
}
signed main() {
  scanf("%lld%lld%lld", &n, &L, &R);
  FOR(i, 0, 29) FOR(j, 0, 18) FOR(k, 0, 12) FOR(l, 0, 10) check(i, j, k, l);
  DP();
  printf("%lld\n", calc(R) - calc(L));
  return 0;
}
/*
 * f[i,j,k]表示长度为i，首位为j，积为k的数的个数
 * k的集合大小不超过6000,开map映射
 * check函数用来预处理合法的k
 * BUG#1:DP的时侯i的界开成了[2,19]。应该是[1,18]
 * BUG#2:没有统计长度小于ld的答案
 * BUG#3:check(i,j,k,l)写成了check(i,k,j,l)...
 */
