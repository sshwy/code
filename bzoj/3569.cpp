#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 1e6 + 5, M = 1e6 + 5;

struct qxx {
  int nex, t, v;
};
qxx e[M];
int h[N], le = 1;
void add_path(int f, int t) { e[++le] = (qxx){h[f], t, 0}, h[f] = le; }
#define FORew(i, u, v, w) \
  for (int i = h[u], v, w; v = e[i].t, w = e[i].v, i; i = e[i].nex)

int n, m;
bool ontree[M], vis[N];
long long eval[N], f[N];

long long dfs1(int u, int p = 0) {
  // printf("dfs1(%d,%d)\n",u,p);
  vis[u] = 1;
  FORew(i, u, v, w) {
    if (v == p) continue;
    if (vis[v]) {
      if (eval[i]) continue;
      long long x = 1ll * rand() * rand();
      eval[i] = eval[i ^ 1] = x;
      // printf("path(%d,%d):%lld\n",u,v,x);
      f[u] ^= x, f[v] ^= x;
    } else {
      long long x = dfs1(v, u);
      eval[i] = eval[i ^ 1] = x;
      f[u] ^= x;
    }
  }
  return f[u];
}
struct basis {
  long long b[70];
  basis() { memset(b, 0, sizeof(b)); }
  bool insert(long long x) {
    ROF(i, 60, 0) {
      if (x >> i & 1) {
        if (b[i])
          x ^= b[i];
        else
          return b[i] = x, 1;
      }
    }
    return 0;
  }
  long long qmax() {
    long long res = 0;
    ROF(i, 60, 0) if ((res ^ b[i]) > res) res ^= b[i];
    return res;
  }
  basis operator+(basis bi) {
    basis res = bi;
    FOR(i, 0, 60) if (b[i]) res.insert(b[i]);
    return res;
  }
};

int main() {
  // cout<<RAND_MAX<<endl;
  srand(clock());
  scanf("%d%d", &n, &m);
  FOR(i, 1, m) {
    int x, y;
    scanf("%d%d", &x, &y);
    add_path(x, y), add_path(y, x);
  }
  dfs1(1);
  int q, las = 0;
  scanf("%d", &q);
  FOR(i, 1, q) {
    // printf("query #%d\n",i);
    basis B;
    int k, fl = 1;
    scanf("%d", &k);
    FOR(i, 1, k) {
      int c;
      scanf("%d", &c);
      // c^=las;
      // printf("c=%d,val=%lld\n",c,eval[c*2]);
      // assert(eval[c*2]=eval[c*2+1]);
      if (!B.insert(eval[c * 2])) fl = 0;
    }
    if (fl)
      printf("Connected\n"), las++;
    else
      printf("Disconnected\n");
  }
  return 0;
}
