#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int P = 998244353, N = 1 << 18;
int pw(int a, int m) {
  if (m < 0) m += P - 1;
  int res = 1;
  while (m) m & 1 ? res = 1ll * res * a % P : 0, a = 1ll * a * a % P, m >>= 1;
  return res;
}

int n;
int fac[N], fnv[N];
int g[N];
int A[N], B[N], Z[N];

int f(int x) { return 2ll * fnv[x] % P; }
int f1(int x) { return f(x + 1); }

int tr[N];
int init(int l) {
  int len = 1;
  while (len <= l) len <<= 1;
  FOR(i, 0, len - 1) tr[i] = tr[i >> 1] >> 1 | ((i & 1) * (len >> 1));
  return len;
}
void dft(int *f, int len, int tag) {
  FOR(i, 0, len - 1) if (i < tr[i]) swap(f[i], f[tr[i]]);
  for (int j = 1; j < len; j <<= 1)
    for (int i = 0, step = pw(3, (P - 1) / (j << 1) * tag); i < len; i += j << 1)
      for (int k = i, u, v, w = 1; k < i + j; k++, w = 1ll * w * step % P)
        u = f[k], v = 1ll * f[k + j] * w % P, f[k] = (u + v) % P,
        f[k + j] = (u - v + P) % P;
  if (tag == -1) {
    int ilen = pw(len, P - 2);
    FOR(i, 0, len - 1) f[i] = 1ll * f[i] * ilen % P;
  }
}

void solve(int l, int r) {
  if (l == r) return l == 0 ? g[0] = 1 : 0, void();
  int mid = (l + r) >> 1;
  solve(l, mid);

  int lz = r - 1 - l;
  int len = init(lz + lz);

  memset(A, 0, sizeof(int) * (len));
  memset(B, 0, sizeof(int) * (len));

  FOR(i, 0, lz) A[i] = f1(i);
  FOR(i, 0, mid - l) B[i] = g[i + l];

  dft(A, len, 1), dft(B, len, 1);
  FOR(i, 0, len - 1) Z[i] = 1ll * A[i] * B[i] % P;
  dft(Z, len, -1);

  FOR(i, mid + 1, r) g[i] = (g[i] + Z[i - 1 - l]) % P;
  solve(mid + 1, r);
}

int main() {
  scanf("%d", &n);

  fac[0] = 1;
  FOR(i, 1, n) fac[i] = 1ll * fac[i - 1] * i % P;
  fnv[n] = pw(fac[n], P - 2);
  ROF(i, n, 1) fnv[i - 1] = fnv[i] * 1ll * i % P;

  solve(0, n);
  FOR(i, 0, n) g[i] = 1ll * g[i] * fac[i] % P;
  // FOR(i,0,n)printf("%d%c",g[i]," \n"[i==n]);
  int ans = 0;
  FOR(i, 0, n) ans = (ans + g[i]) % P;
  printf("%d\n", ans);

  return 0;
}
