// BZOJ3239
#include <cmath>
#include <cstdio>
#include <cstring>
using namespace std;
int gcd(int a, int b) { return b ? gcd(b, a % b) : a; }
int exgcd(int a, int b, int &x, int &y) {
  if (!b) return x = 1, y = 0, a;
  int g = exgcd(b, a % b, y, x);
  return y = y - x * (a / b), g;
}
int inv(int a, int p) { // 求逆元
  int b, t;
  int g = exgcd(a, p, b, t);
  if (g != 1) return -1;
  return ((b % p) + p) % p;
}
const int SZ = 2e6;
struct hash_map { // 哈希表模板
  struct data {
    int u, v, nex;
  };
  data e[SZ << 1];
  int h[SZ], cnt;
  int hash(int u) { return u % SZ; }
  int &operator[](int u) {
    int hu = hash(u);
    for (int i = h[hu]; i; i = e[i].nex)
      if (e[i].u == u) return e[i].v;
    return e[++cnt] = (data){u, -1, h[hu]}, h[hu] = cnt, e[cnt].v;
  }
  void init() {
    cnt = 0;
    memset(h, 0, sizeof(h));
  }
};
int pow(int a, int m, int p) { // 快速幂
  int res = 1;
  while (m) {
    if (m & 1) res = (long long)res * a % p;
    a = (long long)a * a % p, m >>= 1;
  }
  return res;
}
hash_map h;
int bsgs(int a, int b, int p) {
  int t = sqrt(p), cur = b; // 分块大小
  h.init();
  for (int i = 0; i < t; i++) h[cur] = i, cur = (long long)cur * a % p;
  cur = pow(a, t, p);
  int tot = 1;
  for (int i = 0; i <= t; i++) {
    if (h[tot] != -1 && i * t >= h[tot]) return i * t - h[tot];
    tot = (long long)tot * cur % p;
  }
  return -1;
}
int exbsgs(int a, int b, int p) {
  int g = gcd(a, p), k = 0;
  long long cq = 1, ia; // a 的分母系数，a 的逆元
  while (g != 1) {
    if (b % g) return -1; // 无解
    cq = cq * g % p, k++; // 迭代次数 +1
    b /= g, p /= g, g = gcd(a, p);
  }
  ia = inv(a, p);
  for (int i = 1; i <= k; i++) cq = cq * ia % p; // 系数的逆元
  printf("f:%lld\n", cq);
  b = b * cq % p;
  return bsgs(a, b, p) + k;
}
signed main() {
  int a, b, p;
  while (~scanf("%d%d%d", &p, &a, &b)) {
    int ans = exbsgs(a, b, p);
    if (ans == -1)
      puts("no solution");
    else
      printf("%d\n", ans);
  }
  return 0;
}
