#include <cstdio>
using namespace std;
const int N = 1e4 + 5, LST = 1 << 18, BASE = 7;
int t, n;
int a[N], b[N]; //

int min(int a, int b) { return a < b ? a : b; }
int h1[LST], h2[LST]; //正反Hash值

void init(int u = 1, int l = 1, int r = n) {
  h1[u] = h2[u] = 0;
  if (l == r) return;
  int mid = (l + r) >> 1;
  init(u << 1, l, mid), init(u << 1 | 1, mid + 1, r);
}
void modify(int pos, int v, int u = 1, int l = 1, int r = n) { // 0变1或者1变0
  int a1 = pos - l + 1, a2 = r - pos + 1; //对于正反的相对位置
  h1[u] += b[a1] * v, h2[u] += b[a2] * v;
  if (l == r) return;
  int mid = (l + r) >> 1;
  if (pos <= mid)
    modify(pos, v, u << 1, l, mid);
  else
    modify(pos, v, u << 1 | 1, mid + 1, r);
}
int query1(int L, int R, int u = 1, int l = 1, int r = n) {
  if (L <= l && r <= R) return h1[u] * b[l - L];
  int mid = (l + r) >> 1, res = 0;
  if (L <= mid) res += query1(L, R, u << 1, l, mid);
  if (mid < R) res += query1(L, R, u << 1 | 1, mid + 1, r);
  return res;
}
int query2(int L, int R, int u = 1, int l = 1, int r = n) {
  if (L <= l && r <= R) return h2[u] * b[R - r];
  int mid = (l + r) >> 1, res = 0;
  if (mid < R) res += query2(L, R, u << 1 | 1, mid + 1, r);
  if (L <= mid) res += query2(L, R, u << 1, l, mid);
  return res;
}

void go() {
  scanf("%d", &n);
  for (int i = 1; i <= n; i++) scanf("%d", &a[i]);
  //初始化
  init();
  for (int i = 1; i <= n; i++) { //初始时，所有权值都在右边
    modify(a[i], 1);
  }
  // a[1]显然无解，从2考虑
  for (int i = 2; i <= n; i++) {       //从a[i-1]变到a[i]
    modify(a[i - 1], -1);              // a[i-1]变成了左边的位置
    int len = min(a[i] - 1, n - a[i]); //序列的长度
    if (!len) continue;                //无解
    int q1 = query1(a[i] - len, a[i] - 1), q2 = query2(a[i] + 1, a[i] + len);
    if (q1 != q2) {
      puts("Y");
      return;
    }
  }
  puts("N");
}
int main() {
  scanf("%d", &t);
  b[0] = 1;
  for (int i = 1; i <= 10000; i++) b[i] = b[i - 1] * BASE; //自然溢出，对2^31-1取模
  while (t--) go();
  return 0;
}
