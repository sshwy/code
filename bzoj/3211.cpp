#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
const ll N = 100005;
ll n, m, a[N], cas;
ll s[1 << 18], mx[1 << 18];

inline ll max(ll a, ll b) { return a > b ? a : b; }
inline void push_up(ll rt) {
  s[rt] = s[rt << 1] + s[rt << 1 | 1];
  mx[rt] = max(mx[rt << 1], mx[rt << 1 | 1]);
}
ll lst_build(ll l, ll r, ll rt) {
  if (l == r) return s[rt] = mx[rt] = a[l];
  ll mid = (l + r) >> 1;
  lst_build(l, mid, rt << 1), lst_build(mid + 1, r, rt << 1 | 1);
  push_up(rt);
}
ll vio_sqrt(ll l, ll r, ll rt) {
  if (mx[rt] == 1) return 0;
  if (l == r) return s[rt] = sqrt(s[rt]), mx[rt] = s[rt];
  ll mid = (l + r) >> 1;
  vio_sqrt(l, mid, rt << 1), vio_sqrt(mid + 1, r, rt << 1 | 1);
  push_up(rt);
}
ll lst_sqrt(ll L, ll R, ll l, ll r, ll rt) {
  if (R < l || r < L) return 0;
  if (L <= l && r <= R) return vio_sqrt(l, r, rt);
  ll mid = (l + r) >> 1;
  lst_sqrt(L, R, l, mid, rt << 1), lst_sqrt(L, R, mid + 1, r, rt << 1 | 1);
  push_up(rt);
}
ll lst_sum(ll L, ll R, ll l, ll r, ll rt) {
  if (R < l || r < L) return 0;
  if (L <= l && r <= R) return s[rt];
  ll mid = (l + r) >> 1;
  return lst_sum(L, R, l, mid, rt << 1) + lst_sum(L, R, mid + 1, r, rt << 1 | 1);
}
int main() {
  while (~scanf("%lld", &n)) {
    printf("Case #%d:\n", ++cas);
    for (ll i = 1; i <= n; i++) scanf("%lld", &a[i]);
    scanf("%lld", &m);
    lst_build(1, n, 1);
    for (ll i = 1, x, p, q; i <= m; i++) {
      scanf("%lld%lld%lld", &x, &p, &q);
      if (p > q) p ^= q ^= p ^= q;
      if (x == 1)
        printf("%lld\n", lst_sum(p, q, 1, n, 1));
      else
        lst_sqrt(p, q, 1, n, 1);
    }
    puts("");
  }
  return 0;
}
