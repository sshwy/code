#include <bits/stdc++.h>
#define N 1000001
#define A 100001
using namespace std;
int n, a[N], b[N];
struct disjoint {
  int f[A], v[A];
  void clear(int k) {
    for (int i = 0; i <= k; i++) f[i] = i, v[i] = i;
  }
  int gf(int k) { return f[k] == k ? k : f[k] = gf(f[k]); }
  bool find(int a, int b) { return gf(a) == gf(b); }
  void un(int a, int b) {
    f[gf(a)] = gf(b), v[gf(b)] = min(v[gf(b)], min(v[a], v[b]));
  }
};
int main() {
  scanf("%d", &n);
  for (int i = 1; i <= n; i++) { scanf("%d%d", &a[i], &b[i]); }
  return 0;
}
