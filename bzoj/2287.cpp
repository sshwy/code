#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
namespace IO {
  char nc() {
    static char bf[100000], *p1 = bf, *p2 = bf;
    return p1 == p2 && (p2 = (p1 = bf) + fread(bf, 1, 100000, stdin), p1 == p2)
               ? EOF
               : *p1++;
  }
  int rd() {
    int res = 0;
    char c = getchar();
    while (!isdigit(c)) c = getchar();
    while (isdigit(c)) res = res * 10 + c - '0', c = getchar();
    return res;
  }
} // namespace IO
/******************heading******************/
const int N = 2e3 + 5, M = 2e3 + 5;

int n, m;
int w[N];
int f[M], g[M];

void calc(int k) {
  FOR(i, 0, w[k] - 1) g[i] = f[i];
  FOR(i, w[k], m) g[i] = (f[i] - g[i - w[k]] + 10) % 10;
  FOR(i, 1, m) printf("%d", g[i]);
  puts("");
}
int main() {
  scanf("%d%d", &n, &m);
  FOR(i, 1, n) scanf("%d", &w[i]);
  f[0] = 1;
  FOR(i, 1, n) {
    ROF(j, m, w[i]) {
      f[j] += f[j - w[i]];
      f[j] %= 10;
    }
  }
  FOR(i, 1, n) calc(i);
  return 0;
}
