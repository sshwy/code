// by Sshwy
#define DEBUGGER
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

#ifdef DEBUGGER

#define log_prefix()                                                \
  {                                                                 \
    fprintf(stderr, "\033[37mLine %-3d [%dms]:\033[0m  ", __LINE__, \
        (int)clock() / 1000);                                       \
  }
#define ilog(...) \
  { fprintf(stderr, __VA_ARGS__); }
#define llog(...)      \
  {                    \
    log_prefix();      \
    ilog(__VA_ARGS__); \
  }
#define log(...)           \
  {                        \
    log_prefix();          \
    ilog(__VA_ARGS__);     \
    fprintf(stderr, "\n"); \
  }
#define red(...)       \
  {                    \
    log_prefix();      \
    ilog("\033[31m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define green(...)     \
  {                    \
    log_prefix();      \
    ilog("\033[32m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define blue(...)      \
  {                    \
    log_prefix();      \
    ilog("\033[34m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }

#else

#define log_prefix() ;
#define ilog(...) ;
#define llog(...) ;
#define log(...) ;
#define red(...) ;
#define green(...) ;
#define blue(...) ;

#endif

namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
// int xxx=0;
const int N = 5e4 + 5, SZ = 2e6 + 5;

int n, m, T;
int a[N];

struct data {
  int a, b, c, d, e;
} qry[N];
bool cmp(data x, data y) {
  if (x.a / T != y.a / T) return x.a < y.a;
  return x.b < y.b;
}

int c[N];

int tot;
int lc[SZ], rc[SZ], sz[SZ];

struct Set {
  int root;
  Set() { root = 0; }
  bool empty() { return !root || !sz[root]; }
  void add(int pos, int v, int &u, int l, int r) {
    if (!u) u = ++tot;
    if (l == r) return sz[u] += v, void();
    int mid = (l + r) >> 1;
    if (pos <= mid)
      add(pos, v, lc[u], l, mid);
    else
      add(pos, v, rc[u], mid + 1, r);
    sz[u] = sz[lc[u]] + sz[rc[u]];
  }
  void insert(int x) { add(x, 1, root, 1, n); }
  void erase(int x) { add(x, -1, root, 1, n); }
  int query(int k, int u, int l, int r) {
    if (l == r) return l;
    int mid = (l + r) >> 1;
    if (sz[lc[u]] < k)
      return query(k - sz[lc[u]], rc[u], mid + 1, r);
    else
      return query(k, lc[u], l, mid);
  }
  int operator[](int x) { return query(x, root, 1, n); }
} s[N], A;

void insert(int x) {
  if (c[x]) {
    s[c[x]].erase(x);
    if (s[c[x]].empty()) A.erase(c[x]);
  }
  ++c[x];
  if (c[x]) {
    if (s[c[x]].empty()) A.insert(c[x]);
    s[c[x]].insert(x);
  }
}
void remove(int x) {
  if (c[x]) {
    s[c[x]].erase(x);
    if (s[c[x]].empty()) A.erase(c[x]);
  }
  --c[x];
  if (c[x]) {
    if (s[c[x]].empty()) A.insert(c[x]);
    s[c[x]].insert(x);
  }
}
int L = 1, R = 0;
void addL() { remove(a[L++]); }
void subL() { insert(a[--L]); }
void addR() { insert(a[++R]); }
void subR() { remove(a[R--]); }

int query(int k1, int k2) { return s[A[k1]][k2]; }
int ans[N];
int main() {
  scanf("%d", &n);
  T = max(1, (int)sqrt(n));
  FOR(i, 1, n) scanf("%d", a + i);
  scanf("%d", &m);
  FOR(i, 1, m) {
    scanf("%d%d%d%d", &qry[i].a, &qry[i].b, &qry[i].c, &qry[i].d);
    qry[i].e = i;
  }
  sort(qry + 1, qry + m + 1, cmp);

  FOR(i, 1, m) {
    int l = qry[i].a, r = qry[i].b;
    while (R < r) addR();
    while (l < L) subL();
    while (R > r) subR();
    while (L < l) addL();
    ans[qry[i].e] = query(qry[i].c, qry[i].d);
  }
  FOR(i, 1, m) printf("%d\n", ans[i]);
  return 0;
}
