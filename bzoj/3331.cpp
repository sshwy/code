#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
namespace IO {
  char nc() {
    static char bf[100000], *p1 = bf, *p2 = bf;
    return p1 == p2 && (p2 = (p1 = bf) + fread(bf, 1, 100000, stdin), p1 == p2)
               ? EOF
               : *p1++;
  }
  int rd() {
    int res = 0;
    char c = getchar();
    while (!isdigit(c)) c = getchar();
    while (isdigit(c)) res = res * 10 + c - '0', c = getchar();
    return res;
  }
} // namespace IO
/******************heading******************/
const int N = 5e5 + 5, M = N;

struct qxx {
  int nex, t;
};
qxx e[M];
int h[N], le = 1;
void add_path(int f, int t) { e[++le] = (qxx){h[f], t}, h[f] = le; }

int n, m, q;

int dfn[N], low[N], totdfn;
int s[N], tp;
int vcc;
namespace G {
  qxx G_e[N * 2];
  int G_h[M * 2], G_le = 1;
  int dep[N * 2], fa[N * 2][20], val[N * 2];
  void G_add_path(int f, int t) { G_e[++G_le] = (qxx){G_h[f], t}, G_h[f] = G_le; }
  void G_add_both(int f, int t) { G_add_path(f, t), G_add_path(t, f); }
  void dfs(int u, int p) {
    // if(u<=n)printf("\033[31mdfs(%d,%d)\033[0m\n",u,p);
    // else printf("\033[32mdfs(%d,%d)\033[0m\n",u,p);
    dep[u] = dep[p] + 1, fa[u][0] = p;
    FOR(i, 1, 19) {
      fa[u][i] = fa[fa[u][i - 1]][i - 1];
      if (!fa[u][i]) break;
    }
    for (int i = G_h[u]; i; i = G_e[i].nex) {
      const int v = G_e[i].t;
      if (v == p) continue;
      dfs(v, u);
    }
  }
  void make(int u, int v) {
    // printf("make(%d,%d)\n",u,v);
    if (dep[u] < dep[v]) swap(u, v);
    int u1 = u, v1 = v;
    ROF(i, 19, 0) {
      if (dep[u] - (1 << i) <= dep[v]) continue;
      u = fa[u][i];
    }
    if (fa[u][0] == v) {
      // printf("lca=%d\n",v);
      val[u1]++, val[fa[v][0]]--;
      return;
    }
    if (dep[u] > dep[v]) u = fa[u][0];
    ROF(i, 19, 0) {
      if (fa[u][i] == fa[v][i]) continue;
      u = fa[u][i], v = fa[v][i];
    }
    u = fa[u][0];
    // printf("lca=%d\n",u);
    val[u1]++, val[v1]++, val[u]--, val[fa[u][0]]--;
  }
  void dfs2(int u, int p) {
    // printf("dfs2(%d,%d)\n",u,p);
    for (int i = G_h[u]; i; i = G_e[i].nex) {
      const int v = G_e[i].t;
      if (v == p) continue;
      dfs2(v, u);
      val[u] += val[v];
    }
  }
} // namespace G
void tarjan(int u, int p) {
  // printf("tarjan(%d,%d)\n",u,p);
  dfn[u] = low[u] = ++totdfn;
  s[++tp] = u;
  for (int i = h[u]; i; i = e[i].nex) {
    const int v = e[i].t;
    if (!dfn[v]) {
      tarjan(v, u);
      low[u] = min(low[u], low[v]);
      if (dfn[u] <= low[v]) {
        ++vcc;
        // printf("VCC#%d: ",vcc);
        // printf("%d ",u);
        G::G_add_both(u, vcc + n);
        do {
          G::G_add_both(s[tp], vcc + n);
          // printf("%d ",s[tp]);
        } while (s[tp--] != v);
        // puts("");
      }
    } else
      low[u] = min(low[u], dfn[v]);
  }
}

int main() {
  scanf("%d%d%d", &n, &m, &q);
  FOR(i, 1, m) { // BUG#1:m写成了n
    int u, v;
    scanf("%d%d", &u, &v);
    add_path(u, v);
    add_path(v, u);
  }
  tarjan(1, 0);
  G::dfs(1, 0);
  FOR(i, 1, q) {
    int u, v;
    scanf("%d%d", &u, &v);
    G::make(u, v);
  }
  G::dfs2(1, 0);
  FOR(i, 1, n) printf("%d\n", G::val[i]);
  return 0;
}
/*
 * BUG#2:写完一定要测极限数据！不然莫名RE
 */
