#include <algorithm>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <string>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
/******************heading******************/
const int N = 1e3 + 5, LIM = 1e9;
int n;
int x[N], y[N], w[N];
double ax, ay, ca; // calc_a

double rlf() { return rand() * 1.0 / RAND_MAX; }

double calc(double nx, double ny) { //计算(nx,ny)点离平衡点的代价
  double rx = 0, ry = 0;            // x,y方向上的合力
  FOR(i, 1, n) {
    double dx = nx - x[i], dy = ny - y[i];
    double dz = sqrt(dx * dx + dy * dy);
    rx += dx / dz * w[i];
    ry += dy / dz * w[i];
  }
  return sqrt(rx * rx + ry * ry);
}

bool P(double e, double t) { //对于决策更劣的情况，返回是否转移
  return rlf() < exp(-e / t);
}

void simulate_anneal() {
  double t = 100000;
  const double D = 0.97;
  while (t > 1e-3) {
    double nx = ax + t * (rlf() * 2 - 1); //在[-t,t]的实数内
    double ny = ay + t * (rlf() * 2 - 1); //在[-t,t]的实数内
    double cn = calc(nx, ny);             //计算能量
    if (cn < ca || P(abs(cn - ca), t)) ax = nx, ay = ny, ca = cn; //能量更小，直接转移
    t *= D;
    printf("t:%.3lf,  %.3lf %.3lf %.3lf\n", t, ax, ay, ca);
  }
}
int main() {
  srand(time(0));
  scanf("%d", &n);
  FOR(i, 1, n) {
    scanf("%d%d%d", &x[i], &y[i], &w[i]);
    ax += x[i], ay += y[i];
  }
  ax /= n, ay /= n;
  ca = calc(ax, ay);
  printf("%.3lf %.3lf %.3lf\n", ax, ay, ca);
  simulate_anneal();
  printf("%.3lf %.3lf\n", ax, ay);
  return 0;
}
/*
 * 我们设计一个函数，来计算两个点之间的能量差。
 * 以往在做一类用到状态转移思想的题目，我们会直接转移到最优状态
 * 而这次我们不这么干，我们快速判断状态的优劣之后，不忙下定是否转移
 * 而是根据一个概览函数来决定。这样就有可能转移到一个更劣的决策
 * 但是更劣的决策有一定的概览导向更优的解。
 */
