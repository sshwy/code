#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
namespace IO {
  char nc() {
    static char bf[100000], *p1 = bf, *p2 = bf;
    return p1 == p2 && (p2 = (p1 = bf) + fread(bf, 1, 100000, stdin), p1 == p2)
               ? EOF
               : *p1++;
  }
  int rd() {
    int res = 0;
    char c = getchar();
    while (!isdigit(c)) c = getchar();
    while (isdigit(c)) res = res * 10 + c - '0', c = getchar();
    return res;
  }
} // namespace IO
/******************heading******************/
using namespace IO;
const int N = 5e5 + 5;
struct disjoint {
  int f[400005], dep[400005];
  void init(int n) { FOR(i, 0, n) f[i] = i, dep[i] = 0; }
  int get(int x) {
    if (f[x] == x) return x;
    int res = get(f[x]);
    dep[x] += dep[f[x]];
    return f[x] = res;
  }
  bool find(int x, int y) { return get(x) == get(y); }
  void merge(int x, int y, int p) {
    x = get(x), y = get(y), f[x] = f[y] = p, dep[x] = dep[y] = 1;
  }
} s;

int n, m, k;
int g[200005], c[N], d[N], dep[400005];
int p[400005][18], w[400005][18], tot;

int calc(int x, int y) {
  int res = 0;
  if (dep[x] < dep[y]) swap(x, y);
  ROF(j, 17, 0) {
    if (dep[x] - (1 << j) < dep[y]) continue;
    if (p[x][j] == y) continue;
    res = max(res, w[x][j]);
    x = p[x][j];
  }
  if (p[x][0] == y) return res;
  ROF(j, 17, 0) {
    if (dep[x] <= (1 << j)) continue;
    if (p[x][j] == p[y][j]) continue;
    res = max(res, w[x][j]);
    res = max(res, w[y][j]);
    x = p[x][j], y = p[y][j];
  }
  res = max(res, w[x][0]);
  res = max(res, w[y][0]);
  return res;
}
pii q[N];
int lq;
lld ans;
int main() {
  n = rd(), m = rd(), k = rd();
  FOR(i, 1, n) g[i] = rd();
  s.init(n * 2), tot = n;
  FOR(i, 1, m) {
    int a = rd(), b = rd();
    int x = s.get(a), y = s.get(b);
    if (s.find(x, y)) continue;
    s.merge(x, y, ++tot);
    p[x][0] = p[y][0] = tot, w[x][0] = w[y][0] = i;
  }
  FOR(i, 1, n * 2) {
    s.get(i);
    dep[i] = s.dep[i] + 1;
  }
  FOR(j, 1, 17) FOR(i, 1, n * 2) {
    if (dep[i] <= (1 << j)) continue;
    p[i][j] = p[p[i][j - 1]][j - 1];
    w[i][j] = max(w[i][j - 1], w[p[i][j - 1]][j - 1]);
  }
  FOR(i, 1, k) c[i] = rd(), d[i] = rd();
  FOR(i, 1, k) q[i] = mk(calc(c[i], d[i]), i);
  sort(q + 1, q + k + 1);
  FOR(i, 1, k) {
    pii cur = q[i];
    int x = c[cur.se], y = d[cur.se];
    if (s.get(x) != s.get(y)) continue;
    int d = min(g[x], g[y]);
    ans += d * 2ll;
    g[x] -= d, g[y] -= d;
  }
  printf("%lld\n", ans);
  return 0;
}
