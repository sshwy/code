#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
namespace IO {
  char nc() {
    static char bf[100000], *p1 = bf, *p2 = bf;
    return p1 == p2 && (p2 = (p1 = bf) + fread(bf, 1, 100000, stdin), p1 == p2)
               ? EOF
               : *p1++;
  }
  int rd() {
    int res = 0;
    char c = getchar();
    while (!isdigit(c)) c = getchar();
    while (isdigit(c)) res = res * 10 + c - '0', c = getchar();
    return res;
  }
} // namespace IO
/******************heading******************/

int a, b, c, xl, xr, yl, yr;
int exgcd(int a, int b, int &x, int &y) {
  if (!b) return x = 1, y = 0, a;
  int t = exgcd(b, a % b, y, x);
  return y = y - (a / b) * x, t;
}

int ce(int m, int n) {
  bool fg = (m < 0) ^ (n < 0);
  m = abs(m), n = abs(n);
  if (fg)
    return -(m / n);
  else
    return m / n + (m % n != 0);
}
int fl(int m, int n) {
  bool fg = (m < 0) ^ (n < 0);
  m = abs(m), n = abs(n);
  if (fg)
    return -ce(m, n);
  else
    return m / n;
}
int main() {
  scanf("%d%d%d%d%d%d%d", &a, &b, &c, &xl, &xr, &yl, &yr);
  if (a == 0 && b == 0) {
    if (c == 0)
      return printf("%lld", (xr - xl + 1ll) * (yr - yl + 1)), 0;
    else
      return printf("0"), 0;
  }
  if (a == 0) {
    if (c % b) return printf("0"), 0;
    if (c / b >= yl && c / b <= yr)
      return printf("%lld", xr - xl + 1), 0;
    else
      return printf("0"), 0;
  }
  if (b == 0) {
    if (c % a) return printf("0"), 0;
    if (c / a >= xl && c / a <= xr)
      return printf("%lld", yr - yl + 1), 0;
    else
      return printf("0"), 0;
  }
  c = -c;
  int x, y, g = exgcd(a, b, x, y);
  if (c % g) return puts("0"), 0;
  x *= c / g, y *= c / g;
  a = -a / g, b = b / g;
  xl -= x, xr -= x;
  int l1, r1;
  if (b1 >= 0)
    l1 = ce(xl, b1), r1 = fl(xr, b1);
  else
    l1 = ce(xr, b1), r1 = fl(xl, b1);
  yr = y - yr, yl = y - yl;
  int l2, r2;
  if (a1 >= 0)
    l2 = ce(yr, a1), r2 = fl(yl, a1);
  else
    l2 = ce(yl, a1), r2 = fl(yr, a1);
  l1 = max(l1, l2), r1 = min(r1, r2);
  printf("%d\n", max(r1 - l1 + 1, 0));
  return 0;
}
