#include <cstdio>
using namespace std;
#define int long long
int n, l, r, ans;
signed main() {
  scanf("%lld%lld%lld", &n, &l, &r);
  for (int i = l; i < r; i++) {
    int m = i, res = 1;
    while (m) res *= m % 10, m /= 10;
    if (0 < res && res <= n) ans++;
  }
  printf("%lld\n", ans);
  return 0;
}
