#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 1e5 + 5;

struct qxx {
  int nex, t, v;
};
qxx e[N * 2];
int h[N], le = 1;
void add_path(int f, int t, int v) { e[++le] = (qxx){h[f], t, v}, h[f] = le; }
#define FORe(i, u, v, w) \
  for (int i = h[u], v, w; v = e[i].t, w = e[i].v, i; i = e[i].nex)

int n, L, U;
int hgh[N], hson[N], fw[N];

namespace seg {
  double val[N << 2], tag[N << 2];
  inline void node_add(int u, double v) { val[u] += v, tag[u] += v; }
  inline void pushdown(int u) {
    if (abs(tag[u]) > 1e-4)
      node_add(u << 1, tag[u]), node_add(u << 1 | 1, tag[u]), tag[u] = 0;
  }
  inline void pushup(int u) { val[u] = max(val[u << 1], val[u << 1 | 1]); }
  void init(int u = 1, int l = 1, int r = n) {
    val[u] = 0;
    if (l == r) return;
    int mid = (l + r) >> 1;
    init(u << 1, l, mid), init(u << 1 | 1, mid + 1, r);
  }
  void assign(int pos, double v, int u = 1, int l = 1, int r = n) {
    if (l == r) return val[u] = v, void();
    int mid = (l + r) >> 1;
    pushdown(u);
    if (pos <= mid)
      assign(pos, v, u << 1, l, mid);
    else
      assign(pos, v, u << 1 | 1, mid + 1, r);
    pushup(u);
  }
  void assign_max(int pos, double v, int u = 1, int l = 1, int r = n) {
    if (l == r) return val[u] = max(val[u], v), void();
    int mid = (l + r) >> 1;
    pushdown(u);
    if (pos <= mid)
      assign_max(pos, v, u << 1, l, mid);
    else
      assign_max(pos, v, u << 1 | 1, mid + 1, r);
    pushup(u);
  }
  void add(int L, int R, double v, int u = 1, int l = 1, int r = n) {
    if (L <= l && r <= R) return node_add(u, v);
    int mid = (l + r) >> 1;
    pushdown(u);
    if (L <= mid) add(L, R, v, u << 1, l, mid);
    if (mid < R) add(L, R, v, u << 1 | 1, mid + 1, r);
    pushup(u);
  }
  double query_max(int L, int R, int u = 1, int l = 1, int r = n) {
    if (L > R || R < l || r < L) return -1e18;
    if (L <= l && r <= R) return val[u];
    int mid = (l + r) >> 1;
    pushdown(u);
    return max(
        query_max(L, R, u << 1, l, mid), query_max(L, R, u << 1 | 1, mid + 1, r));
  }
  inline double query(int pos) { return query_max(pos, pos); }
} // namespace seg

void dfs1(int u, int p, int val) {
  hgh[u] = 1, fw[u] = val;
  int mx = 0, son = -1;
  FORe(i, u, v, w) if (v != p) dfs1(v, u, w), hgh[u] = max(hgh[u], hgh[v] + 1),
                                              hgh[v] > mx ? mx = hgh[v], son = v : 0;
  hson[u] = son;
}
int la, id[N];
int dfs2(int u, int p, double x) {
  ++la;
  id[u] = la - 1;
  if (hson[u] == -1) { return 0; }
  if (dfs2(hson[u], u, x)) return 1;
  FORe(i, u, v, w) if (v != p && v != hson[u]) if (dfs2(v, u, x)) return 1;
  //继承
  seg::assign(id[u] + 1, fw[hson[u]] - x);
  seg::add(id[u] + 2, id[u] + hgh[u] - 1, fw[hson[u]] - x);
  FORe(i, u, v, w) {
    if (v == p || v == hson[u]) continue;
    // j==0
    // f[u,L-1..U-1]
    if (max(L - 1 <= 0 && 0 <= U - 1 ? 0 : -1e18,
            seg::query_max(id[u] + max(1, L - 1), id[u] + min(hgh[u] - 1, U - 1))) +
            w - x >
        0)
      return 1;
    //算答案
    FOR(j, 1, hgh[v] - 1) { // f[v,j]
      // f[v,j] f[u,L-j-1,U-j-1]
      if (seg::query(id[v] + j) +
              max(L - j - 1 <= 0 && 0 <= U - j - 1 ? 0 : -1e18,
                  seg::query_max(id[u] + max(1, L - j - 1),
                      id[u] + min(hgh[u] - 1, U - j - 1))) +
              w - x >
          0)
        return 1;
    }
    //合并
    // j==0
    seg::assign_max(id[u] + 1, w - x);
    FOR(j, 1, hgh[v] - 1) { // f[v,j]
      seg::assign_max(id[u] + j + 1, seg::query(id[v] + j) + w - x);
    }
  }
  if (seg::query_max(id[u] + L, id[u] + min(hgh[u] - 1, U)) > 0) return 1;
  return 0;
}
bool check(double x) {
  la = 0;
  seg::init();
  return dfs2(1, 0, x);
}
int main() {
  scanf("%d%d%d", &n, &L, &U);
  FOR(i, 1, n - 1) {
    int a, b, v;
    scanf("%d%d%d", &a, &b, &v);
    add_path(a, b, v), add_path(b, a, v);
  }
  double l = 0, r = 1e6, mid;
  dfs1(1, 0, 0);
  while (r - l > 1e-4) mid = (l + r) / 2, check(mid) ? l = mid : r = mid;
  printf("%.3lf", l);
  return 0;
}
