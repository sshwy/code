#include <bits/stdc++.h>
using namespace std;
int n;
int a[100001], b[100001];
int main() {
  scanf("%d", &n);
  for (int i = 1; i <= n; i++) scanf("%d", &a[i]);
  for (int i = 1; i <= n; i++) scanf("%d", &b[i]);
  sort(a + 1, a + n + 1);
  sort(b + 1, b + n + 1);
  // a win b
  int ib = 1, ta = 0;
  for (int i = 1; i <= n; i++) { // a[i]
    if (a[i] > b[ib])
      ta += 2, ++ib;
    else if (a[i] == b[ib])
      ta += 1, ++ib;
  }
  // b win a
  int ia = 1, tb = 0;
  for (int i = 1; i <= n; i++) { // b[i]
    if (b[i] > a[ia])
      tb += 2, ++ia;
    else if (b[i] == a[ia])
      tb += 1, ++ia;
  }
  printf("%d %d", ta, (n << 1) - tb);
  return 0;
}
//
// 2 4 5 6 7 8 11
// 1 3 4 8 8 9 10
