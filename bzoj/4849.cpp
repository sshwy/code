// by Sshwy
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA
const int N = 1e5 + 5, INF = 1e9;
int n, m;
int c[N], f[N], up[N], dn[N]; // up flow & down flow

int val(int u) {
  if (up[u])
    return f[u] - 1;
  else
    return f[u] + 1;
}
void pushup(int u) {
  f[u] = INF;
  if (c[u]) f[u] = 0;
  if (u * 2 <= n) f[u] = min(f[u], val(u * 2));
  if (u * 2 + 1 <= n) f[u] = min(f[u], val(u * 2 + 1));
}
void init(int u) {
  if (u * 2 <= n) init(u * 2);
  if (u * 2 + 1 <= n) init(u * 2 + 1);
  pushup(u);
}
int ans;
void to(int u) {
  if (up[u])
    --up[u];
  else
    ++dn[u];
  if (c[u]) return --c[u], pushup(u), void();
  assert(u * 2 <= n);
  if (u * 2 + 1 > n)
    to(u * 2);
  else if (val(u * 2) < val(u * 2 + 1))
    to(u * 2);
  else
    to(u * 2 + 1);
  pushup(u);
}
void upup(int u) {
  pushup(u);
  if (u != 1) upup(u / 2);
}
void insert(int u) {
  int mv = INF;
  mv = min(mv, f[u]);
  int acc = 0;
  for (int v = u; v != 1; v /= 2) {
    if (dn[v])
      --acc; // reverse flow
    else
      ++acc;
    if (c[v / 2]) mv = min(mv, acc); // u -> v/2
    mv = min(mv, acc + val(v ^ 1));  // u -> v/2 -> v^1
  }
  ans += mv;
  acc = 0;
  if (c[u]) return --c[u], upup(u), void();
  if (u * 2 <= n && val(u * 2) == mv) return to(u * 2), upup(u), void();
  if (u * 2 + 1 <= n && val(u * 2 + 1) == mv) return to(u * 2 + 1), upup(u), void();
  for (int v = u; v != 1; v /= 2) {
    if (dn[v])
      --acc, --dn[v];
    else
      ++acc, ++up[v];
    pushup(v / 2);
    if (c[v / 2]) return --c[v / 2], upup(v / 2), void();
    if (mv == acc + val(v ^ 1)) return to(v ^ 1), upup(v / 2), void();
  }
}
int main() {
  fill(f, f + N, INF);
  scanf("%d%d", &n, &m);
  FOR(i, 1, n) scanf("%d", &c[i]);
  init(1);
  FOR(i, 1, m) {
    int x;
    scanf("%d", &x);
    insert(x);
    printf("%d%c", ans, " \n"[i == m]);
  }
  return 0;
}
