#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
#define int long long
const int N = 1 << 19, P = 998244353;

int tr[N];
int pw(int a, int m) {
  int res = 1;
  if (m < 0) m += P - 1;
  while (m) m & 1 ? res = 1ll * res * a % P : 0, a = 1ll * a * a % P, m >>= 1;
  return res;
}
int init(int l) {
  int len = 1;
  while (len <= l) len <<= 1;
  FOR(i, 0, len - 1) tr[i] = tr[i >> 1] >> 1 | ((i & 1) * (len >> 1));
  return len;
}
void dft(int *f, int len, int tag) {
  FOR(i, 0, len - 1) if (i < tr[i]) swap(f[i], f[tr[i]]);
  for (int j = 1; j < len; j <<= 1)
    for (int i = 0, step = pw(3, (P - 1) / (j << 1) * tag); i < len; i += j << 1)
      for (int k = i, u, v, w = 1; k < i + j; k++, w = 1ll * w * step % P)
        u = f[k], v = 1ll * f[k + j] * w % P, f[k] = (u + v) % P,
        f[k + j] = (u - v + P) % P;
  if (tag == -1) {
    int ilen = pw(len, P - 2);
    FOR(i, 0, len - 1) f[i] = 1ll * f[i] * ilen % P;
  }
}

int n, k;
int fac[N], fnv[N];
int S[N], g[N], f[N];

signed main() {
  scanf("%lld%lld", &n, &k);
  --n;

  fac[0] = 1;
  FOR(i, 1, k) fac[i] = 1ll * fac[i - 1] * i % P;
  fnv[k] = pw(fac[k], P - 2);
  ROF(i, k, 1) fnv[i - 1] = 1ll * fnv[i] * i % P;

  FOR(i, 0, k) S[i] = 1ll * pw(i, k) * fnv[i] % P;
  FOR(i, 0, k) g[i] = 1ll * (1ll - 2 * (i & 1)) * fnv[i] % P;

  int len = init(k + k);
  dft(S, len, 1), dft(g, len, 1);
  FOR(i, 0, len - 1) S[i] = 1ll * S[i] * g[i] % P;
  dft(S, len, -1);

  // FOR(i,0,k)printf("%lld%c",S[i]," \n"[i==k]);
  FOR(i, 0, k) S[i] = (S[i] + P) % P;

  f[0] = 1;
  FOR(i, 1, k) f[i] = 1ll * f[i - 1] * (n - i + 1) % P;
  // FOR(i,1,k)printf("%lld%c\n",f[i]," \n"[i==k]);

  int ans = 0;
  FOR(i, 0, k) { ans = (ans + 1ll * S[i] * pw(2, n - i) % P * f[i]) % P; }
  // printf("%lld\n",ans);
  // printf("%lld\n",n*(n-1)/2);
  ans = 1ll * ans * pw(2, ((n * (n - 1ll)) >> 1) % (P - 1)) % P;
  ans = ans * (n + 1) % P; // n事先减了1
  printf("%lld\n", ans);
  return 0;
}
