#include <cstdio>
#define int long long
using namespace std;
const int P = 1e9 + 7;
int ans;
signed main() {
  int l, r;
  scanf("%lld%lld", &l, &r);
  for (int i = l; i <= r; i++) {
    int d[30], ld = 0, m = i;
    while (m) d[++ld] = m % 10, m /= 10;
    for (int j = 1; j < ld; j++) {
      if (d[j] == d[j + 1] || (j + 1 < ld && d[j] == d[j + 2])) {
        ans = (ans + 1) % P;
        break;
      }
    }
  }
  printf("%lld\n", ans);
  return 0;
}
