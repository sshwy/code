#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
namespace IO {
  char nc() {
    static char bf[100000], *p1 = bf, *p2 = bf;
    return p1 == p2 && (p2 = (p1 = bf) + fread(bf, 1, 100000, stdin), p1 == p2)
               ? EOF
               : *p1++;
  }
  int rd() {
    int res = 0;
    char c = getchar();
    while (!isdigit(c)) c = getchar();
    while (isdigit(c)) res = res * 10 + c - '0', c = getchar();
    return res;
  }
} // namespace IO
/******************heading******************/
const int N = 100;

int sr, sb, sg, m, P, n;
int p[N];
int a[N], la;
int f[N][N][N];
bool vis[N];

void DP() {
  memset(f, 0, sizeof(f));
  f[0][0][0] = 1;
  FOR(i, 1, la) {
    ROF(x, sr, 0) {
      ROF(y, sb, 0) {
        ROF(z, sg, 0) {
          if (x >= a[i]) f[x][y][z] += f[x - a[i]][y][z];
          if (y >= a[i]) f[x][y][z] += f[x][y - a[i]][z];
          if (z >= a[i]) f[x][y][z] += f[x][y][z - a[i]];
          f[x][y][z] %= P;
          // printf("f[%d,%d,%d,%d]=%d\n",i,x,y,z,f[x][y][z]);
        }
      }
    }
  }
}
int dfs(int u) {
  vis[u] = 1;
  if (vis[p[u]]) return 1;
  return dfs(p[u]) + 1;
}
int calc() {
  // printf("calc()\n");
  la = 0;
  FOR(i, 1, n) vis[i] = 0;
  FOR(i, 1, n) if (!vis[i]) a[++la] = dfs(i);
  // FOR(i,1,la)printf("%d%c",a[i]," \n"[i==la]);
  DP();
  return f[sr][sb][sg];
}
int ans;
int pw(int a, int m, int p) {
  int res = 1;
  while (m) m & 1 ? res = res * a % P : 0, a = a * a % P, m >>= 1;
  return res;
}
int main() {
  scanf("%d%d%d%d%d", &sr, &sb, &sg, &m, &P);
  n = sr + sb + sg;
  FOR(i, 1, m) {
    FOR(i, 1, n) scanf("%d", &p[i]);
    ans += calc();
    ans %= P;
  }
  FOR(i, 1, n) p[i] = i;
  ans += calc();
  ans %= P;
  ans = ans * pw(m + 1, P - 2, P) % P;
  printf("%d\n", ans);
  return 0;
}
/*
 * BUG#1:没有加入恒等置换
 */
