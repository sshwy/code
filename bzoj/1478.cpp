#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
namespace IO {
  char nc() {
    static char bf[100000], *p1 = bf, *p2 = bf;
    return p1 == p2 && (p2 = (p1 = bf) + fread(bf, 1, 100000, stdin), p1 == p2)
               ? EOF
               : *p1++;
  }
  int rd() {
    int res = 0;
    char c = getchar();
    while (!isdigit(c)) c = getchar();
    while (isdigit(c)) res = res * 10 + c - '0', c = getchar();
    return res;
  }
} // namespace IO
/******************heading******************/
const int N = 4e5 + 5;
int n, m, p;

int a[N], c[N];
long long ans;
long long fac[N], fnv[N];
long long pw(long long a, long long m, long long p) {
  long long res = 1;
  while (m) m & 1 ? res = res * a % p : 0, a = a * a % p, m >>= 1;
  return res;
}
long long gcd(long long a, long long b) { return b ? gcd(b, a % b) : a; }
void calc(int len) {
  // printf("calc:\n");
  // FOR(i,1,len)printf("%d%c",a[i]," \n"[i==len]);
  // FOR(i,1,len)printf("%d%c",c[i]," \n"[i==len]);
  long long C = 0, S = fac[n];
  FOR(i, 1, len) C = (C + a[i] / 2 * c[i]) % p;
  FOR(i, 1, len) {
    C = (C + a[i] * (c[i] * (c[i] - 1) / 2 % p)) % p;
    FOR(j, 1, i - 1) C = (C + gcd(a[i], a[j]) * c[i] % p * c[j]) % p;
  }
  FOR(i, 1, len) {
    S = S * pw(fnv[a[i]] * fac[a[i] - 1] % p, c[i], p) % p * fnv[c[i]] % p;
  }
  ans = (ans + S * pw(m, C, p)) % p;
}
void dfs(int k, int s) {
  if (s == 0) {
    calc(k - 1);
    return;
  }
  if (s <= a[k - 1]) return;
  FOR(i, a[k - 1] + 1, s / 2) {
    int lim = s / i;
    a[k] = i;
    FOR(j, 1, lim) {
      c[k] = j;
      dfs(k + 1, s - i * j);
    }
  }
  a[k] = s, c[k] = 1, calc(k);
}
void init() {
  fac[0] = 1;
  FOR(i, 1, n) fac[i] = fac[i - 1] * i % p;
  fnv[n] = pw(fac[n], p - 2, p);
  ROF(i, n, 1) fnv[i - 1] = fnv[i] * i % p;
}
int main() {
  scanf("%d%d%d", &n, &m, &p);
  init();
  dfs(1, n);
  ans = ans * fnv[n] % p;
  printf("%lld\n", ans);
  return 0;
}
