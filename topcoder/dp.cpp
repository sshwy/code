#include <cstdio>
#include <cstdlib>
#include <ctime>
using namespace std;

int main() {
  system("g++ 14250.cpp -o .usr");
  system("g++ a2.cpp -o .std");
  system("g++ gen.cpp -o .gen");
  int t = 0;
  while (++t) {
    system("./.gen > .fin");
    if (system("./.usr < .fin > .fout")) break;
    system("./.std < .fin > .fstd");
    if (system("diff -w .fout .fstd")) break;
    printf("AC#%d\n", t);
  }
  printf("WA#%d\n", t);
  return 0;
}
