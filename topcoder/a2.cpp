#include <bits/stdc++.h>
using namespace std;

const int maxn = 14, maxk = 1 << maxn, maxm = 1 << 20, mod = 998244353;

class HamiltonianPaths {
public:
  bool a[maxn + 3][maxn + 3];
  int n, k, dp[maxk + 3][maxn + 3], cnt[maxk + 3], f[maxk + 3][maxn + 3];
  int g[maxm + 3], h[maxm + 3], g_n, h_n, lim, bit, rev[maxm + 3], A[maxm + 3],
      B[maxm + 3];

  inline int func(const int &x) { return x < mod ? x : x - mod; }

  int qpow(int a, int b) {
    b < 0 ? b += mod - 1 : 0;
    int c = 1;
    for (; b; b >>= 1, a = 1ll * a * a % mod) {
      if (b & 1) c = 1ll * a * c % mod;
    }
    return c;
  }

  void dft(int a[], int n, int type) {
    for (int i = 0; i < n; i++) {
      if (i < rev[i]) swap(a[i], a[rev[i]]);
    }
    for (int k = 1; k < n; k <<= 1) {
      int x = qpow(3, (mod - 1) / (k << 1) * type);
      for (int i = 0; i < n; i += k << 1) {
        int y = 1;
        for (int j = i; j < i + k; j++, y = 1ll * x * y % mod) {
          int p = a[j], q = 1ll * a[j + k] * y % mod;
          a[j] = func(p + q), a[j + k] = func(p - q + mod);
        }
      }
    }
    if (type == -1) {
      int x = qpow(n, mod - 2);
      for (int i = 0; i < n; i++) { a[i] = 1ll * a[i] * x % mod; }
    }
  }

  void mult(int a[], int &n, int b[], int m) {
    for (lim = 1, bit = 0; lim <= n + m; lim <<= 1) bit++;
    for (int i = 1; i < lim; i++) {
      rev[i] = (rev[i >> 1] >> 1) | ((i & 1) << (bit - 1));
    }
    for (int i = 0; i <= n; i++) { A[i] = a[i]; }
    for (int i = 0; i <= m; i++) { B[i] = b[i]; }
    fill(A + n + 1, A + lim, 0);
    fill(B + m + 1, B + lim, 0);
    dft(A, lim, 1), dft(B, lim, 1);
    for (int i = 0; i < lim; i++) { A[i] = 1ll * A[i] * B[i] % mod; }
    dft(A, lim, -1);
    n += m;
    for (int i = 0; i <= n; i++) { a[i] = A[i]; }
  }

  int countPaths(int N, vector<int> A, vector<int> B, int K) {
    n = N, k = K;
    for (int i = 0; i < A.size(); i++) {
      a[A[i] + 1][B[i] + 1] = true;
      a[B[i] + 1][A[i] + 1] = true;
    }
    for (int i = 1; i <= n; i++) { dp[1 << (i - 1)][i] = 1; }
    for (int msk = 1; msk < 1 << n; msk++) {
      for (int i = 1; i <= n; i++)
        if (dp[msk][i]) {
          for (int j = 1; j <= n; j++)
            if (a[i][j] && (~msk >> (j - 1) & 1)) {
              dp[msk | (1 << (j - 1))][j] =
                  func(dp[msk | (1 << (j - 1))][j] - dp[msk][i] + mod);
            }
        }
      for (int i = 1; i <= n; i++)
        if (dp[msk][i]) { cnt[msk] = func(cnt[msk] + dp[msk][i]); }
    }
    f[0][0] = 1;
    for (int msk = 1; msk < 1 << n; msk++) {
      int p = 0;
      for (int i = 1; i <= n; i++) {
        if (msk >> (i - 1) & 1) {
          p = i;
          break;
        }
      }
      int t = msk ^ (1 << (p - 1));
      for (int k = 0; k < n; k++) { f[msk][k + 1] = f[t][k]; }
      for (int i = t; i; i = (i - 1) & t) {
        for (int k = 0; k < n; k++) {
          f[msk][k + 1] = (f[msk][k + 1] + 1ll * cnt[i | (1 << (p - 1))] *
                                               f[msk ^ (i | 1 << (p - 1))][k]) %
                          mod;
        }
      }
    }
    for (int i = 1; i <= n; i++) {
      g[i] = f[(1 << n) - 1][i];
      printf("%d%c", g[i], " \n"[i == n]);
    }
    h[0] = 1;
    int g_n = n, h_n = 0;
    for (int i = k; i; i >>= 1, mult(g, g_n, g, g_n)) {
      if (i & 1) mult(h, h_n, g, g_n);
    }
    int cur = 1, ret = 0;
    for (int i = 1; i <= n * k; i++) {
      cur = 1ll * cur * i % mod;
      ret = (ret + 1ll * h[i] * cur) % mod;
    }
    return ret;
  }
};

HamiltonianPaths test;

#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
#define fi first
#define se second
#define pb push_back
#define mk make_pair

int main() {
  int k, n, c;
  vector<int> a, b;
  cin >> k >> c;
  FOR(i, 1, c) {
    int x;
    cin >> x;
    a.pb(x);
  }
  FOR(i, 1, c) {
    int x;
    cin >> x;
    b.pb(x);
  }
  cin >> n;
  cout << test.countPaths(k, a, b, n);
  return 0;
}
