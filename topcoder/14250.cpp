#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/

const int SZ = 1 << 21, K = 16, N = 5e4 + 5, P = 998244353;

class HamiltonianPaths {
public:
  vector<int> G[K];
  int lim;

  int pw(int a, int m) {
    if (m < 0) m += P - 1;
    int res = 1;
    while (m) m & 1 ? res = 1ll * res * a % P : 0, a = 1ll * a * a % P, m >>= 1;
    return res;
  }
  int tr[SZ];
  int init(int l) {
    int len = 1;
    while (len <= l) len <<= 1;
    FOR(i, 0, len - 1) tr[i] = tr[i >> 1] >> 1 | ((i & 1) * (len >> 1));
    return len;
  }
  void fft(int *f, int len, int tag) {
    FOR(i, 0, len - 1) if (i < tr[i]) swap(f[i], f[tr[i]]);
    FOR(i, 0, len - 1)
    f[i] = (f[i] + P) % P; // BUG#1:如果传入的数组有负数，fft会出错
    for (int j = 1; j < len; j <<= 1)
      for (int i = 0, wn = pw(3, (P - 1) / (j << 1) * tag); i < len; i += j << 1)
        for (int k = i, u, v, w = 1; k < i + j; k++, w = 1ll * w * wn % P)
          u = f[k], v = 1ll * f[k + j] * w % P, f[k] = (u + v) % P,
          f[k + j] = (u - v + P) % P;
    if (tag == -1) {
      int ilen = pw(len, P - 2);
      FOR(i, 0, len - 1) f[i] = 1ll * f[i] * ilen % P;
    }
  }

  int f[1 << K][K];        //集合S里的路径数乘上容斥贡献，终点为i
  int g[1 << K][K], h[SZ]; //把集合S分成若干个路径的方案数
  int countPaths(int m, vector<int> a, vector<int> b, int n) {
    int c = (int)a.size() - 1;
    FOR(i, 0, c) G[a[i]].pb(b[i]), G[b[i]].pb(a[i]);
    lim = 1 << m;

    FOR(i, 0, m - 1) f[1 << i][i] = 1;
    FOR(i, 0, lim - 1) FOR(j, 0, m - 1) if (i >> j & 1) {
      for (int k : G[j])
        if (!(i >> k & 1)) {
          f[(1 << k) | i][k] = (f[(1 << k) | i][k] - f[i][j]) % P;
        }
      g[i][1] = (g[i][1] + f[i][j]) % P;
    }
    FOR(i, 0, lim - 1) FOR(j, 2, m) {
      for (int k = i; k; k = (k - 1) & i)
        if ((k & -k) == (i & -i)) { //枚举最后一个数的集合
          g[i][j] = (g[i][j] + g[k][1] * 1ll * g[i ^ k][j - 1]) % P;
        }
    }
    FOR(i, 0, m) h[i] = g[lim - 1][i];

    int len = init(n * m);
    fft(h, len, 1);
    FOR(i, 0, len - 1) h[i] = pw(h[i], n);
    fft(h, len, -1);

    int ans = 0, fac = 1;
    FOR(i, 0, len - 1)
    fac = fac * 1ll * max(1, i) % P, ans = (ans + h[i] * 1ll * fac) % P;
    ans = (ans + P) % P;

    return ans;
  }
};

HamiltonianPaths test;

int main() {
  int k, n, c;
  vector<int> a, b;
  cin >> k >> c;
  FOR(i, 1, c) {
    int x;
    cin >> x;
    a.pb(x);
  }
  FOR(i, 1, c) {
    int x;
    cin >> x;
    b.pb(x);
  }
  cin >> n;
  cout << test.countPaths(k, a, b, n) << endl;
  return 0;
}
