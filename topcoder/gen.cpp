#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/

using namespace RA;

vector<pii> a;
int main() {
  srand(clock());
  int n = r(1, 14), m = r(0, n * (n - 1) / 2);
  FOR(i, 0, n - 1) FOR(j, i + 1, n - 1) a.pb({i, j});
  random_shuffle(a.begin(), a.end());
  assert(a.size() == n * (n - 1) / 2);
  printf("%d %d\n", n, m);
  FOR(i, 0, m - 1) printf("%d%c", a[i].fi, " \n"[i == m - 1]);
  FOR(i, 0, m - 1) printf("%d%c", a[i].se, " \n"[i == m - 1]);
  printf("%d\n", r(1, 5e4));
  return 0;
}
