// by Sshwy
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA
const int N = 2e5 + 5, M = 1.5e5 + 5;
int n, m;
struct edge {
  int u, v, w;
};
vector<edge> v[M << 2];
void add(int L, int R, edge e, int u = 1, int l = 0, int r = M - 1) {
  if (R < l || r < L || L > R) return;
  if (L <= l && r <= R) return v[u].pb(e), void();
  int mid = (l + r) >> 1;
  add(L, R, e, u << 1, l, mid);
  add(L, R, e, u << 1 | 1, mid + 1, r);
}
struct qxx {
  int nex, t, v;
} e[N * 2];
int h[N], le = 1;
void add_path(int f, int t, int v) { e[++le] = {h[f], t, v}, h[f] = le; }
long long d[N];
int dep[N], fa[N][22];
void dfs(int u, int p) {
  dep[u] = dep[p] + 1;
  fa[u][0] = p;
  FOR(j, 1, 19) {
    fa[u][j] = fa[fa[u][j - 1]][j - 1];
    if (fa[u][j] == 0) break;
  }
  for (int i = h[u]; i; i = e[i].nex) {
    int v = e[i].t, w = e[i].v;
    if (v != p) {
      d[v] = d[u] + w;
      dfs(v, u);
    }
  }
}
int lca(int u, int v) {
  if (dep[u] < dep[v]) swap(u, v);
  ROF(j, 19, 0) if (dep[u] - (1 << j) >= dep[v]) u = fa[u][j];
  if (u == v) return u;
  ROF(j, 19, 0) if (fa[u][j] != fa[v][j]) u = fa[u][j], v = fa[v][j];
  return fa[u][0];
}
long long dist(int u, int v) { return d[u] + d[v] - 2 * d[lca(u, v)]; }
long long dist(pair<int, int> p) { return dist(p.first, p.second); }
int f[N], g[N];
int get(int u) { return f[u] == u ? u : get(f[u]); }
pair<int, int> dia[N];
stack<pair<int *, int>> rec;
pair<int, int> ad;
void link(edge e) {
  int u = e.u, v = e.v;
  u = get(u), v = get(v);
  assert(u != v);
  if (g[u] > g[v]) swap(u, v);
  int t[] = {dia[u].first, dia[u].second, dia[v].first, dia[v].second};
  long long curdist = dist(dia[v]);
  rec.push({&dia[v].first, dia[v].first});
  rec.push({&dia[v].second, dia[v].second});
  FOR(i, 0, 3) FOR(j, 0, i - 1) {
    long long td = dist(t[i], t[j]);
    if (td > curdist) dia[v] = {t[i], t[j]}, curdist = td;
  }
  rec.push({f + u, f[u]});
  rec.push({g + v, g[v]});
  f[u] = v;
  g[v] += g[u];
  if (dist(ad) < curdist) {
    rec.push({&ad.first, ad.first});
    rec.push({&ad.second, ad.second});
    ad = dia[v];
  }
}
long long ans;
void dfs(int u, int l, int r) {
  int tag = rec.size();
  for (auto e : v[u]) link(e);
  if (l == r) {
    if (l >= m) ans = max(ans, dist(ad) * (l - m));
    while (rec.size() > tag) *rec.top().first = rec.top().second, rec.pop();
    return;
  }
  int mid = (l + r) >> 1;
  dfs(u << 1, l, mid);
  dfs(u << 1 | 1, mid + 1, r);
  while (rec.size() > tag) *rec.top().first = rec.top().second, rec.pop();
}
int main() {
  scanf("%d%d", &n, &m);
  FOR(i, 1, n - 1) {
    int u, v, w;
    scanf("%d%d%d", &u, &v, &w);
    edge eg = {u, v, w};
    add(w, w + m, eg);
    add_path(u, v, w);
    add_path(v, u, w);
  }
  dfs(1, 0);
  FOR(i, 1, n) f[i] = i, g[i] = 1;
  FOR(i, 1, n) dia[i] = {i, i};
  dfs(1, 0, M - 1);
  printf("%lld\n", ans);
  return 0;
}
