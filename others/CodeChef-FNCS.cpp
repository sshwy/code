// by Sshwy
//#define DEBUGGER
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

#ifdef DEBUGGER

#define log_prefix()                                                \
  {                                                                 \
    fprintf(stderr, "\033[37mLine %-3d [%dms]:\033[0m  ", __LINE__, \
        (int)clock() / 1000);                                       \
  }
#define ilog(...) \
  { fprintf(stderr, __VA_ARGS__); }
#define llog(...)      \
  {                    \
    log_prefix();      \
    ilog(__VA_ARGS__); \
  }
#define log(...)           \
  {                        \
    log_prefix();          \
    ilog(__VA_ARGS__);     \
    fprintf(stderr, "\n"); \
  }
#define red(...)       \
  {                    \
    log_prefix();      \
    ilog("\033[31m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define green(...)     \
  {                    \
    log_prefix();      \
    ilog("\033[32m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define blue(...)      \
  {                    \
    log_prefix();      \
    ilog("\033[34m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }

#else

#define log_prefix() ;
#define ilog(...) ;
#define llog(...) ;
#define log(...) ;
#define red(...) ;
#define green(...) ;
#define blue(...) ;

#endif

namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 1e5 + 5, SQRTN = 400;

int n, T, lt;

unsigned long long b[N];
void add(unsigned long long *f, int pos, unsigned long long v) {
  for (int i = pos; i <= n; i += i & -i) f[i] += v;
}
unsigned long long pre(unsigned long long *f, int pos) {
  unsigned long long res = 0;
  for (int i = pos; i > 0; i -= i & -i) res += f[i];
  log("pre(%d)=%lld", pos, res);
  return res;
}

int a[N];
int L[N], R[N];
int f[SQRTN][N];
unsigned long long s[SQRTN];

void assign(int pos, unsigned long long v) {
  add(b, pos, v - a[pos]);
  FOR(i, 0, lt) s[i] += 1ll * f[i][pos] * (v - a[pos]);
  a[pos] = v;
}
unsigned long long query(int l, int r) {
  green("query(%d,%d)", l, r);
  unsigned long long res = 0;
  if (l / T == r / T) {
    FOR(i, l, r) res += pre(b, R[i]) - pre(b, L[i] - 1);
    return res;
  } else {
    int x = min(n, l / T * T + T - 1);
    log("l=%d,T=%d,x=%d", l, T, x);
    FOR(i, l, x) res += pre(b, R[i]) - pre(b, L[i] - 1);
    l = x + 1;

    x = max(1, r / T * T);
    FOR(i, x, r) res += pre(b, R[i]) - pre(b, L[i] - 1);
    r = x - 1;

    l /= T, r /= T;
    FOR(i, l, r) res += s[i];

    return res;
  }
}

int main() {
  scanf("%d", &n);
  FOR(i, 1, n) scanf("%d", &a[i]);
  FOR(i, 1, n) scanf("%d%d", L + i, R + i);

  T = max((int)sqrt(n), 1);
  lt = n / T;

  FOR(i, 0, lt) {
    int l = max(1, i * T), r = min(n, i * T + T - 1);
    int *c = f[i];
    log("i=%d,l=%d,r=%d", i, l, r);
    FOR(j, l, r) {
      c[L[j]]++;
      c[R[j] + 1]--;
    }
    FOR(j, 1, n) c[j] += c[j - 1];
    FOR(j, 1, n) s[i] += c[j] * 1ll * a[j];
    llog("s=%lld, f[%d]: ", s[i], i);
    FOR(j, 1, n) ilog("%d%c", f[i][j], " \n"[j == n]);
  }
  FOR(i, 1, n) add(b, i, a[i]);

  int q;
  scanf("%d", &q);
  FOR(i, 1, q) {
    int x, y, z;
    scanf("%d%d%d", &x, &y, &z);
    if (x == 1) {
      assign(y, z);
    } else {
      printf("%llu\n", query(y, z));
    }
  }

  return 0;
}

// 对函数分块
// 分块大小为T
// 则有n/T块
//
// 整块：
//     预处理f[i][j]表示第i块里，a[j]出现了多少次
//     维护s[i]表示第i个块的询问和。
//     单点修改的时候可以O(n/T)。
//     询问的时候是O(n/T)的。
//
// 零散：
//     枚举每个函数
//     那么可以树状数组维护前缀和
//     这样查询是O(Tlog n)
//     修改是O(log n)
//
// 复杂度是 O(log n+n/T) - O(n/T+Tlog n)
// T=sqrt(n/log n)
// 但这样的话tag数组会开不下
// 于是T稍微调大一点
