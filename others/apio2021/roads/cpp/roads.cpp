#include "roads.h"
#include <bits/stdc++.h>
#define FOR(a, b, c) for (int a = (int)(b); a <= (int)(c); a++)
#define ROF(a, b, c) for (int a = (int)(b); a >= (int)(c); a--)
using namespace std;

typedef long long LL;
void getMin(LL &x, LL y) { x = min(x, y); }
const int N = 1e5 + 5;

struct qxx {
  int nex, t, v;
} e[N * 2];
int h[N], le = 1;
void ae(int u, int v, int w) {
  // printf("ae %d %d %d\n", u, v, w);
  e[++le] = {h[u], v, w}, h[u] = le;
}

LL f[N][2];
// f[u][0]: 不封闭父边
// f[u][1]: 封闭父边
LL t[N];
int lt;

void dfs(int u, int p, int pval, const int k) {
  for (int i = h[u]; i; i = e[i].nex) {
    int v = e[i].t, w = e[i].v;
    if (v == p) continue;
    dfs(v, u, w, k);
  }
  lt = 0;
  LL s = 0;
  for (int i = h[u]; i; i = e[i].nex) {
    int v = e[i].t, w = e[i].v;
    if (v == p) continue;
    t[++lt] = f[v][0] - f[v][1];
    s += f[v][1];
  }
  sort(t + 1, t + lt + 1);
  assert(k > 0);
  f[u][0] = s;
  f[u][1] = s + pval;
  FOR(i, 1, min(k, lt)) { // 恢复 i 条边
    if (t[i] > 0) break;
    s += t[i];
    if (i < k) getMin(f[u][0], s);
    getMin(f[u][1], s + pval);
  }
}
LL calc(int k) {
  // printf("k %d\n", k);
  memset(f, 0x3f, sizeof(f));
  dfs(0, -1, 0, k);
  return f[0][1];
}

vector<LL> minimum_closure_costs(int n, vector<int> U, vector<int> V, vector<int> W) {
  vector<LL> ans(n, 0);
  FOR(i, 0, n - 2) { ans[0] += W[i]; }

  bool flower = true, chain = true;
  FOR(i, 0, n - 2) {
    if (U[i] != 0) flower = false;
    if (U[i] != i || V[i] != i + 1) chain = false;
  }
  if (flower) {
    sort(W.begin(), W.end());
    LL sum = 0;
    FOR(i, 0, n - 2) {
      sum += W[i];
      ans[n - 2 - i] = sum;
    }
    return ans;
  }
  if (chain) {
    memset(f, 0x3f, sizeof(f));
    f[0][0] = 0;
    f[0][1] = W[0];
    FOR(i, 1, n - 2) {
      f[i][0] = f[i - 1][1];
      f[i][1] = W[i] + min(f[i - 1][0], f[i - 1][1]);
    }
    ans[1] = min(f[n - 2][0], f[n - 2][1]);
    return ans;
  }

  FOR(i, 0, n - 2) {
    ae(U[i], V[i], W[i]);
    ae(V[i], U[i], W[i]);
  }
  FOR(i, 1, n - 1) { ans[i] = calc(i); }
  return ans;
}
