#include "jumps.h"
#include <bits/stdc++.h>
#define FOR(a, b, c) for (int a = (int)(b); a <= (int)(c); a++)
#define ROF(a, b, c) for (int a = (int)(b); a >= (int)(c); a--)
using namespace std;

const int N = 2e5 + 5;

int L[N][18], R[N][18], fa[N], anc[N][18], dep[N], smax[N][18], smin[N][18], lg2[N];
int s[N], tp;
vector<int> g[N], h;

void dfs(int u) {
  anc[u][0] = fa[u];
  if (fa[u] != -1) {
    dep[u] = dep[fa[u]] + 1;
    FOR(j, 1, 17) {
      anc[u][j] = anc[anc[u][j - 1]][j - 1];
      if (anc[u][j] == -1) break;
    }
  } else {
    dep[u] = 0;
  }
  for (int v : g[u]) dfs(v);
}

void init(int n, vector<int> H) {
  h = H;
  memset(anc, -1, sizeof(anc));
  memset(L, -1, sizeof(L));
  memset(R, -1, sizeof(R));

  tp = 0;
  FOR(i, 0, n - 1) {
    while (tp && h[s[tp - 1]] <= h[i]) --tp;
    if (tp == 0)
      L[i][0] = -1;
    else
      L[i][0] = s[tp - 1];
    s[tp++] = i;
  }
  tp = 0;
  ROF(i, n - 1, 0) {
    while (tp && h[s[tp - 1]] <= h[i]) --tp;
    if (tp == 0)
      R[i][0] = -1;
    else
      R[i][0] = s[tp - 1];
    s[tp++] = i;
  }
  FOR(i, 0, n - 1) {
    if (~L[i][0] && ~R[i][0])
      fa[i] = h[L[i][0]] > h[R[i][0]] ? L[i][0] : R[i][0];
    else if (L[i][0] == -1)
      fa[i] = R[i][0];
    else
      fa[i] = L[i][0];
  }
  int root = -1;
  FOR(i, 0, n - 1) {
    if (fa[i] != -1)
      g[fa[i]].push_back(i);
    else
      root = i;
  }

  FOR(i, 0, n - 1) {
    if (L[i][0] != -1) {
      FOR(j, 1, 17) {
        L[i][j] = L[L[i][j - 1]][j - 1];
        if (L[i][j] == -1) break;
      }
    }
  }

  ROF(i, n - 1, 0) {
    if (R[i][0] != -1) {
      FOR(j, 1, 17) {
        R[i][j] = R[R[i][j - 1]][j - 1];
        if (R[i][j] == -1) break;
      }
    }
  }

  assert(root != -1);
  dfs(root);

  FOR(i, 2, n) lg2[i] = lg2[i / 2] + 1;

  FOR(i, 0, n - 1) smax[i][0] = i;
  FOR(j, 1, 17) {
    int mxI = n - (1 << j);
    FOR(i, 0, mxI) {
      int x = smax[i][j - 1], y = smax[i + (1 << j - 1)][j - 1];
      smax[i][j] = h[x] > h[y] ? x : y;
    }
  }

  FOR(i, 0, n - 1) smin[i][0] = i;
  FOR(j, 1, 17) {
    int mxI = n - (1 << j);
    FOR(i, 0, mxI) {
      int x = smin[i][j - 1], y = smin[i + (1 << j - 1)][j - 1];
      smin[i][j] = h[x] < h[y] ? x : y;
    }
  }
}

int arg_max(int l, int r) {
  int j = lg2[r - l + 1];
  int x = smax[l][j], y = smax[r - (1 << j) + 1][j];
  return h[x] > h[y] ? x : y;
}

int arg_min(int l, int r) {
  int j = lg2[r - l + 1];
  int x = smin[l][j], y = smin[r - (1 << j) + 1][j];
  return h[x] < h[y] ? x : y;
}

int env_steps;

int go_L(int u, int h_lim) { // go left to somewhere's h <= h_lim
  env_steps = 0;
  ROF(j, 17, 0) if (L[u][j] != -1) {
    if (h[L[u][j]] > h_lim) continue;
    u = L[u][j];
    env_steps += 1 << j;
  }
  return u;
}

int go_R(int u, int h_lim) { // go right to somewhere's h <= h_lim
  env_steps = 0;
  ROF(j, 17, 0) if (R[u][j] != -1) {
    if (h[R[u][j]] > h_lim) continue;
    u = R[u][j];
    env_steps += 1 << j;
  }
  return u;
}

int go(int u, int h_lim) { // go to somewhere's h <= h_lim
  env_steps = 0;
  ROF(j, 17, 0) if (anc[u][j] != -1) {
    if (h[anc[u][j]] > h_lim) continue;
    u = anc[u][j];
    env_steps += 1 << j;
  }
  return u;
}

int minimum_jumps(int A, int B, int C, int D) {
  int x = arg_max(A, B);
  int y = arg_max(C, D);

  if (h[B] > h[y]) return -1;
  if (B + 1 == C) return 1;

  assert(B + 1 <= C - 1);

  int z = arg_max(B + 1, C - 1);
  if (h[z] > h[y]) return -1;

  if (h[x] < h[y]) { // start from x
    if (h[x] > h[z]) return 1;
    int steps = 0;
    // 1. 跳到一个 <= h[z] 的地方上去
    int p = go(x, h[z]);
    steps += env_steps;
    assert(fa[p] != -1);

    if (fa[p] == y) return steps + 1; // 3. 如果第一步 = h[y] 就win

    int p2 = go_R(p, h[z]); // 跳到 h[z] 上
    assert(p2 == z);

    int res = steps + env_steps + 1;

    if (h[fa[p]] >
        h[y]) { // 2. 如果第一步直接超过 h[y] 则说明只有一个方向有救（go_R）
      return res;
    }

    // 现在你可以选择直接 go_R 走过去
    // 或者先 go 一步 < h[y] 那么下一步一定win
    //        因为第一步跳到的位置一定 > h[z]
    assert(h[fa[p]] < h[y]);
    p = fa[p];
    if (C <= p && p <= D) return min(res, steps + 1);

    return min(res, steps + 2);
  }
  // 只能 go_R
  int u = go_L(B, h[y]); // start from u
  assert(A <= u && u <= B);

  if (h[u] > h[z]) return 1;

  int p = go_R(u, h[z]); // 到 h[z]

  assert(p == z);
  return env_steps + 1;
}
