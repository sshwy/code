// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

using namespace RA;

const int N = 2e5 + 5;
int h[N];

int main() {
  srand(clock() + time(0));
  int n = rnd(3, 1000), q = rnd(1000, 2000);
  FOR(i, 1, n) h[i] = i;
  random_shuffle(h + 1, h + n + 1);
  printf("%d %d\n", n, q);
  FOR(i, 1, n) printf("%d%c", h[i], " \n"[i == n]);
  FOR(i, 1, q) {
    int t[] = {rnd(n), rnd(n), rnd(n), rnd(n)};
    sort(t, t + 4);
    while (t[1] == t[2]) {
      FOR(i, 0, 3) t[i] = rnd(n);
      sort(t, t + 4);
    }
    printf("%d %d %d %d\n", t[0], t[1], t[2], t[3]);
  }
  return 0;
}
