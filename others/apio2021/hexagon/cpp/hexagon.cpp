#include "hexagon.h"
#include <bits/stdc++.h>
#define FOR(a, b, c) for (int a = (int)(b); a <= (int)(c); a++)
#define ROF(a, b, c) for (int a = (int)(b); a >= (int)(c); a--)
#define x first
#define y second
using namespace std;

typedef pair<int, int> pt;
const int P = 1e9 + 7, I6 = (P + 1) / 6;

int f2(int n) { return 1ll * n * (n + 1) % P * (n * 2 + 1) % P * I6 % P; }

const int L = 2e3 + 5;

set<pt> inner, border;
set<pt> visited;
queue<pt> q;
int _dist[L + L][L + L];
int &dist(pt u) {
  visited.insert(u);
  return _dist[u.x + L][u.y + L];
}

int draw_territory(int n, int A, int B, vector<int> D, vector<int> L) {
  if (n == 3) {
    int d = L[0];
    int s = (1ll * d * (d + 1) / 2 % P + d) % P * A % P;
    s = (s + 1ll * d * (d + 1) / 2 % P * B % P) % P;
    s = (s + 1ll * f2(d) * B) % P;
    s = (s + A) % P;
    return s;
  }

  int ang = 0;
  FOR(i, 0, n - 1) {
    int j = (i + 1) % n;
    int di = D[i] % 6;
    int dj = D[j] % 6;
    if ((di + 1) % 6 == dj) ang += 1;
    if ((di + 2) % 6 == dj) ang += 2;
    if ((di - 1 + 6) % 6 == dj) ang -= 1;
    if ((di - 2 + 6) % 6 == dj) ang -= 2;
  }

  assert(abs(ang) == 6);
  if (ang == -6) {
    reverse(L.begin(), L.end());
    reverse(D.begin(), D.end());
    for (int &x : D) {
      x += 3;
      if (x > 6) x -= 6;
    }
  }

  auto trans = [](pt &u, int dir) {
    assert(dir >= 1 && dir <= 6);
    if (dir == 1) u.x++;
    if (dir == 2) u.x++, u.y++;
    if (dir == 3) u.y++;
    if (dir == 4) u.x--;
    if (dir == 5) u.x--, u.y--;
    if (dir == 6) u.y--;
  };

  auto distance = [](pt p1, pt p2) {
    int dx = p2.x - p1.x, dy = p2.y - p1.y;
    if (dx < 0 && dy > 0 || dx > 0 && dy < 0) return abs(dx) + abs(dy);
    return max(abs(dx), abs(dy));
  };

  pt u(0, 0);
  border.insert(u);

  FOR(i, 0, n - 1) {
    FOR(j, 1, L[i]) {
      auto u2 = u;
      trans(u, D[i]);
      int d2 = (D[i] + 1);
      if (d2 > 6) d2 -= 6;
      trans(u2, d2);
      inner.insert(u2);
      border.insert(u);
    }
  }

  assert(u == pt(0, 0));

  memset(_dist, 0x3f, sizeof(_dist));

  dist(u) = 0;
  q.push(pt(0, 0));

  while (q.size()) {
    pt u = q.front();
    q.pop();
    if (border.count(u)) {
      FOR(i, 1, 6) {
        pt v = u;
        trans(v, i);
        if (border.count(v) || inner.count(v)) {
          if (dist(v) > dist(u) + 1) {
            dist(v) = dist(u) + 1;
            q.push(v);
          }
        }
      }
    } else {
      FOR(i, 1, 6) {
        pt v = u;
        trans(v, i);
        if (dist(v) > dist(u) + 1) {
          dist(v) = dist(u) + 1;
          q.push(v);
        }
      }
    }
  }

  int ans = 0;
  for (auto u : visited) { ans = (ans + 1ll * dist(u) * B % P + A) % P; }

  return ans;
}
