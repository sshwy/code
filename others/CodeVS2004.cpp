#include <bits/stdc++.h>
int n, cnt, tot;
struct data {
  int x, l, r, v;
}; // row,l,r,up or down
data rtg[202];
bool cmp(data d1, data d2) { return d1.x < d2.x; }
int main() {
  scanf("%d", &n);
  for (int i = 1, a, b, c, d; i <= n; i++) {
    scanf("%d%d%d%d", &a, &b, &c, &d);
    rtg[++cnt] = (data){a, b, d, 1};  // up
    rtg[++cnt] = (data){c, b, d, -1}; // down
  }
  sort(rtg + 1, rtg + cnt + 1, cmp);
  for (int i = 1; i <= n; i++) {
    lst_upd(rtg[i].l, rtg[i].r, rtg[i].v, 1, 100000, 1);
    tot += lst_sum(1, 100000, 1, 100000, 1) * (rtg[i + 1].x - rtg[i].x);
  }
  printf("%d", tot);
  return 0;
}
