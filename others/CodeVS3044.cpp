#include <bits/stdc++.h>
using namespace std;
int n, cnt, tot;
struct data {
  int x, l, r, v;
}; // row,l,r,up or down
data rtg[202];
bool cmp(data d1, data d2) { return d1.x < d2.x; }

const int SZ = 1 << 18;
int s[SZ], tg[SZ];

void push_down(int l, int r, int rt) {
  int mid = (l + r) >> 1;
  s[rt << 1] += tg[rt] * (mid - l + 1), tg[rt << 1] += tg[rt];
  s[rt << 1 | 1] += tg[rt] * (r - mid), tg[rt << 1 | 1] += tg[rt];
  tg[rt] = 0;
}
void push_up(int rt) { s[rt] = s[rt << 1] + s[rt << 1 | 1]; }

int lst_add(int L, int R, int v, int l, int r, int rt) {
  if (R < l || r < L) return 0;
  if (L <= l && r <= R) return tg[rt] += v, s[rt] += v * (r - l + 1);
  push_down(l, r, rt);
  int mid = (l + r) >> 1;
  lst_add(L, R, v, l, mid, rt << 1), lst_add(L, R, v, mid + 1, r, rt << 1 | 1);
  push_up(rt);
}
int lst_sum(int L, int R, int l, int r, int rt) {
  if (R < l || r < L) return 0;
  if (L <= l && r <= R) return s[rt];
  push_down(l, r, rt);
  int mid = (l + r) >> 1;
  return lst_sum(L, R, l, mid, rt << 1) + lst_sum(L, R, mid + 1, r, rt << 1 | 1);
}

int main() {
  scanf("%d", &n);
  for (int i = 1, a, b, c, d; i <= n; i++) {
    scanf("%d%d%d%d", &a, &b, &c, &d);
    rtg[++cnt] = (data){a, b, d, 1};  // up
    rtg[++cnt] = (data){c, b, d, -1}; // down
  }
  sort(rtg + 1, rtg + cnt + 1, cmp);
  for (int i = 1; i <= n; i++) {
    lst_add(rtg[i].l, rtg[i].r, rtg[i].v, 1, 100000, 1);
    tot += lst_sum(1, 100000, 1, 100000, 1) * (rtg[i + 1].x - rtg[i].x);
  }
  printf("%d", tot);
  return 0;
}
