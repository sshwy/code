// by Sshwy
//#define DEBUGGER
#include <bits/stdc++.h> /*{{{*/
using namespace std;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

#ifdef DEBUGGER

#define log_prefix()                                                \
  {                                                                 \
    fprintf(stderr, "\033[37mLine %-3d [%dms]:\033[0m  ", __LINE__, \
        (int)clock() / 1000);                                       \
  }
#define ilog(...) \
  { fprintf(stderr, __VA_ARGS__); }
#define llog(...)      \
  {                    \
    log_prefix();      \
    ilog(__VA_ARGS__); \
  }
#define log(...)           \
  {                        \
    log_prefix();          \
    ilog(__VA_ARGS__);     \
    fprintf(stderr, "\n"); \
  }
#define red(...)       \
  {                    \
    log_prefix();      \
    ilog("\033[31m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define green(...)     \
  {                    \
    log_prefix();      \
    ilog("\033[32m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define blue(...)      \
  {                    \
    log_prefix();      \
    ilog("\033[34m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }

#else

#define log_prefix() ;
#define ilog(...) ;
#define llog(...) ;
#define log(...) ;
#define red(...) ;
#define green(...) ;
#define blue(...) ;

#endif

namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 1e5 + 5;
typedef long long LL;

int n, q;

#define Md ((l + r) >> 1)
namespace seg {
  const int SZ = (N * 2) << 2;
  LL s[SZ], tag[SZ];
  void Na(int u, int l, int r, LL v) { s[u] += (r - l + 1ll) * v, tag[u] += v; }
  void Pd(int u, int l, int r) {
    if (tag[u])
      Na(u << 1, l, Md, tag[u]), Na(u << 1 | 1, Md + 1, r, tag[u]), tag[u] = 0;
  }
  void A(int L, int R, LL v, int u = 1, int l = 0, int r = n) {
    (L > R || R < l || r < L)
        ? 0
        : ((L <= l && r <= R) ? (Na(u, l, r, v), 0)
                              : (Pd(u, l, r), A(L, R, v, u << 1, l, Md),
                                    A(L, R, v, u << 1 | 1, Md + 1, r),
                                    s[u] = s[u << 1] + s[u << 1 | 1]));
  }
  LL Q(int L, int R, int u = 1, int l = 0, int r = n) {
    return (R < l || r < L)
               ? 0
               : ((L <= l && r <= R)
                         ? s[u]
                         : (Pd(u, l, r), Q(L, R, u << 1, l, Md) +
                                             Q(L, R, u << 1 | 1, Md + 1, r)));
  }
} // namespace seg

namespace lct {
  const int SZ = N * 2;
  int tot, ch[SZ][2], fa[SZ];
  struct data {
    int l1, l2, pos, L1, L2;
  } D[SZ];
  int Nn(int u, int l1, int l2, int pos) { return D[u] = {l1, l2, pos, l1, l2}, u; }
  bool isroot(int u) { return ch[fa[u]][0] != u && ch[fa[u]][1] != u; }
  int get(int u) { return ch[fa[u]][1] == u; }
  void Up(int u) {
    int x = ch[u][0], y = ch[u][1];
    D[u].L1 = D[u].l1, D[u].L2 = D[u].l2;
    if (y) D[u].L1 = D[y].L1;
    if (x) D[u].L2 = D[x].L2;
  }
  void Ass(int u, int Pos) { D[u].pos = Pos; }
  void Pd(int u) {
    if (ch[u][0]) Ass(ch[u][0], D[u].pos);
    if (ch[u][1]) Ass(ch[u][1], D[u].pos);
  }
  void rotate(int u) {
    int p = fa[u], pp = fa[p], k;
    Pd(p), Pd(u), k = get(u), (!isroot(p) ? ch[pp][get(p)] = u : 0),
                  fa[ch[u][!k]] = p, ch[p][k] = ch[u][!k], ch[u][!k] = p, fa[p] = u,
                  fa[u] = pp, Up(p), Up(u);
  }
  void splay(int u) {
    for (int p; p = fa[u], !isroot(u); rotate(u))
      if (!isroot(p)) rotate(get(u) == get(p) ? p : u);
  }
  void extend(int u, int p, int l1, int l2, int pos) {
    u = Nn(u, l1, l2, pos);
    fa[u] = p;
    int pre, v;
    for (pre = u, v = fa[u]; v; pre = v, v = fa[v]) {
      splay(v);
      ch[v][1] = 0, Up(v);
      seg::A(D[v].pos - D[v].L1 + 1, D[v].pos - D[v].L2, -1);
      Ass(v, pos), Pd(v);
      ch[v][1] = pre, Up(v);
    }
    seg::A(D[pre].pos - D[pre].L1 + 1, D[pre].pos - D[pre].L2, 1);
    splay(u);
  }
  void insert(int p, int u, int v, int l1, int l2) { // pos==u.pos
    if (p) splay(p), ch[p][1] = 0, Up(p);
    splay(u);
    assert(ch[u][0] == 0);
    D[u].l2 = l1, Up(u);
    v = Nn(v, l1, l2, D[u].pos);
    fa[u] = v, fa[v] = p;
  }
} // namespace lct

namespace sam {
  const int SZ = N * 2;
  int tot = 1, last = 1, fail[SZ], tr[SZ][26], len[SZ];
  void insert(char c) {
    c -= 'a';
    int u = ++tot, p = last;
    len[u] = len[last] + 1;
    while (p && tr[p][c] == 0) tr[p][c] = u, p = fail[p];
    if (!p)
      fail[u] = 1;
    else {
      int q = tr[p][c];
      if (len[q] == len[p] + 1)
        fail[u] = q;
      else {
        int cq = ++tot;
        len[cq] = len[p] + 1, fail[cq] = fail[q];
        lct::insert(fail[q], q, cq, len[cq], len[fail[cq]]); // 中间插入一个点
        fail[u] = fail[q] = cq, memcpy(tr[cq], tr[q], sizeof(tr[q]));
        while (p && tr[p][c] == q) tr[p][c] = cq, p = fail[p];
      }
    }
    last = u;
    lct::extend(last, fail[last], len[last], len[fail[last]],
        len[last] - 1); // 末端插入一个点 (access)
  }
} // namespace sam

vector<pair<int, int>> qry[N]; // qry[i]:以i为右端点的询问
vector<LL> countSubstrings(string s, vector<vector<int>> queries) {
  vector<LL> res(q, 0);

  for (long unsigned int i = 0; i < queries.size(); i++) {
    qry[queries[i][1]].push_back({queries[i][0], i});
  }

  for (long unsigned int i = 0; i < s.size(); i++) {
    char c = s[i];
    sam::insert(c);
    for (pair<int, int> p : qry[i]) { res[p.se] = seg::Q(p.fi, i); }
  }

  return res;
}

// q次询问，求s[l,r]的本质不同子串的个数
// 考虑离线。
// 每次加入s[i]的时候，我们处理r=i的所有询问。
// SAM 上一个状态u代表了一个左端点区间。
// 维护每个状态出现的最靠后的位置。
// 我们的问题变成了，每次询问左端点大于等于L的不同子串数。
// 在末位加入一个字符后，则fail书上从last到根结点的信息都要改。
// 容易发现，fail树上一条树链的左端点区间是可以恰好拼在一起的
// 因此更新一条树链信息的复杂度是log的（线段树）
// 于是像lct的access一样更新即可

vector<string> split_string(string); /*{{{*/
int main() {
  ofstream fout(getenv("OUTPUT_PATH"));

  string nq_temp;
  getline(cin, nq_temp);

  vector<string> nq = split_string(nq_temp);

  n = stoi(nq[0]);
  q = stoi(nq[1]);

  string s;
  getline(cin, s);

  vector<vector<int>> queries(q);
  for (int queries_row_itr = 0; queries_row_itr < q; queries_row_itr++) {
    queries[queries_row_itr].resize(2);

    for (int queries_column_itr = 0; queries_column_itr < 2; queries_column_itr++) {
      cin >> queries[queries_row_itr][queries_column_itr];
    }

    cin.ignore(numeric_limits<streamsize>::max(), '\n');
  }

  vector<LL> result = countSubstrings(s, queries);

  for (long unsigned int result_itr = 0; result_itr < result.size(); result_itr++) {
    fout << result[result_itr];
    cout << result[result_itr];

    if (result_itr != result.size() - 1) {
      fout << "\n";
      cout << "\n";
    }
  }

  fout << "\n";
  cout << "\n";

  fout.close();

  return 0;
}

vector<string> split_string(string input_string) {
  string::iterator new_end = unique(input_string.begin(), input_string.end(),
      [](const char &x, const char &y) { return x == y and x == ' '; });

  input_string.erase(new_end, input_string.end());

  while (input_string[input_string.length() - 1] == ' ') { input_string.pop_back(); }

  vector<string> splits;
  char delimiter = ' ';

  size_t i = 0;
  size_t pos = input_string.find(delimiter);

  while (pos != string::npos) {
    splits.push_back(input_string.substr(i, pos - i));

    i = pos + 1;
    pos = input_string.find(delimiter, i);
  }

  splits.push_back(input_string.substr(i, min(pos, input_string.length()) - i + 1));

  return splits;
}
/*}}}*/
