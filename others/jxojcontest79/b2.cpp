// by Sshwy
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

int n, m;
vector<long long> v;

int main() {
  scanf("%d%d", &n, &m);
  FOR(i, 1, n) v.pb(0);
  FOR(i, 1, m) {
    int op, x, y;
    scanf("%d%d", &op, &x);
    if (op == 1) {
      FOR(i, 1, x) v.insert(v.begin(), 0);
    } else if (op == 2) {
      FOR(i, 1, x) v.pb(0);
    } else {
      scanf("%d", &y);
      for (int i = 0; i < v.size(); i++) v[i] += x + y * 1ll * i;
    }
    auto p = min_element(v.begin(), v.end());
    printf("%lu %lld\n", p - v.begin() + 1, *p);
  }
  return 0;
}
