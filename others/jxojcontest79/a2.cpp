// by Sshwy
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA
const int N = 2e5 + 5, P = 998244353;
int n, p[N];
vector<pair<int, int>> e;
bool check(pair<int, int> x, pair<int, int> y) {
  int a = x.first, b = x.second, c = y.first, d = y.second;
  a = p[a], b = p[b], c = p[c], d = p[d];
  if (a > b) swap(a, b);
  if (c > d) swap(c, d);
  // printf("%d %d %d %d\n",a,b,c,d);
  if (a == c || a == d || b == c || b == d) return 0;
  if (b < c || d < a) return 0;
  if (a < c && b < d || a > c && b > d) return 1;
  return 0;
}
int main() {
  scanf("%d", &n);
  FOR(i, 1, n - 1) {
    int u, v;
    scanf("%d%d", &u, &v);
    e.pb({u, v});
  }
  FOR(i, 1, n) p[i] = i;
  int ans = 0;
  do {
    bool flag = 1;
    for (auto i : e) {
      for (auto j : e) {
        if (i == j) continue;
        if (check(i, j)) {
          flag = 0;
          break;
        }
      }
      if (flag == 0) break;
    }
    if (flag) ++ans;
  } while (next_permutation(p + 1, p + n + 1));
  printf("%d\n", ans);
  return 0;
}
