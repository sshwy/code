// by Sshwy
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

using namespace RA;

int main() {
  srand(clock() + time(0));
  int LIM = 5;
  int LIM2 = 5;
  int n = rnd(1, LIM), m = rnd(1, 10000);
  printf("%d %d\n", n, m);
  FOR(i, 1, m) {
    int op = rnd(100) < 50 ? rnd(1, 2) : 3;
    if (op == 1 || op == 2)
      printf("%d %d\n", op, rnd(1, LIM));
    else
      printf("%d %d %d\n", op, rnd(LIM2), rnd(LIM2));
  }
  return 0;
}
