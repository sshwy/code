// by Sshwy
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA
const int N = 2e5 + 5, P = 998244353;
int n;
vector<int> g[N];
int sz[N], sc[N], fac[N];
int f[N];
void dfs(int u, int p) {
  f[u] = 1;
  for (int v : g[u])
    if (v != p) dfs(v, u), sc[u]++, f[u] = f[u] * 1ll * f[v] % P;
  if (p)
    f[u] = f[u] * 1ll * fac[sc[u] + 1] % P;
  else
    f[u] = f[u] * 1ll * fac[sc[u]] % P;
}
int main() {
  scanf("%d", &n);
  FOR(i, 1, n - 1) {
    int u, v;
    scanf("%d%d", &u, &v);
    g[u].pb(v);
    g[v].pb(u);
  }
  fac[0] = 1;
  FOR(i, 1, n) fac[i] = fac[i - 1] * 1ll * i % P;
  dfs(1, 0);
  int ans = f[1] * 1ll * n % P;
  printf("%d\n", ans);
  return 0;
}
