// by Sshwy
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

int n, m;

typedef pair<int, long long> point;
typedef __int128 i128;

vector<point> vl, vr;
long long C, D, E;
long long val(point x) { return x.second + C + D * x.first; }
long long ival(int x, long long y) { return y - C - D * x; }
bool check(point x, point y, point z) {
  z.first -= y.first, z.second -= y.second;
  y.first -= x.first, y.second -= x.second;
  return (i128)y.first * z.second - (i128)z.first * y.second <= 0;
}
void push_vr(point p) {
  while (vr.size() > 0 && vr.back() == p ||
         vr.size() > 1 && check(*(vr.end() - 2), vr.back(), p))
    vr.pop_back();
  vr.pb(p);
}
void push_vl(point p) {
  while (vl.size() > 0 && vl.back() == p ||
         vl.size() > 1 && check(p, vl.back(), *(vl.end() - 2)))
    vl.pop_back();
  vl.pb(p);
}
point qry_vl(const vector<point> &v) {
  assert(v.size());
  int l = 0, r = v.size() - 1;
  while (l < r) {
    int mid = (l + r + 1) >> 1;
    if (val(v[mid - 1]) >= val(v[mid]))
      l = mid;
    else
      r = mid - 1;
  }
  return v[l];
}
point qry_vr(const vector<point> &v) {
  assert(v.size());
  int l = 0, r = v.size() - 1;
  while (l < r) {
    int mid = (l + r + 1) >> 1;
    if (val(v[mid - 1]) > val(v[mid]))
      l = mid;
    else
      r = mid - 1;
  }
  return v[l];
}
point merge(point x, point y) {
  // printf("merge (%d,%lld) (%d,%lld)\n",x.first,x.second,y.first,y.second);
  if (val(x) != val(y)) return val(x) < val(y) ? x : y;
  return min(x, y);
}
point qry() {
  assert(vl.size() || vr.size());
  if (vl.empty()) return qry_vr(vr);
  if (vr.empty()) return qry_vl(vl);
  return merge(qry_vl(vl), qry_vr(vr));
}

int main() {
  scanf("%d%d", &n, &m);
  vr.pb({0, 0});
  int L = 0, R = n;
  FOR(i, 1, m) {
    int op, x, y;
    scanf("%d%d", &op, &x);
    if (op == 1) {
      L -= x;
      push_vl({L, ival(L, 0)});
      E += x;
    } else if (op == 2) {
      push_vr({R, ival(R, 0)});
      R += x;
    } else {
      scanf("%d", &y);
      C += x + y * E;
      D += y;
    }
    // if(vl.size()){
    //    printf("vl: ");
    //    for(auto p:vl)printf("(%d,%lld):(%lld,%lld) ",p.first,p.second,
    //            p.first+E+1,val(p));
    //    puts("");
    //}
    // if(vr.size()){
    //    printf("vr: ");
    //    for(auto p:vr)printf("(%d,%lld):(%lld,%lld) ",p.first,p.second,
    //            p.first+E+1,val(p));
    //    puts("");
    //}
    // printf("C %lld D %lld E %lld\n",C,D,E);
    auto p = qry();
    printf("%lld %lld\n", p.first + E + 1, val(p));
  }
  return 0;
}
