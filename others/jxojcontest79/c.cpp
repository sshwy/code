// by Sshwy
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA
const long long INF = 4e18;
const int inf = 1e9, N = 2e5 + 10, M = 2e6 + 100;

struct qxx {
  int nex, t, v;
  long long c;
} e[M];
int h[N], le = 1;
void add_path(int f, int t, int v, long long c) {
  e[++le] = {h[f], t, v, c}, h[f] = le;
}
void add_flow(int f, int t, int v, long long c) {
  add_path(f, t, v, c), add_path(t, f, 0, -c);
}

int n;
int s, t;
long long maxcost;
int maxflow;

queue<int> q;
long long d[N];
bool vis[N];
int pre[N], incf[N];
bool spfa() {
  memset(d, -0x3f, sizeof(d[0]) * (n * 2 + 2));
  // memset(vis,0,sizeof(vis[0])*(n*2+2));

  d[s] = 0;
  incf[s] = inf;
  q.push(s);
  while (!q.empty()) {
    int u = q.front();
    q.pop();
    vis[u] = 0;
    for (int i = h[u]; i; i = e[i].nex) {
      int v = e[i].t, w = e[i].v, c = e[i].c;
      if (!w || d[v] >= d[u] + c) continue;
      d[v] = d[u] + c;
      pre[v] = i;
      incf[v] = min(w, incf[u]);
      if (!vis[v]) q.push(v), vis[v] = 1;
    }
  }
  return d[t] > -INF;
}
void update() {
  maxcost += d[t] * 1ll * incf[t];
  maxflow += incf[t];
  for (int u = t; u != s; u = e[pre[u] ^ 1].t) {
    e[pre[u]].v -= incf[t];
    e[pre[u] ^ 1].v += incf[t];
  }
}

struct atom {
  int x, y, c;
} a[N], b[N];

long long dist(atom x, atom y) { return abs(x.x - y.x) + abs(x.y - y.y); }
int main() {
  scanf("%d", &n);
  FOR(i, 1, n) {
    int x, y, c;
    scanf("%d%d%d", &x, &y, &c);
    a[i] = {x, y, c};
  }
  FOR(i, 1, n) {
    int x, y, c;
    scanf("%d%d%d", &x, &y, &c);
    b[i] = {x, y, c};
  }
  s = 0, t = n * 2 + 1;
  FOR(i, 1, n) {
    add_flow(s, i, a[i].c, 0);
    add_flow(i + n, t, b[i].c, 0);
  }
  FOR(i, 1, n) FOR(j, 1, n) { add_flow(i, j + n, inf, dist(a[i], b[j])); }
  while (spfa()) update();
  printf("%lld\n", maxcost);
  return 0;
}
