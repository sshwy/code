// by Sshwy
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

int main() {
  system("g++ b.cpp -o .usr");
  system("g++ b2.cpp -o .std");
  system("g++ b_gen.cpp -o .gen");
  FOR(t, 1, 100000) {
    system("./.gen > .fin");
    if (system("./.usr < .fin > .fout")) break;
    system("./.std < .fin > .fstd");
    if (system("diff .fout .fstd")) break;
    printf("AC #%d\n", t);
  }
  puts("Finished.");
  return 0;
}
