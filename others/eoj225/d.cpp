// by Sshwy
//#define DEBUGGER
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

#ifdef DEBUGGER

#define log_prefix()                                                \
  {                                                                 \
    fprintf(stderr, "\033[37mLine %-3d [%dms]:\033[0m  ", __LINE__, \
        (int)clock() / 1000);                                       \
  }
#define ilog(...) \
  { fprintf(stderr, __VA_ARGS__); }
#define llog(...)      \
  {                    \
    log_prefix();      \
    ilog(__VA_ARGS__); \
  }
#define log(...)           \
  {                        \
    log_prefix();          \
    ilog(__VA_ARGS__);     \
    fprintf(stderr, "\n"); \
  }
#define red(...)       \
  {                    \
    log_prefix();      \
    ilog("\033[31m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define green(...)     \
  {                    \
    log_prefix();      \
    ilog("\033[32m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define blue(...)      \
  {                    \
    log_prefix();      \
    ilog("\033[34m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }

#else

#define log_prefix() ;
#define ilog(...) ;
#define llog(...) ;
#define log(...) ;
#define red(...) ;
#define green(...) ;
#define blue(...) ;

#endif

namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} /*}}}*/
/******************heading******************/
const int N = 1e6 + 5;
int t;
int n;

struct qxx {
  int nex, t, v;
} e[N * 2];
int h[N], le = 1;
void add_path(int f, int t, int v) { e[++le] = (qxx){h[f], t, v}, h[f] = le; }

vector<pair<int, int>> ans;

pair<int, int> dfs(int u, int p) {
  int L = u, R = u, unex = -1, upre = -1;
  pair<int, int> p1(-1, -1), p0(-1, -1);
  for (int i = h[u]; i; i = e[i].nex) {
    int v = e[i].t, w = e[i].v;
    if (v != p) {
      if (w) { // u -> v
        auto x = dfs(v, u);
        if (R == u) R = x.se, unex = x.fi;
        if (p1.fi != -1) { ans.pb({x.se, p1.fi}); }
        p1 = x;
      } else { // v -> u
        auto x = dfs(v, u);
        if (L == u) L = x.fi, upre = x.se;
        if (p0.fi != -1) { ans.pb({p0.se, x.fi}); }
        p0 = x;
      }
      // printf("after u=%d,v=%d,w=%d: L=%d,R=%d,
      // upre=%d,unex=%d\n",u,v,w,L,R,upre,unex);
    }
  }
  for (int i = h[u]; i; i = e[i].nex) {
    int v = e[i].t, w = e[i].v;
    if (p1.fi != -1)ans.pb({
    }
    return {L,R};
  }
  vector<int> g[N];
  map<int, int> f;
  int calc(int u) {
    if (f.count(u)) return f[u];
    int res = 1;
    for (int v : g[u]) res = max(res, calc(v) + 1);
    // printf("calc %d %d %d\n",u,res,n);
    return f[u] = res;
  }
  void check() {
    for (auto x : ans) g[x.fi].pb(x.se);
    FOR(i, 1, n) g[0].pb(i);
    int t = calc(0);
    // printf("t %d %d\n",t,n+1);
    assert(t == n + 1);
  }
  int ind[N], oud[N];
  void go(int id) {
    scanf("%d", &n);
    FOR(i, 0, n) g[i].clear();
    f.clear();
    fill(h, h + n + 1, 0), le = 1;
    fill(ind, ind + n + 1, 0);
    fill(oud, oud + n + 1, 0);
    ans.clear();

    FOR(i, 1, n - 1) {
      int u, v;
      scanf("%d%d", &u, &v);
      add_path(u, v, 1); // u -> v
      add_path(v, u, 0);
      ind[v]++;
      oud[u]++;
      g[u].pb(v);
    }
    int al = 0;
    FOR(i, 1, n) al += max(ind[i] - 1, 0) + max(oud[i] - 1, 0);
    dfs(1, 0);
    assert(al == int(ans.size()));
    printf("Case %d: %d\n", id, int(ans.size()));
    for (auto x : ans) printf("%d %d\n", x.fi, x.se);
    check();
  }
  int main() {
    scanf("%d", &t);
    FOR(i, 1, t) go(i);
    return 0;
  }
