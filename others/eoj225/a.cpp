// by Sshwy
//#define DEBUGGER
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

#ifdef DEBUGGER

#define log_prefix()                                                  \
  {                                                                   \
    fprintf(stderr, "\033[37mLine %-3d [%lldms]:\033[0m  ", __LINE__, \
        (int)clock() / 1000);                                         \
  }
#define ilog(...) \
  { fprintf(stderr, __VA_ARGS__); }
#define llog(...)      \
  {                    \
    log_prefix();      \
    ilog(__VA_ARGS__); \
  }
#define log(...)           \
  {                        \
    log_prefix();          \
    ilog(__VA_ARGS__);     \
    fprintf(stderr, "\n"); \
  }
#define red(...)       \
  {                    \
    log_prefix();      \
    ilog("\033[31m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define green(...)     \
  {                    \
    log_prefix();      \
    ilog("\033[32m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define blue(...)      \
  {                    \
    log_prefix();      \
    ilog("\033[34m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }

#else

#define log_prefix() ;
#define ilog(...) ;
#define llog(...) ;
#define log(...) ;
#define red(...) ;
#define green(...) ;
#define blue(...) ;

#endif

namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
#define int long long
const int N = 1e6 + 5;
int x[N], y[N];
void go() {
  int k, n, m;
  scanf("%lld%lld%lld", &k, &n, &m);
  FOR(i, 1, k) scanf("%lld%lld", &x[i], &y[i]);
  sort(x + 1, x + k + 1);
  sort(y + 1, y + k + 1);
  int X = max(x[1], n - x[k]), Y = max(y[1], m - y[k]);
  FOR(i, 1, k - 1) X = max(X, (x[i + 1] - x[i]) / 2);
  FOR(i, 1, k - 1) Y = max(Y, (y[i + 1] - y[i]) / 2);
  printf("%lld\n", X + Y);
}
signed main() {
  int t;
  scanf("%lld", &t);
  FOR(i, 1, t) go();
  return 0;
}
