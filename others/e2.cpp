// by Sshwy
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

const int N = 2e5 + 5, M = 5e4 + 5, P = 1e9 + 7;
int n, m, k;
int a[N], b[M], c[40], z[N];

int wash(int x) {
  ROF(i, 29, 0) if (x >> i & 1) {
    if (c[i]) x ^= c[i];
  }
  return x;
}
void insert(int x) {
  ROF(i, 29, 0) if (x >> i & 1) {
    if (c[i] == 0) {
      c[i] = x;
      break;
    } else {
      x ^= c[i];
    }
  }
}
int nex[M];
void go() {
  scanf("%d%d%d", &n, &m, &k);
  FOR(i, 1, n) scanf("%d", &a[i]);
  FOR(i, 1, m) scanf("%d", &b[i]);
  memset(c, 0, sizeof(c));
  FOR(i, 1, k) {
    int x;
    scanf("%d", &x);
    insert(x);
  }
  FOR(i, 1, n) a[i] = wash(a[i]);
  FOR(i, 1, m) b[i] = wash(b[i]);
  z[0] = 1;
  FOR(i, 1, n) z[i] = z[i - 1] * 2ll % P;
  int ans = 0;
  FOR(i, 1, n - m + 1) {
    int flag = 1;
    FOR(j, 0, m - 1) { flag &= a[i + j] == b[j + 1]; }
    if (flag) { ans = (ans + z[i - 1]) % P; }
  }
  printf("%d\n", ans);
}
int main() {
  int t;
  scanf("%d", &t);
  FOR(i, 1, t) go();
  return 0;
}
