#include <algorithm>
#include <cstdio>
using namespace std;
const int N = 1e5 + 5, SZ = N << 2, INF = 0x7fffffff;
int n, m;
int a[N];

int mx[SZ], hmx[SZ];                   // historical max
int tad[SZ], tam[SZ], tv[SZ], tvm[SZ]; // val val_max

void pushup(int u) {
  mx[u] = max(mx[u << 1], mx[u << 1 | 1]);
  hmx[u] = max(hmx[u << 1], hmx[u << 1 | 1]);
}
void pushtag(int u, int v) {
  //把u的标记应用到结点v
  //第一阶段。我们要求tadtam不能和tvtvm同时存在
  if (tad[u] || tam[u]) {
    tam[v] = max(tam[v], tam[u] + tad[v]);
    tad[v] += tad[u];
    hmx[v] = max(hmx[v], mx[v] + tam[u]);
    mx[v] += tad[u];
  }
  //第二阶段
  else if (tv[u] != -INF || tvm[u] != -INF) {
    tvm[v] = max(tvm[v], tvm[u]);
    tv[v] = tv[u];
    hmx[v] = max(hmx[v], tvm[u]); // tvm[u]写成hmx[u]
    mx[v] = tv[u];
    tad[v] = tam[v] = 0; //清空以前的标记
  }
}
void pushdown(int u) {
  pushtag(u, u << 1), pushtag(u, u << 1 | 1);
  tad[u] = tam[u] = 0, tv[u] = tvm[u] = -INF;
}
void build(int u = 1, int l = 1, int r = n) {
  tv[u] = tvm[u] = -INF;
  if (l == r) {
    mx[u] = hmx[u] = a[l];
    return;
  }
  int mid = (l + r) >> 1;
  build(u << 1, l, mid), build(u << 1 | 1, mid + 1, r);
  pushup(u);
}
void add(int L, int R, int v, int u = 1, int l = 1, int r = n) {
  if (R < l || r < L) return;
  if (L <= l && r <= R) {
    if (tv[u] != -INF || tvm[u] != -INF) {
      tad[u] = tam[u] = 0; //以前的标记都不需要了
      v = mx[u] + v;       //相当于作一次mx[u]+v的覆盖
      tvm[u] = max(tvm[u], v);
      tv[u] = v;
      hmx[u] = max(hmx[u], v);
      mx[u] = v;
      return;
    }
    tam[u] = max(tam[u], tad[u] + v);
    tad[u] += v;
    hmx[u] = max(hmx[u], mx[u] + v);
    mx[u] += v;
    return;
  }
  int mid = (l + r) >> 1;
  pushdown(u);
  add(L, R, v, u << 1, l, mid), add(L, R, v, u << 1 | 1, mid + 1, r);
  pushup(u);
}
void cover(int L, int R, int v, int u = 1, int l = 1, int r = n) {
  if (R < l || r < L) return;
  if (L <= l && r <= R) {
    tad[u] = tam[u] = 0; //以前的标记都不需要了
    tvm[u] = max(tvm[u], v);
    tv[u] = v;
    hmx[u] = max(hmx[u], v);
    mx[u] = v;
    return;
  }
  int mid = (l + r) >> 1;
  pushdown(u);
  cover(L, R, v, u << 1, l, mid), cover(L, R, v, u << 1 | 1, mid + 1, r);
  pushup(u);
}
int qmax(int L, int R, int u = 1, int l = 1, int r = n) {
  if (r < L || R < l) return -INF;
  if (L <= l && r <= R) return mx[u];
  int mid = (l + r) >> 1;
  pushdown(u);
  return max(qmax(L, R, u << 1, l, mid), qmax(L, R, u << 1 | 1, mid + 1, r));
}
int qhmax(int L, int R, int u = 1, int l = 1, int r = n) {
  if (R < l || r < L) return -INF;
  if (L <= l && r <= R) return hmx[u];
  int mid = (l + r) >> 1;
  pushdown(u);
  return max(qhmax(L, R, u << 1, l, mid), qhmax(L, R, u << 1 | 1, mid + 1, r));
}

int main() {
  scanf("%d", &n);
  for (int i = 1; i <= n; i++) scanf("%d", &a[i]);
  build();
  scanf("%d", &m);
  for (int i = 1; i <= m; i++) {
    char op[5];
    int x, y, z;
    scanf("%s%d%d", op, &x, &y);
    if (op[0] == 'Q') {
      printf("%d\n", qmax(x, y));
    } else if (op[0] == 'A') {
      printf("%d\n", qhmax(x, y));
    } else if (op[0] == 'P') {
      scanf("%d", &z);
      add(x, y, z);
    } else {
      scanf("%d", &z);
      cover(x, y, z);
    }
  }
  return 0;
}
/*
 * tad:0 表示add标记
 * tam:0 表示add的历史最大值
 * tv:-INF 表示当前的覆盖标记
 * tvm-INF 表示覆盖标记的历史最大值
 */
