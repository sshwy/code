// by Sshwy
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA
const int N = 2e5 + 5;
int b[N];
long long perform(int x, int y) {
  if (b[x] < y) return 0;
  assert(b[x] >= y);
  if (*min_element(b + 1, b + x + 1) >= y) return 0;
  int pos = -1;
  FOR(i, 1, x) if (b[i] < y) pos = i;
  assert(pos != -1);
  long long res = 0;
  FOR(i, pos + 1, x) res += b[i] - (y - 1);
  b[pos] += b[pos + 1] - (y - 1);
  FOR(i, pos + 1, x - 1) b[i] = b[i + 1];
  b[x] = y - 1;
  return res;
}
void go() {
  int n, q;
  scanf("%d%d", &n, &q);
  FOR(i, 1, n) scanf("%d", &b[i]);
  FOR(i, 1, q) {
    int op, x, y;
    scanf("%d%d", &op, &x);
    if (op == 1) {
      scanf("%d", &y);
      printf("%lld\n", perform(x, y));
    } else {
      printf("%d\n", b[x]);
    }
  }
  FOR(i, 1, n) printf("%d%c", b[i], " \n"[i == n]);
}
int main() {
  int t;
  scanf("%d", &t);
  FOR(i, 1, t) go();
  return 0;
}
