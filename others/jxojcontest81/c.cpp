// by Sshwy
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA
const int N = 1e5 + 5, SZ = N * 2;
int n;
int a[N];

void add(int *f, int pos, int v) {
  for (int i = pos; i < SZ; i += i & -i) f[i] += v;
}
int pre(int *f, int pos) {
  int res = 0;
  for (int i = pos; i > 0; i -= i & -i) res += f[i];
  return res;
}
int c[SZ];

long long calc(int val) {
  long long res = 0;
  int base = 0;
  fill(c, c + SZ, 0);
  FOR(i, 1, n) {
    if (a[i] >= val) {
      add(c, N - base, 1);
      // insert -base
      ++base;

    } else {
      add(c, N - base, 1);
      // insert -base
      --base;
    }
    // >= -base
    res += pre(c, SZ - 1) - pre(c, N - base - 1);
  }
  return res;
}
int main() {
  scanf("%d", &n);
  FOR(i, 1, n) scanf("%d", &a[i]);
  int l = 1, r = 1e9 + 1;
  long long m = n * (n + 1ll) / 2;
  while (l < r) {
    int mid = (l + r + 1) >> 1;
    long long t = calc(mid);
    if (t >= (m + 1) / 2)
      l = mid;
    else
      r = mid - 1;
  }
  printf("%d\n", l);
  FOR(i, 1, 9) calc(i);
  return 0;
}
