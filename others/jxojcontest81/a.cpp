// by Sshwy
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

long long n, k;

long long g(long long x) { return x * (x - 1) / 2; }
long long f(long long x) { return g(x) + x * (n - x); }
int main() {
  scanf("%lld%lld", &n, &k);
  long long l = 0, r = n;
  if (f(n) < k) return puts("-1"), 0;
  while (l < r) {
    long long mid = (l + r) >> 1;
    if (f(mid) >= k)
      r = mid;
    else
      l = mid + 1;
  }
  long long L = l;
  l = 0, r = n;
  while (l < r) {
    long long mid = (l + r + 1) >> 1;
    if (g(mid) > k)
      r = mid - 1;
    else
      l = mid;
  }
  long long R = l;
  long long ans = min(L * (n - L), R * (n - R));
  printf("%lld\n", n * (n - 1) / 2 - ans);
  return 0;
}
