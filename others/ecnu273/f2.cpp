// by Sshwy
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA
const int N = 1e5 + 5, S = 1e6 + 5;
int n;
char s[N];
vector<string> v;
vector<int> a;

bool cmp(int x, int y) { // compare v[x],v[y]
  return v[x] < v[y];
}
bool check_prefix(int x, int y) { // v[x] v[y]
  if (v[x].size() > v[y].size()) return 0;
  for (int i = 0; i < v[x].size(); ++i)
    if (v[x][i] != v[y][i]) return 0;
  return 1;
}
int main() {
  scanf("%d", &n);
  FOR(i, 1, n) {
    scanf("%s", s);
    string t = s;
    if (t.size() > 4 && t[t.size() - 4] == '.' && t[t.size() - 3] == 'e' &&
        t[t.size() - 2] == 'o' && t[t.size() - 1] == 'j') {
      v.pb(t);
      a.pb(a.size());
    }
  }
  sort(a.begin(), a.end(), cmp);
  for (auto x : a) cout << v[x] << endl;
  int ans = a.size() ? 1 : 0;
  for (int i = 0; i + 1 < a.size(); ++i) {
    if (check_prefix(a[i], a[i + 1]) == 0 || v[a[i + 1]][v[a[i]].size()] != '/')
      ++ans;
  }
  printf("%d\n", ans);
  return 0;
}
