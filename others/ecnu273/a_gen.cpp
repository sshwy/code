// by Sshwy
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

using namespace RA;

int main() {
  srand(clock() + time(0));
  int n = rnd(1, 8), m = rnd(1, 8);
  vector<string> v;
  FOR(i, 'A', 'H') FOR(j, '1', '8') { v.pb(string(1, i) + string(1, j)); }
  random_shuffle(v.begin(), v.end());
  cout << n << endl;
  FOR(i, 0, n - 1) cout << v[i] << " \n"[i == n - 1];
  cout << m << endl;
  FOR(i, 1, m) cout << v[8 * 8 - i] << " \n"[i == m];
  return 0;
}
