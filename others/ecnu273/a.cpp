// by Sshwy
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA
const int N = 17;
int n;
pair<int, int> a[N];
bool ok[1 << N];
bool check(int S) {
  vector<pair<int, int>> v;
  FOR(i, 0, n - 1) if (S >> i & 1) v.pb(a[i]);
  sort(v.begin(), v.end());
  for (int i = 0; i + 1 < v.size(); ++i) {
    v[i].first -= v[i + 1].first;
    v[i].second -= v[i + 1].second;
  }
  bool flag = 1;
  for (int i = 0; i + 2 < v.size(); ++i) {
    if (v[i].first * v[i + 1].second != v[i].second * v[i + 1].first) {
      flag = 0;
      break;
    }
  }
  return flag;
}
int ans, lim;
void dfs(int S, int cur) {
  if (cur >= ans) return;
  if (S == lim - 1) {
    ans = cur;
    return;
  }
  int T = lim - 1 - S;
  for (int i = T; i; i = (i - 1) & T) {
    if (ok[i])
      dfs(S | i, cur + 1);
    else {
      for (int j = S; j; j = (j - 1) & S) {
        if (ok[i | j]) {
          dfs(S | i, cur + 1);
          break;
        }
      }
    }
  }
}
int go() {
  scanf("%d", &n);
  FOR(i, 0, n - 1) {
    char p[5];
    scanf("%s", p);
    a[i] = {p[1], p[0] - 'A'};
  }
  return n;
}
int main() {
  int x = go();
  int y = go();
  if (x > y)
    puts("Cuber QQ");
  else
    puts("Quber CC");
  return 0;
}
