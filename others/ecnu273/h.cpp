// by Sshwy
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA
const int N = 5005;
int n, m, a, b;
vector<int> g[N];

int vis[N];
vector<int> V;
bool dfs(int u, int col) {
  vis[u] = col;
  V.pb(u);
  for (int v : g[u]) {
    if (vis[v]) {
      if (vis[u] == vis[v]) return 1;
    } else {
      if (dfs(v, col ^ 1)) return 1;
    }
  }
  return 0;
}
int dist[N];
queue<int> q;
void bfs(int s) {
  for (auto i : V) dist[i] = 1e9;
  dist[s] = 0;
  q.push(s);
  while (!q.empty()) {
    int u = q.front();
    q.pop();
    for (int v : g[u]) {
      if (dist[v] <= dist[u] + 1) continue;
      dist[v] = dist[u] + 1;
      q.push(v);
    }
  }
}
vector<pair<int, vector<int>>> S;
int work() {
  int smx = -1, ss = -1;
  for (auto s : V) {
    bfs(s);
    int mx = 0;
    for (auto i : V) mx = max(mx, dist[i]);
    if (mx > smx) ss = s, smx = mx;
  }
  bfs(ss);
  S.pb({smx, V});
  return smx;
}
void go() {
  scanf("%d%d%d%d", &n, &m, &a, &b);
  FOR(i, 1, n) g[i].clear();
  S.clear();
  fill(vis, vis + n + 1, 0);
  FOR(i, 1, m) {
    int u, v;
    scanf("%d%d", &u, &v);
    g[u].pb(v);
    g[v].pb(u);
  }
  int ans = 0;
  FOR(i, 1, n) if (!vis[i]) {
    V.clear();
    if (dfs(i, 2)) return puts("No"), void();
    ans += work() + 1;
  }
  // puts("GG");
  if (ans < b - a + 1) return puts("No"), void();
  int bs = 0;
  for (auto p : S) {
    bs = min(b - a - p.first, bs);
    bs = max(bs, 0);
    // [bs,min(bs+p.first,b-a)]
    for (auto x : p.second) {
      dist[x] += bs;
      // printf("dist %d %d\n",x,dist[x]);
      while (1) {
        if (dist[x] > b - a) {
          dist[x] = 2 * (b - a) - dist[x];
          if (bs == b - a) return puts("No"), void();
        } else if (dist[x] < bs) {
          dist[x] = 2 * bs - dist[x];
          if (bs == b - a) return puts("No"), void();
        } else
          break;
      }
    }
    bs += p.first + 1;
    if (bs > b - a) bs = 0;
  }
  puts("Yes");
  FOR(i, 1, n) printf("%d%c", dist[i] + a, " \n"[i == n]);
}
int main() {
  int t;
  scanf("%d", &t);
  FOR(i, 1, t) go();
  return 0;
}
