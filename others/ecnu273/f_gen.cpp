// by Sshwy
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

using namespace RA;

string s = "/eoj", t = ".eoj";

string gen(int n) {
  --n;
  int x = rnd(0, n);
  vector<string> v;
  FOR(i, 1, x) v.pb(s);
  FOR(i, x + 1, n) v.pb(t);
  random_shuffle(v.begin(), v.end());
  string res;
  for (auto z : v) res += z;
  return s + res;
}

int main() {
  int n = rnd(1, 1e5);
  int len = 1e6 / n / 3;
  cout << n << endl;
  FOR(i, 1, n) cout << gen(len) << endl;
  return 0;
}
