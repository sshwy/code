// by Sshwy
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

const int M = 1e6 + 5, N = 1e6 + 5;
int t, n, m;
int a[M], la;

struct qxx {
  int nex, t, v;
};
qxx e[M];
int h[N], le = 1;
void add_path(int f, int t, int v) { e[++le] = (qxx){h[f], t, v}, h[f] = le; }

bool use[N], vis[N];
void dfs(int u) {
  vis[u] = 1;
  for (int i = h[u]; i; i = h[u]) {
    while (i && use[e[i].v]) i = e[i].nex;
    h[u] = i;
    const int v = e[i].t, w = e[i].v;
    if (i) use[w] = 1, dfs(v), a[++la] = i ^ 1;
  }
}
vector<int> ans;
int main() {
  scanf("%d%d", &n, &m);
  FOR(i, 1, m) {
    int u, v;
    scanf("%d%d", &u, &v);
    add_path(u, v, i);
    add_path(v, u, i);
  }
  FOR(i, 1, n) if (!vis[i]) {
    la = 0;
    dfs(i);
    if (la == 0) continue;
    // FOR(i,1,la)printf("(%d,%d)%c",e[a[i]^1].t,e[a[i]].t," \n"[i==la]);
    int u = e[a[1] ^ 1].t, v = e[a[1]].t;
    ans.pb(e[a[1]].v);
    int l = 2, r = la;
    while (l < r) {
      int x = e[a[r] ^ 1].t, y = e[a[r]].t;
      if (min(u, v) == min(x, y) && max(u, v) == max(x, y)) {
        x = e[a[l] ^ 1].t, y = e[a[l]].t;
        ans.pb(e[a[l]].v);
        ++l;
        if (u == x)
          u = y;
        else if (u == y)
          u = x;
        else if (v == x)
          v = y;
        else
          assert(v == y), v = x;
      } else {
        ans.pb(e[a[r]].v);
        --r;
        if (u == x)
          u = y;
        else if (u == y)
          u = x;
        else if (v == x)
          v = y;
        else
          assert(v == y), v = x;
      }
    }
  }
  printf("%d\n", int(ans.size()));
  for (auto x : ans) printf("%d ", x);
  return 0;
}
