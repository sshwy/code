// by Sshwy
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA
const int N = 1e5 + 5, S = 1e6 + 5;
int n;
char s[N];
vector<string> v;
vector<vector<pair<int, int>>> vh;
vector<int> a;

long long base[2], mod[2];

void make_hsh(const string &t) {
  vector<pair<int, int>> h;
  pair<int, int> cur = {0, 0};
  for (auto c : t) {
    cur.first = (cur.first * base[0] + c) % mod[0];
    cur.second = (cur.second * base[1] + c) % mod[1];
    h.pb(cur);
  }
  vh.pb(h);
}
bool cmp(int x, int y) { // compare v[x],v[y]
  int pos = 0, step = 0;
  while (step >= 0) {
    int np = pos + (1 << step);
    if (np < min(v[x].size(), v[y].size()) && vh[x][np] == vh[y][np]) {
      pos = np;
      ++step;
    } else
      --step;
  }
  if (pos + 1 == v[x].size() && pos + 1 == v[y].size())
    return 0;
  else if (pos + 1 == v[x].size())
    return 1;
  else if (pos + 1 == v[y].size())
    return 0;
  else if (v[x][pos + 1] < v[y][pos + 1])
    return 1;
  else
    return 0;
}
bool check_prefix(int x, int y) { // v[x] v[y]
  if (v[x].size() > v[y].size()) return 0;
  int pos = 0, step = 0;
  while (step >= 0) {
    int np = pos + (1 << step);
    if (np < min(v[x].size(), v[y].size()) && vh[x][np] == vh[y][np]) {
      pos = np;
      ++step;
    } else
      --step;
  }
  if (pos + 1 == v[x].size()) return 1;
  return 0;
}
int main() {
  srand(clock() + time(0));
  base[0] = RA::rnd(20, 150), mod[0] = RA::rnd(5e8, 1e9);
  base[1] = RA::rnd(20, 150), mod[1] = RA::rnd(5e8, 1e9);
  // printf("%lld %lld\n%lld %lld\n",base[0],mod[0],base[1],mod[1]);

  scanf("%d", &n);
  FOR(i, 1, n) {
    scanf("%s", s);
    for (int j = 0; s[j]; ++j)
      if (s[j] == '/') s[j] -= 2;
    string t = s;
    v.pb(t);
    make_hsh(t);
    a.pb(a.size());
  }
  sort(a.begin(), a.end(), cmp);
  // for(auto x:a)cout<<v[x]<<endl;
  int ans = 0;
  for (int i = 0; i < a.size(); ++i) {
    int u = a[i];
    if (v[u].size() > 4 && v[u][v[u].size() - 4] == '.' &&
        v[u][v[u].size() - 3] == 'e' && v[u][v[u].size() - 2] == 'o' &&
        v[u][v[u].size() - 1] == 'j') {
      if (i + 1 == a.size())
        ++ans;
      else {
        if (check_prefix(a[i], a[i + 1]) == 0 ||
            v[a[i + 1]][v[a[i]].size()] != '/' - 2)
          ++ans;
      }
    }
  }
  printf("%d\n", ans);
  return 0;
}
