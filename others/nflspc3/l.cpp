// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

typedef long long LL;
long long dd[100], ld;
namespace math {
  inline int pw(int a, int b, int mod) {
    int res = 1;
    for (; b; b >>= 1, a = 1ll * a * a % mod)
      if (b & 1) res = 1ll * res * a % mod;
    return res;
  }
  inline LL mul(LL a, LL b, LL p) {
    if (p <= 1000000000ll) return 1ll * a * b % p;
    if (p <= 1000000000000ll)
      return (((a * (b >> 20) % p) << 20) % p + a * (b & ((1 << 20) - 1))) % p;
    LL d = floor(a * (long double)b / p);
    LL res = (a * b - d * p) % p;
    if (res < 0) res += p;
    return res;
  }
  inline LL pw(LL a, LL b, LL mod) {
    LL res = 1;
    for (; b; b >>= 1, a = mul(a, a, mod))
      if (b & 1) res = mul(res, a, mod);
    return res;
  }
  inline bool check(LL a, LL x, LL times, LL n) {
    LL tmp = pw(a, x, n);
    while (times--) {
      LL last = mul(tmp, tmp, n);
      if (last == 1 && tmp != 1 && tmp != n - 1) return 0;
      tmp = last;
    }
    return tmp == 1;
  }
  int base[] = {2, 3, 5, 7, 11, 13, 17, 19, 23};
  const int S = 8;
  inline bool Miller(LL n) {
    FOR(i, 0, S) {
      if (n == base[i]) return 1;
      if (n % base[i] == 0) return 0;
    }
    LL x = n - 1, times = 0;
    while (!(x & 1)) times++, x >>= 1;
    FOR(_, 0, S) if (!check(base[_], x, times, n)) return 0;
    return 1;
  }
#define mytz __builtin_ctzll
  inline LL gcd(LL a, LL b) {
    if (!a) return b;
    if (!b) return a;
    register int t = mytz(a | b);
    a >>= mytz(a);
    do {
      b >>= mytz(b);
      if (a > b) {
        LL t = b;
        b = a, a = t;
      }
      b -= a;
    } while (b);
    return a << t;
  }
#define F(x) ((mul(x, x, n) + c) % n)
  inline LL rho(LL n, LL c) {
    LL x = 1ll * rand() * rand() % n, y = F(x);
    while (x ^ y) {
      LL w = gcd(abs(x - y), n);
      if (w > 1 && w < n) return w;
      x = F(x), y = F(y), y = F(y);
    }
    return 1;
  }
#undef F
  /*inline LL calc(LL x){
    if(Miller(x))return x;
    LL fsf=0; //while((fsf=rho(x,rand()%x))==1);
    while((fsf=rho(x,2))==1);
    return max(calc(fsf),calc(x/fsf));
    }*/

  void calc(LL x) {
    if (Miller(x)) {
      dd[++ld] = x;
    } else {
      LL fsf = 0; // while((fsf=rho(x,rand()%x))==1);
      while ((fsf = rho(x, 2)) == 1)
        ;
      calc(fsf), calc(x / fsf);
    }
  }
} // namespace math

namespace IO { // require C++11
  const int _BS = 200000;
  char _ob[_BS], *_z = _ob, _ib[_BS], *_x = _ib, *_y = _ib;
  // Input
  char _nc() {
    return _x == _y && (_y = (_x = _ib) + fread(_ib, 1, _BS, stdin), _x == _y)
               ? EOF
               : *_x++;
  }
  inline void rd(char &c) { c = _nc(); }
  void rd(char *s) {
    char c = _nc();
    while (!isblank(c) && isprint(c) && c != EOF) *s++ = c, c = _nc();
    *s = 0;
  }
  template <class T> void rd(T &res) { // unsigned/signed int/long long/__int128
    res = 0;
    char c = _nc(), flag = 0;
    while (!isdigit(c) && c != '-') c = _nc();
    if (c == '-') flag = 1, c = _nc();
    while (isdigit(c)) res = res * 10 + c - '0', c = _nc();
    if (flag) res = -res;
  }
  template <class T, class... A> void rd(T &hd, A &...rs) { rd(hd), rd(rs...); }
  // Output
  inline void flush() { fwrite(_ob, 1, _z - _ob, stdout), _z = _ob; }
  inline void wrch(char x) {
    if (_z == _ob + _BS - 1) flush();
    *_z = x, ++_z;
  }
  void wr(char *s) {
    while (*s) wrch(*s), ++s;
  }
  void wr(const char *s) {
    while (*s) wrch(*s), ++s;
  }
  template <class T> void wr(T x) { // unsigned/signed int/long long/__int128
    if (x == 0) return wrch('0'), void();
    if (x < 0) wrch('-'), x = -x;
    int _t[50], _lt = 0;
    while (x) _t[++_lt] = x % 10, x /= 10;
    while (_lt > 0) wrch(_t[_lt] + '0'), --_lt;
  }
  inline void wr(char x) { wrch(x); }
  template <class T, class... A> void wr(T hd, A... rs) { wr(hd), wr(rs...); }
  template <class... A> void wrln(A... ar) { wr(ar...), wrch('\n'); }
  struct _Flusher {
    ~_Flusher() { flush(); }
  } _flusher;
} // namespace IO
using IO::rd;
using IO::wrln;

long long a[6];
long long las = 0;

long long b[100], cnt[100], lb;
long long ans;

void check(__int128 c) {
  if (c > ans) return;
  if (((((a[5] * c + a[4]) * c + a[3]) * c + a[2]) * c + a[1]) * c + a[0] == 0) {
    ans = c;
  }
}
void dfs(int cur, long long v) {
  if (v >= ans) return;
  // printf("cur %d v %lld lb %lld\n",cur,v,lb);
  if (cur == lb + 1) {
    // printf("vv %lld\n",v);
    check(v);
    return;
  }
  long long coef = 1;
  FOR(i, 0, cnt[cur]) {
    // printf("i %d\n",i);
    dfs(cur + 1, v * coef);
    if (v * coef >= ans) return;
    coef *= b[cur];
  }
}
long long calc() {
  ans = 1e5 + 5;
  long long x = a[0];

  ld = 0;
  math::calc(x);

  sort(dd + 1, dd + ld + 1);

  lb = 0;
  FOR(i, 1, ld) if (dd[i] == dd[i - 1]) cnt[lb]++;
  else b[++lb] = dd[i], cnt[lb] = 1;

  dfs(1, 1);

  // printf("ans %lld\n",ans);
  return ans;
}

int main() {
  while (1) {
    rd(a[5], a[4], a[3], a[2], a[1], a[0]);
    a[0] ^= las;
    if (*min_element(a, a + 6) == 0 && *max_element(a, a + 6) == 0) { return 0; }
    // FOR(i,0,5)printf("%lld%c",a[i]," \n"[i==5]);
    if (a[0] == 0) {
      las = 0;
    } else {
      if (a[0] < 0) FOR(i, 0, 5) a[i] = -a[i];
      las = calc();
    }
    wrln(las);
  }
  return 0;
}
