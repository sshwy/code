// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

int main() {
  FOR(i, 1, 166) {
    printf("0 %d\n", i * 2);
    printf("1 %d\n", i * 2);
    printf("2 %d\n", i * 2);
    printf("3 %d\n", i * 2);
    printf("0 %d\n", i * 2 - 1);
    printf("3 %d\n", i * 2 - 1);
  }
  printf("0 %d\n", 0);
  printf("3 %d\n", 0);
  printf("0 %d\n", 167 * 2);
  printf("3 %d\n", 167 * 2);
  // printf("2 333");
  return 0;
}
