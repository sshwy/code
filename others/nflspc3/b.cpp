#include <bits/stdc++.h>
#define MaxN 20000123
int n;
long double dp[2];
inline long double E(int i, int j = 1) {
#ifdef Dubug
  std::cout << "E (" << i << " " << j << ") = " << (1. + 1. * (n - i) / (i + 1)) * j
            << std::endl;
#endif
  return (1. + 1. * (n - i) / (i + 1)) * j;
}

inline long double arithsum(long double s) { return s * (s + 1) / 2.; }

int main() {
  scanf("%d", &n);

  n = std::min(n, int(2e7));

  for (int j = 1; j <= n; ++j) dp[n & 1] += E(n, j);
  dp[n & 1] /= n;

  for (int i = n - 1; i; --i) {
    int x = dp[(i + 1) & 1] / E(i);
    dp[i & 1] = (E(i) * arithsum(x) + (i - x) * dp[(i + 1) & 1]) / i;
  }

  printf("%.6Lf\n", dp[1]);
  // std::cout<<dp[1]<<std::endl;

  return 0;
}
