#include <bits/stdc++.h>
#define FOR(a, b, c) for (int a = (b); a <= (int)(c); a++)
#define ROF(a, b, c) for (int a = (b); a >= (int)(c); a--)
using namespace std;
const int N = 1e3 + 5, M = 12;
const int P = 998244353;
int n, K;

long long f[N][M][2]; // 0: 1 boundary; 1: 2 boundary
long long g[N][M][2];
long long c[N][N];
void add(long long &x, long long y) { x = (x + y) % P; }
int pw(int a, int m) {
  int res = 1;
  while (m) m & 1 ? res = 1ll * res * a % P : 0, a = 1ll * a * a % P, m >>= 1;
  return res;
}

int main() {
  scanf("%d", &n);
  K = n;
  c[0][0] = 1;
  FOR(i, 1, n) {
    c[i][0] = 1;
    FOR(j, 1, i) add(c[i][j], c[i - 1][j - 1] + c[i - 1][j]);
  }
  f[0][0][0] = 1;
  f[0][0][1] = 1;
  g[0][0][0] = g[0][0][1] = 1;
  FOR(j, 1, M - 1) g[0][j][0] = g[0][j][1] = 1;
  FOR(i, 1, n) {
    FOR(j, 1, M - 1) {
      FOR(k, 1, i) {
        long long t[2] = {0};
        add(t[1], f[k - 1][j - 1][1] * f[i - k][j - 1][1]);
        add(t[1], f[k - 1][j][1] * g[i - k][j - 1][1]);
        add(t[1], g[k - 1][j - 1][1] * f[i - k][j][1]);

        add(t[0], g[k - 1][j][0] * f[i - k][j - 1][1]);
        if (j >= 2) add(t[0], f[k - 1][j][0] * g[i - k][j - 2][1]);

        add(f[i][j][1], t[1] * c[i - 1][k - 1]);
        add(f[i][j][0], t[0] * c[i - 1][k - 1]);
      }
      add(g[i][j][0], f[i][j][0] + g[i][j - 1][0]);
      add(g[i][j][1], f[i][j][1] + g[i][j - 1][1]);
    }
  }
  long long ans = 0;
  FOR(_K, 1, K) {
    long long s = 0;
    FOR(k, 1, n) {
      add(s, f[k - 1][_K][0] * g[n - k][_K][0] % P * c[n - 1][k - 1]);
      add(s, g[k - 1][_K - 1][0] * f[n - k][_K][0] % P * c[n - 1][k - 1]);
    }
    add(ans, s * _K % P);
  }
  long long s = 1;
  FOR(i, 1, n) s = s * i % P;
  s = pw(s, P - 2);
  ans = ans * 1ll * s % P;
  printf("%lld\n", ans);
  return 0;
}
