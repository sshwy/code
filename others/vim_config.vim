" Basic Setting
syntax on
set nocp nu cin autoindent ts=4 sw=4 mouse=a smartindent smarttab
set backspace=indent,eol,start
set foldmethod=marker foldlevel=99
autocmd FileType cpp set foldlevel=0
set encoding=utf-8 ambiwidth=double
set et  " 编辑时将所有 Tab 替换为空格
set fo+=mB
set selection=inclusive
set wildmenu
set fileencodings=ucs-bom,utf-8,cp936,gb18030,big5,euc-jp,euc-kr,latin1

imap jj <ESC>
nmap <F5> :!clear;g++ % -O2 -ggdb -o %< -Wall <ESC>
nmap <F6> :!clear;time ./%< <ESC>
nmap <F7> :!clear;time ./%< < %<.in <ESC>
nmap <F8> :vsp %<.in <ESC>
nmap <F9> :!clear;gdb --tui %< <ESC>
nmap <C-g> gg"+yG <ESC>
vmap <C-c> "+y <ESC>

set shell=/bin/zsh
au BufRead,BufNewFile *.ejs set filetype=html " 文件类型映射

"hi Conceal ctermfg=NONE
"hi Conceal guifg=NONE
hi Conceal ctermbg=NONE
hi Conceal guibg=NONE

function Copy_file_to_cplibroad()
    let @+=expand('%:p')
endfunction

nmap <C-f> :call Copy_file_to_cplibroad()<cr>

" Gvim Setting

set guioptions-=m  "remove menu bar
set guioptions-=T  "remove toolbar
set guioptions-=r  "remove right-hand scroll bar
set guioptions-=L  "remove left-hand scroll bar
