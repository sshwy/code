// by Sshwy
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

using namespace RA;
const int N = 2e5 + 5;
int b[N];
void go() {
  int n = 2e5, q = 2e5;
  FOR(i, 1, n) b[i] = rnd(1, 1e9);
  printf("%d %d\n", n, q);
  FOR(i, 1, n) printf("%d%c", b[i], " \n"[i == n]);
  FOR(i, 1, q) {
    if (rnd(2)) {
      int x = rnd(1, n), y = rnd(1, 1e9);
      printf("1 %d %d\n", x, y);
    } else
      printf("2 %d\n", rnd(1, n));
  }
}
int main() {
  srand(clock() + time(0));
  int t = 5;
  printf("%d\n", t);
  FOR(i, 1, t) go();
  return 0;
}
