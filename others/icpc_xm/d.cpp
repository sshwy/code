#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
const int N = 3e5 + 5, M = N * 2;

struct qxx {
  int nex, t;
};

namespace G {
  qxx e[N * 2];
  int h[M * 2], le = 1;
  int dg[N * 2];
  void add_path(int f, int t) { e[++le] = (qxx){h[f], t}, h[f] = le, dg[f]++; }
  void add_both(int f, int t) { add_path(f, t), add_path(t, f); }
} // namespace G

qxx e[M];
int h[N], le = 1;
void add_path(int f, int t) { e[++le] = (qxx){h[f], t}, h[f] = le; }

int n, m;

int island[N];

int dfn[N], low[N], totdfn;
int s[N], tp;
int vcc;

void tarjan(int u, int p) {
  dfn[u] = low[u] = ++totdfn;
  s[++tp] = u;
  for (int i = h[u]; i; i = e[i].nex) {
    const int v = e[i].t;
    if (!dfn[v]) {
      tarjan(v, u);
      low[u] = min(low[u], low[v]);
      if (dfn[u] <= low[v]) {
        ++vcc, G::add_both(u, vcc + n);
        do { G::add_both(s[tp], vcc + n); } while (s[tp--] != v);
      }
    } else
      low[u] = min(low[u], dfn[v]);
  }
}

int main() {
  scanf("%d%d", &n, &m);
  FOR(i, 1, n) island[i] = 1;
  FOR(i, 1, m) {
    int u, v;
    scanf("%d%d", &u, &v);
    add_path(u, v);
    add_path(v, u);
    island[u] = island[v] = 0;
  }
  int totc = 0;
  FOR(i, 1, n) if (!dfn[i]) {
    tarjan(i, 0);
    totc++;
  }

  FOR(i, 1, n) {
    int ans = 0;
    if (island[i])
      ans = totc - 1;
    else {
      ans = totc + G::dg[i] - 1;
    }
    printf("%d%c", ans, " \n"[i == n]);
  }

  return 0;
}
