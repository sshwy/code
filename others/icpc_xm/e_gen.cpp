// by Sshwy
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA
using namespace RA;
const int N = 5e5 + 5;
int n, m, p[N];
int main() {
  srand(clock() + time(0));
  int n = rnd(1, 100), m = rnd(1, n);
  FOR(i, 1, m) p[i] = i;
  FOR(i, m + 1, n) p[i] = rnd(1, m);
  random_shuffle(p + 1, p + n + 1);
  printf("%d %d\n", n, m);
  FOR(i, 1, n) printf("%d%c", p[i], " \n"[i == n]);
  return 0;
}
