// by Sshwy
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA
const int N = 1e5 + 5;
int n, p[N];

using namespace RA;
int main() {
  srand(clock() + time(0));
  n = rnd(1, 10000);
  FOR(i, 1, n) p[i] = i;
  printf("%d\n", n);
  random_shuffle(p + 1, p + n + 1);
  FOR(i, 1, n) printf("%d%c", p[i], " \n"[i == n]);
  random_shuffle(p + 1, p + n + 1);
  FOR(i, 1, n) printf("%d%c", p[i], " \n"[i == n]);
  return 0;
}
