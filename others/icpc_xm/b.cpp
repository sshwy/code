// by Sshwy
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

typedef double vtyp;
namespace cg {
  const vtyp eps = 1e-6;
  bool isZero(vtyp x) { return -eps < x && x < eps; }
  bool equal(vtyp x, vtyp y) { return isZero(x - y); }
  bool lt(vtyp x, vtyp y) { return !equal(x, y) && x < y; }
  bool gt(vtyp x, vtyp y) { return !equal(x, y) && x > y; }
  bool leq(vtyp x, vtyp y) { return equal(x, y) || x < y; }
  bool geq(vtyp x, vtyp y) { return equal(x, y) || x > y; }
  struct vec {
    vtyp x, y;
    vec() { x = y = 0; }
    vec(vtyp _x, vtyp _y) { x = _x, y = _y; }
    vec operator+(const vec V) const { return vec(x + V.x, y + V.y); }
    vec operator-() const { return vec(-x, -y); }
    vec operator-(const vec V) const { return *this + (-V); }
    vec operator*(const vtyp a) const { return vec(x * a, y * a); }
    friend vec operator*(const vtyp a, const vec v) { return v * a; }
    vec operator/(const vtyp a) const { return vec(x / a, y / a); }
    operator bool() const { return !(isZero(x) && isZero(y)); }
    bool operator==(const vec V) const { return bool(*this - V) == 0; }
    vtyp length() { return sqrt(x * x + y * y); }
    /**
     * 方向角，单位 rad
     */
    vtyp ang() const { return atan2(y, x); }
    /**
     * 方向向量
     * @return 0向量或者一个单位向量
     */
    vec dir() const {
      if (*this) {
        vtyp ang = atan2(y, x);
        return vec(cos(ang), sin(ang));
      } else
        return vec(0, 0);
    }
    void read() {
      int _x, _y;
      scanf("%d%d", &_x, &_y);
      x = _x, y = _y;
    }
  };
  typedef vec point;

  struct line {
    point p1, p2;
    line() {}
    line(point _p1, point _p2) { p1 = _p1, p2 = _p2; }
    vtyp length() { return (p1 - p2).length(); }
  };
  typedef line segment;

  istream &operator>>(istream &in, vec &v) { return in >> v.x >> v.y, in; }
  ostream &operator<<(ostream &out, const vec &v) {
    return out << v.x << " " << v.y, out;
  }
  ifstream &operator>>(ifstream &in, vec &v) { return in >> v.x >> v.y, in; }
  ofstream &operator<<(ofstream &out, const vec &v) {
    return out << v.x << " " << v.y, out;
  }
  /**
   * 点积
   * a dot b == |a||b|cos theta
   */
  vtyp dot(const vec a, const vec b) { return a.x * b.x + a.y * b.y; }
  /**
   * 叉积
   * 两个向量围成的有向面积
   */
  vtyp det(const vec a, const vec b) { return a.x * b.y - a.y * b.x; }

  /**
   * 投影
   * @param L 直线
   * @param p 要求投影的点
   * @return p 在 L 上的投影坐标（即垂足）
   */
  point projection(line L, point p) {
    vec d = L.p2 - L.p1;
    return L.p1 + (dot(d, p - L.p1) / d.length()) * d.dir();
  }
  /**
   * 对称点
   * @param L 直线
   * @param p 点
   * @return p 关于直线 L 的对称点
   */
  point reflection(line L, point p) {
    point o = projection(L, p);
    return vtyp(2) * (o - p) + p;
  }

  /**
   * 判断向量是否平行
   */
  bool parallel(vec a, vec b) { return isZero(det(a, b)); }
  /**
   * 判断直线是否平行
   */
  bool parallel(line a, line b) { return parallel(a.p2 - a.p1, b.p2 - b.p1); }
  /**
   * 判断向量是否垂直
   */
  bool orthogonal(vec a, vec b) { return isZero(dot(a, b)); }
  /**
   * 判断直线是否垂直
   */
  bool orthogonal(line a, line b) { return orthogonal(a.p2 - a.p1, b.p2 - b.p1); }
  /**
   * 判断点 p 是否在直线L上
   */
  bool online(line L, point p) { return parallel(L.p2 - L.p1, p - L.p1); }
  /**
   * 判断两直线是否重合
   */
  bool coincident(line a, line b) { return online(a, b.p1) && online(a, b.p2); }
  /**
   * 判断点 p 是否与有向线段共线且在反向延长线上
   */
  bool online_back(segment sl, point p) {
    vec a = sl.p2 - sl.p1, b = p - sl.p1;
    return parallel(a, b) && leq(dot(a, b), 0);
  }
  /**
   * 判断点 p 是否与有向线段共线且在正向延长线上
   */
  bool online_front(segment sl, point p) {
    vec a = sl.p1 - sl.p2, b = p - sl.p2; // 倒过来
    return parallel(a, b) && leq(dot(a, b), 0);
  }
  /**
   * 判断点 p 是否在线段上（含端点）
   */
  bool on_segment(segment sl, point p) {
    return online(sl, p) && !online_back(sl, p) && !online_front(sl, p);
  }
  /**
   * 两条直线的交点
   * 需确保两条直线不平行
   */
  point intersection(line a, line b) {
    assert(!parallel(a, b));
    vtyp x = det(a.p1 - b.p1, b.p2 - b.p1);
    vtyp y = det(b.p2 - b.p1, a.p2 - b.p1);
    return a.p1 + (a.p2 - a.p1) * x / (x + y);
  }
  /**
   * 判断两个线段是否相交（含边界）
   */
  bool check_segment_intersection(segment a, segment b) {
    if (cg::coincident(a, b)) {
      if (on_segment(a, b.p1) || on_segment(a, b.p2) || on_segment(b, a.p1) ||
          on_segment(b, a.p2))
        return true;
      else
        return false;
    } else if (cg::parallel(a, b)) {
      return false;
    } else {
      point o = cg::intersection(a, b);
      if (cg::on_segment(a, o) && cg::on_segment(b, o))
        return true;
      else
        return false;
    }
  }
  /**
   * 两个点的距离
   */
  vtyp distance(point a, point b) { return (b - a).length(); }
  /**
   * 两个线段的距离
   */
  vtyp distance(segment a, segment b) {
    if (check_segment_intersection(a, b)) return 0;
    vtyp res = distance(a.p1, b.p1);
    res = min(res, distance(a.p1, b.p2));
    res = min(res, distance(a.p2, b.p1));
    res = min(res, distance(a.p2, b.p2));
    point o;
    if (o = projection(b, a.p1), on_segment(b, o)) res = min(res, distance(a.p1, o));
    if (o = projection(b, a.p2), on_segment(b, o)) res = min(res, distance(a.p2, o));
    if (o = projection(a, b.p1), on_segment(a, o)) res = min(res, distance(b.p1, o));
    if (o = projection(a, b.p2), on_segment(a, o)) res = min(res, distance(b.p2, o));
    return res;
  }
  /**
   * 求简单多边形面积
   * @param g 多边形顶点集
   */
  vtyp area(vector<point> g) {
    vtyp res = 0;
    for (unsigned i = 0; i < g.size(); i++) {
      res += det(g[i], g[(i + 1) % g.size()]);
    }
    res /= 2;
    return abs(res);
  }
  /**
   * 判断点p与多边形的包含关系
   * @param g 多边形顶点集
   * @return 0 表示在多边形外，1 表示在边上，2表示在多边形内
   */
  int polygon_point_containment(vector<point> g, point p) {
    line L(vec(p.x - 1, p.y), p); // 水平方向的射线
    int cnt = 0;
    for (unsigned i = 0; i < g.size(); i++) {
      int j = (i + 1) % g.size();
      line e(g[i], g[j]);
      if (on_segment(e, p)) return 1;
      if (parallel(L, e)) {
        // do nothing.
      } else if (online_front(L, g[i])) {
        if (g[i].y > g[j].y) ++cnt;
      } else if (online_front(L, g[j])) {
        if (g[j].y > g[i].y) ++cnt;
      } else {
        point o = intersection(L, e);
        if (on_segment(e, o) && online_front(L, o)) ++cnt;
      }
    }
    if (cnt % 2) return 2;
    return 0;
  }
} // namespace cg
using cg::line;
using cg::point;
using cg::segment;
const int N = 610;

int n, m, k;
segment l[N];
int tot;
point s, t, p[N];
vtyp w[N][N];

typedef pair<vtyp, int> pii;
priority_queue<pii, vector<pii>, greater<pii>> q;
vtyp dist[N];

void dijkstra(int s) {
  fill(dist, dist + N, 1e18);
  dist[s] = 0, q.push(make_pair(dist[s], s));
  while (q.size()) {
    pii u = q.top();
    q.pop();
    if (dist[u.second] < u.first) continue;
    FOR(i, 1, tot) if (i != u.second) {
      const int &v = i;
      if (dist[v] <= dist[u.second] + w[u.second][v]) continue;
      dist[v] = dist[u.second] + w[u.second][v];
      q.push(make_pair(dist[v], v));
    }
  }
}

int main() {
  scanf("%d%d%d", &n, &m, &k);
  FOR(i, 1, N - 1) FOR(j, 1, i - 1) w[i][j] = w[j][i] = 1e18;
  FOR(i, 1, k) {
    l[i].p1.read(), l[i].p2.read();
    p[++tot] = l[i].p1;
    p[++tot] = l[i].p2;
    w[tot][tot - 1] = w[tot - 1][tot] = l[i].length();
  }
  s.read(), t.read();
  p[++tot] = s, p[++tot] = t;
  FOR(i, 1, tot) FOR(j, 1, i - 1) {
    segment cur(p[i], p[j]);
    bool flag = 1;
    FOR(e, 1, k) if (cg::check_segment_intersection(cur, l[e])) {
      flag = 0;
      break;
    }
    if (flag) w[i][j] = w[j][i] = min(w[i][j], cur.length());
    // printf("w[%d,%d] = %.4lf\n",i,j,w[i][j]);
  }
  dijkstra(tot - 1);
  printf("%.4lf\n", dist[tot]);
  return 0;
}
