// by Sshwy
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA
const int N = 2e5 + 5, INF = 1e9;
int n, m;
int a[N];

int mx_b[N << 2]; // (u,l,r): y=-x+l+mx_b[u]
int mn[N << 2];
bool tgc[N << 2];

void cl(int u, int l, int r, int tb) {
  mx_b[u] = tb;
  mn[u] = -r + l + mx_b[u];
  tgc[u] = 1;
}
void upd(int L, int R, int b, int u, int l, int r) {
  assert(mx_b[u] <= -l + L + b);
  mx_b[u] = -l + L + b;
  mn[u] = -r + l + mx_b[u];
  tgc[u] = 1;
}
void pushdown(int u, int l, int r) {
  int mid = (l + r) >> 1;
  if (tgc[u])
    cl(u << 1, l, mid, mx_b[u]), cl(u << 1 | 1, mid + 1, r, -(mid + 1) + l + mx_b[u]),
        tgc[u] = 0;
}
void assign(int L, int R, int b, int u = 1, int l = 1, int r = n) {
  if (L > R) return;
  // if(u==1)printf(": assign %d %d %d\n",L,R,b);
  // printf("  assign %d %d %d %d %d %d\n",L,R,b,u,l,r);
  if (l == r) {
    mx_b[u] = max(mx_b[u], -l + L + b);
    mn[u] = mx_b[u];
    return;
  } else {
    int mid = (l + r) >> 1;
    pushdown(u, l, r);
    if (L <= l && r <= R) {
      int y_mid = -(mid + 1) + L + b;
      if (mx_b[u << 1 | 1] >= y_mid) {
        assign(L, R, b, u << 1, l, mid);
      } else {
        assign(L, R, b, u << 1 | 1, mid + 1, r);
        upd(L, R, b, u << 1, l, mid);
      }
    } else {
      if (L <= mid) assign(L, R, b, u << 1, l, mid);
      if (mid < R) assign(L, R, b, u << 1 | 1, mid + 1, r);
    }
    mx_b[u] = max(mx_b[u], mx_b[u << 1]);
    mn[u] = min(mn[u << 1], mn[u << 1 | 1]);
  }
}

vector<int> pos[N];
int main() {
  scanf("%d%d", &n, &m);
  FOR(i, 1, m) pos[i].pb(0);
  FOR(i, 1, n) {
    int x;
    scanf("%d", &x);
    pos[x].pb(i);
  }
  fill(mn, mn + (n << 2) + 1, INF);

  FOR(i, 1, m) {
    for (unsigned j = 1; j < pos[i].size(); j++) {
      int x = pos[i][j - 1], y = pos[i][j];
      assign(x + 1, y, y - x);
    }
    int x = pos[i][pos[i].size() - 1];
    assign(x + 1, n, INF);
    printf("%d%c", mn[1], " \n"[i == m]);
  }

  return 0;
}
