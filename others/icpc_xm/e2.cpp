// by Sshwy
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA
const int N = 2e5 + 5;
int n, m;
int c[N], cnt, a[N];
int calc(int x) {
  // printf("calc %d\n",x);
  FOR(i, 1, x) c[i] = 0;
  int r = 0, ans = n;
  cnt = 0;
  FOR(i, 1, n) {
    if (i > 1) {
      if (a[i - 1] <= x && c[a[i - 1]] == 1) --cnt;
      c[a[i - 1]]--;
    }
    while (cnt < x) {
      if (r == n)
        break;
      else if (a[r + 1] > x)
        ++r;
      else {
        if (c[a[r + 1]] == 0) ++cnt;
        ++c[a[r + 1]];
        ++r;
      }
    }
    if (cnt < x) break;
    ans = min(ans, r - i + 1);
    // printf("i %d r %d\n",i,r);
  }
  return ans;
}
int main() {
  scanf("%d%d", &n, &m);
  FOR(i, 1, n) scanf("%d", a + i);
  FOR(i, 1, m) { printf("%d%c", calc(i), " \n"[i == m]); }
  return 0;
}
