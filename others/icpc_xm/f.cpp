// by Sshwy
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

const int N = 1e5 + 5;
int n, L, R;
int a[N], ll[N], rr[N];

bool check(long long x) {
  // printf("check %d\n",x);
  long long rest = 0, tot = 0;
  FOR(i, 1, n) {
    if (x > 1.0 * a[i] / ll[i]) return 0;
    long long t = a[i] - ll[i] * x;
    if ((rr[i] - ll[i]) < 1e18 / x) t = min(t, (rr[i] - ll[i]) * x);
    rest += t;
    tot += ll[i] * x;
  }
  // printf("rest = %lld, tot=%lld\n",rest,tot);
  if (tot * 1.0 / x > R) return 0;
  if ((tot + rest) * 1.0 / x < L) return 0;
  return 1;
}
int main() {
  scanf("%d%d%d", &n, &L, &R);
  FOR(i, 1, n) scanf("%d", a + i);
  FOR(i, 1, n) scanf("%d%d", ll + i, rr + i);
  long long l = 0, r = 1e18;
  while (l < r) {
    long long mid = (l + r + 1) >> 1;
    if (check(mid))
      l = mid;
    else
      r = mid - 1;
  }
  printf("%lld\n", l);
  return 0;
}
