// problem:
#include <bits/stdc++.h>
using namespace std;

#define pb push_back
#define mk make_pair
#define lob lower_bound
#define upb upper_bound
#define fi first
#define se second
#define SZ(x) ((int)(x).size())

typedef unsigned int uint;
typedef long long ll;
typedef unsigned long long ull;
typedef pair<int, int> pii;

template <typename T> inline void ckmax(T &x, T y) { x = (y > x ? y : x); }
template <typename T> inline void ckmin(T &x, T y) { x = (y < x ? y : x); }

const int MAXA = 1e7;
bool v[MAXA + 5];
int p[MAXA + 5], cnt, g[MAXA + 5], h[MAXA + 5];

int n;
int dp[MAXA + 5], ans;
void sieve(int lim) {
  for (int i = 2; i <= lim; ++i) {
    if (!v[i]) {
      p[++cnt] = i;
      g[i] = i;
      h[i] = 1;
    }
    for (int j = 1; j <= cnt && p[j] * i <= lim; ++j) {
      int t = i * p[j];
      v[t] = 1;
      if (i % p[j] == 0) {
        g[t] = p[j];
        h[t] = h[i];
        break;
      }
      g[t] = p[j];
      h[t] = i;
    }
  }
}
int main() {
  sieve(MAXA);
  fill(p + 1, p + MAXA + 1, 0);
  scanf("%d", &n);
  for (int i = 1; i <= n; ++i) {
    int a;
    scanf("%d", &a);
    p[a]++;
  }

  ans = dp[1] = p[1];
  for (int i = 2; i <= MAXA; ++i) {
    int x = i;
    while (x != 1) {
      ckmax(dp[i], dp[i / g[x]] + p[i]);
      x = h[x];
    }
    ckmax(ans, dp[i]);
  }
  printf("%d\n", ans);
  return 0;
}
