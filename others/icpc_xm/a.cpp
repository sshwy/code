// by Sshwy
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA
const int N = 2e5 + 5, W = 1e7 + 5;
bool bp[W];
int p[W], lp;
int g[W];

void sieve(int lim) {
  bp[0] = bp[1] = 1;
  FOR(i, 2, lim) {
    if (bp[i] == 0) p[++lp] = i, g[i] = i;
    FOR(j, 1, lp) {
      if (i * p[j] > lim) break;
      int x = i * p[j];
      bp[x] = 1;
      g[x] = p[j];
      if (i % p[j] == 0) break;
    }
  }
}
int n, a[N], f[W];
int b[100], c[100], lb;
int ans;
void dfs(int &fx, int k, int v) {
  fx = max(fx, f[v]);
  if (k == lb + 1) return;
  dfs(fx, k + 1, v);
  FOR(i, 1, c[k]) {
    v *= b[k];
    dfs(fx, k + 1, v);
  }
}
void work(int x, int cx) {
  // printf("work %d %d\n",x,cx);
  lb = 0;
  int xx = x;
  while (xx > 1) {
    b[++lb] = g[xx], c[lb] = 0;
    while (g[xx] == b[lb]) xx /= g[xx], ++c[lb];
  }
  if (xx > 1) b[++lb] = xx, c[lb] = 1;

  dfs(f[x], 1, 1);

  f[x] += cx;
  ans = max(ans, f[x]);
}
int main() {
  scanf("%d", &n);
  FOR(i, 1, n) scanf("%d", a + i);
  sort(a + 1, a + n + 1);
  sieve(a[n]);
  a[n + 1] = -1;
  int cur = a[1], cnt = 1;
  FOR(i, 2, n + 1) {
    if (a[i] == a[i - 1])
      ++cnt;
    else {
      work(cur, cnt);
      cur = a[i], cnt = 1;
    }
  }
  printf("%d\n", ans);
  return 0;
}
