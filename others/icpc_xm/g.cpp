// by Sshwy
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA
const int N = 2e5 + 5;
vector<pair<int, int>> ans;
struct dsu {
  int f[N];
  dsu() { FOR(i, 0, N - 1) f[i] = i; }
  int get(int x) { return f[x] == x ? x : f[x] = get(f[x]); }
  void remove(int x) { f[get(x)] = get(x + 1); }
} da, db;
int n;
int a[N], b[N];
int pa[N], pb[N];
void add(int u, int v) { ans.pb({u, v}); }

void solve(int ha, int hb) {
  if (ha > n || hb > n) assert(0);
  for (int i = db.get(pb[a[ha]] + 1); i <= n; i = db.get(i)) {
    add(a[ha], b[i]);
    db.remove(i);
    da.remove(pa[b[i]]);
  }
  for (int i = da.get(pa[b[hb]] + 1); i <= n; i = da.get(i)) {
    add(b[hb], a[i]);
    da.remove(i);
    db.remove(pb[a[i]]);
  }
  if (a[ha] == b[hb]) return;
  if (da.get(ha + 1) == pa[b[hb]]) {
    add(a[ha], b[hb]);
    return;
  }
  solve(da.get(ha + 1), db.get(hb + 1));
}
vector<int> g[N];
int fa[N];
bool vis[N];
void dfs(int u, int p) {
  fa[u] = p;
  for (int v : g[u])
    if (v != p) dfs(v, u);
}
int main() {
  scanf("%d", &n);
  FOR(i, 1, n) scanf("%d", a + i), pa[a[i]] = i;
  FOR(i, 1, n) scanf("%d", b + i), pb[b[i]] = i;

  solve(1, 1);

  printf("YES\n");
  for (auto e : ans) {
    printf("%d %d\n", e.first, e.second);
    int u = e.first, v = e.second;
    g[u].pb(v), g[v].pb(u);
  }
  assert(ans.size() == n - 1);
  vis[0] = 1;

  FOR(i, 1, n) fa[i] = -1;
  dfs(a[1], 0);
  FOR(i, 1, n) vis[i] = 0;
  FOR(i, 1, n) if (fa[a[i]] != -1 && vis[fa[a[i]]]) vis[a[i]] = 1;
  else assert(0);

  FOR(i, 1, n) fa[i] = -1;
  dfs(b[1], 0);
  FOR(i, 1, n) vis[i] = 0;
  FOR(i, 1, n) if (fa[b[i]] != -1 && vis[fa[b[i]]]) vis[b[i]] = 1;
  else assert(0);

  return 0;
}
