// by Sshwy
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA
const int N = 1e3 + 5;
int n, m;
char s[N][N];
bool vis[N][N], out[N][N];

void dfs(int i, int j) {
  if (vis[i][j]) return;
  if (i < 1 || i > n || j < 1 || j > m) {
    out[i][j] = 1;
    vis[i][j] = 1;
    return;
  }
  vis[i][j] = 1;
  switch (s[i][j]) {
    case 'W':
      dfs(i - 1, j);
      if (out[i - 1][j]) out[i][j] = 1;
      break;
    case 'A':
      dfs(i, j - 1);
      if (out[i][j - 1]) out[i][j] = 1;
      break;
    case 'S':
      dfs(i + 1, j);
      if (out[i + 1][j]) out[i][j] = 1;
      break;
    case 'D':
      dfs(i, j + 1);
      if (out[i][j + 1]) out[i][j] = 1;
      break;
    default:
      assert(0);
  }
}
int main() {
  scanf("%d%d", &n, &m);
  FOR(i, 1, n) { scanf("%s", s[i] + 1); }
  FOR(i, 1, n) FOR(j, 1, m) dfs(i, j);
  int ans = 0;
  FOR(i, 1, n) FOR(j, 1, m) if (out[i][j])++ ans;
  // FOR(i,1,n)FOR(j,1,m)printf("%d%c",out[i][j]," \n"[j==m]);
  printf("%d\n", ans);
  return 0;
}
