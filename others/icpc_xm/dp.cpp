#include <cstdio>
#include <cstdlib>
#include <ctime>
using namespace std;

int main() {
  system("g++ g.cpp -o .usr");
  system("g++ g_gen.cpp -o .gen");
  int t = 0;
  while (++t) {
    system("./.gen > .fin");
    if (system("./.usr < .fin > .fout")) break;
    printf("AC#%d\n", t);
  }
  printf("WA#%d\n", t);
  return 0;
}
