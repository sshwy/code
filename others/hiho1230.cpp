// by Yao
#include <cstdio>
#include <cstring>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

const int SZ = 1 << 11, P = 1000000007;
int pw(int a, int m) {
  int res = 1;
  while (m) m & 1 ? res = 1ll * res * a % P : 0, a = 1ll * a * a % P, m >>= 1;
  return res;
}

void fwt(int *f, int len, int tag) {
  for (int j = 1; j < len; j <<= 1)
    for (int i = 0; i < len; i += j << 1)
      for (int k = i, u, v; k < i + j; ++k)
        u = f[k], v = f[k + j], f[k] = (u + v) % P, f[k + j] = (u - v + P) % P;
  if (tag == -1)
    for (int i = 0, in = pw(len, P - 2); i < len; i++) f[i] = 1ll * f[i] * in % P;
}

int n, m, L, R;
int a[SZ];

void go() {
  int ans = 0;
  FOR(x, L, R) {
    memset(a, 0, sizeof(a));
    FOR(i, 0, m) a[i + x] = 1;

    fwt(a, SZ, 1);
    FOR(i, 0, SZ - 1) a[i] = pw(a[i], n * 2 + 1);
    fwt(a, SZ, -1);

    ans = (ans + a[0]) % P;
  }
  printf("%d\n", ans);
}

int main() {
  while (~scanf("%d%d%d%d", &n, &m, &L, &R)) go();
  return 0;
}
