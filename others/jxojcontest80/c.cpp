// by Sshwy
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA
const int N = 1e5 + 5, M = 1e5 + 5, D = 55;
int n, m, d;

struct qxx {
  int nex, t;
} e[M];
int h[N], le = 1;
void add_path(int f, int t) { e[++le] = {h[f], t}, h[f] = le; }
char a[N][D];
bool vis[N][D];

queue<pair<int, int>> q;
int main() {
  scanf("%d%d%d", &n, &m, &d);
  FOR(i, 1, m) {
    int u, v;
    scanf("%d%d", &u, &v);
    add_path(u, v);
  }
  FOR(i, 1, n) { scanf("%s", a[i]); }
  q.push({1, 0});
  while (!q.empty()) {
    auto u = q.front();
    q.pop();
    if (vis[u.first][u.second]) continue;
    vis[u.first][u.second] = 1;
    int t = (u.second + 1) % d;
    for (int i = h[u.first]; i; i = e[i].nex) {
      int v = e[i].t;
      // printf("u %d v %d\n",u.first,v);
      if (!vis[v][t]) q.push({v, t});
    }
  }
  int ans = 0;
  FOR(i, 1, n) {
    bool flag = 0;
    FOR(j, 0, d - 1) if (vis[i][j] && a[i][j] == '1') {
      flag = 1;
      break;
    }
    ans += flag;
  }
  printf("%d\n", ans);

  return 0;
}
