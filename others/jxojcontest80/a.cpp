// by Sshwy
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA
const int N = 1e5 + 5;
int n, X;
struct atom {
  int b, l, r;
} a[N];
long long f[N], g[N];
long long b[N], L[N], R[N];
bool vis[N];

long long F(int i, long long x) {
  if (x < b[i]) return L[i] * x;
  return R[i] * (x - b[i]) + L[i] * b[i];
}
long long tot;
int main() {
  scanf("%d%d", &n, &X);
  FOR(i, 1, n) { scanf("%lld%lld%lld", &b[i], &L[i], &R[i]); }
  FOR(i, 1, n) tot -= L[i] * b[i];
  vector<pair<long long, int>> v;
  FOR(i, 1, n) { v.pb({F(i, X), i}); }
  sort(v.begin(), v.end());
  reverse(v.begin(), v.end());
  long long ans = 0;
  for (auto p : v) {
    if (tot + p.first <= 0) {
      tot += p.first;
      vis[p.second] = 1;
      ans += X;
    } else
      break;
  }
  if (tot < 0) {
    long long l = 0, r = X;
    while (l < r) {
      long long mid = (l + r) >> 1;
      long long x = -1e18;
      FOR(i, 1, n) if (!vis[i]) { x = max(x, F(i, mid)); }
      if (tot + x >= 0)
        r = mid;
      else
        l = mid + 1;
    }
    ans += l;
  }
  printf("%lld\n", ans);
  return 0;
}
