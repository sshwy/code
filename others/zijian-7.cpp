// by Sshwy
// http://zijian-lv.com/problem/7
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA
typedef __int128 i128;
typedef pair<long long, i128> point;
const int N = 2e5 + 5;
int n;
long long a, b, c;
int L, R;
long long s[N];
i128 f[N];

long long X(int j) { return s[j]; }
long long K(int i) { return 2 * a * s[i]; }
i128 B(int i) { return -(i128)a * s[i] * s[i] - (i128)b * s[i]; } //+f[i]
i128 Y(int j) {
  return (i128)a * s[j] * s[j] - (i128)b * s[j] + c + f[j];
} // KX+B+f[i]=Y

const int SZ = N << 2;
vector<point> v[SZ];
int use[SZ];
auto check(point x, point y, point z) {
  z.first -= y.first, z.second -= y.second;
  y.first -= x.first, y.second -= x.second;
  return (i128)y.first * z.second - (i128)y.second * z.first >= 0;
}
void build(int u) {
  sort(v[u].begin(), v[u].end());
  // puts("build"); for(auto x:v[u])printf("(%lld,%lld) ",x.first,x.second);
  // puts("");
  vector<point> c;
  for (auto p : v[u]) {
    while (c.size() > 0 && c.back() == p ||
           c.size() > 1 && check(c[c.size() - 2], c.back(), p))
      c.pop_back();
    c.pb(p);
  }
  v[u] = c;
  // for(auto x:v[u])printf("(%lld,%lld) ",x.first,x.second); puts("");
}
void add(int pos, point p, int u = 1, int l = 0, int r = n) {
  v[u].pb(p);
  if (l == r) {
    use[u] = 1;
    return;
  }
  int mid = (l + r) >> 1;
  if (pos <= mid)
    add(pos, p, u << 1, l, mid);
  else
    add(pos, p, u << 1 | 1, mid + 1, r);
  if (use[u << 1] && use[u << 1 | 1] && !use[u]) build(u), use[u] = 1;
}
point qry(long long k, int u) {
  if (v[u].empty()) return {0, -(i128)2e18};
  int l = 0, r = v[u].size() - 1;
  while (l < r) {
    int mid = (l + r + 1) >> 1;
    if (v[u][mid - 1].first == v[u][mid].first || ([](point x, point y, long long k) {
          y.first -= x.first, y.second -= x.second;
          return y.second >= (i128)k * y.first;
        })(v[u][mid - 1], v[u][mid], k))
      l = mid;
    else
      r = mid - 1;
  }
  return v[u][l];
}
point qry(int L, int R, long long k, int u = 1, int l = 0, int r = n) {
  if (R < l || r < L || L > R) return {0, -(i128)2e18};
  if (L <= l && r <= R && use[u]) { return qry(k, u); }
  int mid = (l + r) >> 1;
  point x = qry(L, R, k, u << 1, l, mid), y = qry(L, R, k, u << 1 | 1, mid + 1, r);
  if (x.second - (i128)x.first * k > y.second - (i128)y.first * k) return x;
  return y;
}

int main() {
  scanf("%d%lld%lld%lld", &n, &a, &b, &c);
  scanf("%d%d", &L, &R);
  FOR(i, 1, n) scanf("%lld", &s[i]), s[i] += s[i - 1];
  f[0] = 0;
  FOR(i, 1, n) {
    if (i - L >= 0) {
      if (f[i] > -2e18) add(i - L, {X(i - L), Y(i - L)});
      // printf("add %lld %lld\n",X(i-L),Y(i-L));
    }
    auto p = qry(i - R, i - L, K(i));
    // printf("(%lld, %lld)\n",p.first,p.second);
    f[i] = max((i128)(-2e18), p.second - (i128)p.first * K(i) - B(i));
    // printf("f[%d]=%lld\n",i,f[i]);
  }
  printf("%lld\n", (long long)f[n]);
  return 0;
}
