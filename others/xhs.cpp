#include <bits/stdc++.h>
#define rg register
using namespace std;
const int N = 200010;

int n, m, a[N], st1[N], st2[N], tp1, tp2, rt;
int L[N], R[N], M[N], id[N], cnt, typ[N], bin[20], st[N], tp;

char gc() {
  static char *p1, *p2, s[1000000];
  if (p1 == p2) p2 = (p1 = s) + fread(s, 1, 1000000, stdin);
  return (p1 == p2) ? EOF : *p1++;
}
int rd() {
  int x = 0;
  char c = gc();
  while (c < '0' || c > '9') c = gc();
  while (c >= '0' && c <= '9') x = (x << 1) + (x << 3) + c - '0', c = gc();
  return x;
}
char ps[1000000], *pp = ps;
void flush() {
  fwrite(ps, 1, pp - ps, stdout);
  pp = ps;
}
void push(char x) {
  if (pp == ps + 1000000) flush();
  *pp++ = x;
}
void write(int l, int r) {
  static int sta[N], top;
  if (!l)
    push('0');
  else {
    while (l) sta[++top] = l % 10, l /= 10;
    while (top) push(sta[top--] ^ '0');
  }
  push(' ');
  if (!r)
    push('0');
  else {
    while (r) sta[++top] = r % 10, r /= 10;
    while (top) push(sta[top--] ^ '0');
  }
  push('\n');
}

struct RMQ { //预处理RMQ（Max & Min）
  int lg[N], mn[N][17], mx[N][17];
  void chkmn(int &x, int y) {
    if (x > y) x = y;
  }
  void chkmx(int &x, int y) {
    if (x < y) x = y;
  }
  void build() {
    for (int i = bin[0] = 1; i < 20; ++i) bin[i] = bin[i - 1] << 1;
    for (int i = 2; i <= n; ++i) lg[i] = lg[i >> 1] + 1;
    for (int i = 1; i <= n; ++i) mn[i][0] = mx[i][0] = a[i];
    for (int i = 1; i < 17; ++i)
      for (int j = 1; j + bin[i] - 1 <= n; ++j)
        mn[j][i] = min(mn[j][i - 1], mn[j + bin[i - 1]][i - 1]),
        mx[j][i] = max(mx[j][i - 1], mx[j + bin[i - 1]][i - 1]);
  }
  int ask_mn(int l, int r) {
    int t = lg[r - l + 1];
    return min(mn[l][t], mn[r - bin[t] + 1][t]);
  }
  int ask_mx(int l, int r) {
    int t = lg[r - l + 1];
    return max(mx[l][t], mx[r - bin[t] + 1][t]);
  }
} D;
//维护L_i

struct SEG { //线段树
#define ls (k << 1)
#define rs (k << 1 | 1)
  int mn[N << 1], ly[N << 1]; //区间加；区间最小值
  void pushup(int k) { mn[k] = min(mn[ls], mn[rs]); }
  void mfy(int k, int v) { mn[k] += v, ly[k] += v; }
  void pushdown(int k) {
    if (ly[k]) mfy(ls, ly[k]), mfy(rs, ly[k]), ly[k] = 0;
  }
  void update(int k, int l, int r, int x, int y, int v) {
    if (l == x && r == y) {
      mfy(k, v);
      return;
    }
    pushdown(k);
    int mid = (l + r) >> 1;
    if (y <= mid)
      update(ls, l, mid, x, y, v);
    else if (x > mid)
      update(rs, mid + 1, r, x, y, v);
    else
      update(ls, l, mid, x, mid, v), update(rs, mid + 1, r, mid + 1, y, v);
    pushup(k);
  }
  int query(int k, int l, int r) { //询问0的位置
    if (l == r) return l;
    pushdown(k);
    int mid = (l + r) >> 1;
    if (!mn[ls])
      return query(ls, l, mid);
    else
      return query(rs, mid + 1, r);
    //如果不存在0的位置就会自动返回一个极大值
  }
} T;

int o = 1, hd[N], dep[N], fa[N][18];
struct Edge {
  int v, nt;
} E[N << 1];
void add(int u, int v) { //树结构加边
  E[o] = (Edge){v, hd[u]};
  hd[u] = o++;
  // printf("%d %d\n",u,v);
}
void dfs(int u) {
  for (int i = 1; bin[i] <= dep[u]; ++i) fa[u][i] = fa[fa[u][i - 1]][i - 1];
  for (int i = hd[u]; i; i = E[i].nt) {
    int v = E[i].v;
    dep[v] = dep[u] + 1;
    fa[v][0] = u;
    dfs(v);
  }
}
int go(int u, int d) {
  for (int i = 0; i < 18 && d; ++i)
    if (bin[i] & d) d ^= bin[i], u = fa[u][i];
  return u;
}
int lca(int u, int v) {
  if (dep[u] < dep[v]) swap(u, v);
  u = go(u, dep[u] - dep[v]);
  if (u == v) return u;
  for (int i = 17; ~i; --i)
    if (fa[u][i] != fa[v][i]) u = fa[u][i], v = fa[v][i];
  return fa[u][0];
}

//判断当前区间是否为连续段
bool judge(int l, int r) { return D.ask_mx(l, r) - D.ask_mn(l, r) == r - l; }

//建树
void build() {
  for (int i = 1; i <= n; ++i) {
    //单调栈
    //在区间[st1[tp1-1]+1,st1[tp1]]的最小值就是a[st1[tp1]]
    //现在把它出栈，意味着要把多减掉的Min加回来。
    //线段树的叶结点位置j维护的是从j到当前的i的
    // Max{j,i}-Min{j,i}-(i-j)
    //区间加只是一个Tag。
    //维护单调栈的目的是辅助线段树从i-1更新到i。
    //更新到i后，只需要查询全局最小值即可知道是否有解

    while (tp1 && a[i] <= a[st1[tp1]]) //单调递増的栈，维护Min
      T.update(1, 1, n, st1[tp1 - 1] + 1, st1[tp1], a[st1[tp1]]), tp1--;
    while (tp2 && a[i] >= a[st2[tp2]])
      T.update(1, 1, n, st2[tp2 - 1] + 1, st2[tp2], -a[st2[tp2]]), tp2--;

    T.update(1, 1, n, st1[tp1] + 1, i, -a[i]);
    st1[++tp1] = i;
    T.update(1, 1, n, st2[tp2] + 1, i, a[i]);
    st2[++tp2] = i;

    id[i] = ++cnt;
    L[cnt] = R[cnt] = i; //这里的L,R是指值域的上下界
    int le = T.query(1, 1, n), now = cnt;
    while (tp && L[st[tp]] >= le) {
      if (typ[st[tp]] && judge(M[st[tp]], i)) {
        //判断是否能成为儿子，如果能就做
        R[st[tp]] = i, add(st[tp], now), now = st[tp--];
      } else if (judge(L[st[tp]], i)) {
        typ[++cnt] = 1; //合点一定是被这样建出来的
        L[cnt] = L[st[tp]], R[cnt] = i, M[cnt] = L[now];
        add(cnt, st[tp--]), add(cnt, now);
        now = cnt;
      } else {
        add(++cnt, now); //新建一个结点，把now添加为儿子
        //如果从当前结点开始不能构成连续段，就合并。
        //直到找到一个结点能构成连续段。而且我们一定能找到这样
        //一个结点。
        do add(cnt, st[tp--]);
        while (tp && !judge(L[st[tp]], i));
        L[cnt] = L[st[tp]], R[cnt] = i, add(cnt, st[tp--]);
        now = cnt;
      }
    }
    st[++tp] = now; //增量结束，把当前点圧栈

    T.update(1, 1, n, 1, i, -1); //因为区间右端点向后移动一格，因此整体-1
  }

  rt = st[1]; //栈中最后剩下的点是根结点
}
void query(int r, int l) {
  int x = id[l], y = id[r];
  int z = lca(x, y);
  if (typ[z] & 1)
    l = L[go(x, dep[x] - dep[z] - 1)], r = R[go(y, dep[y] - dep[z] - 1)];
  else
    l = L[z], r = R[z];
  write(l, r);
} //分lca为析或和，这里把叶子看成析的

int main() {
  freopen("c.in", "r", stdin);
  freopen("c.out", "w", stdout);
  n = rd();
  for (int i = 1; i <= n; ++i) a[i] = rd();
  D.build();
  build();
  dfs(rt);
  m = rd();
  for (int i = 1; i <= m; ++i) query(rd(), rd());
  return flush(), 0;
}
// 20190612
//析合树
