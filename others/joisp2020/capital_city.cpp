// by Sshwy
//#define DEBUGGER
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

#ifdef DEBUGGER

#define log_prefix()                                                \
  {                                                                 \
    fprintf(stderr, "\033[37mLine %-3d [%dms]:\033[0m  ", __LINE__, \
        (int)clock() / 1000);                                       \
  }
#define ilog(...) \
  { fprintf(stderr, __VA_ARGS__); }
#define llog(...)      \
  {                    \
    log_prefix();      \
    ilog(__VA_ARGS__); \
  }
#define log(...)           \
  {                        \
    log_prefix();          \
    ilog(__VA_ARGS__);     \
    fprintf(stderr, "\n"); \
  }
#define red(...)       \
  {                    \
    log_prefix();      \
    ilog("\033[31m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define green(...)     \
  {                    \
    log_prefix();      \
    ilog("\033[32m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define blue(...)      \
  {                    \
    log_prefix();      \
    ilog("\033[34m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }

#else

#define log_prefix() ;
#define ilog(...) ;
#define llog(...) ;
#define log(...) ;
#define red(...) ;
#define green(...) ;
#define blue(...) ;

#endif

namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 2e5 + 5;
int n, k;
vector<int> g[N], nd[N];
int a[N], Cut[N], sz[N], ms[N], vis[N], fa[N], use[N], Ans = 1e9;

void Size(int u, int p) {
  sz[u] = 1, ms[u] = 0;
  for (int v : g[u])
    if (v != p && !Cut[v]) Size(v, u), sz[u] += sz[v], ms[u] = max(ms[u], sz[v]);
  // printf("sz[%d]=%d\n",u,sz[u]);
}
pair<int, int> Core(int u, int p, int T) {
  pair<int, int> res(u, max(ms[u], T - sz[u]));
  for (int v : g[u])
    if (v != p && !Cut[v]) {
      auto x = Core(v, u, T);
      if (x.se < res.se) res = x;
    }
  return res;
}
queue<int> q;
void dfs(int u, int p, int tag) {
  vis[u] = tag, use[u] = 0, fa[u] = p;
  for (int v : g[u])
    if (v != p && !Cut[v]) dfs(v, u, tag);
}
int counter;
void Calc(int u, int p) {
  Size(u, p);
  int core = Core(u, p, sz[u]).fi;

  bool flag = 1;
  dfs(core, 0, ++counter);
  while (q.size()) q.pop();
  q.push(a[core]);
  set<int> s;
  while (!q.empty()) {
    int col = q.front();
    q.pop();
    s.insert(col);
    for (auto x : nd[col]) {
      if (vis[x] == counter) {
        for (int v = x; v; v = fa[v]) {
          if (use[v]) break;
          use[v] = 1;
          if (s.count(a[v])) continue;
          s.insert(a[v]);
          q.push(a[v]);
        }
      } else {
        flag = 0;
        break;
      }
    }
    if (flag == 0) break;
  }
  if (flag) Ans = min(Ans, int(s.size()) - 1);

  Cut[core] = 1;
  for (int v : g[core])
    if (!Cut[v]) Calc(v, core);
}
int main() {
  scanf("%d%d", &n, &k);
  FOR(i, 1, n - 1) {
    int u, v;
    scanf("%d%d", &u, &v);
    g[u].pb(v);
    g[v].pb(u);
  }
  FOR(i, 1, n) scanf("%d", &a[i]), nd[a[i]].pb(i);
  Calc(1, 0);
  printf("%d\n", Ans);
  return 0;
}
