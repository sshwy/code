// by Sshwy
//#define DEBUGGER
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

#ifdef DEBUGGER

#define log_prefix()                                                \
  {                                                                 \
    fprintf(stderr, "\033[37mLine %-3d [%dms]:\033[0m  ", __LINE__, \
        (int)clock() / 1000);                                       \
  }
#define ilog(...) \
  { fprintf(stderr, __VA_ARGS__); }
#define llog(...)      \
  {                    \
    log_prefix();      \
    ilog(__VA_ARGS__); \
  }
#define log(...)           \
  {                        \
    log_prefix();          \
    ilog(__VA_ARGS__);     \
    fprintf(stderr, "\n"); \
  }
#define red(...)       \
  {                    \
    log_prefix();      \
    ilog("\033[31m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define green(...)     \
  {                    \
    log_prefix();      \
    ilog("\033[32m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define blue(...)      \
  {                    \
    log_prefix();      \
    ilog("\033[34m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }

#else

#define log_prefix() ;
#define ilog(...) ;
#define llog(...) ;
#define log(...) ;
#define red(...) ;
#define green(...) ;
#define blue(...) ;

#endif

namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 2e5 + 5, M = 5e5 + 5;

int n, a[N], m, tmp[N];
struct atom {
  int x, y, c;
} b[M];

struct node {
  int l, r, h;
  bool operator!=(const node x) const {
    if (l == x.l && r == x.r && h == x.h) return 0;
    return 1;
  }
  bool operator<(const node x) const {
    return l != x.l ? l < x.l : r != x.r ? r < x.r : h < x.h;
  }
};

int mn[20][N];

inline pair<int, int> left_most(int pos, int y) {
  int v = a[pos];
  ROF(j, 18, 0) {
    int npos = pos - (1 << j);
    if (npos >= 1 && mn[j][npos] >= y) v = min(v, mn[j][npos]), pos = npos;
  }
  return {pos, v};
}
inline pair<int, int> right_most(int pos, int y) {
  int v = a[pos];
  ROF(j, 18, 0) {
    int npos = pos + (1 << j);
    if (npos <= n && mn[j][pos + 1] >= y) v = min(v, mn[j][pos + 1]), pos = npos;
  }
  return {pos, v};
}
node get_node(int x, int y) {
  auto L = left_most(x, y), R = right_most(x, y);
  return {L.fi, R.fi, min(L.se, R.se)};
}
node get_fa(node u) {
  if (u.l == 1 && u.r == n) return u;
  return get_node(u.l, max(a[u.l - 1], a[u.r + 1]));
}
map<pair<int, int>, long long> ans;

struct data {
  int l, r;
  long long v;
  int h;
  bool operator<(const data d) const { return h < d.h; }
};
priority_queue<data> q;

long long sm[N];
void add(int pos, long long v) {
  for (int i = pos; i < N; i += i & -i) sm[i] += v;
}
void add(int L, int R, long long v) {
  if (L > R) return;
  add(L, v);
  add(R + 1, -v);
}
long long query(int pos) {
  long long res = 0;
  for (int i = pos; i > 0; i -= i & -i) res += sm[i];
  return res;
}

signed main() {
  scanf("%d", &n);
  FOR(i, 1, n) scanf("%d", &a[i]);
  scanf("%d", &m);
  FOR(i, 1, m) scanf("%d%d%d", &b[i].x, &b[i].y, &b[i].c);

  FOR(i, 1, n) a[i] = n + 1 - a[i];
  FOR(i, 1, m) b[i].y = n + 2 - b[i].y;

  FOR(i, 1, m) if (a[b[i].x] == b[i].y) tmp[b[i].x] = 1;
  FOR(i, 1, n) if (tmp[i] == 0) b[++m] = {i, a[i], 0};

  FOR(i, 1, n) mn[0][i] = a[i];
  FOR(j, 1, 19) {
    FOR(i, 1, n - (1 << j) + 1) {
      mn[j][i] = min(mn[j - 1][i], mn[j - 1][i + (1 << j - 1)]);
    }
  }

  sort(b + 1, b + m + 1, [](atom A, atom B) { return A.y > B.y; });
  // FOR(i,1,m)printf("(%lld,%lld,%lld)\n",b[i].x,b[i].y,b[i].c);

  FOR(i, 1, m) {
    atom cur = b[i];

    while (q.size() && q.top().h >= cur.y) {
      auto x = q.top();
      q.pop();
      add(x.l, x.r, x.v);
    }

    node u = get_node(cur.x, cur.y);
    long long s = query(cur.x) + cur.c; // query: [u.l,y.r]内，去掉cur.x的链的答案
    if (!ans.count({u.l, u.r})) {
      ans[{u.l, u.r}] = s;
      node fu = get_fa(u);
      if (fu != u) {
        q.push({fu.l, u.l - 1, s, fu.h});
        q.push({u.r + 1, fu.r, s, fu.h});
      }
    } else {
      long long &ansu = ans[{u.l, u.r}];
      if (ansu < s) {
        node fu = get_fa(u);
        if (fu != u) {
          q.push({fu.l, u.l - 1, -ansu, fu.h});
          q.push({fu.l, u.l - 1, s, fu.h});
          q.push({u.r + 1, fu.r, -ansu, fu.h});
          q.push({u.r + 1, fu.r, s, fu.h});
        }
        ansu = s;
      }
    }
  }
  long long tot = 0;
  FOR(i, 1, m) tot += b[i].c;
  auto root = get_node(1, 1);
  tot -= ans[{root.l, root.r}];
  printf("%lld\n", tot);
  return 0;
}
