// by Sshwy
//#define DEBUGGER
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

#ifdef DEBUGGER

#define log_prefix()                                                \
  {                                                                 \
    fprintf(stderr, "\033[37mLine %-3d [%dms]:\033[0m  ", __LINE__, \
        (int)clock() / 1000);                                       \
  }
#define ilog(...) \
  { fprintf(stderr, __VA_ARGS__); }
#define llog(...)      \
  {                    \
    log_prefix();      \
    ilog(__VA_ARGS__); \
  }
#define log(...)           \
  {                        \
    log_prefix();          \
    ilog(__VA_ARGS__);     \
    fprintf(stderr, "\n"); \
  }
#define red(...)       \
  {                    \
    log_prefix();      \
    ilog("\033[31m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define green(...)     \
  {                    \
    log_prefix();      \
    ilog("\033[32m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define blue(...)      \
  {                    \
    log_prefix();      \
    ilog("\033[34m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }

#else

#define log_prefix() ;
#define ilog(...) ;
#define llog(...) ;
#define log(...) ;
#define red(...) ;
#define green(...) ;
#define blue(...) ;

#endif

namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 505;
int n, m;
int use[N][N];
char a[N][N];

bool check(int x1, int y1, int x2, int y2, int x3, int y3) {
  if (x1 < 1 || x1 > n) return 0;
  if (x2 < 1 || x2 > n) return 0;
  if (x3 < 1 || x3 > n) return 0;
  if (y1 < 1 || y1 > m) return 0;
  if (y2 < 1 || y2 > m) return 0;
  if (y3 < 1 || y3 > m) return 0;
  if (use[x1][y1] || use[x2][y2] || use[x3][y3]) return 0;
  if (a[x2][y2] != 'W') return 0;
  if (a[x1][y1] == 'W' || a[x3][y3] == 'W') return 0;
  if (a[x1][y1] == a[x3][y3]) return 0;
  return 1;
}
void trymake(int x1, int y1, int x2, int y2, int x3, int y3, char c) {
  if (check(x1, y1, x2, y2, x3, y3)) {
    use[x1][y1] = use[x2][y2] = use[x3][y3] = 1;
    a[x2][y2] = c;
  }
}
void make(int x, int y) {
  trymake(x + 1, y - 1, x, y, x - 1, y + 1, '/');
  trymake(x - 1, y - 1, x, y, x + 1, y + 1, '\\');
  trymake(x, y - 1, x, y, x, y + 1, '-');
  trymake(x - 1, y, x, y, x + 1, y, '|');
}
int main() {
  srand(clock() + time(0));
  scanf("%d%d", &n, &m);
  FOR(i, 1, n) scanf("%s", a[i] + 1);
  vector<pair<int, int>> v;
  FOR(i, 1, n) FOR(j, 1, m) v.pb({i, j});
  // random_shuffle(v.begin(),v.end());
  for (auto x : v) make(x.fi, x.se);
  FOR(i, 1, n) printf("%s\n", a[i] + 1);
  return 0;
}
