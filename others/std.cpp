#include <algorithm>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iomanip>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <stack>
#include <vector>
#define rep(i, x, y) for (register int i = (x); i <= (y); ++i)
#define dwn(i, x, y) for (register int i = (x); i >= (y); --i)
#define view(u, k) for (int k = fir[u]; ~k; k = nxt[k])
#define LL long long
#define maxn 62503
#define LD long double
#define ls (u << 1)
#define rs (u << 1 | 1)
#define mi (l + r >> 1)
#define inf 9223372036854775807ll
using namespace std;
int read() {
  int x = 0, f = 1;
  char ch = getchar();
  while (!isdigit(ch) && ch != '-') ch = getchar();
  if (ch == '-') f = -1, ch = getchar();
  while (isdigit(ch)) x = (x << 1) + (x << 3) + ch - '0', ch = getchar();
  return x * f;
}
void write(LL x) {
  if (x == 0) {
    putchar('0'), putchar('\n');
    return;
  }
  int f = 0;
  char ch[20];
  if (x < 0) putchar('-'), x = -x;
  while (x) ch[++f] = x % 10 + '0', x /= 10;
  while (f) putchar(ch[f--]);
  putchar('\n');
  return;
}
vector<int> q[maxn << 2];
int tq[maxn], tmp[maxn], hd, tl, n, aa, bb, cc, ll, rr, vis[maxn << 2];
LL f[maxn], a[maxn];
inline LL getans(int i, int j) {
  return (LL)aa * (a[i] - a[j]) * (a[i] - a[j]) + (LL)bb * (a[i] - a[j]) + (LL)cc +
         f[j];
}
inline LL gx(int i) { return a[i]; }
inline LL gy(int i) { return f[i] + (LL)aa * a[i] * a[i] - (LL)bb * a[i]; }
bool cmp(int x, int y) { return a[x] < a[y]; }
int jud(int p1, int p2, int p3) {
  LL x1 = gx(p1), x2 = gx(p2), x3 = gx(p3), y1 = gy(p1), y2 = gy(p2), y3 = gy(p3);
  return ((LD)(y2 - y1) * (x3 - x2) <= (LD)(y3 - y2) * (x2 - x1)) ? 1 : 0;
}
void inq(int i) {
  while (hd < tl && jud(tq[tl - 1], tq[tl], i)) tl--;
  tq[++tl] = i;
}
void add(int u, int l, int r, int x) {
  if (x == r) {
    hd = 1, tl = 0;
    int cnt = 0;
    vis[u] = 1;
    rep(i, l, r) if (f[i] != -inf) tmp[++cnt] = i;
    //	rep(i,1,cnt)cout<<gx(tmp[i])<<" ";cout<<endl;
    sort(tmp + 1, tmp + cnt + 1, cmp);
    //	rep(i,1,cnt)cout<<gx(tmp[i])<<" ";cout<<endl;
    rep(i, 1, cnt) inq(tmp[i]);
    rep(i, hd, tl) q[u].push_back(tq[i]);
    // cout<<u<<":";
    // rep(i,hd,tl)cout<<tq[i]<<" ";cout<<endl;
    // rep(i,1,cnt)cout<<tmp[i]<<" ";cout<<endl;
    // rep(i,1,cnt-1)cout<<(gy(tmp[i])-gy(tmp[i+1]))/(gx(tmp[i])-gx(tmp[i+1]))<<"
    // ";cout<<endl;
  }
  if (l == r) return;
  if (x <= mi)
    add(ls, l, mi, x);
  else
    add(rs, mi + 1, r, x);
}
void push(int x) { add(1, 0, n - 1, x); }
LL getmax(int u, int i) {
  // cout<<q[u].size()<<" "<<i<<" "<<u<<endl;
  if (!q[u].size()) return -inf;
  int L = 1, R = q[u].size() - 1;
  LL res = getans(i, q[u][0]);
  while (L <= R) {
    int mid = (L + R >> 1);
    res = max(res, getans(i, q[u][mid]));
    if (getans(i, q[u][mid]) > getans(i, q[u][mid - 1]))
      L = mid + 1;
    else
      R = mid - 1;
  } // cout<<res<<" "<<u<<" "<<i<<endl;
  // cout<<u<<":"<<q[u].size()<<endl;
  // rep(j,0,(int)q[u].size()-3){cout<<j<<"
  // "<<(int)q[u].size()-3;cout<<jud(q[u][j],q[u][j+1],q[u][j+2])<<endl;}
  // rep(j,0,(int)q[u].size()-2){cout<<(gy(q[u][j+1])-gy(q[u][j]))/(gx(q[u][j+1])-gx(q[u][j]))<<endl;}
  // rep(j,0,q[u].size()-1)cout<<q[u][j]<<"
  // "<<getans(i,q[u][j])<<endl,res=max(res,getans(i,q[u][j]));
  return res;
}
LL ask(int u, int l, int r, int L, int R, int x) {
  // cout<<u<<" "<<L<<" "<<R<<" "<<l<<" "<<r<<endl;
  if (L > r || R < l) return -inf;
  if (L <= l && r <= R) { /*cout<<x<<" "<<l<<" "<<r<<endl;*/
    return getmax(u, x);
  }
  LL res = -inf;
  if (L <= mi) res = ask(ls, l, mi, L, R, x);
  if (R > mi) res = max(res, ask(rs, mi + 1, r, L, R, x));
  return res;
}
int main() {
  // freopen(".in","r",stdin);
  // freopen(".out","w",stdout);
  n = read(), aa = read(), bb = read(), cc = read(), ll = read(), rr = read();
  rep(i, 1, n) a[i] = a[i - 1] + read();
  ll = max(1, ll), rr = min(n, rr);
  rep(i, 1, n) {
    push(i - 1);
    // cout<<i<<endl;
    f[i] = ask(1, 0, n - 1, i - rr, i - ll, i);
    // cout<<f[i]<<endl;
    // write(f[i]);
  }
  write(f[n]);
  return (0 ^ 0);
}
/*
10 -1 21629 19297
5 9
2363 1470 3189 -5015 -3546 -2155 2251 1613 3783 4558

*/
/*
10 -6 11986 26398
1 5
-4685 914 -335 4465 2585 4405 -1414 -67 -2910 2484

*/
