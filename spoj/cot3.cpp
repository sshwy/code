#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 1 << 23, MAXN = 1 << 17;
struct qxx {
  int nex, t;
};
qxx e[MAXN * 2];
int h[MAXN], le = 1;
void add_path(int f, int t) { e[++le] = (qxx){h[f], t}, h[f] = le; }
#define FORe(i, u, v) for (int i = h[u], v; v = e[i].t, i; i = e[i].nex)

int n;
int c[N];

int lb;
int lc[N], rc[N], sz[N], tag[N], dep[N]; // tag:异或的值,dep:深度。叶子的深度为0

int seg[N];        //结点i的线段树根结点编号
int sg[N], sum[N]; // sg值

void pushdown(int u, int l, int r) {
  if (tag[u] == 0) return;
  if (tag[u] >> (dep[u] - 1) & 1) swap(lc[u], rc[u]);
  if (lc[u]) tag[lc[u]] ^= tag[u];
  if (rc[u]) tag[rc[u]] ^= tag[u];
  tag[u] = 0;
}
void pushup(int u) {
  sz[u] = sz[lc[u]] + sz[rc[u]];
  dep[u] = max(dep[lc[u]], dep[rc[u]]) + 1;
}
void insert(int u, const int &val, int l = 0, int r = MAXN - 1) {
  if (l == r) return sz[u] = 1, void();
  int mid = (l + r) >> 1;
  pushdown(u, l, r);
  if (val <= mid)
    insert(lc[u] ? lc[u] : lc[u] = ++lb, val, l, mid);
  else
    insert(rc[u] ? rc[u] : rc[u] = ++lb, val, mid + 1, r);
  pushup(u);
}
void xor_all(int u, const int &val, int l = 0, int r = MAXN - 1) { tag[u] ^= val; }
void merge(int x, int y, int l = 0, int r = MAXN - 1) { // merge y to x
  if (l == r) return sz[x] |= sz[y], void();
  int mid = (l + r) >> 1;
  pushdown(x, l, r), pushdown(y, l, r);
  if (!lc[x])
    lc[x] = lc[y];
  else if (lc[y])
    merge(lc[x], lc[y], l, mid);
  if (!rc[x])
    rc[x] = rc[y];
  else if (rc[y])
    merge(rc[x], rc[y], mid + 1, r);
  pushup(x);
}
int mex(int u, int l = 0, int r = MAXN - 1) {
  if (u == 0) return l; //一个空的结点
  if (l == r) return l;
  int mid = (l + r) >> 1;
  pushdown(u, l, r);
  if (sz[lc[u]] < mid - l + 1)
    return mex(lc[u], l, mid);
  else
    return mex(rc[u], mid + 1, r);
}
void dfs(int u, int p) {
  seg[u] = ++lb;
  FORe(i, u, v) if (v != p) dfs(v, u), sum[u] ^= sg[v];
  if (c[u] == 0) {
    insert(seg[u], sum[u]); //插入
  }
  FORe(i, u, v) if (v != p) xor_all(seg[v], sum[u] ^ sg[v]), merge(seg[u], seg[v]);
  sg[u] = mex(seg[u]);
}
int ans[N], la;
void get_ans(int u, int p, int s) {
  if (c[u] == 0 && s == 0) ans[++la] = u;
  FORe(i, u, v) if (v != p) get_ans(v, u, s ^ sg[v] ^ sum[v]);
}
int main() {
  scanf("%d", &n);
  FOR(i, 1, n) scanf("%d", &c[i]);
  FOR(i, 1, n - 1) {
    int a, b;
    scanf("%d%d", &a, &b);
    add_path(a, b), add_path(b, a);
  }
  dfs(1, 0);
  if (sg[1] == 0) return puts("-1"), 0;
  get_ans(1, 0, sum[1]);
  sort(ans + 1, ans + la + 1);
  FOR(i, 1, la) printf("%d\n", ans[i]);
  return 0;
}
