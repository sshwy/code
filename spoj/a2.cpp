#include <bits/stdc++.h>
using namespace std;
#define FOR(i, n) for (int i = 0; i < n; ++i)

const int MAXV = 100010;
const int MAXE = MAXV << 1;

const int white = 0;
int max_log;

int head[MAXV], color[MAXV], ecnt;
int to[MAXE], nxt[MAXE];
int n;

void initGraph() {
  memset(head + 1, -1, n * sizeof(int));
  ecnt = 0;
}

void add_edge(int u, int v) {
  to[ecnt] = v;
  nxt[ecnt] = head[u];
  head[u] = ecnt++;
  to[ecnt] = u;
  nxt[ecnt] = head[v];
  head[v] = ecnt++;
}

struct Node {
  Node *go[2];
  int size, txor, mex;
};
Node statePool[20 * MAXV];
Node *nil, *leaf;
Node *stk[20 * MAXV];
int ncnt, top;

Node *new_node() {
  Node *t = top ? stk[--top] : &statePool[ncnt++];
  FOR(i, 2) t->go[i] = nil;
  t->size = t->txor = t->mex = 0;
  return t;
}

void remove(Node *t) { stk[top++] = t; }

void initTree() {
  nil = statePool;
  FOR(i, 2) nil->go[i] = nil;
  ncnt = 1;
  leaf = new_node();
  leaf->mex = leaf->size = 1;
}

void pushdown(Node *t, int k) {
  if (k > 0) {
    if ((t->txor >> (k - 1)) & 1) swap(t->go[0], t->go[1]);
    FOR(i, 2) t->go[i]->txor ^= t->txor;
    t->txor = 0;
  }
}

void update(Node *t, int k) {
  if (k > 0) {
    int size = 1 << (k - 1);
    t->mex = (t->go[0]->size < size ? t->go[0]->mex : size + t->go[1]->mex);
    t->size = t->go[0]->size + t->go[1]->size;
  }
}

Node *merge(Node *a, Node *b, int k) {
  if (a == nil) return b;
  if (b == nil) return a;
  if (a == leaf && b == leaf) return leaf;

  Node *res = new_node();
  pushdown(a, k), pushdown(b, k);
  FOR(i, 2) res->go[i] = merge(a->go[i], b->go[i], k - 1);
  update(res, k);
  remove(a), remove(b);
  return res;
}

void insert(Node *&t, int k, int val) {
  if (k == 0)
    t = leaf;
  else {
    if (t == nil) t = new_node();
    pushdown(t, k);
    insert(t->go[(val >> (k - 1)) & 1], k - 1, val);
    update(t, k);
  }
}

Node *root[MAXV];
int dp[MAXV];

void dfs(int u, int f) {
  int tmp = 0;
  for (int p = head[u]; ~p; p = nxt[p]) {
    int v = to[p];
    if (v != f) dfs(v, u), tmp ^= dp[v];
  }
  if (color[u] == white) insert(root[u], max_log, tmp);
  for (int p = head[u]; ~p; p = nxt[p]) {
    int v = to[p];
    if (v == f) continue;
    root[v]->txor ^= tmp ^ dp[v];
    root[u] = merge(root[u], root[v], max_log);
  }
  dp[u] = root[u]->mex;
  // printf("dp[%d]=%d\n",u,dp[u]);
}

vector<int> ans;

void dfs_ans(int u, int f, int sg) {
  int tmp = 0;
  for (int p = head[u]; ~p; p = nxt[p]) {
    int v = to[p];
    if (v != f) tmp ^= dp[v];
  }
  if (color[u] == white && (sg ^ tmp) == 0) ans.push_back(u);
  // printf("m[%d]=%d\n",u,tmp^sg);
  for (int p = head[u]; ~p; p = nxt[p]) {
    int v = to[p];
    if (v != f) dfs_ans(v, u, sg ^ tmp ^ dp[v]);
  }
}

int main() {
  scanf("%d", &n);
  for (int i = 1; i <= n; ++i) scanf("%d", &color[i]);
  initGraph();
  for (int i = 1, u, v; i < n; ++i) {
    scanf("%d%d", &u, &v);
    add_edge(u, v);
  }
  initTree();
  while ((1 << max_log) <= n) ++max_log;

  for (int i = 1; i <= n; ++i) root[i] = nil;
  dfs(1, 0);
  dfs_ans(1, 0, 0);

  sort(ans.begin(), ans.end());
  for (int x : ans) printf("%d\n", x);
  if (ans.size() == 0) puts("-1");
  return 0;
}
