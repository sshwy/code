// by Sshwy
#pragma GCC optimize(2)
#pragma GCC optimize(3)
#pragma GCC optimize("Ofast")
#pragma GCC optimize("inline")
#pragma GCC optimize("-fgcse")
#pragma GCC optimize("-fgcse-lm")
#pragma GCC optimize("-fipa-sra")
#pragma GCC optimize("-ftree-pre")
#pragma GCC optimize("-ftree-vrp")
#pragma GCC optimize("-fpeephole2")
#pragma GCC optimize("-ffast-math")
#pragma GCC optimize("-fsched-spec")
#pragma GCC optimize("unroll-loops")
#pragma GCC optimize("-falign-jumps")
#pragma GCC optimize("-falign-loops")
#pragma GCC optimize("-falign-labels")
#pragma GCC optimize("-fdevirtualize")
#pragma GCC optimize("-fcaller-saves")
#pragma GCC optimize("-fcrossjumping")
#pragma GCC optimize("-fthread-jumps")
#pragma GCC optimize("-funroll-loops")
#pragma GCC optimize("-fwhole-program")
#pragma GCC optimize("-freorder-blocks")
#pragma GCC optimize("-fschedule-insns")
#pragma GCC optimize("inline-functions")
#pragma GCC optimize("-ftree-tail-merge")
#pragma GCC optimize("-fschedule-insns2")
#pragma GCC optimize("-fstrict-aliasing")
#pragma GCC optimize("-fstrict-overflow")
#pragma GCC optimize("-falign-functions")
#pragma GCC optimize("-fcse-skip-blocks")
#pragma GCC optimize("-fcse-follow-jumps")
#pragma GCC optimize("-fsched-interblock")
#pragma GCC optimize("-fpartial-inlining")
#pragma GCC optimize("no-stack-protector")
#pragma GCC optimize("-freorder-functions")
#pragma GCC optimize("-findirect-inlining")
#pragma GCC optimize("-fhoist-adjacent-loads")
#pragma GCC optimize("-frerun-cse-after-loop")
#pragma GCC optimize("inline-small-functions")
#pragma GCC optimize("-finline-small-functions")
#pragma GCC optimize("-ftree-switch-conversion")
#pragma GCC optimize("-foptimize-sibling-calls")
#pragma GCC optimize("-fexpensive-optimizations")
#pragma GCC optimize("-funsafe-loop-optimizations")
#pragma GCC optimize("inline-functions-called-once")
#pragma GCC optimize("-fdelete-null-pointer-checks")
//#define DEBUGGER
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

#ifdef DEBUGGER

#define log_prefix()                                                \
  {                                                                 \
    fprintf(stderr, "\033[37mLine %-3d [%dms]:\033[0m  ", __LINE__, \
        (int)clock() / 1000);                                       \
  }
#define ilog(...) \
  { fprintf(stderr, __VA_ARGS__); }
#define llog(...)      \
  {                    \
    log_prefix();      \
    ilog(__VA_ARGS__); \
  }
#define log(...)           \
  {                        \
    log_prefix();          \
    ilog(__VA_ARGS__);     \
    fprintf(stderr, "\n"); \
  }
#define red(...)       \
  {                    \
    log_prefix();      \
    ilog("\033[31m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define green(...)     \
  {                    \
    log_prefix();      \
    ilog("\033[32m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define blue(...)      \
  {                    \
    log_prefix();      \
    ilog("\033[34m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }

#else

#define log_prefix() ;
#define ilog(...) ;
#define llog(...) ;
#define log(...) ;
#define red(...) ;
#define green(...) ;
#define blue(...) ;

#endif

namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
namespace IO { // require C++11
  const int _BS = 200000;
  char _ob[_BS], *_z = _ob, _ib[_BS], *_x = _ib, *_y = _ib;
  // Input
  char _nc() {
    return _x == _y && (_y = (_x = _ib) + fread(_ib, 1, _BS, stdin), _x == _y)
               ? EOF
               : *_x++;
  }
  inline void rd(char &c) { c = _nc(); }
  void rd(char *s) {
    char c = _nc();
    while (!isblank(c) && isprint(c) && c != EOF) *s++ = c, c = _nc();
    *s = 0;
  }
  template <class T> void rd(T &res) { // unsigned/signed int/long long
    res = 0;
    char c = _nc(), flag = 0;
    while (!isdigit(c) && c != '-') c = _nc();
    if (c == '-') flag = 1, c = _nc();
    while (isdigit(c)) res = res * 10 + c - '0', c = _nc();
    if (flag) res = -res;
  }
  template <class T, class... A> void rd(T &hd, A &...rs) { rd(hd), rd(rs...); }
  // Output
  inline void flush() { fwrite(_ob, 1, _z - _ob, stdout), _z = _ob; }
  inline void wrch(char x) {
    if (_z == _ob + _BS - 1) flush();
    *_z = x, ++_z;
  }
  void wr(char *s) {
    while (*s) wrch(*s), ++s;
  }
  template <class T> void wr(T x) { // unsigned/signed int/long long
    if (x == 0) return wrch('0'), void();
    if (x < 0) wrch('-'), x = -x;
    int _t[30], _lt = 0;
    while (x) _t[++_lt] = x % 10, x /= 10;
    while (_lt > 0) wrch(_t[_lt] + '0'), --_lt;
  }
  inline void wr(char x) { wrch(x); }
  template <class T, class... A> void wr(T hd, A... rs) { wr(hd), wr(rs...); }
  template <class... A> void wrln(A... ar) { wr(ar...), wrch('\n'); }
  struct _Flusher {
    ~_Flusher() { flush(); }
  } _flusher;
} // namespace IO
const int N = 5005;

int n;
int lu[N][N], rd[N][N];
int fa[N], w[N], nd[N];
char a[N][N];

int get(int u) { return fa[u] == u ? u : fa[u] = get(fa[u]); }
void merge(int u, int v) {
  u = get(u), v = get(v);
  if (u == v) return;
  if (w[u] < w[v]) {
    fa[u] = v;
    w[v] += w[u];
  } else {
    fa[v] = u;
    w[u] += w[v];
    nd[u] = nd[v];
  }
}

int main() {
  IO::rd(n);
  FOR(i, 1, n) {
    IO::rd(a[i] + 1);
    FOR(j, 1, n) a[i][j] -= '0';
  }

  memset(lu, 0x3f, sizeof(lu));
  memset(rd, 0x3f, sizeof(rd));

  FOR(i, 1, n) { // L
    int cur = 0;
    FOR(j, 1, n) cur = a[i][j] ? 0 : cur + 1, lu[i][j] = min(lu[i][j], cur);
  }
  FOR(j, 1, n) { // U
    int cur = 0;
    FOR(i, 1, n) cur = a[i][j] ? 0 : cur + 1, lu[i][j] = min(lu[i][j], cur);
  }
  FOR(i, 1, n) { // R
    int cur = 0;
    ROF(j, n, 1) cur = a[i][j] ? 0 : cur + 1, rd[i][j] = min(rd[i][j], cur);
  }
  FOR(j, 1, n) { // D
    int cur = 0;
    ROF(i, n, 1) cur = a[i][j] ? 0 : cur + 1, rd[i][j] = min(rd[i][j], cur);
  }
  int ans = 0;

  FOR(d, 1 - n, n - 1) {
    int L = max(1, 1 - d), R = min(n, n - d);
    FOR(i, L, R + 1) fa[i] = i, w[i] = 1, nd[i] = i;
    FOR(y, L, R) {
      int x = nd[get(y - (lu[y][y + d] - 1))];
      while (x + (rd[x][x + d] - 1) < y) merge(x, x + 1), x = nd[get(x)];
      ans = max(ans, y - x + 1);
    }
  }
  // FOR(i,1,n)FOR(j,1,n)v[i-j+n].pb({lu[i][j]-1,rd[i][j]-1});

  IO::wrln(ans * ans);
  return 0;
}

// 问题转化为
// 给两个序列，L[i],R[i]
// 找到 x,y(x<y) 使得x+R[x]>=y, y-L[y]<=x, 最大化y-x.
// 枚举 y。那么我们维护满足x+R[x]>=y的x，找到其中满足y-L[y]<=x的最小的x即可。
