// by Sshwy
//#define DEBUGGER
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

#ifdef DEBUGGER

#define log_prefix()                                                \
  {                                                                 \
    fprintf(stderr, "\033[37mLine %-3d [%dms]:\033[0m  ", __LINE__, \
        (int)clock() / 1000);                                       \
  }
#define ilog(...) \
  { fprintf(stderr, __VA_ARGS__); }
#define llog(...)      \
  {                    \
    log_prefix();      \
    ilog(__VA_ARGS__); \
  }
#define log(...)           \
  {                        \
    log_prefix();          \
    ilog(__VA_ARGS__);     \
    fprintf(stderr, "\n"); \
  }
#define red(...)       \
  {                    \
    log_prefix();      \
    ilog("\033[31m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define green(...)     \
  {                    \
    log_prefix();      \
    ilog("\033[32m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define blue(...)      \
  {                    \
    log_prefix();      \
    ilog("\033[34m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }

#else

#define log_prefix() ;
#define ilog(...) ;
#define llog(...) ;
#define log(...) ;
#define red(...) ;
#define green(...) ;
#define blue(...) ;

#endif

namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 4e5 + 5, ALP = 26, SZ = N * 80;

int n, m, typ, tr[N][ALP], val[N], len[N], fail[N], nd[N], fa[N], D[N], Tot, lc[SZ],
    rc[SZ], sz[SZ];
long long va[SZ], sum[SZ];

namespace treap {
  int cp(int u) {
    int u2 = ++Tot;
    assert(Tot < SZ);
    lc[u2] = lc[u], rc[u2] = rc[u], va[u2] = va[u], sz[u2] = sz[u], sum[u2] = sum[u];
    // printf("rand: %d\n",rnd[u2]);
    return u2;
  }
  void pushup(int u) {
    assert(sum[0] == 0);
    assert(sz[0] == 0);
    sum[u] = sum[lc[u]] + sum[rc[u]] + va[u];
    sz[u] = sz[lc[u]] + sz[rc[u]] + 1;
  }
  void split(int u, int k, int &x, int &y) { //按照sz==k
    if (!u) return x = y = 0, void();
    if (k == 0) return x = 0, y = u, void();
    int nu = cp(u);
    if (sz[lc[u]] < k)
      x = nu, split(rc[x], k - sz[lc[u]] - 1, rc[x], y);
    else
      y = nu, split(lc[y], k, x, lc[y]);
    pushup(nu);
  }
  int merge(int x, int y) {
    //两棵树合并。保证不相交。
    //返回修改后的数据结构
    if (!x || !y) return x + y;
    int nx = cp(x), ny = cp(y);
    if (rand() % (sz[nx] + sz[ny]) < sz[nx])
      return rc[nx] = merge(rc[nx], y), pushup(nx), nx;
    else
      return lc[ny] = merge(x, lc[ny]), pushup(ny), ny;
  }
  int add_first(int u, long long val) {
    //在1位置加上一个数v
    //返回修改后的数据结构
    if (!u) {
      int nu = ++Tot;
      assert(Tot < SZ);
      va[nu] = val, sz[nu] = 1;
      pushup(nu);
      return nu;
    }
    int x, y;
    split(u, 1, x, y);
    va[x] += val;
    pushup(x);
    int res = treap::merge(x, y);
    return res;
  }
  long long query(int u, int S) {
    int x, y;
    // int TTT=Tot;
    split(u, sz[u] - S, x, y);
    // printf("delta %d\n",Tot-TTT);
    return sum[y];
  }
} // namespace treap
int tot;

void insert(char *s, int v, int u = 0) {
  if (!*s) return val[u] += v, void();
  if (!tr[u][*s - 'a']) tr[u][*s - 'a'] = ++tot, fa[tot] = u, len[tot] = len[u] + 1;
  insert(s + 1, v, tr[u][*s - 'a']);
}
queue<int> q;
void build() {
  fa[0] = -1;
  FOR(i, 0, ALP - 1) if (tr[0][i]) q.push(tr[0][i]);
  D[0] = treap::add_first(D[0], 0);
  len[0] = 1;
  while (!q.empty()) {
    int u = q.front();
    q.pop();
    //求D[u]
    int x1 = D[fail[u]];
    int x2, y2;
    treap::split(D[fa[u]], len[u] - len[fail[u]], x2, y2);
    D[u] = treap::merge(x2, x1);

    D[u] = treap::add_first(D[u], val[u]); // D[u][1] += val[u]

    assert(sz[D[u]] == len[u]);

    FOR(i, 0, ALP - 1) {
      if (tr[u][i])
        fail[tr[u][i]] = tr[fail[u]][i], q.push(tr[u][i]);
      else
        tr[u][i] = tr[fail[u]][i];
    }
  }
}

char s[N], t[N];

int main() {
  srand(clock() + time(0));
  scanf("%d%d%d", &n, &m, &typ);
  scanf("%s", s + 1);
  FOR(i, 1, m) {
    int x;
    scanf("%s%d", t, &x);
    insert(t, x);
  }
  insert(s + 1, 0);

  // puts("GG");
  build();
  // puts("GG");
  for (int i = 1, u = 0; i <= n; i++) {
    u = tr[u][s[i] - 'a'];
    assert(u);
    nd[i] = u;
  }

  long long las = 0, q;
  scanf("%lld", &q);
  FOR(i, 1, q) {
    long long l, r;
    scanf("%lld%lld", &l, &r);
    l = l ^ (las * typ);
    r = r ^ (las * typ);
    las = treap::query(D[nd[r]], r - l + 1);
    printf("%lld\n", las);
  }

  return 0;
}

// AC 自动机 review：
//     AC自动机每个结点（状态）表示的是一个模式串的前缀。
//     fail指针指向最长的后缀状态。
//
// 首先，字符串去重。这个可以在 build 阿城的时候做。
// 然后我们用数据结构维护串 S 中的匹配权值状态，记作 D(S)。
// D(S)[i]表示S中以S[i]开头的匹配串的权值和。（1<=i<=|S|）。
// 然后我们求D(S)的区间和即可。
