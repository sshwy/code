// by Sshwy
//#define DEBUGGER
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

#ifdef DEBUGGER

#define log_prefix()                                                \
  {                                                                 \
    fprintf(stderr, "\033[37mLine %-3d [%dms]:\033[0m  ", __LINE__, \
        (int)clock() / 1000);                                       \
  }
#define ilog(...) \
  { fprintf(stderr, __VA_ARGS__); }
#define llog(...)      \
  {                    \
    log_prefix();      \
    ilog(__VA_ARGS__); \
  }
#define log(...)           \
  {                        \
    log_prefix();          \
    ilog(__VA_ARGS__);     \
    fprintf(stderr, "\n"); \
  }
#define red(...)       \
  {                    \
    log_prefix();      \
    ilog("\033[31m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define green(...)     \
  {                    \
    log_prefix();      \
    ilog("\033[32m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define blue(...)      \
  {                    \
    log_prefix();      \
    ilog("\033[34m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }

#else

#define log_prefix() ;
#define ilog(...) ;
#define llog(...) ;
#define log(...) ;
#define red(...) ;
#define green(...) ;
#define blue(...) ;

#endif

namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 2e5 + 5, ALP = 26;

int n, m, typ, tr[N][ALP], val[N], len[N], fail[N], nd[N], fa[N], D[N];

namespace treap {
  int Tot;
  vector<vector<pair<int, int>>> V(1);
  int add(int u, int pos, int val) {
    //在pos位置加上一个数v
    //返回修改后的数据结构
    int nw = V.size();
    vector<pair<int, int>> v = V[u];
    auto p = lower_bound(v.begin(), v.end(), make_pair(pos, -1));
    if (p != v.end() && p->fi == pos) {
      p->se += val;
    } else {
      v.insert(p, make_pair(pos, val));
      sort(v.begin(), v.end());
    }
    V.pb(v);
    return nw;
  }
  int shift(int u, int d) {
    //把所有数的pos都+d
    //返回修改后的数据结构
    int nw = V.size();
    vector<pair<int, int>> v = V[u];
    for (auto &x : v) x.fi += d;
    V.pb(v);
    return nw;
  }
  int query(int u, int L, int R) {
    //求pos in [L,R]的权值和
    int res = 0;
    for (auto x : V[u])
      if (L <= x.fi && x.fi <= R) res += x.se;
    return res;
  }
  int merge(int x, int y) {
    //两棵树合并。保证不相交。
    //返回修改后的数据结构
    int nw = V.size();
    vector<pair<int, int>> v = V[x];
    v.insert(v.end(), V[y].begin(), V[y].end());
    V.pb(v);
    return nw;
  }
  int range_tree(int u, int L, int R) {
    // u的pos in [L,R]的子树
    //返回split出的数据结构
    int nw = V.size();
    vector<pair<int, int>> v;
    for (auto x : V[u])
      if (L <= x.fi && x.fi <= R) v.pb(x);
    V.pb(v);
    return nw;
  }
} // namespace treap
int tot;

void insert(char *s, int v, int u = 0) {
  if (!*s) return val[u] += v, void();
  if (!tr[u][*s - 'a']) tr[u][*s - 'a'] = ++tot, fa[tot] = u, len[tot] = len[u] + 1;
  insert(s + 1, v, tr[u][*s - 'a']);
}
queue<int> q;
void build() {
  fa[0] = -1;
  FOR(i, 0, ALP - 1) if (tr[0][i]) q.push(tr[0][i]);
  while (!q.empty()) {
    int u = q.front();
    q.pop();
    //求D[u]
    int x1 = treap::shift(D[fail[u]], len[u] - len[fail[u]]);
    int x2 = treap::range_tree(D[fa[u]], 1, len[u] - len[fail[u]]);
    // x2的最大pos是小于x1的最小pos的
    D[u] = treap::merge(x2, x1);
    if (val[u]) D[u] = treap::add(D[u], 1, val[u]);

    FOR(i, 0, ALP - 1) {
      if (tr[u][i])
        fail[tr[u][i]] = tr[fail[u]][i], q.push(tr[u][i]);
      else
        tr[u][i] = tr[fail[u]][i];
    }
  }
}

char s[N], t[N];

int main() {
  scanf("%d%d%d", &n, &m, &typ);
  scanf("%s", s + 1);
  FOR(i, 1, m) {
    int x;
    scanf("%s%d", t, &x);
    insert(t, x);
  }
  insert(s + 1, 0);

  build();
  for (int i = 1, u = 0; i <= n; i++) {
    u = tr[u][s[i] - 'a'];
    assert(u);
    nd[i] = u;
  }

  int q, las = 0;
  scanf("%d", &q);
  FOR(i, 1, q) {
    int l, r;
    scanf("%d%d", &l, &r);
    l = l ^ (las * typ);
    r = r ^ (las * typ);
    las = treap::query(D[nd[r]], l, r);
    printf("%d\n", las);
  }

  return 0;
}

// AC 自动机 review：
//     AC自动机每个结点（状态）表示的是一个模式串的前缀。
//     fail指针指向最长的后缀状态。
//
// 首先，字符串去重。这个可以在 build 阿城的时候做。
// 然后我们用数据结构维护串 S 中的匹配权值状态，记作 D(S)。
// D(S)[i]表示S中以S[i]开头的匹配串的权值和。（1<=i<=|S|）。
// 然后我们求D(S)的区间和即可。
