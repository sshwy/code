// by Sshwy
//#define DEBUGGER
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

#ifdef DEBUGGER

#define log_prefix()                                                \
  {                                                                 \
    fprintf(stderr, "\033[37mLine %-3d [%dms]:\033[0m  ", __LINE__, \
        (int)clock() / 1000);                                       \
  }
#define ilog(...) \
  { fprintf(stderr, __VA_ARGS__); }
#define llog(...)      \
  {                    \
    log_prefix();      \
    ilog(__VA_ARGS__); \
  }
#define log(...)           \
  {                        \
    log_prefix();          \
    ilog(__VA_ARGS__);     \
    fprintf(stderr, "\n"); \
  }
#define red(...)       \
  {                    \
    log_prefix();      \
    ilog("\033[31m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define green(...)     \
  {                    \
    log_prefix();      \
    ilog("\033[32m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define blue(...)      \
  {                    \
    log_prefix();      \
    ilog("\033[34m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }

#else

#define log_prefix() ;
#define ilog(...) ;
#define llog(...) ;
#define log(...) ;
#define red(...) ;
#define green(...) ;
#define blue(...) ;

#endif

namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 305;
int n, m, c[N];
vector<int> a[N];

int main() {
  scanf("%d%d", &n, &m);
  FOR(i, 1, n) {
    FOR(j, 1, m) {
      int x;
      scanf("%d", &x);
      a[i].pb(x);
    }
  }
  int ans = n;
  FOR(_, 1, m) {
    // FOR(i,1,n){
    //    for(int x:a[i])printf("%d ",x);
    //    puts("");
    //}
    fill(c, c + m + 1, 0);
    FOR(i, 1, n) c[*a[i].begin()]++;
    auto p = max_element(c + 1, c + m + 1);
    ans = min(ans, *p);
    int v = p - c;
    FOR(i, 1, n) {
      bool flag = 0;
      for (auto x = a[i].begin(); x != a[i].end(); ++x) {
        if (*x == v) {
          a[i].erase(x);
          flag = 1;
          break;
        }
      }
      assert(flag);
    }
  }
  printf("%d\n", ans);
  return 0;
}
