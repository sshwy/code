// by Sshwy
//#define DEBUGGER
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

#ifdef DEBUGGER

#define log_prefix()                                                \
  {                                                                 \
    fprintf(stderr, "\033[37mLine %-3d [%dms]:\033[0m  ", __LINE__, \
        (int)clock() / 1000);                                       \
  }
#define ilog(...) \
  { fprintf(stderr, __VA_ARGS__); }
#define llog(...)      \
  {                    \
    log_prefix();      \
    ilog(__VA_ARGS__); \
  }
#define log(...)           \
  {                        \
    log_prefix();          \
    ilog(__VA_ARGS__);     \
    fprintf(stderr, "\n"); \
  }
#define red(...)       \
  {                    \
    log_prefix();      \
    ilog("\033[31m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define green(...)     \
  {                    \
    log_prefix();      \
    ilog("\033[32m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define blue(...)      \
  {                    \
    log_prefix();      \
    ilog("\033[34m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }

#else

#define log_prefix() ;
#define ilog(...) ;
#define llog(...) ;
#define log(...) ;
#define red(...) ;
#define green(...) ;
#define blue(...) ;

#endif

namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
#include <stack>
const int N = 2005;
int n, m, v, r;
bool ban[N];
int a[N][N], b[N], b1[N];

void kick(int *q, int &pos) {
  while (pos <= m && ban[q[pos]]) ++pos;
}

int go() {
  scanf("%d%d", &n, &m);
  if (n == 0 && m == 0) return 1;
  scanf("%d%d", &v, &r);
  FOR(i, 1, m) ban[i] = 0;
  FOR(i, 1, n) {
    FOR(j, 1, m) {
      int x;
      scanf("%d", &x);
      a[i][j] = x;
    }
  }
  FOR(i, 1, n) b[i] = 1;

  FOR(i, 1, v - 1) {
    kick(a[i], b[i]);
    if (b[i] <= m) ban[a[i][b[i]]] = 1;
    if (ban[r] == 1) {
      printf("%d\n", m - i + 1);
      return 0;
    }
  }
  FOR(i, 1, n) {
    FOR(j, 1, m) a[i + n][j] = a[i][j];
    b[i + n] = b[i];
  }
  auto A = a + v;
  auto B = b + v;
  int rest = m - v + 1, t = 0;
  // printf("rest=%d,t=%d\n",rest,t);
  while (1) { // A[0..n-1]
    ++t;      // A[0]要ban掉这么多人
    // printf("t=%d\n",t);
    stack<int> s;
    FOR(i, 1, n - 1) b1[i] = B[i];
    FOR(i, 1, n - 1) {
      kick(A[i], B[i]);
      if (B[i] <= m) ban[A[i][B[i]]] = 1, s.push(A[i][B[i]]);
    }
    // FOR(i,1,n-1)kick(A[i],B[i]);
    // puts("==========");
    // FOR(i,1,n-1){
    //    FOR(j,B[i],m)printf("%d ",A[i][j]); puts("");
    //} puts("==========");
    // printf("rest = %d size %d\n",rest,s.size());
    if (ban[r] == 1 || rest - s.size() <= t) {
      // undo
      while (s.size()) ban[s.top()] = 0, s.pop();
      FOR(i, 1, n - 1) B[i] = b1[i];

      // FOR(i,1,n-1)kick(A[i],B[i]);
      // puts("undo ==========");
      // FOR(i,1,n-1){
      //    FOR(j,B[i],m)printf("%d ",A[i][j]); puts("");
      //} puts("==========");
      // printf("rest = %d size %d\n",rest,s.size());

      if (rest == t) { //刚好，挂在自己手上
        printf("1\n");
        return 0;
      }

      FOR(i, 1, n - 1) {
        assert(rest >= t + 1);
        if (rest == t + 1) { //最后一个被干
          printf("1\n");
          return 0;
        } else {
          kick(A[i], B[i]);
          if (B[i] <= m) ban[A[i][B[i]]] = 1, --rest;
          if (ban[r] == 1) { //被ban了
            rest -= t;       // v搞的t个人
            printf("%d\n", rest + 1);
            return 0;
          }
        }
      }
      assert(0); //没被干掉，就nm离谱
    } else {
      rest -= s.size();
      // keep going!
    }
  }
}
int main() {
  while (go() == 0)
    ;
  return 0;
}

// 可以先执行v前面的人的操作，把v变成第一个裁判。
// 如果 v 不做操作，然后这一轮 x 被干了，那么 v 是无法挽救的。
// 如果 x 这一轮没有被干，那么我们要给 v 决定一个操作，使得他操作之后 x
// 仍然不会被干。 当然如果会被干就gg
// 考虑把操作囤着。每次我们检查，v做t次操作后，这一轮x是否被干。
// 如果不被干就继续下一轮。否则我们让v做最优的t次操作来让x的排列最靠前。
//
// 1. 如何检查？
// 2. 如果做最优t次操作？
//
// 检查：
//     如果v不做操作x都会被干，那么gg
//     否则，我们考虑，让v干掉那些，没有被这一轮后面的人干掉的数。
//     显然这不会对后面的人产生影响。
//     那么显然，只要这一轮完后剩余人数大于t，就是OK的。
//
// 最优操作：
//     否则，说明剩余人数<=t。我们可以考虑让后面的数先删。直到剩余人数为t+1，
//     然后把v的t次操作补掉。这样最后一个删的就是x。
