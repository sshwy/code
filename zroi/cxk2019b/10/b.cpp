#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
namespace IO {
  char nc() {
    static char bf[100000], *p1 = bf, *p2 = bf;
    return p1 == p2 && (p2 = (p1 = bf) + fread(bf, 1, 100000, stdin), p1 == p2)
               ? EOF
               : *p1++;
  }
  int rd() {
    int res = 0;
    char c = getchar();
    while (!isdigit(c)) c = getchar();
    while (isdigit(c)) res = res * 10 + c - '0', c = getchar();
    return res;
  }
} // namespace IO
/******************heading******************/
#define int long long
typedef pair<int, int> pii;
const int N = 2e5 + 50, P = 1e9 + 7;
int n, c[N];

struct qxx {
  int nex, t;
};
qxx e[N * 2];
int h[N], le = 1;
void add_path(int f, int t) { e[++le] = (qxx){h[f], t}, h[f] = le; }

bool vis[N];
int sz[N];
int calc_sz(int u, int p) { //{{{
  sz[u] = 1;
  for (int i = h[u]; i; i = e[i].nex) {
    const int v = e[i].t;
    if (v == p || vis[v]) continue;
    sz[u] += calc_sz(v, u);
  }
  return sz[u];
} //}}}
pii find_core(int u, int p, int r) { //{{{
  int mx = sz[r] - sz[u], w = u;
  for (int i = h[u]; i; i = e[i].nex) {
    const int v = e[i].t;
    if (v == p || vis[v]) continue;
    mx = max(mx, sz[v]);
  }
  for (int i = h[u]; i; i = e[i].nex) {
    const int v = e[i].t;
    if (v == p || vis[v]) continue;
    pii x = find_core(v, u, r);
    if (x.se < mx) w = x.fi, mx = x.se;
  }
  return mk(w, mx);
} //}}}

vector<pii> key_sz[N];
// key_sz[i]: the size of keynode which color is k.
// The second element is its root-son tree.
// Each subtree must be separated. MAKESURE TO CLEAR!

int cnt_col[N];
void find_key_node(int u, int p, int rson) { //{{{
  // MAKESURE TO CALC_SZ BEFORE!
  if (!cnt_col[c[u]]) {
    int len = key_sz[c[u]].size();
    if (!len || key_sz[c[u]][len - 1].se != rson)
      key_sz[c[u]].pb(
          mk(sz[u], rson)); //,printf("\t\033[32mfind key node %lld!\033[0m\n",u);
    else
      key_sz[c[u]][len - 1].fi += sz[u];
  }
  cnt_col[c[u]]++;

  for (int i = h[u]; i; i = e[i].nex) {
    const int v = e[i].t;
    if (v == p || vis[v]) continue;
    find_key_node(v, u, rson);
  }

  cnt_col[c[u]]--;
} //}}}
int calc1(int core) { //{{{
  calc_sz(core, core);

  FOR(i, 1, n) key_sz[i].clear();
  //-CLEAR cnt_col

  if (!cnt_col[c[core]]) key_sz[c[core]].pb(mk(sz[core], core));
  cnt_col[c[core]]++;

  int tot_sz2 = 0;
  for (int i = h[core]; i; i = e[i].nex) {
    const int v = e[i].t;
    if (vis[v]) continue;
    find_key_node(v, core, v);
    tot_sz2 = (tot_sz2 + sz[v] * sz[v] % P) % P;
  }
  int tot = 0, tot_sz = sz[core] - 1;
  tot += (tot_sz * tot_sz - tot_sz2) % P;
  FOR(i, 1, n) {
    // consider different color
    if (key_sz[i].empty()) continue;
    int tot_i = 0, tot_i2 = 0;
    FOR(j, 0, key_sz[i].size() - 1) {
      pii x = key_sz[i][j];
      tot = (tot + x.fi * 2) % P; // A.1
      if (x.se != core) {
        tot = tot + (tot_sz - sz[x.se]) * x.fi * 2 % P; // B.1
        tot %= P;
        tot_i = (tot_i + x.fi) % P;
        tot_i2 = (tot_i2 + x.fi * x.fi) % P;
      }
    }
    tot = (tot - tot_i * tot_i % P + tot_i2) % P; // B.2
  }
  cnt_col[c[core]]--;
  return tot;
} //}}}
int ans = 0;
void divide(int u) {
  calc_sz(u, u);
  pii x = find_core(u, u, u);
  int core = x.fi;
  ans = (ans + calc1(core)) % P;
  vis[core] = 1;
  for (int i = h[core]; i; i = e[i].nex) {
    const int v = e[i].t;
    if (vis[v]) continue;
    divide(v);
  }
}

signed main() {
  scanf("%lld", &n);
  FOR(i, 1, n) scanf("%lld", &c[i]);
  FOR(i, 2, n) {
    int a, b;
    scanf("%lld%lld", &a, &b);
    add_path(a, b), add_path(b, a);
  }
  divide(1);
  ans = (ans - n * 2);
  ans = (ans % P + P) % P;
  int m = 1;
  FOR(i, 1, n - 1) m = 1ll * m * i % P;
  ans = 1ll * ans * m % P;
  printf("%lld\n", ans);
  return 0;
}
