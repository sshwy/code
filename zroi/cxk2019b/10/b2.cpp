#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
namespace IO {
  char nc() {
    static char bf[100000], *p1 = bf, *p2 = bf;
    return p1 == p2 && (p2 = (p1 = bf) + fread(bf, 1, 100000, stdin), p1 == p2)
               ? EOF
               : *p1++;
  }
  int rd() {
    int res = 0;
    char c = getchar();
    while (!isdigit(c)) c = getchar();
    while (isdigit(c)) res = res * 10 + c - '0', c = getchar();
    return res;
  }
} // namespace IO
/******************heading******************/
#define int long long
const int N = 5050, P = 1e9 + 7;
int n, c[N];
int f[N][N];

struct qxx {
  int nex, t;
};
qxx e[N * 2];
int h[N], le = 1;
void add_path(int f, int t) { e[++le] = (qxx){h[f], t}, h[f] = le; }

int cnt[N], tot;
void add(int col) {
  if (!cnt[col]) ++tot;
  cnt[col]++;
}
void dec(int col) {
  cnt[col]--;
  if (!cnt[col]) --tot;
}
void dfs(int u, int p, int r) {
  add(c[u]);
  f[r][u] = tot;
  for (int i = h[u]; i; i = e[i].nex) {
    const int v = e[i].t;
    if (v == p) continue;
    dfs(v, u, r);
  }
  dec(c[u]);
}

signed main() {
  scanf("%lld", &n);
  FOR(i, 1, n) scanf("%lld", &c[i]);
  FOR(i, 2, n) {
    int a, b;
    scanf("%lld%lld", &a, &b);
    add_path(a, b), add_path(b, a);
  }
  FOR(i, 1, n) { dfs(i, i, i); }
  int m = 1, ans = 0;
  FOR(i, 1, n - 1) m = 1ll * m * i % P;
  FOR(i, 1, n) {
    FOR(j, 1, n) {
      if (i == j) continue;
      assert(f[i][j] == f[j][i]);
      ans = (ans + f[i][j]) % P;
    }
  }
  ans = 1ll * ans * m % P;
  printf("%lld\n", ans);
  return 0;
}
