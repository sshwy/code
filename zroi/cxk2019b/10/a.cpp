#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
namespace IO {
  char nc() {
    static char bf[100000], *p1 = bf, *p2 = bf;
    return p1 == p2 && (p2 = (p1 = bf) + fread(bf, 1, 100000, stdin), p1 == p2)
               ? EOF
               : *p1++;
  }
  int rd() {
    int res = 0;
    char c = getchar();
    while (!isdigit(c)) c = getchar();
    while (isdigit(c)) res = res * 10 + c - '0', c = getchar();
    return res;
  }
} // namespace IO
/******************heading******************/
const int N = 1e6 + 50;

char s[N];
namespace SA {
  int l;
  int sa[N], rk[N];
  int t[N], bin[N], sz;
  int h[N], he[N]; // h,height
  void init(int n) {
    memset(sa, 0, sizeof(int) * n);
    memset(rk, 0, sizeof(int) * n);
    l = 0;
  }
  void qsort() {
    FOR(i, 0, sz) bin[i] = 0;
    FOR(i, 1, l) bin[rk[i]]++;
    FOR(i, 1, sz) bin[i] += bin[i - 1];
    ROF(i, l, 1) sa[bin[rk[t[i]]]--] = t[i];
  }
  void make() {
    l = strlen(s + 1), sz = max(l, 4);
    FOR(i, 1, l) t[i] = i, rk[i] = s[i] - 'a' + 1;
    qsort();
    for (int j = 1; j <= l; j <<= 1) {
      int tot = 0;
      FOR(i, l - j + 1, l) t[++tot] = i;
      FOR(i, 1, l) if (sa[i] - j > 0) t[++tot] = sa[i] - j;
      qsort(), swap(t, rk);
      rk[sa[1]] = tot = 1;
      FOR(i, 2, l)
      rk[sa[i]] =
          t[sa[i - 1]] == t[sa[i]] && t[sa[i - 1] + j] == t[sa[i] + j] ? tot : ++tot;
    }
    // printf("sa: ");FOR(i,1,l)printf("%2d%c",sa[i]," \n"[i==l]);
    // printf("rk: ");FOR(i,1,l)printf("%2d%c",rk[i]," \n"[i==l]);
  }
  int move(int x, int y, int len) {
    while (x + len <= l && y + len <= l && s[x + len] == s[y + len]) ++len;
    return len;
  }
  void calc_h() {
    FOR(i, 1, l)
    h[i] = rk[i] == 1 ? 0 : move(i, sa[rk[i] - 1], max(h[i - 1] - 1, 0));
    FOR(i, 1, l) he[i] = h[sa[i]];
    // printf("h: "); FOR(i,1,l)printf("%d%c",h[sa[i]]," \n"[i==l]);
  }
} // namespace SA

void calc(int k) {
  int l = -1, r = -1;
  FOR(i, 1, SA::l) {
    int mn = 0x3f3f3f3f;
    FOR(j, i + 1, SA::l) {
      mn = min(mn, SA::he[j]);
      // printf("i=%d,j=%d,mn=%d\n",i,j,mn);
      if (mn == k) {
        int x = SA::sa[i], y = SA::sa[j];
        if (x > y) swap(x, y);
        if (~l && ~r) {
          if (x < l)
            l = x, r = y;
          else if (x == l && y < r)
            l = x, r = y;
        } else {
          l = x, r = y;
        }
      }
    }
  }
  if (~l && ~r)
    printf("%d %d\n", l, r);
  else
    printf("%d %d\n", SA::l - k + 1, SA::l - k + 1);
}
void go() {
  scanf("%s", s + 1);
  SA::init(strlen(s + 1) + 5);
  SA::make();
  SA::calc_h();
  FOR(k, 1, SA::l) { calc(k); }
}
int main() {
  int t;
  scanf("%d", &t);
  while (t--) go();
  return 0;
}
