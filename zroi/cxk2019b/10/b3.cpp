#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
namespace IO {
  char nc() {
    static char bf[100000], *p1 = bf, *p2 = bf;
    return p1 == p2 && (p2 = (p1 = bf) + fread(bf, 1, 100000, stdin), p1 == p2)
               ? EOF
               : *p1++;
  }
  int rd() {
    int res = 0;
    char c = getchar();
    while (!isdigit(c)) c = getchar();
    while (isdigit(c)) res = res * 10 + c - '0', c = getchar();
    return res;
  }
} // namespace IO
/******************heading******************/
#define int long long
const int N = 1e6 + 50, P = 1e9 + 7, INF = 0x3f3f3f3f;
int n, c[N];

struct qxx {
  int nex, t;
};
qxx e[N * 2];
int h[N], le = 1;
void add_path(int f, int t) { e[++le] = (qxx){h[f], t}, h[f] = le; }

bool vis[N];
int sz[N];
int max(int a, int b) { return a > b ? a : b; }
int calc_sz(int u, int p) { //{{{
  sz[u] = 1;
  for (int i = h[u]; i; i = e[i].nex) {
    const int v = e[i].t;
    if (v == p || vis[v]) continue;
    sz[u] += calc_sz(v, u);
  }
  return sz[u];
} //}}}
int core_found, core_max;
void find_core(int u, int p, int r) { //{{{
  int mx = sz[r] - sz[u];
  for (int i = h[u]; i; i = e[i].nex) {
    const int v = e[i].t;
    if (v == p || vis[v]) continue;
    mx = max(mx, sz[v]);
  }
  if (mx < core_max) core_found = u, core_max = mx;
  for (int i = h[u]; i; i = e[i].nex) {
    const int v = e[i].t;
    if (v == p || vis[v]) continue;
    find_core(v, u, r);
  }
} //}}}

namespace KEY_SZ {
  // key_sz[i]: the size of keynode which color is k.
  // The second element is its root-son tree.
  // Each subtree must be separated. MAKESURE TO CLEAR!
  struct qxx {
    int nex, fi, se;
  };
  qxx e[N * 2];
  int h[N], le = 1;
  void add_pair(int col, int fi, int se) {
    e[++le] = (qxx){h[col], fi, se}, h[col] = le;
  }
  void init(int n) { memset(h, 0, sizeof(int) * n), le = 1; }
} // namespace KEY_SZ

int cnt_col[N];
int exists_col[N], lc;
bool exists[N];
void find_key_node(int u, int p, int rson) { //{{{
  // MAKESURE TO CALC_SZ BEFORE!
  if (!cnt_col[c[u]]) {
    if (!KEY_SZ::h[c[u]] || KEY_SZ::e[KEY_SZ::h[c[u]]].se != rson)
      KEY_SZ::add_pair(c[u], sz[u], rson);
    else
      KEY_SZ::e[KEY_SZ::h[c[u]]].fi += sz[u];
    if (!exists[c[u]]) {
      exists[c[u]] = 1;
      exists_col[++lc] = c[u];
    }
  }
  cnt_col[c[u]]++;

  for (int i = h[u]; i; i = e[i].nex) {
    const int v = e[i].t;
    if (v == p || vis[v]) continue;
    find_key_node(v, u, rson);
  }

  cnt_col[c[u]]--;
} //}}}
int calc1(int core) { //{{{
  calc_sz(core, core);

  // KEY_SZ::init(n+5);
  FOR(i, 1, lc) KEY_SZ::h[exists_col[i]] = 0, exists[exists_col[i]] = 0;
  lc = 0;

  if (!cnt_col[c[core]]) KEY_SZ::add_pair(c[core], sz[core], core);
  cnt_col[c[core]]++;

  if (!exists[c[core]]) {
    exists[c[core]] = 1;
    exists_col[++lc] = c[core];
  }

  int tot_sz2 = 0, tot = 0, tot_sz = sz[core] - 1;
  for (int i = h[core]; i; i = e[i].nex) {
    const int v = e[i].t;
    if (vis[v]) continue;
    find_key_node(v, core, v);
    tot_sz2 = (tot_sz2 + 1ll * sz[v] * sz[v] % P) % P;
  }
  tot += (1ll * tot_sz * tot_sz % P - tot_sz2) % P;
  FOR(_i, 1, lc) {
    int i = exists_col[_i];
    if (!KEY_SZ::h[i]) continue;
    // consider different color
    int tot_i = 0, tot_i2 = 0;
    for (int j = KEY_SZ::h[i]; j; j = KEY_SZ::e[j].nex) {
      KEY_SZ::qxx x = KEY_SZ::e[j];
      tot = (tot + (x.fi << 1)) % P; // A.1
      if (x.se != core) {
        tot = (tot + 2ll * (tot_sz - sz[x.se]) % P * x.fi) % P; // B.1
        tot_i = (tot_i + x.fi) % P;
        tot_i2 = (tot_i2 + 1ll * x.fi * x.fi % P) % P;
      }
    }
    tot = (tot - 1ll * tot_i * tot_i % P + tot_i2 % P) % P; // B.2
  }
  cnt_col[c[core]]--;
  return tot;
} //}}}
int ans = 0;
void divide(int u) {
  calc_sz(u, u);
  core_found = 0, core_max = INF;
  find_core(u, u, u);
  ans = (ans + calc1(core_found)) % P;
  vis[core_found] = 1;
  for (int i = h[core_found]; i; i = e[i].nex) {
    const int v = e[i].t;
    if (vis[v]) continue;
    divide(v);
  }
}

signed main() {
  scanf("%lld", &n);
  FOR(i, 1, n) scanf("%lld", &c[i]);
  FOR(i, 2, n) {
    int a, b;
    scanf("%lld%lld", &a, &b);
    add_path(a, b), add_path(b, a);
  }
  divide(1);
  ans = (ans - n * 2);
  ans = (ans % P + P) % P;
  int m = 1;
  FOR(i, 1, n - 1) m = 1ll * m * i % P;
  ans = 1ll * ans * m % P;
  printf("%lld\n", ans);
  return 0;
}
