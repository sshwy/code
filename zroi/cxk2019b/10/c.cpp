#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
namespace IO {
  char nc() {
    static char bf[100000], *p1 = bf, *p2 = bf;
    return p1 == p2 && (p2 = (p1 = bf) + fread(bf, 1, 100000, stdin), p1 == p2)
               ? EOF
               : *p1++;
  }
  int rd() {
    int res = 0;
    char c = getchar();
    while (!isdigit(c)) c = getchar();
    while (isdigit(c)) res = res * 10 + c - '0', c = getchar();
    return res;
  }
} // namespace IO
/******************heading******************/
const int N = 1e3 + 50;
int l[N], r[N], fa[N];
int l1[N], r1[N], fa1[N];
void left(int x) {
  int y = fa[x];
  int z = fa[y];
  r[y] = l[x];
  if (l[x]) fa[l[x]] = y;
  fa[x] = z;
  if (z) {
    if (l[z] == y)
      l[z] = x;
    else
      r[z] = x;
  }
  l[x] = y;
  fa[y] = x;
}
void right(int x) {
  int y = fa[x];
  int z = fa[y];
  l[y] = r[x];
  if (r[x]) fa[r[x]] = y;
  fa[x] = z;
  if (z) {
    if (l[z] == y)
      l[z] = x;
    else
      r[z] = x;
  }
  r[x] = y;
  fa[y] = x;
}
void splay(int x) {
  while (fa[x]) {
    int y = fa[x];
    int z = fa[y];
    if (!z) {
      if (l[y] == x)
        right(x);
      else
        left(x);
    } else {
      if (r[z] == y) {
        if (r[y] == x)
          left(y), left(x);
        else
          right(x), left(x);
      } else {
        if (l[y] == x)
          right(y), right(x);
        else
          left(x), right(x);
      }
    }
  }
}
int n;
int ans[N];
void calc(int u, int dp) {
  if (!u) return;
  ans[u] += dp;
  calc(l[u], dp + 1);
  calc(r[u], dp + 1);
}
void work(int i) {
  FOR(i, 1, n) {
    fa[i] = fa1[i];
    l[i] = l1[i];
    r[i] = r1[i];
  }
  splay(i);
  calc(i, 0);
}
int main() {
  scanf("%d", &n);
  FOR(i, 1, n) {
    scanf("%d%d", &l1[i], &r1[i]);
    fa1[l1[i]] = fa1[r1[i]] = i;
  }
  FOR(i, 1, n) work(i);
  FOR(i, 1, n) printf("%d\n", ans[i]);
  return 0;
}
