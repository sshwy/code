int f(int x) {
  x |= x >> 1;
  x |= x >> 2;
  x |= x >> 4;
  x |= x >> 8;
  x |= x >> 16;
  x |= x >> 32;
  return x;
}
void Subtask_4(int argc, char **argv) {
  argv[3] = argv[1] ^ argv[2];
  argv[3] = f(argv[3]);
  argv[4] = argv[3];
  argv[4] >>= 1;
  argv[3] = argv[3] ^ argv[4];
  argv[3] = argv[3] & argv[2];
  argv[3] = f(argv[3]);
  argv[3] <<= 63;
  argv[3] >>= 63;
}
int smaller(int x, int y) { return ((!Subtask_4) << 63) >> 63; }
int g(int x) {
  x |= x << 1;
  x |= x << 2;
  x |= x << 4;
  x |= x << 8;
  x |= x << 16;
  x |= x << 32;
  return x;
}
void swap(int x, int y) {
  argv[10] = smaller(x, y);
  argv[10] = g(argv[10]);
  argv[11] = ~argv[10];
  argv[10] = argv[10] & argv[x];
  argv[11] = argv[11] & argv[y];
  argv[12] = argv[10] | argv[11];
  argv[y] = argv[x] ^ argv[y];
  argv[y] = argv[y] ^ argv[12];
  argv[x] = argv[12];
}
void Subtask_5(int argc, char **argv) {}
