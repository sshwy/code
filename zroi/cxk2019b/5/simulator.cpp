#include <cstdio>
#include <cstdlib>
#include <cstring>
unsigned long long arr[45];
char op[15];
FILE *fin, *fcode, *fout;
inline int get_idx() {
  int val;
  fscanf(fcode, "%d", &val);
  if (val <= 0 || val > 40) {
    fprintf(stderr, "Error! Index %d violates the range [1, 40].\n", val);
    exit(-1);
  }
  return val;
}
inline int get_value() {
  int val;
  fscanf(fcode, "%d", &val);
  if (val < 0 || val > 64) {
    fprintf(stderr, "Error! Index %d violates the range [0, 64].\n", val);
    exit(-1);
  }
  return val;
}
int main(int argc, char **argv) {
  if (argc != 4) {
    puts("USAGE: ./simulator <input> <code> <output>");
    puts("       <input>  : Initial values for the registers.");
    puts("       <code>   : Your operation sequence.");
    puts("       <output> : The result.");
    puts("       You can use '-' to represent stdin/stdout.");
    return -1;
  }
  fin = !strcmp(argv[1], "-") ? stdin : fopen(argv[1], "r");
  fcode = !strcmp(argv[2], "-") ? stdin : fopen(argv[2], "r");
  fout = !strcmp(argv[3], "-") ? stdout : fopen(argv[3], "wt");
  if (!fin || !fcode || !fout) {
    fprintf(stderr, "Error! Cannot open file.\n");
    exit(-1);
  }
  int n;
  fscanf(fin, "%d", &n);
  for (int i = 1; i <= n; i++) fscanf(fin, "%llu", arr + i);
  while (~fscanf(fcode, "%s", op)) {
    if (!strcmp(op, "set")) {
      int a = get_idx(), b = get_idx();
      arr[a] = arr[b];
    } else if (!strcmp(op, "xor")) {
      int a = get_idx(), b = get_idx(), c = get_idx();
      arr[a] = arr[b] ^ arr[c];
    } else if (!strcmp(op, "and")) {
      int a = get_idx(), b = get_idx(), c = get_idx();
      arr[a] = arr[b] & arr[c];
    } else if (!strcmp(op, "or")) {
      int a = get_idx(), b = get_idx(), c = get_idx();
      arr[a] = arr[b] | arr[c];
    } else if (!strcmp(op, "shl")) {
      int a = get_idx(), b = get_value();
      arr[a] <<= b;
    } else if (!strcmp(op, "shr")) {
      int a = get_idx(), b = get_value();
      arr[a] >>= b;
    } else if (!strcmp(op, "not")) {
      int a = get_idx(), b = get_idx();
      arr[a] = ~arr[b];
    } else {
      fprintf(stderr, "Error! Unknown operation %s.\n", op);
      return -1;
    }
    for (int i = 1; i <= 40; i++) fprintf(fout, "%llu%c", arr[i], " \n"[i == 40]);
  }
  for (int i = 1; i <= 40; i++) fprintf(fout, "%llu%c", arr[i], " \n"[i == 40]);
  return 0;
}
