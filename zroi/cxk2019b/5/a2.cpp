#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
namespace IO {
  char nc() {
    static char bf[100000], *p1 = bf, *p2 = bf;
    return p1 == p2 && (p2 = (p1 = bf) + fread(bf, 1, 100000, stdin), p1 == p2)
               ? EOF
               : *p1++;
  }
  int rd() {
    int res = 0;
    char c = getchar();
    while (!isdigit(c)) c = getchar();
    while (isdigit(c)) res = res * 10 + c - '0', c = getchar();
    return res;
  }
} // namespace IO
/******************heading******************/
const int N = 5003, INF = 0x3f3f3f3f;
int n, m;
int a[N];
int f[N][N], g[N][N];
int minlen = INF, ans;
int main() {
  memset(f, 0x3f, sizeof(f));
  scanf("%d%d", &n, &m);
  FOR(i, 1, n) scanf("%d", &a[i]);
  sort(a + 1, a + n + 1);
  // FOR(j,0,m)f[0][j]=-1;// f[0,0]=0;
  FOR(i, 1, n) {
    FOR(j, 0, m) {
      if (j == a[i]) {
        f[i][j] = 1, g[i][j] = a[i];
      } else {
        if (f[i - 1][j] < f[i][j]) { f[i][j] = f[i - 1][j], g[i][j] = g[i - 1][j]; }
        if (j >= a[i]) {
          if (f[i - 1][j - a[i]] + 1 < f[i][j]) {
            f[i][j] = f[i - 1][j - a[i]] + 1, g[i][j] = g[i - 1][j - a[i]];
          } else if (f[i - 1][j - a[i]] + 1 == f[i][j]) {
            g[i][j] = max(g[i][j], g[i - 1][j - a[i]]);
          }
        }
      }
      // if(f[i][j]!=INF)printf("f[%d,%2d]=%-10d,g[%d,%2d]=%-10d\n",i,j,f[i][j],i,j,g[i][j]);
    }
    if (f[i][m] != INF) {
      if (f[i][m] < minlen) {
        // printf("f[%d,%2d]=%-10d,g[%d,%2d]=%-10d\n",i,m,f[i][m],i,m,g[i][m]);
        minlen = f[i][m], ans = a[i] - g[i][m];
      } else if (f[i][m] == minlen) {
        // printf("f[%d,%2d]=%-10d,g[%d,%2d]=%-10d\n",i,m,f[i][m],i,m,g[i][m]);
        ans = min(ans, a[i] - g[i][m]);
      }
    }
  }
  printf("%d\n", ans);
  return 0;
}
