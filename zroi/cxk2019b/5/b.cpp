#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
namespace IO {
  char nc() {
    static char bf[100000], *p1 = bf, *p2 = bf;
    return p1 == p2 && (p2 = (p1 = bf) + fread(bf, 1, 100000, stdin), p1 == p2)
               ? EOF
               : *p1++;
  }
  int rd() {
    int res = 0;
    char c = getchar();
    while (!isdigit(c)) c = getchar();
    while (isdigit(c)) res = res * 10 + c - '0', c = getchar();
    return res;
  }
} // namespace IO
/******************heading******************/
const lld N = 30, INF = 0x3f3f3f3f3f3f3f3f;
lld n, a[N];
lld calc(lld k) {
  lld res = 0;
  FOR(i, 1, n) {
    lld ai, bi, ci = a[i], t;
    ai = ci / k, bi = min(ai, ci - ai * k);
    t = (ai - bi) / (k + 1);
    ai -= t, bi += k * t;
    res += ai * (k - 1) + bi;
  }
  return res;
}
lld mxa;
lld get(lld k) {
  lld res = INF;
  FOR(i, 1, n) if (a[i] >= k) res = min(res, a[i] / (a[i] / k));
  return res;
}
lld ans;
void go() {
  lld k = 1;
  while (k <= mxa) {
    k = get(k);
    ans = max(ans, calc(k));
    ++k;
  }
}
int main() {
  scanf("%lld", &n);
  FOR(i, 1, n) scanf("%lld", &a[i]), mxa = max(mxa, a[i]);
  sort(a + 1, a + n + 1);
  go();
  printf("%lld\n", ans);
  return 0;
}
