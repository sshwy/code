#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
namespace IO {
  char nc() {
    static char bf[100000], *p1 = bf, *p2 = bf;
    return p1 == p2 && (p2 = (p1 = bf) + fread(bf, 1, 100000, stdin), p1 == p2)
               ? EOF
               : *p1++;
  }
  int rd() {
    int res = 0;
    char c = getchar();
    while (!isdigit(c)) c = getchar();
    while (isdigit(c)) res = res * 10 + c - '0', c = getchar();
    return res;
  }
} // namespace IO
/******************heading******************/
unsigned long long argv[50], argc;
void print() { FOR(i, 1, 40) printf("%llu%c", argv[i], " \n"[i == 40]); }
int f(int x) {
  argv[x] |= argv[x] >> 1;
  argv[x] |= argv[x] >> 2;
  argv[x] |= argv[x] >> 4;
  argv[x] |= argv[x] >> 8;
  argv[x] |= argv[x] >> 16;
  argv[x] |= argv[x] >> 32;
  printf("set 30 %d\n", x);
  printf("shr 30 1\n");
  printf("or %d %d 30\n", x, x);
  printf("set 30 %d\n", x);
  printf("shr 30 2\n");
  printf("or %d %d 30\n", x, x);
  printf("set 30 %d\n", x);
  printf("shr 30 4\n");
  printf("or %d %d 30\n", x, x);
  printf("set 30 %d\n", x);
  printf("shr 30 8\n");
  printf("or %d %d 30\n", x, x);
  printf("set 30 %d\n", x);
  printf("shr 30 16\n");
  printf("or %d %d 30\n", x, x);
  printf("set 30 %d\n", x);
  printf("shr 30 32\n");
  printf("or %d %d 30\n", x, x);
  return x;
}
int smaller(int x, int y) {
  argv[20] = argv[x] ^ argv[y];
  printf("xor 20 %d %d\n", x, y);
  // argv[20]=argv[f(20)];
  f(20);
  argv[21] = argv[20];
  printf("set 21 20\n");
  argv[21] >>= 1;
  printf("shr 21 1\n");
  argv[20] = argv[20] ^ argv[21];
  printf("xor 20 20 21\n");
  argv[20] = argv[20] & argv[y];
  printf("and 20 20 %d\n", y);
  // argv[20]=argv[f(20)];
  f(20);
  argv[20] <<= 63;
  // printf("shl 20 63\n");
  argv[20] >>= 63;
  // printf("shr 20 63\n");
  return 20;
}
int g(int x) {
  argv[x] |= argv[x] << 1;
  argv[x] |= argv[x] << 2;
  argv[x] |= argv[x] << 4;
  argv[x] |= argv[x] << 8;
  argv[x] |= argv[x] << 16;
  argv[x] |= argv[x] << 32;
  printf("set 30 %d\n", x);
  printf("shl 30 1\n");
  printf("or %d %d 30\n", x, x);
  printf("set 30 %d\n", x);
  printf("shl 30 2\n");
  printf("or %d %d 30\n", x, x);
  printf("set 30 %d\n", x);
  printf("shl 30 4\n");
  printf("or %d %d 30\n", x, x);
  printf("set 30 %d\n", x);
  printf("shl 30 8\n");
  printf("or %d %d 30\n", x, x);
  printf("set 30 %d\n", x);
  printf("shl 30 16\n");
  printf("or %d %d 30\n", x, x);
  printf("set 30 %d\n", x);
  printf("shl 30 32\n");
  printf("or %d %d 30\n", x, x);
  return x;
}
void check(int x, int y) {
  smaller(x, y); // argv[20]=argv[smaller(x,y)];
  // printf("set 20 20\n");
  // argv[20]=argv[g(20)];
  g(20);
  argv[11] = ~argv[20];
  printf("not 11 20\n");
  argv[20] = argv[20] & argv[x];
  printf("and 20 20 %d\n", x);
  argv[11] = argv[11] & argv[y];
  printf("and 11 11 %d\n", y);
  argv[12] = argv[20] | argv[11];
  printf("or 12 20 11\n");
  argv[y] = argv[x] ^ argv[y];
  printf("xor %d %d %d\n", y, x, y);
  argv[y] = argv[y] ^ argv[12];
  printf("xor %d %d 12\n", y, y);
  argv[x] = argv[12];
  printf("set %d 12\n", x);
}
int main() {
  freopen("calculate6.ans", "w", stdout);
  scanf("%llu", &argc);
  FOR(i, 1, argc) scanf("%llu", &argv[i]);
  FOR(i, 1, 9) {
    FOR(j, 1, i - 1) { check(j, i); }
  }
  // puts(""); print();
  return 0;
}
