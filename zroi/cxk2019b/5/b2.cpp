#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
namespace IO {
  char nc() {
    static char bf[100000], *p1 = bf, *p2 = bf;
    return p1 == p2 && (p2 = (p1 = bf) + fread(bf, 1, 100000, stdin), p1 == p2)
               ? EOF
               : *p1++;
  }
  int rd() {
    int res = 0;
    char c = getchar();
    while (!isdigit(c)) c = getchar();
    while (isdigit(c)) res = res * 10 + c - '0', c = getchar();
    return res;
  }
} // namespace IO
/******************heading******************/
const int N = 30;
int n, a[N];
int calc(int k, int x) {
  printf("calc(%d,%d)\n", k, x);
  int p = 0, q = 0;
  FOR(i, k, n) {
    // printf("a[%d]=%d,t=%d,s=%d\n",i,a[i],t,s);
    int t = a[i] / (x + 1), s = a[i] % (x + 1);
    p += t;
    if (s == x) ++q;
  }
  // printf("p=%d,q=%d\n",p,q);
  return p * x + q * (x - 1);
}
int t, ans, mxa;
int main() {
  scanf("%d", &n);
  FOR(i, 1, n) scanf("%d", &a[i]), mxa = max(mxa, a[i]);
  sort(a + 1, a + n + 1);
  FOR(i, 1, n) {
    FOR(j, 1, a[i]) {
      int t = calc(i, j);
      ans = max(ans, t);
    }
  }
  printf("%d\n", ans);
  return 0;
}
