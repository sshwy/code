#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
namespace IO {
  char nc() {
    static char bf[100000], *p1 = bf, *p2 = bf;
    return p1 == p2 && (p2 = (p1 = bf) + fread(bf, 1, 100000, stdin), p1 == p2)
               ? EOF
               : *p1++;
  }
  int rd() {
    int res = 0;
    char c = getchar();
    while (!isdigit(c)) c = getchar();
    while (isdigit(c)) res = res * 10 + c - '0', c = getchar();
    return res;
  }
} // namespace IO
/******************heading******************/
unsigned long long argv[50], argc;
void print() { FOR(i, 1, 40) printf("%llu%c", argv[i], " \n"[i == 40]); }
int g(int x, int dig) {
  argv[x] |= argv[x] << 1;
  argv[x] |= argv[x] << 2;
  argv[x] |= argv[x] << 4;
  argv[x] |= argv[x] << 8;
  argv[x] |= argv[x] << 16;
  argv[x] |= argv[x] << 32;
  if (dig > 1) {
    printf("set 30 %d\n", x);
    printf("shl 30 1\n");
    printf("or %d %d 30\n", x, x);
  }
  if (dig > 2) {
    printf("set 30 %d\n", x);
    printf("shl 30 2\n");
    printf("or %d %d 30\n", x, x);
  }
  if (dig > 4) {
    printf("set 30 %d\n", x);
    printf("shl 30 4\n");
    printf("or %d %d 30\n", x, x);
  }
  if (dig > 8) {
    printf("set 30 %d\n", x);
    printf("shl 30 8\n");
    printf("or %d %d 30\n", x, x);
  }
  if (dig > 16) {
    printf("set 30 %d\n", x);
    printf("shl 30 16\n");
    printf("or %d %d 30\n", x, x);
  }
  if (dig > 32) {
    printf("set 30 %d\n", x);
    printf("shl 30 32\n");
    printf("or %d %d 30\n", x, x);
  }
  return x;
}
void upd(int dig) {
  argv[3] = argv[1] & argv[6];
  printf("and 3 1 6\n");
  argv[1] >>= 1;
  printf("shr 1 1\n");
  g(3, dig);
  argv[5] = argv[2];
  printf("set 5 2\n");
  argv[5] <<= 1;
  printf("shl 5 1\n");
  argv[5] |= argv[6];
  printf("or 5 5 6\n");
  argv[5] &= argv[3];
  printf("and 5 5 3\n");
  argv[2] |= argv[5];
  printf("or 2 2 5\n");
}
void pre() {
  argv[6] = ~argv[6];
  argv[6] <<= 1;
  argv[6] = ~argv[6];
  printf("not 6 6\n");
  printf("shl 6 1\n");
  printf("not 6 6\n");
}
int main() {
  freopen("calculate5.ans", "w", stdout);
  scanf("%llu", &argc);
  FOR(i, 1, argc) scanf("%llu", &argv[i]);
  pre();
  // print();
  FOR(i, 1, 64) {
    upd(i);
    // print();
  }
  // puts(""); print();
  return 0;
}
