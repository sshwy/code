#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
namespace IO {
  char nc() {
    static char bf[100000], *p1 = bf, *p2 = bf;
    return p1 == p2 && (p2 = (p1 = bf) + fread(bf, 1, 100000, stdin), p1 == p2)
               ? EOF
               : *p1++;
  }
  int rd() {
    int res = 0;
    char c = getchar();
    while (!isdigit(c)) c = getchar();
    while (isdigit(c)) res = res * 10 + c - '0', c = getchar();
    return res;
  }
} // namespace IO
/******************heading******************/
const int N = 503, INF = 0x3f3f3f3f;
int n, m;
int a[N];
int f[N][N][N]; // f[i,j,k]:ai..ai,len=j,sum=k,first max
int ans = 0x3f3f3f3f, minlen = 0x3f3f3f3f;
int main() {
  scanf("%d%d", &n, &m);
  FOR(i, 1, n) scanf("%d", &a[i]);
  sort(a + 1, a + n + 1);
  FOR(j, 0, n) FOR(k, 0, m) f[0][j][k] = -1;
  FOR(i, 1, n) {
    FOR(j, 0, i) {
      FOR(k, 0, m) {
        f[i][j][k] = -1;
        if (i > j) f[i][j][k] = max(f[i][j][k], f[i - 1][j][k]);
        if (k == a[i]) f[i][j][k] = max(f[i][j][k], a[i]);
        if (k > a[i] && j > 0)
          f[i][j][k] = max(f[i][j][k], f[i - 1][j - 1][k - a[i]]);
      }
      if (~f[i][j][m]) {
        // printf("f[%d,%d,%d]=%d\n",i,j,m,f[i][j][m]);
        if (j < minlen)
          minlen = j, ans = a[i] - f[i][j][m];
        else if (j == minlen)
          ans = min(ans, a[i] - f[i][j][m]);
      }
    }
  }
  printf("%d\n", ans == INF ? -1 : ans);
  return 0;
}
