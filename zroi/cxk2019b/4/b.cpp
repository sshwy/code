#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
namespace IO {
  char nc() {
    static char bf[100000], *p1 = bf, *p2 = bf;
    return p1 == p2 && (p2 = (p1 = bf) + fread(bf, 1, 100000, stdin), p1 == p2)
               ? EOF
               : *p1++;
  }
  int rd() {
    int res = 0;
    char c = getchar();
    while (!isdigit(c)) c = getchar();
    while (isdigit(c)) res = res * 10 + c - '0', c = getchar();
    return res;
  }
} // namespace IO
/******************heading******************/
const int N = 1006;
int n, m;
int a[N][N], b[N][N], tmp[N][N];
int br[N], bc[N];
int mr[N], mc[N];
int canr[N], canc[N];
int cntr[N], cntc[N];
int ahas[2], bhas[2];
vector<pii> check;
int main() {
  scanf("%d%d", &n, &m);
  char s[N];
  FOR(i, 1, n) {
    scanf("%s", s + 1);
    FOR(j, 1, m) a[i][j] = s[j] == 'X', ahas[a[i][j]] = 1;
  }
  FOR(i, 1, n) {
    scanf("%s", s + 1);
    FOR(j, 1, m) b[i][j] = s[j] == 'X', bhas[b[i][j]] = 1;
  }
  if (!ahas[0] && bhas[0]) return puts("0"), 0;
  if (ahas[1] && !bhas[1]) return puts("0"), 0;
  int c;
  FOR(i, 1, n) {
    c = 0;
    FOR(j, 1, m) c += b[i][j];
    if (c <= 1) continue;
    br[i] = 1;
    FOR(j, 1, m) {
      if (a[i][j] == 0 && b[i][j] == 1) return puts("0"), 0;
      if (b[i][j]) bc[j] = 1;
    }
  }
  FOR(j, 1, m) {
    c = 0;
    FOR(i, 1, n) c += b[i][j];
    if (c <= 1) continue;
    bc[j] = 1;
    FOR(i, 1, n) {
      if (a[i][j] == 0 && b[i][j] == 1) return puts("0"), 0;
      if (b[i][j]) br[i] = 1;
    }
  }
  FOR(i, 1, n) {
    if (!br[i]) FOR(j, 1, m) {
        if (!bc[j]) cntr[i] += !a[i][j], cntc[j] += !a[i][j];
      }
  } // cound for O!
  FOR(i, 1, n) {
    FOR(j, 1, m) {
      if (a[i][j] == 1 && b[i][j] == 0) {
        if (br[i] && bc[j])
          ;
        else if (br[i]) {
          mc[j] = 1;
        } else if (bc[j]) {
          mr[i] = 1;
        } else {
          check.pb(mk(i, j)); // white
        }
      } else if (a[i][j] == 0 && b[i][j] == 1) {
        if (br[i] || bc[j]) return puts("0"), 0;
        canr[i] = 1, canc[j] = 1;
      }
    }
  }
  FOR(i, 1, n) {
    FOR(j, 1, m) {
      if (br[i] || bc[j]) continue;
      if (canr[i]) canc[j] = 1;
      if (canc[j]) canr[i] = 1;
      if (a[i][j] == 0 && b[i][j] == 0) canr[i] = canc[j] = 1;
    }
  }
  FOR(i, 1, n) if (mr[i] && !canr[i]) return puts("0"), 0;
  FOR(j, 1, m) if (mc[j] && !canc[j]) return puts("0"), 0;
  for (int i = 0; i < check.size(); i++) {
    const int x = check[i].fi, y = check[i].se;
    if (canr[x] || canc[y])
      ;
    else
      return puts("0"), 0;
  }
  puts("1");
  return 0;
}
