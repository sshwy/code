// Code by Enderturtle
#include <bits/stdc++.h>
#define rep(i, a, b) for (register int i = a; i <= b; ++i)
#define repe(i, a) for (register int i = head[a]; i; i = e[i].nxt)
#define LL long long
#define il inline
#define pii pair<int, int>
#define mp(a, b) make_pair(a, b)
#define N 100010
using namespace std;
il void filejudge() {
  freopen(".in", "r", stdin);
  freopen(".out", "w", stdout);
}
il int read_int() {
  int x = 0;
  bool f = 1;
  char ch = getchar();
  while (!isdigit(ch)) {
    if (ch == '-') f = 0;
    ch = getchar();
  }
  while (isdigit(ch)) {
    x = (x << 3) + (x << 1) + (ch ^ 48);
    ch = getchar();
  }
  return f ? x : -x;
}
il LL read_ll() {
  LL x = 0;
  bool f = 1;
  char ch = getchar();
  while (!isdigit(ch)) {
    if (ch == '-') f = 0;
    ch = getchar();
  }
  while (isdigit(ch)) {
    x = (x << 3) + (x << 1) + (ch ^ 48);
    ch = getchar();
  }
  return f ? x : -x;
}
/*----- head end -----*/
int n, m, p;
LL d[N], s[N], c[N];
LL dp[N];
struct node {
  LL usd, num;
} q[3 * N];

int main() {
  n = read_int();
  m = read_int();
  p = read_int();
  rep(i, 2, n) {
    d[i] = read_ll();
    d[i] += d[i - 1];
  }
  rep(i, 1, m) {
    LL h = read_ll(), t = read_ll();
    c[i] = t - d[h];
  }
  //	rep(i,1,n) cout<<d[i]<<endl;
  //	rep(i,1,m) cout<<c[i]<<endl;
  sort(c + 1, c + 1 + m);
  rep(i, 1, m) s[i] += s[i - 1] + c[i];
  rep(i, 1, m) dp[i] = INT_MAX;
  rep(i, 1, p) {
    q[1] = (node){0, 0};
    int l = 1, r = 1;
    for (register LL j = 1; j <= m; ++j) {
      while (l < r && (q[l + 1].num - q[l].num) <=
                          (c[j] * (q[l + 1].usd - q[l].usd))) { /*cerr<<"*"<<endl;*/
        l++;
      }
      while (l < r && ((s[j] + dp[j] - q[r].num) * (q[r].usd - q[r - 1].usd)) <=
                          (q[r].num - q[r - 1].num) * (j - q[r].usd)) { /*cerr<<"*"*/
        ;
        r--;
      }
      q[++r] = (node){j, dp[j] + s[j]};
      //	cerr<<r<<endl;
      dp[j] = q[l].num - s[j] + c[j] * (j - q[l].usd);
      //	cerr<<dp[j]<<endl;
    }
  }
  printf("%lld", dp[m]);
  return 0;
}