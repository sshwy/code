#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
namespace IO {
  char nc() {
    static char bf[100000], *p1 = bf, *p2 = bf;
    return p1 == p2 && (p2 = (p1 = bf) + fread(bf, 1, 100000, stdin), p1 == p2)
               ? EOF
               : *p1++;
  }
  int rd() {
    int res = 0;
    char c = getchar();
    while (!isdigit(c)) c = getchar();
    while (isdigit(c)) res = res * 10 + c - '0', c = getchar();
    return res;
  }
} // namespace IO
/******************heading******************/
const int N = 1e5 + 5, M = 5e4 + 5;
const lld INF = 0x3f3f3f3f3f3f3f3f;
int n, m, p;
lld d[N], a[N], A[N];
lld f[M][1001];
int main() {
  // freopen(".fin","r",stdin);
  scanf("%d%d%d", &n, &m, &p);
  FOR(i, 2, n) scanf("%lld", &d[i]), d[i] += d[i - 1];
  FOR(i, 1, m) {
    int h, t;
    scanf("%d%d", &h, &t);
    a[i] = t - d[h];
    // printf("a[%d]=%lld\n",i,a[i]);
  }
  sort(a + 1, a + m + 1);
  FOR(i, 1, m) A[i] = A[i - 1] + a[i];
  // printf("a: "); FOR(i,1,m)printf("%lld%c",a[i]," \n"[i==m]);
  FOR(i, 1, m) f[i][0] = INF;
  f[0][0] = 0;
  FOR(j, 1, p) {
    FOR(i, 1, m) {
      f[i][j] = INF;
      FOR(k, 0, i - 1) {
        f[i][j] = min(f[i][j], f[k][j - 1] + A[k] - a[i] * k + a[i] * i - A[i]);
      }
      // printf("f[%d,%d]=%lld\n",i,j,f[i][j]);
    }
  }
  printf("%lld\n", f[m][p]);
  return 0;
}
