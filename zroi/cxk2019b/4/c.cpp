#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
namespace IO {
  char nc() {
    static char bf[100000], *p1 = bf, *p2 = bf;
    return p1 == p2 && (p2 = (p1 = bf) + fread(bf, 1, 100000, stdin), p1 == p2)
               ? EOF
               : *p1++;
  }
  int rd() {
    int res = 0;
    char c = getchar();
    while (!isdigit(c)) c = getchar();
    while (isdigit(c)) res = res * 10 + c - '0', c = getchar();
    return res;
  }
} // namespace IO
/******************heading******************/
const int N = 1e7 + 7, P = 998244353;
int a[N], la;
namespace Subtask1 {
  int n;
  lld F[N];
  bool vis[N];
  void calc(int k) {
    FOR(i, 2, k) {
      if (vis[i]) continue;
      a[++la] = i;
      lld x = i;
      FOR(j, 1, k) {
        F[x] = i, vis[x] = 1;
        x *= i;
        if (x > k) break;
      }
    }
    // FOR(i,2,k)(F[i]+=F[i-1])%=P;
  }
  void go() {
    scanf("%d", &n);
    printf("%lld\n", F[n]);
  }
} // namespace Subtask1
namespace Subtask2 {
  int n;
  void go() {
    scanf("%d", &n);
    lld ans = 0, tot = 0;
    FOR(i, 1, la) {
      lld cur = a[i];
      if (cur * cur > n) break;
      lld cnt = 0, m = cur;
      while (m <= n) ++cnt, (tot += m) %= P, m *= cur;
      (ans += cnt * cur) %= P;
    }
    tot = (1ll * n * (n + 1) / 2 - 1 - tot) % P;
    printf("%lld\n", (ans + tot) % P);
  }
} // namespace Subtask2
int t;
int main() {
  scanf("%d", &t);
  if (t <= 600) {
    Subtask1::calc(40000);
    while (t--) Subtask2::go();
  } else {
    Subtask1::calc(10000000);
    while (t--) Subtask1::go();
  }
  return 0;
}
