#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
namespace IO {
  char nc() {
    static char bf[100000], *p1 = bf, *p2 = bf;
    return p1 == p2 && (p2 = (p1 = bf) + fread(bf, 1, 100000, stdin), p1 == p2)
               ? EOF
               : *p1++;
  }
  int rd() {
    int res = 0;
    char c = getchar();
    while (!isdigit(c)) c = getchar();
    while (isdigit(c)) res = res * 10 + c - '0', c = getchar();
    return res;
  }
} // namespace IO
/******************heading******************/
const int N = 1e5 + 5, M = 1e5 + 5; //,INF=0x3f3f3f3f;
int n, m, p;
lld d[N], a[N], A[N];
lld f[M], g[M];
int q[N], ql, qr;
inline lld X(int k) { return k; }
inline lld K(int i) { return a[i]; }
inline lld Y(int k) { return g[k] + A[k]; }
inline double slope(int p1, int p2) {
  return (Y(p1) - Y(p2)) * 1.0 / (X(p1) - X(p2));
}
int main() {
  cin >> n >> m >> p;
  FOR(i, 2, n) cin >> d[i], d[i] += d[i - 1];
  FOR(i, 1, m) {
    int h, t;
    cin >> h >> t;
    a[i] = t - d[h];
  }
  sort(a + 1, a + m + 1);
  FOR(i, 1, m) A[i] = A[i - 1] + a[i];
  FOR(i, 1, m) { g[i] = a[i] * i - A[i]; }
  g[0] = 0;
  FOR(j, 2, p) { // g -> f
    ql = 1, qr = 0;
    q[++qr] = 0; // init
    FOR(i, 1, m) {
      while (ql < qr && slope(q[ql], q[ql + 1]) < K(i)) ++ql;
      const int k = q[ql];
      f[i] = -K(i) * X(k) + Y(k);
      f[i] += a[i] * i - A[i];
      while (ql < qr && slope(q[qr - 1], q[qr]) > slope(q[qr], i)) --qr;
      q[++qr] = i;
    }

    swap(f, g);
  }
  cout << g[m];
  return 0;
}
