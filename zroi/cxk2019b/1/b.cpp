#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
namespace IO {
  char nc() {
    static char bf[100000], *p1 = bf, *p2 = bf;
    return p1 == p2 && (p2 = (p1 = bf) + fread(bf, 1, 100000, stdin), p1 == p2)
               ? EOF
               : *p1++;
  }
  int rd() {
    int res = 0;
    char c = getchar();
    while (!isdigit(c)) c = getchar();
    while (isdigit(c)) res = res * 10 + c - '0', c = getchar();
    return res;
  }
} // namespace IO
/******************heading******************/
const int N = 1e6 + 500;
int n, m1, m2, q;
char s[N];

struct disjoint {
  int f[N];
  int get(int u) { return f[u] == u ? u : f[u] = get(f[u]); }
  bool find(int u, int v) { return get(u) == get(v); }
  void init(int x) { FOR(i, 0, x) f[i] = i; }
  void merge(int x, int y) { f[get(x)] = get(y); }
} d[21];

int main() {
  scanf("%d%d%d%d", &n, &m1, &m2, &q);
  FOR(i, 1, m1) {
    int p;
    char c[5];
    scanf("%d%s", &p, c);
    // printf("p=%d,c=%c\n",p,c[0]);
    s[p] = c[0];
  }
  FOR(i, 0, 20) d[i].init(n);
  FOR(i, 1, m2) {
    int l1, r1, l2, r2;
    scanf("%d%d%d%d", &l1, &r1, &l2, &r2);
    ROF(j, 20, 0) {
      if (l1 + (1 << j) - 1 <= r1) {
        d[j].merge(l1, l2);
        l1 += 1 << j;
        l2 += 1 << j;
      }
    }
  }
  ROF(j, 20, 0) {
    if ((1 << j) > n) continue;
    FOR(i, 1, n) {
      if (i == d[j + 1].get(i)) continue;
      d[j].merge(i, d[j + 1].get(i));
      if (i + (1 << (j + 1)) - 1 <= n)
        d[j].merge(i + (1 << j), d[j + 1].get(i) + (1 << j));
    }
  }
  FOR(i, 1, n) if (s[i]) s[d[0].get(i)] = s[i];
  FOR(i, 1, q) {
    int p;
    scanf("%d", &p);
    // printf("p=%d\n",p);
    char ans = s[d[0].get(p)];
    printf("%c\n", ans ? ans : '?');
  }

  return 0;
}
