#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
namespace IO {
  char nc() {
    static char bf[100000], *p1 = bf, *p2 = bf;
    return p1 == p2 && (p2 = (p1 = bf) + fread(bf, 1, 100000, stdin), p1 == p2)
               ? EOF
               : *p1++;
  }
  int rd() {
    int res = 0;
    char c = getchar();
    while (!isdigit(c)) c = getchar();
    while (isdigit(c)) res = res * 10 + c - '0', c = getchar();
    return res;
  }
} // namespace IO
/******************heading******************/
const int N = 1e5 + 50;
int t, n, k;
int cnt[N];
int sz[N], sl[N], sr[N];

vector<int> bin[N], ans[N];

void go() {
  scanf("%d%d", &n, &k);
  FOR(i, 1, n) bin[i].clear();
  FOR(i, 1, k) ans[i].clear();
  FOR(i, 1, n) {
    int x;
    scanf("%d", &x);
    bin[x].pb(i);
  }
  lld need = 1, lim = 1; //当前满需要的数的个数/当前一个桶放的数的限制
  int pos = 1;
  FOR(i, 1, n) {
    if (need > n) break;
    int cnt = bin[i].size();
    if (need == 0) {
      while (cnt > 0) --cnt, ans[pos].pb(bin[i][cnt]);
    } else {
      while (cnt >= need) {
        FOR(_, 1, need)-- cnt, ans[pos].pb(bin[i][cnt]);
        if (pos < k)
          ++pos, need = lim;
        else {
          need = 0;
          break;
        }
      }
      while (cnt > 0) --cnt, ans[pos].pb(bin[i][cnt]), need > 0 ? --need : 0;
    }
    lim *= k, need *= k;
  }
  if (pos == k && need == 0) {
    puts("1");
    FOR(i, 1, k) {
      printf("%d ", ans[i].size());
      for (int j = 0; j < ans[i].size(); j++) printf("%d ", ans[i][j]);
      puts("");
    }
  } else {
    puts("0");
  }
}

int main() {
  scanf("%d", &t);
  while (t--) go();
  return 0;
}
