#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
namespace IO {
  char nc() {
    static char bf[100000], *p1 = bf, *p2 = bf;
    return p1 == p2 && (p2 = (p1 = bf) + fread(bf, 1, 100000, stdin), p1 == p2)
               ? EOF
               : *p1++;
  }
  int rd() {
    int res = 0;
    char c = getchar();
    while (!isdigit(c)) c = getchar();
    while (isdigit(c)) res = res * 10 + c - '0', c = getchar();
    return res;
  }
} // namespace IO
/******************heading******************/
const int M = 1e6 + 500, N = 2e5 + 50;
int n, m;

namespace g {
  int e_cnt;
  struct qxx {
    int nex, t, id, id1, id2;
  };
  qxx e[M];
  int h[N], le = 1;
  void add_path(int f, int t, int id, int id1, int id2) {
    e[++le] = (qxx){h[f], t, id, id1, id2}, h[f] = le;
  }

  bool vis[N];
  int a[N], la = 0;
  vector<int> ans[N];
  void dfs(int u) {
    for (int i = h[u]; i; i = h[u]) {
      while (i && vis[e[i].id]) i = e[i].nex;
      h[u] = i;
      const int v = e[i].t, id = e[i].id;
      if (i) vis[id] = 1, dfs(v), a[++la] = i; // i 即 u -> v的边的编号
    }
  }
  void go() {
    for (int i = 1; i < n; i += 2) {
      ++e_cnt;
      add_path(i, i + 1, e_cnt, m + 1,
          m + 1); //表示添加的是无意义的边，求完欧拉回路后就可以不要的
      add_path(i + 1, i, e_cnt, m + 1,
          m + 1); //表示添加的是无意义的边，求完欧拉回路后就可以不要的
    }
    dfs(1);
    int pos = 0, st = la;
    while (st >= 1 && e[a[st]].id1 != m + 1) --st;
    FOR(_, 1, la) {
      const qxx &now = e[a[st]];
      if (now.id1 == m + 1)
        ++pos;
      else
        ans[pos].pb(now.id1), ans[pos].pb(now.id2);
      st--, st < 1 ? st += la : 0;
    }
    puts("1");
    FOR(i, 1, pos) {
      printf("%d ", ans[i].size());
      for (int j = 0; j < ans[i].size(); j++) printf("%d ", ans[i][j]);
      puts("");
    }
  }
} // namespace g
namespace ori {
  struct qxx {
    int nex, t, idx;
  };
  qxx e[M];
  int h[N], le = 1;
  void add_path(int f, int t, int idx) { e[++le] = (qxx){h[f], t, idx}, h[f] = le; }

  bool vis[N];
  int par[N], dep[N], match[M];

  void dfs(int u, int dp, int par_path) {
    vis[u] = 1, dep[u] = dp;
    for (int i = h[u]; i; i = e[i].nex) {
      const int v = e[i].t, idx = e[i].idx;
      if (!vis[v]) par[v] = u, dfs(v, dp + 1, i ^ 1);
    }
    vector<int> path;
    path.clear();
    for (int i = h[u]; i; i = e[i].nex) {
      const int v = e[i].t, idx = e[i].idx;
      if (dep[v] < dep[u]) continue; //保证不是返祖边
      if (match[idx]) continue;      //没有使用过
      path.pb(i);
    }
    for (int i = 0; i + 1 < path.size(); i += 2) {
      int x = path[i], y = path[i + 1];
      ++g::e_cnt;
      g::add_path(e[x].t, e[y].t, g::e_cnt, e[x].idx, e[y].idx);
      g::add_path(e[y].t, e[x].t, g::e_cnt, e[y].idx, e[x].idx);
      match[e[x].idx] = 1, match[e[y].idx] = 1;
    }
    if (path.size() & 1) {
      int tm = path[path.size() - 1];
      ++g::e_cnt;
      g::add_path(e[par_path].t, e[tm].t, g::e_cnt, e[par_path].idx, e[tm].idx);
      g::add_path(e[tm].t, e[par_path].t, g::e_cnt, e[tm].idx, e[par_path].idx);
      match[e[par_path].idx] = 1, match[e[tm].idx] = 1;
    }
  }
} // namespace ori
int dg[N];
int main() {
  scanf("%d%d", &n, &m);
  if (m & 1) return puts("0"), 0;
  FOR(i, 1, m) {
    int u, v;
    scanf("%d%d", &u, &v);
    ori::add_path(u, v, i), ori::add_path(v, u, i);
    dg[u]++, dg[v]++;
  }
  FOR(i, 1, n) if (!(dg[i] & 1)) return puts("0"), 0;
  ori::dfs(1, 1, 0);
  g::go();
  return 0;
}
