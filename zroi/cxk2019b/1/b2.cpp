#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
namespace IO {
  char nc() {
    static char bf[100000], *p1 = bf, *p2 = bf;
    return p1 == p2 && (p2 = (p1 = bf) + fread(bf, 1, 100000, stdin), p1 == p2)
               ? EOF
               : *p1++;
  }
  int rd() {
    int res = 0;
    char c = getchar();
    while (!isdigit(c)) c = getchar();
    while (isdigit(c)) res = res * 10 + c - '0', c = getchar();
    return res;
  }
} // namespace IO
/******************heading******************/
const int N = 2e3 + 500;
int n, m1, m2, q;

int p[N], sp[N];
char sc[N];
map<int, char> s;

struct trans {
  int l, r, x, y, d;
} a[2000];
bool cmp(trans x, trans y) { return x.y < y.y; }

int get(int pos) {
  int res = lower_bound(p + 1, p + m2 + 1, pos) - p;
  if (res < 1 || res > m2 || pos < a[res].x) return -1;
  return pos - (pos - a[res].l) / a[res].d * a[res].d;
}
void jump(int pos, char c) {
  do s[pos] = c, pos = get(pos);
  while (pos != -1);
}
char query(int pos) {
  do {
    if (s.count(pos)) return s[pos];
    pos = get(pos);
  } while (pos != -1);
  return '?';
}
int main() {
  scanf("%d%d%d%d", &n, &m1, &m2, &q);
  FOR(i, 1, m1) {
    int p;
    char c[5];
    scanf("%d%s", &p, c), sp[i] = p, sc[i] = c[0];
  }
  FOR(i, 1, m2) {
    int l1, r1, l2, r2;
    scanf("%d%d%d%d", &l1, &r1, &l2, &r2);
    a[i] = (trans){l1, r1, l2, r2, l2 - l1};
  }
  sort(a + 1, a + m2 + 1, cmp);
  FOR(i, 1, m2) p[i] = a[i].y;
  FOR(i, 1, m1) jump(sp[i], sc[i]);
  FOR(i, 1, q) {
    int p;
    scanf("%d", &p);
    printf("%c\n", query(p));
  }
  return 0;
}
