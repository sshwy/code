#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
namespace IO {
  char nc() {
    static char bf[100000], *p1 = bf, *p2 = bf;
    return p1 == p2 && (p2 = (p1 = bf) + fread(bf, 1, 100000, stdin), p1 == p2)
               ? EOF
               : *p1++;
  }
  int rd() {
    int res = 0;
    char c = getchar();
    while (!isdigit(c)) c = getchar();
    while (isdigit(c)) res = res * 10 + c - '0', c = getchar();
    return res;
  }
} // namespace IO
/******************heading******************/
const int N = 3050, P = 1e9 + 7;
int n;
pii a[N];
bool cmp(pii x, pii y) { return x.se < y.se; }
int f[N][2];
int pw(int a, int m) {
  int res = 1;
  while (m) m & 1 ? res = 1ll * res * a % P : 0, a = 1ll * a * a % P, m >>= 1;
  return res;
}
int main() {
  scanf("%d", &n);
  FOR(i, 1, n) scanf("%d%d", &a[i].fi, &a[i].se);
  sort(a + 1, a + n + 1, cmp);
  f[1][0] = 1, f[1][1] = 1;
  f[0][0] = 1, f[0][1] = 0;
  FOR(i, 2, n) {
    printf("\033[31mi=%d\033[0m\n", i);
    int x = i;
    while (x > 0 && a[x].se >= a[i].fi) --x;
    printf("x=%d\n", x);
    f[i][1] = 1ll * (f[x][0] + f[x][1]) * pw(2, i - x - 1) % P;
    int maxl = a[i].fi;
    ROF(j, i - 1, 0) {
      if (a[j].se >= maxl) {
        f[i][0] += f[j][1];
        printf("%d <- %d\n", i, j);
        maxl = max(maxl, a[j].fi);
      } else
        break;
    }
    printf("f[%d,%d]=%d,f[%d,%d]=%d\n", i, 0, f[i][0], i, 1, f[i][1]);
  }
  printf("%d\n", (f[n][0] + f[n][1]) % P);
  return 0;
}
