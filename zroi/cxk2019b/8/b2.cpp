#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
namespace IO {
  char nc() {
    static char bf[100000], *p1 = bf, *p2 = bf;
    return p1 == p2 && (p2 = (p1 = bf) + fread(bf, 1, 100000, stdin), p1 == p2)
               ? EOF
               : *p1++;
  }
  int rd() {
    int res = 0;
    char c = getchar();
    while (!isdigit(c)) c = getchar();
    while (isdigit(c)) res = res * 10 + c - '0', c = getchar();
    return res;
  }
} // namespace IO
/******************heading******************/
const int N = 1050, P = 998244353;
#define int long long
int n, m;
int a[N];

int pw(int a, int m) {
  int res = 1;
  while (m) m & 1 ? res = 1ll * res * a % P : 0, a = 1ll * a * a % P, m >>= 1;
  return res;
}
int query(int l, int r) {
  // printf("query(%lld,%lld)\n",l,r);
  // FOR(i,l,r)printf("%lld%c",a[i]," \n"[i==r]);
  int lim = (1 << (r - l + 1)) - 1;
  int res = 0;
  FOR(i, 0, lim) {
    int sum = 0, cnt = 0, tot = 0;
    FOR(j, l, r) {
      if ((i >> j - l) & 1) {
        // printf("%lld ",a[j]);
        (sum += a[j]) %= P, ++cnt;
      }
    }
    // printf("sum=%lld,cnt=%lld\n",sum,cnt);
    sum = 1ll * sum * pw(cnt, P - 2) % P;
    FOR(j, l, r) {
      if ((i >> j - l) & 1) (tot += pw(sum - a[j], 2)) %= P;
    }
    tot = 1ll * tot * pw(cnt, P - 2) % P;
    (res += tot) %= P;
  }
  return res;
}
signed main() {
  scanf("%lld%lld", &n, &m);
  FOR(i, 1, m) {
    int op, l, r, v;
    scanf("%lld%lld%lld", &op, &l, &r);
    if (op == 1) {
      scanf("%lld", &v);
      FOR(i, l, r)(a[i] += v) %= P;
    } else {
      printf("%lld\n", query(l, r));
    }
  }
  return 0;
}
