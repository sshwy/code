#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
namespace IO {
  char nc() {
    static char bf[100000], *p1 = bf, *p2 = bf;
    return p1 == p2 && (p2 = (p1 = bf) + fread(bf, 1, 100000, stdin), p1 == p2)
               ? EOF
               : *p1++;
  }
  int rd() {
    int res = 0;
    char c = getchar();
    while (!isdigit(c)) c = getchar();
    while (isdigit(c)) res = res * 10 + c - '0', c = getchar();
    return res;
  }
} // namespace IO
/******************heading******************/
const int N = 5e6 + 500, P = 998244353, SGT = N << 2;
#define int long long
int n, m;
int b[N];

int fac[N], fnv[N]; /*{{{*/
int pw(int a, int m, int p) {
  int res = 1;
  while (m) m & 1 ? res = 1ll * res * a % p : 0, a = 1ll * a * a % p, m >>= 1;
  return res;
}
void pre(int k) {
  fac[0] = 1;
  FOR(i, 1, k) fac[i] = 1ll * i * fac[i - 1] % P;
  fnv[k] = pw(fac[k], P - 2, P);
  ROF(i, k, 1) fnv[i - 1] = 1ll * fnv[i] * i % P;
}
int binom(int n, int m) {
  if (n < m) return 0;
  return 1ll * fac[n] * fnv[m] % P * fnv[n - m] % P;
} /*}}}*/

int A[N], B[N], C[N];
int m2[N], inv[N];
// int P[N],Q[N];

/*
 * a*b=1 mod P
 * P=a*k+r(0<=r<a)
 * -r*b=k
 * b=-k/r
 */

void CB_pre(int x) {
  m2[0] = 1;
  inv[1] = 1;
  FOR(i, 2, x) inv[i] = 1ll * (P - P / i) * inv[P % i] % P;
  FOR(i, 1, x) m2[i] = m2[i - 1] * 2ll % P;
  FOR(i, 1, x) A[i] = (m2[i] - 1ll) * inv[i] % P;
  FOR(i, 1, x) B[i] = (B[i - 1] + (m2[i] - 1ll) * inv[i]) % P;
  FOR(i, 1, x) B[i] = 1ll * B[i] * inv[i] % P;
  FOR(i, 1, x)
  C[i] = ((C[i - 1] + (i - 1ll) * inv[i] % P + (m2[i - 1] - 1ll) -
              inv[i] * (m2[i] - 2ll) % P) %
                 P +
             P) %
         P;
  FOR(i, 1, x) C[i] = 1ll * C[i] * inv[i] % P * inv[i - 1] % P;
}

struct node { //{{{
  int sum, s2, tg;
} t[SGT];
void pushup(int u) {
  t[u].sum = (t[u << 1].sum + t[u << 1 | 1].sum) % P;
  t[u].s2 = (t[u << 1].s2 + t[u << 1 | 1].s2) % P;
}
void appadd(int u, int l, int r, int v) {
  t[u].s2 = (t[u].s2 + 2ll * t[u].sum * v % P + (r - l + 1ll) * v % P * v % P) % P;
  t[u].sum = (t[u].sum + (r - l + 1ll) * v % P) % P;
  t[u].tg = (t[u].tg + v) % P;
}
void pushdown(int u, int l, int r) {
  int mid = (l + r) >> 1;
  if (t[u].tg)
    appadd(u << 1, l, mid, t[u].tg), appadd(u << 1 | 1, mid + 1, r, t[u].tg),
        t[u].tg = 0;
}
void add(int L, int R, int v, int u = 1, int l = 1, int r = n) {
  if (R < l || r < L) return;
  if (L <= l && r <= R) return appadd(u, l, r, v), void();
  pushdown(u, l, r);
  int mid = (l + r) >> 1;
  add(L, R, v, u << 1, l, mid), add(L, R, v, u << 1 | 1, mid + 1, r);
  pushup(u);
}
int query_sum(int L, int R, int u = 1, int l = 1, int r = n) {
  // printf("\033[34mu=%d,l=%d,r=%d,sum=%d\033[0m\n",u,l,r,t[u].sum);
  if (R < l || r < L) return 0;
  if (L <= l && r <= R) return t[u].sum;
  pushdown(u, l, r);
  int mid = (l + r) >> 1;
  return (query_sum(L, R, u << 1, l, mid) + query_sum(L, R, u << 1 | 1, mid + 1, r)) %
         P;
}
int query_sum2(int L, int R, int u = 1, int l = 1, int r = n) {
  if (R < l || r < L) return 0;
  if (L <= l && r <= R) return t[u].s2;
  pushdown(u, l, r);
  int mid = (l + r) >> 1;
  return (query_sum2(L, R, u << 1, l, mid) +
             query_sum2(L, R, u << 1 | 1, mid + 1, r)) %
         P;
} //}}}

int query(int l, int r) {
  int s1 = query_sum(l, r), s2 = query_sum2(l, r), len = r - l + 1;
  int x1 = 1ll * s2 * A[len] % P;
  int x2 = 1ll * s1 * s1 % P * C[len] % P;
  int x3 = 1ll * s2 * C[len] % P;
  int x4 = 1ll * s2 * B[len] % P;
  int res = (x1 - (x2 - x3 + x4)) % P;
  res = (res + P) % P;
  return res;
}
signed main() {
  scanf("%lld%lld", &n, &m);
  pre(n + 1);
  CB_pre(n + 1);
  FOR(i, 1, m) {
    int op, l, r, v;
    scanf("%lld%lld%lld", &op, &l, &r);
    if (op == 1) {
      scanf("%lld", &v);
      // printf("\033[31madd(%lld,%lld,%lld)\033[0m\n",l,r,v);
      add(l, r, v);
    } else {
      printf("%lld\n", query(l, r));
    }
  }
  return 0;
}
