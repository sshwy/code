#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
namespace IO {
  char nc() {
    static char bf[100000], *p1 = bf, *p2 = bf;
    return p1 == p2 && (p2 = (p1 = bf) + fread(bf, 1, 100000, stdin), p1 == p2)
               ? EOF
               : *p1++;
  }
  int rd() {
    int res = 0;
    char c = getchar();
    while (!isdigit(c)) c = getchar();
    while (isdigit(c)) res = res * 10 + c - '0', c = getchar();
    return res;
  }
} // namespace IO
/******************heading******************/
#include <fstream>
const int N = 1000;
int n, a, b, c, t;
int p[N];
int tr[1000][1000];
ifstream fin, fout;
void trans(int id) {
  int t[N];
  FOR(i, 1, n) t[i] = p[tr[id][i]];
  FOR(i, 1, n) p[i] = t[i];
}
void go() {
  FOR(i, 1, n) fin >> p[i];
  int c;
  fout >> c;
  FOR(i, 1, c) {
    int id;
    fout >> id;
    trans(id);
  }
  FOR(i, 1, n) {
    if (p[i] != i) {
      puts("\033[31mYour answer is not correct!\033[0m");
      exit(1);
    }
  }
}
int main(int argc, char **argv) {
  fin.open(argv[1]);
  fout.open(argv[2]);
  int n, a, b, c;
  fin >> n >> a >> b >> c >> t;
  FOR(i, 1, a) FOR(j, 1, n) fout >> tr[i][j];
  while (t--) go();
  return 0;
}
