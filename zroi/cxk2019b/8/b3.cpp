#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
namespace IO {
  char nc() {
    static char bf[100000], *p1 = bf, *p2 = bf;
    return p1 == p2 && (p2 = (p1 = bf) + fread(bf, 1, 100000, stdin), p1 == p2)
               ? EOF
               : *p1++;
  }
  int rd() {
    int res = 0;
    char c = getchar();
    while (!isdigit(c)) c = getchar();
    while (isdigit(c)) res = res * 10 + c - '0', c = getchar();
    return res;
  }
} // namespace IO
/******************heading******************/
const int N = 1050, P = 998244353;
#define int long long
int b[N];

int fac[N], fnv[N];
int pw(int a, int m, int p) {
  int res = 1;
  while (m) m & 1 ? res = 1ll * res * a % p : 0, a = 1ll * a * a % p, m >>= 1;
  return res;
}
void pre(int k) {
  fac[0] = 1;
  FOR(i, 1, k) fac[i] = 1ll * i * fac[i - 1] % P;
  fnv[k] = pw(fac[k], P - 2, P);
  ROF(i, k, 1) fnv[i - 1] = 1ll * fnv[i] * i % P;
}
int binom(int n, int m) {
  assert(m >= 0), assert(n >= 0);
  if (n < m) return 0;
  return 1ll * fac[n] * fnv[m] % P * fnv[n - m] % P;
}
int f1(int m) {
  int res = 0;
  FOR(n, 1, m) { res = (res + 1ll * binom(m - 1, n - 1) * pw(n, P - 2, P) % P) % P; }
  return res;
}
int f2(int m) {
  int res = 0;
  FOR(n, 2, m) {
    int in = pw(n, P - 2, P);
    res = (res + 1ll * binom(m - 2, n - 2) * in % P * in % P) % P;
  }
  return res;
}
int f3(int m) {
  int res = 0;
  FOR(n, 1, m) {
    int in = pw(n, P - 2, P);
    res = (res + 1ll * binom(m - 1, n - 1) * in % P * in % P) % P;
  }
  return res;
}
int query(int l, int r) {
  int s1 = 0, s2 = 0, m = r - l + 1;
  FOR(i, l, r) s1 = (s1 + b[i]) % P, s2 = (s2 + 1ll * b[i] * b[i] % P) % P;
  // printf("sum(%lld,%lld)=%lld,sum2(%lld,%lld)=%lld\n",l,r,s1,l,r,s2);
  int x1 = 1ll * s2 * f1(m) % P;
  int x2 = 1ll * s1 * s1 % P * f2(m) % P;
  int x3 = 1ll * s2 * f2(m) % P;
  int x4 = 1ll * s2 * f3(m) % P;
  int res = (x1 - (x2 - x3 + x4)) % P;
  res = (res + P) % P;
  return res;
}
int n, m;
signed main() {
  // freopen("tmpou","w",stdout);
  scanf("%lld%lld", &n, &m);
  pre(n);
  FOR(i, 1, m) {
    int op, l, r, v;
    scanf("%lld%lld%lld", &op, &l, &r);
    if (op == 1) {
      scanf("%lld", &v);
      FOR(i, l, r) b[i] = (b[i] + v) % P;
      // printf("\033[31madd(%lld,%lld,%lld)\033[0m\n",l,r,v);

    } else {
      printf("%lld\n", query(l, r));
    }
  }
  return 0;
}
