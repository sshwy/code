#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
namespace IO {
  char nc() {
    static char bf[100000], *p1 = bf, *p2 = bf;
    return p1 == p2 && (p2 = (p1 = bf) + fread(bf, 1, 100000, stdin), p1 == p2)
               ? EOF
               : *p1++;
  }
  int rd() {
    int res = 0;
    char c = getchar();
    while (!isdigit(c)) c = getchar();
    while (isdigit(c)) res = res * 10 + c - '0', c = getchar();
    return res;
  }
} // namespace IO
/******************heading******************/
const int N = 20, P = 1e9 + 7;
int n;
pii a[N];
bool cmp(pii x, pii y) { return x.se < y.se; }
int f[N][2];
int pw(int a, int m) {
  int res = 1;
  while (m) m & 1 ? res = 1ll * res * a % P : 0, a = 1ll * a * a % P, m >>= 1;
  return res;
}
bool check(int x, int y) { return !(a[x].se < a[y].fi || a[y].se < a[x].fi); }
int calc(int s) {
  bool vis[N] = {0};
  FOR(i, 1, n) {
    if ((s >> i - 1) & 1) FOR(j, 1, n) {
        if (check(i, j)) vis[j] = 1;
      }
  }
  FOR(i, 1, n) if (!vis[i]) return 0;
  return 1;
}
int ans;
int main() {
  scanf("%d", &n);
  FOR(i, 1, n) scanf("%d%d", &a[i].fi, &a[i].se);
  int lim = 1 << n;
  FOR(i, 0, lim - 1) { ans += calc(i); }
  printf("%d\n", ans);
  return 0;
}
