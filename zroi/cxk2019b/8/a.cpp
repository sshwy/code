#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
namespace IO {
  char nc() {
    static char bf[100000], *p1 = bf, *p2 = bf;
    return p1 == p2 && (p2 = (p1 = bf) + fread(bf, 1, 100000, stdin), p1 == p2)
               ? EOF
               : *p1++;
  }
  int rd() {
    int res = 0;
    char c = getchar();
    while (!isdigit(c)) c = getchar();
    while (isdigit(c)) res = res * 10 + c - '0', c = getchar();
    return res;
  }
} // namespace IO
/******************heading******************/
const int N = 1000;
int n, a, b, c, t;

namespace P { //{{{
  int swp[N], r1[N], r3[N], r8[N], r20[N];
  void init() {
    swp[1] = 2, swp[2] = 1;
    FOR(i, 3, n) swp[i] = i;

    r1[1] = 1;
    FOR(i, 2, n - 1) r1[i] = i + 1;
    r1[n] = 2;

    r3[1] = 1;
    FOR(i, 2, n - 3) r3[i] = i + 3;
    FOR(i, 2, 4) r3[n + i - 4] = i;

    r8[1] = 1;
    FOR(i, 2, n - 8) r8[i] = i + 8;
    FOR(i, 2, 9) r8[n + i - 9] = i;

    r20[1] = 1;
    FOR(i, 2, n - 20) r20[i] = i + 20;
    FOR(i, 2, 21) r20[n + i - 21] = i;

    FOR(i, 1, n) printf("%d%c", swp[i], " \n"[i == n]);
    FOR(i, 1, n) printf("%d%c", r1[i], " \n"[i == n]);
    FOR(i, 1, n) printf("%d%c", r3[i], " \n"[i == n]);
    FOR(i, 1, n) printf("%d%c", r8[i], " \n"[i == n]);
    FOR(i, 1, n) printf("%d%c", r20[i], " \n"[i == n]);
    FOR(i, 1, a - 5) { FOR(j, 1, n) printf("%d%c", j, " \n"[j == n]); }
  }
  void tr(int *p, int typ) {
    int t[N];
    /*
     * 0:swp
     * 1:r1
     * 3:r3
     * 8:r8
     * 20:r20
     */
    if (typ == 0)
      FOR(i, 1, n) t[i] = p[swp[i]];
    else if (typ == 1)
      FOR(i, 1, n) t[i] = p[r1[i]];
    else if (typ == 3)
      FOR(i, 1, n) t[i] = p[r3[i]];
    else if (typ == 8)
      FOR(i, 1, n) t[i] = p[r8[i]];
    else if (typ == 20)
      FOR(i, 1, n) t[i] = p[r20[i]];
    else
      return puts("Error!"), void();
    FOR(i, 1, n) p[i] = t[i];
  }
} // namespace P

int p[N], q[N];
int ans[N], la;
int find2() {
  FOR(i, 1, n) if (p[i] == 2) return i;
  // puts("Error!"),exit(0);
  return 0;
}
// void print_p(char *name){
//    printf(name);
//    FOR(i,1,n)printf("%d%c",p[i]," \n"[i==n]);
//}
int check() { // if good,return 0
  if (p[1] != 1) return 1;
  int p2 = find2();
  if (p2 == 1) return 1;
  // printf("p2=%d\n",p2);
  // print_p("p: ");
  FOR(i, p2, n) if (p[i] != i - p2 + 2) return i;
  FOR(i, 2, p2 - 1) if (p[i] != 1 + n - p2 + i) return i;
  return 0;
}
void trans(int pos) {
  // trans pos to 2's position
  while (pos > 2) {
    if (pos - 2 >= 20)
      P::tr(p, 20), ans[++la] = 5, pos -= 20;
    else if (pos - 2 >= 8)
      P::tr(p, 8), ans[++la] = 4, pos -= 8;
    else if (pos - 2 >= 3)
      P::tr(p, 3), ans[++la] = 3, pos -= 3;
    else
      P::tr(p, 1), ans[++la] = 2, pos--;
  }
}
void go() {
  la = 0;
  FOR(i, 1, n) scanf("%d", &p[i]);
  // print_p("\033[32mp: \033[0m");
  int cur;
  while ((cur = check())) {
    // printf("\033[31mcur=%d\033[0m\n",cur);
    // print_p("\033[32mp: \033[0m");
    // cur:不对的位置
    if (cur == 1) {
      if (p[1] == 2)
        P::tr(p, 0), ans[++la] = 1;
      else {
        // printf("cur=%d\n",cur);
        int pos = find2();
        pos += p[1] - 2;
        pos > n ? pos -= n - 1 : 0;
        trans(pos);
        P::tr(p, 0), ans[++la] = 1;
      }
    } else {
      trans(cur);
      P::tr(p, 0), ans[++la] = 1;
    }
  }
  cur = find2();
  trans(cur);
  // print_p("\033[33mp: \033[0m");
  printf("%d ", la);
  FOR(i, 1, la) printf("%d ", ans[i]);
  puts("");
}
int main() {
  scanf("%d%d%d%d%d", &n, &a, &b, &c, &t);
  P::init();
  FOR(i, 1, t) go();
  return 0;
}
