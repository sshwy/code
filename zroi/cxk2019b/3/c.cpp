#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
namespace IO {
  char nc() {
    static char bf[100000], *p1 = bf, *p2 = bf;
    return p1 == p2 && (p2 = (p1 = bf) + fread(bf, 1, 100000, stdin), p1 == p2)
               ? EOF
               : *p1++;
  }
  int rd() {
    int res = 0;
    char c = getchar();
    while (!isdigit(c)) c = getchar();
    while (isdigit(c)) res = res * 10 + c - '0', c = getchar();
    return res;
  }
} // namespace IO
/******************heading******************/
const int N = 1e5 + 5, M = 2e5 + 5;
int n, m, q;
bool sig[M];

int f[N];
void init(int x) { FOR(i, 0, x) f[i] = i; }
int get(int x) { return f[x] == x ? x : f[x] = get(f[x]); }
void merge(int x, int y) {
  // printf("merge %d to %d\n",x,y);
  f[get(x)] = get(y);
} // merge x to y
bool find(int x, int y) { return get(x) == get(y); }

struct qxx {
  int nex, t, id;
};
qxx e[M];
int h[N], le = 1;
void add_path(int f, int t, int id) { e[++le] = (qxx){h[f], t, id}, h[f] = le; }
int u[N], v[N];
int op[N], cnt[N];
bool vis[N];
void dfs(int u, int p) {
  vis[u] = 1;
  for (int i = h[u]; i; i = e[i].nex) {
    const int v = e[i].t, id = e[i].id;
    if (!sig[id]) continue;
    if (v == p) continue;
    if (find(u, v)) continue;
    merge(v, u), dfs(v, u);
  }
}
vector<int> a[N];
int main() {
  scanf("%d%d%d", &n, &m, &q);
  FOR(i, 1, n - 1) {
    scanf("%d%d", &u[i], &v[i]);
    add_path(u[i], v[i], i), add_path(v[i], u[i], i);
  }
  FOR(i, 1, m) { scanf("%d", &op[i]), a[op[i]].pb(i); }
  FOR(i, 1, n) cnt[i] = 1;
  // FOR(i,1,n){
  //    //printf("i=%d\n",i);
  //    init(n);
  //    memset(sig,0,sizeof(sig));
  //    FOR(j,1,m){
  //        const int k=op[j];
  //        sig[k]^=1;
  //        if(!sig[k])continue;
  //        int fu=get(u[k]),fv=get(v[k]);
  //        if(fu==fv)continue;
  //        if(fu==i)merge(fv,fu),dfs(fv,fv);
  //        if(fv==i)merge(fu,fv),dfs(fu,fu);
  //    }
  //    //FOR(j,1,n){ if(get(j)==i)yacnt[j]++; }
  //}
  FOR(i, 1, q) {
    int x;
    scanf("%d", &x);
    memset(vis, 0, sizeof(vis));
    dfs(x);
    // printf("%d\n",cnt[x]);
  }
  return 0;
}
