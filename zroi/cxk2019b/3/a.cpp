#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
namespace IO {
  char nc() {
    static char bf[100000], *p1 = bf, *p2 = bf;
    return p1 == p2 && (p2 = (p1 = bf) + fread(bf, 1, 100000, stdin), p1 == p2)
               ? EOF
               : *p1++;
  }
  int rd() {
    int res = 0;
    char c = getchar();
    while (!isdigit(c)) c = getchar();
    while (isdigit(c)) res = res * 10 + c - '0', c = getchar();
    return res;
  }
} // namespace IO
/******************heading******************/
const int N = 5000;
int h, w, n;

pii mirror(pii x) { return mk(x.fi, w + 1 - x.se); }
pii trans_odd(pii x, int x0) {
  int t = (x.fi - x0 - 1) / (w - 1) + 1;
  if (t % 2 == 0) x = mk(x.fi, w - x.se);
  x.se += (t - 1) * w;
  return x;
}
int check_right(pii a, pii x) { // check if x is on a's road
  // printf("check_right((%d,%d),(%d,%d))\n",a.fi,a.se,x.fi,x.se);
  // printf("\tx(transed)=(%d,%d)\n",x.fi,x.se);
  x = trans_odd(x, a.fi - (a.se - 1));
  if (x.fi - x.se != a.fi - a.se) return 0;
  return 2 * (((x.fi - (a.fi - (a.se - 1)) - 1) / (w - 1) + 1) & 1) -
         1; //奇数1,偶数-1
}
int query_right(pii x) {
  // printf("query_right(%d,%d)\n",x.fi,x.se);
  // printf("t=%d\n",t);
  // printf("p=%d,q=%d\n",p,q);
  if (x.fi > h) return x.se;
  int t = (x.se - 1 + h - x.fi);
  int p = t / (w - 1) + 1, q = (t) % (w - 1);
  if (p & 1)
    return q + 1 + 1;
  else
    return w - q;
}
int query(pii x, bool pos) {
  if (pos)
    return query_right(x);
  else
    return w + 1 - query_right(mirror(mk(x.fi + 1, x.se)));
}

pii a[N];
int solve(pii cur) {
  bool sta = 0; //整个图的状态
  if (!(cur.se & 1)) cur = mirror(cur), sta ^= 1;
  FOR(j, 1, n) {
    // printf("\033[31m (%d,%d) \033[0m\n",a[j].fi,a[j].se);
    pii x = sta ? mk(a[j].fi, w - a[j].se) : a[j];
    int t;
    if ((t = check_right(cur, x))) {
      if (t == 1)
        cur = mirror(mk(x.fi + 1, x.se)), sta ^= 1;
      else
        cur = x;
    }
    // printf("\033[32mcur=(%d,%d)\033[0m\n",cur.fi,cur.se);
  }
  int res = query_right(cur);
  if (sta) return w + 1 - res;
  return res;
}
int main() {
  scanf("%d%d%d", &h, &w, &n);
  FOR(i, 1, n) { scanf("%d%d", &a[i].fi, &a[i].se); }
  sort(a + 1, a + n + 1);
  FOR(i, 1, w) { // i,1
    printf("%d\n", solve(mk(1, i)));
  }
  return 0;
}
