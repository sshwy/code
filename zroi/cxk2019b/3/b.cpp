#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
namespace IO {
  char nc() {
    static char bf[100000], *p1 = bf, *p2 = bf;
    return p1 == p2 && (p2 = (p1 = bf) + fread(bf, 1, 100000, stdin), p1 == p2)
               ? EOF
               : *p1++;
  }
  int rd() {
    int res = 0;
    char c = getchar();
    while (!isdigit(c)) c = getchar();
    while (isdigit(c)) res = res * 10 + c - '0', c = getchar();
    return res;
  }
} // namespace IO
/******************heading******************/
const int N = 6, P = 1e9 + 7;
int t, n;
int a[N], b[N];

void go() {
  scanf("%d", &n);
  FOR(i, 1, n) scanf("%d", &a[i]);
  FOR(i, 1, n) scanf("%d", &b[i]);
  int x1, y1, x2, y2;
  lld ans = 1;
  x1 = y1 = x2 = y2 = 1, a[1]--, b[1]--;
  while (1) { // x1>=x2 or x1==x2 ^ y1<=y2
    // printf("(%d,%d),(%d,%d)\n",x1,y1,x2,y2);
    // printf("a: ");FOR(i,1,n)printf("%d ",a[i]); puts("");
    // printf("b: ");FOR(i,1,n)printf("%d ",b[i]); puts("");
    if (x1 == x2 && y1 == y2) {
      if (x1 == n && y1 == n) {
        printf("%lld\n", ans);
        return;
      } else if (a[x1] && b[y1]) {
        a[x1]--, b[y1]--;
        x1++, y2++;
        if (!a[x1] || !b[y2]) return (void)puts("0");
        a[x1]--, b[y2]--;
        ans = ans * 2 % P;
      } else if (a[x1]) {
        y1++, y2++;
        if (!b[y1]) return (void)puts("0");
        a[x1]--, b[y1]--;
      } else if (b[y1]) {
        x1++, x2++;
        if (!a[x1]) return (void)puts("0");
        b[y1]--, a[x1]--;
      } else {
        return (void)puts("0");
      }
    } else if (x1 == x2) {
      assert(y1 <= y2);
      if (b[y1]) {
        while (b[y1]) {
          ++x1;
          if (!a[x1]) return (void)puts("0");
          a[x1]--;
          b[y1]--;
        }
      } else {
        y1++;
        if (!b[y1]) return (void)puts("0");
        b[y1]--;
      }
    } else {
      if (a[x2]) {
        while (a[x2]) {
          ++y2;
          if (!b[y2]) return (void)puts("0");
          b[y2]--;
          a[x2]--;
        }
      } else {
        x2++;
        if (!a[x2]) return (void)puts("0");
        a[x2]--;
      }
    }
  }
  puts("Wild");
}
int main() {
  scanf("%d", &t);
  while (t--) go();
  return 0;
}
