#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
namespace IO {
  char nc() {
    static char bf[100000], *p1 = bf, *p2 = bf;
    return p1 == p2 && (p2 = (p1 = bf) + fread(bf, 1, 100000, stdin), p1 == p2)
               ? EOF
               : *p1++;
  }
  int rd() {
    int res = 0;
    char c = getchar();
    while (!isdigit(c)) c = getchar();
    while (isdigit(c)) res = res * 10 + c - '0', c = getchar();
    return res;
  }
} // namespace IO
/******************heading******************/
const int N = 200005;
struct node {
  int x, y;
} a[N];
int h, w, n, i = 1, s[N];
inline int cmp(node a, node b) { return a.x > b.x; }
inline int calc(int h, int w, int x) {
  if (x & 1) {
    if (h + x <= w) return h + x;
    (h += x - w - 1) %= (w << 1);
    if (h < w) return w - h;
    return h - w + 1;
  }
  if (h < x) return x - h;
  (h -= x) %= (w << 1);
  if (h < w) return h + 1;
  return (w << 1) - h;
}
inline int find(int x, int y, int z) {
  return calc(x - ((x & 1) ^ 1), w, y + (z ^ (x & 1) ^ 1));
}
int main() {
  for (scanf("%d%d%d", &h, &w, &n); i <= w; i++) s[i] = calc(h, w, i);
  for (i = 1; i <= n; i++) scanf("%d%d", &a[i].x, &a[i].y);
  sort(a + 1, a + 1 + n, cmp);
  for (i = 1; i <= n; i++)
    swap(s[find(a[i].x, a[i].y, 0)], s[find(a[i].x, a[i].y, 1)]);
  for (i = 1; i <= w; i++) printf("%d\n", s[i]);
}
