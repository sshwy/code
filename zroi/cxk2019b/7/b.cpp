#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
namespace IO {
  char nc() {
    static char bf[100000], *p1 = bf, *p2 = bf;
    return p1 == p2 && (p2 = (p1 = bf) + fread(bf, 1, 100000, stdin), p1 == p2)
               ? EOF
               : *p1++;
  }
  int rd() {
    int res = 0;
    char c = getchar();
    while (!isdigit(c)) c = getchar();
    while (isdigit(c)) res = res * 10 + c - '0', c = getchar();
    return res;
  }
} // namespace IO
/******************heading******************/
int P;
const int N = 2e5 + 5;
int fac[N], fnv[N];
int pw(int a, int m, int p) {
  int res = 1;
  while (m) m & 1 ? res = 1ll * res * a % p : 0, a = 1ll * a * a % p, m >>= 1;
  return res;
}
void pre(int k) {
  fac[0] = 1;
  FOR(i, 1, k) fac[i] = 1ll * i * fac[i - 1] % P;
  fnv[k] = pw(fac[k], P - 2, P);
  ROF(i, k, 1) fnv[i - 1] = 1ll * fnv[i] * i % P;
}
int binom(int n, int m) {
  if (n < m) return 0;
  return 1ll * fac[n] * fnv[m] % P * fnv[n - m] % P;
}
int ans;
int n;
int g[N];

int main() {
  scanf("%d%d", &n, &P);
  pre(n);
  n--;
  FOR(d, 1, n) {
    int lim = n / d + 1;
    FOR(x, 2, lim) {
      // printf("d=%d,x=%d\n",d,x);
      int p = n - (d - 1) * (x - 1), q = x - 1;
      g[d] += -2ll * binom(p + 1, q + 2) % P;
      g[d] < 0 ? g[d] += P : 0;
      g[d] = (g[d] + 1ll * n * binom(p + 1, q + 1)) % P;
    }
  }
  FOR(d, 1, n) { (ans += 1ll * (g[d] - g[d + 1]) * d % P) %= P; }
  printf("%d\n", ans);
  return 0;
}
