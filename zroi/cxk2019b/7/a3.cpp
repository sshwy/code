#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
namespace IO {
  char nc() {
    static char bf[100000], *p1 = bf, *p2 = bf;
    return p1 == p2 && (p2 = (p1 = bf) + fread(bf, 1, 100000, stdin), p1 == p2)
               ? EOF
               : *p1++;
  }
  int rd() {
    int res = 0;
    char c = getchar();
    while (!isdigit(c)) c = getchar();
    while (isdigit(c)) res = res * 10 + c - '0', c = getchar();
    return res;
  }
} // namespace IO
/******************heading******************/
const int BIT = 1 << 21, UNIT_BIT = 1 << (1 << 4);
typedef unsigned long long ull;
typedef unsigned long long bitll[BIT >> 6];
int tr[BIT];
int n, nn;
ull ans, f[UNIT_BIT], g[UNIT_BIT];
char s[BIT + 1];
void bitreverse() {
  FOR(i, 1, nn - 1) tr[i] = (tr[i >> 1] >> 1) | ((i & 1) << n - 1);
  FOR(i, 0, nn - 1) if (i < tr[i]) swap(s[i], s[tr[i]]);
}
bitll a, b, c, d;

char str[65];
char *binstr(int x) {
  FOR(i, 0, 31) str[31 - i] = '0' + ((x >> i) & 1);
  str[32] = 0;
  return str;
}
char *binstr(ull x) {
  FOR(i, 0, 63) str[63 - i] = '0' + ((x >> i) & 1);
  str[64] = 0;
  return str;
}
void print(bitll x, char *name) { /*{{{*/
  if (nn <= 64) {
    printf("%s=%s\n", name, binstr(x[0]));
  } else {
    printf("%s=", name);
    ROF(i, (nn >> 6) - 1, 0) { printf("%s,", binstr(x[i])); }
    puts("");
  }
} /*}}}*/

void calc(ull x, int len) {
  // printf("calc:x=%s\n",binstr(x));
  // printf("len=%d\n",len);
  cout << x << endl;
  if (len <= 16) {
    // printf("x=%llu\n",x);
    return ans += f[x], void();
  }
  len >>= 1;
  ull a = x >> len, b = x & ((1ll << len) - 1);
  // printf("a=%s,b=%s\n",binstr(a),binstr(b));
  calc(a | b, len), calc(a & b, len), calc(a ^ b, len);
}
void dfs(bitll A, int len) {
  print(A, "dfs:A");
  printf("len=%d\n", len);
  if (len <= 64) return calc(A[0], len);
  bitll B;
  len >>= 1;
  FOR(i, 0, (len >> 6) - 1) B[i] = A[i] | A[i + (len >> 6)];
  dfs(B, len);
  FOR(i, 0, (len >> 6) - 1) B[i] = A[i] & A[i + (len >> 6)];
  dfs(B, len);
  FOR(i, 0, (len >> 6) - 1) B[i] = A[i] ^ A[i + (len >> 6)];
  dfs(B, len);
}
void work(ull *f, ull *g, int len) {
  FOR(i, 0, (1 << len) - 1) {
    int a = i >> (len >> 1), b = i & ((1ll << (len >> 1)) - 1);
    // printf("i=%d,a=%d,b=%d\n",i,a,b);
    f[i] = g[a | b] + g[a & b] + g[a ^ b];
  }
}

int main() {
  scanf("%d", &n);
  nn = 1 << n;
  scanf("%s", s);
  // cout<<s<<endl;
  bitreverse();
  ull lim = nn >> 6 ? nn >> 6 : 1;
  FOR(i, 0, lim - 1) { // a[i]
    a[i] = 0;
    FOR(j, 0, (1 << 6) - 1) {
      int k = (i << 6) + j;
      s[k] == '1' ? a[i] |= 1ll << j : 0;
    }
  }
  f[1] = 1;
  FOR(i, 1, 4) {
    swap(g, f);
    work(f, g, 1 << i);
    // FOR(j,0,(1<<(1<<i))-1) printf("f[%s]=%llu\n",binstr(j),f[j]);
    if (nn == (1 << i)) {
      printf("%llu\n", f[a[0]]);
      return 0;
    }
  }
  dfs(a, nn);
  printf("%llu\n", ans);
  return 0;
}
