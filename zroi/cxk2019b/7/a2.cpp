#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
namespace IO {
  char nc() {
    static char bf[100000], *p1 = bf, *p2 = bf;
    return p1 == p2 && (p2 = (p1 = bf) + fread(bf, 1, 100000, stdin), p1 == p2)
               ? EOF
               : *p1++;
  }
  int rd() {
    int res = 0;
    char c = getchar();
    while (!isdigit(c)) c = getchar();
    while (isdigit(c)) res = res * 10 + c - '0', c = getchar();
    return res;
  }
} // namespace IO
/******************heading******************/
#include <bitset>
// n<=15
int n, lim, ans;
ull s;
char str[1 << 16];

ull _and(ull x, int step) { return x & (x >> (1 << step)); }
ull _or(ull x, int step) { return x | (x >> (1 << step)); }
ull _xor(ull x, int step) { return x ^ (x >> (1 << step)); }

int a[1000];
int go(ull s) {
  ans = 0;
  FOR(i, 0, lim - 1) {
    int cur = i;
    ull s1 = s;
    FOR(j, 0, n - 1) {
      if (cur % 3 == 0)
        s1 = _and(s1, j);
      else if (cur % 3 == 1)
        s1 = _or(s1, j);
      else
        s1 = _xor(s1, j);
      cur /= 3;
    }
    if (s1[0]) ans++;
  }
  cout << s << ": " << ans << endl;
  return ans;
}
int main() {
  cin >> n;
  lim = 1;
  FOR(i, 1, n) lim *= 3;
  FOR(i, 0, (1 << (1 << n)) - 1) {
    int j = i;
    s = 0;
    FOR(k, 1, (1 << n)) {
      s <<= 1;
      if (j & 1) s[0] = 1;
      j >>= 1;
    }
    cout << s << endl;
    int res = go();
    if (a[__builtin_popcount(i)] == res)
      ;
    else if (!a[__builtin_popcount(i)])
      a[__builtin_popcount(i)] = res;
    else {
      printf("i=%d\n", i);
      puts("gg"), exit(0);
    }
  }
  printf("%d\n", ans);
  return 0;
}
