#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
namespace IO {
  char nc() {
    static char bf[100000], *p1 = bf, *p2 = bf;
    return p1 == p2 && (p2 = (p1 = bf) + fread(bf, 1, 100000, stdin), p1 == p2)
               ? EOF
               : *p1++;
  }
  int rd() {
    int res = 0;
    char c = getchar();
    while (!isdigit(c)) c = getchar();
    while (isdigit(c)) res = res * 10 + c - '0', c = getchar();
    return res;
  }
} // namespace IO
/******************heading******************/
const int N = 1000;
lld a, b;
deque<pii> q;
int f[N][2], g[N][2];
void go() {
  scanf("%lld%lld", &a, &b);
  if (a == b) {
    puts("0");
    return;
  }

  memset(f, 0x3f, sizeof(f));
  f[a][0] = f[a][1] = 0;
  q.push_back(mk(a, 0)), q.push_back(mk(a, 1));
  while (!q.empty()) {
    pii cur = q.front();
    q.pop_front();
    int u = cur.fi;
    if (u == 0) continue;
    int p = (cur.fi - 1) / 2;
    if (u & 1 && cur.se) {
      f[p][0] = min(f[p][0], f[u][1] + 1);
      f[p][1] = min(f[p][1], f[u][1] + 1);
      q.push_back(mk(p, 0));
      q.push_back(mk(p, 1));
    } else if (u & 1 && !cur.se) {
      f[p][0] = min(f[p][0], f[u][0]);
      f[p][1] = min(f[p][1], f[u][0] + 1);
      q.push_front(mk(p, 0));
      q.push_back(mk(p, 1));
    } else if (cur.se) {
      f[p][1] = min(f[p][1], f[u][1]);
      f[p][0] = min(f[p][0], f[u][1] + 1);
      q.push_front(mk(p, 1));
      q.push_back(mk(p, 0));
    } else {
      f[p][0] = min(f[p][0], f[u][0] + 1);
      f[p][1] = min(f[p][1], f[u][0] + 1);
      q.push_back(mk(p, 0));
      q.push_back(mk(p, 1));
    }
  }

  memset(g, 0x3f, sizeof(g));
  g[b][0] = g[b][1] = 0;
  q.push_back(mk(b, 0)), q.push_back(mk(b, 1));
  while (!q.empty()) {
    pii cur = q.front();
    q.pop_front();
    int u = cur.fi;
    if (u == 0) continue;
    int p = (cur.fi - 1) / 2;
    if (u & 1 && cur.se) {
      g[p][0] = min(g[p][0], g[u][1] + 1);
      g[p][1] = min(g[p][1], g[u][1] + 1);
      q.push_back(mk(p, 0));
      q.push_back(mk(p, 1));
    } else if (u & 1 && !cur.se) {
      g[p][0] = min(g[p][0], g[u][0]);
      g[p][1] = min(g[p][1], g[u][0] + 1);
      q.push_front(mk(p, 0));
      q.push_back(mk(p, 1));
    } else if (cur.se) {
      g[p][1] = min(g[p][1], g[u][1]);
      g[p][0] = min(g[p][0], g[u][1] + 1);
      q.push_front(mk(p, 1));
      q.push_back(mk(p, 0));
    } else {
      g[p][0] = min(g[p][0], g[u][0] + 1);
      g[p][1] = min(g[p][1], g[u][0] + 1);
      q.push_back(mk(p, 0));
      q.push_back(mk(p, 1));
    }
  }

  int ans = 0;
  FOR(i, 1, min(a, b)) {
    int cur = min(f[i * 2 + 1][1] + g[i * 2 + 2][0], f[i][0] + g[i][1] + 1);
    int cur2 = min(f[i * 2 + 1][0] + g[i * 2 + 2][1], f[i][1] + g[i][0] + 1);
    ans = min(ans, min(cur, cur2));
  }
  printf("%d\n", ans);
}
int t;
int main() {
  scanf("%d", &t);
  while (t--) go();
  return 0;
}
