#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
namespace IO {
  char nc() {
    static char bf[100000], *p1 = bf, *p2 = bf;
    return p1 == p2 && (p2 = (p1 = bf) + fread(bf, 1, 100000, stdin), p1 == p2)
               ? EOF
               : *p1++;
  }
  int rd() {
    int res = 0;
    char c = getchar();
    while (!isdigit(c)) c = getchar();
    while (isdigit(c)) res = res * 10 + c - '0', c = getchar();
    return res;
  }
} // namespace IO
/******************heading******************/
const int N = 70;
lld va[N], la, vb[N], lb;
lld fa[N][2], fb[N][2]; // go to this point's leftup/rightup
lld a, b;
bool isleft() {
  FOR(i, 0, min(la, lb) - 1) {
    // va[la-i],vb[lb-i]
    if (va[la - i] == vb[lb - i]) continue;
    return va[la - i] < vb[lb - i];
  }
  if (la == lb)
    return 1;
  else if (la > lb && va[la - lb + 1] * 2 + 1 == va[la - lb])
    return 1;
  else if (la < lb && va[lb - la + 1] * 2 + 2 == va[lb - la])
    return 1;
  else
    return 0;
}
void go() {
  scanf("%lld%lld", &a, &b);
  if (a == b) {
    puts("0");
    return;
  }
  va[la = 1] = a, vb[lb = 1] = b;
  while (va[la]) va[la + 1] = (va[la] - 1) / 2, ++la;
  while (vb[lb]) vb[lb + 1] = (vb[lb] - 1) / 2, ++lb;
  if (!isleft()) swap(va, vb), swap(la, lb), swap(a, b);
  // printf("va: ");FOR(i,1,la)printf("%d%c",va[i]," \n"[i==la]);
  // printf("vb: ");FOR(i,1,lb)printf("%d%c",vb[i]," \n"[i==lb]);

  while (va[la] == vb[lb]) --la, --lb;
  ++la, ++lb; // for LCA
  // printf("va: ");FOR(i,1,la)printf("%d%c",va[i]," \n"[i==la]);
  // printf("vb: ");FOR(i,1,lb)printf("%d%c",vb[i]," \n"[i==lb]);

  fa[1][0] = fa[1][1] = 0, fb[1][0] = fb[1][1] = 0;
  FOR(i, 2, la) {
    if (va[i] * 2 + 1 == va[i - 1]) {
      fa[i][0] = fa[i - 1][0];
      fa[i][1] = min(fa[i - 1][0] + 1, fa[i - 1][1] + 1);
    } else {
      fa[i][1] = fa[i - 1][1];
      fa[i][0] = min(fa[i - 1][0] + 1, fa[i - 1][1] + 1);
    }
  }
  FOR(i, 2, lb) {
    if (vb[i] * 2 + 1 == vb[i - 1]) {
      fb[i][0] = fb[i - 1][0];
      fb[i][1] = min(fb[i - 1][0] + 1, fb[i - 1][1] + 1);
    } else {
      fb[i][1] = fb[i - 1][1];
      fb[i][0] = min(fb[i - 1][0] + 1, fb[i - 1][1] + 1);
    }
  }
  lld ans;
  if (la == 1 && lb == 1) {
    puts("0");
    // assert(0);
    return;
  } else if (la == 1) {
    ans = min(fb[lb - 1][0], fb[lb - 1][1]);
  } else if (lb == 1) {
    ans = min(fa[la - 1][0], fa[la - 1][1]);
  } else {
    ans = min(fa[la][0] + fb[lb][1] + 1, fa[la - 1][1] + fb[lb - 1][0]);
  }
  printf("%lld\n", ans);
}
int t;
int main() {
  scanf("%d", &t);
  while (t--) go();
  return 0;
}
