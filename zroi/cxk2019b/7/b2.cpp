#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
namespace IO {
  char nc() {
    static char bf[100000], *p1 = bf, *p2 = bf;
    return p1 == p2 && (p2 = (p1 = bf) + fread(bf, 1, 100000, stdin), p1 == p2)
               ? EOF
               : *p1++;
  }
  int rd() {
    int res = 0;
    char c = getchar();
    while (!isdigit(c)) c = getchar();
    while (isdigit(c)) res = res * 10 + c - '0', c = getchar();
    return res;
  }
} // namespace IO
/******************heading******************/
int P;
/*namespace DP{
    const int N=405;
    int n=20;
    lld f[N][N][N];
    lld g[N][N];
    lld G[N];
    lld c[N][N];
    int binom(int n,int m){
        if(n<m)return 0;
        if(n==m||m==0)return 1;
        if(c[n][m])return c[n][m];
        return c[n][m]=binom(n-1,m-1)+binom(n-1,m);
    }
    void go(){
        FOR(i,1,N-1)f[0][0][i]=1;
        FOR(i,1,n){
            FOR(j,1,i){
                ROF(k,i/j,1){
                    FOR(x,1,min(i/k,j))
f[i][j][k]+=binom(j,x)*f[i-k*x][j-x][k+1]; f[i][j][k]+=f[i][j][k+1];
                    g[i][j]+=(f[i][j][k]-f[i][j][k+1])*k;
                }
            }
            FOR(j,1,i)G[i]+=g[i][j];
            //printf("\033[31mG[%d]=%lld\033[0m\n",i,G[i]);
        }
    }
}*/
namespace B {
  const int N = 3005;
  int fac[N], fnv[N];
  int pw(int a, int m, int p) {
    int res = 1;
    while (m) m & 1 ? res = 1ll * res * a % p : 0, a = 1ll * a * a % p, m >>= 1;
    return res;
  }
  void pre(int k) {
    fac[0] = 1;
    FOR(i, 1, k) fac[i] = 1ll * i * fac[i - 1] % P;
    fnv[k] = pw(fac[k], P - 2, P);
    ROF(i, k, 1) fnv[i - 1] = 1ll * fnv[i] * i % P;
  }
  int binom(int n, int m) {
    if (n < m) return 0;
    return 1ll * fac[n] * fnv[m] % P * fnv[n - m] % P;
  }
  int g(int n) {
    int res = 0;
    FOR(r, 1, n) {
      int lim = (n - r) / r;
      FOR(k, 0, lim) {
        res += binom(n - (r - 1) * (k + 1) - 1, k);
        res < P ? 0 : res -= P;
      }
    }
    return res;
  }
} // namespace B
int ans;
int n;
int main() {
  scanf("%d%d", &n, &P);
  // DP::go();
  B::pre(n);
  FOR(d, 1, n - 1) { ans = (ans + 1ll * (n - d) * d % P * B::g(d)) % P; }
  // FOR(i,1,20)printf("g(%d)=%d\n",i,B::g(i));
  printf("%d\n", ans);
  return 0;
}
