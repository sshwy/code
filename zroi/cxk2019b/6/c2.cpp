#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
namespace IO {
  char nc() {
    static char bf[100000], *p1 = bf, *p2 = bf;
    return p1 == p2 && (p2 = (p1 = bf) + fread(bf, 1, 100000, stdin), p1 == p2)
               ? EOF
               : *p1++;
  }
  int rd() {
    int res = 0;
    char c = getchar();
    while (!isdigit(c)) c = getchar();
    while (isdigit(c)) res = res * 10 + c - '0', c = getchar();
    return res;
  }
} // namespace IO
/******************heading******************/
const int N = 1e5 + 5;
int p[N], a[N], col[N]; // col[i]: number i's color
int n, t, la;
pii ans[N * 4];
bool vis[N];

map<int, int> mp;        //<color,id>
void add(int x, int y) { //!!!!!! has bug
  ans[++la] = mk(p[x], p[y]);
  // assert(col[x]!=col[y]);
  if (mp.find(col[x]) == mp.end()) mp[col[x]] = x;
  if (mp.find(col[y]) == mp.end()) mp[col[y]] = y;
}
void dfs_del(int u, int st) {
  if (p[u] == st) return;
  dfs_del(p[u], st);
  add(u, p[u]);
  swap(p[u], p[p[u]]);
}
bool proc(int x) {
  printf("\033[31mproc(%d)\033[0m\n", x);
  if (p[x] == x) {
    if (mp.find(col[x]) == mp.end()) mp[col[x]] = x;
    return 1; // correct
  }

  int c = col[x], fl = 0, len = 1;
  vis[x] = 1;
  printf("member: %d ", x);
  for (int i = p[x]; i != x; i = p[i]) {
    printf("%d ", i);
    if (col[i] != c) fl = 1;
    vis[i] = 1, ++len;
  }
  printf("\nlen=%d\n", len);
  if (!fl) { return 0; }
  if (len == 2) {
    add(x, p[x]);
    swap(p[x], p[p[x]]);
    return 1;
  }
  int ed = 1;
  for (int i = p[x]; ed; i = p[i]) {
    if (len <= 2) break;
    int j = p[i], k = p[j];
    while (len > 2 && col[i] != col[j] && col[i] != col[k]) {
      add(i, j);
      // printf("del %d\n",j);
      if (j == x) ed = 0, x = i;
      swap(p[i], p[j]), j = p[i], k = p[j], --len;
    }
    if (i == x) break;
  }
  if (len == 2) {
    add(x, p[x]);
    swap(p[x], p[p[x]]);
    return 1;
  }
  dfs_del(x, x);
  return 1;
}
int b[N], lb, bcol[N];
bool markb[N];
int getnot(int c) { // return a node that color is not c
  int res;
  map<int, int>::iterator it = mp.begin();
  if (it->fi != c)
    res = it->se;
  else
    ++it, res = it->se;
  printf("getnot(%d)=%d\n", c, res);
  return res;
}
void proc2(int x) {
  printf("\033[32mproc2(%d)\033[0m\n", x);
  int y = getnot(col[x]), len = 1;
  // assert(p[y]==y);
  add(x, y); // x -> y
  swap(p[x], p[y]);
  printf("member: %d ", y);
  for (int i = p[y]; i != y; i = p[i]) {
    printf("%d ", i);
    ++len;
  }
  printf("\nlen=%d\n", len);
  while (len > 2) {
    add(y, p[y]);
    swap(p[y], p[p[y]]);
    --len;
  }
  if (len == 2) {
    add(y, p[y]);
    swap(p[y], p[p[y]]);
  }
}
void work() {
  FOR(i, 1, n) {
    if (!vis[i]) {
      bool res = proc(i);
      if (!res) b[++lb] = i, bcol[lb] = col[i], markb[i] = 1;
    }
  }
  // printf("b: "); FOR(i,1,lb)printf("%d%c",b[i]," \n"[i==lb]);
  FOR(i, 1, n) {
    if (markb[i]) { proc2(i); }
  }
}
int main() {
  scanf("%d%d", &n, &t);
  FOR(i, 1, n) {
    scanf("%d", &a[i]), p[a[i]] = i; // a[i] -> i
  }
  FOR(i, 1, n) printf("p[%d]=%d\n", i, p[i]);
  FOR(i, 1, n) {
    int x;
    scanf("%d", &x), col[a[i]] = x;
  }
  FOR(i, 1, n) printf("col[%d]=%d\n", i, col[i]);
  work();
  printf("%d\n", la);
  FOR(i, 1, la) printf("%d %d\n", ans[i].fi, ans[i].se);
  return 0;
}
