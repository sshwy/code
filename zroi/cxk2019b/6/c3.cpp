#include <bits/stdc++.h>
#define debug(...) fprintf(stderr, __VA_ARGS__)
using namespace std;

const int maxn = 1e5;
int n, a[maxn + 3], c[maxn + 3], p[maxn + 3];
int all, lft[maxn + 3], rht[maxn + 3];
int m, c1, c2, p1, p2, pos[maxn + 3];
bool vis[maxn + 3];
priority_queue<pair<int, int>> H;

struct circle {
  int col, pos;
  circle(int col = 0, int pos = 0)
      : col(col)
      , pos(pos) {}
  friend bool operator<(const circle &a, const circle &b) {
    return a.col == b.col ? a.pos < b.pos : a.col < b.col;
  }
} cir[maxn + 3];

void settings() {
#ifndef ONLINE_JUDGE
  freopen("input.txt", "r", stdin);
  freopen("output.txt", "w", stdout);
#endif
}

void add(int u, int v) {
  u = p[u], v = p[v];
  if (u > v) { swap(u, v); }
  all++;
  lft[all] = u;
  rht[all] = v;
}

void true_swap(int u, int v) { swap(p[u], p[v]); }

void read_in() {
  scanf("%d %*d", &n);
  for (int i = 1; i <= n; i++) {
    scanf("%d", &a[i]);
    p[a[i]] = i;
  }
  for (int i = 1; i <= n; i++) { scanf("%d", &c[a[i]]); }
  bool flag = true;
  for (int i = 1; i <= n; i++) { flag &= a[i] == i; }
  if (flag) {
    puts("0");
    exit(0);
  }
  flag = true;
  for (int i = 1; i < n; i++) { flag &= c[i] == c[i + 1]; }
  if (flag) {
    puts("-1");
    exit(0);
  }
}

void prework() {
  c1 = c[1], p1 = 1;
  for (int i = 2; i <= n; i++) {
    if (c[i] != c1) {
      c2 = c[i];
      p2 = i;
      break;
    }
  }
  for (int i = 1; i <= n; i++) {
    if (vis[i]) continue;
    vis[i] = true;
    int x = p[i], t = c[i], len = 1;
    bool flag = true;
    while (x != i) {
      vis[x] = true;
      len++;
      if (c[x] != t) { flag = false; }
      x = p[x];
    }
    if (flag && len >= 2) { cir[++m] = circle(t, i); }
  }
  sort(cir + 1, cir + m + 1);
  // output c, p
  // debug("%d %d\n%d %d\n", c1, p1, c2, p2);
}

int get(int x) { return cir[pos[x]++].pos; }

void eliminate() {
  for (int i = 1, k = 1; k <= n; k++) {
    pos[k] = i;
    int cnt = 0;
    while (cir[i].col == k) { cnt++, i++; }
    if (cnt) { H.push(make_pair(cnt, k)); }
  }
  for (pair<int, int> u, v; H.size() >= 2;) {
    u = H.top(), H.pop();
    v = H.top(), H.pop();
    int p = u.first;
    int q = v.first;
    int x = get(u.second);
    int y = get(v.second);
    add(x, y);
    true_swap(x, y);
    if (p > 1) { H.push(make_pair(p - 1, u.second)); }
    if (q > 1) { H.push(make_pair(q - 1, v.second)); }
  }
  if (!H.empty()) {
    int cnt = H.top().first, col = H.top().second;
    if (c1 == col) {
      swap(c1, c2);
      swap(p1, p2);
    }
    for (int i = 0, t; i < cnt; i++) {
      t = get(col);
      add(p1, t);
      true_swap(p1, t);
    }
  }
}

void solve_cir(int i, int col, int pos) {
  // print circle
  /*
  debug("%d %d %d\n", i, a[i], c[i]);
  int t = a[i];
  while (t != i) {
    debug("%d %d %d\n", t, a[t], c[t]);
    t = a[t];
  }
  */
  int x = a[i], y = i;
  while (x != pos) {
    if (c[x] == col) {
      y = x;
    } else {
      add(x, y);
      true_swap(x, y);
    }
    x = a[x];
  }
  x = a[pos];
  while (x != pos) {
    if (c[x] == col) {
      add(pos, x);
      true_swap(pos, x);
    }
    x = a[x];
  }
}

void solve() {
  for (int i = 1; i <= n; i++) {
    vis[i] = false;
    a[i] = p[i];
  }
  for (int i = 1; i <= n; i++) {
    if (vis[i]) continue;
    vis[i] = true;
    int x = a[i], col = c[i], pos = 0, len = 1;
    while (x != i) {
      if (c[x] != col) { pos = x; }
      len++;
      vis[x] = true;
      x = a[x];
    }
    if (len >= 2) { solve_cir(i, col, pos); }
  }
}

void print_ans() {
  printf("%d\n", all);
  for (int i = 1; i <= all; i++) { printf("%d %d\n", lft[i], rht[i]); }
}

int main() {
  // freopen(".2fin","r",stdin); freopen("g.txt","w",stdout);
  // settings();
  read_in();
  prework();
  eliminate();
  solve();
  print_ans();
  // for (int i = 1; i <= n; i++) {
  // 	printf("%d %d %d\n", i, p[i], c[i]);
  // }
  return 0;
}
