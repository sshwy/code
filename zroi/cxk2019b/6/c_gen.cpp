#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
namespace IO {
  char nc() {
    static char bf[100000], *p1 = bf, *p2 = bf;
    return p1 == p2 && (p2 = (p1 = bf) + fread(bf, 1, 100000, stdin), p1 == p2)
               ? EOF
               : *p1++;
  }
  int rd() {
    int res = 0;
    char c = getchar();
    while (!isdigit(c)) c = getchar();
    while (isdigit(c)) res = res * 10 + c - '0', c = getchar();
    return res;
  }
} // namespace IO
/******************heading******************/
using namespace RA;
const int N = 1e5 + 400;
int a[N], seg;
int b[N];
int main() {
  srand(clock());
  int n = r(10000, 100000), t = 1;
  printf("%d %d\n", n, t);
  seg = 10 * sqrt(n) + 1;
  FOR(i, 2, seg - 1) b[i] = r(1, n);
  b[1] = 1, b[seg] = n + 1;
  sort(b + 1, b + seg + 1);
  FOR(i, 1, n) a[i] = i;
  FOR(i, 1, seg - 1) random_shuffle(a + b[i], a + b[i + 1]);
  // random_shuffle(a+1,a+n+1);
  FOR(i, 1, n) printf("%d%c", a[i], " \n"[i == n]);
  // FOR(i,1,seg-1) FOR(_,b[i],b[i+1]-1)printf("%d ",i);
  FOR(i, 1, n) printf("%d%c", r(1, min(n, 10)), " \n"[i == n]);
  return 0;
}
