#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
namespace IO {
  char nc() {
    static char bf[100000], *p1 = bf, *p2 = bf;
    return p1 == p2 && (p2 = (p1 = bf) + fread(bf, 1, 100000, stdin), p1 == p2)
               ? EOF
               : *p1++;
  }
  int rd() {
    int res = 0;
    char c = getchar();
    while (!isdigit(c)) c = getchar();
    while (isdigit(c)) res = res * 10 + c - '0', c = getchar();
    return res;
  }
} // namespace IO
/******************heading******************/
const int N = 2e5 + 50, M = 22, MM = 1 << 21;
int n, m, mm;
int a[M], b[M], c[M];
int in[N];
int g1[MM], g[MM], g2[MM], f[MM];
lld dp[MM];

void hyp(int *f) { //高维后缀和
  FOR(i, 0, m - 1)
  FOR(j, 0, mm - 1)
  if ((j >> i) & 1) f[j - (1 << i)] += f[j];
}
void sub(int *f) { //子集前缀和
  FOR(i, 0, m - 1)
  FOR(j, 0, mm - 1)
  if ((j >> i) & 1) f[j] += f[j - (1 << i)];
}
char res[100];
char *bintr(int x) {
  FOR(i, 0, m - 1) res[i] = '0' + (x & 1), x >>= 1;
  reverse(res, res + m);
  return res;
}
void print(int *f, char *name) {
  FOR(j, 0, mm - 1) printf("%s[%s]=%d\n", name, bintr(j), f[j]);
  puts("");
}
void print(lld *f, char *name) {
  FOR(j, 0, mm - 1) printf("%s[%s]=%lld\n", name, bintr(j), f[j]);
  puts("");
}
int binom_2(int x) { return x * (x - 1ll) / 2; }
int popsig(int x) { return __builtin_popcount(x) & 1 ? -1 : 1; }
unsigned int cost(int i, int x) {
  return (unsigned int)(1ll * a[i] * x * x + 1ll * b[i] * x + c[i]);
}
int main() {
  scanf("%d%d", &n, &m);
  mm = 1 << m;
  FOR(i, 1, m) {
    int s, x;
    scanf("%d%d%d%d", &a[i], &b[i], &c[i], &s);
    FOR(j, 1, s) {
      scanf("%d", &x);
      in[x] |= 1 << i - 1;
    }
  }
  FOR(i, 1, n) {
    if (in[i]) g1[in[i]]++;
  }
  hyp(g1);
  FOR(i, 0, mm - 1) g[i] = binom_2(g1[i]);
  FOR(i, 1, mm - 1) f[i] = -popsig(i) * g[i]; // 1!!!
  sub(f);
  FOR(i, 0, mm - 1) {
    FOR(j, 1, m) {
      if ((i >> j - 1) & 1) {
        dp[i] =
            max(dp[i], dp[i ^ (1 << j - 1)] + cost(j, f[i] - f[i ^ (1 << j - 1)]));
      }
    }
  }
  printf("%lld\n", dp[mm - 1]);
  return 0;
}
