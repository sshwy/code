#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
namespace IO {
  char nc() {
    static char bf[100000], *p1 = bf, *p2 = bf;
    return p1 == p2 && (p2 = (p1 = bf) + fread(bf, 1, 100000, stdin), p1 == p2)
               ? EOF
               : *p1++;
  }
  int rd() {
    int res = 0;
    char c = getchar();
    while (!isdigit(c)) c = getchar();
    while (isdigit(c)) res = res * 10 + c - '0', c = getchar();
    return res;
  }
} // namespace IO
/******************heading******************/
#include <fstream>
const int N = 1e5 + 400;
pii a[N];
int main(int argc, char **argv) { // .fin .fout
  ifstream fin(argv[1]);
  ifstream fout(argv[2]);
  int n, t;
  fin >> n >> t;
  FOR(i, 1, n) fin >> a[i].fi;
  FOR(i, 1, n) fin >> a[i].se;
  fout >> t;
  FOR(i, 1, t) {
    int x, y;
    fout >> x >> y;
    if (!x || !y) {
      printf("\033[31mMaybe there's something wrong with your "
             "generator!\033[0m\n");
      return 1;
    }
    if (a[x].se == a[y].se) {
      printf("\033[31mYou swap the same color!\033[0m\n");
      return 1;
    }
    swap(a[x], a[y]);
  }
  FOR(i, 1, n) if (a[i].fi != i) {
    printf("\033[31mYour final sequence is not correct!\033[0m\n");
    return 1;
  }
  return 0;
}
