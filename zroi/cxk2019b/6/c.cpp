#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
namespace IO {
  char nc() {
    static char bf[100000], *p1 = bf, *p2 = bf;
    return p1 == p2 && (p2 = (p1 = bf) + fread(bf, 1, 100000, stdin), p1 == p2)
               ? EOF
               : *p1++;
  }
  int rd() {
    int res = 0;
    char c = getchar();
    while (!isdigit(c)) c = getchar();
    while (isdigit(c)) res = res * 10 + c - '0', c = getchar();
    return res;
  }
} // namespace IO
/******************heading******************/
const int N = 1e5 + 5;
int p[N], a[N], col[N];
int n, t, la;
pii ans[N * 4];
bool vis[N];

map<int, int> mp;
void add(int x, int y) {
  if (!x || !y) puts("-1"), exit(0);
  ans[++la] = mk(p[x], p[y]);
  if (mp.find(col[x]) == mp.end()) mp[col[x]] = x;
  if (mp.find(col[y]) == mp.end()) mp[col[y]] = y;
}
void dfs_del(int u, int st) {
  if (p[u] == st) return;
  dfs_del(p[u], st);
  add(u, p[u]);
  swap(p[u], p[p[u]]);
}
bool proc(int x) {
  if (p[x] == x) {
    if (mp.find(col[x]) == mp.end()) mp[col[x]] = x;
    return 1;
  }
  int c = col[x], fl = 0, len = 1;
  vis[x] = 1;
  for (int i = p[x]; i != x; i = p[i]) {
    if (col[i] != c) fl = 1;
    vis[i] = 1, ++len;
  }
  if (!fl) return 0;
  if (len == 2) {
    add(x, p[x]);
    swap(p[x], p[p[x]]);
    return 1;
  }
  int ed = 1;
  for (int i = p[x]; ed; i = p[i]) {
    if (len <= 2) break;
    int j = p[i], k = p[j];
    while (len > 2 && col[i] != col[j] && col[i] != col[k]) {
      add(i, j);
      if (j == x) ed = 0, x = i;
      swap(p[i], p[j]), j = p[i], k = p[j], --len;
    }
    if (i == x) break;
  }
  if (len == 2) {
    add(x, p[x]);
    swap(p[x], p[p[x]]);
    return 1;
  }
  dfs_del(x, x);
  return 1;
}
int getnot(int c) {
  int res;
  map<int, int>::iterator it = mp.begin();
  if (it->fi != c)
    res = it->se;
  else
    ++it, res = it->se;
  return res;
}
void proc2(int x) {
  int y = getnot(col[x]), len = 1;
  add(x, y);
  swap(p[x], p[y]);
  for (int i = p[y]; i != y; i = p[i]) ++len;
  while (len > 2) {
    add(y, p[y]);
    swap(p[y], p[p[y]]);
    --len;
  }
  if (len == 2) {
    add(y, p[y]);
    swap(p[y], p[p[y]]);
  }
}
queue<int> qcol[N];
priority_queue<pii> q;
bool markb[N];
void pro_que(int x) {
  while (qcol[x].size()) proc2(qcol[x].front()), qcol[x].pop();
}
void match(int x, int y) { add(x, y), swap(p[x], p[y]), proc(x); }
void pro_match(int x, int y) {
  while (qcol[x].size() && qcol[y].size()) {
    int tx = qcol[x].front(), ty = qcol[y].front();
    qcol[x].pop();
    qcol[y].pop();
    match(tx, ty);
  }
}
void work() {
  FOR(i, 1, n) {
    if (!vis[i]) {
      bool res = proc(i);
      if (!res) markb[i] = 1, qcol[col[i]].push(i);
    }
  }
  FOR(i, 1, n) if (qcol[i].size()) q.push(mk(qcol[i].size(), i));
  while (!q.empty()) {
    pii x = q.top();
    q.pop();
    if (q.empty()) {
      pro_que(x.se);
      break;
    }
    pii y = q.top();
    q.pop();
    pro_match(x.se, y.se);
    if (qcol[x.se].size())
      q.push(mk(qcol[x.se].size(), x.se));
    else if (qcol[y.se].size())
      q.push(mk(qcol[y.se].size(), y.se));
  }
}
int main() {
  scanf("%d%d", &n, &t);
  FOR(i, 1, n) scanf("%d", &a[i]), p[a[i]] = i;
  int c = 0, cnt = 0;
  FOR(i, 1, n) {
    int x;
    scanf("%d", &x), col[a[i]] = x;
    if (x != c) ++cnt, c = x;
  }
  if (cnt < 2) return puts("-1"), 0;
  work();
  printf("%d\n", la);
  FOR(i, 1, la) printf("%d %d\n", ans[i].fi, ans[i].se);
  return 0;
}
