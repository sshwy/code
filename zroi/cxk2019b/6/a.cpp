#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
namespace IO {
  char nc() {
    static char bf[100000], *p1 = bf, *p2 = bf;
    return p1 == p2 && (p2 = (p1 = bf) + fread(bf, 1, 100000, stdin), p1 == p2)
               ? EOF
               : *p1++;
  }
  int rd() {
    int res = 0;
    char c = getchar();
    while (!isdigit(c)) c = getchar();
    while (isdigit(c)) res = res * 10 + c - '0', c = getchar();
    return res;
  }
} // namespace IO
/******************heading******************/
const int N = 1e6 + 50;
char s[N];
char st[N];
int tp, n;
lld ans, pre[N], cnt[N];
void get(int x) {
  if (s[x - 1] == s[x]) {
    pre[x] = x - 2;
    return;
  } else {
    int t = pre[x - 1];
    while (t && s[t] != s[x]) t = pre[t];
    pre[x] = t - 1;
  }
}
int main() {
  scanf("%s", s + 1);
  n = strlen(s + 1);
  pre[0] = -1;
  FOR(i, 1, n) get(i);
  // printf("pre: ");FOR(i,0,n)printf("%d%c",pre[i]," \n"[i==n]);
  FOR(i, 1, n) {
    if (~pre[i]) cnt[i] = cnt[pre[i]] + 1;
    ans += cnt[i];
  }
  printf("%lld\n", ans);
  return 0;
}
