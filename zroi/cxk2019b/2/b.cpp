#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
namespace IO {
  char nc() {
    static char bf[100000], *p1 = bf, *p2 = bf;
    return p1 == p2 && (p2 = (p1 = bf) + fread(bf, 1, 100000, stdin), p1 == p2)
               ? EOF
               : *p1++;
  }
  int rd() {
    int res = 0;
    char c = getchar();
    while (!isdigit(c)) c = getchar();
    while (isdigit(c)) res = res * 10 + c - '0', c = getchar();
    return res;
  }
} // namespace IO
/******************heading******************/
#define int long long
const int N = 2e5 + 5;
int n, m;
int a[N], a2[N];
int p[N];

struct ss {
  int u, v, w, id;
} s[N];
bool cmp(ss x, ss y) { return x.w < y.w; }

int f[N];
void init(int x) { FOR(i, 0, x) f[i] = i; }
int get(int u) { return f[u] == u ? u : f[u] = get(f[u]); }
int find(int x, int y) { return get(x) == get(y); }
void merge(int x, int y) { f[get(x)] = get(y); }

struct qxx {
  int nex, t, v, id;
};
qxx e[N * 2];
int h[N], le = 1;
void add_path(int f, int t, int v, int id) {
  e[++le] = (qxx){h[f], t, v, id}, h[f] = le;
}

bool vis[N], ev[N];
int val[N], sum[N], ans[N], la;
int sumdfs(int u) {
  vis[u] = 1, sum[u] = a[u];
  for (int i = h[u]; i; i = e[i].nex) {
    const int v = e[i].t, w = e[i].v;
    if (!vis[v]) sum[u] += sumdfs(v) - w;
  }
  return sum[u];
}
void dfs(int u) { //返回缩点后的点权
  vis[u] = 1;
  for (int i = h[u]; i; i = e[i].nex) {
    const int v = e[i].t, w = e[i].v, id = e[i].id;
    if (vis[v]) continue;
    if (sum[v] - w >= 0) dfs(v), ans[++la] = id;
  }
  for (int i = h[u]; i; i = e[i].nex) {
    const int v = e[i].t, w = e[i].v, id = e[i].id;
    if (vis[v]) continue;
    if (sum[v] - w < 0) {
      if (sum[v] >= 0)
        dfs(v), ans[++la] = id;
      else
        ans[++la] = id, dfs(v);
    }
  }
}
main() {
  scanf("%lld%lld", &n, &m);
  FOR(i, 1, n) scanf("%lld", &a[i]);
  FOR(i, 1, m) scanf("%lld%lld%lld", &s[i].u, &s[i].v, &s[i].w), s[i].id = i;
  sort(s + 1, s + m + 1, cmp);
  init(n);
  FOR(i, 1, m) {
    int u = s[i].u, v = s[i].v, w = s[i].w, id = s[i].id;
    if (find(u, v)) continue;
    merge(u, v), add_path(u, v, w, id), add_path(v, u, w, id);
  }
  sumdfs(1);
  if (sum[1] < 0) return puts("0"), 0;
  memset(vis, 0, sizeof(vis)), dfs(1), puts("1");
  FOR(i, 1, la) printf("%lld ", ans[i]);
  return 0;
}
