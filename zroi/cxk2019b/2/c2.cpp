#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
namespace IO {
  char nc() {
    static char bf[100000], *p1 = bf, *p2 = bf;
    return p1 == p2 && (p2 = (p1 = bf) + fread(bf, 1, 100000, stdin), p1 == p2)
               ? EOF
               : *p1++;
  }
  int rd() {
    int res = 0;
    char c = getchar();
    while (!isdigit(c)) c = getchar();
    while (isdigit(c)) res = res * 10 + c - '0', c = getchar();
    return res;
  }
} // namespace IO
/******************heading******************/
const int N = 100;
int c[N], l = 0, xx;
int p[] = {2, 3, 5, 7, 11, 13, 17, 18, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71,
    73, 79, 83, 89, 97};
int a[N]; //指数的上界
bool flag;

void go() {
  printf("a: ");
  FOR(i, 0, l) printf("%d ", a[i]);
  puts("");
}
void dfs_a(int k) {
  if (flag) return;
  if (k > l) {
    go();
    return;
  }
  if (flag) return;
  for (int i = (c[k] + 1) / 2 * 2; !flag; i += 2) {
    a[k] = i;
    dfs_a(k + 1);
    if (flag) return;
  }
}
int f(int x) {
  flag = 0;
  for (int i = 0; p[i] <= x; i++) {
    if (x % p[i]) continue;
    l = i;
    while (x % p[i] == 0) c[i]++, x /= p[i];
  }
  xx = x;
  // if(x>1)p[++l]=x,c[l]=1;
  dfs_a(0);
}

int main() { return 0; }
