#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
namespace IO {
  char nc() {
    static char bf[100000], *p1 = bf, *p2 = bf;
    return p1 == p2 && (p2 = (p1 = bf) + fread(bf, 1, 100000, stdin), p1 == p2)
               ? EOF
               : *p1++;
  }
  int rd() {
    int res = 0;
    char c = getchar();
    while (!isdigit(c)) c = getchar();
    while (isdigit(c)) res = res * 10 + c - '0', c = getchar();
    return res;
  }
} // namespace IO
/******************heading******************/
const int N = 1e6 + 6, INF = 0x3f3f3f3f;

namespace MCMF {
  struct qxx {
    int nex, t, v, c;
  };
  qxx e[N];
  int h[N], le = 1;
  void add_path(int f, int t, int v, int c) {
    e[++le] = (qxx){h[f], t, v, c}, h[f] = le;
  }
  void add_flow(int f, int t, int v, int c) {
    add_path(f, t, v, c), add_path(t, f, 0, -c);
  }

  bool vis[N];
  queue<int> q;
  int d[N], pre[N], incf[N];
  int s, t;
  bool spfa() {
    memset(d, -1, sizeof(d));
    q.push(s), d[s] = 0, incf[s] = INF, incf[t] = 0;
    while (!q.empty()) {
      int u = q.front();
      q.pop();
      vis[u] = 0;
      for (int i = h[u]; i; i = e[i].nex) {
        const int v = e[i].t, w = e[i].v, c = e[i].c;
        if (!w || d[v] >= d[u] + c) continue;
        d[v] = d[u] + c, incf[v] = min(incf[u], w), pre[v] = i;
        if (!vis[v]) q.push(v), vis[v] = 1;
      }
    }
    return incf[t];
  }
  int maxflow, maxcost;
  void update() {
    maxflow += incf[t];
    for (int u = t; u != s; u = e[pre[u] ^ 1].t) {
      e[pre[u]].v -= incf[t], e[pre[u] ^ 1].v += incf[t];
      maxcost += incf[t] * e[pre[u]].c;
    }
  }
  void go() {
    while (spfa()) update();
  }
} // namespace MCMF
int n, r1, r2, q1, q2;
int c[N];
int lim[N];
namespace T {
  struct qxx {
    int nex, t;
  };
  qxx e[N];
  int h[N], le = 1;
  void add_path(int f, int t) { e[++le] = (qxx){h[f], t}, h[f] = le; }
  bool vis[N];
  void dfs(int u) {
    vis[u] = 1;
    for (int i = h[u]; i; i = e[i].nex) {
      const int v = e[i].t;
      if (vis[v]) continue;
      MCMF::add_flow(u, v, lim[v], 0);
      dfs(v);
    }
  }
} // namespace T
int main() {
  memset(lim, 0x3f, sizeof(lim));
  scanf("%d", &n);
  MCMF::s = 0, MCMF::t = n * 3 + 1;
  FOR(i, 1, n) scanf("%d", &c[i]);
  scanf("%d", &r1);
  FOR(i, 1, n - 1) {
    int u, v;
    scanf("%d%d", &u, &v);
    T::add_path(u, v), T::add_path(v, u);
  }
  scanf("%d", &r2), r2 += n;
  FOR(i, 1, n - 1) {
    int u, v;
    scanf("%d%d", &u, &v);
    u += n, v += n;
    T::add_path(u, v), T::add_path(v, u);
  }
  scanf("%d", &q1);
  FOR(i, 1, q1) {
    int a, p;
    scanf("%d%d", &a, &p), lim[a] = p;
  }
  scanf("%d", &q2);
  FOR(i, 1, q2) {
    int a, p;
    scanf("%d%d", &a, &p), a += n, lim[a] = p;
  }
  T::dfs(r1);
  T::dfs(r2);
  FOR(i, 1, n) {
    MCMF::add_flow(i, i + n + n, 1, 0);
    MCMF::add_flow(i + n, i + n + n, 1, 0);
    MCMF::add_flow(i + n + n, MCMF::t, 1, c[i]);
  }
  MCMF::add_flow(MCMF::s, r1, lim[r1], 0);
  MCMF::add_flow(MCMF::s, r2, lim[r2], 0);
  MCMF::go();
  printf("%d\n", MCMF::maxcost);
  return 0;
}
