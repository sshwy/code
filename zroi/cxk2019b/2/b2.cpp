#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
namespace IO {
  char nc() {
    static char bf[100000], *p1 = bf, *p2 = bf;
    return p1 == p2 && (p2 = (p1 = bf) + fread(bf, 1, 100000, stdin), p1 == p2)
               ? EOF
               : *p1++;
  }
  int rd() {
    int res = 0;
    char c = getchar();
    while (!isdigit(c)) c = getchar();
    while (isdigit(c)) res = res * 10 + c - '0', c = getchar();
    return res;
  }
} // namespace IO
/******************heading******************/
const int N = 2e5 + 5, M = 2e5 + 5;
int n, m;
int a[N], a2[N];
int p[N];

namespace d {
  int f[N], fv[N];
  void init(int x) { FOR(i, 0, x) f[i] = i, fv[i] = a[i]; }
  int get(int u) { return f[u] == u ? u : f[u] = get(f[u]); }
  int find(int x, int y) { return get(x) == get(y); }
  void merge(int x, int y, int w) { f[get(x)] = get(y), fv[get(y)] = w; }
} // namespace d

struct qxx {
  int nex, t, v;
};
qxx e[M];
int h[N], le = 1;
void add_path(int f, int t, int v) { e[++le] = (qxx){h[f], t, v}, h[f] = le; }

priority_queue<pii> q;

int main() {
  scanf("%d%d", &n, &m);
  FOR(i, 1, n) scanf("%d", &a[i]);
  FOR(i, 1, m) {
    int u, v, w;
    scanf("%d%d%d", &u, &v, &w);
    add_path(u, v, w);
    add_path(v, u, w);
  }
  FOR(i, 2, le) q.push(mk(a[e[i].t] + a[e[i ^ 1].t] - e[i].v, i));
  d::init(n);
  while (!q.empty()) {
    pii u = q.top();
    q.pop();
    const int id = u.se;
    int x = d::get(e[id].t), y = d::get(e[id ^ 1].t), w = e[id].v;
    if (x == y) continue;
    int v = d::fv[x] + d::fv[y] - w;
    if (v >= e[id].fi) {
      q.push(mk(v, id));
    } else if (v == e[id].fi) {
      if (v < 0) continue;
      d::merge(x, y, v);
      q.push(mk(v, id));
    } else {
    }
    if (v < 0) {
      if (v <= e[id].fi) continue; //无利用价值
      q.push(mk(v, id));
    } else {
      q.push(mk(v, id));
    }
  }
  puts("0");
  return 0;
}
