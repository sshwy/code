#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
namespace IO {
  char nc() {
    static char bf[100000], *p1 = bf, *p2 = bf;
    return p1 == p2 && (p2 = (p1 = bf) + fread(bf, 1, 100000, stdin), p1 == p2)
               ? EOF
               : *p1++;
  }
  int rd() {
    int res = 0;
    char c = getchar();
    while (!isdigit(c)) c = getchar();
    while (isdigit(c)) res = res * 10 + c - '0', c = getchar();
    return res;
  }
} // namespace IO
/******************heading******************/
const int N = 2000;
int n, m;
int a[N], a2[N];
int u[N], v[N], w[N];
int p[N];

int f[N], fv[N];
void init(int x) { FOR(i, 0, x) f[i] = i, fv[i] = a[i]; }
int get(int u) { return f[u] == u ? u : f[u] = get(f[u]); }
int find(int x, int y) { return get(x) == get(y); }
void merge(int x, int y, int w) { f[get(x)] = get(y), fv[get(y)] = w; }

bool vis[N];
int ans[N], la;
int main() {
  scanf("%d%d", &n, &m);
  FOR(i, 1, n) scanf("%d", &a[i]);
  FOR(i, 1, m) scanf("%d%d%d", &u[i], &v[i], &w[i]);
  init(n);
  FOR(i, 1, n - 1) {
    int mn = 0x3f3f3f3f, p = 0;
    FOR(j, 1, m) {
      if (vis[j]) continue;
      int x = get(u[j]), y = get(v[j]), z = w[j];
      if (x == y) continue;
      if (z < mn && fv[x] + fv[y] - z >= 0) mn = z, p = j;
    }
    int x = get(u[p]), y = get(v[p]), z = w[p];
    if (!p) return puts("0"), 0;
    ans[++la] = p;
    merge(x, y, fv[x] + fv[y] - z);
  }
  if (fv[get(1)] < 0) return puts("0"), 0;
  puts("1");
  FOR(i, 1, la) printf("%d ", ans[i]);
  return 0;
}
