#include <algorithm>/*{{{*/
#include <bitset>
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
namespace IO {
  char nc() {
    static char bf[100000], *p1 = bf, *p2 = bf;
    return p1 == p2 && (p2 = (p1 = bf) + fread(bf, 1, 100000, stdin), p1 == p2)
               ? EOF
               : *p1++;
  }
  int rd() {
    int res = 0;
    char c = getchar();
    while (!isdigit(c)) c = getchar();
    while (isdigit(c)) res = res * 10 + c - '0', c = getchar();
    return res;
  }
} // namespace IO
/******************heading******************/
const int N = 200000, M = 1e6 + 500;

int pn[N], lp;
int bp[M], tr[M];
void sieve(int k) {
  bp[0] = bp[1] = 1;
  FOR(i, 2, k) {
    if (!bp[i]) pn[++lp] = i, bp[i] = lp, tr[i] = lp;
    FOR(j, 1, lp) {
      if (i * pn[j] > k) break;
      bp[i * pn[j]] = 1, tr[i * pn[j]] = j;
      if (i % pn[j] == 0) break;
    }
  }
}
typedef bitset<200> bin;
typedef pair<int, bitset<200>> pib;
pib trans(int x) {
  bin res;
  res.reset();
  while (x > 1) {
    if (pn[tr[x]] > 1000) break;
    res[tr[x]] = !res[tr[x]];
    x /= pn[tr[x]];
  }
  if (x > 1) res[199] = 1;
  return mk(bp[x], res);
}
namespace LB {
  bin p[M]; // p[i][199]的状态是i的状态，i是大质数
  void insert(pib a) {
    //分成两个向量处理
    if (a.fi > 1 && !p[a.fi][199]) {
      p[a.fi] = a.se;
      return;
    }
    a.se ^= p[a.fi];
    ROF(j, 180, 0) {
      if (!(a.se[j])) continue;
      if (!p[j][j]) {
        p[j] = a.se;
        break;
      }
      a.se ^= p[j];
    }
  }
  bool check(pib a) {
    if (a.fi > 1 && !p[a.fi][199]) return 0; //不能被线性表出
    ROF(j, 180, 0) {
      if (!(a.se[j])) continue;
      if (!p[j][j]) return 0;
      a.se ^= p[j];
    }
    return 1; //能被线性表出
  }
} // namespace LB

int x;
int main() {
  sieve(1e6);
  scanf("%d", &x);
  pib a = trans(x);
  if (a.fi == 1 && a.se.none()) return printf("1\n%d", x), 0;
  ROF(i, x - 1, 1) {
    //要求i xor x能被[i+1,x-1]的向量空间线性表出，但i不能被线性表出。
    if (LB::check(trans(i))) continue;
    LB::insert(trans(i));
    if (LB::check(a)) return printf("1\n%d", i), 0;
  }
  puts("0");
  return 0;
}
