#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
namespace IO {
  char nc() {
    static char bf[100000], *p1 = bf, *p2 = bf;
    return p1 == p2 && (p2 = (p1 = bf) + fread(bf, 1, 100000, stdin), p1 == p2)
               ? EOF
               : *p1++;
  }
  int rd() {
    int res = 0;
    char c = getchar();
    while (!isdigit(c)) c = getchar();
    while (isdigit(c)) res = res * 10 + c - '0', c = getchar();
    return res;
  }
} // namespace IO
/******************heading******************/
const int N = 503, INF = 0x3f3f3f3f;
int n;
int c[N], l1[N], l2[N];
int r1, r2, q1, q2;

struct qxx {
  int nex, t;
};
qxx e[N * 2];
int h[N], le = 1;
void add_path(int f, int t) { e[++le] = (qxx){h[f], t}, h[f] = le; }

bool vis[N];
lld f[N][N][N], ans;
int sz[N];
int sz_dfs(int u) {
  sz[u] = 1;
  for (int i = h[u]; i; i = e[i].nex) {
    const int v = e[i].t;
    if (sz[v]) continue;
    sz[u] += sz_dfs(v);
  }
  return sz[u];
}
void dfs(int u) {
  // printf("dfs(%d),lim=%d,%d\n",u,l1[u],l2[u]);
  vis[u] = 1;
  if (l1[u] > 1) f[u][1][0] = c[u];
  if (l2[u] > 1) f[u][0][1] = c[u];
  for (int i = h[u]; i; i = e[i].nex) {
    const int v = e[i].t;
    if (vis[v]) continue;
    dfs(v);
    ROF(j1, min(l1[u], sz[u]), 0) {
      ROF(j2, min(l2[u], sz[u] - j1), 0) {
        FOR(k1, 0, j1) {
          FOR(k2, 0, j2) {
            f[u][j1][j2] = max(f[u][j1][j2], f[u][k1][k2] + f[v][j1 - k1][j2 - k2]);
          }
        }
      }
    }
  }
  ROF(j1, min(l1[u], sz[u]), 0) {
    ROF(j2, min(l2[u], sz[u] - j1), 0) { ans = max(ans, f[u][j1][j2]); }
  }
  // FOR(i,0,sz[u])FOR(j,0,sz[u]){
  // if(f[u][i][j])printf("f[%d,%d,%d]=%d\n",u,i,j,f[u][i][j]); }
}

int main() {
  scanf("%d", &n);
  FOR(i, 1, n) scanf("%d", &c[i]), l1[i] = l2[i] = INF;
  scanf("%d", &r1);
  FOR(i, 1, n - 1) {
    int u, v;
    scanf("%d%d", &u, &v);
    add_path(u, v), add_path(v, u);
  }
  scanf("%d", &r2);
  FOR(i, 1, n - 1) {
    int u, v;
    scanf("%d%d", &u, &v);
  }
  scanf("%d", &q1);
  FOR(i, 1, q1) {
    int a, p;
    scanf("%d%d", &a, &p);
    l1[a] = p;
  }
  scanf("%d", &q2);
  FOR(i, 1, q2) {
    int a, p;
    scanf("%d%d", &a, &p);
    l2[a] = p;
  }
  sz_dfs(r1);
  dfs(r1);
  printf("%lld", ans);
  return 0;
}
