#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
namespace IO {
  char nc() {
    static char bf[100000], *p1 = bf, *p2 = bf;
    return p1 == p2 && (p2 = (p1 = bf) + fread(bf, 1, 100000, stdin), p1 == p2)
               ? EOF
               : *p1++;
  }
  int rd() {
    int res = 0;
    char c = getchar();
    while (!isdigit(c)) c = getchar();
    while (isdigit(c)) res = res * 10 + c - '0', c = getchar();
    return res;
  }
} // namespace IO
/******************heading******************/
const int N = 5050, P = 1e9 + 7;
lld n, q, ans;
lld a[N][N];
lld tot;
void work(int l, int r, int x, int y) {
  if (l == r) return;
  ans++;
  int mid = (l + r) >> 1;
  lld tot1 = if (x <= mid) work(l, mid, x, y);
  if (y > mid) work(mid + 1, r, x, y);
}
lld b2(lld x) { return (x * (x - 1) / 2) % P; }
void go() {
  lld l, r;
  scanf("%lld%lld", &l, &r);
}
int main() {
  scanf("%lld%lld", &n, &q);
  FOR(i, 1, q) go();
  return 0;
}
