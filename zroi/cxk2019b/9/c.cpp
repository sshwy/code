#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
namespace IO {
  char nc() {
    static char bf[100000], *p1 = bf, *p2 = bf;
    return p1 == p2 && (p2 = (p1 = bf) + fread(bf, 1, 100000, stdin), p1 == p2)
               ? EOF
               : *p1++;
  }
  int rd() {
    int res = 0;
    char c = getchar();
    while (!isdigit(c)) c = getchar();
    while (isdigit(c)) res = res * 10 + c - '0', c = getchar();
    return res;
  }
} // namespace IO
/******************heading******************/
const int N = 5050, P = 1e9 + 7;
void getnex(char *s, int len, lld *nex) {
  nex[0] = -1;
  int j = 0, i = 1;
  while (i <= len) {
    if (j == 0 || s[i] == s[j])
      nex[i] = j, ++i, ++j;
    else
      j = nex[j - 1] + 1;
  }
  // FOR(i,1,len)printf("%c%c",s[i]," \n"[i==len]);
  // FOR(i,1,len)printf("%d%c",nex[i]," \n"[i==len]);
}

lld f[N], a[N][N], nex[N];
int n;
char s[N];
int main() {
  scanf("%d", &n);
  scanf("%s", s + 1);
  FOR(i, 0, n - 1) {
    getnex(s + i, n - i, nex);
    f[0] = -1;
    FOR(j, 1, n - i) f[j] = f[nex[j]] + 1;
    FOR(j, 2, n - i) f[j] = (f[j] + f[j - 1]) % P;
    FOR(j, 1, n - i) {
      a[i + 1][i + j] = f[j];
      // printf("a[%d,%d]=%d\n",i+1,i+j,a[i+1][i+j]);
    }
  }
  FOR(len, 1, n - 1) {
    FOR(i, 1, n - len) {
      int j = i + len;
      if (len == 1)
        a[i][j] = (a[i][j] + a[i][j - 1] + a[i + 1][j]) % P;
      else
        a[i][j] = (a[i][j] + a[i][j - 1] + a[i + 1][j] - a[i + 1][j - 1]) % P;
    }
  }
  FOR(i, 1, n) printf("%lld\n", a[1][i]);
  return 0;
}
