#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
namespace IO {
  char nc() {
    static char bf[100000], *p1 = bf, *p2 = bf;
    return p1 == p2 && (p2 = (p1 = bf) + fread(bf, 1, 100000, stdin), p1 == p2)
               ? EOF
               : *p1++;
  }
  int rd() {
    int res = 0;
    char c = getchar();
    while (!isdigit(c)) c = getchar();
    while (isdigit(c)) res = res * 10 + c - '0', c = getchar();
    return res;
  }
} // namespace IO
/******************heading******************/
const int N = 1e4 + 5, INF = 0x3f3f3f3f;
int n, k;

struct qxx {
  int nex, t, v, c;
};
qxx e[N * 10];
int h[N * 10], le = 1;
void add_path(int f, int t, int v, int c) {
  e[++le] = (qxx){h[f], t, v, c}, h[f] = le;
}
void add_flow(int f, int t, int v, int c) {
  // printf("add_flow(%d,%d,%d,%d)\n",f,t,v,c);
  add_path(f, t, v, c), add_path(t, f, 0, -c);
}

int s, t1, t;
int d[N * 10], incf[N * 10], pre[N * 10];
bool vis[N * 10];
queue<int> q;
bool check_same(int x, int y) {
  // printf("\t\033[35mcheck_same(%d,%d)\033[0m\n",x,y);
  if (!x || !y) return x == y;
  if (x > n) x -= n;
  if (y > n) y -= n;
  return x == y;
}
bool check_on_path(int x, int y) {
  // printf("\033[34mcheck_on_path(%d,%d)\033[0m\n",x,y);
  for (int u = x;; u = e[pre[u] ^ 1].t) {
    // printf("%d,%d ",u,d[u]);
    if (check_same(u, y)) return 1;
    if (u == s) break;
  }
  // puts("");
  return 0;
}
bool spfa() {
  memset(d, -0x3f, sizeof(d));
  d[s] = 0, incf[s] = INF, incf[t] = 0;
  q.push(s);
  while (!q.empty()) {
    int u = q.front();
    q.pop();
    vis[u] = 0;
    // printf("u=%d,d[u]=%d\n",u,d[u]);
    for (int i = h[u]; i; i = e[i].nex) {
      const int &v = e[i].t, &w = e[i].v, &c = e[i].c;
      // printf("v=%d\n",v);
      if (!w || d[v] >= d[u] + c) continue;
      if (!check_same(u, v) && check_on_path(u, v)) continue;
      d[v] = d[u] + c, incf[v] = min(incf[u], w), pre[v] = i;
      // printf("u=%d -> v=%d, pre=%d\n",u,v,i);
      if (!vis[v]) q.push(v), vis[v] = 1;
    }
  }
  return incf[t];
}

int maxflow, maxcost;

int update() {
  if (d[t] <= 0) return 1;
  maxflow += incf[t], maxcost += incf[t] * d[t];
  for (int u = t; u != s; u = e[pre[u] ^ 1].t) {
    e[pre[u]].v -= incf[t];
    e[pre[u] ^ 1].v += incf[t];
  }
  return 0;
}

int ans[N][2], la;
int anex[N];
int dg[N];

void calc() {
  FOR(i, 1, n) dg[i] = anex[i] = 0;
  FOR(i, 2, le) {
    if (i & 1) continue;
    int u = e[i ^ 1].t, v = e[i].t, w = e[i].v;
    if (w) continue;
    // printf("\033[33m(%d,%d,%d)\033[0m\n",u,v,w);
    if (u <= n) continue;
    u -= n;
    if (v == t1)
      anex[u] = -1, dg[u]++;
    else
      anex[u] = v, dg[u]++, dg[v]++;
    // printf("anex[%d]=%d\n",u,anex[u]);
  }
  la = 0;
  FOR(i, 1, n) {
    if (dg[i] == 1) {
      ++la, ans[la][0] = i;
      int u = i;
      while (anex[u] != -1) u = anex[u];
      ans[la][1] = u;
    }
  }
}

void go() {
  scanf("%d%d", &n, &k);
  s = 0, t1 = n + n + 1, t = n + n + 2;
  memset(h, 0, sizeof(h)), le = 1, maxflow = 0, maxcost = 0;
  FOR(i, 1, n) add_flow(s, i, 1, 0);
  FOR(i, 1, n) add_flow(i + n, t1, 1, 0);
  FOR(i, 1, n) add_flow(i, i + n, 1, 1);
  FOR(i, 2, n) {
    int u, v;
    scanf("%d%d", &u, &v);
    add_flow(u + n, v, 1, 0), add_flow(v + n, u, 1, 0);
  }
  add_flow(t1, t, k, 0);
  while (spfa())
    if (update()) break;
  // printf("maxflow=%d,maxcost=%d\n",maxflow,maxcost);
  calc();
  printf("%d\n%d\n", maxcost, la);
  FOR(i, 1, la) printf("%d %d\n", ans[i][0], ans[i][1]);
}
int main() {
  //    freopen("A/ex_A2.in","r",stdin);
  int t;
  scanf("%d", &t);
  while (t--) go();
  return 0;
}
