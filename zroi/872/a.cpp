// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

typedef pair<int, int> pt;
const int SZ = 1 << 17, BASE = SZ / 2, P = 3;

int n, a[SZ], b[SZ], c[SZ];

pt fac(int x) { // a * 3^b
  int p3 = 0;
  for (int x2 = x; x2; x2 /= P) p3 += x2 / P;
  int c2 = x / P + (x % P == 2);
  return {c2 % 2 ? 2 : 1, p3};
}

int binom(int a, int b) {
  if (a < b) return 0;
  if (a == b) return 1;
  pt x = fac(a), y = fac(b), z = fac(a - b);
  if (x.second > y.second + z.second) return 0;
  return x.first * y.first * z.first % P;
}

int main() {
  scanf("%d", &n);
  FOR(i, 0, n - 1) scanf("%d", a + i);
  FOR(i, 0, n - 1) scanf("%d", b + i);
  FOR(i, 0, n - 1) FOR(j, 0, n - 1) {
    c[i + j] = (c[i + j] + a[i] * b[j] * binom(i + j, i)) % P;
  }
  FOR(i, 0, 2 * n - 2) printf("%d%c", c[i], " \n"[i == 2 * n - 2]);
  return 0;
}
