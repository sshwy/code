#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <climits>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA

long long mul(long long a, long long b, long long p) {
  // a%=p,b%=p;
  if (a >= p) a -= p;
  if (b >= p) b -= p;
  if (a < 1e9 && b < 1e9) return a * b % p;
  long long c = (long double)a * b / p;
  long long res = a * b - c * p;
  if (res < 0) res += p;
  return res;
}
long long pw(long long a, long long m, long long p) {
  long long res = 1;
  while (m) m & 1 ? res = mul(res, a, p) : 0, a = mul(a, a, p), m >>= 1;
  return res;
}
long long n, a, b, p;
struct triple {
  long long a, b, c;
};
triple f(long long x) { // return f(x), a^x, b^x
  if (x == 1) return {(a + b) % p, a, b};
  triple t = f(x / 2);
  long long A = t.b;
  long long B = t.c;
  if (x & 1)
    return {mul(mul(A, a, p) + mul(B, b, p), t.a, p), mul(mul(A, A, p), a, p),
        mul(mul(B, B, p), b, p)};
  else
    return {((mul(A + B, t.a, p) - mul(A, B, p)) % p + p) % p, mul(A, A, p),
        mul(B, B, p)};
}

void go() {
  scanf("%lld%lld%lld%lld", &n, &a, &b, &p);
  printf("%lld\n", f(n).a);
}
int main() {
  int t;
  scanf("%d", &t);
  FOR(i, 1, t) go();
  return 0;
}
