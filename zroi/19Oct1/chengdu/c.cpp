#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
#define int lld
const int N = 1e3 + 5, P = 1e4 + 5, INF = 0x3f3f3f3f3f3f3f3f;

int n, p;
int a[N], b[N], c[N];
int f$[P]; //\sum a\le x\le \sum b 的最小值
int g$[P]; //\sum a\le x\le \sum b 的最小值
int *f, *g;
int q[P], l, r;
void DP() {
  memset(f$, 0x3f, sizeof(f$));
  f[0] = 0;
  FOR(i, 1, n) {
    // printf("i=%lld,c=%lld\n",i,c[i]);
    l = 1, r = 0;
    int unvis = 0;
    FOR(j, 0, p) {
      g[j] = f[j];
      // j-b[i],j-a[i]
      if (unvis <= j - a[i]) {
        while (l <= r && f[q[r]] > f[unvis]) --r;
        q[++r] = unvis;
        ++unvis;
      }
      while (l <= r && q[l] < j - b[i]) ++l;
      // printf("q:");FOR(i,l,r)printf("%lld ",q[i]); puts("");
      if (l <= r) g[j] = min(g[j], f[q[l]] + c[i]);
    }
    swap(f, g);
  }
}
void go() {
  scanf("%lld%lld", &n, &p);
  FOR(i, 1, n) scanf("%lld", &a[i]);
  FOR(i, 1, n) scanf("%lld", &b[i]);
  FOR(i, 1, n) scanf("%lld", &c[i]);
  f = f$, g = g$;
  DP();
  if (f[p] == INF)
    puts("IMPOSSIBLE!!!");
  else
    printf("%lld\n", f[p]);
}
signed main() {
  int t;
  scanf("%lld", &t);
  FOR(i, 1, t) go();
  return 0;
}
