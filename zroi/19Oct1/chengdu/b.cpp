#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
#include <stack>
/******************heading******************/
const int NM = 2 * 3005 * 3005;

int n, m, k;
bool vis[3005][6005];

struct dsu {
  int f[NM], g[NM];
  stack<pair<int *, int>> s;
  void init(int k) { FOR(i, 0, k) f[i] = i, g[i] = 1; }
  int get(int u) { return f[u] == u ? u : get(f[u]); }
  bool find(int u, int v) { return get(u) == get(v); }
  int tag() { return s.size(); }
  void merge(int u, int v) {
    // printf("\033[31mmerge(%d,%d)(%d,%d)\033[0m\n",u/(m*2),u%(m*2),v/(m*2),v%(m*2));
    u = get(u), v = get(v);
    if (g[u] > g[v]) swap(u, v);
    s.push(mk(f + u, f[u])), f[u] = v;
    if (g[u] == g[v]) s.push(mk(g + v, g[v])), g[v]++;
  }
  void undo(int tim) {
    while (s.size() > tim) *s.top().fi = s.top().se, s.pop();
  }
} d;

int tr(int x, int y) { return x * (m * 2) + y; }
int ans;
int dir[][2] = {0, 1, 0, -1, 1, 0, -1, 0, 1, 1, 1, -1, -1, -1, -1, 1};
void link(int x, int y) {
  FOR(i, 0, 7) {
    int nx = x + dir[i][0], ny = y + dir[i][1];
    if (ny == -1) ny = m * 2 - 1;
    if (ny == m * 2) ny = 0;
    if (nx >= 0 && nx < n && ny >= 0 && ny < m * 2 && vis[nx][ny])
      d.merge(tr(x, y), tr(nx, ny));
  }
}
int main() {
  scanf("%d%d%d", &n, &m, &k);
  d.init(n * m * 2);
  FOR(i, 1, k) {
    int x, y;
    scanf("%d%d", &x, &y);
    --x, --y;
    // printf("x=%d,y=%d\n",x,y);
    int tag = d.tag();
    link(x, y);
    link(x, y + m);
    if (d.find(tr(x, y), tr(x, y + m)))
      d.undo(tag);
    else
      ans++, vis[x][y] = vis[x][y + m] = 1;
  }
  printf("%d\n", ans);
  return 0;
}
