#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
#define int lld
const int N = 1e5 + 5, SZ = 6333323;

int n, m;
int a[N], b[N];

struct hash_map {
  struct data {
    int nex, u, v;
  };
  data e[SZ];
  int h[SZ], le;
  int hash(int u) { return u % SZ; }
  int &operator[](int u) {
    int hu = hash(u);
    for (int i = h[hu]; i; i = e[i].nex)
      if (e[i].u == u) return e[i].v;
    return e[++le] = (data){h[hu], u, 0}, h[hu] = le, e[le].v;
  }
} A, A2;
long long ans;
signed main() {
  scanf("%lld%lld", &n, &m);
  FOR(i, 1, n) scanf("%lld", &a[i]);
  FOR(i, 1, m) scanf("%lld", &b[i]);
  FOR(i, 1, n) FOR(j, 0, 29) {
    int x = a[i] ^ (1 << j);
    A[x]++;
  }
  FOR(i, 1, n) A2[a[i]]++;
  FOR(i, 1, m) FOR(j, 0, 29) {
    int x = b[i] ^ (1 << j);
    ans += A[x];
    ans -= A2[b[i]];
  }
  printf("%lld\n", ans / 2);
  return 0;
}
