#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
#define int lld
const int N = 1e5 + 5, SZ = N * 10;

struct frac {
  int p, q;
  frac(int pp = 0, int qq = 1) { p = pp, q = qq; }
  bool operator<(frac f) const { return p * f.q < q * f.p; }
  bool operator<=(frac f) const { return p * f.q <= q * f.p; }
  void read() { scanf("%lld%lld", &p, &q); }
};
typedef pair<frac, int> key;
int n, m, T;

int tot;
int lc[SZ], rc[SZ], rnd[SZ], sz[SZ], id[SZ];
int p[SZ], sp[SZ], t[SZ], st[SZ], f[SZ];

int seed = 101;
int rrand() { return seed = seed * 482711; }

void pushup(int u) {
  if (!u) return;
  // printf("pushup(%lld)\n",u);
  // printf("u=%lld,lc=%lld,rc=%lld\n",u,lc[u],rc[u]);
  sz[u] = sz[lc[u]] + sz[rc[u]] + 1;
  sp[u] = sp[lc[u]] + sp[rc[u]] + p[u];
  st[u] = st[lc[u]] + st[rc[u]] + t[u];
  f[u] = f[lc[u]] + f[rc[u]] + (st[lc[u]] + t[u]) * (p[u] + sp[rc[u]]);
}
int newnode(int pi, int ti, int idx) {
  // printf("newnode(%lld,%lld,%lld)\n",pi,ti,idx);
  ++tot;
  sz[tot] = 1, p[tot] = sp[tot] = pi, t[tot] = st[tot] = ti, rnd[tot] = rrand();
  f[tot] = pi * ti, id[tot] = idx;
  return tot;
}
int merge(int x, int y) {
  if (!x || !y) return x + y;
  if (rnd[x] < rnd[y])
    return rc[x] = merge(rc[x], y), pushup(x), x;
  else
    return lc[y] = merge(x, lc[y]), pushup(y), y; // BUG#1:pushup(y)写成pushup(x)
}
void split(int u, int k, int &x, int &y) {
  if (!u) return x = 0, y = 0, void();
  if (k <= sz[lc[u]])
    y = u, split(lc[u], k, x, lc[y]);
  else
    x = u, split(rc[u], k - sz[lc[u]] - 1, rc[x], y);
  pushup(x), pushup(y);
}
void split(int u, key k, int &x, int &y) {
  if (!u) return x = 0, y = 0, void();
  if (k < mk(frac(t[u], p[u]), id[u]))
    y = u, split(lc[u], k, x, lc[y]); // BUG#2:<写成<=
  else
    x = u, split(rc[u], k, rc[x], y);
  pushup(x), pushup(y);
}
int root;
void insert(int u) { // return rank
  // printf("\033[32minsert(%lld)\033[0m\n",u);
  if (!root) return root = u, void();
  int x, y;
  split(root, mk(frac(t[u], p[u]), id[u]), x, y);
  root = merge(x, merge(u, y));
}
void printnode(int u) {
  printf("u=%lld,lc=%lld,rc=%lld,sz=%lld,p=%lld,t=%lld,sp=%lld,st=%lld,id=%lld,"
         "f=%lld\n",
      u, lc[u], rc[u], sz[u], p[u], t[u], sp[u], st[u], id[u], f[u]);
}
void print(int u) {
  if (!u) return;
  printnode(u);
  print(lc[u]);
  print(rc[u]);
}
void remove(int u) {
  // printf("\033[31mremove(%lld)\033[0m\n",u);
  int x, y, z;
  split(root, mk(frac(t[u], p[u]), id[u] - 1), x, y);
  // printf("x:\n"); print(x); puts("");
  // printf("y:\n"); print(y); puts("");
  // printf("z:\n"); print(z); puts("");
  // puts("EOJ C");
  split(y, mk(frac(t[u], p[u]), id[u]), y, z);
  // printf("x:\n"); print(x); puts("");
  // printf("y:\n"); print(y); puts("");
  // printf("z:\n"); print(z); puts("");
  // puts("EOJ C");
  assert(sz[y] == 1);
  root = merge(x, z);
}

int node[N];
int S;

int query() { return T * S - f[root]; }
signed main() {
  scanf("%lld%lld%lld", &n, &m, &T);
  FOR(i, 1, n) {
    int pp, tt;
    scanf("%lld%lld", &pp, &tt);
    S += pp;
    int u = newnode(pp, tt, i);
    insert(u), node[i] = u;
    // printf("node[%lld]=%lld\n",i,u);
    // print(root);
  }
  printf("%lld\n", query());
  FOR(i, 1, m) {
    int x, a, b;
    scanf("%lld%lld%lld", &x, &a, &b);
    S -= p[node[x]];
    remove(node[x]);
    node[x] = newnode(a, b, x);
    insert(node[x]);
    S += p[node[x]];
    printf("%lld\n", query());
  }
  return 0;
}
