#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
#define int lld
const int N = 1e6 + 5;

int n;
struct point {
  int x, y;
  void read() { scanf("%lld%lld", &x, &y); }
  point(int xx = 0, int xy = 0) { x = xx, y = xy; }
  point operator-(point p) { return point(x - p.x, y - p.y); }
  point abs() {
    return x > 0 ? point(x, y) : x == 0 ? point(x, y > 0 ? y : -y) : point(-x, -y);
  }
  int det(point p) { return x * p.y - y * p.x; }
};
typedef point vec;
struct line {
  point a, b;
  vec slp() { return (a - b).abs(); }
  void read() { a.read(), b.read(); }
};
bool cmp(line a, line b) { return a.slp().det(b.slp()) > 0; }
line l[N];
void go() {
  scanf("%lld", &n);
  FOR(i, 1, n) { l[i].read(); }
  sort(l + 1, l + n + 1, cmp);
  int tot = 1;
  FOR(i, 2, n) {
    if (l[i].slp().det(l[i - 1].slp())) ++tot;
  }
  if (tot <= 1)
    puts("0");
  else if (tot == 2) {
    if (n <= 2)
      puts("1");
    else
      puts("2");
  } else
    puts("3");
}
signed main() {
  int t;
  scanf("%lld", &t);
  FOR(i, 1, t) go();
  return 0;
}
