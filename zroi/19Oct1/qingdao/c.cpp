#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/

int main() { return 0; }
/*
 * f(i,c,S):长度为i，最高位的取值为1-c，数位乘积不超过S的数的个数。
 * g(i,c,S):长度为i，最高位取值为c，数位乘积和为S的数的个数。
 * g(i,c,S)=\sum g(i-1,j,S/c) (1<=j<=9,S>0,c|S,c>0)
 * g(i,c,0)=\sum g(i-1,j,0)+10^{i-2} (1<=j<=9,c>0)
 * g(1,c,c)=1 (c>0)
 */
