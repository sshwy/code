#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
namespace IO {
  char nc() {
    static char bf[100000], *p1 = bf, *p2 = bf;
    return p1 == p2 && (p2 = (p1 = bf) + fread(bf, 1, 100000, stdin), p1 == p2)
               ? EOF
               : *p1++;
  }
  int rd() {
    int res = 0;
    char c = getchar();
    while (!isdigit(c)) c = getchar();
    while (isdigit(c)) res = res * 10 + c - '0', c = getchar();
    return res;
  }
} // namespace IO
/******************heading******************/
#define int lld
const int P = 998244353;

int c[10][10];

int pw(int a, int m, int p) {
  int res = 1;
  while (m) m & 1 ? res = res * a % p : 0, a = a * a % p, m >>= 1;
  return res;
}
int n, m, ans = 1;
int gcd(int a, int b) { return b ? gcd(b, a % b) : a; }
int lcm(int a, int b) {
  // printf("lcm(%lld,%lld)\n",a,b);
  if (!b || !a) return a + b;
  int res = a * b % P * pw(gcd(a, b), P - 2, P) % P;
  // printf("%lld\n",res);
  return res;
}
void dfs(int k, int lst, int g, int l, int x) {
  // printf("dfs(%lld,%lld,%lld,%lld,%lld)\n",k,lst,g,l,x);
  if (k > n) return;
  if (k == n) {
    ans = ans * pw(pw(l, g, P), x, P) % P;
    return;
  }
  FOR(i, lst + 1, m) {
    int ggi = gcd(g, i), lgi = lcm(l, i);
    FOR(j, 1, n - k) { dfs(k + j, i, ggi, lgi, x * c[n - k][j] % P); }
  }
}
int tot = 1;
signed main() {
  scanf("%lld%lld", &n, &m);
  FOR(i, 0, n) c[i][0] = c[i][i] = 1;
  FOR(i, 1, n) FOR(j, 0, i - 1) {
    c[i][j] = c[i - 1][j - 1] + c[i - 1][j];
    c[i][j] %= P;
  }
  dfs(0, 0, 0, 0, 1);
  printf("%lld\n", ans);
  return 0;
}
