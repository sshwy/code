#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long LL;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
namespace IO {
  char nc() {
    static char bf[100000], *p1 = bf, *p2 = bf;
    return p1 == p2 && (p2 = (p1 = bf) + fread(bf, 1, 100000, stdin), p1 == p2)
               ? EOF
               : *p1++;
  }
  int rd() {
    int res = 0;
    char c = getchar();
    while (!isdigit(c)) c = getchar();
    while (isdigit(c)) res = res * 10 + c - '0', c = getchar();
    return res;
  }
} // namespace IO
/******************heading******************/
const LL M = 3e5 + 5, P = 998244353;

LL pw(LL a, LL m, LL p) {
  LL res = 1;
  while (m) m & 1 ? res = res * a % p : 0, a = a * a % p, m >>= 1;
  return res;
}
LL n, m;

LL v[M], v1[M];

bool bp[M];
int pn[M], lp;
void sieve(int k) {
  bp[0] = bp[1] = 1;
  FOR(i, 2, k) {
    if (!bp[i]) pn[++lp] = i;
    FOR(j, 1, lp) {
      int pj = pn[j];
      if (i * pj > k) break;
      bp[i * pj] = 1;
      if (i % pj == 0) break;
    }
  }
}
LL V1(LL d) { //所有数为d的倍数的lcm之积
  LL x = m / d;
  LL t = pw(d, pw(x, n, P - 1) + P - 1, P);
  // printf("%lld^{%lld^%lld}=%lld\n",d,x,n,t);
  //所有数在[1,x]内的lcm之积乘t
  FOR(i, 1, lp) {
    if (pn[i] > x) break;
    LL p = pn[i], pe = 1, pe1 = p; // p^{e+1}
    FOR(e, 1, x) {
      pe1 *= p, pe *= p;
      if (pe > x) break;
      // p^e在lcm乘积中的贡献
      // printf("less than %lld^%d occurred %lld times.\n",p,e,x-x/pe1);
      // printf("less than %lld^%d occurred %lld times.\n",p,e-1,x-x/pe);
      t = t *
          pw(pe, P - 1 + pw(x - x / pe1, n, P - 1) - pw(x - x / pe, n, P - 1), P) % P;
    }
  }
  // printf("V1(%lld)=%lld\n",d,t);
  return t;
}
LL V(LL d) { // gcd为d的序列的lcm之积
  if (v[d]) return v[d];
  LL res = V1(d), ilim = m / d, t = 1;
  FOR(i, 2, ilim) t = t * V(i * d) % P;
  res = res * pw(t, P - 2, P) % P;
  return v[d] = res;
}
int main() {
  scanf("%lld%lld", &n, &m);
  sieve(m);
  LL ans = 1;
  FOR(d, 1, m) ans = ans * pw(V(d), d, P) % P;
  // printf("V:");FOR(i,1,m)printf("%lld%c",v[i]," \n"[i==m]);
  printf("%lld\n", ans);
  return 0;
}
