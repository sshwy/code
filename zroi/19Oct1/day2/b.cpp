#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
namespace IO {
  char nc() {
    static char bf[100000], *p1 = bf, *p2 = bf;
    return p1 == p2 && (p2 = (p1 = bf) + fread(bf, 1, 100000, stdin), p1 == p2)
               ? EOF
               : *p1++;
  }
  int rd() {
    int res = 0;
    char c = getchar();
    while (!isdigit(c)) c = getchar();
    while (isdigit(c)) res = res * 10 + c - '0', c = getchar();
    return res;
  }
} // namespace IO
/******************heading******************/
const int N = 1005, M = N * N * 2;

int n, totim;
char a[N][N];

struct qxx {
  int nex, t;
};
qxx e[M];
int h[N], le;
void add_path(int f, int t) { e[++le] = (qxx){h[f], t}, h[f] = le; }

int dg[N], fa[N];
int sig[N], rest[N], dep[N];
int vis[N];
void match(int u, int v) {
  // printf("match(%d,%d)\n",u,v);
  if (dep[u] < dep[v]) u ^= v ^= u ^= v;
  while (dep[u] > dep[v]) {
    // printf("link(%d,%d)\n",u,fa[u]);
    a[u][fa[u]] = a[fa[u]][u] = '2', u = fa[u];
  }
  while (u != v) {
    // printf("link(%d,%d)\n",u,fa[u]);
    // printf("link(%d,%d)\n",v,fa[v]);
    a[u][fa[u]] = a[fa[u]][u] = '2', a[v][fa[v]] = a[fa[v]][v] = '2';
    u = fa[u], v = fa[v];
  }
}
bool dfs(int u, int p) {
  fa[u] = p;
  dep[u] = dep[p] + 1;
  vis[u] = 1;
  int cur = sig[u] ? u : 0;
  for (int i = h[u]; i; i = e[i].nex) {
    const int v = e[i].t;
    if (vis[v]) continue;
    dfs(v, u);
    if (rest[v]) {
      if (cur) {
        match(cur, rest[v]);
        cur = 0;
      } else {
        cur = rest[v];
      }
    }
  }
  if (cur) return rest[u] = cur, 1;
  return 0;
}
bool go(int u) {
  // printf("go(%d)\n",u);
  if (dfs(u, 0)) return 1;
  return 0;
}
int ke[N], lk;
void dfs2(int u) {
  vis[u] = totim;
  for (int i = h[u]; i; i = e[i].nex) {
    const int v = e[i].t;
    if (vis[v]) continue;
    dfs2(v);
  }
}
int main() {
  scanf("%d", &n);
  FOR(i, 1, n) scanf("%s", a[i] + 1);
  le = 0;
  FOR(i, 1, n) FOR(j, 1, i - 1) {
    if (a[i][j] == '0') {
      add_path(i, j), add_path(j, i); // reverse graph
    } else {
      dg[i]++, dg[j]++;
    }
  }
  FOR(i, 1, n) if (dg[i] & 1) sig[i] = 1;
  FOR(i, 1, n) {
    if (!vis[i]) {
      if (go(i)) return puts("No"), 0;
    }
  }
  memset(vis, 0, sizeof(vis));
  memset(h, 0, sizeof(h));
  le = 0;
  FOR(i, 1, n) FOR(j, 1, i - 1) {
    if (a[i][j] != '0') { add_path(i, j), add_path(j, i); }
  }
  FOR(i, 1, n) {
    ++totim;
    if (!vis[i]) {
      dfs2(i);
      ke[++lk] = i;
    }
  }
  // FOR(i,1,n)printf("%s\n",a[i]+1);
  // FOR(i,1,lk)printf("%d%c",ke[i]," \n"[i==lk]);
  if (lk > 2) {
    FOR(i, 1, lk - 1) a[ke[i]][ke[i + 1]] = a[ke[i + 1]][ke[i]] = '2';
    a[ke[1]][ke[lk]] = a[ke[lk]][ke[1]] = '2';
  } else if (lk == 2) {
    int t1[N], l1 = 0;
    int t2[N], l2 = 0;
    FOR(i, 1, n) if (vis[i] == vis[1]) t1[++l1] = i;
    else t2[++l2] = i;
    // FOR(i,1,l1)printf("%d%c",t1[i]," \n"[i==l1]);
    // FOR(i,1,l2)printf("%d%c",t2[i]," \n"[i==l2]);
    int fl = 0;
    if (l1 > 1 && l2 > 1) {
      a[t1[1]][t2[1]] = a[t2[1]][t1[1]] = '2';
      a[t1[2]][t2[1]] = a[t2[1]][t1[2]] = '2';
      a[t1[1]][t2[2]] = a[t2[2]][t1[1]] = '2';
      a[t1[2]][t2[2]] = a[t2[2]][t1[2]] = '2';
      fl = 1;
    } else if (l1 == 1) {
      FOR(i, 1, l2) FOR(j, 1, i - 1) {
        if (fl) break;
        if (a[t2[i]][t2[j]] == '0') {
          a[t2[i]][t2[j]] = a[t2[j]][t2[i]] = '2';
          a[t2[i]][t1[1]] = a[t1[1]][t2[i]] = '2';
          a[t2[j]][t1[1]] = a[t1[1]][t2[j]] = '2';
          fl = 1;
        } else if (a[t2[i]][t2[j]] == '2') {
          a[t2[i]][t2[j]] = a[t2[j]][t2[i]] = '0';
          a[t2[i]][t1[1]] = a[t1[1]][t2[i]] = '2';
          a[t2[j]][t1[1]] = a[t1[1]][t2[j]] = '2';
          fl = 1;
        }
      }
    } else if (l2 == 1) {
      FOR(i, 1, l1) FOR(j, 1, i - 1) {
        if (fl) break;
        if (a[t1[i]][t1[j]] == '0') {
          a[t1[i]][t1[j]] = a[t1[j]][t1[i]] = '2';
          a[t1[i]][t2[1]] = a[t2[1]][t1[i]] = '2';
          a[t1[j]][t2[1]] = a[t2[1]][t1[j]] = '2';
          fl = 1;
        } else if (a[t1[i]][t1[j]] == '2') {
          a[t1[i]][t1[j]] = a[t1[j]][t1[i]] = '0';
          a[t1[i]][t2[1]] = a[t2[1]][t1[i]] = '2';
          a[t1[j]][t2[1]] = a[t2[1]][t1[j]] = '2';
          fl = 1;
        }
      }
    }
    if (!fl) return puts("No"), 0;
  }
  // FOR(i,1,n)printf("%s\n",a[i]+1);
  puts("Yes");
  FOR(i, 1, n) FOR(j, 1, n) if (a[i][j] != '0') a[i][j] = '1';
  FOR(i, 1, n) printf("%s\n", a[i] + 1);
  return 0;
}
