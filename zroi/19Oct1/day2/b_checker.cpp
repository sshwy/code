#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
namespace IO {
  char nc() {
    static char bf[100000], *p1 = bf, *p2 = bf;
    return p1 == p2 && (p2 = (p1 = bf) + fread(bf, 1, 100000, stdin), p1 == p2)
               ? EOF
               : *p1++;
  }
  int rd() {
    int res = 0;
    char c = getchar();
    while (!isdigit(c)) c = getchar();
    while (isdigit(c)) res = res * 10 + c - '0', c = getchar();
    return res;
  }
} // namespace IO
/******************heading******************/
const int N = 1e3 + 5, M = N * N * 2;

int n;
char a[2000][2000], s[20];

struct qxx {
  int nex, t;
};
qxx e[M];
int h[N], le;
void add_path(int f, int t) { e[++le] = (qxx){h[f], t}, h[f] = le; }

bool vis[N];
int dg[N];
void dfs(int u) {
  vis[u] = 1;
  for (int i = h[u]; i; i = e[i].nex) {
    if (vis[e[i].t]) continue;
    dfs(e[i].t);
  }
}
int main(int argc, char **argv) {
  FILE *fin, *fout;
  fin = fopen(argv[1], "r");
  fout = fopen(argv[2], "r");
  fscanf(fin, "%d", &n);
  fscanf(fout, "%s", s);
  if (n & 1) {
    if (s[0] != 'Y') {
      printf("Wrong Answer!");
      return 1;
    }
  }
  if (s[0] == 'N') return 0;
  FOR(i, 1, n) { fscanf(fout, "%s", a[i] + 1); }
  FOR(i, 1, n) FOR(j, 1, i - 1) if (a[i][j] == '1') {
    dg[i]++, dg[j]++, add_path(i, j), add_path(j, i);
  }
  FOR(i, 1, n) if (dg[i] & 1 || !dg[i]) {
    printf("Wrong Answer!");
    return 1;
  }
  dfs(1);
  FOR(i, 1, n) if (!vis[i]) {
    printf("is not connected!");
    return 1;
  }
  return 0;
}
