#include <bits/stdc++.h>
using namespace std;
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
#define LL long long
/******************heading******************/
const int SZ = 1 << 25;

LL n;
int tot = 1;
int lc[SZ], rc[SZ];
LL odd[SZ], even[SZ];
bool cv[SZ];

void pushdown(int u, LL l, LL r) {
  if (!lc[u]) lc[u] = ++tot;
  if (!rc[u]) rc[u] = ++tot;
  if (cv[u]) {
    cv[lc[u]] = cv[rc[u]] = 1;
    if (odd[u] > 1) {
      odd[lc[u]] = odd[rc[u]] = odd[u] / 2;
      even[lc[u]] = even[rc[u]] = even[u] / 2;
    } else {
      __builtin_popcountll(l) & 1
          ? (odd[lc[u]] = 1, odd[rc[u]] = 0, even[lc[u]] = 0, even[rc[u]] = 1)
          : (odd[lc[u]] = 0, odd[rc[u]] = 1, even[lc[u]] = 1, even[rc[u]] = 0);
    }
  }
}
void pushup(int u, LL l, LL r) {
  odd[u] = odd[lc[u]] + odd[rc[u]];
  even[u] = even[lc[u]] + even[rc[u]];
  if (odd[u] + even[u] == r - l + 1) cv[u] = 1;
}
void cover(int L, int R, int u = 1, LL l = 0, LL r = (1LL << 31) - 1) {
  if (r < L || R < l) return;
  if (L <= l && r <= R) {
    cv[u] = 1;
    if (l == r) {
      __builtin_popcountll(l) & 1 ? (odd[u] = 1, even[u] = 0)
                                  : (odd[u] = 0, even[u] = 1);
    } else {
      odd[u] = even[u] = (r - l + 1) / 2;
    }
    return;
  }
  LL mid = (l + r) >> 1;
  pushdown(u, l, r);
  cover(L, R, lc[u], l, mid), cover(L, R, rc[u], mid + 1, r);
  pushup(u, l, r);
}
int main() {
  scanf("%lld", &n);
  FOR(i, 1, n) {
    int l, r;
    scanf("%d%d", &l, &r);
    cover(l, r);
    printf("%lld\n", odd[1] * even[1]);
  }
  return 0;
}
/*
 * f[i]表示长度为i的二进制数中，1的个数为奇数的数的个数
 * g[i]表示长度为i的二进制数中，1的个数为偶数的数的个数
 * f[1]=1
 * g[1]=1
 * f[i]=2^{i-1}
 *
 *
 */
