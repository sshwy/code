#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
namespace IO {
  char nc() {
    static char bf[100000], *p1 = bf, *p2 = bf;
    return p1 == p2 && (p2 = (p1 = bf) + fread(bf, 1, 100000, stdin), p1 == p2)
               ? EOF
               : *p1++;
  }
  int rd() {
    int res = 0;
    char c = getchar();
    while (!isdigit(c)) c = getchar();
    while (isdigit(c)) res = res * 10 + c - '0', c = getchar();
    return res;
  }
} // namespace IO
/******************heading******************/
#define int lld
const int N = 1e6 + 5, P = 998244353, SZ = 7;

struct matrix {
  int h, w;
  int a[SZ][SZ];
  matrix() { h = 0, w = 0; }
  matrix(int _h, int _w) {
    h = _h, w = _w;
    FOR(i, 1, h) FOR(j, 1, w) a[i][j] = 0;
  }
  matrix(int _h) {
    h = w = _h;
    FOR(i, 1, h) FOR(j, 1, w) a[i][j] = 0;
    FOR(i, 1, h) a[i][i] = 1;
  }
  matrix operator*(matrix m) {
    // printf("\033[31m");
    // print();
    // printf("\033[32m");
    // m.print();
    // printf("\033[0m");
    matrix x(m.h, w);
    if (h != m.w) return x;
    FOR(i, 1, m.h) {
      FOR(j, 1, w) {
        FOR(k, 1, h) {
          x.a[i][j] += m.a[i][k] * a[k][j];
          x.a[i][j] %= P;
        }
      }
    }
    return x;
  }
  // void print(){
  //    FOR(i,1,h){
  //        printf("|");
  //        FOR(j,1,w)printf("%3lld",a[i][j]);
  //        printf("|");
  //        puts("");
  //    }
  //}
};

int n, ans;
// int f[N],S[N];

matrix pw(matrix a, int m) {
  // printf("pw\n");
  // a.print();
  // printf("m=%lld\n",m);
  matrix res(a.h);
  while (m) m & 1 ? res = res * a : 0, a = a * a, m >>= 1;
  // printf("res\n");
  // res.print();
  // puts("");
  return res;
}

matrix a(5, 5), b(5, 1);
signed main() {
  scanf("%lld", &n);
  // f[0]=1,f[1]=1;
  // FOR(i,2,n+3)f[i]=(f[i-1]+f[i-2])%P;
  // S[0]=1,S[1]=3;
  // FOR(i,2,n){
  //    S[i]=(S[i-1]+S[i-2]+f[i+2]-1)%P;
  //}
  // printf("%lld\n",S[n]);
  a.a[1][1] = 1, a.a[1][2] = 1, a.a[1][3] = 1, a.a[1][4] = 0, a.a[1][5] = 1;
  a.a[2][1] = 1, a.a[2][2] = 0, a.a[2][3] = 0, a.a[2][4] = 0, a.a[2][5] = 0;
  a.a[3][1] = 0, a.a[3][2] = 0, a.a[3][3] = 1, a.a[3][4] = 1, a.a[3][5] = 0;
  a.a[4][1] = 0, a.a[4][2] = 0, a.a[4][3] = 1, a.a[4][4] = 0, a.a[4][5] = 0;
  a.a[5][1] = 0, a.a[5][2] = 0, a.a[5][3] = 0, a.a[5][4] = 0, a.a[5][5] = 1;
  // a.print();

  b.a[1][1] = 3;
  b.a[2][1] = 1;
  b.a[3][1] = 5;
  b.a[4][1] = 3;
  b.a[5][1] = -1;

  a = pw(a, n - 1);
  // a.print();
  b = b * a;
  b.a[1][1] = (b.a[1][1] + P) % P;
  // assert(b.a[1][1]==S[n]);
  printf("%lld\n", b.a[1][1]);

  return 0;
}
