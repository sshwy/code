#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
namespace IO {
  char nc() {
    static char bf[100000], *p1 = bf, *p2 = bf;
    return p1 == p2 && (p2 = (p1 = bf) + fread(bf, 1, 100000, stdin), p1 == p2)
               ? EOF
               : *p1++;
  }
  int rd() {
    int res = 0;
    char c = getchar();
    while (!isdigit(c)) c = getchar();
    while (isdigit(c)) res = res * 10 + c - '0', c = getchar();
    return res;
  }
} // namespace IO
/******************heading******************/
#define int lld
const int N = 205, P = 998244353;

namespace DC { // BUG#1: 没有使用__int128
  __int128 dc[N * N];
  int ld;
  void add(__int128 x) { dc[++ld] = x; }
  void work() {
    sort(dc + 1, dc + ld + 1), ld = unique(dc + 1, dc + ld + 1) - dc - 1;
  }
  int tr(__int128 x) { return lower_bound(dc + 1, dc + ld + 1, x) - dc; }
} // namespace DC

int n, x, y, z, la;
int a[N], sa, ans;
int Cmod[N][N];
int dp[N][N][N]; //前i种颜色，一共选j张牌，颜色胜率最大为k的局面的出现概率
__int128 C[N][N];
__int128 w[N][N]; //第i种颜色选j个获胜的概率
int DCw[N][N];

int pw(int a, int m, int p) {
  int res = 1;
  while (m) m & 1 ? res = 1ll * res * a % p : 0, a = 1ll * a * a % p, m >>= 1;
  return res;
}
signed main() {
  // freopen("t1.txt","w",stdout);
  cin >> n >> x >> y >> z;
  FOR(i, 1, n) {
    cin >> a[i];
    sa += a[i];
  }

  C[0][0] = Cmod[0][0] = 1;
  FOR(i, 0, sa) {
    C[i][0] = C[i][i] = 1;
    Cmod[i][0] = Cmod[i][i] = 1;
    FOR(j, 1, i - 1) {
      C[i][j] = C[i - 1][j - 1] + C[i - 1][j];
      Cmod[i][j] = (Cmod[i - 1][j - 1] + Cmod[i - 1][j]) % P;
    }
  }

  FOR(i, 1, n) {
    FOR(j, 0, a[i]) {            // w[i,j],枚举小E拿到的数量
      FOR(k, 0, min(j - z, y)) { // k枚举小F拿到的数量
        w[i][j] += C[a[i] - j][k] * C[sa - x - a[i] + j][y - k];
      }
      DC::add(w[i][j]);
      // printf("func[%d,%d]=%d\n",i,j,(int)w[i][j]);
    }
  }           //最后要除以C[sa-x][y]
  DC::work(); //离散化
  FOR(i, 1, n) {
    FOR(j, 0, a[i]) {
      DCw[i][j] = DC::tr(w[i][j]);
      // printf("DCfunc[%d,%d]=%d\n",i,j,(int)DCw[i][j]);
    }
  }
  int T = DC::ld;
  dp[0][0][0] = pw(Cmod[sa - x][y], P - 2, P) * pw(Cmod[sa][x], P - 2, P) % P;
  FOR(i, 1, n) {       //前i种颜色
    FOR(j, 0, a[i]) {  //当前颜色j个
      FOR(k, j, x) {   //一共选k张
        FOR(l, 0, T) { //枚举胜率
          dp[i][k][max(l, DCw[i][j])] =
              (dp[i][k][max(l, DCw[i][j])] + Cmod[a[i]][j] * dp[i - 1][k - j][l]) % P;
        }
      }
    }
  }
  FOR(i, 1, T) { ans = (ans + DC::dc[i] % P * dp[n][x][i]) % P; }
  cout << ans << endl;
  return 0;
}
