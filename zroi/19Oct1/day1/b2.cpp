#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
namespace IO {
  char nc() {
    static char bf[100000], *p1 = bf, *p2 = bf;
    return p1 == p2 && (p2 = (p1 = bf) + fread(bf, 1, 100000, stdin), p1 == p2)
               ? EOF
               : *p1++;
  }
  int rd() {
    int res = 0;
    char c = getchar();
    while (!isdigit(c)) c = getchar();
    while (isdigit(c)) res = res * 10 + c - '0', c = getchar();
    return res;
  }
} // namespace IO
/******************heading******************/
#define int lld
const int N = 1.5e5 + 5, M = N * 2;

struct qxx {
  int nex, t;
};
qxx e[M];
int h[N], le = 1;
void add_path(int f, int t) { e[++le] = (qxx){h[f], t}, h[f] = le; }

int n, m;
int dep[N], fa[N][20];

lld ans, totlen; // of edge

lld val[N];
lld f[N];
int g[N];           // f:max contri; g:chosen
int mx1[N], mx2[N]; // adj. max1 max2 node idx

void dfs(int u, int p) {
  dep[u] = dep[p] + 1, fa[u][0] = p;
  FOR(j, 1, 19) {
    fa[u][j] = fa[fa[u][j - 1]][j - 1];
    if (!fa[u][j]) break;
  }
  for (int i = h[u]; i; i = e[i].nex) {
    const int v = e[i].t;
    if (v == p) continue;
    dfs(v, u);
  }
}
void make(int x, int y) {
  if (x == y) return;
  if (dep[x] < dep[y]) x ^= y ^= x ^= y;
  // printf("make(%lld,%lld)\n",x,y);
  int x1 = x, y1 = y;
  ROF(j, 19, 0) {
    if (dep[x] - (1 << j) <= dep[y]) continue;
    x = fa[x][j];
  }
  if (fa[x][0] == y) { // x1 --> x -> y
    val[x1]++, val[y]--;
    totlen += dep[x1] - dep[y];
    // printf("x1=%lld,y=%lld\n",x1,y);
    return;
  }
  if (dep[x] > dep[y]) x = fa[x][0];
  ROF(j, 19, 0) {
    if (fa[x][j] == fa[y][j]) continue;
    x = fa[x][j], y = fa[y][j];
  }
  x = fa[x][0];
  // printf("x1=%lld,y1=%lld,x=%lld\n",x1,y1,x);
  val[x1]++, val[y1]++, val[x]--, val[x]--;
  totlen += dep[x1] - dep[x];
  totlen += dep[y1] - dep[x];
}
void dfs5(int u, int p) {
  for (int i = h[u]; i; i = e[i].nex) {
    const int v = e[i].t;
    if (v == p) continue;
    dfs5(v, u);
    val[u] += val[v];
  }
  // printf("val[%lld]=%lld\n",u,val[u]);
}
void dfs2(int u, int p) { // path to u
  mx1[u] = mx2[u] = -1;
  if (p) mx1[u] = u;
  for (int i = h[u]; i; i = e[i].nex) {
    const int v = e[i].t;
    if (v == p) continue;
    if (val[v] > val[mx1[u]])
      mx2[u] = mx1[u], mx1[u] = v;
    else if (val[v] > val[mx2[u]])
      mx2[u] = v;
  }
  // printf("u=%lld,mx1=%lld,mx2=%lld\n",u,mx1[u],mx2[u]);
  for (int i = h[u]; i; i = e[i].nex) {
    const int v = e[i].t;
    if (v == p) continue;
    dfs2(v, u);
  }
}
void dfs3(int u, int p) { // first time DP
  if (mx1[u] == u)
    g[u] = mx2[u], f[u] = val[mx2[u]]; // have to chose son path
  else
    g[u] = mx1[u], f[u] = val[mx1[u]];
  for (int i = h[u]; i; i = e[i].nex) {
    const int v = e[i].t;
    if (v == p) continue;
    dfs3(v, u);
    f[u] += f[v];
  }
  // printf("u=%lld, chose %lld, f=%lld\n",u,g[u],f[u]);
}
void dfs4(int u, int p) { // change root
  // printf("\033[32mdfs4(%lld,%lld)\033[0m\n",u,p);
  // change from p to u
  if (!p) { // u is the root
    // printf("u=%lld,f=%lld,chose %lld\n",u,f[u],g[u]);
    for (int i = h[u]; i; i = e[i].nex) {
      const int v = e[i].t;
      if (v == p) continue;
      dfs4(v, u);
    }
    return;
  }
  lld fp = f[p], fu = f[u];
  lld gp = g[p], gu = g[u];
  f[p] -= f[u];

  if (g[p] == u) {
    f[p] -= val[g[p]];
    g[p] = mx1[p] != u ? mx1[p] : mx2[p];
    if (g[p] != -1) f[p] += val[g[p]];
  }
  f[u] += f[p];

  // printf("u=%lld,f=%lld,chose %lld\n",u,f[u],g[u]);
  if (mx1[u] == u) {
    f[u] -= val[g[u]];
    g[u] = u;
    f[u] += val[g[u]];
  }
  // printf("u=%lld,f=%lld,chose %lld\n",u,f[u],g[u]);
  // printf("p=%lld,f=%lld,chose %lld\n",p,f[p],g[p]);
  // do sth for u
  ans = max(ans, f[u]);

  for (int i = h[u]; i; i = e[i].nex) {
    const int v = e[i].t;
    if (v == p) continue;
    dfs4(v, u);
  }

  g[p] = gp, g[u] = gu;
  f[p] = fp, f[u] = fu;
}
signed main() {
  scanf("%lld%lld", &n, &m);
  FOR(i, 1, n - 1) {
    int u, v;
    scanf("%lld%lld", &u, &v);
    add_path(u, v);
    add_path(v, u);
  }
  // set 1 as root
  dfs(1, 0);
  FOR(i, 1, m) {
    int x, y;
    scanf("%lld%lld", &x, &y);
    make(x, y);
  }
  // printf("val:");
  // FOR(i,1,n)printf("%lld%c",val[i]," \n"[i==n]);
  dfs5(1, 0);
  // printf("val:");
  // FOR(i,1,n)printf("%lld%c",val[i]," \n"[i==n]);
  dfs2(1, 0);
  dfs3(1, 0);
  dfs4(1, 0);
  printf("%lld", totlen - ans);
  return 0;
}
