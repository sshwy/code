#include <bits/stdc++.h>
using namespace std;

typedef __int128 ll;
const int maxn = 100, mod = 998244353;
int n, m, x, y, z, a[maxn + 3], tot, f[maxn + 3][maxn + 3];
int c_mod[maxn + 3][maxn + 3], c_inv[maxn + 3][maxn + 3],
    dp[maxn + 3][maxn + 3][maxn + 3];
ll binom[maxn + 3][maxn + 3], func[maxn + 3][maxn + 3], arr[maxn + 3];

int qpow(int a, int b) {
  int c = 1;
  for (; b; b >>= 1, a = 1ll * a * a % mod) {
    if (b & 1) c = 1ll * a * c % mod;
  }
  return c;
}

void get_binom(int n) {
  for (int i = 0; i <= n; i++) {
    binom[i][0] = binom[i][i] = 1;
    for (int j = 1; j < i; j++) {
      binom[i][j] = binom[i - 1][j - 1] + binom[i - 1][j];
    }
  }
  for (int i = 0; i <= n; i++) {
    c_mod[i][0] = c_mod[i][i] = 1;
    for (int j = 1; j < i; j++) {
      c_mod[i][j] = c_mod[i - 1][j - 1] + c_mod[i - 1][j];
      c_mod[i][j] < mod ? 0 : c_mod[i][j] -= mod;
    }
  }
  for (int i = 0; i <= n; i++) {
    for (int j = 0; j <= i; j++) { c_inv[i][j] = qpow(c_mod[i][j], mod - 2); }
  }
}

int main() {
  freopen("t2.txt", "w", stdout);
  scanf("%d %d %d %d", &n, &x, &y, &z);
  for (int i = 1; i <= n; i++) { scanf("%d", &a[i]), m += a[i]; }
  get_binom(m);
  for (int i = 1; i <= n; i++) {
    for (int j = 0; j <= a[i]; j++) {
      for (int k = 0; k <= j - z && k <= y; k++) {
        func[i][j] += binom[a[i] - j][k] * binom[m - x - a[i] + j][y - k];
      }
      arr[++tot] = func[i][j];
      printf("func[%d,%d]=%d\n", i, j, (int)func[i][j]);
    }
  }
  sort(arr + 1, arr + tot + 1);
  tot = unique(arr + 1, arr + tot + 1) - (arr + 1);
  for (int i = 1; i <= n; i++) {
    for (int j = 0; j <= a[i]; j++) {
      f[i][j] = lower_bound(arr + 1, arr + tot + 1, func[i][j]) - arr;

      printf("DCfunc[%d,%d]=%d\n", i, j, (int)f[i][j]);
    }
  }
  dp[0][0][0] = 1ll * c_inv[m][x] * c_inv[m - x][y] % mod;
  for (int i = 1; i <= n; i++) {
    for (int j = 0; j <= a[i]; j++) {
      for (int k = j; k <= x; k++) {
        for (int l = 0; l <= tot; l++) {
          dp[i][k][max(l, f[i][j])] =
              (dp[i][k][max(l, f[i][j])] +
                  1ll * c_mod[a[i]][j] * dp[i - 1][k - j][l]) %
              mod;
        }
      }
    }
  }
  int ans = 0;
  for (int i = 1; i <= tot; i++) { ans = (ans + (arr[i] % mod) * dp[n][x][i]) % mod; }
  printf("%d\n", ans);
  return 0;
}
