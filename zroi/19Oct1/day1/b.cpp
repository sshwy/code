#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
namespace IO {
  char nc() {
    static char bf[100000], *p1 = bf, *p2 = bf;
    return p1 == p2 && (p2 = (p1 = bf) + fread(bf, 1, 100000, stdin), p1 == p2)
               ? EOF
               : *p1++;
  }
  int rd() {
    int res = 0;
    char c = getchar();
    while (!isdigit(c)) c = getchar();
    while (isdigit(c)) res = res * 10 + c - '0', c = getchar();
    return res;
  }
} // namespace IO
/******************heading******************/
#define int lld
const int N = 5009, M = N * 2;

struct qxx {
  int nex, t, v;
};
qxx e[M];
int h[N], le = 1;
void add_path(int f, int t, int v) { e[++le] = (qxx){h[f], t, v}, h[f] = le; }

int n, m, ans;

int dep[N], fa[N], val[N];
void dfs(int u, int p) {
  dep[u] = dep[p] + 1, fa[u] = p;
  // printf("dep[%lld]=%lld,fa[%lld]=%lld\n",u,dep[u],u,fa[u]);
  for (int i = h[u]; i; i = e[i].nex) {
    const int v = e[i].t;
    if (v == p) continue;
    dfs(v, u);
  }
}
int lca(int u, int v) {
  if (dep[u] < dep[v]) u ^= v ^= u ^= v;
  while (dep[u] > dep[v]) u = fa[u];
  while (u != v) u = fa[u], v = fa[v];
  return u;
}
int mx1[N], mx2[N];       // adj. max1 max2 node idx
void dfs2(int u, int p) { // path to u
  mx1[u] = mx2[u] = -1;
  if (p) mx1[u] = u;
  for (int i = h[u]; i; i = e[i].nex) {
    const int v = e[i].t;
    if (v == p) continue;
    if (val[v] > val[mx1[u]])
      mx2[u] = mx1[u], mx1[u] = v;
    else if (val[v] > val[mx2[u]])
      mx2[u] = v;
  }
  // printf("u=%lld,mx1=%lld,mx2=%lld\n",u,mx1[u],mx2[u]);
  for (int i = h[u]; i; i = e[i].nex) {
    const int v = e[i].t;
    if (v == p) continue;
    dfs2(v, u);
  }
}
int totlen;               // of edge
int f[N], g[N];           // f:max contri; g:chosen
void dfs3(int u, int p) { // first time DP
  if (mx1[u] == u)
    g[u] = mx2[u], f[u] = val[mx2[u]]; // have to chose son path
  else
    g[u] = mx1[u], f[u] = val[mx1[u]];
  for (int i = h[u]; i; i = e[i].nex) {
    const int v = e[i].t;
    if (v == p) continue;
    dfs3(v, u);
    f[u] += f[v];
  }
  // printf("u=%lld, chose %lld, f=%lld\n",u,g[u],f[u]);
}
void dfs4(int u, int p) { // change root
  // printf("\033[32mdfs4(%lld,%lld)\033[0m\n",u,p);
  // change from p to u
  if (!p) { // u is the root
    // printf("u=%lld,f=%lld,chose %lld\n",u,f[u],g[u]);
    for (int i = h[u]; i; i = e[i].nex) {
      const int v = e[i].t;
      if (v == p) continue;
      dfs4(v, u);
    }
    return;
  }
  int fp = f[p], fu = f[u];
  int gp = g[p], gu = g[u];
  f[p] -= f[u];

  if (g[p] == u) {
    f[p] -= val[g[p]];
    g[p] = mx1[p] != u ? mx1[p] : mx2[p];
    if (g[p] != -1) f[p] += val[g[p]];
  }
  f[u] += f[p];

  // printf("u=%lld,f=%lld,chose %lld\n",u,f[u],g[u]);
  if (mx1[u] == u) {
    f[u] -= val[g[u]];
    g[u] = u;
    f[u] += val[g[u]];
  }
  // printf("u=%lld,f=%lld,chose %lld\n",u,f[u],g[u]);
  // printf("p=%lld,f=%lld,chose %lld\n",p,f[p],g[p]);
  // do sth for u
  ans = max(ans, f[u]);

  for (int i = h[u]; i; i = e[i].nex) {
    const int v = e[i].t;
    if (v == p) continue;
    dfs4(v, u);
  }

  g[p] = gp, g[u] = gu;
  f[p] = fp, f[u] = fu;
}
signed main() {
  scanf("%lld%lld", &n, &m);
  FOR(i, 1, n - 1) {
    int u, v;
    scanf("%lld%lld", &u, &v);
    add_path(u, v, 0);
    add_path(v, u, 0);
  }
  // set 1 as root
  dfs(1, 0);
  FOR(i, 1, m) {
    int x, y;
    scanf("%lld%lld", &x, &y);
    int z = lca(x, y);
    // printf("lca(%lld,%lld)=%lld\n",x,y,z);
    // add val
    while (x != z) totlen++, val[x]++, x = fa[x];
    while (y != z) totlen++, val[y]++, y = fa[y];
  }
  // printf("val:");
  // FOR(i,1,n)printf("%lld%c",val[i]," \n"[i==n]);
  dfs2(1, 0);
  dfs3(1, 0);
  dfs4(1, 0);
  printf("%lld", totlen - ans);
  return 0;
}
