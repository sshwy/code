// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

const int N = 3005, LIM = 1 << 13, D = 13;

int id[N], totid;
void work(int n, vector<int> vp[D], vector<int> vq[D]) {
  FOR(i, 0, D - 1) {
    FOR(j, 1, n) {
      if (id[j] >> i & 1)
        vp[i].pb(j);
      else
        vq[i].pb(j);
    }
  }
}
int n;

void uni(vector<int> &v) {
  sort(v.begin(), v.end());
  v.erase(unique(v.begin(), v.end()), v.end());
  while (v.size() && v.back() > n) v.erase(v.end() - 1);
}
vector<int> tp[D], tq[D], ap[D + D], aq[D + D];
int ans;

int main() {
  scanf("%d", &n);
  FOR(i, 0, LIM - 1) {
    if (totid == n) break;
    if (__builtin_popcount(i) == 6) { id[++totid] = i; }
  }
  if (n % 2) {
    work(n / 2 + 1, tp, tq);
    FOR(i, 0, D - 1) {
      for (int x : tp[i]) ap[i].pb(x * 2 - 1), ap[i].pb(x * 2);
      for (int x : tq[i]) aq[i].pb(x * 2 - 1);
      tp[i].clear(), tq[i].clear();
    }
    work(n / 2, tp, tq);
    FOR(i, 0, D - 1) {
      for (int x : tp[i]) ap[i + D].pb(x * 2), ap[i + D].pb(x * 2 + 1);
      for (int x : tq[i]) aq[i + D].pb(x * 2);
      ap[i + D].pb(1);
    }
  } else {
    work(n / 2, tp, tq);
    FOR(i, 0, D - 1) {
      for (int x : tp[i]) ap[i].pb(x * 2 - 1), ap[i].pb(x * 2);
      for (int x : tq[i]) aq[i].pb(x * 2 - 1);
    }
    FOR(i, 0, D - 1) {
      for (int x : tp[i]) ap[i + D].pb(x * 2), ap[i + D].pb(x * 2 + 1);
      for (int x : tq[i]) aq[i + D].pb(x * 2);
      ap[i + D].pb(1);
    }
  }
  FOR(i, 0, D + D - 1) {
    uni(ap[i]);
    uni(aq[i]);
    if (ap[i].size() && aq[i].size()) ++ans;
  }
  printf("%d\n", ans);
  FOR(i, 0, D + D - 1) if (ap[i].size() && aq[i].size()) {
    printf("%lu %lu", ap[i].size(), aq[i].size());
    for (int x : ap[i]) printf(" %d", x);
    for (int x : aq[i]) printf(" %d", x);
    puts("");
  }
  return 0;
}
