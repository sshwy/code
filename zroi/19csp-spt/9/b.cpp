#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 505, K = 1002, MASK = 1 << 10, P = 1e9 + 7;

int n, m;
int a[N];
int f[MASK];
bool vis[MASK];

int F[N][K], G[N][K];

int main() {
  scanf("%d%d", &n, &m);
  FOR(i, 1, n) scanf("%d", &a[i]);
  sort(a + 1, a + n + 1);
  // auto f=F,g=G;
  int(*f)[1002] = F, (*g)[1002] = G;
  f[0][0] = 1;
  FOR(i, 1, n) {
    FOR(j, 0, i - 1) FOR(k, 0, m) g[j][k] = 0;
    FOR(j, 0, i - 1) {
      int x = j * (a[i] - a[i - 1]);
      FOR(k, 0, m) {
        if (!f[j][k] || k + x > m) continue;
        g[j + 1][k + x] = (g[j + 1][k + x] + f[j][k]) % P;
        if (j - 1 >= 0) g[j - 1][k + x] = (g[j - 1][k + x] + 1ll * f[j][k] * j) % P;
        g[j][k + x] = (g[j][k + x] + 1ll * f[j][k] * (j + 1)) % P;
      }
    }
    swap(f, g);
  }
  int ans = 0;
  FOR(i, 0, m) ans = (ans + f[0][i]) % P;
  printf("%d\n", ans);
  return 0;
}
