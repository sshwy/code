#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 5005;

int n;
int a[N], b[N], c[N], p[N];
int ans;

int main() {
  scanf("%d", &n);
  FOR(i, 1, n) scanf("%d", &a[i]);
  FOR(i, 1, n) scanf("%d", &b[i]);
  FOR(i, 1, n) p[i] = i;
  do {
    int tot = 0;
    FOR(i, 1, n) tot += a[i] < b[p[i]];
    if (tot > ans) {
      ans = tot;
      FOR(i, 1, n) c[i] = b[p[i]];
    } else if (tot == ans) {
      FOR(i, 1, n) {
        if (c[i] > b[p[i]])
          break;
        else if (c[i] < b[p[i]]) {
          FOR(i, 1, n) c[i] = b[p[i]];
          break;
        }
      }
    }
  } while (next_permutation(p + 1, p + n + 1));
  FOR(i, 1, n) printf("%d%c", c[i], " \n"[i == n]);
  return 0;
}
