#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 505, MASK = 1 << 10;

int n, k;
int a[N];
int f[MASK];
bool vis[MASK];

int dp(int mask, int lim) {
  if (mask == 0) return 1;
  int res = 0;
  for (int s = mask; s; s = (s - 1) & mask) {
    if ((s & -s) != (mask & -mask)) continue;
    assert(s);
    int mn = 0, mx = 0;
    FOR(i, 1, n) if (s >> (i - 1) & 1) {
      mn = a[i];
      break;
    }
    ROF(i, n, 1) if (s >> (i - 1) & 1) {
      mx = a[i];
      break;
    }
    if (mx - mn <= lim) { res += dp(mask - s, lim - (mx - mn)); }
  }
  return res;
}

int main() {
  scanf("%d%d", &n, &k);
  FOR(i, 1, n) scanf("%d", &a[i]);
  sort(a + 1, a + n + 1);
  printf("%d\n", dp((1 << n) - 1, k));
  return 0;
}
