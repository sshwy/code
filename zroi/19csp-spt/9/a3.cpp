#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 5005;

int n;
int b[N], c[N], p[N], tb[N];
pii a[N], t[N];
int ans;
int l[N], r[N];

int calc(int pos) {
  int las = 0, tot = 0;
  FOR(i, 1, pos) {
    ++las;
    while (las <= pos && a[las].fi >= tb[i]) ++las;
    if (las <= pos)
      ++tot;
    else
      break;
  }
  return tot;
}
void work(int pos) {
  if (pos == 1) {
    c[a[1].se] = b[1];
    return;
  }
  FOR(i, 1, pos) tb[i] = b[i];
  int tot = calc(pos), p = n + 1, pi = n + 1;
  FOR(i, 1, pos) t[i] = a[i];
  FOR(i, 1, pos - 1) tb[i] = b[i + 1];
  if (tot == 0) {
    FOR(i, 1, pos) if (t[i].se < p) p = t[i].se, pi = i;
  } else {
    FOR(i, 1, pos) {
      // assume b[1] -> a[i].se
      FOR(j, 1, i - 1) a[j] = t[j];
      FOR(j, i, pos - 1) a[j] = t[j + 1];
      if (calc(pos - 1) + (b[1] > t[i].fi) == tot) {
        if (t[i].se < p) { p = t[i].se, pi = i; }
      }
    }
  }
  assert(p != n + 1);
  c[p] = b[1];
  FOR(i, 1, pi - 1) a[i] = t[i];
  FOR(i, pi, pos - 1) a[i] = t[i + 1];
  FOR(i, 1, pos - 1) b[i] = b[i + 1];
}
int main() {
  scanf("%d", &n);
  FOR(i, 1, n) scanf("%d", &a[i].fi), a[i].se = i;
  FOR(i, 1, n) scanf("%d", &b[i]);
  sort(a + 1, a + n + 1);
  reverse(a + 1, a + n + 1);
  sort(b + 1, b + n + 1);
  reverse(b + 1, b + n + 1);
  ROF(i, n, 1) work(i);
  FOR(i, 1, n) printf("%d%c", c[i], " \n"[i == n]);
  return 0;
}
