#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 1e5 + 5;
int n, a[N], c[N], f[1 << 16];
bool vis[1 << 16];
#define popcnt(x) __builtin_popcount(x)
int calc(int mask) {
  int res = 0;
  FOR(i, 0, 15) if (mask >> i & 1) res ^= i;
  return res;
}
int dp(int mask) {
  if (mask == 0) return 0;
  if (vis[mask]) return f[mask];
  f[mask] = -0x3f3f3f3f;
  for (int s = mask; s; s = (s - 1) & mask)
    if (calc(s) == 0) f[mask] = max(f[mask], 1 + dp(mask - s));
  vis[mask] = 1;
  return f[mask];
}
int main() {
  scanf("%d", &n);
  FOR(i, 1, n - 1) {
    int u, v, w;
    scanf("%d%d%d", &u, &v, &w);
    a[u] ^= w, a[v] ^= w;
  }
  int ans = 0;
  FOR(i, 1, n) c[a[i]]++;
  FOR(i, 1, 15) ans += c[i] / 2, c[i] &= 1;
  int s = 0;
  FOR(i, 1, 15) s |= c[i] << i;
  printf("%d\n", popcnt(s) - dp(s) + ans);
  return 0;
}
