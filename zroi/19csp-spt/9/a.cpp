#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 5005;

int n, b[N], c[N];
pii a[N];

bool can_move[N]; // means if we remove i, we can move step 1.that can be easy,
                  // since we just calculate l[i] and check if there is a empty
                  // place
bool vis[N];
int calc1(int pos) { // match a[1..pos] to tb[1..pos]
  int las = 0, tot = 0;
  FOR(i, 1, pos) {
    ++las;
    while (las <= pos && a[las].fi >= b[i]) ++las;
    if (las <= pos) ++tot;
  }
  return tot;
}
int calc(int pos) { // match a[1..pos] to tb[1..pos-1]
  int tot = 0, las = 0;
  FOR(i, 1, pos) vis[i] = 0;
  FOR(i, 1, pos - 1) {
    ++las;
    while (las <= pos && a[las].fi >= b[i]) ++las;
    if (las <= pos) ++tot, vis[las] = 1;
  }
  can_move[pos] = !vis[pos];
  ROF(i, pos - 1, 1) vis[i] &= vis[i + 1], can_move[i] = !vis[i];
  return tot;
}
void work(int pos) {
  if (pos == 1) return c[a[1].se] = b[1], void();
  int tot = calc1(pos), b1 = b[1], p = n + 1, pi = n + 1;
  FOR(i, 1, pos - 1) b[i] = b[i + 1];
  if (tot == 0) {
    FOR(i, 1, pos) if (a[i].se < p) p = a[i].se, pi = i;
  } else {
    int t2 = calc(pos);
    ROF(i, pos, 1) {
      // assume b[1] -> a[i].se
      if (t2 - (!can_move[i]) + (b1 > a[i].fi) == tot)
        if (a[i].se < p) p = a[i].se, pi = i;
    }
  }
  assert(p != n + 1);
  c[p] = b1;
  FOR(i, pi, pos - 1) a[i] = a[i + 1];
}
int main() {
  scanf("%d", &n);
  FOR(i, 1, n) scanf("%d", &a[i].fi), a[i].se = i;
  FOR(i, 1, n) scanf("%d", &b[i]);
  sort(a + 1, a + n + 1), reverse(a + 1, a + n + 1);
  sort(b + 1, b + n + 1), reverse(b + 1, b + n + 1);
  ROF(i, n, 1) work(i);
  FOR(i, 1, n) printf("%d%c", c[i], " \n"[i == n]);
  return 0;
}
