#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 505, K = 505;

int n;
int len[N];
double p[N][K];
bool ban[N];
pair<double, double> line[N];
int tot;

bool cmp(pair<double, double> l1, pair<double, double> l2) {
  return (l2.fi - 1) * l1.se < (l1.fi - 1) * l2.se;
}
int main() {
  scanf("%d", &n);
  FOR(i, 1, n) {
    scanf("%d", &len[i]);
    FOR(j, 1, len[i]) {
      scanf("%lf", &p[i][j]);
      if (p[i][j] == 0) --j, --len[i];
    }
    sort(p[i] + 1, p[i] + len[i] + 1);
    reverse(p[i] + 1, p[i] + len[i] + 1);
    if (len[i] == 0 || p[i][1] == 1) {
      ban[i] = 1;
    } else if (p[i][1] == 0) {
      puts("0");
      return 0;
    }
  }
  FOR(i, 1, n) {
    if (ban[i]) continue;
    double k = 0, b = 0, pre = 1;
    FOR(j, 1, len[i]) {
      b += pre * p[i][j] * j;
      k += pre * p[i][j];
      pre *= 1 - p[i][j];
    }
    b += pre * len[i];
    line[++tot] = mk(k, b);
  }
  sort(line + 1, line + tot + 1, cmp);
  double ans = 0;
  FOR(i, 1, tot) ans = line[i].fi * ans + line[i].se;
  printf("%.10lf", ans);
  return 0;
}
