#include <algorithm>
#include <bitset>
#include <cassert>
#include <cctype>
#include <climits>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <functional>
#include <iostream>
#include <map>
#include <numeric>
#include <queue>
#include <set>
#include <stack>
#include <string>
#include <vector>
using namespace std;
#define rep(i, a, b) for (register int i = (a); i <= (b); ++i)
#define per(i, a, b) for (register int i = (a); i >= (b); --i)
#define loop(it, v) for (auto it = v.begin(); it != v.end(); it++)
#define cont(i, x) for (register int i = head[x]; i; i = edge[i].nex)
#define clr(a) memset(a, 0, sizeof(a))
#define ass(a, cnt) memset(a, cnt, sizeof(a))
#define cop(a, b) memcpy(a, b, sizeof(a))
#define lowbit(x) (x & -x)
#define all(x) x.begin(), x.end()
#define SC(t, x) static_cast<t>(x)
#define ub upper_bound
#define lb lower_bound
#define pqueue priority_queue
#define mp make_pair
#define pb push_back
#define pof pop_front
#define pob pop_back
#define fi first
#define se second
#define y1 y1_
#define Pi acos(-1.0)
#define iv inline void
#define enter putchar('\n')
#define siz(x) ((int)x.size())
#define file(x) freopen(x ".in", "r", stdin), freopen(x ".out", "w", stdout)
typedef double db;
typedef long long ll;
typedef unsigned long long ull;
typedef pair<int, int> pii;
typedef vector<int> vi;
typedef vector<pii> vii;
typedef queue<int> qi;
typedef queue<pii> qii;
typedef set<int> si;
typedef map<int, int> mii;
typedef map<string, int> msi;
const int maxn = 2e6 + 100;
const int inf = 0x3f3f3f3f;
const int iinf = 1 << 30;
const ll linf = 2e18;
const ll mod = 998244353;
const double eps = 1e-7;
template <class T = int> T read() {
  T f = 1, a = 0;
  char ch = getchar();
  while (!isdigit(ch)) {
    if (ch == '-') f = -1;
    ch = getchar();
  }
  while (isdigit(ch)) {
    a = (a << 3) + (a << 1) + ch - '0';
    ch = getchar();
  }
  return a * f;
}

int n, m, t, pos;

int a[maxn];

int suf[maxn], pre, b[maxn];

pii uni(pii a, pii b) {
  if (a.fi == b.fi) return mp(a.fi, a.se + b.se);
  return a > b ? a : b;
}

pii solve(int l, int r, int bit, int mask) {
  if (bit == -1) return mp(mask, 1);
  if (b[l] & (1 << bit)) return solve(l, r, bit - 1, mask | (1 << bit));
  if (!(b[r] & (1 << bit))) return solve(l, r, bit - 1, mask | (1 << bit));
  rep(i, l, r) if (!(b[i] & (1 << bit))) pos = i;
  else break;
  return uni(solve(l, pos, bit - 1, mask), solve(pos + 1, r, bit - 1, mask));
}

signed main() {
  scanf("%d%d%d", &n, &m, &t);
  rep(i, 1, m) scanf("%d", &a[i]);
  per(i, m, 1) suf[i] = suf[i + 1] ^ a[i];
  b[0] = suf[1];
  rep(i, 1, m) pre ^= ((a[i] << 1) + a[i] / (1 << (n - 1))) % (1 << n),
      b[i] = pre ^ suf[i + 1];
  sort(b, b + m + 1);
  pii ans = solve(0, m, n - 1, 0);
  printf("%d\n", ans.fi);
  if (t) printf("%d\n", ans.se);
  return 0;
}
