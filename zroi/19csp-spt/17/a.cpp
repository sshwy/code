#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 5004;

int n;
int lc[N], rc[N], fa[N], ans[N];
int f
    [N][N]
    [2]; // f[i,j,0/1]表示子树i内，每条路径黑点个数为j且第i个点是红/黑点的方案是否存在

void dfs(int u) { //每个点要么有两个儿子，要么只有左儿子，要么没有儿子
  if (!u) return;
  if (!lc[u] && !rc[u]) {
    f[u][1][1] = f[u][0][0] = 1;
    return;
  } else if (!rc[u]) {
    dfs(lc[u]);
    f[u][1][1] = f[lc[u]][0][0];
    f[u][0][0] = f[lc[u]][0][1];
    // FOR(i,0,n)f[u][i][0]=f[lc[u]][i][1];
    // FOR(i,1,n)f[u][i][1]=f[lc[u]][i-1][1]|f[lc[u]][i-1][0];
    return;
  } else {
    dfs(lc[u]), dfs(rc[u]);
    FOR(i, 0, n) f[u][i][0] = f[lc[u]][i][1] & f[rc[u]][i][1];
    FOR(i, 1, n)
    f[u][i][1] = (f[lc[u]][i - 1][1] | f[lc[u]][i - 1][0]) &
                 (f[rc[u]][i - 1][1] | f[rc[u]][i - 1][0]);
  }
}
void getans(int u, int cnt, int col) {
  if (!u) return;
  assert(f[u][cnt][col]);
  ans[u] = col;
  if (!lc[u] && !rc[u]) return;
  if (!rc[u]) {
    assert(cnt == col);
    if (cnt == 1)
      getans(lc[u], 0, 0);
    else
      getans(lc[u], 0, 1);
    return;
  } else {
    if (col == 0) {
      getans(lc[u], cnt, 1);
      getans(rc[u], cnt, 1);
    } else {
      if (f[lc[u]][cnt - 1][1])
        getans(lc[u], cnt - 1, 1);
      else
        getans(lc[u], cnt - 1, 0);
      if (f[rc[u]][cnt - 1][1])
        getans(rc[u], cnt - 1, 1);
      else
        getans(rc[u], cnt - 1, 0);
    }
  }
}
int main() {
  scanf("%d", &n);
  int root = 0;
  FOR(i, 1, n) {
    scanf("%d", &fa[i]);
    if (!fa[i])
      root = i;
    else if (lc[fa[i]])
      rc[fa[i]] = i;
    else
      lc[fa[i]] = i;
  }
  dfs(root);
  FOR(i, 0, n) {
    if (f[root][i][0]) {
      getans(root, i, 0);
      FOR(i, 1, n) printf("%c", "RB"[ans[i]]);
      return 0;
    }
    if (f[root][i][1]) {
      getans(root, i, 1);
      FOR(i, 1, n) printf("%c", "RB"[ans[i]]);
      return 0;
    }
  }
  puts("Impossible");
  return 0;
}
