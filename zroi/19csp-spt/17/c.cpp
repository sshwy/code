#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int M = 1e6 + 5;

int n, m, T;
int a[M], b[M], s;

pii calc(int l, int r,
    int bit) { //当前考虑的是第bit位。返回一个[0,2^bit)的作为最终值，以及达到这个最终值的方案数
  if (bit < 0) return mk(0, 1);
  int mid = -1;
  FOR(i, l, r) {
    if (b[i] >> bit & 1) {
      mid = i;
      break;
    }
  }
  //[l,mid-1],[mid,r]
  if (mid == -1 || mid == l) { //当前的这一位数都相同
    pii res = calc(l, r, bit - 1);
    res.fi |= 1 << bit; // b[i]的这一位都一样，则最终值的这一位显然可以为1
    return res;
  }
  pii resl = calc(l, mid - 1, bit - 1), resr = calc(mid, r, bit - 1);
  if (resl.fi == resr.fi) return mk(resl.fi, resl.se + resr.se);
  if (resl.fi < resr.fi) return resr;
  return resl;
}

int main() {
  scanf("%d%d%d", &n, &m, &T);
  FOR(i, 1, m) scanf("%d", &a[i]), s ^= a[i];
  FOR(i, 1, m) {
    b[i] = s;
    s ^= a[i];
    a[i] = ((a[i] >> (n - 1)) + (a[i] << 1)) & ((1 << n) - 1);
    s ^= a[i];
  }
  ++m;
  b[m] = s;
  sort(b + 1, b + m + 1);
  pii ans = calc(1, m, n - 1);
  printf("%d\n", ans.fi);
  if (T) printf("%d\n", ans.se);
  return 0;
}
