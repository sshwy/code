#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 4005;

int n, m;
char s[N], t[N];
bool check(int mask, int len) {
  // printf("check(%d,%d)\n",mask,len);
  int ps = 0, pt = 0;
  FOR(i, 0, len - 1) {
    if (mask >> (len - 1 - i) & 1) {
      while (++ps <= n)
        if (s[ps] == 1) break;
      while (++pt <= m)
        if (t[pt] == 1) break;
    } else {
      while (++ps <= n)
        if (s[ps] == 0) break;
      while (++pt <= m)
        if (t[pt] == 0) break;
    }
    // printf("ps=%d,pt=%d\n",ps,pt);
    if (ps > n && pt > m) return 1;
  }
  return 0;
}
int main() {
  scanf("%d%d", &n, &m);
  scanf("%s", s + 1);
  scanf("%s", t + 1);
  FOR(i, 1, n) s[i] -= '0';
  FOR(i, 1, m) t[i] -= '0';
  int len = 0;
  while (++len) {
    int lim = 1 << len;
    int fl = 0, ans = lim;
    FOR(i, 0, lim - 1) {
      if (check(i, len)) { ans = min(ans, i), fl = 1; }
    }
    if (fl) {
      FOR(j, 0, len - 1) printf("%d", ans >> (len - j - 1) & 1);
      return 0;
    }
  }
  return 0;
}
