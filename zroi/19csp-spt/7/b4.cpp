#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
#define LL long long
typedef pair<LL, LL> pll;
const int T = 5000;

LL n;

LL tot, ans;
struct cmp {
  bool operator()(const pll &a, const pll &b) const {
    if (a.fi <= 30 && b.fi <= 30)
      return (1ll << a.fi) * a.se > (1ll << b.fi) * b.se;
    else
      return pow(2, a.fi) * 1.0 * a.se > pow(2, b.fi) * 1.0 * b.se;
  }
};
pll a[T];
priority_queue<pll, vector<pll>, cmp> q;

int t;
LL as[T];
void go() {
  ans = tot = 0;
  int pos = 1;
  scanf("%lld", &n);
  // printf("n=%lld\n",n);
  while (!q.empty()) q.pop();
  q.push(mk(0, 1));
  while (pos <= t) {
    pll cur = q.top();
    q.pop();
    if (pow(2, cur.fi) * 1.0 * cur.se > 1e18)
      tot = 1e18;
    else
      tot += (1ll << cur.fi) * cur.se;
    ++ans;
    // printf("cur = (%lld,%lld)\n",cur.fi,cur.se);
    // printf("tot=%lld,ans=%lld\n",tot,ans);
    while (pos <= t && a[pos].fi < tot) as[a[pos].se] = ans - 1, ++pos;
    if (pos > t) break;
    if (cur.fi == 0) q.push(mk(cur.fi, cur.se + 1));
    q.push(mk(cur.fi + 1, cur.se));
  }
}
int main() {
  scanf("%d", &t);
  FOR(i, 1, t) {
    LL x;
    scanf("%lld", &x);
    a[i] = mk(x, i);
  }
  sort(a + 1, a + t + 1);
  // FOR(i,1,t)printf("%lld %lld\n",a[i].fi,a[i].se);
  go();
  FOR(i, 1, t) printf("%lld\n", as[i]);
  return 0;
}
