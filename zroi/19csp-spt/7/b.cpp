#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
#define LL long long
const int T = 5000, SZ = 1e7;

LL n;

LL calc(LL x) {
  LL res = 0;
  FOR(i, 0, 30) {
    if (!x) break;
    res += ((x + 1) * x / 2) << i;
    x >>= 1;
  }
  return res;
}
LL calc2(LL x) {
  LL res = 0;
  FOR(i, 0, 30) {
    res += x;
    x >>= 1;
  }
  return res;
}
void go() {
  scanf("%lld", &n);
  LL l = 0, r = 1e9;
  while (l < r) {
    LL mid = (l + r + 1) >> 1;
    if (calc(mid) <= n)
      l = mid;
    else
      r = mid - 1;
  }
  LL ans = calc2(l) + (n - calc(l)) / (l + 1);
  printf("%lld\n", ans);
}
int main() {
  int t;
  scanf("%d", &t);
  FOR(i, 1, t) go();
  return 0;
}
