#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 4005;

int n, m;
char s[N], t[N];
int snex[N][2], tnex[N][2];

bool vis[N][N], vs[N], vt[N];
int mtc[N][N], Sm[N], Tm[N];
int S_match(int pos) { // s[pos+1..n]'s SAS
  if (pos == n) return Sm[pos] = 1;
  if (pos > n) return Sm[pos] = 0;
  if (vs[pos]) return Sm[pos];
  vs[pos] = 1, Sm[pos] = 1 + min(S_match(snex[pos][0]), S_match(snex[pos][1]));
  // printf("\033[31m");
  // printf("S:%d <= %d:%d\n",pos,snex[pos][0],Sm[snex[pos][0]]);
  // printf("S:%d <= %d:%d\n",pos,snex[pos][1],Sm[snex[pos][1]]);
  // printf("Sm[%d]=%d\n",pos,Sm[pos]);
  // printf("\033[0m");
  return Sm[pos];
}
int T_match(int pos) { // t[pos+1..m]'s SAS
  if (pos == m) return Tm[pos] = 1;
  if (pos > m) return Tm[pos] = 0;
  if (vt[pos]) return Tm[pos];
  return vt[pos] = 1, Tm[pos] = 1 + min(T_match(tnex[pos][0]), T_match(tnex[pos][1]));
}
int match(int x, int y) { // s[x+1..n] & t[y+1..m]'s SCAS
  if (x > n && y > m) return mtc[x][y] = 0;
  if (x >= n && y >= m) return mtc[x][y] = 1;
  if (vis[x][y]) return mtc[x][y];
  if (x >= n) return vis[x][y] = 1, mtc[x][y] = T_match(y);
  if (y >= m) return vis[x][y] = 1, mtc[x][y] = S_match(x);
  vis[x][y] = 1,
  mtc[x][y] = 1 + min(match(snex[x][0], tnex[y][0]), match(snex[x][1], tnex[y][1]));
  // printf("(%d,%d) <=
  // (%d,%d):%d\n",x,y,snex[x][0],tnex[y][0],mtc[snex[x][0]][tnex[y][0]]);
  // printf("(%d,%d) <=
  // (%d,%d):%d\n",x,y,snex[x][1],tnex[y][1],mtc[snex[x][1]][tnex[y][1]]);
  // printf("mtc[%d,%d]=%d\n",x,y,mtc[x][y]);
  return mtc[x][y];
}
void S_match_print(int pos) {
  // printf("\nS_match_print(%d)\n",pos);
  if (pos > n) return void();
  if (pos >= n) return putchar('0'), void();
  // printf("\033[31m");
  // printf("S:%d <= %d:%d\n",pos,snex[pos][0],Sm[snex[pos][0]]);
  // printf("S:%d <= %d:%d\n",pos,snex[pos][1],Sm[snex[pos][1]]);
  // printf("Sm[%d]=%d\n",pos,Sm[pos]);
  // printf("\033[0m");
  if (Sm[snex[pos][0]] <= Sm[snex[pos][1]])
    putchar('0'), S_match_print(snex[pos][0]);
  else
    putchar('1'), S_match_print(snex[pos][1]);
}
void T_match_print(int pos) {
  // printf("\nT_match_print(%d)\n",pos);
  if (pos > m) return void();
  if (pos >= m) return putchar('0'), void();
  if (Tm[tnex[pos][0]] <= Tm[tnex[pos][1]])
    putchar('0'), T_match_print(tnex[pos][0]);
  else
    putchar('1'), T_match_print(tnex[pos][1]);
}
void match_print(int x, int y) {
  if (x > n && y > m) return void();
  if (x >= n && y >= m) return putchar('0'), void();
  if (x >= n) return T_match_print(y), void();
  if (y >= m) return S_match_print(x), void();
  if (mtc[snex[x][0]][tnex[y][0]] <= mtc[snex[x][1]][tnex[y][1]])
    putchar('0'), match_print(snex[x][0], tnex[y][0]);
  else
    putchar('1'), match_print(snex[x][1], tnex[y][1]);
}
int main() {
  scanf("%d%d", &n, &m);
  scanf("%s", s + 1);
  scanf("%s", t + 1);
  FOR(i, 1, n) s[i] -= '0';
  FOR(i, 1, m) t[i] -= '0';
  snex[n][0] = snex[n][1] = n + 1;
  tnex[m][0] = tnex[m][1] = m + 1;
  ROF(i, n - 1, 0) {
    snex[i][0] = s[i + 1] == 0 ? i + 1 : snex[i + 1][0];
    snex[i][1] = s[i + 1] == 1 ? i + 1 : snex[i + 1][1];
  }
  ROF(i, m - 1, 0) {
    tnex[i][0] = t[i + 1] == 0 ? i + 1 : tnex[i + 1][0];
    tnex[i][1] = t[i + 1] == 1 ? i + 1 : tnex[i + 1][1];
  }
  // FOR(i,0,n)printf("%d%c",snex[i][0]," \n"[i==n]);
  // FOR(i,0,n)printf("%d%c",snex[i][1]," \n"[i==n]);
  match(0, 0);
  // printf("%d\n",match(0,0));
  match_print(0, 0);
  return 0;
}
