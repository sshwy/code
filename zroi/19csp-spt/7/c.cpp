#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 3e5 + 5, INF = 0x3f3f3f3f;

int n;
int a[N];
long long ans;
vector<int> g[N];

struct fenwick {
  int c[N], sz;
  void init(int _sz = 0) {
    sz = _sz;
    FOR(i, 0, sz) c[i] = 0;
  }
  void add(int pos, int x) {
    for (int i = pos; i <= sz; i += i & -i) c[i] += x;
  }
  int pre(int pos = -1) {
    int res = 0;
    for (int i = ~pos ? pos : sz; i > 0; i -= i & -i) res += c[i];
    return res;
  }
} b;
int main() {
  scanf("%d", &n);
  b.init(n);
  FOR(i, 1, n) b.add(i, 1);
  FOR(i, 1, n) scanf("%d", &a[i]), g[a[i]].pb(i);
  FOR(i, 1, n) if (g[i].size()) {
    int l = 0, r = g[i].size() - 1;
    while (l <= r) {
      const int &L = g[i][l], &R = g[i][r];
      if (b.pre(L - 1) < b.pre() - b.pre(R))
        ans += b.pre(L - 1), b.add(L, -1), ++l;
      else
        ans += b.pre() - b.pre(R), b.add(R, -1), --r;
    }
  }
  printf("%lld\n", ans);
  return 0;
}
