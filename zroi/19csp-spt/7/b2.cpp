#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
#define LL long long
const int N = 100;

LL n;

int b[N], tot, ans;
void dfs(int k, int rest, int las) {
  ans = max(ans, tot);
  if (k == 0) { return; }
  FOR(i, las, rest - k + 1) {
    tot -= __builtin_popcount(b[i]);
    ++b[i];
    tot += __builtin_popcount(b[i]);
    dfs(k - 1, rest - i, i);
    tot -= __builtin_popcount(b[i]);
    --b[i];
    tot += __builtin_popcount(b[i]);
  }
}
void go() {
  ans = 0;
  scanf("%lld", &n);
  FOR(i, 1, n) dfs(i, n, 1);
  printf("%d\n", ans);
}
int main() {
  int t;
  scanf("%d", &t);
  FOR(i, 1, t) go();
  return 0;
}
