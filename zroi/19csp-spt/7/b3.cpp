#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
#define LL long long
typedef pair<LL, LL> pll;
const int N = 100;

LL n;

int b[N], tot, ans;
struct cmp {
  bool operator()(const pll &a, const pll &b) const {
    if (a.fi <= 30 && b.fi <= 30)
      return (1ll << a.fi) * a.se > (1ll << b.fi) * b.se;
    else
      return pow(2, a.fi) * 1.0 * a.se > pow(2, b.fi) * 1.0 * b.se;
  }
};
priority_queue<pll, vector<pll>, cmp> q;
void go() {
  ans = 0;
  scanf("%lld", &n);
  // printf("n=%lld\n",n);
  while (!q.empty()) q.pop();
  q.push(mk(0, 1));
  while (n) {
    pll cur = q.top();
    q.pop();
    // printf("cur = (%lld,%lld)\n",cur.fi,cur.se);
    if (pow(2, cur.fi) * 1.0 * cur.se > n) break;
    ++ans, n -= (1ll << cur.fi) * cur.se;
    if (cur.fi == 0) q.push(mk(cur.fi, cur.se + 1));
    q.push(mk(cur.fi + 1, cur.se));
  }
  printf("%d\n", ans);
}
int main() {
  int t;
  scanf("%d", &t);
  FOR(i, 1, t) go();
  return 0;
}
