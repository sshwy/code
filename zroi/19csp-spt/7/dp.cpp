#include <cstdio>
#include <cstdlib>
#include <ctime>
using namespace std;

int main() {
  system("g++ b.cpp -o .usr");
  system("g++ b4.cpp -o .std");
  system("g++ b_gen.cpp -o .gen");
  int t = 0;
  while (++t < 100000) {
    system("./.gen > .fin");
    system("./.usr < .fin > .fout");
    system("./.std < .fin > .fstd");
    if (system("diff .fout .fstd")) break;
    printf("AC#%d\n", t);
  }
  printf("WA#%d\n", t);
  return 0;
}
