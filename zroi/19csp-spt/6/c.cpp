#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
#define int lld
const int N = 2e3 + 4, P = 998244353;

struct qxx {
  int nex, t;
};
qxx e[N * 2];
int h[N], le = 1;
void add_path(int f, int t) { e[++le] = (qxx){h[f], t}, h[f] = le; }
#define FORe(i, u, v) for (int i = h[u], v; v = e[i].t, i; i = e[i].nex)
int n, nn, tot;

int f[N][N], g[N], g1[N], sz[N];
int dfs(int u, int p) {
  sz[u] = 1;
  FORe(i, u, v) if (v != p) sz[u] += dfs(v, u);
  if (sz[u] == 1) return f[u][0] = f[u][1] = 1, 1;
  memset(g, 0, sizeof(int) * (sz[u] + 1));
  memset(g1, 0, sizeof(int) * (sz[u] + 1));
  g[0] = 1;
  int lim = 0;
  FORe(i, u, v) {
    if (v == p) continue;
    lim += sz[v];
    FOR(i, 0, lim) {
      g1[i] = 0;
      int limj = min(sz[v], i);
      FOR(j, 0, limj) g1[i] = (g1[i] + 1ll * f[v][j] * g[i - j]) % P;
    }
    memcpy(g, g1, sizeof(int) * (lim + 1));
  }
  f[u][1] += g[0];
  FOR(i, 0, lim) f[u][i] += g[i + 1] + g[i];
  FOR(i, 0, lim) f[u][i] %= P;
  return sz[u];
}
signed main() {
  scanf("%lld", &n);
  FOR(i, 1, n - 1) {
    int u, v;
    scanf("%lld%lld", &u, &v);
    add_path(u, v), add_path(v, u);
  }
  dfs(1, 0);
  tot = 1;
  FOR(i, 1, n) tot = tot * 2 % P;
  tot -= f[1][0];
  tot = (tot + P) % P;
  printf("%lld\n", tot);
  return 0;
}
