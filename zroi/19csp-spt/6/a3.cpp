#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  long long r(long long p) { return 1ll * rand() * rand() % p; }
  long long r(long long L, long long R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
#define LL long long
const LL N = 2e5 + 5, H = 1e6 + 5;

LL n, T;
LL h[N];
LL tot;

struct dsu {
  LL f[N], s[N], c[N], mn[N];
  void init(int k) { FOR(i, 0, k) f[i] = i, s[i] = mn[i] = h[i], c[i] = 1; }
  int get(int u) { return f[u] == u ? u : f[u] = get(f[u]); }
  void merge(int u, int v) {
    u = get(u), v = get(v);
    int x = min(mn[u], mn[v]);
    tot += (s[u] - x * c[u]) * c[v];
    tot += (s[v] - x * c[v]) * c[u];
    f[u] = v, s[v] += s[u], c[v] += c[u], mn[v] = x;
  }
  bool find(int u, int v) { return get(u) == get(v); }
} d;
pii tmp[N];

int main() {
  scanf("%lld", &n);
  FOR(i, 1, n) scanf("%lld", &h[i]);
  scanf("%lld", &T);
  d.init(n);
  FOR(i, 1, n - 1) tmp[i] = mk(-min(h[i], h[i + 1]), i);
  sort(tmp + 1, tmp + n);
  FOR(i, 1, n - 1) {
    assert(!d.find(tmp[i].se, tmp[i].se + 1));
    d.merge(tmp[i].se, tmp[i].se + 1);
  }
  FOR(i, 1, n) tot += i * (n - i);
  scanf("%lld", &T);
  printf("%lld\n", tot);
  return 0;
}
