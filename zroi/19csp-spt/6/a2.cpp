#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 2e5 + 5;

int n, T;
int h[N];
long long tot;

int main() {
  scanf("%d", &n);
  FOR(i, 1, n) scanf("%d", &h[i]);
  scanf("%d", &T);
  FOR(i, 1, n) {
    int mn = h[i];
    FOR(j, i + 1, n) {
      mn = min(mn, h[j]);
      tot += abs(h[i] - mn) + abs(h[j] - mn) + j - i;
    }
  }
  printf("%lld\n", tot);
  return 0;
}
