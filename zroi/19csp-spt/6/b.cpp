#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 2e5 + 5, M = N;

int n, m;
int a[N], b[N];

set<int> s;
int tot, c1; //统计非零数的个数
vector<int> g[N];

void insert(int x) {
  // printf("insert(%d)\n",x);
  if (s.empty()) {
    s.insert(x);
    return;
  }
  // assert(s.find(x)==s.end());//保证不存在
  set<int>::iterator it = s.lower_bound(x);
  int l, r;
  if (it == s.end() || it == s.begin()) {
    it = s.end();
    --it;
    l = *it, r = *s.begin();
  } else {
    r = *it, --it, l = *it;
  }
  // printf("l=%d,r=%d\n",l,r);
  int len = (r - l);
  int l1 = (x - l), l2 = (r - x);
  if (len < 0) len += n;
  if (l1 < 0) l1 += n;
  if (l2 < 0) l2 += n;
  tot -= len / 2;
  tot += l1 / 2;
  tot += l2 / 2;
  s.insert(x);
}
void remove(int x) {
  // printf("remove(%d)\n",x);
  // assert(s.find(x)!=s.end());//保证存在
  set<int>::iterator it = s.find(x), tt = it;
  ++tt;
  if (s.size() == 1) {
    s.erase(x);
    return;
  }
  int l, r;
  if (it == s.begin()) {
    ++it, r = *it;
    it = s.end(), --it, l = *it;
  } else if (tt == s.end()) {
    --it, l = *it;
    r = *s.begin();
  } else {
    --it, l = *it;
    ++it, ++it, r = *it;
  }
  // printf("l=%d,r=%d\n",l,r);
  int len = (r - l);
  int l1 = (x - l), l2 = (r - x);
  if (len < 0) len += n;
  if (l1 < 0) l1 += n;
  if (l2 < 0) l2 += n;
  tot -= l1 / 2;
  tot -= l2 / 2;
  tot += len / 2;
  s.erase(x);
}
int getans() {
  if (c1 == n) return -1;
  return tot + c1;
}
int calc(int k) {
  // printf("\033[31mcalc(%d)\033[0m\n",k);
  //把等于k的都变成0
  for (int pos : g[k]) {
    // printf("TURN %d to 0\n",pos);
    //把b[pos]置为0
    //如果一开始b[pos-1]!=b[pos],且都不为0。那么就需要添加分界线pos。否则不需要额外添加
    // assert(b[pos]==1);
    if (b[(pos - 1 + n) % n] + b[pos] == 0) { insert(pos); }
    //还有pos+1
    if (b[(pos + 1) % n] + b[pos] == 0) { insert((pos + 1) % n); }
    b[pos] = 0, c1--;
  }
  //把等于k-1的都变成-1
  for (int pos : g[k - 1]) {
    // printf("TURN %d to -1\n",pos);
    //把b[pos]置为-1
    // assert(b[pos]==0);
    b[pos] = -1, c1++;
    if (b[(pos - 1 + n) % n] + b[pos] == 0) { remove(pos); }
    //还有pos+1
    if (b[(pos + 1) % n] + b[pos] == 0) { remove((pos + 1) % n); }
  }
  // FOR(i,0,n-1)printf("%d%c",b[i]," \n"[i==n-1]);
  return getans();
}
void init() { // i表示第i个元素前的分割线
  FOR(i, 0, n - 1) b[i] = 1;
  FOR(i, 0, n - 1) s.insert(i); //初始时都是1
  c1 = n;
}
int main() {
  scanf("%d%d", &n, &m);
  init();
  FOR(i, 0, n - 1) scanf("%d", &a[i]), g[a[i]].pb(i);
  FOR(k, 1, m) printf("%d ", calc(k));
  return 0;
}
