#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 1e5 + 5;

int n;
int a[N];
int q1[N], q2[N], l1, l2;

bool v1[N], v2[N];
priority_queue<pii, vector<pii>, greater<pii>> pq1, pq2;
int tot;
int ans[N];
int ans2[N];
void calc() {
  memset(v1, 0, sizeof(bool) * (n + 3));
  memset(v2, 0, sizeof(bool) * (n + 3));
  int p1 = 1, p2 = 1;
  int c1 = 1, c2 = 1;
  printf("q1:");
  FOR(i, 1, l1) printf("%d%c", q1[i], " \n"[i == l1]);
  printf("q2:");
  FOR(i, 1, l2) printf("%d%c", q2[i], " \n"[i == l2]);
  while (pq1.size()) pq1.pop();
  while (pq2.size()) pq2.pop();
  while (p1 <= l1 && p2 <= l2) {
    if (v1[p1])
      ++p1;
    else if (v2[p2])
      ++p2;
    else {
      while (c1 <= l1 && c2 <= l2 && (q2[c2] <= q1[p1] || q1[c1] <= q2[p2])) {
        pq1.push(mk(a[q1[c1]], c1));
        pq2.push(mk(a[q2[c2]], c2));
        printf("push q1:%d and q2:%d\n", c1, c2);
        ++c1, ++c2;
      }
      if (q1[p1] < q2[p2]) {
        assert(pq2.size());
        pii cur = pq2.top();
        pq2.pop();
        printf("match %d,%d\n", q1[p1], q2[cur.se]);
        // FOR(i,1,n)printf("%d%c",ans[i]," \n"[i==n]);
        swap(ans[q1[p1]], ans[q2[cur.se]]);
        tot += abs(q2[cur.se] - q1[p1]);
        // FOR(i,1,n)printf("%d%c",ans[i]," \n"[i==n]);
        ++p1, v2[cur.se] = 1;
      } else {
        assert(pq1.size());
        pii cur = pq1.top();
        pq1.pop();
        printf("match %d,%d\n", q1[cur.se], q2[p2]);
        swap(ans[q1[cur.se]], ans[q2[p2]]);
        tot += abs(q1[cur.se] - q2[p2]);
        ++p2, v1[cur.se] = 1;
      }
    }
  }
}
int f2 = 0, tot2 = 0;
int main() {
  // freopen("seq2.in","r",stdin);
  scanf("%d", &n);
  FOR(i, 1, n) scanf("%d", &a[i]);
  l1 = l2 = tot = 0;
  FOR(i, 1, n) {
    ans[i] = a[i];
    if ((i & 1) == (a[i] & 1)) { continue; }
    if (a[i] & 1)
      q1[++l1] = i;
    else
      q2[++l2] = i;
  }
  if (l1 == l2) {
    puts("#1");
    calc();
    printf("tot=%d,", tot);
    FOR(i, 1, n) printf("%d%c", ans[i], " \n"[i == n]);
    FOR(i, 1, n) ans2[i] = ans[i];
    tot2 = tot;
    f2 = 1;
  }
  l1 = l2 = tot = 0;
  FOR(i, 1, n) {
    ans[i] = a[i];
    if ((i & 1) != (a[i] & 1)) { continue; }
    if (a[i] & 1)
      q1[++l1] = i;
    else
      q2[++l2] = i;
  }
  if (l1 == l2) {
    puts("#2");
    calc();
    printf("tot=%d,", tot);
    FOR(i, 1, n) printf("%d%c", ans[i], " \n"[i == n]);
    if (f2) {
      if (tot2 < tot) {
        FOR(j, 1, n) ans[j] = ans2[j];
      } else if (tot2 == tot) {
        FOR(i, 1, n) {
          if (ans2[i] == ans[i])
            continue;
          else if (ans2[i] > ans[i])
            break;
          FOR(j, 1, n) ans[j] = ans2[j];
          break;
        }
      }
    }
  } else {
    FOR(i, 1, n) ans[i] = ans2[i];
  }
  FOR(i, 1, n) printf("%d%c", ans[i], " \n"[i == n]);
  return 0;
}
