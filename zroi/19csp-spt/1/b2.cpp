#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 2e5 + 5;

int n, q, k;
int c[N], vis[N];

int main() {
  scanf("%d%d%d", &n, &q, &k);
  FOR(i, 1, n) scanf("%d", &c[i]);
  FOR(i, 1, q) {
    int x;
    scanf("%d", &x);
    vis[x] = !vis[x];
    int ans = 0;
    FOR(j, 1, n) {
      if (vis[c[j]] && !vis[c[j + 1]]) ans++;
    }
    printf("%d\n", ans);
  }
  return 0;
}
