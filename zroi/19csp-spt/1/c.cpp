#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
#define int long long
const int N = 2e5 + 5, P = 998244353;
int pw(int a, int m, int p) {
  a %= p;
  int res = 1;
  while (m) m & 1 ? res = 1ll * res * a % p : 0, a = 1ll * a * a % p, m >>= 1;
  return res;
}

int n, a, b, p, q;

int v[N], g[N], ans;
int w(int i, int j) {
  if (i < j)
    return p;
  else
    return P + 1 - p;
}
int calc(int mask) {
  int res = 1;
  FOR(i, 0, n - 1) FOR(j, 0, n - 1) {
    if (!(mask >> i & 1)) continue;
    if (mask >> j & 1) continue;
    res = res * w(i, j) % P;
  }
  return res;
}
int mp[N], mq[N];
int f[N];
signed main() {
  scanf("%lld%lld%lld", &n, &a, &b);
  p = a * pw(b, P - 2, P) % P;
  q = (P + 1 - p) % P;
  // printf("p=%lld,q=%lld\n",p,q);
  mp[0] = mq[0] = 1;
  FOR(i, 1, n) mp[i] = mp[i - 1] * p % P, mq[i] = mq[i - 1] * q % P;
  if (p != q) {
    f[0] = 1;
    FOR(i, 1, n - 1) {
      f[i] = f[i - 1] * (mp[n - i + 1] - mq[n - i + 1]) % P *
             pw(mp[i] - mq[i], P - 2, P) % P;
      f[i] = (f[i] + P) % P;
    }
  } else {
    int binom = 1, i2 = 998244354 / 2;
    FOR(i, 1, n - 1) {
      binom = binom * (n - i + 1) % P * pw(i, P - 2, P) % P;
      f[i] = binom * pw(i2, i * (n - i), P) % P;
    }
  }

  g[1] = 1;
  FOR(i, 2, n - 1) g[i] = (g[i - 1] * g[i - 1] + 2) % P;
  FOR(i, 1, n - 1) ans = (ans + f[i] * g[i]) % P;
  printf("%lld\n", ans);
  return 0;
}
