#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 1e5 + 5;

int n;
int a[N], c1, c2;
int ans[2][N], tot[2];

int vpos[N], pos[N], lv, lp;
priority_queue<int> q2;
priority_queue<int, vector<int>, greater<int>> q1;
bool check(int x, int y) {
  return (vpos[x] < pos[x]) == (vpos[y] < pos[y]) &&
         (vpos[x] == pos[x]) == (vpos[y] == pos[y]);
}
void calc(int id, int l, int r) {
  if (vpos[l] == pos[l]) {
    FOR(i, l, r) ans[id][pos[i]] = a[vpos[i]];
    return;
  } else if (vpos[l] < pos[l]) { // left
    int cur = l, curv = l;
    while (cur <= r) { // match pos[cur]
      while (curv <= r && vpos[curv] <= pos[cur]) q1.push(a[vpos[curv]]), ++curv;
      ans[id][pos[cur]] = q1.top();
      q1.pop();
      ++cur;
    }
  } else {
    int cur = r, curv = r;
    while (cur >= l) { // match pos[cur]
      while (curv >= l && vpos[curv] >= pos[cur]) q2.push(a[vpos[curv]]), --curv;
      ans[id][pos[cur]] = q2.top();
      q2.pop();
      --cur;
    }
  }
}
void work(int id) { // calc vpos & pos, record answer to ans[]
  int l = 1, r = 1;
  while (l <= lv) {
    r = l;
    while (r <= n && check(l, r)) ++r;
    calc(id, l, r - 1);
    l = r;
  }
}
void go(int id) {
  tot[id] = 0;
  lv = lp = 0;
  FOR(i, 1, n) {
    if (a[i] & 1) vpos[++lv] = i;
    if (i & 1) pos[++lp] = i;
  }
  if (lv != lp) return;
  FOR(i, 1, lv) tot[id] += abs(vpos[i] - pos[i]);
  work(id);
  lv = lp = 0;
  FOR(i, 1, n) {
    if (!(a[i] & 1)) vpos[++lv] = i;
    if (!(i & 1)) pos[++lp] = i;
  }
  if (lv != lp) return;
  FOR(i, 1, lv) tot[id] += abs(vpos[i] - pos[i]);
  work(id);
  ans[id][0] = 1; // get answer
}
int main() {
  scanf("%d", &n);
  FOR(i, 1, n) scanf("%d", &a[i]);
  go(0);
  FOR(i, 1, n) a[i]++;
  go(1);
  FOR(i, 1, n) ans[1][i]--;
  if (ans[0][0] && ans[1][0]) {
    if (tot[1] < tot[0]) {
      FOR(i, 1, n) ans[0][i] = ans[1][i];
    } else if (tot[1] == tot[0]) {
      FOR(i, 1, n) {
        if (ans[0][i] == ans[1][i])
          continue;
        else if (ans[0][i] > ans[1][i]) {
          FOR(i, 1, n) ans[0][i] = ans[1][i];
          break;
        } else
          break;
      }
    }
  } else if (ans[1][0]) {
    FOR(i, 1, n) ans[0][i] = ans[1][i];
  }
  FOR(i, 1, n) printf("%d%c", ans[0][i], " \n"[i == n]);
  return 0;
}
