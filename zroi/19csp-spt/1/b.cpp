#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
namespace IO {
  char nc() {
    static char bf[100000], *p1 = bf, *p2 = bf;
    return p1 == p2 && (p2 = (p1 = bf) + fread(bf, 1, 100000, stdin), p1 == p2)
               ? EOF
               : *p1++;
  }
  int rd() {
    int res = 0;
    char c = getchar();
    while (!isdigit(c)) c = getchar();
    while (isdigit(c)) res = res * 10 + c - '0', c = getchar();
    return res;
  }
} // namespace IO
using namespace IO;

const int N = 3e5 + 5;

int n, q, k, T;
int c[N], vis[N], tag[N]; // tag对big有定义，表示有多少个小灯是与它相邻并亮着的
int self[N];
int col[N], on, ct;

vector<int> pos[N];
int cnt[N], id[N], totid, rk[N];
int g[600][600];

int main() {
  n = rd(), q = rd(), k = rd();
  T = sqrt(n);
  FOR(i, 1, n) {
    c[i] = rd();
    if (c[i] == c[i - 1]) --i, --n;
  }
  FOR(i, 1, n) col[c[i]]++, pos[c[i]].pb(i);
  FOR(i, 1, k) {
    if (col[i] >= T) id[i] = ++totid, rk[totid] = i;
  }
  FOR(i, 1, n - 1)
  if (col[c[i]] >= T && col[c[i + 1]] >= T)
    g[id[c[i]]][id[c[i + 1]]]++, g[id[c[i + 1]]][id[c[i]]]++;
  FOR(ii, 1, q) {
    int x;
    scanf("%d", &x);
    int sig = vis[x] ? -1 : 1;
    vis[x] ^= 1;
    if (col[x] < T) { // small
      for (int cur : pos[x]) {
        int cc1 = c[cur - 1];
        if (cur - 1 > 0) {
          if (vis[cc1]) ct += sig;
          if (col[cc1] >= T) tag[cc1] += sig;
        }
        cc1 = c[cur + 1];
        if (cur + 1 <= n) {
          if (col[cc1] >= T) tag[cc1] += sig;
          if (vis[cc1]) ct += sig;
        }
      }
    } else { // big
      int di = id[x];
      FOR(i, 1, totid) {
        if (vis[rk[i]]) ct += g[di][i] * sig;
      }
      ct += tag[x] * sig;
    }
    on += col[x] * sig;
    printf("%d\n", on - ct);
  }
  return 0;
}
/*
 * BUG#1:没有圧边为边数
 * BUG#2:没有O(n)预处理大点之间的边
 */
