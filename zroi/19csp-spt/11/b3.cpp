#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
#define int lld
const int N = 5005, INF = 0x3f3f3f3f3f3f3f3f;

int n;
int a[N], b[N];
int f[N][N][2];
bool vis[N][N][2];

int st[N][20];
void init() {
  FOR(i, 1, n - 1) st[i][0] = b[i];
  FOR(j, 1, 20) {
    FOR(i, 1, n - (1 << j)) {
      st[i][j] = max(st[i][j - 1], st[i + (1 << (j - 1))][j - 1]);
    }
  }
}
int getmax(int l, int r) {
  int j = log(r - l + 1) / log(2);
  return max(st[l][j], st[r - (1 << j) + 1][j]);
}
int dp(int l, int r, int tag) {
  if (l == 1 && r == n) return 0;
  if (vis[l][r][tag]) return f[l][r][tag];
  if (tag == 0)
    f[l][r][0] = min(l > 1 ? b[l - 1] + max(0ll, dp(l - 1, r, 0) - a[l - 1]) : INF,
        r < n ? max(getmax(l, r), dp(l, r + 1, 1) + b[r] - a[r + 1]) : INF);
  else
    f[l][r][1] = min(r < n ? b[r] + max(0ll, dp(l, r + 1, 1) - a[r + 1]) : INF,
        l > 1 ? max(getmax(l - 1, r - 1), dp(l - 1, r, 0) + b[l - 1] - a[l - 1])
              : INF);
  vis[l][r][tag] = 1;
  return f[l][r][tag];
}
signed main() {
  scanf("%lld", &n);
  FOR(i, 1, n) scanf("%lld", &a[i]);
  FOR(i, 1, n - 1) scanf("%lld", &b[i]);
  init();
  FOR(i, 1, n) {
    assert(dp(i, i, 0) == dp(i, i, 1));
    printf("%lld ", max(0ll, dp(i, i, 0) - a[i]));
  }
  return 0;
}
