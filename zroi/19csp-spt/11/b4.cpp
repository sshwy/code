#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
#define int lld
const int N = 5005, INF = 0x3f3f3f3f3f3f3f3f;

int n;
int a[N], b[N];

namespace g1 {
  int f[N][N];
  int D[N][N], R[N][N];

  void calc(int x, int y) {
    assert(x <= y);
    if (x == y) return;
    // x,y-1 -> x,y
    D[x][y] = max(0ll, (b[y - 1] - b[x - 1]) - (a[y - 1] - a[x - 1]));
    // x+1,y -> x,y
    R[x][y] = max(0ll, (b[y - 1] - b[x - 1]) - (a[y] - a[x]));
    // printf("D[%lld,%lld]=%lld,R[%lld,%lld]=%lld\n",x,y,D[x][y],x,y,R[x][y]);
  }
  signed go1() {
    FOR(i, 1, n) scanf("%lld", &a[i]), a[i] += a[i - 1];
    FOR(i, 1, n - 1) scanf("%lld", &b[i]), b[i] += b[i - 1];
    FOR(i, 1, n) FOR(j, i, n) calc(i, j), f[i][j] = INF;
    f[1][n] = 0;
    ROF(len, n - 1, 1) {
      FOR(i, 1, n - len) {
        int j = i + len;
        f[i][j - 1] = min(f[i][j - 1], max(f[i][j], D[i][j]));
        f[i + 1][j] = min(f[i + 1][j], max(f[i][j], R[i][j]));
      }
    }
    FOR(i, 1, n) printf("%lld%c", f[i][i], " \n"[i == n]);
    return 0;
  }
} // namespace g1

namespace g2 {

  int f[N][N][2]; //走到l,r的左/右最少剩下多少（把当前格子的拿在手上）
  bool vis[N][N][2];

  int st[N][20];
  void init() {
    FOR(i, 1, n - 1) st[i][0] = b[i];
    FOR(j, 1, 20) {
      FOR(i, 1, n - (1 << j)) {
        st[i][j] = max(st[i][j - 1], st[i + (1 << (j - 1))][j - 1]);
      }
    }
  }
  int getmax(int l, int r) {
    int j = log(r - l + 1) / log(2);
    return max(st[l][j], st[r - (1 << j) + 1][j]);
  }
  int dp(int l, int r, int tag) {
    // printf("dp(%lld,%lld,%lld)\n",l,r,tag);
    if (l == r) return f[l][r][tag];
    if (vis[l][r][tag]) return f[l][r][tag];
    if (tag == 0) { // left
      f[l][r][0] = min(f[l][r][0], max(0ll, dp(l + 1, r, 0) - b[l]) + a[l]);
      // or coming from f[l+1,r,1]
      int mx = getmax(l, r - 1);
      f[l][r][0] = min(f[l][r][0], max(0ll, max(mx, dp(l + 1, r, 1)) - b[l]) + a[l]);
    } else {
      f[l][r][1] = min(f[l][r][1], max(0ll, dp(l, r - 1, 1) - b[r - 1]) + a[r]);
      int mx = getmax(l, r - 1);
      f[l][r][1] =
          min(f[l][r][1], max(0ll, max(mx, dp(l, r - 1, 0)) - b[r - 1]) + a[r]);
    }
    vis[l][r][tag] = 1;
    return f[l][r][tag];
  }
  int tot, sum;
  void work(int x) {
    FOR(i, 1, n)
    FOR(j, 1, n) f[i][j][0] = f[i][j][1] = INF, vis[i][j][0] = vis[i][j][1] = 0;
    f[x][x][0] = f[x][x][1] = a[x];
    int c1 = dp(1, n, 0) + tot - sum;
    int c2 = dp(1, n, 1) + tot - sum;
    c1 = min(c1, c2);
    printf("%lld ", c1);
  }
  signed go2() {
    FOR(i, 1, n) scanf("%lld", &a[i]), sum += a[i];
    FOR(i, 1, n - 1) scanf("%lld", &b[i]), tot += b[i];
    init();
    FOR(i, 1, n) { work(i); }
    return 0;
  }
} // namespace g2
signed main() {
  scanf("%lld", &n);
  if (n <= 200)
    g2::go2();
  else
    g1::go1();
}
