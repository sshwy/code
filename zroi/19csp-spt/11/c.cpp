#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
#define LL long long
#define min3(a, b, c) min(min(a, b), c)
#define max3(a, b, c) max(max(a, b), c)
const int N = 1e5 + 5, SZ = 1 << 25, INF = 0x3f3f3f3f;

int n, maxv;
LL F, a[N][4];
LL dc[2000005], ld;

pair<LL, LL> range[N][4];

int val[SZ];
int tadd[SZ], tmin[SZ], tsufmin[SZ]; // tsufmin是一个bool的tag
void pushup(int u) { val[u] = min(val[u << 1], val[u << 1 | 1]); }
void node_add(int u, int v) {
  val[u] += v;
  tadd[u] += v;
  tmin[u] += v;
}
void node_assign_min(int u, int v) {
  val[u] = min(val[u], v);
  tmin[u] = min(tmin[u], v);
}
void node_assign_sufmin(int u) { //区间最小值是不会改变的
  tsufmin[u] = 1;
}
void pushdown(int u) {
  if (tadd[u]) node_add(u << 1, tadd[u]), node_add(u << 1 | 1, tadd[u]), tadd[u] = 0;
  if (tmin[u] < INF)
    node_assign_min(u << 1, tmin[u]), node_assign_min(u << 1 | 1, tmin[u]),
        tmin[u] = INF;
  if (tsufmin[u])
    node_assign_min(u << 1, val[u << 1 | 1]), node_assign_sufmin(u << 1),
        node_assign_sufmin(u << 1 | 1), tsufmin[u] = 0;
}
void build(int u = 1, int l = 1, int r = maxv) {
  tmin[u] = INF;
  if (l == r) return val[u] = l == maxv ? 0 : INF, void();
  int mid = (l + r) >> 1;
  build(u << 1, l, mid), build(u << 1 | 1, mid + 1, r);
  pushup(u);
}
void add(int L, int R, int v, int u = 1, int l = 1, int r = maxv) {
  if (L > R) return;
  if (L <= l && r <= R) return node_add(u, v);
  int mid = (l + r) >> 1;
  pushdown(u);
  if (L <= mid) add(L, R, v, u << 1, l, mid);
  if (mid < R) add(L, R, v, u << 1 | 1, mid + 1, r);
  pushup(u);
}
int get_min(int L, int R, int u = 1, int l = 1, int r = maxv) {
  if (r < L || R < l) return INF;
  if (L <= l && r <= R) return val[u];
  int mid = (l + r) >> 1;
  pushdown(u);
  return min(get_min(L, R, u << 1, l, mid), get_min(L, R, u << 1 | 1, mid + 1, r));
}
int t_suffix_min;
void assign_suffix_min_(int L, int R, int u = 1, int l = 1, int r = maxv) {
  if (L > R) return;
  if (L <= l && r <= R)
    return node_assign_min(u, t_suffix_min), node_assign_sufmin(u),
           t_suffix_min = min(t_suffix_min, val[u]), void();
  int mid = (l + r) >> 1;
  pushdown(u);
  if (mid < R) assign_suffix_min_(L, R, u << 1 | 1, mid + 1, r);
  if (L <= mid) assign_suffix_min_(L, R, u << 1, l, mid);
  pushup(u);
}
void assign_suffix_min(int L, int R) {
  t_suffix_min = get_min(R + 1, maxv);
  assign_suffix_min_(L, R);
}

int main() {
  scanf("%d%lld", &n, &F);
  FOR(i, 1, n) FOR(j, 1, 3) scanf("%lld", &a[i][j]);
  //计算区间，离散化
  FOR(i, 1, n) {
    LL s = a[i][1] + a[i][2] + a[i][3];
    range[i][0] = mk(s, s);
    LL l1 = min3(s - a[i][1], s - a[i][2], s - a[i][3]),
       r1 = max3(s - a[i][1], s - a[i][2], s - a[i][3]);
    range[i][1] = mk(l1, r1 + F);
    range[i][2] = mk(s - r1, s - l1 + F * 2);
    range[i][3] = mk(0, F * 3);
    // FOR(j,0,3)printf("[%lld,%lld]%c",range[i][j].fi,range[i][j].se,"
    // \n"[j==3]);
    FOR(j, 0, 3) dc[++ld] = range[i][j].fi, dc[++ld] = range[i][j].se;
  }
  sort(dc + 1, dc + ld + 1);
  ld = unique(dc + 1, dc + ld + 1) - dc - 1;
  FOR(i, 1, n) FOR(j, 0, 3) {
    range[i][j].fi = lower_bound(dc + 1, dc + ld + 1, range[i][j].fi) - dc;
    range[i][j].se = lower_bound(dc + 1, dc + ld + 1, range[i][j].se) - dc;
  }
  // FOR(i,1,n)FOR(j,0,3)printf("[%lld,%lld]%c",range[i][j].fi,range[i][j].se,"
  // \n"[j==3]);
  maxv = ld;

  build();

  FOR(i, 1, n) {
    // 0次
    assign_suffix_min(range[i][0].fi, range[i][0].se);
    // 1次
    assign_suffix_min(range[i][1].fi, range[i][0].fi - 1);
    assign_suffix_min(range[i][0].se + 1, range[i][1].se);
    // 2次
    assign_suffix_min(range[i][2].fi, range[i][1].fi - 1);
    assign_suffix_min(range[i][1].se + 1, range[i][2].se);
    // 3次
    assign_suffix_min(range[i][3].fi, range[i][2].fi - 1);
    assign_suffix_min(range[i][2].se + 1, range[i][3].se);

    // 1次
    add(range[i][1].fi, range[i][0].fi - 1, 1);
    add(range[i][0].se + 1, range[i][1].se, 1);
    // 2次
    add(range[i][2].fi, range[i][1].fi - 1, 2);
    add(range[i][1].se + 1, range[i][2].se, 2);
    // 3次
    add(range[i][3].fi, range[i][2].fi - 1, 3);
    add(range[i][2].se + 1, range[i][3].se, 3);
  }
  int ans = get_min(1, maxv);
  printf("%d\n", ans);
  return 0;
}
