#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 5005, INF = 0x3f3f3f3f;

int n;
int a[N], y[N];
int b[N], p[N];
int ans[N];

void work() {
  int cur = p[1], x = 0; //所在的仓库,随身的箱子
  int tot = 0;
  FOR(i, 1, n) y[i] = a[i];
  FOR(i, 2, n) {
    int nex = p[i];
    while (cur < nex) {
      x += y[cur], y[cur] = 0;
      if (x < b[cur]) tot += b[cur] - x, x = b[cur];
      x -= b[cur];
      y[cur] += b[cur];
      ++cur;
    }
    while (cur > nex) {
      x += y[cur], y[cur] = 0;
      if (x < b[cur - 1]) tot += b[cur - 1] - x, x = b[cur - 1];
      x -= b[cur - 1];
      y[cur] += b[cur - 1];
      --cur;
    }
    assert(cur == nex);
  }
  ans[p[1]] = min(ans[p[1]], tot);
}
int main() {
  scanf("%d", &n);
  FOR(i, 1, n) scanf("%d", &a[i]);
  FOR(i, 1, n) ans[i] = INF;
  FOR(i, 1, n - 1) scanf("%d", &b[i]);
  FOR(i, 1, n) p[i] = i;
  do { work(); } while (next_permutation(p + 1, p + n + 1));
  FOR(i, 1, n) printf("%d%c", ans[i], " \n"[i == n]);
  return 0;
}
