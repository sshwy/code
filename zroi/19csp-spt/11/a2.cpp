#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
#include <bitset>
const int N = 402;

int n, m;
char a[N][N];

bool colvis[N], vis[N];
void del(int col, char c) { FOR(i, 1, n) if (!vis[i] && a[i][col] == c) vis[i] = 1; }
int count(int col, char c) {
  int res = 0;
  FOR(i, 1, n) if (!vis[i] && a[i][col] == c) res++;
  return res;
}
bool check(int x) {
  FOR(i, 1, m) colvis[i] = 0;
  FOR(i, 1, n) vis[i] = 0;
  FOR(i, 1, m)
  if (!colvis[i])
    if (a[1][i] == 'Y' && a[x][i] == 'Y') del(i, 'N'), colvis[i] = 1;
  //到目前为止rk1和1都是并列第一
  int ny = 0, ny1 = 0;
  FOR(i, 1, m) if (!colvis[i]) {
    if (a[1][i] == 'N' && a[x][i] == 'Y') {
      ny = 1;
      if (count(i, 'Y') == 1) ny1 = 1, colvis[i] = 1;
    }
  }
  vis[x] = ny1;
  if (!ny || !vis[x]) return 0; // rk1比1小，不成立 or 找不到使得1成为rk2
  //接下来只需要保证1是剩下的人里的rk1即可
  FOR(i, 1, m) if (!colvis[i]) if (a[1][i] == 'Y') del(i, 'N'), colvis[i] = 1;
  //现在YY,YN的情况都排除了，如果存在NY的情况则无解
  FOR(i, 1, m)
  if (!colvis[i])
    if (a[1][i] == 'N')
      if (count(i, 'Y') > 0) return 0;
  return 1;
}
void go() {
  scanf("%d%d", &n, &m);
  FOR(i, 1, n) scanf("%s", a[i] + 1);
  FOR(i, 2, n) if (check(i)) return puts("YES"), void();
  puts("NO");
}
int main() {
  int t;
  scanf("%d", &t);
  FOR(i, 1, t) go();
  return 0;
}
