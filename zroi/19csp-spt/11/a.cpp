#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 1005;

int n, m;
char a[N][N];
int p[N];

void go() {
  scanf("%d%d", &n, &m);
  FOR(i, 1, n) scanf("%s", a[i] + 1);
  FOR(i, 1, m) p[i] = i;
  do {
    int t[N];
    FOR(i, 1, n) {
      t[i] = 0;
      FOR(j, 1, m) {
        if (a[i][j] == 'Y') t[i] += 1 << p[j];
      }
    }
    int cnt = 0;
    FOR(i, 2, n) if (t[i] > t[1])++ cnt;
    if (cnt == 1) return puts("YES"), void();
  } while (next_permutation(p + 1, p + m + 1));
  puts("NO");
}
int main() {
  int t;
  scanf("%d", &t);
  FOR(i, 1, t) go();
  return 0;
}
