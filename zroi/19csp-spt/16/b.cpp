#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
int pw(int a, int m, int p) {
  int res = 1;
  while (m) m & 1 ? res = 1ll * res * a % p : 0, a = 1ll * a * a % p, m >>= 1;
  return res;
}
const int N = 22, M = 500, P = 1e9 + 7, NN = 1 << 20;

struct qxx {
  int nex, t;
};
qxx e[M];
int h[N], le = 1;
void add_path(int f, int t) { e[++le] = (qxx){h[f], t}, h[f] = le; }
#define FORe(i, u, v) for (int i = h[u], v; v = e[i].t, i; i = e[i].nex)

int n, m;
int fac[M], fnv[M];

int dep[N], fa[N];
void dfs(int u, int p = 0) {
  fa[u] = p, dep[u] = dep[p] + 1;
  FORe(i, u, v) if (v != p) dfs(v, u);
}
int calc(int u, int v) { //每条树边的标号是它下面的结点的编号减2（0起点，方便状圧
  int s = 0;
  if (u == v) return s;
  if (dep[u] < dep[v]) swap(u, v);
  while (dep[u] > dep[v]) s |= 1 << (u - 2), u = fa[u];
  while (u != v) s |= 1 << (u - 2) | 1 << (v - 2), u = fa[u], v = fa[v];
  return s;
}
int g[NN], f1[NN], f0[NN], lim, U;

void DP() {
  // F0
  f0[0] = 1;
  FOR(S, 0, lim - 1) {
    // printf("f0[%d]=%d\n",S,f0[S]);
    FOR(i, 0, n - 2) {
      if (S >> i & 1) continue;
      int k = g[U - S] - g[U - S - (1 << i)],
          h = m - (n - 1) - g[U - S] + __builtin_popcount(S);
      // h表示的是目前已有的边数，注意h != g[S]+popcount(S)
      int c = 1ll * fac[h + k] * fnv[h] % P;
      f0[S + (1 << i)] = (f0[S + (1 << i)] + 1ll * f0[S] * c % P) % P;
    }
  }
  // F1
  FOR(S, 0, lim - 1) {
    FOR(i, 0, n - 2) {
      if (S >> i & 1) continue;
      int k = g[U - S] - g[U - S - (1 << i)],
          h = m - (n - 1) - g[U - S] + __builtin_popcount(S);
      int c = 1ll * fac[h + k] * fnv[h] % P;
      f1[S + (1 << i)] = (f1[S + (1 << i)] + 1ll * c * f1[S]) % P; //原本的贡献
      f1[S + (1 << i)] =
          (f1[S + (1 << i)] + 1ll * c * (__builtin_popcount(S) + 1) % P * f0[S]) %
          P; //新的元素的贡献
      f1[S + (1 << i)] =
          (f1[S + (1 << i)] + 1ll * f1[S] * k % P * fac[h + k] % P * fnv[h + 1] % P) %
          P; //增量的期望和
    }
  }
}
int main() {
  scanf("%d%d", &n, &m);
  FOR(i, 1, n - 1) {
    int u, v;
    scanf("%d%d", &u, &v);
    add_path(u, v);
    add_path(v, u);
    // printf("tree(%d,%d)\n",u,v);
  }
  dfs(1);
  FOR(i, n, m) {
    int u, v;
    scanf("%d%d", &u, &v);
    // printf("calc(%d,%d)=%d\n",u,v,calc(u,v));
    g[calc(u, v)]++;
  }
  lim = (1 << (n - 1)), U = lim - 1;

  FOR(i, 0, n - 2)
  FOR(j, 0, lim - 1) if (j >> i & 1) g[j] += g[j - (1 << i)]; // fmt

  fac[0] = 1;
  FOR(i, 1, m) fac[i] = 1ll * i * fac[i - 1] % P;
  fnv[m] = pw(fac[m], P - 2, P);
  ROF(i, m, 1) fnv[i - 1] = 1ll * fnv[i] * i % P;

  DP();

  printf("%d\n", f1[U]);
  return 0;
}
