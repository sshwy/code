
#include <cstdio>
#include <stack>

using namespace std;
const int MOD(1000000000 + 7);
const int Max_N(25);
const int Max_M(405);

constexpr int Add(int a, int b) { return a + b >= MOD ? a + b - MOD : a + b; }

constexpr int Sub(int a, int b) { return a - b < 0 ? a - b + MOD : a - b; }

constexpr int Mult(int a, int b) { return a * 1LL * b % MOD; }

void exgcd(int a, int b, int &x, int &y) {
  if (b == 0)
    x = 1, y = 0;
  else
    exgcd(b, a % b, y, x), y -= x * (a / b);
}

inline int inverse(int a) {
  int invx, invy;
  exgcd(a, MOD, invx, invy);
  return (invx % MOD + MOD) % MOD;
}

inline void upd(int &a, int b) { a = Add(a, b); }

int N, M, Head[Max_N], Total, To[Max_N << 1], Next[Max_N << 1], Weight[Max_N << 1],
    Can[1 << 19];

inline void Add_Edge(int s, int t, int w) {
  ++Total, To[Total] = t, Next[Total] = Head[s], Head[s] = Total, Weight[Total] = w;
}

bool done[Max_N];
void dfs(int u, int T, stack<int> S) {
  if (u == T) {
    int All(0);
    while (S.empty() == false) All |= (1 << S.top()), S.pop();
    ++Can[All];
    return;
  }
  done[u] = true;
  for (int i = Head[u], v; i; i = Next[i])
    if (!done[v = To[i]]) S.push(Weight[i]), dfs(v, T, S), S.pop();
  done[u] = false;
}

void FMT(int F[1 << 19]) {
  for (int i = 0; i <= N - 2; ++i)
    for (int S = 0; S < (1 << (N - 1)); ++S)
      if ((S & (1 << i)) == 0) upd(F[S + (1 << i)], F[S]);
}

int F0[1 << 19], F1[1 << 19], Fac[Max_M], Inv[Max_M];

int main() {
  scanf("%d%d", &N, &M);
  Fac[0] = 1;
  for (int i = 1; i <= M; ++i) Fac[i] = Mult(Fac[i - 1], i);
  Inv[M] = inverse(Fac[M]);
  for (int i = M - 1; i >= 0; --i) Inv[i] = Mult(Inv[i + 1], i + 1);
  for (int i = 0, a, b; i <= M - 1; ++i) {
    scanf("%d%d", &a, &b);
    if (i <= N - 2)
      Add_Edge(a, b, i), Add_Edge(b, a, i);
    else
      dfs(a, b, stack<int>());
  }
  FMT(Can);
  for (int S = 0; S <= (1 << (N - 1)) - 1; S++) printf("g[%d]=%d\n", S, Can[S]);
  F0[0] = 1, F1[0] = 0;
  for (int S = 0, have, _S; S < (1 << (N - 1)); ++S) {
    have = M - (N - 1) - Can[(1 << (N - 1)) - 1 - S], _S = 0;
    for (int i = 0; i <= N - 2; ++i)
      have += ((S & (1 << i)) != 0), _S += ((S & (1 << i)) != 0);
    for (int i = 0, cnt, xs; i <= N - 2; ++i)
      if ((S & (1 << i)) == 0) {
        cnt = Can[(1 << (N - 1)) - 1 - S] - Can[(1 << (N - 1)) - 1 - (S + (1 << i))];
        xs = Mult(Fac[have + cnt], Inv[have]);
        upd(F0[S + (1 << i)], Mult(F0[S], xs));
        upd(F1[S + (1 << i)], Mult(_S + 1, Mult(F0[S], xs)));
        upd(F1[S + (1 << i)], Mult(F1[S], xs));
        if (cnt)
          upd(F1[S + (1 << i)],
              Mult(cnt, Mult(F1[S], Mult(Fac[have + cnt], Inv[have + 1]))));
      }
    printf("F0[%d]=%d\n", S, F0[S]);
  }
  printf("%d", F1[(1 << (N - 1)) - 1]);
  return 0;
}
