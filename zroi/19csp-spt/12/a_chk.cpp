#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 3e5 + 5;

int b[N];
int c[N];
int a[N];
int n, m, k;
char msg[20];

int main(int argc, char **argv) {
  FILE *fin = fopen(argv[1], "r");
  FILE *fout = fopen(argv[2], "r");
  fscanf(fin, "%d%d%d", &n, &m, &k);
  FOR(i, 1, k) fscanf(fin, "%d", &b[i]);
  fscanf(fout, "%s", msg);
  if (msg[0] == 'N') {
    puts("No solution.");
    return 1;
  } else {
    FOR(i, 1, n) fscanf(fout, "%d", &a[i]);
    memset(c, 0x3f, sizeof(c));
    FOR(i, 1, n) * upper_bound(c + 1, c + n + 1, a[i]) = a[i];
    FOR(i, 1, k) if (c[i] != b[i]) {
      puts("Incorrect!");
      FOR(i, 1, k) printf("%d%c", b[i], " \n"[i == k]);
      FOR(i, 1, k) printf("%d%c", c[i], " \n"[i == k]);
      return 1;
    }
  }
  return 0;
}
