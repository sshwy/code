#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 502;
using namespace RA;

int n, m;
bool vis[N][N];

int main() {
  // freopen("b.in","w",stdout);
  srand(clock());
  n = r(2, 100);
  int lim = n * (n - 1) / 2, p = 10000, rate = p / 10;
  FOR(i, 1, n) FOR(j, i + 1, n) {
    if (m == lim - 1) break;
    vis[i][j] = vis[j][i] = r(p) < rate;
    m += vis[i][j];
  }
  printf("%d %d\n", n, m);
  FOR(i, 1, n) FOR(j, i + 1, n) if (vis[i][j]) printf("%d %d\n", i, j);
  return 0;
}
