#include <algorithm>
#include <cstdio>
#include <vector>
constexpr double eps = 1e-9;
using range = std::pair<double, double>;
struct point {
  double x, y;
} arr[50005];
struct wall {
  double x1, x2, y;
} seg[50005];
std::vector<wall> vec[35];
std::vector<double> left[35], right[35];
range make_proj(point p, wall w) {
  return {
      p.x - (p.x - w.x1) / (p.y - w.y) * p.y, p.x - (p.x - w.x2) / (p.y - w.y) * p.y};
}
range make_proj(point p, std::vector<wall> walls) {
  range res = {-1e30, 1e30};
  for (auto &it : walls) {
    auto sing = make_proj(p, it);
    res.first = std::max(res.first, sing.first);
    res.second = std::min(res.second, sing.second);
  }
  return res;
}
int calc(point p, int mask) {
  auto cut = make_proj(p, vec[mask]);
  if (cut.first > cut.second + eps) return 0;
  int r = std::upper_bound(left[mask].begin(), left[mask].end(), cut.first + eps) -
          left[mask].begin();
  int l = std::lower_bound(right[mask].begin(), right[mask].end(), cut.second - eps) -
          right[mask].begin();
  return r - l;
}
int main() {
  int n, m, q;
  scanf("%d%d%d", &n, &m, &q);
  for (int i = 0; i < n; i++) scanf("%lf%lf", &arr[i].x, &arr[i].y);
  for (int i = 0; i < m; i++) scanf("%lf%lf%lf", &seg[i].x1, &seg[i].x2, &seg[i].y);
  for (int mask = 1; mask < 1 << m; mask++) {
    for (int i = 0; i < m; i++) {
      if (mask & (1 << i)) vec[mask].push_back(seg[i]);
    }
    for (int i = 0; i < n; i++) {
      bool ok = true;
      for (auto &it : vec[mask]) ok &= arr[i].y > it.y + eps;
      if (!ok) continue;
      auto cut = make_proj(arr[i], vec[mask]);
      if (cut.first <= cut.second + eps) {
        left[mask].push_back(cut.first);
        right[mask].push_back(cut.second);
      }
    }
    std::sort(left[mask].begin(), left[mask].end());
    std::sort(right[mask].begin(), right[mask].end());
  }
  while (q--) {
    point p;
    scanf("%lf%lf", &p.x, &p.y);
    int ans = n;
    for (int mask = 1; mask < 1 << m; mask++)
      ans += calc(p, mask) * (vec[mask].size() & 1 ? -1 : 1);
    printf("%d\n", ans);
  }
  return 0;
}
