#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 502, INF = 0x3f3f3f3f;

int n, m;
int dg[N], d[N];
bool vis[N][N], v[N][N];

bool check(int k) {
  FOR(i, 1, n) d[i] = dg[i];
  FOR(i, 1, n) FOR(j, 1, n) v[i][j] = vis[i][j];
  int fl;
  do {
    fl = 0;
    FOR(i, 1, n) FOR(j, 1, n) if (!v[i][j] && d[i] + d[j] >= k) {
      fl = 1;
      ++d[i], ++d[j], v[i][j] = v[j][i] = 1;
    }
  } while (fl);
  FOR(i, 1, n) FOR(j, 1, n) if (!v[i][j]) return 0;
  return 1;
}
int main() {
  scanf("%d%d", &n, &m);
  FOR(i, 1, m) {
    int u, v;
    scanf("%d%d", &u, &v);
    dg[u]++, dg[v]++;
    vis[u][v] = vis[v][u] = 1;
  }
  FOR(i, 1, n) vis[i][i] = 1;
  int l = 0, r = n + 1, mid;
  while (l < r) mid = (l + r + 1) >> 1, check(mid) ? l = mid : r = mid - 1;
  while (check(l + 1)) ++l;
  printf("%d\n", l);
  return 0;
}
