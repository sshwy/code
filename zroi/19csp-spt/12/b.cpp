#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 502, INF = 0x3f3f3f3f;

int n, m;
int dg[N];
bool vis[N][N];

struct dat {
  int u, v, dudv;
  dat(int _u, int _v, int _du_dv) {
    u = _u, v = _v, dudv = _du_dv;
    if (u > v) swap(u, v);
  }
  bool operator<(dat d) const {
    return dudv != d.dudv ? dudv < d.dudv : u != d.u ? u < d.u : v < d.v;
  }
  bool operator==(dat d) const {
    return dudv != d.dudv ? 0 : u != d.u ? 0 : v == d.v;
  }
};

priority_queue<dat> q, del;

int main() {
  scanf("%d%d", &n, &m);
  FOR(i, 1, m) {
    int u, v;
    scanf("%d%d", &u, &v);
    dg[u]++, dg[v]++;
    vis[u][v] = vis[v][u] = 1;
  }
  FOR(i, 1, n) vis[i][i] = 1;
  FOR(i, 1, n) FOR(j, i + 1, n) if (!vis[i][j]) {
    // printf("add(%d,%d,%d,%d)\n",i,j,dg[i],dg[j]);
    q.push(dat(i, j, dg[i] + dg[j]));
  }
  int k = INF;
  while (!q.empty()) {
    dat cur = q.top();
    q.pop();
    k = min(k, cur.dudv);
    int u = cur.u, v = cur.v;
    vis[u][v] = vis[v][u] = 1;
    FOR(i, 1, n) if (!vis[u][i]) {
      // printf("del(%d,%d,%d,%d)\n",u,i,dg[u],dg[i]);
      del.push(dat(u, i, dg[u] + dg[i]));
    }
    FOR(i, 1, n) if (!vis[v][i]) {
      // printf("del(%d,%d,%d,%d)\n",v,i,dg[v],dg[i]);
      del.push(dat(v, i, dg[v] + dg[i]));
    }
    dg[u]++, dg[v]++;
    FOR(i, 1, n) if (!vis[u][i]) {
      // printf("add(%d,%d,%d,%d)\n",u,i,dg[u],dg[i]);
      q.push(dat(u, i, dg[u] + dg[i]));
    }
    FOR(i, 1, n) if (!vis[v][i]) {
      // printf("add(%d,%d,%d,%d)\n",v,i,dg[v],dg[i]);
      q.push(dat(v, i, dg[v] + dg[i]));
    }
    while (!q.empty() && !del.empty() && q.top() == del.top()) q.pop(), del.pop();
  }
  printf("%d\n", k);
  return 0;
}
