#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 3e5 + 5;

int n, m, k;
int b[N];
int ans[N], la;

int main() {
  scanf("%d%d%d", &n, &m, &k);
  FOR(i, 1, k) scanf("%d", &b[i]);
  FOR(i, 1, k - 1) if (b[i] > b[i + 1]) return puts("No"), 0;
  int rest = n - k, pos = 0;
  while (rest && pos < k) {
    ++pos; //在pos-1和pos之间的位置插入
    int x = m;
    while (x > b[pos] && rest) { --rest, ans[++la] = x, --x; }
    ans[++la] = b[pos];
  }
  if (rest) return puts("No"), 0;
  while (pos < k) ans[++la] = b[++pos];
  assert(la == n);
  puts("Yes");
  FOR(i, 1, n) printf("%d%c", ans[i], " \n"[i == n]);
  return 0;
}
