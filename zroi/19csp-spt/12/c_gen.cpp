#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/

using namespace RA;
int n, m, q;
int LIM = 1000000;
int main() {
  srand(clock());
  n = r(1, 40000), m = 1, q = r(10000, 30000);
  printf("%d %d %d\n", n, m, q);
  // FOR(i,1,n)printf("%d %d\n",r(-LIM,LIM),r(LIM+1,LIM*2));
  FOR(i, 1, n) printf("%d %d\n", 0, r(LIM + 1, LIM * 2));
  FOR(i, 1, m) {
    int li = r(-LIM, LIM - 1), ri = r(li + 1, LIM), pi = r(1, LIM);
    printf("%d %d %d\n", li, ri, pi);
  }
  FOR(i, 1, q) printf("%d %d\n", r(-LIM, LIM), r(-LIM, -1));
  return 0;
}
