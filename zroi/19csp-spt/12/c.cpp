#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
typedef long long LL;
const LL INF = 0x3f3f3f3f;
struct frac {
  LL p, q;
  frac() { p = 0, q = 1; }
  frac(LL x) { p = x, q = 1; }
  frac(LL _p, LL _q) { p = _p, q = _q; }
  LL gcd(LL a, LL b) { return b ? gcd(b, a % b) : a; }
  frac zip() {
    if (p == INF || q == INF) return *this;
    LL g = gcd(p, q);
    p /= g, q /= g;
    if (q < 0) q = -q, p = -p;
    return *this;
  }
  frac operator+(frac x) { return frac(p * x.q + q * x.p, q * x.q).zip(); }
  frac operator-() { return frac(-p, q); }
  frac operator-(frac x) { return *this + (-x); }
  frac operator*(frac x) { return frac(p * x.p, q * x.q).zip(); }
  frac operator/(frac x) { return frac(p * x.q, q * x.p).zip(); }
  friend frac operator/(int x, frac y) { return frac(x * y.q, y.p).zip(); }
  frac operator/(int x) { return frac(p, q * x).zip(); }
  bool operator==(frac x) { return (p == x.p && q == x.q) || (p * x.q == q * x.p); }
  bool operator<(const frac &x) const { return p * x.q < q * x.p; }
  bool operator>(const frac &x) const { return p * x.q > q * x.p; }
  bool operator<=(const frac &x) const { return p * x.q <= q * x.p; }
  bool operator>=(const frac &x) const { return p * x.q >= q * x.p; }
};
#define double frac
typedef pair<frac, frac> Range;
const int N = 1e5 + 5, SZ = 2e5 + 5;

int n, m, q;
int l[10], r[10], p[10];
int ans[N], cnt[N];

pii a[N], b[N];

Range range(pii x, int c) {
  frac lk, lx, rk, rx;
  if (x.fi == l[c])
    lx = x.fi;
  else if (x.se == p[c])
    return mk(frac(1, 1), frac(0, 1)); //空集
  else
    lk = frac(x.se - p[c], x.fi - l[c]), lx = -x.se / lk + x.fi;
  if (x.fi == r[c])
    rx = x.fi;
  else if (x.se == p[c])
    return mk(frac(1, 1), frac(0, 1)); //空集
  else
    rk = frac(x.se - p[c], x.fi - r[c]), rx = -(x.se / rk) + x.fi;
  return mk(lx, rx);
}
Range inter(Range r1, Range r2) { return mk(max(r1.fi, r2.fi), min(r1.se, r2.se)); }
bool empty(Range rg) { return rg.fi > rg.se; }
vector<pair<Range, int>> attacker;
vector<Range> target;

double dc[N * 2];
int ld;

struct fenwick {
  int c[SZ], lim;
  void add(int pos, int v) {
    for (int i = pos; i <= lim; i += i & -i) c[i] += v;
  }
  int pre(int pos = -1) {
    if (pos == -1) pos = lim;
    int res = 0;
    for (int i = pos; i > 0; i -= i & -i) res += c[i];
    return res;
  }
  void clear(int li) {
    FOR(i, 0, li) c[i] = 0;
    lim = li;
  }
} BIT;
void work() {
  FOR(i, 1, q) cnt[i] = 0;

  sort(target.begin(), target.end());
  sort(attacker.begin(), attacker.end());

  ld = 0;
  for (Range x : target) dc[++ld] = x.se;
  for (pair<Range, int> x : attacker) dc[++ld] = x.fi.se;
  BIT.clear(ld);
  sort(dc + 1, dc + ld + 1);
  ld = unique(dc + 1, dc + ld + 1) - dc - 1;
  int lt = target.size() - 1, pos = 0;
  for (pair<Range, int> x : attacker) {
    while (pos <= lt && target[pos].fi <= x.fi.fi) {
      BIT.add(lower_bound(dc + 1, dc + ld + 1, target[pos].se) - dc, 1);
      ++pos;
    }
    int c = BIT.pre() - BIT.pre(lower_bound(dc + 1, dc + ld + 1, x.fi.se) - dc - 1);
    cnt[x.se] = c;
  }
}
Range ra[N][6], rb[N][6];
int main() {
  scanf("%d%d%d", &n, &m, &q);
  FOR(i, 1, n) scanf("%d%d", &a[i].fi, &a[i].se);
  FOR(i, 1, m) scanf("%d%d%d", &l[i], &r[i], &p[i]);
  FOR(i, 1, q) scanf("%d%d", &b[i].fi, &b[i].se);
  FOR(i, 1, q) ans[i] = n;
  int lim = 1 << m;
  FOR(i, 1, n) FOR(j, 1, m) ra[i][j] = range(a[i], j);
  FOR(i, 1, q) FOR(j, 1, m) rb[i][j] = range(b[i], j);
  FOR(s, 1, lim - 1) {
    target.clear();
    FOR(i, 1, n) { //第i个目标的区间
      Range rg = mk(frac(-INF, 1ll), frac(INF, 1ll));
      FOR(j, 1, m) if (s >> (j - 1) & 1) rg = inter(rg, ra[i][j]);
      if (empty(rg)) continue;
      target.pb(rg);
    }
    attacker.clear();
    FOR(i, 1, q) {
      Range rg = mk(frac(-INF, 1ll), frac(INF, 1ll));
      FOR(j, 1, m) if (s >> (j - 1) & 1) rg = inter(rg, rb[i][j]);
      if (empty(rg)) continue;
      attacker.pb(mk(rg, i));
    }
    work();
    int sig = 1 - 2 * (__builtin_popcount(s) & 1);
    for (pair<Range, int> x : attacker) { ans[x.se] += cnt[x.se] * sig; }
  }
  FOR(i, 1, q) printf("%d\n", ans[i]);
  return 0;
}
