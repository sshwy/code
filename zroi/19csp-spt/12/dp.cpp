#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/

int main() {
  system("g++ c.cpp -o .usr");
  system("g++ c2.cpp -o .std");
  system("g++ c_gen.cpp -o .gen");
  FOR(i, 1, 10000) {
    if (system("./.gen > .fin;./.usr < .fin > .fout;./.std < .fin > .fstd")) break;
    if (system("diff -w .fout .fstd")) break;
    printf("AC #%d\n", i);
  }
  puts("finished.");
  return 0;
}
