#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/

int main() {
  system("g++ a.cpp -o .usr");
  system("g++ a_gen.cpp -o .gen");
  system("g++ a_chk.cpp -o .chk");
  FOR(i, 1, 10000) {
    system("./.gen > .fin;./.usr < .fin > .fout");
    if (system("./.chk .fin .fout")) break;
    printf("\033[2JAC #%d\n", i);
  }
  puts("finished.");
  return 0;
}
