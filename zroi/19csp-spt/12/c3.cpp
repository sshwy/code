#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
typedef pair<double, double> Range;
const int N = 1e5 + 5, SZ = 2e5 + 5;

int n, m, q;
int l[10], r[10], p[10];
int ans[N], cnt[N];

pii a[N], b[N];

Range range(pii x, int c) {
  double lk, lx, rk, rx;
  if (x.fi == l[c])
    lx = x.fi;
  else if (x.se == p[c])
    return mk(1, 0); //空集
  else {
    lk = (x.se - p[c]) * 1.0 / (x.fi - l[c]);
    lx = -x.se / lk + x.fi;
  }
  if (x.fi == r[c])
    rx = x.fi;
  else if (x.se == p[c])
    return mk(1, 0);
  else {
    rk = (x.se - p[c]) * 1.0 / (x.fi - r[c]);
    printf("rk=%.2lf\n", rk);
    rx = -x.se / rk + x.fi;
  }
  printf("x=(%d,%d),pc=%d,lc=%d,rc=%d\n", x.fi, x.se, p[c], l[c], r[c]);
  printf("range=[%.2lf,%.2lf]\n", lx, rx);
  return mk(lx, rx);
}
Range inter(Range r1, Range r2) { return mk(max(r1.fi, r2.fi), min(r1.se, r2.se)); }
bool empty(Range rg) { return rg.fi > rg.se; }
const double eps = 1e-5;
bool db_equal(const double &x, const double &y) { return abs(x - y) < eps; }
bool db_lesser(const double &x, const double &y) {
  if (abs(x - y) < eps) return 0; // equal
  return x < y;
}
bool db_lesser_or_equal(const double &x, const double &y) {
  return db_equal(x, y) || db_lesser(x, y);
}
vector<pair<Range, int>> attacker;
vector<Range> target;

double dc[N * 2];
int ld;

struct fenwick {
  int c[SZ], lim;
  void add(int pos, int v) {
    // printf("\033[34madd(%d,%d)\033[0m\n",pos,v);
    for (int i = pos; i <= lim; i += i & -i) c[i] += v;
  }
  int pre(int pos = -1) {
    if (pos == -1) pos = lim;
    // printf("\033[34mpre(%d)\033[0m\n",pos);
    int res = 0;
    for (int i = pos; i > 0; i -= i & -i) res += c[i];
    // printf("res=%d\n",res);
    return res;
  }
  void clear(int li) {
    memset(c, 0, sizeof(c));
    lim = li;
  }
} BIT;
void work() {
  // printf("work()\n");
  FOR(i, 1, q) cnt[i] = 0;

  sort(target.begin(), target.end());
  sort(attacker.begin(), attacker.end());

  // printf("target:");
  // for(Range x:target)printf("[%.2lf,%.2lf] ",x.fi,x.se);
  // puts("");
  // printf("attacker:");
  // for(pair<Range,int> x:attacker)printf("[%.2lf,%.2lf] ",x.fi.fi,x.fi.se);
  // puts("");

  ld = 0;
  for (Range x : target) dc[++ld] = x.se;
  for (pair<Range, int> x : attacker) dc[++ld] = x.fi.se;
  BIT.clear(ld);
  sort(dc + 1, dc + ld + 1);
  ld = unique(dc + 1, dc + ld + 1, db_equal) - dc - 1;
  int lt = target.size() - 1, pos = 0;
  for (pair<Range, int> x : attacker) {
    while (pos <= lt && db_lesser_or_equal(target[pos].fi, x.fi.fi)) {
      // printf("add %.2lf %.2lf\n",target[pos].fi,target[pos].se);
      BIT.add(lower_bound(dc + 1, dc + ld + 1, target[pos].se, db_lesser) - dc, 1);
      ++pos;
    }
    int c = BIT.pre() -
            BIT.pre(lower_bound(dc + 1, dc + ld + 1, x.fi.se, db_lesser) - dc - 1);
    cnt[x.se] = c;
  }
  // printf("\033[33mcnt:");
  // FOR(i,1,q)printf("%d ",cnt[i]);
  // printf("\033[0m\n");
}
int main() {
  scanf("%d%d%d", &n, &m, &q);
  FOR(i, 1, n) scanf("%d%d", &a[i].fi, &a[i].se);
  FOR(i, 1, m) scanf("%d%d%d", &l[i], &r[i], &p[i]);
  FOR(i, 1, q) scanf("%d%d", &b[i].fi, &b[i].se);
  FOR(i, 1, q) ans[i] = n;
  int lim = 1 << m;
  FOR(s, 1, lim - 1) {
    // printf("\033[31ms(status)=%d\033[0m\n",s);
    target.clear();
    FOR(i, 1, n) { //第i个目标的区间
      // printf("calc %d\n",i);
      Range rg = mk(-1e20, 1e20);
      FOR(j, 1, m) if (s >> (j - 1) & 1) rg = inter(rg, range(a[i], j));
      if (empty(rg)) continue;
      // printf("\033[32mrg=(%.2lf,%.2lf)\033[0m\n",rg.fi,rg.se);
      target.pb(rg);
    }
    attacker.clear();
    FOR(i, 1, q) {
      Range rg = mk(-1e20, 1e20);
      FOR(j, 1, m) if (s >> (j - 1) & 1) rg = inter(rg, range(b[i], j));
      if (empty(rg)) continue;
      attacker.pb(mk(rg, i));
    }
    work(); //把答案统计到cnt数组中
    int sig = 1 - 2 * (__builtin_popcount(s) & 1);
    for (pair<Range, int> x : attacker) { ans[x.se] += cnt[x.se] * sig; }
  }
  FOR(i, 1, q) printf("%d\n", ans[i]);
  return 0;
}
