#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 502, INF = 0x3f3f3f3f;

int n, m;
int dg[N];
bool vis[N][N];

struct dat {
  int u, v, dudv;
  dat(int _u, int _v, int _du_dv) {
    u = _u, v = _v, dudv = _du_dv;
    if (u > v) swap(u, v);
  }
  bool operator<(dat d) const {
    return dudv != d.dudv ? dudv < d.dudv : u != d.u ? u < d.u : v < d.v;
  }
  bool operator==(dat d) const {
    return dudv != d.dudv ? 0 : u != d.u ? 0 : v == d.v;
  }
};

set<dat> s;

struct dsu {
  int f[N];
  void init(int lim) { FOR(i, 0, lim) f[i] = i; }
  int get(int u) { return f[u] == u ? u : f[u] = get(f[u]); }
  void merge(int u, int v) { f[get(u)] = get(v); }
  bool find(int u, int v) { return get(u) == get(v); }
} g[N];

int main() {
  freopen("b4.in", "r", stdin);
  scanf("%d%d", &n, &m);
  FOR(i, 1, n) g[i].init(n + 1);
  FOR(i, 1, m) {
    int u, v;
    scanf("%d%d", &u, &v);
    dg[u]++, dg[v]++;
    vis[u][v] = vis[v][u] = 1;
    g[u].merge(v, v + 1);
    g[v].merge(u, u + 1);
  }
  FOR(i, 1, n) vis[i][i] = 1, g[i].merge(i, i + 1);
  FOR(i, 1, n) FOR(j, i + 1, n) if (!vis[i][j]) {
    s.insert(dat(i, j, dg[i] + dg[j]));
  }
  int k = INF;
  while (!s.empty()) {
    set<dat>::iterator it = s.end();
    --it;
    dat cur = *it;
    s.erase(it);
    k = min(k, cur.dudv);
    int u = cur.u, v = cur.v;
    vis[u][v] = vis[v][u] = 1;
    g[u].merge(v, v + 1);
    g[v].merge(u, u + 1);
    for (int i = g[u].get(1); i != n + 1; i = g[u].get(i + 1)) {
      if (dg[u] + dg[i] < k) {
        s.erase(dat(u, i, dg[u] + dg[i]));
        s.insert(dat(u, i, dg[u] + dg[i] + 1));
      }
    }
    for (int i = g[v].get(1); i != n + 1; i = g[v].get(i + 1)) {
      if (dg[v] + dg[i] < k) {
        s.erase(dat(v, i, dg[v] + dg[i]));
        s.insert(dat(v, i, dg[v] + dg[i] + 1));
      }
    }
    dg[u]++, dg[v]++;
  }
  printf("%d\n", k);
  return 0;
}
