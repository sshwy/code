#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 502, INF = 0x3f3f3f3f, SZ = 2e6 + 5;

int n, m;
int dg[N];
bool vis[N][N];

struct dat {
  int u, v, dudv;
  dat() {}
  dat(int _u, int _v, int _du_dv) {
    u = _u, v = _v, dudv = _du_dv;
    if (u > v) swap(u, v);
  }
  bool operator<(dat d) const {
    return dudv != d.dudv ? dudv < d.dudv : u != d.u ? u < d.u : v < d.v;
  }
  bool operator==(dat d) const { return dudv == d.dudv && u == d.u && v == d.v; }
  bool operator<=(dat d) const { return *this < d || *this == d; }
};

struct Treap {
  unsigned int seed = 1, rnd[SZ];
  int root, tot;
  int ch[SZ][2];
  int sz[SZ]; // sz:子树大小
  dat val[SZ];

  Treap() { root = tot = 0; }
  int rrand() { return seed = seed * 482711; }
  void pushup(int u) { sz[u] = sz[ch[u][0]] + sz[ch[u][1]] + 1; }
  void split(int u, dat key, int &x, int &y) {
    if (!u)
      x = y = 0;
    else {
      if (val[u] <= key)
        x = u, split(ch[u][1], key, ch[u][1], y);
      else
        y = u, split(ch[u][0], key, x, ch[u][0]);
      pushup(u);
    }
  }
  int merge(int x, int y) {     // x<y
    if (!x || !y) return x + y; //返回x，y或0
    if (rnd[x] < rnd[y])
      return ch[x][1] = merge(ch[x][1], y), pushup(x), x;
    else
      return ch[y][0] = merge(x, ch[y][0]), pushup(y), y;
  }
  void insert(dat v) { //插入v
    int x, y, u = ++tot;
    ch[tot][0] = ch[tot][1] = sz[tot] = 0;
    val[u] = v, sz[u] = 1, rnd[u] = rrand();
    split(root, v, x, y);
    root = merge(merge(x, u), y);
  }
  void del(dat v) {
    int x, y, z;
    split(root, dat(v.u, v.v - 1, v.dudv), x, y); //所有的v就被分在y中
    split(y, v, y, z);                            //所有的v仍被分在y中
    if (sz[y] != 1) {
      printf("sz[y]=%d\n", sz[y]);
      printf("(%d,%d,%d)\n", val[y].u, val[y].v, val[y].dudv);
      printf("(%d,%d,%d)\n", val[ch[y][0]].u, val[ch[y][0]].v, val[ch[y][0]].dudv);
      printf("(%d,%d,%d)\n", val[ch[y][1]].u, val[ch[y][1]].v, val[ch[y][1]].dudv);
    }
    assert(sz[y] == 1);
    if (!y) return;                //不存在v这个权值
    y = merge(ch[y][0], ch[y][1]); //根节点的不要了
    root = merge(x, merge(y, z));
  }
  int rank(dat v) { //即相同的数中，第一个数的排名
    int x, y, res;
    split(root, dat(v.u, v.v - 1, v.dudv), x, y);
    res = sz[x] + 1, root = merge(x, y);
    return res;
  }
  dat kth(int k) { //查询排名为k的数
    int u = root;
    while (k != sz[ch[u][0]] + 1) {
      if (k <= sz[ch[u][0]])
        u = ch[u][0];
      else
        k -= sz[ch[u][0]] + 1, u = ch[u][1];
    }
    return val[u];
  }
  int size() { return sz[root]; }
  bool empty() { return sz[root] == 0; }
} s;

int main() {
  scanf("%d%d", &n, &m);
  FOR(i, 1, m) {
    int u, v;
    scanf("%d%d", &u, &v);
    dg[u]++, dg[v]++;
    vis[u][v] = vis[v][u] = 1;
  }
  FOR(i, 1, n) vis[i][i] = 1;
  FOR(i, 1, n) FOR(j, i + 1, n) if (!vis[i][j]) {
    s.insert(dat(i, j, dg[i] + dg[j]));
  }
  int k = INF;
  while (!s.empty()) {
    dat cur = s.kth(s.size());
    s.del(cur);
    k = min(k, cur.dudv);
    int u = cur.u, v = cur.v;
    vis[u][v] = vis[v][u] = 1;
    FOR(i, 1, n) if (!vis[u][i] && dg[u] + dg[i] < k) {
      s.del(dat(u, i, dg[u] + dg[i]));
      s.insert(dat(u, i, dg[u] + dg[i] + 1));
    }
    FOR(i, 1, n) if (!vis[v][i] && dg[v] + dg[i] < k) {
      s.del(dat(v, i, dg[v] + dg[i]));
      s.insert(dat(v, i, dg[v] + dg[i] + 1));
    }
    dg[u]++, dg[v]++;
  }
  printf("%d\n", k);
  return 0;
}
