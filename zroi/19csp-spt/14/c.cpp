#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 2e5 + 5, SZ = 1e6 + 5, INF = 0x7fffffff;

int n;
int p[N], w[N];

int f[N];
bool vis[N];

pii operator+(pii x, pii y) { return mk(max(x.fi, y.fi), min(x.se, y.se)); }

namespace SEG {
  pii origin[SZ], prefix[SZ]; // origin:原值；prefix：前缀的转移值
  void build(int u = 1, int l = 1, int r = n) {
    origin[u] = prefix[u] = mk(0, INF);
    if (l == r) { return; }
    int mid = (l + r) >> 1;
    build(u << 1, l, mid), build(u << 1 | 1, mid + 1, r);
  }
  pii calc(int u, int l, int r, pii x) { //计算出能更新x的pair
    if (l == r) return origin[u].fi > x.fi ? origin[u] : mk(0, INF);
    int mid = (l + r) >> 1;
    if (origin[u << 1 | 1].fi < x.fi) return calc(u << 1, l, mid, x);
    return calc(u << 1 | 1, mid + 1, r, x) + prefix[u];
  }
  void pushup(int u, int l, int r) { //合并u<<1,u<<1|1的信息得到u
    int mid = (l + r) >> 1;
    prefix[u] = calc(u << 1, l, mid, origin[u << 1 | 1]);
    origin[u] = prefix[u] + origin[u << 1 | 1];
  }
  void trans(int L, int R, pii &x, int u = 1, int l = 1,
      int r = n) { //用L,R的信息更新x
    if (r < L || R < l) return;
    if (L <= l && r <= R) { return x = x + calc(u, l, r, x), void(); }
    int mid = (l + r) >> 1;
    if (mid < R) trans(L, R, x, u << 1 | 1, mid + 1, r);
    if (L <= mid) trans(L, R, x, u << 1, l, mid);
  }
  void modify(int pos, pii x, int u = 1, int l = 1, int r = n) {
    if (l == r) {
      origin[u] = prefix[u] = x;
      return;
    }
    int mid = (l + r) >> 1;
    if (mid < pos)
      modify(pos, x, u << 1 | 1, mid + 1, r);
    else
      modify(pos, x, u << 1, l, mid);
    pushup(u, l, r);
  }
} // namespace SEG
int query(int pos) { //查询前缀
  pii x = mk(0, INF);
  SEG::trans(1, pos, x); //用区间[1-pos]的信息来更新x
  return x.se;
}
void modify(int pos, pii x) { //在位置pos上添加一个x
  SEG::modify(pos, x);
}

int main() {
  scanf("%d", &n);
  SEG::build();
  FOR(i, 1, n) scanf("%d", &p[i]);
  FOR(i, 1, n) scanf("%d", &w[i]);
  FOR(i, 1, n) { // x axis up
    f[i] = query(p[i]);
    if (f[i] == INF) f[i] = 0;
    f[i] += w[i];
    modify(p[i], mk(i, f[i]));
  }
  int ans = query(n);
  printf("%d\n", ans);
  return 0;
}
