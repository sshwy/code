#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 2e5 + 5, INF = 0x3f3f3f3f;

int n;
int p[N], w[N];

int f[N];
bool vis[N];

int main() {
  scanf("%d", &n);
  FOR(i, 1, n) scanf("%d", &p[i]);
  FOR(i, 1, n) scanf("%d", &w[i]);
  FOR(i, 1, n) { // x axis up
    f[i] = INF;
    int las = 0;
    ROF(j, i - 1, 1) {
      if (p[j] < p[i] && p[j] > las) {
        vis[j] = 1;
        las = p[j];
        if (f[i] > f[j]) f[i] = min(f[i], f[j]);
      }
    }
    if (f[i] == INF) f[i] = 0;
    f[i] += w[i];
  }
  int ans = INF;
  FOR(i, 1, n) if (!vis[i]) ans = min(ans, f[i]);
  printf("%d\n", ans);
  return 0;
}
