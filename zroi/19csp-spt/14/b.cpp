#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 5002;

int n, k;

struct point {
  long double x, y;
  point() {}
  point(long double _x, long double _y) { x = _x, y = _y; }
  long double det(point v) { return x * v.y - y * v.x; }
};
typedef point vec;
vec a[N];

long double c_k2[N];             // x^{__k-2__}(k-1)k/n^{__k__}
long double calc(int l, int r) { //计算l det r的期望
  if (l == r) return 0;          // incorrect
  int cnt = r - l + 1;
  if (cnt <= 0) cnt += n;
  cnt = n - cnt;
  if (cnt < k - 2) return 0;
  // printf("calc(%d,%d)\n",l,r);
  return c_k2[cnt] * a[l].det(a[r]);
}
void init() {
  c_k2[k - 2] = 1;
  FOR(i, 1, k - 2) c_k2[k - 2] *= i, c_k2[k - 2] /= (n - i), assert(n - i);
  c_k2[k - 2] /= (n - 0), assert(n);
  c_k2[k - 2] /= (n - k + 1), assert(n - k + 1);
  c_k2[k - 2] *= k;
  c_k2[k - 2] *= (k - 1);
  FOR(i, k - 2 + 1, n)
  c_k2[i] = c_k2[i - 1] / (i - (k - 2)) * i, assert(i - (k - 2));
}
int main() {
  scanf("%d%d", &n, &k);
  init();
  FOR(i, 1, n) scanf("%Lf%Lf", &a[i].x, &a[i].y);
  reverse(a + 1, a + n + 1); // counterclockwise
  long double s = 0;
  FOR(i, 1, n) FOR(j, 1, n) s += calc(i, j);
  s /= 2;
  printf("%.9Lf\n", s);
  return 0;
}
