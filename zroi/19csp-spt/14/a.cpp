#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 1e5 + 5, M = N * 2;

bool is_leaf[N];
int n;
int main() {
  scanf("%d", &n);
  FOR(i, 1, n) is_leaf[i] = 1;
  FOR(i, 2, n) {
    int x;
    scanf("%d", &x);
    is_leaf[x] = 0;
  }
  int cnt = 0;
  FOR(i, 1, n) cnt += is_leaf[i];
  puts(n - cnt < cnt ? "Y" : "D");
  return 0;
}
