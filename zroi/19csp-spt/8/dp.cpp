#include <cstdio>
#include <cstdlib>
#include <ctime>
using namespace std;

int main() {
  system("g++ b.cpp -o .usr");
  system("g++ b3.cpp -o .std");
  // system("g++ a_chk.cpp -o .chk");
  system("g++ b_gen.cpp -o .gen");
  int t = 0;
  while (++t < 100000) {
    system("./.gen > .fin");
    system("./.usr < .fin > .fout");
    system("./.std < .fin > .fstd");
    // if(system("./.chk"))break;
    if (system("diff .fout .fstd")) break;
    printf("AC#%d\n", t);
  }
  printf("WA#%d\n", t);
  return 0;
}
/*
3 5
2 1
1 1 3
2 3
1 1 2
2 2

*/
