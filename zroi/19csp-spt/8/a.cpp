#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 1e5 + 5;

int n, mn;
int a[N], pos[N];
int d[N], p[N];

int f[N];
void init(int lim) { FOR(i, 0, lim) f[i] = i; }
int get(int u) { return f[u] == u ? u : f[u] = get(f[u]); }
void merge(int u, int v) {
  // printf("merge(%d,%d)\n",u,v);
  f[get(u)] = get(v);
}
bool find(int u, int v) { return get(u) == get(v); }

int main() {
  scanf("%d", &n);
  FOR(i, 1, n) scanf("%d", &a[i]), pos[a[i]] = i;
  d[a[1]] = 1;
  FOR(i, 2, n) {
    if (a[i - 1] < a[i])
      d[a[i]] = d[a[i - 1]];
    else
      d[a[i]] = d[a[i - 1]] + 1;
  }
  // printf("d:");FOR(i,1,n)printf("%d%c",d[i]," \n"[i==n]);
  init(n + 1);
  p[1] = d[1], mn = p[1];
  merge(p[1], p[1] + 1);
  FOR(i, 2, n) {
    // printf("i=%d\n",i);
    if (d[i] == mn) {
      p[i] = get(p[i - 1]);
      merge(p[i], p[i] + 1);
    } else {
      p[i] = get(d[i]);
      merge(p[i], p[i] + 1);
    }
  }
  FOR(i, 1, n) printf("%d%c", p[i], " \n"[i == n]);
  return 0;
}
