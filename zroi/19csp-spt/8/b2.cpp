#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
#define LL long long
const int N = 5e5 + 5, P = 998244353;

int n, m;
int tot;

struct data {
  int op, u, v;
  void read() {
    scanf("%d", &op);
    if (op == 1)
      scanf("%d%d", &u, &v);
    else
      scanf("%d", &u);
  }
} d[N];
int a[N], p[N];
void check(int mask) {
  FOR(i, 1, n) {
    a[i] = mask % 3;
    mask /= 3;
    p[i] = i;
  }
  // printf("check:");FOR(i,1,n)printf("%d%c",a[i]," \n"[i==n]);
  FOR(i, 1, m) {
    if (d[i].op == 1) {
      int u = d[i].u, v = d[i].v;
      // printf("u=%d,v=%d\n",u,v);
      if ((a[p[u]] + 1) % 3 == a[p[v]] || a[p[u]] == a[p[v]])
        p[v] = -1; //笼子v没了
      else
        p[u] = p[v], p[v] = -1;
    } else {
      int u = d[i].u, fl = 0;
      FOR(i, 1, n) if (p[i] == u) { fl = 1; }
      // printf("u=%d\n",u);
      if (fl) {
        ++d[i].v;
        if (i == 5) {
          // printf("i=%d,",i);FOR(i,1,n)printf("%d%c",a[i]," \n"[i==n]);
        }
      }
    }
  }
}
int main() {
  scanf("%d%d", &n, &m);
  FOR(i, 1, m) { d[i].read(); }
  int lim = 1;
  FOR(i, 1, n) lim *= 3;
  FOR(i, 0, lim - 1) { check(i); }
  FOR(i, 1, m) if (d[i].op == 2) printf("%d\n", d[i].v);
  return 0;
}
