#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 1e5 + 5;

int n, p1[N], p2[N];

int main() {
  FILE *fout = fopen(".fout", "w"), *fchk = fopen(".fchk", "w");
  FILE *fin = fopen(".fin", "w");
  fscanf(fin, "%d", &n);
  FOR(i, 1, n) fscanf(fout, "%d", &p1[i]);
  FOR(i, 1, n) fscanf(fchk, "%d", &p2[i]);
  FOR(i, 2, n) p1[i] = min(p1[i - 1], p1[i]);
  FOR(i, 2, n) p2[i] = min(p2[i - 1], p2[i]);
  FOR(i, 1, n) if (p1[i] != p2[i]) return 1;
  return 0;
}
