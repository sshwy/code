#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
#define LL long long
const int N = 5e5 + 5, P = 998244353;

int n, m;
int fa[N], ch[N][2], sz[N];
int get(int u) { return fa[u] == u ? u : get(fa[u]); }
LL f[N][3], m3[N]; //在结点i上获胜的类别是j的方案数
int tot;

int getch(int u) { return ch[fa[u]][1] == u; }
void merge(int u, int v) { // u是主
  // printf("merge(%d,%d)\n",u,v);
  u = get(u), v = get(v);
  int z = ++tot;
  // printf("z=%d,u=%d,v=%d\n",z,u,v);
  fa[u] = fa[v] = z, ch[z][0] = u, ch[z][1] = v, sz[z] = sz[u] + sz[v];
  // ch[0]是主场
  f[z][0] = (1ll * f[v][0] * f[u][1] % P + 1ll * f[u][0] * (f[v][0] + f[v][1])) %
            P; // 0 win 1
  f[z][1] = (1ll * f[v][1] * f[u][2] % P + 1ll * f[u][1] * (f[v][1] + f[v][2])) %
            P; // 1 win 2
  f[z][2] = (1ll * f[v][2] * f[u][0] % P + 1ll * f[u][2] * (f[v][2] + f[v][0])) %
            P; // 2 win 0
  // printf("f[%d]= %lld, %lld, %lld\n",z,f[z][0],f[z][1],f[z][2]);
}
int query(int u) {
  // printf("\033[32mquery(%d)\033[0m\n",u);
  int p = u, k;
  LL ans[3] = {1, 1, 1};
  while (fa[p] != p) {
    k = getch(p), p = fa[p];
    int v = ch[p][!k]; //兄弟结点
    ans[0] = 1ll * ans[0] * (f[v][1] + (!k) * f[v][0]) % P;
    ans[1] = 1ll * ans[1] * (f[v][2] + (!k) * f[v][1]) % P;
    ans[2] = 1ll * ans[2] * (f[v][0] + (!k) * f[v][2]) % P;
  }
  int res = (ans[0] + ans[1] + ans[2]) % P;
  res = 1ll * res * m3[n - sz[p]] % P;
  return res;
}
int main() {
  scanf("%d%d", &n, &m);
  m3[0] = 1;
  FOR(i, 1, n) m3[i] = m3[i - 1] * 3ll % P;
  FOR(i, 1, n * 2) fa[i] = i;
  FOR(i, 1, n) sz[i] = 1;
  tot = n;
  FOR(i, 1, n) f[i][0] = f[i][1] = f[i][2] = 1;
  FOR(i, 1, m) {
    int op, u, v;
    scanf("%d", &op);
    if (op == 2) {
      scanf("%d", &u);
      printf("%d\n", query(u));
    } else {
      scanf("%d%d", &u, &v);
      merge(u, v);
    }
  }
  return 0;
}
