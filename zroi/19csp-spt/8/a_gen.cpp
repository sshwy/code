#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 2e5 + 5, INF = 0x3f3f3f3f;

using namespace RA;
int n, p[N], a[N], d[N];
bool vis[N];

bool check() {
  d[1] = p[1];
  FOR(i, 2, n) d[i] = min(d[i - 1], p[i]);
  FOR(i, 1, n) vis[i] = 0;
  FOR(i, 1, n) {
    int mn = INF, u = 0;
    FOR(j, 1, n) if (!vis[j]) {
      if (d[j] < mn) mn = d[j], u = j;
    }
    a[i] = u;
    vis[u] = 1;
  }
  FOR(i, 1, n) printf("%d%c", a[i], " \n"[i == n]);
  return 1;
}
int main() {
  srand(clock());
  n = r(5, 10000);
  printf("%d\n", n);
  FOR(i, 1, n) p[i] = i;
  random_shuffle(p + 1, p + n + 1);
  random_shuffle(p + 1, p + n / 2 + 1);
  check();
  FILE *ans = fopen(".fchk", "w");
  FOR(i, 1, n) fprintf(ans, "%d%c", p[i], " \n"[i == n]);
  fclose(ans);
  return 0;
}
