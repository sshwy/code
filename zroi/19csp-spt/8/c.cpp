#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 1e5 + 5, M = N * 2, P = 998244353;

struct qxx {
  int nex, t;
};
qxx e[M];
int h[N], le = 1;
void add_path(int f, int t) { e[++le] = (qxx){h[f], t}, h[f] = le; }
#define FORe(i, u, v) for (int i = h[u], v; v = e[i].t, i; i = e[i].nex)

int n, m;

int use[N], tot;
int d[N]; //记录子节点中有几个是use
void dfs1(int u, int p = 0) {
  use[u] = 1;
  FORe(i, u, v) {
    if (v == p) continue;
    dfs1(v, u);
    if (use[v]) use[u] = 0, d[u]++;
  }
  tot += use[u];
}
int root_use[N];
void dfs2(int u, int p = 0) {
  root_use[u] = use[u];
  FORe(i, u, v) {
    if (v == p) continue;
    // u换到v
    int useu = use[u], usev = use[v], dv = d[v];
    if (d[u] == 1 && use[v]) { //则换根后，use[u]=1
      use[u] = 1, use[v] = 0;
      d[v]++;
    } else if (use[u]) {
      use[v] = 0, d[v]++;
    } else { // use[u]=0，则对v没有影响
    }
    dfs2(v, u);
    use[u] = useu, use[v] = usev, d[v] = dv;
  }
}

int main() {
  scanf("%d%d", &n, &m);
  FOR(i, 1, n - 1) {
    int u, v;
    scanf("%d%d", &u, &v);
    add_path(u, v), add_path(v, u);
  }
  dfs1(1);
  dfs2(1);
  int ans = tot, las = root_use[1];
  printf("%d\n", ans);
  FOR(i, 1, m) {
    int k;
    scanf("%d", &k);
    if (las) {
      ans = 1ll * ans * n % P;
      las = 0; //这次根结点没选
    } else {
      ans = 1ll * ans * n % P;
      ans = (ans + tot) % P;
      las = root_use[k];
    }
    printf("%d\n", ans);
  }
  return 0;
}
