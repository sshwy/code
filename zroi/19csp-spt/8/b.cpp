#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
#define int long long
const int N = 5e5 + 5, P = 998244353, MTX = 5;

int n, m;
int fa[N], ch[N][2], sz[N];
int f[N][3], m3[N]; //在结点i上获胜的类别是j的方案数
int tot;

struct Matrix {
  int h, w;
  int c[MTX][MTX];
  Matrix() {
    h = w = 0;
    FOR(i, 1, h) FOR(j, 1, w) c[i][j] = 0;
  }
  Matrix(int _h, int _w) {
    h = _h, w = _w;
    FOR(i, 1, h) FOR(j, 1, w) c[i][j] = 0;
  }
  Matrix(int _h) {
    h = w = _h;
    FOR(i, 1, h) FOR(j, 1, w) c[i][j] = 0;
    FOR(i, 1, h) c[i][i] = 1;
  }
  Matrix(int _h, int _w, int _c[MTX][MTX]) {
    h = _h, w = _w;
    FOR(i, 1, h) FOR(j, 1, w) c[i][j] = _c[i][j];
  }
  Matrix operator*(const Matrix &m) {
    assert(w == m.h);
    Matrix res(h, m.w);
    FOR(i, 1, h) FOR(k, 1, w) FOR(j, 1, m.w) {
      res.c[i][j] = (res.c[i][j] + 1ll * c[i][k] * m.c[k][j]) % P;
    }
    return res;
  }
  Matrix &operator*=(const Matrix &m) {
    *this = *this * m;
    return *this;
  }
};
Matrix g[N]; // g[u]表示从u到fa[u]路径上的矩阵乘积和。初始化时是单位矩阵。
int get(int u) {
  if (fa[u] == u) return u;
  int x = fa[u];
  fa[u] = get(fa[u]);
  g[u] *= g[x];
  return fa[u];
}
int getch(int u) { return ch[fa[u]][1] == u; }
void merge(int u, int v) { // u是主
  u = get(u), v = get(v);
  int z = ++tot;
  fa[u] = fa[v] = z, ch[z][0] = u, ch[z][1] = v, sz[z] = sz[u] + sz[v];

  int gu[MTX][MTX] = {{0}};
  gu[1][1] = (f[v][1] + f[v][0]) % P;
  gu[2][2] = (f[v][2] + f[v][1]) % P;
  gu[3][3] = (f[v][0] + f[v][2]) % P;
  g[u] *= Matrix(3, 3, gu);

  int gv[MTX][MTX] = {{0}};
  gv[1][1] = f[u][1];
  gv[2][2] = f[u][2];
  gv[3][3] = f[u][0];
  g[v] *= Matrix(3, 3, gv);

  // ch[0]是主场
  f[z][0] = (1ll * f[v][0] * f[u][1] % P + 1ll * f[u][0] * (f[v][0] + f[v][1])) %
            P; // 0 win 1
  f[z][1] = (1ll * f[v][1] * f[u][2] % P + 1ll * f[u][1] * (f[v][1] + f[v][2])) %
            P; // 1 win 2
  f[z][2] = (1ll * f[v][2] * f[u][0] % P + 1ll * f[u][2] * (f[v][2] + f[v][0])) %
            P; // 2 win 0
}
int query(int u) {
  int Mans[MTX][MTX] = {{0}};
  Mans[1][1] = Mans[1][2] = Mans[1][3] = 1;
  get(u);
  Matrix mans = Matrix(1, 3, Mans) * g[u];
  int res = (mans.c[1][1] + mans.c[1][2] + mans.c[1][3]) % P;
  res = 1ll * res * m3[n - sz[get(u)]] % P;
  return res;
}
signed main() {
  scanf("%lld%lld", &n, &m);
  m3[0] = 1;
  FOR(i, 1, n) m3[i] = m3[i - 1] * 3ll % P;
  FOR(i, 1, n * 2) fa[i] = i;
  FOR(i, 1, n * 2) g[i] = Matrix(3);
  FOR(i, 1, n) sz[i] = 1;
  tot = n;
  FOR(i, 1, n) f[i][0] = f[i][1] = f[i][2] = 1;
  FOR(i, 1, m) {
    int op, u, v;
    scanf("%lld", &op);
    if (op == 2) {
      scanf("%lld", &u);
      printf("%lld\n", query(u));
    } else {
      scanf("%lld%lld", &u, &v);
      merge(u, v);
    }
  }
  return 0;
}
