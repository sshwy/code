#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
using namespace RA;
const int N = 5e5 + 5;

int f[N];
void init(int lim) { FOR(i, 0, lim) f[i] = i; }
int get(int u) { return f[u] == u ? u : f[u] = get(f[u]); }
void merge(int u, int v) {
  // printf("merge(%d,%d)\n",u,v);
  f[get(u)] = get(v);
}
bool find(int u, int v) { return get(u) == get(v); }

int main() {
  srand(clock());
  int n = 3e3, m = (n - 1) * 2 + 1;
  printf("%d %d\n", n, m);
  printf("2 %d\n", r(1, n));
  ROF(i, n, 2) {
    printf("1 %d %d\n", r(1, i - 1), i);
    printf("2 %d\n", r(1, n));
  }
  return 0;
}
