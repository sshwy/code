#include <bits/stdc++.h>
#define FOR(a, b, c) for (int a = (b); a <= (c); a++)
using namespace std;
const int N = 1e6 + 5;

int n, m;

long long calc(int *A, int Asig, int *B, int Bsig) {
  long long res = 0;
  int bL = n, bR = n, cnt = B[n];
  for (int i = Asig; i <= 2 * n - 1; i += 2) {
    int len = i <= n ? i : n * 2 - i;
    int L = i <= n ? n + 1 - i : n + 1 - (n * 2 - i);
    int R = i <= n ? L + 2 * (i - 1) : L + 2 * ((n * 2 - i) - 1);
    assert(L <= n);
    assert(R >= n);
    while (L < bL) --bL, cnt += B[bL];
    while (L > bL) cnt -= B[bL], ++bL;
    while (bR < R) ++bR, cnt += B[bR];
    while (bR > R) cnt -= B[bR], --bR;
    // printf("i=%d,L=%d,R=%d,cnt=%d,len=%d,A=%d\n",i,L,R,cnt,len,A[i]);
    if (A[i])
      res += len;
    else
      res += cnt;
  }
  return res;
}

int a[N * 2]; // L
int b[N * 2]; // J
int A[2][N * 2], B[2][N * 2];

int main() {
  scanf("%d%d", &n, &m);
  FOR(i, 1, m) {
    int x, y;
    scanf("%d%d", &x, &y);
    b[x + y - 1] = 1;
    a[x - y + n] = 1;
  }
  FOR(i, 1, n * 2 - 1) if (a[i]) A[i & 1][i] = 1;
  FOR(i, 1, n * 2 - 1) if (b[i]) B[i & 1][i] = 1;
  // FOR(i,1,n*2-1)printf("%d%c",a[i]," \n"[i==n*2-1]);
  // FOR(i,1,n*2-1)printf("%d%c",b[i]," \n"[i==n*2-1]);
  long long ans = 0;
  if (n & 1)
    ans = calc(A[0], 2, B[0], 2) + calc(A[1], 1, B[1], 1);
  else
    ans = calc(A[0], 2, B[1], 1) + calc(A[1], 1, B[0], 2);
  printf("%lld\n", ans);
  return 0;
}
