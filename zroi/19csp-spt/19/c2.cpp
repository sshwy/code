// problem:
#include <bits/stdc++.h>
using namespace std;

#define pb push_back
#define mk make_pair
#define lob lower_bound
#define upb upper_bound
#define fst first
#define scd second

typedef long long ll;
typedef unsigned long long ull;
typedef pair<int, int> pii;
typedef pair<ll, ll> pll;

inline int read() {
  int f = 1, x = 0;
  char ch = getchar();
  while (!isdigit(ch)) {
    if (ch == '-') f = -1;
    ch = getchar();
  }
  while (isdigit(ch)) {
    x = x * 10 + ch - '0';
    ch = getchar();
  }
  return x * f;
}
inline ll readll() {
  ll f = 1, x = 0;
  char ch = getchar();
  while (!isdigit(ch)) {
    if (ch == '-') f = -1;
    ch = getchar();
  }
  while (isdigit(ch)) {
    x = x * 10 + ch - '0';
    ch = getchar();
  }
  return x * f;
}

const int MAXN = 5e5 + 5;
int n, a[MAXN], nxt[MAXN];
map<int, int> pos;

int main() {
  n = read();
  for (int i = 1; i <= n; ++i) a[i] = read();
  for (int i = n; i >= 1; --i)
    nxt[i] = (pos[a[i]] ? pos[a[i]] : n + 1), pos[a[i]] = i;
  ll ans = 0, res = 0;
  set<int> s;
  for (int i = 1; i <= n; ++i) {
    s.insert(a[i]);
    ans += (ll)s.size() * i;
  }
  res = ans;
  int bg = clock();
  for (int i = 1; i <= n; ++i) {
    ll t = 1e18;
    if (pos[a[i]] != i) continue;
    for (int j = 1; j < i; ++j) {
      t = min(
          t, abs(a[i] - a[j]) + (ll)i * (i - 1) / 2 - (ll)nxt[i] * (nxt[i] - 1) / 2);
    }
    for (int j = i + 1; j < nxt[i]; ++j) {
      t = min(
          t, abs(a[i] - a[j]) + (ll)j * (j - 1) / 2 - (ll)nxt[i] * (nxt[i] - 1) / 2);
    }
    res = min(res, ans + t);
    s.insert(a[i]);
    if (1.0 * (clock() - bg) / CLOCKS_PER_SEC > 0.666) {
      cout << res << endl;
      return 0;
    }
  }
  cout << res << endl;
  return 0;
}
