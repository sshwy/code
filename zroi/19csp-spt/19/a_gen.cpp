#include <bits/stdc++.h>
using namespace std;
int r(int p) { return rand() % p; }
int r(int L, int R) { return r(R - L + 1) + L; }
int main() {
  srand(clock());
  int n = 3, L = r(1, 100), R = min((int)100, L + n);
  printf("%d\n", n);
  for (int i = 1; i <= n; i++) printf("%d ", r(L, R));
  return 0;
}
