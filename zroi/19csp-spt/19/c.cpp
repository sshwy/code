#include <bits/stdc++.h>
#define FOR(a, b, c) for (int a = (b); a <= (c); a++)
#define ROF(a, b, c) for (int a = (b); a >= (c); a--)
using namespace std;
const int N = 5e5 + 5;
const long long INF = 0x3f3f3f3f3f3f3f3f;

int n, m;
int a[N], b[N];
int dc[N], ld;
long long w, ans;

set<int> s;
int last_pos[N];
bool first_appear[N];
int nex[N];

struct fenwick {
  long long c[N], lim;
  void clear(int _lim) {
    lim = _lim;
    FOR(i, 0, lim) c[i] = INF;
  }
  void add(int pos, long long v) {
    for (int i = pos; i <= lim; i += i & -i) c[i] = min(c[i], v);
  }
  long long pre_min(int pos = -1) {
    if (pos == -1) pos = lim;
    long long res = INF;
    for (int i = pos; i > 0; i -= i & -i) res = min(res, c[i]);
    return res;
  }
} pre, suf;

long long S(int x) {
  if (x > n) return 0;
  return 1ll * (x + n) * (n - x + 1) / 2;
}

int main() {
  scanf("%d", &n);
  pre.clear(n), suf.clear(n);
  FOR(i, 1, n) scanf("%d", &a[i]);
  FOR(i, 1, n) dc[i] = a[i];
  sort(dc + 1, dc + n + 1);
  FOR(i, 1, n) b[i] = lower_bound(dc + 1, dc + n + 1, a[i]) - dc;
  FOR(i, 1, n) {
    s.insert(a[i]);
    w += 1ll * s.size() * i;
  }
  // printf("w=%lld\n",w);
  ans = w;
  ROF(i, n, 1) {
    if (last_pos[b[i]])
      nex[i] = last_pos[b[i]];
    else
      nex[i] = n + 1;
    last_pos[b[i]] = i;
  }
  FOR(i, 1, n) first_appear[i] = 1;
  FOR(i, 1, n) first_appear[nex[i]] = 0;
  // s.clear();
  ROF(i, n, 1) {
    if (first_appear[i]) {
      assert(s.find(a[i]) != s.end());
      s.erase(a[i]);
      // case1
      if (!s.empty()) {
        set<int>::iterator it = s.lower_bound(a[i]);
        long long d = INF;
        if (it != s.end()) d = min(d, 0ll + abs(*it - a[i]));
        if (it != s.begin()) --it, d = min(d, 0ll + abs(*it - a[i]));
        ans = min(ans, w + d - S(i) + S(nex[i]));
      }
      // case2 i < j
      // a[i]  -a[j]-S(j) +S(nex[i]) b[i]>b[j]
      //-a[i] +a[j]-S(j) +S(nex[i]) b[i]<b[j]

      long long x = pre.pre_min(b[i] - 1);
      ans = min(ans, w + a[i] + x + S(nex[i]));
      x = suf.pre_min(n - b[i]);
      ans = min(ans, w - a[i] + x + S(nex[i]));

      // if(ans==202)printf("i=%d,nex[%d]=%d\n",i,i,nex[i]);

      pre.add(b[i], -a[i] - S(i));
      // printf("pre add (%d,%lld)\n",b[i],-a[i]-S(i));
      suf.add(n - b[i] + 1, a[i] - S(i));
      // printf("suf add (%d,%lld)\n",b[i],a[i]-S(i));
    }
  }
  printf("%lld\n", ans);
  return 0;
}
