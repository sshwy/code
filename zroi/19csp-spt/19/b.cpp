#include <bits/stdc++.h>
#define FOR(a, b, c) for (int a = (b); a <= (c); a++)
using namespace std;
const int N = 5000, P = 1e9 + 7;
int INF;

int n, k;

int x[N], y[N];
bool use[N];

bool inter(int l, int r, int L, int R) {
  return (l < L && L < r && r < R) || (L < l && l < R && R < r);
}
int calc(int L, int R) {
  int res = 0;
  FOR(i, 1, k) res += inter(L, R, x[i], y[i]);
  return res;
}
int f[N][N], g[N][N];
int a[N][N];

int main() {
  scanf("%d%d", &n, &k);
  INF = n * n;
  FOR(i, 1, k)
  scanf("%d%d", &x[i], &y[i]), use[x[i]] = use[y[i]] = 1,
                               x[i] > y[i] ? x[i] ^= y[i] ^= x[i] ^= y[i] : 0;
  FOR(i, 1, n * 2) FOR(j, i + 1, n * 2) a[i][j] = calc(i, j);
  FOR(i, 1, n * 2) g[i][i - 1] = 1;
  FOR(i, 1, n * 2) if (use[i]) g[i][i] = 1;
  FOR(len, 1, n * 2 - 1) {
    FOR(i, 1, n * 2 - len) {
      int j = i + len;
      if (use[i])
        f[i][j] = f[i + 1][j], g[i][j] = g[i + 1][j];
      else if (use[j])
        f[i][j] = f[i][j - 1], g[i][j] = g[i][j - 1];
      else {
        f[i][j] = INF;
        FOR(k, i, j - 1) {
          if (use[k]) continue;
          // bucket k,j
          int fnew = f[i][k - 1] + f[k + 1][j - 1] + a[k][j];
          int gnew = 1ll * g[i][k - 1] * g[k + 1][j - 1] % P;
          if (!gnew) continue;
          if (f[i][j] > fnew) {
            f[i][j] = fnew, g[i][j] = gnew;
            // printf("(%d,%d) <= (%d)\n",i,j,k);
          } else if (f[i][j] == fnew) {
            g[i][j] = (g[i][j] + gnew) % P;
          }
        }
        // if(g[i][j])printf("f[%d,%d]=%d,
        // g[%d,%d]=%d\n",i,j,f[i][j],i,j,g[i][j]);
      }
    }
  }
  printf("%d\n", g[1][n * 2]);
  return 0;
}
