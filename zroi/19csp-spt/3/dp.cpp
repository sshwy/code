#include <cstdio>
#include <cstdlib>
#include <ctime>
using namespace std;

int main() {
  system("g++ c.cpp -o .usr");
  system("g++ c3.cpp -o .std");
  system("g++ c_gen.cpp -o .gen");
  int t = 0;
  while (++t) {
    // system("./.gen > .fin");
    system("./.gen");
    system("./.usr > c.out");
    system("./.std > c3.out");
    if (system("diff c.out c3.out")) break;
    printf("AC#%d\n", t);
  }
  printf("WA#%d\n", t);
  return 0;
}
/*
10 7 2
1 1 1
1 3 1
2 5 1
1 6 1
1 1 1
3 7 1
5 6 1

*/
