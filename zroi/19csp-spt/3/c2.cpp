#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
int pw(int a, int m, int p) {
  int res = 1;
  while (m) m & 1 ? res = 1ll * res * a % p : 0, a = 1ll * a * a % p, m >>= 1;
  return res;
}
const int N = 5000, P = 998244353;
int n, m, s;
struct dat {
  int l, r, b;
  void read() { scanf("%d%d%d", &l, &r, &b); }
};
bool cmp(dat x, dat y) {
  return x.l != y.l ? x.l < y.l : (x.r != y.r ? x.r < y.r : x.b < y.b);
}
dat d[N];

int sig(int k) { return 1 - 2 * (abs(k) & 1); }
bool inter(int x, int y) { return !(d[x].r < d[y].l || d[y].r < d[x].l); }
bool vis[N];
int calc(int mask) {
  int las = -1, tot = n, cnt = 0;
  FOR(i, 0, m - 1) if (mask >> i & 1) {
    if (las != -1 && inter(las, i) && d[las].b != d[i].b) return 0;
    las = i;
  }
  // printf("calc(%d)\n",mask);
  las = 0;
  FOR(i, 0, m - 1) if (mask >> i & 1) {
    // printf("(%d,%d),las=%d\n",d[i].l,d[i].r,las);
    las = max(las, d[i].l - 1);
    cnt += max(0, d[i].r - las);
    las = max(las, d[i].r);
  }
  // FOR(i,1,n)vis[i]=0;
  // FOR(i,0,m-1)if(mask>>i&1){
  //    FOR(j,d[i].l,d[i].r)vis[j]=1;
  //}
  tot -= cnt;
  // printf("tot=%d,sig=%d\n",tot,sig(__builtin_popcount(mask)));
  // FOR(i,1,n)printf("%d%c",vis[i]," \n"[i==n]);
  return pw(s, tot, P) * sig(__builtin_popcount(mask));
}
int ans;
int main() {
  scanf("%d%d%d", &n, &m, &s);
  FOR(i, 0, m - 1) { d[i].read(); }
  sort(d, d + m, cmp);
  int mm = 1 << m;
  FOR(i, 0, mm - 1) { ans = (ans + calc(i)) % P; }
  ans = (ans + P) % P;
  printf("%d\n", ans);
  return 0;
}
