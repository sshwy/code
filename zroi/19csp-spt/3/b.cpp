#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
int pw(int a, int m, int p) {
  int res = 1;
  while (m) m & 1 ? res = 1ll * res * a % p : 0, a = 1ll * a * a % p, m >>= 1;
  return res;
}
const int M = 1e5 + 5, N = 1e5 + 5, P = 998244353;

struct qxx {
  int nex, t;
};
qxx e[M];
int h[N], le;
void add_path(int f, int t) { e[++le] = (qxx){h[f], t}, h[f] = le; }
#define FORe(i, u, v) for (int i = h[u], v; v = e[i].t, i; i = e[i].nex)

int m, n;
int dp[N][2];
int a[N][2];

void dfs(int u) {
  dp[u][0] = 1;
  dp[u][1] = 1;
  FORe(i, u, v) {
    dfs(v);
    if (dp[v][0] == 1) dp[u][0] = 0;
    if (dp[v][1] == 1) dp[u][1] = 0;
  }
  if (!h[u])
    dp[u][0] = 1, dp[u][1] = 0; // dp[u,0]表示保留先手，dp[u,1]表示强制转让先手
}
void readtree(int id) {
  scanf("%d", &n);
  memset(h, 0, sizeof(int) * (n + 1));
  le = 0;
  FOR(i, 2, n) {
    int x;
    scanf("%d", &x);
    add_path(x, i);
  }
  dfs(1);
  a[id][0] = dp[1][0], a[id][1] = dp[1][1];
}
int cnt[2][2], ans;
int main() {
  scanf("%d", &m);
  FOR(i, 1, m) readtree(i);
  FOR(i, 1, m) cnt[a[i][0]][a[i][1]]++;
  // printf("%d %d %d %d\n",cnt[0][0],cnt[0][1],cnt[1][0],cnt[1][1]);
  ans =
      (1ll * (pw(2, cnt[1][1], P) - 1) * pw(2, cnt[0][0] + cnt[0][1], P) % P +
          (cnt[0][1] ? 1ll * pw(2, cnt[0][1] - 1, P) * pw(2, cnt[0][0], P) % P : 0)) %
      P * pw(2, cnt[1][0], P) % P;
  printf("%d\n", ans);
  return 0;
}
/*
 * 对于(1,0)的情况：先手如果必胜，则它可以保留先手；先手如果必败，则在走的时侯后手可以不让先手转让。因此(1,0)无论怎样都不会改状态。
 * (0,1)：先手可以强制转让给后手，则如果有奇数个(0,1)先手必胜，偶数先手必败；
 * (1,1)：先手想干嘛干嘛。对于一个局面，如果有至少一个(1,1)，可以先用这个直到只剩一个，然后根据剩下局面的先手胜负情况决定最后一个是转让还是不转
 * 即，有(1,1)必胜。
 * (0,0)：先手啥都决定不了，决定权在后手。如果我们只有1个(0,0)，则先手必败；如果有若干个(0,0)，则后手可以强制让先手每次都先手；
 * 直到只剩一个(0,0)，然后还是先手必败。
 * (0,1)和(0,0)：如果先手选择走(0,0)，则先手必输；因此先手会尽可能走(0,1)。如果(0,1)有奇数个，则先手必胜；否则先手必败。
 */
