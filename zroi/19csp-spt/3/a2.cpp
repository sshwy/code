#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/

int n, nn;
char s[100];

int ans;
void check(int mask) {
  int t1[100], l1 = 0;
  int t2[100], l2 = 0;
  FOR(i, 0, n * 2 - 1) {
    if (mask >> i & 1)
      t1[++l1] = i;
    else
      t2[++l2] = i;
  }
  bool fl = 1;
  assert(l1 == l2 && l1 == n);
  FOR(i, 1, n)
  fl &= (t1[i] < t2[i] && s[i] == '0') || (t1[i] > t2[i] && s[i] == '1');
  ans += fl;
  // if(fl){ FOR(i,0,n*2-1)printf("%d",mask>>i&1); puts(""); }
}
int main() {
  scanf("%d", &n);
  scanf("%s", s + 1);
  nn = 1 << (n * 2);
  FOR(i, 0, nn - 1) {
    if (__builtin_popcount(i) != n) continue;
    check(i);
  }
  printf("%d\n", ans);
  return 0;
}
