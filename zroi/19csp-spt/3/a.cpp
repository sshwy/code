#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
#define int lld
int pw(int a, int m, int p) {
  int res = 1;
  while (m) m & 1 ? res = 1ll * res * a % p : 0, a = 1ll * a * a % p, m >>= 1;
  return res;
}
const int N = 2e5 + 5, P = 998244353;

int n;
char s[N];

int ans = 1;
int T[N];
void calc(int l, int c) {
  if (c == 2) return;
  ans = 1ll * ans * T[l + 1] % P;
}
signed main() {
  scanf("%lld", &n);
  scanf("%s", s + 1);
  FOR(i, 1, n) s[i] -= '0';

  T[0] = T[1] = 1;
  FOR(i, 2, n * 2)
  T[i] = (4ll * i - 2ll) * pw(i + 1, P - 2, P) % P * T[i - 1] % P;

  int l = 1, r = n;
  if (s[l] == 0) ++l;
  if (s[r] == 1) --r;
  FOR(i, l, r - 1) if (s[i] == 1 && s[i + 1] == 0) s[i] = s[i + 1] = 2;
  s[l - 1] = 2;
  s[r + 1] = 2;
  int len = 1;
  FOR(i, l, r + 1) {
    if (s[i] == s[i - 1]) {
      len++;
    } else {
      calc(len, s[i - 1]);
      len = 1;
    }
  }
  printf("%lld\n", ans);
  return 0;
}
