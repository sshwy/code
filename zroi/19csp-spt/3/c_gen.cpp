#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/

using namespace RA;
int n, m, s;

int main() {
  freopen("c.in", "w", stdout);
  srand(clock());
  n = r(5, 10), m = r(5, 10), s = r(1, 10);
  printf("%d %d %d\n", n, m, s);
  FOR(i, 1, m) {
    int L = r(1, n), R = r(1, n), b = r(1, s);
    printf("%d %d %d\n", min(L, R), max(L, R), b);
  }
  return 0;
}
