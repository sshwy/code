#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
#define int lld
const int N = 2e5 + 5, P = 998244353;
int n, m, s;
struct dat {
  int l, r, b;
  void read() { scanf("%lld%lld%lld", &l, &r, &b); }
};
bool cmp(dat x, dat y) {
  return x.b != y.b ? x.b < y.b : x.r != y.r ? x.r < y.r : x.l > y.l;
}
bool cmp2(dat x, dat y) { return x.r != y.r ? x.r < y.r : x.b > y.b; }
dat d[N], a[N];
int la;

int sig(int k) { return 1 - 2 * (abs(k) & 1); }
bool inter(int x, int y) { return !(d[x].r < d[y].l || d[y].r < d[x].l); }
bool vis[N], del[N];
int ans;

vector<int> q[N];
int ql[N], qr[N], sum[N];
int f[N], g[N], h[N];

int G(int k) { // make sure to deal with all interval (l,r),r<=k
  if (k == 0) return g[k] = 1;
  if (g[k]) return g[k];
  g[k] = (1ll * s * G(k - 1) % P + h[k]) % P;
  return g[k];
}
void DP() {
  FOR(i, 1, m) {
    const dat &cur = a[i];
    f[i] = G(cur.l - 1);
    int &l = ql[cur.b], &r = qr[cur.b], &s = sum[cur.b];
    vector<int> &v = q[cur.b];
    while (r + 1 < v.size() && v[r + 1] < i) ++r, s += f[v[r]];
    while (l <= r && a[v[l]].r < cur.l) s -= f[v[l]], ++l;
    f[i] = P - (f[i] + s) % P;
    h[cur.r] = (h[cur.r] + f[i]) % P;
  }
}
signed main() {
  // freopen("c.in","r",stdin);
  // freopen("c.out","w",stdout);
  scanf("%lld%lld%lld", &n, &m, &s);
  FOR(i, 1, m) d[i].read();
  sort(d + 1, d + m + 1, cmp);
  int las = 1;
  FOR(i, 2, m) {
    if (d[las].l >= d[i].l && d[las].b == d[i].b)
      del[i] = 1;
    else
      las = i;
  }
  FOR(i, 1, m) {
    if (del[i]) continue;
    a[++la] = d[i];
  }
  m = la;
  sort(a + 1, a + m + 1, cmp2);
  FOR(i, 1, m) { q[a[i].b].pb(i); }
  FOR(i, 1, s) ql[i] = 0, qr[i] = -1;
  DP();
  ans = G(n);
  ans = (ans + P) % P;
  printf("%lld", ans);
  return 0;
}
