#include <bits/stdc++.h>
#define Mod(x) (x >= mod ? x - mod : x)
using namespace std;
const int mod = 998244353;
struct Seg {
  int l, r, v, id;
} se[200010];
int n, m, s, tmp, l[200010], Sum[200010], g[200010] = {1}, dp[200010], nw = 1,
                                          ch[200010], p;
vector<Seg> S[200010];
bool cmp(Seg x, Seg y) { return x.r == y.r ? x.l > y.l : x.r < y.r; }
int DP(const Seg &x) {
  while (l[p = x.v] < S[p].size() && S[p][l[p]].r < x.l)
    Sum[p] = Mod(Sum[p] - dp[S[p][l[p]].id] + mod), ++l[p];
  return Sum[p] = (Sum[p] + (dp[x.id] = mod - Mod(Sum[p] + g[x.l - 1]))) % mod,
         dp[x.id];
}
int main() {
  freopen("c.in", "r", stdin);
  // freopen("c3.out","w",stdout);
  scanf("%d%d%d", &n, &m, &s);
  for (int i = 1; i <= m; ++i) scanf("%d%d%d", &se[i].l, &se[i].r, &se[i].v);
  sort(se + 1, se + m + 1, cmp);
  for (int i = 1; i <= m; ++i) {
    tmp = se[i].v, se[i].id = i;
    if (S[tmp].empty() || S[tmp][S[tmp].size() - 1].l < se[i].l)
      ch[i] = 1, S[tmp].push_back(se[i]);
  }
  for (int i = 1; i <= n; ++i) {
    g[i] = 1ll * g[i - 1] * s % mod;
    for (; nw <= m && se[nw].r == i; ++nw)
      if (ch[nw]) {
        // printf("%d,%d,%d,%d\n",se[nw].l,se[nw].r,se[nw].v,se[nw].id);
        g[i] = (g[i] + DP(se[nw])) % mod;
      }
    printf("G(%d)=%d\n", i, g[i]);
  }
  return printf("%d", g[n]), 0;
}
