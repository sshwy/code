#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 1 << 16;

int n, m, p[N], a[N];
int ans;

bool use[N];
void check() {
  bool fl = 1;
  FOR(i, 0, (1 << m) - 1) {
    int mx = -1, mxp = -1;
    FOR(j, 1, n) if (mx < (a[j] ^ i)) mxp = j, mx = (a[j] ^ i);
    if (mxp != p[i]) fl = 0;
  }
  // FOR(i,1,n)printf("%d%c",a[i]," \n"[i==n]);
  ans += fl;
}
void dfs(int k) {
  if (k > n) {
    check();
    return;
  }
  FOR(i, 0, (1 << m) - 1) if (!use[i]) {
    a[k] = i, use[i] = 1;
    dfs(k + 1);
    use[i] = 0;
  }
}
int main() {
  scanf("%d%d", &n, &m);
  FOR(i, 0, (1 << m) - 1) scanf("%d", &p[i]);
  FOR(i, 1, (1 << m)) a[i] = i - 1;
  dfs(1);
  printf("%d\n", ans);
  return 0;
}
