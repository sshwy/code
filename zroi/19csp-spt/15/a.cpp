#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 1e5 + 5, M = 102;

int n, m;
char a[N], b[N];
int nex[N][26];

int main() {
  scanf("%d%d", &n, &m);
  scanf("%s", a + 1);
  scanf("%s", b + 1);
  FOR(i, 1, n) a[i] -= 'a';
  FOR(i, 1, m) b[i] -= 'a';
  FOR(i, 0, 25) nex[n][i] = nex[n + 1][i] = n + 1;
  ROF(i, n - 1, 0) {
    FOR(j, 0, 25) { nex[i][j] = a[i + 1] == j ? i + 1 : nex[i + 1][j]; }
  }
  long long ans = 0;
  FOR(i, 1, n) {
    int pos = i - 1;
    FOR(j, 1, m) pos = nex[pos][b[j]];
    if (pos == n + 1) continue;
    ans += n - pos + 1;
  }
  printf("%lld\n", ans);
  return 0;
}
