#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 1 << 16, P = 1e9 + 7;

int n, m, p[N], a[N];
int ans = 1, tim;
int vis[N], cnt;

void dfs(int l, int r) {
  if (l == r) return;
  int mid = (l + r) >> 1;
  int f1 = 1;
  ++tim;
  FOR(i, 0, mid - l) if (p[l + i] != p[mid + 1 + i]) f1 = 0;
  if (f1) return ans = ans * 2 % P, dfs(l, mid), void();
  FOR(i, l, mid) vis[p[i]] = tim;
  FOR(i, mid + 1, r) if (vis[p[i]] == tim) {
    puts("0");
    exit(0);
  }
  dfs(l, mid), dfs(mid + 1, r);
}
int main() {
  scanf("%d%d", &n, &m);
  FOR(i, 0, (1 << m) - 1) scanf("%d", &p[i]);
  FOR(i, 0, (1 << m) - 1) cnt += !vis[p[i]], vis[p[i]] = 1;
  tim = 2;
  if (cnt < n) return puts("0"), 0;
  dfs(0, (1 << m) - 1);
  printf("%d\n", ans);
  return 0;
}
