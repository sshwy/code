#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
typedef long long LL;
const LL MTX = 16, INF = 0x3f3f3f3f3f3f;
#define for_subset(i, mask, x) \
  for (int i = mask, x; x = __builtin_ctz(i), i; i -= i & -i)

int n;

int w[MTX][MTX];
LL f[1 << 15][MTX][MTX], g[1 << 15];
bool vis[1 << 15];

void calc(int mask) {
  if (mask == 0 || (mask & -mask) == mask) return;
  if (vis[mask]) return;
  // printf("calc(%d)\n",mask);
  g[mask] = INF;
  int gx = -1, gy = -1;
  for_subset(i, mask, x) {
    // printf("for_subset(%d,%d,%d)\n",i,mask,x);
    int mask2 = mask - (1 << x);
    calc(mask2);              //剩下的点搞一个生成树
    for_subset(j, mask2, y) { //枚举(y,x)
      // printf("for_subset(%d,%d,%d)\n",j,mask2,y);
      LL dis = 0;
      for_subset(k, mask2, u) {
        // printf("for_subset(%d,%d,%d)\n",k,mask2,u);
        // printf("u=%d,y=%d,y=%d,x=%d\n",u,y,y,x);
        dis += f[mask2][u][y] + w[y][x];
      }
      if (g[mask] > dis + g[mask2]) {
        g[mask] = dis + g[mask2];
        gx = x, gy = y;
      }
    }
  }
  // printf("calc(%d)\n",mask);
  // printf("gx=%d,gy=%d\n",gx,gy);
  int mask2 = mask - (1 << gx);
  memcpy(f[mask], f[mask2], sizeof(f[mask]));
  for_subset(i, mask2, y) {
    // printf("for_subset(%d,%d,%d)\n",i,mask2,y);
    f[mask][y][gx] = f[mask][gx][y] = w[gx][gy] + f[mask2][gy][y];
  }
  vis[mask] = 1;
}
int main() {
  scanf("%d", &n);
  FOR(i, 0, n - 1)
  FOR(j, i + 1, n - 1) scanf("%d", &w[i][j]), w[j][i] = w[i][j];
  FOR(i, 0, n - 1) FOR(j, 0, n - 1) if (i != j) f[0][i][j] = INF;
  calc((1 << n) - 1);
  printf("%lld\n", g[(1 << n) - 1]);
  return 0;
}
