#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
//#define int lld
const int N = 2e5 + 5;

int n, d;
char s[N];
int b[N], a[N], pre[N][2], lb;
bool vis[N];

bool check(int k) {}

int main() {
  scanf("%d%d", &n, &d);
  scanf("%s", s + 1);
  FOR(i, 1, n) scanf("%d", &a[i]);

  int preIP = 0, nexIP = 0;

  FOR(i, 1, n) {
    if (nexIP <= i) {
      nexIP = i + 1;
      while (nexIP <= n && s[nexIP] == 'B') ++nexIP;
    }
    if (s[i] == 'B') {
      pre[i][0] = preIP, pre[i][1] = nexIP;
      if (!vis[nexIP]) b[++lb] = nexIP, vis[nexIP] = 1;
    } else if (s[i] == 'P') {
      pre[i][0] = preIP;
      preIP = i;
    } else {
      preIP = i;
    }
    if (!vis[i]) b[++lb] = i, vis[i] = 1;
  }
  FOR(i, 1, lb) printf("%d%c", b[i], " \n"[i == lb]);
  assert(lb == n);

  FOR(i, 1, n) printf("%2c%c", s[i], " \n"[i == n]);
  FOR(i, 1, n) printf("%2d%c", pre[i][0], " \n"[i == n]);
  FOR(i, 1, n) printf("%2d%c", pre[i][1], " \n"[i == n]);
  int l = 1, r = n + 1, mid;
  while (l < r) mid = (l + r) >> 1, check(mid) ? r = mid : l = mid + 1;
  printf("%d\n", l);
  return 0;
}
