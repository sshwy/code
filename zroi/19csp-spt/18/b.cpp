#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 2e5 + 5;

int n, m, q;

int f[N];
void init(int lim) { FOR(i, 0, lim) f[i] = i; }
int get(int u) { return f[u] == u ? u : f[u] = get(f[u]); }

set<int> s[N];

int main() {
  scanf("%d%d%d", &n, &m, &q);
  FOR(i, 1, m) {
    int x, y;
    scanf("%d%d", &x, &y);
    s[x].insert(y), s[y].insert(x); // x不能和y在一起
  }
  init(n);
  FOR(i, 1, q) {
    int x, y;
    scanf("%d%d", &x, &y);
    x = get(x), y = get(y);
    if (s[x].size() > s[y].size()) swap(x, y);
    if (s[x].find(y) == s[x].end()) {
      for (int z : s[x]) {
        s[y].insert(z);
        s[z].erase(x);
        s[z].insert(y);
      }
      f[x] = y;
      printf("1");
    } else {
      printf("0");
    }
  }
  return 0;
}
