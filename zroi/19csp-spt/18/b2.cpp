#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 2e5 + 5;

int n, m, q;
int f[N], g[N], x[N], y[N];

void init(int lim) { FOR(i, 0, lim) f[i] = i, g[i] = 1; }
int get(int u) { return f[u] == u ? u : f[u] = get(f[u]); }
void merge(int u, int v) {
  u = get(u), v = get(v);
  if (g[u] > g[v]) swap(u, v);
  f[u] = v, g[v] += g[u];
}

int main() {
  scanf("%d%d%d", &n, &m, &q);
  init(n);
  FOR(i, 1, m) { scanf("%d%d", &x[i], &y[i]); }
  FOR(i, 1, q) {
    int u, v, fl = 1;
    scanf("%d%d", &u, &v);
    FOR(i, 1, m) {
      if (get(x[i]) == get(u) && get(y[i]) == get(v) ||
          get(x[i]) == get(v) && get(y[i]) == get(u)) {
        fl = 0;
        break;
      }
    }
    if (fl) {
      printf("1"), merge(u, v);
    } else
      printf("0");
  }
  return 0;
}
