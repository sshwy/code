#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 2e6 + 5, SZ = 1 << 24;

int n, ans[N];

vector<int> cls[N]; // fi:在线段树上的下标.se:大小size

int tot, col[N];
// tot:有多少团人,col:每团人的颜色
int rk[N];

namespace seg {
  int s[SZ];
  void clear(int u = 1, int l = 1, int r = n) {
    s[u] = 0;
    if (l == r) return;
    int mid = (l + r) >> 1;
    clear(u << 1, l, mid), clear(u << 1 | 1, mid + 1, r);
  }
  void add(int pos, int v, int u = 1, int l = 1, int r = n) {
    if (l == r) return s[u] += v, void();
    int mid = (l + r) >> 1;
    if (pos <= mid)
      add(pos, v, u << 1, l, mid);
    else
      add(pos, v, u << 1 | 1, mid + 1, r);
    s[u] = s[u << 1] + s[u << 1 | 1];
    // printf("sum[%d,%d]=%d\n",l,r,s[u]);
  }
  int sum_query(int L, int R, int u = 1, int l = 1, int r = n) {
    if (r < L || R < l) return 0;
    if (L <= l && r <= R) return s[u];
    int mid = (l + r) >> 1;
    return sum_query(L, R, u << 1, l, mid) + sum_query(L, R, u << 1 | 1, mid + 1, r);
  }
  int suffix_query_pos;
  int _suffix_query(
      int L, int R, int S, int u = 1, int l = 1, int r = n) { //返回的是后缀和
    if (r < L || R < l) return 0;
    // printf("_suffix_query(%d,%d,%d,%d,%d,%d)\n",L,R,S,u,l,r);
    if (L <= l && r <= R) {
      if (s[u] < S)
        return s[u];
      else {
        if (l == r) {
          suffix_query_pos = l; //找到了
          return s[u];
        }
        int mid = (l + r) >> 1;
        int x = _suffix_query(L, R, S, u << 1 | 1, mid + 1, r);
        if (x >= S) return x;
        return x + _suffix_query(L, R, S - x, u << 1, l, mid);
      }
    } else {
      int mid = (l + r) >> 1;
      int x = _suffix_query(L, R, S, u << 1 | 1, mid + 1, r);
      if (x >= S) return x;
      return x + _suffix_query(L, R, S - x, u << 1, l, mid);
    }
  }
  int suffix_sum;
  int suffix_query(int L, int R, int S) {
    suffix_sum = _suffix_query(L, R, S);
    return suffix_query_pos;
  }
  int query_zero_pos(int pos, int u = 1, int l = 1, int r = n) {
    if (r - l + 1 - s[u] < pos) return -1;
    if (l == r) {
      assert(s[u] == 0);
      return l;
    }
    int mid = (l + r) >> 1;
    int x = query_zero_pos(pos, u << 1, l, mid);
    if (x == -1)
      return query_zero_pos(pos - (mid - l + 1 - s[u << 1]), u << 1 | 1, mid + 1, r);
    return x;
  }
} // namespace seg

int main() {
  freopen("c.txt", "w", stdout);
  scanf("%d", &n);
  FOR(i, 1, n) {
    int c, a;
    scanf("%d%d", &c, &a);
    a = min(a, i - 1);
    // printf("c[%d]=%d,a[%d]=%d\n",i,c,i,a);
    int pos = seg::suffix_query(1, tot, a); //查询区间[1,tot]的后缀和大于等于a的pos
    // printf("suffix_sum=%d\n",seg::suffix_sum);
    if (col[pos] == c || seg::suffix_sum == a && pos > 1 &&
                             col[pos - 1] == c) { //可以插入到第a个人前面
      rk[i] = i - a;
      seg::add(col[pos] == c ? pos : pos - 1, 1);
      // printf("add(%d,1)\n",col[pos]==c?pos:pos-1);
      // printf("\t\t\trk[%d]=%d\n",i,rk[i]);
    } else {
      vector<int>::iterator x = (lower_bound(cls[c].begin(), cls[c].end(),
          pos));               //位置比pos大的第一个颜色是c的block
      if (x == cls[c].end()) { //只能站队尾
        ++tot;
        col[tot] = c;
        cls[c].pb(tot); //这个block的位置
        seg::add(tot, 1);
        // printf("add(%d,1)\n",tot);
        rk[i] = i;
      } else {
        // printf("cls[%d]:",c);for(int x:cls[c]){
        // printf("%d ",x);
        //}
        // puts("");
        // printf("x=%d,tot=%d\n",*x,tot);
        rk[i] = i - seg::sum_query(*x, tot);
        // printf("sum_query(%d,%d)=%d\n",*x,tot,seg::sum_query(*x,tot));
        seg::add(*x, 1);
        // printf("add(%d,1)\n",*x);
      }
    }
  }
  seg::clear();
  ROF(i, n, 1) {
    int pos = seg::query_zero_pos(rk[i]);
    ans[pos] = i;
    seg::add(pos, 1);
  }
  FOR(i, 1, n) printf("%d%c", ans[i], " \n"[i == n]);
  return 0;
}
