// by zgh
#include <bits/stdc++.h>
#define lowbit(i) ((i) & (-(i)))
using namespace std;
int n;
const int MAX_N = 500005;
int bit[MAX_N];
int A[MAX_N], C[MAX_N];
int rk[MAX_N], ans[MAX_N];
vector<int> vec[MAX_N];
int query(int x) {
  int ret = 0;
  for (int i = x; i > 0; i -= lowbit(i)) ret += bit[i];
  return ret;
}
void modify(int x, int y) {
  for (int i = x; i <= n; i += lowbit(i)) bit[i] += y;
}
int binary(int sum) {
  int k = query(n) - sum;
  int pos = 0, cur = 0;
  for (int i = 18; i >= 0; i--) {
    if ((pos | (1 << i)) <= n && bit[pos | (1 << i)] + cur < k) {
      cur += bit[pos | (1 << i)];
      pos |= 1 << i;
    }
  }
  return pos + 1;
}
int main() {
  cin >> n;
  for (int i = 1; i <= n; i++) cin >> C[i] >> A[i];
  int cur = 0;
  for (int i = 1; i <= n; i++) {
    int pt = binary(A[i]);
    vector<int> &v = vec[C[i]];
    int pos = lower_bound(v.begin(), v.end(), pt) - v.begin();
    if (pos == v.size()) v.push_back(++cur);
    pos = v[pos];
    modify(pos, 1);
    rk[i] = query(pos - 1) + 1;
    if (i - rk[i] > A[i]) rk[i] = i - A[i];
  }
  for (int i = 1; i <= n; i++) bit[i] = 0;
  for (int i = 1; i <= n; i++) modify(i, 1);
  for (int i = n; i >= 1; i--) {
    int pos = binary(i - rk[i]);
    ans[pos] = i;
    modify(pos, -1);
  }
  for (int i = 1; i <= n; i++) cout << ans[i] << " \n"[i == n];
  return 0;
}
