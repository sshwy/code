#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 2e5 + 5, LST = N << 2, INF = 0x3f3f3f3f;

int n, m, sum;
int mn[LST];
void assign(int p, int v, int u = 1, int l = 1, int r = n) {
  // printf("\033[31massign(%d,%d)\033[0m\n",p,v);
  if (l == r) return mn[u] = v, void();
  int mid = (l + r) >> 1;
  if (p <= mid)
    assign(p, v, u << 1, l, mid);
  else
    assign(p, v, u << 1 | 1, mid + 1, r);
  mn[u] = min(mn[u << 1], mn[u << 1 | 1]);
  // printf("l=%d,r=%d,mn[%d]=%d\n",l,r,u,mn[u]);
}
int qmin(int L, int R, int u = 1, int l = 1, int r = n) {
  if (R < l || r < L) return INF;
  if (L <= l && r <= R) return mn[u];
  int mid = (l + r) >> 1;
  return min(qmin(L, R, u << 1, l, mid), qmin(L, R, u << 1 | 1, mid + 1, r));
}

char s[N], t[N];
int a[N], nex[N];
vector<pii> v, p;
bool cmp(pii x, pii y) {
  return x.se != y.se ? x.se < y.se : x.fi > y.fi; //右端点升序，左端点降序
}

void getNext(char *s, int l) {
  memset(nex, 0, sizeof(int) * (l + 1));
  nex[0] = -1;
  int i = 0, j = -1;
  while (i < l) {
    if (j == -1 || s[i] == s[j])
      i++, j++, nex[i] = j;
    else
      j = nex[j];
  }
}

void kmp(char *s, int ls, char *t, int lt) {
  // printf("s=%s,t=%s\n",s,t);
  getNext(s, ls);
  int i = 0, j = 0;
  while (j < lt) {
    if (i == ls) {
      v.pb(mk(j - i + 1, j));
      // printf("[%d,%d]\n",j-i+1,j);
      i = nex[i];
    } else if (i == -1 || s[i] == t[j])
      i++, j++;
    else
      i = nex[i];
  }
  if (i == ls) {
    v.pb(mk(j - i + 1, j));
    // printf("[%d,%d]\n",j-i+1,j);
  }
}
int f[N];

int st[N][22];
void init() {
  FOR(i, 1, n) st[i][0] = a[i];
  FOR(j, 1, 21) {
    if ((1 << j) > n) break;
    int lim = n - (1 << j) + 1;
    FOR(i, 1, lim) {
      st[i][j] = min(st[i][j - 1], st[i + (1 << (j - 1))][j - 1]);
      // printf("st[%d,%d]=%d\n",i,j,st[i][j]);
    }
  }
}
int querymin(int l, int r) {
  int j = log(r - l + 1) / log(2);
  // printf("querymin(%d,%d)=%d\n",l,r,min(st[l][j],st[r-(1<<j)+1][j]));
  return min(st[l][j], st[r - (1 << j) + 1][j]);
}

int main() {
  scanf("%d%d", &n, &m);
  scanf("%s", s + 1);
  FOR(i, 1, n) scanf("%d", &a[i]);
  init();
  FOR(i, 1, m) {
    scanf("%s", t + 1);
    kmp(t + 1, strlen(t + 1), s + 1, n);
  }
  sort(v.begin(), v.end(), cmp);
  // printf("origin:");for(pii x:v){ printf("[%d,%d] ",x.fi,x.se); } puts("");
  int mx = 0;
  for (pii x : v) {
    if (x.fi > mx) { p.pb(x), mx = x.fi; }
  }
  // printf("rest:");for(pii x:p){ printf("[%d,%d] ",x.fi,x.se); } puts("");
  int pos = -1, l = 0, lp = p.size(); // p[pos]
  FOR(i, 1, n) {
    while (pos < lp - 1 && p[pos + 1].fi <= i) { ++pos; }
    while (l < lp && p[l].se < i) ++l;
    // printf("cross i(i=%d) : %d - %d\n",i,l,pos);
    //[l,pos]
    f[i] = 0x3f3f3f3f;
    if (l <= pos) {
      if (p[pos].fi < i) f[i] = f[i - 1];
      if (p[l].fi - 1 > 0) {
        // printf("\033[32mqmin(%d,%d)=%d\033[0m\n",p[l].fi-1,i-1,qmin(p[l].fi-1,i-1));
        f[i] = min(f[i], qmin(p[l].fi - 1, i - 1) + querymin(p[pos].fi, i));
      } else
        f[i] = querymin(p[pos].fi, i);
    } else {
      f[i] = f[i - 1];
    }
    // printf("f[%d]=%d\n",i,f[i]);
    assign(i, f[i]);
  }
  printf("%d\n", f[n]);
  return 0;
}
