#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 1e6 + 5;

struct qxx {
  int nex, t, v;
};
qxx e[N * 2];
int h[N], le = 1;
void add_path(int f, int t) { e[++le] = (qxx){h[f], t}, h[f] = le; }
#define FORe(i, u, v) for (int i = h[u], v; v = e[i].t, i; i = e[i].nex)

int n;
int phead[N], pend[N];

void dfs(int u, int p, int tag) { // tag=1:将原方案倒序输出
  // printf("dfs(%d,%d,%d)\n",u,p,tag);
  if (tag == 0) {
    FORe(i, u, v) {
      if (v == p) continue;
      dfs(v, u, 1);
      // ROF(i,ans[v].size()-1,0)ans[u].pb(ans[v][i]);
    }
    printf("%d ", u);
    // ans[u].pb(u);
  } else {
    printf("%d ", u);
    vector<int> son;
    FORe(i, u, v) if (v != p) son.pb(v);
    ROF(i, son.size() - 1, 0) { dfs(son[i], u, 0); }
  }
}
int main() {
  scanf("%d", &n);
  FOR(i, 1, n - 1) {
    int u, v;
    scanf("%d%d", &u, &v);
    add_path(u, v), add_path(v, u);
  }
  puts("Yes");
  dfs(1, 0, 0);
  // for(int i:ans[1])printf("%d ",i);
  return 0;
}
