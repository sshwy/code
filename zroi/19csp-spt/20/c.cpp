#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 1e5 + 5;

int n, k;
int a[N], b[N], rk[N];
int la;

struct fenwick {
  int c[N], lim;
  void reset(int _lim) {
    lim = _lim;
    FOR(i, 0, lim) c[i] = 0;
  }
  void assign_max(int pos, int val) {
    for (int i = pos; i <= lim; i += i & -i) c[i] = max(c[i], val);
  }
  int pre_max(int pos) {
    int res = 0;
    for (int i = pos; i > 0; i -= i & -i) res = max(res, c[i]);
    return res;
  }
} T;
bool check(int x) {
  T.reset(la);
  T.assign_max(1, 1); // b[1]
  assert(rk[1] == 1);
  int len = 1;
  FOR(i, 2, n) {
    int pos = upper_bound(a + 1, a + la + 1, x - b[i]) - a - 1;
    int cur = T.pre_max(pos) + 1;
    if (b[i] + b[1] <= x) len = max(len, cur);
    T.assign_max(rk[i], cur);
  }
  return len >= k;
}
int main() {
  scanf("%d%d", &n, &k);
  FOR(i, 1, n) scanf("%d", &a[i]);
  int pos = 1;
  FOR(i, 2, n) if (a[i] < a[pos]) pos = i;
  FOR(i, 1, n) {
    int x = i - pos + 1;
    if (x < 1) x += n;
    b[x] = a[i];
  }
  sort(a + 1, a + n + 1);
  la = unique(a + 1, a + n + 1) - a - 1;
  FOR(i, 1, n) rk[i] = lower_bound(a + 1, a + la + 1, b[i]) - a;
  // FOR(i,1,n)printf("%d%c",b[i]," \n"[i==n]);
  // FOR(i,1,n)printf("%d%c",rk[i]," \n"[i==n]);
  long long l = 0, r = 2e9 + 1, mid;
  while (l < r) mid = (l + r) >> 1, check(mid) ? r = mid : l = mid + 1;
  printf("%lld\n", l);
  return 0;
}
