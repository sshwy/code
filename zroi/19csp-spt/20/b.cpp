#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 1e5 + 5;

int n, c[N], p[N];
int dg[N];
bool use[1 << 20];

int main() {
  scanf("%d", &n);
  FOR(i, 1, n) scanf("%d", &c[i]);
  FOR(i, 1, n - 1) {
    int u, v;
    scanf("%d%d", &u, &v);
    dg[u] ^= 1, dg[v] ^= 1;
  }
  int tot = n, fin = 0;
  FOR(i, 1, n) {
    if (c[i] == 0) {
      if (dg[i] == 1) tot ^= i;
      p[i] = i, use[i] = 1;
    }
  }
  int pos = 1, totfin = 0;
  FOR(i, 1, n) if (!p[i] && dg[i] == 1)++ totfin;
  FOR(i, 1, n) {
    if (!p[i] && dg[i] == 1) { // has contribution
      while (use[pos]) ++pos;
      if (fin == totfin - 3) { // makesure tot>0
        while (use[pos] || (tot ^ pos) == 0) ++pos;
        p[i] = pos, use[pos] = 1;
      } else if (fin == totfin - 2) {
        if (!tot) return puts("No"), 0;
        p[i] = pos + (1 << 17), use[p[i]] = 1;
      } else if (fin == totfin - 1) {
        if (use[tot] || !tot) return puts("No"), 0;
        p[i] = tot, use[tot] = 1;
      } else {
        p[i] = pos, use[p[i]] = 1;
      }
      tot ^= p[i], ++fin;
    }
  }
  FOR(i, 1, n) {
    if (!p[i]) {
      if (dg[i] == 0) {
        while (use[pos]) ++pos;
        p[i] = pos, use[pos] = 1;
      }
    }
  }
  if (tot) return puts("No"), 0;
  assert(!tot);
  puts("Yes");
  FOR(i, 1, n) printf("%d%c", p[i], " \n"[i == n]);
  return 0;
}
