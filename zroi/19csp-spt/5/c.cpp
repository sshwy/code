#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
namespace IO {
  char nc() {
    static char bf[100000], *p1 = bf, *p2 = bf;
    return p1 == p2 && (p2 = (p1 = bf) + fread(bf, 1, 100000, stdin), p1 == p2)
               ? EOF
               : *p1++;
  }
  int rd() {
    int res = 0;
    char c = getchar();
    while (!isdigit(c)) c = getchar();
    while (isdigit(c)) res = res * 10 + c - '0', c = getchar();
    return res;
  }
  int tmp[20], lt;
  char OBF[100000], *OBP = OBF;
  void flush() { fwrite(OBF, 1, OBP - OBF, stdout), OBP = OBF; }
  void wrch(char x) {
    if (OBP == OBF + 99999) flush();
    *OBP = x, ++OBP;
  }
  void wr(int x) {
    lt = 0;
    while (x) tmp[++lt] = x % 10, x /= 10;
    while (lt > 0) wrch(tmp[lt] + '0'), --lt;
  }
  void wr(long long x) {
    lt = 0;
    while (x) tmp[++lt] = x % 10, x /= 10;
    while (lt > 0) wrch(tmp[lt] + '0'), --lt;
  }
  void wr(const char *s) {
    while (*s) wrch(*s), ++s;
  }
  void wr(char x) { wrch(x); }
} // namespace IO
#define int lld
const int SZ = 1 << 23, N = 1e5 + 5;

int n, nn, q;
int c[SZ], d[SZ], a[N];

void fwt_or(int *f, int len) {
  for (int j = 1; j < len; j <<= 1)
    for (int i = 0; i < len; i += j << 1)
      for (int k = i; k < i + j; k++) f[k + j] += f[k];
}
void ifwt_or(int *f, int len) {
  for (int j = 1; j < len; j <<= 1)
    for (int i = 0; i < len; i += j << 1)
      for (int k = i; k < i + j; k++) f[k + j] -= f[k];
}
void fwt_and(int *f, int len) {
  for (int j = 1; j < len; j <<= 1)
    for (int i = 0; i < len; i += j << 1)
      for (int k = i; k < i + j; k++) f[k] += f[k + j];
}
void ifwt_and(int *f, int len) {
  for (int j = 1; j < len; j <<= 1)
    for (int i = 0; i < len; i += j << 1)
      for (int k = i; k < i + j; k++) f[k] -= f[k + j];
}
void fwt_xor(int *f, int len) {
  for (int j = 1; j < len; j <<= 1)
    for (int i = 0; i < len; i += j << 1)
      for (int k = i, u, v; k < i + j; k++)
        u = f[k], v = f[k + j], f[k] = u + v, f[k + j] = u - v;
}
void ifwt_xor(int *f, int len) {
  for (int j = 1; j < len; j <<= 1)
    for (int i = 0; i < len; i += j << 1)
      for (int k = i, u, v; k < i + j; k++)
        u = f[k], v = f[k + j], f[k] = (u + v) / 2, f[k + j] = (u - v) / 2;
}
void S1() {
  FOR(i, 1, n) scanf("%lld", &a[i]);
  if (q == 1) {
    int cnt = 0, mx = 0;
    FOR(i, 1, n) FOR(j, 1, i - 1) {
      if ((a[i] & a[j]) > mx)
        mx = a[i] & a[j], cnt = 1;
      else if ((a[i] & a[j]) == mx)
        cnt++;
    }
    printf("%lld %lld", mx, cnt);
  } else if (q == 2) {
    int cnt = 0, mx = 0;
    FOR(i, 1, n) FOR(j, 1, i - 1) {
      if ((a[i] ^ a[j]) > mx)
        mx = a[i] ^ a[j], cnt = 1;
      else if ((a[i] ^ a[j]) == mx)
        cnt++;
    }
    printf("%lld %lld", mx, cnt);
  } else {
    int cnt = 0, mx = 0;
    FOR(i, 1, n) FOR(j, 1, i - 1) {
      if ((a[i] | a[j]) > mx)
        mx = a[i] | a[j], cnt = 1;
      else if ((a[i] | a[j]) == mx)
        cnt++;
    }
    printf("%lld %lld", mx, cnt);
  }
}
void S2() {
  FOR(i, 1, n) {
    int x;
    x = IO::rd(), nn = max(nn, x), c[x]++;
  }
  n = sizeof(int) * 8 - __builtin_clzll(nn), nn = 1 << n;
  FOR(i, 0, nn - 1) d[i] = c[i];
  if (q == 1) {
    fwt_and(d, nn);
    FOR(i, 0, nn - 1) d[i] *= d[i];
    ifwt_and(d, nn);
    ROF(i, nn - 1, 0)
    if (d[i] - c[i]) return printf("%lld %lld", i, (d[i] - c[i]) / 2), void();
  } else if (q == 2) {
    fwt_xor(d, nn);
    FOR(i, 0, nn - 1) d[i] *= d[i];
    ifwt_xor(d, nn);
    FOR(i, 0, nn - 1) {
      if (!c[i]) continue;
      d[0] -= c[i] * c[i], d[0] += c[i] * (c[i] - 1);
    }
    ROF(i, nn - 1, 0) if (d[i]) return printf("%lld %lld", i, d[i] / 2), void();
  } else {
    fwt_or(d, nn);
    FOR(i, 0, nn - 1) d[i] *= d[i];
    ifwt_or(d, nn);
    ROF(i, nn - 1, 0)
    if (d[i] - c[i]) return printf("%lld %lld", i, (d[i] - c[i]) / 2), void();
  }
}
signed main() {
  n = IO::rd(), q = IO::rd();
  // scanf("%lld%lld",&n,&q);
  if (n <= 2000)
    S1();
  else
    S2();
  return 0;
}
