#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
#define int lld
const int LIM = 1e17;

int q, m, k;
int a[100];
int mul3[500000];
int mul[61][20000];
int t[20000000], lt;

void go() {
  scanf("%lld%lld", &m, &k);

  FOR(i, 1, k) scanf("%lld", &a[i]);
  sort(a + 1, a + k + 1);
  FOR(i, 1, k) {
    FOR(j, 1, k) {
      if (i == j) continue;
      if (~a[i] && ~a[j] && a[i] % a[j] == 0) a[i] = 100;
    }
  }
  sort(a + 1, a + k + 1);
  while (a[k] == 100) --k;

  lt = 0;
  FOR(i, 1, k) {
    if (a[i] == 1) { FOR(j, 1, m) t[++lt] = j; }
    if (a[i] == 2) {
      FOR(j, 1, m) t[++lt] = j * j;
    } else if (a[i] == 3) {
      int lim = mul3[0];
      FOR(j, 1, lim) { t[++lt] = mul3[j]; }
    } else {
      int lim = mul[a[i]][0];
      FOR(j, 1, lim) { t[++lt] = mul[a[i]][j]; }
    }
    sort(t + 1, t + lt + 1);
    lt = unique(t + 1, t + lt + 1) - t - 1;
  }
  sort(t + 1, t + lt + 1);
  lt = unique(t + 1, t + lt + 1) - t - 1;
  assert(m <= lt);
  printf("%lld\n", t[m]);
}
int pw(int a, int m) {
  int res = 1;
  while (m) {
    if (m & 1) {
      if ((long double)res * a > LIM) return -1;
      res = res * a;
    }
    a = a * a, m >>= 1;
  }
  return res;
}
void init() {
  // mul3
  for (int &i = mul3[0] = 1, x; x = i * i * i, x <= LIM; i++) mul3[i] = x;
  --mul3[0];
  FOR(base, 4, 60) {
    int lim = base == 4 ? mul3[0] : mul[base - 1][0];
    for (int i = 1, x;
         x = base == 4 ? mul3[i] * i : mul[base - 1][i] * i, x <= LIM && i <= lim;
         i++)
      mul[base][i] = x, mul[base][0] = i;
  }
}
signed main() {
  init();
  scanf("%lld", &q);
  FOR(i, 1, q) go();
  return 0;
}
