#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 1e5 + 5;

int n, q;
int a[N];

int main() {
  scanf("%d%d", &n, &q);
  FOR(i, 1, n) scanf("%d", &a[i]);
  if (q == 1) {
    int cnt = 0, mx = 0;
    FOR(i, 1, n) FOR(j, 1, i - 1) {
      if ((a[i] & a[j]) > mx)
        mx = a[i] & a[j], cnt = 1;
      else if ((a[i] & a[j]) == mx)
        cnt++;
    }
    printf("%d %d\n", mx, cnt);
  } else if (q == 2) {
    int cnt = 0, mx = 0;
    FOR(i, 1, n) FOR(j, 1, i - 1) {
      if ((a[i] ^ a[j]) > mx)
        mx = a[i] ^ a[j], cnt = 1;
      else if ((a[i] ^ a[j]) == mx)
        cnt++;
    }
    printf("%d %d\n", mx, cnt);
  } else {
    int cnt = 0, mx = 0;
    FOR(i, 1, n) FOR(j, 1, i - 1) {
      if ((a[i] | a[j]) > mx)
        mx = a[i] | a[j], cnt = 1;
      else if ((a[i] | a[j]) == mx)
        cnt++;
    }
    printf("%d %d\n", mx, cnt);
  }
  return 0;
}
