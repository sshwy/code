#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
#define LL long long
const LL LIM = 1e17, K = 52, N = 100;
int q, m, k;
int a[K];

int g[K][N];
LL gcd(LL a, LL b) { return b ? gcd(b, a % b) : a; }
LL lcm(LL a, LL b) { return a / gcd(a, b) * b; }

void calc_g() {
  memset(g, 0, sizeof(g));
  FOR(i, 1, k) {
    FOR(j, 1, 60) {
      g[i][j] += g[i - 1][j];
      if (j == a[i]) ++g[i][j];
      int l = lcm(j, a[i]);
      if (l > 60) continue;
      g[i][l] -= g[i - 1][j];
    }
    // FOR(j,1,60)if(g[i][j])printf("g[%d,%d]=%d\n",i,j,g[i][j]);
  }
  // FOR(i,1,60)if(g[k][i])printf("g[%d]=%d\n",i,g[k][i]);
}
LL pw(LL a, LL m) {
  LL res = 1;
  while (m) {
    if (m & 1) {
      if (res > LIM / a) return LIM + 1;
      res = res * a;
    }
    if (m > 1 && a > LIM / a) return LIM + 1;
    a = a * a, m >>= 1;
  }
  return res;
}
LL calc(int n, LL x) {
  LL res = pow(x, 1.0 / n);
  while (pw(res, n) <= x) ++res;
  while (pw(res, n) > x) --res;
  // printf("calc(%d,%lld)=%lld\n",n,x,res);
  return res;
}
bool check(LL x) {
  LL res = 0;
  FOR(i, 1, 60) {
    if (g[k][i]) res = res + (calc(i, x) - 1) * g[k][i];
  }
  ++res;
  return res >= m;
}
void go() {
  scanf("%d%d", &m, &k);
  FOR(i, 1, k) scanf("%d", &a[i]);
  calc_g();
  LL l = 1, r = LIM;
  while (l < r) {
    LL mid = (l + r) >> 1;
    if (check(mid))
      r = mid;
    else
      l = mid + 1;
  }
  printf("%lld\n", l);
}
int main() {
  scanf("%d", &q);
  FOR(i, 1, q) go();
  return 0;
}
