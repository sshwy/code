#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 1e5 + 5, M = 2e5 + 5;

struct qxx {
  int nex, t;
};
qxx e[M * 2];
int h[N], le = 1;
void add_path(int f, int t) { e[++le] = (qxx){h[f], t}, h[f] = le; }

int n, m, ans;

int f[N];
void init(int k) { FOR(i, 0, k) f[i] = i; }
int get(int u) { return f[u] == u ? u : f[u] = get(f[u]); }
void merge(int u, int v) { f[get(u)] = get(v); }
bool find(int u, int v) { return get(u) == get(v); }

int main() {
  scanf("%d%d", &n, &m);
  init(n);
  FOR(i, 1, m) {
    int u, v;
    scanf("%d%d", &u, &v);
    if (find(u, v))
      ans++;
    else
      merge(u, v);
  }
  printf("%d\n", ans);
  return 0;
}
