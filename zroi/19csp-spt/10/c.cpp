#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 5e4 + 5, M = N * 2, INF = 0x3f3f3f3f;

int n, m;

struct qxx {
  int nex, t, v;
};
qxx e[M];
int h[N], le = 1;
int hh[N];
void add_path(int f, int t, int v) { e[++le] = (qxx){h[f], t, v}, h[f] = le; }
void add_flow(int f, int t, int v) { add_path(f, t, v), add_path(t, f, 0); }
#define FORe(i, u, v, w) \
  for (int i = h[u], v, w; v = e[i].t, w = e[i].v, i; i = e[i].nex)
#define FORflowe(i, u, v, w) \
  for (int &i = hh[u], v, w; v = e[i].t, w = e[i].v, i; i = e[i].nex)

int s, t;
int dep[N];
queue<int> q;

bool bfs() {
  memset(dep, 0, sizeof(dep));
  dep[s] = 1;
  q.push(s);
  while (!q.empty()) {
    int u = q.front();
    q.pop();
    FORe(i, u, v, w) if (!dep[v] && w) dep[v] = dep[u] + 1, q.push(v);
  }
  return dep[t] != 0;
}
int dfs(int u, int flow) {
  if (u == t || !flow) return flow;
  int rest = flow;
  FORflowe(i, u, v, w) {
    if (!w || dep[v] != dep[u] + 1) continue;
    int k = dfs(v, min(rest, w));
    e[i].v -= k, e[i ^ 1].v += k, rest -= k;
    if (!rest) break;
  }
  return flow - rest;
}
int dinic() {
  int maxflow = 0;
  while (bfs()) {
    memcpy(hh, h, sizeof(h));
    for (int x; (x = dfs(s, INF));) maxflow += x;
  }
  return maxflow;
}

char fet[500][500], dir[500][500];
int tr(int x, int y) { return x * m + y; }

void work(int a, int b) { // a 'L' b 'R'
  int ax = a / m, ay = a % m, bx = b / m, by = b % m;
  if (ay == 0) ay = m, ax--;
  if (by == 0) by = m, bx--;
  if (ax == bx && ay + 1 == by) {
    dir[ax][ay] = dir[bx][by] = 'U';
  } else if (ax == bx && ay - 1 == by) {
    dir[ax][ay] = dir[bx][by] = 'D';
  } else if (ax + 1 == bx && ay == by) {
    dir[ax][ay] = dir[bx][by] = 'R';
  } else if (ax - 1 == bx && ay == by) {
    dir[ax][ay] = dir[bx][by] = 'L';
  } else {
    printf("\033[41mError!\033[0m\n");
  }
}
int main() {
  scanf("%d%d", &n, &m);
  s = 0, t = tr(n, m) + 1;
  FOR(i, 1, n) scanf("%s", fet[i] + 1);
  FOR(i, 1, n) scanf("%s", dir[i] + 1);
  FOR(i, 1, n) FOR(j, 1, m) {
    if (fet[i][j] == 'L')
      add_flow(s, tr(i, j), 1);
    else
      add_flow(tr(i, j), t, 1);
  }
  FOR(i, 1, n) FOR(j, 1, m) {
    if (j < m && fet[i][j] != fet[i][j + 1])
      fet[i][j] == 'L' ? add_flow(tr(i, j), tr(i, j + 1), 1)
                       : add_flow(tr(i, j + 1), tr(i, j), 1);
    if (i < n && fet[i][j] != fet[i + 1][j])
      fet[i][j] == 'L' ? add_flow(tr(i, j), tr(i + 1, j), 1)
                       : add_flow(tr(i + 1, j), tr(i, j), 1);
  }
  int tot = dinic(), totdir = 0, totmtc = 0;
  if (tot * 2 < n * m) return printf("%d", tot), 0;
  int c[300];
  c['L'] = 0, c['U'] = 1, c['R'] = 2, c['D'] = 3;
  FOR(i, 1, n) FOR(j, 1, m) totdir += c[dir[i][j]];
  FOR(i, 1, n) FOR(j, 1, m) {
    if (fet[i][j] == 'L') {
      FORe(path, tr(i, j), v, w) {
        if (v == s) continue;
        if (w == 0) work(tr(i, j), v);
      }
    }
  }
  FOR(i, 1, n) FOR(j, 1, m) totmtc += c[dir[i][j]];
  totdir %= 4, totmtc %= 4;
  if (totdir == totmtc)
    return printf("%d", tot), 0;
  else
    return printf("%d", tot - 1), 0;
  return 0;
}
