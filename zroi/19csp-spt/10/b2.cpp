#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
#define int lld
const int N = 3e5 + 5;

int n;
int a[N], b[N], p[N];
int tot = 0x3f3f3f3f, ans[N];

signed main() {
  scanf("%lld", &n);
  FOR(i, 1, n) scanf("%lld%lld", &a[i], &b[i]);
  FOR(i, 1, n) p[i] = i;
  do {
    int x = 1, y = 1;
    int d = 0;
    FOR(i, 1, n) {
      d = max(d, 2 * (max(a[p[i]] - x, 0ll) + max(b[p[i]] - y, 0ll)));
      x = max(x, a[p[i]]), y = max(y, b[p[i]]);
    }
    if (d < tot) {
      // d=0;
      // x=1,y=1;
      // printf("(1,1) ");
      // FOR(i,1,n){
      //    d=max(d,2*(max(a[p[i]]-x,0ll)+max(b[p[i]]-y,0ll)));
      //    printf("-> (%lld,%lld) ",max(x,a[p[i]]),max(y,b[p[i]]));
      //    x=max(x,a[p[i]]),y=max(y,b[p[i]]);
      //}
      // puts("");
      tot = d;
      FOR(i, 1, n) ans[i] = p[i];
    } else if (d == tot) {
      // d=0;
      // x=1,y=1;
      // printf("(1,1) ");
      // FOR(i,1,n){
      //    d=max(d,2*(max(a[p[i]]-x,0ll)+max(b[p[i]]-y,0ll)));
      //    printf("-> (%lld,%lld) ",max(x,a[p[i]]),max(y,b[p[i]]));
      //    x=max(x,a[p[i]]),y=max(y,b[p[i]]);
      //}
      // puts("");
    }
  } while (next_permutation(p + 1, p + n + 1));
  FOR(i, 1, n) printf("%lld%c", ans[i], " \n"[i == n]);
  return 0;
}
