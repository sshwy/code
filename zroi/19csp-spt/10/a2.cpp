#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int K = 1e4, N = 1e6 + 5;
const double eps = 1e-10;

int n, m, k;
pii p[N];
int f[N];

void init(int lim) { FOR(i, 0, lim) f[i] = i; }
int get(int u) { return f[u] == u ? u : f[u] = get(f[u]); }
void merge(int u, int v) { f[get(u)] = get(v); }
bool find(int u, int v) { return get(u) == get(v); }

double sqr(int x) { return 1.0 * x * x; }
double dis(pii a, pii b) { return sqrt(sqr(a.fi - b.fi) + sqr(a.se - b.se)); }

int main() {
  scanf("%d%d%d", &n, &m, &k);
  FOR(i, 1, k) { scanf("%d%d", &p[i].fi, &p[i].se); }
  sort(p + 1, p + k + 1);
  double l = 0, r = 1e6;
  while (r - l > eps) {
    double mid = (l + r) / 2;
    init(k + 1);
    FOR(i, 1, k) {
      if (p[i].se < mid) merge(0, i);
      if (abs(p[i].se - m) < mid) merge(k + 1, i);
      if (i > 1 && dis(p[i], p[i - 1]) < mid) merge(i, i - 1);
    }
    if (find(k + 1, 0))
      r = mid;
    else {
      FOR(i, 1, k) {
        FOR(j, 1, i - 1) {
          if (dis(p[i], p[i - j]) < mid) merge(i, i - j);
          if (abs(p[i].fi - p[i - j].fi) > mid) break;
          if (find(k + 1, 0)) break;
        }
        if (find(k + 1, 0)) break;
      }
      if (find(k + 1, 0))
        r = mid;
      else
        l = mid;
    }
  }
  printf("%.8lf", l / 2);
  return 0;
}
