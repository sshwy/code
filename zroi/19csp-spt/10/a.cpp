#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int K = 1e4, N = 1e6 + 5;
const double eps = 1e-10;

int n, m, k;
pii p[N];
double d[N];
bool vis[N];

double sqr(int x) { return 1.0 * x * x; }
double dis(pii a, pii b) { return sqrt(sqr(a.fi - b.fi) + sqr(a.se - b.se)); }

int main() {
  scanf("%d%d%d", &n, &m, &k);
  FOR(i, 1, k) { scanf("%d%d", &p[i].fi, &p[i].se); }
  sort(p + 1, p + k + 1);
  FOR(i, 1, k) d[i] = 1.0 * p[i].se;
  d[k + 1] = m;
  double ans = 0;
  FOR(_, 1, k + 1) {
    int j = k + 1;
    FOR(i, 1, k) {
      if (vis[i]) continue;
      if (d[i] < d[j]) j = i;
    }
    ans = max(ans, d[j]);
    if (j == k + 1) {
      printf("%.8lf", ans / 2);
      return 0;
    }
    FOR(i, 1, k) {
      if (!vis[i]) d[i] = min(d[i], dis(p[i], p[j]));
    }
    vis[j] = 1;
    d[k + 1] = min(d[k + 1], 1.0 * m - p[j].se);
  }
  printf("%.8lf", ans / 2);
  return 0;
}
