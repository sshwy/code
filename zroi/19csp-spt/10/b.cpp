#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
#define int lld
const int N = 3e5 + 5;

int n;
struct data {
  int x, y, id;
  bool operator<(const data &d) const { return x != d.x ? x < d.x : y < d.y; }
  bool operator<<(const data &d) const { return x < d.x && y < d.y; }
  int come(const data &d) { return max(x - d.x, 0ll) + max(y - d.y, 0ll); }
  void print() { printf("(%lld,%lld,id:%lld)", x, y, id); }
  void getmax(data d) { x = max(x, d.x), y = max(y, d.y); }
} a[N];

int nex[N]; // nex[i]: a[i]'s next point that
            // a[i].x<a[nex[i]].x,a[i].y<a[nex[i]].y

bool vis[N];
int ans[N], la;
bool check(int lim) {
  data cur = (data){1, 1, 0};
  FOR(i, 1, n) vis[i] = 0;
  la = 0;
  FOR(_, 1, n) {
    int fl = 0;
    FOR(i, 1, n) {
      if (!vis[i]) {
        if (a[i].come(cur) <= lim) {
          fl = 1;
          ans[++la] = a[i].id;
          cur.getmax(a[i]);
          vis[i] = 1;
        }
      }
    }
    if (!fl) break;
  }
  if (la == n) return 1;
  return 0;
}
signed main() {
  scanf("%lld", &n);
  FOR(i, 1, n) scanf("%lld%lld", &a[i].x, &a[i].y), a[i].id = i;
  sort(a + 1, a + n + 1);
  int l = 0, r = 2e9 + 1;
  while (l < r) {
    int mid = (l + r) >> 1;
    if (check(mid))
      r = mid;
    else
      l = mid + 1;
  }
  check(l);
  // printf("max_delta = %lld\n",l);
  FOR(i, 1, n) printf("%lld%c", ans[i], " \n"[i == n]);
  return 0;
}
