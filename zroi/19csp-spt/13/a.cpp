#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
#define int lld
const int N = 1e5 + 5, P = 998244353;

int n;
int lc[N], rc[N], ans;

int x, y, s;

int a[N], b[N], c[N];
int la, lb;
void dfs1(int u) {
  if (!u) return;
  a[++la] = u;
  dfs1(lc[u]), dfs1(rc[u]);
}
void dfs2(int u) {
  if (!u) return;
  dfs2(lc[u]), dfs2(rc[u]);
  b[++lb] = u;
}
signed main() {
  scanf("%lld", &n);
  FOR(i, 1, n) scanf("%lld%lld", &lc[i], &rc[i]);
  dfs1(1);
  dfs2(1);
  assert(la == lb && la == n);
  // FOR(i,1,n)printf("%lld%c",a[i]," \n"[i==n]);
  // FOR(i,1,n)printf("%lld%c",b[i]," \n"[i==n]);
  FOR(i, 1, n) c[i] = -1;
  FOR(i, 1, n) {
    if (c[a[i]] == -1 && c[b[i]] == -1) {
      c[a[i]] = 1, c[b[i]] = 0;
    } else if (c[a[i]] == -1 && c[b[i]] != -1) {
      c[a[i]] = 1;
    } else if (c[a[i]] != -1 && c[b[i]] == -1) {
      c[b[i]] = 0;
    }
  }
  FOR(i, 1, n) x = (x * 2 + c[a[i]]) % P;
  FOR(i, 1, n) y = (y * 2 + c[b[i]]) % P;
  ans = (x - y) % P;
  ans = (ans + P) % P;
  printf("%lld\n", ans);
  return 0;
}
