#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 1e6 + 50, P = 998244353;

int n;
char s[N];
int g[N][4][14],
    h[N][4]; // g[i,j,k]表示第i个位置之后（严格）第j类宝石出现的第3^k个的位置

void make_g() {
  FOR(i, 0, 3) FOR(j, 0, 13) g[n + 1][i][j] = g[n][i][j] = n + 1;
  FOR(i, 0, 3) h[n][i] = h[n + 1][i] = n + 1;
  ROF(i, n - 1, 0) {
    FOR(j, 0, 3) {
      g[i][j][0] = s[i + 1] == j ? i + 1 : g[i + 1][j][0];
      FOR(k, 1, 13) {
        g[i][j][k] = g[g[g[i][j][k - 1]][j][k - 1]][j][k - 1];
        assert(g[i][j][k]);
      }
    }
    FOR(j, 0, 3) {
      h[i][j] = n + 1;
      FOR(k, 0, 3) {
        if (k == j) continue;
        h[i][j] = min(h[i][j], g[i][k][0]);
      }
    }
  }
}
int f[N][14][2], ans;
void dp() {
  FOR(j, 0, 3) FOR(k, 0, 13) {
    if (g[0][j][k] != n + 1) { f[g[0][j][k]][k][0] = 1; }
  }
  FOR(i, 1, n) {
    FOR(j, 1, 13) FOR(k, 0, 1) {
      f[i][j][k] = (0ll + f[i][j][k] + f[i][j - 1][k]) % P;
    }
    FOR(k, 0, 1) {
      FOR(d, 0, 3) {
        if (s[i] != d) {
          FOR(x, 0, 13) {
            // FOR(j,0,13)f[g[i][d][x]][x][0]=(f[g[i][d][x]][x][0]+f[i][j][k])%P;
            f[g[i][d][x]][x][0] = (0ll + f[g[i][d][x]][x][0] + f[i][13][k]) % P;
          }
        } else {
          FOR(x, 0, 13) {
            // FOR(j,x+1,13)f[g[i][d][x]][x][0]=(f[g[i][d][x]][x][0]+f[i][j][k])%P;
            f[g[i][d][x]][x][0] =
                (0ll + f[g[i][d][x]][x][0] + f[i][13][k] - f[i][x][k]) % P;
            if (k == 0)
              f[g[i][d][x]][x][1] = (0ll + f[g[i][d][x]][x][1] + f[i][x][k] -
                                        (x == 0 ? 0 : f[i][x - 1][k])) %
                                    P;
            // FOR(j,0,x-1)f[g[h[i][s[i]]][d][x]][x][0]=(f[g[h[i][s[i]]][d][x]][x][0]+f[i][j][k])%P;
            f[g[h[i][s[i]]][d][x]][x][0] =
                (0ll + f[g[h[i][s[i]]][d][x]][x][0] + (x == 0 ? 0 : f[i][x - 1][k])) %
                P;
          }
        }
      }
    }
  }
  FOR(i, 1, n) FOR(k, 0, 1) { ans = (ans + f[i][13][k]) % P; }
  ans = (ans + P) % P;
}
int main() {
  scanf("%d", &n);
  scanf("%s", s + 1);
  FOR(i, 1, n) s[i] -= 'A';
  make_g();
  dp();
  printf("%d\n", ans);
  return 0;
}
