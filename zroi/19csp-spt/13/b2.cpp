#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 2005, P = 998244353, SZ = 1 << 22;

int m;
char c[N];
char a[SZ];
int f[SZ], las[300];

int main() {
  scanf("%d", &m);
  scanf("%s", c);
  int len = strlen(c);
  ROF(i, len - 1, 0) {
    int step = len - i, x = 1 << (step - 1);
    int cnt = 1 << i;
    FOR(j, 0, cnt - 1) { a[x + (j << step)] = c[i]; }
  }
  // printf("%s\n",a+1);
  int la = (1 << m) - 1;
  f[0] = 1;
  FOR(i, 1, la) {
    if (!las[a[i]]) {
      // printf("f[%d]=f[%d]*2\n",i,i-1);
      f[i] = (f[i - 1] * 2ll) % P;
    } else {
      // printf("f[%d]=f[%d]*2-f[%d]\n",i,i-1,las[a[i]]-1);
      f[i] = (f[i - 1] * 2ll - f[las[a[i]] - 1]) % P;
    }
    las[a[i]] = i;
    // printf("f[%d]=%d\n",i,f[i]);
  }
  int ans = f[la] - 1;
  ans = (ans + P) % P;
  printf("%d\n", ans);
  return 0;
}
