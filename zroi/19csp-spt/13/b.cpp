#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 2005, P = 998244353, SZ = 1 << 22, MTX = 30;

int m, lc;
char c[N];
char a[SZ];
int f[SZ], las[300];

struct Matrix {
  int h, w;
  int c[MTX][MTX];
  Matrix() {}
  Matrix(int _h) {
    h = w = _h;
    FOR(i, 1, h) FOR(j, 1, w) c[i][j] = 0;
    FOR(i, 1, h) c[i][i] = 1;
  }
  Matrix(int _h, int _w) {
    h = w = _h;
    FOR(i, 1, h) FOR(j, 1, w) c[i][j] = 0;
  }
  Matrix operator*(const Matrix &m) {
    assert(w == m.h);
    Matrix res(h, m.w);
    FOR(i, 1, h) FOR(k, 1, w) FOR(j, 1, m.w) {
      res.c[i][j] = (res.c[i][j] + 1ll * c[i][k] * m.c[k][j]) % P;
    }
    return res;
  }
};

Matrix make(char x) {
  x -= 'a' - 1;
  Matrix res(27);
  FOR(i, 1, 27) res.c[x][i] = 1;
  return res;
}

Matrix getans(int pos) {
  if (pos == lc) return make(c[pos]);
  Matrix f = getans(pos + 1);
  Matrix t = f * make(c[pos]);
  FOR(i, 1, t.h) FOR(j, 1, t.w) {
    if (f.c[i][j])
      printf("%d%c", f.c[i][j], " \n"[j == t.w]);
    else
      printf(" %c", " \n"[j == t.w]);
  }
  FOR(i, 1, t.h) FOR(j, 1, t.w) {
    if (t.c[i][j])
      printf("%d%c", t.c[i][j], " \n"[j == t.w]);
    else
      printf(" %c", " \n"[j == t.w]);
  }
  return f * make(c[pos]) * f;
}
int main() {
  scanf("%d", &m);
  scanf("%s", c + 1);
  lc = strlen(c + 1);
  Matrix f = getans(1);
  int ans = 0;
  FOR(i, 1, 26) ans = (ans + f.c[i][27]) % P;
  printf("%d\n", ans);
  return 0;
}
