#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 1e5 + 5;

int n;
int lc[N], rc[N], ans = -0x3f3f3f3f;

int x, y, s;
void dfs1(int u) {
  if (!u) return;
  x = x << 1 | (s >> (u - 1) & 1);
  dfs1(lc[u]), dfs1(rc[u]);
}
void dfs2(int u) {
  if (!u) return;
  dfs2(lc[u]), dfs2(rc[u]);
  y = y << 1 | (s >> (u - 1) & 1);
}
void calc(int mask) {
  s = mask;
  x = 0;
  dfs1(1);

  y = 0;
  dfs2(1);
  ans = max(ans, x - y);
}
int main() {
  scanf("%d", &n);
  FOR(i, 1, n) scanf("%d%d", &lc[i], &rc[i]);
  int lim = 1 << n;
  FOR(i, 0, lim - 1) { calc(i); }
  printf("%d\n", ans);
  return 0;
}
