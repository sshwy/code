#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
using namespace RA;
const int N = 1e5 + 5;

int n;

vector<int> g[N];

int lc[N], rc[N];

int main() {
  // freopen("a.in","w",stdout);
  srand(clock());
  n = r(10, 20);
  printf("%d\n", n);
  FOR(i, 1, n) g[r(0, i - 1)].pb(i);
  FOR(u, 0, n) {
    int sz = g[u].size();
    if (!sz) continue;
    lc[u] = g[u][0];
    for (int i = 0; i < sz - 1; i++) rc[g[u][i]] = g[u][i + 1];
  }
  FOR(i, 1, n) printf("%d %d\n", lc[i], rc[i]);
  return 0;
}
