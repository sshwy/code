#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
#define int long long
const int N = 2000, INF = 0x3f3f3f3f3f3f3f3f;

int n, m, k;
int a[N];
int f[N][N];

int t[N], lt;
int w(int l, int r) {
  int res = 0;
  FOR(i, l, r) res += a[i];
  return res;
}
int ans = INF;
void go() {
  f[1][1] = 0; // a[1] -> 0
  FOR(i, 2, n) {
    int lim = min(i, k);
    f[i][1] = w(1, i);
    FOR(j, 2, lim) {
      f[i][j] = INF;
      FOR(k, j - 1, i - 1) { f[i][j] = min(f[i][j], max(f[k][j - 1], w(k + 1, i))); }
    }
  }
  FOR(i, 1, k) ans = min(ans, f[n][i]);
}
signed main() {
  scanf("%lld%lld%lld", &n, &m, &k);
  FOR(i, 1, n) scanf("%lld", &a[i]);
  go();
  FOR(i, 1, m) {
    FOR(j, 1, n) a[j] = (a[j] + 1) % m;
    go();
  }
  printf("%lld\n", ans);
  return 0;
}
