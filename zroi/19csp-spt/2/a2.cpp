#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 22, SZ = 1 << 21;

int nn, n;
int a[N];
double f[SZ]; // f[mask]:在mask的石子已经取走的情况下第0堆石子被取走的期望
bool vis[SZ];

double dp(int mask) {
  if (mask & 1) return 0;
  if (vis[mask]) return f[mask];
  double tot = 0;
  FOR(i, 0, n - 1)
  if (!(mask >> i & 1)) tot += a[i];
  FOR(i, 0, n - 1)
  if (!(mask >> i & 1)) { f[mask] += a[i] / tot * dp(mask + (1 << i)); }
  f[mask] += 1, vis[mask] = 1;
  return f[mask];
}
int main() {
  scanf("%d", &n);
  FOR(i, 0, n - 1) scanf("%d", &a[i]);
  nn = 1 << n;
  printf("%.10lf", dp(0));
  return 0;
}
