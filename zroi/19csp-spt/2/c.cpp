#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
#define int long long
int pw(int a, int m, int p) {
  a %= p;
  int res = 1;
  while (m) m & 1 ? res = res * a % p : 0, a = a * a % p, m >>= 1;
  return res;
}
const int P = 998244353;

namespace math {
  inline lld mul(lld a, lld b, lld p) {
    if (p <= 1000000000ll) return 1ll * a * b % p;
    if (p <= 1000000000000ll)
      return (((a * (b >> 20) % p) << 20) % p + a * (b & ((1 << 20) - 1))) % p;
    lld d = floor(a * (long double)b / p);
    lld res = (a * b - d * p) % p;
    if (res < 0) res += p;
    return res;
  }
  inline lld powmod(lld a, lld b, lld mod) {
    lld res = 1;
    for (; b; b >>= 1, a = mul(a, a, mod))
      if (b & 1) res = mul(res, a, mod);
    return res;
  }
  inline bool check(lld a, lld x, lld times, lld n) {
    lld tmp = powmod(a, x, n);
    while (times--) {
      lld last = mul(tmp, tmp, n);
      if (last == 1 && tmp != 1 && tmp != n - 1) return 1;
      tmp = last;
    }
    return tmp != 1;
  }
  int base[] = {2, 3, 5, 7, 11, 13, 17, 19, 23};
  const int S = 8;
  inline bool Miller(lld n) {
    FOR(i, 0, S) {
      if (n == base[i]) return 1;
      if (n % base[i] == 0) return 0;
    }
    lld x = n - 1, times = 0;
    while (!(x & 1)) times++, x >>= 1;
    FOR(_, 0, S) if (check(base[_], x, times, n)) return 0;
    return 1;
  }
#define mytz __builtin_ctzll
  inline lld gcd(lld a, lld b) {
    if (!a) return b;
    if (!b) return a;
    register int t = mytz(a | b);
    a >>= mytz(a);
    do {
      b >>= mytz(b);
      if (a > b) {
        lld t = b;
        b = a, a = t;
      }
      b -= a;
    } while (b);
    return a << t;
  }
#define F(x) ((mul(x, x, n) + c) % n)
  inline lld rho(lld n, lld c) {
    lld x = 1ll * rand() * rand() % n, y = F(x);
    while (x ^ y) {
      lld w = gcd(abs(x - y), n);
      if (w > 1 && w < n) return w;
      x = F(x), y = F(y), y = F(y);
    }
    return 1;
  }
  inline lld calc(lld x) {
    // printf("calc(%lld)\n",x);
    if (Miller(x)) return x;
    lld fsf = 0; // while((fsf=rho(x,rand()%x))==1);
    while ((fsf = rho(x, 2)) == 1)
      ;
    return max(calc(fsf), calc(x / fsf));
  }
} // namespace math

int x;

int ans;
struct divide {
  int p[50], c[50], lp;
  divide() { lp = 0; }
  void div(int x) {
    lp = 0;
    while (x > 1) {
      int cur = math::calc(x);
      p[++lp] = cur, c[lp] = 0;
      while (x % cur == 0) x /= cur, c[lp]++;
    }
  }
} x_div;
inline int h_pk(int k) {
  if (k == 0) return 1;
  if (k == 1) return -2;
  if (k == 2) return 1;
  return 0;
}
map<int, int> g, h;
void dfs_g(int k, int d, int cnt) {
  g[d] = pw(2, cnt, P) - 1;
  // printf("g[%lld]=%lld\n",d,cnt);
  FOR(i, k + 1, x_div.lp) {
    int pi = x_div.p[i], ci = x_div.c[i];
    int pj = 1;
    FOR(j, 1, ci) {
      pj *= pi;
      dfs_g(i, d * pj, cnt * (j + 1));
    }
  }
}
void dfs_h(int k, int d, int hd) {
  h[d] = hd;
  // printf("h[%lld]=%lld\n",d,hd);
  FOR(i, k + 1, x_div.lp) {
    int pi = x_div.p[i], ci = x_div.c[i];
    int pj = 1;
    FOR(j, 1, ci) {
      pj *= pi;
      dfs_h(i, d * pj, hd * h_pk(j) % P);
    }
  }
}
void dfs_calc(int k, int d) {
  ans = (ans + g[d] * h[x / d]) % P;
  FOR(i, k + 1, x_div.lp) {
    int pi = x_div.p[i], ci = x_div.c[i];
    int pj = 1;
    FOR(j, 1, ci) {
      pj *= pi;
      dfs_calc(i, d * pj);
    }
  }
}
signed main() {
  srand(clock());
  scanf("%lld", &x);
  x_div.div(x);
  dfs_g(0, 1, 1);
  dfs_h(0, 1, 1);
  dfs_calc(0, 1);
  ans = (ans + P) % P;
  printf("%lld\n", ans);
  return 0;
}
