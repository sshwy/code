#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 200005, INF = 0x3f3f3f3f;

int n, m, k;
int a[N], p[N], aa[N];

int ans = INF;
bool check(int lim) {
  int cur = 0, cnt = 0;
  FOR(i, 1, n) {
    if (a[i] > lim) return 0;
    if (cur + a[i] > lim) cur = 0, ++cnt;
    cur += a[i];
  }
  ++cnt;
  return cnt <= k;
}
void go() {
  int l = 0, r = m * n;
  while (l < r) {
    int mid = (l + r) >> 1;
    if (check(mid))
      r = mid;
    else
      l = mid + 1;
  }
  ans = min(ans, l);
}
signed main() {
  srand(clock());
  scanf("%d%d%d", &n, &m, &k);
  FOR(i, 1, n) scanf("%d", &aa[i]);
  FOR(i, 1, m) p[i] = i;
  random_shuffle(p + 1, p + m + 1);
  random_shuffle(p + 1, p + m + 1);
  FOR(j, 1, n) a[j] = aa[j];
  go();
  FOR(i, 1, m) {
    FOR(j, 1, n) a[j] = (aa[j] + p[i]) % m;
    if (check(ans - 1)) { go(); }
  }
  printf("%d\n", ans);
  return 0;
}
