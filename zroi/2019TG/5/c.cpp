#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
namespace IO {
  char nc() {
    static char bf[100000], *p1 = bf, *p2 = bf;
    return p1 == p2 && (p2 = (p1 = bf) + fread(bf, 1, 100000, stdin), p1 == p2)
               ? EOF
               : *p1++;
  }
  int rd() {
    int res = 0;
    char c = getchar();
    while (!isdigit(c)) c = getchar();
    while (isdigit(c)) res = res * 10 + c - '0', c = getchar();
    return res;
  }
} // namespace IO
/******************heading******************/
const int N = 1e7 + 5;

int ms, ks, m = 1, k = 1;
bool vis[N];

int pw(int a, int m, int p) {
  int res = 1;
  while (m) m & 1 ? res = 1ll * res * a % p : 0, a = 1ll * a * a % p, m >>= 1;
  return res;
}
bool check(int x) {
  int t = pw(x, k, m);
  return vis[t] == 0 ? vis[t] = 1, 1 : 0;
}
int ans;
int main() {
  scanf("%d", &ms);
  FOR(i, 1, ms) {
    int p, a;
    scanf("%d%d", &p, &a);
    FOR(i, 1, a) m *= p;
  }
  scanf("%d", &ks);
  FOR(i, 1, ks) {
    int p, a;
    scanf("%d%d", &p, &a);
    FOR(i, 1, a) k *= p;
  }
  FOR(i, 0, m - 1) ans += check(i);
  printf("%d\n", ans);
  return 0;
}
