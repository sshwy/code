#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 5e5 + 5;

int sg(int x) { return x & -x; }
int f(int x) {
  ROF(i, 25, 0) if (x >> (i + 1) & 1) x ^= 1 << i;
  return x;
}
int n, m, totsg;
char a[N];
int c[N]; // c[i]表示第i位为1的数的个数

int get_ans() {
  ROF(i, 25, 0) if (totsg >> i & 1) return c[i];
  return 0;
}
void insert(int x) {
  a[x] = 1;
  FOR(i, 0, 25) c[i] += x >> i & 1;
}
void remove(int x) {
  a[x] = 0;
  FOR(i, 0, 25) c[i] -= x >> i & 1;
}
int main() {
  scanf("%d", &n);
  scanf("%s", a + 1);
  reverse(a + 1, a + n + 1);
  FOR(i, 1, n) a[i] -= '0';

  FOR(i, 1, n) if (a[i] == 1) {
    totsg ^= f(sg(i));
    FOR(j, 0, 25) c[j] += i >> j & 1;
  }

  scanf("%d", &m);
  FOR(i, 1, m) {
    int x;
    scanf("%d", &x);
    x = n - x + 1;
    if (a[x])
      remove(x);
    else
      insert(x);
    totsg ^= f(sg(x));
    printf("%d\n", get_ans());
  }
  return 0;
}
