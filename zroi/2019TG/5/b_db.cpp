#include <algorithm>/*{{{*/
#include <cctype>
/*typedef bitset<5000> B;
int mex(B b){
    for(int i=0;i<5000;i++){
        if(b[i]==0)return i;
    }
    return 5000;
}
int sg[N],vis[N];
int calc(int x){
    if(vis[x])return sg[x];
    B b;
    b.reset();
    b[0]=1;
    int t=0;
    ROF(i,x-1,1){
        t^=calc(i);
        b[t]=1;
    }
    return vis[x]=1,sg[x]=mex(b);
}*/
#include <cassert>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
namespace IO {
  char nc() {
    static char bf[100000], *p1 = bf, *p2 = bf;
    return p1 == p2 && (p2 = (p1 = bf) + fread(bf, 1, 100000, stdin), p1 == p2)
               ? EOF
               : *p1++;
  }
  int rd() {
    int res = 0;
    char c = getchar();
    while (!isdigit(c)) c = getchar();
    while (isdigit(c)) res = res * 10 + c - '0', c = getchar();
    return res;
  }
} // namespace IO
#include <bitset>
/******************heading******************/
const int N = 5e5 + 5;

int n, m;
char s[N];

int sg(int x) { return x & -x; }
int f(int x) {
  ROF(i, 25, 0) if (x >> (i + 1) & 1) x ^= 1 << i;
  return x;
}
int F(int x) {
  if (x == 0) return 0;
  return F(x - 1) ^ f(sg(x));
}
int main() {
  FOR(i, 1, 1000) assert(i == F(i));
  FOR(i, 1, 1000) FOR(j, 1, 1000) assert(f(i ^ j) == (f(i) ^ f(j)));
  puts("Win!");
  FOR(i, 1, 10) {
    printf("i=%2d, ", i);
    FOR(j, 1, i) printf("%2d%c", i ^ (j - 1), " \n"[j == i]);
  }
  return 0;
}
