#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
namespace IO {
  char nc() {
    static char bf[100000], *p1 = bf, *p2 = bf;
    return p1 == p2 && (p2 = (p1 = bf) + fread(bf, 1, 100000, stdin), p1 == p2)
               ? EOF
               : *p1++;
  }
  int rd() {
    int res = 0;
    char c = getchar();
    while (!isdigit(c)) c = getchar();
    while (isdigit(c)) res = res * 10 + c - '0', c = getchar();
    return res;
  }
} // namespace IO
/******************heading******************/
const int N = 1e7 + 5;

int ms, ks, m = 1, k = 1;
bool vis[N];

int pw(int a, int m, int p) {
  int res = 1;
  while (m) m & 1 ? res = 1ll * res * a % p : 0, a = 1ll * a * a % p, m >>= 1;
  return res;
}
bool bp[N];
int pn[N], lp;
int f[N];
int ans;
void sieve(int lim) {
  bp[0] = bp[1] = 1;
  f[1] = 1;
  FOR(i, 2, lim) {
    if (!bp[i]) pn[++lp] = i, f[i] = pw(i, k, m);
    FOR(j, 1, lp) {
      int pj = pn[j];
      if (i * pj > lim) break;
      bp[i * pj] = 1;
      f[i * pj] = 1ll * f[i] * f[pj] % m;
      if (i % pj == 0) break;
    }
  }
  FOR(i, 1, lim) {
    if (!vis[f[i]]) ans++;
    vis[f[i]] = 1;
  }
}
int main() {
  scanf("%d", &ms);
  FOR(i, 1, ms) {
    int p, a;
    scanf("%d%d", &p, &a);
    FOR(i, 1, a) m *= p;
  }
  scanf("%d", &ks);
  FOR(i, 1, ks) {
    int p, a;
    scanf("%d%d", &p, &a);
    FOR(i, 1, a) k *= p;
  }
  sieve(m);
  printf("%d\n", ans);
  return 0;
}
