#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
namespace IO {
  char nc() {
    static char bf[100000], *p1 = bf, *p2 = bf;
    return p1 == p2 && (p2 = (p1 = bf) + fread(bf, 1, 100000, stdin), p1 == p2)
               ? EOF
               : *p1++;
  }
  int rd() {
    int res = 0;
    char c = getchar();
    while (!isdigit(c)) c = getchar();
    while (isdigit(c)) res = res * 10 + c - '0', c = getchar();
    return res;
  }
} // namespace IO
/******************heading******************/
#define int lld
const int N = 2e6 + 6, P = 1e9 + 7;

int n, m, c;
int a[N];
int nk[N];
int inv[N];
int f[N];

int pw(int a, int m, int p) {
  int res = 1;
  while (m) m & 1 ? res = 1ll * res * a % p : 0, a = 1ll * a * a % p, m >>= 1;
  return res;
}
int I(int a) { return pw(a, P - 2, P); }
lld ans;
signed main() {
  scanf("%lld%lld%lld", &n, &m, &c);
  FOR(i, 1, m) scanf("%lld", &a[i]);
  if (n < m) return printf("0"), 0;
  n -= m;
  inv[1] = 1;
  FOR(i, 2, m) inv[i] = 1ll * (P - (P / i)) * inv[P % i] % P;
  FOR(i, 2, m) assert(inv[i] == I(i));
  nk[0] = 1;
  FOR(i, 1, m) nk[i] = 1ll * nk[i - 1] * (n + i) % P * inv[i] % P;
  int p = (c - 1ll) * I(c) % P;
  int i1p = I(p - 1), pn1 = pw(p, n + 1, P);
  f[0] = (pn1 - 1ll) * i1p % P;
  FOR(i, 1, m) {
    f[i] = f[i - 1] - 1ll * nk[i] * pn1 % P;
    f[i] = 1ll * (P + f[i]) * (P - i1p) % P;
  }
  printf("%lld", 1ll * f[m - 1] * pw(c, n, P) % P);
  return 0;
}
