#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
namespace IO {
  char nc() {
    static char bf[100000], *p1 = bf, *p2 = bf;
    return p1 == p2 && (p2 = (p1 = bf) + fread(bf, 1, 100000, stdin), p1 == p2)
               ? EOF
               : *p1++;
  }
  int rd() {
    int res = 0;
    char c = getchar();
    while (!isdigit(c)) c = getchar();
    while (isdigit(c)) res = res * 10 + c - '0', c = getchar();
    return res;
  }
} // namespace IO
/******************heading******************/
const int N = 1e5 + 5, SZ = N << 2, INF = 0x3f3f3f3f;

struct sg {
  int mn[SZ], mx[SZ], n;
  void set(int p, int x) { set(p, x, 1, 1, n); }
  void set(int p, int x, int u, int l, int r) { /*{{{*/
    if (l == r) return mx[u] = mn[u] = x, void();
    int mid = (l + r) >> 1;
    if (p <= mid)
      set(p, x, u << 1, l, mid);
    else
      set(p, x, u << 1 | 1, mid + 1, r);
    mn[u] = min(mn[u << 1], mn[u << 1 | 1]);
    mx[u] = max(mx[u << 1], mx[u << 1 | 1]);
  } /*}}}*/
  int find_left(int L, int R, int v) { return find_left(L, R, v, 1, 1, n); }
  int find_left(int L, int R, int v, int u, int l, int r) { /*{{{*/
    if (R < l || r < L) return INF;
    if (l == r) return mn[u] <= v ? l : INF;
    int mid = (l + r) >> 1;
    if (L <= l && r <= R) {
      if (mn[u << 1] <= v)
        return find_left(L, R, v, u << 1, l, mid);
      else
        return find_left(L, R, v, u << 1 | 1, mid + 1, r);
    } else {
      int x = find_left(L, R, v, u << 1, l, mid);
      if (x != INF) return x;
      return find_left(L, R, v, u << 1 | 1, mid + 1, r);
    }
  } /*}}}*/
  int find_right(int L, int R, int v) { return find_right(L, R, v, 1, 1, n); }
  int find_right(int L, int R, int v, int u, int l, int r) { /*{{{*/
    if (R < l || r < L) return -INF;
    if (l == r) return mn[u] <= v ? l : -INF;
    int mid = (l + r) >> 1;
    if (L <= l && r <= R) {
      if (mn[u << 1 | 1] <= v)
        return find_right(L, R, v, u << 1 | 1, mid + 1, r);
      else
        return find_right(L, R, v, u << 1, l, mid);
    } else {
      int x = find_right(L, R, v, u << 1 | 1, mid + 1, r);
      if (x != -INF) return x;
      return find_right(L, R, v, u << 1, l, mid);
    }
  } /*}}}*/
  int find_max(int L, int R) { return find_max(L, R, 1, 1, n); }
  int find_max(int L, int R, int u, int l, int r) { /*{{{*/
    if (R < l || r < L) return -INF;
    if (L <= l && r <= R) return mx[u];
    int mid = (l + r) >> 1;
    return max(
        find_max(L, R, u << 1, l, mid), find_max(L, R, u << 1 | 1, mid + 1, r));
  } /*}}}*/
} R, C;

int n, m, Q;
int row[N], col[N];

int query(int a, int b, int c, int d, int v) {
  int a1 = a, b1 = b, c1 = c, d1 = d;
  if (a1 > c1) swap(a1, c1);
  if (b1 > d1) swap(b1, d1);
  // printf("query(%d,%d,%d,%d,%d)\n",a,b,c,d,v);
  if (a == c && b == d) return 0;
  if (row[a] <= v && col[d] <= v || row[c] <= v && col[b] <= v ||
      a == c && row[a] <= v || b == d && col[b] <= v)
    return abs(a - c) + abs(b - d);
  if (row[a] <= v && row[c] <= v) {
    if (C.find_left(b1, d1, v) != INF)
      return abs(a - c) + abs(b - d);
    else {
      int x = C.find_left(d1, m, v);
      int y = C.find_right(1, b1, v);
      if (x != INF && y != -INF) {
        return min(abs(b - x) + abs(a - c) + abs(x - d),
            abs(b - y) + abs(a - c) + abs(y - d));
      } else if (x != INF) {
        return abs(b - x) + abs(a - c) + abs(x - d);
      } else if (y != -INF) {
        return abs(b - y) + abs(a - c) + abs(y - d);
      }
    }
  }
  if (col[b] <= v && col[d] <= v) {
    if (R.find_left(a1, c1, v) != INF)
      return abs(a - c) + abs(b - d);
    else {
      int x = R.find_left(c1, n, v);
      int y = R.find_right(1, a1, v);
      if (x != INF && y != INF) {
        return min(abs(a - x) + abs(b - d) + abs(c - x),
            abs(a - y) + abs(b - d) + abs(c - y));
      } else if (x != INF) {
        return abs(a - x) + abs(b - d) + abs(c - x);
      } else if (y != -INF) {
        return abs(a - y) + abs(b - d) + abs(c - y);
      }
    }
  }
  if (R.find_max(a1, c1) <= v || C.find_max(b1, d1) <= v)
    return abs(b - d) + abs(a - c);
  return -1;
}
int main() {
  scanf("%d%d%d", &n, &m, &Q);
  R.n = n, C.n = m;
  FOR(i, 1, Q) {
    int op, x, a, b, c, d, v;
    scanf("%d", &op);
    if (op == 1) {
      scanf("%d", &x);
      row[x] = -i;
      R.set(x, -i);
      // printf("Row[%d]=%d\n",x,-i);
    } else if (op == 2) {
      scanf("%d", &x);
      col[x] = -i;
      C.set(x, -i);
      // printf("Col[%d]=%d\n",x,-i);
    } else {
      scanf("%d%d%d%d%d", &a, &b, &c, &d, &v);
      printf("%d\n", query(a, b, c, d, v - i));
    }
  }
  return 0;
}
