#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
namespace IO {
  char nc() {
    static char bf[100000], *p1 = bf, *p2 = bf;
    return p1 == p2 && (p2 = (p1 = bf) + fread(bf, 1, 100000, stdin), p1 == p2)
               ? EOF
               : *p1++;
  }
  int rd() {
    int res = 0;
    char c = getchar();
    while (!isdigit(c)) c = getchar();
    while (isdigit(c)) res = res * 10 + c - '0', c = getchar();
    return res;
  }
} // namespace IO
/******************heading******************/
const int N = 1e5 + 50;

int ls;
char s[N];
int a[N], c[N], la, b[N], lb;

int lastt, tot, ans;

void check(int x) { // x表示连续的一段0的长度，res表示段数
  int c0 = 0, res = 0;
  FOR(i, 1, ls) {
    if (s[i]) {
      if (c0 >= x) ++res, c0 = 0;
    } else {
      c0++;
    }
    // printf("s[%d]=%d,c0=%d\n",i,s[i],c0);
  }
  if (c0 >= x) ++res;
  // printf("res=%d,x=%d\n",res,x);
  if (res > 1) {
    int t = res * x + res - 1;
    // if(lastt>t)++tot;
    // if(t>=ans)printf("x=%d,res=%d,t=%d\n",x,res,t);
    ans = max(ans, t);
  }
}
int main() {
  scanf("%s", s + 1);
  ls = strlen(s + 1);
  // printf("ls=%d\n",ls);
  FOR(i, 1, ls) s[i] -= '0';
  FOR(i, 1, ls) if (s[i])++ ans;
  FOR(i, 1, ls) check(i);
  printf("%d", ans);
  // cout<<endl<<tot;
  return 0;
}
