#include <bits/stdc++.h>
using namespace std;
typedef long long lld;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
#define int lld
const int N = 2e5 + 50, M = 2e5 + 50;

int n, m, Q;

struct qxx {
  int nex, t, v;
};
qxx e[M];
int h[M], le = 0;
void add_path(int f, int t, int v) { e[++le] = (qxx){h[f], t, v}, h[f] = le; }

typedef vector<int> Vi;

Vi d[N];

void merge_to(int u, int v, int w) {
  Vi r, r2;
  for (int i = 0, j = 0; i < d[u].size() || j < d[v].size();) {
    if (i >= d[u].size())
      r.pb(d[v][j]), ++j;
    else if (j >= d[v].size())
      r.pb(d[u][i] + w), ++i;
    else if (d[u][i] + w == d[v][j])
      r.pb(d[u][i] + w), ++i, ++j;
    else if (d[u][i] + w < d[v][j])
      r.pb(d[u][i] + w), ++i;
    else
      r.pb(d[v][j]), ++j;
  }
  if (r.size()) r2.pb(r[0]);
  for (int i = 1; i < r.size() - 1; i++) {
    if (r2[r2.size() - 1] * 1.1 < r[i + 1]) r2.pb(r[i]);
  }
  if (r.size() > 1) r2.pb(r[r.size() - 1]);
  d[v] = r2;
}

signed main() {
  scanf("%lld%lld%lld", &n, &m, &Q);
  FOR(i, 1, m) {
    int x, y, w;
    scanf("%lld%lld%lld", &x, &y, &w);
    add_path(x, y, w);
  }
  d[1].pb(0);
  FOR(u, 1, n) {
    for (int i = h[u]; i; i = e[i].nex) {
      int v = e[i].t, w = e[i].v;
      merge_to(u, v, w);
    }
  }
  FOR(i, 1, Q) {
    int k, D;
    scanf("%lld%lld", &k, &D);
    Vi::iterator pos = lower_bound(d[k].begin(), d[k].end(), D);
    if (pos == d[k].end())
      puts("NO");
    else if (D * 1.1 < *pos)
      puts("NO");
    else
      puts("YES");
  }
  return 0;
}
