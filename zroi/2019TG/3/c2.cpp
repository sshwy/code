#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
namespace IO {
  char nc() {
    static char bf[100000], *p1 = bf, *p2 = bf;
    return p1 == p2 && (p2 = (p1 = bf) + fread(bf, 1, 100000, stdin), p1 == p2)
               ? EOF
               : *p1++;
  }
  int rd() {
    int res = 0;
    char c = getchar();
    while (!isdigit(c)) c = getchar();
    while (isdigit(c)) res = res * 10 + c - '0', c = getchar();
    return res;
  }
} // namespace IO
/******************heading******************/

int n, m, Q;

namespace S1 { /*{{{*/
  const int N = 3005;
  int A[N][N];
  int D[N][N], dt[N][N];
  queue<pii> q;
  int dir[4][2] = {0, 1, 0, -1, 1, 0, -1, 0};
  int totime;
  int bfs(int a, int b, int c, int d, int v) {
    ++totime;
    if (a == c && b == d) return 0;
    D[a][b] = 0;
    if (A[a][b] <= v) q.push(mk(a, b));
    while (!q.empty()) {
      pii u = q.front();
      q.pop();
      FOR(i, 0, 3) {
        int nx = u.fi + dir[i][0], ny = u.se + dir[i][1];
        if (nx < 1 || nx > n || ny < 1 || ny > m || A[nx][ny] > v) continue;
        if (dt[nx][ny] == totime) continue;
        D[nx][ny] = D[u.fi][u.se] + 1, dt[nx][ny] = totime;
        q.push(mk(nx, ny));
      }
    }
    if (dt[c][d] == totime)
      return D[c][d];
    else
      return -1;
  }
  void go() {
    FOR(i, 1, Q) {
      // FOR(i,1,n)FOR(j,1,m)printf("%3d%c",A[i][j]," \n"[j==m]);
      // puts("");
      int op, x, a, b, c, d, v;
      scanf("%d", &op);
      if (op == 1) {
        scanf("%d", &x);
        FOR(j, 1, m) A[x][j] = -i;
      } else if (op == 2) {
        scanf("%d", &x);
        FOR(j, 1, n) A[j][x] = -i;
      } else {
        scanf("%d%d%d%d%d", &a, &b, &c, &d, &v);
        printf("%d\n", bfs(a, b, c, d, v - i));
      }
    }
  }
} // namespace S1

namespace S2 { /*{{{*/
  namespace T {
    const int SZ = 1e6;
    int mx[SZ];
    void set(int p, int v, int u = 1, int l = 1, int r = n) {
      if (l == r) {
        // printf("\033[32mu=%d,l=%d,r=%d,mx=%d\033[0m\n",u,l,r,v);
        return mx[u] = v, void();
      }
      int mid = (l + r) >> 1;
      if (p <= mid)
        set(p, v, u << 1, l, mid);
      else
        set(p, v, u << 1 | 1, mid + 1, r);
      mx[u] = max(mx[u << 1], mx[u << 1 | 1]);
    }
    int query(int L, int R, int u = 1, int l = 1, int r = n) {
      if (R < l || r < L) return -0x3f3f3f3f;
      if (L <= l && r <= R) {
        // printf("\033[31mu=%d,l=%d,r=%d,mx=%d\033[0m\n",u,l,r,mx[u]);
        return mx[u];
      }
      int mid = (l + r) >> 1;
      return max(query(L, R, u << 1, l, mid), query(L, R, u << 1 | 1, mid + 1, r));
    }
  } // namespace T
  void go() {
    FOR(i, 1, Q) {
      int x, a, b, c, d, v, op;
      scanf("%d", &op);
      if (op == 1) {
        scanf("%d", &x);
        T::set(x, -i);
        // printf("a[%d]=%d\n",x,-i);
      } else {
        scanf("%d%d%d%d%d", &a, &b, &c, &d, &v);
        // printf("query(%d,%d),v=%d\n",a,c,v-i);
        int t;
        if (a == c && b == d)
          printf("0\n");
        else if (v - i < (t = T::query(min(a, c), max(a, c)))) {
          // printf("t=%d\n",t);
          printf("-1\n");
        } else {
          // printf("t=%d\n",t);
          printf("%d\n", abs(a - c) + abs(b - d));
        }
      }
    }
  }
} // namespace S2

int main() {
  scanf("%d%d%d", &n, &m, &Q);
  S2::go();
  // if(n<=3000&&m<=3000)S1::go();
  // else S2::go();
  return 0;
}
