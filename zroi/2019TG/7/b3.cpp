#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
#define int lld
const int N = 1e6, INF = 1e18;

int n, a, b, ans;

void dfs(int k, int x, int d, bool s, int prd, int sum) {
  if (prd >= n) {
    // printf("update ans with %lld (n=%lld)\n",prd,n);
    ans = min(ans, x * b + sum * a);
    return;
  }
  if (!k) return;
  dfs(k - 1, x, d, s, prd * d, sum + d - 1);
  if (!s) dfs(k - 1, x, d + 1, 1, prd * (d + 1), sum + d);
}
void go() {
  scanf("%lld%lld%lld", &n, &b, &a);
  if (n == 1) return puts("0"), void();
  ans = INF;
  FOR(x, 1, 30) { //强制游戏进行x轮
    if ((1 << x) >= n) {
      ans = min(ans, x * b + x * a);
      break;
    }
    int d = pow(n, 1.0 / x);
    dfs(x, x, d, 0, 1, 0);
  }
  printf("%lld\n", ans);
}
signed main() {
  int t;
  scanf("%lld", &t);
  FOR(i, 1, t) go();
  return 0;
}
