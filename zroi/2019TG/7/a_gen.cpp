#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/

using namespace RA;
const int N = 1e6 + 6;

pii a[N];
int main() {
  srand(clock());
  int n = 5;
  printf("%d\n", n);
  int L = r(1, 10 - 1), R = r(L + 1, 10);
  a[1] = mk(L + 1, R + 1);
  L = r(a[0].fi, 2 * 10 - 1), R = r(L + 1, 2 * 10);
  a[2] = mk(L + 1, R + 1);
  FOR(i, 3, n) {
    L = r(a[i - 2].se, i * 10 - 1), R = r(L + 1, i * 10);
    a[i] = mk(L + 1, R + 1);
  }
  FOR(i, 1, n) printf("%d %d\n", a[i].fi, a[i].se);
  return 0;
}
