#include <cstdio>
#include <cstdlib>
#include <ctime>
using namespace std;

int main() {
  system("g++ a.cpp -o .usr");
  system("g++ a2.cpp -o .std");
  system("g++ a_gen.cpp -o .gen");
  int t = 10000;
  while (t-- > 0) {
    system("./.gen > .fin");
    system("./.usr < .fin > .fout");
    system("./.std < .fin > .fstd");
    if (system("diff .fout .fstd")) break;
    printf("AC#%d\n", t);
  }
  printf("WA#%d\n", t);
  return 0;
}
