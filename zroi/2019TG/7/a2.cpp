#include <bits/stdc++.h>

#define pb push_back
#define mp make_pair
#define fi first
#define se second

using namespace std;

typedef long long ll;
typedef unsigned long long ull;
typedef pair<int, int> pii;
typedef pair<ll, ll> pll;

bool chkmax(int &x, int y) { return x < y ? x = y, true : false; }
bool chkmin(int &x, int y) { return x > y ? x = y, true : false; }

int readint() {
  int x = 0, f = 1;
  char ch = getchar();
  while (ch < '0' || ch > '9') {
    if (ch == '-') f = -1;
    ch = getchar();
  }
  while (ch >= '0' && ch <= '9') {
    x = x * 10 + ch - '0';
    ch = getchar();
  }
  return x * f;
}

const int cys = 998244353;
int n;
pii a[100005];

ll qpow(ll x, ll p) {
  ll ret = 1;
  for (; p; p >>= 1, x = x * x % cys)
    if (p & 1) ret = ret * x % cys;
  return ret;
}

int main() {
  n = readint();
  for (int i = 1; i <= n; i++) a[i].fi = readint(), a[i].se = readint() - 1;
  sort(a + 1, a + n + 1);
  int cnt = 0, mx = 0, tmp = 0;
  for (int i = 1; i <= n; i++) {
    if (a[i].fi <= mx) return printf("0\n"), 0;
    if (a[i].fi <= tmp)
      mx = min(tmp, a[i].se);
    else
      cnt++;
    chkmax(tmp, a[i].se);
  }
  printf("%d\n", qpow(2, cnt));
  return 0;
}
