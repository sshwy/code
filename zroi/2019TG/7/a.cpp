#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 1e6 + 6, P = 998244353;

int n;
pii a[N];

int main() {
  scanf("%d", &n);
  FOR(i, 1, n) scanf("%d%d", &a[i].fi, &a[i].se);
  sort(a + 1, a + n + 1);
  a[0] = mk(-1e9, -1e9);
  int mx1 = -1, mx2 = -1;
  FOR(i, 1, n) {
    if (a[i].fi >= max(mx1, mx2)) {
      mx1 > mx2 ? mx1 = a[i].se : mx2 = a[i].se;
    } else if (a[i].fi >= mx1) {
      mx1 = a[i].se;
    } else if (a[i].fi >= mx2) {
      mx2 = a[i].se;
    } else {
      return puts("0"), 0;
    }
  }
  int cnt = 0, res = 1, mx = a[0].se;
  FOR(i, 1, n) {
    if (mx <= a[i].fi) { ++cnt; }
    mx = max(mx, a[i].se);
  }
  FOR(i, 1, cnt) res = 1ll * res * 2 % P;
  printf("%d\n", res);
  return 0;
}
