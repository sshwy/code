#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
#define int lld
const int N = 1e5 + 5, INF = 1e18;

int n, m, q;
int a[N], b[N];
int pa[N], pb[N];

int query(int x1, int y1, int x2, int y2) {
  int res = INF;
  if (x1 > x2) swap(x1, x2), swap(y1, y2);
  // x1<=x2
  if (y1 <= y2) {
    int t = pa[x2] - pa[x1] + pb[y2] - pb[y1];
    int lim = min(x2 - x1, y2 - y1);
    FOR(i, 0, lim) {
      if (lim == x2 - x1) {
        int x = y2 - y1 - lim;
        res = min(res, t + min(a[x1 + i], a[x1 + i - 1]) * (x - (x & 1)));
      } else {
        int x = x2 - x1 - lim;
        res = min(res, t + min(b[y1 + i], b[y1 + i - 1]) * (x - (x & 1)));
      }
    }
  } else {
    int t = pa[x2] - pa[x1] + pb[y1] - pb[y2];
    int lim = min(x2 - x1, y1 - y2);
    FOR(i, 0, lim) {
      if (lim == x2 - x1) {
        int x = y1 - y2 - lim;
        res = min(res, t + min(a[x1 + i], a[x1 + i - 1]) * (x - (x & 1)));
      } else {
        int x = x2 - x1 - lim;
        res = min(res, t + min(b[y2 + i], b[y2 + i - 1]) * (x - (x & 1)));
      }
    }
  }
  return res;
}
signed main() {
  scanf("%lld%lld%lld", &n, &m, &q);
  FOR(i, 1, n - 1) scanf("%lld", &a[i]);
  a[0] = b[0] = INF;
  FOR(i, 1, m - 1) scanf("%lld", &b[i]);
  FOR(i, 1, n - 1) pa[i] = pa[i - 1] + a[i - 1];
  FOR(i, 1, m - 1) pb[i] = pb[i - 1] + b[i - 1];
  FOR(i, 1, q) {
    int x1, x2, y1, y2;
    scanf("%lld%lld%lld%lld", &x1, &y1, &x2, &y2);
    printf("%lld\n", query(x1, y1, x2, y2));
  }
  return 0;
}
