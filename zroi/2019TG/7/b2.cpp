#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
#define int lld
const int N = 1e6, INF = 0x3f3f3f3f3f3f3f3f;

int n, a, b;
int f[N]; // f[i]表示i个人玩游戏的最小时间
int vis[N];

int F(int x) {
  if (x <= 1) return 0;
  if (vis[x]) return f[x];
  f[x] = INF;
  FOR(k, 2, n) { f[x] = min(f[x], F(!!(x % k) + x / k) + (k - 1) * a + b); }
  // printf("F(%lld)=%lld\n",x,f[x]);
  return vis[x] = 1, f[x];
}
void go() {
  scanf("%lld%lld%lld", &n, &b, &a);
  memset(vis, 0, sizeof(int) * (n + 2));
  printf("%lld\n", F(n));
}
signed main() {
  int t;
  scanf("%lld", &t);
  FOR(i, 1, t) go();
  return 0;
}
