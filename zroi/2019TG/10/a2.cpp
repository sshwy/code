#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
#define int lld
const int N = 2e5 + 5, INF = 0x3f3f3f3f3f3f3f3f;

int n, q;
int a[N], f[N];

void work() {
  f[1] = 0;
  FOR(i, 2, n) {
    f[i] = 0;
    ROF(j, i - 1, 1) { f[i] = max(f[i], f[j] + max(a[i], a[j])); }
  }
  printf("%lld\n", f[n]);
}
signed main() {
  scanf("%lld%lld", &n, &q);
  FOR(i, 1, n) scanf("%lld", &a[i]);
  work();
  FOR(i, 1, q) {
    int x, y;
    scanf("%lld%lld", &x, &y);
    a[x] = y;
    work();
  }
  return 0;
}
