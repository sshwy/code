#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 2e5 + 5, M = N * 2, P = 1e9 + 7;

struct qxx {
  int nex, t, v;
};
qxx e[M];
int h[N], le = 1;
void add_path(int f, int t, int v) { e[++le] = (qxx){h[f], t, v}, h[f] = le; }
#define FORew(i, u, v, w) \
  for (int i = h[u], v, w; v = e[i].t, w = e[i].v, i; i = e[i].nex)

int n;
int dep[N], fa[N][21], fw[N][21];
int dfn[N], totdfn;
int a[N];

void dfs(int u, int p = 0, int val = 0) {
  dfn[u] = ++totdfn;
  dep[u] = dep[p] + 1;
  fa[u][0] = p, fw[u][0] = val;
  FOR(j, 1, 20) {
    fa[u][j] = fa[fa[u][j - 1]][j - 1];
    fw[u][j] = max(fw[u][j - 1], fw[fa[u][j - 1]][j - 1]);
    if (!fa[u][j]) break;
  }
  FORew(i, u, v, w) {
    if (v == p) continue;
    dfs(v, u, w);
  }
}
int lca(int x, int y) {
  if (x == y) return x;
  if (dep[x] < dep[y]) swap(x, y);
  ROF(j, 20, 0) if (dep[x] - (1 << j) >= dep[y]) x = fa[x][j];
  if (x == y) return x;
  ROF(j, 20, 0) if (fa[x][j] != fa[y][j]) x = fa[x][j], y = fa[y][j];
  assert(fa[x][0] == fa[y][0]);
  return fa[x][0];
}
int calc(int x, int y) {
  assert(x != y);
  int res = 0;
  if (dep[x] < dep[y]) swap(x, y);
  ROF(j, 20, 0)
  if (dep[x] - (1 << j) >= dep[y]) res = max(res, fw[x][j]), x = fa[x][j];
  if (x == y) return res;
  ROF(j, 20, 0)
  if (fa[x][j] != fa[y][j])
    res = max(res, fw[x][j]), res = max(res, fw[y][j]), x = fa[x][j], y = fa[y][j];
  res = max(res, fw[x][0]);
  res = max(res, fw[y][0]);
  return res;
}
bool cmp(const int &x, const int &y) { return dfn[x] < dfn[y]; }

struct edge {
  int u, v, w;
};
edge ve[M];
bool cmpv(const edge &x, const edge &y) { return x.w < y.w; }
int s[N], tp, lv;

int f[N], g[N], t[N];

int get(int u) { return f[u] == u ? u : f[u] = get(f[u]); }

void merge(edge x) {
  int u = x.u, v = x.v, w = x.w;
  u = get(u), v = get(v);
  // printf("merge(%d,%d,%d)\n",u,v,w);
  assert(u != v);
  if (g[u] > g[v]) swap(u, v);
  if (g[u] == -1) {
    if (g[v] == -1) {
      f[u] = v;
    } else if (g[v] == 0) {
      f[u] = v, g[v] = -1;
    } else {
      t[g[v]] = w;
      // printf("(%d)\n",g[v]);
      f[u] = v;
      g[v] = -1;
    }
  } else if (g[u] == 0) {
    if (g[v] == 0) {
      f[u] = v;
    } else {
      f[u] = v;
    }
  } else {
    assert(g[u] != g[v]);
    // printf("%d,%d\n",g[u],g[v]);
    t[g[u]] = t[g[v]] = w;
    f[u] = v, g[v] = -1;
  }
}

void go() {
  // printf("go\n");
  lv = 0, tp = 0;
  int k;
  scanf("%d", &k);
  FOR(i, 1, k) scanf("%d", &a[i]);
  sort(a + 1, a + k + 1, cmp); //按dfn从小到达排序
  int root = a[1];             //虚树的根
  FOR(i, 2, k) root = lca(root, a[i]);
  s[++tp] = root;
  // printf("root=%d\n",root);
  FOR(i, 1, k) {
    // printf("i=%d\n",i);
    // FOR(i,1,tp)printf("%d%c",s[i]," \n"[i==tp]);
    int z = lca(s[tp], a[i]);
    while (tp >= 2) {
      if (dep[z] <= dep[s[tp - 1]]) {
        ve[++lv] = (edge){s[tp - 1], s[tp], calc(s[tp - 1], s[tp])}; //连边
        --tp;
      } else
        break;
    }
    assert(tp);
    if (dep[s[tp]] < dep[z]) {
      s[++tp] = z;
      s[++tp] = a[i];
    } else if (dep[s[tp]] == dep[z]) {
      assert(s[tp] == z);
      if (s[tp] != a[i]) s[++tp] = a[i];
    } else {
      ve[++lv] = (edge){s[tp], z, calc(s[tp], z)}; //连边
      --tp;
      s[++tp] = z;
      s[++tp] = a[i];
    }
  }
  while (tp >= 2) {
    ve[++lv] = (edge){s[tp - 1], s[tp], calc(s[tp - 1], s[tp])}; //连边
    --tp;
  }
  sort(ve + 1, ve + lv + 1, cmpv);
  // FOR(i,1,lv)printf("(%d,%d,%d)\n",ve[i].u,ve[i].v,ve[i].w);
  FOR(i, 1, lv) {
    f[ve[i].u] = ve[i].u;
    g[ve[i].u] = 0; // 0:没有关键点；x：有唯一的关键点，是x；-1：有多个关键点
    f[ve[i].v] = ve[i].v;
    g[ve[i].v] = 0;
  }
  FOR(i, 1, k) g[a[i]] = a[i];
  FOR(i, 1, k) t[a[i]] = n;
  FOR(i, 1, lv) { merge(ve[i]); }
  int ans = 1;
  // FOR(i,1,k)printf("t[%d]=%d\n",a[i],t[a[i]]);
  FOR(i, 1, k) ans = 1ll * ans * t[a[i]] % P;
  printf("%d\n", ans);
}
int main() {
  freopen("b.out", "w", stdout);
  scanf("%d", &n);
  FOR(i, 1, n - 1) {
    int u, v;
    scanf("%d%d", &u, &v);
    add_path(u, v, i);
    add_path(v, u, i);
  }
  dfs(1);
  int q;
  scanf("%d", &q);
  FOR(i, 1, q) go();
  return 0;
}
