#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 2e5 + 5, M = N * 2, P = 1e9 + 7;

struct qxx {
  int nex, t, v;
};
qxx e[M];
int h[N], le = 1;
void add_path(int f, int t, int v) { e[++le] = (qxx){h[f], t, v}, h[f] = le; }
#define FORew(i, u, v, w) \
  for (int i = h[u], v, w; v = e[i].t, w = e[i].v, i; i = e[i].nex)

int n;
int dep[N], fa[N], fw[N];
int a[N];

void dfs(int u, int p = 0) {
  dep[u] = dep[p] + 1;
  FORew(i, u, v, w) {
    if (v == p) continue;
    fa[v] = u, fw[v] = w;
    dfs(v, u);
  }
}
int calc(int x, int y) {
  assert(x != y);
  int res = 0;
  if (dep[x] < dep[y]) swap(x, y);
  while (dep[x] > dep[y]) res = max(res, fw[x]), x = fa[x];
  while (x != y) res = max(res, fw[x]), res = max(res, fw[y]), x = fa[x], y = fa[y];
  return res;
}
int main() {
  scanf("%d", &n);
  FOR(i, 1, n - 1) {
    int u, v;
    scanf("%d%d", &u, &v);
    add_path(u, v, i);
    add_path(v, u, i);
  }
  dfs(1);
  int q;
  scanf("%d", &q);
  FOR(i, 1, q) {
    int k;
    scanf("%d", &k);
    FOR(i, 1, k) scanf("%d", &a[i]);
    int ans = 1;
    FOR(i, 1, k) {
      int mx = n;
      FOR(j, 1, k) {
        if (j == i) continue;
        mx = min(mx, calc(a[i], a[j]));
      }
      ans = 1ll * ans * mx % P;
    }
    printf("%d\n", ans);
  }
  return 0;
}
