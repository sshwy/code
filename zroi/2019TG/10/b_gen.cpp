#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 2e5 + 5;

using namespace RA;

vector<pii> edge;
int p[N];
int s[N];
int pos[N];
int main() {
  freopen("b.in", "w", stdout);
  srand(clock());
  int n = 2e5, q = 500, sumk = 2e5;
  printf("%d\n", n);
  FOR(i, 2, n) edge.pb(mk(r(1, i - 1), i));
  random_shuffle(edge.begin(), edge.end());
  for (pii x : edge) printf("%d %d\n", x.fi, x.se);
  printf("%d\n", q);

  FOR(i, 1, sumk - 1) s[i] = i;
  random_shuffle(s + 1, s + sumk);
  sort(s + 1, s + q); // s[1]..s[q-1];
  s[0] = 0, s[q] = sumk;

  FOR(i, 1, n) p[i] = i;
  FOR(i, 1, q) {
    int k = s[i] - s[i - 1];
    printf("%d ", k);
    random_shuffle(p + 1, p + n + 1);
    FOR(i, 1, k) printf("%d%c", p[i], " \n"[i == k]);
  }
  return 0;
}
