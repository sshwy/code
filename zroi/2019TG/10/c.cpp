#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
#define int lld
const int N = 1e5 + 5;

int n, m;
int p[N][10], k[N];
int f[N], g[N]; // f[i,j]:前i门，总价格为j的最大收益

signed main() {
  scanf("%lld", &n);
  FOR(i, 1, n) {
    scanf("%lld", &k[i]);
    FOR(j, 1, k[i]) scanf("%lld", &p[i][j]);
    m += k[i];
  }
  memset(f, -0x3f, sizeof(f));
  memset(g, -0x3f, sizeof(g));
  f[0] = 0;
  FOR(i, 1, n) {
    FOR(j, 1, m) {
      int lim = min(j, k[i]);
      FOR(x, 1, lim) { g[j] = max(g[j], f[j - x] + p[i][x]); }
    }
    memcpy(f, g, sizeof(g));
    memset(g, -0x3f, sizeof(g));
  }
  FOR(i, n, m) printf("%lld%c", f[i], " \n"[i == m]);
  return 0;
}
