#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
#define int lld
const int N = 2e5 + 5, INF = 0x3f3f3f3f3f3f3f3f;

int n, q;
int a[N];

int ansA;

void Ainsert(int pos) {
  if (a[pos] <= 0) return; // no contribution
  if (pos > 1) {
    if (a[pos - 1] <= 0)
      ansA += a[pos];
    else
      ansA += max(a[pos], a[pos - 1]), ansA -= a[pos - 1];
  }
  if (pos < n) {
    if (a[pos + 1] <= 0)
      ansA += a[pos];
    else
      ansA += max(a[pos], a[pos + 1]), ansA -= a[pos + 1];
  }
}
void Aerase(int pos) {
  if (a[pos] <= 0) return; // no effect
  if (pos > 1) {
    if (a[pos - 1] <= 0)
      ansA -= a[pos];
    else
      ansA -= max(a[pos], a[pos - 1]), ansA += a[pos - 1];
  }
  if (pos < n) {
    if (a[pos + 1] <= 0)
      ansA -= a[pos];
    else
      ansA -= max(a[pos], a[pos + 1]), ansA += a[pos + 1];
  }
}
void update(int pos, int val) {
  Aerase(pos);
  a[pos] = val;
  Ainsert(pos);
}

signed main() {
  scanf("%lld%lld", &n, &q);
  FOR(i, 1, n) a[i] = -1;
  FOR(i, 1, n) {
    int x;
    scanf("%lld", &x);
    update(i, x);
  }
  printf("%lld\n", ansA);
  FOR(i, 1, q) {
    int x, y;
    scanf("%lld%lld", &x, &y);
    update(x, y);
    printf("%lld\n", ansA);
  }
  return 0;
}
