#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/

typedef pair<pii, pii> ppp;

map<pii, bool> point;
map<ppp, bool> edge;
char s[500005];
int tote, totp;

int main() {
  scanf("%s", s + 1);
  int x = 0, y = 0;
  point[mk(0, 0)] = 1, ++totp;
  for (int i = 1; s[i]; i++) {
    int nx = x, ny = y;
    if (s[i] == 'L') {
      ny++;
    } else if (s[i] == 'R') {
      ny--;
    } else if (s[i] == 'U') {
      nx++;
    } else {
      nx--;
    }
    if (!point.count(mk(nx, ny))) ++totp;
    point[mk(nx, ny)] = 1;
    if (mk(x, y) < mk(nx, ny)) {
      if (!edge.count(mk(mk(x, y), mk(nx, ny)))) ++tote;
      edge[mk(mk(x, y), mk(nx, ny))] = 1;
    } else {
      if (!edge.count(mk(mk(nx, ny), mk(x, y)))) ++tote;
      edge[mk(mk(nx, ny), mk(x, y))] = 1;
    }
    x = nx, y = ny;
  }
  // printf("%d %d\n",tote,totp);
  printf("%d", tote + 2 - totp);
  return 0;
}
