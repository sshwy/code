#include <bits/stdc++.h>
using namespace std;
typedef pair<int, int> pii;
#define X first
#define Y second
#define pb push_back
#define M make_pair
#define FOR(i, a, b) for (int i = (a); i <= (int)b; ++i)
#define ROF(i, a, b) for (int i = (a); i >= (int)b; --i)
const int N = 1005;

int n, a[N][N], lp, ans[N][N];
vector<pii> p;

int get_type(pii x, pii y) {
  if (x.X - y.X == x.Y - y.Y)
    return 0;
  else {
    int len = min(y.X - x.X, y.Y - x.Y);
    if (len & 1)
      return 1;
    else
      return 2;
  }
}
pair<pii, pii> cross_point(pii x, pii y) {
  if (x.X - y.X == x.Y - y.Y)
    return M(M(y.X, x.Y), M(x.X, y.Y));
  else {
    int len = min(y.X - x.X, y.Y - x.Y);
    if (len & 1) {
      pii mid = M(x.X + y.X, x.Y + y.Y);
      return M(M((mid.X + len) >> 1, (mid.Y - len) >> 1),
          M((mid.X - len) >> 1, (mid.Y + len) >> 1));
    } else {
      len >>= 1;
      pii mid = M((x.X + y.X) >> 1, (x.Y + y.Y) >> 1);
      return M(M(mid.X + len, mid.Y - len), M(mid.X - len, mid.Y + len));
    }
  }
}
pii hori_mirror(pii x) { return M(x.X, -x.Y); }
int hori[N][N], vert[N][N], linj[N][N], linl[N][N], rang[N][N];
void rang_add(int x, int y, int a, int b) {
  rang[x][y]++, rang[x][b + 1]--, rang[a + 1][y]--, rang[a + 1][b + 1]++;
}

int main() {
  scanf("%d", &n);
  if (n == 1) return puts("N"), 0;
  FOR(i, 1, n)
  FOR(j, 1, n) scanf("%d", &a[i][j]), a[i][j] ? p.pb(M(i, j)), 0 : 0;
  if ((int)p.size() >= 2 * n - 1) {
    FOR(i, 1, n) FOR(j, 1, n) printf("%c%c", 'Y', " \n"[j == n]);
    return 0;
  }
  FOR(i, 0, p.size() - 1) FOR(j, i + 1, p.size() - 1) {
    pii x = p[i], y = p[j];
    if (x == y || ((x.X + y.X + x.Y + y.Y) & 1)) continue;
    if (x.X > y.X) swap(x, y);
    int tag = 0, typ;
    if (!(x.X <= y.X && x.Y <= y.Y)) tag = 1;
    if (tag) x = hori_mirror(x), y = hori_mirror(y);

    typ = get_type(x, y);
    pair<pii, pii> res = cross_point(x, y);
    pii C = res.X, D = res.Y;

    if (tag == 0) {
      if (typ == 0)
        linj[C.X - 1][C.Y + 1]++, linj[D.X][D.Y]--, rang_add(C.X, 1, n, C.Y),
            rang_add(1, D.Y, D.X, n);
      else {
        if (x.Y == y.Y)
          hori[(x.X + y.X) >> 1][1]++;
        else if (x.X == y.X)
          vert[1][(x.Y + y.Y) >> 1]++;
        else {
          linj[C.X - 1][C.Y + 1]++, linj[D.X][D.Y]--;
          if (abs(x.X - y.X) > abs(x.Y - y.Y))
            hori[C.X][1]++, hori[C.X][C.Y + 1]--, hori[D.X][D.Y]++;
          else
            vert[C.X][C.Y]++, vert[1][D.Y]++, vert[D.X + 1][D.Y]--;
        }
      }
    } else {
      x = hori_mirror(x), y = hori_mirror(y);
      C = hori_mirror(C), D = hori_mirror(D);
      if (typ == 0)
        linl[D.X + 1][D.Y + 1]++, linl[C.X][C.Y]--, rang_add(1, 1, D.X, D.Y),
            rang_add(C.X, C.Y, n, n);
      else {
        if (x.Y == y.Y)
          hori[(x.X + y.X) >> 1][1]++;
        else if (x.X == y.X)
          vert[1][(x.Y + y.Y) >> 1]++;
        else {
          linl[D.X + 1][D.Y + 1]++, linl[C.X][C.Y]--;
          if (abs(x.X - y.X) > abs(x.Y - y.Y))
            hori[D.X][1]++, hori[D.X][D.Y + 1]--, hori[C.X][C.Y]++;
          else
            vert[1][D.Y]++, vert[D.X + 1][D.Y]--, vert[C.X][C.Y]++;
        }
      }
    }
  }
  FOR(i, 1, n) FOR(j, 1, n) hori[i][j] += hori[i][j - 1];
  FOR(i, 1, n) FOR(j, 1, n) vert[i][j] += vert[i - 1][j];
  ROF(i, n, 1) FOR(j, 1, n) linj[i][j] += linj[i + 1][j - 1];
  FOR(i, 1, n) FOR(j, 1, n) linl[i][j] += linl[i - 1][j - 1];
  FOR(i, 1, n) FOR(j, 1, n) rang[i][j] += rang[i][j - 1];
  FOR(i, 1, n) FOR(j, 1, n) rang[i][j] += rang[i - 1][j];
  FOR(i, 1, n)
  FOR(j, 1, n)
  ans[i][j] = hori[i][j] || vert[i][j] || linj[i][j] || linl[i][j] || rang[i][j];
  FOR(i, 1, n) FOR(j, 1, n) printf("%c%c", "NY"[ans[i][j]], " \n"[j == n]);
  return 0;
}
