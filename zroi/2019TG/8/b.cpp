#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
typedef long long LL;
const int N = 1000, P = 1e9 + 7;
;

char s[N], t[N];
int n, m;
LL gs[N][2], gt[N][2];
// gs[i][bit]第j位为1表示s[i-j]=bit
// gt[i][bit]第j位为1表示t[j]=bit
map<LL, int> f[102];
// f[i,mask]:
// mask第j位为1表示这个串可能由s[0..i-j-1]和t[0..j-1]融合而成（即s的前i-j位和t的前j位

int main() {
  scanf("%s%s", s, t);
  n = strlen(s), m = strlen(t);
  FOR(i, 0, n - 1) s[i] -= '0';
  FOR(j, 0, m - 1) t[j] -= '0';
  FOR(i, 0, n + m) {
    FOR(bit, 0, 1) {
      FOR(j, 0, i) {
        if (i - j >= 0 && i - j < n && s[i - j] == bit) gs[i][bit] |= 1ll << j;
      }
      FOR(j, 0, i) {
        if (j < m && t[j] == bit) gt[i][bit] |= 1ll << j;
      }
    }
  }
  f[0][~0] = 1;
  FOR(i, 0, n + m - 1) { // f[i] -> f[i+1]
    FOR(bit, 0, 1) {
      // printf("i=%d,bit=%d\n",i,bit);
      for (pair<LL, int> x : f[i]) { //枚举状态
        // printf("x=(mask=%lld,f[%lld]=%d)\n",x.fi,x.fi,x.se);
        LL mask = x.fi;
        int f_i_mask = x.se;
        LL mask1 = (mask & gs[i][bit]) | ((mask & gt[i][bit]) << 1ll);
        // printf("mask1=%lld\n",mask1);
        f[i + 1][mask1] = (f[i + 1][mask1] + f_i_mask) % P;
      }
    }
  }
  printf("%d\n", f[n + m][1ll << m]);
  return 0;
}
