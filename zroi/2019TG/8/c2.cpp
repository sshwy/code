#include <bits/stdc++.h>
#define mxn 1000010
#define db double
#define LL long long
#define ldb long double
#define pb push_back
#define ppb pop_back
#define pf push_front
#define pii pair<int, int>
#define mp make_pair
#define fr first
#define sc second
#define x1 faqx1
#define y1 faqy1
#define x2 faqx2
#define y2 faqy2
using namespace std;
const int inf = 1e9;
const int s[] = {'N', 'Y'};
int n, sl, fh, a[1010][1010], s1[1010][1010], s2[1010][1010], ans[1010][1010];
int rd() {
  sl = 0;
  fh = 1;
  char ch = getchar();
  while (ch < '0' || '9' < ch) {
    if (ch == '-') fh = -1;
    ch = getchar();
  }
  while ('0' <= ch && ch <= '9') sl = sl * 10 + ch - '0', ch = getchar();
  return sl * fh;
}
int main() {
  n = rd();
  int x;
  for (int i = 1; i <= n; ++i)
    for (int j = 1; j <= n; ++j) {
      x = rd();
      a[i][j] = x;
      s1[i][j] = s1[i - 1][j - 1] + x;
      s2[i][j] = s2[i - 1][j + 1] + x;
    }
  int x1, y1, x2, y2, cnt;
  for (int i = 1; i <= n; ++i)
    for (int j = 1; j <= n; ++j)
      for (int k = 1; k <= n * 2; ++k) {
        cnt = 0;
        if (i > k)
          x1 = i - k, y1 = j;
        else
          x1 = 1, y1 = j - 1 + i - k;
        if (j > k)
          y2 = j - k, x2 = i;
        else
          y2 = 1, x2 = i - 1 + j - k;
        if (1 <= x1 && x1 <= n && 1 <= y1 && y1 <= n && 1 <= x2 && x2 <= n &&
            1 <= y2 && y2 <= n)
          cnt += s2[x2][y2] - s2[x1 - 1][y1 + 1];
        if (i > k)
          x1 = i - k, y1 = j;
        else
          x1 = 1, y1 = j + 1 - i + k;
        if (j + k <= n)
          y2 = j + k, x2 = i;
        else
          y2 = n, x2 = i - j - k + n;
        if (1 <= x1 && x1 <= n && 1 <= y1 && y1 <= n && 1 <= x2 && x2 <= n &&
            1 <= y2 && y2 <= n)
          cnt += s1[x2][y2] - s1[x1 - 1][y1 - 1];
        if (i + k <= n)
          x1 = i + k, y1 = j;
        else
          x1 = n, y1 = j - i - k + n;
        if (j > k)
          y2 = j - k, x2 = i;
        else
          y2 = 1, x2 = i + 1 - j + k;
        if (1 <= x1 && x1 <= n && 1 <= y1 && y1 <= n && 1 <= x2 && x2 <= n &&
            1 <= y2 && y2 <= n)
          cnt += s1[x1][y1] - s1[x2 - 1][y2 - 1];
        if (i + k <= n)
          x1 = i + k, y1 = j;
        else
          x1 = n, y1 = j + i + k - n;
        if (j + k <= n)
          y2 = j + k, x2 = i;
        else
          y2 = n, x2 = i + j + k - n;
        if (1 <= x1 && x1 <= n && 1 <= y1 && y1 <= n && 1 <= x2 && x2 <= n &&
            1 <= y2 && y2 <= n)
          cnt += s2[x1][y1] - s2[x2 - 1][y2 + 1];
        if (i > k) x1 = i - k, y1 = j, cnt -= a[x1][y1];
        if (j > k) y2 = j - k, x2 = i, cnt -= a[x2][y2];
        if (i + k <= n) x1 = i + k, y1 = j, cnt -= a[x1][y1];
        if (j + k <= n) y2 = j + k, x2 = i, cnt -= a[x2][y2];
        if (cnt >= 2) {
          ans[i][j] = 1;
          break;
        }
      }

  for (int i = 1; i <= n; ++i, puts(""))
    for (int j = 1; j <= n; ++j) printf("%c ", s[ans[i][j]]);
  return 0;
}
