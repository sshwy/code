#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long LL;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
LL pw(LL a, LL m, LL p) {
  a %= p;
  LL res = 1;
  while (m) m & 1 ? res = res * a % p : 0, a = a * a % p, m >>= 1;
  return res;
}
const int N = 4e5 + 6, P = 998244353;

LL fac[N], fnv[N];
void init(int k) {
  fac[0] = 1;
  FOR(i, 1, k) fac[i] = fac[i - 1] * i % P;
  fnv[k] = pw(fac[k], P - 2, P);
  ROF(i, k, 1) fnv[i - 1] = fnv[i] * i % P;
}
LL binom(LL n, LL m) {
  if (n < m) return 0;
  return fac[n] * fnv[m] % P * fnv[n - m] % P;
}
int n, m, k;
int a[N], tot;
int gcd(int a, int b) { return b ? gcd(b, a % b) : a; }
void dfs(int cur, int g, int cnt, int d) {
  if (cur > n) {
    if (g != d) return;
    if (cnt < k) return;
    tot++;
    return;
  }
  FOR(i, 1, m) { dfs(cur + 1, gcd(g, i), cnt + (a[cur] != i), d); }
}
LL calc(int d) {
  tot = 0;
  dfs(1, 0, 0, d);
  return tot;
}
int main() {
  scanf("%d%d%d", &n, &m, &k);
  init(n);
  FOR(i, 1, n) scanf("%d", &a[i]);
  FOR(d, 1, m) { printf("%lld%c", calc(d), " \n"[d == m]); }
  return 0;
}
