#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
#define int lld
const int N = 5000;

int n, m;
int a[N], b[N];
int A[N], B[N], la, lb;

signed main() {
  scanf("%lld%lld", &n, &m);
  FOR(i, 0, n - 1) scanf("%lld", &a[i]);
  FOR(i, 0, m - 1) scanf("%lld", &b[i]);
  int nn = 1 << n;
  FOR(i, 1, nn - 1) {
    int x = 0;
    FOR(j, 0, n - 1) {
      if (i >> j & 1) x = x * 11 + a[j];
    }
    A[++la] = x;
  }
  int mm = 1 << m;
  FOR(i, 1, mm - 1) {
    int x = 0;
    FOR(j, 0, m - 1) {
      if (i >> j & 1) x = x * 11 + b[j];
    }
    B[++lb] = x;
  }
  sort(A + 1, A + la + 1), sort(B + 1, B + lb + 1);
  // FOR(i,1,la)printf("%lld%c",A[i]," \n"[i==la]);
  // FOR(i,1,lb)printf("%lld%c",B[i]," \n"[i==lb]);
  la = unique(A + 1, A + la + 1) - A - 1;
  lb = unique(B + 1, B + lb + 1) - B - 1;
  // FOR(i,1,la)printf("%lld%c",A[i]," \n"[i==la]);
  // FOR(i,1,lb)printf("%lld%c",B[i]," \n"[i==lb]);
  int ans = 0;
  FOR(i, 1, la) FOR(j, 1, lb) if (A[i] > B[j])++ ans;
  printf("%lld\n", ans);
  return 0;
}
