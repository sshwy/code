#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long LL;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
int pw(int a, int m, int p) {
  a %= p;
  int res = 1;
  while (m) m & 1 ? res = 1ll * res * a % p : 0, a = 1ll * a * a % p, m >>= 1;
  return res;
}
const int N = 4e5 + 6, P = 998244353;

int fac[N], fnv[N];
void init(int k) {
  fac[0] = 1;
  FOR(i, 1, k) fac[i] = 1ll * fac[i - 1] * i % P;
  fnv[k] = pw(fac[k], P - 2, P);
  ROF(i, k, 1) fnv[i - 1] = 1ll * fnv[i] * i % P;
}
int binom(int n, int m) {
  if (n < m) return 0;
  return 1ll * fac[n] * fnv[m] % P * fnv[n - m] % P;
}
int n, m, k;
int a[N];

int f_[N];
int F_[N];
int c[N];

// int cnt;
int F(int d) {
  if (F_[d]) return F_[d];
  // printf("%d\n",++cnt);
  int x = m / d, tot = 0;
  tot = c[d];
  // FOR(i,1,n)if(a[i]%d==0)++tot;
  // assert(tot==c[d]);
  int k1 = max(k - (n - tot), 0);
  int res = 0, px = pw(x - 1, k1, P);
  FOR(j, k1, tot) {
    res = (res + 1ll * binom(tot, j) * px) % P;
    px = 1ll * px * (x - 1) % P;
  }
  res = 1ll * res * pw(x, n - tot, P) % P;
  F_[d] = res;
  return res;
}
int f(int d) {
  if (f_[d]) return f_[d];
  int lim = m / d;
  int res = F(d);
  ROF(i, lim, 2) res -= f(i * d), res += res < 0 ? P : 0;
  res += res < 0 ? P : 0;
  // res=(res+P)%P;
  return f_[d] = res;
}
int main() {
  scanf("%d%d%d", &n, &m, &k);
  init(n);
  FOR(i, 1, n) scanf("%d", &a[i]);
  FOR(i, 1, n) {
    for (int j = 1; j * j <= a[i]; j++) {
      if (a[i] % j) continue;
      c[j]++;
      if (j * j < a[i]) c[a[i] / j]++;
    }
  }
  FOR(d, 1, m) {
    // printf("d=%d\n",d);
    printf("%d%c", f(d), " \n"[d == m]);
  }
  return 0;
}
