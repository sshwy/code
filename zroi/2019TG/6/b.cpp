#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long LL;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
int pw(int a, int m, int p) { /*{{{*/
  // a%=p;
  int res = 1;
  while (m) m & 1 ? res = 1ll * res * a % p : 0, a = 1ll * a * a % p, m >>= 1;
  return res;
} /*}}}*/
const int N = 4e5 + 6, P = 998244353;

int fac[N], fnv[N];
inline void init(int k) { /*{{{*/
  fac[0] = 1;
  FOR(i, 1, k) fac[i] = 1ll * fac[i - 1] * i % P;
  fnv[k] = pw(fac[k], P - 2, P);
  ROF(i, k, 1) fnv[i - 1] = 1ll * fnv[i] * i % P;
}
inline int binom(int n, int m) {
  if (n < m) return 0;
  return 1ll * fac[n] * fnv[m] % P * fnv[n - m] % P;
} /*}}}*/
int n, m, k;
int a[N], f_[N], F_[N], c[N];

inline int F(int d) { /*{{{*/
  if (F_[d]) return F_[d];
  int x = m / d, tot = 0;
  tot = c[d];
  int k1 = max(k - (n - tot), 0);
  int res = 0, px = pw(x - 1, k1, P);
  FOR(j, k1, tot) {
    res = (res + 1ll * binom(tot, j) * px) % P;
    px = 1ll * px * (x - 1) % P;
  }
  res = 1ll * res * pw(x, n - tot, P) % P;
  F_[d] = res;
  return res;
}
int f(int d) {
  int lim = m / d;
  int res = F_[d];
  ROF(i, lim, 2) res -= f_[i * d], res += res < 0 ? P : 0;
  res += res < 0 ? P : 0;
  return f_[d] = res;
} /*}}}*/

bool bp[N];
int pn[N], lp, tr[N];
void sieve(int lim) {
  bp[0] = bp[1] = 1;
  FOR(i, 2, lim) {
    if (!bp[i]) pn[++lp] = i, tr[i] = i;
    FOR(j, 1, lp) {
      int pj = pn[j];
      if (i * pj > lim) break;
      bp[i * pj] = 1, tr[i * pj] = pj;
      if (i % pj == 0) break;
    }
  }
}
int p[50], q[50], le;
void div(int x) {
  le = 0;
  while (x > 1) {
    int cur = tr[x];
    p[++le] = cur, q[le] = 0;
    while (x % cur == 0) q[le]++, x /= cur;
  }
}
void dfs(int cur, int prd) {
  // printf("dfs(%d,%d)\n",cur,prd);
  c[prd]++;
  FOR(i, cur + 1, le) { // we chose p[i]
    int pj = 1;
    FOR(j, 1, q[i]) {
      pj *= p[i];
      dfs(i, prd * pj);
    }
  }
}
int c2[N];
int main() {
  scanf("%d%d%d", &n, &m, &k);
  init(max(n, m));
  sieve(max(n, m));
  FOR(i, 1, n) scanf("%d", &a[i]);
  FOR(i, 1, n) {
    div(a[i]);
    dfs(0, 1);
  }
  FOR(d, 1, m) F(d);
  ROF(d, m, 1) f(d);
  FOR(d, 1, m) printf("%d%c", f_[d], " \n"[d == m]);
  return 0;
}
