#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
namespace IO {
  char nc() {
    static char bf[100000], *p1 = bf, *p2 = bf;
    return p1 == p2 && (p2 = (p1 = bf) + fread(bf, 1, 100000, stdin), p1 == p2)
               ? EOF
               : *p1++;
  }
  int rd() {
    int res = 0;
    char c = getchar();
    while (!isdigit(c)) c = getchar();
    while (isdigit(c)) res = res * 10 + c - '0', c = getchar();
    return res;
  }
} // namespace IO
/******************heading******************/
const int N = 30;
int n, m;
char s[N], t[N];

bool check(int S) {
  int c[N], lc = 0;
  c[0] = 0;
  FOR(i, 1, n) if ((S >> (i - 1)) & 1) c[++lc] = i;
  FOR(i, 1, m) if (s[c[i]] != t[i]) return 0;
  c[++lc] = n + 1;
  FOR(i, 0, m) {
    FOR(j, c[i] + 1, c[i + 1] - 2) {
      if (s[j] != s[j + 1]) return 0;
    }
  }
  FOR(i, 1, m) printf("%d%c", c[i], " \n"[i == m]);
  return 1;
}
int main() {
  scanf("%d%d", &n, &m);
  scanf("%s", s + 1);
  scanf("%s", t + 1);
  int lim = 1 << n;
  FOR(i, 0, lim - 1) {
    if (__builtin_popcount(i) != m) continue;
    if (check(i)) return 0;
  }
  puts("-1");
  return 0;
}
