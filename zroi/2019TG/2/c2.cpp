#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
namespace IO {
  char nc() {
    static char bf[100000], *p1 = bf, *p2 = bf;
    return p1 == p2 && (p2 = (p1 = bf) + fread(bf, 1, 100000, stdin), p1 == p2)
               ? EOF
               : *p1++;
  }
  int rd() {
    int res = 0;
    char c = getchar();
    while (!isdigit(c)) c = getchar();
    while (isdigit(c)) res = res * 10 + c - '0', c = getchar();
    return res;
  }
} // namespace IO
/******************heading******************/
const int N = 2050;
int n, q;
int dg[N];

struct qxx {
  int nex, t, v;
};
qxx e[N * 2];
int h[N], le = 1;
void add_path(int f, int t, int v) { e[++le] = (qxx){h[f], t, v}, h[f] = le; }

int fw[20][N], fp[20][N], dep[N];
int gcd(int a, int b) { return b ? gcd(b, a % b) : a; }
int a[N], g[N], la;
void dfs(int u, int p, int w) {
  printf("dfs(%d,%d,%d)\n", u, p, w);
  a[++la] = w;
  g[u] = la;
  for (int i = h[u]; i; i = e[i].nex) {
    if (e[i].t == p) continue;
    dfs(e[i].t, u, e[i].v);
  }
}
int main() {
  scanf("%d%d", &n, &q);
  FOR(i, 1, n - 1) {
    int u, v, w;
    scanf("%d%d%d", &u, &v, &w);
    add_path(u, v, w), add_path(v, u, w);
    dg[u]++, dg[v]++;
  }
  FOR(i, 1, n) if (dg[i] == 2) {
    dfs(i, 0, 0);
    break;
  }
  printf("a: ");
  FOR(i, 1, n) printf("%d%c", a[i], " \n"[i == n]);
  printf("g: ");
  FOR(i, 1, n) printf("%d%c", g[i], " \n"[i == n]);
  return 0;
}
