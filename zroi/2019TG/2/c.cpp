#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
namespace IO {
  char nc() {
    static char bf[100000], *p1 = bf, *p2 = bf;
    return p1 == p2 && (p2 = (p1 = bf) + fread(bf, 1, 100000, stdin), p1 == p2)
               ? EOF
               : *p1++;
  }
  int rd() {
    int res = 0;
    char c = getchar();
    while (!isdigit(c)) c = getchar();
    while (isdigit(c)) res = res * 10 + c - '0', c = getchar();
    return res;
  }
} // namespace IO
/******************heading******************/
const int N = 20500, W = 1e6 + 500;
int n, q;

struct qxx {
  int nex, t, v;
};
qxx e[N * 2];
int h[N], le = 1;
void add_path(int f, int t, int v) { e[++le] = (qxx){h[f], t, v}, h[f] = le; }

int fw[20][N], fp[20][N], dep[N], pidx[N];
int gcd(int a, int b) { return b ? gcd(b, a % b) : a; }
void dfs(int u, int p, int w) { /*{{{*/
  dep[u] = dep[p] + 1;
  fp[0][u] = p;
  fw[0][u] = w;
  FOR(j, 1, 19) {
    fp[j][u] = fp[j - 1][fp[j - 1][u]];
    fw[j][u] = gcd(fw[j - 1][u], fw[j - 1][fp[j - 1][u]]);
    if (!fp[j][u]) break;
  }
  for (int i = h[u]; i; i = e[i].nex) {
    if (e[i].t == p) continue;
    pidx[e[i].t] = i;
    dfs(e[i].t, u, e[i].v);
  }
} /*}}}*/

bool bp[W];
int prime[N], lp, tr[W];
void sieve(int k) { /*{{{*/
  bp[0] = bp[1] = 1;
  FOR(i, 2, k) {
    if (!bp[i]) prime[++lp] = i, tr[i] = i;
    FOR(j, 1, lp) {
      const int pj = prime[j];
      if (i * pj > k) break;
      bp[i * pj] = 1, tr[i * pj] = pj;
      if (i % pj == 0) break;
    }
  }
} /*}}}*/

struct Ans {
  int len, pu, pv, w, res;
};
Ans ans[N];
int la;
typedef map<int, vector<int>> Mivi;
typedef map<int, vector<pii>> Mivpii;
Mivpii edge;
Mivi query;

void calc(int u, int v) { /*{{{*/
  if (dep[u] < dep[v]) swap(u, v);
  int res = 0, pathu, pathv, len = dep[u] + dep[v];
  pathu = pidx[u];
  ROF(i, 19, 0) {
    if (dep[u] - dep[v] <= (1 << i)) continue;
    res = gcd(res, fw[i][u]);
    u = fp[i][u];
  }
  res = gcd(res, fw[0][u]);
  if (fp[0][u] == v) {
    pathv = pidx[u] ^ 1;
    // return len+calc(pathu,pathv,res);
  } else {
    u = fp[0][u];
    pathv = pidx[v];
    ROF(i, 19, 0) {
      if (fp[i][u] == fp[i][v]) continue;
      res = gcd(res, fw[i][u]), res = gcd(res, fw[i][v]);
      u = fp[i][u], v = fp[i][v];
    }
    res = gcd(res, fw[0][u]), res = gcd(res, fw[0][v]);
  }
  u = fp[0][u];
  len -= dep[u] + dep[fp[0][u]];
  ans[++la] = (Ans){len, pathu, pathv, res};
  int r2 = res;
  while (r2 > 1) {
    int cur = tr[r2];
    query[cur].pb(la); // add this query to list
    while (r2 % cur == 0) r2 /= cur;
  } // return len+calc(pathu,pathv,res);
} /*}}}*/
int totime;
int tmp[N][2], lt;
int Q[N], l, r;
namespace G {
  struct qxx {
    int nex, t;
  };
  qxx ee[N * 2];
  int hh[N], ht[N], le = 1;
  void add_path(int f, int t) {
    ee[++le] = (qxx){ht[f] == totime ? hh[f] : 0, t}, hh[f] = le, ht[f] = totime;
  }
  int f[N], ft[N];
  int dfsdp1(int path) { /*{{{*/
    int res = 0, u = ee[path].t;
    if (ht[u] == totime)
      for (int i = hh[u]; i; i = ee[i].nex) {
        if ((i ^ 1) == path) continue;
        res = max(res, dfsdp1(i));
      }
    return ft[path] = totime, f[path] = res + 1;
  }                           /*}}}*/
  void dfsdp2(int u, int p) { /*{{{*/
    lt = 0;
    if (ht[u] == totime)
      for (int i = hh[u]; i; i = ee[i].nex) {
        ++lt;
        tmp[lt][0] = i;                          // path
        tmp[lt][1] = ft[i] == totime ? f[i] : 0; // dp value
      }
    FOR(i, 1, lt) tmp[i + lt][0] = tmp[i][0], tmp[i + lt][1] = tmp[i][1];
    l = 1, r = 0;
    FOR(i, 1, lt) { // add to Que
      while (l <= r && tmp[Q[r]][1] <= tmp[i][1]) --r;
      Q[++r] = i;
    }
    FOR(i, lt + 1, lt + lt) {
      while (l <= r && Q[l] <= i - lt) ++l;
      const int &path = tmp[i][0];
      if (l > r) {
        f[path ^ 1] = 1, ft[path ^ 1] = totime;
      } else {
        if (ft[path ^ 1] == totime) { assert(f[path ^ 1] == tmp[Q[l]][1] + 1); }
        f[path ^ 1] = tmp[Q[l]][1] + 1, ft[path ^ 1] = totime;
      }
      while (l <= r && tmp[Q[r]][1] <= tmp[i][1]) --r;
      Q[++r] = i;
    }
    if (ht[u] == totime)
      for (int i = hh[u]; i; i = ee[i].nex) {
        if (ee[i].t == p) continue;
        dfsdp2(ee[i].t, u);
      }
  }                /*}}}*/
  void DP(int u) { /*{{{*/
    if (ht[u] == totime)
      for (int i = hh[u]; i; i = ee[i].nex) dfsdp1(i);
    dfsdp2(u, 0); // swap root
  }               /*}}}*/
  void calc(int idx) {
    int pu = ans[idx].pu, pv = ans[idx].pv;
    int u = e[pu].t, v = e[pv].t;
    if (ft[pu] != totime) DP(u);
    if (ht[u] == totime)
      for (int i = hh[u]; i; i = ee[i].nex) {
        if (ee[i].t == e[pu ^ 1].t) {
          pu = i ^ 1;
          break;
        }
      }
    if (ft[pv] != totime) DP(v);
    if (ht[v] == totime)
      for (int i = hh[v]; i; i = ee[i].nex) {
        if (ee[i].t == e[pv ^ 1].t) {
          pv = i ^ 1;
          break;
        }
      }
    ans[idx].res = max(ans[idx].res, f[pu] + f[pv]);
  }
} // namespace G
void build_graph(const vector<pii> &eg) {
  G::le = 1;
  for (unsigned int i = 0; i < eg.size(); i++) {
    G::add_path(eg[i].fi, eg[i].se);
    G::add_path(eg[i].se, eg[i].fi);
  }
}
int f[N];
int dfsdp1(int path) { /*{{{*/
  int res = 0, u = e[path].t;
  for (int i = h[u]; i; i = e[i].nex) {
    if ((i ^ 1) == path) continue;
    res = max(res, dfsdp1(i));
  }
  return f[path] = res + 1;
} /*}}}*/
void dfsdp2(int u, int p) { /*{{{*/
  lt = 0;
  for (int i = h[u]; i; i = e[i].nex) {
    ++lt;
    tmp[lt][0] = i;    // path
    tmp[lt][1] = f[i]; // dp value
  }
  FOR(i, 1, lt) tmp[i + lt][0] = tmp[i][0], tmp[i + lt][1] = tmp[i][1];
  l = 1, r = 0;
  FOR(i, 1, lt) { // add to Que
    while (l <= r && tmp[Q[r]][1] <= tmp[i][1]) --r;
    Q[++r] = i;
  }
  FOR(i, lt + 1, lt + lt) {
    while (l <= r && Q[l] <= i - lt) ++l;
    const int &path = tmp[i][0];
    if (l > r) {
      f[path ^ 1] = 1;
    } else {
      if (f[path ^ 1]) assert(f[path ^ 1] == tmp[Q[l]][1] + 1);
      f[path ^ 1] = tmp[Q[l]][1] + 1;
    }
    while (l <= r && tmp[Q[r]][1] <= tmp[i][1]) --r;
    Q[++r] = i;
  }
  for (int i = h[u]; i; i = e[i].nex) {
    if (e[i].t == p) continue;
    dfsdp2(e[i].t, u);
  }
} /*}}}*/
void DP(int u) { /*{{{*/
  for (int i = h[u]; i; i = e[i].nex) dfsdp1(i);
  dfsdp2(u, 0); // swap root
} /*}}}*/
int main() {
  sieve(1000000);
  scanf("%d%d", &n, &q);
  FOR(i, 1, n - 1) {
    int u, v, w;
    scanf("%d%d%d", &u, &v, &w);
    int w2 = w;
    while (w2 > 1) {
      int cur = tr[w2];
      edge[cur].pb(mk(u, v));
      while (w2 % cur == 0) w2 /= cur;
    }
    add_path(u, v, w), add_path(v, u, w);
  }
  dfs(1, 0, 0);
  DP(1);
  FOR(i, 1, q) {
    int u, v;
    scanf("%d%d", &u, &v);
    calc(u, v);
  }
  assert(la == q);
  for (Mivpii::iterator it = edge.begin(); it != edge.end(); it++) {
    int cur = it->fi;
    const vector<pii> &cure = it->se;
    ++totime;
    build_graph(cure);
    const vector<int> &qr = query[cur];
    for (unsigned i = 0; i < qr.size(); i++) { G::calc(qr[i]); }
  }
  FOR(i, 1, la) {
    int pu = ans[i].pu, pv = ans[i].pv, len = ans[i].len, res = ans[i].res,
        w = ans[i].w;
    int mx = f[pu] + f[pv] + len - 2;
    if (w == 1)
      printf("%d\n", len - 1);
    else if (len + res - 2 + 1 > mx)
      puts("-1");
    else
      printf("%d\n", len + res - 2);
  }
  return 0;
}
