#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
namespace IO {
  char nc() {
    static char bf[100000], *p1 = bf, *p2 = bf;
    return p1 == p2 && (p2 = (p1 = bf) + fread(bf, 1, 100000, stdin), p1 == p2)
               ? EOF
               : *p1++;
  }
  int rd() {
    int res = 0;
    char c = getchar();
    while (!isdigit(c)) c = getchar();
    while (isdigit(c)) res = res * 10 + c - '0', c = getchar();
    return res;
  }
} // namespace IO
/******************heading******************/
const int N = 1e6 + 5, INF = 0x3f3f3f3f;
int n;
int a[N], b[N];

struct data {
  int u, typ, v;
  bool operator<(data d) const { return v > d.v; }
};
priority_queue<data> q;
int da[N], db[N];

data tmp[N];
bool check(int k) {
  // printf("check(%d)\n",k);
  FOR(i, 0, n) da[i] = INF;
  FOR(i, 0, n) db[i] = INF;
  while (!q.empty()) q.pop();
  da[1] = db[1] = 0;
  q.push((data){1, 0, 0});
  q.push((data){1, 1, 0});
  int t = 0;
  int lt = 0;
  while (!q.empty()) {
    ++t;
    data cur = q.top();
    q.pop();
    const int &u = cur.u, typ = cur.typ, v = cur.v;
    if (t > 7e7) {
      printf("check(%d)\n", k);
      printf("(%d,%d,%d)\n", u, typ, v);
      FOR(i, 1, lt) printf("(%d,%d)", tmp[i].typ, tmp[i].v);
      exit(0);
    }
    if (u == 13751) { tmp[++lt] = cur; }
    // printf("(%d,%d,%d)\n",u,typ,v);
    if (typ == 0) { // A
      if (1 < db[b[u]]) { db[b[u]] = 1, q.push((data){b[u], 1, 1}); }
      if (v < k && v + 1 < da[a[u]]) {
        da[a[u]] = v + 1, q.push((data){a[u], 0, v + 1});
      }
    } else {
      if (1 < da[a[u]]) { da[a[u]] = 1, q.push((data){a[u], 0, 1}); }
      if (v < k && v + 1 < db[b[u]]) {
        db[b[u]] = v + 1, q.push((data){b[u], 1, v + 1});
      }
    }
    if (da[n] != INF || db[n] != INF) return 1;
  }
  return 0;
}
int main() {
  // freopen("ex_walk/ex_walk3.in","r",stdin);
  scanf("%d", &n);
  FOR(i, 1, n) scanf("%d%d", &a[i], &b[i]);
  int l = 1, r = n + 1;
  while (l < r) {
    int mid = (l + r) >> 1;
    if (check(mid))
      r = mid;
    else
      l = mid + 1;
  }
  if (l == n + 1)
    puts("-1");
  else
    printf("%d", l);
  return 0;
}
