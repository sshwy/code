#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
namespace IO {
  char nc() {
    static char bf[100000], *p1 = bf, *p2 = bf;
    return p1 == p2 && (p2 = (p1 = bf) + fread(bf, 1, 100000, stdin), p1 == p2)
               ? EOF
               : *p1++;
  }
  int rd() {
    int res = 0;
    char c = getchar();
    while (!isdigit(c)) c = getchar();
    while (isdigit(c)) res = res * 10 + c - '0', c = getchar();
    return res;
  }
} // namespace IO
/******************heading******************/
const int N = 5e5 + 5, A = 1e6 + 6;

bool bp[A];
int pn[N], tr[A], lp;

void sieve(int k) {
  bp[0] = bp[1] = 1;
  FOR(i, 2, k) {
    if (!bp[i]) pn[++lp] = i, tr[i] = i;
    FOR(j, 1, lp) {
      const int pj = pn[j];
      if (i * pj > k) break;
      bp[i * pj] = 1;
      tr[i * pj] = pj;
      if (i % pj == 0) break;
    }
  }
}
map<int, vector<int>> mp;
int a[N];
int n;

pii t[N];
pii tmin[N];
int lt;

int al, ar;
void upd(int l, int r) {
  if (r - l > ar - al) return al = l, ar = r, void();
  if (r - l == ar - al && l < al) return al = l, ar = r, void();
}

void calc(const vector<int> &v) {
  // printf("calc: ");
  // for(unsigned i=0;i<v.size();i++)printf("%d ",v[i]);
  // puts("");
  lt = 0;
  for (unsigned i = 0; i < v.size(); i++) { t[++lt] = mk(v[i] - 2 * i, i); }
  sort(t + 1, t + lt + 1);
  int j = 1, mx = 0, p = 0; // mx记录右端点最大值。v[t[i].se]记录当前左端点
  FOR(i, 1, lt) {
    while (j <= lt && t[j].fi <= t[i].fi + 1) {
      if (v[t[j].se] > mx) mx = v[t[j].se], p = t[j].se;
      ++j;
    }
    int l = v[t[i].se], r = mx, cnt = p - t[i].se + 1;
    if (r - l + 1 <= cnt * 2) {
      l = max(1, r - cnt * 2 + 1);
      r = min(n, l + cnt * 2 - 1);
      upd(l, r);
    }
  }
}

int main() {
  scanf("%d", &n);
  FOR(i, 1, n) scanf("%d", &a[i]);
  sieve(1000000);
  FOR(i, 1, n) {
    int x = a[i];
    while (x > 1) {
      int cur = tr[x];
      mp[cur].pb(i);
      while (x % cur == 0) x /= cur;
    }
  }
  for (map<int, vector<int>>::iterator it = mp.begin(); it != mp.end(); ++it) {
    calc(it->second);
  }
  printf("%d %d\n", al, ar);
  return 0;
}
