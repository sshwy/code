#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 5000, M = 5000;

struct qxx {
  int nex, t;
};
qxx e[N * 2];
int h[N], le;
void add_path(int f, int t) { e[++le] = (qxx){h[f], t}, h[f] = le; }
#define FORe(i, u, v) for (int i = h[u], v; v = e[i].t, i; i = e[i].nex)

int n, m;
int w[N], s[N], c[N];

int totdfn, sz[N];
int f[N][M];
int dfs(int u) {
  sz[u] = 1;
  FORe(i, u, v) sz[u] += dfs(v);
  int i = ++totdfn;
  int lim = s[u];
  f[i][0] = 0;
  // f[i,j] <- f[i-sz[u],j] , f[i-1,j]
  for (int j = m; j >= 0; j--) {
    if (j >= c[u])
      f[i][j] =
          max(f[i][j], f[i - 1][j - c[u]] + w[u]); //至少选一个物品，才能从i-1转移
    f[i][j] = max(f[i][j], f[i - sz[u]][j]);
  }
  --lim;
  for (int k = 1; k <= lim; lim -= k,
           k <<= 1) { //在之前i-1和i-sz转移的基础上，加入多个物品（二进制）
    for (int j = m; j >= k * c[u]; j--) {
      f[i][j] = max(f[i][j], f[i][j - k * c[u]] + k * w[u]);
    }
  }
  if (lim) { //剩下一个二进制余项
    for (int j = m; j >= lim * c[u]; j--) {
      f[i][j] = max(f[i][j], f[i][j - lim * c[u]] + lim * w[u]);
    }
  }
  return sz[u];
}

int main() {
  scanf("%d%d", &n, &m);
  FOR(i, 2, n) {
    int x;
    scanf("%d", &x);
    add_path(x, i);
  }
  FOR(i, 1, n) { scanf("%d%d%d", &w[i], &c[i], &s[i]); }
  dfs(1);
  FOR(i, 1, m) printf("%d%c", f[totdfn][i], " \n"[i == m]);
  return 0;
}
