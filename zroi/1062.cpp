#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
#define LL long long
const int N = 100;

LL n, m, d;
LL l[N], r[N];
void go() {
  scanf("%lld%lld%lld", &n, &m, &d);
  FOR(i, 1, d) scanf("%lld-%lld", &l[i], &r[i]);
  ROF(i, d, 1) {
    if (l[i] <= m) m += r[i] - l[i] + 1;
  }
  if (m > n)
    puts("-1");
  else
    printf("%lld\n", m);
}
int main() {
  int k;
  scanf("%d", &k);
  FOR(I, 1, k) go();
  return 0;
}
