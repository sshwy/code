#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/

vector<int> a;
int T, typ, lans;

int main() {
  scanf("%d%d", &T, &typ);
  FOR(i, 1, T) {
    int op, x, y, z;
    // for(int i:a)printf("%d ",i); puts("");
    scanf("%d", &op);
    if (op == 0) {
      scanf("%d%d", &x, &y);
      if (typ) x ^= lans, y ^= lans;
      a.insert(a.begin() + x - 1, y);
    } else {
      scanf("%d%d%d", &x, &y, &z);
      if (typ) x ^= lans, y ^= lans, z ^= lans;
      int ans = 0;
      FOR(i, x - 1, y - 1) {
        // printf("%d,%d\n",a[i],z);
        ans = max(ans, a[i] ^ z);
      }
      printf("%d\n", ans);
      lans = ans;
    }
  }
  return 0;
}
