// by Sshwy
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/

using namespace RA;

void go() {
  int n = r(2, 100), m = r(1, 70), k = min(n, 4);
  printf("%d %d %d\n", n, m, k);
  FOR(i, 1, m) {
    int u = 0, v = 0;
    while (u == v) u = r(1, n), v = r(1, n);
    if (u > v) swap(u, v);
    printf("%d %d\n", u, v);
  }
}
int main() {
  srand(clock());
  int t = 100;
  printf("%d\n", t);
  FOR(i, 1, t) go();
  return 0;
}
