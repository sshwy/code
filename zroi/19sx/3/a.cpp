// by Sshwy
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 305, M = 405;

int h[N], nex[M], to[M], le;
void add_path(int f, int t) { ++le, nex[le] = h[f], to[le] = t, h[f] = le; }

int n, m, k, ans;
int ban[N];

void dfs(int cur) {
  int f[N] = {}, ne[N] = {}, mx = 0, id = 0;
  ROF(i, n, 1) {
    if (ban[i])
      f[i] = -n;
    else {
      for (int _j = h[i]; _j; _j = nex[_j])
        if (f[i] < f[to[_j]]) f[i] = f[ne[i] = to[_j]];
      ++f[i];
    }
    if (f[i] > mx) mx = f[id = i];
    if (cur > k && mx >= ans) return;
  }
  if (cur > k)
    ans = mx;
  else {
    for (int i = id; i; i = ne[i]) ban[i] = 1, dfs(cur + 1), ban[i] = 0;
  }
}
void go() {
  scanf("%d%d%d", &n, &m, &k);
  ans = n;
  memset(h, 0, sizeof(h)), le = 0;
  FOR(i, 1, m) {
    int u, v;
    scanf("%d%d", &u, &v);
    add_path(u, v);
  }
  dfs(1);
  printf("%d\n", ans);
}
int main() {
  int t;
  scanf("%d", &t);
  FOR(i, 1, t) go();
  return 0;
}
