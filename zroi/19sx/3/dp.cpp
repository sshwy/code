#include <cstdio>
#include <cstdlib>
#include <ctime>
using namespace std;

int main() {
  system("g++ a.cpp -o .usr");
  system("g++ std.cpp -o .std");
  system("g++ gen.cpp -o .gen");
  int t = 0;
  while (++t) {
    system("./.gen > .fin");
    if (system("./.usr < .fin > .fout")) break;
    system("./.std < .fin > .fstd");
    if (system("diff .fout .fstd")) break;
    printf("AC#%d\n", t);
  }
  printf("WA#%d\n", t);
  return 0;
}
