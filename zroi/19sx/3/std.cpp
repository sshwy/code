#include <algorithm>
#include <cctype>
#include <cstdio>
#include <cstring>
using namespace std;
const int N = 550;
int n, m, k;

int gti(void) {
  char c = getchar();
  int ret = 0, st = 1;
  for (; !isdigit(c); c = getchar())
    if (c == '-') st = -1;
  for (; isdigit(c); c = getchar()) ret = ret * 10 + c - '0';
  return ret * st;
}

struct edge {
  edge *nxt;
  int to;
} pool[N], *pt[N], *p;
struct Edge {
  Edge *nxt;
  int to;
} Pool[N], *Pt[N], *P;
void addedge(int a, int b) {
  *(++p) = (edge){pt[a], b}, pt[a] = p;
  *(++P) = (Edge){Pt[b], a}, Pt[b] = P;
}
int ban[N], fr[N], dis[N], pts[10][N], ans, vis[N], Dis[N];

int table[20][N * 10], lg[N], pw[20];
void init(void) {
  lg[1] = 0;
  for (int i = 2; i <= n; i++) lg[i] = lg[i >> 1] + 1;
  for (int i = 0; i < 9; i++) pw[i] = 1 << i;
  for (int i = 0; i < 9; i++)
    for (int j = 1; j <= n; j++) table[i][j] = 0;
}
void upd(int l, int r, int val) {
  if (l > r) return;
  int g = lg[r - l + 1];
  table[g][l] = max(table[g][l], val);
  table[g][r - pw[g] + 1] = max(table[g][r - pw[g] + 1], val);
}
int pushdown(void) {
  for (int i = 8; i > 0; i--)
    for (int j = 1; j <= n; j++) {
      table[i - 1][j] = max(table[i - 1][j], table[i][j]);
      table[i - 1][j + pw[i - 1]] = max(table[i - 1][j + pw[i - 1]], table[i][j]);
    }
  int ans = 0x3f3f3f3f;
  for (int i = 1; i <= n; i++)
    if (!vis[i]) ans = min(ans, table[0][i]);
  return ans;
}

void dfs(int k) {
  int x = 0, tot = 0;
  for (int i = n; i >= 1; i--) {
    dis[i] = 1, fr[i] = 0;
    if (vis[i]) continue;
    for (edge *e = pt[i]; e; e = e->nxt)
      if (!vis[e->to] && dis[e->to] + 1 > dis[i]) {
        dis[i] = dis[e->to] + 1, fr[i] = e->to;
        if (dis[i] >= ans) break;
      }
    if (dis[i] > dis[x]) x = i;
    if (dis[i] >= ans) break;
  }
  if (k == 0) {
    ans = min(ans, dis[x]);
    return;
  }
  if (k == 1) {
    init();
    for (int i = 1; i <= n; i++) {
      if (vis[i]) continue;
      Dis[i] = 1;
      for (Edge *e = Pt[i]; e; e = e->nxt)
        if (!vis[e->to] && Dis[e->to] + 1 > Dis[i]) Dis[i] = Dis[e->to] + 1;
      upd(1, i - 1, dis[i]), upd(i + 1, n, Dis[i]);
      for (Edge *e = Pt[i]; e; e = e->nxt)
        if (!vis[e->to] && e->to < i - 1) upd(e->to + 1, i - 1, Dis[e->to] + dis[i]);
    }
    ans = min(ans, pushdown());
    return;
  }
  for (int i = x; i; i = fr[i]) pts[k][++tot] = i;
  for (int i = 1; i <= tot; i++) {
    vis[pts[k][i]] = true;
    dfs(k - 1);
    vis[pts[k][i]] = false;
  }
}

int main(void) {
  for (int t = gti(); t >= 1; t--) {
    ans = 0x3f3f3f3f;
    n = gti(), m = gti(), k = gti();
    for (int i = 1; i <= n; i++) pt[i] = NULL, Pt[i] = NULL;
    p = pool, P = Pool;
    for (int i = 1; i <= m; i++) {
      int u = gti(), v = gti();
      addedge(u, v);
    }
    dfs(k);
    printf("%d\n", ans);
  }
  return 0;
}
