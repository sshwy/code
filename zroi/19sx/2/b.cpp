// by Sshwy
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 3005;

int n;
long long T, U[N], V[N], D[N], E[N];
long long f
    [N]
    [N]; // f[i,j]前i个车站，j个未闭的环，你当前在上行车站的最小代价（不考虑从0到n+1的一条通路）

void get_min(long long &x, long long y) { x = min(x, y); }
int main() {
  scanf("%d%lld", &n, &T);
  FOR(i, 1, n) { scanf("%lld%lld%lld%lld", &U[i], &V[i], &D[i], &E[i]); }
  memset(f, 0x3f, sizeof(f));
  f[0][0] = 0;

  FOR(i, 1, n) {
    //加一个环
    FOR(j, 1, n)
    get_min(f[i][j], f[i - 1][j - 1] + 2 * T * (j - 1) + D[i] + V[i]);
    FOR(j, 1, n) get_min(f[i][j], f[i][j - 1] + D[i] + V[i]);
    //去掉一个环
    FOR(j, 0, n - 1)
    get_min(f[i][j], f[i - 1][j + 1] + 2 * T * (j + 1) + U[i] + E[i]);
    ROF(j, n - 1, 0) get_min(f[i][j], f[i][j + 1] + U[i] + E[i]);
    //不去掉环
    FOR(j, 0, n) get_min(f[i][j], f[i - 1][j] + 2 * T * j + U[i] + V[i]);
    FOR(j, 1, n) get_min(f[i][j], f[i - 1][j] + 2 * T * j + D[i] + E[i]);
  }

  printf("%lld\n", f[n][0] + T * (n + 1));
  return 0;
}
