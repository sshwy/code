#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
namespace IO {
  char nc() {
    static char bf[100000], *p1 = bf, *p2 = bf;
    return p1 == p2 && (p2 = (p1 = bf) + fread(bf, 1, 100000, stdin), p1 == p2)
               ? EOF
               : *p1++;
  }
  int rd() {
    int res = 0;
    char c = nc();
    while (!isdigit(c)) c = nc();
    while (isdigit(c)) res = res * 10 + c - '0', c = nc();
    return res;
  }
  int tmp[20], lt;
  char OBF[100000], *OBP = OBF;
  void flush() { fwrite(OBF, 1, OBP - OBF, stdout), OBP = OBF; }
  void wrch(char x) {
    if (OBP == OBF + 99999) flush();
    *OBP = x, ++OBP;
  }
  void wr(int x) {
    lt = 0;
    while (x) tmp[++lt] = x % 10, x /= 10;
    while (lt > 0) wrch(tmp[lt] + '0'), --lt;
  }
  void wr(long long x) {
    lt = 0;
    while (x) tmp[++lt] = x % 10, x /= 10;
    while (lt > 0) wrch(tmp[lt] + '0'), --lt;
  }
  void wr(const char *s) {
    while (*s) wrch(*s), ++s;
  }
  void wr(char x) { wrch(x); }
} // namespace IO
/******************heading******************/
const int N = 5e5 + 5, M = 6e6 + 5;

struct qxx {
  int nex, t, v;
};
qxx e[M];
int h[N], le = 1;
void add_path(int f, int t, int v) { e[++le] = (qxx){h[f], t, v}, h[f] = le; }
#define FORe(i, _u, _v, _w) \
  for (int i = h[_u], _v, _w; _v = e[i].t, _w = e[i].v, i; i = e[i].nex)

int n, m;
int p[N], col[N];
typedef pair<long long, long long> expr;
expr A[N]; // x*fi + se 的形式

expr operator-(int x, expr a) { return {-a.fi, x - a.se}; }
expr operator+(expr a, expr b) { return {a.fi + b.fi, a.se + b.se}; }

long long L, R;
expr X;
void upd(expr a, int l, int r) {
  assert(a.fi);
  if (a.fi > 0) { // l - se <= fi*x <= r - se
    L = max(L, (l - a.se + a.fi - 1) / a.fi);
    R = min(R, (r - a.se + a.fi - 1) / a.fi);
  } else { // se - r <= -fi*x <= se-l
    L = max(L, (a.se - r - a.fi - 1) / (-a.fi));
    R = min(R, (a.se - l - a.fi - 1) / (-a.fi));
  }
}
long long f(expr a, int x) { return a.fi * x + a.se; }
bool vis[N];
bool check(expr a, int v) {
  if (a.fi == 0) return a.se != v;
  // a.fi*x=v-a.se
  int x = (v - a.se) / a.fi;
  if (a.fi * x + a.se != v) return 1; // NIE
  upd({1, 0}, x, x);
  return 0;
}
void dfs(int u) {
  FORe(i, u, v, w) {
    if (!vis[v]) {
      A[v] = w - A[u], vis[v] = 1, dfs(v);
    } else {
      if (A[v] != w - A[u]) {
        if (check(A[v] + A[u], w)) puts("NIE"), exit(0);
      }
    }
  }
  upd(A[u], 0, p[u]);
  X = X + A[u];
}
long long A1, A2;
void work(int u) {
  A[u] = make_pair(1, 0), vis[u] = 1;
  X = {0, 0};
  L = 0, R = p[u];
  dfs(u);
  if (L > R) puts("NIE"), exit(0);
  long long a1 = f(X, L), a2 = f(X, R);
  if (a1 > a2) swap(a1, a2);
  A1 += a1, A2 += a2;
}
int main() {
  n = IO::rd(), m = IO::rd();
  FOR(i, 1, n) p[i] = IO::rd();
  FOR(i, 1, m) {
    int u, v, w;
    u = IO::rd(), v = IO::rd(), w = IO::rd();
    add_path(u, v, p[u] + p[v] - w), add_path(v, u, p[u] + p[v] - w);
  }

  FOR(i, 1, n) if (!vis[i]) work(i);
  printf("%lld %lld\n", A1, A2);

  return 0;
}
