#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/

using namespace RA;

const int N = 1000;
vector<pii> E;
int main() {
  int LIM = 100;
  srand(clock());
  int n = r(2, 10);
  FOR(i, 1, n) FOR(j, 1, i - 1) if (r(100) > 50) E.pb({i, j});
  printf("%d %d\n", n, E.size());
  FOR(i, 1, n) printf("%d%c", r(1, LIM), " \n"[i == n]);
  for (pii x : E) printf("%d %d %d\n", x.fi, x.se, r(1, LIM));
  return 0;
}
