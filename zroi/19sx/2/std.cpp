#include <algorithm>
#include <cstdio>
#include <cstring>
using namespace std;
const int mxn = 500010;
int n, m, M, mn, mx, flg = 1, head[mxn], a[mxn], b[mxn], lim[mxn];
long long ansmn, ansmx, sum, suma, sumb;
struct ed {
  int to, nxt, val;
} edge[6000010];
void addedge(int u, int v, int w) { edge[++M] = (ed){v, head[u], w}, head[u] = M; }
void dfs(int u) {
  for (int i = head[u], v, x; i && flg; i = edge[i].nxt)
    if (!a[v = edge[i].to]) {
      a[v] = -a[u], b[v] = edge[i].val - b[u], suma += a[v], sumb += b[v];
      if (a[v] == 1)
        mn = max(mn, -b[v]), mx = min(mx, lim[v] - b[v]);
      else
        mn = max(mn, b[v] - lim[v]), mx = min(mx, b[v]);
      dfs(v);
    } else if (a[v] == -a[u])
      if (b[v] + b[u] != edge[i].val)
        flg = 0;
      else
        ;
    else if (x = a[u] * (edge[i].val - b[u]) - a[v] * b[v], x & 1)
      flg = 0;
    else
      mn = max(mn, x >> 1), mx = min(mx, x >> 1);
}
inline char gc() {
  static char buf[100000], *p1, *p2;
  return p1 == p2 && (p2 = (p1 = buf) + fread(buf, 1, 100000, stdin), p1 == p2)
             ? EOF
             : *p1++;
}
int read() {
  int x = 0;
  char c = gc();
  for (; c < 48 || c > 57; c = gc())
    ;
  for (; c > 47 && c < 58; x = x * 10 + c - 48, c = gc())
    ;
  return x;
}
int main() {
  scanf("%d%d", &n, &m);
  for (int i = 1; i <= n; ++i) sum += (lim[i] = read());
  for (int i = 1, x, y, z; i <= m; ++i)
    x = read(), y = read(), z = read(), addedge(x, y, z), addedge(y, x, z);
  for (int i = 1; i <= n; ++i)
    if (!a[i]) {
      mn = 0, mx = lim[i], suma = 1, sumb = 0, a[i] = 1, dfs(i);
      if (!flg || mn > mx) return puts("NIE"), 0;
      ansmn += min(mn * suma + sumb, mx * suma + sumb);
      ansmx += max(mn * suma + sumb, mx * suma + sumb);
    }
  return printf("%lld %lld\n", sum - ansmx, sum - ansmn), 0;
}
