#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/

unsigned long long L, R, ans;

int p[100], c[100], lp;
void div(unsigned long long x) {
  lp = 0;
  for (unsigned long long i = 2; i * i <= x; i++) {
    if (x % i) continue;
    p[++lp] = i;
    while (x % i == 0) ++c[lp], x /= i;
  }
  if (x > 1) p[++lp] = x, c[lp] = 1;
}
unsigned long long calc(unsigned long long x) {
  div(x);
  unsigned long long res = 0;
  printf("x=%llu\n", x);
  FOR(i, 1, lp) {
    int x = c[i] / 4;
    if (x == 0) continue;
    if (res == 0)
      res = x;
    else
      res = res * x * 8;
  }
  printf("res=%llu\n", res);
  return res;
}

int main() {
  scanf("%llu%llu", &L, &R);
  FOR(i, L, R) ans = ans + calc(i);
  printf("%llu\n", ans);
  return 0;
}
