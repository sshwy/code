#include <bits/stdc++.h>
using namespace std;
#define ld long double
const int N = 100010;
int n, nn;
struct P {
  ld x, y;
} a[N], s[N];
bool cmp(P a, P b) { return a.x < b.x || (a.x == b.x && a.y < b.y); }
bool cmp1(P x, P y) {
  ld a1 = atan2((x.y - a[1].y), (x.x - a[1].x)),
     a2 = atan2((y.y - a[1].y), (y.x - a[1].x));
  if (a1 != a2)
    return a1 < a2;
  else
    return x.x > y.x;
}
P operator+(P a, P b) { return (P){a.x + b.x, a.y + b.y}; }
P operator-(P a, P b) { return (P){a.x - b.x, a.y - b.y}; }
ld crs(P a, P b) { return a.x * b.y - a.y * b.x; }
int main() {
  scanf("%d", &n);
  for (int i = 1, x, t = 0; i <= n; i++) scanf("%d", &x), t += x, a[i] = (P){x, t};
  for (int i = 1; i <= n; i++) a[i].x = a[i].y / a[i].x;
  sort(a + 1, a + n + 1, cmp);
  sort(a + 2, a + n + 1, cmp1);
  s[1] = a[1];
  nn = 1;
  for (int i = 2; i <= n; i++) {
    while (nn > 1 && crs(a[i] - s[nn - 1], s[nn] - s[nn - 1]) >= 0) nn--;
    s[++nn] = a[i];
  }
  ld ans = crs(s[nn], s[1]);
  for (int i = 1; i < nn; i++) ans += crs(s[i], s[i + 1]);
  printf("%.5lf\n", (double)ans / 2);
  return 0;
}
