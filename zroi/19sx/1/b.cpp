// problem:
#include <bits/stdc++.h>
using namespace std;

#define pb push_back
#define mk make_pair
#define lob lower_bound
#define upb upper_bound
#define fst first
#define scd second

typedef long long ll;
typedef unsigned long long ull;
typedef pair<int, int> pii;
typedef pair<ll, ll> pll;

namespace Fread {
  const int MAXN = 1 << 18 | 1;
  char buffer[MAXN], *S, *T;
  inline char getchar() {
    if (S == T) {
      T = (S = buffer) + fread(buffer, 1, MAXN, stdin);
      if (S == T) return EOF;
    }
    return *S++;
  }
} // namespace Fread
#ifdef ONLINE_JUDGE
#define getchar Fread::getchar
#endif
inline int read() {
  int f = 1, x = 0;
  char ch = getchar();
  while (!isdigit(ch)) {
    if (ch == '-') f = -1;
    ch = getchar();
  }
  while (isdigit(ch)) {
    x = x * 10 + ch - '0';
    ch = getchar();
  }
  return x * f;
}
inline ll readll() {
  ll f = 1, x = 0;
  char ch = getchar();
  while (!isdigit(ch)) {
    if (ch == '-') f = -1;
    ch = getchar();
  }
  while (isdigit(ch)) {
    x = x * 10 + ch - '0';
    ch = getchar();
  }
  return x * f;
}

int n, a[505];
int dp[505][505][7];
void solve1() {
  memset(dp, 0x3f, sizeof(dp));
  for (int i = 1; i <= n; ++i) {
    for (int j = 0; j < 7; ++j) dp[i][i][j] = 1;
    dp[i][i][a[i]] = 0;
  }
  for (int len = 2; len <= n; ++len) {
    for (int i = 1; i + len - 1 <= n; ++i) {
      int j = i + len - 1;
      for (int v = 0; v < 7; ++v) {
        for (int k = i; k < j; ++k) {
          for (int v2 = 0; v2 < 7; ++v2) {
            dp[i][j][v] =
                min(dp[i][j][v], dp[i][k][v2] + dp[k + 1][j][v] + (v2 != v) -
                                     (a[k] - v2 == a[k + 1] - v && a[k] != v2));
            dp[i][j][v] =
                min(dp[i][j][v], dp[i][k][v] + dp[k + 1][j][v2] + (v2 != v) -
                                     (a[k] - v == a[k + 1] - v2 && a[k] != v));
          }
          dp[i][j][v] = min(dp[i][j][v], dp[i][k][v] + dp[k + 1][j][a[k]]);
          dp[i][j][v] = min(dp[i][j][v], dp[i][k][a[k + 1]] + dp[k + 1][j][v]);
          int dt = ((v - a[k + 1]) % 7 + 7) % 7;
          int t = ((a[k] + dt) % 7 + 7) % 7;
          dp[i][j][v] = min(
              dp[i][j][v], dp[i][k][t] + dp[k + 1][j][a[k + 1]] + (a[k] != a[k + 1]));
          dt = ((v - a[k]) % 7 + 7) % 7;
          t = ((a[k + 1] + dt) % 7 + 7) % 7;
          dp[i][j][v] =
              min(dp[i][j][v], dp[i][k][a[k]] + dp[k + 1][j][t] + (a[k] != a[k + 1]));
        }
      }
    }
  }
  cout << dp[1][n][0] << endl;
  // int i,j,k;while(cin>>i>>j>>k)cout<<dp[i][j][k]<<endl;
}
int main() {
  n = read();
  for (int i = 1; i <= n; ++i) a[i] = read();
  solve1();
  return 0;
}
/*
5
1 3 2 5 3
*/
