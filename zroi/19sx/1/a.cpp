#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 1e5 + 5;

int n;
int a[N], b[N];

#define double long double
typedef pair<double, double> point;
typedef pair<double, double> vec;
point p[N];
double det(vec a, vec b) { return a.fi * b.se - a.se * b.fi; }
vec operator-(point a, point b) { return {a.fi - b.fi, a.se - b.se}; }

int vis[N];
int s[N], tp;
void andrew() {
  sort(p + 1, p + n + 1);
  s[++tp] = 1;
  FOR(i, 2, n) {
    while (tp > 1 && det(p[s[tp]] - p[s[tp - 1]], p[i] - p[s[tp]]) <= 0) --tp;
    s[++tp] = i;
  }
  FOR(i, 1, tp) vis[s[i]] = 1;
  vis[1] = 0;
  int tt = tp;
  ROF(i, n, 1) if (!vis[i]) {
    while (tp > tt && det(p[s[tp]] - p[s[tp - 1]], p[i] - p[s[tp]]) <= 0) --tp;
    s[++tp] = i;
  }
  assert(s[tp] == 1 && s[1] == 1);
  --tp;
}

int main() {
  scanf("%d", &n);
  FOR(i, 1, n) scanf("%d", &a[i]), b[i] = b[i - 1] + a[i];
  FOR(i, 1, n) p[i] = {b[i], b[i] * 1.0 / a[i]};

  // FOR(i,1,n)printf("%Lf,%Lf\n",p[i].fi,p[i].se);

  andrew();
  s[++tp] = 1;
  // FOR(i,1,tp)printf("%d%c",s[i]," \n"[i==tp]);
  double ans = 0;
  FOR(i, 1, tp - 1) ans += det(p[s[i]], p[s[i + 1]]);
  printf("%.5Lf\n", ans / 2);
  return 0;
}
