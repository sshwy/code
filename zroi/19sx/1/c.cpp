#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
namespace IO {
  char nc() {
    static char bf[100000], *p1 = bf, *p2 = bf;
    return p1 == p2 && (p2 = (p1 = bf) + fread(bf, 1, 100000, stdin), p1 == p2)
               ? EOF
               : *p1++;
  }
  int rd() {
    int res = 0;
    char c = nc();
    while (!isdigit(c)) c = nc();
    while (isdigit(c)) res = res * 10 + c - '0', c = nc();
    return res;
  }
  int tmp[20], lt;
  char OBF[100000], *OBP = OBF;
  void flush() { fwrite(OBF, 1, OBP - OBF, stdout), OBP = OBF; }
  void wrch(char x) {
    if (OBP == OBF + 99999) flush();
    *OBP = x, ++OBP;
  }
  void wr(int x) {
    lt = 0;
    while (x) tmp[++lt] = x % 10, x /= 10;
    while (lt > 0) wrch(tmp[lt] + '0'), --lt;
  }
  void wr(long long x) {
    lt = 0;
    while (x) tmp[++lt] = x % 10, x /= 10;
    while (lt > 0) wrch(tmp[lt] + '0'), --lt;
  }
  void wr(const char *s) {
    while (*s) wrch(*s), ++s;
  }
  void wr(char x) { wrch(x); }
} // namespace IO
/******************heading******************/
const int X = 22, Y = 1e5 + 5, P = 1e9 + 7;

int pw(int a, int m) {
  int res = 1;
  while (m) m & 1 ? res = 1ll * res * a % P : 0, a = 1ll * a * a % P, m >>= 1;
  return res;
}

const int SZ = Y;
struct fenwick {
  long long c[SZ], n;
  fenwick(int _n = SZ - 1) { n = _n; }
  void add(int pos, int v) {
    for (int i = pos; i <= n; i += i & -i) c[i] = c[i] + v;
  }
  int pre(int pos) {
    long long res = 0;
    for (int i = pos; i > 0; i -= i & -i) res = res + c[i];
    return res % P;
  }
} B[22];

int n, m;
int c[Y];
int fac[X + Y], fnv[X + Y], desc[X][Y]; // desc[i,j]: (-j-1) down i

int main() {
  n = IO::rd(), m = IO::rd();

  fac[0] = 1;
  FOR(i, 1, n + 20) fac[i] = 1ll * fac[i - 1] * i % P;
  fnv[n + 20] = pw(fac[n + 20], P - 2);
  ROF(i, n + 20, 1) fnv[i - 1] = fnv[i] * 1ll * i % P;

  FOR(i, 0, n) desc[0][i] = 1;
  FOR(i, 1, 20) FOR(j, 0, n) desc[i][j] = desc[i - 1][j] * 1ll * (-j - i) % P;

  FOR(i, 1, n) c[i] = IO::rd();

  FOR(i, 0, 20) FOR(j, 1, n) B[i].add(j, desc[i][j] * 1ll * c[j] % P);

  FOR(i, 1, m) {
    int o, x, y;
    o = IO::rd(), x = IO::rd(), y = IO::rd();
    if (o == 1) {
      FOR(i, 0, 20) B[i].add(x, desc[i][x] * 1ll * (y - c[x]) % P);
      c[x] = y;
    } else {
      if (x == 0)
        IO::wr(c[y]), IO::wrch('\n');
      else {
        int ans = 0;
        FOR(k, 0, x - 1) {
          int cof = fnv[x - 1 - k] * 1ll * fnv[y + 1 + k] % P * fnv[k] % P;
          ans = (ans + cof * 1ll * B[k].pre(y)) % P;
        }
        ans = ans * 1ll * fac[x + y] % P;
        IO::wr((ans + P) % P), IO::wrch('\n');
      }
    }
  }
  IO::flush();
  return 0;
}
