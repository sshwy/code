#include <algorithm>
#include <cstdio>
#include <cstring>
#define Rint register int
using namespace std;
const int N = 504;
int n, A[N], cnt[7], dp[2][N][N][7], X, Y, Z, ans, cur;
int main() {
  scanf("%d", &n);
  for (Rint i = 1; i <= n; i++) {
    scanf("%d", A + i);
    ++cnt[(A[i] - A[i - 1] + 7) % 7];
  }
  cnt[7 - A[n]]++;
  if (cnt[1] > cnt[6]) {
    X = 1;
    swap(cnt[1], cnt[6]);
  } else
    X = 6;
  if (cnt[2] > cnt[5]) {
    Y = 2;
    swap(cnt[2], cnt[5]);
  } else
    Y = 5;
  if (cnt[3] > cnt[4]) {
    Z = 3;
    swap(cnt[3], cnt[4]);
  } else
    Z = 4;
  ans = cnt[4] + cnt[5] + cnt[6];
  for (Rint i = 0; i <= cnt[6] - cnt[1]; i++, cur ^= 1) {
    for (Rint j = 0; j <= cnt[5] - cnt[2]; j++)
      for (Rint k = 0; k <= cnt[4] - cnt[3]; k++)
        for (Rint l = 0; l < 7; l++) {
          dp[cur ^ 1][j][k][(l + X) % 7] =
              max(dp[cur ^ 1][j][k][(l + X) % 7], dp[cur][j][k][l] + !((l + X) % 7));
          dp[cur][j + 1][k][(l + Y) % 7] =
              max(dp[cur][j + 1][k][(l + Y) % 7], dp[cur][j][k][l] + !((l + Y) % 7));
          dp[cur][j][k + 1][(l + Z) % 7] =
              max(dp[cur][j][k + 1][(l + Z) % 7], dp[cur][j][k][l] + !((l + Z) % 7));
        }
  }
  printf("%d\n", ans - dp[cur ^ 1][cnt[5] - cnt[2]][cnt[4] - cnt[3]][0]);
}
