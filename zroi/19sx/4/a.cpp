// by Sshwy
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 5e5 + 5;

int n, leaf;
int d[N];

vector<int> g[N], L, A;
vector<pii> ans;

int fa[N];
int get(int u) { return fa[u] == u ? u : fa[u] = get(fa[u]); }
void merge(int u, int v) { fa[get(u)] = get(v); }

int sz[N], core_mx, core;
void dfs(int u, int p) {
  sz[u] = (d[u] == 1);
  int mx = 0;
  for (int v : g[u])
    if (v != p) dfs(v, u), sz[u] += sz[v], mx = max(mx, sz[v]);
  mx = max(mx, leaf - sz[u]);
  if (core_mx > mx) core_mx = mx, core = u;
}
int key[N];
void dfs2(int u, int p, int tag) {
  if (g[u].size() == 1) L.pb(u), merge(u, tag), key[tag] = u;
  for (int v : g[u])
    if (v != p) dfs2(v, u, tag);
}

int tot = 0;

void print_data() {
  printf("%d\n", n);
  FOR(i, 1, n) for (int j : g[i]) if (i < j) {
    ++tot;
    if (1 <= tot && tot <= 10) printf("%d %d ", i, j);
  }
  exit(0);
}
void go() {
  leaf = 0;
  scanf("%d", &n);
  FOR(i, 1, n) g[i].clear(), d[i] = 0;
  ans.clear();
  FOR(i, 1, n - 1) {
    int x, y;
    scanf("%d%d", &x, &y);
    g[x].pb(y), g[y].pb(x);
    d[x]++, d[y]++;
  }
  FOR(i, 1, n) if (d[i] == 1) leaf++;
  core = 1, core_mx = n;
  dfs(1, 0);
  L.clear();
  FOR(i, 1, n) fa[i] = i;
  for (int u : g[core]) dfs2(u, core, u);
  // printf("core=%d\n",core);
  for (int i = 0, j = ((int)L.size() + 1) / 2; j < L.size(); i++, j++) {
    ans.pb({L[i], L[j]});
    merge(L[i], L[j]);
    key[get(L[i])] = L[i];
  }
  if (L.size() & 1) {
    ans.pb({L[L.size() / 2], L[0]});
    key[get(L[L.size() / 2])] = L[L.size() / 2];
  }
  if (*max_element(d + 1, d + n + 1) - 1 > ((int)L.size() + 1) / 2) {
    A.clear();
    for (int u : g[core]) A.pb(key[u]);
    FOR(i, 1, (int)A.size() - 1) {
      if (get(A[i - 1]) != get(A[i])) {
        ans.pb({A[i - 1], A[i]});
        // printf("- %d %d\n",A[i-1],A[i]);
        merge(A[i - 1], A[i]);
      }
    }
  }
  printf("%d\n", ans.size());
  for (pii x : ans)
    printf("%d %d\n", x.fi, x.se),
        assert(x.fi != x.se && 1 <= x.fi && x.fi <= n && 1 <= x.se && x.se <= n);
  // for(pii
  // x:ans)if(!(x.fi!=x.se&&1<=x.fi&&x.fi<=n&&1<=x.se&&x.se<=n))print_data();
}
int main() {
  int t;
  scanf("%d", &t);
  FOR(i, 1, t) go();
  return 0;
}
