// by Sshwy
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 305, P = 1e9 + 7;

int n;
int g[N][N], sbinom[N][N];
int c[N], cl[N], ban[N];
int G[N], cnt[N]; // cnt[i]表示颜色i的出现次数

int main() {
  scanf("%d", &n);
  FOR(i, 1, n) FOR(j, 1, n) { scanf("%d", g[i] + j); }

  FOR(i, 1, n) {
    sbinom[i][0] = sbinom[i][i] = 1;
    FOR(j, 1, i - 1)
    sbinom[i][j] = (sbinom[i - 1][j] + sbinom[i - 1][j - 1]) % P;
  }
  FOR(i, 1, n) {
    FOR(j, 1, n) {
      sbinom[i][j] = (sbinom[i][j] + sbinom[i][j - 1]) % P;
      // printf("sbinom(%d,%d)=%d\n",i,j,sbinom[i][j]);
    }
  }

  FOR(i, 1, n - 1) {
    FOR(j, 1, n) if (!ban[j]) {
      int col = 0;
      FOR(k, 1, n) if (!ban[k] && k != j) {
        if (!col)
          col = g[j][k];
        else if (col != g[j][k])
          col = -1;
      }
      if (col != -1) {
        c[i] = j;
        cl[i] = col;
        ban[j] = 1;
        // printf("c[%d]=%d,cl=%d\n",i,c[i],cl[i]);
        break;
      }
    }
  }

  G[1] = n;
  FOR(i, 1,
      n) { //把c[i]做为最后加入的点。相当于c[1]..c[i-1]向c[i]连 **各自** 的颜色

    FOR(j, 2, n) { // G[j]
      int x = 1;   //只选择最后一个点，即c[i]

      FOR(k, 1, 300) { // color
        if (cnt[k]) {
          x = 1ll * x * sbinom[cnt[k]][j - 1] % P; //在cnt[k]里选择最多j-1个点的方案数
        }
      }

      G[j] = (G[j] + x) % P;
    }

    cnt[cl[i]]++;
  }
  ROF(i, n, 2) G[i] = (G[i] - G[i - 1] + P) % P;

  int ans = 0;
  FOR(i, 1, n) ans = (ans + G[i] * 1ll * i) % P;
  printf("%d\n", ans);
  return 0;
}
