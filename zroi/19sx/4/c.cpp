// by Sshwy
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 505;
const int oo = 0x3f3f3f3f;

int n;
int x[N], y[N], vx[N], vy[N];

// KM algorithm
int Lx[N], Ly[N]; //左部右部的顶标 (Label)
bool S[N], T[N];  //标记，是否属于左部的相等子图，右部相等子图
int mtc[N];       //记录右部点的匹配点
int w[N][N];      //边权
int inc[N];       //右部的点顶标的增量

bool dfs(int u) { //寻找増广路。u是左部点
  S[u] = 1;
  FOR(v, 1, n) if (!T[v]) {
    int d = Lx[u] + Ly[v] - w[u][v];
    if (d == 0) { //相等子图上的边
      T[v] = 1;
      if (mtc[v] == -1 || dfs(mtc[v])) { //増广
        mtc[v] = u;
        return 1;
      }
    } else { // u在S中。那么如果v不在T中，这条边就有可能被加入到相等子图中
      inc[v] = min(inc[v], d);
    }
  }
  return 0;
}
inline void update() {
  int x = oo;
  FOR(i, 1, n) if (!T[i]) x = min(x, inc[i]);
  FOR(i, 1, n) {
    if (S[i]) Lx[i] -= x;
    if (T[i])
      Ly[i] += x;
    else
      inc[i] -= x;
  }
}
inline void KM() {
  FOR(i, 1, n) Lx[i] = Ly[i] = 0, mtc[i] = -1;
  FOR(i, 1, n) FOR(j, 1, n) Lx[i] = max(Lx[i], w[i][j]);
  FOR(i, 1, n) {
    fill(inc + 1, inc + n + 1, oo);
    while (1) {
      fill(S, S + n + 1, 0), fill(T, T + n + 1, 0);
      if (dfs(i))
        break;
      else
        update(); //更新顶标
    }
  }
}
int ans[N];
int main() {

  scanf("%d", &n);
  FOR(i, 1, n) scanf("%d%d", &x[i], &y[i]);
  FOR(i, 1, n) scanf("%d%d", &vx[i], &vy[i]);

  FOR(i, 1, n) FOR(j, 1, n) w[i][j] = 1ll * x[i] * vx[j] + 1ll * y[i] * vy[j];

  KM();

  puts("Yes");
  FOR(i, 1, n) ans[mtc[i]] = i;
  FOR(i, 1, n) printf("%d ", ans[i]);

  return 0;
}
