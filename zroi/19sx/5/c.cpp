// by Sshwy
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int SZ = 2e5 + 500, ALP = 26;
const int N = 2e5 + 5;

struct fenwick {
  int c[SZ], n, sum;
  void resize(int _n) { n = _n; }
  fenwick(int _n = SZ - 1) { n = _n, sum = 0; }
  void add(int pos, int v) {
    sum += v;
    for (int i = pos; i <= n; i += i & -i) c[i] += v;
  }
  int pre(int pos) {
    int res = 0;
    for (int i = pos; i > 0; i -= i & -i) res += c[i];
    return res;
  }
} B;

int n, q;
vector<pii> qry[N];
int ans[N];

namespace seg {
  int mx[N << 2];
  void assign(int pos, int v, int u = 1, int l = 1, int r = n + 1) { // n+1 == dfn
    if (l == r) return mx[u] = v, void();
    int mid = (l + r) >> 1;
    if (pos <= mid)
      assign(pos, v, u << 1, l, mid);
    else
      assign(pos, v, u << 1 | 1, mid + 1, r);
    mx[u] = max(mx[u << 1], mx[u << 1 | 1]);
  }
  int query_max(int L, int R, int u = 1, int l = 1, int r = n + 1) {
    if (R < l || r < L) return 0;
    if (L <= l && r <= R) return mx[u];
    int mid = (l + r) >> 1;
    return max(
        query_max(L, R, u << 1, l, mid), query_max(L, R, u << 1 | 1, mid + 1, r));
  }
} // namespace seg
struct PAM {
  /*
   * 0 号结点表示 0 结点
   * 1 号结点表示 -1 结点，长度为 -1
   * last 记录上一次插入的字符所在结点的编号
   */
  int tot, last;
  int len[SZ], tr[SZ][ALP], fail[SZ];
  int s[SZ], ls; // 字符串的内容 -'0'
  int pos[SZ];   // s[i]对应的pam结点
  int tp[SZ];    //上一个等差数列的链底；等差数列链顶
  int diff[SZ];  //差
  int newnode(int l) {
    ++tot, len[tot] = l, fail[tot] = 0;
    FOR(i, 0, ALP - 1) tr[tot][i] = 0;
    return tot;
  }
  void clear() {
    tot = -1, newnode(0), newnode(-1), fail[0] = 1, last = 0,
    s[ls = 0] = -1; //!!!!!! 减掉'a'后0就不是非匹配字符了，所以要整成-1
  }
  PAM() { clear(); }
  int getfail(int u) {
    while (s[ls - len[u] - 1] != s[ls]) u = fail[u];
    return u;
  }
  void insert(char c) {
    s[++ls] = (c -= 'a');
    int cur = getfail(last);
    if (!tr[cur][c]) { // 如果没有转移就添加
      int u = newnode(len[cur] + 2);
      fail[u] = tr[getfail(fail[cur])][c];
      tr[cur][c] = u;

      diff[u] = len[u] - len[fail[u]];
      if (diff[u] == diff[fail[u]])
        tp[u] = tp[fail[u]];
      else
        tp[u] = u;
    }
    last = tr[cur][c];
    pos[ls] = last;
  }
  vector<int> g[N];
  int L[N], R[N], dfn = 0;
  void make() { FOR(i, 2, tot) g[fail[i]].pb(i); }
  void dfs(int u) {
    L[u] = ++dfn;
    for (int v : g[u]) dfs(v);
    R[u] = dfn;
  }
  void work() {
    FOR(i, 1, n) {
      for (int u = pos[i]; u; u = fail[tp[u]]) {
        int t = seg::query_max(L[u], R[u]);
        if (t) B.add(t - len[u] + 1, -1); //等差数列最后一个
        B.add(i - len[tp[u]] + 1, 1);     //等差数列第一个
      }
      seg::assign(L[pos[i]], i); // i这个状态（以及其祖先状态）出现在i这个位置
      for (pii x : qry[i]) ans[x.se] = B.sum - B.pre(x.fi - 1);
    }
  }
} pam;

char s[N];

int main() {
  scanf("%d%d", &n, &q);
  scanf("%s", s + 1);

  B.resize(n);

  FOR(i, 1, q) {
    int l, r;
    scanf("%d%d", &l, &r);
    qry[r].pb({l, i});
  }

  FOR(i, 1, n) pam.insert(s[i]);
  pam.make();
  pam.dfs(0);
  pam.work();

  FOR(i, 1, q) printf("%d\n", ans[i]);
  return 0;
}
