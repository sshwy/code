#include <algorithm>
#include <cstdio>
#define mxn 200010
#define LL long long
using namespace std;
int n, Q, sl, flg, a[mxn];
struct seg {
  int l, r;
  LL v;
  int ans() { return l + (!v); }
} ret, tre[mxn << 2];
seg operator+(seg x, seg y) {
  if (x.v & 1)
    x.v--, x.r++;
  else
    y.l++;
  if (y.l & 1) {
    int t = min(x.r, y.r);
    x.r -= t;
    y.r -= t;
    y.l += t << 1;
    if (x.r) x.r--, y.l++, y.v++;
  }
  return (seg){x.l + y.l, x.r + y.r, x.v + y.v};
}
inline char gc() {
  static char buf[1 << 20], *S, *T;
  if (S == T) {
    T = (S = buf) + fread(buf, 1, 1 << 20, stdin);
    if (S == T) return EOF;
  }
  return *S++;
}
inline int rd() {
  sl = 0;
  char ch = gc();
  while (ch < '0' || '9' < ch) ch = gc();
  while ('0' <= ch && ch <= '9') sl = sl * 10 + ch - '0', ch = gc();
  return sl;
}
#define mid ((l + r) >> 1)
void build(int no, int l, int r) {
  if (l == r) {
    tre[no].v = rd();
    return;
  }
  build(no << 1, l, mid);
  build(no << 1 | 1, mid + 1, r);
  tre[no] = tre[no << 1] + tre[no << 1 | 1];
}
void change(int no, int l, int r, int p, int v) {
  if (l == r) {
    tre[no].v = v;
    return;
  }
  if (p <= mid)
    change(no << 1, l, mid, p, v);
  else
    change(no << 1 | 1, mid + 1, r, p, v);
  tre[no] = tre[no << 1] + tre[no << 1 | 1];
}
void query(int no, int l, int r, int L, int R) {
  if (L <= l && r <= R) {
    if (flg)
      ret = ret + tre[no];
    else
      ret = tre[no], flg = 1;
    return;
  }
  if (L <= mid) query(no << 1, l, mid, L, R);
  if (R > mid) query(no << 1 | 1, mid + 1, r, L, R);
}
int main() {
  n = rd();
  Q = rd();
  int l, r, typ;
  build(1, 1, n);
  while (Q--) {
    typ = rd();
    l = rd();
    r = rd();
    if (typ & 1)
      change(1, 1, n, l, r);
    else
      flg = 0, query(1, 1, n, l, r), printf("%d\n", ret.ans());
  }
  return 0;
}
