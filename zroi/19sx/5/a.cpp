// by Sshwy
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
namespace IO {
  char nc() {
    static char bf[100000], *p1 = bf, *p2 = bf;
    return p1 == p2 && (p2 = (p1 = bf) + fread(bf, 1, 100000, stdin), p1 == p2)
               ? EOF
               : *p1++;
  }
  int rd() {
    int res = 0;
    char c = nc();
    while (!isdigit(c)) c = nc();
    while (isdigit(c)) res = res * 10 + c - '0', c = nc();
    return res;
  }
  int tmp[20], lt;
  char OBF[100000], *OBP = OBF;
  void flush() { fwrite(OBF, 1, OBP - OBF, stdout), OBP = OBF; }
  void wrch(char x) {
    if (OBP == OBF + 99999) flush();
    *OBP = x, ++OBP;
  }
  void wr(int x) {
    lt = 0;
    while (x) tmp[++lt] = x % 10, x /= 10;
    while (lt > 0) wrch(tmp[lt] + '0'), --lt;
  }
  void wr(long long x) {
    lt = 0;
    while (x) tmp[++lt] = x % 10, x /= 10;
    while (lt > 0) wrch(tmp[lt] + '0'), --lt;
  }
  void wr(const char *s) {
    while (*s) wrch(*s), ++s;
  }
  void wr(char x) { wrch(x); }
} // namespace IO
/*}}}*/
/******************heading******************/
const int N = 5e5 + 5, P = 998244353;

int pw(int a, int m) {
  int res = 1;
  while (m) m & 1 ? res = 1ll * res * a % P : 0, a = 1ll * a * a % P, m >>= 1;
  return res;
}
int I1e6;

struct qxx {
  int nex, t, v;
};
qxx e[N * 2];
int h[N], le = 1;
void add_path(int f, int t, int v) { e[++le] = (qxx){h[f], t, v}, h[f] = le; }
#define FORe(i, _u, _v, _w) \
  for (int i = h[_u], _v, _w; _v = e[i].t, _w = e[i].v, i; i = e[i].nex)

int n, ans;

int f[N], g[N];
void dfs1(int u, int p) {
  FORe(i, u, v, x) if (v != p) {
    dfs1(v, u);
    f[u] = (f[u] + 2ll * x * (f[v] + g[v]) % P + x) % P;
    g[u] = (g[u] + 2ll * x * g[v] % P + x) % P;
  }
}
void dfs2(int u, int p) {
  // printf("f[%d]=%d\n",u,f[u]);

  ans = (ans + f[u]) % P;

  FORe(i, u, v, x) if (v != p) {
    int ix = (P + 1 - x) % P;
    // u -> v
    int fu = f[u], gu = g[u], fv = f[v], gv = g[v];

    f[u] = (f[u] - 2ll * x * (f[v] + g[v]) % P - x) % P;
    g[u] = (g[u] - 2ll * x * g[v] % P - x) % P;

    f[v] = (f[v] + 2ll * ix * (f[u] + g[u]) % P + ix) % P;
    g[v] = (g[v] + 2ll * ix * g[u] % P + ix) % P;

    dfs2(v, u);

    f[u] = fu, g[u] = gu, f[v] = fv, g[v] = gv;
  }
}

int main() {
  I1e6 = pw(1e6, P - 2);
  n = IO::rd();
  FOR(i, 2, n) {
    int u, v, p;
    u = IO::rd(), v = IO::rd();
    p = IO::rd();
    if (p == 1)
      IO::rd();
    else
      p = IO::rd(), p = 1ll * p * I1e6 % P;
    add_path(u, v, p);
    add_path(v, u, (P + 1 - p) % P);
  }
  dfs1(1, 0);
  dfs2(1, 0);
  ans = (ans + P) % P;
  printf("%d\n", ans);
  return 0;
}
