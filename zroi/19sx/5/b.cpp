// by Sshwy
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 2e5 + 5, SZ = N << 2, INF = 0x3f3f3f3f;

int n, q;
int a[N];

struct atom {
  int c0, c1, trs;
  atom() { c0 = c1 = trs = -1; } //-1表示不存在
  atom(int C0, int C1, int Trs) { c0 = C0, c1 = C1, trs = Trs; }
  friend bool operator==(atom a, atom b) {
    return a.c0 == b.c0 && a.c1 == b.c1 && a.trs == b.trs;
  }
  friend atom operator+(atom a, atom b) {
    if (a == atom()) return b;
    if (b == atom()) return a;
    // printf("a=(%d,%d,%d),b=(%d,%d,%d)\n",a.c0,a.c1,a.trs,b.c0,b.c1,b.trs);
    atom c;
    c.trs = a.trs / 2 * 2 + b.trs, a.trs %= 2;
    a.trs == 0 ? ++b.c0 : ++a.c1;
    if (b.c0 & 1) {            //合并1
      if (b.c1 == 0 && a.c1) { //丢一个到垃圾
        a.c1--, c.trs++, b.c0++;
        c.c1 = a.c1;
        c.c0 = a.c0 + b.c0;
      } else {
        int t = min(a.c1, b.c1);
        c.trs += t * 2;
        c.c1 = a.c1 + b.c1 - t * 2;
        c.c0 = a.c0 + b.c0 + t * 2;
        if (a.c1 > b.c1) { //前面还剩下了一些1
          c.c1--, c.c0++, c.trs++;
        }
      }
    } else {
      c.c1 = a.c1 + b.c1;
      c.c0 = a.c0 + b.c0;
    }
    // printf("c=(%d,%d,%d)\n",c.c0,c.c1,c.trs);
    return c;
  }
  friend atom operator+=(atom &a, atom b) { return a = a + b, a; }
};

atom I; //(-1,-1,-1)
atom A[SZ];
int mx[SZ];
void build(int u = 1, int l = 1, int r = n) {
  if (l == r) return A[u] = atom(0, 0, a[l]), mx[u] = a[l], void();
  int mid = (l + r) >> 1;
  build(u << 1, l, mid), build(u << 1 | 1, mid + 1, r);
  A[u] = A[u << 1] + A[u << 1 | 1], mx[u] = max(mx[u << 1], mx[u << 1 | 1]);
}
void assign(int pos, int v, int u = 1, int l = 1, int r = n) {
  if (l == r) return A[u] = atom(0, 0, v), mx[u] = v, void();
  int mid = (l + r) >> 1;
  if (pos <= mid)
    assign(pos, v, u << 1, l, mid);
  else
    assign(pos, v, u << 1 | 1, mid + 1, r);
  A[u] = A[u << 1] + A[u << 1 | 1], mx[u] = max(mx[u << 1], mx[u << 1 | 1]);
}
atom query_atom(int L, int R, int u = 1, int l = 1, int r = n) {
  if (R < l || r < L) return I;
  if (L <= l && r <= R) return A[u];
  int mid = (l + r) >> 1;
  return query_atom(L, R, u << 1, l, mid) + query_atom(L, R, u << 1 | 1, mid + 1, r);
}
int query(int L, int R) {
  atom x = query_atom(L, R);
  // printf("query(%d,%d)\n",L,R);
  // FOR(i,L,R)printf("%d%c",a[i]," \n"[i==R]);
  // printf("x=(%d,%d,%d)\n",x.c0,x.c1,x.trs);
  int res = x.c0;
  if (x.trs == 0) res++;
  return res;
}

int main() {
  scanf("%d%d", &n, &q);
  FOR(i, 1, n) scanf("%d", &a[i]);
  build();
  FOR(i, 1, q) {
    int op, x, y;
    scanf("%d%d%d", &op, &x, &y);
    if (op == 1) {
      assign(x, y);
      a[x] = y;
    } else {
      printf("%d\n", query(x, y));
    }
  }
  return 0;
}
