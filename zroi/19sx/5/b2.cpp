#include <bits/stdc++.h>
using namespace std;
#define li long long
#define gc getchar()
#define pc putchar
li read() {
  li x = 0, y = 0, c = gc;
  while (!isdigit(c)) y = c, c = gc;
  while (isdigit(c)) x = (x << 1) + (x << 3) + (c ^ '0'), c = gc;
  return y == '-' ? -x : x;
}
void print(li q) {
  if (q < 0) {
    pc('-');
    q = -q;
  }
  if (q >= 10) print(q / 10);
  pc(q % 10 + '0');
}
li s1 = 19260817, s2 = 23333333, s3 = 998244853, srd;
li rd() { return srd = (srd * s1 + s2 + rand()) % s3; }
int n, m, a[200010];
void file() {
  freopen(".in", "r", stdin);
  freopen(".out", "w", stdout);
}
// const int mo = 998244353;
// const int mo = 1000000007;
struct tree {
  int p1, p2;
  li p3;
} t[800010];
inline tree operator+(tree q, tree w) {
  tree as;
  if (q.p3 & 1) {
    ++q.p2;
    as.p3 = q.p3 + w.p3 - 1;
  } else {
    ++w.p1;
    as.p3 = q.p3 + w.p3;
  }
  if (w.p1 & 1) {
    int tmp = min(q.p2, w.p2);
    as.p1 = q.p1 + w.p1 + 2 * tmp;
    as.p2 = q.p2 + w.p2 - 2 * tmp;
    as.p3 += 2 * tmp;
    if (q.p2 > w.p2) ++as.p1, --as.p2, ++as.p3;
  } else {
    as.p1 = q.p1 + w.p1;
    as.p2 = q.p2 + w.p2;
  }
  return as;
}
#define ls q << 1
#define rs q << 1 | 1
#define ln ls, l, mid
#define rn rs, mid + 1, r
#define md int mid = l + r >> 1
void build(int q, int l, int r) {
  if (l == r) {
    t[q].p3 = a[l];
    return;
  }
  md;
  build(ln);
  build(rn);
  t[q] = t[ls] + t[rs];
}

void xg(int q, int l, int r, int ax, int x) {
  if (l == r) {
    t[q].p3 = a[l] = x;
    return;
  }
  md;
  if (mid >= ax)
    xg(ln, ax, x);
  else
    xg(rn, ax, x);
  t[q] = t[ls] + t[rs];
}
tree cx(int q, int l, int r, int al, int ar) {
  if (l >= al && r <= ar) return t[q];
  md;
  if (mid < al) return cx(rn, al, ar);
  if (mid >= ar) return cx(ln, al, ar);
  return cx(ln, al, ar) + cx(rn, al, ar);
}
int main() {
  srand(time(0));
  rd();
  // file();
  int i, j, op, u, v;
  n = read();
  m = read();
  for (i = 1; i <= n; ++i) a[i] = read();
  build(1, 1, n);
  for (i = 1; i <= m; ++i) {
    op = read();
    u = read();
    v = read();
    if (op == 1)
      xg(1, 1, n, u, v);
    else {
      tree as = cx(1, 1, n, u, v);
      print(as.p1 + !as.p3);
      pc('\n');
    }
  }
  return 0;
}
