// by Sshwy
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA
const int N = 5e4 + 5;
int n, T;
int a[N], b[N];

vector<vector<int>> split() {
  FOR(i, 1, n) b[a[i]] = i;
  vector<vector<int>> res;
  vector<int> cur;
  FOR(i, 1, n) {
    if (i == 1 || b[i - 1] < b[i])
      cur.pb(b[i]);
    else {
      res.pb(cur);
      cur.clear();
      cur.pb(b[i]);
    }
  }
  res.pb(cur);
  return res;
}
bool vis[N];
int na[N];
void operation(vector<vector<int>> sp) {
  memset(vis, 0, sizeof(vis));
  for (int i = 1; i < sp.size(); i += 2)
    for (auto x : sp[i]) vis[x] = 1;
  int la = 0;
  FOR(i, 1, n) if (vis[i] == 0) na[++la] = a[i];
  FOR(i, 1, n) if (vis[i]) na[++la] = a[i];
  assert(la == n);
  FOR(i, 1, n) a[i] = na[i];
}
void pa() { FOR(i, 1, n) printf("%d%c", a[i], " \n"[i == n]); }
int main() {
  scanf("%d%d", &n, &T);
  FOR(i, 1, n) scanf("%d", &a[i]);
  auto sp = split();
  int ans = 0;
  while ((1 << ans) < sp.size()) ++ans;
  printf("%d\n", ans);
  if (T) pa();
  while (sp.size() > 1) {
    operation(sp);
    if (T) pa();
    sp = split();
  }
  return 0;
}
