// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

const int SZ = 1 << 21, P = 998244353;

int pw(int a, int m) {
  if (m < 0) m += P - 1;
  int res = 1;
  while (m) m & 1 ? res = 1ll * res * a % P : 0, a = 1ll * a * a % P, m >>= 1;
  return res;
}
int tr[SZ];
int init(int n) { // n 指多项式的长度，非度数
  int len = 1;
  while (len < n) len <<= 1;
  FOR(i, 0, len - 1) tr[i] = tr[i >> 1] >> 1 | ((i & 1) * (len / 2));
  return len;
}
int dft_w[2][19][SZ];
void init_G() {
  FOR(j2, 0, 18) {
    int j = 1 << j2, wn = pw(3, (P - 1) / (j << 1)), *W = dft_w[0][j2];
    W[0] = 1;
    FOR(i, 1, j - 1) W[i] = W[i - 1] * 1ll * wn % P;
    wn = pw(3, -(P - 1) / (j << 1)), W = dft_w[1][j2];
    W[0] = 1;
    FOR(i, 1, j - 1) W[i] = W[i - 1] * 1ll * wn % P;
  }
}
void dft(int *f, int len, int tag) {
  FOR(i, 0, len - 1) if (i < tr[i]) swap(f[i], f[tr[i]]);
  FOR(i, 0, len - 1) f[i] = (f[i] + P) % P;
  for (int j = 1, j2 = 0; j < len; j <<= 1, ++j2)
    for (int i = 0, *W = dft_w[tag == -1][j2] /*pw(3,(P-1)/(j<<1)*tag)*/; i < len;
         i += j << 1)
      for (int k = i, u, v, w = 1; k < i + j; k++, w = W[k - i] /*1ll*w*wn%P*/)
        u = f[k], v = f[k + j] * 1ll * w % P, f[k] = u + v, f[k] -= f[k] >= P ? P : 0,
        f[k + j] = u - v, f[k + j] += f[k + j] < 0 ? P : 0;
  if (tag == -1)
    for (int i = 0, ilen = pw(len, P - 2); i < len; i++) f[i] = 1ll * f[i] * ilen % P;
}
int inv_h[SZ];
void inv(const int *f, int *g, int n) { // f*g=1 mod x^n
  if (n == 1) return g[0] = pw(f[0], P - 2), void();
  inv(f, g, (n + 1) / 2);

  int len = init(2 * n);
  FOR(i, 0, len - 1) inv_h[i] = 0;
  FOR(i, (n + 1) / 2, len - 1) g[i] = 0;

  FOR(i, 0, n - 1) inv_h[i] = f[i];
  dft(inv_h, len, 1), dft(g, len, 1);
  FOR(i, 0, len - 1) inv_h[i] = 1ll * inv_h[i] * g[i] % P * g[i] % P;
  FOR(i, 0, len - 1) g[i] = (g[i] * 2ll - inv_h[i] + P) % P;
  dft(g, len, -1);

  // FOR(i,n,len-1)g[i]=0;
}
// make sure to init_G before dft!

int fac[SZ], fnv[SZ], f[SZ];

void work(int n) {
  fac[0] = 1;
  FOR(i, 1, n) fac[i] = fac[i - 1] * 1ll * i % P;
  fnv[n] = pw(fac[n], P - 2);
  ROF(i, n, 1) fnv[i - 1] = fnv[i] * 1ll * i % P;

  FOR(i, 0, n) f[i] = fnv[i] * (P - 2ll) % P;
  f[0] = (f[0] + 3) % P;

  int len = init(n * 2);
}

int main() {
  int t;
  scanf("%d", &t);

  init_G();

  work(1e6 + 5);

  return 0;
}
