// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

const int N = 1e5 + 5;
int n;

struct qxx {
  int nex, t, v;
} e[N * 2];
int h[N], le = 1;
void ae(int f, int t, int v) { e[++le] = (qxx){h[f], t, v}, h[f] = le; }

int val[N], ans[N];

int dfs(int u, int p, int cur) {
  int res = cur;
  for (int i = h[u]; i; i = e[i].nex) {
    int v = e[i].t, id = e[i].v;
    if (v == p) continue;
    int x = dfs(v, u, cur ^ val[id]);
    res = min(res, x);
    ans[id] = min(ans[id], x);
  }
  return res;
}

int main() {
  freopen("wound.in", "r", stdin);
  freopen("wound.out", "w", stdout);
  scanf("%d", &n);
  FOR(i, 1, n - 1) {
    int x, y, w;
    scanf("%d%d%d", &x, &y, &w);
    ae(x, y, i), ae(y, x, i);
    val[i] = w;
    ans[i] = w;
  }
  FOR(i, 1, n) dfs(i, i, 0);
  FOR(i, 1, n - 1) printf("%d\n", ans[i]);
  return 0;
}
