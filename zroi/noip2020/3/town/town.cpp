// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

const int N = 2e7 + 5;
bool bp[N];
int pn[N], lp;
int g[N];

void sieve(int lim) {
  bp[0] = bp[1] = 1;
  FOR(i, 2, lim) {
    if (!bp[i]) pn[++lp] = i, g[i] = i;
    FOR(j, 1, lp) {
      if (i * pn[j] > lim) break;
      bp[i * pn[j]] = 1;
      g[i * pn[j]] = pn[j];
      if (i % pn[j] == 0) break;
    }
  }
}
int main() {
  freopen("town.in", "r", stdin);
  freopen("town.out", "w", stdout);

  int n;
  scanf("%d", &n);

  long long ans = 0, lim = n * 1ll * n;
  int j = 0;
  ROF(i, n, 1) {
    while (i * 1ll * i + (j + 1) * 1ll * (j + 1) <= lim) ++j;
    ans += j;
  }
  // printf("%lld\n",ans);
  sieve(n);

  FOR(i, 2, n) {
    int x = i;
    long long tot = 1;
    while (x > 1) {
      int a = g[x], b = 0;
      while (x % a == 0) x /= a, ++b;
      // printf("a %d b %d\n",a,b);
      if (a % 4 == 1) {
        tot *= b * 2ll + 1;
      } else {
      }
    }
    --tot;
    // printf("i %d tot %d\n",i,tot);
    ans -= tot;
  }
  ans *= 4;

  printf("%lld\n", ans);

  return 0;
}
