// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

int main() {
  freopen("town.in", "r", stdin);
  // freopen("town.out","w",stdout);

  int n;
  scanf("%d", &n);
  long long lim = n * 1ll * n;

  int j = 0;
  long long ans = 0;
  ROF(i, n, 1) {
    while (i * 1ll * i + (j + 1) * 1ll * (j + 1) <= lim) ++j;
    long long r = 0;
    FOR(k, 1, j) {
      while (r * r < i * 1ll * i + k * 1ll * k) ++r;
      if (r * r == i * 1ll * i + k * 1ll * k) {
        printf("i %d k %d\n", i, k);
      } else {
        ans++;
      }
    }
  }
  ans *= 4;
  printf("%lld\n", ans);

  return 0;
}
