// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

long long binom[20][20];
long long c2(int *c, int tot) {
  long long res = 0;
  FOR(c7, 0, tot) { // 7 的出现次数
    int rest = tot - c7;
    long long f[12][20] = {{0}};
    f[0][0] = 1;
    FOR(i, 1, 10) { // i-1
      if (i - 1 == 7) {
        ROF(k, rest, 0) f[i][k] = f[i - 1][k];
        continue;
      }
      FOR(j, 0, rest) { // i-1 的出现次数
        if (j + c[i - 1] == c7 + c[7]) continue;
        ROF(k, rest, j) if (f[i - 1][k - j]) { //背包大小
          f[i][k] = (f[i][k] + f[i - 1][k - j] * binom[k][j]);
        }
      }
    }
    res += f[10][rest] * binom[tot][c7];
  }
  return res;
}
long long calc(const long long x) {
  int a[20] = {0}, la = 0;
  long long x2 = x;
  while (x2) a[++la] = x2 % 10, x2 /= 10;
  reverse(a + 1, a + la + 1);
  long long res = 0;
  FOR(i, 1, la - 1) FOR(j, 1, 9) {
    int c[10] = {0};
    c[j] = 1;
    res += c2(c, i - 1);
  }
  FOR(i, 1, la) {
    int lj = i == 1 ? 1 : 0, rj = i == la ? a[i] : a[i] - 1;
    FOR(j, lj, rj) {
      int c[10] = {0};
      FOR(k, 1, i - 1) c[a[k]]++;
      c[j]++;
      res += c2(c, la - i);
    }
  }
  return res;
}
int main() {
  freopen("seven.in", "r", stdin);
  freopen("seven.out", "w", stdout);

  binom[0][0] = 1;
  FOR(i, 1, 19) FOR(j, 1, 19) {
    binom[i][0] = 1;
    binom[i][j] = binom[i - 1][j - 1] + binom[i - 1][j];
  }

  long long L, R;
  scanf("%lld%lld", &L, &R);
  long long res = calc(R) - calc(L - 1);

  printf("%lld\n", res);
  return 0;
}
