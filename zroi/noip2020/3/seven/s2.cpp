// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

long long L, R;
bool check(long long x) {
  int c[10] = {0};
  while (x) c[x % 10]++, x /= 10;
  FOR(i, 0, 9) if (i != 7 && c[i] == c[7]) return false;
  return true;
}
int main() {
  scanf("%lld%lld", &L, &R);
  long long ans = 0;
  for (long long i = L; i <= R; i++)
    if (check(i)) ans++;
  printf("%lld\n", ans);
  return 0;
}
