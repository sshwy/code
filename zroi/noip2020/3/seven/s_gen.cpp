// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  long long rnd(long long p) { return 1ll * rand() * rand() % p; }
  long long rnd(long long L, long long R) { return rnd(R - L + 1) + L; }
} // namespace RA

using namespace RA;

int main() {
  srand(clock() + time(0));
  long long L = rnd(1, 1e12);
  long long R = rnd(1, 1e12);
  if (L > R) swap(L, R);
  R = min(R, L + 100000);
  if (L > R) swap(L, R);
  printf("%lld %lld\n", L, R);
  return 0;
}
