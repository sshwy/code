g++ seven.cpp -o .u
g++ s2.cpp -o .v
g++ s_gen.cpp -o .w

while true; do 
  ./.w > seven.in
  ./.u
  ./.v < seven.in > .y
  if diff seven.out .y; then
    echo AC
  else
    echo WA
    break
  fi
done
