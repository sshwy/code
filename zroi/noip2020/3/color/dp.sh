g++ color.cpp -o .u
g++ c2.cpp -o .v
g++ c_gen.cpp -o .w

while true; do 
  ./.w > color.in
  ./.u
  ./.v < color.in > .y
  if diff color.out .y; then
    echo AC
  else
    echo WA
    break
  fi
done
