// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  long long rnd(long long p) { return 1ll * rand() * rand() % p; }
  long long rnd(long long L, long long R) { return rnd(R - L + 1) + L; }
} // namespace RA

using namespace RA;

const int T = 400;

int n;

int B(int i) { return i / T; }
int BL(int i) { return max(1, i * T); }
int BR(int i) { return min(n, i * T + T - 1); }

int main() {
  srand(clock() + time(0));
  n = 1e5;
  printf("%d\n", n);
  int w = rnd(1, 5000);
  w = rnd(1, 1e5);
  FOR(i, 1, n) printf("%lld%c", rnd(1, w), " \n"[i == n]);
  int q = n;
  printf("%d\n", q);
  int Li = B(1), Ri = B(n);
  FOR(i, 1, q) {
    int l = rnd(Li, Ri), r = rnd(Li, Ri);
    if (l > r) swap(l, r);
    printf("%d %d\n", BL(l), BR(r));
  }
  return 0;
}
