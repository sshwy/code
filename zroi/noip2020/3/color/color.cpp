// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

const int N = 1e5 + 5, P = 998244353, T = 333;
// const int N=5000, P=998244353, T=80;
int n, q, las, maxa;

int B(int i) { return i / T; }
int BL(int i) { return max(1, i * T); }
int BR(int i) { return min(n, i * T + T - 1); }

vector<int> pos[N];
int cnt[N], ccnt[N], big[N], lbig, d[N], inv[N], a[N];
int f[N / T + 2][N / T + 2][T]; // 第[i,j]块中出现次数为k的数的个数
int g[N][N / T + 2];            // i在前j个块中的出现次数

bool inB(int p, int i) { return BL(i) <= p && p <= BR(i); }
int qry(int l, int r) {
  int bl = B(l), br = B(r);
  // printf("\033[32ml %d r %d bl %d br %d\033[0m\n",l,r,bl,br);
  if (bl == br) {
    int res = 1;
    FOR(i, l, r) cnt[a[i]] = 0;
    FOR(i, l, r) cnt[a[i]]++;
    FOR(i, l, r) {
      res = res * 1ll * (cnt[a[i]] + 1) % P;
      cnt[a[i]] = 0;
    }
    return res;
  } else {
    assert(bl < br);
    int res = 1;
    FOR(i, 1, T - 1) { // 出现次数为i 的贡献积
      int v = bl + 1 <= br - 1 ? f[bl + 1][br - 1][i] : 1;
      res = 1ll * res * v % P;
    }
    // printf("res %d\n",res);
    int R_bl = BR(bl), L_br = BL(br);
    // printf("R_bl %d L_br %d\n",R_bl,L_br);
    FOR(i, l, R_bl) cnt[a[i]] = 0;
    FOR(i, L_br, r) cnt[a[i]] = 0;
    FOR(i, 1, lbig) cnt[big[i]] = 0;
    FOR(i, l, R_bl) {
      if (!cnt[a[i]]) {
        d[a[i]] = g[a[i]][br - 1] - g[a[i]][bl]; // 在块中出现的次数
        cnt[a[i]] += d[a[i]];
      }
      cnt[a[i]]++;
    }
    FOR(i, L_br, r) {
      if (!cnt[a[i]]) {
        d[a[i]] = g[a[i]][br - 1] - g[a[i]][bl]; // 在块中出现的次数
        cnt[a[i]] += d[a[i]];
      }
      cnt[a[i]]++;
    }
    FOR(i, 1, lbig) { // 把 Big 的贡献修正
      int bi = big[i];
      if (!cnt[bi]) {                      // 在边角中没出现
        d[bi] = g[bi][br - 1] - g[bi][bl]; // 在块中出现的次数
        cnt[bi] = d[bi];
      }
      if (d[bi] >= T) { // 没有算在f里
        d[bi] = 0;
      }
    }
    FOR(i, l, R_bl) {
      if (cnt[a[i]]) {
        res = 1ll * res * inv[d[a[i]] + 1] % P * (cnt[a[i]] + 1) % P;
        cnt[a[i]] = 0;
      }
    }
    FOR(i, L_br, r) {
      if (cnt[a[i]]) {
        res = 1ll * res * inv[d[a[i]] + 1] % P * (cnt[a[i]] + 1) % P;
        cnt[a[i]] = 0;
      }
    }
    FOR(i, 1, lbig) {
      int bi = big[i];
      if (cnt[bi]) {
        res = 1ll * res * inv[d[bi] + 1] % P * (cnt[bi] + 1) % P;
        cnt[bi] = 0;
      }
    }
    return res;
  }
  return 0;
}
int main() {
  freopen("color.in", "r", stdin);
  freopen("color.out", "w", stdout);

  scanf("%d", &n);
  FOR(i, 1, n) {
    scanf("%d", a + i);
    maxa = max(a[i], maxa);
    cnt[a[i]]++;
    if (cnt[a[i]] == T) big[++lbig] = a[i];
    pos[a[i]].push_back(i);
  }

  inv[1] = 1;
  FOR(i, 2, n) inv[i] = P - (P / i) * 1ll * inv[P % i] % P;

  int Li = B(1), Ri = B(n);
  FOR(i, Li, Ri) {
    fill(cnt, cnt + N, 0);
    fill(ccnt, ccnt + N, 1);
    // fill(cnt,cnt+max(T,maxa+1),0);
    // fill(ccnt,ccnt+max(T,maxa+1),1);
    FOR(j, i, Ri) {
      int Lj = BL(j), Rj = BR(j);
      FOR(k, Lj, Rj) {
        int ak = a[k];
        int cak = cnt[ak];
        ccnt[cak] = ccnt[cak] * 1ll * inv[cak + 1] % P;
        cnt[ak]++;
        ccnt[cak + 1] = ccnt[cak + 1] * 1ll * (cak + 2) % P;
      }
      FOR(k, 1, T - 1) f[i][j][k] = ccnt[k];
    }
  }

  // assert(maxa < N);
  FOR(ai, 1, maxa) if (pos[ai].size()) {
    unsigned p = 0;
    FOR(i, Li, Ri) {
      g[ai][i] = i == Li ? 0 : g[ai][i - 1];
      while (p < pos[ai].size() && inB(pos[ai][p], i)) g[ai][i]++, ++p;
    }
  }

  scanf("%d", &q);
  FOR(i, 1, q) {
    int l, r;
    scanf("%d%d", &l, &r);
    l ^= las, r ^= las;
    assert(l <= r);
    las = qry(l, r);
    printf("%d\n", las);
  }
  return 0;
}
