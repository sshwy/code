// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

const int N = 1e5 + 5, P = 998244353;
int n, q, las;

int cnt[N], a[N];

int qry(int l, int r) {
  int res = 1;
  FOR(i, l, r) cnt[a[i]] = 0;
  FOR(i, l, r) cnt[a[i]]++;
  FOR(i, l, r) {
    res = res * 1ll * (cnt[a[i]] + 1) % P;
    cnt[a[i]] = 0;
  }
  return res;
  return 0;
}
int main() {
  scanf("%d", &n);
  FOR(i, 1, n) scanf("%d", a + i);
  scanf("%d", &q);
  FOR(i, 1, q) {
    int l, r;
    scanf("%d%d", &l, &r);
    // l^=las, r^=las;
    assert(l <= r);
    las = qry(l, r);
    printf("%d\n", las);
  }
  return 0;
}
