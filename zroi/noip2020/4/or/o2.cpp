// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

long long n, t;
long long a[10000];
long long ans;

void dfs(int cur) {
  if (cur == n + 1) {
    long long x = 0;
    FOR(i, 1, n) x |= a[i];
    if (x == t) ans++;
    return;
  }
  for (int i = 0; i <= t; i += 3) {
    a[cur] = i;
    dfs(cur + 1);
  }
}

int main() {
  cin >> n >> t;
  dfs(1);
  cout << ans << endl;
  return 0;
}
