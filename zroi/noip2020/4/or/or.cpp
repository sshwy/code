// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA
const int P = 998244353, D = 60;
int pw(int a, int m) {
  int res = 1;
  while (m) m & 1 ? res = 1ll * res * a % P : 0, a = 1ll * a * a % P, m >>= 1;
  return res;
}
long long n, t;
int a, b;
int binom[100][100];

int calc(int x, int y) {
  int res = 0;
  FOR(i, 0, x) FOR(j, 0, y) if ((i + (j * 2)) % 3 == 0) {
    res = (res + binom[x][i] * 1ll * binom[y][j]) % P;
  }
  res = pw(res, n % (P - 1));
  return res;
}

int main() {
  freopen("or.in", "r", stdin);
  freopen("or.out", "w", stdout);

  binom[0][0] = 1;
  FOR(i, 1, D) {
    binom[i][0] = 1;
    FOR(j, 1, i) binom[i][j] = (binom[i - 1][j - 1] + binom[i - 1][j]) % P;
  }

  scanf("%lld%lld", &n, &t);
  FOR(i, 0, 60) if (t >> i & 1) {
    if (i % 2 == 0)
      a++;
    else
      b++;
  }
  const int c = a + b;
  int ans = 0;
  FOR(i, 0, c) {
    // (-1)^i
    int maxJ = min(i, a);
    int s = 0;
    FOR(j, 0, maxJ) {
      s = (s + 1ll * binom[a][j] * binom[b][i - j] % P * calc(a - j, b - (i - j))) %
          P;
    }
    // printf("i=%d s=%d\n",i,s);
    if (i % 2 == 1)
      ans = (ans - s + P) % P;
    else
      ans = (ans + s) % P;
  }
  printf("%d\n", ans);
  return 0;
}
