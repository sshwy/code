// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

const int N = 2005, M = 305, P = 998244353;
int binom[M][M], K, m;

vector<int> vT[N], wT[N];
vector<int> depS[M]; // 深度集合
int keyCnt[N];
bool keyV[N];
int dfsV(int u, int p, int pw) {
  depS[u].pb(0);
  int res = 0;
  if (keyV[u]) keyCnt[u]++;
  for (unsigned i = 0; i < vT[u].size(); i++) {
    int v = vT[u][i], w = wT[u][i];
    if (v == p) continue;
    res = (res + dfsV(v, u, w)) % P;
    for (unsigned j = 0; j < depS[v].size(); j++) { depS[u].pb(depS[v][j] + w); }
    keyCnt[u] += keyCnt[v];
  }
  sort(depS[u].begin(), depS[u].end());
  if (p) {
    int mxI = min(keyCnt[u], K - 1);
    int s = 0;
    FOR(i, 1, mxI) {
      s = (s + binom[keyCnt[u]][i] * 1ll * binom[m - keyCnt[u]][K - i]) % P;
    }
    s = s * 1ll * pw % P;
    res = (res + s) % P;
  }
  return res;
}

long long f[M][M][M]; // f[i,k,j]: j表示，深度<=depS[i][j]
long long g[M][M], g1[M][M];
void add(long long &x, long long y) {
  assert(y < P);
  x = (x + y) % P;
}
void dfsV2(int u, int p) {
  // 0<=x<=k f[u,0]:不选点。f[u,0,j] = 1
  // g[k,j]
  // g'[k,j] <- g[k-x,j] * f[v,x,j]
  // f[u,k,j] = g[k,j] ( 不选u
  // f[u,k,j] <- g[k-1,j] （选u
  // f[u,0,j] = 1（u的子树不选
  for (unsigned i = 0; i < vT[u].size(); i++) {
    int v = vT[u][i];
    if (v != p) dfsV2(v, u);
  }
  memset(g, 0, sizeof(g));
  for (unsigned j = 0; j < depS[u].size(); j++) g[0][j] = 1;
  int maxY = 0;
  for (unsigned i = 0; i < vT[u].size(); i++) {
    int v = vT[u][i], w = wT[u][i];
    if (v == p) continue; // v in son(u)

    unsigned jv = 0;
    for (unsigned j = 0; j < depS[u].size(); j++) {
      FOR(y, 0, maxY) add(g1[y][j], g[y][j]); // 如果v里不选点

      int depJ = depS[u][j];
      while (jv + 1 < depS[v].size() && depS[v][jv + 1] + w <= depJ) ++jv;

      if (depS[v][jv] + w <= depJ) {
        FOR(y, 0, maxY) {
          int maxX = min(K, keyCnt[v]);
          FOR(x, 1, maxX) {
            if (x + y > K) break;
            add(g1[x + y][j], g[y][j] * f[v][x][jv] % P);
          }
        }
      }
    }
    memcpy(g, g1, sizeof(g));
    memset(g1, 0, sizeof(g1));
    maxY += keyCnt[v];
    maxY = min(maxY, K);
  }
  FOR(k, 0, maxY) {
    for (unsigned j = 0; j < depS[u].size(); j++) {
      f[u][k][j] = g[k][j];
      printf("g[%d,%d] = %lld\n", k, j, g[k][j]);
    }
  }
  if (keyV[u]) { // 选u
    ++maxY;
    assert(maxY == keyCnt[u]);
    FOR(k, 1, maxY) {
      for (unsigned j = 0; j < depS[u].size(); j++) { add(f[u][k][j], g[k - 1][j]); }
    }
  }
  if (keyV[u]) printf("%d 是 key \n", u);
  FOR(k, 0, maxY) {
    for (unsigned j = 0; j < depS[u].size(); j++) {
      printf("%d 子树内，深度<= %d，选 %d 个点的方案数 %lld\n", u, depS[u][j], k,
          f[u][k][j]);
    }
  }
}

vector<int> t[N];
int n;
bool key[N];
int vN[N], vid[N], totVN;
int dep[N];

void vAe(int u, int v, int w) {
  if (vid[u] == 0) vid[u] = ++totVN;
  if (vid[v] == 0) vid[v] = ++totVN;
  u = vid[u], v = vid[v];
  printf("ae %d %d %d\n", u, v, w);
  vT[u].pb(v), vT[v].pb(u), wT[u].pb(w), wT[v].pb(w);
}
void dfs(int u, int p) {
  dep[u] = dep[p] + 1;
  if (key[u]) vN[u] = u;
  for (unsigned i = 0; i < t[u].size(); i++) {
    int v = t[u][i];
    if (v == p) continue;
    dfs(v, u);
    if (vN[v]) {
      int vtv = vN[v];
      if (vN[u]) {
        if (vN[u] != u) vAe(u, vN[u], dep[vN[u]] - dep[u]);
        vAe(u, vtv, dep[vtv] - dep[u]);
        vN[u] = u;
      } else
        vN[u] = vtv;
    }
  }
  // printf("u %d vN %d\n",u,vN[u]);
}

int main() {
  freopen("tree.in", "r", stdin);
  // freopen("tree.out","w",stdout);
  scanf("%d%d%d", &n, &m, &K);

  binom[0][0] = 1;
  FOR(i, 1, m) {
    binom[i][0] = 1;
    FOR(j, 1, i) binom[i][j] = (binom[i - 1][j - 1] + binom[i - 1][j]) % P;
  }

  FOR(i, 1, m) {
    int x;
    scanf("%d", &x);
    key[x] = 1;
  }
  FOR(i, 1, n - 1) {
    int u, v;
    scanf("%d%d", &u, &v);
    t[u].pb(v), t[v].pb(u);
  }
  dfs(1, 0);
  int root = 1;
  printf("root %d\n", root);

  FOR(i, 1, n) if (vid[i] && key[i]) keyV[vid[i]] = 1;

  int tot = dfsV(root, 0, 0); // 总虚树的边权和
  printf("tot %d\n", tot);
  tot = tot * 2ll % P;

  dfsV2(root, 0);

  return 0;
}
