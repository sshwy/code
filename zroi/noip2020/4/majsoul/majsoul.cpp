// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

char a[20][20];
string s[20];

bool isSeven() {
  for (int i = 1; i < 14; i += 2)
    if (s[i] != s[i + 1]) return false;
  for (int i = 1; i < 14; i += 2)
    for (int j = i + 2; j < 14; j += 2)
      if (s[i] == s[j]) return false;
  return true;
}
bool isNine() {
  FOR(i, 1, 14) if (s[i].size() != 2) return false;
  FOR(i, 1, 13) if (s[i][1] != s[i + 1][1]) return false;
  int cnt[10] = {0};
  FOR(i, 1, 14) cnt[s[i][0] - '0']++;
  FOR(i, 1, 9) cnt[i]--;
  cnt[1] -= 2;
  cnt[9] -= 2;
  int s = 0;
  FOR(i, 1, 9) if (cnt[i] < 0) return false;
  FOR(i, 1, 9) s += cnt[i];
  assert(s == 1);
  return true;
}
bool isThirteen() {
  int cnt = 0;
  FOR(i, 1, 13) if (s[i] == s[i + 1])++ cnt;
  if (cnt != 1) return false;
  assert(unique(s + 1, s + 15) - s - 1 == 13);
  string t = "";
  FOR(i, 1, 13) t += s[i];
  if (t == "1m1p1s9m9p9sBEFNSWZ") return true;
  return false;
}
void go() {
  FOR(i, 1, 14) scanf("%s", a[i]), s[i] = a[i];
  sort(s + 1, s + 15);
  if (isSeven())
    puts("Seven");
  else if (isNine())
    puts("Nine");
  else if (isThirteen())
    puts("Thirteen");
  else
    puts("I don't know.");
}
int main() {
  freopen("majsoul.in", "r", stdin);
  freopen("majsoul.out", "w", stdout);

  int t;
  scanf("%d", &t);
  FOR(i, 1, t) go();
  return 0;
}
