// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

const int N = 500, P = 998244353;

int n;

vector<pair<int, int>> a[N];
bool cmp(edge a, edge b) { return a.w < b.w; }

int fa[N];
int get(int u) { return fa[u] == u ? u : fa[u] = get(fa[u]); }

int x, y;
int f[N][N], g[N][N], c[N], sz[N];
int h(int i) { return c[i] * 1ll * sz[i] % P; }
// 2 points, 1 fixed and 1, 1 point, c: 连接的方案数

int main() {
  freopen("rebuild.in", "r", stdin);
  freopen("rebuild.out", "w", stdout);

  scanf("%d", &n);
  FOR(i, 1, n - 1) {
    int u, v, w;
    scanf("%d%d%d", &u, &v, &w);
    a[w].push_back(make_pair(u, v));
  }
  scanf("%d%d", &x, &y);
  if (x == y) { todo }

  FOR(i, 1, n) {
    fa[i] = i;
    if (i == x || i == y) {
      g[i][0] = 1; // distance == 0
    }
    h[i] = 1;
    c[i] = 1;
    sz[i] = 1;
  }

  FOR(i, 1, n - 1) if (a[i].size()) {
    memcpy(ga, fa, sizeof(fa));
    for (unsigned j = 0; j < a[i].size(); j++) {
      int u = a[i][j].first, v = a[i][j].second;
      u = get(u), v = get(v);
      fa[u] = v;
    }
    if (get(x) == get(y)) { FOR(i, 1, n) if (get(i) == get(x)) vis[i] = 1; }
    memcpy(fa, ga, sizeof(fa));
    for (unsigned j = 0; j < a[i].size(); j++) {
      int u = a[i][j].first, v = a[i][j].second;
      u = get(u), v = get(v);
      if (vis[u]) A.push_back(u), vis[u] = 0;
      if (vis[v]) A.push_back(v), vis[v] = 0;
    }
    if (A.size()) {
      for (unsigned j = 0; j < A.size(); j++) {
        int u = A[j];
        if (u == x || u == y) continue;
        int v = get(x);

        fa[u] = v; // u -> v
        int cv = c[u] * 1ll * c[v] % P * sz[u] % P * sz[v] % P;
        ROF(i, n, 1) {
          int fvi = 0, gvi = 0;
          {
            long long s = 0;
            FOR(j, 1, i - 1) { s = (s + 1ll * f[u][j] * f[v][i - j - 1]) % P; }

            if (i >= 2) {
              s = (s + h(u) * 1ll * f[v][i - 1] % P + h(v) * 1ll * f[u][i - 1] % P) %
                  P;
            } else {
              s = (s + h(u) * 1ll * h(v)) % P;
            }
            s = s * 2 % P;
            fvi = (s + f[v][i] + f[u][i]) % P;
          }

          if (get(u) == get(x) && get(v) == get(y) ||
              get(u) == get(y) && get(v) == get(x)) {
            assert(0);
          } else if (get(u) == get(x) || get(u) == get(y)) {
            long long s = 0;
            FOR(j, 1, i - 1) { s = (s + g[u][j] * 1ll * f[v][i - j - 1]) % P; }
            s = (s + g[u][i - 1] * 1ll * h(v)) % P;
            s = (s + g[u][i]) % P;
            gvi = s;
          } else if (get(v) == get(x) || get(v) == get(y)) {
            swap(u, v);
            long long s = 0;
            FOR(j, 1, i - 1) { s = (s + g[u][j] * 1ll * f[v][i - j - 1]) % P; }
            s = (s + g[u][i - 1] * 1ll * h(v)) % P;
            s = (s + g[u][i]) % P;
            gvi = s;
            swap(u, v);
          }
          f[v][i] = fvi;
          g[v][i] = gvi;
        }
        c[v] = cv;
      }
    }
    for (unsigned j = 0; j < a[i].size(); j++) {
      int u = a[i][j].first, v = a[i][j].second;
      u = get(u), v = get(v);
      fa[u] = v; // u -> v
      int cv = c[u] * 1ll * c[v] % P * sz[u] % P * sz[v] % P;
      ROF(i, n, 1) {
        int fvi = 0, gvi = 0;
        {
          long long s = 0;
          FOR(j, 1, i - 1) { s = (s + 1ll * f[u][j] * f[v][i - j - 1]) % P; }

          if (i >= 2) {
            s = (s + h(u) * 1ll * f[v][i - 1] % P + h(v) * 1ll * f[u][i - 1] % P) % P;
          } else {
            s = (s + h(u) * 1ll * h(v)) % P;
          }
          s = s * 2 % P;
          fvi = (s + f[v][i] + f[u][i]) % P;
        }

        if (get(u) == get(x) && get(v) == get(y) ||
            get(u) == get(y) && get(v) == get(x)) {
          assert(0);
        } else if (get(u) == get(x) || get(u) == get(y)) {
          long long s = 0;
          FOR(j, 1, i - 1) { s = (s + g[u][j] * 1ll * f[v][i - j - 1]) % P; }
          s = (s + g[u][i - 1] * 1ll * h(v)) % P;
          s = (s + g[u][i]) % P;
          gvi = s;
        } else if (get(v) == get(x) || get(v) == get(y)) {
          swap(u, v);
          long long s = 0;
          FOR(j, 1, i - 1) { s = (s + g[u][j] * 1ll * f[v][i - j - 1]) % P; }
          s = (s + g[u][i - 1] * 1ll * h(v)) % P;
          s = (s + g[u][i]) % P;
          gvi = s;
          swap(u, v);
        }
        f[v][i] = fvi;
        g[v][i] = gvi;
      }
      c[v] = cv;
    }
  }

  return 0;
}
