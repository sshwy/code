// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

const int N = 2200005;

struct qxx {
  int nex, t;
} e[N];
int h[N], le;
void ae(int f, int t) { e[++le] = (qxx){h[f], t}, h[f] = le; }

int L[N], R[N], totdfn;
void dfs(int u) {
  L[u] = ++totdfn;
  for (int i = h[u]; i; i = e[i].nex) {
    int v = e[i].t;
    dfs(v);
  }
  R[u] = totdfn;
}
bool isIn(int i, int j) { return L[j] <= L[i] && R[i] <= R[j]; }
int n;
int s[N], tp, ans[N];
int main() {
  freopen("ancestor.in", "r", stdin);
  freopen("ancestor.out", "w", stdout);

  scanf("%d", &n);
  FOR(i, 2, n) {
    int x;
    scanf("%d", &x);
    ae(x, i);
  }

  dfs(1);

  FOR(i, 1, n) {
    while (tp > 0 && !isIn(i, s[tp])) {
      ans[i]--;
      --tp;
    }
    s[++tp] = i;
  }
  FOR(i, 1, n) ans[i]++;
  FOR(i, 1, n) ans[i] += ans[i - 1];
  FOR(i, 1, n) printf("%d\n", ans[i]);
  return 0;
}
