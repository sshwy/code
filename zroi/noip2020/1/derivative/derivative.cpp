// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

const int P = 998244353;
const int N = 1e6 + 5;

int pw(int a, int m) {
  int res = 1;
  while (m) m & 1 ? res = res * 1ll * a % P : 0, a = 1ll * a * a % P, m >>= 1;
  return res;
}
long long n, k, a, b, c;

int fac[N], fnv[N];

int main() {
  freopen("derivative.in", "r", stdin);
  freopen("derivative.out", "w", stdout);

  scanf("%lld%lld%lld%lld", &n, &k, &a, &b);
  c = a * pw(b, k) % P;

  if (k == 1) {
    printf("%d\n", pw((c + 1) % P, n % (P - 1)));
    return 0;
  }

  fac[0] = 1;
  FOR(i, 1, n * k) fac[i] = fac[i - 1] * 1ll * i % P;
  fnv[n * k] = pw(fac[n * k], P - 2);
  ROF(i, n * k, 1) fnv[i - 1] = fnv[i] * 1ll * i % P;

  long long s = 0;
  FOR(i, 0, n) {
    s = (s + 1ll * pw(c, i) * fac[n * k] % P * fnv[i * k] % P * fnv[n * k - i * k] %
                 P) %
        P;
  }
  printf("%lld\n", s);
  return 0;
}
