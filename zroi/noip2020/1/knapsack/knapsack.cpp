// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

const int N = 20005, Q = 1e5 + 5, P = 998244353, M = 505;

int n, q;
int val[N], w[N];

struct qr {
  int l, r, m, id;
};
vector<qr> qry[N * 4];
void aq(int u, int l, int r, qr v) {
  if (l == r) {
    qry[u].push_back(v);
    return;
  }
  int mid = (l + r) >> 1;
  if (v.l <= mid && v.r > mid) {
    qry[u].push_back(v);
    return;
  }
  if (v.r <= mid)
    aq(u << 1, l, mid, v);
  else
    aq(u << 1 | 1, mid + 1, r, v);
}
struct atom {
  long long a, b;
  atom(long long _a = 0, long long _b = 0) { a = _a, b = _b; }
};
atom operator+(atom x, atom y) { return atom(x.a + y.a, x.b * y.b % P); }
atom merge(atom x, atom y) {
  if (x.a > y.a) return x;
  if (x.a < y.a) return y;
  return atom(x.a, (x.b + y.b) % P);
}
atom calc(atom *A, atom *B, int m) {
  atom res = B[m]; // i=0
  FOR(i, 1, m) res = merge(res, A[i] + B[m - i]);
  if (res.a == 0) res.b = 0;
  return res;
}
atom f[N][M], ans[Q];
void dfs(int u, int l, int r) {
  // printf("dfs %d %d %d\n",u,l,r);
  if (l == r) {
    assert(u < (N << 2));
    // printf("dfs 2 %d %d %d\n",u,l,r);
    if (!qry[u].empty()) {
      for (unsigned _ = 0; _ < qry[u].size(); _++) {
        qr x = qry[u][_];
        // printf("%d %d %d %d\n",x.l,x.r,x.m,x.id);
        assert(x.id < Q && x.m < M);
        ans[x.id] = x.m >= w[l] ? atom(val[l], 1) : atom(0, 0);
      }
    }
    // printf("dfs 3 %d %d %d\n",u,l,r);
    return;
  }
  int mid = (l + r) >> 1;
  if (qry[u].size()) {
    FOR(j, 0, w[mid] - 1) f[mid][j] = atom(0, 1);
    FOR(j, w[mid], M - 1) f[mid][j] = atom(val[mid], 1);
    ROF(i, mid - 1, l) ROF(j, M - 1, 0) {
      f[i][j] = f[i + 1][j];
      if (j >= w[i]) f[i][j] = merge(f[i + 1][j - w[i]] + atom(val[i], 1), f[i][j]);
    }

    FOR(j, 0, M - 1) f[mid + 1][j] = atom(0, 0);
    f[mid + 1][0] = atom(0, 1);
    f[mid + 1][w[mid + 1]] = atom(val[mid + 1], 1);
    FOR(i, mid + 2, r) ROF(j, M - 1, 0) {
      f[i][j] = f[i - 1][j];
      if (j >= w[i]) f[i][j] = merge(f[i - 1][j - w[i]] + atom(val[i], 1), f[i][j]);
    }

    for (unsigned _ = 0; _ < qry[u].size(); _++) {
      qr x = qry[u][_];
      ans[x.id] = calc(f[x.l], f[x.r], x.m);
    }
  }
  dfs(u << 1, l, mid);
  dfs(u << 1 | 1, mid + 1, r);
}
int main() {
  freopen("knapsack.in", "r", stdin);
  freopen("knapsack.out", "w", stdout);

  scanf("%d", &n);
  FOR(i, 1, n) scanf("%d%d", val + i, w + i);
  scanf("%d", &q);
  FOR(i, 1, q) {
    int l, r, m;
    scanf("%d%d%d", &l, &r, &m);
    qr v = (qr){l, r, m, i};
    aq(1, 1, n, v);
  }

  dfs(1, 1, n);

  FOR(i, 1, q) printf("%lld %lld\n", ans[i].a, ans[i].b);
  return 0;
}
