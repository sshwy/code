g++ knapsack.cpp --std=c++98 -o .u
g++ k2.cpp -o .v
g++ k_gen.cpp -o .w

while true; do
  ./.w > knapsack.in
  ./.u
  ./.v < knapsack.in > .y
  if diff knapsack.out .y; then
    echo AC
  else 
    echo WA
    break
  fi
done
