// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

const int N = 20005, M = 505, P = 998244353;
int n, v[N], w[N], q;
long long f[N][M], g[N][M];
void calc(int l, int r, int m) {
  FOR(j, 0, m) f[l][j] = g[l][j] = 0;
  FOR(j, 0, w[l] - 1) g[l][j] = 1;
  FOR(j, w[l], m) f[l][j] = v[l], g[l][j] = 1;
  FOR(i, l + 1, r) ROF(j, m, 0) {
    if (j >= w[i]) {
      if (f[i - 1][j] == f[i - 1][j - w[i]] + v[i]) {
        f[i][j] = f[i - 1][j];
        g[i][j] = (g[i - 1][j] + g[i - 1][j - w[i]]) % P;
      } else if (f[i - 1][j] > f[i - 1][j - w[i]] + v[i]) {
        f[i][j] = f[i - 1][j];
        g[i][j] = g[i - 1][j];
      } else {
        f[i][j] = f[i - 1][j - w[i]] + v[i];
        g[i][j] = g[i - 1][j - w[i]];
      }
    } else {
      f[i][j] = f[i - 1][j], g[i][j] = g[i - 1][j];
    }
  }
  if (f[r][m] == 0)
    puts("0 0");
  else
    printf("%lld %lld\n", f[r][m], g[r][m]);
}
int main() {
  scanf("%d", &n);
  FOR(i, 1, n) scanf("%d%d", v + i, w + i);
  scanf("%d", &q);
  FOR(i, 1, q) {
    int l, r, m;
    scanf("%d%d%d", &l, &r, &m);
    calc(l, r, m);
  }
  return 0;
}
