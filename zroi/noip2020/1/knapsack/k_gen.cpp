// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

using namespace RA;

int main() {
  srand(clock() + time(0));
  int n = rnd(1, 100);
  // n = 20000;
  int W = 10, M = 100, V = 2000;
  // W = 10, M=500,V=1e6;
  printf("%d\n", n);
  FOR(i, 1, n) printf("%d %d\n", rnd(1, V), rnd(1, W));
  int q = n * 5;
  printf("%d\n", q);
  FOR(i, 1, q) {
    int l = rnd(1, n), r = rnd(1, n), m = rnd(1, M);
    if (l > r) swap(l, r);
    printf("%d %d %d\n", l, r, m);
  }
  return 0;
}
