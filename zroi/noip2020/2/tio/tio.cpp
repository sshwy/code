// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

namespace GenHelper {
  unsigned z1, z2, z3, z4, b;
  unsigned rand_() {
    b = ((z1 << 6) ^ z1) >> 13;
    z1 = ((z1 & 4294967294U) << 18) ^ b;
    b = ((z2 << 2) ^ z2) >> 27;
    z2 = ((z2 & 4294967288U) << 2) ^ b;
    b = ((z3 << 13) ^ z3) >> 21;
    z3 = ((z3 & 4294967280U) << 7) ^ b;
    b = ((z4 << 3) ^ z4) >> 12;
    z4 = ((z4 & 4294967168U) << 13) ^ b;
    return (z1 ^ z2 ^ z3 ^ z4);
  }
} // namespace GenHelper

void get(int *a, int n, unsigned s, int l, int r) {
  using namespace GenHelper;
  z1 = s;
  z2 = unsigned((~s) ^ 0x233333333U);
  z3 = unsigned(s ^ 0x1234598766U);
  z4 = (~s) + 51;
  for (int i = 1; i <= n; i++) {
    int x = rand_() & 32767;
    int y = rand_() & 32767;
    *(++a) = (l + (x * 32768 + y) % (r - l + 1));
  }
}

const int N = 1e7 + 50;
int n, s, l, r, P, a[N], ls[N], rs[N], st[N], tp;
long long prod[N], sprd[N], pprd[N];

void al(int u, int v) { ls[u] = v; }
void ar(int u, int v) { rs[u] = v; }

int ans;
void dfs(int u) {
  int lu = ls[u], ru = rs[u];
  if (lu) dfs(lu);
  if (ru) dfs(ru);
  prod[u] = prod[lu] * 1ll * prod[ru] % P * a[u] % P;
  pprd[u] = (pprd[lu] + (pprd[ru] + 1) * 1ll * prod[lu] % P * a[u] % P) % P;
  sprd[u] = (sprd[ru] + (sprd[lu] + 1) * 1ll * prod[ru] % P * a[u] % P) % P;
  ans = (ans + (sprd[lu] + 1) * 1ll * (pprd[ru] + 1) % P * a[u] % P * a[u] % P) % P;
}
int main() {
  freopen("tio.in", "r", stdin);
  freopen("tio.out", "w", stdout);

  scanf("%d%d%d%d%d", &n, &s, &l, &r, &P);
  get(a, n, s, l, r);
  FOR(i, 1, n) {
    while (tp > 1 && a[st[tp - 1]] <= a[i]) ar(st[tp - 1], st[tp]), --tp;
    if (tp && a[st[tp]] <= a[i]) al(i, st[tp]), --tp;
    st[++tp] = i;
  }
  while (tp > 1) ar(st[tp - 1], st[tp]), --tp;
  prod[0] = 1;
  dfs(st[1]);
  printf("%d\n", ans);
  return 0;
}
