// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

using namespace RA;

int main() {
  srand(clock() + time(0));
  int n = rnd(1, 5000);
  int s = rnd(1, 1e9);
  int l = 0;
  int r = 1e9;
  int p = 998255353;
  printf("%d %d %d %d %d\n", n, s, l, r, p);
  return 0;
}
