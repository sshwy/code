g++ tio.cpp -o .u
g++ t2.cpp -o .v
g++ t_gen.cpp -o .w

while true; do
  ./.w > tio.in
  ./.u
  ./.v < tio.in > .y
  if diff tio.out .y; then
    echo AC
  else
    echo WA
    break
  fi
done
