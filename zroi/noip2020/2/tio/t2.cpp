// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

namespace GenHelper {
  unsigned z1, z2, z3, z4, b;
  unsigned rand_() {
    b = ((z1 << 6) ^ z1) >> 13;
    z1 = ((z1 & 4294967294U) << 18) ^ b;
    b = ((z2 << 2) ^ z2) >> 27;
    z2 = ((z2 & 4294967288U) << 2) ^ b;
    b = ((z3 << 13) ^ z3) >> 21;
    z3 = ((z3 & 4294967280U) << 7) ^ b;
    b = ((z4 << 3) ^ z4) >> 12;
    z4 = ((z4 & 4294967168U) << 13) ^ b;
    return (z1 ^ z2 ^ z3 ^ z4);
  }
} // namespace GenHelper

void get(int *a, int n, unsigned s, int l, int r) {
  using namespace GenHelper;
  z1 = s;
  z2 = unsigned((~s) ^ 0x233333333U);
  z3 = unsigned(s ^ 0x1234598766U);
  z4 = (~s) + 51;
  for (int i = 1; i <= n; i++) {
    int x = rand_() & 32767;
    int y = rand_() & 32767;
    *(++a) = (l + (x * 32768 + y) % (r - l + 1));
  }
}

const int N = 1e7 + 50;
int n, s, l, r, P, a[N];
int main() {
  scanf("%d%d%d%d%d", &n, &s, &l, &r, &P);
  get(a, n, s, l, r);

  int ans = 0;
  FOR(i, 1, n) {
    int mx = a[i];
    int prd = 1;
    FOR(j, i, n) {
      mx = max(mx, a[j]);
      prd = prd * 1ll * a[j] % P;
      ans = (ans + mx * 1ll * prd) % P;
    }
  }
  printf("%d\n", ans);
  return 0;
}
