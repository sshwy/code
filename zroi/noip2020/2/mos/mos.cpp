// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

const int P = 998244353, NM = 2e7 + 5;

int pw(int a, int m) {
  int res = 1;
  while (m) m & 1 ? res = 1ll * res * a % P : 0, a = 1ll * a * a % P, m >>= 1;
  return res;
}

int n, m, s;
int inv[NM];

int main() {
  freopen("mos.in", "r", stdin);
  freopen("mos.out", "w", stdout);

  scanf("%d%d", &n, &m);
  FOR(i, 1, n) {
    int x;
    scanf("%d", &x);
    s = (s + x) % P;
  }
  s = s * 1ll * m % P;
  inv[1] = 1;
  int coef = 0, c2 = 0;
  FOR(i, 2, n * m + 1) {
    inv[i] = P - (P / i) * 1ll * inv[P % i] % P;
    // assert(inv[i] == pw(i,P-2));
  }
  ROF(i, n * m + 1, 2) {
    coef = (coef + inv[i]) % P;
    c2 = (c2 + coef) % P;
  }
  s = s * 1ll * c2 % P;
  s = s * 1ll * inv[n * m] % P;
  printf("%d\n", s);
  return 0;
}
