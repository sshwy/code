// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

const int N = 1e6 + 5;
int n;
int L[N], R[N];

int main() {
  freopen("pag.in", "r", stdin);
  freopen("pag.out", "w", stdout);

  scanf("%d", &n);
  FOR(i, 1, n) { scanf("%d%d", L + i, R + i); }

  printf("%d\n", *max_element(R + 1, R + n + 1));

  return 0;
}
