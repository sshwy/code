// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA
typedef long long LL;

__int128 s(long long n) {
  __int128 x = n;
  return (x * (x + 1)) >> 1;
}

long long n;
__int128 ans;

int b[100], lb;
void print(__int128 x) {
  lb = 0;
  while (x) b[++lb] = x % 10, x /= 10;
  ROF(i, lb, 1) putchar(b[i] + '0');
  putchar('\n');
}
const int L = 1e7 + 200, L2 = 4e6;
bool bp[L];
int mu[L];
int pn[L], lp;
long long c1[L2], l1, c2[L2], l2;
__int128 s1[L2], s2[L2];

void sieve(int lim) {
  bp[0] = bp[1] = 1;
  mu[1] = 1;
  FOR(i, 2, lim) {
    if (bp[i] == 0) pn[++lp] = i, mu[i] = -1;
    FOR(j, 1, lp) {
      if (i * pn[j] > lim) break;
      bp[i * pn[j]] = 1;
      if (i % pn[j] == 0) {
        mu[i * pn[j]] = 0;
        break;
      } else {
        mu[i * pn[j]] = -mu[i];
      }
    }
  }
  FOR(i, 1, lim) {
    if (mu[i] == 1) c1[++l1] = i * 1ll * i;
    if (mu[i] == -1) c2[++l2] = i * 1ll * i;
  }
  FOR(i, 1, l1) s1[i] = s1[i - 1] + c1[i];
  FOR(i, 1, l2) s2[i] = s2[i - 1] + c2[i];
}

void go() {
  scanf("%lld", &n);
  ans = 0;
  int cur = 1;
  while (cur <= l1 && c1[cur] * c1[cur] <= n) {
    ans += s(n / c1[cur]) * c1[cur];
    ++cur;
  }
  while (cur <= l1 && c1[cur] <= n) {
    long long cnex = n / (n / c1[cur]);
    int nex = upper_bound(c1 + cur, c1 + l1 + 1, cnex) - 1 - c1;
    ans += s(n / c1[cur]) * (s1[nex] - s1[cur - 1]);
    cur = nex + 1;
  }
  cur = 1;
  while (cur <= l2 && c2[cur] * c2[cur] <= n) {
    ans -= s(n / c2[cur]) * c2[cur];
    ++cur;
  }
  while (cur <= l2 && c2[cur] <= n) {
    long long cnex = n / (n / c2[cur]);
    int nex = upper_bound(c2 + cur, c2 + l2 + 1, cnex) - 1 - c2;
    ans -= s(n / c2[cur]) * (s2[nex] - s2[cur - 1]);
    cur = nex + 1;
  }
  // assert(ans>=0);
  print(ans);
}
int main() {
  freopen("kfc.in", "r", stdin);
  freopen("kfc.out", "w", stdout);
  int t;
  sieve(L - 1);
  // printf("l1 %lld l2 %lld\n",l1,l2);
  scanf("%d", &t);
  FOR(i, 1, t) go();
  return 0;
}
