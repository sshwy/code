// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA
typedef long long LL;

int mu(int x) {
  int res = 1;
  for (int i = 2; i * i <= x; i++)
    if (x % i == 0) {
      x /= i;
      res = -res;
      if (x % i == 0) return 0;
    }
  if (x > 1) res = -res;
  return res;
}
long long n;
void go() {
  scanf("%lld", &n);
  long long ans = 0;
  FOR(i, 1, n) {
    int mx = mu(i);
    ans = ans + mx * mx * i;
  }
  printf("%lld\n", ans);
}
int main() {
  int t;
  scanf("%d", &t);
  FOR(i, 1, t) go();
  return 0;
}
