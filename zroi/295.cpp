// by Sshwy
//#define DEBUGGER
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

#ifdef DEBUGGER

#define log_prefix()                                                \
  {                                                                 \
    fprintf(stderr, "\033[37mLine %-3d [%dms]:\033[0m  ", __LINE__, \
        (int)clock() / 1000);                                       \
  }
#define ilog(...) \
  { fprintf(stderr, __VA_ARGS__); }
#define llog(...)      \
  {                    \
    log_prefix();      \
    ilog(__VA_ARGS__); \
  }
#define log(...)           \
  {                        \
    log_prefix();          \
    ilog(__VA_ARGS__);     \
    fprintf(stderr, "\n"); \
  }
#define red(...)       \
  {                    \
    log_prefix();      \
    ilog("\033[31m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define green(...)     \
  {                    \
    log_prefix();      \
    ilog("\033[32m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define blue(...)      \
  {                    \
    log_prefix();      \
    ilog("\033[34m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }

#else

#define log_prefix() ;
#define ilog(...) ;
#define llog(...) ;
#define log(...) ;
#define red(...) ;
#define green(...) ;
#define blue(...) ;

#endif

namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 2e5 + 5, P = 998244353, base = 29;
char s[N], t[N];
int m, ls, lt, a[N], b[N], st[17][N], lg[N];
long long hs[N], ht[N], pw[N];

void init() {
  pw[0] = 1;
  FOR(i, 1, max(lt, ls)) pw[i] = pw[i - 1] * base % P;
  FOR(i, 1, ls) hs[i] = (hs[i - 1] * base + s[i] - 'a' + 1) % P;
  FOR(i, 1, lt) ht[i] = (ht[i - 1] * base + t[i] - 'a' + 1) % P;
}
long long hash_s(int l, int r) {
  return ((hs[r] - hs[l - 1] * pw[r - l + 1]) % P + P) % P;
}
long long hash_t(int l, int r) {
  return ((ht[r] - ht[l - 1] * pw[r - l + 1]) % P + P) % P;
}

int main() {
  scanf("%s%s", s + 1, t + 1);
  ls = strlen(s + 1), lt = strlen(t + 1);
  init();
  FOR(i, 1, ls + 1) a[i] = i;
  auto cmp = [](int x, int y) {
    auto cmp_st = [](int Ls, int Rs, int Lt, int Rt) {
      assert(Ls - Rs == Lt - Rt);
      if (Ls > Rs) return 0;
      if (hash_s(Ls, Rs) == hash_t(Lt, Rt)) return 0;
      int l = -1, r = Rs - Ls, mid;
      while (l < r)
        mid = (l + r + 1) >> 1,
        hash_s(Ls, Ls + mid) == hash_t(Lt, Lt + mid) ? l = mid : r = mid - 1;
      assert(s[Ls + l + 1] != t[Lt + l + 1]);
      if (s[Ls + l + 1] < t[Lt + l + 1])
        return -1;
      else
        return 1;
    };
    auto cmp_s = [](int L1, int R1, int L2, int R2) {
      assert(L1 - R1 == L2 - R2);
      if (L1 > R1) return 0;
      if (hash_s(L1, R1) == hash_s(L2, R2)) return 0;
      int l = -1, r = R1 - L1, mid;
      while (l < r)
        mid = (l + r + 1) >> 1,
        hash_s(L1, L1 + mid) == hash_s(L2, L2 + mid) ? l = mid : r = mid - 1;
      assert(s[L1 + l + 1] != s[L2 + l + 1]);
      if (s[L1 + l + 1] < s[L2 + l + 1])
        return -1;
      else
        return 1;
    };
    auto cmp_t = [](int L1, int R1, int L2, int R2) {
      assert(L1 - R1 == L2 - R2);
      if (L1 > R1) return 0;
      if (hash_t(L1, R1) == hash_t(L2, R2)) return 0;
      int l = -1, r = R1 - L1, mid;
      while (l < r)
        mid = (l + r + 1) >> 1,
        hash_t(L1, L1 + mid) == hash_t(L2, L2 + mid) ? l = mid : r = mid - 1;
      assert(t[L1 + l + 1] != t[L2 + l + 1]);
      if (t[L1 + l + 1] < t[L2 + l + 1])
        return -1;
      else
        return 1;
    };

    if (x == y) return false;
    bool flag = 1;
    if (x > y) swap(x, y), flag = 0;
    if (x + lt < y) {
      int t1 = cmp_st(x, x + lt - 1, 1, lt);
      if (t1 == 0) {
        int t2 = cmp_s(x + lt - lt, y - 1 - lt, x + lt, y - 1);
        if (t2 == 0) {
          int t3 = cmp_st(y - lt, y - 1, 1, lt);
          if (t3 == 0) {
            return flag; //相等时按照编号大小排序
          } else
            return t3 == -1 ? flag : !flag;
        } else
          return t2 == -1 ? flag : !flag;
      } else
        return t1 == 1 ? flag : !flag;
    } else {
      int t1 = cmp_st(x, y - 1, 1, y - x);
      if (t1 == 0) {
        int t2 = cmp_t(y - x + 1, lt, 1, x + lt - y);
        if (t2 == 0) {
          int t3 = cmp_st(x, y - 1, x + lt - y + 1, lt);
          if (t3 == 0)
            return flag;
          else
            return t3 == -1 ? flag : !flag;
        } else
          return t2 == -1 ? flag : !flag;
      } else
        return t1 == 1 ? flag : !flag;
    }
  };
  sort(a + 1, a + ls + 1 + 1, cmp);
  // FOR(i,1,ls+1)printf("%d%c",a[i]," \n"[i==ls+1]);

  FOR(i, 1, ls + 1) b[a[i]] = i;
  FOR(i, 1, ls + 1) st[0][i] = b[i];
  FOR(j, 1, 16) {
    FOR(i, 1, ls + 1 - (1 << j) + 1) {
      st[j][i] = min(st[j - 1][i], st[j - 1][i + (1 << (j - 1))]);
    }
  }
  lg[1] = 0;
  FOR(i, 2, ls + 1) lg[i] = lg[i / 2] + 1;

  auto qry = [](int l, int r) {
    int j = lg[r - l + 1];
    return min(st[j][l], st[j][r - (1 << j) + 1]);
  };

  int m;
  scanf("%d", &m);
  FOR(i, 1, m) {
    int a1, b1, c, d, e, ans = -1;
    scanf("%d%d%d%d%d", &a1, &b1, &c, &d, &e);
    for (int l = a1 / c * c; l <= b1; l += c) {
      int L = max(a1, l + d), R = min(b1, l + e);
      // printf("L %d R %d\n",L,R);
      if (L <= R) {
        int t = qry(L + 1, R + 1);
        if (ans == -1 || ans > t) ans = t;
      }
    }
    if (ans != -1) ans = a[ans] - 1;
    printf("%d%c", ans, " \n"[i == m]);
  }
  return 0;
}
