#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 1e5 + 5, SZ = N << 2, TR = 1e7 + 5;

int n, Q;
int a[N];
int ans[N];

namespace seg {
  namespace trie {
    int tot;
    int sz[TR], lc[TR], rc[TR];
    int reuse[TR], lr;
    inline void pushup(int u) { sz[u] = sz[lc[u]] + sz[rc[u]]; }
    void insert(int &u, int val, int h = 29) {
      if (!u) u = lr ? reuse[lr--] : ++tot, lc[u] = rc[u] = sz[u] = 0;
      if (h == -1) return sz[u]++, void();
      if (val >> h & 1)
        insert(rc[u], val, h - 1);
      else
        insert(lc[u], val, h - 1);
      pushup(u);
    }
    void erase(int &u, int val, int h = 29) {
      if (h == -1) {
        sz[u]--;
        if (sz[u] == 0) reuse[++lr] = u, u = 0;
        return;
      }
      if (val >> h & 1)
        erase(rc[u], val, h - 1);
      else
        erase(lc[u], val, h - 1);
      pushup(u);
      if (sz[u] == 0) reuse[++lr] = u, u = 0;
    }
    int query_xor_max(int &u, int val, int h = 29) {
      if (h == -1) return 0;
      if (val >> h & 1) {
        if (sz[lc[u]])
          return query_xor_max(lc[u], val, h - 1) + (1 << h);
        else if (sz[rc[u]])
          return query_xor_max(rc[u], val, h - 1);
        else
          return 0; // puts("ERROR!"),-1;
      } else {
        if (sz[rc[u]])
          return query_xor_max(rc[u], val, h - 1) + (1 << h);
        else if (sz[lc[u]])
          return query_xor_max(lc[u], val, h - 1);
        else
          return 0; // puts("ERROR!"),-1;
      }
    }
    void init() { tot = 1, sz[1] = rc[1] = lc[1] = 0, lr = 0; }
  } // namespace trie
  int root;

  struct data {
    int a, b, c;
  };
  vector<data> oper[SZ];
  void work(vector<data> &A) {
    trie::init();
    root = 1;
    for (data x : A) {
      if (x.a == 0)
        x.c == 1 ? trie::insert(root, x.b) : trie::erase(root, x.b);
      else
        ans[x.c] = max(ans[x.c], trie::query_xor_max(root, x.b));
    }
  }
  void add_modify(int L, int R, int val, int cnt, int u = 1, int l = 1, int r = n) {
    if (L <= l && r <= R) return oper[u].pb({0, val, cnt}), void();
    int mid = (l + r) >> 1;
    if (L <= mid) add_modify(L, R, val, cnt, u << 1, l, mid);
    if (mid < R) add_modify(L, R, val, cnt, u << 1 | 1, mid + 1, r);
  }
  void add_query(int pos, int val, int id, int u = 1, int l = 1, int r = n) {
    oper[u].pb({1, val, id});
    if (l == r) return;
    int mid = (l + r) >> 1;
    if (pos <= mid)
      add_query(pos, val, id, u << 1, l, mid);
    else
      add_query(pos, val, id, u << 1 | 1, mid + 1, r);
  }
  void dfs(int u = 1, int l = 1, int r = n) {
    work(oper[u]);
    if (l == r) return;
    int mid = (l + r) >> 1;
    dfs(u << 1, l, mid), dfs(u << 1 | 1, mid + 1, r);
  }
} // namespace seg

int w[N], fa[N];
vector<int> g[N];

int sz[N];
int tp[N], bt_pos[N], big[N], pos[N], dfn;
void dfs1(int u, int p) {
  sz[u] = 1;
  for (int v : g[u]) dfs1(v, u), sz[u] += sz[v], sz[v] > sz[big[u]] ? big[u] = v : 0;
}
void dfs2(int u, int p, int topp) {
  tp[u] = topp, pos[u] = ++dfn, a[dfn] = w[u];
  if (big[u]) dfs2(big[u], u, topp);
  for (int v : g[u])
    if (v != big[u]) dfs2(v, u, v);
  bt_pos[u] = dfn;
}
int lQ = 0;
int main() {
  scanf("%d%d", &n, &Q);
  FOR(i, 1, n) scanf("%d", &w[i]);
  FOR(i, 1, n - 1) {
    int x;
    scanf("%d", &x);
    g[x].pb(i + 1);
    fa[i + 1] = x;
  }
  dfs1(1, 0);
  dfs2(1, 0, 1);
  FOR(i, 1, n) seg::add_modify(pos[i], bt_pos[i], w[i], 1);

  FOR(i, 1, Q) {
    int op, x, y;
    scanf("%d%d", &op, &x);
    if (op == 0) {
      scanf("%d", &y);
      seg::add_modify(pos[x], bt_pos[x], w[x], -1);
      seg::add_modify(pos[x], bt_pos[x], y, 1);
      w[x] = y;
    } else {
      seg::add_query(pos[x], w[x], ++lQ);
    }
  }
  seg::dfs();
  FOR(i, 1, lQ) printf("%d\n", ans[i]);
  return 0;
}
/*
 * 线段树维护每个点的到根路径的数集。
 * 1. 一个区间的数集都加入点u。
 * 2. 询问一个点的答案。
 *
 * 询问可以对每个线段树区间分别做，取最大值。
 */
