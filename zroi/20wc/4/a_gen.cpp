#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/

using namespace RA;

int main() {
  // freopen("ain","w",stdout);
  srand(clock());
  int n = r(1, 1e3), Q = r(1, 1e3);
  printf("%d %d\n", n, Q);
  int LIM = r(1, 1e9);
  FOR(i, 1, n) printf("%d%c", r(1, LIM), " \n"[i == n]);
  FOR(i, 1, n - 1) printf("%d%c", r(i, i), " \n"[i == n - 1]);
  FOR(i, 1, Q) {
    if (r(100) < 50) {
      printf("0 %d %d\n", r(1, n), r(1, LIM));
    } else {
      printf("1 %d\n", r(1, n));
    }
  }
  return 0;
}
