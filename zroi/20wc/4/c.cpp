#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int oo = 0x3f3f3f3f;

const int N = 5e4 + 5, M = N * 2, INF = 0x3f3f3f3f;

struct qxx {
  int nex, t, v;
};
qxx e[M];
int h[N], le = 1;
int hh[N];
void add_path(int f, int t, int v) { e[++le] = (qxx){h[f], t, v}, h[f] = le; }
void add_flow(int f, int t, int v) { add_path(f, t, v), add_path(t, f, 0); }
void add_bidir_flow(int f, int t, int v) { add_path(f, t, v), add_path(t, f, v); }
#define FORe(i, _u, _v, _w) \
  for (int i = h[_u], _v, _w; _v = e[i].t, _w = e[i].v, i; i = e[i].nex)
#define FORflowe(i, _u, _v, _w) \
  for (int &i = hh[_u], _v, _w; _v = e[i].t, _w = e[i].v, i; i = e[i].nex)

int s, t;
int dep[N];
queue<int> q;

bool bfs() {
  memset(dep, 0, sizeof(dep));
  dep[s] = 1, q.push(s);
  while (!q.empty()) {
    int u = q.front();
    q.pop();
    FORe(i, u, v, w) if (!dep[v] && w) dep[v] = dep[u] + 1, q.push(v);
  }
  return dep[t] != 0;
}
int dfs(int u, int flow) {
  if (u == t || !flow) return flow;
  int rest = flow;
  FORflowe(i, u, v, w) {
    if (!w || dep[v] != dep[u] + 1) continue;
    int k = dfs(v, min(rest, w));
    e[i].v -= k, e[i ^ 1].v += k, rest -= k;
    if (!rest) break;
  }
  return flow - rest;
}
int dinic() {
  int maxflow = 0;
  while (bfs()) {
    memcpy(hh, h, sizeof(h));
    for (int x; (x = dfs(s, INF));) maxflow += x;
  }
  return maxflow;
}
void init() { memset(h, 0, sizeof(h)), le = 1; }

int n, m, a, b, c;
char grid[100][100];

int A1(int x, int y) { return (x - 1) * m + y; }
int A2(int x, int y) { return n * m + (x - 1) * m + y; }
int B1(int x, int y) { return n * m * 2 + (x - 1) * m + y; }
int B2(int x, int y) { return n * m * 3 + (x - 1) * m + y; }

void ax(int a, int x) { add_flow(s, x, a); }
void a1x(int a, int x) { add_flow(x, t, a); }
void a1xy(int a, int x, int y) { add_flow(x, y, a); }

void go() {
  init();
  scanf("%d%d%d%d%d", &n, &m, &a, &b, &c);
  FOR(i, 1, n) scanf("%s", grid[i] + 1);
  s = 0, t = n * m * 4 + 1;
  FOR(x, 1, n) FOR(y, 1, m) {
    ax(a, A1(x, y));
    a1x(a, A2(x, y));
    a1x(a, B1(x, y));
    ax(a, B2(x, y));
    if (y < m) {
      a1xy(b, A1(x, y + 1), A1(x, y));
      a1xy(b, B1(x, y), B1(x, y + 1));
    } else {
      ax(b, A1(x, y));
      a1x(b, B1(x, y));
    }
    if (x < n) {
      a1xy(b, A2(x, y), A2(x + 1, y));
      a1xy(b, B2(x + 1, y), B2(x, y));
    } else {
      a1x(b, A2(x, y));
      ax(b, B2(x, y));
    }
    if (grid[x][y] == '#') {
      a1xy(c, A1(x, y), A2(x, y));
      a1x(oo, B1(x, y));
      ax(oo, B2(x, y));
    } else {
      a1xy(c, B2(x, y), A1(x, y));
      a1xy(c, A2(x, y), B1(x, y));
      a1xy(oo, A2(x, y), A1(x, y));
    }
  }
  int ans = dinic();
  printf("%d\n", ans);
}
int main() {
  int t;
  scanf("%d", &t);
  FOR(i, 1, t) go();
  return 0;
}
