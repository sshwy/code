#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
#define int long long
const int N = 1e5, P = 998244353, I2 = (P + 1) / 2;

map<int, int> s;
int S(int x) {
  if (x == 0) return 0;
  if (x == 1) return 2;
  if (s.count(x)) return s[x];
  int n = x / 2;
  if (n * 2 == x) {
    return s[x] = (S(n) + (S(n) + S(n - 1)) * I2 + n) % P;
  } else {
    return s[x] = (S(n) + (S(n + 1) + S(n)) * I2 + n + 1) % P;
  }
}

void go() {
  int L, R;
  s.clear(); //不clear会MLE
  scanf("%lld%lld", &L, &R);
  printf("%lld\n", (S(R) - S(L - 1) + P) % P);
}
signed main() {
  int t;
  scanf("%lld", &t);
  FOR(i, 1, t) go();
  return 0;
}
