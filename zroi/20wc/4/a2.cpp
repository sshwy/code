#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 1e5 + 5;

int n, Q;
int w[N], fa[N];
vector<int> g[N];

int query(int u) {
  int res = 0, v = u;
  while (v) res = max(res, w[u] ^ w[v]), v = fa[v];
  return res;
}
int main() {
  scanf("%d%d", &n, &Q);
  FOR(i, 1, n) scanf("%d", &w[i]);
  FOR(i, 1, n - 1) {
    int x;
    scanf("%d", &x);
    g[x].pb(i + 1);
    fa[i + 1] = x;
  }
  FOR(i, 1, Q) {
    int op, x, y;
    scanf("%d%d", &op, &x);
    if (op == 0) {
      scanf("%d", &y);
      w[x] = y;
    } else {
      printf("%d\n", query(x));
    }
  }
  return 0;
}
