#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
using namespace RA;
const int N = 1e6 + 5;

vector<int> g[N];
int fa[N];
int w[N];

int n, m, L;
void dfs(int u) {
  int x = r(1000);
  if (u == 1)
    w[u] = 1;
  else if (x < 950) {
    if (fa[u] && x < 850)
      w[u] = w[fa[u]] + 1;
    else
      w[u] = 1;
  } else
    w[u] = r(1, L);
  for (int v : g[u]) dfs(v);
}
int main() {
  freopen(".fin", "w", stdout);
  srand(clock());

  // n=r(1,200),m=n*2,L=r(1,n-n/2);
  n = r(31, 200), m = n * 2, L = r(1, 20);
  printf("%d %d %d\n", n, m, L);
  FOR(i, 1, n - 1)
  printf("%d%c", fa[i + 1] = r(1, i), " \n"[i == n - 1]), g[fa[i + 1]].pb(i + 1);
  dfs(1);
  FOR(i, 1, n) printf("%d%c", w[i], " \n"[i == n]);
  dfs(1);
  FOR(i, 1, n) printf("%d%c", w[i], " \n"[i == n]);
  dfs(1);
  FOR(i, 1, n) printf("%d%c", w[i], " \n"[i == n]);
  return 0;
}
