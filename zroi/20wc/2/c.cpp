#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 1e5 + 5, Q = 2e5 + 5;

int n, q, T, las;
set<int> s[N];
int catg[N];

int main() {
  scanf("%d%d%d", &n, &q, &T);
  FOR(i, 1, n) s[i].insert(i), catg[i] = i;
  FOR(i, 1, q) {
    int u, v;
    scanf("%d%d", &u, &v);
    u = (u + T * las - 1) % n + 1;
    v = (v + T * las - 1) % n + 1;

    if (catg[u] != catg[v]) {
      if (s[catg[u]].size() > s[catg[v]].size()) swap(u, v);
      for (int x : s[catg[u]]) insert(catg[v], x), catg[x] = v;
    }

        printf("%d\n",las?las=n);
  }
  return 0;
}
