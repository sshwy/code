#include <bits/stdc++.h>
using namespace std;

typedef long long ll;
int n, lim, ans, a[25];
ll sum, s[1 << 21], b[25];

int main() {
  scanf("%d", &n), lim = 1 << n;
  for (int i = 1; i <= n; i++) { scanf("%d", &a[i]), sum += a[i]; }
  auto chk = [&](int x) {
    for (int i = 1; i <= n; i++) {
      b[i] = 1LL * x * a[i] - sum;
      printf("b[%d]=%d\n", i, 1ll * x * a[i] - sum);
    }
    for (int i = 1; i < lim; i++) {
      s[i] = 0;
      for (int j = 0; j < n; j++) {
        if (i >> j & 1) s[i] += b[j + 1];
      }
    }
    int mask = lim - 1;
    for (int i = 1; i <= n; i++) {
      if (!b[i]) mask ^= 1 << (i - 1);
    }
    while (mask) {
      bool flag = 1;
      for (int i = mask; i; i = (i - 1) & mask) {
        if (s[i] == -sum) {
          mask ^= i, flag = 0;
          break;
        }
      }
      if (flag) return 0;
    }
    return 1;
  };
  for (int i = (n + 1) / 2; i <= n; i++) {
    if (chk(i)) printf("%d\n", i), exit(0);
  }
  return 0;
}
