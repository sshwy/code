#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
namespace IO {
  char nc() {
    static char bf[100000], *p1 = bf, *p2 = bf;
    return p1 == p2 && (p2 = (p1 = bf) + fread(bf, 1, 100000, stdin), p1 == p2)
               ? EOF
               : *p1++;
  }
  int rd() {
    int res = 0;
    char c = nc();
    while (!isdigit(c)) c = nc();
    while (isdigit(c)) res = res * 10 + c - '0', c = nc();
    return res;
  }
  int tmp[20], lt;
  char OBF[100000], *OBP = OBF;
  void flush() { fwrite(OBF, 1, OBP - OBF, stdout), OBP = OBF; }
  void wrch(char x) {
    if (OBP == OBF + 99999) flush();
    *OBP = x, ++OBP;
  }
  void wr(int x) {
    lt = 0;
    while (x) tmp[++lt] = x % 10, x /= 10;
    while (lt > 0) wrch(tmp[lt] + '0'), --lt;
  }
  void wr(long long x) {
    lt = 0;
    while (x) tmp[++lt] = x % 10, x /= 10;
    while (lt > 0) wrch(tmp[lt] + '0'), --lt;
  }
  void wr(const char *s) {
    while (*s) wrch(*s), ++s;
  }
  void wr(char x) { wrch(x); }
} // namespace IO
const int N = 2e6 + 5, P = 1e9 + 7;

struct qxx {
  int nex, t;
};
qxx e[N * 2];
int h[N], le = 1;
void add_path(int f, int t) { e[++le] = (qxx){h[f], t}, h[f] = le; }
#define FORe(i, _u, _v) for (int i = h[_u], _v; _v = e[i].t, i; i = e[i].nex)

int n, m, L;

int f[N], f2[N];
int g[N], g2[N];
int w[N], w2[N], fa[N];

int tot; //当前的序列个数
int c[N], b[N]; // c[i]表示到根路径上以权值i结尾的1-i序列的个数（基于w2）

inline void add(int &x, int y) { x = (x + y) % P; }
inline void sub(int &x, int y) { x = (x - y + P) % P; }
void dfs1(int u, int p = 0) {
  int wu = w[u], w2u = w2[u], cw2fau = c[w2[p]];
  if (p) add(c[w2[p]], c[w2[p] - 1]);
  int bwu1 = wu == L ? 0 : b[wu + 1], bwu2 = w2u == L ? 0 : b[w2u + 1];
  f[u] = c[wu - 1];
  f2[u] = c[w2u - 1];
  FORe(i, u, v) dfs1(v, u);
  g[u] = b[wu + 1] - bwu1;
  g2[u] = b[w2u + 1] - bwu2;
  add(b[wu], g[u]);
  if (fa[u]) c[w2[p]] = cw2fau;
}

int main() {
  int ans = 0;
  n = IO::rd(), m = IO::rd(), L = IO::rd();
  c[0] = 1;
  b[L + 1] = 1;
  FOR(i, 2, n) fa[i] = IO::rd(), add_path(fa[i], i);
  FOR(i, 1, n) w2[i] = IO::rd(), w[i] = w2[i];
  dfs1(1);
  int t = ceil(m * 1.0 / n), tmp = 0, tmp2 = 0, tot = 0;
  FOR(i, 1, n) if (w[i] == 1) tot = (tot + g[i]) % P;
  FOR(_, 1, t) {
    FOR(i, 1, n) w[i] = w2[i];
    FOR(i, 1, L) c[i] = 0;
    FOR(i, 1, L) b[i] = 0;
    FOR(i, 1, n) {
      ++tmp;
      if (tmp <= m)
        w2[i] = IO::rd();
      else
        w2[i] = w[i];
    }
    dfs1(1);
    FOR(i, 1, n) {
      sub(tot, 1ll * f[i] * g[i] % P);
      add(tot, 1ll * f2[i] * g2[i] % P);
      ++tmp2;
      if (tmp2 <= m)
        ans = (ans + 1ll * tmp2 * tot) % P;
      else
        break;
    }
    if (tmp2 > m) break;
  }
  printf("%d\n", ans);
  return 0;
}
