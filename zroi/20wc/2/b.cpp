#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 21;

int n, nn;
int a[N];
long long b[1 << N], f[1 << N], sum;

bool check(int k) {
  FOR(i, 0, n - 1) b[1 << i] = 1ll * k * a[i] - sum;
  FOR(i, 1, nn - 1) { f[i] = f[i - (i & -i)] + b[i & -i]; }
  int mask = nn - 1;
  while (mask) {
    int fl = 0;
    for (int i = mask; i; i = (i - 1) & mask) {
      if (f[i] == -sum) {
        mask -= i;
        fl = 1;
        break;
      }
    }
    if (!fl) return 0; //无法分组
  }
  return 1;
}
int main() {
  scanf("%d", &n);
  nn = 1 << n;
  FOR(i, 0, n - 1) scanf("%d", &a[i]), sum += a[i];
  FOR(i, (n + 1) / 2, n) if (check(i)) return printf("%d", i), 0;
  return 0;
}
