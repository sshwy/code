#include <algorithm>/*{{{*/
#include <bitset>
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 2000;

int n, m, T;
short D[N][N], R[N][N];
char a[N][N];

struct data {
  short a, b, id;
} qry[N];
bool cmp1(data x, data y) { return x.a / T == y.a / T ? x.b < y.b : x.a < y.a; }

bitset<1510> A[N], B[N];
vector<pair<short, short>> Di[N], Ri[N]; // Di[k]: all D[i][j]=k

int cura = 1, curb = 1, ans[N], curans;

void addA() {
  for (pii p : Di[cura]) A[p.fi][p.se] = 0;
  ++cura;
}
void subA() {
  --cura;
  for (pii p : Di[cura]) A[p.fi][p.se] = 1;
}
void addB() {
  for (pii p : Ri[curb]) B[p.fi][p.se] = 0;
  ++curb;
}
void subB() {
  --curb;
  for (pii p : Ri[curb]) B[p.fi][p.se] = 1;
}
void print() {
  printf("A:\n");
  FOR(i, 1, n) FOR(j, 1, n) printf("%d%c", (int)A[i][j], " \n"[j == n]);
  printf("B:\n");
  FOR(i, 1, n) FOR(j, 1, n) printf("%d%c", (int)B[i][j], " \n"[j == n]);
}
int getans() {
  int res = 0;
  FOR(i, 1, n) {
    bitset<1510> t = (B[i] & B[i + cura - 1] & A[i] & (A[i] >> (curb - 1)));
    res += t.count();
  }
  return res;
}
int main() {
  scanf("%d%d", &n, &m);
  T = sqrt(n);

  FOR(i, 1, n) scanf("%s", a[i] + 1);
  ROF(i, n, 1) ROF(j, n, 1) {
    if (a[i][j] == '0')
      D[i][j] = R[i][j] = 0;
    else
      D[i][j] = D[i + 1][j] + 1, R[i][j] = R[i][j + 1] + 1;
    Di[D[i][j]].pb({i, j});
    Ri[R[i][j]].pb({i, j});
  }
  FOR(i, 1, m) {
    int x, y;
    scanf("%d%d", &x, &y);
    qry[i] = {x, y, i};
  }
  sort(qry + 1, qry + m + 1, cmp1);

  FOR(i, 1, n) FOR(j, 1, n) if (D[i][j]) A[i][j] = 1, ++curans;
  FOR(i, 1, n) FOR(j, 1, n) if (R[i][j]) B[i][j] = 1;
  FOR(i, 1, m) {
    int a = qry[i].a, b = qry[i].b, id = qry[i].id;
    while (cura < a) addA();
    while (cura > a) subA();
    while (curb < b) addB();
    while (curb > b) subB();
    ans[id] = getans();
  }
  FOR(i, 1, m) printf("%d\n", ans[i]);
  return 0;
}
