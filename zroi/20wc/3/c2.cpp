#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 1e5 + 5, P = 998244353;

int x, n, m;
int A[N], B[N], ans, p[N];

int main() {
  scanf("%d%d%d", &x, &n, &m);
  FOR(i, 1, n) scanf("%d", &A[i]);
  FOR(i, 1, m) scanf("%d", &B[i]);
  FOR(i, 1, n + m) p[i] = i;
  do {
    int y = x;
    FOR(i, 1, n + m) {
      if (p[i] <= n)
        y = y / A[p[i]];
      else
        y = y % B[p[i] - n];
    }
    if (y == 0) ++ans;
  } while (next_permutation(p + 1, p + n + m + 1));
  printf("%d\n", ans);
  return 0;
}
