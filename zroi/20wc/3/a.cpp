#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int P = 998244353, I2 = (P + 1) / 2;
int pw(int a, int m) {
  int res = 1;
  while (m) m & 1 ? res = 1ll * res * a % P : 0, a = 1ll * a * a % P, m >>= 1;
  return res;
}
const int N = 17, M = N * N;

int det(int f[N][N], int n) {
  FOR(i, 0, n - 1) {
    FOR(j, i, n - 1) if (f[j][i]) {
      FOR(k, 0, n - 1) swap(f[i][k], f[j][k]);
      break;
    }
    if (!f[i][i]) return 0;
    FOR(j, 0, n - 1) {
      if (j == i) continue;
      int rate = f[j][i] * 1ll * pw(f[i][i], P - 2) % P;
      FOR(k, i, n - 1) f[j][k] = (f[j][k] - 1ll * f[i][k] * rate) % P;
    }
  }
  int res = 1;
  FOR(i, 0, n - 1) res = 1ll * res * f[i][i] % P;
  res = (res + P) % P;
  return res;
}

int n, m, nn, ans;

int g[N][N], dg[N];
vector<pii> E;
vector<int> G[N];

int h[1 << N][N],
    H[1 << N]; // h[i,j]:从集合i中编号最大的点出发，经过集合中的所有点恰好一次，走到j的方案数。
int I[N];

int main() {
  scanf("%d%d", &n, &m);
  nn = 1 << n;
  FOR(i, 1, m) {
    int u, v;
    scanf("%d%d", &u, &v);
    --u, --v;
    E.pb({u, v});
    G[u].pb(v), G[v].pb(u);
  }
  // forest
  memset(g, 0, sizeof(g));
  memset(dg, 0, sizeof(dg));
  for (pii x : E) g[x.fi][x.se] = g[x.se][x.fi] = -1, dg[x.fi]++, dg[x.se]++;
  FOR(i, 0, n - 1) g[i][n] = g[n][i] = -1, dg[i]++, dg[n]++;
  FOR(i, 0, n) g[i][i] = dg[i];
  int T = det(g, n);
  // cout<<"T="<<T<<endl;

  memset(g, 0, sizeof(g));
  for (pii x : E) g[x.fi][x.se] = g[x.se][x.fi] = 1;
  FOR(i, 0, n - 1) {
    int lim = 1 << i;
    // printf("lim=%d\n",lim);
    h[1 << i][i] = 1; //自己到自己
    FOR(j, 0, lim - 1) {
      int mask = j | (1 << i);
      // printf("mask=%d\n",mask);
      FOR(u, 0, n - 1) if (mask >> u & 1) {
        if (h[mask][u]) {
          // printf("h[%d,%d]=%d\n",mask,u,h[mask][u]);
          for (int v : G[u]) {
            if (v < i && !(mask >> v & 1)) {
              h[mask | 1 << v][v] = (h[mask | 1 << v][v] + h[mask][u]) % P;
            }
          }
        }
        if (g[i][u] && __builtin_popcount(mask) > 2)
          H[mask] = (H[mask] + h[mask][u]) % P;
      }
    }
  }
  FOR(i, 0, nn - 1) if (H[i]) H[i] = 1ll * H[i] * I2 % P;
  // FOR(i,0,nn-1)if(H[i])printf("H[%d]=%d\n",i,H[i]);
  FOR(mask, 0, nn - 1) {
    if (!H[mask]) continue;
    FOR(i, 0, n) I[i] = -1;
    int cnt = 0;
    FOR(i, 0, n - 1) {
      if (mask >> i & 1)
        I[i] = 0; //缩到0号点
      else
        I[i] = ++cnt;
    }
    ++cnt;
    memset(g, 0, sizeof(g));
    memset(dg, 0, sizeof(dg));
    for (pii x : E)
      if (I[x.fi] != I[x.se])
        g[I[x.fi]][I[x.se]]--, g[I[x.se]][I[x.fi]]--, dg[I[x.fi]]++, dg[I[x.se]]++;
    int t = __builtin_popcount(mask);
    g[0][cnt] = g[cnt][0] = -t, dg[0] += t, dg[cnt] += t;
    FOR(i, 1, cnt - 1) g[i][cnt]--, g[cnt][i]--, dg[i]++, dg[cnt]++;
    FOR(i, 0, cnt) g[i][i] = dg[i];
    int S = det(g, cnt);
    // printf("mask=%d,S=%d\n",mask,S);
    ans = (ans + 1ll * H[mask] * S) % P;
  }
  ans = (ans + T) % P;
  printf("%d\n", ans);
  return 0;
}
