#include <bits/stdc++.h>
using namespace std;

typedef long long LL;
typedef pair<int, int> P;

const int mod = 998244353;

inline void Inc(int &x, int y = 0) { x += y, x -= x >= mod ? mod : 0; }
inline void Dec(int &x, int y = 0) { x -= y, x += x < 0 ? mod : 0; }
inline int Add(int x, int y) {
  x += y;
  return x >= mod ? x - mod : x;
}
inline int Sub(int x, int y) {
  x -= y;
  return x < 0 ? x + mod : x;
}

inline int Pow(int x, int y) {
  int res = 1;
  for (; y; y >>= 1, x = (LL)x * x % mod)
    if (y & 1) res = (LL)res * x % mod;
  return res;
}

template <class T> inline void read(T &x) {
  x = 0;
  char c = getchar();
  int flg = 0;
  while (!isdigit(c)) flg |= c == '-', c = getchar();
  while (isdigit(c)) x = x * 10 + c - '0', c = getchar();
  if (flg) x = -x;
}

template <class T> inline void print(T x) {
  x < 10 ? putchar('0' + x) : (print(x / 10), putchar('0' + x % 10));
}

const int N = 1510;

bitset<N> G[12][N], A[N]; // 第i行到第i + 2 ^ j - 1行

int lg[N], ans[N];

inline bitset<N> get(int l, int r) {
  int len = lg[r - l + 1];
  return G[len][l] & G[len][r - (1 << len) + 1];
}

char s[N];

vector<P> fucked[N], q[N];

int main() {
  int n, m;
  read(n), read(m);
  for (int i = 2; i <= n; i++) lg[i] = lg[i >> 1] + 1;
  for (int i = 1; i <= n; i++) {
    scanf("%s", s + 1);
    for (int j = 1; j <= n; j++) A[i][j] = (int)s[j] - '0';
    G[0][i] = A[i];
  }
  for (int i = 1; i < 12; i++)
    for (int j = 1; j + (1 << i) - 1 <= n; j++)
      G[i][j] = G[i - 1][j] & G[i - 1][j + (1 << i - 1)];
  for (int i = 1; i <= n; i++)
    for (int j = n, ct = 0; j >= 1; j--) {
      if (A[i][j])
        ct++, fucked[ct].push_back(P(i, j));
      else
        ct = 0;
      // ct: 当前点以及右边有多少个连续的1
    }
  for (int i = 1, r, c; i <= m; i++) read(r), read(c), q[c].push_back(P(r, i));
  for (int i = 1; i <= n; i++) { // 枚举列的限制
    for (int j = 1; j <= n; j++)
      for (auto t : q[i]) {
        if (j + t.first - 1 > n) continue;
        auto tmp = get(j, j + t.first - 1);
        ans[t.second] += (tmp & (tmp >> i - 1) & A[j] & A[j + t.first - 1]).count();
      }
    for (auto p : fucked[i])
      A[p.first][p.second] = 0; // 这些点已不能作为左上角/左下角
  }
  for (int i = 1; i <= m; i++) print(ans[i]), puts("");
}
