// by Sshwy
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int M = 1e5 + 2, N = 1e7 + 2, P = 998244353, K = 1e5 + 5, SZ = 1 << 18;

int pw(int a, int m) {
  int res = 1;
  while (m) m & 1 ? res = 1ll * res * a % P : 0, a = 1ll * a * a % P, m >>= 1;
  return res;
}
int fac[N + M], fnv[N + M];
int binom(int n, int m) {
  if (n < m) return 0;
  return fac[n] * 1ll * fnv[m] % P * fnv[n - m] % P;
}

int tr[SZ];
int init(int n) {
  int len = 1;
  while (len < n) len <<= 1;
  FOR(i, 0, len - 1)
  tr[i] = tr[i >> 1] >> 1 | ((i & 1) * (len >> 1)); // BUG#2:len-1写成len导致UB
  return len;
}
void dft(int *f, int len, int tag) {
  FOR(i, 0, len - 1) if (i < tr[i]) swap(f[i], f[tr[i]]);
  for (int j = 1; j < len; j <<= 1)
    for (int i = 0, wn = pw(3, P - 1 + (P - 1) / (j << 1) * tag); i < len;
         i += j << 1)
      for (int k = i, u, v, w = 1; k < i + j; k++, w = 1ll * w * wn % P)
        u = f[k], v = 1ll * w * f[k + j] % P, f[k] = (u + v) % P,
        f[k + j] = (u - v + P) % P;
  if (tag == -1) {
    int ilen = pw(len, P - 2);
    FOR(i, 0, len - 1) f[i] = 1ll * f[i] * ilen % P;
  }
}

int m, n;
int a[M], s;
int A[SZ], B[SZ];

vector<int> f[M];

int sig(int x) { return 1 - 2 * (x & 1); }

void solve(int l, int r) {
  if (l == r) return;
  int mid = (l + r) >> 1;
  solve(l, mid), solve(mid + 1, r);

  FOR(i, 0, (int)f[mid].size() - 1) A[i] = f[mid][i];
  FOR(i, 0, (int)f[r].size() - 1) B[i] = f[r][i];
  int len = init(f[mid].size() + f[r].size());
  FOR(i, f[mid].size(), len - 1) A[i] = 0;
  FOR(i, f[r].size(), len - 1) B[i] = 0;

  dft(A, len, 1), dft(B, len, 1);
  FOR(i, 0, len - 1) A[i] = 1ll * A[i] * B[i] % P;
  dft(A, len, -1);

  f[r] = vector<int>(A, A + (int)f[mid].size() + (int)f[r].size() - 1);
}

int main() {
  scanf("%d%d", &m, &n);
  FOR(i, 1, m) scanf("%d", &a[i]), s += a[i];

  if (m == 1) return printf("%d\n", pw(n, a[1])), 0;

  int lim = max(s, n + m);
  fac[0] = 1;
  FOR(i, 1, lim) fac[i] = 1ll * fac[i - 1] * i % P;
  fnv[lim] = pw(fac[lim], P - 2);
  ROF(i, lim, 1) fnv[i - 1] = 1ll * fnv[i] * i % P;

  FOR(i, 1, m) {
    FOR(j, 0, a[i])
    A[j] = fnv[j] * 1ll * (P + sig(j)) % P, B[j] = fnv[j] * 1ll * pw(j, a[i]) % P;
    int len = init(a[i] + 1 + a[i] + 1);
    assert(len <= SZ);
    FOR(j, a[i] + 1, len - 1)
    A[j] = B[j] = 0; // BUG#1: len-1写成len导致溢出，把s赋成0
    dft(A, len, 1), dft(B, len, 1);
    FOR(i, 0, len - 1) A[i] = A[i] * 1ll * B[i] % P;
    dft(A, len, -1); // A: S(a[i],j)
    FOR(j, 0, a[i]) f[i].pb(A[j] * 1ll * fac[j] % P);
  }

  solve(1, m);
  vector<int> &F = f[m];

  int ans = 0;
  assert(F.size() > s);
  // printf("%d %d\n",F.size(),s);
  FOR(i, 0, s) ans = (ans + 1ll * F[i] * binom(n + m - 1, m + i - 1)) % P;

  printf("%d\n", ans);

  return 0;
}
