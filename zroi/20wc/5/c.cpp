// by Sshwy
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 200, M = 2e5, M2 = 21, oo = 0x3f3f3f3f;

struct qxx {
  int nex, t, v, c;
};
qxx e[M];
int h[N], le = 1;
void add_path(int f, int t, int v, int c) {
  e[++le] = (qxx){h[f], t, v, c}, h[f] = le;
}
void add_flow(int f, int t, int v, int c) {
  add_path(f, t, v, c), add_path(t, f, 0, -c);
}
#define FORe(i, _u, _v, _w, _c)                                             \
  for (int i = h[_u], _v, _w, _c; _v = e[i].t, _w = e[i].v, _c = e[i].c, i; \
       i = e[i].nex)

int s, t;
long long maxflow, mincost;
int d[N], incf[N], vis[N], pre[N];
queue<int> q;

bool spfa() {
  memset(d, 0x3f, sizeof(d));
  memset(vis, 0, sizeof(vis));
  while (q.size()) q.pop();

  d[s] = 0, incf[s] = oo, q.push(s);
  while (!q.empty()) {
    int u = q.front();
    q.pop();
    vis[u] = 0;
    FORe(i, u, v, w, c) {
      if (!w || d[v] <= d[u] + c) continue;
      d[v] = d[u] + c, incf[v] = min(incf[u], w), pre[v] = i;
      if (!vis[v]) q.push(v), vis[v] = 1;
    }
  }
  return d[t] != oo;
}
void update() {
  maxflow += incf[t];
  mincost += incf[t] * 1ll * d[t];
  for (int u = t; u != s; u = e[pre[u] ^ 1].t)
    e[pre[u]].v -= incf[t], e[pre[u] ^ 1].v += incf[t];
}

int n, m;
int fa[N], a[N], b[N];
int S[N], T[N], V[N];

int f(int x, int y) { return a[x] * y * y + b[x] * y; }

int main() {
  scanf("%d%d", &n, &m);
  s = 0, t = n + 1;
  FOR(i, 2, n) { scanf("%d%d%d", fa + i, a + i, b + i); }
  long long ans = 0;
  FOR(i, 1, m) {
    scanf("%d%d%d", S + i, T + i, V + i);
    ans += V[i];

    add_flow(s, T[i], 1, 0);
    add_flow(S[i], t, 1, 0);
    add_flow(T[i], S[i], 1, V[i]);
  }
  FOR(i, 2, n) {
    FOR(j, 1, m) { add_flow(i, fa[i], 1, f(i, j) - f(i, j - 1)); }
  }
  while (spfa()) update();
  ans -= mincost;
  printf("%lld\n", ans);
  return 0;
}
