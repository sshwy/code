// by Sshwy
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int SZ = 1 << 16;

int n, m, T;
multiset<int> s;
int t[SZ], lt;

void add(int x) { s.insert(x); }
void rem(int x) { s.erase(s.find(x)); }

typedef multiset<int>::iterator itr;
const int LIM = 32;

int query(int l, int r) {
  lt = 0;
  int res = 0;
  itr i = s.upper_bound(r);
  int cnt = 0;
  while (cnt < LIM) {
    if (i == s.begin()) break;
    --i;
    if (*i < l) break;
    ++cnt;
    FOR(j, 1, lt) res = max(res, t[j] & *i);
    t[++lt] = *i;
  }
  return res;
}
namespace IO {
  const int BSZ = 100000;
  char nc() {
    static char bf[BSZ], *p1 = bf, *p2 = bf;
    return p1 == p2 && (p2 = (p1 = bf) + fread(bf, 1, BSZ, stdin), p1 == p2) ? EOF
                                                                             : *p1++;
  }
  int rd() {
    int res = 0;
    char c = nc();
    while (!isdigit(c)) c = nc();
    while (isdigit(c)) res = res * 10 + c - '0', c = nc();
    return res;
  }
  long long rdll() {
    long long res = 0;
    char c = nc();
    while (!isdigit(c)) c = nc();
    while (isdigit(c)) res = res * 10 + c - '0', c = nc();
    return res;
  }
  int tmp[20], lt;
  char OBF[BSZ], *OBP = OBF;
  void flush() { fwrite(OBF, 1, OBP - OBF, stdout), OBP = OBF; }
  void wrch(char x) {
    if (OBP == OBF + BSZ - 1) flush();
    *OBP = x, ++OBP;
  }
  void wr(int x, int base = 10) {
    if (x == 0) return wrch('0'), void();
    lt = 0;
    while (x) tmp[++lt] = x % base, x /= base;
    while (lt > 0) wrch(tmp[lt] + '0'), --lt;
  }
  void wr(long long x, int base = 10) {
    if (x == 0) return wrch('0'), void();
    lt = 0;
    while (x) tmp[++lt] = x % base, x /= base;
    while (lt > 0) wrch(tmp[lt] + '0'), --lt;
  }
  void wr(const char *s) {
    while (*s) wrch(*s), ++s;
  }
  void wr(char x) { wrch(x); }
} // namespace IO
int main() {
  n = IO::rd(), m = IO::rd(), T = IO::rd();
  int las = 0;
  FOR(i, 1, n) {
    int x;
    x = IO::rd();
    add(x);
  }
  FOR(i, 1, m) {
    int op, x, y;
    op = IO::rd(), x = IO::rd(), T ? x ^= las : 0;
    if (op == 1)
      add(x);
    else if (op == 2)
      rem(x);
    else
      y = IO::rd(), (T ? y ^= las : 0), IO::wr(las = query(x, y)), IO::wr('\n');
  }
  IO::flush();
  return 0;
}
