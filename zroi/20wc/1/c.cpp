#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 3000, Q = 5e4 + 5;

int n, q;
int LIM = 2100;

struct present {
  int l, r;
  double k, b;
  void read() { scanf("%d%d%lf%lf", &l, &r, &k, &b); }
} a[N];
bool cmp(present x, present y) { return x.k < y.k; }

void dp_tr(double *f, int w, double v) {
  ROF(i, LIM - w, 0) f[i + w] = min(f[i + w], f[i] + v);
}
double f[N][N], g[50][N];
void solve(int l, int r, int id = 0) {
  if (l == r) return memcpy(f[l], g[id], sizeof(g[id])), void();
  int mid = (l + r) >> 1;

  memcpy(g[id + 1], g[id], sizeof(g[id]));
  FOR(i, mid + 1, r) dp_tr(g[id + 1], a[i].l, a[i].b);
  solve(l, mid, id + 1);

  memcpy(g[id + 1], g[id], sizeof(g[id]));
  FOR(i, l, mid) dp_tr(g[id + 1], a[i].r, (a[i].r - a[i].l) * a[i].k + a[i].b);
  solve(mid + 1, r, id + 1);
}

pair<double, int> qry[Q];
double ans[Q];

int que[N], ql, qr;

int main() {
  scanf("%d%d", &n, &q);
  FOR(i, 1, n) a[i].read();
  sort(a + 1, a + n + 1, cmp);

  FOR(i, 1, LIM) g[0][i] = 2e9;
  solve(1, n);

  FOR(i, 1, q) scanf("%lf", &qry[i].fi), qry[i].se = i;

  sort(qry + 1, qry + q + 1);
  FOR(i, 1, q) ans[i] = 1e9;

  FOR(i, 1, n) {
    ql = 1, qr = 0;
    int len = a[i].r - a[i].l - 1, pos = 0;
    double X = -a[i].l * a[i].k + a[i].b;
    FOR(j, 0, LIM) {
      while (ql <= qr && f[i][que[qr]] - que[qr] * a[i].k >= f[i][j] - j * a[i].k)
        --qr;
      que[++qr] = j;
      while (ql <= qr && j - que[ql] > len) ++ql;
      double Y = ql <= qr ? f[i][que[ql]] - que[ql] * a[i].k : 2e9;
      while (pos < q && floor(qry[pos + 1].fi - a[i].l) < j) ++pos;
      while (pos < q && j == floor(qry[pos + 1].fi - a[i].l)) {
        ++pos, ans[qry[pos].se] = min(ans[qry[pos].se], X + Y + qry[pos].fi * a[i].k);
      }
      if (pos == q) break;
    }
  }
  FOR(i, 1, q) printf("%.15lf\n", ans[i]);
  return 0;
}
