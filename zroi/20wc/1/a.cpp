#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 1e5 + 5, INF = 0x3f3f3f3f;

vector<int> g[N], rg[N];

int n, m, k;
int a[N], ans[N], cnt[N];

pii p[N];

void upd(int u, int val) {
  if (~ans[u]) return;
  if (cnt[u] <= k) return;
  ans[u] = val;
  for (int v : rg[u]) cnt[v]++;
  for (int v : rg[u]) upd(v, val);
}
int main() {
  scanf("%d%d%d", &n, &m, &k);
  FOR(i, 1, n) scanf("%d", &a[i]);
  FOR(i, 1, m) {
    int x, y;
    scanf("%d%d", &x, &y);
    g[x].pb(y);
    rg[y].pb(x);
  }
  FOR(i, 1, n) p[i] = {a[i], i};
  FOR(i, 1, n) ans[i] = -1;
  sort(p + 1, p + n + 1);
  p[n + 1].fi = INF;
  ROF(i, n, 1) {
    if (~ans[p[i].se]) continue;
    // if(p[i].fi==p[i+1].fi||~ans[p[i].se])continue;
    int u = p[i].se, val = p[i].fi;
    // printf("u=%d,val=%d\n",u,val);
    ans[u] = val;
    for (int v : rg[u]) cnt[v]++;
    for (int v : rg[u]) upd(v, val);
  }
  FOR(i, 1, n) printf("%d\n", ans[i]);
  return 0;
}
