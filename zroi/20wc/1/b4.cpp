#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
#define int long long
const int N = 1e5 + 5;

int n;
int a[N], t[N], x, y, ans;
int aL = -1, aR = -1;

int get_max_y() {
  int res = 0;
  FOR(i, 1, n - 2) {
    int c = min(min(t[i], t[i + 1]), t[i + 2]);
    t[i] -= c, t[i + 1] -= c, t[i + 2] -= c;
    res += c;
  }
  return res;
}
int get_min_y() { //-1表示不胡
  int res = 0;
  memcpy(t, a, sizeof(a));
  FOR(i, 1, n - 2) {
    int c = t[i] % 3;
    if (t[i + 1] < c || t[i + 2] < c) return -1;
    t[i] -= c, t[i + 1] -= c, t[i + 2] -= c;
    res += c;
  }
  if (t[n] % 3 || t[n - 1] % 3) return -1;
  FOR(i, 1, n) t[i] /= 3;
  int mx = get_max_y();
  int L = res, R = res + mx * 3;
  if (aL == -1)
    aL = L, aR = R;
  else
    aL = min(aL, L), aR = max(aR, R);
  return res;
}
signed main() {
  scanf("%lld", &n);
  int s = 0;
  FOR(i, 1, n) scanf("%lld", &a[i]), s = (s + a[i]) % 3;
  if (s != 2) return puts("0"), 0;
  FOR(i, 1, n) {
    if (a[i] < 2) continue;
    a[i] -= 2;
    get_min_y();
    a[i] += 2;
  }
  if (aL == -1)
    printf("0\n");
  else {
    ans = (aR - aL) / 3 + 1;
    printf("%lld\n", ans);
  }
  return 0;
}
