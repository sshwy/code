#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 1e5 + 5;

int n;
int a[N], b[N], t[N], x;
map<pair<int, int>, int> ans;

void check() {
  FOR(i, 1, n) t[i] = a[i];
  int y = 0;
  FOR(i, 1, n - 2) {
    if (t[i] < 0) return;
    int c = t[i];
    y += c;
    t[i] -= c;
    t[i + 1] -= c;
    t[i + 2] -= c;
  }
  FOR(i, 1, n) if (t[i]) return;
  // if(!ans.count({x,y}))printf("x=%d,y=%d\n",x,y);
  ans[{x, y}] = 1;
}
void dfs(int cur) {
  if (cur > n) return check(), void();
  int lim = a[cur] / 3;
  FOR(i, 0, lim) {
    a[cur] -= i * 3;
    b[cur] = i;
    x += i;
    dfs(cur + 1);
    x -= i;
    a[cur] += i * 3;
  }
}
int main() {
  scanf("%d", &n);
  FOR(i, 1, n) scanf("%d", &a[i]);
  FOR(i, 1, n) {
    if (a[i] < 2) continue;
    a[i] -= 2;
    // printf("(%d,%d):\n",i,i);
    dfs(1);
    a[i] += 2;
  }
  printf("%d\n", ans.size());
  return 0;
}
