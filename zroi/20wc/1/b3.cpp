#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 1e5 + 5;

int n;
int a[N], t[N], x, y;
map<pair<int, int>, int> ans;

void dfs(int cur) {
  if (cur > n - 2) {
    if (a[cur] % 3 || a[cur + 1] % 3) return;
    ans[{x + a[cur] / 3 + a[cur + 1] / 3, y}] = 1;
    return;
  }
  int lim = a[cur] / 3;
  FOR(i, 0, lim) {
    int rest = a[cur] - i * 3;
    if (rest > a[cur + 1] || rest > a[cur + 2]) continue;
    a[cur] -= i * 3, x += i;

    a[cur] -= rest;
    a[cur + 1] -= rest;
    a[cur + 2] -= rest;
    y += rest;

    dfs(cur + 1);

    a[cur] += rest;
    a[cur + 1] += rest;
    a[cur + 2] += rest;
    y -= rest;

    x -= i, a[cur] += i * 3;
  }
}
int main() {
  scanf("%d", &n);
  FOR(i, 1, n) scanf("%d", &a[i]);
  FOR(i, 1, n) {
    if (a[i] < 2) continue;
    a[i] -= 2;
    // printf("(%d,%d):\n",i,i);
    dfs(1);
    a[i] += 2;
  }
  printf("%d\n", ans.size());
  // for(pair<pii,int> x:ans)printf("%d %d\n",x.fi.fi,x.fi.se);
  return 0;
}
