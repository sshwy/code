#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
#define int long long
const int N = 1e5 + 5;

int n;
int a[N];

struct state {
  int a[5];
  int xmin, len;
  void push(int x) {
    if (xmin == -1) return;
    a[4] = x;
    int c = a[2] % 3;
    if (a[3] < c || a[4] < c) return xmin = -1, void();
    a[2] -= c, a[3] -= c, a[4] -= c, xmin += c;
    c = min(min(a[0], a[1]), a[2]) / 3 * 3;
    a[0] -= c, a[1] -= c, a[2] -= c, len += c;
    a[0] = a[1], a[1] = a[2], a[2] = a[3], a[3] = a[4];
  }
};

state pre[N], suf[N];

int al = -1, ar = -1;
void upd(int L, int R) {
  if (L == -1 || R == -1) return;
  if (al == -1)
    al = L, ar = R;
  else
    al = min(al, L), ar = max(ar, R);
}
void merge(state L, int x, state R) {
  if (L.xmin == -1 || R.xmin == -1) return;
  L.xmin += R.xmin, L.len += R.len;
  L.push(x);
  L.push(R.a[3]);
  L.push(R.a[2]);
  L.push(R.a[1]);
  L.push(R.a[0]);
  L.push(0);
  L.push(0);
  if (L.xmin == -1) return;
  upd(L.xmin, L.xmin + L.len);
}

signed main() {
  scanf("%lld", &n);
  FOR(i, 1, n) scanf("%lld", &a[i]);
  FOR(i, 1, n) pre[i] = pre[i - 1], pre[i].push(a[i]);
  ROF(i, n, 1) suf[i] = suf[i + 1], suf[i].push(a[i]);
  FOR(i, 1, n) {
    if (a[i] < 2) continue;
    merge(pre[i - 1], a[i] - 2, suf[i + 1]);
  }
  if (al == -1 || ar == -1) return printf("0\n"), 0;
  int ans = (ar - al) / 3 + 1;
  printf("%lld\n", ans);

  return 0;
}
