#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/

using namespace RA;

int main() {
  srand(clock());
  int T = 1000, typ = 0, len = 0;
  printf("%d %d\n", T, typ);
  printf("0 %d %d\n", r(1, len + 1), r(1, 1e6)), ++len;
  FOR(i, 2, T) {
    if (r(0, 1)) {
      int L = r(1, len), R = r(L, len);
      printf("%d %d %d %d\n", 1, L, R, r(1, 1e6));
    } else {
      printf("%d %d %d\n", 0, r(1, len + 1), r(1, 1e6));
      ++len;
    }
  }
  return 0;
}
