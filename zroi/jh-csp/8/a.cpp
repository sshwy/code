#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 2003, P = 1e9 + 7;

int n, m;
int c[N][N];

int main() {
  c[0][0] = 1;
  FOR(i, 0, N - 1) c[i][0] = 1;
  FOR(i, 1, N - 1) {
    FOR(j, 1, i - 1) { c[i][j] = (c[i - 1][j - 1] + c[i - 1][j]) % P; }
    c[i][i] = 1;
  }
  int t;
  scanf("%d", &t);
  FOR(i, 1, t) {
    int n, m;
    scanf("%d%d", &m, &n);
    printf("%d\n", c[n][m]);
  }
  return 0;
}
