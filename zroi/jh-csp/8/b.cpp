#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long LL;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
LL mul(LL a, LL b, LL p) {
  a %= p, b %= p;
  if (p <= 1000000000ll) return 1ll * a * b % p;
  LL d = floor(a * (long double)b / p);
  LL res = (a * b - d * p) % p;
  if (res < 0) res += p;
  return res;
}
LL pw(LL a, LL m, LL p) {
  a %= p;
  LL res = 1;
  while (m) m & 1 ? res = mul(res, a, p) : 0, a = mul(a, a, p), m >>= 1;
  return res;
}
const LL PP = 998244353;
LL P;

LL i2 = 499122177;
LL I2 = 498245894148194305ll;

LL n;

LL p[50], c[50], lp;
void div(LL x) {
  lp = 0;
  for (LL i = 2; i * i <= x; i++) {
    if (x % i) continue;
    p[++lp] = i, c[lp] = 0;
    while (x % i == 0) c[lp]++, x /= i;
  }
  if (x > 1) p[++lp] = x, c[lp] = 1;
}
LL tot1;
void work(LL x, LL phi) {
  // printf("work(%lld,%lld)\n",x,phi);
  // assert(n%x==0);//n divided by x
  if (x % 2 == 0) phi = mul(phi, 2, P);
  tot1 = (tot1 + mul(phi, pw(2, n / x, P), P)) % P;
}
void dfs(int k, LL num, LL phi) { // phi(num)
  work(num, phi);
  if (k >= lp) return;
  FOR(i, k + 1, lp) { // now we chose p[i]
    LL pj = 1;
    FOR(j, 1, c[i]) { // p^j
      pj *= p[i];
      dfs(i, num * pj, phi * pj / p[i] * (p[i] - 1));
    }
  }
}
LL calc(LL n) { // don't consider condition 3.
  div(n);
  tot1 = 0;
  dfs(0, 1, 1);
  if (n % PP == 0)
    tot1 = mul(tot1 / PP, pw(n * 2 / PP, P - 2, P), P) % PP;
  else
    tot1 = mul(tot1, pw(n * 2, P - 2, P), P);
  return tot1;
}
void go() {
  // cin>>n;
  scanf("%lld", &n);
  if (n % PP == 0) {
    P = PP * PP;
    LL ans = mul(calc(n) + pw(2, n / 2, P), i2, PP);
    printf("%lld\n", ans);
  } else {
    P = PP;
    LL ans = mul(calc(n) + pw(2, n / 2, P), i2, P);
    printf("%lld\n", ans);
  }
}
int main() {
  int t;
  scanf("%d", &t);
  FOR(i, 1, t) go();
  return 0;
}
/*
 * BUG#1:之前写的时侯，快速幂里没有取模（a%=p）。当n是LL时n*2可能就爆了
 * BUG#2:没有考虑n是模数的倍数的情况（把模数平方，然后计算，最后除n的时侯除回来
 */
