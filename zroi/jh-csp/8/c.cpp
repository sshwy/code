#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 2e5 + 5;

int n;
int a[N];
int gcd(int a, int b) { return b ? gcd(b, a % b) : a; }
int g(int l, int r) {
  int res = 0;
  FOR(i, 1, n) FOR(j, i + 1, n) {
    if (l <= i && i <= r) continue;
    if (l <= j && j <= r) continue;
    res = max(res, gcd(a[i], a[j]));
  }
  return res;
}
void go() {
  int ans = 0;
  scanf("%d", &n);
  FOR(i, 1, n) scanf("%d", &a[i]);
  FOR(i, 1, n) FOR(j, i, n) ans += g(i, j);
  printf("%d\n", ans);
}
int main() {
  int t;
  scanf("%d", &t);
  FOR(i, 1, t) go();
  return 0;
}
