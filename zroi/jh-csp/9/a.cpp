#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
#define int lld
const int N = 50, P = 1e9 + 7;

int n, ans;
int w[N], h[N], W;

int t[N];
int h1[N];
int calc(int lim) {
  // printf("t:");FOR(i,1,n-1)printf("%lld%c",t[i]," \n"[i==n-1]);
  // printf("h1:");FOR(i,1,n-1)printf("%lld%c",h1[i]," \n"[i==n-1]);
  t[0] = 0;
  FOR(i, 1, n - 1) {
    int x = t[i - 1];
    while (x < n - 1 && w[x + 1] * h1[i] < lim) ++x;
    t[i] = x;
  }
  // FOR(i,1,n-1)printf("%lld%c",t[i]," \n"[i==n-1]);
  int res = 1;
  FOR(i, 1, n - 1) {
    if (t[i] < i) return 0;
    res = 1ll * res * (t[i] - i + 1) % P;
  }
  // printf("res=%lld\n",res);
  return res;
}
void go() {
  scanf("%lld", &n);
  FOR(i, 1, n) scanf("%lld", &w[i]);
  FOR(i, 1, n) scanf("%lld", &h[i]);
  W = w[1];
  FOR(i, 1, n - 1) w[i] = w[i + 1];
  sort(w + 1, w + n);
  sort(h + 1, h + n + 1);
  reverse(h + 1, h + n + 1);
  ans = 0;
  FOR(i, 1, n) {
    int lim = W * h[i];
    // printf("lim=%lld\n",lim);
    FOR(j, 1, i - 1) h1[j] = h[j];
    FOR(j, i, n - 1) h1[j] = h[j + 1];
    // printf("t:");FOR(i,1,n-1)printf("%lld%c",t[i]," \n"[i==n-1]);
    ans = (ans + calc(lim)) % P;
  }
  printf("%lld\n", ans);
}
signed main() {
  int r;
  scanf("%lld", &r);
  FOR(i, 1, r) go();
  return 0;
}
