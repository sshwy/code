#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
#define LL long long
const int N = 51, MTX = N;
const LL INF = 0x3f3f3f3f3f3f3f3f;

int n, s, t;
LL w[N][N];

struct Matrix {
  int w, h;
  LL c[MTX][MTX];
  Matrix(int _h = 0, int _w = 0) {
    w = _w, h = _h;
    FOR(i, 1, h) FOR(j, 1, w) c[i][j] = -INF;
  }
  Matrix(int _h, int _w, LL _c[MTX][MTX]) {
    w = _w, h = _h;
    FOR(i, 1, h) FOR(j, 1, w) c[i][j] = _c[i][j];
  }
  Matrix(int _w) {
    w = h = _w;
    FOR(i, 1, h) FOR(j, 1, w) c[i][j] = -INF;
    FOR(i, 1, h) c[i][i] = 0;
  }
  Matrix operator*(const Matrix &m) {
    assert(w == m.h);
    Matrix res(h, m.w);
    FOR(i, 1, h) FOR(k, 1, w) FOR(j, 1, m.w) {
      res.c[i][j] = max(res.c[i][j], c[i][k] + m.c[k][j]);
    }
    return res;
  }
  Matrix &operator*=(const Matrix &m) {
    *this = *this * m;
    return *this;
  }
};
Matrix pw(Matrix a, int m) {
  Matrix res = a;
  m--;
  while (m) m & 1 ? res *= a, 0 : 0, a *= a, m >>= 1;
  return res;
}
void go() {
  LL ans = -INF;
  scanf("%d%d%d", &n, &s, &t);
  FOR(i, 1, n)
  FOR(j, 1, n) scanf("%lld", &w[i][j]), w[i][j] == 1000 ? w[i][j] = -INF : 0;
  Matrix G(n, n, w);
  G = pw(G, s);
  FOR(i, 1, n) G.c[i][i] = max(G.c[i][i], 0ll);
  G = pw(G, t);
  ans = G.c[1][2];
  if (ans < -1e17)
    puts("IMPOSSIBLE");
  else
    printf("%lld\n", ans);
}
int main() {
  int r;
  scanf("%d", &r);
  FOR(i, 1, r) go();
  return 0;
}
