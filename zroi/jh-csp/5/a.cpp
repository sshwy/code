#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
namespace IO {
  char nc() {
    static char bf[100000], *p1 = bf, *p2 = bf;
    return p1 == p2 && (p2 = (p1 = bf) + fread(bf, 1, 100000, stdin), p1 == p2)
               ? EOF
               : *p1++;
  }
  int rd() {
    int res = 0;
    char c = getchar();
    while (!isdigit(c)) c = getchar();
    while (isdigit(c)) res = res * 10 + c - '0', c = getchar();
    return res;
  }
} // namespace IO
/******************heading******************/
const int N = 1e6 + 6;

int x, n;

bool bp[N * 2];
int pm[N], lp = 0;
int tr[N * 2];
void sieve(int k) {
  bp[0] = bp[1] = 1;
  FOR(i, 2, k) {
    if (!bp[i]) pm[++lp] = i, tr[i] = i;
    FOR(j, 1, lp) {
      const int p = pm[j];
      if (i * p > k) break;
      bp[i * p] = 1;
      tr[i * p] = p;
      if (i % p == 0) break;
    }
  }
}

struct qxx {
  int nex, t, v;
};
qxx e[N * 72];
int h[N * 2], le;
void add_path(int f, int t, int v) {
  // printf("add_path(%d,%d,%d)\n",f,t,v);
  e[++le] = (qxx){h[f], t, v}, h[f] = le;
}

int d[N * 2];
int a[N], c[N], la;

void dfs_make(int k, int s, int x) {
  // printf("dfs_make(%d,%d,%d)\n",k,s,x);
  if (k > la) {
    if (s != x) { add_path(s, x, 4 + (x / s - 1) * 2); }
    return;
  }
  int tot = 1;
  FOR(i, 0, c[k]) {
    dfs_make(k + 1, s * tot, x);
    tot *= a[k];
  }
}
void make(int x) {
  // printf("make(%d)\n",x);
  int x2 = x;
  la = 0;
  while (x2 > 1) {
    a[++la] = tr[x2], c[la] = 0;
    while (tr[x2] == a[la]) c[la]++, x2 /= tr[x2];
  }
  // FOR(i,1,la)printf("(%d,times=%d)\n",a[i],c[i]);
  dfs_make(1, 1, x);
}
// priority_queue<pii,vector<pii>,greater<pii> > q;
queue<pii> q;
bool vising[N * 2];
void dijkstra(int end) {
  q.push(mk(d[x], x));
  while (!q.empty()) {
    pii u = q.front();
    q.pop();
    vising[u.se] = 0;
    if (d[u.se] >= d[end]) continue;
    // if(u.fi>d[u.se])continue;
    // printf("u=%d,%d\n",u.fi,u.se);
    for (int i = h[u.se]; i; i = e[i].nex) {
      const int v = e[i].t, w = e[i].v;
      if (d[v] <= d[u.se] + w) continue;
      d[v] = d[u.se] + w;
      if (d[v] > 100) continue;
      if (!vising[v]) q.push(mk(d[v], v)), vising[v] = 1;
    }
  }
}
int main() {
  scanf("%d%d", &x, &n);
  int nn = max(n, x);
  sieve(nn + 100);
  memset(d, 0x3f, sizeof(d));
  d[x] = 0;
  FOR(i, 1, nn + 100) make(i);
  FOR(i, 1, nn + 100) add_path(i + 1, i, 1);
  FOR(i, 1, nn + 100) add_path(i - 1, i, 1), add_path(i, 0, 3);
  dijkstra(n);
  printf("%d\n", d[n]);
  return 0;
}
