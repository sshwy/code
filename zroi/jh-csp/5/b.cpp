#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
namespace IO {
  char nc() {
    static char bf[100000], *p1 = bf, *p2 = bf;
    return p1 == p2 && (p2 = (p1 = bf) + fread(bf, 1, 100000, stdin), p1 == p2)
               ? EOF
               : *p1++;
  }
  int rd() {
    int res = 0;
    char c = getchar();
    while (!isdigit(c)) c = getchar();
    while (isdigit(c)) res = res * 10 + c - '0', c = getchar();
    return res;
  }
} // namespace IO
/******************heading******************/
const int M = 1e6 + 5, N = 2e5 + 5;

int n, m, q;

struct qxx {
  int nex, t;
};
qxx e[M * 2];
int h[N], le = 1;
void add_path(int f, int t) { e[++le] = (qxx){h[f], t}, h[f] = le; }

int scc[N], totscc;

int dfn[N], low[N], totdfn;
int vis[N], s[N], tp;
void tarjan(int u) {
  dfn[u] = low[u] = ++totdfn;
  s[++tp] = u, vis[u] = 1;
  for (int i = h[u]; i; i = e[i].nex) {
    const int v = e[i].t;
    if (!dfn[v])
      tarjan(v), low[u] = min(low[u], low[v]);
    else if (vis[v])
      low[u] = min(low[u], dfn[v]);
  }
  if (dfn[u] == low[u]) {
    ++totscc;
    while (s[tp] != u) scc[s[tp]] = totscc, vis[s[tp]] = 0, --tp;
    scc[s[tp]] = totscc, vis[s[tp]] = 0, --tp;
  }
}

namespace G {
  struct qxx {
    int nex, t;
  };
  qxx G_e[M * 2];
  int G_h[N], le = 1;
  void G_add_path(int f, int t) {
    // printf("G::G_add_path(%d,%d)\n",f,t);
    G_e[++le] = (qxx){G_h[f], t}, G_h[f] = le;
  }
  vector<int> a[N], b[N];
  int ans[N];
  void init() {
    FOR(u, 1, n) {
      for (int i = h[u]; i; i = e[i].nex) {
        const int v = e[i].t;
        if (scc[u] == scc[v]) continue;
        G_add_path(scc[u], scc[v]);
      }
    }
  }
  void merge_to(vector<int> &x, vector<int> &y) {
    vector<int> res;
    for (unsigned i = 0, j = 0; i < x.size() || j < y.size();) {
      if (i >= x.size())
        res.pb(y[j++]);
      else if (j >= y.size())
        res.pb(x[i++]);
      else if (x[i] < y[j])
        res.pb(x[i++]);
      else
        res.pb(y[j++]);
    }
    y = res;
  }
  void check(int u) {
    // printf("\033[31mcheck(%d)\033[0m\n",u);
    // printf("a:");for(unsigned i=0;i<a[u].size();i++)
    //    printf("%d%c",a[u][i],' ');
    // puts("");
    // printf("b:");for(unsigned i=0;i<b[u].size();i++)
    //    printf("%d%c",b[u][i],' ');
    // puts("");
    vector<int> ra, rb;
    for (unsigned i = 0, j = 0; i < a[u].size() || j < b[u].size();) {
      if (i >= a[u].size())
        rb.pb(b[u][j++]);
      else if (j >= b[u].size())
        ra.pb(a[u][i++]);
      else if (a[u][i] < b[u][j])
        ra.pb(a[u][i++]);
      else if (a[u][i] == b[u][j]) {
        ans[a[u][i]] = 1;
        ++i, ++j;
      } else
        rb.pb(b[u][j++]);
    }
    a[u] = ra, b[u] = rb;
    // printf("a:");for(unsigned i=0;i<a[u].size();i++)
    //    printf("%d%c",a[u][i],' ');
    // puts("");
    // printf("b:");for(unsigned i=0;i<b[u].size();i++)
    //    printf("%d%c",b[u][i],' ');
    // puts("");
  }
  void go() {
    FOR(i, 1, q) {
      int u, v;
      scanf("%d%d", &u, &v);
      if (scc[u] == scc[v])
        ans[i] = 1;
      else {
        a[scc[u]].pb(i);
        b[scc[v]].pb(i);
      }
    }
    // printf("totscc=%d\n",totscc);
    ROF(u, totscc, 1) {
      check(u);
      for (int i = G_h[u]; i; i = G_e[i].nex) {
        const int v = G_e[i].t;
        // printf("(%d,%d)\n",u,v);
        merge_to(a[u], a[v]);
        // merge_to(b[u],a[v]);
      }
    }
    FOR(i, 1, q) puts(ans[i] ? "Yes" : "No");
  }
} // namespace G

int main() {
  scanf("%d%d%d", &n, &m, &q);
  FOR(i, 1, m) {
    int u, v;
    scanf("%d%d", &u, &v);
    add_path(u, v);
  }
  FOR(i, 1, n) if (!dfn[i]) tarjan(i);
  G::init();
  G::go();
  return 0;
}
