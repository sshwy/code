#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
namespace IO {
  char nc() {
    static char bf[100000], *p1 = bf, *p2 = bf;
    return p1 == p2 && (p2 = (p1 = bf) + fread(bf, 1, 100000, stdin), p1 == p2)
               ? EOF
               : *p1++;
  }
  int rd() {
    int res = 0;
    char c = getchar();
    while (!isdigit(c)) c = getchar();
    while (isdigit(c)) res = res * 10 + c - '0', c = getchar();
    return res;
  }
} // namespace IO
/******************heading******************/

char sa[1000], sb[1000];
int la, lb;
int acnt[20], bcnt[20];

bool dfs(int k) { //在当前状态下从第k位开始填，能否填出更小的数
  if (k > la) return 1;
  FOR(i, 0, sb[k] - 1) if (acnt[i]) return 1;
  if (acnt[sb[k]] == 0) return 0;
  acnt[sb[k]]--;
  bool res = dfs(k + 1);
  acnt[sb[k]]++;
  return res;
}

void end() {
  ROF(i, 9, 0) { FOR(j, 1, acnt[i]) putchar('0' + i); }
  puts("");
}
void go() {
  memset(acnt, 0, sizeof(acnt));
  memset(bcnt, 0, sizeof(bcnt));
  scanf("%s", sa + 1);
  la = strlen(sa + 1);
  scanf("%s", sb + 1);
  lb = strlen(sb + 1);
  FOR(i, 1, la) sa[i] -= '0';
  FOR(i, 1, lb) sb[i] -= '0';
  FOR(i, 1, la) acnt[sa[i]]++;
  if (lb > la) {
    ROF(i, 9, 0) FOR(j, 1, acnt[i]) putchar('0' + i);
    return puts(""), void();
  }
  FOR(i, 1, la) {
    if (acnt[sb[i]]) {
      acnt[sb[i]]--;
      if (dfs(i + 1))
        putchar('0' + sb[i]);
      else {
        acnt[sb[i]]++;
        ROF(j, sb[i] - 1, 0)
        if (acnt[j]) return putchar('0' + j), acnt[j]--, end();
      }
    } else {
      ROF(j, sb[i] - 1, 0)
      if (acnt[j]) return putchar('0' + j), acnt[j]--, end();
    }
  }
  puts("");
}

int main() {
  int t;
  scanf("%d", &t);
  while (t--) go();
  return 0;
}
