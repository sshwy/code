#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
namespace IO {
  char nc() {
    static char bf[100000], *p1 = bf, *p2 = bf;
    return p1 == p2 && (p2 = (p1 = bf) + fread(bf, 1, 100000, stdin), p1 == p2)
               ? EOF
               : *p1++;
  }
  int rd() {
    int res = 0;
    char c = getchar();
    while (!isdigit(c)) c = getchar();
    while (isdigit(c)) res = res * 10 + c - '0', c = getchar();
    return res;
  }
} // namespace IO
/******************heading******************/
const int N = 3e6;
int n;

int p[N], c[N], ln;
int tot; //记录有几个0次的
int ans[N], la;

void dfs(int sum, int num) {
  if (tot == ln) {
    if (la + 1 == N) {
      sort(ans + 1, ans + la + 1);
      la = unique(ans + 1, ans + la + 1) - ans - 1;
    }
    return ans[++la] = sum + num - 1, void();
  }
  FOR(i, 1, ln) {
    if (!c[i]) continue;
    c[i]--;
    if (!c[i]) ++tot;
    dfs(sum, num * p[i]);
    if (!c[i]) --tot;
    c[i]++;
  }
  if (num > 1) dfs(sum + num - 1, 1);
}

int main() {
  scanf("%d", &n);
  for (int i = 2; i * i <= n; i++) {
    if (n % i) continue;
    p[++ln] = i;
    while (n % i == 0) n /= i, c[ln]++;
  }
  if (n > 1) p[++ln] = n, c[ln]++;
  dfs(0, 1);
  sort(ans + 1, ans + la + 1);
  la = unique(ans + 1, ans + la + 1) - ans - 1;
  printf("%d\n", la);
  FOR(i, 1, la) printf("%d%c", ans[i], " \n"[i == la]);
  return 0;
}
