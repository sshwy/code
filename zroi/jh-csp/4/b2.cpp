#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
namespace IO {
  char nc() {
    static char bf[100000], *p1 = bf, *p2 = bf;
    return p1 == p2 && (p2 = (p1 = bf) + fread(bf, 1, 100000, stdin), p1 == p2)
               ? EOF
               : *p1++;
  }
  int rd() {
    int res = 0;
    char c = getchar();
    while (!isdigit(c)) c = getchar();
    while (isdigit(c)) res = res * 10 + c - '0', c = getchar();
    return res;
  }
} // namespace IO
/******************heading******************/
const int N = 1000000;

map<int, int> f;
int F(int n) {
  if (n < 3) return n;
  if (f.count(n)) return f[n];
  int res = 0, n2 = n;
  int p[30], lp = 0;
  for (int i = 2; i * i <= n2; i++) {
    if (n2 % i) continue;
    while (n2 % i == 0) p[++lp] = i, n2 /= i;
  }
  if (n2 > 1) p[++lp] = n2;
  // printf("n=%d, ",n);
  // FOR(i,1,lp)printf("%d%c",p[i]," \n"[i==lp]);
  FOR(i, 1, lp) res += F(p[i] - 1);
  return f[n] = res + 1;
}

int main() {
  int a;
  while (scanf("%d", &a)) printf("f[%d]=%d\n", a, F(a));
  return 0;
}
