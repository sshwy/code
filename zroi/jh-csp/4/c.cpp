#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
namespace IO {
  char nc() {
    static char bf[100000], *p1 = bf, *p2 = bf;
    return p1 == p2 && (p2 = (p1 = bf) + fread(bf, 1, 100000, stdin), p1 == p2)
               ? EOF
               : *p1++;
  }
  int rd() {
    int res = 0;
    char c = getchar();
    while (!isdigit(c)) c = getchar();
    while (isdigit(c)) res = res * 10 + c - '0', c = getchar();
    return res;
  }
} // namespace IO
/******************heading******************/
const int N = 2e5 + 5;

char s[N], t[N];
int a[N], n;

int query(int len, int c1) {
  int res = 0;
  FOR(i, len, n) { res += a[i] - a[i - len] == c1; }
  return res;
}
int main() {
  scanf("%s", s + 1);
  n = strlen(s + 1);
  FOR(i, 1, n) a[i] = s[i] == '1';
  FOR(i, 1, n) a[i] += a[i - 1];
  int q;
  scanf("%d", &q);
  FOR(i, 1, q) {
    scanf("%s", t + 1);
    int l = strlen(t + 1), c = 0;
    FOR(i, 1, l) c += t[i] == '1';
    printf("%d\n", query(l, c));
  }
  return 0;
}
