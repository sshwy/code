#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
namespace IO {
  char nc() {
    static char bf[100000], *p1 = bf, *p2 = bf;
    return p1 == p2 && (p2 = (p1 = bf) + fread(bf, 1, 100000, stdin), p1 == p2)
               ? EOF
               : *p1++;
  }
  int rd() {
    int res = 0;
    char c = getchar();
    while (!isdigit(c)) c = getchar();
    while (isdigit(c)) res = res * 10 + c - '0', c = getchar();
    return res;
  }
} // namespace IO
/******************heading******************/
const int N = 1000;

int n;

pii ans[N];
int la, tot;

int make(int n) { // return root
  if (n <= 3) {
    ++tot;
    FOR(i, 2, n)++ tot, ans[++la] = mk(tot, tot - 1);
    return tot;
  }
  int n2 = n;
  int p[40], lp = 0;
  for (int i = 2; i * i <= n2; i++) {
    if (n2 % i) continue;
    while (n2 % i == 0) p[++lp] = i, n2 /= i;
  }
  if (n2 > 1) p[++lp] = n2;
  // printf("n=%d, ",n);
  // FOR(i,1,lp)printf("%d%c",p[i]," \n"[i==lp]);
  int u = ++tot;
  FOR(i, 1, lp) {
    int v = make(p[i] - 1);
    ans[++la] = mk(u, v);
  }
  return u;
}
void go() {
  la = tot = 0;
  make(n);
  printf("%d\n", la + 1);
  FOR(i, 1, la) printf("%d %d\n", ans[i].fi, ans[i].se);
}

int main() {
  while (~scanf("%d", &n)) go();
  return 0;
}
