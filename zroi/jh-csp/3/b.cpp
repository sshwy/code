#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
namespace IO {
  char nc() {
    static char bf[100000], *p1 = bf, *p2 = bf;
    return p1 == p2 && (p2 = (p1 = bf) + fread(bf, 1, 100000, stdin), p1 == p2)
               ? EOF
               : *p1++;
  }
  int rd() {
    int res = 0;
    char c = getchar();
    while (!isdigit(c)) c = getchar();
    while (isdigit(c)) res = res * 10 + c - '0', c = getchar();
    return res;
  }
} // namespace IO
/******************heading******************/
const int N = 1020;

int n, w, d, la, C, ans;

pii a[N * 2];
bool cmp(pii a, pii b) { return a.se < b.se; }

void add(int l, int r) {
  l %= C, r %= C;
  if (l < 0) l += C;
  if (r < 0) r += C;
  if (l > r) l -= C;
  a[++la] = mk(l, r);
  a[++la] = mk(l + C, r + C);
  // printf("add(%d,%d)\n",l,r);
  // printf("add(%d,%d)\n",l+C,r+C);
}
void calc(pii *b) {
  // printf("calc(%d)\n",b-a);
  int res = 0, last = -0x3f3f3f3f;
  FOR(i, 1, n) {
    // printf("b[i]=%d,%d\n",b[i].fi,b[i].se);
    if (b[i].fi > last) ++res, last = b[i].se;
  }
  // cout<<res<<endl;
  ans = min(ans, res);
}
int main() {
  scanf("%d%d%d", &n, &w, &d);
  C = 2 * w + 2 * d, ans = n;
  FOR(i, 1, n) {
    int x, y;
    char c[5];
    scanf("%d%d%s", &x, &y, c);
    if (c[0] == 'W')
      add(d - x - y, d + x - y);
    else if (c[0] == 'E')
      add(d + x + y, y - x - d);
    else if (c[0] == 'S')
      add(d + x - y, d + x + y);
    else
      add(y - x - d, d - x - y);
  }
  sort(a + 1, a + la + 1, cmp);
  assert(la == n * 2);
  // FOR(i,1,la)printf("(%d,%d)%c",a[i].fi,a[i].se," \n"[i%n==0]);
  FOR(i, 1, n) calc(a + (i - 1));
  printf("%d", ans);
  return 0;
}
/*
 * C=(w+d)*2
 * W: (x,y) : [ d-x-y , d+x-y ]
 * E: (x,y) : [ d+x+y , y-x-d ]
 * S: (x,y) : [ d+x-y , d+x+y ]
 * N: (x,y) : [ y-x-d , d-x-y ]
 * 在模C意义下
 */
