#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
namespace IO {
  char nc() {
    static char bf[100000], *p1 = bf, *p2 = bf;
    return p1 == p2 && (p2 = (p1 = bf) + fread(bf, 1, 100000, stdin), p1 == p2)
               ? EOF
               : *p1++;
  }
  int rd() {
    int res = 0;
    char c = getchar();
    while (!isdigit(c)) c = getchar();
    while (isdigit(c)) res = res * 10 + c - '0', c = getchar();
    return res;
  }
} // namespace IO
/******************heading******************/
const int N = 1005;

int n, r[N];
double d[N], ans;

int sqr(int x) { return x * x; }
double dist(int r1, int r2) { return sqrt(sqr(r1 + r2) - sqr(r1 - r2)); }

void check(int s) {
  int t[N], lt = 0;
  FOR(i, 1, n) if ((s >> (i - 1)) & 1) t[++lt] = i;
  if (!lt) return;
  double res = r[t[1]];
  FOR(i, 1, lt - 1) res += dist(r[t[i]], r[t[i + 1]]);
  res += r[t[lt]];
  ans = max(ans, res);
}
int main() {
  scanf("%d", &n);
  FOR(i, 1, n) scanf("%d", &r[i]);
  d[1] = r[1];
  int lim = 1 << n;
  FOR(i, 0, lim - 1) check(i);
  printf("%.3lf", ans);
  return 0;
}
