#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
namespace IO {
  char nc() {
    static char bf[100000], *p1 = bf, *p2 = bf;
    return p1 == p2 && (p2 = (p1 = bf) + fread(bf, 1, 100000, stdin), p1 == p2)
               ? EOF
               : *p1++;
  }
  int rd() {
    int res = 0;
    char c = getchar();
    while (!isdigit(c)) c = getchar();
    while (isdigit(c)) res = res * 10 + c - '0', c = getchar();
    return res;
  }
} // namespace IO
/******************heading******************/
const int N = 2e5 + 500;

unsigned int X;
void srand1(unsigned int S) { X = S; }
unsigned int nextInteger1(void) {
  X = X * 1103515245 + 12345;
  return (X / 65536) % 32768;
}
void generator1(int N, unsigned int S, int A[]) {
  srand1(S);
  for (int i = 1; i <= N; i++) { A[i] = nextInteger1() % 2; }
}

unsigned int x, y, z, w;
void srand2(unsigned int S) {
  x = S;
  y = x * S;
  z = y * S;
  w = z * S;
}
unsigned int nextInteger2(void) {
  unsigned t = x ^ (x << 11);
  x = y;
  y = z;
  z = w;
  return w = (w ^ (w >> 19)) ^ (t ^ (t >> 8));
}
void generator2(int N, unsigned int S, int A[]) {
  srand2(S);
  for (int i = 1; i <= N; i++) { A[i] = nextInteger2() % 2; }
}

int bin[N], ls;
char s[N];
bool check1(unsigned int S) {
  int l = min(ls, 40);
  generator1(l, S, bin);
  FOR(i, 1, l) if (s[i] - '0' != bin[i]) return 0;
  // FOR(i,1,ls)printf("%d%c",bin[i],"\0\n"[i==ls]);
  return 1;
}
bool check2(unsigned int S) {
  generator2(ls, S, bin);
  FOR(i, 1, ls) if (s[i] - '0' != bin[i]) return 0;
  return 1;
}

void go() {
  scanf("%s", s + 1);
  ls = strlen(s + 1);
  int lim = 1 << 17;
  FOR(i, 0, lim - 1) {
    if (check1(i)) return puts("LCG"), void();
    // if(check2(i))return puts("Xorshift"),void();
  }
  return puts("Xorshift"), void();
}
int xxx[N], ln = 10;
int main() {
  int t;
  scanf("%d", &t);
  while (t--) go();
  return 0;
}
