#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
#define int lld
const int N = 7005;

int n, m;
int v[N], s[N];

pair<long long, int> a[N];

void make(long long t) { FOR(i, 1, n) a[i] = mk(-s[i] - v[i] * t, i); }
long long query(int k) {
  nth_element(a + 1, a + k, a + n + 1);
  return (a + k)->se;
}
signed main() {
  scanf("%lld", &n);
  FOR(i, 1, n) scanf("%lld%lld", &v[i], &s[i]);
  scanf("%lld", &m);
  FOR(i, 1, m) {
    int t, k;
    scanf("%lld%lld", &t, &k);
    make(t);
    printf("%lld\n", query(k));
  }
  return 0;
}
