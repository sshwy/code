#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int SZ = 4503 * 4503 * 4;

int n, m, P, lastans;
int ch[SZ][2][2], val[SZ], tg[SZ], tot = 1;

void nodeadd(int u, int rl, int rr, int cl, int cr, int v) {
  val[u] = (val[u] + (rr - rl + 1ll) * (cr - cl + 1ll) * v) % P,
  tg[u] = (tg[u] + v) % P;
}
void pushdown(int u, int rl, int rr, int cl, int cr) {
  if (!ch[u][0][0]) ch[u][0][0] = ++tot;
  if (!ch[u][0][1]) ch[u][0][1] = ++tot;
  if (!ch[u][1][0]) ch[u][1][0] = ++tot;
  if (!ch[u][1][1]) ch[u][1][1] = ++tot;
  if (!tg[u]) return;
  int rmid = (rl + rr) >> 1, cmid = (cl + cr) >> 1;
  nodeadd(ch[u][0][0], rl, rmid, cl, cmid, tg[u]);
  nodeadd(ch[u][0][1], rl, rmid, cmid + 1, cr, tg[u]);
  nodeadd(ch[u][1][0], rmid + 1, rr, cl, cmid, tg[u]);
  nodeadd(ch[u][1][1], rmid + 1, rr, cmid + 1, cr, tg[u]);
  tg[u] = 0;
}
void pushup(int u) {
  val[u] = val[ch[u][0][0]] + val[ch[u][0][1]] + val[ch[u][1][0]] + val[ch[u][1][1]];
  val[u] %= P;
}
void add(int rL, int rR, int cL, int cR, int v, int u = 1, int rl = 0, int rr = n - 1,
    int cl = 0, int cr = n - 1) {
  if (rL <= rl && rr <= rR && cL <= cl && cr <= cR) {
    // printf("add(%d,%d,%d,%d,\n\t%d,%d,%d,%d)\n",rL,rR,cL,cR,rl,rr,cl,cr);
    return nodeadd(u, rl, rr, cl, cr, v), void();
  }
  int rmid = (rl + rr) >> 1, cmid = (cl + cr) >> 1;
  pushdown(u, rl, rr, cl, cr);
  if (rL <= rmid && cL <= cmid)
    add(rL, rR, cL, cR, v, ch[u][0][0], rl, rmid, cl, cmid);
  if (rL <= rmid && cmid < cR)
    add(rL, rR, cL, cR, v, ch[u][0][1], rl, rmid, cmid + 1, cr);
  if (rmid < rR && cL <= cmid)
    add(rL, rR, cL, cR, v, ch[u][1][0], rmid + 1, rr, cl, cmid);
  if (rmid < rR && cmid < cR)
    add(rL, rR, cL, cR, v, ch[u][1][1], rmid + 1, rr, cmid + 1, cr);
  pushup(u);
}
int query(int rL, int rR, int cL, int cR, int u = 1, int rl = 0, int rr = n - 1,
    int cl = 0, int cr = n - 1) {
  if (rL <= rl && rr <= rR && cL <= cl && cr <= cR) return val[u];
  int rmid = (rl + rr) >> 1, cmid = (cl + cr) >> 1;
  pushdown(u, rl, rr, cl, cr);
  int res = 0;
  if (rL <= rmid && cL <= cmid)
    res += query(rL, rR, cL, cR, ch[u][0][0], rl, rmid, cl, cmid);
  if (rL <= rmid && cmid < cR)
    res += query(rL, rR, cL, cR, ch[u][0][1], rl, rmid, cmid + 1, cr);
  if (rmid < rR && cL <= cmid)
    res += query(rL, rR, cL, cR, ch[u][1][0], rmid + 1, rr, cl, cmid);
  if (rmid < rR && cmid < cR)
    res += query(rL, rR, cL, cR, ch[u][1][1], rmid + 1, rr, cmid + 1, cr);
  return res % P;
}
int main() {
  scanf("%d%d%d", &n, &m, &P);
  FOR(i, 1, m) {
    int op, x_1, y_1, x_2, y_2;
    scanf("%d%d%d%d%d", &op, &x_1, &x_2, &y_1, &y_2);
    x_1 = (x_1 + lastans) % n;
    y_1 = (y_1 + lastans) % n;
    x_2 = (x_2 + lastans) % n;
    y_2 = (y_2 + lastans) % n;
    if (x_2 < x_1) swap(x_1, x_2);
    if (y_2 < y_1) swap(y_2, y_1);
    // printf("%d %d %d %d %d\n",op,x_1,x_2,y_1,y_2);
    if (op == 1) {
      add(x_1, x_2, y_1, y_2, 1);
    } else {
      int ans = query(x_1, x_2, y_1, y_2);
      printf("%d\n", ans);
      lastans += ans;
    }
  }
  return 0;
}
