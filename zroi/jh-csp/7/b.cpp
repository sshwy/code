#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
#define int lld
const int M = 23, N = 102, INF = 0x3f3f3f3f3f3f3f3f;

int n, m, k, e;
int w[M][M];
int sta[N], a[N][N];
int d[N];
int f[N];
bool vis[N];

int calc(int l, int r) { // satisfy l..r the sp from 1 to n
  int s = 0;
  FOR(i, l, r) s |= sta[i];
  // printf("calc(%lld,%lld),s=%lld\n",l,r,s);
  memset(d, 0x3f, sizeof d);
  memset(vis, 0, sizeof vis);
  d[1] = 0;
  FOR(i, 1, m) {
    int mn = INF, p = 0;
    FOR(j, 1, m)
    if (!(s >> j - 1 & 1) && !vis[j] && d[j] < mn) mn = d[j], p = j;
    vis[p] = 1;
    // printf("p=%d\n",p);
    FOR(j, 1, m) if (!(s >> j - 1 & 1)) d[j] = min(d[j], d[p] + w[p][j]);
    // printf("d[%lld]=%lld\n",4,d[4]);
  }
  // printf("calc(%lld,%lld)=%lld\n",l,r,d[m]);
  return d[m]; // BUG#1:n写成m（样例是m=n
}
signed main() {
  scanf("%lld%lld%lld%lld", &n, &m, &k, &e);
  memset(w, 0x3f, sizeof(w));
  FOR(i, 1, e) {
    int x, y, z;
    scanf("%lld%lld%lld", &x, &y, &z);
    w[x][y] = w[y][x] = z;
  }
  FOR(j, 1, n) FOR(i, 1, m) w[i][i] = 0;
  int d;
  scanf("%lld", &d);
  FOR(i, 1, d) {
    int p, a, b;
    scanf("%lld%lld%lld", &p, &a, &b);
    FOR(i, a, b) sta[i] |= 1 << p - 1;
  }
  FOR(i, 1, n) FOR(j, i, n) a[i][j] = calc(i, j);
  FOR(i, 1, n) {
    f[i] = a[1][i] == INF ? INF : a[1][i] * i;
    FOR(j, 1, i - 1) {
      if (a[j + 1][i] < INF) f[i] = min(f[i], f[j] + a[j + 1][i] * (i - j) + k);
    }
    // printf("f[%lld]=%lld\n",i,f[i]);
  }
  printf("%lld\n", f[n]);
  return 0;
}
