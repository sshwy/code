#include <bits/stdc++.h>
#define FOR(a, b, c) for (int a = (b); a <= (c); a++)
using namespace std;
const int N = 200;

int n;
int a[200];
bool vis[30000];

void go() {
  scanf("%d", &n);
  FOR(i, 1, n) scanf("%d", &a[i]);
  sort(a + 1, a + n + 1);
  FOR(i, 0, a[n]) vis[i] = 0;
  vis[0] = 1;
  int ans = 0;
  FOR(i, 1, n) {
    if (vis[a[i]]) continue;
    FOR(j, a[i], a[n]) {
      if (vis[j - a[i]]) vis[j] = 1;
    }
    ++ans;
  }
  printf("%d\n", ans);
}
int main() {
  int t;
  scanf("%d", &t);
  FOR(i, 1, t) go();
  return 0;
}
