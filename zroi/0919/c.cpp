#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
namespace IO {
  char nc() {
    static char bf[100000], *p1 = bf, *p2 = bf;
    return p1 == p2 && (p2 = (p1 = bf) + fread(bf, 1, 100000, stdin), p1 == p2)
               ? EOF
               : *p1++;
  }
  int rd() {
    int res = 0;
    char c = getchar();
    while (!isdigit(c)) c = getchar();
    while (isdigit(c)) res = res * 10 + c - '0', c = getchar();
    return res;
  }
} // namespace IO
/******************heading******************/
#define int lld

struct vec {
  int a, b;
  vec(int _a = 0, int _b = 0) { a = _a, b = _b; }
  int range() {
    if (a == 0 && b == 0) return 0;
    if (a > 0 && b >= 0) return 1;
    if (a <= 0 && b > 0) return 2;
    if (a < 0 && b <= 0) return 3;
    if (a >= 0 && b < 0) return 4;
    // printf("%d %d ",a,b);
    // puts("Error!");
    return 5;
  }
  vec operator+(vec v) { return vec(a + v.a, b + v.b); }
  vec operator-(vec v) { return vec(a - v.a, b - v.b); }
  int det(vec v) const { return a * v.b - b * v.a; }
  int dot(vec v) const { return a * v.a + b * v.b; }
  bool operator<(vec v) const { return this->det(v) > 0; }
  void read() { scanf("%lld%lld", &a, &b); }
};
int n;
vector<vec> a[5];
int ans;

void calc(const vector<vec> &V) {
  for (unsigned i = 0; i < V.size(); i++) {
    vec v;
    for (unsigned j = i; j < V.size(); j++) {
      v = v + V[j];
      ans = max(ans, v.dot(v));
      // printf("ans=%lld\n",ans);
    }
  }
}

signed main() {
  scanf("%lld", &n);
  FOR(i, 1, n) {
    vec v;
    v.read();
    a[v.range()].pb(v);
  }
  FOR(i, 1, 4) sort(a[i].begin(), a[i].end());

  vector<vec> v = a[1];
  v.insert(v.end(), a[2].begin(), a[2].end());
  v.insert(v.end(), a[3].begin(), a[3].end());
  v.insert(v.end(), a[4].begin(), a[4].end());
  calc(v);

  v = a[1];
  v.insert(v.end(), a[3].begin(), a[3].end());
  v.insert(v.end(), a[2].begin(), a[2].end());
  v.insert(v.end(), a[4].begin(), a[4].end());
  calc(v);

  v = a[3];
  v.insert(v.end(), a[2].begin(), a[2].end());
  v.insert(v.end(), a[4].begin(), a[4].end());
  v.insert(v.end(), a[1].begin(), a[1].end());
  calc(v);

  v = a[2];
  v.insert(v.end(), a[4].begin(), a[4].end());
  v.insert(v.end(), a[1].begin(), a[1].end());
  v.insert(v.end(), a[3].begin(), a[3].end());
  calc(v);

  v = a[4];
  v.insert(v.end(), a[1].begin(), a[1].end());
  v.insert(v.end(), a[3].begin(), a[3].end());
  v.insert(v.end(), a[2].begin(), a[2].end());
  calc(v);

  v = a[2];
  v.insert(v.end(), a[3].begin(), a[3].end());
  v.insert(v.end(), a[4].begin(), a[4].end());
  v.insert(v.end(), a[1].begin(), a[1].end());
  calc(v);

  v = a[3];
  v.insert(v.end(), a[4].begin(), a[4].end());
  v.insert(v.end(), a[1].begin(), a[1].end());
  v.insert(v.end(), a[2].begin(), a[2].end());
  calc(v);

  v = a[4];
  v.insert(v.end(), a[1].begin(), a[1].end());
  v.insert(v.end(), a[2].begin(), a[2].end());
  v.insert(v.end(), a[3].begin(), a[3].end());
  calc(v);

  v = a[2];
  v.insert(v.end(), a[1].begin(), a[1].end());
  v.insert(v.end(), a[4].begin(), a[4].end());
  calc(v);

  printf("%.12lf", sqrt(ans));
  return 0;
}
