#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
namespace IO {
  char nc() {
    static char bf[100000], *p1 = bf, *p2 = bf;
    return p1 == p2 && (p2 = (p1 = bf) + fread(bf, 1, 100000, stdin), p1 == p2)
               ? EOF
               : *p1++;
  }
  int rd() {
    int res = 0;
    char c = getchar();
    while (!isdigit(c)) c = getchar();
    while (isdigit(c)) res = res * 10 + c - '0', c = getchar();
    return res;
  }
} // namespace IO
/******************heading******************/
const int N = 2e5 + 50;

int n, h, w;

struct edge {
  int u, v, w;
  bool operator<(edge e) { return w < e.w; }
} e[N];

int f[N], g[N];
void init(int k) { FOR(i, 0, k) f[i] = i, g[i] = 0; }
int get(int u) { return f[u] == u ? u : f[u] = get(f[u]); }
void merge(int u, int v) { f[get(u)] = get(v); }
bool find(int u, int v) { return get(u) == get(v); }
bool has_circle(int u) { return g[get(u)]; }
void set_circle(int u) { g[get(u)] = 1; }

lld ans;

int main() {
  scanf("%d%d%d", &n, &h, &w);
  init(h + w);
  FOR(i, 1, n) {
    int x, y, z;
    scanf("%d%d%d", &x, &y, &z);
    e[i] = (edge){x, y + h, z};
  }
  sort(e + 1, e + n + 1);
  ROF(i, n, 1) {
    int u = e[i].u, v = e[i].v, w = e[i].w;
    if (find(u, v)) {
      if (!has_circle(u)) ans += w, set_circle(u);
    } else {
      if (has_circle(u) && has_circle(v))
        ;
      else if (has_circle(u))
        ans += w, merge(v, u);
      else
        ans += w, merge(u, v);
    }
  }
  printf("%lld", ans);
  return 0;
}
