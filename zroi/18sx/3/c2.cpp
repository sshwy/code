// by Sshwy
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 5e6 + 5, SZ = N * 2;

char s[N];
int n, ls;

int tot = 1, last = 1;
int len[SZ], fail[SZ], tr[SZ][5];
int cnt[SZ], pos[SZ], a[SZ], bin[SZ];

void insert(char c) {
  c -= 'a';
  int u = ++tot, p = last;
  len[u] = len[last] + 1;
  pos[u] = ++ls;
  cnt[u] = 1;
  while (p && tr[p][c] == 0) tr[p][c] = u, p = fail[p];
  if (!p)
    fail[u] = 1;
  else {
    int q = tr[p][c];
    if (len[q] == len[p] + 1)
      fail[u] = q;
    else {
      int cq = ++tot;
      len[cq] = len[p] + 1, fail[cq] = fail[q], fail[q] = fail[u] = cq;
      pos[cq] = pos[q];
      memcpy(tr[cq], tr[q], sizeof(tr[q]));
      while (p && tr[p][c] == q) tr[p][c] = cq, p = fail[p];
    }
  }
  last = u;
}
void qsort() {
  FOR(i, 1, tot) bin[len[i]]++;
  FOR(i, 1, tot) bin[i] += bin[i - 1];
  FOR(i, 1, tot) a[bin[len[i]]--] = i;
}
int ans[N];

void get_min(int &x, int y) { x = min(x, y); }

int main() {
  scanf("%s", s + 1);

  while (s[n + 1]) ++n, insert(s[n]), ans[n] = 0x3f3f3f3f;
  qsort();
  ROF(i, tot, 1) { cnt[fail[a[i]]] += cnt[a[i]]; }

  /*FOR(u,1,tot){
      if(cnt[u]>1)continue;
      printf("u=%2d, fail=%2d, cnt=%2d | ",u,fail[u],cnt[u]);
      FOR(i,1,pos[u]-len[u])printf(" ");
      FOR(i,pos[u]-len[u]+1,pos[u])printf("%c",s[i]);
      puts("");
  }*/
  FOR(u, 1, tot) {
    if (cnt[u] > 1) continue;
    int L = len[fail[u]] + 1, R = len[u];
    FOR(i, 1, L) get_min(ans[pos[u] - i + 1], L);
    FOR(i, L, R) get_min(ans[pos[u] - i + 1], i);
  }
  long long s = 0;
  FOR(i, 1, n) s += ans[i];
  printf("%lld\n", s);

  return 0;
}
