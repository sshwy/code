// by Sshwy
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 5e6 + 5, SZ = N * 2;

char s[N];
int n, ls;

int tot = 1, last = 1;
int len[SZ], fail[SZ], tr[SZ][5];
int cnt[SZ], pos[SZ];

void insert(char c) {
  c -= 'a';
  int u = ++tot, p = last;
  len[u] = len[last] + 1;
  pos[u] = ++ls;
  cnt[u] = 1;
  while (p && tr[p][c] == 0) tr[p][c] = u, p = fail[p];
  if (!p)
    fail[u] = 1;
  else {
    int q = tr[p][c];
    if (len[q] == len[p] + 1)
      fail[u] = q, cnt[q] = 2; //#1
    else {
      int cq = ++tot;
      len[cq] = len[p] + 1, fail[cq] = fail[q], fail[q] = fail[u] = cq;
      cnt[cq] = 2; //#1
      pos[cq] = pos[q];
      memcpy(tr[cq], tr[q], sizeof(tr[q]));
      while (p && tr[p][c] == q) tr[p][c] = cq, p = fail[p];
    }
  }
  last = u;
}
int ans[N];

void get_min(int &x, int y) { x = min(x, y); }

struct atom {
  int x, y, z;
} A[N];
int la;

int q[N], ql, qr;

int main() {
  scanf("%s", s + 1);

  while (s[n + 1]) ++n, insert(s[n]), ans[n] = 0x3f3f3f3f;
  if (n == 1) return puts("1"), 0;

  FOR(u, 1, tot) if (cnt[u] == 1) {
    int R = pos[u] - len[fail[u]], v = len[fail[u]] + 1;
    get_min(ans[R], v);
    A[++la] = {R, pos[u], v};
    if (la > 1) assert(A[la - 1].y <= A[la].y && A[la - 1].x <= A[la].x);
  }
  ROF(i, n, 1) { get_min(ans[i - 1], ans[i] + 1); }
  ql = 1, qr = 0;
  int pos = 0;

  FOR(i, 1, n) {
    while (pos < la && A[pos + 1].x == i) {
      ++pos;
      while (ql <= qr && A[q[qr]].z >= A[pos].z) --qr;
      q[++qr] = pos;
    }
    while (ql <= qr && A[q[ql]].y < i) ++ql;
    if (ql <= qr) get_min(ans[i], A[q[ql]].z);
  }

  long long s = 0;
  FOR(i, 1, n) s += ans[i];

  printf("%lld\n", s);

  return 0;
}
