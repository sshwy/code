// by Sshwy
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/

using namespace RA;

int main() {
  srand(clock());
  int t = 50, fa[10000];
  printf("%d\n", t);
  FOR(i, 1, t) {
    int n = r(1, 1000);
    printf("%d\n", n);
    FOR(i, 2, n) printf("%d%c", fa[i] = r(1, i - 1), " \n"[i == n]);
    FOR(i, 1, n) printf("%d%c", r(1, max(1, n * 5 - 5 * fa[i])), " \n"[i == n]);
  }
  return 0;
}
