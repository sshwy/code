// by Sshwy
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 1005, W = 5005, INF = 0x3f3f3f3f;

int fa[N];
int n, V[N];
int f[N],
    g[N]; // f[i]表示结点i是黑色，白色点的根的权值和的最小值。g[i]对应白色时，黑色根权值和的最小值。

vector<int> T[N];

int F[W], G[W]; //表示黑色根点权和为i时白色根点权和的最小值
int t[W];

void get_min(int &x, int y) { x = min(x, y); }
void dfs(int u) {
  // if(!T[u].size()){ f[u]=0,g[u]=0; return; }
  for (int v : T[u]) dfs(v);
  // printf("\033[32mdfs(%d)\033[0m\n",u);
  memset(F, 0x3f, sizeof(F));
  memset(G, 0x3f, sizeof(G));
  F[0] = G[0] = 0; //初始时没有结点
  for (int v : T[u]) {
    // V[v],f[v] 或 g[v],V[v] 二选一，但不能不选
    memset(t, 0x3f, sizeof(t));
    FOR(i, 0, V[u] - V[v]) { get_min(t[i + V[v]], F[i] + f[v]); }
    FOR(i, 0, V[u] - g[v]) { get_min(t[i + g[v]], F[i] + V[v]); }
    memcpy(F, t, sizeof(t));
    memset(t, 0x3f, sizeof(t));
    FOR(i, 0, V[u] - V[v]) { get_min(t[i + V[v]], G[i] + g[v]); }
    FOR(i, 0, V[u] - f[v]) { get_min(t[i + f[v]], G[i] + V[v]); }
    memcpy(G, t, sizeof(t));
  }
  f[u] = *min_element(F, F + V[u] + 1);
  g[u] = *min_element(G, G + V[u] + 1);
  // printf("u=%2d, f=%2d, g=%2d, V=%2d, fa=%2d\n",u,f[u],g[u],V[u],fa[u]);
}
void go() {
  scanf("%d", &n);
  memset(f, 0x3f, sizeof(f));
  memset(g, 0x3f, sizeof(g));
  FOR(i, 1, n) T[i].clear();
  FOR(i, 2, n) scanf("%d", &fa[i]), T[fa[i]].pb(i);
  FOR(i, 1, n) scanf("%d", &V[i]);
  dfs(1);
  if (f[1] == INF && g[1] == INF)
    puts("IMPOSSIBLE");
  else
    puts("POSSIBLE");
}
int main() {
  int t;
  scanf("%d", &t);
  FOR(i, 1, t) go();
  return 0;
}
