// by Sshwy
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 305, M = 1e5 + 5, P = 1e9 + 7;

int pw(int a, int m) {
  int res = 1;
  while (m) m & 1 ? res = 1ll * res * a % P : 0, a = 1ll * a * a % P, m >>= 1;
  return res;
}
int t[N][N];
int det(const int f[N][N], int n) {
  memcpy(t, f, sizeof(int) * N * N);
  int sig = 1;
  FOR(i, 1, n) {
    FOR(j, i + 1, n) if (t[j][i]) {
      FOR(k, 1, n) swap(t[i][k], t[j][k]);
      sig = -sig;
      break;
    }
    if (!t[i][i]) return 0;
    FOR(j, 1, n) {
      if (j == i) continue;
      int rate = t[j][i] * 1ll * pw(t[i][i], P - 2) % P;
      FOR(k, i, n) t[j][k] = (t[j][k] - 1ll * t[i][k] * rate % P + P) % P;
    }
  }
  int res = 1;
  FOR(i, 1, n) res = 1ll * res * t[i][i] % P;
  res = (res * sig + P) % P;
  return res;
}
int F[N][N * 2];
bool inv(const int f[N][N], int g[N][N], int n) {
  FOR(i, 1, n) FOR(j, 1, n) F[i][j] = f[i][j], F[i][j + n] = i == j;
  FOR(i, 1, n) {
    FOR(j, i, n) if (F[j][i]) {
      FOR(k, 1, n * 2) swap(F[i][k], F[j][k]);
      break;
    }
    if (!F[i][i]) return 0;
    FOR(j, 1, n) {
      if (j == i) continue;
      int rate = F[j][i] * 1ll * pw(F[i][i], P - 2) % P;
      FOR(k, i, n * 2) F[j][k] = (F[j][k] - 1ll * F[i][k] * rate % P + P) % P;
    }
  }
  FOR(i, 1, n) {
    int x = pw(F[i][i], P - 2);
    FOR(j, 1, n * 2) F[i][j] = F[i][j] * 1ll * x % P;
  }
  FOR(i, 1, n) FOR(j, 1, n) g[i][j] = F[i][j + n];
  return 1;
}

int n, m, d[N];
int u[M], v[M], w[M];

int g[N][N], h[N][N], adj[N][N];

int main() {
  scanf("%d%d", &n, &m);
  FOR(i, 1, m) scanf("%d%d%d", &u[i], &v[i], &w[i]);

  //根向：D_out - A
  FOR(i, 1, m) g[u[i]][v[i]]--, g[u[i]][u[i]]++;

  int tot = det(g, n - 1), ans = 0;

  //伴随 = inv(g/deg(g))
  memcpy(h, g, sizeof(g));

  int itot = pw(tot, P - 2);
  FOR(i, 1, n) FOR(j, 1, n) h[i][j] = 1ll * h[i][j] * itot % P;

  if (!inv(h, adj, n - 1)) return puts("0"), 0; //无解

  //伴随 -> 余子：余子 = (伴随)^T
  FOR(i, 1, n - 1) FOR(j, 1, i - 1) swap(adj[i][j], adj[j][i]);

  /*FOR(i,1,n-1){//检查是不是余子式
      int s=0;
      FOR(j,1,n-1)s=(s+g[i][j]*1ll*adj[i][j])%P;
      s=(s+P)%P;
      assert(s==tot);
  }*/

  FOR(i, 1, m) {
    int x = (tot + adj[u[i]][v[i]] - adj[u[i]][u[i]]) % P;
    ans = (ans + (tot - x) * 1ll * w[i]) % P;
  }
  ans = (ans + P) % P;

  printf("%d\n", ans);
  return 0;
}
