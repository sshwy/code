// by Sshwy
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 1005, W = 5005, INF = 0x3f3f3f3f;

int fa[N];
int n, V[N];
int f[N];

vector<int> T[N];

int F[W];
int t[W];

void get_min(int &x, int y) { x = min(x, y); }
void dfs(int u) {
  for (int v : T[u]) dfs(v);
  memset(F, 0x3f, sizeof(F));
  F[0] = 0;
  for (int v : T[u]) {
    // V[v],f[v] 或 f[v],V[v] 二选一，但不能不选
    memset(t, 0x3f, sizeof(t));
    FOR(i, 0, V[u] - V[v]) get_min(t[i + V[v]], F[i] + f[v]);
    FOR(i, 0, V[u] - f[v]) get_min(t[i + f[v]], F[i] + V[v]);
    memcpy(F, t, sizeof(t));
  }
  f[u] = *min_element(F, F + V[u] + 1);
}
void go() {
  scanf("%d", &n);
  memset(f, 0x3f, sizeof(f));
  FOR(i, 1, n) T[i].clear();
  FOR(i, 2, n) scanf("%d", &fa[i]), T[fa[i]].pb(i);
  FOR(i, 1, n) scanf("%d", &V[i]);
  dfs(1);
  if (f[1] == INF)
    puts("IMPOSSIBLE");
  else
    puts("POSSIBLE");
}
int main() {
  int t;
  scanf("%d", &t);
  FOR(i, 1, t) go();
  return 0;
}
