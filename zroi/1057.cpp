#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int SZ = 1e6 * 30 + 5;

int n, k;
long long ans;

int tr[SZ][2], sz[SZ], tot;
int new_node() {
  ++tot, tr[tot][0] = tr[tot][1] = 0, sz[tot] = 0;
  return tot;
}
void insert(int u, int val, int d) {
  if (d == -1) return sz[u]++, void();
  int c = val >> d & 1;
  if (!tr[u][c]) tr[u][c] = new_node();
  insert(tr[u][c], val, d - 1);
  sz[u] = sz[tr[u][0]] + sz[tr[u][1]];
}
void insert(int x) { insert(1, x, 30); }

void dfs(int x, int y, int val, int d) { // x,y异或出来的大于val的低d位的方案数
  if (!sz[x] || !sz[y]) return;
  if (d == -1) {
    if (x != y)
      ans += 1ll * sz[x] * sz[y];
    else
      ans += 1ll * sz[x] * (sz[x] - 1) / 2;
    return;
  }
  int c = val >> d & 1;
  if (c == 0) {
    ans += 1ll * sz[tr[x][0]] * sz[tr[y][1]];
    if (x != y) ans += 1ll * sz[tr[y][0]] * sz[tr[x][1]];
    dfs(tr[x][0], tr[y][0], val, d - 1);
    dfs(tr[x][1], tr[y][1], val, d - 1);
  } else {
    dfs(tr[x][0], tr[y][1], val, d - 1);
    if (x != y) dfs(tr[x][1], tr[y][0], val, d - 1);
  }
}
void go() {
  tot = ans = 0;
  new_node();
  scanf("%d%d", &n, &k);
  int las = 0, x;
  insert(las);
  FOR(i, 1, n) {
    scanf("%d", &x);
    las ^= x;
    insert(las);
  }
  dfs(1, 1, k, 30);
  printf("%lld\n", ans);
}
int main() {
  int t;
  scanf("%d", &t);
  FOR(i, 1, t) go();
  return 0;
}
