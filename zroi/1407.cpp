// by Sshwy
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA
const int N = 2e5 + 5, MT = 500;
int n, m, q, Tn, Tm;
int a[N], b[N];
struct atom {
  int l, r, vl, vr, id;
};
vector<atom> qry[N];
long long ans[N];
long long c[N], bc[MT], tbc[MT];
void add(int pos, int v) {
  c[pos] += v;
  int id = pos / Tm;
  bc[id] = max(bc[id], c[pos]);
}
void fake_add(int pos, int v) {
  c[pos] += v;
  int id = pos / Tm;
  tbc[id] = max(tbc[id], c[pos]);
}
void fake_sub(int pos, int v) { c[pos] -= v; }
void fake_init() { memcpy(tbc, bc, sizeof(bc)); }
void fake_clear() { memset(tbc, 0, sizeof tbc); }
long long Qry(int L, int R) {
  if (L / Tm == R / Tm) {
    long long res = 0;
    FOR(i, L, R) res = max(res, c[i]);
    return res;
  }
  long long res = 0;
  int lim = min(m, L / Tm * Tm + Tm - 1);
  FOR(i, L, lim) res = max(res, c[i]);
  L = lim + 1;
  lim = max(1, R / Tm * Tm);
  FOR(i, lim, R) res = max(res, c[i]);
  R = lim - 1;
  FOR(i, L / Tm, R / Tm) res = max(res, max(tbc[i], bc[i]));
  return res;
}

void calc(int B, vector<atom> &qs) {
  sort(qs.begin(), qs.end(), [](atom x, atom y) { return x.r < y.r; });
  int R = min(n, B * Tn + Tn - 1);
  int las = R;
  for (auto x : qs) {
    if (x.r <= R) {
      fake_init();
      FOR(i, x.l, x.r) fake_add(a[i], b[i]);
      ans[x.id] = Qry(x.vl, x.vr);
      FOR(i, x.l, x.r) fake_sub(a[i], b[i]);
      fake_clear();
    } else {
      assert(las <= x.r);
      while (las < x.r) ++las, add(a[las], b[las]);
      fake_init();
      FOR(i, x.l, R) fake_add(a[i], b[i]);
      ans[x.id] = Qry(x.vl, x.vr);
      FOR(i, x.l, R) fake_sub(a[i], b[i]);
      fake_clear();
    }
  }
  memset(bc, 0, sizeof(bc));
  memset(c, 0, sizeof(c));
}
int main() {
  scanf("%d%d%d", &n, &m, &q);
  FOR(i, 1, n) scanf("%d", &a[i]);
  FOR(i, 1, n) scanf("%d", &b[i]);
  Tn = max(1, int(sqrt(n)));
  Tm = max(1, int(sqrt(m)));
  FOR(i, 1, q) {
    int l, r, vl, vr;
    scanf("%d%d%d%d", &l, &r, &vl, &vr);
    qry[l / Tn].pb({l, r, vl, vr, i});
  }
  FOR(i, 0, n / Tn) { calc(i, qry[i]); }
  FOR(i, 1, q) printf("%lld\n", ans[i]);
  return 0;
}
