// by Sshwy
//#define DEBUGGER
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

#ifdef DEBUGGER

#define log_prefix()                                                \
  {                                                                 \
    fprintf(stderr, "\033[37mLine %-3d [%dms]:\033[0m  ", __LINE__, \
        (int)clock() / 1000);                                       \
  }
#define ilog(...) \
  { fprintf(stderr, __VA_ARGS__); }
#define llog(...)      \
  {                    \
    log_prefix();      \
    ilog(__VA_ARGS__); \
  }
#define log(...)           \
  {                        \
    log_prefix();          \
    ilog(__VA_ARGS__);     \
    fprintf(stderr, "\n"); \
  }
#define red(...)       \
  {                    \
    log_prefix();      \
    ilog("\033[31m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define green(...)     \
  {                    \
    log_prefix();      \
    ilog("\033[32m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define blue(...)      \
  {                    \
    log_prefix();      \
    ilog("\033[34m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }

#else

#define log_prefix() ;
#define ilog(...) ;
#define llog(...) ;
#define log(...) ;
#define red(...) ;
#define green(...) ;
#define blue(...) ;

#endif

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 5e5 + 5;
using namespace RA;
int dg[N], is_leaf[N];
vector<int> nece, can;
int main() {
  srand(clock() + time(0));
  int n = rnd(1, 5);
  printf("%d\n", n);
  FOR(i, 1, n - 1) {
    int u = rnd(1, i), v = i + 1;
    printf("%d %d %d\n", u, v, rnd(1, 10));
    dg[u]++, dg[v]++;
  }
  FOR(i, 1, n) if (dg[i] == 1) is_leaf[i] = 1;
  is_leaf[1] = 0;
  can.pb(1);
  FOR(i, 2, n) {
    int u, v = i;
    if (nece.size()) {
      random_shuffle(nece.begin(), nece.end());
      u = nece.back();
      nece.pop_back();
    } else {
      u = can[rnd(can.size())];
    }
    printf("%d %d %d\n", u, v, rnd(1, 10));
    if (!is_leaf[v]) {
      nece.pb(v);
      can.pb(v);
    }
  }
  assert(nece.size() == 0);
  return 0;
}
