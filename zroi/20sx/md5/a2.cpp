#include <bits/stdc++.h>
using namespace std;

const int N = 300100;
typedef long long ll;
vector<pair<int, int>> ans;
ll g[N][30], sum[N];
int f[N][30], nxt[N][30], n, q, lim[N];
char s[N], t[N];

void solve(ll K, int L) {
  int XXX = 0;
  ans.clear();
  int all_sz = 0;
  ++K;
  for (int i = 1; --K; ++i) {
    int now = -1, sz = 1;
    for (int j = 0; j < 26; ++j)
      if (K > sum[nxt[i][j] + 1])
        K -= sum[nxt[i][j] + 1], ++XXX;
      else {
        now = j;
        break;
      }
    if (now == -1) {
      cout << -1 << '\n';
      return;
    }
    i = nxt[i][now];
    for (int j = lim[i]; j >= 0; --j)
      if (K > g[i][j] && K <= g[i][j] + sum[f[i][j] + 1])
        K -= g[i][j], sz += 1 << j, i = f[i][j], ++XXX;
    ans.emplace_back(now, sz);
    all_sz += sz;
  }
  for (auto e : ans) { printf("(%d,%d) ", e.first, e.second); }
  puts("");
  reverse(ans.begin(), ans.end());
  L = min(L, all_sz);
  t[L] = 0;
  for (auto i : ans)
    for (int j = i.second; L && j-- > 0; t[--L] = 'a' + i.first)
      ;
  cout << t << '\n';
  assert(XXX < 1000);
  printf("XXX %d\n", XXX);
}
const ll inf = 2e18;
inline void reduce(ll &x) { x > inf ? x = inf : 0; }
int main() {
  ios::sync_with_stdio(0);
  cin.tie(0);
  cout.tie(0);
  cin >> (s + 1) >> q;
  n = strlen(s + 1);
  sum[n + 1] = 1;
  for (int i = 0; i < 26; ++i) nxt[n + 1][i] = n + 1, f[n + 1][i] = n + 1;
  for (int i = n; i; --i) {
    int ch = s[i] - 'a';
    memcpy(nxt[i], nxt[i + 1], sizeof nxt[i]);
    f[i][0] = nxt[i][ch];
    g[i][0] = 1;
    for (int j = 0; j < ch; ++j) reduce(g[i][0] += sum[nxt[i][j] + 1]);
    reduce(sum[i] = sum[i + 1] * 2 - sum[f[i][0] + 1]);
    nxt[i][ch] = i;
    lim[i] = -1;
    for (int j = 1; j < 26; ++j) {
      f[i][j] = f[f[i][j - 1]][j - 1],
      reduce(g[i][j] = g[i][j - 1] + g[f[i][j - 1]][j - 1]);
      if (f[i][j] == n + 1 && lim[i] == -1) lim[i] = j - 1;
    }
  }
  for (; q-- > 0;) {
    ll x;
    int y;
    cin >> x >> y;
    solve(x, y);
  }
  return 0;
}
