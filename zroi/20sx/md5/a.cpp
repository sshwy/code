// by Sshwy
//#define DEBUGGER
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

#ifdef DEBUGGER

#define log_prefix()                                                \
  {                                                                 \
    fprintf(stderr, "\033[37mLine %-3d [%dms]:\033[0m  ", __LINE__, \
        (int)clock() / 1000);                                       \
  }
#define ilog(...) \
  { fprintf(stderr, __VA_ARGS__); }
#define llog(...)      \
  {                    \
    log_prefix();      \
    ilog(__VA_ARGS__); \
  }
#define log(...)           \
  {                        \
    log_prefix();          \
    ilog(__VA_ARGS__);     \
    fprintf(stderr, "\n"); \
  }
#define red(...)       \
  {                    \
    log_prefix();      \
    ilog("\033[31m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define green(...)     \
  {                    \
    log_prefix();      \
    ilog("\033[32m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define blue(...)      \
  {                    \
    log_prefix();      \
    ilog("\033[34m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }

#else

#define log_prefix() ;
#define ilog(...) ;
#define llog(...) ;
#define log(...) ;
#define red(...) ;
#define green(...) ;
#define blue(...) ;

#endif

namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 3e5 + 5, ALP = 26;
const long long INF = 0x3f3f3f3f3f3f3f3f;
char s[N], t[N], a[N];
long long f[N], val[N][30], g[N][30];
int nex[N][26], big[N];
// val: 走重边，排名会增加多少
int n, q;

void qry(long long k, int p) {
  ++k;
  int u = 1;
  vector<pair<int, int>> v;
  int XXX = 0;
  while (k > 1) {
    if (val[u][0] < k && k <= val[u][0] + f[g[u][0]]) {
      int len = 0;
      ROF(j, 19, 0) if (g[u][j]) {
        printf("! val[%d,%d]=%lld\n", u, j, val[u][j]);
        if (val[u][j] < k && k <= val[u][j] + f[g[u][j]]) {
          printf("val[%d,%d]=%lld\n", u, j, val[u][j]);
          k -= val[u][j], u = g[u][j], len += 1 << j;
          ++XXX;
        }
      }
      if (len) v.pb({u, len});
    }
    if (k > 1) {
      if (u > n) return puts("-1"), void();
      --k;
      bool flag = 0;
      FOR(i, 0, ALP - 1) if (nex[u][i]) {
        if (k > f[nex[u][i]])
          k -= f[nex[u][i]];
        else {
          u = nex[u][i];
          v.pb({u, 1});
          flag = 1;
          break;
        }
        ++XXX;
      }
      ++XXX;
      if (flag == 0) return puts("-1"), void();
    }
    if (k == 1) break;
  }
  // for(auto x:v)printf("(%d,%d) ",x.first,x.second);
  reverse(v.begin(), v.end());
  int la = 0;
  for (auto x : v) {
    FOR(i, 1, x.second) {
      if (la == p) break;
      a[++la] = s[x.first - i];
      ++XXX;
    }
    if (la == p) break;
  }
  reverse(a + 1, a + la + 1);
  a[la + 1] = 0;
  printf("%s\n", a + 1);
  fprintf(stderr, "XXX %d\n", XXX);
}
int main() {
  scanf("%s", s + 1);
  n = strlen(s + 1);
  // n+1是整个串的终态。0是不合法转移。
  ROF(i, n, 1) {
    FOR(j, 0, ALP - 1) nex[i][j] = nex[i + 1][j];
    nex[i][s[i] - 'a'] = i + 1;
  }
  f[n + 1] = 1;
  ROF(i, n, 1) {
    FOR(j, 0, ALP - 1) f[i] = min(INF, f[i] + f[nex[i][j]]);
    f[i] = min(INF, f[i] + 1);
  }
  // FOR(i,0,n)printf("%lld%c",f[i]," \n"[i==n]);

  // i的重儿子一定是i+1
  ROF(i, n, 1) {
    val[i][0] = 1; // val[i,j]: 从i出发，跳2^j次重边跳到的点，排名会增加多少
    FOR(j, 0, ALP - 1) if (nex[i][j]) {
      if (f[nex[i][j]] >= INF)
        g[i][0] = nex[i][j];
      else
        g[i][0] = i + 1;
      break;
    }
    // g[i][0]=i+1;
    printf("g[%d,%d]=%d\n", i, 0, g[i][0]);
    int lim = s[i] - 'a' - 1;
    FOR(j, 0, lim) val[i][0] = min(INF, val[i][0] + f[nex[i][j]]);
    FOR(j, 1, 19) {
      g[i][j] = g[g[i][j - 1]][j - 1];
      if (g[i][j] == 0) break;
      val[i][j] = min(INF, val[i][j - 1] + val[g[i][j - 1]][j - 1]);
    }
  }
  scanf("%d", &q);
  FOR(i, 1, q) {
    long long k;
    int p;
    scanf("%lld%d", &k, &p);
    qry(k, p);
  }
  return 0;
}
