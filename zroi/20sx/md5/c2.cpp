// by Sshwy
//#define DEBUGGER
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

#ifdef DEBUGGER

#define log_prefix()                                                \
  {                                                                 \
    fprintf(stderr, "\033[37mLine %-3d [%dms]:\033[0m  ", __LINE__, \
        (int)clock() / 1000);                                       \
  }
#define ilog(...) \
  { fprintf(stderr, __VA_ARGS__); }
#define llog(...)      \
  {                    \
    log_prefix();      \
    ilog(__VA_ARGS__); \
  }
#define log(...)           \
  {                        \
    log_prefix();          \
    ilog(__VA_ARGS__);     \
    fprintf(stderr, "\n"); \
  }
#define red(...)       \
  {                    \
    log_prefix();      \
    ilog("\033[31m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define green(...)     \
  {                    \
    log_prefix();      \
    ilog("\033[32m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define blue(...)      \
  {                    \
    log_prefix();      \
    ilog("\033[34m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }

#else

#define log_prefix() ;
#define ilog(...) ;
#define llog(...) ;
#define log(...) ;
#define red(...) ;
#define green(...) ;
#define blue(...) ;

#endif

namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 5e5 + 5;
int n, dg[N], is_leaf[N];
struct edge {
  int u, v, w;
} E[N];
int le;
bool cmp(edge x, edge y) { return x.w > y.w; }
int f[N];
void init(int lim) { FOR(i, 0, lim) f[i] = i; }
int get(int u) { return f[u] == u ? u : f[u] = get(f[u]); }
bool merge(int u, int v) {
  if (get(u) == get(v)) return 0;
  f[get(u)] = get(v);
  return 1;
}
long long sum = 0;
int main() {
  scanf("%d", &n);
  FOR(i, 1, n - 1) {
    int u, v, w;
    scanf("%d%d%d", &u, &v, &w);
    E[++le] = {u, v, w};
    sum += w;
    dg[u]++, dg[v]++;
  }
  FOR(i, 1, n) if (dg[i] == 1) is_leaf[i] = 1;
  is_leaf[1] = 0;
  FOR(i, 1, n - 1) {
    int u, v, w;
    scanf("%d%d%d", &u, &v, &w);
    if (!is_leaf[u]) u += n;
    if (!is_leaf[v]) v += n;
    E[++le] = {u, v, w};
    sum += w;
  }
  sort(E + 1, E + le + 1, cmp);
  long long ans = 0;
  FOR(i, 1, le) { // force to use i
    printf("%d %d %d\n", E[i].u, E[i].v, E[i].w);
    init(n * 2);
    merge(E[i].u, E[i].v);
    long long s = 0;
    FOR(j, 1, le) if (merge(E[j].u, E[j].v)) s += E[j].w;
    printf("s %lld\n", s);
    ans = max(ans, s);
  }
  ans = sum - ans;
  printf("%lld\n", ans);
  return 0;
}
