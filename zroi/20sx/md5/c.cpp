// by Sshwy
//#define DEBUGGER
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

#ifdef DEBUGGER

#define log_prefix()                                                \
  {                                                                 \
    fprintf(stderr, "\033[37mLine %-3d [%dms]:\033[0m  ", __LINE__, \
        (int)clock() / 1000);                                       \
  }
#define ilog(...) \
  { fprintf(stderr, __VA_ARGS__); }
#define llog(...)      \
  {                    \
    log_prefix();      \
    ilog(__VA_ARGS__); \
  }
#define log(...)           \
  {                        \
    log_prefix();          \
    ilog(__VA_ARGS__);     \
    fprintf(stderr, "\n"); \
  }
#define red(...)       \
  {                    \
    log_prefix();      \
    ilog("\033[31m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define green(...)     \
  {                    \
    log_prefix();      \
    ilog("\033[32m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define blue(...)      \
  {                    \
    log_prefix();      \
    ilog("\033[34m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }

#else

#define log_prefix() ;
#define ilog(...) ;
#define llog(...) ;
#define log(...) ;
#define red(...) ;
#define green(...) ;
#define blue(...) ;

#endif

namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
#include <stack>
const int N = 5e5 + 5;
int n, dg[N], is_leaf[N];
bool vis[N];
struct edge {
  int u, v, w;
} E[N];

long long f[N], g[N];
long long h[N];
stack<pair<long long *, long long>> rec;

int get(int u) { return f[u] == u ? u : get(f[u]); }
bool merge(int u, int v, int w) {
  if (get(u) == get(v)) return 0;
  u = get(u), v = get(v);
  if (g[u] > g[v]) swap(u, v);
  rec.push({f + u, f[u]});
  f[u] = v;
  rec.push({g + v, g[v]});
  g[v] += g[u];
  rec.push({h + v, h[v]});
  h[v] += h[u] + w;
  return 1;
}
bool merge(edge e) { return merge(e.u, e.v, e.w); }
bool find(int u, int v) { return get(u) == get(v); }
void rebase(int tag) {
  while (rec.size() > tag) *rec.top().first = rec.top().second, rec.pop();
}

vector<edge> S[30]; // static
int le;
int D[N], ld; // dynamic edge (modify), record the index of edge
long long ans[N];
bool cmp(edge x, edge y) { return x.w > y.w; }; // big to small
void zip(int l, int r, int dep) {               // remove useless, merge necessary
  vector<edge> &s = S[dep], ns, ne;

  sort(s.begin(), s.end(), cmp);

  // remove useless
  int tag = rec.size();
  for (auto e : s) {
    if (merge(e)) ns.pb(e); // if on Max ST
  }
  s = ns;
  ns.clear();
  rebase(tag);

  // merge necessary
  tag = rec.size();
  FOR(i, l, r) merge(E[D[i]]);
  for (auto e : s)
    if (merge(e)) ne.pb(e);
  rebase(tag);

  for (auto e : ne) merge(e);
  for (auto e : s)
    if (!find(e.u, e.v)) ns.pb(e);
  s = ns;
}
void solve(int l, int r, int dep) {
  zip(l, r, dep);

  if (l == r) {
    int tag = rec.size();
    merge(E[D[l]].u, E[D[l]].v, 0);
    sort(S[dep].begin(), S[dep].end(), cmp);
    for (auto e : S[dep]) merge(e);
    ans[l] = h[get(1)];
    rebase(tag);
    return;
  }
  int mid = (l + r) >> 1;

  // [l,mid]
  // turn some of dynamic into static of [mid,r]
  S[dep + 1] = S[dep];
  FOR(i, l, r) vis[D[i]] = 0;
  FOR(i, l, mid) vis[D[i]] = 1;
  FOR(i, mid + 1, r) if (vis[D[i]] == 0) S[dep + 1].pb(E[D[i]]), vis[D[i]] = 1;
  int tag = rec.size();
  solve(l, mid, dep + 1);
  rebase(tag);

  S[dep + 1] = S[dep];
  FOR(i, l, r) vis[D[i]] = 0;
  FOR(i, mid + 1, r) vis[D[i]] = 1;
  FOR(i, l, mid) if (vis[D[i]] == 0) S[dep + 1].pb(E[D[i]]), vis[D[i]] = 1;
  solve(mid + 1, r, dep + 1);
}
long long sum;
int main() {
  scanf("%d", &n);
  if (n == 1) return puts("0"), 0;
  FOR(i, 1, n - 1) {
    int u, v, w;
    scanf("%d%d%d", &u, &v, &w);
    E[++le] = {u, v, w};
    sum += w;
    D[++ld] = le;
    dg[u]++, dg[v]++;
  }
  FOR(i, 1, n) if (dg[i] == 1) is_leaf[i] = 1;
  is_leaf[1] = 0;

  FOR(i, 1, n - 1) {
    int u, v, w;
    scanf("%d%d%d", &u, &v, &w);
    if (!is_leaf[u]) u += n;
    if (!is_leaf[v]) v += n;
    E[++le] = {u, v, w};
    sum += w;
    D[++ld] = le;
  }
  FOR(i, 1, n * 2) f[i] = i, g[i] = 1, h[i] = 0;

  solve(1, ld, 1);
  // FOR(i,1,ld)printf("ans[%d]=%d
  // (%d,%d,%d)\n",i,ans[i],E[D[i]].u,E[D[i]].v,E[D[i]].w);

  long long as = sum - *max_element(ans + 1, ans + ld + 1);
  printf("%lld\n", as);

  return 0;
}
