#include <bits/stdc++.h>
using namespace std;
#define rep(i, a, n) for (int i = a; i < n; i++)
#define per(i, a, n) for (int i = n - 1; i >= a; i--)
#define pb push_back
#define mp make_pair
#define all(x) (x).begin(), (x).end()
#define fi first
#define se second
#define SZ(x) ((int)(x).size())
typedef vector<int> VI;
typedef long long ll;
typedef pair<int, int> PII;
typedef double db;
mt19937 mrand(random_device{}());
const ll mod = 1000000007;
int rnd(int x) { return mrand() % x; }
ll powmod(ll a, ll b) {
  ll res = 1;
  a %= mod;
  assert(b >= 0);
  for (; b; b >>= 1) {
    if (b & 1) res = res * a % mod;
    a = a * a % mod;
  }
  return res;
}
ll gcd(ll a, ll b) { return b ? gcd(b, a % b) : a; }
// head

const int N = 510;

int n, m, u, v;
ll h[N][N], c[N][N], k;
VI pmod;
VI gao() {
  scanf("%d%d%lld", &n, &m, &k);
  rep(i, 0, m) {
    scanf("%d%d", &u, &v);
    h[u][v]--;
    h[v][u]--;
    h[u][u]++;
    h[v][v]++;
  }

  for (int m = 2; m <= n - 1; m++) {
    int i = m + 1;
    while (h[i][m - 1] == 0 && i <= n) i++;
    if (i > n) continue;
    int t = h[i][m - 1];
    if (i > m) {
      rep(j, 1, n + 1) swap(h[i][j], h[m][j]);
      rep(j, 1, n + 1) swap(h[j][i], h[j][m]);
    }
    ll invt = powmod(t, mod - 2);
    for (int i = m + 1; i <= n; i++) {
      ll u = h[i][m - 1] * invt % mod;
      for (int j = 1; j <= n; j++) {
        h[i][j] = (h[i][j] - u * h[m][j]) % mod;
        h[j][m] = (h[j][m] + u * h[j][i]) % mod;
      }
    }
  }
  c[0][0] = 1;
  for (int m = 1; m <= n; m++) {
    ll t = 1;
    for (int j = 0; j <= m - 1; j++) {
      c[m][j + 1] = (c[m][j + 1] + c[m - 1][j]) % mod;
      c[m][j] = (c[m][j] - h[m][m] * c[m - 1][j]) % mod;
    }
    for (int i = 1; i < m; i++) {
      t = t * h[m - i + 1][m - i] % mod;
      for (int j = 0; j <= m - i - 1; j++) {
        c[m][j] = (c[m][j] - t * h[m - i][m] % mod * c[m - i - 1][j]) % mod;
      }
    }
  }
  VI a;
  rep(i, 1, n + 1) a.pb((c[n][i] + mod) % mod);
  // printf("%lld ",(c[n][i]+mod)%mod);
  return a;
}

VI gaomod(VI b) {
  while (SZ(b) >= SZ(pmod)) {
    ll coef = b.back();
    rep(i, 0, SZ(pmod)) b[SZ(b) - SZ(pmod) + i] =
        (b[SZ(b) - SZ(pmod) + i] - pmod[i] * coef) % mod;
    while (SZ(b) > 1 && b.back() == 0) b.pop_back();
  }
  return b;
}
VI mul(VI a, VI b) {
  if (a.empty() || b.empty()) return {};
  VI c(SZ(a) + SZ(b) - 1, 0);
  rep(i, 0, SZ(a)) rep(j, 0, SZ(b)) c[i + j] = (c[i + j] + (ll)a[i] * b[j]) % mod;
  return gaomod(c);
}
VI add(VI a, VI b) {
  VI c(max(SZ(a), SZ(b)), 0);
  int n = max(SZ(a), SZ(b));
  rep(i, 0, n) c[i] = ((i < SZ(a) ? a[i] : 0) + (i < SZ(b) ? b[i] : 0)) % mod;
  return gaomod(c);
}
typedef VI matrix[2][2];
matrix base;

void multo(matrix &a, matrix b) {
  matrix c;
  rep(i, 0, 2) rep(j, 0, 2) rep(k, 0, 2) c[i][j] =
      add(c[i][j], mul(a[i][k], b[k][j]));
  rep(i, 0, 2) rep(j, 0, 2) a[i][j] = c[i][j];
}
void powmod(matrix &a, ll b) {
  matrix res;
  res[0][0] = res[1][1] = VI{1};
  for (; b; b >>= 1) {
    if (b & 1) multo(res, a);
    multo(a, a);
  }
  rep(i, 0, 2) rep(j, 0, 2) a[i][j] = res[i][j];
}

int resultant(vector<int> a, vector<int> b) {
  ll ans = 1;
  while (true) {
    int n = a.size() - 1;
    int m = b.size() - 1;
    if (n > m) {
      swap(n, m);
      swap(a, b);
      if ((n & 1) && (m & 1)) { ans = (mod - ans) % mod; }
    }
    if (!n) {
      for (int i = 0; i < m; ++i) { ans = ans * a[0] % mod; }
      return ans;
    }
    ll coef = powmod(a.back(), mod - 2) * b.back() % mod;
    for (int i = 0; i < (int)a.size(); ++i) {
      b[b.size() - a.size() + i] = (b[b.size() - a.size() + i] - a[i] * coef) % mod;
    }
    while ((int)b.size() > 1 && b.back() == 0) {
      ans = ans * a[n] % mod;
      b.pop_back();
    }
  }
}

int main() {
  auto a = gao();
  printf("a: ");
  for (int x : a) printf("%d ", x);
  puts("");
  if (n == 1) {
    puts("1");
    return 0;
  }
  if (a[0] == 0) {
    puts("0");
    return 0;
  }
  pmod = a;
  base[0][0] = {2, 1};
  base[0][1] = {mod - 1};
  base[1][0] = {1};
  powmod(base, k);
  VI b = base[1][0];
  printf("b: ");
  for (int x : b) printf("%d ", x);
  puts("");
  ll ans = a[0];
  printf("ans=%lld\n", ans);
  if (n % 2 == 0) ans = ans * (mod - 1) % mod;
  ll t = resultant(a, b);
  printf("resultant=%lld\n", t);
  ans = ans * t % mod;
  printf("ans=%lld\n", ans);
  ans = ans * powmod(n, mod - 2) % mod;
  if (ans < 0) ans += mod;
  printf("%lld\n", ans);
}
