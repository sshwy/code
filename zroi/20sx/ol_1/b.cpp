// by Sshwy
//#define DEBUGGER
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

#ifdef DEBUGGER

#define log_prefix()                                                  \
  {                                                                   \
    fprintf(stderr, "\033[37mLine %-3d [%lldms]:\033[0m  ", __LINE__, \
        (int)clock() / 1000);                                         \
  }
#define ilog(...) \
  { fprintf(stderr, __VA_ARGS__); }
#define llog(...)      \
  {                    \
    log_prefix();      \
    ilog(__VA_ARGS__); \
  }
#define log(...)           \
  {                        \
    log_prefix();          \
    ilog(__VA_ARGS__);     \
    fprintf(stderr, "\n"); \
  }
#define red(...)       \
  {                    \
    log_prefix();      \
    ilog("\033[31m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define green(...)     \
  {                    \
    log_prefix();      \
    ilog("\033[32m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define blue(...)      \
  {                    \
    log_prefix();      \
    ilog("\033[34m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }

#else

#define log_prefix() ;
#define ilog(...) ;
#define llog(...) ;
#define log(...) ;
#define red(...) ;
#define green(...) ;
#define blue(...) ;

#endif

namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
#define int long long
const int N = 505, P = 1e9 + 7;
int pw(int a, int m) {
  int res = 1;
  while (m) m & 1 ? res = 1ll * res * a % P : 0, a = 1ll * a * a % P, m >>= 1;
  return res;
}

typedef vector<int> Poly;

Poly operator-(const Poly &p, const Poly &q) {
  Poly res = p;
  res.resize(max(p.size(), q.size()), 0);
  for (long unsigned i = 0; i < q.size(); ++i) res[i] = (res[i] - q[i] + P) % P;
  return res;
}
Poly operator+(const Poly &p, const Poly &q) {
  Poly res = p;
  res.resize(max(p.size(), q.size()), 0);
  for (long unsigned i = 0; i < q.size(); ++i) res[i] = (res[i] + q[i]) % P;
  return res;
}
Poly operator*(const Poly &p, const Poly &q) {
  if (!p.size() || !q.size()) return Poly();
  Poly res(p.size() + q.size() - 1, 0);
  for (long unsigned i = 0; i < p.size(); i++)
    for (long unsigned j = 0; j < q.size(); j++)
      res[i + j] = (res[i + j] + p[i] * 1ll * q[j] % P) % P;
  return res;
}
Poly operator%(const Poly &p, const Poly &q) { // mod
  assert(q.size());
  Poly res = p;
  while (res.size() >= q.size()) {
    int d = res.size() - q.size();
    int rate = res.back() * 1ll * pw(q.back(), P - 2) % P;
    for (long unsigned i = 0; i < q.size(); i++)
      res[i + d] = (res[i + d] - q[i] * 1ll * rate % P + P) % P;
    assert(res.back() == 0);
    res.pop_back();
  }
  return res;
}

int t[N][N];
int det(int g[N][N], int n) {
  memcpy(t, g, sizeof(t)), g = t;
  int res = 1;
  FOR(i, 1, n) {
    if (!g[i][i]) FOR(j, i + 1, n) if (g[j][i]) {
        FOR(k, i, n) swap(g[i][k], g[j][k]);
        res = P - res;
        break;
      }
    if (!g[i][i]) return 0;
    FOR(j, 1, n) if (i != j) {
      int rate = g[j][i] * 1ll * pw(g[i][i], P - 2) % P;
      FOR(k, i, n) g[j][k] = (g[j][k] - g[i][k] * 1ll * rate % P + P) % P;
    }
  }
  FOR(i, 1, n) res = res * 1ll * g[i][i] % P;
  return res;
}

int t2[N][N];
Poly p[N];
Poly characteristic_polynomial(int M[N][N], int n) { //求M的特征多项式
  memcpy(t2, M, sizeof(t2)), M = t2;
  int x = det(M, n);
  // 1. 化为上海森堡矩阵
  FOR(j, 1, n - 1) { //列
    if (!M[j + 1][j]) FOR(i, j + 2, n) if (M[i][j]) {
        FOR(k, 1, n) swap(M[j + 1][k], M[i][k]); // R[j+1] <-> R[i]
        FOR(k, 1, n) swap(M[k][j + 1], M[k][i]); // C[j+1] <-> C[i]
        break;
      }
    //把第j列的第j+1行以下的位置全部消元
    FOR(i, j + 2, n) if (M[i][j]) {
      int rate = M[i][j] * 1ll * pw(M[j + 1][j], P - 2) % P;
      FOR(k, 1, n)
      M[i][k] = (M[i][k] - M[j + 1][k] * 1ll * rate % P + P) %
                P; // R[i]   = R[i]-rate*R[j+1]
      FOR(k, 1, n)
      M[k][j + 1] =
          (M[k][j + 1] + M[k][i] * 1ll * rate % P) % P; // C[j+1] = C[j+1]+rate*C[i]
    }
  }
  assert(x == det(M, n));
  // 2. 计算特征多项式：即(xI-A)的行列式
  p[n + 1] = Poly(1, 1);
  ROF(i, n, 1) {
    Poly s, t;

    t = Poly(1, P - M[i][n]);
    if (i == n) t.pb(1); // x-M[n][n]
    s = p[n + 1] * t;
    ROF(j, n, i + 1) {
      t = Poly(1, P - M[i][j - 1]);
      if (i == j - 1) t.pb(1);
      s = t * p[j] - Poly(1, P - M[j][j - 1]) * s;
    }
    p[i] = s;
  }
  return p[1];
}

const int SZ = 3;
template <class T>
void Mtx_mul(const T A[SZ][SZ], const T B[SZ][SZ], T C[SZ][SZ], const T MOD, int n) {
  FOR(i, 1, n) FOR(j, 1, n) C[i][j] = T();
  FOR(i, 1, n) FOR(j, 1, n) FOR(k, 1, n) {
    C[i][k] = (C[i][k] + A[i][j] * B[j][k] % MOD) % MOD;
  }
}
Poly Mp[SZ][SZ];

int n, m;
long long k;
int L[N][N];

int resultant(vector<int> a,
    vector<int> b) { // 实际上计算的是 resultant(b,a)。因为b是b减掉a
  long long ans = 1;
  while (true) {
    int n = a.size() - 1;
    int m = b.size() - 1;
    if (n > m) {
      swap(n, m);
      swap(a, b);
      if ((n & 1) && (m & 1)) { ans = (P - ans) % P; }
    }
    if (!n) { // a 是常数
      for (int i = 0; i < m; ++i) { ans = ans * a[0] % P; }
      return ans;
    }
    long long coef = pw(a.back(), P - 2) * 1ll * b.back() % P; // b-a
    for (int i = 0; i < (int)a.size(); ++i) {
      b[b.size() - a.size() + i] =
          (b[b.size() - a.size() + i] - a[i] * coef % P + P) % P;
    }
    while ((int)b.size() > 1 && b.back() == 0) {
      ans = ans * a[n] % P;
      b.pop_back();
    }
  }
}
signed main() {
  scanf("%lld%lld%lld", &n, &m, &k);
  FOR(i, 1, m) {
    int u, v;
    scanf("%lld%lld", &u, &v);
    L[u][v] = (L[u][v] + P - 1) % P, L[v][u] = (L[v][u] + P - 1) % P;
    L[u][u]++, L[v][v]++;
  }
  FOR(i, 1, n) FOR(j, 1, n) L[i][j] = (L[i][j] + P) % P;
  int det_L = det(L, n - 1);
  if (k == 1 || det_L == 0) {
    printf("%lld\n", det_L);
    return 0;
  } else if (n == 1) {
    printf("%lld\n", 1ll);
    return 0;
  }
  Poly Q = characteristic_polynomial(L, n);

  Poly pk = {2, 1};
  Poly Mk[SZ][SZ] = {{}, {Poly(), pk, Poly(1, P - 1)}, {Poly(), Poly(1, 1), Poly()}},
       Mt[SZ][SZ], Mm[SZ][SZ];
  Poly Path, P_2 = {0, 2, 1}, P_3 = {0, 3, 4, 1};

  long long k1 = k;
  if (k == 2) {
    Path = P_2 % Q;
  } else if (k == 3) {
    Path = P_3 % Q;
  } else {
    k -= 3;
    --k;
    FOR(i, 1, 2) FOR(j, 1, 2) Mm[i][j] = Mk[i][j];
    while (k) {
      if (k & 1) {
        Mtx_mul(Mm, Mk, Mt, Q, 2);
        FOR(i, 1, 2) FOR(j, 1, 2) Mm[i][j] = Mt[i][j];
      }
      Mtx_mul(Mk, Mk, Mt, Q, 2);
      FOR(i, 1, 2) FOR(j, 1, 2) Mk[i][j] = Mt[i][j];
      k >>= 1;
    }
    Path = P_3 * Mm[1][1] % Q + P_2 * Mm[1][2] % Q;
  }

  // 2.
  // 计算Q和Path的非零特征值的两两和的积，再乘上Q的非零特征值的积以及Path的非零特征值的积的绝对值
  // 即，计算res(Q/x,Path/x)（除掉x相当于把0的特征值除掉了），然后乘上Q的特征值的积和Path的特征值的积

  Q.erase(Q.begin());
  Path.erase(Path.begin());
  assert(Q.size() && Path.size());
  int ans = resultant(Q, Path);
  ans = (ans + P) % P;

  // ans=1ll*ans*pw(pw(Q.back(),P-2),Path.size()-1)%P; // Q的最高位是1
  // ans=1ll*ans*pw(pw(Path.back(),P-2),Q.size()-1)%P; //
  // Path的最高位（在modQ之前）是1，所以不用除

  ans = ans * 1ll * Q[0] % P * pw(P - 1, Q.size() - 1) % P;
  ans = ans * 1ll * (k1 % P) % P; //本来要乘的两个-1抵消了
  ans = 1ll * ans * pw(n * 1ll * (k1 % P) % P, P - 2) % P;
  printf("%lld\n", ans);
  return 0;
}
// 1.
// 求出L(G)的特征多项式Q和L(Pk)的类似特征多项式的东西（但是要把所有特征根反号。直接方法就是把特征多项式改成|-xI-A|）Path（Path
// mod Q)
// 2. 计算res(Q,Path mod Q)（或者是res(-Q,Path mod Q），细节问题
