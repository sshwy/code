// by Sshwy
//#define DEBUGGER
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

#ifdef DEBUGGER

#define log_prefix()                                                \
  {                                                                 \
    fprintf(stderr, "\033[37mLine %-3d [%dms]:\033[0m  ", __LINE__, \
        (int)clock() / 1000);                                       \
  }
#define ilog(...) \
  { fprintf(stderr, __VA_ARGS__); }
#define llog(...)      \
  {                    \
    log_prefix();      \
    ilog(__VA_ARGS__); \
  }
#define log(...)           \
  {                        \
    log_prefix();          \
    ilog(__VA_ARGS__);     \
    fprintf(stderr, "\n"); \
  }
#define red(...)       \
  {                    \
    log_prefix();      \
    ilog("\033[31m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define green(...)     \
  {                    \
    log_prefix();      \
    ilog("\033[32m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define blue(...)      \
  {                    \
    log_prefix();      \
    ilog("\033[34m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }

#else

#define log_prefix() ;
#define ilog(...) ;
#define llog(...) ;
#define log(...) ;
#define red(...) ;
#define green(...) ;
#define blue(...) ;

#endif

namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 505, P = 1e9 + 7;
int pw(int a, int m) {
  int res = 1;
  while (m) m & 1 ? res = 1ll * res * a % P : 0, a = 1ll * a * a % P, m >>= 1;
  return res;
}

int t[N][N];
int det(int g[N][N], int n) {
  memcpy(t, g, sizeof(t)), g = t;
  int res = 1;
  FOR(i, 1, n) {
    if (!g[i][i]) FOR(j, i + 1, n) if (g[j][i]) {
        FOR(k, i, n) swap(g[i][k], g[j][k]);
        res = P - res;
        break;
      }
    if (!g[i][i]) return 0;
    FOR(j, 1, n) if (i != j) {
      int rate = g[j][i] * 1ll * pw(g[i][i], P - 2) % P;
      FOR(k, i, n) g[j][k] = (g[j][k] - g[i][k] * 1ll * rate % P + P) % P;
    }
  }
  FOR(i, 1, n) res = res * 1ll * g[i][i] % P;
  return res;
}

int n, m, k;
int w[N][N];

int main() {
  scanf("%d%d%d", &n, &m, &k);
  FOR(i, 1, m) {
    int u, v;
    scanf("%d%d", &u, &v);
    FOR(j, 0, k - 1) {
      int U = u + j * n, V = v + j * n;
      w[U][V]--, w[V][U]--;
      w[U][U]++, w[V][V]++;
    }
  }
  FOR(u, 1, n) {
    FOR(j, 0, k - 2) {
      int U = u + j * n, V = u + (j + 1) * n;
      w[U][V]--, w[V][U]--;
      w[U][U]++, w[V][V]++;
    }
  }
  int ans = det(w, n * k - 1);
  printf("%d\n", ans);

  return 0;
}
