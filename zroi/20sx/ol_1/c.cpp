// by Sshwy
#define DEBUGGER
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

#ifdef DEBUGGER

#define log_prefix()                                                \
  {                                                                 \
    fprintf(stderr, "\033[37mLine %-3d [%dms]:\033[0m  ", __LINE__, \
        (int)clock() / 1000);                                       \
  }
#define ilog(...) \
  { fprintf(stderr, __VA_ARGS__); }
#define llog(...)      \
  {                    \
    log_prefix();      \
    ilog(__VA_ARGS__); \
  }
#define log(...)           \
  {                        \
    log_prefix();          \
    ilog(__VA_ARGS__);     \
    fprintf(stderr, "\n"); \
  }
#define red(...)       \
  {                    \
    log_prefix();      \
    ilog("\033[31m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define green(...)     \
  {                    \
    log_prefix();      \
    ilog("\033[32m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define blue(...)      \
  {                    \
    log_prefix();      \
    ilog("\033[34m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }

#else

#define log_prefix() ;
#define ilog(...) ;
#define llog(...) ;
#define log(...) ;
#define red(...) ;
#define green(...) ;
#define blue(...) ;

#endif

namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 2e4 + 5, P = 1e9 + 7;
int pw(int a, int m) {
  int res = 1;
  while (m) m & 1 ? res = 1ll * res * a % P : 0, a = 1ll * a * a % P, m >>= 1;
  return res;
}
int n, k;
map<int, int> s[N];

void div(int x) {
  map<int, int> &A = s[x];
  for (int i = 2; i * i <= x; i++) {
    if (x % i) continue;
    A[i] = 0;
    while (x % i == 0) x /= i, ++A[i];
  }
  if (x > 1) A[x] = 1;
}
map<int, pair<priority_queue<int>, priority_queue<int>>> S;
void insert(int x) {
  for (auto &p : s[x]) { S[p.fi].fi.push(p.se); }
}
void remove(int x) {
  for (auto &p : s[x]) { S[p.fi].se.push(p.se); }
}
int calc() {
  int res = 1;
  for (auto &p : S) {
    while (p.se.se.size() && p.se.fi.top() == p.se.se.top())
      p.se.fi.pop(), p.se.se.pop();
    if (p.se.fi.size()) res = 1ll * res * pw(p.fi, p.se.fi.top()) % P;
  }
  return res;
}
int ans;
int main() {
  scanf("%d%d", &n, &k);
  if (n == 0) return puts("0"), 0;
  FOR(i, 2, n + k) { div(i); }
  FOR(i, 1, k + 1) insert(i);
  ans = calc();
  FOR(i, 2, n) {
    remove(i - 1);
    insert(i + k);
    ans = (ans + calc()) % P;
  }
  printf("%d\n", ans);
  return 0;
}
