// by Sshwy
//#define DEBUGGER
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

#ifdef DEBUGGER

#define log_prefix()                                                \
  {                                                                 \
    fprintf(stderr, "\033[37mLine %-3d [%dms]:\033[0m  ", __LINE__, \
        (int)clock() / 1000);                                       \
  }
#define ilog(...) \
  { fprintf(stderr, __VA_ARGS__); }
#define llog(...)      \
  {                    \
    log_prefix();      \
    ilog(__VA_ARGS__); \
  }
#define log(...)           \
  {                        \
    log_prefix();          \
    ilog(__VA_ARGS__);     \
    fprintf(stderr, "\n"); \
  }
#define red(...)       \
  {                    \
    log_prefix();      \
    ilog("\033[31m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define green(...)     \
  {                    \
    log_prefix();      \
    ilog("\033[32m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define blue(...)      \
  {                    \
    log_prefix();      \
    ilog("\033[34m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }

#else

#define log_prefix() ;
#define ilog(...) ;
#define llog(...) ;
#define log(...) ;
#define red(...) ;
#define green(...) ;
#define blue(...) ;

#endif

namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 2e5 + 5, M = 6e5 + 5;

struct qxx {
  int nex, t;
} e[N * 2];
int h[N], le = 1;
void add_path(int f, int t) { e[++le] = (qxx){h[f], t}, h[f] = le; }

int n, m;
vector<pair<int, int>> qry[N];
vector<int> ans;
vector<int> tags[N];

bool Cut[N];
int sz[N], sm[N], cnt[N];

void Size(int u, int p) {
  sz[u] = 1, sm[u] = 0;
  for (int i = h[u]; i; i = e[i].nex) {
    int v = e[i].t;
    if (!Cut[v] && v != p) { Size(v, u), sz[u] += sz[v], sm[u] = max(sm[u], sz[v]); }
  }
}
int Core(int u, int p, int T) {
  int res = u, mx = max(T - sz[u], sm[u]);
  for (int i = h[u]; i; i = e[i].nex) {
    int v = e[i].t;
    if (!Cut[v] && v != p) {
      int x = Core(v, u, T), y = max(T - sz[x], sm[x]);
      if (y < mx) res = x, mx = y;
    }
  }
  return res;
}
struct atom {
  vector<int>::iterator p, q;
  int u;
  bool operator<(const atom x) const { return *p != *(x.p) ? *p > *(x.p) : u < x.u; }
};
priority_queue<atom> q[N];
void dfs1(int u, int p, int las_time) { // init
  cnt[u] = 1;
  while (q[u].size()) q[u].pop(); // clear
  for (int i = h[u]; i; i = e[i].nex) {
    int v = e[i].t;
    if (!Cut[v] && v != p) {
      auto &tg = tags[i / 2];
      if (tg.back() > las_time) {
        auto pt = lower_bound(tg.begin(), tg.end(), las_time);
        dfs1(v, u, *pt);
        cnt[u] += cnt[v];
        q[u].push(atom{pt, tg.end(), v});
      }
    }
  }
}
void update(int u, int p, int las_time) {
  while (q[u].size()) {
    auto x = q[u].top();
    q[u].pop();
    if (*(x.p) > las_time) return;
    cnt[u] -= cnt[x.u];
    ++x.p;
    if (x.p != x.q) {
      update(x.u, u, *(x.p));
      cnt[u] += cnt[x.u];
      q[u].push(x);
    }
  }
}
void Solve(int u, int p) {
  Size(u, p);
  int core = Core(u, p, sz[u]);

  // do sth ...
  dfs1(core, 0, 0);
  vector<int> tim, sum;

  while (q[core].size()) {
    auto x = q[core].top();
    q[core].pop();
    // record answer
    tim.pb(*(x.p));
    sum.pb(cnt[core]);

    cnt[core] -= cnt[x.u];
    ++x.p;
    if (x.p != x.q) {
      update(x.u, core, *(x.p));
      cnt[core] += cnt[x.u];
      q[core].push(x);
    }
  }

  //从tim出发能到达的点的个数（sum）
  for (auto x : tim) printf("%2d ", x);
  puts("");
  for (auto x : sum) printf("%2d ", x);
  puts("");

  Cut[core] = 1;
  for (int i = h[core]; i; i = e[i].nex) {
    int v = e[i].t;
    if (!Cut[v]) Solve(v, core);
  }
}

int main() {
  scanf("%d%d", &n, &m);
  FOR(i, 1, n - 1) {
    int u, v;
    scanf("%d%d", &u, &v);
    add_path(u, v);
    add_path(v, u);
  }
  FOR(i, 1, m) {
    int t, x;
    scanf("%d%d", &t, &x);
    if (t == 1)
      qry[x].pb({i, ans.size()}), ans.pb(0);
    else
      tags[x].pb(i);
  }

  Solve(1, 0);

  for (auto x : ans) printf("%d\n", x);
  return 0;
}
