// by Sshwy
//#define DEBUGGER
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

#ifdef DEBUGGER

#define log_prefix()                                                \
  {                                                                 \
    fprintf(stderr, "\033[37mLine %-3d [%dms]:\033[0m  ", __LINE__, \
        (int)clock() / 1000);                                       \
  }
#define ilog(...) \
  { fprintf(stderr, __VA_ARGS__); }
#define llog(...)      \
  {                    \
    log_prefix();      \
    ilog(__VA_ARGS__); \
  }
#define log(...)           \
  {                        \
    log_prefix();          \
    ilog(__VA_ARGS__);     \
    fprintf(stderr, "\n"); \
  }
#define red(...)       \
  {                    \
    log_prefix();      \
    ilog("\033[31m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define green(...)     \
  {                    \
    log_prefix();      \
    ilog("\033[32m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define blue(...)      \
  {                    \
    log_prefix();      \
    ilog("\033[34m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }

#else

#define log_prefix() ;
#define ilog(...) ;
#define llog(...) ;
#define log(...) ;
#define red(...) ;
#define green(...) ;
#define blue(...) ;

#endif

namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 1e5 + 5, W = 1 << 30, SZ = 4e6;
int k, n, m;

int root;
namespace sg1 {
  int mn[SZ], c[SZ], tag[SZ], lc[SZ], rc[SZ], v[SZ], tot;
  int W(int l, int r) { return l ^ ((r - l + 1) / 2); }
  int new_node(int l, int r) {
    return ++tot, mn[tot] = 0, c[tot] = r - l + 1, tag[tot] = 0,
                  lc[tot] = rc[tot] = 0, v[tot] = W(l, r), tot;
  }
  void pushup(int u) {
    if (mn[lc[u]] < mn[rc[u]])
      mn[u] = mn[lc[u]], c[u] = c[lc[u]], v[u] = v[lc[u]];
    else if (mn[lc[u]] > mn[rc[u]])
      mn[u] = mn[rc[u]], c[u] = c[rc[u]], v[u] = v[rc[u]];
    else
      mn[u] = mn[lc[u]], c[u] = c[lc[u]] + c[rc[u]], v[u] = v[lc[u]] ^ v[rc[u]];
  }
  void node_add(int u, int v) { mn[u] += v, tag[u] += v; }
  inline void pushdown(int u, int l, int r) {
    int mid = (l + r) >> 1;
    if (!lc[u]) lc[u] = new_node(l, mid);
    if (!rc[u]) rc[u] = new_node(mid + 1, r);
    if (tag[u]) node_add(lc[u], tag[u]), node_add(rc[u], tag[u]), tag[u] = 0;
  }
  void add(int L, int R, int v, int u, int l, int r) {
    if (R < l || r < L) return;
    if (L <= l && r <= R) return node_add(u, v), void();
    int mid = (l + r) >> 1;
    pushdown(u, l, r);
    add(L, R, v, lc[u], l, mid), add(L, R, v, rc[u], mid + 1, r);
    pushup(u);
  }
  pair<int, int> merge(pair<int, int> x, pair<int, int> y) {
    return x.fi == y.fi ? make_pair(x.fi, x.se + y.se) : min(x, y);
  }
  pair<int, int> query(int L, int R, int u, int l, int r) {
    if (R < l || r < L) return {1e9, 0};
    if (L <= l && r <= R) return {mn[u], c[u]};
    int mid = (l + r) >> 1;
    pushdown(u, l, r);
    return merge(query(L, R, lc[u], l, mid), query(L, R, rc[u], mid + 1, r));
  }
  int query2(int L, int R, int u, int l, int r) {
    if (R < l || r < L) return 0;
    if (L <= l && r <= R) return mn[u] == 0 ? W(l, r) : (W(l, r) ^ v[u]);
    int mid = (l + r) >> 1;
    pushdown(u, l, r);
    return query2(L, R, lc[u], l, mid) ^ query2(L, R, rc[u], mid + 1, r);
  }
} // namespace sg1

int main() {
  scanf("%d%d%d", &k, &n, &m);

  struct atom {
    int a, b, c;
  };
  vector<atom> v[N];

  FOR(i, 1, k) {
    int a, b, c, d;
    scanf("%d%d%d%d", &a, &b, &c, &d);
    v[a].pb({b, d, 1});
    v[c + 1].pb({b, d, -1});
  }

  long long ans = 0;

  sg1::tot = 0;
  root = sg1::new_node(0, n);
  FOR(i, 1, n) {
    for (auto x : v[i]) sg1::add(x.a, x.b, x.c, root, 0, n);
    auto p = sg1::query(1, i, root, 0, n); // min , count
    int c = p.fi == 0 ? i - p.se : i;
    if (c & 1) ans ^= i & -i;
  }
  sg1::tot = 0;
  root = sg1::new_node(0, W - 1);
  FOR(i, 1, n) {
    for (auto x : v[i]) sg1::add(x.a, x.b, x.c, root, 0, W - 1);
    auto p = sg1::query2(i + 1, W - 1, root, 0, W - 1);
    ans ^= p;
  }

  printf("%lld\n", ans);

  return 0;
}
