// by Sshwy
//#define DEBUGGER
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

#ifdef DEBUGGER

#define log_prefix()                                                \
  {                                                                 \
    fprintf(stderr, "\033[37mLine %-3d [%dms]:\033[0m  ", __LINE__, \
        (int)clock() / 1000);                                       \
  }
#define ilog(...) \
  { fprintf(stderr, __VA_ARGS__); }
#define llog(...)      \
  {                    \
    log_prefix();      \
    ilog(__VA_ARGS__); \
  }
#define log(...)           \
  {                        \
    log_prefix();          \
    ilog(__VA_ARGS__);     \
    fprintf(stderr, "\n"); \
  }
#define red(...)       \
  {                    \
    log_prefix();      \
    ilog("\033[31m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define green(...)     \
  {                    \
    log_prefix();      \
    ilog("\033[32m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define blue(...)      \
  {                    \
    log_prefix();      \
    ilog("\033[34m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }

#else

#define log_prefix() ;
#define ilog(...) ;
#define llog(...) ;
#define log(...) ;
#define red(...) ;
#define green(...) ;
#define blue(...) ;

#endif

namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 5e4 + 5, P = 998244353;

int gcd(int a, int b) { return b ? gcd(b, a % b) : a; }
int n;
int a[N], ans[N];
struct atom {
  int a, b, c;
};
map<int, vector<atom>> S;

int c[N], C[N];
int c_range(int l, int r) { return (C[r] - (l == 0 ? 0 : C[l - 1]) + P) % P; }
void calc(const vector<atom> &v) {
  // for(atom x:v)printf("(%d %d %d) ",x.a,x.b,x.c);
  // puts("");
  FOR(x, 1, n) { // ans[x]
    FOR(i, 0, n) c[i] = C[i] = 0;
    c[0] = 1, C[0] = 1; //方案
    int pos = 0;
    FOR(i, 1, n) {
      if (i != x) c[i] = c[i - 1];
      while (pos < v.size() && v[pos].c == i) { //右端点
        c[i] %= P;
        if (x < v[pos].a)
          c[i] += c_range(v[pos].a - 1, v[pos].b - 1);
        else if (v[pos].a <= x && x <= v[pos].b)
          c[i] += c_range(v[pos].a - 1, v[pos].b - 1);
        else if (v[pos].b < x && x <= v[pos].c)
          c[i] += c_range(v[pos].a - 1, v[pos].b - 1);
        else
          c[i] += c_range(v[pos].a - 1, v[pos].b - 1);
        ++pos;
      }
      C[i] = (C[i - 1] + c[i]) % P;
      // printf("- %d %d\n",c[i],C[i]);
    }
    // printf("x=%d c[n]=%d\n",x,c[n]);
    // FOR(i,1,n)printf("%d%c",c[i]," \n"[i==n]);
    // FOR(i,1,n)printf("%d%c",C[i]," \n"[i==n]);
    ans[x] += c[n];
    ans[x] = ans[x] % P;
  }
}
int main() {
  scanf("%d", &n);
  FOR(i, 1, n) scanf("%d", &a[i]);

  FOR(i, 1, n) {
    int g = a[i], las = i; //当前gcd
    ROF(j, i - 1, 1) {
      int ng = gcd(g, a[j]);
      if (ng != g) {
        // printf("%d : %d %d %d\n",g,j+1,las,i);
        S[g].push_back({j + 1, las, i});
        g = ng, las = j;
      }
    }
    // printf("%d : %d %d %d\n",g,1,las,i);
    S[g].push_back({1, las, i});
  }

  for (auto &x : S) { calc(x.se); }

  FOR(i, 1, n) printf("%d%c", ans[i], " \n"[i == n]);
  return 0;
}

// O(log)判断是否存在以l为左端点的，gcd为x的区间（及其右端点的可行区间）
// w[i]被选中，则gcd是w[i]的约数。
// 枚举gcd。
// 预处理每个右端点的左端点区间（nlogn个）
// 把相同gcd的区间放一起搞
