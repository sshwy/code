// by SLshwy
//#define DEBUGGER
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

#ifdef DEBUGGER

#define log_prefix()                                                \
  {                                                                 \
    fprintf(stderr, "\033[37mLine %-3d [%dms]:\033[0m  ", __LINE__, \
        (int)clock() / 1000);                                       \
  }
#define ilog(...) \
  { fprintf(stderr, __VA_ARGS__); }
#define llog(...)      \
  {                    \
    log_prefix();      \
    ilog(__VA_ARGS__); \
  }
#define log(...)           \
  {                        \
    log_prefix();          \
    ilog(__VA_ARGS__);     \
    fprintf(stderr, "\n"); \
  }
#define red(...)       \
  {                    \
    log_prefix();      \
    ilog("\033[31m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define green(...)     \
  {                    \
    log_prefix();      \
    ilog("\033[32m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define blue(...)      \
  {                    \
    log_prefix();      \
    ilog("\033[34m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }

#else

#define log_prefix() ;
#define ilog(...) ;
#define llog(...) ;
#define log(...) ;
#define red(...) ;
#define green(...) ;
#define blue(...) ;

#endif

namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
//#define int long long
const int N = 5e4 + 5, P = 998244353, SZ = N << 2;

int gcd(int a, int b) { return b ? gcd(b, a % b) : a; }

int n;
int a[N], ans[N], tota;
struct atom {
  int a, b, c;
};
map<int, pair<vector<atom>, vector<atom>>> S;

struct seg {
  int s[SZ], tag[SZ], b[SZ], tim;
  seg() { tim = 0; }
  void clr(int u) {
    if (b[u] != tim) s[u] = tag[u] = 0;
    b[u] = tim;
  }
  void nodeadd(int u, int l, int r, int v) {
    clr(u);
    tag[u] = (tag[u] + v) % P;
    s[u] = (s[u] + (r - l + 1ll) * v) % P;
  }
  void pushdown(int u, int l, int r) {
    clr(u);
    int mid = (l + r) >> 1;
    if (tag[u])
      nodeadd(u << 1, l, mid, tag[u]), nodeadd(u << 1 | 1, mid + 1, r, tag[u]),
          tag[u] = 0;
  }
  void add(int L, int R, int v, int u = 1, int l = 0, int r = n + 1) {
    clr(u);
    if (R < l || r < L) return;
    if (L <= l && r <= R) return nodeadd(u, l, r, v), void();
    int mid = (l + r) >> 1;
    pushdown(u, l, r), add(L, R, v, u << 1, l, mid),
        add(L, R, v, u << 1 | 1, mid + 1, r);
    s[u] = (s[u << 1] + s[u << 1 | 1]) % P;
  }
  int query(int L, int R, int u = 1, int l = 0, int r = n + 1) {
    clr(u);
    if (R < l || r < L) return 0;
    if (L <= l && r <= R) return s[u];
    int mid = (l + r) >> 1;
    pushdown(u, l, r);
    return (query(L, R, u << 1, l, mid) + query(L, R, u << 1 | 1, mid + 1, r)) % P;
  }
} sgl, sgr;

void calc(const pair<vector<atom>, vector<atom>> &v) {

  sgl.tim++;
  sgl.add(0, n, 1);
  for (auto &x : v.fi) {
    int t = sgl.query(x.a - 1, x.b - 1);
    sgl.add(x.c, n, t);
  }

  sgr.tim++;
  sgr.add(1, n + 1, 1);
  for (auto &x : v.se) {
    int t = sgr.query(x.b + 1, x.c + 1);
    sgr.add(1, x.a, t);
  }

  int tot = sgl.query(n, n);
  // assert(tot==sgr.query(1,1));
  tota = (tota + tot) % P;

  vector<int> u = {1, n};
  for (auto &x : v.fi) u.pb(x.c);
  for (auto &x : v.se) u.pb(x.a);
  sort(u.begin(), u.end());
  u.erase(unique(u.begin(), u.end()), u.end());

  for (int x : u) {
    int t = sgl.query(x - 1, x - 1) * 1ll * sgr.query(x + 1, x + 1) % P;
    ans[x] = (ans[x] - t + P) % P, ans[x + 1] = (ans[x + 1] + t) % P;
  }
  for (long unsigned i = 1; i < u.size(); i++)
    if (u[i - 1] + 1 <= u[i] - 1) {
      int L = u[i - 1] + 1, R = u[i] - 1;
      int t = sgl.query(L - 1, L - 1) * 1ll * sgr.query(R + 1, R + 1) % P;
      ans[L] = (ans[L] - t + P) % P, ans[R + 1] = (ans[R + 1] + t) % P;
    }
}
signed main() {
  scanf("%d", &n);
  FOR(i, 1, n) scanf("%d", &a[i]);

  vector<pair<int, int>> v;
  FOR(i, 1, n) {
    for (auto &x : v) x.se = gcd(x.se, a[i]);
    v.push_back({i, a[i]});
    for (long unsigned x = 1; x < v.size(); ++x)
      if (v[x - 1].se == v[x].se) v.erase(v.begin() + x), --x;
    int las = i;
    // assert(v.size());
    for (int x = v.size() - 1; x >= 0; --x) {
      S[v[x].se].fi.pb({v[x].fi, las, i});
      las = v[x].fi - 1;
    }
  }

  v.clear();
  ROF(i, n, 1) {
    for (auto &x : v) x.se = gcd(x.se, a[i]);
    v.push_back({i, a[i]});
    for (long unsigned x = 1; x < v.size(); ++x)
      if (v[x - 1].se == v[x].se) v.erase(v.begin() + x), --x;
    int las = i;
    // assert(v.size());
    for (int x = v.size() - 1; x >= 0; --x) {
      S[v[x].se].se.pb({i, las, v[x].fi});
      las = v[x].fi + 1;
    }
  }

  for (auto &x : S) calc(x.se);

  FOR(i, 1, n) ans[i] = (ans[i] + ans[i - 1]) % P;
  FOR(i, 1, n) ans[i] = (tota + ans[i]) % P;
  FOR(i, 1, n) printf("%d%c", ans[i], " \n"[i == n]);
  return 0;
}

// O(log)判断是否存在以l为左端点的，gcd为x的区间（及其右端点的可行区间）
// w[i]被选中，则gcd是w[i]的约数。
// 枚举gcd。
// 预处理每个右端点的左端点区间（nlogn个）
// 把相同gcd的区间放一起搞
