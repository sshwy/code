// by Sshwy
#define DEBUGGER
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

#ifdef DEBUGGER

#define log_prefix()                                                \
  {                                                                 \
    fprintf(stderr, "\033[37mLine %-3d [%dms]:\033[0m  ", __LINE__, \
        (int)clock() / 1000);                                       \
  }
#define ilog(...) \
  { fprintf(stderr, __VA_ARGS__); }
#define llog(...)      \
  {                    \
    log_prefix();      \
    ilog(__VA_ARGS__); \
  }
#define log(...)           \
  {                        \
    log_prefix();          \
    ilog(__VA_ARGS__);     \
    fprintf(stderr, "\n"); \
  }
#define red(...)       \
  {                    \
    log_prefix();      \
    ilog("\033[31m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define green(...)     \
  {                    \
    log_prefix();      \
    ilog("\033[32m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define blue(...)      \
  {                    \
    log_prefix();      \
    ilog("\033[34m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }

#else

#define log_prefix() ;
#define ilog(...) ;
#define llog(...) ;
#define log(...) ;
#define red(...) ;
#define green(...) ;
#define blue(...) ;

#endif

namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 505, P = 1e9 + 7;
int pw(int a, int m) {
  int res = 1;
  while (m) m & 1 ? res = 1ll * res * a % P : 0, a = 1ll * a * a % P, m >>= 1;
  return res;
}

int n, fa[N], sz[N], dep[N], fac[N], fnv[N];
vector<int> g[N];
void dfs(int u, int p) {
  fa[u] = p, sz[u] = 1, dep[u] = dep[p] + 1;
  for (int v : g[u])
    if (v != p) dfs(v, u), sz[u] += sz[v];
}

int lca(int u, int v) {
  while (u != v) {
    if (dep[u] < dep[v]) swap(u, v);
    u = fa[u];
  }
  return u;
}
int binom(int n, int m) {
  if (n < m) return 0;
  return fac[n] * 1ll * fnv[m] % P * fnv[n - m] % P;
}
long long tot;
long long h[N][N];
long long Inv(int x) {
  assert(x <= n);
  return fnv[x] * 1ll * fac[x - 1] % P;
}
int calc(int u, int v) { // u,v出现逆序对的方案数。u<v, u --> v
  assert(u < v);

  int z = lca(u, v);
  if (z == u)
    return tot;
  else if (z == v)
    return 0;

  green("calc %d %d", u, v);

  vector<int> A(1, 0), B(1, 0);
  long long coef = 1;
  for (int x = u; x != z; x = fa[x])
    A.pb(x), coef = coef * sz[x] % P; // u的祖先，包括u
  for (int x = fa[v]; x != z; x = fa[x])
    B.pb(x), coef = coef * sz[x] % P; // v的祖先，不包括v
  // for(int x:A)ilog("%d ",x); ilog("\n"); for(int x:B)ilog("%d ",x);
  // ilog("\n");

  int la = A.size() - 1, lb = B.size() - 1;
  vector<long long> fA(1, 1), fB(1, 1);
  FOR(i, 1, la) fA.pb(fA.back() * Inv(sz[A[i]] + sz[v]) % P);
  FOR(i, 1, lb) fB.pb(fB.back() * Inv(sz[B[i]]) % P);

  reverse(A.begin() + 1, A.end());
  reverse(B.begin() + 1, B.end());

  memset(h, 0, sizeof(h));
  h[1][1] = 1;
  FOR(i, 1, la) FOR(j, 1, lb) {
    h[i + 1][j] = (h[i + 1][j] + h[i][j] * Inv(sz[A[i]] + sz[B[j]])) % P;
    h[i][j + 1] = (h[i][j + 1] + h[i][j] * Inv(sz[A[i]] + sz[B[j]])) % P;
  }
  // FOR(i,0,la+1)FOR(j,0,lb+1){ ilog("%-10lld%c",h[i][j]," \n"[j==lb+1]); }
  int x1 = 0;
  log("%d %d", la, lb);
  FOR(i, 1, la) x1 = (x1 + fA[i] * h[la + 1 - i][lb + 1]) % P;
  FOR(i, 1, lb) x1 = (x1 + fB[i] * h[la + 1][lb + 1 - i]) % P;

  coef = x1 * coef % P;
  long long t = tot * coef % P;
  log("x1 %d", x1);
  log("t %lld", t);
  log("tot %lld", tot);
  return t;
}

long long ans = 0;
int main() {
  scanf("%d", &n);

  fac[0] = 1;
  FOR(i, 1, n) fac[i] = 1ll * fac[i - 1] * i % P;
  fnv[n] = pw(fac[n], P - 2);
  ROF(i, n, 1) fnv[i - 1] = 1ll * fnv[i] * i % P;

  FOR(i, 1, n - 1) {
    int u, v;
    scanf("%d%d", &u, &v);
    g[u].pb(v);
    g[v].pb(u);
  }
  dfs(1, 0);

  tot = fac[n];
  FOR(i, 1, n) tot = tot * pw(sz[i], P - 2) % P;
  // log("tot=%lld",tot);

  FOR(i, 1, n) FOR(j, i + 1, n) {
    int p = calc(i, j); // log("%d %d reverse: %d\n",i,j,p);
    ans = (ans + p) % P;
  }

  printf("%lld\n", ans);

  return 0;
}
