// by Sshwy
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

int mex(set<int> &s) {
  int res = 0;
  while (s.find(res) != s.end()) ++res;
  return res;
}
int _sg[101000], vis[101000];
int sg(const int x) {
  if (x == 1) return 0;
  if (vis[x]) return _sg[x];
  set<int> s;
  for (int i = 1; i * i <= x; ++i) {
    if (x % i) continue;
    s.insert(sg(i) * ((x / i) & 1));
    if (i > 1 && i * i < x) s.insert(sg(x / i) * (i & 1));
  }
  vis[x] = 1;
  return _sg[x] = mex(s);
}
map<pair<int, int>, int> mp;
int sg(const int w, const int h) {
  if (mp.count({w, h})) return mp[{w, h}];
  if (w == 1 && h == 1) return 0;
  set<int> s;
  if (w > 1)
    for (int i = 1; i * i <= w; ++i) {
      if (w % i) continue;
      assert(i < w);
      s.insert(sg(i, h) * ((w / i) & 1));
      if (i > 1 && i * i < w) s.insert(sg(w / i, h) * (i & 1));
    }
  if (h > 1)
    for (int i = 1; i * i <= h; ++i) {
      if (h % i) continue;
      assert(i < h);
      s.insert(sg(w, i) * ((h / i) & 1));
      if (i > 1 && i * i < h) s.insert(sg(w, h / i) * (i & 1));
    }
  return mp[{w, h}] = mex(s);
}

int main() {
  // FOR(i,1,100000)printf("sg(%d)=%d\n",i,sg(i));
  FOR(i, 1, 50) {
    FOR(j, 1, 50) { printf("%d ", sg(i, j)); }
    puts("");
  }

  return 0;
}
