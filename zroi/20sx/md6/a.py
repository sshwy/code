def f(x):
    if x==1:
        return 0
    res=0
    for i in range(2,x+1):
        if x%i==0:
            res=max(res,f(x//i)*i+1)
    return res

for i in range(1,100):
    print("%d = %d" % (i,f(i)));
