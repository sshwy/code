// by Sshwy
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA
const int N = 1e5 + 5, W = 1e5 + 5;
int n;
int a[N], b[N], w[N], h[N];
int f[N];
int mex(set<int> &s) {
  int res = 0;
  while (s.find(res) != s.end()) ++res;
  return res;
}
map<pair<int, int>, int> mp;
vector<int> dn[N];
int sg(const int w, const int h) {
  if (mp.count({w, h})) return mp[{w, h}];
  if (w == 1 && h == 1) return 0;
  set<int> s;
  for (auto i : dn[w]) {
    if (i < w) s.insert(sg(i, h) * ((w / i) & 1));
  }
  for (auto i : dn[h]) {
    if (i < h) s.insert(sg(w, i) * ((h / i) & 1));
  }
  return mp[{w, h}] = mex(s);
}
int c[N];
void add(int pos, int v) {
  for (int i = pos; i < N; i += i & -i) c[i] ^= v;
}
int pre(int pos) {
  int res = 0;
  for (int i = pos; i > 0; i -= i & -i) res ^= c[i];
  return res;
}
int main() {
  FOR(i, 1, 100000) {
    int lim = 100000 / i;
    FOR(j, 1, lim) { dn[i * j].pb(i); }
  }

  scanf("%d", &n);
  FOR(i, 1, n) scanf("%d%d%d%d", &a[i], &b[i], &w[i], &h[i]);
  FOR(i, 1, n) {
    // a=1 && b=1
    f[i] = sg(w[i], h[i]);
    add(i, f[i]);
    printf("f[%d]=%d\n", i, f[i]);
  }
  int q;
  scanf("%d", &q);
  FOR(i, 1, q) {
    int l, r;
    scanf("%d%d", &l, &r);
    int t = pre(r) ^ pre(l - 1);
    if (t)
      puts("Yes");
    else
      puts("No");
  }
  return 0;
}
