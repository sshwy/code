// by Sshwy
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA
const int N = 1e5 + 5, P = 998244353, I2 = (P + 1) / 2;
long long n, a[N], k;
long long b[N], sb[N], lb;
long long st_mn[N][20], st_mx[N][20], lg[N];
long long c[N];
void add(int pos, long long v) {
  for (int i = pos; i < N; i += i & -i) c[i] += v;
}
long long pre(int pos) {
  long long res = 0;
  for (int i = pos; i > 0; i -= i & -i) res += c[i];
  return res;
}
long long qb(int L, int R) { return pre(R) - pre(L - 1); }
long long qry_mn(int l, int r) {
  int j = lg[r - l + 1];
  assert((1 << j) <= r - l + 1);
  int x = st_mn[l][j], y = st_mn[r - (1 << j) + 1][j];
  assert(l <= x && x <= r && l <= y && y <= r);
  return sb[x] < sb[y] ? x : y;
}
long long qry_mx(int l, int r) {
  int j = lg[r - l + 1];
  int x = st_mx[l][j], y = st_mx[r - (1 << j) + 1][j];
  return sb[x] > sb[y] ? x : y;
}
map<long long, int> d;
inline void add_d(long long pos, int v) {
  // printf("add_d %lld %d\n",pos,v);
  if (d.count(pos))
    d[pos] += v;
  else
    d[pos] = v;
}
void dfs(int L, int R) {
  if (L > R) return;
  // printf("dfs %d %d\n",L,R);
  if (qb(L, L) <= 0) {
    dfs(L + 1, R);
    return;
  }
  if (L == R) {
    long long t = qb(L, L);
    add_d(0, 1);
    add_d(t, -1);
    return;
  }
  int mid = qry_mn(L, R);
  // printf("qry_mn %d %d\n",L,R);
  // printf("mid %d\n",mid);
  long long t = qb(L, mid);
  // printf("qb(%d,%d)=%lld\n",L,mid,t);
  assert(qb(mid, mid) <= 0);
  if (t <= 0) {
    dfs(L, mid - 1);
    dfs(mid + 1, R);
    return;
  }
  long long val = qry_mx(L, R);
  val = qb(L, val);
  // val -> val-t
  add_d(val - t, 1);
  add_d(val, -1);
  assert(qb(L, L) >= t);
  add(L, -t);
  dfs(L, mid - 1);
  dfs(mid + 1, R);
}
int main() {
  scanf("%lld", &n);
  FOR(i, 1, n) scanf("%lld", &a[i]);
  scanf("%lld", &k);
  b[1] = a[1], lb = 1;
  FOR(i, 2, n) {
    if ((a[i - 1] < 0) == (a[i] < 0))
      b[lb] += a[i];
    else
      b[++lb] = a[i];
  }
  // FOR(i,1,lb)printf("%lld%c",b[i]," \n"[i==lb]);
  FOR(i, 1, lb) sb[i] = sb[i - 1] + b[i];

  FOR(i, 1, lb) st_mn[i][0] = i, st_mx[i][0] = i;
  lg[1] = 0;
  FOR(i, 2, lb) lg[i] = lg[i >> 1] + 1;
  FOR(j, 1, 19) {
    FOR(i, 1, lb - (1 << j) + 1) {
      int x = st_mn[i][j - 1], y = st_mn[i + (1 << j - 1)][j - 1];
      st_mn[i][j] = sb[x] < sb[y] ? x : y;
      x = st_mx[i][j - 1], y = st_mx[i + (1 << j - 1)][j - 1];
      st_mx[i][j] = sb[x] > sb[y] ? x : y;
    }
  }
  FOR(i, 1, lb) add(i, b[i]);
  dfs(1, lb);

  FOR(i, 1, n) a[i] = min(a[i], 0ll);
  sort(a + 1, a + n + 1);
  reverse(a + 1, a + n + 1);
  vector<pair<long long, int>> va;
  for (int i = 1, cur = 0; i <= n; i++) {
    ++cur;
    if (i == n || a[i] != a[i + 1]) va.pb({a[i], cur}), cur = 0;
  }
  // for(auto p:va)printf("(%lld,%d) ",p.first,p.second); puts("");
  assert(va.size());
  add_d(va[0].first, 0);
  int ps = 0;
  va.pb({-1e10, 0});
  for (int i = 0; i + 1 < va.size(); ++i) {
    ps += va[i].second;
    add_d(va[i + 1].first, ps);
    add_d(va[i].first, -ps);
  }

  vector<pair<long long, int>> v;
  for (auto p : d) {
    if (p.second) v.pb(p);
  }
  for (int i = 1; i < v.size(); ++i) v[i].second += v[i - 1].second;
  // for(auto p:v)printf("%-13lld ",p.first); puts("");
  // for(auto p:v)printf("%-13d ",p.second); puts("");
  long long ans = 0;
  for (int i = int(v.size()) - 1; i > 0; --i) {
    long long L = v[i - 1].first, R = v[i].first - 1, step = v[i - 1].second;
    if ((v[i].first - v[i - 1].first) * v[i - 1].second <= k) {
      long long vL = ((L + 1) % P * (step - 1) + L) % P;
      long long vR = ((R + 1) % P * (step - 1) + R) % P;
      long long t = (vL + vR) % P * (R % P - L % P + 1) % P * I2 % P;
      ans = (ans + t) % P;
      k -= (v[i].first - v[i - 1].first) * v[i - 1].second;
    } else {
      long long len = k / step, rest = k % step;
      // printf("len %lld\n",len);
      L = R - len + 1;
      long long vL = ((L + 1) % P * (step - 1) + L) % P;
      long long vR = ((R + 1) % P * (step - 1) + R) % P;
      long long t = (vL + vR) % P * (R % P - L % P + 1) % P * I2 % P;
      ans = (ans + t) % P;
      ans = (ans + L % P * rest) % P;
      k = 0;
      break;
    }
  }
  ans = (ans + P) % P;
  if (k == 0) {
    printf("%lld\n", ans);
    return 0;
  }
  return 0;
}
