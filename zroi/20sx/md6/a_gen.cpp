// by Sshwy
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

using namespace RA;

int main() {
  int n = 1e5;
  printf("%d\n", n);
  FOR(i, 1, n) printf("%d %d %d %d\n", 1, 1, rnd(1, 1e5), rnd(1, 1e5));
  int q = 1e5;
  printf("%d\n", q);
  FOR(i, 1, q) {
    int l = rnd(1, n), r = rnd(1, n);
    if (l > r) swap(l, r);
    printf("%d %d\n", l, r);
  }
  return 0;
}
