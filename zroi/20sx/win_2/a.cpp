// by Sshwy
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
namespace IO {
  const int BSZ = 100000;
  char nc() {
    static char bf[BSZ], *p1 = bf, *p2 = bf;
    return p1 == p2 && (p2 = (p1 = bf) + fread(bf, 1, BSZ, stdin), p1 == p2) ? EOF
                                                                             : *p1++;
  }
  int rd() {
    int res = 0;
    char c = nc();
    while (!isdigit(c)) c = nc();
    while (isdigit(c)) res = res * 10 + c - '0', c = nc();
    return res;
  }
  long long rdll() {
    long long res = 0;
    char c = nc();
    while (!isdigit(c)) c = nc();
    while (isdigit(c)) res = res * 10 + c - '0', c = nc();
    return res;
  }
  void rd(char *s) {
    char c = nc();
    while (!isblank(c) && isprint(c) && c != EOF) *s++ = c, c = nc();
    *s = 0;
  }
  int tmp[20], lt;
  char OBF[BSZ], *OBP = OBF;
  void flush() { fwrite(OBF, 1, OBP - OBF, stdout), OBP = OBF; }
  void wrch(char x) {
    if (OBP == OBF + BSZ - 1) flush();
    *OBP = x, ++OBP;
  }
  void wr(int x, int base = 10) {
    lt = 0;
    while (x) tmp[++lt] = x % base, x /= base;
    while (lt > 0) wrch(tmp[lt] + '0'), --lt;
  }
  void wr(long long x, int base = 10) {
    lt = 0;
    while (x) tmp[++lt] = x % base, x /= base;
    while (lt > 0) wrch(tmp[lt] + '0'), --lt;
  }
  void wr(const char *s) {
    while (*s) wrch(*s), ++s;
  }
  void wr(char x) { wrch(x); }
} // namespace IO
const int N = 1e6 + 5;

int n, P;

bool bp[N];
int pn[N], lp;
int h[N], g[N], f[N];

void go() {
  bp[0] = bp[1] = 1;
  h[1] = 1;
  FOR(i, 2, n) {
    if (!bp[i]) pn[++lp] = i, h[i] = 2;
    FOR(j, 1, lp) {
      int x = i * pn[j];
      if (x > n) break;
      bp[x] = 1;
      if (i % pn[j] == 0) {
        h[x] = h[i];
        break;
      } else {
        h[x] = h[i] * 2 % P;
      }
    }
  }
  FOR(d, 2, n) {
    for (int i = d; i <= n; i += d) { g[i] = (g[i] + h[d - 1]) % P; }
  }
  FOR(i, 2, n) f[i] = (f[i - 1] + g[i - 1]) % P;
  FOR(i, 1, n) f[i] = (f[i] + f[i - 1]) % P;
}
int main() {
  int t;
  t = IO::rd();
  P = IO::rd();
  n = 1e6;
  go();
  FOR(i, 1, t) {
    int k;
    k = IO::rd();
    int ans = (k * 1ll * k % P * k % P - f[k]) % P;
    ans = (ans + P) % P;
    IO::wr(ans);
    IO::wr('\n');
  }
  IO::flush();
  return 0;
}
