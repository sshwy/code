// by Sshwy
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
namespace IO {
  const int BSZ = 100000;
  char nc() {
    static char bf[BSZ], *p1 = bf, *p2 = bf;
    return p1 == p2 && (p2 = (p1 = bf) + fread(bf, 1, BSZ, stdin), p1 == p2) ? EOF
                                                                             : *p1++;
  }
  int rd() {
    int res = 0;
    char c = nc();
    while (!isdigit(c)) c = nc();
    while (isdigit(c)) res = res * 10 + c - '0', c = nc();
    return res;
  }
  long long rdll() {
    long long res = 0;
    char c = nc();
    while (!isdigit(c)) c = nc();
    while (isdigit(c)) res = res * 10 + c - '0', c = nc();
    return res;
  }
  void rd(char *s) {
    char c = nc();
    while (!isblank(c) && isprint(c) && c != EOF) *s++ = c, c = nc();
    *s = 0;
  }
  int tmp[20], lt;
  char OBF[BSZ], *OBP = OBF;
  void flush() { fwrite(OBF, 1, OBP - OBF, stdout), OBP = OBF; }
  void wrch(char x) {
    if (OBP == OBF + BSZ - 1) flush();
    *OBP = x, ++OBP;
  }
  void wr(int x, int base = 10) {
    lt = 0;
    while (x) tmp[++lt] = x % base, x /= base;
    while (lt > 0) wrch(tmp[lt] + '0'), --lt;
  }
  void wr(long long x, int base = 10) {
    lt = 0;
    while (x) tmp[++lt] = x % base, x /= base;
    while (lt > 0) wrch(tmp[lt] + '0'), --lt;
  }
  void wr(const char *s) {
    while (*s) wrch(*s), ++s;
  }
  void wr(char x) { wrch(x); }
} // namespace IO
/******************heading******************/
int xxx;
const int P = 998244353, N = 3e4 + 5, MTX = 4;

int n, Q;
int a[N], X;
int f[N][2][2];

int C[30];

struct Matrix {
  int w, h;
  int c[MTX][MTX];
  Matrix() {}
  Matrix(int _h, int _w) {
    w = _w, h = _h;
    // FOR(i,0,h-1)FOR(j,0,w-1)c[i][j]=0;
  }
  Matrix(int _h, int _w, int _c[MTX][MTX]) {
    w = _w, h = _h;
    FOR(i, 0, h - 1) FOR(j, 0, w - 1) c[i][j] = _c[i][j];
  }
  Matrix operator*(const Matrix &m) {
    assert(w == m.h);
    Matrix res(h, m.w);
    long long R[MTX][MTX] = {{0}}, *p1, t;
    const int *p2;
    FOR(i, 0, h - 1) {
      FOR(k, 0, w - 1) {
        t = c[i][k];
        p1 = R[i], p2 = m.c[k];
        // assert(m.w==4);
        *p1 += t * (*p2), ++p1, ++p2;
        *p1 += t * (*p2), ++p1, ++p2;
        *p1 += t * (*p2), ++p1, ++p2;
        *p1 += t * (*p2), ++p1, ++p2;
      }
    }
    // FOR(i,0,h-1) FOR(k,0,w-1) FOR(j,0,m.w-1) {
    //    R[i][j]+=c[i][k]*1ll*m.c[k][j];
    //    ++xxx;
    //}
    FOR(i, 0, h - 1) FOR(j, 0, m.w - 1) res.c[i][j] = R[i][j] % P; //,++xxx;
    return res;
  }
};

struct Seg {
  Matrix M[N << 2], O[N];
  void build(int u = 1, int l = 1, int r = n) {
    if (l == r) return M[u] = O[l], void();
    int mid = (l + r) >> 1;
    build(u << 1, l, mid);
    build(u << 1 | 1, mid + 1, r);
    M[u] = M[u << 1] * M[u << 1 | 1];
  }
  void assign(int x, const Matrix &m, int u = 1, int l = 1, int r = n) {
    if (l == r) {
      M[u] = m;
      return;
    }
    int mid = (l + r) >> 1;
    if (x <= mid)
      assign(x, m, u << 1, l, mid);
    else
      assign(x, m, u << 1 | 1, mid + 1, r);
    M[u] = M[u << 1] * M[u << 1 | 1];
  }
} seg[29];

void assign(int x, int y) {
  FOR(k, 0, 28) if (a[x] >> k & 1) C[k]--;
  X ^= a[x];
  a[x] = y;
  X ^= a[x];
  FOR(k, 0, 28) if (a[x] >> k & 1) C[k]++;
  FOR(k, 0, 28) {
    int v = (a[x] & ((1 << k) - 1)) + 1, v2 = 1 << k;
    if (a[x] >> k & 1) {
      int m[MTX][MTX] = {0, 1, v, 0, 0, v2, 0, v, v, 0, 0, 1, 0, v, 0, v2};
      seg[k].assign(x, Matrix(4, 4, m));
    } else {
      int m[MTX][MTX] = {v, 0, 0, 0, 0, v, 0, 0, 0, 0, v, 0, 0, 0, 0, v};
      seg[k].assign(x, Matrix(4, 4, m));
    }
  }
}
void Oassign(int x, int y) {
  FOR(k, 0, 28) if (a[x] >> k & 1) C[k]--;
  X ^= a[x];
  a[x] = y;
  X ^= a[x];
  FOR(k, 0, 28) if (a[x] >> k & 1) C[k]++;
  FOR(k, 0, 28) {
    int v = (a[x] & ((1 << k) - 1)) + 1, v2 = 1 << k;
    if (a[x] >> k & 1) {
      int m[MTX][MTX] = {0, 1, v, 0, 0, v2, 0, v, v, 0, 0, 1, 0, v, 0, v2};
      seg[k].O[x] = Matrix(4, 4, m);
    } else {
      int m[MTX][MTX] = {v, 0, 0, 0, 0, v, 0, 0, 0, 0, v, 0, 0, 0, 0, v};
      seg[k].O[x] = Matrix(4, 4, m);
    }
  }
}
int calc(int k) {
  int f[MTX][MTX] = {1, 0, 0, 0};
  Matrix Mf(1, 4, f);
  Mf = Mf * seg[k].M[1];
  int res = Mf.c[0][(X >> k & 1) * 2 + 1];
  return res;
}
int query() {
  int res = 1; // a[i]=b[i]
  FOR(k, 0, 28) res = (res + calc(k)) % P;
  return res;
}
int main() {
  n = IO::rd(), Q = IO::rd();
  FOR(i, 1, n) {
    int x;
    x = IO::rd();
    Oassign(i, x);
  }
  FOR(k, 0, 28) seg[k].build();
  FOR(i, 1, Q) {
    int x, y;
    x = IO::rd();
    y = IO::rd();
    assign(x, y);
    IO::wr(query());
    IO::wr('\n');
  }
  IO::flush();
  // fprintf(stderr,"xxx=%d\n",xxx);
  return 0;
}
