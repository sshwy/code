// by Sshwy
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
namespace IO {
  const int BSZ = 100000;
  char nc() {
    static char bf[BSZ], *p1 = bf, *p2 = bf;
    return p1 == p2 && (p2 = (p1 = bf) + fread(bf, 1, BSZ, stdin), p1 == p2) ? EOF
                                                                             : *p1++;
  }
  int rd() {
    int res = 0;
    char c = nc();
    while (!isdigit(c)) c = nc();
    while (isdigit(c)) res = res * 10 + c - '0', c = nc();
    return res;
  }
  long long rdll() {
    long long res = 0;
    char c = nc();
    while (!isdigit(c)) c = nc();
    while (isdigit(c)) res = res * 10 + c - '0', c = nc();
    return res;
  }
  void rd(char *s) {
    char c = nc();
    while (!isblank(c) && isprint(c) && c != EOF) *s++ = c, c = nc();
    *s = 0;
  }
  int tmp[20], lt;
  char OBF[BSZ], *OBP = OBF;
  void flush() { fwrite(OBF, 1, OBP - OBF, stdout), OBP = OBF; }
  void wrch(char x) {
    if (OBP == OBF + BSZ - 1) flush();
    *OBP = x, ++OBP;
  }
  void wr(int x, int base = 10) {
    lt = 0;
    while (x) tmp[++lt] = x % base, x /= base;
    while (lt > 0) wrch(tmp[lt] + '0'), --lt;
  }
  void wr(long long x, int base = 10) {
    lt = 0;
    while (x) tmp[++lt] = x % base, x /= base;
    while (lt > 0) wrch(tmp[lt] + '0'), --lt;
  }
  void wr(const char *s) {
    while (*s) wrch(*s), ++s;
  }
  void wr(char x) { wrch(x); }
} // namespace IO
/*}}}*/
/******************heading******************/
const int N = 1e5 + 5, P = 998244353;
#define int long long

int n;

struct rag {
  int a, b, c, d;
  void read() { a = IO::rd(), b = IO::rd(), c = IO::rd(), d = IO::rd(); }
  rag() {}
  rag(int A, int B, int C, int D) { a = A, b = B, c = C, d = D; }
  rag null() { return rag(1, 1, 0, 0); }
  long long size() { return (c - a + 1ll) * (d - b + 1ll) % P; }
  rag operator&(rag R) {
    if (c < R.a || R.c < a || d < R.b || R.d < b) return null();
    return rag(max(a, R.a), max(b, R.b), min(c, R.c), min(d, R.d));
  }
} a[N];

int f[N];
int go() {
  f[1] = 1;
  FOR(i, 2, n) {
    FOR(j, 1, i - 1) { f[i] = (f[i] + f[j] * 1ll * (a[i] & a[j]).size() % P) % P; }
    f[i] = (f[i] + 1) % P;
  }
  int ans = 0;
  FOR(i, 1, n) ans = (ans + f[i]) % P;
  ans = (ans + P) % P;
  return ans;
}
signed main() {
  n = IO::rd();
  FOR(i, 1, n) a[i].read();
  IO::wr(go());
  IO::wr('\n');
  IO::flush();
  return 0;
}
