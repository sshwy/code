// by Sshwy
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/

using namespace RA;

int main() {
  srand(time(0) + clock());
  int n = r(1, 3);
  printf("%d\n", n);
  int LIM = 1e9;
  FOR(i, 1, n) {
    int k[] = {r(1, LIM), r(1, LIM), r(1, LIM), r(1, LIM)};
    sort(k, k + 2);
    sort(k + 2, k + 4);
    printf("%d %d %d %d\n", k[0], k[2], k[1], k[3]);
  }
  return 0;
}
