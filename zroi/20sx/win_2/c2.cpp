// by Sshwy
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int P = 998244353, N = 3e4 + 5;

int n, Q;
int a[N], X;
int f[N][2][2];

int calc(int k) {
  memset(f, 0, sizeof(f[0]) * (n + 1));
  f[0][0][0] = 1;
  // printf("k=%d\n",k);
  FOR(i, 1, n) {
    // FOR(x,0,1)FOR(y,0,1)printf("%d ",f[i-1][x][y]); puts("");
    int v = (a[i] & ((1 << k) - 1)) + 1, v2 = 1 << k;
    // printf("v=%d,a[i][k]=%d\n",v,a[i]>>k&1);
    if (a[i] >> k & 1) {
      // a[i]_k=b[i]_k=1
      FOR(x, 0, 1) FOR(y, 0, 1) {
        f[i][x ^ 1][y] += f[i - 1][x][y] * 1ll * v % P;
        f[i][x ^ 1][y] %= P;
      }
      // a[i]_k=1,=b[i]_k=0
      FOR(x, 0, 1) FOR(y, 0, 1) {
        f[i][x][y | 1] += f[i - 1][x][y] * 1ll * (y ? v2 : 1) % P;
        f[i][x][y | 1] %= P;
      }
    } else {
      // a[i]_k=b[i]_k=0
      FOR(x, 0, 1) FOR(y, 0, 1) { f[i][x][y] = f[i - 1][x][y] * 1ll * v % P; }
    }
  }
  // printf("calc(%d): ",k);    FOR(x,0,1)FOR(y,0,1)printf("%d ",f[n][x][y]);
  // puts(""); printf("f[%d,%d,%d]=%d\n",n,X>>k&1,1,f[n][X>>k&1][1]);
  // printf("x=%d\n",X>>k&1);
  return f[n][X >> k & 1][1];
}
int query() {
  // printf("X=%d ",X);
  // FOR(i,1,n)printf("%d%c",a[i]," \n"[i==n]);
  int res = 1; // a[i]=b[i]
  FOR(k, 0, 29) res = (res + calc(k)) % P;
  return res;
}
int main() {
  freopen("c.in", "r", stdin);
  scanf("%d%d", &n, &Q);
  FOR(i, 1, n) {
    scanf("%d", &a[i]);
    X ^= a[i];
  }
  FOR(i, 1, Q) {
    int x, y;
    scanf("%d%d", &x, &y);
    X ^= a[x];
    a[x] = y;
    X ^= a[x];
    printf("%d\n", query());
  }
  return 0;
}
