// by Sshwy
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
namespace IO {
  const int BSZ = 100000;
  char nc() {
    static char bf[BSZ], *p1 = bf, *p2 = bf;
    return p1 == p2 && (p2 = (p1 = bf) + fread(bf, 1, BSZ, stdin), p1 == p2) ? EOF
                                                                             : *p1++;
  }
  int rd() {
    int res = 0;
    char c = nc();
    while (!isdigit(c)) c = nc();
    while (isdigit(c)) res = res * 10 + c - '0', c = nc();
    return res;
  }
  long long rdll() {
    long long res = 0;
    char c = nc();
    while (!isdigit(c)) c = nc();
    while (isdigit(c)) res = res * 10 + c - '0', c = nc();
    return res;
  }
  void rd(char *s) {
    char c = nc();
    while (!isblank(c) && isprint(c) && c != EOF) *s++ = c, c = nc();
    *s = 0;
  }
  int tmp[20], lt;
  char OBF[BSZ], *OBP = OBF;
  void flush() { fwrite(OBF, 1, OBP - OBF, stdout), OBP = OBF; }
  void wrch(char x) {
    if (OBP == OBF + BSZ - 1) flush();
    *OBP = x, ++OBP;
  }
  void wr(int x, int base = 10) {
    lt = 0;
    while (x) tmp[++lt] = x % base, x /= base;
    while (lt > 0) wrch(tmp[lt] + '0'), --lt;
  }
  void wr(long long x, int base = 10) {
    lt = 0;
    while (x) tmp[++lt] = x % base, x /= base;
    while (lt > 0) wrch(tmp[lt] + '0'), --lt;
  }
  void wr(const char *s) {
    while (*s) wrch(*s), ++s;
  }
  void wr(char x) { wrch(x); }
} // namespace IO
/*}}}*/
/******************heading******************/
const int N = 1e5 + 5, P = 998244353;

int D1[N * 2], D2[N * 2], d1, d2;

int n;

int f1(int v) { return lower_bound(D1 + 1, D1 + d1 + 1, v) - D1; }
int f2(int v) { return lower_bound(D2 + 1, D2 + d2 + 1, v) - D2; }

struct rag {
  int a, b, c, d;
  void read() {
    a = IO::rd(), b = IO::rd(), c = IO::rd(), d = IO::rd();
    ++c, ++d;
    D1[++d1] = a, D1[++d1] = c;
    D2[++d2] = b, D2[++d2] = d;
  }
  void trans() {
    a = f1(a), c = f1(c);
    b = f2(b), d = f2(d);
    --c, --d;
    //[a,c],[b,d]
  }
  rag() {}
  rag(int A, int B, int C, int D) { a = A, b = B, c = C, d = D; }
  rag null() { return rag(1, 1, 0, 0); }
  long long size() { return (c - a + 1ll) * (d - b + 1ll); }
  rag operator&(rag R) {
    if (c < R.a || R.c < a || d < R.b || R.d < b) return null();
    return rag(max(a, R.a), max(b, R.b), min(c, R.c), min(d, R.d));
  }
} a[N];

struct op {
  int typ, a, b, c, d;
} O[N * 10];
int lo;

int f[N];

void work(int l, int mid, int r) {
  vector<op> v;
  FOR(i, l, mid) {
    v.pb({1, a[i].a, a[i].b, 1, i});
    v.pb({1, a[i].c + 1, a[i].b, -1, i});
    v.pb({1, a[i].a, a[i].d + 1, -1, i});
    v.pb({1, a[i].c + 1, a[i].d + 1, 1, i});
  }
  FOR(i, mid + 1, r) {
    v.pb({0, a[i].b, a[i].d, 1, i});
    v.pb({0, a[i].a - 1, a[i].d, -1, i});
    v.pb({0, a[i].b, a[i].c - 1, -1, i});
    v.pb({0, a[i].a - 1, a[i].c - 1, 1, i});
  }
  sort(v.begin(), v.end(),
      [](op x, op y) { return x.b != y.b ? x.b < y.b : x.typ > y.typ; });
  for (op x : v) {
    if (x.typ) { //修改
      B1.add(x.a, d1, )
    } else {
    }
  }
}
void solve(int l, int r) {
  if (l == r) {
    f[l] = (f[l] + 1) % P;
    return;
  }
  int mid = (l + r) >> 1;
  solve(l, mid);
  work(l, mid, r);
  solve(mid + 1, r);
}

int main() {
  n = IO::rd();
  FOR(i, 1, n) a[i].read();

  sort(D1 + 1, D1 + d1 + 1);
  d1 = unique(D1 + 1, D1 + d1 + 1) - D1 - 1;
  sort(D2 + 1, D2 + d2 + 1);
  d2 = unique(D2 + 1, D2 + d2 + 1) - D2 - 1;

  D1[d1 + 1] = D1[d1] + 1;
  D2[d2 + 1] = D2[d2] + 1;

  // FOR(i,1,d1)printf("%d%c",D1[i]," \n"[i==d1]);
  // FOR(i,1,d2)printf("%d%c",D2[i]," \n"[i==d2]);

  FOR(i, 1, n) a[i].trans();

  solve(1, n);

  int ans = 0;
  FOR(i, 1, n) ans = (ans + f[i]) % P;

  IO::wr(ans);
  IO::wr('\n');
  IO::flush();
  return 0;
}
