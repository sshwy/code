// by Sshwy
//#define DEBUGGER
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

#ifdef DEBUGGER

#define log_prefix()                                                \
  {                                                                 \
    fprintf(stderr, "\033[37mLine %-3d [%dms]:\033[0m  ", __LINE__, \
        (int)clock() / 1000);                                       \
  }
#define ilog(...) \
  { fprintf(stderr, __VA_ARGS__); }
#define llog(...)      \
  {                    \
    log_prefix();      \
    ilog(__VA_ARGS__); \
  }
#define log(...)           \
  {                        \
    log_prefix();          \
    ilog(__VA_ARGS__);     \
    fprintf(stderr, "\n"); \
  }
#define red(...)       \
  {                    \
    log_prefix();      \
    ilog("\033[31m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define green(...)     \
  {                    \
    log_prefix();      \
    ilog("\033[32m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define blue(...)      \
  {                    \
    log_prefix();      \
    ilog("\033[34m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }

#else

#define log_prefix() ;
#define ilog(...) ;
#define llog(...) ;
#define log(...) ;
#define red(...) ;
#define green(...) ;
#define blue(...) ;

#endif

namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 5005, P = 1e9 + 7;
int n, k, sz[N];
long long f[N][N][2], t[N][2], F[N], G[N], fac[N], fnv[N];
vector<int> T[N];
// f[u,x,0/1]表示，u的子树，k个连通块，0/1表示u所在连通块是否选点，的方案数

void add(long long &x, long long y) { x = (x + y) % P; }
void dfs(int u, int p) {
  for (int v : T[u])
    if (v != p) dfs(v, u);
  f[u][1][0] = f[u][1][1] = 1;
  sz[u] = 1;
  for (int v : T[u])
    if (v != p) {
      FOR(i, 0, sz[u] + sz[v]) t[i][0] = t[i][1] = 0;
      FOR(i, 1, sz[u]) {
        FOR(j, 1, sz[v]) {
          add(t[i + j][0], f[u][i][0] * f[v][j][1]);
          add(t[i + j][1], f[u][i][1] * f[v][j][1]);
          add(t[i + j - 1][0], f[u][i][0] * f[v][j][0]);
          add(t[i + j - 1][1], f[u][i][1] * f[v][j][0]);
          add(t[i + j - 1][1], f[u][i][0] * f[v][j][1]);
        }
      }
      memcpy(f[u], t, sizeof(t[0]) * (sz[u] + sz[v] + 1));
      sz[u] += sz[v];
    }
}

long long pw(long long a, long long m) {
  long long res = 1;
  while (m) m & 1 ? res = res * a % P : 0, a = a * a % P, m >>= 1;
  return res;
}
long long binom(long long a, long long b) {
  if (a < b) return 0;
  return fac[a] * fnv[b] % P * fnv[a - b] % P;
}
int main() {
  scanf("%d%d", &n, &k);
  FOR(i, 1, n - 1) {
    int u, v;
    scanf("%d%d", &u, &v);
    T[u].pb(v), T[v].pb(u);
  }
  dfs(1, 0);

  F[n - 1] = 1;
  FOR(i, 2, n) { F[n - i] = pw(n, i - 2) * f[1][i][1] % P; }
  // FOR(i,0,n-1)printf("%lld%c",F[i]," \n"[i==n-1]);
  // F[i]:枚举i条边与原树相同，其他边随便连的生成树方案数。
  // G[i]:i条边与原树相同，其他边与原树不同的方案数。

  fac[0] = 1;
  FOR(i, 1, n) fac[i] = fac[i - 1] * i % P;
  fnv[n] = pw(fac[n], P - 2);
  ROF(i, n, 1) fnv[i - 1] = fnv[i] * i % P;

  FOR(i, 0, n - 1) {
    FOR(k, i, n - 1)
    G[i] = (G[i] + binom(k, i) * ((k - i) & 1 ? -1 : 1) * F[k] % P + P) % P;
  }
  long long ans = 0;
  FOR(i, 0, k) ans = (ans + G[n - 1 - i]) % P;

  printf("%lld\n", ans);

  return 0;
}
