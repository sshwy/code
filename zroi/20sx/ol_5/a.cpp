// by Sshwy
//#define DEBUGGER
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

#ifdef DEBUGGER

#define log_prefix()                                                \
  {                                                                 \
    fprintf(stderr, "\033[37mLine %-3d [%dms]:\033[0m  ", __LINE__, \
        (int)clock() / 1000);                                       \
  }
#define ilog(...) \
  { fprintf(stderr, __VA_ARGS__); }
#define llog(...)      \
  {                    \
    log_prefix();      \
    ilog(__VA_ARGS__); \
  }
#define log(...)           \
  {                        \
    log_prefix();          \
    ilog(__VA_ARGS__);     \
    fprintf(stderr, "\n"); \
  }
#define red(...)       \
  {                    \
    log_prefix();      \
    ilog("\033[31m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define green(...)     \
  {                    \
    log_prefix();      \
    ilog("\033[32m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define blue(...)      \
  {                    \
    log_prefix();      \
    ilog("\033[34m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }

#else

#define log_prefix() ;
#define ilog(...) ;
#define llog(...) ;
#define log(...) ;
#define red(...) ;
#define green(...) ;
#define blue(...) ;

#endif

namespace IO {
  const int BSZ = 100000;
  char nc() {
    static char bf[BSZ], *p1 = bf, *p2 = bf;
    return p1 == p2 && (p2 = (p1 = bf) + fread(bf, 1, BSZ, stdin), p1 == p2) ? EOF
                                                                             : *p1++;
  }
  int rd() {
    int res = 0;
    char c = nc();
    while (!isdigit(c)) c = nc();
    while (isdigit(c)) res = res * 10 + c - '0', c = nc();
    return res;
  }
  long long rdll() {
    long long res = 0;
    char c = nc();
    while (!isdigit(c)) c = nc();
    while (isdigit(c)) res = res * 10 + c - '0', c = nc();
    return res;
  }
  void rd(char *s) {
    char c = nc();
    while (!isblank(c) && isprint(c) && c != EOF) *s++ = c, c = nc();
    *s = 0;
  }
  int tmp[20], lt;
  char OBF[BSZ], *OBP = OBF;
  void flush() { fwrite(OBF, 1, OBP - OBF, stdout), OBP = OBF; }
  void wrch(char x) {
    if (OBP == OBF + BSZ - 1) flush();
    *OBP = x, ++OBP;
  }
  void wr(int x, int base = 10) {
    if (x == 0) return wrch('0'), void();
    if (x < 0) wrch('-'), x = -x;
    lt = 0;
    while (x) tmp[++lt] = x % base, x /= base;
    while (lt > 0) wrch(tmp[lt] + '0'), --lt;
  }
  void wr(long long x, int base = 10) {
    if (x == 0) return wrch('0'), void();
    if (x < 0) wrch('-'), x = -x;
    lt = 0;
    while (x) tmp[++lt] = x % base, x /= base;
    while (lt > 0) wrch(tmp[lt] + '0'), --lt;
  }
  void wr(const char *s) {
    while (*s) wrch(*s), ++s;
  }
  void wr(char x) { wrch(x); }
} // namespace IO
/******************heading******************/
using IO::rd;
using IO::wr;
const int N = 20, TT = 1e4 + 5;
int n, T, a[N][TT], b[N][N][TT], tmp[TT * 2], ans[1 << N], que[TT * 2], ql, qr;
int f[1 << N][N]; // f[S,x]:S, end at x
// 2^n n^2 T
void work() { // ans[0..2^n-1]
  memset(f, 0x3f, sizeof(f));
  memset(ans, 0x3f, sizeof(ans));
  FOR(i, 0, n - 1) f[1 << i][i] = 0;
  int nn = 1 << n;
  FOR(i, 1, nn - 1) {
    FOR(j, 0, n - 1) if (i >> j & 1) {
      int &y = f[i][j];
      y = y + a[j][y % T];
      ans[i] = min(ans[i], y);
      FOR(k, 0, n - 1) if (!(i >> k & 1)) {
        int &x = f[i ^ (1 << k)][k];
        // f[i][j] -> f[i^(1<<k)][k]
        x = min(x, y + b[min(j, k)][max(j, k)][y % T]);
      }
    }
  }
}

void calc(int *F, int *G) {
  FOR(i, 0, T - 1) F[i + T] = F[i];
  FOR(i, 0, T + T - 1) F[i] += i;
  ql = 1, qr = 0;
  FOR(i, 0, T - 1) {
    while (ql <= qr && F[que[qr]] > F[i]) --qr;
    que[++qr] = i;
  }
  G[0] = F[que[ql]];
  FOR(i, 1, T - 1) { // G[i]
    int I = i + T - 1;
    while (ql <= qr && que[ql] < i) ++ql;
    while (ql <= qr && F[que[qr]] > F[I]) --qr;
    que[++qr] = I;
    G[i] = F[que[ql]] - i;
  }
}

int main() {
  n = rd(), T = rd();
  FOR(i, 0, n - 1) {
    FOR(j, 0, T - 1) tmp[j] = rd();
    calc(tmp, a[i]);
  }
  FOR(i, 0, n - 1) FOR(j, i + 1, n - 1) {
    FOR(k, 0, T - 1) tmp[k] = rd();
    calc(tmp, b[i][j]);
  }
  work();
  int q = rd();
  FOR(i, 1, q) {
    int x = rd();
    wr(ans[x]), wr('\n');
  }
  IO::flush();
  return 0;
}
