// by Sshwy
//#define DEBUGGER
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

#ifdef DEBUGGER

#define log_prefix()                                                \
  {                                                                 \
    fprintf(stderr, "\033[37mLine %-3d [%dms]:\033[0m  ", __LINE__, \
        (int)clock() / 1000);                                       \
  }
#define ilog(...) \
  { fprintf(stderr, __VA_ARGS__); }
#define llog(...)      \
  {                    \
    log_prefix();      \
    ilog(__VA_ARGS__); \
  }
#define log(...)           \
  {                        \
    log_prefix();          \
    ilog(__VA_ARGS__);     \
    fprintf(stderr, "\n"); \
  }
#define red(...)       \
  {                    \
    log_prefix();      \
    ilog("\033[31m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define green(...)     \
  {                    \
    log_prefix();      \
    ilog("\033[32m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define blue(...)      \
  {                    \
    log_prefix();      \
    ilog("\033[34m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }

#else

#define log_prefix() ;
#define ilog(...) ;
#define llog(...) ;
#define log(...) ;
#define red(...) ;
#define green(...) ;
#define blue(...) ;

#endif

namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 5000, P = 998244353;
int n, k, s, fa[N], w[N], f[N][N], g[N], tmp[N], len;

const int SZ = 1 << 19;
int pw(int a, int m) {
  if (m < 0) m += P - 1;
  int res = 1;
  while (m) m & 1 ? res = 1ll * res * a % P : 0, a = 1ll * a * a % P, m >>= 1;
  return res;
}
int tr[SZ];
int init(int n) { // n指多项式的长度，非度数
  int len = 1;
  while (len < n) len <<= 1;
  FOR(i, 0, len - 1) tr[i] = tr[i >> 1] >> 1 | ((i & 1) * (len / 2));
  return len;
}
void dft(int *f, int len, int tag) {
  FOR(i, 0, len - 1) if (i < tr[i]) swap(f[i], f[tr[i]]);
  FOR(i, 0, len - 1) f[i] = (f[i] + P) % P;
  for (int j = 1; j < len; j <<= 1)
    for (int i = 0, wn = pw(3, (P - 1) / (j << 1) * tag); i < len; i += j << 1)
      for (int k = i, u, v, w = 1; k < i + j; k++, w = 1ll * w * wn % P)
        u = f[k], v = f[k + j] * 1ll * w % P, f[k] = (u + v) % P,
        f[k + j] = (u - v + P) % P;
  if (tag == -1)
    for (int i = 0, ilen = pw(len, P - 2); i < len; i++) f[i] = 1ll * f[i] * ilen % P;
}

vector<int> T[N];

void dfs(int u) {
  for (int v : T[u]) dfs(v);
  memset(g, 0, sizeof(g));
  if (w[u] == 0) {
    FOR(i, 0, k) g[i] = 1;
    for (int v : T[u]) {
      if (s > 31) {
        int *F = f[v];
        fill(F + s + 1, F + len, 0);
        fill(g + s + 1, g + len, 0);
        dft(F, len, 1), dft(g, len, 1);
        FOR(i, 0, len - 1) g[i] = g[i] * 1ll * F[i] % P;
        dft(g, len, -1);
      } else {
        fill(tmp, tmp + s + 1, 0);
        FOR(i, 0, s) FOR(j, 0, i) {
          if (g[i - j] && f[v][j])
            tmp[i] = (tmp[i] + 1ll * f[v][j] * g[i - j] % P) % P;
        }
        FOR(i, 0, s) g[i] = tmp[i];
      }
    }
    FOR(i, 0, s) f[u][i] = g[i];
  } else {
    g[0] = 1;
    for (int v : T[u]) {
      if (s > 31) {
        int *F = f[v];
        fill(F + s + 1, F + len, 0);
        fill(g + s + 1, g + len, 0);
        dft(F, len, 1), dft(g, len, 1);
        FOR(i, 0, len - 1) g[i] = g[i] * 1ll * F[i] % P;
        dft(g, len, -1);
      } else {
        fill(tmp, tmp + s + 1, 0);
        FOR(i, 0, s) FOR(j, 0, i) {
          if (g[i - j] && f[v][j])
            tmp[i] = (tmp[i] + 1ll * f[v][j] * g[i - j] % P) % P;
        }
        FOR(i, 0, s) g[i] = tmp[i];
      }
    }
    FOR(i, 0, s) if (i % 2 == 0) f[u][i] = g[i / 2];
    else f[u][i] = 0;
  }
}
int main() {
  scanf("%d%d%d", &n, &k, &s);
  len = init(s + s + 2);
  FOR(i, 2, n) scanf("%d", &fa[i]), T[fa[i]].pb(i);
  FOR(i, 1, n) { scanf("%d", &w[i]); }
  dfs(1);
  int ans = 0;
  FOR(i, 0, s) ans ^= f[1][i];
  printf("%d\n", ans);
  return 0;
}
