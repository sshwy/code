// by Sshwy
//#define DEBUGGER
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

#ifdef DEBUGGER

#define log_prefix()                                                \
  {                                                                 \
    fprintf(stderr, "\033[37mLine %-3d [%dms]:\033[0m  ", __LINE__, \
        (int)clock() / 1000);                                       \
  }
#define ilog(...) \
  { fprintf(stderr, __VA_ARGS__); }
#define llog(...)      \
  {                    \
    log_prefix();      \
    ilog(__VA_ARGS__); \
  }
#define log(...)           \
  {                        \
    log_prefix();          \
    ilog(__VA_ARGS__);     \
    fprintf(stderr, "\n"); \
  }
#define red(...)       \
  {                    \
    log_prefix();      \
    ilog("\033[31m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define green(...)     \
  {                    \
    log_prefix();      \
    ilog("\033[32m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define blue(...)      \
  {                    \
    log_prefix();      \
    ilog("\033[34m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }

#else

#define log_prefix() ;
#define ilog(...) ;
#define llog(...) ;
#define log(...) ;
#define red(...) ;
#define green(...) ;
#define blue(...) ;

#endif

namespace IO {
  const int BSZ = 100000;
  char nc() {
    static char bf[BSZ], *p1 = bf, *p2 = bf;
    return p1 == p2 && (p2 = (p1 = bf) + fread(bf, 1, BSZ, stdin), p1 == p2) ? EOF
                                                                             : *p1++;
  }
  int rd() {
    int res = 0;
    char c = nc();
    while (!isdigit(c)) c = nc();
    while (isdigit(c)) res = res * 10 + c - '0', c = nc();
    return res;
  }
  long long rdll() {
    long long res = 0;
    char c = nc();
    while (!isdigit(c)) c = nc();
    while (isdigit(c)) res = res * 10 + c - '0', c = nc();
    return res;
  }
  void rd(char *s) {
    char c = nc();
    while (!isblank(c) && isprint(c) && c != EOF) *s++ = c, c = nc();
    *s = 0;
  }
  int tmp[20], lt;
  char OBF[BSZ], *OBP = OBF;
  void flush() { fwrite(OBF, 1, OBP - OBF, stdout), OBP = OBF; }
  void wrch(char x) {
    if (OBP == OBF + BSZ - 1) flush();
    *OBP = x, ++OBP;
  }
  void wr(int x, int base = 10) {
    lt = 0;
    while (x) tmp[++lt] = x % base, x /= base;
    while (lt > 0) wrch(tmp[lt] + '0'), --lt;
  }
  void wr(long long x, int base = 10) {
    lt = 0;
    while (x) tmp[++lt] = x % base, x /= base;
    while (lt > 0) wrch(tmp[lt] + '0'), --lt;
  }
  void wr(const char *s) {
    while (*s) wrch(*s), ++s;
  }
  void wr(char x) { wrch(x); }
} // namespace IO
/******************heading******************/
const int N = 20, TT = 1e4 + 5;
int n, T, a[N][TT], b[N][N][TT], ans[1 << N];
int f[1 << N][N]; // f[S,x]:S, end at x

void work() { // ans[0..2^n-1]
  memset(f, 0x3f, sizeof(f));
  memset(ans, 0x3f, sizeof(ans));
  FOR(i, 0, n - 1) f[1 << i][i] = 0;
  int nn = 1 << n;
  FOR(i, 1, nn - 1) {
    FOR(j, 0, n - 1) if (i >> j & 1) {
      int y = 0x3f3f3f3f, z = f[i][j];

      FOR(t, 0, T - 1) { y = min(y, z + t + a[j][(z + t) % T]); }
      f[i][j] = y;
      // printf("f %d %d : %d\n",i,j,f[i][j]);

      ans[i] = min(ans[i], y);
      FOR(k, 0, n - 1) if (!(i >> k & 1)) {
        int &x = f[i ^ (1 << k)][k];
        // f[i][j] -> f[i^(1<<k)][k]
        FOR(t, 0, T - 1) { x = min(x, y + t + b[j][k][(y + t) % T]); }
      }
    }
  }
}

int main() {
  scanf("%d%d", &n, &T);
  FOR(i, 0, n - 1) { FOR(j, 0, T - 1) scanf("%d", &a[i][j]); }
  FOR(i, 0, n - 1)
  FOR(j, i + 1, n - 1)
  FOR(k, 0, T - 1) scanf("%d", &b[i][j][k]), b[j][i][k] = b[i][j][k];
  work();
  int q;
  // long long s=0,sx=0;
  scanf("%d", &q);
  FOR(i, 1, q) {
    int x;
    scanf("%d", &x);
    // s+=ans[x];
    // sx^=ans[x];
    printf("%d\n", ans[x]);
  }
  // printf("%lld %lld\n",s,sx);
  return 0;
}
