// by Sshwy
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int SZ = 5e5 + 5;

int q, T, ans;
set<pair<int, int>> s;

struct hash_map {
  struct data {
    pair<int, int> u, v;
    int nex;
  };
  data e[SZ];
  int h[SZ], le;
  int hash(pair<int, int> u) { return (u.fi * 1ll * u.se % SZ + u.fi) % SZ; }
  pair<int, int> &operator[](pair<int, int> u) {
    int hu = hash(u);
    for (int i = h[hu]; i; i = e[i].nex)
      if (e[i].u == u) return e[i].v;
    return e[++le] = (data){u, {0, 0}, h[hu]}, h[hu] = le, e[le].v;
  }
  bool count(pair<int, int> u) {
    int hu = hash(u);
    for (int i = h[hu]; i; i = e[i].nex)
      if (e[i].u == u) return 1;
    return 0;
  }
  void clear() { memset(h, 0, sizeof(h)), le = 0; }
} f;

// map< pair<int,int>, pair<int,int> > f;//并查集

pii get(pii x) {
  if (f.count(x)) {
    pii y = f[x];
    if (x == y) return x;
    return f[x] = get(y);
  } else
    return x;
}
void merge(pii x, pii y) { f[get(x)] = get(y); }
bool find(pii a, pii b) { return get(a) == get(b); }
inline bool fd(int x, int y) { return s.find({x, y}) != s.end(); }

void work(int x, int y) {
  // printf("work(%d,%d)\n",x,y);
  pii pL = {x, y - 1}, pR = {x, y + 1}, pU = {x - 1, y}, pD = {x + 1, y};
  pii pLU = {x - 1, y - 1}, pRU = {x - 1, y + 1}, pLD = {x + 1, y - 1},
      pRD = {x + 1, y + 1};
  int L = fd(x, y - 1), R = fd(x, y + 1), U = fd(x - 1, y), D = fd(x + 1, y);
  int LU = fd(x - 1, y - 1), RU = fd(x - 1, y + 1), LD = fd(x + 1, y - 1),
      RD = fd(x + 1, y + 1);
  if (L + R + U + D == 4) {
    --ans;
    assert(ans >= 0);
  } else if (L + R + U + D == 3) { //不变
  } else if (L + R + U + D == 2) {
    if ((L && U && RD && find(pL, pRD)) || (L && D && RU && find(pL, pRU)) ||
        (R && U && LD && find(pR, pLD)) || (R && D && LU && find(pR, pLU)))
      ++ans;
    else if ((L && R && find(pL, pR)) || (U && D && find(pU, pD)))
      ++ans;

  } else if (L + R + U + D == 1) {
    // printf("%d,%d,%d,%d,%d,%d,%d,%d\n",L,R,U,D,LU,RU,LD,RD);
    if ((L && RU && RD && find(pL, pRU) && find(pL, pRD)) ||
        (R && LU && LD && find(pR, pLU) && find(pR, pLD)) ||
        (U && LD && RD && find(pU, pLD) && find(pU, pRD)) ||
        (D && LU && RU && find(pD, pLU) && find(pD, pRU)))
      ans += 2;
    else if ((L && RU && find(pL, pRU)) || (L && RD && find(pL, pRD)) ||
             (R && LU && find(pR, pLU)) || (R && LD && find(pR, pLD)) ||
             (U && LD && find(pU, pLD)) || (U && RD && find(pU, pRD)) ||
             (D && LU && find(pD, pLU)) || (D && RU && find(pD, pRU)) ||
             (L && RU && RD && find(pRU, pRD)) || (R && LU && LD && find(pLU, pLD)) ||
             (U && LD && RD && find(pLD, pRD)) || (D && LU && RU && find(pLU, pRU)))
      ++ans;
  } else {
    pii A[] = {get(pLU), get(pLD), get(pRU), get(pRD)};
    sort(A, A + 4);
    int cnt = (A[0] != A[1]) + (A[1] != A[2]) + (A[2] != A[3]) + 1;
    ans += 4 - cnt;
  }
  pii cur = {x, y};
  s.insert(cur);
  //并查集的merge
  if (L) merge(pL, cur);
  if (R) merge(pR, cur);
  if (U) merge(pU, cur);
  if (D) merge(pD, cur);
  if (LU) merge(pLU, cur);
  if (RU) merge(pRU, cur);
  if (LD) merge(pLD, cur);
  if (RD) merge(pRD, cur);
}

int main() {
  scanf("%d%d", &q, &T);
  FOR(i, 1, q) {
    int x, y;
    scanf("%d%d", &x, &y);
    x = x ^ (T * ans);
    y = y ^ (T * ans);
    work(x, y);
    if (i == 1) ++ans;
    printf("%d\n", ans);
  }
  return 0;
}
