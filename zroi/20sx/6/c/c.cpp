#include "maze.h"
// by Sshwy
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 203;
int n, m;

vector<int> g[N], cp[N];  // coming path
int dg[N], fa[N], fap[N]; // father, father path
int g2[N][N];             //邻接表2

void dfs(int coming_node = -1, int compath = 0) {
  int tag = get_label();
  if (tag == 2) { // coming_node -> [cur_node] 是返祖边
    move(get_coming_edge());
    return; // visited
  }
  set_label(2);
  int u = ++n, d = get_edge_number();
  dg[u] = d, fa[u] = coming_node, fap[u] = get_coming_edge();
  if (coming_node != -1) {
    g[coming_node].pb(u);
    g[u].pb(coming_node);
    g2[coming_node][compath] = u;
    g2[u][fap[u]] = coming_node;
  }
  FOR(i, 1, d) {
    if (i == fap[u]) continue;
    move(i);
    dfs(u, i);
  }
  m += d;
  move(fap[u]); //回溯
}

// dfs后，我们把2当作没有访问过的标记
void dfs2(int coming_node = -1, int cur_node = 1,
    int compath = 0) { //找到非树边,g2[coming_node][compath]==cur_node
  printf("dfs2(%d,%d,%d)\n", coming_node, cur_node, compath);
  int tag = get_label();
  {
    int d = dg[cur_node];
    FOR(i, 1, d) printf("%d%c", g2[cur_node][i], " \n"[i == d]);
    printf("fap=%d\n", fap[cur_node]);
    printf("tag=%d\n", tag);
  }
  if (tag == 1) { // coming_node -> [cur_node] 是返祖边
    return;       // visited
  }
  assert(cur_node);
  if (coming_node != -1) assert(g2[coming_node][compath] == cur_node);
  set_label(1);
  int d = dg[cur_node];
  FOR(i, 1, d) printf("%d%c", g2[cur_node][i], " \n"[i == d]);
  FOR(i, 1, d) {
    if (i == fap[cur_node]) continue;
    move(i);
    dfs2(cur_node, g2[cur_node][i], i);
    move(get_coming_edge());
  }
  m += d;
}

void get_edge(int coming_node, int compath) {
  //先清空，把[cur_node]标记为3
  //然后dfs，求出[cur_node]的编号
  //然后加边
  //然后复原
}

int f[N][N];
void dfsT(int u) {
  f[u][0] = 1;
  for (int v : g[u])
    if (v != fa[u]) {
      dfsT(v);
      FOR(i, 1, n) f[u][i] += f[v][i - 1];
    }
}

std::vector<int> solve(int k, int L) {
  // Do whatever you want
  //
  // You can call the following functions:
  // int get_edge_number()
  // int get_coming_edge()
  // int get_label()
  // void set_label(int x)
  // void move(int e)

  assert(k > 1);

  dfs(), m /= 2;

  puts("GG");

  dfs2();

  // Graph
  printf("n=%d,m=%d\n", n, m);
  printf("dg: ");
  FOR(i, 1, n) printf("%d%c", dg[i], " \n"[i == n]);
  printf("GT: ");
  FOR(i, 1, n) for (int v : g[i]) if (i < v) printf("(%d,%d) ", i, v);
  puts("");
  FOR(i, 1, n) {
    printf("g2[%d]: ", i);
    FOR(j, 1, dg[i]) printf("%d%c", g2[i][j], " \n"[j == dg[i]]);
  }
  puts("");

  std::vector<int> answer(n);

  dfsT(1);
  FOR(u, 1, n) FOR(i, 1, n) answer[i - 1] += f[u][i];

  return answer;
}
