// by Sshwy
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/

using namespace RA;

vector<pii> v;
const int SZ = 170;

int main() {
  srand(clock());
  int q = 1e5, T = 0;
  printf("%d %d\n", q, T);
  FOR(i, -SZ, SZ) FOR(j, -SZ, SZ) v.push_back({i, j});
  random_shuffle(v.begin(), v.end());
  assert(v.size() >= q);
  FOR(i, 0, q - 1) { printf("%d %d\n", v[i].fi, v[i].se); }
  return 0;
}
