// by Sshwy
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/

int q, T, ans;
set<pair<int, int>> s;
map<pair<int, int>, pair<int, int>> f; //并查集

pii get(pii x) {
  if (f.count(x)) {
    pii y = f[x];
    if (x == y) return x;
    return f[x] = get(y);
  } else
    return x;
}
bool merge(pii x, pii y) {
  x = get(x), y = get(y);
  if (x == y) return 0;
  f[get(x)] = get(y);
  return 1;
}
bool find(pii a, pii b) { return get(a) == get(b); }
inline bool fd(int x, int y) { return s.find({x, y}) != s.end(); }

const int SZ = 10;
void work(int x, int y) {
  s.insert({x, y});
  f.clear();
  ans = (2 * SZ + 3) * (2 * SZ + 3) - s.size();
  FOR(i, -SZ, SZ) FOR(j, -SZ, SZ) {
    if (fd(i, j)) continue;
    if (!fd(i - 1, j)) ans -= merge({i - 1, j}, {i, j});
    if (!fd(i + 1, j)) ans -= merge({i + 1, j}, {i, j});
    if (!fd(i, j - 1)) ans -= merge({i, j - 1}, {i, j});
    if (!fd(i, j + 1)) ans -= merge({i, j + 1}, {i, j});
  }
  ans -= merge({-SZ - 1, -SZ}, {-SZ - 1, -SZ - 1});
  ans -= merge({-SZ, -SZ - 1}, {-SZ - 1, -SZ - 1});
  ans -= merge({-SZ - 1, SZ}, {-SZ - 1, SZ + 1});
  ans -= merge({-SZ, SZ + 1}, {-SZ - 1, SZ + 1});
  ans -= merge({SZ + 1, SZ}, {SZ + 1, SZ + 1});
  ans -= merge({SZ, SZ + 1}, {SZ + 1, SZ + 1});
  ans -= merge({SZ + 1, -SZ}, {SZ + 1, -SZ - 1});
  ans -= merge({SZ, -SZ - 1}, {SZ + 1, -SZ - 1});
}

int main() {
  scanf("%d%d", &q, &T);
  FOR(i, 1, q) {
    int x, y;
    scanf("%d%d", &x, &y);
    x = x ^ (T * ans);
    y = y ^ (T * ans);
    work(x, y);
    printf("%d\n", ans);
  }
  return 0;
}
