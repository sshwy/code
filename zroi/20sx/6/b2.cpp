// by Sshwy
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 5003;

int n, q;
int f[N][N]; // f[i,j]表示从第i个拍品开始，k=j时优先的人比后手多的个数
char a[N];

void DP() {
  f[n][1] = a[n];
  f[n][0] = 0;
  FOR(j, 2, n) f[n][j] = f[n][1];
  ROF(i, n - 1, 1) {
    FOR(j, 1, n) {
      if (j > n + 1 - i)
        f[i][j] = f[i][n + 1 - i];
      else if (a[i]) {
        f[i][j] = max(1 - f[i + 1][j - 1], min(f[i + 1][j - 1] - 1, f[i + 1][j]));
      } else {
        f[i][j] = max(-f[i + 1][j - 1], min(f[i + 1][j - 1], f[i + 1][j]));
      }
    }
  }
  FOR(i, 1, n) {
    FOR(j, 1, i - 1) printf("  ");
    FOR(j, 1, n + 1 - i) if (f[i][j]) printf("%d ", f[i][j]);
    else printf("  ");
    printf(" | %d", a[i]);
    puts("");
  }
}

int main() {
  scanf("%d%d", &n, &q);
  scanf("%s", a + 1);
  FOR(i, 1, n) a[i] -= '0';
  DP();
  FOR(i, 1, q) {
    int k;
    scanf("%d", &k);
    printf("%d\n", f[1][k]);
  }
  return 0;
}
