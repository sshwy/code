// by Sshwy
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/

int q, T;
int vis[1000][1000];
int SZ = 6;

int main() {
  cin >> q >> T;
  FOR(i, 1, 54) {
    int x, y;
    cin >> x >> y;
    vis[x + SZ][y + SZ] = (i == 54) ? 2 : 1;
  }
  FOR(i, -SZ, SZ) {
    FOR(j, -SZ, SZ) {
      if (vis[i + SZ][j + SZ] == 1)
        printf(" ");
      else if (vis[i + SZ][j + SZ] == 2)
        printf("_");
      else
        printf("0");
    }
    puts("");
  }
  return 0;
}
