// by Sshwy
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 1e4 + 5, M = N * 10;
const double eps = 1e-5;

struct p2 {
  double x, y;
  p2() {}
  p2(double X, double Y) { x = X, y = Y; }
  bool operator==(p2 X) { return abs(x - X.x) < eps && abs(y - X.y) < eps; }
  p2 operator-(p2 X) { return p2(x - X.x, y - X.y); }
};
double det(p2 A, p2 B) { return A.x * B.y - B.x * A.y; }
struct ln {
  p2 a, b;
  double slope() { return (a.y - b.y) / (a.x - b.x); }
  void print() {
    printf("Line( (%.3lf,%.3lf), (%.3lf,%.3lf) )\n", a.x, a.y, b.x, b.y);
  }
};
typedef p2 vec;
inline double sqr(double x) { return x * x; }
inline double Dist2(p2 A, p2 B) { return sqr(A.x - B.x) + sqr(A.y - B.y); }
inline double Dist(p2 A, p2 B) { return sqrt(Dist2(A, B)); }

struct C {
  p2 o;
  double r;
  void print() { printf("Circle( (%.3lf,%.3lf) , %.3lf)\n", o.x, o.y, r); }
};

vector<p2> C_ln(C A, ln L) {
  // printf("\033[34mC_ln:\033[0m\n");
  // A.print();
  // L.print();
  vector<p2> res;
  if (L.a.x == L.b.x) {
    double h = sqr(A.r) - sqr(L.a.x - A.r);
    if (h < 0) return res;
    if (abs(h) < eps) {
      res.pb({L.a.x, A.o.y});
      return res;
    } else {
      h = sqrt(h);
      res.pb({L.a.x, A.o.y + h});
      res.pb({L.a.x, A.o.y - h});
      return res;
    }
  }
  double k = L.slope();
  double b = L.a.y - L.a.x * k;

  // printf("k=%.3lf,b=%.3lf\n",k,b);

  double a1 = 1 + k * k;
  double b1 = 2 * k * b - 2 * A.o.x - 2 * A.o.y * k;
  double c1 = b * b - 2 * A.o.y * b + sqr(A.o.x) + sqr(A.o.y) - sqr(A.r);
  // a1x^2+b1x+c1=0
  double delta = b1 * b1 - 4 * a1 * c1;
  // printf("delta=%.3lf\n",delta);
  if (delta < 0) return res;
  if (abs(delta) < eps) {
    double x = -b1 / (2 * a1);
    double y = k * x + b;
    res.pb({x, y});
    return res;
  } else {
    delta = sqrt(delta);
    double x1 = (-b1 + delta) / (2 * a1), x2 = (-b1 - delta) / (2 * a1);
    double y1 = x1 * k + b, y2 = x2 * k + b;
    // printf("\033[35m%.3lf\033[0m\n",a1*x1*x1+b1*x1+c1);
    res.pb({x1, y1});
    res.pb({x2, y2});
    return res;
  }
}
double Stri(double a, double b, double c) {
  double p = (a + b + c) / 2;
  return sqrt(p * (p - a) * (p - b) * (p - c));
}
vector<p2> InterV(vector<p2> v1, vector<p2> v2) {
  vector<p2> res;
  for (p2 x : v1) {
    int fl = 0;
    // printf("x=(%.3lf,%.3lf)\n",x.x,x.y);
    for (p2 y : v2) {
      // printf("  x=(%lf,%lf),",x.x,x.y);
      // printf("y=(%lf,%lf)\n",y.x,y.y);
      if (x == y) fl = 1;
    }
    // printf("fl=%d\n",fl);
    if (fl) res.pb(x);
  }
  return res;
}
vector<p2> InterC(C A, C B) {
  // printf("\033[33mInterC:\033[0m\n");
  // A.print();
  // B.print();
  // puts("");
  vector<p2> res, r1, r2;
  double D = Dist(A.o, B.o);
  // printf("D=%lf\n",D);
  if (A.r + B.r < D) return res;
  double S = Stri(A.r, B.r, D);
  // printf("S=%lf\n",S);
  double h = S * 2 / abs(A.o.x - B.o.x); // qianchuigao
  if (h < eps) {
    r1 = C_ln(A, {A.o, B.o});
    r2 = C_ln(B, {A.o, B.o});
    return InterV(r1, r2);
  } else {
    // printf("%.3lf,%.3lf,%.3lf,%.3lf\n",A.o.x,A.o.y+h,B.o.x,B.o.y+h);
    r1 = C_ln(A, {{A.o.x, A.o.y + h}, {B.o.x, B.o.y + h}});
    // for(p2 x:r1)printf("(%.3lf,%.3lf) ",x.x,x.y);
    // puts("");
    r2 = C_ln(B, {{A.o.x, A.o.y + h}, {B.o.x, B.o.y + h}});
    res = InterV(r1, r2);
    r1 = C_ln(A, {{A.o.x, A.o.y - h}, {B.o.x, B.o.y - h}});
    r2 = C_ln(B, {{A.o.x, A.o.y - h}, {B.o.x, B.o.y - h}});
    r1 = InterV(r1, r2);
    res.insert(res.end(), r1.begin(), r1.end());
    return res;
  }
}

int n, m;
vector<int> V[N];
bool vis[N];
int que[N], lq;

struct qxx {
  int nex, t;
  double v;
} e[M];
int h[N], le = 1;
void add_path(int f, int t, double v) { e[++le] = {h[f], t, v}, h[f] = le; }

p2 a[N];

vector<p2> calc(vector<int> E) {
  // printf("\033[32mcalc(%d)\033[0m\n",e[E[0]^1].t);
  // printf("E.size()=%d\n",E.size());
  vector<p2> res;
  if (E.size() >= 3) {
    C c[] = {{a[e[E[0]].t], e[E[0]].v}, {a[e[E[1]].t], e[E[1]].v},
        {a[e[E[2]].t], e[E[2]].v}};
    // c[0].print();
    // c[1].print();
    // c[2].print();
    vector<p2> t1 = InterC(c[0], c[1]), t2 = InterC(c[1], c[2]);
    // for(p2 x:t1)printf("(%.3lf,%.3lf) ",x.x,x.y);
    // puts("");
    // for(p2 x:t2)printf("(%.3lf,%.3lf) ",x.x,x.y);
    // puts("");
    // printf("\033[36mInterV\033[0m\n");
    res = InterV(t1, t2);
    // printf("\033[36mInterV Done\033[0m\n");
    // printf("res: ");for(p2 x:res)printf("(%.3lf,%.3lf) ",x.x,x.y);
    // puts("");
    assert(res.size() == 1);
    return res;
  } else {
    assert(E.size() == 2);
    C c[] = {{a[e[E[0]].t], e[E[0]].v}, {a[e[E[1]].t], e[E[1]].v}};
    // c[0].print();
    // c[1].print();
    res = InterC(c[0], c[1]);
    // for(p2 x:res)printf("(%.3lf,%.3lf) ",x.x,x.y);
    // puts("");
    if (res.size() == 1) return res;
    assert(res.size() == 2);
    if (res[0].y < 0) res.erase(res.begin());
    if (res[1].y < 0) res.pop_back();
    if (res.size() == 1) return res;
    p2 tp = e[E[0]].t == 1 || e[E[1]].t == 1 ? a[2] : a[1];
    assert(det(c[1].o - c[0].o, res[0] - c[0].o) *
               det(c[1].o - c[0].o, res[1] - c[0].o) <
           0);
    if (det(c[1].o - c[0].o, tp - c[0].o) * det(c[1].o - c[0].o, res[0] - c[0].o) < 0)
      res.pop_back();
    else
      res.erase(res.begin());
    assert(res.size() == 1);
    return res;
  }
}
void mark(int u) {
  // printf("mark(%d)\n",u);
  // printf("a[%d]=(%.3lf,%.3lf)\n",u,a[u].x,a[u].y);
  vis[u] = 1; //已求出
  for (int i = h[u]; i; i = e[i].nex) {
    int v = e[i].t;
    if (vis[v]) continue;
    V[v].pb(i ^ 1);
  }
  for (int i = h[u]; i; i = e[i].nex) {
    int v = e[i].t;
    if (vis[v]) continue;
    if (V[v].size() >= 3) {
      a[v] = calc(V[v])[0];
      mark(v);
    }
  }
  for (int i = h[u]; i; i = e[i].nex) {
    int v = e[i].t;
    if (vis[v]) continue;
    if (V[v].size() >= 2) {
      a[v] = calc(V[v])[0];
      mark(v);
    }
  }
}
void go() {
  // printf("go()\n");
  scanf("%d%d", &n, &m);

  FOR(i, 1, n) {
    V[i].clear();
    vis[i] = 0;
    h[i] = 0;
  }
  lq = 0;
  le = 1;

  FOR(i, 1, m) {
    int x, y;
    double w;
    scanf("%d%d%lf", &x, &y, &w);
    add_path(x, y, w);
    add_path(y, x, w);
    if (i == 1) a[2] = {w, 0};
  }

  a[1] = {0, 0};

  mark(1);
  mark(2);
  FOR(i, 1, n) assert(vis[i]);

  FOR(i, 1, n) printf("%d %d\n", (int)(a[i].x + 0.5), (int)(a[i].y + 0.5));
  puts("");
}
int main() {
  int t;
  scanf("%d", &t);
  FOR(i, 1, t) go();
  return 0;
}
