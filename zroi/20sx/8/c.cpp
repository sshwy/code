// by Sshwy
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 105, G = 205;
const double eps = 1e-6;

int n, g, t;
int c[N];
int b[N];

double p[N][G][N], f[N][G][N];

double ans = 0, tot = 0;

void upd() {
  bool vis[N] = {0};
  // FOR(i,1,t)printf("%d%c",b[i]," \n"[i==n]);
  FOR(i, 1, t) {
    FOR(j, 1, n) {
      if (vis[j] == 0 && c[j] >= b[i]) {
        vis[j] = 1;
        ans += b[i];
        break;
      }
    }
  }
  tot += 1;
}
void dfs(int cur) {
  if (cur == t + 1) {
    upd();
    return;
  }
  FOR(i, 1, g) {
    b[cur] = i;
    dfs(cur + 1);
  }
}
void DP() {
  FOR(j, 1, g) {
    int cnt = 0;
    FOR(i, 1, n) cnt += c[i] == j;
    p[0][j][cnt] = 1;
    f[0][j][cnt] = 0;
  }
  FOR(j, 1, g) FOR(k, 0, n) {
    if (abs(p[0][j][k] > eps)) {
      printf("%d 个团队，有 %d 个大小恰好为 %d 的空桌子的概率 p[%d,%d,%d]=%.6lf\n", 0,
          k, j, 0, j, k, p[0][j][k]);
    }
  }
  puts("");
  FOR(i, 1, t) {
    FOR(x, 1, g) { //加入一个大小为x的团队
      FOR(j, x, g + 1) {
        double c = 1.0 / g, s = 0;
        FOR(y, x, j - 1) c = c * p[i - 1][y][0];
        // c的概率，选择一个大小为j的桌子
        FOR(k, 1, n) s += p[i - 1][j][k];
        if (j == g + 1) s = 1; //不被接受
        if (j == g + 1) printf("c=%.6lf\n", c);
        if (j <= g) FOR(k, 1, n) {
            p[i][j][k - 1] += c * p[i - 1][j][k];
            f[i][j][k - 1] += c * (f[i - 1][j][k] + x);
            // if(i==2&&j==2&&k-1==0)printf("p(%d,%d,%d) <- %.6lf *
            // p(%d,%d,%d):%.6lf\n",i,j,k-1, c ,i-1,j,k,p[i-1][j][k]);
          }
        FOR(y, 1, x - 1) {
          FOR(k, 0, n) {
            p[i][y][k] += p[i - 1][y][k] * c * s;
            f[i][y][k] += (f[i - 1][y][k] + x) * c * s;
            // if(i==2&&y==2&&k==0)printf("x=%d, p(%d,%d,%d) <- %.6lf *
            // p(%d,%d,%d):%.6lf\n",x,i,y,k, c*s ,i-1,y,k,p[i-1][y][k]);
          }
        }
        FOR(y, x, j - 1) {
          // p[i][y][0]+=p[i-1][y][0]*c*s;
          p[i][y][0] += c * s;
          f[i][y][0] += c * s * x;
          if (i == 2 && y == 2 && 0 == 0) {
            // printf("c=%.3lf,s=%.3lf\n",c,s);
            // printf("j=%d, x=%d, p(%d,%d,%d) <- %.6lf \n",j,x,i,y,0, c*s );
          }
        }
        FOR(y, j + 1, g) {
          FOR(k, 0, n) {
            p[i][y][k] += p[i - 1][y][k] * c * s;
            f[i][y][k] += (f[i - 1][y][k] + x) * c * s;
            // if(i==2&&y==2&&k==0)printf("x=%d, p(%d,%d,%d) <- %.6lf *
            // p(%d,%d,%d):%.6lf\n",x,i,y,k, c*s ,i-1,y,k,p[i-1][y][k]);
          }
        }
      }
    }
    FOR(j, 1, g) FOR(k, 0, n) {
      if (abs(p[i][j][k] > eps)) {
        printf("%d 个团队，有 %d 个大小恰好为 %d 的空桌子的概率 "
               "p[%d,%d,%d]=%.6lf\n",
            i, k, j, i, j, k, p[i][j][k]);
        printf("%d 个团队，有 %d 个大小恰好为 %d 的空桌子的人数期望 "
               "f[%d,%d,%d]=%.6lf\n",
            i, k, j, i, j, k, f[i][j][k]);
      }
    }
    puts("");
  }
}
int main() {
  scanf("%d%d%d", &n, &g, &t);
  FOR(i, 1, n) scanf("%d", &c[i]), c[i] = min(c[i], g);
  sort(c + 1, c + n + 1);
  dfs(1);

  DP();
  FOR(i, 1, t) { //枚举第i个团队
  }

  printf("%.9lf\n", ans / tot);
  return 0;
}
