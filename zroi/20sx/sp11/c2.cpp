// by Sshwy
//#define DEBUGGER
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

#ifdef DEBUGGER

#define log_prefix()                                                \
  {                                                                 \
    fprintf(stderr, "\033[37mLine %-3d [%dms]:\033[0m  ", __LINE__, \
        (int)clock() / 1000);                                       \
  }
#define ilog(...) \
  { fprintf(stderr, __VA_ARGS__); }
#define llog(...)      \
  {                    \
    log_prefix();      \
    ilog(__VA_ARGS__); \
  }
#define log(...)           \
  {                        \
    log_prefix();          \
    ilog(__VA_ARGS__);     \
    fprintf(stderr, "\n"); \
  }
#define red(...)       \
  {                    \
    log_prefix();      \
    ilog("\033[31m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define green(...)     \
  {                    \
    log_prefix();      \
    ilog("\033[32m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define blue(...)      \
  {                    \
    log_prefix();      \
    ilog("\033[34m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }

#else

#define log_prefix() ;
#define ilog(...) ;
#define llog(...) ;
#define log(...) ;
#define red(...) ;
#define green(...) ;
#define blue(...) ;

#endif

namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int W = 4050, Ox = 2000, Oy = 2000;
int n;
int a[W][W], ld[W][W], rd[W][W], lu[W][W], ru[W][W];

int main() {
  scanf("%d", &n);
  FOR(_, 1, n) {
    char typ[5];
    int x, y, d;
    scanf("%s%d%d%d", typ, &x, &y, &d);
    d /= 2;
    if (typ[0] == 'A') {
      for (int i = y - d; i < y + d; i++) {
        a[Ox + x - d][Oy + i]++;
        a[Ox + x + d][Oy + i]--;
      }
    } else if (typ[0] == 'B') {
      for (int i = -(d - 1); i < 0; ++i) {
        a[Ox + x - (d + i)][Oy + y + i]++;
        a[Ox + x + (d + i)][Oy + y + i]--;
      }
      for (int i = 0; i < (d - 1); ++i) {
        a[Ox + x - (d - i - 1)][Oy + y + i]++;
        a[Ox + x + (d - i - 1)][Oy + y + i]--;
      }
      lu[Ox + x][Oy + y - d]++;
      lu[Ox + x + d][Oy + y]--;
      ru[Ox + x - d][Oy + y - 1]++;
      ru[Ox + x][Oy + y - d - 1]--;
      ld[Ox + x][Oy + y + d - 1]++;
      ld[Ox + x + d][Oy + y - 1]--;
      rd[Ox + x - d][Oy + y]++;
      rd[Ox + x][Oy + y + d]--;
    }
  }
  FOR(i, 0, W - 2) FOR(j, 0, W - 1) { a[i + 1][j] += a[i][j]; }
  FOR(i, 0, W - 2) FOR(j, 0, W - 2) {
    lu[i + 1][j + 1] += lu[i][j];
    rd[i + 1][j + 1] += rd[i][j];
  }
  FOR(i, 0, W - 2) FOR(j, 1, W - 1) {
    ld[i + 1][j - 1] += ld[i][j];
    ru[i + 1][j - 1] += ru[i][j];
  }
  // FOR(i,0,W-1)FOR(j,0,W-1){
  //    assert(a[i][j]>=0);
  //    assert(lu[i][j]>=0);
  //    assert(ru[i][j]>=0);
  //    assert(ld[i][j]>=0);
  //    assert(rd[i][j]>=0);
  //}
  // ROF(i,Ox*2,0)FOR(j,0,Oy*2){
  //    if(ld[j][i])printf("%d%c",ld[j][i]," \n"[j==Oy*2]);
  //    else if(rd[j][i])printf("%d%c",rd[j][i]," \n"[j==Oy*2]);
  //    else if(ru[j][i])printf("%d%c",ru[j][i]," \n"[j==Oy*2]);
  //    else if(lu[j][i])printf("%d%c",lu[j][i]," \n"[j==Oy*2]);
  //    else printf(" %c"," \n"[j==Oy*2]);
  //}
  int ap = 0, aq = 0;
  FOR(i, 0, W - 1) FOR(j, 0, W - 1) {
    if (a[i][j])
      ++ap;
    else if (ld[i][j] && ru[i][j])
      ++ap;
    else if (rd[i][j] && lu[i][j])
      ++ap;
    else if (!!ld[i][j] + !!ru[i][j] + !!lu[i][j] + !!rd[i][j] > 1)
      aq += 3;
    else if (ld[i][j] || ru[i][j] || lu[i][j] || rd[i][j])
      aq += 2;
  }
  // printf("ap %d aq %d\n",ap,aq);
  double ans = ap + aq / 4.0;
  printf("%.2lf\n", ans);
  return 0;
}
