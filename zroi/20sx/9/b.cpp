// by Sshwy
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace GraphN {
  struct qxx {
    int nex, t;
  };
  class iterator {
  private:
    qxx *_e;
    int _id;

  public:
    iterator() { _e = NULL, _id = 0; }
    iterator(qxx *E, int ID) { _e = E, _id = ID; }
    bool operator!=(const iterator &it) const { return _id != it._id || _e != it._e; }
    int operator*() const { return (_e + _id)->t; }
    const iterator &operator++() { return _id = (_e + _id)->nex, *this; }
    inline iterator begin() { return *this; }
    inline iterator end() { return iterator(_e, 0); }
  };
  template <const int N, const int M> class Graph {
  private:
    qxx e[M];
    int h[N], le;

  public:
    Graph() { fill(h, h + N, 0), le = 1; }
    void add_path(int f, int t) { e[++le] = {h[f], t}, h[f] = le; }
    iterator operator[](int u) { return iterator(e, h[u]); }
  };
} // namespace GraphN
using GraphN::Graph;

namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 3e5 + 5, M = N * 2, P = 998244353, SZ = 1 << 20;

Graph<N, M> g;
int n, x;
int dep[N], a[N], fa[N][20];

void dfs(int u, int p) { // dep,fa{{{
  dep[u] = dep[p] + 1, fa[u][0] = p;
  FOR(j, 1, 19) {
    fa[u][j] = fa[fa[u][j - 1]][j - 1];
    if (fa[u][j] == 0) break;
  }
  for (int v : g[u])
    if (v != p) dfs(v, u);
}
int lca(int u, int v) {
  if (dep[u] < dep[v]) swap(u, v);
  ROF(j, 19, 0) if (dep[u] - (1 << j) >= dep[v]) u = fa[u][j];
  if (u == v) return v;
  ROF(j, 19, 0) if (fa[u][j] != fa[v][j]) u = fa[u][j], v = fa[v][j];
  return fa[u][0];
}
int dist(int u, int v) {
  int z = lca(u, v);
  return dep[u] + dep[v] - dep[z] * 2;
} /*}}}*/

int POOL[N * 100], *POOL_end = POOL, tot;
int *NewArray(int len) {
  ++len;
  int *res = POOL_end;
  POOL_end += len;
  tot += len;
  assert(tot < N * 40);
  return res;
}
struct fenwick {
  int *c, SZ;
  fenwick() { c = NULL, SZ = 0; }
  fenwick(int *C, int L) { c = C, SZ = L; }
  void add(int pos, int v) {
    for (int i = pos; i < SZ; i += i & -i) c[i] += v;
  }
  int pre(int pos) {
    int res = 0;
    pos = min(SZ - 1, pos);
    for (int i = pos; i > 0; i -= i & -i) res += c[i];
    return res;
  }
} B1[N], B2[N]; // self, Fa

bool vis[N];
int sz[N], Fa[N], Sz[N], Ht[N];

int Size(int u, int p) {
  sz[u] = 1;
  for (int v : g[u])
    if (v != p)
      if (!vis[v]) sz[u] += Size(v, u);
  return sz[u];
}
int Height(int u, int p) {
  Ht[u] = 0;
  for (int v : g[u])
    if (v != p)
      if (!vis[v]) Ht[u] = max(Ht[u], Height(v, u));
  return ++Ht[u];
}
pair<int, int> Core(int u, int p, const int T) {
  int core = u, son = T - sz[u];
  for (int v : g[u])
    if (v != p)
      if (!vis[v]) son = max(son, sz[v]);
  for (int v : g[u])
    if (v != p) {
      if (!vis[v]) {
        pair<int, int> p = Core(v, u, T);
        if (son > p.se) core = p.fi, son = p.se;
      }
    }
  return make_pair(core, son);
}
void D(int u, int p) {
  Size(u, 0);
  int core = Core(u, 0, sz[u]).fi;
  Fa[core] = p, Sz[core] = sz[u], Height(core, 0);
  B1[core] = fenwick(NewArray(Ht[core] + 2), Ht[core] + 2);
  B2[core] = fenwick(NewArray(Ht[p] + 2), Ht[p] + 2);
  vis[core] = 1;
  for (int v : g[core]) {
    if (!vis[v]) D(v, core);
  }
}
void Mark(int u) {
  int d = 0;
  for (int v = u; v; v = Fa[v]) {
    int d1 = Fa[v] ? dist(u, Fa[v]) : -1;
    if (d1 != -1) B2[v].add(d1 + 1, 1);
    B1[v].add(d + 1, 1);
    d = d1;
  }
}
int Query(int u) {            // <= x
  int res = B1[u].pre(x + 1); // self
  for (int v = u; Fa[v]; v = Fa[v]) {
    //经过Fa[v]的 <= x
    int d = x - dist(Fa[v], u);
    res += B1[Fa[v]].pre(d + 1) - B2[v].pre(d + 1);
  }
  return res;
}

int pw(int a, int m) {
  int res = 1;
  while (m) m & 1 ? res = 1ll * res * a % P : 0, a = 1ll * a * a % P, m >>= 1;
  return res;
}
int tr[SZ];
int init(int l) {
  int len = 1;
  while (len < l) len <<= 1;
  FOR(i, 0, len - 1) tr[i] = tr[i >> 1] >> 1 | (i & 1) * (len >> 1);
  return len;
}
void dft(int *f, int len, int tag) {
  FOR(i, 0, len - 1) if (i < tr[i]) swap(f[i], f[tr[i]]);
  for (int j = 1; j < len; j <<= 1)
    for (int i = 0, wn = pw(3, (P - 1) + (P - 1) / (j << 1) * tag); i < len;
         i += j << 1)
      for (int k = i, u, v, w = 1; k < i + j; k++, w = 1ll * w * wn % P)
        u = f[k], v = f[k + j] * 1ll * w % P, f[k] = (u + v) % P,
        f[k + j] = (u - v + P) % P;
  if (tag == -1) {
    int ilen = pw(len, P - 2);
    FOR(i, 0, len - 1) f[i] = 1ll * f[i] * ilen % P;
  }
}

int b[N], c[N];
int fac[N], fnv[N];
int f[SZ], h[SZ];

int G(int i) { return c[i] * 1ll * fac[i] % P; }
int G1(int i) { return G(n - i); }
int F1(int i) { return f[n - i]; }
int F(int i) { return F1(i) * 1ll * fnv[i] % P; }

int main() {
  scanf("%d%d", &n, &x);
  FOR(i, 1, n - 1) {
    int a, b;
    scanf("%d%d", &a, &b);
    g.add_path(a, b);
    g.add_path(b, a);
  }
  dfs(1, 0);
  FOR(i, 1, n) a[i] = i;
  sort(a + 1, a + n + 1, [](int x, int y) { return dep[x] < dep[y]; });

  D(1, 0);
  FOR(i, 1, n) {
    b[i] = Query(a[i]);
    Mark(a[i]);
  }
  FOR(i, 1, n) c[b[i]]++;

  fac[0] = 1;
  FOR(i, 1, n) fac[i] = 1ll * fac[i - 1] * i % P;
  fnv[n] = pw(fac[n], P - 2);
  ROF(i, n, 1) fnv[i - 1] = 1ll * fnv[i] * i % P;

  FOR(i, 0, n) f[i] = G1(i);
  FOR(i, 0, n) h[i] = fnv[i];

  int len = init(2 * (n + 1));
  dft(f, len, 1), dft(h, len, 1);
  FOR(i, 0, len - 1) f[i] = 1ll * f[i] * h[i] % P;
  dft(f, len, -1);

  FOR(i, 0, n - 1) printf("%d%c", F(i), " \n"[i == n - 1]);
  return 0;
}
