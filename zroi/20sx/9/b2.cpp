#include <algorithm>
#include <cstdio>
#include <vector>
#define mxn 1000010
#define LL long long
#define pb push_back
using namespace std;
const int N = 1 << 20, mod = 998244353;
int D, S, Mx, rt, fa[mxn], fr[mxn], stk[mxn], vis[mxn], siz[mxn], mxs[mxn], dep[mxn];
int n, m, sl, fh, *nw, q[mxn], fac[mxn], ifc[mxn], ans[mxn], cnt[mxn], pl[mxn * 10];
int ln, A[N], B[N], rev[N];
LL invn, W[20][N];
int t, h[mxn];
struct Tre {
  int to, nxt;
} e[mxn << 1];
struct BIT {
  int N1, N2, *tr1, *tr2;
  void upd1(int i) {
    for (; i <= N1; i += (i & -i)) tr1[i]++;
  }
  void upd2(int i) {
    for (; i <= N2; i += (i & -i)) tr2[i]++;
  }
  int qry1(int i) {
    if (N1 < i) i = N1;
    int res = 0;
    for (; i; i -= (i & -i)) res += tr1[i];
    return res;
  }
  int qry2(int i) {
    if (N2 < i) i = N2;
    int res = 0;
    for (; i; i -= (i & -i)) res += tr2[i];
    return res;
  }
} T[mxn];
vector<int> dis[mxn];
void chkmax(int &x, int y) {
  if (y > x) x = y;
}
void upd(int &x, int y) {
  if ((x += y) >= mod) x -= mod;
}
int rd() {
  sl = 0;
  fh = 1;
  char ch = getchar();
  while (ch < '0' || '9' < ch) {
    if (ch == '-') fh = -1;
    ch = getchar();
  }
  while ('0' <= ch && ch <= '9') sl = sl * 10 + ch - '0', ch = getchar();
  return sl * fh;
}
int _pow(int k, int i) {
  int t = 1;
  for (; i; i >>= 1, k = 1ll * k * k % mod)
    if (i & 1) t = 1ll * t * k % mod;
  return t;
}
void init() {
  int i, x, wn;
  fac[0] = 1;
  for (i = 0, x = 1; x <= n + n; ++i, x <<= 1) {
    W[i][x] = 1;
    wn = _pow(3, (mod - 1) / (x << 1));
    for (int j = 1; j < x; ++j) W[i][x + j] = wn * W[i][x + j - 1] % mod;
    wn = _pow(wn, mod - 2);
    for (int j = 1; j < x; ++j) W[i][x - j] = wn * W[i][x - j + 1] % mod;
  }
  ln = x;
  for (int i = 1; i < ln; ++i) rev[i] = (rev[i >> 1] >> 1) | ((i & 1) * (ln >> 1));
  invn = _pow(ln, mod - 2);
  for (int i = 1; i <= n; ++i) fac[i] = 1ll * i * fac[i - 1] % mod;
  ifc[n] = _pow(fac[n], mod - 2);
  for (int i = n; i; --i) ifc[i - 1] = 1ll * i * ifc[i] % mod;
}
void NTT(int *e, int flg) {
  for (int i = 1; i < ln; ++i)
    if (i < rev[i]) swap(e[i], e[rev[i]]);
  LL fx, fy;
  for (int i = 1, cnt = 0; i < ln; ++cnt, i <<= 1)
    for (int j = 0; j < ln; j += i << 1)
      for (int k = j, t = i; k < j + i; ++k, t += flg) {
        fx = e[k];
        fy = e[k + i] * W[cnt][t];
        e[k] = (fx + fy) % mod;
        e[k + i] = (fx - fy) % mod;
      }
  if (flg == -1)
    for (int i = 0; i < ln; ++i) e[i] = invn * (e[i] + mod) % mod;
}
void add(int u, int v) {
  e[++t] = (Tre){v, h[u]};
  h[u] = t;
  e[++t] = (Tre){u, h[v]};
  h[v] = t;
}
void getrt(int u, int f, int d = 1) {
  int v;
  chkmax(D, d);
  siz[u] = 1;
  mxs[u] = 0;
  dis[u].pb(d);
  for (int i = h[u]; i; i = e[i].nxt)
    if ((v = e[i].to) != f && !vis[v]) {
      getrt(v, u, d + 1);
      siz[u] += siz[v];
      mxs[u] = max(mxs[u], siz[v]);
    }
  mxs[u] = max(mxs[u], S - siz[u]);
  if (mxs[u] < Mx) Mx = mxs[u], rt = u;
}
void Divide(int u) {
  int v, mxd = 0;
  vis[u] = 1;
  dis[u].pb(0);
  for (int i = h[u]; i; i = e[i].nxt)
    if (!vis[v = e[i].to]) {
      D = 0;
      Mx = n;
      S = siz[v];
      getrt(v, 0, 1);
      chkmax(mxd, D);
      T[rt].N2 = D + 1;
      T[rt].tr2 = nw;
      nw += D + 1;
      fa[rt] = u;
      Divide(rt);
    }
  T[u].N1 = mxd + 1;
  T[u].tr1 = nw;
  nw += mxd + 1;
}
int main() {
  n = rd();
  m = rd();
  int x, y;
  init();
  nw = pl;
  for (int i = 1; i < n; ++i) x = rd(), y = rd(), add(x, y);
  Mx = n;
  S = n;
  getrt(1, 0);
  Divide(rt);
  int u, v, d, sz, hd, tl, num;
  q[hd = tl = 1] = 1;
  fr[1] = 0;
  while (hd <= tl) {
    u = q[hd++];
    num = 0;
    sz = dis[u].size();
    for (int i = u, lst = 0, j = sz - 1; i; --j, lst = i, i = fa[i]) {
      d = dis[u][j];
      if (d <= m) {
        num += T[i].qry1(m - d + 1);
        if (lst) num -= T[lst].qry2(m - d + 1);
      }
    }
    cnt[num]++;
    for (int j = sz - 1, i = u; i; --j, i = fa[i]) {
      T[i].upd1(dis[u][j] + 1);
      if (fa[i]) T[i].upd2(dis[u][j - 1] + 1);
    }
    for (int i = h[u]; i; i = e[i].nxt)
      if ((v = e[i].to) != fr[u]) fr[v] = u, q[++tl] = v;
  }
  for (int i = 0; i <= n; i++) printf("%d%c", cnt[i], " \n"[i == n]);
  for (int i = 0; i <= n; ++i) B[i] = ifc[i], A[n - i] = 1ll * cnt[i] * fac[i] % mod;
  NTT(A, 1);
  NTT(B, 1);
  for (int i = 0; i < ln; ++i) A[i] = 1ll * A[i] * B[i] % mod;
  NTT(A, -1);
  for (int i = 0; i < n; ++i)
    printf("%lld%c", 1ll * A[n - i] * ifc[i] % mod, " \n"[i == n - 1]);
  return 0;
}
