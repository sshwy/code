// by Sshwy
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
typedef long long LL;
const int N = 3e5 + 5;

int n, A, B;
int h[N], ans;
int d1[N], d2[N];
LL a[N];

namespace seg {
  const int SZ = 1 << 20;
  LL mn[SZ], tag[SZ];
  void pushup(int u) { mn[u] = min(mn[u << 1], mn[u << 1 | 1]); }
  void nodeadd(int u, int v) { mn[u] += v, tag[u] += v; }
  void pusndown(int u) {
    if (tag[u]) nodeadd(u << 1, tag[u]), nodeadd(u << 1 | 1, tag[u]), tag[u] = 0;
  }
  void build(int u = 1, int l = 1, int r = n) {
    if (l == r) return mn[u] = a[l], void();
    int mid = (l + r) >> 1;
    build(u << 1, l, mid), build(u << 1 | 1, mid + 1, r);
    pushup(u);
  }
  LL rangeMin(int L, int R, int u = 1, int l = 1, int r = n) {
    if (R < l || r < L) return 1e18;
    if (L <= l && r <= R) return mn[u];
    int mid = (l + r) >> 1;
    pusndown(u);
    return min(
        rangeMin(L, R, u << 1, l, mid), rangeMin(L, R, u << 1 | 1, mid + 1, r));
  }
  void rangeAdd(int L, int R, int v, int u = 1, int l = 1, int r = n) {
    if (R < l || r < L) return;
    if (L <= l && r <= R) return nodeadd(u, v);
    int mid = (l + r) >> 1;
    pusndown(u);
    rangeAdd(L, R, v, u << 1, l, mid), rangeAdd(L, R, v, u << 1 | 1, mid + 1, r);
    pushup(u);
  }
} // namespace seg
int main() {
  scanf("%d%d%d", &n, &A, &B);
  FOR(i, 1, n) scanf("%d", &h[i]), --h[i];
  FOR(i, 1, n) d1[i] = h[i] / B - ((h[i] % B) / A + 1);
  FOR(i, 1, n) d2[i] = (h[i] / B + 1);
  // FOR(i,1,n)printf("%d%c",d1[i]," \n"[i==n]);
  // FOR(i,1,n)printf("%d%c",d2[i]," \n"[i==n]);
  a[0] = 1;
  FOR(i, 1, n) a[i] = (a[i - 1] + d2[i]);
  // FOR(i,1,n)printf("%d%c",a[i]," \n"[i==n]);
  seg::build();
  priority_queue<pair<LL, int>> q;
  FOR(i, 1, n) q.push({d1[i] - d2[i], i});
  while (!q.empty()) {
    pair<LL, int> u = q.top();
    q.pop();
    if (seg::rangeMin(u.se, n) + u.fi < 0) continue;
    seg::rangeAdd(u.se, n, u.fi);
    ans++;
  }
  printf("%d\n", ans);
  return 0;
}
