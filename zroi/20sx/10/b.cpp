// by Sshwy
//#define DEBUGGER
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

#ifdef DEBUGGER

#define log_prefix()                                                \
  {                                                                 \
    fprintf(stderr, "\033[37mLine %-3d [%dms]:\033[0m  ", __LINE__, \
        (int)clock() / 1000);                                       \
  }
#define ilog(...) \
  { fprintf(stderr, __VA_ARGS__); }
#define llog(...)      \
  {                    \
    log_prefix();      \
    ilog(__VA_ARGS__); \
  }
#define log(...)           \
  {                        \
    log_prefix();          \
    ilog(__VA_ARGS__);     \
    fprintf(stderr, "\n"); \
  }
#define red(...)       \
  {                    \
    log_prefix();      \
    ilog("\033[31m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define green(...)     \
  {                    \
    log_prefix();      \
    ilog("\033[32m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define blue(...)      \
  {                    \
    log_prefix();      \
    ilog("\033[34m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }

#else

#define log_prefix() ;
#define ilog(...) ;
#define llog(...) ;
#define log(...) ;
#define red(...) ;
#define green(...) ;
#define blue(...) ;

#endif

namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 3005;

int n, m;
char s[N][N];
int p[N][N], R[N][N], f[N][N], g[N][N], q[N][N];
long long ans;

int main() {
  scanf("%d%d", &n, &m);
  FOR(i, 1, n) scanf("%s", s[i] + 1);
  ROF(i, n, 1) ROF(j, m, 1) {
    if (s[i][j] == '*')
      p[i][j] = 0;
    else if (i == n && j == m)
      p[i][j] = 1;
    else if (i == n)
      p[i][j] = p[i][j + 1];
    else if (j == m)
      p[i][j] = p[i + 1][j];
    else
      p[i][j] = p[i + 1][j] | p[i][j + 1];
  }
  FOR(i, 1, n) FOR(j, 1, m) {
    if (s[i][j] == '*')
      q[i][j] = 0;
    else if (i == 1 && j == 1)
      q[i][j] = 1;
    else if (i == 1)
      q[i][j] = q[i][j - 1];
    else if (j == 1)
      q[i][j] = q[i - 1][j];
    else
      q[i][j] = q[i - 1][j] | q[i][j - 1];
  }
  long long tot = 0;
  FOR(i, 1, n) FOR(j, 1, m) tot += s[i][j] == '.';
  if (p[1][1] == 0 || p[n][m] == 0) {
    tot = tot * (tot - 1) / 2;
    printf("%lld\n", tot);
    return 0;
  }

  int x = 1, y = 1;
  R[x][y] = 1;
  while (x != n || y != m) {
    if (p[x][y + 1])
      ++y;
    else
      ++x;
    assert(p[x][y]);
    R[x][y] = 1;
  }
  ROF(i, n, 1) ROF(j, m, 1) {
    if (s[i][j] == '*' || p[i][j] == 0)
      f[i][j] = 0;
    else if (i == n && j == m)
      f[i][j] = 1;
    else if (i == n)
      f[i][j] = R[i][j] + f[i][j + 1];
    else
      f[i][j] = R[i][j] + (p[i + 1][j] ? f[i + 1][j] : f[i][j + 1]);
  }
  FOR(i, 1, n) FOR(j, 1, m) {
    if (s[i][j] == '*' || q[i][j] == 0)
      g[i][j] = 0;
    else if (i == 1 && j == 1)
      g[i][j] = 1;
    else if (j == 1)
      g[i][j] = R[i][j] + g[i - 1][j];
    else
      g[i][j] = R[i][j] + (q[i][j - 1] ? g[i][j - 1] : g[i - 1][j]);
  }

  x = 1, y = 1;
  int cnt = 0, t = 0;
  while (x != n || y != m) {
    assert(p[x][y]);
    if (R[x][y]) { //必经点
      ans += tot - 1;
    } else {
      int px = x - 1, py = y + 1;
      while (px > 0 && (!q[px][py] || !p[px][py])) --px;
      assert(px);
      t = f[px][py] + g[px][py] - R[px][py];
      ans += t;
    }
    cnt += R[x][y];
    if (x < n && p[x + 1][y])
      ++x;
    else
      ++y;
  }
  assert(p[x][y]);
  if (R[x][y]) { //必经点
    ans += tot - 1;
  } else {
    int px = x - 1, py = y + 1;
    while (px > 0 && (!q[px][py] || !p[px][py])) --px;
    assert(px);
    t = f[px][py] + g[px][py] - R[px][py];
    ans += t;
  }

  int substract =
      f[1][1] * 1ll * (f[1][1] - 1) / 2 + f[1][1] * 1ll * (n + m - 1 - f[1][1]);
  ans -= substract;
  printf("%lld\n", ans);
  return 0;
}
