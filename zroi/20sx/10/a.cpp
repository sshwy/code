// by Sshwy
//#define DEBUGGER
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
#ifdef DEBUGGER
#define log(...)                                                    \
  {                                                                 \
    fprintf(stderr, "\033[37mLine %-3d [%dms]:\033[0m  ", __LINE__, \
        (int)clock() / 1000);                                       \
    fprintf(stderr, __VA_ARGS__);                                   \
    fprintf(stderr, "\n");                                          \
  }
#define red(...)                                                    \
  {                                                                 \
    fprintf(stderr, "\033[37mLine %-3d [%dms]:\033[0m  ", __LINE__, \
        (int)clock() / 1000);                                       \
    fprintf(stderr, "\033[31m");                                    \
    fprintf(stderr, __VA_ARGS__);                                   \
    fprintf(stderr, "\033[0m");                                     \
    fprintf(stderr, "\n");                                          \
  }
#define green(...)                                                  \
  {                                                                 \
    fprintf(stderr, "\033[37mLine %-3d [%dms]:\033[0m  ", __LINE__, \
        (int)clock() / 1000);                                       \
    fprintf(stderr, "\033[32m");                                    \
    fprintf(stderr, __VA_ARGS__);                                   \
    fprintf(stderr, "\033[0m");                                     \
    fprintf(stderr, "\n");                                          \
  }
#define blue(...)                                                   \
  {                                                                 \
    fprintf(stderr, "\033[37mLine %-3d [%dms]:\033[0m  ", __LINE__, \
        (int)clock() / 1000);                                       \
    fprintf(stderr, "\033[34m");                                    \
    fprintf(stderr, __VA_ARGS__);                                   \
    fprintf(stderr, "\033[0m");                                     \
    fprintf(stderr, "\n");                                          \
  }
#else
#define log(...) ;
#define red(...) ;
#define green(...) ;
#define blue(...) ;
#endif
/******************heading******************/
#include <bitset>
const int N = 5e5 + 5;

int f(int x, int i) { return i % (x + 1); }

int n;
int c[N];
int ans[N];

int sc[N];
int v(int l, int r) { return sc[r] ^ sc[l - 1]; }

int h[N];
int w(int j, int L, int R) {
  int t = (R - L + 1) >> j + 1;
  int l = L, r = L + (t << j + 1);
  return h[l] ^ h[r] ^ (r + (1 << j) <= R ? v(r + (1 << j), R) : 0);
}
void calc(int j) {
  red("%d", j);
  ROF(i, n, 0) {
    if (i + (1 << j) > n)
      h[i] = 0;
    else
      h[i] = (i + (1 << j + 1) <= n ? h[i + (1 << j + 1)] : 0) ^
             v(i + (1 << j), min(i + (1 << j + 1) - 1, n));
  }
  FOR(x, 1, n) {
    int tot = 0;
    for (int i = 0; i <= n; i += x + 1) {
      int t = w(j, i, min(i + x, n));
      tot ^= t;
    }
    ans[x] |= tot;
  }
}

int main() {
  scanf("%d", &n);
  FOR(i, 1, n) {
    int x;
    scanf("%d", &x);
    c[x] ^= 1;
  }

  FOR(i, 1, n) sc[i] = sc[i - 1] ^ c[i];

  FOR(j, 0, 29) { calc(j); }
  FOR(i, 1, n) printf("%s ", ans[i] ? "Alice" : "Bob");
  return 0;
}
