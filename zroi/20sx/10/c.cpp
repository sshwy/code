// by Sshwy
//#define DEBUGGER
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
namespace GraphN {
  template <const int N, const int M> class Graph {
  private:
    struct qxx {
      int nex, t;
    };
    qxx e[M];
    int h[N], le;

  public:
    class iterator {
    private:
      qxx *_e;
      int _id;

    public:
      iterator() { _e = NULL, _id = 0; }
      iterator(qxx *E, int ID) { _e = E, _id = ID; }
      bool operator!=(const iterator &it) const {
        return _id != it._id || _e != it._e;
      }
      inline int operator*() const { return (_e + _id)->t; }
      inline int operator&() const { return _id; }
      inline iterator operator~() const { return iterator(_e, _id ^ 1); }
      const iterator &operator++() { return _id = (_e + _id)->nex, *this; }
      inline iterator begin() { return *this; }
      inline iterator end() { return iterator(_e, 0); }
      inline iterator reverse_edge() {
        return iterator(_e, _id ^ 1);
      } // usually use with add_both
      inline int id() { return _id; }
    };
    vector<iterator> ei[N];
    void init() { fill(h, h + N, 0), le = 1; }
    Graph() { init(); }
    inline void add_path(int f, int t) {
      e[++le] = {h[f], t}, h[f] = le, ei[f].push_back(iterator(e, le));
    }
    inline void add_both(int f, int t) { add_path(f, t), add_path(t, f); }
    inline iterator operator[](int u) { return iterator(e, h[u]); }
    const vector<iterator> &edges(int u) { return ei[u]; }
  };
} // namespace GraphN
using GraphN::Graph;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

#ifdef DEBUGGER

#define log_prefix()                                                \
  {                                                                 \
    fprintf(stderr, "\033[37mLine %-3d [%dms]:\033[0m  ", __LINE__, \
        (int)clock() / 1000);                                       \
  }
#define ilog(...) \
  { fprintf(stderr, __VA_ARGS__); }
#define llog(...)      \
  {                    \
    log_prefix();      \
    ilog(__VA_ARGS__); \
  }
#define log(...)           \
  {                        \
    log_prefix();          \
    ilog(__VA_ARGS__);     \
    fprintf(stderr, "\n"); \
  }
#define red(...)       \
  {                    \
    log_prefix();      \
    ilog("\033[31m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define green(...)     \
  {                    \
    log_prefix();      \
    ilog("\033[32m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define blue(...)      \
  {                    \
    log_prefix();      \
    ilog("\033[34m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }

#else

#define log_prefix() ;
#define ilog(...) ;
#define llog(...) ;
#define log(...) ;
#define red(...) ;
#define green(...) ;
#define blue(...) ;

#endif

namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 1e5 + 5, M = 2e5 + 5;
typedef Graph<N, M * 2> Gr;
typedef Graph<N, M * 2>::iterator Itr;

int n, m, k;
Gr g;
bool vis[N];
bool in_tree[M * 2], ban[M * 2];
int d[20][11][N];
int f[N][11];
int sz[N];

int Dep[N], Fa[N];
bool Cut[N];

void get_st(int u, int p) {
  log("get_st(%d,%d)", u, p);
  vis[u] = 1;
  for (Itr e : g.edges(u)) {
    if (!vis[*e]) get_st(*e, u), in_tree[&e] = in_tree[&~e] = 1;
  }
}

int Size(int u, int p) {
  sz[u] = 1;
  for (Itr e : g.edges(u)) {
    if (in_tree[&e] && !Cut[*e] && *e != p) { sz[u] += Size(*e, u); }
  }
  return sz[u];
}
typedef pair<int, int> pii;
pii Core(int u, int p, const int Tsize) {
  pii res(-1, n);
  int mx = 0;
  for (Itr e : g.edges(u)) {
    if (in_tree[&e] && !Cut[*e] && *e != p) {
      pii p = Core(*e, u, Tsize);
      if (p.se < res.se) res = p;
      mx = max(mx, sz[*e]);
    }
  }
  if (max(Tsize - sz[u], mx) < res.se) res = {u, max(Tsize - sz[u], mx)};
  return res;
}
int Col[N];
void Paint(int u, int p, int col) {
  // log("Paint(%d,%d,%d)",u,p,col);
  Col[u] = col;
  for (Itr e : g.edges(u)) {
    if (in_tree[&e] && !Cut[*e] && *e != p) { Paint(*e, u, col); }
  }
}

void Find(int u, int p, vector<Itr> &S) { // find key edge
  // log("Find(%d,%d)",u,p);
  for (Itr e : g.edges(u)) {
    // log("Find(%d,%d): (%d,%d)",u,p,*e.reverse_edge(),*e);
    if (Cut[*e]) continue;
    if (in_tree[&e]) {
      if (*e != p) Find(*e, u, S);
    } else if (!ban[&e] && Col[*~e] < Col[*e]) {
      S.push_back(e);
    }
  }
}

int Cnt[N]; // count of key
int tmp[N];
int vis_tim[N], totim;
queue<int> q;
void bfs(int u, int *dist) {
  blue("bfs(%d)", u);
  ++totim;
  q.push(u);
  dist[u] = 0;
  vis_tim[u] = totim;
  while (!q.empty()) {
    int u = q.front();
    q.pop();
    log("dist[%d]=%d", u, dist[u]);
    for (Itr e : g.edges(u)) {
      const int v = *e;
      if (!ban[&e] && !Cut[v] && vis_tim[v] != totim) {
        q.push(v);
        dist[v] = dist[u] + 1;
        vis_tim[v] = totim;
      }
    }
  }
}
void Solve(int u, int p, int pcore) {
  red("Solve(%d,%d)", u, p);
  //找重心
  Size(u, p);
  int core = Core(u, p, sz[u]).fi;
  Dep[core] = Dep[pcore] + 1, Fa[core] = pcore;
  log("core=%d", core);
  //求关键点
  for (Itr e : g.edges(core)) {
    if (in_tree[&e] && !Cut[*e]) {
      log("GG");
      Paint(*e, core, *e);
    }
  }
  vector<Itr> S;
  Find(core, 0, S);
  llog("S: ");
  for (Itr e : S) ilog("%d:(%d,%d) ", &e, *~e, *e);
  ilog("\n");
  // Bfs求d[u]
  bfs(core, d[Dep[core]][0]);
  Cnt[core] = S.size();
  for (int i = 0; i < S.size(); i++) { bfs(*S[i], d[Dep[core]][i + 1]); }
  for (Itr e : S) ban[&e] = ban[&~e] = 1;
  //递归
  Cut[core] = 1;
  for (Itr e : g.edges(core)) {
    if (in_tree[&e] && !Cut[*e]) { Solve(*e, core, core); }
  }
}
int main() {
  memset(d, 0x3f, sizeof(d));
  memset(f, 0x3f, sizeof(f));

  scanf("%d%d%d", &n, &m, &k);
  FOR(i, 1, m) {
    int x, y;
    scanf("%d%d", &x, &y);
    g.add_both(x, y);
  }

  get_st(1, 0);
  memset(vis, 0, sizeof(vis));
  Solve(1, 0, 0);

  llog("Fa: ");
  FOR(i, 1, n) ilog("%d%c", Fa[i], " \n"[i == n]);

  FOR(i, 1, n) {
    green("i=%d", i);
    for (int u = i; u; u = Fa[u]) {
      llog("u=%d,Dep=%d,Cnt=%d: ", u, Dep[u], Cnt[u]);
      FOR(k, 0, Cnt[u]) { ilog("%d%c", d[Dep[u]][k][i], " \n"[k == Cnt[u]]); }
    }
  }

  blue("");
  blue("==========[ QUERY ]==========");
  blue("");

  int q;
  scanf("%d", &q);
  FOR(i, 1, q) {
    int t, v;
    scanf("%d%d", &t, &v);
    if (t == 1) {
      green("mark %d", v);
      for (int u = v; u; u = Fa[u]) {
        log("u=%d", u);
        FOR(j, 0, Cnt[u]) { f[u][j] = min(f[u][j], d[Dep[u]][j][v]); }
      }
    } else {
      red("query %d", v);
      int ans = 0x3f3f3f3f;
      for (int u = v; u; u = Fa[u]) {
        log("u=%d", u);
        FOR(j, 0, Cnt[u]) { ans = min(ans, d[Dep[u]][j][v] + f[u][j]); }
      }
      printf("%d\n", ans);
    }
  }

  return 0;
}
// 点分治。
// 随便找一棵生成树。
// 找重心core
// 有一些非树边会横跨子树，但这样的边数量不超过 k。
// 考虑当前重心下的一个询问
// 这个询问的路径
// a. 要么经过重心
// b. 要么经过k条边之一
// c. 要么就子树递归解决
// 我们取重心和这k条边的任意一个端点，共k+1个点为关键点
// 预处理每个点到关键点的距离，预处理距离关键点最近的 marked 点的距离
// 就可以回答关于当前重心的询问
// 搞出点分树就可以在线并支持修改了
//
// 由于点分树上每个点的深度是log的，则父亲不超过log个，因此和这个点有关的关键点不超过klog个
// 则我们可以记d[j,k,i]表示结点i与它深度为j的父亲的第k个关键点的距离
// 记f[i,j]表示距离结点i第j个关键点最近的 marked 点的距离
// 修改和查询都是klog的
//
// 总复杂度O(nklogn)
