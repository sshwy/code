// by Sshwy
//#define DEBUGGER
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

#ifdef DEBUGGER
#define log(...)                                                    \
  {                                                                 \
    fprintf(stderr, "\033[37mLine %-3d [%dms]:\033[0m  ", __LINE__, \
        (int)clock() / 1000);                                       \
    fprintf(stderr, __VA_ARGS__);                                   \
    fprintf(stderr, "\n");                                          \
  }
#define red(...)                                                    \
  {                                                                 \
    fprintf(stderr, "\033[37mLine %-3d [%dms]:\033[0m  ", __LINE__, \
        (int)clock() / 1000);                                       \
    fprintf(stderr, "\033[31m");                                    \
    fprintf(stderr, __VA_ARGS__);                                   \
    fprintf(stderr, "\033[0m");                                     \
    fprintf(stderr, "\n");                                          \
  }
#define green(...)                                                  \
  {                                                                 \
    fprintf(stderr, "\033[37mLine %-3d [%dms]:\033[0m  ", __LINE__, \
        (int)clock() / 1000);                                       \
    fprintf(stderr, "\033[32m");                                    \
    fprintf(stderr, __VA_ARGS__);                                   \
    fprintf(stderr, "\033[0m");                                     \
    fprintf(stderr, "\n");                                          \
  }
#define blue(...)                                                   \
  {                                                                 \
    fprintf(stderr, "\033[37mLine %-3d [%dms]:\033[0m  ", __LINE__, \
        (int)clock() / 1000);                                       \
    fprintf(stderr, "\033[34m");                                    \
    fprintf(stderr, __VA_ARGS__);                                   \
    fprintf(stderr, "\033[0m");                                     \
    fprintf(stderr, "\n");                                          \
  }
#else
#define log(...) ;
#define red(...) ;
#define green(...) ;
#define blue(...) ;
#endif

namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/

using namespace RA;

int main() {
  srand(clock() + time(0));
  int n = 40, m = 40;
  printf("%d %d\n", n, m);
  FOR(i, 1, n) {
    FOR(j, 1, m) printf("%c", r(100) < 95 ? '.' : '*');
    puts("");
  }

  return 0;
}
