// by Sshwy
//#define DEBUGGER
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

#ifdef DEBUGGER

#define log_prefix()                                                \
  {                                                                 \
    fprintf(stderr, "\033[37mLine %-3d [%dms]:\033[0m  ", __LINE__, \
        (int)clock() / 1000);                                       \
  }
#define ilog(...) \
  { fprintf(stderr, __VA_ARGS__); }
#define llog(...)      \
  {                    \
    log_prefix();      \
    ilog(__VA_ARGS__); \
  }
#define log(...)           \
  {                        \
    log_prefix();          \
    ilog(__VA_ARGS__);     \
    fprintf(stderr, "\n"); \
  }
#define red(...)       \
  {                    \
    log_prefix();      \
    ilog("\033[31m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define green(...)     \
  {                    \
    log_prefix();      \
    ilog("\033[32m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define blue(...)      \
  {                    \
    log_prefix();      \
    ilog("\033[34m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }

#else

#define log_prefix() ;
#define ilog(...) ;
#define llog(...) ;
#define log(...) ;
#define red(...) ;
#define green(...) ;
#define blue(...) ;

#endif

namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/

using namespace RA;
const int N = 100;
bool vis[N][N];
vector<int> v;
int main() {
  srand(clock());
  int C = 3;
  int n = 10, m = n - 1 + C, k = 10;
  printf("%d %d %d\n", n, m, k);
  FOR(i, 2, n) {
    int x = r(1, i - 1);
    vis[x][i] = vis[i][x] = 1;
    printf("%d %d\n", x, i);
  }
  FOR(i, 1, C) {
    int x = 1, y = 2;
    while (vis[x][y] || x == y) x = r(1, n), y = r(1, n);
    printf("%d %d\n", x, y);
    vis[x][y] = vis[y][x] = 1;
  }
  int q = 50;
  printf("%d\n", q);
  FOR(i, 1, n) v.pb(i);
  random_shuffle(v.begin(), v.end());
  FOR(i, 1, q) {
    if (v.size() == n || (v.size() && r(100) < 30)) {
      printf("1 %d\n", v[v.size() - 1]);
      v.pop_back();
    } else {
      printf("2 %d\n", r(1, n));
    }
  }
  return 0;
}
