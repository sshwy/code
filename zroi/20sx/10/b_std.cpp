// Love and Freedom.
#include <algorithm>
#include <cmath>
#include <cstdio>
#include <cstring>
#include <vector>
#define ll long long
#define inf 20021225
#define N 5010
#define pa pair<int, int>
#define mp make_pair
using namespace std;
int read() {
  int s = 0, t = 1;
  char ch = getchar();
  while (ch < '0' || ch > '9') {
    if (ch == '-') t = -1;
    ch = getchar();
  }
  while (ch >= '0' && ch <= '9') s = s * 10 + ch - '0', ch = getchar();
  return s * t;
}
bool pvis[N][N], svis[N][N], gg[N][N];
int n, m, cnt;
char ch[N];
void init() {
  for (int i = 1; i <= n; i++)
    for (int j = 1; j <= m; j++)
      if (gg[i][j])
        continue;
      else if (i == 1 && j == 1)
        pvis[i][j] = 1;
      else
        pvis[i][j] = pvis[i - 1][j] | pvis[i][j - 1];
  for (int i = n; i; i--)
    for (int j = m; j; j--)
      if (gg[i][j])
        continue;
      else if (i == n && j == m)
        svis[i][j] = 1;
      else
        svis[i][j] = svis[i + 1][j] | svis[i][j + 1];
}
vector<pa> path, htap;
bool op[N][N], oh[N][N];
bool getpath() {
  if (!svis[1][1]) return 0;
  int tx = 1, ty = 1;
  while (tx != n || ty != m) {
    path.push_back(mp(tx, ty)), op[tx][ty] = 1;
    if (svis[tx + 1][ty])
      tx++;
    else
      ty++;
  }
  path.push_back(mp(tx, ty)), op[tx][ty] = 1;
  return 1;
}
void gethtap() {
  int tx = 1, ty = 1;
  while (tx != n || ty != m) {
    oh[tx][ty] = 1;
    if (svis[tx][ty + 1])
      ty++;
    else
      tx++;
  }
  oh[tx][ty] = 1;
}
int wei;
int solve(int x, int y) {
  if (oh[x][y]) {
    wei++;
    return cnt - wei;
  }
  for (int i = y + 1; i <= m; i++)
    if (pvis[x - 1][i] && svis[x][i]) {
      y = i;
      break;
    }
  int tx = x, ty = y, tmp = 0;
  tx--;
  while (tx != 1 || ty != 1) {
    if (oh[tx][ty] && (!op[tx][ty])) tmp++;
    if (pvis[tx][ty - 1])
      ty--;
    else
      tx--;
  }
  if (oh[tx][ty] && (!op[tx][ty])) tmp++;
  tx = x, ty = y;
  while (tx != n || ty != m) {
    if (oh[tx][ty] && (!op[tx][ty])) tmp++;
    if (svis[tx + 1][ty])
      tx++;
    else
      ty++;
  }
  if (oh[tx][ty] && (!op[tx][ty])) tmp++;
  return tmp;
}
int main() {
  n = read(), m = read();
  cnt = n * m;
  for (int i = 1; i <= n; i++) {
    scanf("%s", ch + 1);
    for (int j = 1; j <= m; j++) gg[i][j] = ch[j] == '*', cnt -= gg[i][j];
  }
  init();
  ll ans = 0;
  if (!getpath()) return printf("%lld\n", 1ll * cnt * (cnt - 1) / 2), 0;
  gethtap();
  for (int i = 0; i < path.size(); i++) ans += solve(path[i].first, path[i].second);
  printf("%lld\n", ans);
  return 0;
}
