#include <bits/stdc++.h>
using namespace std;

const int maxn = 1e5, maxm = 4e5, inf = 1e9;
int n, m, k, q, tot, ter[maxm + 3], nxt[maxm + 3], lnk[maxn + 3];
int all, cent, sz[maxn + 3], mx[maxn + 3], col[maxn + 3], cur, num[maxn + 3];
int fa[maxn + 3], dep[maxn + 3], cnt[maxn + 3], dist[17][11][maxn + 3],
    mn[maxn + 3][11];
bool ban[maxm + 3], vis[maxn + 3], ok[maxm + 3];
vector<int> S, T;

void add(int u, int v) { ter[++tot] = v, nxt[tot] = lnk[u], lnk[u] = tot; }
inline int rev(int x) { return x & 1 ? x + 1 : x - 1; }

void work(int u, int from = 0) {
  vis[u] = true;
  for (int i = lnk[u], v; i; i = nxt[i]) {
    v = ter[i];
    if (rev(i) == from) continue;
    if (!vis[v]) ok[i] = ok[rev(i)] = true, work(v, i);
  }
}

void centroid(int u, int from = 0) {
  sz[u] = 1, mx[u] = 0;
  for (int i = lnk[u], v; i; i = nxt[i]) {
    v = ter[i];
    if (!ok[i] || rev(i) == from || vis[v]) continue;
    centroid(v, i), sz[u] += sz[v], mx[u] = max(mx[u], sz[v]);
  }
  mx[u] = max(mx[u], all - sz[u]);
  if (!cent || mx[u] < mx[cent]) cent = u;
}

void dfs(int u, int c, int from = 0) {
  sz[u] = 1, col[u] = c;
  for (int i = lnk[u], v; i; i = nxt[i]) {
    v = ter[i];
    if (!ok[i] || rev(i) == from || vis[v]) continue;
    dfs(v, c, i), sz[u] += sz[v];
  }
}

void find(int u, int from = 0) {
  for (int i = lnk[u], v; i; i = nxt[i]) {
    v = ter[i];
    if (ok[i] && rev(i) != from && !vis[v]) find(v, i);
    if (!ok[i] && !ban[i] && col[u] < col[v]) S.push_back(u), T.push_back(i);
  }
}

void bfs(int x, int i) {
  int d = dep[cent];
  queue<int> Q;
  Q.push(x);
  int *cist = dist[d][i];
  cist[x] = 0, num[x] = ++cur;
  while (!Q.empty()) {
    int u = Q.front();
    Q.pop();
    for (int i = lnk[u], v; i; i = nxt[i]) {
      v = ter[i];
      if (vis[v] || ban[i]) continue;
      if (num[v] != cur) num[v] = cur, cist[v] = cist[u] + 1, Q.push(v);
    }
  }
}

void solve(int u, int m = n, int f = 0) {
  all = m, cent = 0, centroid(u);
  u = cent, fa[u] = f, dep[u] = f ? dep[f] + 1 : 0;
  col[u] = u, sz[u] = 1;
  for (int i = lnk[u], v; i; i = nxt[i]) {
    v = ter[i];
    if (ok[i] && !vis[v]) dfs(v, v, i), sz[u] += sz[v];
  }
  vector<int>().swap(S), vector<int>().swap(T);
  find(u), assert(S.size() <= k), S.push_back(u), cnt[u] = S.size();
  for (int i = 0; i < S.size(); i++) bfs(S[i], i);
  for (int i = 0; i < T.size(); i++) ban[T[i]] = true, ban[rev(T[i])] = true;
  vis[u] = true;
  for (int i = lnk[u], v; i; i = nxt[i]) {
    v = ter[i];
    if (ok[i] && !vis[v]) solve(v, sz[v], u);
  }
}

int main() {
  scanf("%d %d %d", &n, &m, &k);
  for (int i = 1, u, v; i <= m; i++) {
    scanf("%d %d", &u, &v);
    add(u, v), add(v, u);
  }
  work(1);
  memset(vis, false, sizeof(vis));
  solve(1);
  memset(mn, 0x3f, sizeof(mn));
  scanf("%d", &q);
  for (int t, x; q-- > 0;) {
    scanf("%d %d", &t, &x);
    if (t == 1) {
      for (int i = dep[x], y = x; ~i; i--, y = fa[y]) {
        for (int j = 0; j < cnt[y]; j++) mn[y][j] = min(mn[y][j], dist[i][j][x]);
      }
    } else {
      int res = inf;
      for (int i = dep[x], y = x; ~i; i--, y = fa[y]) {
        for (int j = 0; j < cnt[y]; j++) res = min(res, mn[y][j] + dist[i][j][x]);
      }
      printf("%d\n", res);
    }
  }
  return 0;
}
