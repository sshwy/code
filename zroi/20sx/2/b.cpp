// by Sshwy
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int D = 70;

long long R;
int a[D], la;
int b[D];
long long f[2 /*滚动*/][2 /*高位是否接受进位*/][2 /*低位是否向下一位进位*/]
           [2 /*高位的部分小于限制(1)or等于限制*/]
           [2 /*低位的部分大于限制(1)or小于等于限制*/];
long long g[2][2][2][2][2]; // f记方案数，g记权值（1的个数）

long long calc(int len, int *lim) { //数字长度为len，限制lim[0..len-1]
  memset(f, 0, sizeof(f)), memset(g, 0, sizeof(g));
  int cur = 0; //当前的dp值
  f[0][0][0][0][0] = f[0][1][0][0][0] = g[0][1][0][0][0] =
      1; //可能不进位，也可能最高位还进一位上去，这时会有一个1的贡献。
  for (int l = 0, r = len - 1; l < r; l++, r--) {
    cur ^= 1; //从f[cur^1]转移到f[cur]
    memset(f[cur], 0, sizeof(f[cur])), memset(g[cur], 0, sizeof(g[cur]));
    FOR(a, 0, 1) FOR(b, 0, 1) FOR(c, 0, 1) FOR(d, 0, 1) {
      if (!f[cur ^ 1][a][b][c][d]) continue;
      long long F = f[cur ^ 1][a][b][c][d], G = g[cur ^ 1][a][b][c][d];
      FOR(L, 0, 1) FOR(R, 0, 1) { //高位的数字r填R，低位l填L
        //填的数字的限制：最高位不能是0；高位不能大于限制
        if (r == len - 1 && R == 0) continue;
        if (c == 0 && R > lim[r]) continue;
        //填完数字，可以算出新的高低位限制状态
        int nc = c != 0 || R < lim[r];
        int nd = L > lim[l] || (L == lim[l] && d == 1);
        //枚举r这一位是否接收一个进位
        FOR(na, 0, 1) {
          //那么必须要满足a（表示r+1是否接受进位）
          if ((R + na + L >= 2) != a) continue;
          //然后可以算出低位是否向下一位进位，并计算出新増的1的个数(l,r这两个位)
          int nb = L + b + R >= 2;
          long long v = (na ^ L ^ R) + (L ^ R ^ b);
          //转移
          f[cur][na][nb][nc][nd] += F;
          g[cur][na][nb][nc][nd] += G + F * v;
        }
      }
    }
  }
  //算答案
  long long res = 0;
  if (len & 1) { //长度为奇数，枚举中间的数填啥
    FOR(a, 0, 1) FOR(b, 0, 1) FOR(c, 0, 1) FOR(d, 0, 1) FOR(e, 0, 1) {
      if (!f[cur][a][b][c][d]) continue;
      //要满足进位，不能超过限制
      if ((e + e + b >= 2) != a) continue;
      if (c == 0 && (e > lim[len / 2] || (e == lim[len / 2] && d == 1))) continue;
      res += g[cur][a][b][c][d] + f[cur][a][b][c][d] * (e ^ e ^ b);
    }
  } else {
    FOR(ab, 0, 1) FOR(c, 0, 1) FOR(d, 0, 1) { // a==b
      if (!f[cur][ab][ab][c][d]) continue;
      if (c == 0 && d == 1) continue; //不能超过限制
      res += g[cur][ab][ab][c][d];
    }
  }
  return res;
}

int main() {
  scanf("%lld", &R);
  while (R) a[la++] = R & 1, R /= 2;
  FOR(i, 0, la - 1) b[i] = 1;
  long long ans = 0;
  FOR(i, 1, la - 1) ans += calc(i, b);
  ans += calc(la, a);
  printf("%lld\n", ans);
  return 0;
}
