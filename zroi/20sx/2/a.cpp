// by Sshwy
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int P = 998244353, N = 1e5 + 5, SZ = 1 << 18;
int pw(int a, int m) {
  int res = 1;
  while (m) m & 1 ? res = 1ll * res * a % P : 0, a = 1ll * a * a % P, m >>= 1;
  return res;
}

int tr[SZ];
int init(int n) {
  int len = 1;
  while (len < n) len <<= 1;
  FOR(i, 0, len - 1) tr[i] = tr[i >> 1] >> 1 | ((i & 1) * (len >> 1));
  return len;
}
void dft(int *f, int len, int tag) {
  FOR(i, 0, len - 1) f[i] = (f[i] + P) % P;
  FOR(i, 0, len - 1) if (i < tr[i]) swap(f[i], f[tr[i]]);
  for (int j = 1; j < len; j <<= 1)
    for (int i = 0, wn = pw(3, P - 1 + (P - 1) / (j << 1) * tag); i < len;
         i += j << 1)
      for (int k = i, u, v, w = 1; k < i + j; k++, w = 1ll * w * wn % P)
        u = f[k], v = f[k + j] * 1ll * w % P, f[k] = (u + v) % P,
        f[k + j] = (u - v + P) % P;
  if (tag == -1) {
    int ilen = pw(len, P - 2);
    FOR(i, 0, len - 1) f[i] = 1ll * f[i] * ilen % P;
  }
}

int n, x, y, p;
int fac[N], fnv[N];

int binom(int n, int m) {
  if (n < m) return 0;
  return fac[n] * 1ll * fnv[m] % P * fnv[n - m] % P;
}
int f[N], g[N];
int A[SZ], B[SZ];

void solve(int l, int r) {
  if (l == r) {
    if (l == 1)
      f[1] = 0;
    else
      f[l] = ((f[l] * 1ll * fac[l] % P + 1) * 1ll *
                     pw((1 - pw(p, l) - pw(1 - p, l)) % P, P - 2) % P +
                 P) %
             P;
    return;
  }
  int mid = (l + r) >> 1;
  solve(l, mid);

  // f[l..mid] -> f[i], i in [mid+1..r]
  // g[1..r-l]
  int len = init(mid - l + 1 + r - l);
  memset(A, 0, sizeof(int) * len), memset(B, 0, sizeof(int) * len);
  FOR(i, 0, mid - l) A[i] = f[i + l] * 1ll * pw(p, i + l) % P * fnv[i + l] % P;
  FOR(i, 0, r - l - 1) B[i] = g[i + 1];

  dft(A, len, 1), dft(B, len, 1);
  FOR(i, 0, len - 1) A[i] = 1ll * A[i] * B[i] % P;
  dft(A, len, -1);

  FOR(i, mid + 1, r) f[i] = (f[i] + A[i - l - 1]) % P;

  solve(mid + 1, r);
}
int main() {
  scanf("%d%d%d", &n, &x, &y);
  p = x * 1ll * pw(y, P - 2) % P;

  fac[0] = 1;
  FOR(i, 1, n) fac[i] = 1ll * fac[i - 1] * i % P;
  fnv[n] = pw(fac[n], P - 2);
  ROF(i, n, 1) fnv[i - 1] = 1ll * fnv[i] * i % P;

  FOR(i, 1, n) g[i] = pw(1 - p, i) * 1ll * fnv[i] % P;
  FOR(i, 1, n) g[i] = (g[i] + P) % P;

  solve(1, n);

  printf("%d\n", f[n]);
  return 0;
}
