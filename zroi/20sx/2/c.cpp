// by Sshwy
// 30pts
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 3e5 + 5;

int n, q;
int fa[N], big[N];
int tp[N];

unsigned w[N], sz[N], f1[N], f2[N], tag[N], son_S[N],
    G[N]; // son_S表示所有轻儿子的S(v)(sv[v]+1)的和
vector<int> g[N];
int L[N], R[N], dfn; //子树对应的区间,L[u]是u的dfn序

// fenwick 维护S(u)
unsigned c[N];
void add(int pos, unsigned val) {
  for (int i = pos; i <= n; i += i & -i) c[i] += val;
}
unsigned pre(int pos) {
  unsigned res = 0;
  for (int i = pos; i > 0; i -= i & -i) res += c[i];
  return res;
}

inline unsigned S(int u) { return pre(R[u]) - pre(L[u] - 1); }
inline unsigned SS(int u) {
  return son_S[u] + (big[u] ? S(big[u]) * (sz[big[u]] + 1) : 0);
}
inline unsigned H(int u) { return sz[u] - 1 + f1[u] - 1; }

void dfs1(int u) {
  sz[u] = 1, f1[u] = f2[u] = 1;
  for (int v : g[u])
    dfs1(v), sz[u] += sz[v], f1[u]++, f2[u] += f1[v], G[u] += f1[v] * (sz[v] + 1),
        sz[v] > sz[big[u]] ? big[u] = v : 0;
}
void dfs2(int u, int topp) {
  L[u] = ++dfn, tp[u] = topp;
  if (big[u]) dfs2(big[u], topp);
  for (int v : g[u])
    if (v != big[u]) dfs2(v, v); // son_S[u]+=S(v)*(sz[v]+1);
  R[u] = dfn;
}
void modify(int u, unsigned val) {
  // printf("modify(%d,%u)\n",u,val);

  for (int x = tp[u]; x; x = tp[fa[x]]) { son_S[fa[x]] -= S(x) * (sz[x] + 1); }

  add(L[u], val * f2[u]);

  for (int x = tp[u]; x; x = tp[fa[x]]) { son_S[fa[x]] += S(x) * (sz[x] + 1); }

  tag[u] += val;
}
inline unsigned query(int u) {
  return (S(u) + tag[fa[u]] * f1[u] + tag[fa[fa[u]]]) * (sz[u] + 1) - SS(u) -
         tag[u] * G[u] - tag[fa[u]] * H(u);
}
int main() {
  scanf("%d%d", &n, &q);
  FOR(i, 2, n) {
    scanf("%d", &fa[i]);
    g[fa[i]].pb(i);
  }
  dfs1(1);
  dfs2(1, 1);
  FOR(i, 1, n) scanf("%d", &w[i]), add(L[i], w[i]);
  FOR(i, 1, n) if (big[fa[i]] != i) son_S[fa[i]] += S(i) * (sz[i] + 1);
  FOR(i, 1, q) {
    int o, x, y;
    scanf("%d%d", &o, &x);
    if (o == 1) {
      scanf("%d", &y);
      modify(x, y);
    } else
      printf("%u\n", query(x));
  }
  return 0;
}
