// by Sshwy
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/

long long R;

long long g(long long x) {
  long long c[100], lc = 0;
  while (x) c[++lc] = x & 1, x >>= 1;
  long long res = 0;
  FOR(i, 1, lc) res = res * 2 + c[i];
  return res;
}
inline int f(long long x) { return __builtin_popcountll(x); }

int main() {
  freopen("table.txt", "w", stdout);
  int T = 5e6;
  scanf("%lld", &R);
  long long ans = 0;
  printf("0,");
  FOR(i, 1, R) {
    ans += f(i + g(i));
    if (i % T == 0) {
      printf("%lld,", ans);
      cerr << "i=" << i << endl;
    }
  }
  return 0;
}
