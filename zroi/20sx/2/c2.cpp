// by Sshwy
// 30pts
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 3e5 + 5;

int n, q;
int fa[N];

unsigned w[N], sz[N];
vector<int> g[N];

void modify(int u, unsigned val) {
  // printf("modify(%d,%u)\n",u,val);
  w[u] += val;
  for (int v : g[u]) {
    w[v] += val;
    for (int x : g[v]) w[x] += val;
  }
}
void dfs1(int u) {
  sz[u] = 1;
  for (int v : g[u]) dfs1(v), sz[u] += sz[v];
}
unsigned dfs(int u) {
  unsigned res = w[u];
  for (int v : g[u]) res += dfs(v);
  return res;
}
unsigned query(int u) {
  unsigned sum = w[u], res = 0;
  for (int v : g[u]) {
    unsigned x = dfs(v);
    res -= x * (sz[v] + 1);
    sum += x;
  }
  res += sum * (sz[u] + 1);
  return res;
}
int main() {
  scanf("%d%d", &n, &q);
  FOR(i, 2, n) {
    scanf("%d", &fa[i]);
    g[fa[i]].pb(i);
    // printf("%d -> %d\n",fa[i],i);
  }
  dfs1(1);
  FOR(i, 1, n) scanf("%d", &w[i]);
  FOR(i, 1, q) {
    int o, x, y;
    scanf("%d%d", &o, &x);
    if (o == 1) {
      scanf("%d", &y);
      modify(x, y);
    } else
      printf("%u\n", query(x));
  }
  return 0;
}
