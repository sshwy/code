// by Sshwy
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/

using namespace RA;

int main() {
  srand(clock());
  int n = r(5, 7), q = r(10, 12), LIM = 20;
  printf("%d %d\n", n, q);
  FOR(i, 2, n) printf("%d%c", r(1, i - 1), " \n"[i == n]);
  FOR(i, 1, n) printf("%d%c", 0, " \n"[i == n]);
  FOR(i, 1, q) {
    if (r(100) < 50) {
      printf("%d %d %d\n", 1, r(1, n), 1);
    } else {
      printf("%d %d\n", 2, r(1, n));
    }
  }
  return 0;
}
