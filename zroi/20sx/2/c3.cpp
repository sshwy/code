// problem:zr1221
#include <bits/stdc++.h>
using namespace std;

#define pb push_back
#define mk make_pair
#define lob lower_bound
#define upb upper_bound
#define fst first
#define scd second

typedef unsigned int uint;
typedef long long ll;
typedef unsigned long long ull;
typedef pair<int, int> pii;
typedef pair<ll, ll> pll;
typedef pair<int, ll> pil;
typedef pair<ll, int> pli;

namespace Fread {
  const int MAXN = 1 << 20;
  char buffer[MAXN], *S, *T;
  inline char getchar() {
    if (S == T) {
      T = (S = buffer) + fread(buffer, 1, MAXN, stdin);
      if (S == T) return EOF;
    }
    return *S++;
  }
} // namespace Fread
#ifdef ONLINE_JUDGE
#define getchar Fread::getchar
#endif
inline int read() {
  int f = 1, x = 0;
  char ch = getchar();
  while (!isdigit(ch)) {
    if (ch == '-') f = -1;
    ch = getchar();
  }
  while (isdigit(ch)) {
    x = x * 10 + ch - '0';
    ch = getchar();
  }
  return x * f;
}
inline ll readll() {
  ll f = 1, x = 0;
  char ch = getchar();
  while (!isdigit(ch)) {
    if (ch == '-') f = -1;
    ch = getchar();
  }
  while (isdigit(ch)) {
    x = x * 10 + ch - '0';
    ch = getchar();
  }
  return x * f;
}

const int MAXN = 3e5 + 5;
vector<int> G[MAXN];
uint val[MAXN], sum[MAXN], sz[MAXN], ct2[MAXN], f[MAXN], tag[MAXN], w1[MAXN],
    w2[MAXN];
int n, q, fa[MAXN], son[MAXN], dep[MAXN];
void dfs1(int u) {
  dep[u] = dep[fa[u]] + 1;
  sum[u] = val[u];
  sz[u] = 1;
  for (int i = 0; i < (int)G[u].size(); ++i) {
    int v = G[u][i];
    dfs1(v);
    sum[u] += sum[v];
    sz[u] += sz[v];
    if (!son[u] || sz[v] > sz[son[u]]) son[u] = v;
  }
  f[u] = val[u] * (sz[u] + 1);
  w1[u] = w2[u] = sz[u] + 1;
  for (int i = 0; i < (int)G[u].size(); ++i) {
    int v = G[u][i];
    f[u] += sum[v] * (sz[u] - sz[v]);
    w1[u] += sz[u] - sz[v];
    w2[u] += (sz[u] - sz[v]) * (1 + G[v].size());
  }
}
int top[MAXN], dfn[MAXN], ofn[MAXN], idx, rev[MAXN];
void dfs2(int u, int t) {
  top[u] = t;
  dfn[u] = ++idx;
  rev[idx] = u;
  if (son[u]) dfs2(son[u], t);
  for (int i = 0; i < (int)G[u].size(); ++i) {
    int v = G[u][i];
    if (v == son[u]) continue;
    dfs2(v, v);
  }
  ofn[u] = idx;
}
int rt[MAXN];
struct FrogTree {
  int cnt, ls[MAXN * 40], rs[MAXN * 40];
  uint sum[MAXN * 40];
  void modify(int &x, int y, int l, int r, int pos) {
    x = ++cnt;
    sum[x] = sum[y] + 1, ls[x] = ls[y], rs[x] = rs[y];
    if (l == r) return;
    int mid = (l + r) >> 1;
    if (pos <= mid)
      modify(ls[x], ls[y], l, mid, pos);
    else
      modify(rs[x], rs[y], mid + 1, r, pos);
  }
  uint query(int x, int y, int l, int r, int ql, int qr) {
    if (ql <= l && qr >= r) return sum[x] - sum[y];
    int mid = (l + r) >> 1;
    uint res = 0;
    if (ql <= mid) res = query(ls[x], ls[y], l, mid, ql, qr);
    if (qr > mid) res += query(rs[x], rs[y], mid + 1, r, ql, qr);
    return res;
  }
  FrogTree() {}
} T1;
struct SegmentTree {
  uint c[MAXN];
  void modify(int p, uint x) {
    for (; p <= n; p += (p & (-p))) c[p] += x;
  }
  uint query(int p) {
    uint res = 0;
    for (; p; p -= (p & (-p))) res += c[p];
    return res;
  }
  SegmentTree() {}
} T2;
int main() {
  n = read();
  q = read();
  for (int i = 2; i <= n; ++i) fa[i] = read(), G[fa[i]].pb(i);
  for (int i = 1; i <= n; ++i) val[i] = read();
  dfs1(1), dfs2(1, 1);
  for (int i = 1; i <= n; ++i) T1.modify(rt[i], rt[i - 1], 1, n, dep[rev[i]]);
  for (int i = 1; i <= n; ++i)
    ct2[i] = T1.query(rt[ofn[i]], rt[dfn[i] - 1], 1, n, dep[i], dep[i] + 2);
  while (q--) {
    int op = read();
    if (op == 1) {
      int u = read();
      uint x = read();
      tag[u] += x;
      f[u] += w2[u] * x;
      T2.modify(dfn[u], ct2[u] * x);
      while (fa[top[u]]) {
        f[fa[top[u]]] += (sz[fa[top[u]]] - sz[top[u]]) * ct2[u] * x;
        u = fa[top[u]];
      }
    } else {
      int u = read();
      uint res = f[u];
      if (son[u])
        res += (sz[u] - sz[son[u]]) *
               (T2.query(ofn[son[u]]) - T2.query(dfn[son[u]] - 1));
      if (fa[u]) res += tag[fa[u]] * w1[u];
      if (fa[fa[u]]) res += tag[fa[fa[u]]] * (sz[u] + 1);
      printf("%u\n", res);
    }
  }
  return 0;
}
