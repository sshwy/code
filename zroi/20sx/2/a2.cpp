// by Sshwy
// 50pts
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int P = 998244353, N = 5005;

int pw(int a, int m) {
  int res = 1;
  while (m) m & 1 ? res = 1ll * res * a % P : 0, a = 1ll * a * a % P, m >>= 1;
  return res;
}

int n, x, y, p;
int fac[N], fnv[N];

int binom(int n, int m) {
  if (n < m) return 0;
  return fac[n] * 1ll * fnv[m] % P * fnv[n - m] % P;
}
int w[N][N];
int f[N];

int main() {
  scanf("%d%d%d", &n, &x, &y);
  p = x * 1ll * pw(y, P - 2) % P;

  fac[0] = 1;
  FOR(i, 1, n) fac[i] = 1ll * fac[i - 1] * i % P;
  fnv[n] = pw(fac[n], P - 2);
  ROF(i, n, 1) fnv[i - 1] = 1ll * fnv[i] * i % P;

  FOR(i, 1, n) {
    FOR(j, 1, i - 1) {
      w[i][j] = pw(P + 1 - p, i - j) * 1ll * pw(p, j) % P * binom(i, j) % P;
    }
    w[i][i] = (pw(p, i) + pw(P + 1 - p, i)) % P;
  }

  f[1] = 0;
  FOR(i, 2, n) {
    int s = 0;
    FOR(j, 1, i - 1) s = (s + w[i][j] * 1ll * f[j]) % P;
    printf("i=%d,s=%d\n", i, s);
    s = (s + 1) % P;
    s = 1ll * s * pw(P + 1 - w[i][i], P - 2) % P;
    f[i] = s;
  }

  FOR(i, 1, n) printf("f[%d]=%d\n", i, f[i]);
  printf("%d\n", f[n]);

  return 0;
}
