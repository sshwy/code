// by Sshwy
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int P = 998244353;

int check(int *a, int n) {
  int nex[20] = {0};
  FOR(i, 1, n) {
    nex[i] = n + 1;
    FOR(j, i + 1, n) if (a[i] < a[j]) {
      nex[i] = j;
      break;
    }
  }
  int cnt = 0;
  FOR(i, 1, n) {
    FOR(j, nex[i], n) if (a[i] < a[j]) {
      FOR(k, nex[j], n) if (a[j] < a[k]) {
        ++cnt;
        if (cnt > 3) return cnt;
      }
    }
  }
  return cnt;
}
int calc(int *a, int n) {
  int res = 0;
  FOR(i, 1, n) FOR(j, i + 1, n) res += a[i] > a[j];
  return res;
}
void calc(int n) {
  int p[20], ans[4] = {0};
  FOR(i, 1, n) p[i] = i;
  do {
    int t = check(p, n);
    if (t <= 3) {
      // printf("t=%d ",t);FOR(i,1,n)printf("%d%c",p[i]," \n"[i==n]);
      ans[t] += calc(p, n), ans[t] -= ans[t] >= P ? P : 0;
    }
  } while (next_permutation(p + 1, p + n + 1));
  FOR(i, 0, 3) printf("ans[%d][%d]=%d;\n", n, i, ans[i]);
}
int main() {
  FOR(i, 1, 15) calc(i);
  return 0;
}
