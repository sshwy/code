// by Sshwy
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int M = 1e5 + 5, SZ = 32 * M * 3 * 4;

int n, m, X, Y;
struct atom {
  int c, l, r;
} A[M];
bool cmp(atom x, atom y) { return x.r < y.r; }

long long f[M]; // f[i]最后一个区间选i的最大价值

int lc[SZ], rc[SZ], tot;
long long mx[SZ], tag[SZ];

int new_node() {
  ++tot;
  mx[tot] = tag[tot] = -1e18, lc[tot] = rc[tot] = 0;
  return tot;
}
void assign_max(int L, int R, long long v, int &u, int l = 1, int r = n) {
  if (!u) u = new_node(); // new node
  if (L <= l && r <= R) return mx[u] = max(mx[u], v), tag[u] = max(tag[u], v), void();
  int mid = (l + r) >> 1;
  if (L <= mid) assign_max(L, R, v, lc[u], l, mid);
  if (mid < R) assign_max(L, R, v, rc[u], mid + 1, r);
  mx[u] = max(mx[lc[u]], mx[rc[u]]);
}
long long query_max(int L, int R, int &u, int l = 1, int r = n) { //查询区间max
  if (!u || R < l || r < L) return -1e18;
  if (L <= l && r <= R) return mx[u];
  int mid = (l + r) >> 1;
  return max(tag[u],
      max(query_max(L, R, lc[u], l, mid), query_max(L, R, rc[u], mid + 1, r)));
}

int main() {
  scanf("%d%d%d%d", &n, &m, &X, &Y);
  FOR(i, 1, m) {
    int c, l, r;
    scanf("%d%d%d", &c, &l, &r);
    A[i] = {c, l, r};
  }
  sort(A + 1, A + m + 1, cmp);
  long long ans = 0;
  int T1 = new_node();
  int T2[4] = {0, new_node(), new_node(), new_node()};
  int T3[4] = {0, new_node(), new_node(), new_node()};
  FOR(i, 1, m) {
    f[i] = (A[i].r - A[i].l + 1) * 1ll * X; //只选自己
    f[i] = max(f[i], query_max(1, A[i].l - 1, T1) + (A[i].r - A[i].l + 1) * 1ll * X);
    f[i] = max(f[i], query_max(A[i].l, A[i].l, T2[A[i].c]) + A[i].r * 1ll * X);
    FOR(x, 1, 3)
    if (x != A[i].c)
      f[i] = max(f[i], query_max(A[i].l, A[i].l, T3[x]) + A[i].r * 1ll * X +
                           (A[i].l - 1) * 1ll * (X + Y));

    assign_max(A[i].r, A[i].r, f[i], T1);
    assign_max(A[i].l, A[i].r, f[i] - A[i].r * 1ll * X, T2[A[i].c]);
    assign_max(A[i].l, A[i].r, f[i] - A[i].r * 1ll * (2 * X + Y), T3[A[i].c]);

    ans = max(ans, f[i]);
  }
  printf("%lld\n", ans);
  return 0;
}
