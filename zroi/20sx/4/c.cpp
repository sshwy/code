// by Sshwy
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/

namespace IO {
  const int BSZ = 100000;
  char nc() {
    static char bf[BSZ], *p1 = bf, *p2 = bf;
    return p1 == p2 && (p2 = (p1 = bf) + fread(bf, 1, BSZ, stdin), p1 == p2) ? EOF
                                                                             : *p1++;
  }
  int rd() {
    int res = 0;
    char c = nc();
    while (!isdigit(c)) c = nc();
    while (isdigit(c)) res = res * 10 + c - '0', c = nc();
    return res;
  }
  long long rdll() {
    long long res = 0;
    char c = nc();
    while (!isdigit(c)) c = nc();
    while (isdigit(c)) res = res * 10 + c - '0', c = nc();
    return res;
  }
  void rd(char *s) {
    char c = nc();
    while (!isblank(c) && isprint(c) && c != EOF) *s++ = c, c = nc();
    *s = 0;
  }
  int tmp[20], lt;
  char OBF[BSZ], *OBP = OBF;
  void flush() { fwrite(OBF, 1, OBP - OBF, stdout), OBP = OBF; }
  void wrch(char x) {
    if (OBP == OBF + BSZ - 1) flush();
    *OBP = x, ++OBP;
  }
  void wr(int x, int base = 10) {
    lt = 0;
    while (x) tmp[++lt] = x % base, x /= base;
    while (lt > 0) wrch(tmp[lt] + '0'), --lt;
  }
  void wr(long long x, int base = 10) {
    lt = 0;
    while (x) tmp[++lt] = x % base, x /= base;
    while (lt > 0) wrch(tmp[lt] + '0'), --lt;
  }
  void wr(const char *s) {
    while (*s) wrch(*s), ++s;
  }
  void wr(char x) { wrch(x); }
} // namespace IO
const int N = 4e4 + 5, K = 9;

int n, k, lim;
int g[N][1 << K];        //第i层的点集j与下一层连通的点集
int f[2][1 << K][K + 1]; //从第i层点集j出发，用l个点最早被堵死在哪里

void get_min(int &x, int y) { x = min(x, y); }
int main() {
  n = IO::rd(), k = IO::rd();
  lim = 1 << k;
  FOR(i, 1, n - 1) {
    FOR(j, 0, k - 1) {
      char s[20];
      IO::rd(s);
      FOR(l, 0, k - 1) if (s[l] == '1') g[i][1 << j] |= 1 << l;
    }
    FOR(j, 1, lim - 1) g[i][j] = g[i][j & (j - 1)] | g[i][j & -j];
  }
  FOR(i, 0, lim - 1) {
    int x = __builtin_popcount(i);
    FOR(j, 0, x - 1) f[n & 1][i][j] = n + 1;
    FOR(j, x, k) f[n & 1][i][j] = n;
  }
  long long ans = 0;
  ROF(i, n - 1, 1) {
    int cur = i & 1;
    FOR(j, 0, k) f[cur][0][j] = i; // mask=0
    FOR(mask, 1, lim - 1) {
      FOR(j, 0, k) f[cur][mask][j] = f[cur ^ 1][g[i][mask]][j]; //第i层不删点
      FOR(x, 0, k - 1)
      if (mask >> x & 1) FOR(j, 1, k)
      get_min(f[cur][mask][j], f[cur][mask ^ (1 << x)][j - 1]);
    }
    // FOR(mask,0,lim-1)FOR(j,0,k)printf("f[%d,%d,%d]=%d\n",i,mask,j,f[i][mask][j]);
    FOR(j, 0, k - 1) {
      //我到f[i][lim-1][j]就截住了，相当于在这之前的都要多堵至少个点
      ans += f[cur][lim - 1][j] - i - 1;
    }
  }
  printf("%lld\n", ans);
  return 0;
}
