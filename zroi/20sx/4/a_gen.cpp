// by Sshwy
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/

using namespace RA;

int main() {
  srand(clock());
  int n = r(1, 100), m = r(1, 100), x = r(100), y = r(100);
  printf("%d %d %d %d\n", n, m, x, y);
  FOR(i, 1, m) {
    int c = r(1, 3), L = r(1, n), R = r(1, n);
    if (L > R) swap(L, R);
    printf("%d %d %d\n", c, L, R);
  }
  return 0;
}
