// by Sshwy
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/

int ans[20][4];

int main() {
  ans[1][0] = 0;
  ans[1][1] = 0;
  ans[1][2] = 0;
  ans[1][3] = 0;
  ans[2][0] = 1;
  ans[2][1] = 0;
  ans[2][2] = 0;
  ans[2][3] = 0;
  ans[3][0] = 9;
  ans[3][1] = 0;
  ans[3][2] = 0;
  ans[3][3] = 0;
  ans[4][0] = 55;
  ans[4][1] = 14;
  ans[4][2] = 3;
  ans[4][3] = 0;
  ans[5][0] = 290;
  ans[5][1] = 149;
  ans[5][2] = 98;
  ans[5][3] = 21;
  ans[6][0] = 1418;
  ans[6][1] = 1054;
  ans[6][2] = 1074;
  ans[6][3] = 484;
  ans[7][0] = 6629;
  ans[7][1] = 6236;
  ans[7][2] = 8245;
  ans[7][3] = 5430;
  ans[8][0] = 30091;
  ans[8][1] = 33398;
  ans[8][2] = 52812;
  ans[8][3] = 44472;
  ans[9][0] = 133806;
  ans[9][1] = 167990;
  ans[9][2] = 303340;
  ans[9][3] = 304997;
  ans[10][0] = 586054;
  ans[10][1] = 809664;
  ans[10][2] = 1621010;
  ans[10][3] = 1867456;
  ans[11][0] = 2537370;
  ans[11][1] = 3784560;
  ans[11][2] = 8232983;
  ans[11][3] = 10573956;
  ans[12][0] = 10886566;
  ans[12][1] = 17289420;
  ans[12][2] = 40276667;
  ans[12][3] = 56564788;
  ans[13][0] = 46369284;
  ans[13][1] = 77603161;
  ans[13][2] = 191487706;
  ans[13][3] = 289885815;
  ans[14][0] = 196323476;
  ans[14][1] = 343494418;
  ans[14][2] = 890245676;
  ans[14][3] = 438658583;
  int n, m;
  cin >> n >> m;
  cout << ans[n][m];
  return 0;
}
