// by Sshwy
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int M = 1e5 + 5;

int n, m, X, Y;
struct atom {
  int c, l, r;
} A[M];
bool cmp(atom x, atom y) { return x.r < y.r; }

long long f[M]; // f[i]最后一个区间选i的最大价值
int main() {
  scanf("%d%d%d%d", &n, &m, &X, &Y);
  FOR(i, 1, m) {
    int c, l, r;
    scanf("%d%d%d", &c, &l, &r);
    A[i] = {c, l, r};
  }
  sort(A + 1, A + m + 1, cmp);
  long long ans = 0;
  FOR(i, 1, m) {
    f[i] = (A[i].r - A[i].l + 1) * 1ll * X; //只选自己
    FOR(j, 1, i - 1) {                      //上一个选择的区间
      if (A[j].r < A[i].l)
        f[i] = max(f[i], f[j] + (A[i].r - A[i].l + 1) * 1ll * X);
      else if (A[j].l <= A[i].l) { //相交且不包含
        if (A[i].c == A[j].c) {
          f[i] = max(f[i], f[j] + (A[i].r - A[j].r) * 1ll * X);
        } else {
          f[i] = max(f[i], f[j] + (A[i].r - A[j].r) * 1ll * X -
                               (A[j].r - A[i].l + 1) * 1ll * (X + Y));
        }
      }
    }
    // printf("f[%d]=%lld\n",i,f[i]);
    ans = max(ans, f[i]);
  }
  printf("%lld\n", ans);
  return 0;
}
