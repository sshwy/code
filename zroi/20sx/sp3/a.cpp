// by Sshwy
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
#define int long long
const int INF = 0x3f3f3f3f, N = 60, _N = N * N * 3, _M = 1e6 + 5;

struct qxx {
  int nex, t, v, c;
} e[_M];
int h[_N], le = 1;
void add_path(int f, int t, int v, int c) {
  e[++le] = (qxx){h[f], t, v, c}, h[f] = le;
}
void add_flow(int f, int t, int v, int c) {
  // printf("add_flow(%lld,%lld,%lld,%lld)\n",f,t,v,c);
  add_path(f, t, v, c), add_path(t, f, 0, -c);
}

bool vis[_N];
queue<int> q;
long long d[_N];
int pre[_N], incf[_N];
int s, t;

bool spfa() {
  memset(d, -0x3f, sizeof(d));
  q.push(s), d[s] = 0, incf[s] = INF, incf[t] = 0;
  while (!q.empty()) {
    int u = q.front();
    q.pop();
    vis[u] = 0;
    for (int i = h[u]; i; i = e[i].nex) {
      const int v = e[i].t, w = e[i].v, c = e[i].c;
      if (!w || d[v] >= d[u] + c) continue;
      d[v] = d[u] + c, incf[v] = min(incf[u], w), pre[v] = i;
      if (!vis[v]) q.push(v), vis[v] = 1;
    }
  }
  if (d[t] < 0) return 0;
  return incf[t];
}
int maxflow;
long long maxcost;
void update() {
  maxflow += incf[t], maxcost += incf[t] * 1ll * d[t];
  // printf("update %d %d\n",d[t],incf[t]);
  for (int u = t; u != s; u = e[pre[u] ^ 1].t) {
    // printf("u=%d\n",u);
    e[pre[u]].v -= incf[t], e[pre[u] ^ 1].v += incf[t];
  }
}
void go() {
  while (spfa()) update();
}

int n, m, k;
int a[N][N];
long long sum;

inline int I(int x, int y) { return (x - 1) * n + y; }
signed main() {
  scanf("%lld%lld%lld", &n, &m, &k);
  FOR(i, 1, n) {
    FOR(j, 1, n) {
      scanf("%lld", &a[i][j]);
      sum += a[i][j];
    }
  }
  FOR(i, 1, k) {
    int x, y;
    scanf("%lld%lld", &x, &y);
    a[x][y] = -1;
  }
  // FOR(i,1,n)FOR(j,1,n)printf("%3d%c",a[i][j]," \n"[j==n]);
  s = 0, t = n * n * 2 + 3;
  int s1 = n * n * 2 + 4;
  FOR(i, 1, n) FOR(j, 1, n) {
    if (a[i][j] == -1) continue;
    int u = I(i, j);
    if ((i + j) & 1) {
      add_flow(u * 2, u * 2 + 1, 1, a[i][j]);
      if (j & 1) {
        if (i > 1 && a[i - 1][j] != -1) add_flow(I(i - 1, j) * 2 + 1, u * 2, 1, 0);
        if (i < n && a[i + 1][j] != -1) add_flow(I(i + 1, j) * 2 + 1, u * 2, 1, 0);
        if (j > 1 && a[i][j - 1] != -1) add_flow(u * 2 + 1, I(i, j - 1) * 2, 1, 0);
        if (j < n && a[i][j + 1] != -1) add_flow(u * 2 + 1, I(i, j + 1) * 2, 1, 0);
      } else {
        if (i > 1 && a[i - 1][j] != -1) add_flow(u * 2 + 1, I(i - 1, j) * 2, 1, 0);
        if (i < n && a[i + 1][j] != -1) add_flow(u * 2 + 1, I(i + 1, j) * 2, 1, 0);
        if (j > 1 && a[i][j - 1] != -1) add_flow(I(i, j - 1) * 2 + 1, u * 2, 1, 0);
        if (j < n && a[i][j + 1] != -1) add_flow(I(i, j + 1) * 2 + 1, u * 2, 1, 0);
      }
    } else {
      // printf("I(%d,%d)=%d\n",i,j,u);
      add_flow(u * 2, u * 2 + 1, 1, 0);
      if (i & 1) {
        add_flow(s1, u * 2, 1, 0);
      } else {
        add_flow(u * 2 + 1, t, 1, 0);
      }
    }
  }
  add_flow(s, s1, m, 0);
  // puts("GG");
  go();
  // printf("maxflow=%lld,maxcost=%lld\n",maxflow,maxcost);
  // printf("sum=%d\n",sum);
  printf("%lld\n", sum - maxcost);

  return 0;
}
