// by Sshwy
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 5e5 + 5;

int n;
int sa[N], rk[N], h_rk[N], h[N];
long long v[N];

namespace SA {
  int l;
  int t[N], bin[N], sz;
  void qsort() {
    for (int i = 0; i <= sz; i++) bin[i] = 0;
    for (int i = 1; i <= l; i++) bin[rk[i]]++;
    for (int i = 1; i <= sz; i++) bin[i] += bin[i - 1];
    for (int i = l; i >= 1; i--) sa[bin[rk[t[i]]]--] = t[i];
  }
  void make(const char *s) {
    l = strlen(s + 1), sz = max(l, 'z' - 'a' + 1);
    for (int i = 1; i <= l; i++) t[i] = i, rk[i] = s[i] - 'a' + 1;
    qsort();
    for (int j = 1; j <= l; j <<= 1) {
      int tot = 0;
      for (int i = l - j + 1; i <= l; i++) t[++tot] = i;
      for (int i = 1; i <= l; i++)
        if (sa[i] - j > 0) t[++tot] = sa[i] - j;
      qsort();
      swap(t, rk);
      rk[sa[1]] = tot = 1;
      for (int i = 2; i <= l; i++)
        rk[sa[i]] = t[sa[i - 1]] == t[sa[i]] && t[sa[i - 1] + j] == t[sa[i] + j]
                        ? tot
                        : ++tot;
    }
  }
} // namespace SA
char s[N];

int move(int x, int y, int len) {
  while (x + len <= n && y + len <= n && s[x + len] == s[y + len]) ++len;
  return len;
}
void calc_h_rk() {
  FOR(i, 1, n) {
    if (rk[i] == 1)
      h_rk[i] = 0;
    else
      h_rk[i] = move(i, sa[rk[i] - 1], max(0, h_rk[i - 1] - 1));
  }
}

long long V(int l, int r) { return v[r] - v[l - 1]; }
pair<int, int> ans[N];
int la;

namespace seg {
  int tot = 1;
  int lc[N << 2], rc[N << 2];
  long long val[N << 2];
  bool tag[N << 2];
  void node_assign(int u, long long w) {
    val[u] = w;
    tag[u] = 1;
  }
  void pushdown(int u, int l, int r) {
    if (lc[u] == 0) lc[u] = ++tot;
    if (rc[u] == 0) rc[u] = ++tot;
    int mid = (l + r) >> 1;
    if (tag[u]) {
      node_assign(lc[u], val[u]);
      node_assign(rc[u], val[u] + mid - l + 1);
      tag[u] = 0;
    }
  }
  void assign(int L, int R, long long w, int u = 1, int l = 1, int r = n) {
    if (R < l || r < L) return;
    if (L <= l && r <= R) {
      node_assign(u, w + l - L);
      return;
    }
    pushdown(u, l, r);
    int mid = (l + r) >> 1;
    assign(L, R, w, lc[u], l, mid);
    assign(L, R, w, rc[u], mid + 1, r);
  }
  long long query(int pos, int u = 1, int l = 1, int r = n) {
    if (l == r) return val[u];
    int mid = (l + r) >> 1;
    pushdown(u, l, r);
    if (pos <= mid)
      return query(pos, lc[u], l, mid);
    else
      return query(pos, rc[u], mid + 1, r);
  }
} // namespace seg

int main() {
  scanf("%s", s + 1);
  n = strlen(s + 1);
  FOR(i, 1, n) scanf("%lld", &v[i]), v[i] += v[i - 1];

  SA::make(s);
  // FOR(i,1,n)printf("%d%c",sa[i]," \n"[i==n]);
  // FOR(i,1,n)printf("%d%c",rk[i]," \n"[i==n]);
  calc_h_rk();
  // FOR(i,1,n)printf("%d%c",h_rk[i]," \n"[i==n]);
  FOR(i, 1, n) h[rk[i]] = h_rk[i];
  // FOR(i,1,n)printf("%d%c",h[i]," \n"[i==n]);

  long long tot = n * (n + 1ll) / 2, pc = 0;
  FOR(i, 1, n) tot -= h[i];
  // printf("tot=%lld\n",tot);

  FOR(i, 1, n) { //排名为i的后缀
    // printf("\033[32mi=%d\033[0m\n",i);
    int L = sa[i] + h[i], R = n;
    // FOR(j,sa[i],L-1)printf("\033[33m%c\033[0m",s[j]);
    // FOR(j,L,R)printf("%c",s[j]);
    // puts("");

    // printf("pc=%d\n",pc);
    // FOR(j,L,R)printf("[%d,%d]: rk=%d,val=%d\n",sa[i],j,pc+j-L+1,V(sa[i],j));
    // printf("h[%d]=%d, ",i,h[i]);FOR(j,1,h[i])printf("%d%c",pos[j],"
    // \n"[j==h[i]]);

    int l = L, r = R;
    while (l < r) {
      int mid = (l + r) >> 1;
      if (pc + mid - L + V(sa[i], mid) >= tot)
        r = mid;
      else
        l = mid + 1;
    }
    if (l == r && pc + l - L + V(sa[i], l) == tot) {
      // printf("add(%d,%d)\n",sa[i],l);
      // ans.pb({sa[i],l});
      ans[++la] = make_pair(sa[i], l);
    }
    l = 1, r = h[i];
    while (l < r) {
      int mid = (l + r) >> 1;
      if (seg::query(mid) - 1 + V(sa[i], sa[i] + mid - 1) >= tot)
        r = mid;
      else
        l = mid + 1;
    }
    if (l == r && seg::query(l) - 1 + V(sa[i], sa[i] + l - 1) == tot) {
      // printf("add2(%d,%d)\n",sa[i],sa[i]+l-1);
      // ans.pb({sa[i],sa[i]+l-1});
      ans[++la] = make_pair(sa[i], sa[i] + l - 1);
    }
    // FOR(j,h[i]+1,h[i+1]) pos[j]=pc+j-h[i];
    seg::assign(h[i] + 1, h[i + 1], pc + 1);
    pc += R - L + 1;
  }
  sort(ans + 1, ans + la + 1);
  printf("%d\n", la);
  FOR(i, 1, la) printf("%d %d\n", ans[i].fi, ans[i].se);
  // IO::wr(la),IO::wr('\n');
  // FOR(i,1,la)IO::wr(ans[i].fi),IO::wr(' '),IO::wr(ans[i].se),IO::wr('\n');
  // IO::flush();
  return 0;
}
