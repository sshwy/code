// by Sshwy
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/

using namespace RA;

int main() {
  srand(clock() + time(0));
  int n = 10, m = r(0, n * n / 3), k = r(0, n * n);
  int L = 10;
  printf("%d %d %d\n", n, m, k);
  FOR(i, 1, n) FOR(j, 1, n) {
    if ((i + j) & 1)
      printf("%d%c", r(0, L), " \n"[j == n]);
    else
      printf("0%c", " \n"[j == n]);
  }
  FOR(i, 1, k) {
    int x = r(1, n), y = r(1, n);
    printf("%d %d\n", x, y);
  }
  return 0;
}
