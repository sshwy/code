// by Sshwy
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/

using namespace RA;

string s, t;

int main() {
  srand(clock() + time(0));
  int n = 1e2, m = 10, L = n / 10;
  FOR(i, 1, m) {
    s.clear();
    int len = r(1, n);
    FOR(i, 1, len) s.pb(r('a', 'b'));
    int cnt = r(1, m);
    FOR(i, 1, cnt) t += s;
  }
  printf("%s\n", t.c_str());
  int lt = t.size();
  puts("");
  FOR(i, 1, lt) printf("%d%c", r(1, L), " \n"[i == n]);
  return 0;
}
