// by Sshwy
//#define DEBUGGER
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

#ifdef DEBUGGER

#define log_prefix()                                                \
  {                                                                 \
    fprintf(stderr, "\033[37mLine %-3d [%dms]:\033[0m  ", __LINE__, \
        (int)clock() / 1000);                                       \
  }
#define ilog(...) \
  { fprintf(stderr, __VA_ARGS__); }
#define llog(...)      \
  {                    \
    log_prefix();      \
    ilog(__VA_ARGS__); \
  }
#define log(...)           \
  {                        \
    log_prefix();          \
    ilog(__VA_ARGS__);     \
    fprintf(stderr, "\n"); \
  }
#define red(...)       \
  {                    \
    log_prefix();      \
    ilog("\033[31m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define green(...)     \
  {                    \
    log_prefix();      \
    ilog("\033[32m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define blue(...)      \
  {                    \
    log_prefix();      \
    ilog("\033[34m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }

#else

#define log_prefix() ;
#define ilog(...) ;
#define llog(...) ;
#define log(...) ;
#define red(...) ;
#define green(...) ;
#define blue(...) ;

#endif

namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 5e5 + 5;
char s[N];
int n, nex[N];
long long L, R;
struct atom {
  long long c[30];
  atom() { fill(c, c + 30, 0); }
  atom operator-(const atom x) const {
    atom res = *this;
    FOR(i, 0, 25) res.c[i] -= x.c[i];
    return res;
  }
  atom operator+(const atom x) const {
    atom res = *this;
    FOR(i, 0, 25) res.c[i] += x.c[i];
    return res;
  }
  void print() { FOR(i, 0, 25) printf("%lld%c", c[i], " \n"[i == 25]); }
};

void solve1(int k) { // per
  auto calc = [k](long long x) {
    atom res;
    if (x <= n) {
      FOR(i, 1, x) res.c[s[i] - 'a']++;
      return res;
    }
    FOR(i, 1, n) res.c[s[i] - 'a']++;
    x -= n;
    long long cnt = x / k;
    FOR(i, 1, k) res.c[s[i] - 'a'] += cnt;
    x %= k;
    FOR(i, 1, x) res.c[s[i] - 'a']++;
    return res;
  };
  atom ans = calc(R) - calc(L - 1);
  ans.print();
}
long long g[N];
atom f[N];
void solve2(int k) { // len(T)
  g[0] = k, g[1] = n;
  FOR(i, 1, k) f[0].c[s[i] - 'a']++;
  FOR(i, 1, n) f[1].c[s[i] - 'a']++;
  int l = 1;
  while (g[l] * 2 <= R) ++l, g[l] = g[l - 1] + g[l - 2], f[l] = f[l - 1] + f[l - 2];
  auto calc = [l](long long x) {
    atom res;
    for (int i = l; i >= 1; --i) { //不能到0
      if (g[i] <= x) {
        res = res + f[i];
        x -= g[i];
      }
    }
    FOR(i, 1, x) res.c[s[i] - 'a']++;
    return res;
  };
  atom ans = calc(R) - calc(L - 1);
  ans.print();
}
int main() {
  scanf("%s", s + 1);
  scanf("%lld%lld", &L, &R);
  n = strlen(s + 1);
  n /= 2;
  nex[1] = 0, nex[0] = -1;
  FOR(i, 2, n) {
    int j = nex[i - 1];
    while (j != -1 && s[j + 1] != s[i]) j = nex[j];
    nex[i] = j + 1; // including -1
  }
  int k = n - nex[n];
  if (n % k == 0)
    solve1(k);
  else
    solve2(k);
  return 0;
}

// 当前的串是SS。则nex(SS)？
// 假设后面接了2k个字符。那么容易发现nex(SS)长成
// S[k个字符]S[k个字符]的形式，其中两个中括号是匹配的
// 也长成SS[2k个字符]的形式
// 因此可以发现这个[k个字符]就是S的长度为k的前缀，不妨记作T。
// 因此nex(SS)=STST。
// 另一方面SS[2k个字符],因此S有长度为|S|-k的border。因此k是S的一个周期。
// 于是我们只需要找到S的最小周期就可以确定nex(SS)的样子。
//
// 不妨设g(S)=ST。
// 如果k | |S|。易证ST的最小周期仍是T。因此就是STTTT...。即g^i(S)=g^{i-1}(S)T
// 否则。我们可以证明 ST 的最小周期是 S。
//     首先，S是ST的一个周期。
//     而如果存在比S小的x，则它要么在S里构成gcd(x,T)<T的周期，
//     要么在ST里与S构成gcd(x,S)<T的周期。都是矛盾的。
// 于是per(ST)=S。
// 因此我们得到g(S)=ST. g(ST)=STS. g^i = g^{i-1}g^{i-2}.
