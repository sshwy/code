// by Sshwy
//#define DEBUGGER
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

#ifdef DEBUGGER

#define log_prefix()                                                \
  {                                                                 \
    fprintf(stderr, "\033[37mLine %-3d [%dms]:\033[0m  ", __LINE__, \
        (int)clock() / 1000);                                       \
  }
#define ilog(...) \
  { fprintf(stderr, __VA_ARGS__); }
#define llog(...)      \
  {                    \
    log_prefix();      \
    ilog(__VA_ARGS__); \
  }
#define log(...)           \
  {                        \
    log_prefix();          \
    ilog(__VA_ARGS__);     \
    fprintf(stderr, "\n"); \
  }
#define red(...)       \
  {                    \
    log_prefix();      \
    ilog("\033[31m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define green(...)     \
  {                    \
    log_prefix();      \
    ilog("\033[32m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define blue(...)      \
  {                    \
    log_prefix();      \
    ilog("\033[34m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }

#else

#define log_prefix() ;
#define ilog(...) ;
#define llog(...) ;
#define log(...) ;
#define red(...) ;
#define green(...) ;
#define blue(...) ;

#endif

namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 1e5 + 5;
int n, m, dg[N], dep[N], a[N * 4], st[20][N * 4], la, L[N], R[N], X[N], Y[N], tag[N];
long long d[N], f[N];

struct qxx {
  int nex, t, v;
} e[N * 2];
int h[N], le = 1;
void add_path(int f, int t, int v) { e[++le] = {h[f], t, v}, h[f] = le; }

void dfs(int u, int p) {
  dep[u] = dep[p] + 1;
  a[++la] = u, L[u] = la;
  for (int i = h[u]; i; i = e[i].nex) {
    int v = e[i].t, w = e[i].v;
    if (v != p) {
      d[v] = d[u] + w;
      dfs(v, u);
      a[++la] = u;
    }
  }
  R[u] = la;
}
int lg[N];
int qry(int l, int r) {
  int j = lg[r - l + 1];
  return dep[st[j][l]] < dep[st[j][r - (1 << j) + 1]] ? st[j][l]
                                                      : st[j][r - (1 << j) + 1];
}
int lca(int u, int v) {
  if (L[u] > L[v]) swap(u, v);
  if (R[v] < R[u]) return u;
  return qry(L[u], R[v]);
}
long long dist(int u, int v) { return d[u] + d[v] - d[lca(u, v)] * 2; }
void solve2() {
  dfs(1, 0);
  FOR(i, 1, la) st[0][i] = a[i];
  FOR(j, 1, 19) FOR(i, 1, la - (1 << j) + 1) {
    st[j][i] = dep[st[j - 1][i]] < dep[st[j - 1][i + (1 << (j - 1))]]
                   ? st[j - 1][i]
                   : st[j - 1][i + (1 << (j - 1))];
  }
  lg[1] = 0;
  FOR(i, 2, la) lg[i] = lg[i / 2] + 1;
  long long ans = 0;
  FOR(i, 1, m) FOR(j, i + 1, m) {
    long long t = dist(X[i], X[j]) + dist(Y[i], Y[j]);
    ans = max(ans, t);
  }
  printf("%lld\n", ans);
}
void solve1() { solve2(); }

void dfs1(int u, int p) {
  if (tag[u]) f[u] = 0;
  for (int i = h[u]; i; i = e[i].nex) {
    int v = e[i].t, w = e[i].v;
    if (v != p) {
      dfs1(v, u);
      f[u] = max(f[u], f[v] + w);
    }
  }
}
long long ans = 0;
void dfs2(int u, int p) {
  ans = max(ans, f[u]);
  pair<int, long long> v1 = {-1, -1e18}, v2 = {-1, -1e18};
  for (int i = h[u]; i; i = e[i].nex) {
    int v = e[i].t, w = e[i].v;
    if (v != p) {
      if (v1.fi == -1 || v1.se < f[v] + w)
        v2 = v1, v1 = {v, f[v] + w};
      else if (v2.fi == -1 || v2.se < f[v] + w)
        v2 = {v, f[v] + w};
    }
  }
  for (int i = h[u]; i; i = e[i].nex) {
    int v = e[i].t, w = e[i].v;
    if (v != p) {
      // u -> v
      int fu = f[u], fv = f[v];
      f[u] = v == v1.fi ? v2.se : v1.se;
      if (tag[u]) f[u] = max(f[u], 0ll);
      f[v] = max(f[v], f[u] + w);
      dfs2(v, u);
      f[u] = fu, f[v] = fv;
    }
  }
}
void solve3() {
  fill(f + 1, f + n + 1, -1e18);
  FOR(i, 1, m) tag[Y[i]] = 1;
  dfs1(1, 0);
  dfs2(1, 0);
  printf("%lld\n", ans);
}
int main() {
  scanf("%d%d", &n, &m);
  FOR(i, 1, n - 1) {
    int u, v, w;
    scanf("%d%d%d", &u, &v, &w);
    add_path(u, v, w), add_path(v, u, w);
    dg[u]++, dg[v]++;
  }
  FOR(i, 1, m) {
    int a, b;
    scanf("%d%d", &a, &b);
    X[i] = a, Y[i] = b;
  }
  solve2();
  return 0;
}
