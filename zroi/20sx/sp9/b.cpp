#include <bits/stdc++.h>
using namespace std;
#define fi first
#define se second
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
const int N = 2e5 + 5;
typedef pair<int, long long> nd;
int n, m, dep[N], a[N * 4], st[20][N * 2], la, L[N * 4], R[N * 4], lg[N * 4];
long long d[N], ans;
vector<int> B[N];
vector<nd> vn[N];

struct qxx {
  int nex, t, v;
} e[N * 2];
int h[N], le = 1;
void add_path(int f, int t, int v) { e[++le] = {h[f], t, v}, h[f] = le; }

void dfs(int u, int p) {
  dep[u] = dep[p] + 1;
  a[++la] = u, L[u] = la;
  for (int i = h[u]; i; i = e[i].nex) {
    int v = e[i].t, w = e[i].v;
    if (v != p) {
      d[v] = d[u] + w;
      dfs(v, u);
      a[++la] = u;
    }
  }
  R[u] = la;
}
int qry(int l, int r) {
  int j = lg[r - l + 1];
  return dep[st[j][l]] < dep[st[j][r - (1 << j) + 1]] ? st[j][l]
                                                      : st[j][r - (1 << j) + 1];
}
int lca(int u, int v) {
  if (L[u] > L[v]) swap(u, v);
  if (R[v] < R[u]) return u;
  return qry(L[u], R[v]);
}

long long dist(int u, int v) { return d[u] + d[v] - d[lca(u, v)] * 2; }
long long dist(nd x, nd y) { return dist(x.fi, y.fi) + x.se + y.se; }
long long dist(vector<nd> p) { return p.size() == 2 ? dist(p[0], p[1]) : -1e18; }

vector<nd> merge(const vector<nd> &x, const vector<nd> &y, long long cur) {
  assert(x.size() <= 2 && y.size() <= 2);
  if (x.size() == 0) return y;
  if (y.size() == 0) return x;
  vector<nd> res;
  for (auto a : x)
    for (auto b : y) {
      if (dist(a, b) > dist(res)) res.resize(2), res[0] = a, res[1] = b;
      ans = max(ans, dist(a, b) - cur * 2);
    }
  if (dist(x) > dist(res)) res = x;
  if (dist(y) > dist(res)) res = y;
  return res;
}
void dfs2(int u, int p) {
  for (int x : B[u]) vn[u] = merge(vn[u], vector<nd>(1, nd(x, d[u])), d[u]);
  for (int i = h[u]; i; i = e[i].nex) {
    int v = e[i].t;
    if (v != p) {
      dfs2(v, u);
      vn[u] = merge(vn[u], vn[v], d[u]);
    }
  }
}

int main() {
  scanf("%d%d", &n, &m);
  FOR(i, 1, n - 1) {
    int u, v, w;
    scanf("%d%d%d", &u, &v, &w);
    add_path(u, v, w), add_path(v, u, w);
  }
  FOR(i, 1, m) {
    int a, b;
    scanf("%d%d", &a, &b);
    B[a].push_back(b);
  }
  dfs(1, 0);
  FOR(i, 1, la) st[0][i] = a[i];
  FOR(j, 1, 19) FOR(i, 1, la - (1 << j) + 1) {
    st[j][i] = dep[st[j - 1][i]] < dep[st[j - 1][i + (1 << (j - 1))]]
                   ? st[j - 1][i]
                   : st[j - 1][i + (1 << (j - 1))];
  }
  lg[1] = 0;
  FOR(i, 2, la) lg[i] = lg[i / 2] + 1;

  dfs2(1, 0);

  printf("%lld\n", ans);
  return 0;
}
