// by Sshwy
#define DEBUGGER
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

#ifdef DEBUGGER

#define log_prefix()                                                \
  {                                                                 \
    fprintf(stderr, "\033[37mLine %-3d [%dms]:\033[0m  ", __LINE__, \
        (int)clock() / 1000);                                       \
  }
#define ilog(...) \
  { fprintf(stderr, __VA_ARGS__); }
#define llog(...)      \
  {                    \
    log_prefix();      \
    ilog(__VA_ARGS__); \
  }
#define log(...)           \
  {                        \
    log_prefix();          \
    ilog(__VA_ARGS__);     \
    fprintf(stderr, "\n"); \
  }
#define red(...)       \
  {                    \
    log_prefix();      \
    ilog("\033[31m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define green(...)     \
  {                    \
    log_prefix();      \
    ilog("\033[32m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define blue(...)      \
  {                    \
    log_prefix();      \
    ilog("\033[34m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }

#else

#define log_prefix() ;
#define ilog(...) ;
#define llog(...) ;
#define log(...) ;
#define red(...) ;
#define green(...) ;
#define blue(...) ;

#endif

namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
#include <unordered_map>
const int SZ = 5e6, SQ = 2200, FSZ = SQ * SQ;

const long long P = 1004535809ll, I2 = (P + 1) / 2;
// const long long P2=P*P;
long long n, ans;
int _f[FSZ], mu[SZ], z[SZ], _sg[SZ], pn[SZ], lp, co[SZ];
long long XXX;

int calc_f(long long n) {
  long long s = 0;
  long long y = sqrt(n);
  for (long long i = 1; i * i <= n; ++i) {
    while (i * i + y * y > n) --y;
    s = (s + i * (2 * y + 1));
  }
  s %= P;
  return s;
}
int f(long long n) {
  if (n < FSZ) return _f[n];
  return calc_f(n);
}

map<long long, int> val;
int sg(long long n) {
  if (n < SZ) return _sg[n];
  if (val.count(n)) return val[n];
  long long s = 0;
  for (long long l = 1, r; r = l <= n ? n / (n / l) : 0, l <= n; l = r + 1) {
    s = (s + (n / l) * (l + r) % P * (r - l + 1) % P);
  }
  s = s % P * I2 % P;
  return val[n] = s;
}

void init() {
  auto add = [](int &x, int y) { x = (x + y) % P; };
  FOR(x, 1, SQ) {
    if (x * x < FSZ) add(_f[x * x], x);
    FOR(y, 1, SQ) {
      int t = x * x + y * y;
      if (t < FSZ) add(_f[t], x * 2);
    }
  }
  FOR(i, 1, FSZ - 1) add(_f[i], _f[i - 1]);
  co[0] = co[1] = 1, mu[1] = 1, _sg[1] = 1;
  FOR(i, 2, SZ - 1) {
    if (!co[i]) pn[++lp] = i, mu[i] = -1, z[i] = i, _sg[i] = i + 1;
    FOR(j, 1, lp) {
      int x = i * pn[j];
      if (x >= SZ) break;
      co[x] = 1;
      if (i % pn[j] == 0) {
        mu[x] = 0;
        z[x] = z[i] * pn[j];
        if (z[x] == x)
          _sg[x] = _sg[i] + x;
        else
          _sg[x] = _sg[x / z[x]] * 1ll * _sg[z[x]] % P;
        break;
      } else {
        mu[x] = -mu[i];
        z[x] = pn[j];
        _sg[x] = _sg[i] * 1ll * _sg[pn[j]] % P;
      }
    }
  }
  FOR(i, 1, SZ - 1) _sg[i] = (_sg[i] + _sg[i - 1]) % P;
}
int main() {
  init();
  cin >> n;
  long long ans = 0;
  for (long long d = 1; d * d <= n; ++d) {
    long long lim = n / d / d;
    long long s = 0, x1 = sg(0), x2;
    for (long long l = 1, r; r = l <= lim ? lim / (lim / l) : 0, l <= lim;
         l = r + 1) {
      x2 = sg(r);
      s = (s + f(lim / l) * 1ll * (x2 - x1)) % P;
      x1 = x2;
    }
    s = s * d * mu[d] % P;
    ans += s;
  }

  cout << (ans % P + P) % P << endl;
  return 0;
}
