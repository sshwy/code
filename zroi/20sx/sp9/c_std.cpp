#include <bits/stdc++.h>
const int N = 2e3 + 5, M = 5e6, mo = 1004535809, iv2 = (mo + 1) / 2;
typedef long long ll;
ll n, a[N];
int gcd(int a, int b) { return b ? gcd(b, a % b) : a; }
int i, j, p[M + 5], mn[M + 5], d[M + 5], d2[N], ans[M + 5], ans2[N], xb, res;
bool b[M + 5];
inline int askd(ll x) { return x <= M ? d[x] : d2[n / x]; }
inline int aska(ll x) { return x <= M ? ans[x] : ans2[n / x]; }
inline int s2(ll x) {
  x %= mo;
  return 1ll * (x + 1) * x % mo * iv2 % mo;
}
int main() {
  scanf("%lld", &n);
  d[1] = 1;
  for (i = 2; i <= M; ++i) {
    if (!b[i]) p[++xb] = i, mn[i] = i, d[i] = 1 + i;
    for (j = 1; j <= xb; ++j) {
      int x = i * p[j];
      if (x > M) break;
      b[x] = 1;
      if (i % p[j] == 0) {
        mn[x] = mn[i] * p[j], d[x] = mn[x] == x ? (d[x / p[j]] + x) % mo
                                                : 1ll * d[x / mn[x]] * d[mn[x]] % mo;
        break;
      }
      d[x] = 1ll * (p[j] + 1) * d[i] % mo;
      mn[x] = p[j];
    }
  }
  for (i = 1; i * i <= M; ++i)
    for (j = 1; i * i + j * j <= M; ++j)
      if (gcd(i, j) == 1) ans[i * i + j * j] = (ans[i * i + j * j] + i) % mo;
  for (i = 1; i <= M; ++i)
    d[i] = (d[i - 1] + d[i]) % mo, ans[i] = (ans[i - 1] + ans[i]) % mo;
  for (i = 1, xb = 0; n / i > M; ++i) a[++xb] = n / i;
  for (i = xb; i; --i) {
    ll x, y, z;
    for (j = 1; 1ll * j * j < a[i]; ++j)
      ans2[i] = (ans2[i] + 1ll * j * (ll(sqrt(a[i] - 1ll * j * j)) % mo)) % mo;
    for (x = 1; x <= a[i]; x = y + 1) {
      z = a[i] / x;
      y = a[i] / z;
      d2[i] = (d2[i] + 1ll * (s2(y) + mo - s2(x - 1)) % mo * (z % mo)) % mo;
    }
    z = sqrt(a[i]);
    for (x = 2; x * x <= a[i]; ++x)
      ans2[i] = (ans2[i] + mo - 1ll * x * aska(a[i] / x / x) % mo) % mo;
  }
  ll x, y, z;
  for (x = 1; x <= n; x = y + 1) {
    z = n / x;
    y = n / z;
    res = (res + 1ll * (askd(y) + mo - askd(x - 1)) * aska(z)) % mo;
  }
  res = (2ll * res + askd(n)) % mo;
  printf("%d\n", res);
  return 0;
}
