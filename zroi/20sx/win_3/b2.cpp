// by Sshwy
#include <algorithm>/*{{{*/
#include <bitset>
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int SZ = 6e4 + 5, Q = 6e4 + 5, S = 6e4 + 5;

int tot = 3;
int r[4] = {0, 1, 2, 3};
int tr[SZ][26];
bitset<S> A[SZ];

void insert(int u, char *s, int id) {
  A[u][id] = 1;
  if (*s) {
    char c = *s - 'a';
    if (!tr[u][c]) tr[u][c] = ++tot;
    insert(tr[u][c], s + 1, id);
  }
}

char s1[S], s2[S], s3[S];

int n, q;
int que[4][Q];
int l[4];
int c[S], cur;

void add(int k, char c) {
  c -= 'a';
  int *qk = que[k], &lk = l[k];
  if (qk[lk] == 0)
    qk[++lk] = 0;
  else {
    int u = qk[lk];
    qk[++lk] = tr[u][c];
  }
}
void del(int k) {
  int *qk = que[k], &lk = l[k];
  --lk;
}
inline int get_ans() {
  FOR(i, 1, 3) if (que[i][l[i]] == 0) return 0;
  return (A[que[1][l[1]]] & A[que[2][l[2]]] & A[que[3][l[3]]]).count();
}

int main() {
  scanf("%d", &n);
  FOR(i, 1, n) {
    scanf("%s%s%s", s1, s2, s3);
    insert(r[1], s1, i);
    insert(r[2], s2, i);
    insert(r[3], s3, i);
  }

  que[1][1] = r[1];
  que[2][1] = r[2];
  que[3][1] = r[3];
  l[1] = l[2] = l[3] = 1;

  scanf("%d", &q);
  FOR(i, 1, q) {
    char op[5], c[5];
    int k;
    scanf("%s", op);
    if (op[0] == '+') {
      scanf("%d%s", &k, c);
      add(k, c[0]);
    } else {
      scanf("%d", &k);
      del(k);
    }
    printf("%d\n", get_ans());
  }
  return 0;
}
