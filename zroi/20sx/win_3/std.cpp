#include <bits/stdc++.h>
using namespace std;
#define int long long
const int MOD = 998244353;
struct Matrix {
  int num[2][2];
  inline Matrix() { memset(num, 0, sizeof(num)); }
  inline void e() { memset(num, 0, sizeof(num)), num[0][0] = 1, num[1][1] = 1; }
};
inline Matrix mul(Matrix a, Matrix b, int MOD_) {
  Matrix ans;
  ans.num[0][0] = (a.num[0][0] * b.num[0][0] + a.num[0][1] * b.num[1][0]) % MOD_;
  ans.num[0][1] = (a.num[0][0] * b.num[0][1] + a.num[0][1] * b.num[1][1]) % MOD_;
  ans.num[1][0] = (a.num[1][0] * b.num[0][0] + a.num[1][1] * b.num[1][0]) % MOD_;
  ans.num[1][1] = (a.num[1][0] * b.num[0][1] + a.num[1][1] * b.num[1][1]) % MOD_;
  return ans;
}
inline Matrix quickpower(Matrix a, int b, int MOD_) {
  Matrix i, s;
  for (i = a, s.e(); b; b >>= 1, i = mul(i, i, MOD_))
    if (b & 1) s = mul(s, i, MOD_);
  return s;
}
Matrix Sin, tmp; // quickpower(Sin,k)->Sin.num[1][0]=Fib[k]
int q, n, a, b, c, d, x, y, g, t, ans;
signed main() {
  ios::sync_with_stdio(false), cin.tie(0), cout.tie(0);
  Sin.num[0][0] = Sin.num[0][1] = Sin.num[1][0] = 1;
  cin >> q;
  while (q--) {
    cin >> n >> a >> b >> c >> d;
    while (c) x = a % c, y = b - a / c * d, a = c, b = d, c = x, d = y;
    c = d;
    if (c < 0) c = -c;
    if (a < 0) a = -a, b = -b;
    if (!c) {
      tmp = quickpower(Sin, n, MOD);
      ans = tmp.num[1][0] * a % MOD;
      tmp = mul(tmp, Sin, MOD);
      ans = (ans + tmp.num[1][0] * (b + MOD)) % MOD;
      cout << ans << "\n";
      continue;
    }
    if (!a) {
      ans = __gcd(b, c);
      tmp = quickpower(Sin, n + 1, MOD);
      ans = ans * tmp.num[1][0] % MOD;
      cout << ans << "\n";
      continue;
    }
    tmp = quickpower(Sin, n + 1, a);
    g = __gcd(a, tmp.num[1][0]);
    tmp = quickpower(Sin, n, g * c);
    t = a * tmp.num[1][0] % (g * c);
    tmp = mul(tmp, Sin, g * c);
    t = (t + b * tmp.num[1][0] % (g * c) + g * c) % (g * c);
    ans = __gcd(t, g * c) % MOD;
    cout << ans << "\n";
  }
  return 0;
}
