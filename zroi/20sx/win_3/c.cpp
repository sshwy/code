// by Sshwy
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef pair<long long, long long> pii;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (long long i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (long long i = (a); i >= (b); --i)
namespace RA {
  long long r(long long p) { return 1ll * rand() * rand() % p; }
  long long r(long long L, long long R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
typedef long long LL;

pair<LL, LL> F(LL n, const LL P) {
  if (n == 0) return {0, 1};
  pair<LL, LL> p = F(n >> 1, P);
  LL c = p.fi * (2 * p.se % P - p.fi) % P;
  LL d = p.fi * p.fi % P + p.se * p.se % P;
  c = (c + P) % P, d %= P;
  if (n & 1)
    return {d, (c + d) % P};
  else
    return {c, d};
}

const LL P = 998244353;

LL n, a, b, c, d;

void gao_ac(LL &a, LL &b, LL &c, LL &d) {
  if (c == 0) return;
  int t = a / c;
  a -= t * c;
  b -= t * d;
  swap(a, c), swap(b, d);
  gao_ac(a, b, c, d);
}
void go() {
  scanf("%lld%lld%lld%lld%lld", &n, &a, &b, &c, &d);
  if ((c || d) && (a || b)) gao_ac(a, b, c, d);
  if (a < 0) a = -a, b = -b;
  if (d < 0) d = -d, c = -c;
  if (a == 0 && b == 0) {
    pair<LL, LL> Fn = F(n, P);
    LL ans = (c * Fn.fi + d * Fn.se) % P;
    printf("%lld\n", ans);
  } else if (c == 0 && d == 0) {
    pair<LL, LL> Fn = F(n, P);
    LL ans = (a * Fn.fi + b * Fn.se) % P;
    printf("%lld\n", ans);
  } else if (b == 0 && d == 0) {
    LL ans = __gcd(a, c) * F(n, P).fi % P;
    printf("%lld\n", ans);
  } else if (a == 0 && c == 0) {
    LL ans = __gcd(b, d) * F(n + 1, P).fi % P;
    printf("%lld\n", ans);
  } else {
    LL g = __gcd(a, F(n + 1, a).fi), dg = d * g;
    pair<LL, LL> Fn = F(n, dg);
    LL ans = __gcd(((a * Fn.fi + b * Fn.se) % (dg) + dg) % dg, dg);
    printf("%lld\n", ans);
  }
}
int main() {
  LL q;
  scanf("%lld", &q);
  FOR(i, 1, q) { go(); }
  return 0;
}
