// by Sshwy
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/

using namespace RA;

int l[4];

void gs() {
  int len = r(1, 10);
  FOR(i, 1, len) printf("%c", r('a', 'z'));
  puts("");
}
int main() {
  srand(clock() + time(0));
  int n = r(1, 3);
  printf("%d\n", n);
  FOR(i, 1, n) FOR(j, 1, 3) gs();
  int q = r(1, 10);
  printf("%d\n", q);
  FOR(i, 1, q) {
    if (l[1] + l[2] + l[3] == 0 || r(100) > 40) {
      int k = r(1, 3);
      printf("+ %d %c\n", k, r('a', 'z'));
      ++l[k];
    } else {
      int k = r(1, 3);
      while (l[k] <= 0) k = r(1, 3);
      printf("- %d\n", k);
      --l[k];
    }
  }
  return 0;
}
