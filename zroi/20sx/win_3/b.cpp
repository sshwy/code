// by Sshwy
#include <algorithm>/*{{{*/
#include <bitset>
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int SZ = 3e5 + 5, Q = 3e5 + 5, S = 3e5 + 5;

int tot = 3;
int r[4] = {0, 1, 2, 3};
int tr[SZ][26], L[SZ], R[SZ], dfn;
vector<int> A[SZ];
struct p3 {
  int a, b, c;
};
vector<p3> Qry[SZ];
int node[S][4];

void insert(int u, char *s, int id, int k) {
  A[u].pb(id);
  if (*s) {
    char c = *s - 'a';
    if (!tr[u][c]) tr[u][c] = ++tot;
    insert(tr[u][c], s + 1, id, k);
  } else {
    node[id][k] = u;
  }
}

char s1[S], s2[S], s3[S];

int n, q;
int que[4][Q];
int l[4];
int c[S], cur;

void add(int k, char c) {
  c -= 'a';
  int *qk = que[k], &lk = l[k];
  if (qk[lk] == 0)
    qk[++lk] = 0;
  else {
    int u = qk[lk];
    qk[++lk] = tr[u][c];
  }
}
void del(int k) { --l[k]; }

void dfs(int u) {
  L[u] = ++dfn;
  FOR(i, 0, 25) if (tr[u][i]) dfs(tr[u][i]);
  R[u] = dfn;
}

int ans[Q], la;

void add_query() {
  int x[] = {que[1][l[1]], que[2][l[2]], que[3][l[3]]};
  ++la;
  if (x[0] == 0 || x[1] == 0 || x[2] == 0) return ans[la] = 0, void();
  Qry[x[0]].pb({x[1], x[2], la});
}

struct fenwick {
  int c[SZ];
  void add(int pos, int v) {
    for (int i = pos; i < SZ; i += i & -i) c[i] += v;
  }
  int pre(int pos) {
    int res = 0;
    for (int i = pos; i > 0; i -= i & -i) res += c[i];
    return res;
  }
} B;
void solve(int u) {
  struct p5 {
    int a, b, c, d, e;
  };
  vector<p5> v;
  for (int x : A[u]) v.pb({1, L[node[x][2]], L[node[x][3]]});
  for (p3 x : Qry[u]) {
    v.pb({0, R[x.a], R[x.b], 1, x.c});
    v.pb({0, R[x.a], L[x.b] - 1, -1, x.c});
    v.pb({0, L[x.a] - 1, R[x.b], -1, x.c});
    v.pb({0, L[x.a] - 1, L[x.b] - 1, 1, x.c});
  }
  sort(v.begin(), v.end(),
      [](p5 x, p5 y) { return x.b != y.b ? x.b < y.b : x.a > y.a; });
  for (p5 x : v) {
    if (x.a)
      B.add(x.c, 1);
    else
      ans[x.e] += x.d * B.pre(x.c);
  }
  for (p5 x : v)
    if (x.a) B.add(x.c, -1);
}
void dfs2(int u) {
  FOR(i, 0, 25) if (tr[u][i]) dfs2(tr[u][i]);
  if (Qry[u].size()) solve(u);
}

int main() {
  scanf("%d", &n);
  FOR(i, 1, n) {
    scanf("%s%s%s", s1, s2, s3);
    insert(r[1], s1, i, 1);
    insert(r[2], s2, i, 2);
    insert(r[3], s3, i, 3);
  }

  dfs(r[2]);
  dfn = 0;
  dfs(r[3]);

  que[1][1] = r[1];
  que[2][1] = r[2];
  que[3][1] = r[3];
  l[1] = l[2] = l[3] = 1;

  scanf("%d", &q);
  FOR(i, 1, q) {
    char op[5], c[5];
    int k;
    scanf("%s", op);
    if (op[0] == '+') {
      scanf("%d%s", &k, c);
      add(k, c[0]);
    } else {
      scanf("%d", &k);
      del(k);
    }
    add_query();
  }
  dfs2(r[1]);
  FOR(i, 1, la) printf("%d\n", ans[i]);
  return 0;
}
