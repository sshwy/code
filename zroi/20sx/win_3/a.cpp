// by Sshwy
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 5e3 + 5, P = 1e9 + 7;

vector<int> g[N];

int n;
int f[N][N]; // f[i,j]表示i的子树，i在点分树的深度为j的点分树的方案数
int t[N], h[N], sz[N], c[N][N];

void dfs(int u, int p) {
  for (int v : g[u])
    if (v != p) dfs(v, u);
  sz[u] = 1;
  f[u][1] = 1;
  for (int v : g[u])
    if (v != p) {
      // g[i]=f[u][j]*f[v][k]*binom(i-1,j-1) (1<=j<=i,i-j<=k<=sz[v])
      memset(h, 0, sizeof(int) * (sz[u] + sz[v] + 1));
      ROF(i, sz[v], 0) h[i] = (h[i + 1] + f[v][i]) % P;
      FOR(i, 1, sz[u] + sz[v]) {
        FOR(j, max(1, i - sz[v]), min(i, sz[u])) {
          t[i] = (t[i] + f[u][j] * 1ll * c[i - 1][j - 1] % P * h[i - j]) % P;
        }
      }
      sz[u] += sz[v];
      memcpy(f[u], t, sizeof(int) * (sz[u] + 1));
      memset(t, 0, sizeof(int) * (sz[u] + 1));
    }
  // printf("f[%d]: ",u);FOR(i,1,sz[u])printf("%d%c",f[u][i]," \n"[i==sz[u]]);
}

int main() {
  scanf("%d", &n);
  c[0][0] = 1;
  FOR(i, 1, n) {
    c[i][0] = 1;
    FOR(j, 1, i) c[i][j] = (c[i - 1][j] + c[i - 1][j - 1]) % P;
    // FOR(j,1,i)printf("%d%c",c[i][j]," \n"[j==i]);
  }
  FOR(i, 1, n - 1) {
    int u, v;
    scanf("%d%d", &u, &v);
    g[u].pb(v), g[v].pb(u);
  }
  dfs(1, 0);
  int ans = 0;
  FOR(i, 1, n) ans = (ans + f[1][i]) % P;
  printf("%d\n", ans);
  return 0;
}
