#include <bits/stdc++.h>
#define ci const int &
using namespace std;
const long long base = 1e16;
struct Num {
  long long v[50], sz;
  bool operator>(const Num &y) {
    if (sz != y.sz) return sz > y.sz;
    for (int i = sz; i >= 1; --i)
      if (v[i] != y.v[i]) return v[i] > y.v[i];
    return false;
  }
  void operator+=(const Num &y) {
    int w = 0;
    sz = max(sz, y.sz);
    for (int i = 1; i <= sz; ++i) {
      v[i] += y.v[i] + w, w = v[i] / base, v[i] %= base;
      if (w && i == sz) ++sz;
    }
  }
  Num operator*(ci x) {
    Num tmp = (*this);
    int w = 0;
    for (int i = 1; i <= tmp.sz; ++i) {
      tmp.v[i] *= x, tmp.v[i] += w, w = tmp.v[i] / base, tmp.v[i] %= base;
      if (w && i == tmp.sz) ++tmp.sz;
    }
    return tmp;
  }
  int operator%(ci x) {
    long long tmp = 0, tv;
    for (int i = sz; i >= 1; --i) tv = v[i] + tmp * base, tmp = tv % x;
    return tmp;
  }
  void operator/=(ci x) {
    long long tmp = 0;
    for (int i = sz; i >= 0; --i) {
      v[i] += tmp * base, tmp = v[i] % x, v[i] /= x;
      if (i == sz && !v[i]) --sz;
    }
  }
  void prt() {
    for (int i = sz; i >= 1; --i) {
      if (i != sz) {
        long long tmp = base / 10;
        while (tmp >= 10 && v[i] < tmp) putchar('0'), tmp /= 10;
      }
      printf("%lld", v[i]);
    }
  }
} dp[310][150], ans, pw;
struct MS {
  int n[15];
  bool operator<(const MS &y) const {
    for (int i = 1; i <= 14; ++i)
      if (n[i] != y.n[i]) return n[i] < y.n[i];
    return false;
  }
} tt, t, vl[150];
int n, K, tv, cnt, wy[150][150], mx;
map<MS, int> mp;
void dfs(ci x, ci y) {
  if (!x) return (void)(mp[tt] = ++cnt, vl[cnt] = tt);
  for (int i = y; i <= x; ++i) ++tt.n[i], dfs(x - i, i), --tt.n[i];
}
int main() {
  scanf("%d%d", &n, &K);
  dfs(n, 1);
  for (int i = 1; i <= cnt; ++i) {
    for (int j = 1; j <= n; ++j)
      if (vl[i].n[j]) {
        for (int k = 1; k <= j; ++k) {
          tt = vl[i];
          --tt.n[j], ++tt.n[k], ++tt.n[j - k];
          wy[i][mp[tt]] += vl[i].n[j] * j;
        }
      }
    for (int j = 1; j <= n; ++j)
      if (vl[i].n[j]) {
        t = vl[i], --t.n[j];
        for (int k = 1; k <= n; ++k)
          if (t.n[k]) {
            tt = t, --tt.n[k], ++tt.n[j + k];
            wy[i][mp[tt]] += vl[i].n[j] * t.n[k] * j * k;
          }
      }
  }
  for (int i = 1; i <= cnt; i++) {
    for (int j = 1; j <= cnt; j++) { printf("%d%c", wy[i][j], ' '); }
    puts("");
  }
  dp[0][1].v[1] = dp[0][1].sz = pw.v[1] = pw.sz = 1;
  for (int i = 1; i <= K; ++i, pw = pw * (n * n))
    for (int j = 1; j <= cnt; ++j)
      if (dp[i - 1][j].sz)
        for (int k = 1; k <= cnt; ++k)
          if (wy[j][k]) dp[i][k] += dp[i - 1][j] * wy[j][k];
  ans = dp[K][1], tv = n;
  ans.prt();
  puts("");
  pw.prt();
  puts("");
  for (int i = 2; i * i <= tv; ++i)
    if (tv % i == 0) {
      while (tv % i == 0) tv /= i;
      while (ans % i == 0) ans /= i, pw /= i;
    }
  if (tv != 1)
    while (ans % tv == 0) ans /= tv, pw /= tv;
  for (int i = 1; i <= n; ++i) printf("%d%c", i, " \n"[i == n]);
  ans.prt(), putchar('/'), pw.prt();
  return 0;
}
