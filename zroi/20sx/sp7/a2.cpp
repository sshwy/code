// by Sshwy
//#define DEBUGGER
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

#ifdef DEBUGGER

#define log_prefix()                                                \
  {                                                                 \
    fprintf(stderr, "\033[37mLine %-3d [%dms]:\033[0m  ", __LINE__, \
        (int)clock() / 1000);                                       \
  }
#define ilog(...) \
  { fprintf(stderr, __VA_ARGS__); }
#define llog(...)      \
  {                    \
    log_prefix();      \
    ilog(__VA_ARGS__); \
  }
#define log(...)           \
  {                        \
    log_prefix();          \
    ilog(__VA_ARGS__);     \
    fprintf(stderr, "\n"); \
  }
#define red(...)       \
  {                    \
    log_prefix();      \
    ilog("\033[31m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define green(...)     \
  {                    \
    log_prefix();      \
    ilog("\033[32m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define blue(...)      \
  {                    \
    log_prefix();      \
    ilog("\033[34m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }

#else

#define log_prefix() ;
#define ilog(...) ;
#define llog(...) ;
#define log(...) ;
#define red(...) ;
#define green(...) ;
#define blue(...) ;

#endif

namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 1e5 + 5, SZ = N * 2;
int n, k;
pair<int, int> p[N], a[N];

int LU[N], RU[N], LD[N], RD[N]; //四个方向的顶点的个数
vector<int> D;
vector<int> v[SZ], v2[SZ];
map<pair<int, int>, vector<int>> mp;

int c[SZ], tot;
void add(int pos, int v) {
  // printf("add %d %d\n",pos,v);
  for (int i = pos; i < SZ; i += i & -i) c[i] += v;
  tot += v;
}
int pre(int pos) {
  int res = 0;
  for (int i = pos; i > 0; i -= i & -i) res += c[i];
  // printf("pre %d : %d\n",pos,res);
  return res;
}

int main() {
  scanf("%d%d", &n, &k);
  FOR(i, 1, n) scanf("%d%d", &p[i].fi, &p[i].se);
  FOR(i, 1, n) a[i] = {p[i].fi + p[i].se, p[i].se - p[i].fi};
  FOR(i, 1, n) printf("%d %d\n", a[i].fi, a[i].se);

  FOR(i, 1, n) D.pb(a[i].fi), D.pb(a[i].se);
  sort(D.begin(), D.end());
  D.erase(unique(D.begin(), D.end()), D.end());
  FOR(i, 1, n) {
    a[i].fi = lower_bound(D.begin(), D.end(), a[i].fi) - D.begin() + 1;
    a[i].se = lower_bound(D.begin(), D.end(), a[i].se) - D.begin() + 1;
  }

  //按x排序
  FOR(i, 1, n) v[a[i].fi].pb(i);

  FOR(i, 1, int(D.size())) {
    if (v[i].size()) {
      for (int x : v[i]) {
        LD[x] += pre(a[x].se - 1);
        LU[x] += tot - pre(a[x].se);
      }
      for (int x : v[i]) add(a[x].se, 1);
    }
  }

  fill(c, c + SZ, 0), tot = 0;

  ROF(i, int(D.size()), 1) {
    if (v[i].size()) {
      for (int x : v[i]) {
        RD[x] += pre(a[x].se - 1);
        RU[x] += tot - pre(a[x].se);
      }
      for (int x : v[i]) add(a[x].se, 1);
    }
  }

  fill(c, c + SZ, 0), tot = 0;

  FOR(i, 1, int(D.size())) {
    if (v[i].size() > 1) {
      for (int x : v[i]) {
        LD[x] += pre(a[x].se - 1);
        LU[x] += tot - pre(a[x].se);
        add(a[x].se, 1);
      }
      for (int x : v[i]) add(a[x].se, -1);
    }
  }

  FOR(i, 1, int(D.size())) {
    if (v[i].size() > 1) {
      reverse(v[i].begin(), v[i].end());
      for (int x : v[i]) {
        RD[x] += pre(a[x].se - 1);
        RU[x] += tot - pre(a[x].se);
        add(a[x].se, 1);
      }
      for (int x : v[i]) add(a[x].se, -1);
    }
  }

  //按y排序
  FOR(i, 1, n) v2[a[i].se].pb(i);

  fill(c, c + SZ, 0), tot = 0;

  FOR(i, 0, SZ - 1) assert(c[i] == 0 && tot == 0);

  FOR(i, 1, int(D.size())) {
    if (v2[i].size() > 1) {
      for (int x : v2[i]) {
        LD[x] += pre(a[x].fi - 1);
        RD[x] += tot - pre(a[x].fi);
        add(a[x].fi, 1);
      }
      for (int x : v2[i]) add(a[x].fi, -1);
    }
  }
  FOR(i, 0, SZ - 1) assert(c[i] == 0 && tot == 0);

  FOR(i, 1, int(D.size())) {
    if (v2[i].size() > 1) {
      reverse(v2[i].begin(), v2[i].end());
      for (int x : v2[i]) {
        LU[x] += pre(a[x].fi - 1);
        RU[x] += tot - pre(a[x].fi);
        add(a[x].fi, 1);
      }
      for (int x : v2[i]) add(a[x].fi, -1);
    }
  }
  FOR(i, 0, SZ - 1) assert(c[i] == 0 && tot == 0);

  FOR(i, 1, n) mp[a[i]].pb(i);
  for (auto &p : mp) {
    auto x = p.se;
    if (x.size() > 1) {
      for (int i = 0; i < x.size(); i++) {
        LD[x[i]] += i;
        RU[x[i]] += x.size() - i - 1;
      }
    }
  }

  FOR(i, 1, n)
  printf("(%d,%d) (%d,%d) LU %d LD %d RU %d RD %d\n", p[i].fi, p[i].se, a[i].fi,
      a[i].se, LU[i], LD[i], RU[i], RD[i]);

  return 0;
}

// 一个点到P的距离，是关于x的一个类似绝对值的函数。
// 一个点成为最近的k个点之一，是一段连续的区间？是的
//     当它往下走的时候，排名只会变小
//     当它往上走的时候，排名只会变大
// 这个区间必然包含函数的顶点。
// 如果求出了区间，就做完了。
// 对每个函数二分！
// 问题转化为，我们要求在x=X处，f(i)的值的排名。
// 相当于求，四个方向的顶点个数。
// 距离相同的按标号排序。
//
