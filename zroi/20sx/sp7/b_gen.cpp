// by Sshwy
//#define DEBUGGER
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

#ifdef DEBUGGER

#define log_prefix()                                                \
  {                                                                 \
    fprintf(stderr, "\033[37mLine %-3d [%dms]:\033[0m  ", __LINE__, \
        (int)clock() / 1000);                                       \
  }
#define ilog(...) \
  { fprintf(stderr, __VA_ARGS__); }
#define llog(...)      \
  {                    \
    log_prefix();      \
    ilog(__VA_ARGS__); \
  }
#define log(...)           \
  {                        \
    log_prefix();          \
    ilog(__VA_ARGS__);     \
    fprintf(stderr, "\n"); \
  }
#define red(...)       \
  {                    \
    log_prefix();      \
    ilog("\033[31m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define green(...)     \
  {                    \
    log_prefix();      \
    ilog("\033[32m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define blue(...)      \
  {                    \
    log_prefix();      \
    ilog("\033[34m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }

#else

#define log_prefix() ;
#define ilog(...) ;
#define llog(...) ;
#define log(...) ;
#define red(...) ;
#define green(...) ;
#define blue(...) ;

#endif

namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/

using namespace RA;
const int N = 1e5 + 5;
int a[N];
pair<int, int> p[N];
int v[N];

pair<int, int> range(int L, int R) {
  int x = r(L, R), y = r(L, R);
  if (x > y) swap(x, y);
  if (r(100) < 70) {
    while ((y - x) * 2.5 < R - L) x = r(L, R), y = r(L, R);
  }
  return {x, y};
}
int main() {
  srand(clock() + time(0));
  int n = 100000, q = 100000;
  FOR(i, 1, n) a[i] = r(1, 100000);
  int X = r(3000, 5000);
  FOR(i, 1, q) {
    p[i] = range(1, n);
    // v[i]=X;
    v[i] = r(3000, 5000);
  }

  printf("%d %d\n", n, q);
  FOR(i, 1, n) printf("%d%c", a[i], " \n"[i == n]);
  FOR(i, 1, q) printf("%d %d %d\n", p[i].fi, p[i].se, v[i]);

  return 0;
}
