partid={}
totid=0

def dfs(x,las,tp): # 整数划分
    global partid,totid
    if x==0:
        partid[tp]=totid
        totid+=1
        return
    for i in range(las,x+1):
        dfs(x-i,i,tp+(i,))

def make_permutation(tp): # 根据划分的元组创建一个排列 (list)
    las=0
    res = []
    for x in tp:
        L=las+1
        R=las+x
        res.append(R)
        for i in range(L,R):
            res.append(i)
        las=R
    return res

def make_part(p): # 求出它的划分
    vis = [ 0 for i in p ]
    tmp = []
    for x in p:
        if vis[x-1]==0:
            l=1
            u=p[x-1]
            vis[x-1]=1
            while u!=x:
                l+=1
                vis[u-1]=1
                u=p[u-1]
            tmp.append(l)
    tmp.sort()
    tp=()
    for x in tmp:
        tp = tp+(x,)
    return tp
            
# main

n, k = map(int, input().split())

dfs(n,1,())

f = [[0 for i in range(totid)] for i in range(totid) ]

for x in partid:
    ix=partid[x]
    a = make_permutation(x)
    for i in range(n):
        for j in range(n):
            t=a[i]
            a[i]=a[j]
            a[j]=t
            
            y=make_part(a)
            f[ix][partid[y]] += 1

            t=a[i]
            a[i]=a[j]
            a[j]=t

g = [0 for i in range(totid)]

g[0]=1

for _ in range(k):
    h = [0 for i in range(totid)]
    for i in range(totid):
        for j in range(totid):
            h[j] += g[i]*f[i][j]
    g = h

ax = g[0]
ay = n**(k*2)

for i in range(n):
    x = i+1
    if n%x or x==1:
        continue
    while ax%x==0 and ay%x==0:
        ax//=x
        ay//=x

for i in range(n):
    print(i+1, end=' ')

print()

print("%d/%d" % (ax,ay))
