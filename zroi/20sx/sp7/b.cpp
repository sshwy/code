// by Sshwy
//#define DEBUGGER
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

#ifdef DEBUGGER

#define log_prefix()                                                \
  {                                                                 \
    fprintf(stderr, "\033[37mLine %-3d [%dms]:\033[0m  ", __LINE__, \
        (int)clock() / 1000);                                       \
  }
#define ilog(...) \
  { fprintf(stderr, __VA_ARGS__); }
#define llog(...)      \
  {                    \
    log_prefix();      \
    ilog(__VA_ARGS__); \
  }
#define log(...)           \
  {                        \
    log_prefix();          \
    ilog(__VA_ARGS__);     \
    fprintf(stderr, "\n"); \
  }
#define red(...)       \
  {                    \
    log_prefix();      \
    ilog("\033[31m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define green(...)     \
  {                    \
    log_prefix();      \
    ilog("\033[32m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define blue(...)      \
  {                    \
    log_prefix();      \
    ilog("\033[34m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }

#else

#define log_prefix() ;
#define ilog(...) ;
#define llog(...) ;
#define log(...) ;
#define red(...) ;
#define green(...) ;
#define blue(...) ;

#endif

namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 1e5 + 5, SQ = 4005, SZ = N << 2, W = 2e5;
int n, q, T, lt, f[SQ][SQ], a[N];
vector<int> v[SZ];

vector<int> merge(const vector<int> &x, const vector<int> &y) {
  vector<int> res;
  auto px = x.begin(), py = y.begin();
  while (px != x.end() && py != y.end()) {
    if (*px < *py)
      res.pb(*px), ++px;
    else
      res.pb(*py), ++py;
  }
  while (px != x.end()) res.pb(*px), ++px;
  while (py != y.end()) res.pb(*py), ++py;
  return res;
}
void build(int u = 1, int l = 1, int r = n) {
  if (l == r) {
    v[u].pb(a[l]);
    return;
  }
  int mid = (l + r) >> 1;
  build(u << 1, l, mid), build(u << 1 | 1, mid + 1, r);
  v[u] = merge(v[u << 1], v[u << 1 | 1]);
}
const int LIM = 50;
int Query(int L, int R, int k, int u = 1, int l = 1, int r = n) {
  if (L > R || R < l || r < L) return 0;
  if (L <= l && r <= R) {
    const auto &V = v[u];
    auto las = V.begin();
    int res = 0;
    if (V.size() < LIM) {
      for (const int &x : V) res = max(res, x % k);
    } else {
      for (int i = V[0] / k * k + k; i <= W; i += k) {
        auto p = lower_bound(las, V.end(), i);
        if (p == V.begin()) continue;
        --p;
        res = max(res, *p % k);
        if ((p + 1) == V.begin()) break;
        las = p;
      }
    }
    return res;
  }
  int mid = (l + r) >> 1;
  if (R <= mid) return Query(L, R, k, u << 1, l, mid);
  if (mid < L) return Query(L, R, k, u << 1 | 1, mid + 1, r);
  return max(Query(L, R, k, u << 1, l, mid), Query(L, R, k, u << 1 | 1, mid + 1, r));
}

int query(int l, int r, int k) {
  if (k <= T) {
    int res = 0;
    if (l / T == r / T) {
      FOR(i, l, r) res = max(res, a[i] % k);
    } else {
      int lim = l / T * T + T - 1;
      FOR(i, l, lim) res = max(res, a[i] % k);
      l = lim + 1;
      lim = r / T * T;
      FOR(i, lim, r) res = max(res, a[i] % k);
      r = lim - 1;
      l /= T, r /= T;
      FOR(i, l, r) res = max(res, f[i][k]);
    }
    return res;
  } else {
    return Query(l, r, k);
  }
}
int main() {
  scanf("%d%d", &n, &q);
  FOR(i, 1, n) scanf("%d", &a[i]);

  T = max(int(sqrt(n)), 4000);
  lt = n / T;

  FOR(i, 0, lt) {
    int l = max(i * T, 1);
    int r = min((i + 1) * T - 1, n);
    // printf("i %d : %d %d\n",i,l,r);
    FOR(k, 1, T) {
      int mx = 0;
      FOR(x, l, r) mx = max(mx, a[x] % k);
      f[i][k] = mx;
      // printf("f %d %d : %d\n",i,k,mx);
    }
  }

  build();

  FOR(i, 1, q) {
    int l, r, k;
    scanf("%d%d%d", &l, &r, &k);
    printf("%d\n", query(l, r, k));
  }

  return 0;
}

// 根号分治。
// k<=sqrt(W)。预处理。分块
// f[i,j]表示第i块模j的最大值。sqrt。
//
// k>sqrt(W)。枚举t，求[kt,k(t+1)-1]中的最大值。sqrtlog。
// f[n/T][T]
//
// n/T+T  log*W/T*log
