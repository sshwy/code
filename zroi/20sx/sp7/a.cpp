// by Sshwy
#pragma GCC optimize(2)
#pragma GCC optimize(3)
#pragma GCC optimize("Ofast")
#pragma GCC optimize("inline")
#pragma GCC optimize("-fgcse")
#pragma GCC optimize("-fgcse-lm")
#pragma GCC optimize("-fipa-sra")
#pragma GCC optimize("-ftree-pre")
#pragma GCC optimize("-ftree-vrp")
#pragma GCC optimize("-fpeephole2")
#pragma GCC optimize("-ffast-math")
#pragma GCC optimize("-fsched-spec")
#pragma GCC optimize("unroll-loops")
#pragma GCC optimize("-falign-jumps")
#pragma GCC optimize("-falign-loops")
#pragma GCC optimize("-falign-labels")
#pragma GCC optimize("-fdevirtualize")
#pragma GCC optimize("-fcaller-saves")
#pragma GCC optimize("-fcrossjumping")
#pragma GCC optimize("-fthread-jumps")
#pragma GCC optimize("-funroll-loops")
#pragma GCC optimize("-fwhole-program")
#pragma GCC optimize("-freorder-blocks")
#pragma GCC optimize("-fschedule-insns")
#pragma GCC optimize("inline-functions")
#pragma GCC optimize("-ftree-tail-merge")
#pragma GCC optimize("-fschedule-insns2")
#pragma GCC optimize("-fstrict-aliasing")
#pragma GCC optimize("-fstrict-overflow")
#pragma GCC optimize("-falign-functions")
#pragma GCC optimize("-fcse-skip-blocks")
#pragma GCC optimize("-fcse-follow-jumps")
#pragma GCC optimize("-fsched-interblock")
#pragma GCC optimize("-fpartial-inlining")
#pragma GCC optimize("no-stack-protector")
#pragma GCC optimize("-freorder-functions")
#pragma GCC optimize("-findirect-inlining")
#pragma GCC optimize("-fhoist-adjacent-loads")
#pragma GCC optimize("-frerun-cse-after-loop")
#pragma GCC optimize("inline-small-functions")
#pragma GCC optimize("-finline-small-functions")
#pragma GCC optimize("-ftree-switch-conversion")
#pragma GCC optimize("-foptimize-sibling-calls")
#pragma GCC optimize("-fexpensive-optimizations")
#pragma GCC optimize("-funsafe-loop-optimizations")
#pragma GCC optimize("inline-functions-called-once")
#pragma GCC optimize("-fdelete-null-pointer-checks")
#include <bits/stdc++.h>
using namespace std;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace IO { // require C++11
  const int _BS = 200000;
  char _ob[_BS], *_z = _ob, _ib[_BS], *_x = _ib, *_y = _ib;
  // Input
  char _nc() {
    return _x == _y && (_y = (_x = _ib) + fread(_ib, 1, _BS, stdin), _x == _y)
               ? EOF
               : *_x++;
  }
  inline void rd(char &c) { c = _nc(); }
  void rd(char *s) {
    char c = _nc();
    while (!isblank(c) && isprint(c) && c != EOF) *s++ = c, c = _nc();
    *s = 0;
  }
  template <class T> void rd(T &res) { // unsigned/signed int/long long
    res = 0;
    char c = _nc(), flag = 0;
    while (!isdigit(c) && c != '-') c = _nc();
    if (c == '-') flag = 1, c = _nc();
    while (isdigit(c)) res = res * 10 + c - '0', c = _nc();
    if (flag) res = -res;
  }
  template <class T, class... A> void rd(T &hd, A &...rs) { rd(hd), rd(rs...); }
  // Output
  inline void flush() { fwrite(_ob, 1, _z - _ob, stdout), _z = _ob; }
  inline void wrch(char x) {
    if (_z == _ob + _BS - 1) flush();
    *_z = x, ++_z;
  }
  void wr(char *s) {
    while (*s) wrch(*s), ++s;
  }
  template <class T> void wr(T x) { // unsigned/signed int/long long
    if (x == 0) return wrch('0'), void();
    if (x < 0) wrch('-'), x = -x;
    int _t[30], _lt = 0;
    while (x) _t[++_lt] = x % 10, x /= 10;
    while (_lt > 0) wrch(_t[_lt] + '0'), --_lt;
  }
  inline void wr(char x) { wrch(x); }
  template <class T, class... A> void wr(T hd, A... rs) { wr(hd), wr(rs...); }
  template <class... A> void wrln(A... ar) { wr(ar...), wrch('\n'); }
  struct _Flusher {
    ~_Flusher() { flush(); }
  } _flusher;
} // namespace IO

const int N = 5e5 + 5;
int n, k;
long long c1[N], c2[N], d1[N], d2[N], ans = 1e18;
pair<int, int> p[N];
void add(long long *f, int pos, long long v) {
  for (int i = pos; i < N; i += i & -i) f[i] += v;
}
long long pre(long long *f, int pos) {
  long long res = 0;
  for (int i = pos; i > 0; i -= i & -i) res += f[i];
  return res;
}
vector<int> D;

int main() {
  IO::rd(n, k);
  FOR(i, 1, n) {
    int x, y;
    IO::rd(x, y);
    p[i] = {x, y};
    D.pb(x + y);
    D.pb(y - x);
  }
  sort(p + 1, p + n + 1);
  sort(D.begin(), D.end());
  D.erase(unique(D.begin(), D.end()), D.end());

  FOR(i, 1, n) {
    int pos = lower_bound(D.begin(), D.end(), p[i].fi + p[i].se) - D.begin() + 1;
    add(c2, pos, 1);
    add(d2, pos, p[i].fi + p[i].se);
  }
  FOR(i, 1, n) {
    long long x = p[i].fi;
    long long l = 0, r = 2e9 + 1, mid;
    while (l < r) {
      mid = (l + r + 1) >> 1;
      long long t = pre(c1, upper_bound(D.begin(), D.end(), mid - x) - D.begin()) +
                    pre(c2, upper_bound(D.begin(), D.end(), mid + x) - D.begin());
      if (t >= k)
        r = mid - 1;
      else
        l = mid;
    }
    int p1 = upper_bound(D.begin(), D.end(), l - x) - D.begin(),
        p2 = upper_bound(D.begin(), D.end(), l + x) - D.begin();
    long long t = pre(c1, p1) + pre(c2, p2);
    if (t <= k)
      ans = min(ans, pre(c1, p1) * x - pre(c2, p2) * x + pre(d1, p1) + pre(d2, p2) +
                         (k - t) * (l + 1ll));
    else
      ans = 0;

    if (ans == 0) break;

    int x2 = lower_bound(D.begin(), D.end(), p[i].fi + p[i].se) - D.begin() + 1,
        x1 = lower_bound(D.begin(), D.end(), p[i].se - p[i].fi) - D.begin() + 1;
    add(c2, x2, -1);
    add(c1, x1, 1);
    add(d2, x2, -p[i].fi - p[i].se);
    add(d1, x1, p[i].se - p[i].fi);
  }
  IO::wrln(ans);
  return 0;
}
