#include <algorithm>
#include <bitset>
#include <cmath>
#include <cstdio>
#include <cstring>
#include <iostream>
#include <map>
#include <queue>
#define ll long long
#define maxn 405
#define inf 1e9
#define re(i, a, b) for (int i = a; i <= b; i++)
#define _re(i, a, b) for (int i = a; i >= b; i--)
#define for_edge(x) for (int i = head[x]; i; i = e[i].nxt)
#define pr2(x)                            \
  {                                       \
    _re(i, n, 1) printf("%d", bit(x, i)); \
    putchar(' ');                         \
  }
#define p 998244353
using namespace std;
inline ll read() {
  char c = getchar();
  int f = 1;
  ll ans = 0;
  while (c > '9' || c < '0') {
    if (c == '-') f = -f;
    c = getchar();
  }
  while (c <= '9' && c >= '0') {
    ans = ans * 10 + c - '0';
    c = getchar();
  }
  return ans * f;
}
//_____________________________________________________________________________________________________
inline ll ksm(ll x, ll b) {
  ll ans = 1;
  while (b) {
    if (b & 1) (ans *= x) %= p;
    (x *= x) %= p;
    b >>= 1;
  }
  return ans;
}
struct node {
  ll F0, F1;
  inline node(int F0 = 0, int F1 = 0)
      : F0(F0)
      , F1(F1){};
  inline node operator*(const node a) const {
    return node(F0 * a.F0 % p, (F1 * a.F0 + F0 * a.F1) % p);
  }
  inline node operator*(const int a) const { return node(F0 * a % p, F1 * a % p); }
  inline node operator+(const node a) const {
    return node((F0 + a.F0) % p, (F1 + a.F1) % p);
  }
  inline node inv() {
    ll inv = ksm(F0, p - 2);
    return node(inv, -F1 * inv % p * inv % p);
  }

} mix[maxn][maxn];
int n;
inline void gauss() {
  re(i, 1, n) {
    int t = i;
    while (t <= n && mix[t][i].F0 == 0 && mix[t][i].F1 == 0) t++;
    swap(mix[i], mix[t]);
    re(j, i + 1, n) {
      node rat = mix[j][i] * mix[i][i].inv() * (-1);
      re(t, i, n) mix[j][t] = mix[j][t] + rat * mix[i][t];
    }
  }
}
int main() {
  n = read();
  re(i, 1, n) re(j, 1, n) {
    char c = getchar();
    while (!isdigit(c)) c = getchar();
    if (c == '1') {
      mix[i][j].F0--;
      mix[i][i].F0++;
    }
  }

  re(i, 1, n) re(j, 1, n) {
    char c = getchar();
    while (!isdigit(c)) c = getchar();
    if (mix[i][j].F0 && c == '1') {
      mix[i][j].F1--;
      mix[i][i].F1++;
    }
  }
  n--;
  gauss();
  node ans = node(1, 0);
  re(i, 1, n) ans = ans * mix[i][i];
  printf("%lld\n", (ans.F1 + p) % p);
  return 0;
}
