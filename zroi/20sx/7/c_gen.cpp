// by Sshwy
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
using namespace RA;
const int N = 500, P = 998244353;

int pw(int a, int m) {
  int res = 1;
  while (m) m & 1 ? res = 1ll * res * a % P : 0, a = 1ll * a * a % P, m >>= 1;
  return res;
}
int t[N][N];
int det(const int f[N][N], int n) {
  memcpy(t, f, sizeof(int) * N * N);
  int sig = 1;
  FOR(i, 1, n) {
    FOR(j, i + 1, n) if (t[j][i]) {
      FOR(k, 1, n) swap(t[i][k], t[j][k]);
      sig = -sig;
      break;
    }
    if (!t[i][i]) return 0;
    FOR(j, 1, n) if (i != j) {
      int rate = t[j][i] * 1ll * pw(t[i][i], P - 2) % P;
      FOR(k, i, n) t[j][k] = (t[j][k] - 1ll * t[i][k] * rate % P + P) % P;
    }
  }
  int res = 1;
  FOR(i, 1, n) res = 1ll * res * t[i][i] % P;
  res = (res * sig + P) % P;
  return res;
}

int n;
int g[N][N];

int main() {
  srand(clock() + time(0));
  int n = 4;
  printf("%d\n", n);
  while (det(g, n) == 0) {
    FOR(i, 1, n) FOR(j, 1, i - 1) g[i][j] = r(100) < 80;
    FOR(i, 1, n) FOR(j, i + 1, n) g[i][j] = g[j][i];
  }
  FOR(i, 1, n) {
    FOR(j, 1, n) printf("%d", g[i][j]);
    puts("");
  }
  FOR(i, 1, n) FOR(j, 1, i - 1) g[i][j] = r(100) < 80;
  FOR(i, 1, n) FOR(j, i + 1, n) g[i][j] = g[j][i];
  while (det(g, n) == 0) {
    FOR(i, 1, n) FOR(j, 1, i - 1) g[i][j] = r(100) < 90;
    FOR(i, 1, n) FOR(j, i + 1, n) g[i][j] = g[j][i];
  }
  FOR(i, 1, n) {
    FOR(j, 1, n) printf("%d", g[i][j]);
    puts("");
  }
  return 0;
}
