// by Sshwy
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
#define double long double
const int N = 104;

int n;
double rx, ry;

struct point {
  double x, y;
  int tag;
  point() { x = y = 0; }
  point(double _x, double _y) { x = _x, y = _y; }
  point operator-(point X) { return point(x - X.x, y - X.y); }
  point operator+(point X) { return point(x + X.x, y + X.y); }
  point operator*(point X) {
    return point(x * X.x - y * X.y, x * X.y + y * X.x);
  } // complex multiply
} pR;

struct circle {
  point o;
  double r;
  circle() { o.x = o.y = r = 0; }
  circle(double _x, double _y, double _r) { o = point(_x, _y), r = _r; }
  void read() { scanf("%Lf%Lf%Lf", &o.x, &o.y, &r), o = o * pR; }
  point angle_cross_point(double rad) {
    return o + point(cos(rad) * r, sin(rad) * r);
  }
} C[N];

const double PI = acos(-1), RT = PI / 2, eps = 1e-11;

double _sqr(double x) { return x * x; }
double distance(point A, point B) { return sqrt(_sqr(A.x - B.x) + _sqr(A.y - B.y)); }
double distance2(point A, point B) { return _sqr(A.x - B.x) + _sqr(A.y - B.y); }
bool iszero(double x) { return abs(x) < eps; }

int lp;
point p[N * N * 4];

bool check(point x) { //检查是否在某个圆内
  FOR(i, 1, n) if (distance(x, C[i].o) + eps < C[i].r) return 1;
  return 0;
}
void add(const point x, const int tag) {
  if (check(x)) return;
  p[++lp] = x;
  p[lp].tag = tag;
}
void add_tan_point(circle A, int tagA, circle B, int tagB) {
  if (A.r > B.r) swap(A, B), swap(tagA, tagB);
  if (distance(A.o, B.o) + A.r < B.r + eps) return; //圆套圆
  //我们只需要求外公切线。而且排除了圆套圆后，可以证明，一个外公切点最多在一个圆上。
  double h = sqrt(distance2(A.o, B.o) - _sqr(A.r - B.r));
  double Alpha = atan2(abs(A.r - B.r), h);
  double Beta = iszero(B.o.x - A.o.x) ? RT : atan2(B.o.y - A.o.y, B.o.x - A.o.x);
  add(A.angle_cross_point(Beta + Alpha + RT), tagA);
  add(B.angle_cross_point(Beta + Alpha + RT), tagB);
  add(A.angle_cross_point(Beta - Alpha - RT), tagA);
  add(B.angle_cross_point(Beta - Alpha - RT), tagB);
}

double det(point a, point b) { return a.x * b.y - a.y * b.x; }

int vis[N * N * 4], s[N * N * 4], tp;
void andrew() { //求凸包，开头结尾是重复的
  sort(p + 1, p + lp + 1,
      [](point x, point y) { return iszero(x.x - y.x) ? x.y < y.y : x.x < y.x; });
  FOR(i, 0, lp) vis[i] = 0;
  s[++tp] = 1;
  FOR(i, 2, lp) {
    while (tp > 1 && det(p[s[tp]] - p[s[tp - 1]], p[i] - p[s[tp]]) < eps) --tp;
    s[++tp] = i;
  }
  FOR(i, 1, tp) vis[s[i]] = 1;
  vis[1] = 0;
  int tt = tp;
  ROF(i, lp, 1) if (!vis[i]) {
    while (tp > tt && det(p[s[tp]] - p[s[tp - 1]], p[i] - p[s[tp]]) < eps) --tp;
    s[++tp] = i;
  }
  assert(s[tp] == 1 && s[1] == 1);
}

double calc(point A, point B) {
  if (A.tag != B.tag) return distance(A, B);
  circle c = C[A.tag];
  double ang = (atan2(B.y - c.o.y, B.x - c.o.x) - atan2(A.y - c.o.y, A.x - c.o.x));
  if (ang < 0) ang += PI * 2;
  return c.r * ang;
}

void go() {
  double R = rand() % 998244353 / 998244353.0 * 2 * PI;
  pR = point(cos(R), ry = sin(R)); //把所有圆的圆心随机旋转一个角度，防止爆精度
  lp = 0, tp = 0;
  scanf("%d", &n);
  FOR(i, 1, n) C[i].read();
  FOR(i, 1, n) FOR(j, i + 1, n) add_tan_point(C[i], i, C[j], j);
  andrew();
  double ans = 0;
  if (tp == 1) { //一个圆覆盖了其他所有，答案就是这个圆的周长
    FOR(i, 1, n) ans = max(ans, C[i].r * 2 * PI);
    printf("%.11Lf\n", ans);
  } else {
    FOR(i, 1, tp - 1) ans += calc(p[s[i]], p[s[i + 1]]);
    printf("%.11Lf\n", ans);
  }
}
int main() {
  srand(time(0));
  int t;
  scanf("%d", &t);
  FOR(i, 1, t) go();
  return 0;
}
