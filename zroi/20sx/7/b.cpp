// by Sshwy
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
namespace IO {
  const int BSZ = 100000;
  char nc() {
    static char bf[BSZ], *p1 = bf, *p2 = bf;
    return p1 == p2 && (p2 = (p1 = bf) + fread(bf, 1, BSZ, stdin), p1 == p2) ? EOF
                                                                             : *p1++;
  }
  int rd() {
    int res = 0;
    char c = nc();
    while (!isdigit(c)) c = nc();
    while (isdigit(c)) res = res * 10 + c - '0', c = nc();
    return res;
  }
  long long rdll() {
    long long res = 0;
    char c = nc();
    while (!isdigit(c)) c = nc();
    while (isdigit(c)) res = res * 10 + c - '0', c = nc();
    return res;
  }
  void rd(char *s) {
    char c = nc();
    while (!isblank(c) && isprint(c) && c != EOF) *s++ = c, c = nc();
    *s = 0;
  }
  int tmp[20], lt;
  char OBF[BSZ], *OBP = OBF;
  void flush() { fwrite(OBF, 1, OBP - OBF, stdout), OBP = OBF; }
  void wrch(char x) {
    if (OBP == OBF + BSZ - 1) flush();
    *OBP = x, ++OBP;
  }
  void wr(int x, int base = 10) {
    lt = 0;
    while (x) tmp[++lt] = x % base, x /= base;
    while (lt > 0) wrch(tmp[lt] + '0'), --lt;
  }
  void wr(long long x, int base = 10) {
    lt = 0;
    while (x) tmp[++lt] = x % base, x /= base;
    while (lt > 0) wrch(tmp[lt] + '0'), --lt;
  }
  void wr(const char *s) {
    while (*s) wrch(*s), ++s;
  }
  void wr(char x) { wrch(x); }
} // namespace IO
/******************heading******************/
const int N = 1e5 + 5, SQRTN = 400;

int c[SQRTN][N], tag[SQRTN], cnt[SQRTN], bpos[SQRTN];
int t[SQRTN][N]; // tmp
//小于tag的数的BIT；tag值；=tag的数的个数

int n, m, T, lt;
int a[N], b[N];

void add(int *f, int lim, int pos, int v) {
  for (int i = pos; i <= lim; i += i & (-i)) f[i] += v;
}
int pre(int *f, int lim, int pos) {
  int res = 0;
  for (int i = pos; i > 0; i -= i & -i) res += f[i];
  return res;
}
void modify(int l, int r, int x) {
  int L = l / T, R = r / T;
  // printf("modify(%d,%d,%d)\n",l,r,x);
  // printf("L=%d,R=%d\n",L,R);
  if (L == R) {
    if (tag[L] > x) {
      FOR(i, l, r) {
        a[i] = min(a[i], tag[L]);
        if (a[i] > x) add(c[L], n + 1, a[i], -1), a[i] = x, add(c[L], n + 1, a[i], 1);
      }
    }
  } else {
    if (tag[L] > x) {
      int rr = L * T + T - 1;
      FOR(i, l, rr) {
        a[i] = min(a[i], tag[L]);
        if (a[i] > x) add(c[L], n + 1, a[i], -1), a[i] = x, add(c[L], n + 1, a[i], 1);
      }
    }
    ++L;
    if (tag[R] > x) {
      int ll = R * T;
      FOR(i, ll, r) {
        a[i] = min(a[i], tag[R]);
        if (a[i] > x) add(c[R], n + 1, a[i], -1), a[i] = x, add(c[R], n + 1, a[i], 1);
      }
    }
    --R;
    FOR(i, L, R) {
      if (tag[i] > x) {
        cnt[i] = pre(c[i], n + 1, n + 1) - pre(c[i], n + 1, x - 1);
        tag[i] = x;
      }
    }
    // puts("GG");
  }
}

int count(int l, int r, int pos) { //[pos-lowbit(pos)+1,pos]的数的个数
  int L = l / T, R = r / T, res = 0;
  int pL = pos - (pos & -pos) + 1, pR = pos;
  if (L == R) {
    FOR(i, l, r) res += pL <= a[i] && a[i] <= pR;
  } else {
    int rr = L * T + T - 1;
    FOR(i, l, rr) res += pL <= a[i] && a[i] <= pR;
    ++L;
    int ll = R * T;
    FOR(i, ll, r) res += pL <= a[i] && a[i] <= pR;
    --R;
    FOR(i, L, R) {
      int addv = pos < tag[i] ? c[i][pos] : T - t[i][pos - (pos & -pos)];
      res += addv;
      t[i][pos] = t[i][pos - (pos & -pos)] + addv;
    }
  }

  return res;
}
void refresh(int l, int r) {
  int L = l / T, R = r / T;
  if (L == R) {
    FOR(i, l, r) a[i] = min(a[i], tag[L]);
  } else {
    int rr = L * T + T - 1;
    FOR(i, l, rr) a[i] = min(a[i], tag[L]);
    int ll = R * T;
    FOR(i, ll, r) a[i] = min(a[i], tag[R]);
  }
}
int query(int l, int r, int k) {
  int pos = 0, tot = 0;
  refresh(l, r);
  ROF(j, 20, 0) {
    int step = 1 << j;
    int t = count(l, r, pos + step);
    if (t + tot < k) {
      pos += step;
      tot += t;
    }
  }
  return pos + 1;
}

int main() {
  n = IO::rd(), m = IO::rd();
  FOR(i, 0, n - 1) a[i] = IO::rd(), b[i] = a[i];

  T = max((int)(sqrt(n * log(n) / log(2))), 2);
  lt = (n - 1) / T;

  FOR(i, 0, n - 1) add(c[i / T], n + 1, a[i], 1);
  FOR(i, 0, lt) {
    int L = i * T, R = min(i * T + T - 1, n - 1);
    bpos[i] = R;
    sort(b + L, b + R + 1);
    tag[i] = n + 1;
  }

  FOR(i, 1, m) {
    int op, l, r, x;
    op = IO::rd(), l = IO::rd(), r = IO::rd(), x = IO::rd();
    --l, --r;
    if (op == 1) {
      modify(l, r, x);
    } else {
      IO::wr(query(l, r, x));
      IO::wr('\n');
    }
  }
  IO::flush();
  return 0;
}
