// by Sshwy
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/

using namespace RA;

int main() {
  srand(clock());
  int n = 60, m = 60;
  printf("%d %d\n", n, m);
  FOR(i, 1, n) printf("%d%c", r(1, n), " \n"[i == n]);
  FOR(i, 1, m) {
    if (r(100) < 50) {
      int l = r(1, n), R = r(1, n), x = r(1, n);
      if (l > R) swap(l, R);
      while (R - l < n / 10) l = r(1, n), R = r(1, n);
      // l=1,R=n;
      printf("%d %d %d %d\n", 1, l, R, x);
    } else {
      int l = r(1, n), R = r(1, n), x = r(1, n);
      if (l > R) swap(l, R);
      // l=1,R=n;
      while (R - l < n / 3) l = r(1, n), R = r(1, n);
      x = r(1, R - l + 1);
      printf("%d %d %d %d\n", 2, l, R, x);
    }
  }
  return 0;
}
