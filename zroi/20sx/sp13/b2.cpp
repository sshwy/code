#include <bits/stdc++.h>
#define fi first
#define se second
using namespace std;

typedef long long ll;
const int maxn = 1e5, maxm = maxn * 2, mod = 998244353;
int n, m, k, cur, rt, tmp, sz[maxn + 3], mx[maxn + 3], res[maxm + 3];
int tot, ter[maxm + 3], wei[maxm + 3], nxt[maxm + 3], lnk[maxn + 3];
int fact[maxn + 3], finv[maxn + 3];
ll dep[maxn + 3];
vector<ll> vec[maxn + 3];
bool vis[maxn + 3];

int qpow(int a, int b) {
  int c = 1;
  for (; b; b >>= 1, a = 1ll * a * a % mod)
    if (b & 1) c = 1ll * a * c % mod;
  return c;
}

void prework(int n) {
  fact[0] = 1;
  for (int i = 1; i <= n; i++) fact[i] = 1ll * fact[i - 1] * i % mod;
  finv[n] = qpow(fact[n], mod - 2);
  for (int i = n; i; i--) finv[i - 1] = 1ll * finv[i] * i % mod;
}

void add(int u, int v, int w) {
  ter[++tot] = v, wei[tot] = w, nxt[tot] = lnk[u], lnk[u] = tot;
}

void cent(int u, int f = 0) {
  sz[u] = 1, mx[u] = 0;
  for (int i = lnk[u]; i; i = nxt[i]) {
    int v = ter[i];
    if (vis[v] || v == f) continue;
    cent(v, u);
    sz[u] += sz[v];
    mx[u] = max(mx[u], sz[v]);
  }
  mx[u] = max(mx[u], cur - sz[u]);
  if (!rt || mx[u] < mx[rt]) rt = u;
}

void dfs(int u, int f = 0) {
  vec[tmp].push_back(dep[u]);
  sz[u] = 1;
  for (int i = lnk[u]; i; i = nxt[i]) {
    int v = ter[i];
    if (vis[v] || v == f) continue;
    dep[v] = dep[u] + wei[i];
    dfs(v, u);
    sz[u] += sz[v];
  }
}

int id(int i) { return (i + 1) >> 1; }

void work(int u, int f, int e) {
  int c =
      upper_bound(vec[0].begin(), vec[0].end(), k - (dep[u] + cur)) - vec[0].begin();
  c -= upper_bound(vec[tmp].begin(), vec[tmp].end(), k - (dep[u] + cur * 2ll)) -
       vec[tmp].begin();
  res[u] += c, res[e] += c;
  for (int i = lnk[u]; i; i = nxt[i]) {
    int v = ter[i];
    if (vis[v] || v == f) continue;
    work(v, u, id(i) + n);
  }
}

void solve(int u, int tot) {
  cur = tot, rt = 0, cent(u);
  u = rt, vis[u] = true;
  int cnt = 0;
  for (int i = lnk[u]; i; i = nxt[i]) cnt += !vis[ter[i]];
  for (int i = 0; i <= cnt; i++) vector<ll>().swap(vec[i]);
  tmp = 0;
  for (int i = lnk[u]; i; i = nxt[i]) {
    int v = ter[i], w = wei[i];
    if (vis[v]) continue;
    ++tmp, dep[v] = 0, dfs(v, u);
    for (int j = 0; j < vec[tmp].size(); j++) { vec[0].push_back(vec[tmp][j] + w); }
  }
  vec[0].push_back(0);
  for (int i = 0; i <= cnt; i++) sort(vec[i].begin(), vec[i].end());
  tmp = 0, cur = 0;
  int c = upper_bound(vec[0].begin(), vec[0].end(), k) - vec[0].begin();
  res[u] += c;
  for (int i = lnk[u]; i; i = nxt[i]) {
    int v = ter[i], w = wei[i];
    if (vis[v]) continue;
    ++tmp, cur = w;
    res[id(i) + n] +=
        upper_bound(vec[tmp].begin(), vec[tmp].end(), k - w) - vec[tmp].begin();
    work(v, u, id(i) + n);
  }
  for (int i = lnk[u]; i; i = nxt[i]) {
    int v = ter[i];
    if (vis[v]) continue;
    solve(v, sz[v]);
  }
}

int binom(int n, int m) {
  if (m > n) return 0;
  return 1ll * fact[n] * finv[m] % mod * finv[n - m] % mod;
}

int main() {
  scanf("%d %d %d", &n, &m, &k);
  prework(n);
  for (int i = 1; i < n; i++) {
    int u, v, w;
    scanf("%d %d %d", &u, &v, &w);
    add(u, v, w), add(v, u, w);
  }
  solve(1, n);
  int sum = 0;
  for (int i = 1; i <= n; i++) sum = (sum + binom(res[i], m)) % mod;
  for (int i = n + 1; i < n << 1; i++) sum = (sum + mod - binom(res[i], m)) % mod;
  sum = 1ll * sum * fact[m] % mod;
  printf("%d\n", sum);
  return 0;
}
