// by Sshwy
//#define DEBUGGER
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

#ifdef DEBUGGER

#define log_prefix()                                                \
  {                                                                 \
    fprintf(stderr, "\033[37mLine %-3d [%dms]:\033[0m  ", __LINE__, \
        (int)clock() / 1000);                                       \
  }
#define ilog(...) \
  { fprintf(stderr, __VA_ARGS__); }
#define llog(...)      \
  {                    \
    log_prefix();      \
    ilog(__VA_ARGS__); \
  }
#define log(...)           \
  {                        \
    log_prefix();          \
    ilog(__VA_ARGS__);     \
    fprintf(stderr, "\n"); \
  }
#define red(...)       \
  {                    \
    log_prefix();      \
    ilog("\033[31m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define green(...)     \
  {                    \
    log_prefix();      \
    ilog("\033[32m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define blue(...)      \
  {                    \
    log_prefix();      \
    ilog("\033[34m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }

#else

#define log_prefix() ;
#define ilog(...) ;
#define llog(...) ;
#define log(...) ;
#define red(...) ;
#define green(...) ;
#define blue(...) ;

#endif

namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 1e5 + 5, P = 998244353;

struct qxx {
  int nex, t, v;
} e[N * 2];
int h[N], le = 1;
void add_path(int f, int t, int v) { e[++le] = {h[f], t, v}, h[f] = le; }

int n, m, k, sz[N], sm[N], Cut[N], fa[N][20], dep[N];
long long d[N];
vector<int> s1[N], s2[N];
// s1: 到连通块重心的距离集合
// s2: 到点分树父亲的距离集合

void Size(int u, int p) {
  sz[u] = 1, sm[u] = 0;
  for (int i = h[u]; i; i = e[i].nex) {
    int v = e[i].t;
    if (v != p && !Cut[v]) Size(v, u), sz[u] += sz[v], sm[u] = max(sm[u], sz[v]);
  }
}
int Core(int u, int p, int T) {
  int z = u;
  for (int i = h[u]; i; i = e[i].nex) {
    int v = e[i].t;
    if (v != p && !Cut[v]) {
      int x = Core(v, u, T);
      if (max(sm[z], T - sz[z]) > max(sm[x], T - sz[x])) z = x;
    }
  }
  return z;
}
int Fa[N], Fw[N];
void dfs1(int u, int p, int dist, vector<int> &S) {
  if (dist > k) return;
  S.pb(dist);
  for (int i = h[u]; i; i = e[i].nex) {
    int v = e[i].t, w = e[i].v;
    if (v != p && !Cut[v]) { dfs1(v, u, dist + w, S); }
  }
}
void Solve(int u, int p, int val) {
  Size(u, p);
  int core = Core(u, p, sz[u]);
  Fa[core] = p, Fw[core] = val;
  dfs1(core, 0, 0, s1[core]);
  if (p) dfs1(u, p, val, s2[core]);
  sort(s1[core].begin(), s1[core].end());
  sort(s2[core].begin(), s2[core].end());
  Cut[core] = 1;
  for (int i = h[core]; i; i = e[i].nex) {
    int v = e[i].t, w = e[i].v;
    if (!Cut[v]) { Solve(v, core, w); }
  }
}
int f1[N], f2[N];
pair<int, int> stk[N];
int tp;
void dfs2(int u, int p, long long dist, int pos) {
  stk[++tp] = {u, dist};
  d[u] = dist;
  dep[u] = dep[p] + 1;
  fa[u][0] = p;
  FOR(j, 1, 19) {
    fa[u][j] = fa[fa[u][j - 1]][j - 1];
    if (fa[u][j] == 0) break;
  }
  while (dist - stk[pos].se > k) ++pos;
  if (pos) f2[stk[pos].fi]++;
  for (int i = h[u]; i; i = e[i].nex) {
    int v = e[i].t, w = e[i].v;
    if (v != p) dfs2(v, u, dist + w, pos);
  }
  --tp;
}
int lca(int u, int v) {
  if (dep[u] < dep[v]) swap(u, v);
  ROF(j, 19, 0) if (dep[u] - (1 << j) >= dep[v]) u = fa[u][j];
  if (u == v) return u;
  ROF(j, 19, 0) if (fa[u][j] != fa[v][j]) u = fa[u][j], v = fa[v][j];
  return fa[u][0];
}
long long dis(int u, int v) { return d[u] + d[v] - 2 * d[lca(u, v)]; }
int leq(const vector<int> &V, int val) {
  return upper_bound(V.begin(), V.end(), val) - V.begin();
}
int calc(int u) {
  int z = s1[u].size(), u1 = u;
  while (Fa[u]) {
    long long dist = k - dis(u1, Fa[u]);
    if (dist >= 0) z += leq(s1[Fa[u]], dist) - leq(s2[u], dist);
    u = Fa[u];
  }
  return z;
}
int fac[N], fnv[N];
int pw(int a, int m) {
  int res = 1;
  while (m) m & 1 ? res = 1ll * res * a % P : 0, a = 1ll * a * a % P, m >>= 1;
  return res;
}
int binom(int a, int b) {
  if (a < b) return 0;
  return fac[a] * 1ll * fnv[b] % P * fnv[a - b] % P;
}

int main() {
  scanf("%d%d%d", &n, &m, &k);
  if (m == 1) return printf("%d\n", n), 0;
  FOR(i, 1, n - 1) {
    int u, v, w;
    scanf("%d%d%d", &u, &v, &w);
    add_path(u, v, w);
    add_path(v, u, w);
  }
  dfs2(1, 0, 0, 0);
  Solve(1, 0, 0);
  FOR(i, 1, n) f1[i] = calc(i);
  f2[1] = f1[1];

  fac[0] = 1;
  FOR(i, 1, n) fac[i] = 1ll * fac[i - 1] * i % P;
  fnv[n] = pw(fac[n], P - 2);
  ROF(i, n, 1) fnv[i - 1] = fnv[i] * 1ll * i % P;

  int ans = 0;
  FOR(i, 1, n) {
    assert(f1[i] >= f2[i]);
    int z = (binom(f1[i], m) - binom(f1[i] - f2[i], m)) % P;
    ans = (0ll + ans + z) % P;
  }
  ans = (ans + P) % P;
  ans = ans * 1ll * fac[m] % P;
  printf("%d\n", ans);

  return 0;
}

// 与m个点距离不超过k的是一个连通块。
// 以连通块上深度最浅的点代表它。
// 先定根。
// 换言之，确定了点u，我们要统计选m个点距离u<=k，至少有一个点距离fa(u)>k的方案数。
// 也就是说我们要统计：
// 1. 距离u<=k的点数f1[u]；
// 2. 距离u<=k,fa(u)>k的点数f2[u]。
// 对于1，可以点分树来求。
// 对于2，这样的点一定在u子树里。我们可以计算每个点的贡献，做树上差分即可。
