// by Sshwy
//#define DEBUGGER
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

#ifdef DEBUGGER

#define log_prefix()                                                \
  {                                                                 \
    fprintf(stderr, "\033[37mLine %-3d [%dms]:\033[0m  ", __LINE__, \
        (int)clock() / 1000);                                       \
  }
#define ilog(...) \
  { fprintf(stderr, __VA_ARGS__); }
#define llog(...)      \
  {                    \
    log_prefix();      \
    ilog(__VA_ARGS__); \
  }
#define log(...)           \
  {                        \
    log_prefix();          \
    ilog(__VA_ARGS__);     \
    fprintf(stderr, "\n"); \
  }
#define red(...)       \
  {                    \
    log_prefix();      \
    ilog("\033[31m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define green(...)     \
  {                    \
    log_prefix();      \
    ilog("\033[32m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define blue(...)      \
  {                    \
    log_prefix();      \
    ilog("\033[34m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }

#else

#define log_prefix() ;
#define ilog(...) ;
#define llog(...) ;
#define log(...) ;
#define red(...) ;
#define green(...) ;
#define blue(...) ;

#endif

namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 3e4 + 5, SZ = 1e6 + 5;
int n, r, k;
int a[N], b[N], c[N];
long long sa;
long long XXX;

template <class T> struct Treap {
  int root, tot;
  int lc[SZ], rc[SZ], sz[SZ];
  unsigned int rnd[SZ];
  T val[SZ];
  void clear() {
    fill(lc, lc + tot + 1, 0);
    fill(rc, rc + tot + 1, 0);
    fill(sz, sz + tot + 1, 0);
    root = tot = 0;
  }
  Treap() {
    root = tot = 0;
    srand(clock() + time(0));
  }
  inline void pushup(int u) { sz[u] = sz[lc[u]] + sz[rc[u]] + 1; }
  void split_upper(int u, const T key, int &x, int &y) { // x:<=key; y:>key
    if (!u) return x = y = 0, void();
    if (val[u] <= key)
      x = u, split_upper(rc[u], key, rc[u], y);
    else
      y = u, split_upper(lc[u], key, x, lc[u]);
    pushup(u);
  }
  void split_lower(int u, const T key, int &x, int &y) { // x:<key; y:>=key
    if (!u) return x = y = 0, void();
    if (val[u] < key)
      x = u, split_lower(rc[u], key, rc[u], y);
    else
      y = u, split_lower(lc[u], key, x, lc[u]);
    pushup(u);
  }
  int merge(int x, int y) { // x<y
    if (!x || !y) return x + y;
    // if(rand()%(sz[x]+sz[y])<sz[x])return rc[x]=merge(rc[x],y), pushup(x), x;
    if (rnd[x] < rnd[y])
      return rc[x] = merge(rc[x], y), pushup(x), x;
    else
      return lc[y] = merge(x, lc[y]), pushup(y), y;
  }
  void insert(const T v) { // 插入 v
    int x, y, u = ++tot;
    val[u] = v, sz[u] = 1, rnd[u] = rand();
    split_upper(root, v, x, y);
    root = merge(merge(x, u), y);
  }
  void remove(const T v) {
    int x, y, z;
    split_lower(root, v, x, y);
    split_upper(y, v, y, z);
    if (!y) assert(0); // 删除不存在的元素
    y = merge(lc[y], rc[y]);
    root = merge(x, merge(y, z));
  }
  int rank(T v) { // 即相同的数中，第一个数的排名
    int x, y, res;
    split_lower(root, v, x, y);
    res = sz[x] + 1, root = merge(x, y);
    return res;
  }
  T kth(int k) { // 查询排名为 k 的数
    int u = root;
    while (k != sz[lc[u]] + 1) {
      if (k <= sz[lc[u]])
        u = lc[u];
      else
        k -= sz[lc[u]] + 1, u = rc[u];
    }
    return val[u];
  }
};
Treap<long long> s1, s2;
long long t[N], t2[N];
int calc(long long x) { // <= x
  // puts("");
  s1.clear();
  s2.clear();
  int res = 0;
  // x=1 [1,r]
  set<long long> s;
  FOR(i, 1, r) t[i] = c[i];
  FOR(i, r + 1, n) t[i] = b[i];
  FOR(i, 1, n) t[i] += t[i - 1];
  FOR(i, 2, min(r, n - r + 1)) {
    long long val = t[i + r - 1] - t[i - 1];
    if (r > 1) {
      s1.insert(val);
      // printf("s1 insert %lld\n",val);
    }
    t2[i] = val;
  }
  FOR(i, r + 1, n - r + 1) {
    long long val = t[i + r - 1] - t[i - 1];
    s2.insert(val);
    t2[i] = val;
  }
  long long cur = sa, tag1 = 0;
  FOR(i, 1, r) cur += b[i];
  // printf("i %d cur %lld\n",1,cur);
  if (r > 1) res += max(0, s1.rank(x - cur - tag1 + 1) - 1);
  res += max(0, s2.rank(x - cur + 1) - 1);
  FOR(i, 2, n - r) { // x=i
    // 删除 [i,i+r-1]
    if (r > 1) { s1.remove(t2[i]); }
    // s1 加入 [i+r-1,i+r-1+r-1]（s2 删除）
    if (i + r - 1 + r - 1 <= n) {
      s2.remove(t2[i + r - 1]);
      if (r > 1) {
        t2[i + r - 1] -= tag1;
        s1.insert(t2[i + r - 1]);
        // printf("s1 insert %lld\n",t2[i+r-1]);
      }
    }
    // 改变 [i+r-1] 处的点值，即 s1 的所有区间的权值增加 c[i+r-1]-b[i+r-1]
    tag1 += c[i + r - 1] - b[i + r - 1];
    // printf("tag1 %lld\n",tag1);
    // 更新 cur
    cur += b[i + r - 1] - b[i - 1];
    // printf("i %d cur %lld\n",i,cur);
    if (r > 1) res += max(0, s1.rank(x - cur - tag1 + 1) - 1);
    res += max(0, s2.rank(x - cur + 1) - 1);
  }
  // printf("calc %lld %d\n",x,res);
  return res;
}
int main() {
  scanf("%d%d%d", &n, &r, &k);
  FOR(i, 1, n) scanf("%d", &a[i]), sa += a[i];
  FOR(i, 1, n) scanf("%d", &b[i]), b[i] -= a[i];
  FOR(i, 1, n) scanf("%d", &c[i]), c[i] -= a[i] + b[i];

  long long l = 0, r = 1ll << 40;
  while (l < r) {
    long long mid = (l + r) >> 1;
    if (calc(mid) < k)
      l = mid + 1;
    else
      r = mid;
  }
  printf("%lld\n", l);
  return 0;
}
