// by Sshwy
//#define DEBUGGER
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

#ifdef DEBUGGER

#define log_prefix()                                                \
  {                                                                 \
    fprintf(stderr, "\033[37mLine %-3d [%dms]:\033[0m  ", __LINE__, \
        (int)clock() / 1000);                                       \
  }
#define ilog(...) \
  { fprintf(stderr, __VA_ARGS__); }
#define llog(...)      \
  {                    \
    log_prefix();      \
    ilog(__VA_ARGS__); \
  }
#define log(...)           \
  {                        \
    log_prefix();          \
    ilog(__VA_ARGS__);     \
    fprintf(stderr, "\n"); \
  }
#define red(...)       \
  {                    \
    log_prefix();      \
    ilog("\033[31m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define green(...)     \
  {                    \
    log_prefix();      \
    ilog("\033[32m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define blue(...)      \
  {                    \
    log_prefix();      \
    ilog("\033[34m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }

#else

#define log_prefix() ;
#define ilog(...) ;
#define llog(...) ;
#define log(...) ;
#define red(...) ;
#define green(...) ;
#define blue(...) ;

#endif

namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 3005, M = 6005;
int n, m, k;

struct qxx {
  int nex, t, v;
} e[M];
int h[N], le = 1;
void add_path(int f, int t, int v) { e[++le] = {h[f], t, v}, h[f] = le; }
long long d[N];
priority_queue<pair<long long, int>> q;
long long calc(int x) {
  fill(d, d + n + 1, 1ll << 60);
  d[1] = 0;
  q.push({-d[1], 1});
  while (!q.empty()) {
    auto u = q.top();
    q.pop();
    if (d[u.se] < -u.fi) continue;
    for (int i = h[u.se]; i; i = e[i].nex) {
      int v = e[i].t, w = e[i].v;
      w = max(w - x, 0);
      if (d[u.se] + w >= d[v]) continue;
      d[v] = d[u.se] + w;
      q.push({-d[v], v});
    }
  }
  // printf("x %d dist %lld\n",x,d[n]);
  return d[n] + k * 1ll * x;
}
int main() {
  scanf("%d%d%d", &n, &m, &k);
  vector<int> V;
  FOR(i, 1, m) {
    int u, v, w;
    scanf("%d%d%d", &u, &v, &w);
    add_path(u, v, w), add_path(v, u, w);
    V.pb(w);
  }
  long long ans = calc(0);
  for (auto x : V) ans = min(ans, calc(x));
  printf("%lld\n", ans);
  return 0;
}
