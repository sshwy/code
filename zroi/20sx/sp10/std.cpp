#include <bits/stdc++.h>
#define rep(i, a, b) for (int i = (a); i <= int(b); i++)
#define per(i, a, b) for (int i = (a); i >= int(b); i--)
#define fi first
#define se second
using namespace std;

typedef long long ll;
const int maxn = 3e4;
const ll inf = 1e12;
int n, r, k, rt, tot, val[maxn + 3], ch[maxn + 3][2], sz[maxn + 3], num[maxn + 3],
    cnt[maxn + 3];
ll a[maxn + 3], b[maxn + 3], c[maxn + 3];
ll key[maxn + 3];

int new_node(ll k = 0) {
  int x = ++tot;
  key[x] = k, val[x] = rand(), ch[x][0] = ch[x][1] = 0, sz[x] = 1, num[x] = 1,
  cnt[x] = 1;
  return x;
}

void maintain(int x) {
  sz[x] = sz[ch[x][0]] + sz[ch[x][1]] + 1;
  cnt[x] = cnt[ch[x][0]] + cnt[ch[x][1]] + num[x];
}

int merge(int x, int y) {
  if (!x || !y) return x + y;
  if (val[x] > val[y]) {
    ch[x][1] = merge(ch[x][1], y);
    maintain(x);
    return x;
  } else {
    ch[y][0] = merge(x, ch[y][0]);
    maintain(y);
    return y;
  }
}

void split(int x, ll k, int &p, int &q) {
  if (!x) {
    p = q = 0;
    return;
  }
  if (key[x] <= k) {
    p = x;
    split(ch[p][1], k, ch[p][1], q);
  } else {
    q = x;
    split(ch[q][0], k, p, ch[q][0]);
  }
  maintain(x);
}

void insert(ll x) {
  // printf("I %lld\n", x);
  int p, q;
  split(rt, x, p, q);
  int t = p;
  while (t && ch[t][1]) t = ch[t][1];
  if (t && key[t] == x) {
    num[t]++, cnt[t]++;
    int r;
    split(p, x - 1, p, r);
    rt = merge(p, merge(r, q));
  } else {
    rt = merge(p, merge(new_node(x), q));
  }
}

void erase(ll x) {
  // printf("E %lld\n", x);
  int p, q, r;
  split(rt, x, p, q);
  split(p, x - 1, p, r);
  num[r]--, cnt[r]--;
  rt = merge(p, merge(r, q));
}

int count(ll x) {
  // printf("C %lld\n", x);
  int p, q;
  split(rt, x, p, q);
  int ans = cnt[p];
  rt = merge(p, q);
  // printf("- %d\n", ans);
  return ans;
}

ll solve(ll M) {
  tot = 0, rt = 0;
  ll ans = 0;
  for (int i = r * 2; i <= n; i++) {
    insert(b[i - r] - b[i - r * 2]);
    ans += count(M - (b[i] - b[i - r]));
  }
  // puts("======");
  tot = 0, rt = 0;
  for (int i = r; i <= n; i++) {
    if (i >= r * 2) erase(c[i - r] - b[i - r] - b[i - r * 2]);
    ans += count(M - (b[i] - c[i - r] + b[i - r]));
    insert(c[i] - b[i] - b[i - r]);
  }
  return ans;
}

int main() {
  srand(time(0) ^ (unsigned long long)(new char));
  scanf("%d %d %d", &n, &r, &k);
  rep(i, 1, n) scanf("%lld", &a[i]);
  rep(i, 1, n) scanf("%lld", &b[i]), b[i] -= a[i];
  rep(i, 1, n) scanf("%lld", &c[i]), c[i] -= a[i];
  rep(i, 1, n) a[i] += a[i - 1];
  rep(i, 1, n) b[i] += b[i - 1];
  rep(i, 1, n) c[i] += c[i - 1];
  ll L = -inf, R = inf;
  while (L < R) {
    ll M = (L + R) >> 1;
    if (solve(M) >= k)
      R = M;
    else
      L = M + 1;
  }
  printf("%lld\n", L + a[n]);
  return 0;
}
