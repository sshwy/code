// by Sshwy
//#define DEBUGGER
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

#ifdef DEBUGGER

#define log_prefix()                                                \
  {                                                                 \
    fprintf(stderr, "\033[37mLine %-3d [%dms]:\033[0m  ", __LINE__, \
        (int)clock() / 1000);                                       \
  }
#define ilog(...) \
  { fprintf(stderr, __VA_ARGS__); }
#define llog(...)      \
  {                    \
    log_prefix();      \
    ilog(__VA_ARGS__); \
  }
#define log(...)           \
  {                        \
    log_prefix();          \
    ilog(__VA_ARGS__);     \
    fprintf(stderr, "\n"); \
  }
#define red(...)       \
  {                    \
    log_prefix();      \
    ilog("\033[31m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define green(...)     \
  {                    \
    log_prefix();      \
    ilog("\033[32m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define blue(...)      \
  {                    \
    log_prefix();      \
    ilog("\033[34m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }

#else

#define log_prefix() ;
#define ilog(...) ;
#define llog(...) ;
#define log(...) ;
#define red(...) ;
#define green(...) ;
#define blue(...) ;

#endif

namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
typedef unsigned long long ull;
const int N = 100, SZ = 1 << 23;
int n;
ull b[N], a[N], s;
pair<ull, int> a1[SZ], a2[SZ];
vector<ull> v1, v2;

void solve1() {
  FOR(i, 1, n / 2) v1.pb(b[i]);
  FOR(i, n / 2 + 1, n) v2.pb(b[i]);
  int l1 = 1 << v1.size();
  int l2 = 1 << v2.size();
  FOR(i, 1, l1 - 1) { a1[i] = {a1[i ^ (i & -i)].fi + v1[__builtin_ctz(i & -i)], i}; }
  FOR(i, 1, l2 - 1) { a2[i] = {a2[i ^ (i & -i)].fi + v2[__builtin_ctz(i & -i)], i}; }
  sort(a1, a1 + l1);
  sort(a2, a2 + l2);
  int pos = 0; // a2

  __int128 Q = 1;
  Q <<= 64;

  FOR(i, 0, l1 - 1) {
    __int128 val = a1[i].fi;
    while (val + a2[pos].fi < s && pos + 1 < l2) ++pos;
    while (val + a2[pos].fi > s && pos - 1 >= 0) --pos;
    if (val + a2[pos].fi == s) {
      FOR(x, 0, int(v1.size()) - 1) printf("%d", a1[i].se >> x & 1);
      FOR(x, 0, int(v2.size()) - 1) printf("%d", a2[pos].se >> x & 1);
      puts("");
      return;
    }
  }

  pos = 0;
  FOR(i, 0, l1 - 1) {
    __int128 val = a1[i].fi;
    while (val + a2[pos].fi < s + Q && pos + 1 < l2) ++pos;
    while (val + a2[pos].fi > s + Q && pos - 1 >= 0) --pos;
    if (val + a2[pos].fi == s + Q) {
      FOR(x, 0, int(v1.size()) - 1) printf("%d", a1[i].se >> x & 1);
      FOR(x, 0, int(v2.size()) - 1) printf("%d", a2[pos].se >> x & 1);
      puts("");
      return;
    }
  }
  assert(0);
}
template <class T> T exgcd(T a, T b, T &x, T &y) {
  if (!b) return x = 1, y = 0, a;
  T t = exgcd(b, a % b, y, x);
  return y = y - (a / b) * x, t;
}
void solve2() {
  int lim = 1 << (65 - n); // 0<a[1]<lim

  __int128 Q = 1;
  Q <<= 64;

  FOR(a1, 1, lim - 1) {
    // b1 = a1*r+k*q => lowbit(b1)==lowbit(a1)
    // gcd(a1,q)==gcd(a1,2^64)==(a1&-a1)
    if (__builtin_ctz(a1) != __builtin_ctzll(b[1])) continue;
    // printf("a1 %d\n",a1);
    int x = __builtin_ctz(a1);
    // printf("x %d\n",x);
    __int128 B = b[1] >> x, A = a1 >> x, QQ = Q >> x;
    __int128 r0, k;
    exgcd(A, QQ, r0, k);

    r0 *= B, k *= B;
    FOR(t, 0, (1 << x) - 1) {
      ull r = (r0 + t * QQ) % Q;
      // printf("r %llu\n",r);
      if (r < 0) r += Q;
      assert(__builtin_ctzll(r) == 0);
      auto inv = [&Q](__int128 a) { // inv mod Q
        __int128 x, y;
        exgcd(a, Q, x, y);
        x %= Q;
        if (x < 0) x += Q;
        return x;
      };
      ull Ir = inv(r);
      // printf("Ir %llu\n",Ir);
      FOR(i, 1, n) a[i] = b[i] * Ir;
      __int128 pre = 0;
      bool flag = 1;
      FOR(i, 1, n) {
        if (a[i] <= pre) {
          flag = 0;
          break;
        }
        pre += a[i];
      }
      if (!flag) continue;
      // FOR(i,1,n)cout<<a[i]<<" ";
      // cout<<endl;
      ull ss = s * Ir;

      __int128 S = ss;
      ull ans = 0;
      ROF(i, n, 1) if (S >= a[i]) S -= a[i], ans ^= 1ll << (i - 1);
      if (S == 0) {
        FOR(i, 1, n) printf("%llu", ans >> (i - 1) & 1);
        puts("");
        return;
      }

      S = ss + Q;
      ans = 0;
      ROF(i, n, 1) if (S > a[i]) S -= a[i], ans ^= 1ll << (i - 1);
      if (S == 0) {
        FOR(i, 1, n) printf("%llu", ans >> (i - 1) & 1);
        puts("");
        return;
      }
    }
  }
  assert(0);
}
int main() {
  scanf("%d", &n);
  FOR(i, 1, n) scanf("%llu", &b[i]);
  scanf("%llu", &s);
  if (n < 44)
    solve1();
  else
    solve2();
  return 0;
}
