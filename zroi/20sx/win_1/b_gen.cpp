// by Sshwy
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
using namespace RA;
const int N = 1e6 + 5;

bool bp[N];
int pn[N], lp;
void sieve(int lim) {
  bp[0] = bp[1] = 1;
  FOR(i, 2, lim) {
    if (!bp[i]) pn[++lp] = i;
    FOR(j, 1, lp) {
      if (i * pn[j] > lim) break;
      bp[i * pn[j]] = 1;
      if (i % pn[j] == 0) break;
    }
  }
}

int main() {
  srand(time(0) + clock());
  sieve(100000);
  assert(lp > 500);
  int T = 500;
  printf("%d\n", T);
  int start = r(0, lp - T);
  FOR(i, 1, T) printf("%d\n", pn[i + start]);
  printf("lp=%d\n", lp);
  return 0;
}
