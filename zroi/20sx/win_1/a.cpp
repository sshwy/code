// by Sshwy
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 3005;

int n;
int a[N];
int b[6];

int main() {
  scanf("%d", &n);
  FOR(i, 1, n) scanf("%d", &a[i]);
  FOR(i, 1, 5) scanf("%d", &b[i]);
  int ans = 0;
  // int xxx=0;
  FOR(k1, 1, n) FOR(k2, k1 + 1, n) {
    if ((b[1] == b[2]) == (a[k1] == a[k2])) FOR(k3, k2 + 1, n) {
        if ((b[1] == b[3]) == (a[k1] == a[k3]) && (b[2] == b[3]) == (a[k2] == a[k3]))
          FOR(k4, k3 + 1, n) {
            if ((b[1] == b[4]) == (a[k1] == a[k4]) &&
                (b[2] == b[4]) == (a[k2] == a[k4]) &&
                (b[3] == b[4]) == (a[k3] == a[k4]))
              FOR(k5, k4 + 1, n) {
                if ((b[1] == b[5]) == (a[k1] == a[k5]) &&
                    (b[2] == b[5]) == (a[k2] == a[k5]) &&
                    (b[3] == b[5]) == (a[k3] == a[k5]) &&
                    (b[4] == b[5]) == (a[k4] == a[k5]))
                  ans++;
                //++xxx;
              }
          }
      }
  }
  printf("%d\n", ans);
  // printf("xxx=%d\n",xxx);
  return 0;
}
