// by Sshwy
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 1e5 + 5;

int inv[N], ans[N];
void go() {
  int p;
  scanf("%d", &p);
  inv[1] = 1;
  int mn = p, la = 0;
  FOR(i, 2, p - 1) {
    inv[i] = 1ll * (p - p / i) * inv[p % i] % p, inv[i] < mn ? ans[++la] = i,
    mn = inv[i] : 0;
    if (mn == 2) break;
  }
  printf("%d\n", la);
  FOR(i, 1, la) printf("%d %d\n", ans[i], inv[ans[i]]);
}
int main() {
  int t;
  scanf("%d", &t);
  FOR(i, 1, t) go();

  return 0;
}
