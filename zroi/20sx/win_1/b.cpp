// by Sshwy
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 5e7 + 5;

long long inv[N], ans[N];

inline long long mul(long long a, long long b, long long p) {
  if (p <= 1000000000ll) return 1ll * a * b % p;
  long long d = floor(a * (long double)b / p);
  long long res = (a * b - d * p) % p;
  if (res < 0) res += p;
  return res;
}

void go() {
  long long p;
  scanf("%lld", &p);
  inv[1] = 1;
  long long mn = p, la = 0;
  FOR(i, 2, p - 1) {
    inv[i] = mul((p - p / i), inv[p % i], p);
    if (inv[i] < mn) ans[++la] = i, mn = inv[i];
    if (mn <= i) break;
  }
  if (mn < ans[la]) {
    --la;
    printf("%lld\n", la * 2);
    FOR(i, 1, la) printf("%lld %lld\n", ans[i], inv[ans[i]]);
    ROF(i, la, 1) printf("%lld %lld\n", inv[ans[i]], ans[i]);
  } else if (mn == ans[la]) {
    printf("%lld\n", la * 2 - 1);
    FOR(i, 1, la) printf("%lld %lld\n", ans[i], inv[ans[i]]);
    ROF(i, la - 1, 1) printf("%lld %lld\n", inv[ans[i]], ans[i]);
  } else {
    printf("%lld\n", la);
    FOR(i, 1, la) printf("%lld %lld\n", ans[i], inv[ans[i]]);
  }
}
int main() {
  int t;
  scanf("%d", &t);
  FOR(i, 1, t) go();

  return 0;
}
