// by Sshwy
//#define DEBUGGER
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

#ifdef DEBUGGER

#define log_prefix()                                                \
  {                                                                 \
    fprintf(stderr, "\033[37mLine %-3d [%dms]:\033[0m  ", __LINE__, \
        (int)clock() / 1000);                                       \
  }
#define ilog(...) \
  { fprintf(stderr, __VA_ARGS__); }
#define llog(...)      \
  {                    \
    log_prefix();      \
    ilog(__VA_ARGS__); \
  }
#define log(...)           \
  {                        \
    log_prefix();          \
    ilog(__VA_ARGS__);     \
    fprintf(stderr, "\n"); \
  }
#define red(...)       \
  {                    \
    log_prefix();      \
    ilog("\033[31m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define green(...)     \
  {                    \
    log_prefix();      \
    ilog("\033[32m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define blue(...)      \
  {                    \
    log_prefix();      \
    ilog("\033[34m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }

#else

#define log_prefix() ;
#define ilog(...) ;
#define llog(...) ;
#define log(...) ;
#define red(...) ;
#define green(...) ;
#define blue(...) ;

#endif

namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
namespace IO {
  const int BSZ = 100000;
  char nc() {
    static char bf[BSZ], *p1 = bf, *p2 = bf;
    return p1 == p2 && (p2 = (p1 = bf) + fread(bf, 1, BSZ, stdin), p1 == p2) ? EOF
                                                                             : *p1++;
  }
  int rd() {
    int res = 0;
    char c = nc();
    while (!isdigit(c)) c = nc();
    while (isdigit(c)) res = res * 10 + c - '0', c = nc();
    return res;
  }
  long long rdll() {
    long long res = 0;
    char c = nc();
    while (!isdigit(c)) c = nc();
    while (isdigit(c)) res = res * 10 + c - '0', c = nc();
    return res;
  }
  void rd(char *s) {
    char c = nc();
    while (!isblank(c) && isprint(c) && c != EOF) *s++ = c, c = nc();
    *s = 0;
  }
  int tmp[20], lt;
  char OBF[BSZ], *OBP = OBF;
  void flush() { fwrite(OBF, 1, OBP - OBF, stdout), OBP = OBF; }
  void wrch(char x) {
    if (OBP == OBF + BSZ - 1) flush();
    *OBP = x, ++OBP;
  }
  void wr(int x, int base = 10) {
    if (x == 0) return wrch('0'), void();
    if (x < 0) wrch('-'), x = -x;
    lt = 0;
    while (x) tmp[++lt] = x % base, x /= base;
    while (lt > 0) wrch(tmp[lt] + '0'), --lt;
  }
  void wr(long long x, int base = 10) {
    if (x == 0) return wrch('0'), void();
    if (x < 0) wrch('-'), x = -x;
    lt = 0;
    while (x) tmp[++lt] = x % base, x /= base;
    while (lt > 0) wrch(tmp[lt] + '0'), --lt;
  }
  void wr(const char *s) {
    while (*s) wrch(*s), ++s;
  }
  void wr(char x) { wrch(x); }
} // namespace IO
const int N = 1e6 + 5;
typedef long long LL;
const LL INF = 1e18;
LL a[N], n, k, k1, k2;
__int128 f[N], g[N], Ws, Wb;

multiset<__int128> s1;
__int128 sum1;

using IO::rdll;
int main() {
  n = rdll(), k = rdll();
  FOR(i, 1, n) a[i] = rdll();
  Ws = rdll(), Wb = rdll();

  LL k1 = k / 2, k2 = k - k1;

  FOR(i, 1, n) {
    LL x = a[i] + Ws * i;
    s1.insert(x);
    sum1 += x;
    if (s1.size() > k1) {
      sum1 -= *s1.begin();
      s1.erase(s1.begin());
    }
    if (s1.size() == k1) { f[i] = sum1 - Ws * i * k1; }
  }

  sum1 = 0;
  s1.clear();
  reverse(a + 1, a + n + 1);
  FOR(i, 1, n) {
    __int128 x = a[i] + Ws * i;
    s1.insert(x);
    sum1 += x;
    if (s1.size() > k2) {
      sum1 -= *s1.begin();
      s1.erase(s1.begin());
    }
    if (s1.size() == k2) { g[i] = sum1 - Ws * i * k2; }
  }
  reverse(g + 1, g + n + 1);

  __int128 ans = 0, tot = 0, d = 0;
  FOR(i, 1, n) tot += a[i];
  ans = tot;
  if (k1) d = k1 * (k1 - 1ll) / 2;
  if (k2) d += k2 * (k2 - 1ll) / 2;
  d *= Ws;
  FOR(i, k1, n - k2) ans = min(ans, tot - f[i] - g[i + 1] - d + Wb);

  LL Ans = ans;

  printf("%lld\n", Ans);

  return 0;
}
