// by Sshwy
#define DEBUGGER
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

#ifdef DEBUGGER

#define log_prefix()                                                \
  {                                                                 \
    fprintf(stderr, "\033[37mLine %-3d [%dms]:\033[0m  ", __LINE__, \
        (int)clock() / 1000);                                       \
  }
#define ilog(...) \
  { fprintf(stderr, __VA_ARGS__); }
#define llog(...)      \
  {                    \
    log_prefix();      \
    ilog(__VA_ARGS__); \
  }
#define log(...)           \
  {                        \
    log_prefix();          \
    ilog(__VA_ARGS__);     \
    fprintf(stderr, "\n"); \
  }
#define red(...)       \
  {                    \
    log_prefix();      \
    ilog("\033[31m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define green(...)     \
  {                    \
    log_prefix();      \
    ilog("\033[32m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define blue(...)      \
  {                    \
    log_prefix();      \
    ilog("\033[34m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }

#else

#define log_prefix() ;
#define ilog(...) ;
#define llog(...) ;
#define log(...) ;
#define red(...) ;
#define green(...) ;
#define blue(...) ;

#endif

namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 1e5 + 5;

int n, k, q, T;
int a[N];
int b[N];
int has[N];
int las;
int mx(int l, int r) {
  int res = 0;
  FOR(i, l, r) res = max(res, a[i]);
  return res;
}
int mn(int l, int r) {
  int res = 1e9 + 1;
  FOR(i, l, r) res = min(res, a[i]);
  return res;
}

typedef pair<int, int> pii;
int calc(vector<pii> vp, char c) {
  if (c == 'D') {
    if (vp.size() & 1) {
      int L = 0, R = 0, p = vp.size() / 2, len = vp.size() - 1;
      return vp[p].fi;
      FOR(i, 0, p) L = max(L, vp[i].fi);
      FOR(i, p, len) R = max(R, vp[i].fi);
      return min(L, R);
    } else {
      auto p = vp.back();
      vp.pop_back();
      int res = calc(vp, 'Y');
      vp.push_back(p);
      vp.erase(vp.begin());
      res = max(res, calc(vp, 'Y'));
      return res;
    }
  } else {
    if (vp.size() & 1) {
      int L = 1e9, R = 1e9, p = vp.size() / 2, len = vp.size() - 1;
      return vp[p].se;
      FOR(i, 0, p) L = min(L, vp[i].se);
      FOR(i, p, len) R = min(R, vp[i].se);
      return max(L, R);
    } else {
      auto p = vp.back();
      vp.pop_back();
      int res = calc(vp, 'D');
      vp.push_back(p);
      vp.erase(vp.begin());
      res = min(res, calc(vp, 'D'));
      return res;
    }
  }
}
int go() {
  vector<int> v;
  vector<pii> vp;
  FOR(i, 1, n) if (has[i]) v.pb(i);
  llog("v: ");
  for (int x : v) ilog("%d ", x);
  ilog("\n");
  llog("a: ");
  FOR(i, 1, n) ilog("%d ", a[i]);
  ilog("\n");
  for (long unsigned i = 1; i < v.size(); i++)
    vp.pb({mx(v[i - 1], v[i]), mn(v[i - 1], v[i])});
  for (auto p : vp) ilog("(%d,%d) ", p.fi, p.se);
  ilog("\n");
  if (v.size() == 1) return las = a[v[0]];
  return las = calc(vp, 'D');
}
int main() {
  scanf("%d%d%d%d", &n, &k, &q, &T);
  FOR(i, 1, n) { scanf("%d", &a[i]); }
  FOR(i, 1, k) {
    scanf("%d", &b[i]);
    has[b[i]] = 1;
  }
  printf("%d\n", go());
  FOR(i, 1, q) {
    int op, x, v;
    scanf("%d%d", &op, &x);
    if (T == 1) x = (x - 1 + las) % n + 1;
    if (op == 1)
      has[x] ^= 1;
    else {
      scanf("%d", &v);
      a[x] = v;
    }
    printf("%d\n", go());
  }
  // log("xxx %d",xxx);
  return 0;
}
