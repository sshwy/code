// by Sshwy
#define DEBUGGER
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

#ifdef DEBUGGER

#define log_prefix()                                                \
  {                                                                 \
    fprintf(stderr, "\033[37mLine %-3d [%dms]:\033[0m  ", __LINE__, \
        (int)clock() / 1000);                                       \
  }
#define ilog(...) \
  { fprintf(stderr, __VA_ARGS__); }
#define llog(...)      \
  {                    \
    log_prefix();      \
    ilog(__VA_ARGS__); \
  }
#define log(...)           \
  {                        \
    log_prefix();          \
    ilog(__VA_ARGS__);     \
    fprintf(stderr, "\n"); \
  }
#define red(...)       \
  {                    \
    log_prefix();      \
    ilog("\033[31m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define green(...)     \
  {                    \
    log_prefix();      \
    ilog("\033[32m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define blue(...)      \
  {                    \
    log_prefix();      \
    ilog("\033[34m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }

#else

#define log_prefix() ;
#define ilog(...) ;
#define llog(...) ;
#define log(...) ;
#define red(...) ;
#define green(...) ;
#define blue(...) ;

#endif

namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 1e5 + 5;

int n, k, q, T;
int a[N];
int b[N];
int has[N];
int las;
int mx(int l, int r) {
  int res = 0;
  FOR(i, l, r) res = max(res, a[i]);
  return res;
}
int mn(int l, int r) {
  int res = 1e9 + 1;
  FOR(i, l, r) res = min(res, a[i]);
  return res;
}

typedef pair<int, int> pii;
// int xxx;
int dfs(vector<pii> vp, char c) {
  // log("vpsize %d",vp.size());
  // for(auto & x:vp)ilog("(%d,%d) ",x.fi,x.se);
  // ilog("\n");
  // xxx+=vp.size();
  if (c == 'D') {
    if (vp.size() == 1) return vp[0].fi;
    if (vp.size() & 1) {
      int p = vp.size() / 2;
      int res = vp[p].fi;
      vp.erase(vp.begin() + p);
      res = max(res, dfs(vp, 'Y'));
      return res;
    } else {
      auto x = vp.back();
      vp.pop_back();
      int res = dfs(vp, 'Y');
      vp.push_back(x);
      vp.erase(vp.begin());
      res = max(res, dfs(vp, 'Y'));
      return res;
    }
  } else {
    if (vp.size() == 1) return vp[0].se;
    if (vp.size() & 1) {
      int p = vp.size() / 2;
      int res = vp[p].se;
      vp.erase(vp.begin() + p);
      res = min(res, dfs(vp, 'D'));
      return res;
    } else {
      auto x = vp.back();
      vp.pop_back();
      int res = dfs(vp, 'D');
      vp.push_back(x);
      vp.erase(vp.begin());
      res = min(res, dfs(vp, 'D'));
      return res;
    }
  }
}
int go() {
  vector<int> v;
  vector<pii> vp;
  FOR(i, 1, n) if (has[i]) v.pb(i);
  for (long unsigned i = 1; i < v.size(); i++)
    vp.pb({mx(v[i - 1], v[i]), mn(v[i - 1], v[i])});
  if (v.size() == 1) return las = a[v[0]];
  return las = dfs(vp, 'D');
}
int main() {
  scanf("%d%d%d%d", &n, &k, &q, &T);
  FOR(i, 1, n) { scanf("%d", &a[i]); }
  FOR(i, 1, k) {
    scanf("%d", &b[i]);
    has[b[i]] = 1;
  }
  printf("%d\n", go());
  FOR(i, 1, q) {
    int op, x, v;
    scanf("%d%d", &op, &x);
    if (T == 1) x = (x - 1 + las) % n + 1;
    if (op == 1)
      has[x] ^= 1;
    else {
      scanf("%d", &v);
      a[x] = v;
    }
    printf("%d\n", go());
  }
  // log("xxx %d",xxx);
  return 0;
}
