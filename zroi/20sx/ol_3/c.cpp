// by Sshwy
//#define DEBUGGER
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

#ifdef DEBUGGER

#define log_prefix()                                                \
  {                                                                 \
    fprintf(stderr, "\033[37mLine %-3d [%dms]:\033[0m  ", __LINE__, \
        (int)clock() / 1000);                                       \
  }
#define ilog(...) \
  { fprintf(stderr, __VA_ARGS__); }
#define llog(...)      \
  {                    \
    log_prefix();      \
    ilog(__VA_ARGS__); \
  }
#define log(...)           \
  {                        \
    log_prefix();          \
    ilog(__VA_ARGS__);     \
    fprintf(stderr, "\n"); \
  }
#define red(...)       \
  {                    \
    log_prefix();      \
    ilog("\033[31m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define green(...)     \
  {                    \
    log_prefix();      \
    ilog("\033[32m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define blue(...)      \
  {                    \
    log_prefix();      \
    ilog("\033[34m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }

#else

#define log_prefix() ;
#define ilog(...) ;
#define llog(...) ;
#define log(...) ;
#define red(...) ;
#define green(...) ;
#define blue(...) ;

#endif

namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 16, W = 1 << 15;
typedef long long LL;
LL n, nn, a[N], f[N][W], ans[N];
LL gcd(LL a, LL b) { return b ? gcd(b, a % b) : a; }
int main() {
  scanf("%lld", &n);
  nn = 1 << n;
  FOR(i, 0, n - 1) scanf("%lld", &a[i]);
  FOR(i, 0, n - 1) f[1][1 << i] = a[i];
  FOR(j, 0, nn - 1) f[1][j] = gcd(f[1][j ^ (j & -j)], f[1][j & -j]);

  FOR(i, 2, n) {
    FOR(j, 0, nn - 1) {
      for (int k = j; k; k = (k - 1) & j) {
        if (j ^ k) { f[i][j] = max(f[i][j], f[i - 1][k] + f[1][j ^ k]); }
      }
    }
  }

  FOR(i, 1, n) printf("%lld\n", f[i][nn - 1]);
  return 0;
}
