// by Sshwy
//#define DEBUGGER
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

#ifdef DEBUGGER

#define log_prefix()                                                \
  {                                                                 \
    fprintf(stderr, "\033[37mLine %-3d [%dms]:\033[0m  ", __LINE__, \
        (int)clock() / 1000);                                       \
  }
#define ilog(...) \
  { fprintf(stderr, __VA_ARGS__); }
#define llog(...)      \
  {                    \
    log_prefix();      \
    ilog(__VA_ARGS__); \
  }
#define log(...)           \
  {                        \
    log_prefix();          \
    ilog(__VA_ARGS__);     \
    fprintf(stderr, "\n"); \
  }
#define red(...)       \
  {                    \
    log_prefix();      \
    ilog("\033[31m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define green(...)     \
  {                    \
    log_prefix();      \
    ilog("\033[32m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define blue(...)      \
  {                    \
    log_prefix();      \
    ilog("\033[34m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }

#else

#define log_prefix() ;
#define ilog(...) ;
#define llog(...) ;
#define log(...) ;
#define red(...) ;
#define green(...) ;
#define blue(...) ;

#endif

namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int SZ = 1 << 19, P = 998244353;
int pw(int a, int m) {
  int res = 1;
  while (m) m & 1 ? res = res * 1ll * a % P : 0, a = a * 1ll * a % P, m >>= 1;
  return res;
}

void fwt(int *f, int len, int tag) {
  for (int j = 1; j < len; j <<= 1) {
    for (int i = 0; i < len; i += j << 1) {
      for (int k = i, u, v; k < i + j; k++) {
        u = f[k], v = f[k + j], f[k] = (u + v) % P, f[k + j] = (u - v + P) % P;
      }
    }
  }
  if (tag == -1) {
    int iv = pw(len, P - 2);
    for (int i = 0; i < len; i++) f[i] = f[i] * 1ll * iv % P;
  }
}

int n, a[SZ], b[SZ], s;

int main() {
  scanf("%d", &n);
  FOR(i, 1, n) {
    int x;
    scanf("%d", &x);
    a[x]++;
    s ^= x;
  }

  fwt(a, SZ, 1);
  b[s] = 1;
  fwt(b, SZ, 1);
  int inv_SZ = pw(SZ, P - 2);

  FOR(i, 0, 20) {
    int b0 = 0;
    FOR(j, 0, SZ - 1) b0 = (b0 + b[j]) % P;
    b0 = b0 * 1ll * inv_SZ % P;
    if (b0) {
      printf("%d\n", n - i);
      return 0;
    }
    FOR(j, 0, SZ - 1) b[j] = 1ll * b[j] * a[j] % P;
  }
  puts("0");
  return 0;
}

// 最大xor序列
// 转化为最小xor序列
// 对于最小的问题，显然数字个数不超过log个。因为线性基的大小是log的。
// 记f[i,j]表示i个数xor和为j的方案数。
// 显然就是i次fwt的值。
