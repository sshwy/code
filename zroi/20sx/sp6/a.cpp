// by Sshwy
//#define DEBUGGER
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

#ifdef DEBUGGER

#define log_prefix()                                                \
  {                                                                 \
    fprintf(stderr, "\033[37mLine %-3d [%dms]:\033[0m  ", __LINE__, \
        (int)clock() / 1000);                                       \
  }
#define ilog(...) \
  { fprintf(stderr, __VA_ARGS__); }
#define llog(...)      \
  {                    \
    log_prefix();      \
    ilog(__VA_ARGS__); \
  }
#define log(...)           \
  {                        \
    log_prefix();          \
    ilog(__VA_ARGS__);     \
    fprintf(stderr, "\n"); \
  }
#define red(...)       \
  {                    \
    log_prefix();      \
    ilog("\033[31m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define green(...)     \
  {                    \
    log_prefix();      \
    ilog("\033[32m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define blue(...)      \
  {                    \
    log_prefix();      \
    ilog("\033[34m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }

#else

#define log_prefix() ;
#define ilog(...) ;
#define llog(...) ;
#define log(...) ;
#define red(...) ;
#define green(...) ;
#define blue(...) ;

#endif

namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 3e5 + 5, SZ = N * 30, W = 1 << 30;
int n, k;
int a[N], p[N], c[N];

map<int, set<int>> mp;

namespace get_min_xor {
  int root;
  int lc[SZ], rc[SZ], tot;
  void insert(int x, int &u = root, int l = 0, int r = W - 1) {
    if (!u) u = ++tot;
    if (l == r) return;
    int mid = (l + r) >> 1;
    if (x <= mid)
      insert(x, lc[u], l, mid);
    else
      insert(x, rc[u], mid + 1, r);
  }
  int query(int x, int u = root, int l = 0, int r = W - 1,
      int k = 29) { // xor x 最小的数
    if (l == r) return x ^ l;
    int mid = (l + r) >> 1;
    if (!rc[u]) return query(x, lc[u], l, mid, k - 1);
    if (!lc[u]) return query(x, rc[u], mid + 1, r, k - 1);
    if (x >> k & 1)
      return query(x, rc[u], mid + 1, r, k - 1);
    else
      return query(x, lc[u], l, mid, k - 1);
  }
} // namespace get_min_xor

int root_c0, root_c1;
int root_e0, root_e1;
namespace counter {
  int lc[SZ], rc[SZ], tot;
  long long cnt[SZ];
  void add(int x, int v, int &u, int l = 0, int r = W - 1) {
    if (!u) u = ++tot;
    if (l == r) return cnt[u] += v, void();
    int mid = (l + r) >> 1;
    if (x <= mid)
      add(x, v, lc[u], l, mid);
    else
      add(x, v, rc[u], mid + 1, r);
    cnt[u] = lc[u] ? cnt[lc[u]] : 0 + rc[u] ? cnt[rc[u]] : 0;
  }
  long long count_le_xor(
      int x, int lim, int u, int l = 0, int r = W - 1, int k = 29) {
    if (!u) return 0;
    if (l == r) return cnt[u];
    int mid = (l + r) >> 1;
    if (lim >> k & 1) {
      if (x >> k & 1)
        return count_le_xor(x, lim, lc[u], l, mid, k - 1) + rc[u] ? cnt[rc[u]] : 0;
      else
        return count_le_xor(x, lim, rc[u], mid + 1, r, k - 1) + lc[u] ? cnt[lc[u]]
                                                                      : 0;
    } else {
      if (x >> k & 1)
        return count_le_xor(x, lim, rc[u], mid + 1, r, k - 1);
      else
        return count_le_xor(x, lim, lc[u], l, mid, k - 1);
    }
  }
} // namespace counter

int root_n0, root_n1;
namespace node {
  int lc[SZ], rc[SZ], tot;
  pair<int, int> d[SZ], zero(0, 0);
  pair<int, int> merge(pair<int, int> a, pair<int, int> b) {
    int t[] = {a.fi, a.se, b.fi, b.se};
    sort(t, t + 4, [](int x, int y) {
      if (x == y) return false;
      if (x == 0) return false;
      if (y == 0) return true;
      return x < y;
    });
    return {t[0], t[1]};
  }
  void pushup(int u) {
    if (!rc[u])
      d[u] = d[lc[u]];
    else if (!lc[u])
      d[u] = d[rc[u]];
    else {
      int t[] = {d[lc[u]].fi, d[lc[u]].se, d[rc[u]].fi, d[rc[u]].se};
      sort(t, t + 4, [](int x, int y) {
        if (x == y) return false;
        if (x == 0) return false;
        if (y == 0) return true;
        return x < y;
      });
      d[u] = {t[0], t[1]};
    }
  }
  void insert(int x, int id, int &u, int l = 0, int r = W - 1) {
    assert(a[id] == x);
    if (!u) u = ++tot;
    if (l == r) {
      mp[x].insert(id);
      if (d[u].fi == 0)
        d[u].fi = id;
      else if (d[u].fi < id)
        d[u].se = id;
      else
        d[u].se = d[u].fi, d[u].fi = id;
      return;
    }
    int mid = (l + r) >> 1;
    if (x <= mid)
      insert(x, id, lc[u], l, mid);
    else
      insert(x, id, rc[u], mid + 1, r);
    pushup(u);
  }
  pair<int, int> node_le_xor(
      int x, int lim, int u, int l = 0, int r = W - 1, int k = 29) {
    if (!u) return zero;
    if (l == r) return d[u];
    int mid = (l + r) >> 1;
    if (lim >> k & 1) {
      if (x >> k & 1)
        return merge(
            node_le_xor(x, lim, lc[u], l, mid, k - 1), rc[u] ? d[rc[u]] : zero);
      else
        return merge(
            node_le_xor(x, lim, rc[u], mid + 1, r, k - 1), lc[u] ? d[lc[u]] : zero);
    } else {
      if (x >> k & 1)
        return node_le_xor(x, lim, rc[u], mid + 1, r, k - 1);
      else
        return node_le_xor(x, lim, lc[u], l, mid, k - 1);
    }
  }
} // namespace node

int main() {
  scanf("%d", &n);
  FOR(i, 1, n) scanf("%d", &a[i]);

  for (k = 30; k >= 0; --k) {
    int flag = 0;
    FOR(i, 1, n - 1) if ((a[i] >> k & 1) != (a[i + 1] >> k & 1)) flag = 1;
    if (flag) break;
  }

  printf("k %d\n", k);

  set<int> s0, s1;
  FOR(i, 1, n) {
    if (a[i] >> k & 1)
      s1.insert(i), c[i] = 1;
    else
      s0.insert(i), c[i] = 0;
  }
  for (int x : s0) printf("%d ", a[x]);
  puts("");
  for (int x : s1) printf("%d ", a[x]);
  puts("");

  int min_val = W - 1;
  for (int x : s0) get_min_xor::insert(a[x]);
  for (int x : s1) min_val = min(min_val, get_min_xor::query(a[x]));

  printf("min_val %d\n", min_val);
  for (int x : s0)
    for (int y : s1) assert(min_val <= (a[x] ^ a[y]));

  for (int x : s0) counter::add(a[x], 1, root_c0);
  for (int x : s1) counter::add(a[x], 1, root_c1);

  for (int x : s0) {
    int v = counter::count_le_xor(a[x], min_val, root_c1);
    counter::add(a[x], v, root_e0);
  }
  for (int x : s1) {
    int v = counter::count_le_xor(a[x], min_val, root_c0);
    counter::add(a[x], v, root_e1);
  }

  for (int x : s0) { node::insert(a[x], x, root_n0); }
  for (int x : s1) { node::insert(a[x], x, root_n1); }

  int las = -1;
  vector<int> ans;
  FOR(_, 1, n) {

    auto get_sameside = [&]() {
      assert(las != -1);
      if (las == 0) {
        if (!s0.size()) return -1;
      } else {
      }
    };
    auto get_diffside =
        [&]() {
          assert(las != -1);
          int las_val = a[ans.back()];
        }

    int x0 = ans.size()  ? get_sameside()
             : s0.size() ? *s0.begin()
                         : -1;
    int x1 = ans.size() ? get_diffside() : s1.size() ? *s1.begin() : -1;
    printf("x0 %d x1 %d\n", x0, x1);

    assert(x0 != x1);

    if (las == 1) swap(x0, x1);

    if (x0 == -1) {
      if (ans.size()) assert((a[ans.back()] ^ a[x1]) <= min_val);
      ans.pb(x1);
      erase1(s1.begin());
      las = c[x1];
    } else if (x1 == -1) {
      if (ans.size()) assert((a[ans.back()] ^ a[x0]) <= min_val);
      ans.pb(x0);
      erase0(s0.begin());
      las = c[x0];
    } else {
      if (x0 < x1) {
        if (ans.size()) assert((a[ans.back()] ^ a[x0]) <= min_val);
        ans.pb(x0);
        erase0(s0.begin());
        las = c[x0];
      } else {
        if (ans.size()) assert((a[ans.back()] ^ a[x1]) <= min_val);
        ans.pb(x1);
        erase1(s1.begin());
        las = c[x1];
      }
    }
  }

  for (int x : ans) printf("%d ", x);
  puts("");

  return 0;
}

// 把数字分成两个集合S0,S1
// 集合内的数可以随意排列。
// 而属于不同集合的数就不能超过最小值。
// 我们把不超过最小值的连边。
// 二分图。
// 优化建图（其实不用建图
// 每次选一个数。
// 1. 如果选的是与las同一个集合的点u，就判断
//    or 对面的size为0 (get)
//    or 这边size为1（只有u）且有边  (get)
//    or 这个集合存在一个不是u的点使得它有边 (get)
//    否则，你去了就回不来了
// 2. 如果选的是与las不在同一集合的点u，则判断
//    and las到u有边
//    and
//        or 这边size为0  (get)
//        or 对面size为1且有边  (get)
//        or 对面存在不是u的点有边  (get)
//
// 选完了就要删。
// 记e0[l,r]表示S0的a[u] in [l,r]的出边个数和（multi），e1[l,r]同理。
// 删除一个数，则一边做区间减，一边做单点减。
// 而2.1的处理，再记一个nd0[l,r],nd1[l,r]表示这个区间里的结点集合。但实际上只用记最大和次大即可（1.3和2.2.3）
