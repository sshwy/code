#ifndef _LEXER_H_
#define _LEXER_H_

#include "util.h"
#include <iostream>
#include <string>
#include <vector>

using std::cout;
using std::endl;
using std::string;
using std::vector;

namespace lexer {

  const char LPAREN = '(';
  const char RPAREN = ')';
  const char LAMBDA = '\\';
  const char DOT = '.';
  const char LCID = 'a';

  const unsigned Lparen = 0;
  const unsigned Rparen = 1;
  const unsigned Lambda = 2;
  const unsigned Dot = 3;
  const unsigned Lcid = 4;

  class Token {
  private:
    unsigned Type;
    string content;

  public:
    Token(unsigned _type_, string _content_ = "") {
      Type = _type_;
      content = _content_;
    }
    unsigned type() { return Type; }
    string name() { return content; }
    void set_name(string s) { content = s; }
  };

  vector<Token> Tokenize(string s) {
    vector<Token> v, v2, v3;
    int L = 0;
    for (char c : s) {
      if (c == LPAREN) {
        v.push_back(Token(Lparen));
        ++L;
      } else if (c == RPAREN) {
        v.push_back(Token(Rparen));
        ensuref(L, "There's no Lparen!");
        if (Status != 0) return v;
        --L;
      } else if (c == LAMBDA) {
        v.push_back(Token(Lambda));
      } else if (c == DOT) {
        v.push_back(Token(Dot));
      } else {
        v.push_back(Token(Lcid, string(1, c)));
      }
    }
    ensuref(!L, "There's too many Lparen!");
    if (Status != 0) return v;
    auto las = v.end();
    for (auto p = v.begin(); p != v.end(); ++p) {
      if (p->type() != Lcid) {
        if (las != v.end()) v2.push_back(*las), las = v.end();
        v2.push_back(*p);
      } else if (notprint(p->name())) {
        if (las != v.end()) v2.push_back(*las), las = v.end();
      } else {
        if (las == v.end()) {
          las = p;
        } else {
          las->set_name(las->name() + p->name());
        }
      }
    }
    if (las != v.end()) v2.push_back(*las), las = v.end();

    v = v2;
    v.insert(v.begin(), Token(Lparen));
    v.push_back(Token(Rparen));
    return v;
  }

  void Token_print(vector<Token> v) {
    cout << "------------------------" << endl;
    int len = 0;
    for (auto t : v) {
      switch (t.type()) {
        case Lparen:
          cout << "\033[34m(\033[0m" << endl;
          len += 4;
          cout << indent(len);
          break;
        case Rparen:
          cout << endl;
          len -= 4;
          cout << indent(len) << "\033[34m)\033[0m";
          break;
        case Lambda:
          cout << "\033[32m\\\033[0m";
          break;
        case Dot:
          cout << "\033[32m.\033[0m";
          break;
        default:
          cout << "\033[37m[\033[0m" << t.name() << "\033[37m]\033[0m";
      }
    }
    cout << endl << "------------------------" << endl;
  }

} // namespace lexer

#endif
