#include "ast.h"
#include "lexer.h"
#include <fstream>
#include <iostream>
#include <map>
#include <string>

using std::cerr;
using std::cin;
using std::cout;
using std::endl;
using std::map;
using std::string;

using ast::AST;
using ast::Lambda_expr;
using ast::PNode;

using lexer::Tokenize;

map<string, string> env;

#define work_ensuref(a, b) \
  {                        \
    if ((a) == 0) {        \
      cerr << (b) << endl; \
      Status = 0;          \
      return;              \
    }                      \
  }

#define check_status()                                    \
  {                                                       \
    if (Status != 0) {                                    \
      cerr << "\033[31mError\033[0m " << Message << endl; \
      Status = 0;                                         \
      return;                                             \
    }                                                     \
  }

PNode splay(PNode node) {
  cout << "splay:" << endl;
  AST_print(node);
  bool flag = 1;
  while (flag) {
    flag = 0;
    for (auto p : env) {
      if (ast::find_var(node, p.first)) {
        flag = 1;
        node = ast::replace_var(node, p.first, AST(Tokenize(p.second)));
      }
    }
  }
  cout << "splayed:" << endl;
  AST_print(node);
  return node;
}
string eval_step(string s) {
  auto tree = AST(Tokenize(s));
  if (Status != 0) return "";
  tree = splay(tree);
  if (Status != 0) return "";
  string s1 = Lambda_expr(tree), s2;
  if (Status != 0) return "";
  string res = "\n";
  res += "  " + s1 + "\n";
  do {
    tree = ast::eval(tree);
    if (Status != 0) return "";
    s2 = Lambda_expr(tree);
    if (Status != 0) return "";
    if (s2 != s1) {
      s1 = s2;
      res += "= " + s1 + "\n";
    } else
      break;
  } while (1);
  return res;
}
string eval(string s) {
  auto tree = AST(Tokenize(s));
  if (Status != 0) return "";
  tree = splay(tree);
  if (Status != 0) return "";
  string s1 = Lambda_expr(tree), s2;
  if (Status != 0) return "";
  do {
    cout << "before" << endl;
    AST_print(tree);
    tree = ast::eval(tree);
    cout << "after" << endl;
    AST_print(tree);
    if (Status != 0) return "";
    s2 = Lambda_expr(tree);
    if (Status != 0) return "";
    if (s2 != s1)
      s1 = s2;
    else
      break;
  } while (1);
  ast::AST_print(tree);
  return s1;
}

void print_help() {
  cout << endl
       << "Usage:" << endl
       << ">>> true = \\x.\\y.x       || Define a lambda expression." << endl
       << ">>> del true             || Remove an expression." << endl
       << ">>> ? true x y           || Evaluate an expression." << endl
       << ">>> ?? true x y          || Evaluator an expression step by step." << endl
       << ">>> show                 || Show all expressions you have defined." << endl
       << ">>> read file.in         || read lines from file.in." << endl
       << ">>> write file.out       || write all expressions you have defined "
          "to file.out."
       << endl
       << ">>> exit                 || Exit." << endl
       << endl;
}
void show_variables() {
  cout << endl;
  for (auto x : env) cout << x.first << " = " << x.second << endl;
  cout << endl;
}
void work(string s) {
  if (notprint(s)) return;

  while (s.size() && notprint(*s.begin())) s.erase(s.begin());

  if (*s.begin() == '?') {
    s.erase(s.begin());
    work_ensuref(s.size(), "Invalid Input");
    if (*s.begin() == '?') {
      s.erase(s.begin());

      string str = eval_step(s);
      check_status();
      cout << str << endl;
    } else {

      string str = eval(s);
      check_status();
      cout << endl << str << endl << endl;
    }
  } else {
    string var;
    while (s.size() && *s.begin() != '=' && !notprint(*s.begin())) {
      var.push_back(*s.begin()), s.erase(s.begin());
    }
    work_ensuref(var.size(), "Invalid input");

    if (var == "exit") {
      exit(0);
    } else if (var == "help") {
      print_help();
      return;
    } else if (var == "show") {
      show_variables();
      return;
    } else if (var == "read") {
      var = "";
      while (s.size() && notprint(*s.begin())) s.erase(s.begin());
      while (s.size() && !notprint(*s.begin())) {
        var.push_back(*s.begin()), s.erase(s.begin());
      }
      work_ensuref(var.size() && notprint(s), "Invalid file name");
      std::ifstream file(var);
      if (file.good()) {
        while (!file.eof()) {
          string s;
          getline(file, s);
          work(s);
        }
        file.close();
      } else {
        work_ensuref(0, "Error while reading the file!");
      }
      cout << "Successfully read " << var << endl;
    } else if (var == "write") {
      var = "";
      while (s.size() && notprint(*s.begin())) s.erase(s.begin());
      while (s.size() && !notprint(*s.begin())) {
        var.push_back(*s.begin()), s.erase(s.begin());
      }
      work_ensuref(var.size() && notprint(s), "Invalid file name");
      std::ofstream file(var);
      if (file.good()) {
        for (auto x : env) file << x.first << " = " << x.second << endl;
        file.close();
      } else {
        work_ensuref(0, "Error while reading the file!");
      }
      cout << "Successfully write " << var << endl;
    } else if (var == "del") { // remove
      var = "";
      while (s.size() && notprint(*s.begin())) s.erase(s.begin());
      while (s.size() && !notprint(*s.begin())) {
        var.push_back(*s.begin()), s.erase(s.begin());
      }
      work_ensuref(notprint(s) && env.count(var), "Invalid variable name");
      env.erase(env.find(var));
    } else {
      while (s.size() && notprint(*s.begin())) s.erase(s.begin());
      work_ensuref(s.size(), "Invalid input");
      work_ensuref(*s.begin() == '=', "Variable name should be continous!");
      s.erase(s.begin());

      s = Lambda_expr(AST(Tokenize(s)));
      check_status();

      work_ensuref(!env.count(var), "You should remove " + var + " first!");

      env[var] = s;

      // cerr<<"Variable name:  "<<var<<endl;
      // cerr<<"Variable value: "<<s<<endl;
    }
  }
}
int main(int argc, char **argv) {
  cout << "=== Lambda Evaluator ===" << endl << endl;
  while (!cin.eof()) {
    cout << ">>> ";
    string s;
    getline(cin, s);
    work(s);
  }
  return 0;
}
