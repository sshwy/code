#ifndef _AST_H_
#define _AST_H_

#include "lexer.h"
#include "util.h"
#include <algorithm>
#include <map>
#include <string>
#include <vector>

using lexer::Dot;
using lexer::Lambda;
using lexer::Lcid;
using lexer::Lparen;
using lexer::Rparen;
using lexer::Token;
using std::map;
using std::string;

namespace ast {

  const unsigned Application = 0;
  const unsigned Abstraction = 1;
  const unsigned Identifier = 2;

  const string Untitled = "Untitled";

  class Value {
  private:
    string Name;

  public:
    int Id;
    Value() {
      Name = Untitled;
      Id = 0;
    }
    Value(int _id_) { Id = _id_; }
    Value(string _name_, int _id_) {
      Name = _name_;
      Id = _id_;
    }
    string name() { return Name; }
    int id() { return Id; }
    bool operator==(const Value y) const { return Name == y.Name && Id == y.Id; }
  };
  int id_counter;
  class Node {
  private:
    unsigned int Type;
    /**
     * Application: ch[0],ch[1]
     * Abstraction: pram, ch[1]
     * Identifier: pram
     */
  public:
    Value pram;
    Node *ch[2];
    Node(const unsigned _type_) {
      Type = _type_;
      ch[0] = ch[1] = nullptr;
    }
    int type() { return Type; }
    void setpram(Value _pram_) { pram = _pram_; }
    void dfs_setid(Node *node, Value pram) {
      ensuref(node != nullptr, "empty Node!");
      if (Status != 0) return;
      switch (node->type()) {
        case Application:
          dfs_setid(node->ch[0], pram);
          dfs_setid(node->ch[1], pram);
          break;
        case Abstraction:
          dfs_setid(node->ch[1], pram);
          break;
        case Identifier:
          if (node->pram.name() == pram.name() && node->pram.id() == -1) {
            node->setpram(pram);
          }
          break;
        default:
          ensuref(0, "Type Error!");
          return;
      }
    }
    Node(const unsigned _Abstraction_, Token _parm_, Node *child) {
      ensuref(_Abstraction_ == Abstraction && child != nullptr,
          "Invaild Node construction!");
      if (Status == 0) {
        Type = Abstraction;
        pram = Value(_parm_.name(), ++id_counter);
        ch[1] = child, ch[0] = nullptr;
        dfs_setid(ch[1], pram);
      }
    }
    Node(const unsigned _Identifier_, Token _pram_) {
      ensuref(_Identifier_ == Identifier, "Invaild Node construction!");
      if (Status == 0) {
        Type = Identifier;
        pram = Value(_pram_.name(), -1);
        ch[0] = ch[1] = nullptr;
      }
    }
    Node(const unsigned _Identifier_, Value _pram_) {
      ensuref(_Identifier_ == Identifier, "Invaild NOde construction!");
      if (Status == 0) {
        Type = Identifier;
        pram = _pram_;
        ch[0] = ch[1] = nullptr;
      }
    }
    Node(const unsigned _Application_, Node *lc, Node *rc) {
      ensuref(_Application_ == Application && lc != nullptr && rc != nullptr,
          "Invaild Node construction!");
      if (Status == 0) {
        Type = Application;
        ch[0] = lc, ch[1] = rc;
      }
    }
  };

  typedef Node *PNode;

  PNode AST(vector<Token> v) {
    ensuref(v.begin()->type() == Lparen, "v[0] should be (!");
    ensuref(v.rbegin()->type() == Rparen, "v[end] should be )!");
    ensuref(v.size() > 2, "v should not be empty!");
    if (Status != 0) return nullptr;
    int dep = 0;
    for (auto p = v.begin(); p != v.end(); ++p) {
      if (p->type() == Lparen)
        ++dep;
      else if (p->type() == Rparen)
        --dep;
      if (dep == 1 && p->type() == Dot) {
        v.insert(p + 1, Token(Lparen));
        v.push_back(Token(Rparen));
        break;
      }
    }
    dep = 0;
    vector<int> L;
    map<int, int> nex;
    // cout<<"AFTER"<<endl; print(v);
    bool flag = 0;
    for (auto p = v.begin(); p != v.end(); ++p) {
      if (p->type() == Lparen) {
        L.push_back(p - v.begin());
        ++dep;
      } else if (p->type() == Rparen) {
        nex[L.back()] = p - v.begin();
        L.pop_back();
        --dep;
      }
      if (dep == 1 && p->type() == Dot) {
        ensuref(flag == 0, "There are multi dot!");
        if (Status != 0) return nullptr;
        flag = 1;
      }
    }
    vector<PNode> children;
    for (int i = 1; i + 1 < int(v.size()); ++i) {
      if (nex.count(i)) { //子元素
        vector<Token> sub(v.begin() + i, v.begin() + nex[i] + 1);
        children.push_back(AST(sub));
        if (Status != 0) return nullptr;
        i = nex[i];
      } else if (v[i].type() == Lambda) {
        ensuref(v[i + 1].type() == Lcid && i + 2 < int(v.size()) &&
                    v[i + 2].type() == Dot && i + 3 < int(v.size()) &&
                    v[i + 3].type() == Lparen && nex.count(i + 3),
            "Invaild Lambda Expression!");
        if (Status != 0) return nullptr;
        vector<Token> sub(v.begin() + i + 3, v.begin() + nex[i + 3] + 1);
        auto child = AST(sub);
        if (Status != 0) return nullptr;
        children.push_back(new Node(Abstraction, v[i + 1], child));
        i = nex[i + 3];
      } else {
        children.push_back(new Node(Identifier, v[i]));
      }
    }
    ensuref(children.size(), "children should exist!");
    if (Status != 0) return nullptr;
    reverse(children.begin(), children.end());
    PNode res = children.back();
    children.pop_back();
    while (children.size()) {
      res = new Node(Application, res, children.back());
      ensuref(res != nullptr, "null!");
      if (Status != 0) return nullptr;
      children.pop_back();
    }
    return res;
  }

  void AST_print(PNode node, int len = 0) {
    cout << indent(len) << node << endl;
    switch (node->type()) {
      case Application:
        cout << indent(len) << "\033[32mApplication\033[0m: {" << endl;
        AST_print(node->ch[0], len + 4);
        AST_print(node->ch[1], len + 4);
        cout << indent(len) << "}" << endl;
        break;
      case Abstraction:
        cout << indent(len) << "\033[32mAbstraction\033[0m: " << node->pram.name()
             << "," << node->pram.id() << " {" << endl;
        AST_print(node->ch[1], len + 4);
        cout << indent(len) << "}" << endl;
        break;
      case Identifier:
        cout << indent(len) << "\033[32mIdentifier\033[0m: " << node->pram.name()
             << "," << node->pram.id() << endl;
        break;
      default:
        ensuref(0, "Type Error!");
        if (Status != 0) return;
    }
  }

  map<int, int> newid;
  PNode build_new_tree(PNode node) {
    PNode res = new Node(*node);
    if (res->type() != Application && res->pram.id() != -1) {
      if (!newid.count(res->pram.id())) newid[res->pram.id()] = ++id_counter;
      res->pram.Id = newid[res->pram.id()];
    }
    if (res->ch[0] != nullptr) res->ch[0] = build_new_tree(res->ch[0]);
    if (res->ch[1] != nullptr) res->ch[1] = build_new_tree(res->ch[1]);
    cout << "res: " << res->pram.name() << " " << res->pram.id() << endl;
    return res;
  }
  PNode new_tree(PNode node) {
    newid.clear();
    return build_new_tree(node);
  }
  bool find_var(PNode node, string name) {
    ensuref(node != nullptr, "dfs empty!");
    if (Status != 0) return 0;
    switch (node->type()) {
      case Application:
        if (find_var(node->ch[0], name)) return 1;
        if (find_var(node->ch[1], name)) return 1;
        return 0;
      case Abstraction:
        if (find_var(node->ch[1], name)) return 1;
        return 0;
      case Identifier:
        if (node->pram.name() == name) { return 1; }
        return 0;
      default:
        ensuref(0, "Type Error");
        return 0;
    }
  }
  PNode replace_var(PNode node, string name, PNode replaced) {
    //替换自由元
    ensuref(node != nullptr, "dfs empty!");
    if (Status != 0) return nullptr;
    switch (node->type()) {
      case Application:
        node->ch[0] = replace_var(node->ch[0], name, replaced);
        node->ch[1] = replace_var(node->ch[1], name, replaced);
        return node;
      case Abstraction:
        node->ch[1] = replace_var(node->ch[1], name, replaced);
        return node;
      case Identifier:
        if (node->pram.name() == name) {
          delete node;
          return new_tree(replaced);
        }
        return node;
      default:
        ensuref(0, "Type Error");
        return nullptr;
    }
  }
  PNode dfs_replace(PNode node, Value val, PNode replaced) {
    ensuref(node != nullptr, "dfs empty!");
    if (Status != 0) return nullptr;
    switch (node->type()) {
      case Application:
        node->ch[0] = dfs_replace(node->ch[0], val, replaced);
        node->ch[1] = dfs_replace(node->ch[1], val, replaced);
        return node;
      case Abstraction:
        if (node->pram == val) { return dfs_replace(node->ch[1], val, replaced); }
        node->ch[1] = dfs_replace(node->ch[1], val, replaced);
        return node;
      case Identifier:
        cout << "r: " << val.name() << "," << val.id() << endl;
        if (replaced->type() == Identifier)
          cout << "replaced: " << replaced->pram.name() << endl;
        cout << node << " " << node->pram.name() << " " << node->pram.id() << endl;
        if (node->pram == val) {
          delete node;
          return new_tree(replaced);
        }
        return node;
      default:
        ensuref(0, "Type Error");
        return nullptr;
    }
  }
  PNode eval(PNode node) {
    cout << "eval " << node << " type: " << node->type() << endl;
    if (node->type() == Identifier) cout << node->pram.name() << endl;
    switch (node->type()) {
      case Application:
        // First Left then Right
        node->ch[0] = eval(node->ch[0]);
        node->ch[1] = eval(node->ch[1]);
        if (node->ch[0]->type() == Abstraction
            /*&& node->ch[1]->type() == Identifier*/
        ) {
          return dfs_replace(node->ch[0]->ch[1], node->ch[0]->pram, node->ch[1]);
        }
        return node;
      case Abstraction:
        node->ch[1] = eval(node->ch[1]);
        return node;
      case Identifier:
        return node;
      default:
        ensuref(0, "Type Error");
        return nullptr;
    }
  }

  string Lambda_expr(PNode node) {
    ensuref(node != nullptr, "Lambda dfs empty!");
    if (Status != 0) return "";
    string s;
    switch (node->type()) {
      case Application:
        s += "(";
        s += Lambda_expr(node->ch[0]);
        if (Status != 0) return "";
        s += " ";
        s += Lambda_expr(node->ch[1]);
        if (Status != 0) return "";
        s += ")";
        break;
      case Abstraction:
        s += "(\\";
        s += node->pram.name();
        s += ".";
        s += Lambda_expr(node->ch[1]);
        if (Status != 0) return "";
        s += ")";
        break;
      case Identifier:
        s += node->pram.name();
        break;
      default:
        ensuref(0, "Type Error!");
        if (Status != 0) return "";
    }
    return s;
  }

} // namespace ast

#endif
