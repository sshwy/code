#ifndef _UTIL_H_
#define _UTIL_H_

#include <iostream>
#include <string>

int Status;
std::string Message;

void ensuref(bool flag, const char *s) {
  if (!flag) {
    std::cout << "\033[31mERROR\033[0m " << s << std::endl;
    Status = 1;
    Message = s;
  }
}
void ensuref(bool flag, char *s) {
  if (!flag) {
    std::cout << "\033[31mERROR\033[0m " << s << std::endl;
    Status = 1;
    Message = s;
  }
}

std::string indent(int len) { return std::string(len, ' '); }

bool notprint(char s) {
  if (!(s == ' ' || s == '\n' || s == '\t' || s == '\b' || s == '\r')) return 0;
  return 1;
}
bool notprint(std::string S) {
  for (char s : S) {
    if (!(s == ' ' || s == '\n' || s == '\t' || s == '\b' || s == '\r')) return 0;
  }
  return 1;
}

#endif
