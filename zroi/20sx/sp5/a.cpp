// by Sshwy
//#define DEBUGGER
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

#ifdef DEBUGGER

#define log_prefix()                                                \
  {                                                                 \
    fprintf(stderr, "\033[37mLine %-3d [%dms]:\033[0m  ", __LINE__, \
        (int)clock() / 1000);                                       \
  }
#define ilog(...) \
  { fprintf(stderr, __VA_ARGS__); }
#define llog(...)      \
  {                    \
    log_prefix();      \
    ilog(__VA_ARGS__); \
  }
#define log(...)           \
  {                        \
    log_prefix();          \
    ilog(__VA_ARGS__);     \
    fprintf(stderr, "\n"); \
  }
#define red(...)       \
  {                    \
    log_prefix();      \
    ilog("\033[31m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define green(...)     \
  {                    \
    log_prefix();      \
    ilog("\033[32m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define blue(...)      \
  {                    \
    log_prefix();      \
    ilog("\033[34m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }

#else

#define log_prefix() ;
#define ilog(...) ;
#define llog(...) ;
#define log(...) ;
#define red(...) ;
#define green(...) ;
#define blue(...) ;

#endif

namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
#include <list>
namespace IO {
  const int BSZ = 100000;
  char nc() {
    static char bf[BSZ], *p1 = bf, *p2 = bf;
    return p1 == p2 && (p2 = (p1 = bf) + fread(bf, 1, BSZ, stdin), p1 == p2) ? EOF
                                                                             : *p1++;
  }
  int rd() {
    int res = 0;
    char c = nc();
    while (!isdigit(c)) c = nc();
    while (isdigit(c)) res = res * 10 + c - '0', c = nc();
    return res;
  }
  long long rdll() {
    long long res = 0;
    char c = nc();
    while (!isdigit(c)) c = nc();
    while (isdigit(c)) res = res * 10 + c - '0', c = nc();
    return res;
  }
  void rd(char *s) {
    char c = nc();
    while (!isblank(c) && isprint(c) && c != EOF) *s++ = c, c = nc();
    *s = 0;
  }
  int tmp[20], lt;
  char OBF[BSZ], *OBP = OBF;
  void flush() { fwrite(OBF, 1, OBP - OBF, stdout), OBP = OBF; }
  void wrch(char x) {
    if (OBP == OBF + BSZ - 1) flush();
    *OBP = x, ++OBP;
  }
  void wr(int x, int base = 10) {
    lt = 0;
    while (x) tmp[++lt] = x % base, x /= base;
    while (lt > 0) wrch(tmp[lt] + '0'), --lt;
  }
  void wr(long long x, int base = 10) {
    lt = 0;
    while (x) tmp[++lt] = x % base, x /= base;
    while (lt > 0) wrch(tmp[lt] + '0'), --lt;
  }
  void wr(const char *s) {
    while (*s) wrch(*s), ++s;
  }
  void wr(char x) { wrch(x); }
} // namespace IO

typedef pair<int, int> pii;
struct block {
  typedef vector<pii> V;
  V v;
  block() {}
  void insert(pii x) { v.pb(x); }
  void insert(int pos, pii x) { v.insert(v.begin() + pos, x); }
  pair<block, block> split(int k) const {
    block A, B;
    FOR(i, 0, k - 1) A.insert(v[i]);
    FOR(i, k, (int)v.size() - 1) B.insert(v[i]);
    return pair<block, block>(A, B);
  }
};
list<block> List;
int T;
void insert_after(int pos, int v, int id) {
  // log("insert_after(%d,%d)",pos,v);
  if (List.empty()) {
    block b;
    b.insert({v, id});
    List.pb(b);
  } else {
    list<block>::iterator it = List.begin(), it2;
    int s = 0;
    while (it->v.size() + s < pos) s += it->v.size(), ++it;
    it->insert(pos - s, {v, id});
    if (it->v.size() >= T * 2) {
      pair<block, block> p = it->split(T);
      it2 = it, ++it2;
      List.erase(it), List.insert(it2, p.fi), List.insert(it2, p.se);
    }
  }
}

const int N = 2e5 + 5;

int q, T2;
struct atom {
  int a, b, c, d;
} qry[N], op[N];
bool cmp(atom x, atom y) {
  return x.a / T2 != y.a / T2   ? x.a < y.a
         : x.b / T2 != y.b / T2 ? x.b < y.b
                                : x.c < y.c;
}
int lq, lo;

int cl = 1, cr = 0, co;
int cL, cR; // cL指[1,cl-1]里非零数的个数，cR指[1,cr]里非0数个数

int c[N];
int a[N];
void insert(int x) { c[x]++; }
void remove(int x) { c[x]--; }
int mex() {
  int res = 1;
  while (c[res]) ++res;
  return res;
}
void addR() {
  insert(a[++cr]);
  if (a[cr]) ++cR;
}
void subR() {
  if (a[cr]) --cR;
  remove(a[cr]), --cr;
}
void addL() {
  if (a[cl]) ++cL;
  remove(a[cl]), ++cl;
}
void subL() {
  insert(a[--cl]);
  if (a[cl]) --cL;
}
void addO() {
  ++co;
  int pos = op[co].a, v = op[co].b;
  if (pos < cl)
    ++cL, ++cR;
  else if (cl <= pos && pos <= cr)
    insert(v), ++cR;
  a[pos] = v;
}
void subO() {
  int pos = op[co].a, v = op[co].b;
  if (pos < cl)
    --cL, --cR;
  else if (cl <= pos && pos <= cr)
    remove(v), --cR;
  a[pos] = 0;
  --co;
}
int ans[N];
int main() {
  q = IO::rd();
  T = max(1, (int)sqrt(q));
  FOR(i, 1, q) {
    int x = IO::rd(), y = IO::rd(), z = IO::rd();
    if (x == 1) {
      op[++lo] = {z, y}; //第z个位置后面插入y
      insert_after(z, y, lo);
    } else {
      qry[++lq] = {y, z, lo, lq};
    }
  }
  int n = 0;
  for (block b : List)
    for (pii x : b.v) op[x.se].a = ++n, a[n] = op[x.se].b;
  T2 = max(1, (int)pow(n, 2.0 / 3));
  sort(qry + 1, qry + lq, cmp);
  cl = 1, cr = 0, co = lo;
  FOR(i, 1, lq) {
    int l = qry[i].a, r = qry[i].b, o = qry[i].c, id = qry[i].d;
    while (co < o) addO();
    while (o < co) subO();
    while (cR < r) addR();
    while (l - 1 < cL) subL();
    while (r < cR) subR();
    while (cL < l - 1) addL();
    ans[id] = mex();
  }
  FOR(i, 1, lq) IO::wr(ans[i]), IO::wr('\n');
  IO::flush();
  return 0;
}
