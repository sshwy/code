// by Sshwy
//#define DEBUGGER
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

#ifdef DEBUGGER

#define log_prefix()                                                \
  {                                                                 \
    fprintf(stderr, "\033[37mLine %-3d [%dms]:\033[0m  ", __LINE__, \
        (int)clock() / 1000);                                       \
  }
#define ilog(...) \
  { fprintf(stderr, __VA_ARGS__); }
#define llog(...)      \
  {                    \
    log_prefix();      \
    ilog(__VA_ARGS__); \
  }
#define log(...)           \
  {                        \
    log_prefix();          \
    ilog(__VA_ARGS__);     \
    fprintf(stderr, "\n"); \
  }
#define red(...)       \
  {                    \
    log_prefix();      \
    ilog("\033[31m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define green(...)     \
  {                    \
    log_prefix();      \
    ilog("\033[32m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define blue(...)      \
  {                    \
    log_prefix();      \
    ilog("\033[34m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }

#else

#define log_prefix() ;
#define ilog(...) ;
#define llog(...) ;
#define log(...) ;
#define red(...) ;
#define green(...) ;
#define blue(...) ;

#endif

namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 2e5 + 5;

int n, m;
int a[N];
int nex[N];
int c[N];

void trans(int x, int y) {
  // log("trans(%d,%d)",x,y);
  // log("trans(%d,%d)",c[x],c[y]);
  // for(int u=c[x];u;u=nex[u])ilog("(%d,%d)",u,a[u]);
  // ilog("\n");
  // for(int u=c[y];u;u=nex[u])ilog("(%d,%d)",u,a[u]);
  // ilog("\n");
  if (x == y || c[x] == c[y]) return;
  if (c[x] == 0) return;
  if (c[y] == 0) {
    c[y] = c[x];
    c[x] = 0;
  } else {
    int p1 = c[x], p2 = c[y];
    c[y] = min(p1, p2);
    c[x] = 0;
    while (p1 && p2) {
      if (p1 > p2) swap(p1, p2);
      // log("p1=%d,p2=%d",p1,p2);
      assert(p1 != p2);
      while (nex[p1] && nex[p1] < p2) p1 = nex[p1];
      assert(nex[p1] != p2);
      int t = nex[p1];
      nex[p1] = p2;
      p1 = t;
    }
  }
}
int query(int x, int y) {
  if (c[x] == 0 || c[y] == 0) return -1;
  int p1 = c[x], p2 = c[y];
  int res = abs(p1 - p2);
  while (p1 && p2) {
    if (p1 > p2) swap(p1, p2);
    while (nex[p1] && nex[p1] < p2) p1 = nex[p1];
    res = min(res, p2 - p1);
    p1 = nex[p1];
  }
  return res;
}
int main() {
  scanf("%d%d", &n, &m);
  FOR(i, 1, n) scanf("%d", &a[i]);
  ROF(i, n, 1) {
    if (c[a[i]] == 0)
      nex[i] = 0;
    else
      nex[i] = c[a[i]];
    c[a[i]] = i;
  }
  int las = 0;
  FOR(i, 1, n) {
    int op, x, y;
    scanf("%d%d%d", &op, &x, &y);
    x ^= las, y ^= las;
    if (op == 1) {
      trans(x, y);
    } else {
      int t = query(x, y);
      if (t == -1)
        puts("yyb is our red sun and zsy is our blue moon"), las = 0;
      else
        printf("%d\n", las = t);
    }
  }
  return 0;
}
