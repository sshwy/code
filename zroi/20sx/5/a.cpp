// by Sshwy
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 4, NN = 1 << N, NNN = 1 << NN;

int n, nn, nnn;
int w[N], W[NN], f[NNN][NN], pre[NNN][NN];

void print(int mask, int i) {
  if (!mask) return;
  assert(mask >> i & 1);
  print(mask ^ (1 << i), pre[mask][i]);
  printf("%d ", i);
}
int main() {
  scanf("%d", &n);
  nn = 1 << n;
  nnn = 1 << nn;
  FOR(i, 0, n - 1) scanf("%d", w + i), W[1 << i] = w[i];
  FOR(i, 1, nn - 1) W[i] = W[i & (i - 1)] ^ W[i & -i];

  FOR(i, 1, nnn - 2) {
    FOR(j, 1, nn - 1) if (i >> j & 1) {
      FOR(k, 1, nn - 1) if (!(i >> k & 1)) {
        if (f[i ^ (1 << k)][k] < f[i][j] + W[j ^ k]) {
          f[i ^ (1 << k)][k] = f[i][j] + W[j ^ k];
          pre[i ^ (1 << k)][k] = j;
          assert(W[j ^ k] == (W[j] ^ W[k]));
        }
      }
    }
  }
  int pos = max_element(f[nnn - 2] + 1, f[nnn - 2] + nn) - f[nnn - 2];
  printf("%d\n", f[nnn - 2][pos]);
  print(nnn - 2, pos);
  return 0;
}
