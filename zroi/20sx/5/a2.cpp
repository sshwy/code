// by Sshwy
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 6, NN = 1 << N;

int n;
int w[N], W[NN];
int p[NN], ans[NN], mx;

int main() {
  scanf("%d", &n);
  FOR(i, 0, n - 1) scanf("%d", w + i), W[1 << i] = w[i];
  int lim = 1 << n;
  FOR(i, 1, lim - 1) W[i] = W[i & (i - 1)] ^ W[i & -i];
  FOR(i, 1, lim - 1) p[i] = i;
  mx = -1;
  do {
    int tot = 0;
    FOR(i, 2, lim - 1) tot += W[p[i - 1] ^ p[i]];
    if (tot > mx) {
      mx = tot, memcpy(ans, p, sizeof(p));
      printf("tot=%d\n", tot);
      FOR(i, 1, lim - 1) printf("%d ", p[i]);
      puts("");
    }
  } while (next_permutation(p + 1, p + lim));
  printf("%d\n", mx);
  FOR(i, 1, lim - 1) printf("%d ", ans[i]);
  puts("");
  return 0;
}
