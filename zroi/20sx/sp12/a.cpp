// by Sshwy
//#define DEBUGGER
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

#ifdef DEBUGGER

#define log_prefix()                                                \
  {                                                                 \
    fprintf(stderr, "\033[37mLine %-3d [%dms]:\033[0m  ", __LINE__, \
        (int)clock() / 1000);                                       \
  }
#define ilog(...) \
  { fprintf(stderr, __VA_ARGS__); }
#define llog(...)      \
  {                    \
    log_prefix();      \
    ilog(__VA_ARGS__); \
  }
#define log(...)           \
  {                        \
    log_prefix();          \
    ilog(__VA_ARGS__);     \
    fprintf(stderr, "\n"); \
  }
#define red(...)       \
  {                    \
    log_prefix();      \
    ilog("\033[31m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define green(...)     \
  {                    \
    log_prefix();      \
    ilog("\033[32m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define blue(...)      \
  {                    \
    log_prefix();      \
    ilog("\033[34m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }

#else

#define log_prefix() ;
#define ilog(...) ;
#define llog(...) ;
#define log(...) ;
#define red(...) ;
#define green(...) ;
#define blue(...) ;

#endif

namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 2e5 + 5, SZ = N * 30, W = 2e5, INF = 1e9;
int n, k, seed, P;
int a[N];
vector<int> v[N];

int tot, sz[SZ], lc[SZ], rc[SZ], mn[SZ], rt[N];
int cp(int u) {
  ++tot, sz[tot] = sz[u], lc[tot] = lc[u], rc[tot] = rc[u], mn[tot] = mn[u];
  return tot;
}
int insert(int u, int v, int l = 1, int r = W) {
  int u2 = cp(u);
  sz[u2]++, mn[u2] = min(mn[u2], v);
  if (l == r) return u2;
  int mid = (l + r) >> 1;
  if (v <= mid)
    lc[u2] = insert(lc[u2], v, l, mid);
  else
    rc[u2] = insert(rc[u2], v, mid + 1, r);
  return u2;
}
int lb(int u, int v, int l = 1, int r = W) { // lower_bound
  if (sz[u] == 0) return INF;
  if (l == r) return v;
  int mid = (l + r) >> 1;
  if (v <= mid) {
    return min(lb(lc[u], v, l, mid), mn[rc[u]]);
  } else {
    return lb(rc[u], v, mid + 1, r);
  }
}

void dfs(vector<int> ps, int hsh) {
  // printf("dfs: "); for(auto x:ps)cout<<x<<" "; cout<<endl;
  assert(ps.size());
  int y = *ps.begin() + 1;
  // printf("y %d\n",y);
  for (int val = lb(rt[y], 1); val != INF; val = lb(rt[y], val + 1)) {
    int nsh = (hsh * 1ll * seed + val) % P;
    // printf("val %d nsh %d\n",val,nsh);
    vector<int> np;
    for (auto x : ps) {
      auto nx = lower_bound(v[val].begin(), v[val].end(), x + 1);
      while (nx != v[val].end()) {
        if (k) {
          --k;
          printf("%d\n", nsh);
        } else
          return;
        np.pb(*nx);
        ++nx;
      }
    }
    sort(np.begin(), np.end());
    dfs(np, nsh);
    if (k == 0) return;
  }
}
int main() {
  mn[0] = INF;
  scanf("%d%d%d%d", &n, &k, &seed, &P);
  FOR(i, 1, n) scanf("%d", &a[i]), v[a[i]].pb(i);
  ROF(i, n, 1) rt[i] = insert(rt[i + 1], a[i]);
  rt[0] = rt[1];
  vector<int> I = {0};
  dfs(I, 0);
  return 0;
}
