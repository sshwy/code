// by Sshwy
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

const int N = 3e5 + 5;
int n, a, b;
vector<int> g[N];
int f[N], fa[N];
void dfs(int u, int p) {
  fa[u] = p;
  for (int v : g[u])
    if (v != p) dfs(v, u);
}
bool ban[N];
bool ok(int u, int v) {
  if (fa[u] == v && ban[u]) return 0;
  if (fa[v] == u && ban[v]) return 0;
  return 1;
}
void dfs1(int u, int p) {
  f[u] = 0;
  for (int v : g[u])
    if (v != p && ok(u, v)) dfs1(v, u);
  vector<int> V;
  for (int v : g[u])
    if (v != p && ok(u, v)) { V.pb(f[v]); }
  sort(V.begin(), V.end());
  for (int i = 0; i < int(V.size()); i++) {
    f[u] = max(f[u], int(V.size()) - i + V[i]);
  }
}
int ans1 = 1e9, ans2 = 1e9;
void dfs2(int u, int p) {
  vector<pair<int, int>> vp;
  for (int v : g[u]) { vp.pb({f[v], v}); }
  sort(vp.begin(), vp.end());
  f[u] = 0;
  for (int i = 0; i < int(vp.size()); i++) {
    f[u] = max(f[u], int(vp.size()) - i + vp[i].first);
  }
  ans1 = min(ans1, f[u]);

  vector<int> f1, f2;
  for (int i = 0; i < vp.size(); ++i) {
    f1.pb(vp[i].first + vp.size() - i - 1);
    f2.pb(vp[i].first + vp.size() - i);
  }
  for (int i = 1; i < f1.size(); ++i) f1[i] = max(f1[i], f1[i - 1]);
  for (int i = int(f2.size()) - 1; i > 0; --i) f2[i - 1] = max(f2[i - 1], f2[i]);
  for (int i = 0; i < vp.size(); ++i)
    if (vp[i].second != p) {
      int v = vp[i].second;
      int fu = f[u];

      f[u] = 0;
      if (i > 0) f[u] = max(f[u], f1[i - 1]);
      if (i + 1 < vp.size()) f[u] = max(f[u], f2[i + 1]);

      dfs2(v, u);

      f[u] = fu;
    }
}

int main() {
  scanf("%d%d%d", &n, &a, &b);
  FOR(i, 1, n - 1) {
    int u, v;
    scanf("%d%d", &u, &v);
    g[u].pb(v);
    g[v].pb(u);
  }
  dfs(a, 0);
  dfs1(a, 0);
  dfs2(a, 0);
  printf("%d\n", ans1);
  vector<int> path;
  for (int u = b; u != a; u = fa[u]) path.pb(u);
  int l = 0, r = int(path.size()) - 1;
  while (l < r) {
    int mid = (l + r) >> 1;
    ban[path[mid]] = 1;

    dfs1(b, 0);
    dfs1(a, 0);

    if (f[b] - f[a] < 0)
      l = mid + 1;
    else
      r = mid;

    ban[path[mid]] = 0;
  }
  ban[path[l]] = 1;
  dfs1(b, 0);
  dfs1(a, 0);
  ans2 = max(f[b], f[a]);
  ban[path[l]] = 0;
  if (l > 0) {
    --l;
    ban[path[l]] = 1;
    dfs1(b, 0);
    dfs1(a, 0);
    ans2 = min(ans2, max(f[b], f[a]));
    ban[path[l]] = 0;
  }
  printf("%d\n", ans2);
  return 0;
}
