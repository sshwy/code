// by Sshwy
//#define DEBUGGER
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

#ifdef DEBUGGER

#define log_prefix()                                                \
  {                                                                 \
    fprintf(stderr, "\033[37mLine %-3d [%dms]:\033[0m  ", __LINE__, \
        (int)clock() / 1000);                                       \
  }
#define ilog(...) \
  { fprintf(stderr, __VA_ARGS__); }
#define llog(...)      \
  {                    \
    log_prefix();      \
    ilog(__VA_ARGS__); \
  }
#define log(...)           \
  {                        \
    log_prefix();          \
    ilog(__VA_ARGS__);     \
    fprintf(stderr, "\n"); \
  }
#define red(...)       \
  {                    \
    log_prefix();      \
    ilog("\033[31m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define green(...)     \
  {                    \
    log_prefix();      \
    ilog("\033[32m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define blue(...)      \
  {                    \
    log_prefix();      \
    ilog("\033[34m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }

#else

#define log_prefix() ;
#define ilog(...) ;
#define llog(...) ;
#define log(...) ;
#define red(...) ;
#define green(...) ;
#define blue(...) ;

#endif

namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 2e5 + 5, W = 1e5, SZ = N * 60;
int n, a[N], fa[N], rt[N], tot, lc[SZ], rc[SZ], sz[SZ], m1[N], m2[N], ans[N];
vector<int> v[N];

long long calc() {
  FOR(i, 1, n) v[i] = {a[i]};
  ROF(i, n, 2) v[fa[i]].insert(v[fa[i]].end(), v[i].begin(), v[i].end());
  FOR(i, 1, n) sort(v[i].begin(), v[i].end());
  long long res = 0;
  FOR(i, 1, n) res += v[i][(v[i].size() + 1) / 2 - 1];
  return res;
}
int main() {
  scanf("%d", &n);
  FOR(i, 1, n) scanf("%d", &a[i]);
  FOR(i, 2, n) scanf("%d", &fa[i]);

  FOR(i, 1, n) {
    int v = a[i];
    a[i] = W;
    printf("%lld\n", calc());
    a[i] = v;
  }

  return 0;
}
