// by Sshwy
//#define DEBUGGER
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

#ifdef DEBUGGER

#define log_prefix()                                                \
  {                                                                 \
    fprintf(stderr, "\033[37mLine %-3d [%dms]:\033[0m  ", __LINE__, \
        (int)clock() / 1000);                                       \
  }
#define ilog(...) \
  { fprintf(stderr, __VA_ARGS__); }
#define llog(...)      \
  {                    \
    log_prefix();      \
    ilog(__VA_ARGS__); \
  }
#define log(...)           \
  {                        \
    log_prefix();          \
    ilog(__VA_ARGS__);     \
    fprintf(stderr, "\n"); \
  }
#define red(...)       \
  {                    \
    log_prefix();      \
    ilog("\033[31m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define green(...)     \
  {                    \
    log_prefix();      \
    ilog("\033[32m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define blue(...)      \
  {                    \
    log_prefix();      \
    ilog("\033[34m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }

#else

#define log_prefix() ;
#define ilog(...) ;
#define llog(...) ;
#define log(...) ;
#define red(...) ;
#define green(...) ;
#define blue(...) ;

#endif

namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 2e5 + 5, W = 1e5, SZ = N * 60;
int n, a[N], fa[N], rt[N], tot, lc[SZ], rc[SZ], sz[SZ], m1[N], m2[N];
vector<int> g[N];

void insert(int &u, int v, int l = 1, int r = W) {
  if (!u) u = ++tot;
  if (l == r) return ++sz[u], void();
  int mid = (l + r) >> 1;
  if (v <= mid)
    insert(lc[u], v, l, mid);
  else
    insert(rc[u], v, mid + 1, r);
  sz[u] = sz[lc[u]] + sz[rc[u]];
  // assert(sz[0]==0);
}
int merge(int x, int y) {
  if (!x || !y) return x + y;
  int z = ++tot;
  lc[z] = merge(lc[x], lc[y]), rc[z] = merge(rc[x], rc[y]);
  sz[z] = sz[x] + sz[y];
  // if(lc[z] || rc[z])assert(sz[z]==sz[lc[z]]+sz[rc[z]]);
  // assert(sz[0]==0);
  return z;
}
int kth(int u, int k, int l = 1, int r = W) {
  // assert(k<=sz[u]);
  if (l == r) return l;
  int mid = (l + r) >> 1;
  if (k <= sz[lc[u]])
    return kth(lc[u], k, l, mid);
  else
    return kth(rc[u], k - sz[lc[u]], mid + 1, r);
}

long long c[N], ans[N];
void add(int pos, long long v) {
  for (int i = pos; i < N; i += i & -i) c[i] += v;
}
long long pre(int pos) {
  long long res = 0;
  for (int i = pos; i > 0; i -= i & -i) res += c[i];
  return res;
}
long long query(int l, int r) { return pre(r) - pre(l - 1); }

void dfs(int u) {
  add(m1[u], m2[u] - m1[u]);
  long long delta = query(a[u], W);
  // printf("u %d delta %lld\n",u,delta);
  ans[u] = delta;
  // if(sz[rt[u]]==1){ assert(g[u].size() == 0); }
  for (int v : g[u]) { dfs(v); }
  add(m1[u], m1[u] - m2[u]);
}

int main() {
  scanf("%d", &n);
  FOR(i, 1, n) scanf("%d", &a[i]);
  FOR(i, 2, n) scanf("%d", &fa[i]), g[fa[i]].pb(i);

  FOR(i, 1, n) insert(rt[i], a[i]);
  ROF(i, n, 2) { rt[fa[i]] = merge(rt[fa[i]], rt[i]); }

  FOR(i, 1, n) {
    // assert(sz[rt[i]]);
    if (sz[rt[i]] == 1) {
      m1[i] = kth(rt[i], 1);
      m2[i] = W;
    } else {
      int x = (sz[rt[i]] + 1) / 2;
      m1[i] = kth(rt[i], x);
      m2[i] = kth(rt[i], x + 1);
    }
    // printf("i %d m1 %d m2 %d\n",i,m1[i],m2[i]);
  }

  long long tot = 0;
  FOR(i, 1, n) tot += m1[i];

  dfs(1);
  FOR(i, 1, n) printf("%lld\n", ans[i] + tot);

  return 0;
}
