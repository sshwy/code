// by Sshwy
//#define DEBUGGER
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

#ifdef DEBUGGER

#define log_prefix()                                                \
  {                                                                 \
    fprintf(stderr, "\033[37mLine %-3d [%dms]:\033[0m  ", __LINE__, \
        (int)clock() / 1000);                                       \
  }
#define ilog(...) \
  { fprintf(stderr, __VA_ARGS__); }
#define llog(...)      \
  {                    \
    log_prefix();      \
    ilog(__VA_ARGS__); \
  }
#define log(...)           \
  {                        \
    log_prefix();          \
    ilog(__VA_ARGS__);     \
    fprintf(stderr, "\n"); \
  }
#define red(...)       \
  {                    \
    log_prefix();      \
    ilog("\033[31m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define green(...)     \
  {                    \
    log_prefix();      \
    ilog("\033[32m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define blue(...)      \
  {                    \
    log_prefix();      \
    ilog("\033[34m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }

#else

#define log_prefix() ;
#define ilog(...) ;
#define llog(...) ;
#define log(...) ;
#define red(...) ;
#define green(...) ;
#define blue(...) ;

#endif

namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 2e5 + 5, P = 998244353;
int pw(int a, int m) {
  int res = 1;
  while (m) m & 1 ? res = 1ll * res * a % P : 0, a = 1ll * a * a % P, m >>= 1;
  return res;
}
int n, m, a[N], p[N], fac[N], fnv[N], vis[N];
int binom(int a, int b) {
  if (a < b) return 0;
  return fac[a] * 1ll * fnv[b] % P * fnv[a - b] % P;
}
int pre(int pos) {
  while (!vis[pos]) --pos;
  return pos;
}
int nex(int pos) {
  while (!vis[pos]) ++pos;
  return pos;
}
int main() {
  scanf("%d%d", &n, &m);
  FOR(i, 1, n) scanf("%d", &a[i]);
  FOR(i, 1, n) scanf("%d", &p[i]);
  sort(a + 1, a + n + 1);

  fac[0] = 1;
  FOR(i, 1, m) fac[i] = 1ll * fac[i - 1] * i % P;
  fnv[m] = pw(fac[m], P - 2);
  ROF(i, m, 1) fnv[i - 1] = fnv[i] * 1ll * i % P;

  vis[0] = vis[n + 1] = 1;
  a[0] = 0, a[n + 1] = m + 1;
  int ans = 0;
  FOR(i, 1, n) { // p[1]..p[i-1]都等于，p[i]严格小
    int x = p[i], L = x - 1, R = x + 1;
    while (!vis[L]) --L;
    while (!vis[R]) ++R;
    // printf("x %d L %d R %d\n",x,L,R);
    int s = 0;
    FOR(v, a[L] + 1, a[x] - 1) { // x上填的数
      // printf("v %d\n",v);
      // printf("%d %d %d %d\n",v-a[L]-1,x-L-1,a[R]-v-1,R-x-1);
      int t =
          binom(v - a[L] - 1, x - L - 1) * 1ll * binom(a[R] - v - 1, R - x - 1) % P;
      s = (s + t) % P;
    }
    // printf("s %d\n",s);
    if (s) {
      if (L > 0) {
        int nexL = pre(L - 1);
        while (nexL > 0) {
          s = s * 1ll * binom(a[L] - a[nexL] - 1, L - nexL - 1) % P;
          L = nexL;
          nexL = pre(L - 1);
        }
        s = s * 1ll * binom(a[L] - a[nexL] - 1, L - nexL - 1) % P;
      }
      if (R <= n) {
        int nexR = nex(R + 1);
        while (nexR <= n) {
          s = s * 1ll * binom(a[nexR] - a[R] - 1, nexR - R - 1) % P;
          R = nexR;
          nexR = nex(R + 1);
        }
        s = s * 1ll * binom(a[nexR] - a[R] - 1, nexR - R - 1) % P;
      }
    }
    ans = (ans + s) % P;

    vis[p[i]] = 1;
  }
  printf("%d\n", ans);
  return 0;
}
