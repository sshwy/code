// by Sshwy
//#define DEBUGGER
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

#ifdef DEBUGGER

#define log_prefix()                                                \
  {                                                                 \
    fprintf(stderr, "\033[37mLine %-3d [%dms]:\033[0m  ", __LINE__, \
        (int)clock() / 1000);                                       \
  }
#define ilog(...) \
  { fprintf(stderr, __VA_ARGS__); }
#define llog(...)      \
  {                    \
    log_prefix();      \
    ilog(__VA_ARGS__); \
  }
#define log(...)           \
  {                        \
    log_prefix();          \
    ilog(__VA_ARGS__);     \
    fprintf(stderr, "\n"); \
  }
#define red(...)       \
  {                    \
    log_prefix();      \
    ilog("\033[31m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define green(...)     \
  {                    \
    log_prefix();      \
    ilog("\033[32m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define blue(...)      \
  {                    \
    log_prefix();      \
    ilog("\033[34m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }

#else

#define log_prefix() ;
#define ilog(...) ;
#define llog(...) ;
#define log(...) ;
#define red(...) ;
#define green(...) ;
#define blue(...) ;

#endif

namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 1e5 + 5, P = 1e9 + 7;
int gcd(int a, int b) { return b ? gcd(b, a % b) : a; }
int n, p[N], ans;
vector<int> a[N];
int calc(vector<int> &v) {
  // for(int x:v)printf("%d ",x); puts("");
  int res = 0;
  // for(int i=0;i<v.size();i++){
  //    int g=v[i];
  //    for(int j=i;j<v.size();j++){
  //        g=gcd(g,v[j]);
  //        res=(res+g)%P;
  //    }
  //}
  // return res;

  vector<pair<int, int>> V;
  for (int i = 0; i < v.size(); i++) {
    for (auto &p : V) p.se = gcd(p.se, v[i]);
    V.pb({i, v[i]});
    for (int j = 1; j < V.size(); j++) {
      if (V[j - 1].se == V[j].se) V.erase(V.begin() + j), --j;
    }
    int las = i;
    res = (res + v[i]) % P;
    for (int j = (int)V.size() - 1; j >= 0; j--) {
      res = (res + (las - V[j].fi) * 1ll * V[j].se % P) % P;
      las = V[j].fi;
    }
  }
  return res;
}
int main() {
  scanf("%d", &n);
  FOR(i, 1, n) {
    int k;
    scanf("%d", &k);
    FOR(j, 1, k) {
      int x;
      scanf("%d", &x);
      a[i].pb(x);
    }
  }
  FOR(i, 1, n) p[i] = i;
  do {
    vector<int> s;
    FOR(i, 1, n) { s.insert(s.end(), a[p[i]].begin(), a[p[i]].end()); }
    ans = (ans + calc(s)) % P;
  } while (next_permutation(p + 1, p + n + 1));
  printf("%d\n", ans);
  return 0;
}
