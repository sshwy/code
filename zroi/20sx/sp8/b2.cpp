// by Sshwy
//#define DEBUGGER
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

#ifdef DEBUGGER

#define log_prefix()                                                \
  {                                                                 \
    fprintf(stderr, "\033[37mLine %-3d [%dms]:\033[0m  ", __LINE__, \
        (int)clock() / 1000);                                       \
  }
#define ilog(...) \
  { fprintf(stderr, __VA_ARGS__); }
#define llog(...)      \
  {                    \
    log_prefix();      \
    ilog(__VA_ARGS__); \
  }
#define log(...)           \
  {                        \
    log_prefix();          \
    ilog(__VA_ARGS__);     \
    fprintf(stderr, "\n"); \
  }
#define red(...)       \
  {                    \
    log_prefix();      \
    ilog("\033[31m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define green(...)     \
  {                    \
    log_prefix();      \
    ilog("\033[32m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define blue(...)      \
  {                    \
    log_prefix();      \
    ilog("\033[34m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }

#else

#define log_prefix() ;
#define ilog(...) ;
#define llog(...) ;
#define log(...) ;
#define red(...) ;
#define green(...) ;
#define blue(...) ;

#endif

namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 1e6 + 5, M = N * 20;

struct qxx {
  int nex, t, v;
} e[M * 2];
int h[N], le = 1;
void add_path(int f, int t, int v) { e[++le] = {h[f], t, v}, h[f] = le; }

int n, m, s, tot, dep[N], fa[N];
long long d[N];
vector<int> g[N];

void dfs1(int u, int p) {
  dep[u] = dep[p] + 1, fa[u] = p;
  for (int v : g[u])
    if (v != p) dfs1(v, u);
}
vector<int> path(int u, int v) {
  vector<int> res;
  while (u != v) {
    if (dep[u] < dep[v]) swap(u, v);
    res.pb(u);
    u = fa[u];
  }
  res.pb(u);
  return res;
}
priority_queue<pair<long long, int>, vector<pair<long long, int>>,
    greater<pair<long long, int>>>
    q;
void dijk(int x) {
  memset(d, 0x3f, sizeof(d));
  d[x] = 0, q.push({0ll, x});
  while (!q.empty()) {
    auto u = q.top();
    q.pop();
    if (d[u.se] < u.fi) continue;
    for (int i = h[u.se]; i; i = e[i].nex) {
      int v = e[i].t, w = e[i].v;
      if (d[u.se] + w >= d[v]) continue;
      d[v] = d[u.se] + w, q.push({d[v], v});
    }
  }
}

int main() {
  scanf("%d%d%d", &n, &m, &s);
  FOR(i, 1, n - 1) {
    int u, v, w;
    scanf("%d%d%d", &u, &v, &w), g[u].pb(v), g[v].pb(u), add_path(u, v, w),
        add_path(v, u, w);
  }
  dfs1(1, 0);

  FOR(i, 1, m) {
    int a, b, c, d, e;
    scanf("%d%d%d%d%d", &a, &b, &c, &d, &e);
    for (int x : path(c, d))
      for (int y : path(a, b)) { add_path(x, y, e); }
  }

  dijk(s);
  FOR(i, 1, n) printf("%lld\n", d[i]);

  return 0;
}
