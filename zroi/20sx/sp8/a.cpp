// by Sshwy
//#define DEBUGGER
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

#ifdef DEBUGGER

#define log_prefix()                                                \
  {                                                                 \
    fprintf(stderr, "\033[37mLine %-3d [%dms]:\033[0m  ", __LINE__, \
        (int)clock() / 1000);                                       \
  }
#define ilog(...) \
  { fprintf(stderr, __VA_ARGS__); }
#define llog(...)      \
  {                    \
    log_prefix();      \
    ilog(__VA_ARGS__); \
  }
#define log(...)           \
  {                        \
    log_prefix();          \
    ilog(__VA_ARGS__);     \
    fprintf(stderr, "\n"); \
  }
#define red(...)       \
  {                    \
    log_prefix();      \
    ilog("\033[31m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define green(...)     \
  {                    \
    log_prefix();      \
    ilog("\033[32m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define blue(...)      \
  {                    \
    log_prefix();      \
    ilog("\033[34m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }

#else

#define log_prefix() ;
#define ilog(...) ;
#define llog(...) ;
#define log(...) ;
#define red(...) ;
#define green(...) ;
#define blue(...) ;

#endif

namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 1e6 + 5;

struct qxx {
  int nex, t;
} e[N * 2];
int h[N], le = 1;
void add_path(int f, int t) { e[++le] = {h[f], t}, h[f] = le; }

int n, m, dfn[N], low[N], totdfn, s[N], tp, vis[N], scc[N], totscc, U[N], V[N], cnt,
    a[N], d[N], c[N];
queue<int> q;

void dfs(int u) {
  dfn[u] = low[u] = ++totdfn;
  s[++tp] = u, vis[u] = 1;
  for (int i = h[u]; i; i = e[i].nex) {
    int v = e[i].t;
    if (!dfn[v])
      dfs(v), low[u] = min(low[u], low[v]);
    else if (vis[v])
      low[u] = min(low[u], dfn[v]);
  }
  if (low[u] == dfn[u]) {
    ++totscc;
    while (s[tp] != u) scc[s[tp]] = totscc, vis[s[tp]] = 0, --tp;
    scc[s[tp]] = totscc, vis[s[tp]] = 0, --tp;
  }
}

int main() {
  scanf("%d%d", &n, &m);
  FOR(i, 1, m) {
    int a, b;
    scanf("%d%d", &a, &b);
    add_path(a, b);
  }
  FOR(i, 1, n) if (!dfn[i]) dfs(i);
  FOR(i, 1, n) a[scc[i]]++, assert(scc[i]);
  FOR(u, 1, n) for (int i = h[u]; i; i = e[i].nex) {
    int v = e[i].t;
    if (scc[u] != scc[v]) {
      ++cnt;
      U[cnt] = scc[u];
      V[cnt] = scc[v];
    }
  }
  fill(h, h + N, 0);
  le = 1;
  FOR(i, 1, cnt) add_path(U[i], V[i]), d[V[i]]++;
  FOR(i, 1, totscc) if (d[i] == 0) {
    c[i] = a[i];
    q.push(i);
  }
  while (!q.empty()) {
    int u = q.front();
    q.pop();
    for (int i = h[u]; i; i = e[i].nex) {
      int v = e[i].t;
      d[v]--;
      c[v] = max(c[v], c[u] + a[v]);
      if (d[v] == 0) q.push(v);
    }
  }
  int ans = *max_element(c + 1, c + totscc + 1);
  printf("%d\n", ans);
  return 0;
}
