// by Sshwy
//#define DEBUGGER
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

#ifdef DEBUGGER

#define log_prefix()                                                \
  {                                                                 \
    fprintf(stderr, "\033[37mLine %-3d [%dms]:\033[0m  ", __LINE__, \
        (int)clock() / 1000);                                       \
  }
#define ilog(...) \
  { fprintf(stderr, __VA_ARGS__); }
#define llog(...)      \
  {                    \
    log_prefix();      \
    ilog(__VA_ARGS__); \
  }
#define log(...)           \
  {                        \
    log_prefix();          \
    ilog(__VA_ARGS__);     \
    fprintf(stderr, "\n"); \
  }
#define red(...)       \
  {                    \
    log_prefix();      \
    ilog("\033[31m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define green(...)     \
  {                    \
    log_prefix();      \
    ilog("\033[32m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define blue(...)      \
  {                    \
    log_prefix();      \
    ilog("\033[34m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }

#else

#define log_prefix() ;
#define ilog(...) ;
#define llog(...) ;
#define log(...) ;
#define red(...) ;
#define green(...) ;
#define blue(...) ;

#endif

namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 2e6 + 5, N2 = 3e5 + 5, SZ = N2 << 2, M2 = 1e5 + 5,
          M = N2 * 10 + M2 * 19 * 6;

struct qxx {
  int nex, t, v;
} e[M * 2];
int h[N], le = 1;
void add_path(int f, int t, int v) { e[++le] = {h[f], t, v}, h[f] = le; }

int n, m, s, tot, sz[N2], dep[N2], big[N2], id[N2], nd[N2], totid, top[N2], in[SZ],
    ot[SZ], in2[N2], ot2[N2], fa[N2];
long long d[N];
vector<int> g[N2];

void dfs1(int u, int p) {
  sz[u] = 1, dep[u] = dep[p] + 1, big[u] = 0, fa[u] = p;
  for (int v : g[u])
    if (v != p) dfs1(v, u), sz[u] += sz[v], sz[big[u]] < sz[v] ? big[u] = v : 0;
}
void dfs2(int u, int p, int tp) {
  id[u] = ++totid, nd[totid] = u, top[u] = tp;
  if (big[u]) dfs2(big[u], u, tp);
  for (int v : g[u])
    if (v != p && v != big[u]) dfs2(v, u, v);
}
void build(int u = 1, int l = 1, int r = n) {
  in[u] = l == r ? nd[l] : ++tot, ot[u] = l == r ? nd[l] : ++tot;
  if (u > 1) add_path(ot[u], ot[u >> 1], 0), add_path(in[u >> 1], in[u], 0);
  if (l == r) return;
  int mid = (l + r) >> 1;
  build(u << 1, l, mid), build(u << 1 | 1, mid + 1, r);
}
void link_out(int L, int R, int nd, int u = 1, int l = 1, int r = n) {
  if (R < l || r < L) return;
  if (L <= l && r <= R) return add_path(ot[u], nd, 0), void();
  int mid = (l + r) >> 1;
  link_out(L, R, nd, u << 1, l, mid), link_out(L, R, nd, u << 1 | 1, mid + 1, r);
}
void link_in(int L, int R, int nd, int u = 1, int l = 1, int r = n) {
  if (R < l || r < L) return;
  if (L <= l && r <= R) return add_path(nd, in[u], 0), void();
  int mid = (l + r) >> 1;
  link_in(L, R, nd, u << 1, l, mid), link_in(L, R, nd, u << 1 | 1, mid + 1, r);
}
int query_out(int u, int v) {
  int res = ++tot;
  while (top[u] != top[v]) {
    if (dep[top[u]] < dep[top[v]]) swap(u, v);
    add_path(ot2[u], res, 0), u = fa[top[u]];
  }
  if (dep[u] < dep[v]) swap(u, v);
  link_out(id[v], id[u], res);
  return res;
}
int query_in(int u, int v) {
  int res = ++tot;
  while (top[u] != top[v]) {
    if (dep[top[u]] < dep[top[v]]) swap(u, v);
    add_path(res, in2[u], 0), u = fa[top[u]];
  }
  if (dep[u] < dep[v]) swap(u, v);
  link_in(id[v], id[u], res);
  return res;
}

priority_queue<pair<long long, int>, vector<pair<long long, int>>,
    greater<pair<long long, int>>>
    q;
void dijk(int x) {
  memset(d, 0x3f, sizeof(d));
  d[x] = 0, q.push({0ll, x});
  while (!q.empty()) {
    auto u = q.top();
    q.pop();
    if (d[u.se] < u.fi) continue;
    for (int i = h[u.se]; i; i = e[i].nex) {
      int v = e[i].t, w = e[i].v;
      if (d[u.se] + w >= d[v]) continue;
      d[v] = d[u.se] + w, q.push({d[v], v});
    }
  }
}

int main() {
  scanf("%d%d%d", &n, &m, &s);
  FOR(i, 1, n - 1) {
    int u, v, w;
    scanf("%d%d%d", &u, &v, &w), g[u].pb(v), g[v].pb(u), add_path(u, v, w),
        add_path(v, u, w);
  }
  dfs1(1, 0);
  dfs2(1, 0, 1);

  tot = n;
  FOR(u, 1, n)
  in2[u] = ++tot, ot2[u] = ++tot, add_path(in2[u], u, 0), add_path(u, ot2[u], 0);
  FOR(u, 1, n)
  if (top[u] != u) add_path(ot2[fa[u]], ot2[u], 0), add_path(in2[u], in2[fa[u]], 0);

  build();

  FOR(i, 1, m) {
    int a, b, c, d, e;
    scanf("%d%d%d%d%d", &a, &b, &c, &d, &e);
    int u = query_out(c, d), v = query_in(a, b);
    add_path(u, v, e);
  }

  dijk(s);
  long long X = 0;
  FOR(i, 1, n) printf("%lld\n", d[i]), X ^= d[i];
  printf("%lld\n", X);

  return 0;
}
