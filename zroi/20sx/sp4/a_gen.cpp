// by Sshwy
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/

using namespace RA;

int v[1000000][3], lv;
int p[1000000], q[1000000];
int LIM = 3e3, LIM2 = 10;

int main() {
  srand(clock() + time(0));
  int m = 500000, n = 0;
  FOR(i, 1, m) {
    if (r(LIM) < LIM * 0.8 || n == 0) {
      ++n;
      v[i][0] = 0;
      v[i][1] = r(0, n - 1);
      v[i][2] = r(1, LIM);
    } else {
      v[i][0] = 1;
      v[i][1] = r(0, n - 1);
      v[i][2] = r(1, LIM);
    }
  }
  printf("%d %d\n", n, m);
  FOR(i, 1, m) {
    if (v[i][0] == 0)
      printf("Insert %d %d\n", v[i][1], v[i][2]);
    else
      printf("Change %d %d\n", v[i][1], v[i][2]);
  }
  FOR(i, 1, n) {
    p[i] = p[i - 1] + r(1, LIM2);
    q[i] = q[i - 1] + r(1, LIM);
  }
  FOR(i, 1, n) printf("%d %d\n", p[i], q[i]);
  return 0;
}
