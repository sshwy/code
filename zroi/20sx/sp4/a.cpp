// by Sshwy
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
namespace IO {
  const int BSZ = 100000;
  char nc() {
    static char bf[BSZ], *p1 = bf, *p2 = bf;
    return p1 == p2 && (p2 = (p1 = bf) + fread(bf, 1, BSZ, stdin), p1 == p2) ? EOF
                                                                             : *p1++;
  }
  int rd() {
    int res = 0;
    char c = nc();
    while (!isdigit(c)) c = nc();
    while (isdigit(c)) res = res * 10 + c - '0', c = nc();
    return res;
  }
  long long rdll() {
    long long res = 0;
    char c = nc();
    while (!isdigit(c)) c = nc();
    while (isdigit(c)) res = res * 10 + c - '0', c = nc();
    return res;
  }
  void rd(char *s) {
    char c = nc();
    while (!isblank(c) && isprint(c) && c != EOF) *s++ = c, c = nc();
    *s = 0;
  }
  int tmp[20], lt;
  char OBF[BSZ], *OBP = OBF;
  void flush() { fwrite(OBF, 1, OBP - OBF, stdout), OBP = OBF; }
  void wrch(char x) {
    if (OBP == OBF + BSZ - 1) flush();
    *OBP = x, ++OBP;
  }
  void wr(int x, int base = 10) {
    if (x == 0) return wrch('0'), void();
    lt = 0;
    while (x) tmp[++lt] = x % base, x /= base;
    while (lt > 0) wrch(tmp[lt] + '0'), --lt;
  }
  void wr(long long x, int base = 10) {
    if (x == 0) return wrch('0'), void();
    lt = 0;
    while (x) tmp[++lt] = x % base, x /= base;
    while (lt > 0) wrch(tmp[lt] + '0'), --lt;
  }
  void wr(const char *s) {
    while (*s) wrch(*s), ++s;
  }
  void wr(char x) { wrch(x); }
} // namespace IO
/******************heading******************/
const int N = 5e5 + 5, M = N;
const double eps = 1e-9;

int a[N], la;

namespace Treap {
  int tot, seed = 1;
  int lc[N], rc[N], sz[N], val[N], rnd[N];
  int rrand() { return seed = seed * 482711; }
  int new_node(int v) {
    ++tot;
    rnd[tot] = rrand();
    val[tot] = v;
    sz[tot] = 1;
    return tot;
  }
  void pushup(int u) { sz[u] = sz[lc[u]] + sz[rc[u]] + 1; }
  void split(int u, int k, int &x, int &y) { //前一半的大小为k
    if (!u) return x = 0, y = 0, void();
    assert(k <= sz[u]);
    if (k <= sz[lc[u]])
      y = u, split(lc[u], k, x, lc[y]);
    else
      x = u, split(rc[u], k - sz[lc[u]] - 1, rc[x], y);
    pushup(u);
  }
  int merge(int x, int y) {
    if (!x || !y) return x + y;
    if (rnd[x] < rnd[y])
      return rc[x] = merge(rc[x], y), pushup(x), x;
    else
      return lc[y] = merge(x, lc[y]), pushup(y), y;
  }
  void insert_back(int &root, int pos, int v) {
    // printf("insert_back(%d,%d,%d)\n",root,pos,v);
    int x, y;
    split(root, pos, x, y);
    int u = new_node(v);
    root = merge(merge(x, u), y);
    // printf("root=%d\n",root);
  }
  void change(int &root, int pos, int v) {
    int x, y, z;
    split(root, pos, x, y);
    split(y, 1, y, z);
    assert(y);
    val[y] = v;
    root = merge(merge(x, y), z);
  }
  void print(int u) {
    if (u == 0) return;
    print(lc[u]);
    // printf("%d ",val[u]);
    a[++la] = val[u];
    print(rc[u]);
  }
} // namespace Treap
namespace TreapConvex {
  int tot, seed = 1;
  int lc[N], rc[N], sz[N], rnd[N];
  struct Segment {
    long long k, b; // f(x)=kx+b
    double xl, xr;  //端点

    long long f(int x) { return k * x + b; }
    long long f(long long x) { return k * x + b; }
    double f(double x) { return k * x + b; }

    bool beyond(long long K, long long B) {
      if (xl < -1e98 && xr > 1e98) {
        if (K == k && b >= B) return 1;
        return 0;
      } else if (xr > 1e98) {
        if (f(xl) > xl * K + B - eps && k >= K) return 1;
        return 0;
      } else if (xl < -1e98) {
        if (k <= K && f(xr) > xr * K + B - eps) return 1;
        return 0;
      } else {
        if (f(xl) > xl * K + B - eps && f(xr) > xr * K + B - eps) return 1;
        return 0;
      }
    }
    bool under(long long K, long long B) {
      if (xl < -1e98 && xr > 1e98) {
        if (K == k && b < B) return 1;
        return 0;
      } else if (xr > 1e98) {
        if (f(xl) < xl * K + B - eps && k <= K) return 1;
        return 0;
      } else if (xl < -1e98) {
        if (k >= K && f(xr) < xr * K + B - eps) return 1;
        return 0;
      } else {
        if (f(xl) < xl * K + B - eps && f(xr) < xr * K + B - eps) return 1;
        return 0;
      }
    }
    double cross_x(long long K, long long B) {
      assert(k != K);
      double x = (B - b) * 1.0 / (k - K);
      assert(xl - eps <= x && x <= xr + eps);
      return x;
    }
    bool left_cross(long long K, long long B) {
      assert(k != K);
      double x = (B - b) * 1.0 / (k - K);
      assert(xl - eps <= x && x <= xr + eps);
      if (K > k) return 1;
      return 0;
    }
  } sg[N];
  int rrand() { return seed = seed * 482711; }
  int new_node(long long k, long long b, double xl, double xr) {
    ++tot;
    rnd[tot] = rrand();
    sg[tot] = {k, b, xl, xr};
    sz[tot] = 1;
    return tot;
  }
  void pushup(int u) { sz[u] = sz[lc[u]] + sz[rc[u]] + 1; }
  void split(int u, int k, int &x, int &y) { //前一半的大小为k
    if (!u) return x = 0, y = 0, void();
    assert(k <= sz[u]);
    if (k <= sz[lc[u]])
      y = u, split(lc[u], k, x, lc[y]);
    else
      x = u, split(rc[u], k - sz[lc[u]] - 1, rc[x], y);
    pushup(u);
  }
  void split_slope(int u, long long k, int &x, int &y) { //前一半的斜率小于等于k
    if (!u) return x = 0, y = 0, void();
    if (k < sg[u].k)
      y = u, split_slope(lc[u], k, x, lc[y]);
    else
      x = u, split_slope(rc[u], k, rc[x], y);
    pushup(u);
  }
  void split_not_under(int u, long long k, long long b, int &x, int &y) {
    if (!u) return x = 0, y = 0, void();
    if (sg[u].under(k, b))
      y = u, split_not_under(lc[u], k, b, x, lc[y]);
    else
      x = u, split_not_under(rc[u], k, b, rc[x], y);
    pushup(u);
  }
  void split_under(int u, long long k, long long b, int &x, int &y) {
    if (!u) return x = 0, y = 0, void();
    if (!sg[u].under(k, b))
      y = u, split_under(lc[u], k, b, x, lc[y]);
    else
      x = u, split_under(rc[u], k, b, rc[x], y);
    pushup(u);
  }
  void split_xl(int u, long long X, int &x, int &y) {
    if (!u) return x = 0, y = 0, void();
    if (sg[u].xl > X)
      y = u, split_xl(lc[u], X, x, lc[y]);
    else
      x = u, split_xl(rc[u], X, rc[x], y);
    pushup(u);
  }
  int merge(int x, int y) {
    if (!x || !y) return x + y;
    if (rnd[x] < rnd[y])
      return rc[x] = merge(rc[x], y), pushup(x), x;
    else
      return lc[y] = merge(x, lc[y]), pushup(y), y;
  }
  void print_s(int u) {
    if (u == 0) return;
    printf("u=%d, Line: k=%lld, b=%lld, xl=%.3g,xr=%.3g\n", u, sg[u].k, sg[u].b,
        sg[u].xl, sg[u].xr);
  }
  void print(int u) {
    if (u == 0) return;
    print(lc[u]);
    printf("u=%d, Line: k=%lld, b=%lld, xl=%.3g,xr=%.3g\n", u, sg[u].k, sg[u].b,
        sg[u].xl, sg[u].xr);
    print(rc[u]);
  }
  void insert(int &root, long long k, long long b) { //插入一条直线
    // printf("insert(%d,%d,%d)\n",root,k,b);
    if (!root) {
      root = new_node(k, b, -1e99, 1e99);
      return;
    }
    //找到斜率小于等于k的斜率最大的直线
    int x, y, z;
    int x1, x2, z1, z2;
    split_slope(root, k, x, y);
    if (sz[x])
      z = y, split(x, sz[x] - 1, x, y);
    else
      assert(sz[y]), split(y, 1, y, z);
    // printf("y: ");assert(sz[y]==1);
    // print_s(y);
    if (sg[y].beyond(k, b)) return; //扔了。如果线段重合也认为是beyond
    if (sg[y].under(k, b)) {
      // puts("GG");
      split_not_under(x, k, b, x1, x2); //只要不全部在下面的就在前一半
      // cout<<sg[2].under(k,b)<<endl;
      // print_s(2);
      // puts("x:begin");
      // print(x);
      // puts("x:end");
      // printf("x1=%d,x2=%d\n",x1,x2);
      // puts("x1:begin");
      // print(x1);
      // puts("x1:end");
      split_under(z, k, b, z1, z2);
      int L, R;
      split(x1, sz[x1] - 1, x1, L);
      split(z2, 1, R, z2);
      // x1,L,u,R,z2
      // printf("L=%d,R=%d\n",L,R);
      int u = new_node(
          k, b, L ? sg[L].cross_x(k, b) : -1e99, R ? sg[R].cross_x(k, b) : 1e99);
      if (L) sg[L].xr = sg[u].xl;
      if (R) sg[R].xl = sg[u].xr;
      root = merge(x1, merge(L, merge(u, merge(R, z2))));
      return;
    }
    //看是哪种cross
    if (sg[y].left_cross(k, b)) { //左上到右下
      split_under(z, k, b, z1, z2);
      int R = 0;
      if (z2) split(z2, 1, R, z2);
      // x,y,u,R,z2
      int u = new_node(k, b, sg[y].cross_x(k, b), R ? sg[R].cross_x(k, b) : 1e99);
      sg[y].xr = sg[u].xl;
      if (R) sg[R].xl = sg[u].xr;
      root = merge(x, merge(y, merge(u, merge(R, z2))));
    } else {
      split_not_under(x, k, b, x1, x2); //只要不全部在下面的就在前一半
      // printf("%d %d %d\n",x,y,z);
      int L = 0;
      if (x1) split(x1, sz[x1] - 1, x1, L);
      // x1,L,u,y,z
      int u = new_node(k, b, L ? sg[L].cross_x(k, b) : -1e99, sg[y].cross_x(k, b));
      if (L) sg[L].xr = sg[u].xl;
      sg[y].xl = sg[u].xr;
      // printf("%d %d %d %d %d\n",x1,L,u,y,z);
      root = merge(x1, merge(L, merge(u, merge(y, z))));
    }
  }
  long long query(int &root, long long X) { //查询x位置的点值
    // printf("query(%d,%lld)\n",root,X);
    int x, y, z;
    split_xl(root, X, x, z);
    assert(sz[x]);
    split(x, sz[x] - 1, x, y);
    long long res = sg[y].f(X);
    root = merge(merge(x, y), z);
    return res;
  }
} // namespace TreapConvex

int n, m, root;

int main() {
  n = IO::rd(), m = IO::rd();
  FOR(i, 1, m) {
    char op[20];
    int t, u;
    IO::rd(op);
    t = IO::rd();
    u = IO::rd();
    if (op[0] == 'I')
      Treap::insert_back(root, t, u);
    else
      Treap::change(root, t, u);
  }
  Treap::print(root);
  // puts("");
  assert(la == n);

  root = 0;
  FOR(i, 2, n) {
    // printf("\033[32mi=%d\033[0m\n",i);
    int p, q;
    p = IO::rd();
    q = IO::rd();
    TreapConvex::insert(root, p, q);
    // TreapConvex::print(root);
    IO::wr(max(TreapConvex::query(root, -a[i]), 0ll));
    IO::wr('\n');
  }
  IO::flush();
  return 0;
}
