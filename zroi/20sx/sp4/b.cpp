// by Sshwy
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
typedef long long LL;
const int N = 2e7 + 5, P = 998244353;

LL n, m;
bool bp[N];
int pn[N], lp;
int phi[N];

void sieve(int lim) {
  bp[0] = bp[1] = 1;
  FOR(i, 2, lim) {
    if (!bp[i]) pn[++lp] = i, phi[i] = i - 1;
    FOR(j, 1, lp) {
      if (i * pn[j] > lim) break;
      bp[i * pn[j]] = 1;
      if (i % pn[j] == 0) {
        phi[i * pn[j]] = phi[i] * pn[j];
      } else {
        phi[i * pn[j]] = phi[i] * (pn[j] - 1);
      }
    }
  }
}

signed main() {
  scanf("%lld%lld", &n, &m);
  sieve(n + m);
  long long ans = 0;
  FOR(k, 1, n + m) {
    if (n % k + m % k >= k) { ans += phi[k]; }
  }
  ans = ans % P * phi[n] % P * phi[m] % P;
  printf("%lld\n", ans);
  return 0;
}
