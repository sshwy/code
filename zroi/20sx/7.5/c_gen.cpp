// by Sshwy
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/

using namespace RA;

void go() {
  int n = 30000, m = 30000, k = 4;
  printf("%d %d %d\n", n, k, m);
  FOR(i, 1, n) printf("%d%c", r(k), " \n"[i == n]);
  FOR(i, 1, m) {
    int x = r(100), L = r(1, n), R = r(1, n);
    while (R - L < n / 10) R = r(1, n), L = r(1, n);
    if (L > R) swap(L, R);
    if (x < 30) {
      printf("1 %d %d %d\n", L, R, r(k));
    } else if (x < 70) {
      printf("2 %d %d %d\n", L, R, r(k));
    } else {
      printf("3 %d %d\n", L, R);
    }
  }
}
int main() {
  srand(clock());
  int t = 3;
  printf("%d\n", t);
  FOR(i, 1, t) go();
  return 0;
}
