// by Sshwy
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 504, P = 998244353;

int n;

int f[N][N], g[N][N], f0[N][N], g0[N][N];

void DP() {
  f0[0][0] = 1;
  g0[0][0] = 1;
  g[0][0] = g[1][1] = 1;
  f[1][1] = 1;
  FOR(i, 2, n) FOR(j, 1, n) {
    f0[i][j] = (g[i - 1][j] - g0[i - 1][j] + P) % P;
    f[i][j] = (g0[i - 1][j - 1] + f0[i][j]) % P;
    g[i][j] = [&]() {
      int res = 0;
      FOR(x, 1, i)
      FOR(y, 1, j) res = (res + f[x][y] * 1ll * g[i - x][j - y]) % P;
      return res;
    }();
    g0[i][j] = [&]() {
      int res = 0;
      FOR(x, 1, i)
      FOR(y, 1, j) res = (res + f0[x][y] * 1ll * g0[i - x][j - y]) % P;
      return res;
    }();
  }
}
int main() {
  scanf("%d", &n);
  DP();
  FOR(i, 1, n) FOR(j, 0, n) printf("%d%c", f[i][j], " \n"[j == n]);
  return 0;
}
