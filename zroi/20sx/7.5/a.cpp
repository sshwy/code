// by Sshwy
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 20, S = 51;
const int P = 998244353;

int n, totQ;
char s[S];
int f[1 << N][S], g[1 << N][S], h[1 << N], LF[1 << N], m2[N * S];

int main() {
  scanf("%d", &n);
  m2[0] = 1;
  FOR(i, 1, n * (S - 1)) m2[i] = m2[i - 1] * 2ll % P;
  FOR(i, 0, n - 1) {
    scanf("%s", s);
    int x = 1 << i, len = strlen(s);
    FOR(j, 0, len - 1) if (s[j] == '?') h[x]++, totQ++;
    FOR(j, 0, len - 1)
    f[x][j] = s[j] == '?' ? 3 : s[j] - '0' + 1; // 1: 0, 2: 1, 3: ?, 4: EOF
    f[x][LF[x] = len] = 4;
    {
      int cnt = 1;
      ROF(j, len, 0) {
        if (s[j] == '?') cnt = cnt * 2ll % P;
        g[x][j] = cnt;
      }
    }
    FOR(mask, 1, x - 1) {
      int l2 = min(LF[mask], len), nex = mask | x;
      FOR(j, 0, l2 - 1) {
        if (f[x][j] == 3)
          f[nex][j] = f[mask][j];
        else if (f[mask][j] == 3)
          f[mask | x][j] = f[x][j];
        else
          f[nex][j] = f[x][j] == f[mask][j] ? f[mask][j] : 4;
      }
      f[nex][LF[mask | x] = l2] = 4;
      ROF(j, len, 0) g[nex][j] = g[mask][j] * 1ll * g[x][j] % P;
      h[nex] = h[mask] + h[x];
    }
  }
  int nn = 1 << n, ans = 0;
  auto pw = [](int a, int m) {
    int res = 1;
    while (m) m & 1 ? res = 1ll * res * a % P : 0, a = 1ll * a * a % P, m >>= 1;
    return res;
  };
  FOR(i, 1, nn - 1) {
    auto sig = [](int x) { return 1 - 2 * (x & 1); };
    auto calc = [&](int mask) {
      int res = 0, cur = 1;
      FOR(j, 0, S - 1) {
        res = (res + cur * 1ll * g[mask][j]) % P;
        if (f[mask][j] == 4) break;
        if (f[mask][j] == 3) cur = cur * 2 % P;
      }
      res = res * 1ll * m2[totQ - h[mask]] % P;
      return res;
    };
    ans = (ans + calc(i) * sig(__builtin_popcount(i) ^ 1)) % P;
  }
  ans = (ans + P) % P;
  printf("%d\n", ans);
  return 0;
}
