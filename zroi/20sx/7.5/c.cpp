// by Sshwy
#include <algorithm>/*{{{*/
#include <bitset>
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
namespace IO {
  const int BSZ = 100000;
  char nc() {
    static char bf[BSZ], *p1 = bf, *p2 = bf;
    return p1 == p2 && (p2 = (p1 = bf) + fread(bf, 1, BSZ, stdin), p1 == p2) ? EOF
                                                                             : *p1++;
  }
  int rd() {
    int res = 0;
    char c = nc();
    while (!isdigit(c)) c = nc();
    while (isdigit(c)) res = res * 10 + c - '0', c = nc();
    return res;
  }
  long long rdll() {
    long long res = 0;
    char c = nc();
    while (!isdigit(c)) c = nc();
    while (isdigit(c)) res = res * 10 + c - '0', c = nc();
    return res;
  }
  void rd(char *s) {
    char c = nc();
    while (!isblank(c) && isprint(c) && c != EOF) *s++ = c, c = nc();
    *s = 0;
  }
  int tmp[20], lt;
  char OBF[BSZ], *OBP = OBF;
  void flush() { fwrite(OBF, 1, OBP - OBF, stdout), OBP = OBF; }
  void wrch(char x) {
    if (OBP == OBF + BSZ - 1) flush();
    *OBP = x, ++OBP;
  }
  void wr(int x, int base = 10) {
    lt = 0;
    while (x) tmp[++lt] = x % base, x /= base;
    while (lt > 0) wrch(tmp[lt] + '0'), --lt;
  }
  void wr(long long x, int base = 10) {
    lt = 0;
    while (x) tmp[++lt] = x % base, x /= base;
    while (lt > 0) wrch(tmp[lt] + '0'), --lt;
  }
  void wr(const char *s) {
    while (*s) wrch(*s), ++s;
  }
  void wr(char x) { wrch(x); }
} // namespace IO
/*}}}*/
/******************heading******************/
const int P = 998244353, N = 3e4 + 5, MTX = 6, PMT = MTX, ALP = MTX;

int xxx;

int n, k, m, I = 10;
int a[N];

struct Matrix {
  int w, h;
  int c[MTX][MTX];
  Matrix() {}
  Matrix(int _w) {
    w = h = _w;
    FOR(i, 0, h) FOR(j, 0, w) c[i][j] = 0;
    FOR(i, 0, h) c[i][i] = 1;
  }
  Matrix(int _h, int _w) {
    w = _w, h = _h;
    FOR(i, 0, h) FOR(j, 0, w) c[i][j] = 0;
  }
  Matrix(int _h, int _w, int _c[MTX][MTX]) {
    w = _w, h = _h;
    FOR(i, 0, h) FOR(j, 0, w) c[i][j] = _c[i][j];
  }
  Matrix operator*(const Matrix &m) {
    long long t[MTX][MTX] = {{0}};
    // assert(w==m.h);
    Matrix res(h, m.w);
    FOR(i, 0, h) FOR(k, 0, w) FOR(j, 0, m.w) { t[i][j] += c[i][k] * 1ll * m.c[k][j]; }
    FOR(i, 0, h) FOR(j, 0, m.w) res.c[i][j] = t[i][j] % P;
    return res;
  }
};
struct Permutation {
  int p[PMT], len;
  Permutation() {}
  Permutation(int L) {
    len = L;
    FOR(i, 0, L) p[i] = i;
  }
} P_k;
Permutation operator*(const Permutation &P, const Permutation &Q) {
  Permutation res;
  res.len = P.len;
  FOR(i, 0, res.len) res.p[i] = P.p[Q.p[i]];
  return res;
}
namespace seg {
  Matrix M[N << 2];
  Permutation tag[N << 2];
  bitset<ALP> C[N << 2];
  inline void pushup(int u) {
    M[u] = M[u << 1] * M[u << 1 | 1];
    C[u] = C[u << 1] | C[u << 1 | 1];
  }
  inline void Matrix_permutation(Matrix &m, const Permutation &p) {
    Matrix res;
    FOR(i, 0, m.h) FOR(j, 0, m.w) res.c[p.p[i]][p.p[j]] = m.c[i][j];
    FOR(i, 0, m.h) FOR(j, 0, m.w) m.c[i][j] = res.c[i][j];
    // m=res;
  }
  inline void Bitset_permutation(bitset<ALP> &B, const Permutation &p) {
    bitset<ALP> res;
    // res.reset();
    FOR(i, 0, k) res[p.p[i]] = B[i];
    B = res;
  }
  inline void node_trans(int u, const Permutation &p) {
    if (p.p[0] == I) return;
    Matrix_permutation(M[u], p);
    Bitset_permutation(C[u], p);
    if (tag[u].p[0] == I)
      tag[u] = p;
    else
      tag[u] = p * tag[u];
  }
  inline void leaf_mul(int u, int v) {
    int ch = [&C, &u]() { FOR(i, 0, k - 1) if (C[u][i]) return i; }();
    ch = ch * v % k;
    M[u] = Matrix(k);
    FOR(i, 0, k) M[u].c[ch][i] = 1;
    C[u].reset(), C[u][ch] = 1;
  }
  inline void pushdown(int u) {
    if (tag[u].p[0] == I) return;
    node_trans(u << 1, tag[u]);
    node_trans(u << 1 | 1, tag[u]);
    tag[u].p[0] = I;
  }
  void build(int u = 1, int l = 1, int r = n) {
    tag[u].p[0] = I;
    if (l == r) { //单个字符
      M[u] = Matrix(k);
      FOR(i, 0, k) M[u].c[a[l]][i] = 1;
      C[u].reset(), C[u][a[l]] = 1;
      return;
    }
    int mid = (l + r) >> 1;
    build(u << 1, l, mid), build(u << 1 | 1, mid + 1, r);
    pushup(u);
  }
  void trans(int L, int R, const Permutation &p, int u = 1, int l = 1, int r = n) {
    if (R < l || r < L) return;
    if (L <= l && r <= R) return node_trans(u, p), void();
    int mid = (l + r) >> 1;
    pushdown(u);
    if (L <= mid) trans(L, R, p, u << 1, l, mid);
    if (mid < R) trans(L, R, p, u << 1 | 1, mid + 1, r);
    pushup(u);
  }
  void mul(int L, int R, int v, int u = 1, int l = 1, int r = n) {
    if (R < l || r < L) return;
    if (l == r) return leaf_mul(u, v), void();
    if (L <= l && r <= R) {
      int x = [&]() {
        bitset<ALP> T;
        T.reset();
        FOR(i, 0, k - 1) if (C[u][i]) {
          if (T[i * v % k]) return 0;
          T[i * v % k] = 1;
        }
        return 1;
      }();
      if (x) {
        Permutation p = [&]() {
          bitset<ALP> T;
          T.reset();
          Permutation P;
          P.len = k;
          FOR(i, 0, k) P.p[i] = -1;
          FOR(i, 0, k - 1) if (C[u][i]) P.p[i] = i * v % k, T[i * v % k] = 1;
          FOR(i, 0, k)
          if (P.p[i] == -1)
            P.p[i] = [&T]() { FOR(i, 0, k) if (T[i] == 0) return T[i] = 1, i; }();
          return P;
        }();
        node_trans(u, p);
        return;
      }
    }
    int mid = (l + r) >> 1;
    pushdown(u);
    if (L <= mid) mul(L, R, v, u << 1, l, mid);
    if (mid < R) mul(L, R, v, u << 1 | 1, mid + 1, r);
    pushup(u);
  }
  Matrix query(int L, int R, int u = 1, int l = 1, int r = n) {
    if (R < l || r < L) return Matrix(k);
    if (L <= l && r <= R) return M[u];
    int mid = (l + r) >> 1;
    pushdown(u);
    if (L <= mid && mid < R)
      return query(L, R, u << 1, l, mid) * query(L, R, u << 1 | 1, mid + 1, r);
    else if (L <= mid)
      return query(L, R, u << 1, l, mid);
    else
      return query(L, R, u << 1 | 1, mid + 1, r);
  }
} // namespace seg

inline int query(int l, int r) {
  Matrix m = seg::query(l, r);
  int res = 0;
  FOR(i, 0, k - 1) res = (res + m.c[i][k]) % P;
  return res;
}

inline void add(int l, int r, int x) {
  Permutation P(k);
  FOR(i, 0, k - 1) P.p[i] = (i + x) % k;
  P.p[k] = k;
  seg::trans(l, r, P);
}

inline void mul(int l, int r, int x) { seg::mul(l, r, x); }

void go() {
  n = IO::rd();
  k = IO::rd();
  m = IO::rd();

  P_k = Permutation(k);
  FOR(i, 1, n) a[i] = IO::rd();

  seg::build();

  FOR(i, 1, m) {
    int op, l, r, x;
    op = IO::rd();
    l = IO::rd();
    r = IO::rd();
    if (op == 1) {
      x = IO::rd();
      add(l, r, x);
    } else if (op == 2) {
      x = IO::rd();
      mul(l, r, x);
    } else {
      IO::wr(query(l, r));
      IO::wr('\n');
    }
  }
}
int main() {
  int t;
  t = IO::rd();
  FOR(i, 1, t) go();
  IO::flush();
  return 0;
}
