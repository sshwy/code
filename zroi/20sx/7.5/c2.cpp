// by Sshwy
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int P = 998244353;
const int N = 3e4 + 5;

int n, k, m;
int a[N], f[N];

int query(int l, int r) {
  int las[] = {-1, -1, -1, -1, -1};
  f[l - 1] = 1;
  FOR(i, l, r) {
    if (las[a[i]] == -1)
      f[i] = f[i - 1] * 2ll % P;
    else
      f[i] = (f[i - 1] * 2ll - f[las[a[i]] - 1]) % P;
    las[a[i]] = i;
  }
  return (f[r] - 1 + P) % P;
}

void go() {
  scanf("%d%d%d", &n, &k, &m);
  FOR(i, 1, n) scanf("%d", &a[i]);
  FOR(i, 1, m) {
    int op, l, r, x;
    scanf("%d%d%d", &op, &l, &r);
    if (op == 1) {
      scanf("%d", &x);
      FOR(i, l, r) a[i] = (a[i] + x) % k;
    } else if (op == 2) {
      scanf("%d", &x);
      FOR(i, l, r) a[i] = (a[i] * x) % k;
    } else {
      printf("%d\n", query(l, r));
    }
  }
}
int main() {
  int t;
  scanf("%d", &t);
  FOR(i, 1, t) go();
  return 0;
}
