#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
namespace IO {
  char nc() {
    static char bf[100000], *p1 = bf, *p2 = bf;
    return p1 == p2 && (p2 = (p1 = bf) + fread(bf, 1, 100000, stdin), p1 == p2)
               ? EOF
               : *p1++;
  }
  int rd() {
    int res = 0;
    char c = getchar();
    while (!isdigit(c)) c = getchar();
    while (isdigit(c)) res = res * 10 + c - '0', c = getchar();
    return res;
  }
} // namespace IO
/******************heading******************/
#define int lld
using namespace RA;
const int N = 11;

int w[N][N];
int a[] = {0, 1, 2, 4, 7, 12, 20, 29, 38, 52, 73};

int getmax(int x) {
  int p[N], res = 0;
  FOR(i, 1, x) p[i] = i;
  do {
    int s = 0;
    FOR(i, 1, x - 1) s += w[p[i]][p[i + 1]];
    res = max(res, s);
  } while (next_permutation(p + 1, p + x + 1));
  return res;
}
void make(int k) {
  int m = getmax(k - 1);
  m++;
  FOR(i, 1, k - 1) w[i][k] = w[k][i] = a[i] * m;
}
int n;
signed main() {
  scanf("%lld", &n);
  FOR(i, 2, n) make(i);
  FOR(i, 1, n) FOR(j, 1, n) printf("%lld%c", w[i][j], " \n"[j == n]);
  return 0;
}
