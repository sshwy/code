#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
namespace IO {
  char nc() {
    static char bf[100000], *p1 = bf, *p2 = bf;
    return p1 == p2 && (p2 = (p1 = bf) + fread(bf, 1, 100000, stdin), p1 == p2)
               ? EOF
               : *p1++;
  }
  int rd() {
    int res = 0;
    char c = getchar();
    while (!isdigit(c)) c = getchar();
    while (isdigit(c)) res = res * 10 + c - '0', c = getchar();
    return res;
  }
} // namespace IO
/******************heading******************/
const int N = 1e5 + 5;

int n;
int a[N];
int maxa, maxp, mina, minp;

pii ans[N];
int la;

int main() {
  scanf("%d", &n);
  FOR(i, 1, n) scanf("%d", &a[i]);
  if (a[1] < a[2])
    mina = a[1], minp = 1, maxa = a[2], maxp = 2;
  else
    mina = a[2], minp = 2, maxa = a[1], maxp = 1;
  FOR(i, 1, n) if (a[i] < mina) mina = a[i], minp = i;
  FOR(i, 1, n) if (a[i] > maxa) maxa = a[i], maxp = i;
  FOR(i, 1, n) {
    if (i == minp || i == maxp) continue;
    if (a[i] <= 0) {
      ans[++la] = (pii){maxa, a[i]}, maxa -= a[i];
    } else {
      ans[++la] = (pii){mina, a[i]}, mina -= a[i];
    }
  }
  ans[++la] = (pii){maxa, mina}, maxa -= mina;
  printf("%d\n", maxa);
  FOR(i, 1, la) printf("%d %d\n", ans[i].fi, ans[i].se);
  return 0;
}
