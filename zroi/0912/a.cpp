#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
namespace IO {
  char nc() {
    static char bf[100000], *p1 = bf, *p2 = bf;
    return p1 == p2 && (p2 = (p1 = bf) + fread(bf, 1, 100000, stdin), p1 == p2)
               ? EOF
               : *p1++;
  }
  int rd() {
    int res = 0;
    char c = getchar();
    while (!isdigit(c)) c = getchar();
    while (isdigit(c)) res = res * 10 + c - '0', c = getchar();
    return res;
  }
} // namespace IO
/******************heading******************/
const int SZ = 1000039;

struct point {
  int x, y;
  point(int _x = 0, int _y = 0) { x = _x, y = _y; }
  void read() { scanf("%d%d", &x, &y); }
} p[100];
int n;

struct hash_map {
  struct data {
    int nex;
    lld u;
    int v;
  };
  data e[SZ];
  int h[SZ], le;
  int hash(lld u) { return (u % SZ + SZ) % SZ; }
  int &operator[](lld u) {
    int hu = hash(u);
    for (int i = h[hu]; i; i = e[i].nex)
      if (e[i].u == u) return e[i].v;
    e[++le] = (data){h[hu], u, 0}, h[hu] = le;
    return e[le].v;
  }
} h;
lld td(int x, int y) { return 1ll * x * 1000000001 + y; }

int go(int a, int b) {
  int res = n;
  FOR(i, 1, n) {
    if (h[td(p[i].x + a, p[i].y + b)]) res--;
  }
  return res;
}

int main() {
  int ans = 0x3f3f3f3f;
  scanf("%d", &n);
  FOR(i, 1, n) p[i].read();
  if (n == 1) return puts("1"), 0;
  FOR(i, 1, n) h[td(p[i].x, p[i].y)]++;
  FOR(i, 1, n) FOR(j, 1, n) {
    if (i == j) continue;
    int a = p[i].x - p[j].x, b = p[i].y - p[j].y;
    ans = min(ans, go(a, b));
  }
  printf("%d", ans);
  return 0;
}
