#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
namespace IO {
  char nc() {
    static char bf[100000], *p1 = bf, *p2 = bf;
    return p1 == p2 && (p2 = (p1 = bf) + fread(bf, 1, 100000, stdin), p1 == p2)
               ? EOF
               : *p1++;
  }
  int rd() {
    int res = 0;
    char c = getchar();
    while (!isdigit(c)) c = getchar();
    while (isdigit(c)) res = res * 10 + c - '0', c = getchar();
    return res;
  }
} // namespace IO
/******************heading******************/
const int N = 3e5 + 5;

int n;
int a[N], b[N], c[N];

struct dis {
  int f[N];
  dis(int k) { FOR(i, 0, k) f[i] = i; }
  int get(int k) { return f[k] == k ? k : f[k] = get(f[k]); }
  void merge(int x, int y) { f[get(x)] = get(y); }
};

int main() {
  scanf("%d", &n);
  int nn = 1 << n;
  dis d(nn + 1);
  FOR(i, 1, nn) scanf("%d", &a[i]);
  sort(a + 1, a + nn + 1);
  FOR(i, 1, nn) {
    int nex = i;
    while (nex < nn && a[nex] == a[nex + 1]) a[nex] = a[i - 1] + 1, ++nex;
    a[nex] = a[i - 1] + 1, i = nex;
  }
  reverse(a + 1, a + nn + 1);
  b[1] = a[1], d.merge(1, 2); // merge 1 to 2
  FOR(i, 1, n) {
    // printf("i=%d\n",i);
    //找到最大的1<<i个数，和b[1..1<<i]检查
    int lim = 1 << (i - 1);
    FOR(j, 1, lim) {
      int cur = d.get(c[j] + 1); // 1 right max number's pos
      while (cur <= nn && a[cur] >= b[j]) { cur = d.get(cur + 1); }
      if (cur > nn) return puts("No"), 0;
      b[lim + j] = a[cur], d.merge(cur, cur + 1);
      c[lim + j] = cur;
    }
    // FOR(j,1,1<<i)printf("%d%c",b[j]," \n"[j==(1<<i)]);
  }
  puts("Yes");
  return 0;
}
/*

1 2 2 2 2 2 2 3 3 3 3 3 3 3 3 4 4 4 4 5 5 6 6 6 6 7 7 7 7 7 8 9

9 8
7 7

9 8 7 7
7 7 6 6

9 8 7 7 7 7 6 6
7 6 6 5 5 4 4 4

9 8 7 7 7 7 6 6 7 6 6 5 5 4 4 4
4 3 3 3 3 3 3 3 3 2 2 2 2 2 2 1
*/
