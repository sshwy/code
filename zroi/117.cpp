#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
typedef long long LL;
const int K = 104;

LL n;
int k;
LL ans;

map<LL, LL> mp[K];
LL dfs(LL n, int k) {
  if (mp[k].count(n)) return mp[k][n];
  if (k == 1) return mp[k][n] = n;
  LL res = dfs(n / 5, k - 1) + n % 5;
  res = min(res, dfs(n / 4, k - 1) + n % 4);
  res = min(res, dfs(n / 3, k - 1) + n % 3);
  res = min(res, dfs(n / 2, k - 1) + n % 2);
  return mp[k][n] = res;
}
int main() {
  scanf("%lld%d", &n, &k);
  ans = n;
  printf("%lld\n", dfs(n, k));
  return 0;
}
