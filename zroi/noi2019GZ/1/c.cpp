#include <algorithm>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
/******************heading******************/
const int N = 2e5 + 5, SZ = 1 << 24;
int n, q;

struct qxx {
  int nex, t;
};
qxx e[N * 2];
int h[N], le;
void add_path(int f, int t) { e[++le] = (qxx){h[f], t}, h[f] = le; }
int par[N], a[N], root; // root是全局最小值的树的根

int tot;
int lc[SZ], rc[SZ];
int cnt[SZ], rt[N];
void pushup(int u) { cnt[u] = cnt[lc[u]] + cnt[rc[u]]; }
void modify(int &u, int v, int c, lld L = -1e14, lld R = 1e14) { //把v修改c个
  // printf("modify(%d,%d,%d,%d,%d)\n",u,v,c,L,R);
  if (!u) u = ++tot; //没有就新建
  if (L == R) {
    cnt[u] += c;
    return;
  }
  lld mid = (L + R) >> 1;
  if (v <= mid)
    modify(lc[u], v, c, L, mid);
  else
    modify(rc[u], v, c, mid + 1, R);
  pushup(u);
}
lld query_max(int u, lld L = -1e14, lld R = 1e14) {
  if (!cnt[u]) {
    // printf("query_max(%d,%lld,%lld)\n",u,L,R);
    return -1e14 - 1; //没有最大值
  }
  if (L == R) return L;
  lld mid = (L + R) >> 1;
  if (cnt[rc[u]])
    return query_max(rc[u], mid + 1, R);
  else
    return query_max(lc[u], L, mid);
}
lld query_min(int u, lld L = -1e14, lld R = 1e14) {
  // printf("query_min(%d,%d,%d)\n",u,L,R);
  if (!cnt[u]) return 1e14 + 1; //没有最小值
  if (L == R) return L;
  lld mid = (L + R) >> 1;
  if (cnt[lc[u]])
    return query_min(lc[u], L, mid);
  else
    return query_min(rc[u], mid + 1, R);
}
//#include<cassert>
lld calc(int u) { //返回u的价值
  lld res = (cnt[rt[u]] ? query_max(rt[u]) : 0ll) + a[u];
  // assert(res!=-2891202220);
  return res;
}

void dfs_build(int u) {
  rt[u] = ++tot; //建一个空的树
  for (int i = h[u]; i; i = e[i].nex) {
    const int &v = e[i].t;
    dfs_build(v);
    modify(rt[u], calc(v), 1);
  }
}
void tree_modify(int u, int v) {
  for (int i = u; i; i = par[i]) {
    modify(root, calc(i), -1);
    // printf("i:%d,root delete %lld\n",i,calc(i));
  }
  for (int i = u; par[i]; i = par[i]) modify(rt[par[i]], calc(i), -1);
  //把路径的原来的全部减掉
  a[u] = v;
  for (int i = u; par[i]; i = par[i]) { modify(rt[par[i]], calc(i), 1); }
  for (int i = u; i; i = par[i]) {
    modify(root, calc(i), 1);
    // printf("root add %lld\n",calc(i));
  }
}

int main() {
  // freopen("ex_korone.in","r",stdin);
  root = ++tot;
  scanf("%d%d", &n, &q);
  FOR(i, 1, n) scanf("%d", &a[i]);
  FOR(i, 2, n) {
    scanf("%d", &par[i]);
    add_path(par[i], i);
  }
  dfs_build(1);
  FOR(i, 1, n) modify(root, calc(i), 1);
  FOR(i, 1, q) {
    int x, v;
    scanf("%d%d", &x, &v);
    tree_modify(x, v);
    // FOR(i,1,n)printf("%lld ",calc(i));
    // puts("");
    printf("%lld\n", query_min(root));
  }
  return 0;
}
