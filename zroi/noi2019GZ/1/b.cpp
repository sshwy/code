#include <algorithm>
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <iostream>/*{{{*/
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (register int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (register int i = (a); i >= (b); --i)
/******************heading******************/

char nc() {
  static char buf[100000], *p1 = buf, *p2 = buf;
  return p1 == p2 && (p2 = (p1 = buf) + fread(buf, 1, 100000, stdin), p1 == p2)
             ? EOF
             : *p1++;
}
int rd() {
  int res = 0;
  char c = nc();
  while (!isdigit(c)) c = nc();
  while (isdigit(c)) res = res * 10 + c - '0', c = nc();
  return res;
} /*}}}*/

const int N = 5e3 + 5, H = 2e3 + 5;
const double eps = 1e-6;
int T, n;
int h[N], d[N];
int g[N * H], s[N * H];
int pre[N], pr[N], suc[N], su[N];

int D, mx;
//最终的答案一定小于等于2000
void go() {
  D = 0, n = rd(), mx = 0;
  FOR(i, 1, n) h[i] = rd();
  FOR(i, 1, n) d[i] = rd(), D += d[i], mx = max(mx, h[i] + d[i]);

  int l = 1, r = n;
  while (!d[l]) ++l;
  while (!d[r]) --r;

  pr[0] = h[0] = h[n + 1] = su[n + 1] = 1e4;
  FOR(i, 1, n) pr[i] = i < l ? h[i] : min(pr[i - 1], h[i]);
  FOR(i, 1, n) pre[i] = i < l ? 0 : pre[i - 1] + h[i] - pr[i];
  ROF(i, n, 1) su[i] = i > r ? h[i] : min(su[i + 1], h[i]);
  ROF(i, n, 1) suc[i] = i > r ? 0 : suc[i + 1] + h[i] - su[i];

  int *gi = g + H, *gi1 = g;
  FOR(i, 1, n) {
    int *gj = gi, *gj1 = gi1;
    FOR(j, 0, h[i]) { *gj = *gj1 + 1, ++gj, ++gj1; }
    FOR(j, h[i] + 1, mx) { *gj = *gj1, ++gj, ++gj1; }
    gi += H, gi1 += H;
  }
  int *si = s + H, *si1 = s, *hi = h + 1;
  FOR(i, 1, n) {
    int *sj = si, *sj1 = si1;
    FOR(j, 0, *hi) { *sj = *sj1 + *hi, ++sj, ++sj1; }
    FOR(j, h[i] + 1, mx) { *sj = *sj1, ++sj, ++sj1; }
    si += H, si1 += H, ++hi;
  }
  FOR(i, 1, n) h[i] += h[i - 1];

  double ans = 1e9;
  hi = h;
  FOR(i, 1, n) {
    int x = mx;
    double cur = 1e9;
    int *sj = &s[i * H + x + 1];
    int *si = &s[(i - 1) * H + x + 1];
    int *gj = &g[i * H + x + 1];
    int *gi = &g[(i - 1) * H + x + 1];
    int *hj = hi + 1;
    FOR(j, i, n) {
      int a, b, dh = *hj - *hi, dij = j - i + 1;
      a = dh - (*sj - *si) + (*gj - *gi - dij) * x + D, b = dij - (*gj - *gi);
      while ((a < 0) ^ (b < 0)) {
        --x, --sj, --gj, --si, --gi;
        a = dh - (*sj - *si) + (*gj - *gi - dij) * x + D, b = dij - (*gj - *gi);
      }
      if (a < b) cur = x + a * 1.0 / b;
      double t = (*sj - *si) - (*gj - *gi) * cur;
      int mn = min(pr[i - 1], su[j + 1]);
      if (cur > mn) t += dij * (cur - mn);
      ans = min(ans, t + pre[i - 1] + suc[j + 1]);
      sj += H, gj += H, ++hj;
    }
    ++hi;
  }
  printf("%.5lf\n", ans);
}
int main() {
  // freopen("ex_aqua.in","r",stdin);
  T = rd();
  while (T--) go();
  return 0;
}
