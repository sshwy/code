#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i) /*}}}*/
/******************heading******************/

int f[100][100];

int main() {
  FOR(i, 1, 99) FOR(j, 1, 99) f[i][j] = i * j;
  // printf("%d\n%d\n%d\n%d",**f,*(*(f+1)+1),**(f+101),*(f+101));
  FOR(i, 0, 99) printf("%d ", *(*(f + i) + 1));
  return 0;
}
