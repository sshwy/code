#include <algorithm>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
/******************heading******************/
const int N = 52;
const lld INF = 1ll << 60;
int n, k, T;
int a[N][N];
lld f[N][N], s1[N][N], s2[N][N], m1[N][N], m2[N][N];

void solve2() {
  memset(f, 0, sizeof(f));
  memset(s1, 0, sizeof(s1));
  memset(s2, 0, sizeof(s2));
  memset(m1, -0x3f, sizeof(m1));
  memset(m2, -0x3f, sizeof(m2));

  FOR(i, 1, n) FOR(j, 1, n) {
    s1[i][j] = s1[i][j - 1] + a[i][j], m1[i][j] = max(m1[i][j - 1], s1[i][j]);
    s2[j][i] = s2[j][i - 1] + a[i][j], m2[j][i] = max(m2[j][i - 1], s2[j][i]);
  }
  FOR(i, 1, n) FOR(j, 1, n) {
    if (i == 1)
      f[i][j] = s1[1][j];
    else if (j == 1)
      f[i][j] = s2[1][i];
    else
      f[i][j] = max(f[i - 1][j] + m1[i][j], f[i][j - 1] + m2[j][i]);
  }
  lld ans = 0;
  FOR(i, 1, n) {
    lld s = f[i][1];
    FOR(j, 1, n) s = max(s, f[i][j]);
    lld t = 0;
    FOR(j, i + 1, n) t += m1[j][n], ans = max(ans, s + t);
  }
  FOR(i, 1, n) {
    lld s = f[1][i];
    FOR(j, 1, n) s = max(s, f[j][i]);
    lld t = 0;
    FOR(j, i + 1, n) t += m2[j][n], ans = max(ans, s + t);
  }
  printf("%lld\n", ans);
}
/*
 * s1表示横着的最大值
 * s2表示竖着的最大值
 */
void solve1() {
  lld t1 = 0, t2 = 0, s = 0;
  FOR(i, 1, n) s += a[1][i], t1 = max(t1, s);
  s = 0;
  FOR(i, 1, n) s += a[i][1], t2 = max(t2, s);
  printf("%lld\n", t1 + t2);
}
void go() {
  memset(a, 0, sizeof(a));
  scanf("%d%d", &n, &k);
  FOR(i, 2, n) FOR(j, 1, i) scanf("%d", &a[i - j + 1][j]); //转45度
  if (k == 1)
    solve1();
  else
    solve2();
}
int main() {
  scanf("%d", &T);
  while (T--) go();
  return 0;
}
