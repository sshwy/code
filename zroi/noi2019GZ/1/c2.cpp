#include <algorithm>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
/******************heading******************/

int main() {
  // freopen("ex_korone.in","r",stdin);
  root = ++tot;
  scanf("%d%d", &n, &q);
  FOR(i, 1, n) scanf("%d", &a[i]);
  FOR(i, 2, n) {
    scanf("%d", &par[i]);
    add_path(par[i], i);
  }
  dfs_build(1);
  FOR(i, 1, n) modify(root, calc(i), 1);
  FOR(i, 1, q) {
    int x, v;
    scanf("%d%d", &x, &v);
    tree_modify(x, v);
    // FOR(i,1,n)printf("%lld ",calc(i));
    // puts("");
    printf("%lld\n", query_min(root));
  }
  return 0;
}
