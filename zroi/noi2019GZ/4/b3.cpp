#include <algorithm>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
/******************heading******************/
const int N = 2e5 + 5;
const lld INF = 1ll << 50;
int W, H;
vector<int> r[N], c[N];

lld f[N], g[N];
lld calc(int st) {
  f[st] = 0;
  ROF(i, st - 1, 0) f[i] = f[i + 1] + c[i][0];
  FOR(i, st + 1, W - 1) f[i] = f[i - 1] + c[i - 1][0];
  FOR(i, 0, H - 2) { //层转移
    FOR(j, 0, W - 1) f[j] += r[j][i], g[j] = INF;
    g[0] = f[0];
    FOR(j, 1, W - 1) {
      g[j] = g[j - 1] + c[j - 1][i + 1];
      g[j] = min(g[j], f[j]);
    }
    ROF(j, W - 2, 0) { g[j] = min(g[j], g[j + 1] + c[j][i + 1]); }
    swap(g, f);
  }
  lld ans = 0;
  FOR(i, 0, W - 1) ans += f[i];
  return ans;
}

int main() {
  scanf("%d%d", &W, &H);
  FOR(i, 0, W - 1) {
    FOR(j, 0, H - 2) {
      int x;
      scanf("%d", &x);
      r[i].pb(x);
    }
  }
  FOR(i, 0, W - 2) {
    FOR(j, 0, H - 1) {
      int x;
      scanf("%d", &x);
      c[i].pb(x);
    }
  }                  //往下走
  FOR(i, 0, W - 1) { // start at (i,0)
    printf("%lld\n", calc(i));
  }
  return 0;
}
