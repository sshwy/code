#include <algorithm>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
/******************heading******************/
const int N = 23;

lld f[1 << 23];
int n, m;
int s[1 << 23], ls; //合法的状态
int _(int u, int v) { return (1 << (u - 1)) | (1 << (v - 1)); }

void DP() {
  int S = 1 << n;
  FOR(i, 0, S - 1) {
    if (!f[i]) continue;
    FOR(j, 1, ls) {
      if (s[j] > i && (i | s[j]) == (i ^ s[j])) f[i ^ s[j]] += f[i];
    }
  }
}

int main() {
  scanf("%d %d", &n, &m);
  FOR(i, 1, m) {
    int u, v;
    scanf("%d%d", &u, &v);
    f[s[++ls] = _(u, v)] = 1;
  }
  DP();
  printf("%lld\n", f[(1 << n) - 1]);
  return 0;
}
