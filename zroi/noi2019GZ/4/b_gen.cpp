#include <algorithm>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
/******************heading******************/

int r(int p) { return rand() % p; }
int r(int L, int R) { return r(R - L + 1) + L; }

int main() {
  // freopen("/tmp/in.txt","w",stdout);
  srand(time(0));
  // int W=100,H=200;
  int W = r(10, 30), H = r(30, 500) / W;
  printf("%d %d\n", W, H);
  puts("");
  FOR(i, 1, W) {
    FOR(j, 1, H - 1) printf("%d ", r(1, 1000000));
    puts("");
  }
  puts("");
  FOR(i, 1, W - 1) {
    FOR(j, 1, H) printf("%d ", r(1, 1000000));
    puts("");
  }
  return 0;
}
