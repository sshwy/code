#include <algorithm>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
/******************heading******************/
const int N = 2e5 + 5;
int W, H;
vector<int> r[N], c[N];
// lld f[505][2005];

struct qxx {
  int nex, t, v;
};
qxx e[N];
int h[N], le;
void add_path(int f, int t, int v) { e[++le] = (qxx){h[f], t, v}, h[f] = le; }

int _(int x, int y) { return x * H + y; }

lld d[N];
priority_queue<pii> q;
lld dijk(int s, int t) {
  memset(d, 0x3f, sizeof(d));
  d[s] = 0;
  q.push(mk(d[s], s));
  while (!q.empty()) {
    pii u = q.top();
    q.pop();
    if (d[u.se] < u.fi) continue;
    for (int i = h[u.se]; i; i = e[i].nex) {
      const int v = e[i].t, w = e[i].v;
      if (d[u.se] + w >= d[v]) continue;
      d[v] = d[u.se] + w, q.push(mk(d[v], v));
    }
  }
  return d[t];
}

int main() {
  scanf("%d%d", &W, &H);
  FOR(i, 0, W - 1) {
    FOR(j, 0, H - 2) {
      int x;
      scanf("%d", &x);
      r[i].pb(x);
      add_path(_(i, j), _(i, j + 1), x);
    }
  }
  FOR(i, 0, W - 2) {
    FOR(j, 0, H - 1) {
      int x;
      scanf("%d", &x);
      c[i].pb(x);
      add_path(_(i, j), _(i + 1, j), x);
      add_path(_(i + 1, j), _(i, j), x);
    }
  } //往下走
  FOR(i, 0, W - 1) {
    lld ans = 0;
    FOR(j, 0, W - 1) { ans += dijk(_(i, 0), _(j, H - 1)); }
    printf("%lld\n", ans);
  }
  return 0;
}
