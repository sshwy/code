#include <algorithm>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
/******************heading******************/
const int N = 2e5 + 5, N2 = 5e3 + 5;
const lld INF = 1ll << 40;
int n;
lld a[N], f[N2][N2];

int main() {
  scanf("%d", &n);
  FOR(i, 1, n) scanf("%lld", &a[i]);
  FOR(i, 1, n) a[i] += a[i - 1];
  ROF(len, n - 1, 0) {
    FOR(i, 1, n - len) {
      int j = i + len;
      if (i == 1 && j == n) continue;
      f[i][j] = INF;
      if (i > 1) f[i][j] = min(f[i][j], f[i - 1][j] + a[j] - a[i - 2]);
      if (j < n) f[i][j] = min(f[i][j], f[i][j + 1] + a[j + 1] - a[i - 1]);
    }
  }
  FOR(i, 1, n) printf("%lld\n", f[i][i]);
  return 0;
}
