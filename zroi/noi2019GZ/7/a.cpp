#include <algorithm>
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
/******************heading******************/
const int N = 50005;
int n, k;
int a[N], ls;

pii ans[N];
bool dfs(int _k) {
  bool res = 0;
  FOR(i, 1, n) {
    FOR(j, 1, min(a[i], _k)) {
      a[i] -= j;
      res |= !dfs(j);
      a[i] += j;
    }
  }
  // printf("k:%d,res:%d ,a: ",_k,res); FOR(i,1,n)printf("%d ",a[i]); puts("");
  return res;
}
bool go() {
  ls = 0;
  FOR(i, 1, n) {
    FOR(j, 1, min(a[i], k)) {
      a[i] -= j;
      if (!dfs(j)) ans[++ls] = mk(i, j);
      a[i] += j;
    }
  }
  // if(ls)puts("1");
  // else puts("0");
  FOR(i, 1, ls) printf("<%d,%d> ", ans[i].fi, ans[i].se);
  return ls;
}

int main() {
  // scanf("%d%d",&n,&k);
  // FOR(i,1,n)scanf("%d",&a[i]);
  n = 4;
  for (int i = 1; i <= 5; i++) {
    FOR(j, i, 5) {
      FOR(p, j, 5) {
        FOR(q, p, 5) {
          a[1] = i, a[2] = j, a[3] = p, a[4] = q; //,k=j;
          k = max(max(i, q), max(j, p));
          // if(go())printf("1");
          printf("(%d,%d,%d,%d->%d).", i, j, p, q, i ^ j ^ p ^ q);
          go();
          puts("");
          // else printf("0");
        }
      }
      puts("");
    }
  }
  return 0;
}
