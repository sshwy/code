#include <algorithm>
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
/******************heading******************/

int main() {
  system("g++ a_gen.cpp -O2 -lm -o .gen");
  system("g++ a6.cpp -O2 -lm -o .std");
  system("g++ a7.cpp -O2 -lm -o .usr");
  FOR(i, 1, 10000) {
    system("./.gen > .fin");
    system("./.usr < .fin > .fout");
    system("./.std < .fin > .fstd");
    if (system("diff .fout .fstd"))
      break;
    else
      printf("AC #%d\n", i);
  }
  printf("WA or Fin");
  return 0;
}
