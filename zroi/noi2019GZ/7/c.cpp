#include <algorithm>
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (int)(b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (int)(b); --i)
/******************heading******************/
const int N = 2e5 + 5;
int n, q;
int p[N];
lld a[N];
vector<pii> s[N];

namespace S1 {
  bool check(int l, int r) {
    bool vis[N] = {0};
    int x = 0;
    FOR(i, l, r) vis[p[i]] = 1;
    FOR(i, l, r) x += (!vis[p[i] - 1]) + (!vis[p[i] + 1]);
    return x == 2;
  }
  void go() {
    FOR(i, 1, n) a[i] += a[i - 1];
    FOR(i, 1, n) {
      FOR(j, i, n) {
        if (check(i, j)) {
          pii p = mk(i, j);
          FOR(k, i, j) s[k].pb(p);
        }
      }
    }
    scanf("%d", &q);
    FOR(i, 1, q) {
      int op, k, v;
      scanf("%d%d", &op, &k);
      lld ans = 0x3f3f3f3f;
      if (op == 1) {
        FOR(j, 0, s[k].size() - 1) {
          ans = min(ans, a[s[k][j].se] - a[s[k][j].fi - 1]);
        }
        printf("%lld\n", ans);
      } else {
        scanf("%d", &v);
        v = v - a[k] + a[k - 1];
        FOR(i, k, n) a[i] += v;
      }
    }
  }
} // namespace S1
namespace S2 {
  void go() {
    scanf("%d", &q);
    FOR(i, 1, q) {
      int op, k, v;
      scanf("%d%d", &op, &k);
      if (op == 1) {
        printf("%lld\n", a[k]);
      } else {
        scanf("%d", &v);
        a[k] = v;
      }
    }
  }
} // namespace S2

int main() {
  bool fl = 1;
  scanf("%d", &n);
  FOR(i, 1, n) scanf("%d", &p[i]);
  FOR(i, 1, n) scanf("%lld", &a[i]), fl &= (a[i] >= 0);
  if (fl)
    S2::go();
  else
    S1::go();
  return 0;
}
