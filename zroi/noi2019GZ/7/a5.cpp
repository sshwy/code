#include <algorithm>
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
/******************heading******************/
const int N = 50005;
int n, k;
int a[N], ls;

pii ans[N];
bool dfs(int _k) {
  bool res = 0;
  int s = 0;
  FOR(i, 1, n) s ^= a[i];
  // printf("s:%d\n",s);
  FOR(i, 1, n) {
    for (int j = 1; j <= s && j <= _k; j <<= 1) {
      if (!(j & s)) continue;
      int t = s & ((j << 1) - 1);
      if (t > a[i]) break;
      // printf("t:%d\n",t);
      a[i] -= t;
      res |= !dfs(j);
      a[i] += t;
    }
  }
  // FOR(i,1,n)printf("%d ",a[i]); printf(":%d\n",res);
  // printf("k:%d,res:%d ,a: ",_k,res); FOR(i,1,n)printf("%d ",a[i]); puts("");
  return res;
}

int main() {
  scanf("%d%d", &n, &k);
  FOR(i, 1, n) scanf("%d", &a[i]);
  FOR(i, 1, n) {
    FOR(j, 1, min(a[i], k)) {
      a[i] -= j;
      // printf("\t\t"); FOR(i,1,n)printf("%d ",a[i]); puts("");
      if (!dfs(j)) {
        ans[++ls] = mk(i, j);
        // printf("\033[32mWin!\033[0m\t");
        // FOR(i,1,n)printf("%-2d ",a[i]); printf("lim:%-2d  ",j);
        int s = 0;
        FOR(i, 1, n) s ^= a[i];
        assert(s == 0 || (s & -s) > j);
        // printf("\033[32mxor:%-2d\033[0m, cmp:%d\n",s,s==0||(s&-s)>j);
      } else {
        // printf("\033[31mFail!\033[0m\t");
        // FOR(i,1,n)printf("%-2d ",a[i]); printf("lim:%-2d  ",j);
        int s = 0;
        FOR(i, 1, n) s ^= a[i];
        assert((s & -s) <= j);
        // printf("\033[31mxor:%-2d\033[0m, cmp:%d\n",s,(s&-s)<=j);
      }
      a[i] += j;
    }
  }
  if (ls)
    puts("1");
  else
    return puts("0"), 0;
  FOR(i, 1, ls) printf("%d %d\n", ans[i].fi, ans[i].se);
  return 0;
}
