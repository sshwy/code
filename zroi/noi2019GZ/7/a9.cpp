#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i) /*}}}*/
/******************heading******************/
const int N = 5e4 + 5;
int n, sx, k, a[N];

int check(int _k) {
  int x = 0;
  while ((1 << x) <= _k) ++x;
  return (sx & ((1 << x) - 1)) > 0 ? 1 : 0; //如果后x位不为0，则有必胜策略
}

int main() {
  scanf("%d%d", &n, &k);
  FOR(i, 1, n) scanf("%d", &a[i]), sx ^= a[i]; //异或和
  int ans = check(k);
  printf("%d\n", ans);
  if (!ans) return 0;
  FOR(i, 1, n) {
    sx ^= a[i];
    for (int j = 1; (1 << (j - 1)) <= min(a[i], k); j++) {
      int now = ((a[i] + (1 << j) - sx) & ((1 << j) - 1));
      if ((1 << (j - 1)) <= now && now <= min(a[i], k)) { printf("%d %d\n", i, now); }
    }
    sx ^= a[i];
  }
  return 0;
}
