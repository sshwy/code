#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i) /*}}}*/
/******************heading******************/
const int N = 50005;
int n, k, sx;
int a[N], ls;

pii ans[N];

int calc(int sx, int x, int j) {
  int m2 = 1;
  while (m2 <= j) m2 <<= 1;
  // m2>>=1;
  int low = sx & (m2 - 1);
  x -= low;
  return x / m2;
}

int main() {
  scanf("%d%d", &n, &k);
  FOR(i, 1, n) scanf("%d", &a[i]), sx ^= a[i];
  if (sx == 0) return puts("0"), 0;
  // printf("sx:%d\n",sx);
  FOR(i, 1, n) {
    sx ^= a[i];
    int lim = min(a[i], k), t1, cnt;
    if (a[i] - lim <= sx && sx <= a[i] - 1) {
      t1 = a[i] - 1 - sx + 1, cnt = 0;
      t1 /= 2;
      while ((1 << cnt) <= t1) ++cnt;
      --cnt;
    }
    FOR(j, 1, lim) {
      printf("[%d,%d]\n", a[i] - lim, a[i] - 1);
      printf("sx:%d, x:%d\t", sx, a[i] - j);
      sx ^= a[i] - j;
      if (sx == 0 || (sx & -sx) > j) ans[++ls] = mk(i, j);
      printf("sx:%-2d, %-2d, %-2d\n", sx, sx & -sx, j);
      sx ^= a[i] - j;
    }
    puts("");
    sx ^= a[i];
  }
  if (ls)
    puts("1");
  else
    return puts("0"), 0;
  FOR(i, 1, ls) printf("%d %d\n", ans[i].fi, ans[i].se);
  return 0;
}
