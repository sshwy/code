#include <algorithm>
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
/******************heading******************/
const int N = 50005;
int n, k;
int a[N];
int s, la;
pii ans[30 * 50000];
int main() {
  scanf("%d%d", &n, &k);
  FOR(i, 1, n) scanf("%d", &a[i]), s ^= a[i];
  if (!s) return puts("0"), 0;
  FOR(i, 1, n) {
    for (int j = 1; j <= s; j <<= 1) {
      if (!(j & s)) continue;
      int t = s & ((j << 1) - 1);
      if (t > a[i] || t > k) break;
      ans[++la] = mk(i, t);
    }
  }
  if (!la) return puts("0"), 0;
  puts("1");
  FOR(i, 1, la) printf("%d %d\n", ans[i].fi, ans[i].se);
  return 0;
}
