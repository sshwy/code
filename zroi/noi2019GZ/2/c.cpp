#include <algorithm>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
/******************heading******************/
const int N = 1e4 + 5, P = 323232323;
int n, root;
int a[N], r[N]; // r[i]表示i限制到的位置

vector<int> f[N]; //每个结点上吊一个vector
int t[N];

void make_suc(int u) { //对f[u]做后缀和并向后平移一位
  int s = f[u].size();
  t[0] = 0, t[s] = f[u][s - 1];
  ROF(i, s - 1, 1) t[i] = (t[i + 1] + f[u][i - 1]) % P;
  f[u].clear();
  FOR(i, 0, s) f[u].pb(t[i]);
}
void trans(int u, int v) { //把v挂到u上的转移。即直接做卷积。保证u是做过make_suc
  int s = f[u].size() + f[v].size() - 2; //接上后的最大的右链长度
  memset(t, 0, (s + 5) * sizeof(int));
  FOR(i, 0, f[u].size() - 1) FOR(j, 0, f[v].size() - 1) {
    (t[i + j] += 1ll * f[u][i] * f[v][j] % P) %= P;
  }
  f[u].clear();
  FOR(i, 0, s) f[u].pb(t[i]);
}
int main() {
  root = n + 1;
  scanf("%d", &n);
  FOR(i, 1, n) scanf("%d", &a[i]);
  a[1] = n;
  ROF(i, n, 1) {
    r[i] = i + a[i] - 1;
    FOR(j, 1, a[i] - 1) r[i] = max(r[i], r[i + j]);
  }
  ROF(i, n, 1) {
    f[i].resize(1), f[i][0] = 1; //初始时把它当作叶结点，叶结点没有右链
    for (int j = i + 1; j <= r[i]; j = r[j] + 1)
      make_suc(i), trans(i, j), f[j].clear();
    //枚举j是i的子节点
  }
  make_suc(1);
  printf("%d\n", f[1][1]);
  return 0;
}
