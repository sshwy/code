#include <algorithm>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
/******************heading******************/
const int N = 5e3 + 5, P = 323232323;
int n;
int a[N];
int f[N][N], g[N][N];
#include <cassert>
int main() {
  scanf("%d", &n);
  FOR(i, 1, n) {
    scanf("%d", &a[i]);
    if (a[i] == 1) f[i][i] = g[i][i] = 1;
  }
  ROF(i, n, 1) FOR(j, i + 1, n) {
    if (j - i + 1 < a[i]) continue;
    g[i][j] = f[i + 1][j];
    FOR(p, i, j - 1) f[i][j] = (f[i][j] + 1ll * g[i][p] * f[p + 1][j]) % P;
    f[i][j] += g[i][j];
    f[i][j] %= P;
  }
  printf("%d\n", g[1][n]);
  return 0;
}
