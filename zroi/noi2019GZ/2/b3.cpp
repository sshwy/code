#include <algorithm>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
/******************heading******************/
const int N = 5e5 + 5;
int n;
bool vis[N];
pii a[N];

int main() {
  scanf("%d", &n);
  FOR(i, 1, n) scanf("%d%d", &a[i].fi, &a[i].se), a[i].fi <<= 1, a[i].se <<= 1;
  sort(a + 1, a + n + 1);
  FOR(i, 2, n) {
    if (a[i - 1].se >= a[i].se) vis[i - 1] = 1;
  }
  int last = 0, ans = 0;
  FOR(i, 1, n) {
    if (vis[i]) continue;
    if (a[i].fi > last) ++ans, last = a[i].se;
  }
  int ans2 = 0x3f3f3f3f;
  if (n <= 10) {
    int s = 1 << (n << 1);
    FOR(i, 0, s - 1) {
      int sum = 0, c = 0;
      FOR(k, 1, n * 2) if ((i >> (k - 1)) & 1) c++;
      if (c > ans) continue;
      FOR(j, 1, n) {
        int mid = (a[j].fi + a[j].se) / 2, x = n;
        FOR(k, 1, n * 2) {
          if (a[j].fi <= k && k <= a[j].se && (i >> (k - 1)) & 1)
            x = min(x, abs(mid - k));
        }
        sum += x;
      }
      ans2 = min(ans, sum);
    }
  }
  printf("%d %d", ans, ans2);
  return 0;
}
