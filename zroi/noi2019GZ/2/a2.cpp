#include <algorithm>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
/******************heading******************/
const int W = 1e5 + 5, P = 323232323;
int w, h, n;
int gcd(int a, int b) { return b ? gcd(b, a % b) : a; }

int main() {
  scanf("%d%d%d", &w, &h, &n);
  --w, --h, --n;
  int ans = 0;
  if (n == 0) return printf("%d\n", 1ll * w * h % P), 0;
  FOR(i, 1, w / n)
  FOR(j, 1, h / n)
  if (gcd(i, j) == 1) ans = (ans + (w - i * n + 1ll) * (h - j * n + 1)) % P;
  ans = (ans * 2ll + (w - n + 1) * (h + 1ll) + (h - n + 1ll) * (w + 1)) % P;
  printf("%d\n", ans);
  return 0;
}
