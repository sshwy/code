#include <algorithm>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
/******************heading******************/
const int N = 2002, INF = 0x3f3f3f3f;
int n;
pii a[N];

struct qxx {
  int nex, t, v, c;
};
qxx e[N * 4];
int h[N], le = 1;
void add_path(int f, int t, int v, int c) {
  e[++le] = (qxx){h[f], t, v, c}, h[f] = le;
}
void add_flow(int f, int t, int v, int c) {
  printf("add_flow(%d,%d,%d,%d)\n", f, t, v, c);
  add_path(f, t, v, c), add_path(t, f, 0, -c);
}

int d[N];
int s, s1, t;
bool spfa() {
  memset(d, 0x3f, sizeof(d));
  bool vising[N] = {0};
  queue<int> q;
  d[s] = 0, q.push(s);
  while (!q.empty()) {
    int u = q.front();
    q.pop();
    vising[u] = 0;
    for (int i = h[u]; i; i = e[i].nex) {
      const int &v = e[i].t, &w = e[i].v, &c = e[i].c;
      if (!w || d[v] <= d[u] + c) continue;
      d[v] = d[u] + c;
      if (!vising[v]) q.push(v), vising[v] = 1;
    }
  }
  return d[t] != INF;
}
bool vis[N];
int maxflow, mincost;
int dfs(int u, int flow) {
  vis[u] = 1;
  if (u == t) return flow;
  int rest = flow;
  for (int i = h[u]; i; i = e[i].nex) {
    const int &v = e[i].t, &w = e[i].v, &c = e[i].c;
    if (vis[v] == 1 && v != t || !w || d[v] != d[u] + c) continue;
    int k = dfs(v, min(w, rest));
    if (k)
      mincost += k * c, e[i].v -= k, e[i ^ 1].v += k, rest -= k;
    else
      d[v] = 0;
  }
  return flow - rest;
}

int main() {
  scanf("%d", &n);
  FOR(i, 1, n) scanf("%d%d", &a[i].fi, &a[i].se), a[i].fi <<= 1, a[i].se <<= 1;
  sort(a + 1, a + n + 1);
  FOR(i, 2, n) {
    if (a[i - 1].se >= a[i].se) vis[i - 1] = 1;
  }
  int last = 0, ans1 = 0;
  FOR(i, 1, n) {
    if (vis[i]) continue;
    if (a[i].fi > last) ++ans1, last = a[i].se;
  }
  printf("ans1:%d\n", ans1);
  //[1,n*2]
  // s=0,s1=n*3+1,t=n*3+2;
  s = 0, s1 = 1000 + 1, t = 1000 + 2;
  FOR(i, 1, n) {
    printf("[%d,%d]\n", a[i].fi, a[i].se);
    int mid = (a[i].fi + a[i].se) / 2;
    FOR(j, a[i].fi, a[i].se) { add_flow(i, j + n, 1, abs(mid - j)); }
  }
  FOR(i, 1, n) add_flow(s1, i, 1, 0);
  // FOR(i,1,n*2)add_flow(i+n,t,INF,0);
  FOR(i, 1, 100) add_flow(i + n, t, INF, 0);
  // add_flow(s,s1,ans1,0);
  add_flow(s, s1, n, 0);
  printf("n:%d\n", n);
  while (spfa()) {
    vis[t] = 1;
    while (vis[t]) {
      memset(vis, 0, sizeof(vis));
      maxflow += dfs(s, INF);
      printf("maxflow:%d\n", maxflow);
    }
  }
  printf("%d %d\n", maxflow, mincost);
  FOR(i, 2, le) {
    if (!e[i].v && (i % 2 == 0))
      printf("%d -> %d (%d,%d)\n", e[i ^ 1].t, e[i].t, e[i ^ 1].v, e[i].c);
  }
  return 0;
}
