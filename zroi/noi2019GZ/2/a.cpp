#include <algorithm>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
/******************heading******************/
const lld W = 1e5 + 5, P = 323232323, I2 = 161616162;
lld w, h, n;
lld gcd(lld a, lld b) { return b ? gcd(b, a % b) : a; }

bool bp[W];
lld pn[W], lp, mu[W];
void sieve(lld k) {
  bp[0] = bp[1] = 1, mu[1] = 1;
  FOR(i, 2, k) {
    if (!bp[i]) pn[++lp] = i, mu[i] = -1;
    FOR(j, 1, lp) {
      if (i * pn[j] > k) break;
      bp[i * pn[j]] = 1;
      if (i % pn[j] == 0) {
        mu[i * pn[j]] = 0;
        break;
      } else
        mu[i * pn[j]] = -mu[i];
    }
  }
}
lld fac[W], inv[W];
lld pw(lld a, lld m) {
  lld res = 1;
  while (m) m & 1 ? res = res * a % P : 0, a = a * a % P, m >>= 1;
  return res;
}
lld binom(lld n, lld m) {
  if (n < m) return 0;
  return fac[n] * inv[m] % P * inv[n - m] % P;
}
lld calc(lld k) {
  lld res = 0, w1 = (w - 1) / k, h1 = (h - 1) / k;
  lld x = (w1 + 1) * w1 % P * I2 % P, y = (h1 + 1) * h1 % P * I2 % P;
  res += w1 * h1 % P * w % P * h % P;
  res -= w * k % P * y % P * w1 % P;
  res -= h * k % P * x % P * h1 % P;
  res += x * y % P * k % P * k % P;
  res = (res % P + P) % P;
  return res;
}
lld go() {
  lld res = 0;
  FOR(d, 1, w - 1) {
    lld c = binom(d - 1, n - 2), s = 0;
    FOR(x, 1, (w - 1) / d) s += calc(d * x) * mu[x] % P;
    res += s % P * c % P;
  }
  return (res % P + P) % P;
}
void pre_work() {
  fac[0] = 1;
  FOR(i, 1, w) fac[i] = fac[i - 1] * i % P;
  inv[w] = pw(fac[w], P - 2);
  ROF(i, w, 1) inv[i - 1] = inv[i] * i % P;
}
signed main() {
  scanf("%lld%lld%lld", &w, &h, &n);
  if (n == 1) return printf("%lld\n", 1ll * w * h % P), 0;
  pre_work();
  sieve(w - 1);
  lld ans = go();
  printf("%lld\n", (ans * 2 % P + binom(w, n) * h % P + binom(h, n) * w % P) % P);
  return 0;
}
