#include <algorithm>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
/******************heading******************/
const int N = 6e5, Q = 6e5 + 5, P = 998244353;
int n, q;

int f[N];
int gf(int u) { return f[u] == u ? u : f[u] = gf(f[u]); }
void merge(int u, int v) { f[gf(u)] = gf(v); }
bool find(int u, int v) { return gf(u) == gf(v); }
void init(int L, int R) { FOR(i, L, R) f[i] = i; }

struct qxx {
  int nex, t;
};
qxx e[Q * 2];
int h[N], le;
void add_path(int f, int t) { e[++le] = (qxx){h[f], t}, h[f] = le; }

int p[N], ans;

void sol(int l, int r) {
  if (r - l == 1) return;
  int t = q;
  FOR(i, l + 1, r) f[i] = i;
  FOR(i, l + 1, r - 1) {
    for (int j = h[i]; j; j = e[j].nex) {
      int v = e[j].t;
      if (v <= l || v >= r) v = r;
      if (i > v) continue;
      merge(i, v);
    }
  }
  FOR(i, l + 1, r) if (f[i] != i)-- t;
  ans = (ans + p[t]) % P;
  int mid = (l + r) >> 1;
  sol(l, mid), sol(mid, r);
}

int main() {
  scanf("%d%d", &n, &q);
  FOR(_, 1, q) {
    int x, y;
    scanf("%d%d", &x, &y), ++y;
    add_path(x, y), add_path(y, x);
  }
  p[0] = 1;
  FOR(i, 1, q) p[i] = p[i - 1] * 2, p[i] >= P ? p[i] -= P : 0;
  sol(0, n);
  ans = (P - ans + 1ll * (n - 1) * p[q]) % P;
  printf("%d\n", ans);
  return 0;
}
