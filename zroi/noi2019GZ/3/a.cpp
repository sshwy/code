#include <algorithm>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
/******************heading******************/
const int N = 1e4 + 4, M = 3e5 + 5, INF = 0x3f3f3f3f;
int n, m, q;

struct qxx {
  int nex, t;
};
qxx e[M * 2];
int h[N], le;
void add_path(int f, int t) { e[++le] = (qxx){h[f], t}, h[f] = le; }

int mark[N], d[N], a[N];

int k;

int bfs(int s) {
  queue<int> q;
  bool vis[N] = {0};
  q.push(s), vis[s] = 1, d[s] = 0;
  int res = 1;
  while (!q.empty()) {
    int u = q.front();
    q.pop();
    for (int i = h[u]; i; i = e[i].nex) {
      const int v = e[i].t;
      if (mark[v] && !vis[v]) d[v] = d[u] + 1, q.push(v), vis[v] = 1, res++;
    }
  }
  return res;
}
void go() {
  scanf("%d", &k);
  memset(mark, 0, sizeof(mark));
  FOR(i, 1, k) {
    scanf("%d", &a[i]);
    mark[a[i]] = 1;
  }
  int res = bfs(a[1]); // BFS判联通
  if (res < k) {
    puts("-1");
    return;
  }
  int tot = 0;
  FOR(i, 1, k) {
    int u = a[i];
    for (int j = h[u]; j; j = e[j].nex) {
      const int v = e[j].t;
      if (mark[v]) tot++;
    }
  }
  if (tot == k * (k - 1ll))
    puts("1"); // tot把每个边都算了两次的
  else
    puts("2");
}
int main() {
  scanf("%d%d", &n, &m);
  FOR(i, 1, m) {
    int u, v;
    scanf("%d%d", &u, &v);
    add_path(u, v), add_path(v, u);
  }
  scanf("%d", &q);
  FOR(i, 1, q) { go(); }
  return 0;
}
