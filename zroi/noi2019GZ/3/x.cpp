#include <algorithm>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
/******************heading******************/

const int N = 1000;
int F[N];
int f(int x) {
  if (x == 1) return 1;
  if (F[x]) return F[x];
  if (x % 2 == 0)
    return f(x / 2);
  else
    return f(x / 2) + f(x / 2 + 1);
}

int main() {

  freopen("tmp.txt", "w", stdout);
  FOR(i, 1, 1000) { printf("%3d:%-3d ", i, f(i)); }
  return 0;
}
