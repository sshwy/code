#include <algorithm>
#include <bitset>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
/******************heading******************/

int r(int p) { return rand() % p; }
int r(int L, int R) { return r(R - L + 1) + L; }

const int SZ = 1200;
struct graph {
  int n;
  bool e[SZ][SZ];
  void make(int k) {
    n = k;
    FOR(i, 1, k) FOR(j, i + 1, k) e[i][j] = 1;
  }
  void make(int l, int r) { //结点l到r的完全图
    n = r;
    FOR(i, 1, l - 1) FOR(j, i + 1, l - 1) e[i][j] = 0;
    FOR(i, l, r) FOR(j, i + 1, r) e[i][j] = 1;
  }
  void print() {
    int m = 0;
    FOR(i, 1, n) FOR(j, i + 1, n) m += e[i][j];
    printf("%d %d\n", n, m);
    FOR(i, 1, n) FOR(j, i + 1, n) if (e[i][j]) printf("%d %d\n", i, j);
  }
  graph operator+(graph g) {
    graph s = *this;
    s.n = max(n, g.n);
    FOR(i, 1, s.n) FOR(j, i + 1, s.n) s.e[i][j] = s.e[i][j] | g.e[i][j];
    return s;
  }
  graph operator-(graph g) {
    graph s = *this;
    s.n = max(n, g.n);
    FOR(i, 1, s.n) FOR(j, i + 1, s.n) s.e[i][j] = s.e[i][j] ^ g.e[i][j];
    return s;
  }
  graph() { memset(e, 0, sizeof(e)); }
};

int main() {
  srand(time(0));
  int a[SZ], la = r(3, 20);
  FOR(i, 1, la) { a[i] = r(a[i - 1] + 1, a[i - 1] + 100); }
  bool fl = 1;
  graph g;
  for (int i = 1, j = la; i < j; i++, j--) {
    graph x;
    x.make(a[i], a[j]);
    if (fl)
      g = g + x;
    else
      g = g - x;
    fl = !fl;
  }
  g.print();
  int q = r(1, 100);
  int sk = r(700, 1000);
  printf("%d\n", q);
  FOR(i, 1, q) {
    int k = min(r(2, g.n), sk / q);
    printf("%d ", k);
    int last = 0;
    FOR(j, 1, k) { printf("%d ", last = r(last + 1, g.n - k + j)); }
    puts("");
  }
  return 0;
}
