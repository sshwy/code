#include <algorithm>
#include <cstdio>
#include <cstring>
#include <deque>
#include <iostream>
#include <map>
#include <set>

using namespace std;

struct node2 {
  int wy, t, r;
  int id;
  bool operator<(const node2 &a) const { return t < a.t; }
};
struct node3 {
  int wy, t;
  int id;
  bool operator<(const node3 &a) const { return t < a.t; }
};

int op[100050], oop[100050];
node2 ins[1050];
node3 del[1050];
int main() {
  int q;
  scanf("%d", &q);
  int typ;
  scanf("%d", &typ);
  int lstans = 0;
  char s[10];
  if (q <= 1000) {
    int cnt1 = 0, cnt2 = 0;
    for (int i = 1; i <= q; ++i) {
      scanf(" %s", s + 1);
      if (s[1] == 'I') {
        op[i] = 1;
        int t, d, r;
        scanf("%d%d%d", &t, &d, &r);
        if (typ) t ^= lstans;
        node2 nw;
        nw.id = i;
        nw.r = r;
        nw.t = t;
        nw.wy = d;
        ins[++cnt1] = nw;
        sort(ins + 1, ins + 1 + cnt1);
      } else if (s[1] == 'E') {
        op[i] = 2;
        int t, d, r;
        scanf("%d%d", &t, &d);
        if (typ) t ^= lstans;
        node3 nw;
        nw.id = i;
        nw.t = t;
        nw.wy = d;
        del[++cnt2] = nw;
        sort(del + 1, del + 1 + cnt2);
      } else if (s[1] == 'D') {
        int t;
        scanf("%d", &t);
        if (typ) t ^= lstans;
        for (int j = 1; j <= cnt1; ++j) {
          if (ins[j].t == t) {
            for (int k = j; k <= cnt1; ++k) ins[k] = ins[k + 1];
            cnt1--;
            break;
          }
        }
        for (int j = 1; j <= cnt2; ++j) {
          if (del[j].t == t) {
            for (int k = j; k <= cnt2; ++k) del[k] = del[k + 1];
            cnt2--;
            break;
          }
        }
      } else {
        int T;
        scanf("%d", &T);
        if (typ) T ^= lstans;
        deque<int> Q;
        int cur1 = 1, cur2 = 1;
        while (cur1 <= cnt1 || cur2 <= cnt2) {
          if ((cur1 <= cnt1 && ins[cur1].t <= del[cur2].t) || cur2 > cnt2) {
            if (ins[cur1].t > T) break;
            if (ins[cur1].wy)
              Q.push_back(ins[cur1].r);
            else
              Q.push_front(ins[cur1].r);
            cur1++;
          } else {
            if (del[cur2].t > T) break;
            if (del[cur2].wy)
              Q.pop_back();
            else
              Q.pop_front();
            cur2++;
          }
        }
        int ii;
        scanf("%d", &ii);
        ii--;
        while (ii--) Q.pop_front();
        printf("%d\n", lstans = Q.front());
      }
    }
  }
}
