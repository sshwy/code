#include <algorithm>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
/******************heading******************/
const int N = 1e4 + 4, M = 3e5 + 5, INF = 0x3f3f3f3f;
int n, m, q;

struct qxx {
  int nex, t;
};
qxx e[M * 2];
int h[N], le;
void add_path(int f, int t) { e[++le] = (qxx){h[f], t}, h[f] = le; }

int mark[N], d[N], a[N];

int k;

int bfs(int s) {
  queue<int> q;
  bool vis[N] = {0};
  q.push(s), vis[s] = 1, d[s] = 0;
  int res = 1;
  while (!q.empty()) {
    int u = q.front();
    q.pop();
    for (int i = h[u]; i; i = e[i].nex) {
      const int v = e[i].t;
      if (mark[v] && !vis[v]) d[v] = d[u] + 1, q.push(v), vis[v] = 1, res++;
    }
  }
  // FOR(i,1,k)printf("%d:%d, ",a[i],d[a[i]]); puts("");
  return res;
}
void go() {
  scanf("%d", &k);
  memset(mark, 0, sizeof(mark));
  FOR(i, 1, k) {
    scanf("%d", &a[i]);
    mark[a[i]] = 1;
  }
  int ans = 0;
  FOR(i, 1, k) {
    int res = bfs(a[i]);
    if (res != k) {
      puts("-1");
      return;
    }
    FOR(j, 1, k) ans = max(ans, d[a[j]]);
  }
  printf("%d\n", ans);
}
int main() {
  scanf("%d%d", &n, &m);
  FOR(i, 1, m) {
    int u, v;
    scanf("%d%d", &u, &v);
    add_path(u, v), add_path(v, u);
  }
  scanf("%d", &q);
  FOR(i, 1, q) { go(); }
  return 0;
}
