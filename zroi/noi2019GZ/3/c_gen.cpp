#include <algorithm>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
/******************heading******************/

int r(int p) { return rand() % p; }
int r(int L, int R) { return r(R - L + 1) + L; }
map<int, int> mp;

int tot = 0;
int main() {
  srand(time(0));
  int q = 8, typ = 0;
  printf("%d %d\n", q, typ);
  printf("I 0 0 %d\n", r(1, 100));
  mp[0] = 1;
  ++tot;
  FOR(i, 1, q) {
    int op = r(0, 3);
    int d = r(0, 1);
    if (tot == 0) op = 0;
    if (op == 0) {
      int t = i; // r(1,100);
      while (mp.find(t) != mp.end()) ++t;
      mp[t] = 1;
      printf("I %d %d %d\n", t, d, r(1, 100));
      ++tot;
    } else if (op == 1) {
      int t = i; // r(1,100);
      while (mp.find(t) != mp.end()) ++t;
      mp[t] = 2;
      printf("E %d %d\n", t, d);
      --tot;
    } else if (op == 2) {
      int x = mp.size();
      map<int, int>::iterator it = mp.begin();
      while (--x) ++it;
      printf("D %d\n", it->fi);
      if (it->se == 1)
        --tot;
      else if (it->se == 2)
        ++tot;
      mp.erase(it);
    } else if (op == 3) {
      int t = i;
      printf("Q %d %d\n", t, r(1, tot));
    }
  }
  return 0;
}
