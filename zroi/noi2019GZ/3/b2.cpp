#include <algorithm>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
/******************heading******************/
const int N = 20;

pii a[N];

int calc(int l, int r) {
  int res = 0;
  while (l <= r) {
    res += r - l + 1;
    l = (l + 1) >> 1, r = (r - 1) >> 1;
  }
  return res;
}
int main() {
  int n, q;
  scanf("%d%d", &n, &q);
  FOR(i, 1, q) { scanf("%d%d\n", &a[i].fi, &a[i].se); }
  int s = 1 << q, tot = 0;
  FOR(i, 0, s - 1) {
    int c[N] = {0};
    FOR(j, 1, q) if ((i >> (j - 1)) & 1) c[a[j].fi]++, c[a[j].se + 1]--;
    FOR(j, 1, n - 1) c[j] += c[j - 1];
    // FOR(j,0,n-1)printf("c[%d]:%d ",j,c[j]); puts("");
    int last = 0;
    FOR(j, 0, n - 1) {
      if (c[j] != c[last]) tot += calc(last, j - 1), last = j;
    }
    tot += calc(last, n - 1);
  }
  printf("%lld\n", (n * 2ll - 1) * s - tot);
  return 0;
}
