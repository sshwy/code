#include <algorithm>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
/******************heading******************/
const int N = 2000, NIE = 0x3f3f3f3f, SZ = 2e5 + 5;

struct FHQ {
  int tot, root;
  int seed = 1;
  int lc[SZ], rc[SZ], sz[SZ], rnd[SZ];
  int op[SZ], tm[SZ], val[SZ];     // op,time
  int sum[SZ], smax[SZ], smin[SZ]; //权值和，后缀max/min

  int r() { return seed * 482711; }
  void pushup(int u) {
    sum[u] = sum[lc[u]] + sum[rc[u]] + op[u];
    smax[u] = max(smax[rc[u]], max(max(0, op[u]), op[u] + smax[lc[u]]) + sum[rc[u]]);
    smin[u] = min(smin[rc[u]], min(min(0, op[u]), op[u] + smin[lc[u]]) + sum[rc[u]]);
  }
  void split(int u, int k, int &x, int &y) {
    if (!u) {
      x = y = 0;
      return;
    }
    // pushdown
    if (tm[u] <= k)
      x = u, split(rc[u], k, rc[u], y);
    else
      y = u, split(lc[u], k, x, lc[u]);
    pushup(u);
  }
  int merge(int x, int y) {
    // pushdown
    if (!x || !y) return x + y;
    if (rnd[x] > y)
      return rc[x] = merge(rc[x], y), pushup(x), x;
    else
      return lc[y] = merge(x, lc[y]), pushup(y), y;
  }
  void insert(int _tm, int _op, int _val) { //在时间_tm插入操作_op，值为_val
    int x, y;
    split(root, _tm - 1, x, y);
    ++tot, lc[tot] = rc[tot] = 0, sz[tot] = 1, rnd[tot] = r(),
           sum[tot] = smax[tot] = smin[tot] = op[tot] = _op, tm[tot] = _tm,
           val[tot] = _val;
    root = merge(merge(x, tot), y);
  }
  void del(int _tm) { //删除所有时间戳为_tm的操作
    int x, y, z;
    split(root, _tm - 1, x, y);
    split(y, _tm, y, z);
    root = merge(x, z);
  }
  int query_sum(int _tm) { //求前_tm时间内的操作的权值和
    int x, y, res;
    split(root, _tm, x, y);
    res = sum[x];
    root = merge(x, y);
    return res;
  }
  int query(int _tm,
      int k) { //前_tm时间内的操作后，最后一个前缀和为k且为加一的位置,返回这个位置的val
    printf("FHQ::query(%d,%d)\n", _tm, k);
    int x, y;
    split(root, _tm, x, y);
    //在x上二分后缀和
    k = sum[x] - k + 1; // k转化为后缀和
    printf("k':%d\n", k);
    int u = x, fl = 0;
    if (u == 0) return NIE;
    while (op[u] + sum[rc[u]] != k) {
      printf("op[%d]:%d,sum[rc[%d]]:%d\n", u, op[u], u, sum[rc[u]]);
      if (smin[rc[u]] <= k && k <= smax[rc[u]])
        u = rc[u];
      else if (smin[lc[u]] <= k && k <= smax[lc[u]])
        k -= sum[rc[u]], u = lc[u]; //相当于在左子树内求后缀和为k的点
      else {
        printf("ERR!");
        fl = 1;
        break;
      }
    }
    root = merge(x, y);
    if (fl) return NIE;
    return val[u];
  }
} tl, tr;

int query(int _tm, int k) { //经过前_tm个操作后求队列的第k个数
  printf("query(%d,%d)\n", _tm, k);
  int lsum = tl.query_sum(_tm);
  printf("lsum:%d\n", lsum);
  int res = tr.query(_tm, k - lsum);
  printf("res:%d\n", res);
  if (res == NIE) return tl.query(_tm, lsum - k + 1);
  return res;
}
int main() {
  int q, typ, last = 0;
  scanf("%d%d", &q, &typ);
  FOR(i, 1, q) {
    char op[5];
    int t, d, r;
    scanf("%s%d", op, &t);
    if (typ) t ^= last;
    if (op[0] == 'I') {
      scanf("%d%d", &d, &r);
      printf("\033[32mInsert( time at %d, from %s, %d)\033[0m\n", t,
          d ? "right" : "left", r);
      if (d == 0)
        tl.insert(t, 1, r);
      else
        tr.insert(t, 1, r);
    } else if (op[0] == 'E') {
      scanf("%d", &d);
      printf("\033[32mErase( time at %d, from %s)\033[0m\n", t, d ? "right" : "left");
      if (d == 0)
        tl.insert(t, -1, 0);
      else
        tr.insert(t, -1, 0);
    } else if (op[0] == 'D') {
      printf("\033p32mDelete operation at time %d\033[0m\n", t);
      tl.del(t), tr.del(t);
    } else {
      scanf("%d", &d);
      printf("\033[32mQuery the %d-th number at time %d\033[0m\n", d, t);
      printf("%d\n", last = query(t, d));
    }
  }
  return 0;
}
