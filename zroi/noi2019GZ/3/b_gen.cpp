#include <algorithm>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
/******************heading******************/

int r(int p) { return rand() % p; }
int r(int L, int R) { return r(R - L + 1) + L; }

int main() {
  freopen("tmpout.txt", "w", stdout);
  srand(time(0));
  int n = 524288;
  int q = 6e5;
  printf("%d %d\n", n, q);
  FOR(i, 1, q) {
    int L = r(0, n - 1), R = r(L, n - 1);
    printf("%d %d\n", L, R);
  }
  return 0;
}
