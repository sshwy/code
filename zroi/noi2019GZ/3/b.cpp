#include <algorithm>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
/******************heading******************/
const int N = 2e3 + 4;

int n, q;
pii a[N];

int tot;
int f[N][N];

int calc(int l, int r) {
  int res = 0;
  while (l <= r) {
    res += r - l + 1;
    l = (l + 1) >> 1, r = (r - 1) >> 1;
  }
  return res;
}
int solve1() {
  int s = 1 << q, tot = 0;
  FOR(i, 0, s - 1) {
    int c[N] = {0};
    FOR(j, 1, q) if ((i >> (j - 1)) & 1) c[a[j].fi]++, c[a[j].se + 1]--;
    FOR(j, 1, n - 1) c[j] += c[j - 1];
    int last = 0;
    FOR(j, 0, n - 1) {
      if ((c[j] & 1) != (c[last] & 1)) tot += calc(last, j - 1), last = j;
    }
    tot += calc(last, n - 1);
  }
  printf("%lld\n", (n * 2ll - 1) * s - tot);
  return 0;
}

void dfs_calc(int l, int r) {
  // printf("dfs_calc(%d,%d)\n",l,r);
  int non = 0, c = 0;
  FOR(i, 0, n - 1) FOR(j, i, n - 1) f[i][j] = 0;
  FOR(i, 1, q) {
    if (a[i].se < l || r < a[i].fi)
      non++;
    else if (a[i].fi <= l && r <= a[i].se)
      c++;
    else
      f[a[i].fi][a[i].se]++;
  }
  int s = 0;
  ROF(i, n - 1, 0) {
    FOR(j, i + 1, n - 1) {
      if (f[i][j]) f[i][j] = (1 << f[i][j]) - 1;
      FOR(k, i, j - 1) f[i][j] += f[i][k] * f[k + 1][j];
    }
  }
  FOR(i, 0, l) FOR(j, r, n - 1) s += f[i][j];
  int ctr = (1 << (non + c)) * (s + 1);
  tot += ctr;
  if (l == r) return;
  int mid = (l + r) >> 1;
  dfs_calc(l, mid), dfs_calc(mid + 1, r);
}
int main() {
  scanf("%d%d", &n, &q);
  FOR(i, 1, q) { scanf("%d%d\n", &a[i].fi, &a[i].se); }
  if (n <= 16) return solve1();
  sort(a + 1, a + q + 1); //左端点从小到大排序
  dfs_calc(0, n - 1);
  printf("%d\n", (n * 2 - 1) * (1 << q) - tot);
  return 0;
}
