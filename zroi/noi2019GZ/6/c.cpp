#include <algorithm>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
/******************heading******************/
const int S = 8000;
int ls, q, lr;
char s[S];
pii rk[52 * 52];
int f[52][52][52];

bool cmp(pii x, pii y) {
  for (int p = x.fi, q = y.fi; p <= x.se && q <= y.se; p++, q++) {
    if (s[p] != s[q]) return s[p] < s[q];
  }
  return x.se - x.fi < y.se - y.fi;
}

int main() {
  scanf("%s%d", s + 1, &q);
  ls = strlen(s + 1);
  FOR(i, 1, ls) {
    FOR(j, i, ls) { rk[++lr] = mk(i, j); }
  }
  sort(rk + 1, rk + lr + 1, cmp);
  FOR(i, 1, lr) { f[rk[i].fi][rk[i].se][1] = i; }
  FOR(k, 2, ls) {
    FOR(i, 1, ls) {
      FOR(j, i + k - 1, ls) {
        f[i][j][k] = f[i][j][k - 1];
        FOR(p, i + k - 2, j - 1) {
          f[i][j][k] = min(f[i][j][k], max(f[i][p][k - 1], f[p + 1][j][1]));
        }
      }
    }
  }
  FOR(i, 1, q) {
    int l, k;
    scanf("%d%d", &l, &k);
    pii ans = rk[f[l][ls][k]];
    printf("%d %d\n", ans.fi, ans.se);
  }
  return 0;
}
