#include <algorithm>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
/******************heading******************/
const int N = 100, P = 998244353;
int n, k;
int a[N];
int b[N], c[N];

void g(int, int, int);
void f(int a, int i, int t) {
  // printf("f[%d,%d,%d]\n",a,i,t);
  if (t == n) {
    (b[i] += a) %= P;
    return;
  }
  f((P - 1ll * a * i % P) % P, i + 1, t + 1), g(a, i, t + 1);
}
void g(int a, int i, int t) {
  // printf("g[%d,%d,%d]\n",a,i,t);
  if (t == n) {
    (c[i] += a) %= P;
    return;
  }
  g((P - 1ll * a * i % P) % P, i + 1, t + 1), f(a, i, t + 1);
}

int main() {
  scanf("%d%d", &n, &k);
  FOR(i, 1, k) scanf("%d", &a[i]);
  FOR(i, 1, k) f(a[i], i, 0);
  FOR(i, 1, n + k) printf("%d ", b[i]);
  puts("");
  FOR(i, 1, n + k) printf("%d ", c[i]);
  puts("");
  return 0;
}
