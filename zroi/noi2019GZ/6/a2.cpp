#include <algorithm>
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
/******************heading******************/
const int N = 1e5 + 5, K = 1e5 + 5, P = 998244353;
int n, k;
int a[N + K];
int b[N + K], c[N + K];
int fac[N + K], inv[N + K];

int pw(int a, int m) {
  int res = 1;
  while (m) (m & 1 ? res = 1ll * res * a % P : 0), a = 1ll * a * a % P, m >>= 1;
  return res;
}
int binom(int n, int m) {
  if (n < m) return 0;
  return 1ll * fac[n] * inv[m] % P * inv[n - m] % P;
}
int G(int p) { return 1ll * binom(n, p) * (P - 2 * ((p + (n - p) / 2) & 1) + 1) % P; }
int F(int p) { return 1ll * a[p] * inv[p - 1] % P; }
void calc(int x) {
  // printf("\033[31mcalc(%d)\033[0m\n",x);
  int res[] = {0, 0};
  // int l=max(0,x-k),r=min(n,x-1);
  int sig = n & 1;
  FOR(p, 0, x / 2) { (res[sig] += 1ll * G(p * 2) * F(x - p * 2) % P) %= P; }
  FOR(p, 0, (x - 1) / 2) {
    (res[!sig] += 1ll * G(p * 2 + 1) * F(x - p * 2 - 1) % P) %= P;
  }
  b[x] = 1ll * res[0] * fac[x - 1] % P, c[x] = 1ll * res[1] * fac[x - 1] % P;
}

int main() {
  // freopen("ex_data2.in","r",stdin);
  scanf("%d%d", &n, &k);
  FOR(i, 1, k) scanf("%d", &a[i]);
  fac[0] = 1;
  FOR(i, 1, n + k) fac[i] = 1ll * i * fac[i - 1] % P;
  inv[n + k] = pw(fac[n + k], P - 2);
  ROF(i, n + k, 1) inv[i - 1] = 1ll * i * inv[i] % P;
  FOR(i, 1, n + k) calc(i); // b[i],c[i]
  FOR(i, 1, n + k) printf("%d ", b[i]);
  puts("");
  FOR(i, 1, n + k) printf("%d ", c[i]);
  puts("");
  return 0;
}
