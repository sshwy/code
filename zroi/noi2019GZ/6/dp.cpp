#include <algorithm>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
/******************heading******************/

int main() {
  // if(system("diff -w A.out tmpout"))puts("gg");
  // puts("agaga");
  system("g++ a_gen.cpp -O2 -lm -o .gen");
  system("g++ a3.cpp -O2 -lm -o .std");
  system("g++ a2.cpp -O2 -lm -o .usr");
  FOR(i, 1, 100000) {
    system("./.gen > .fin");
    system("./.usr < .fin > .fout");
    system("./.std < .fin > .fstd");
    if (system("diff -w .fout .fstd")) break;
    printf("AC#%d\n", i);
  }
  printf("WA or finish");
  return 0;
}
