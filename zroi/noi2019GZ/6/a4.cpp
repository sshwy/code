#include <algorithm>
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define PI M_PI
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
/******************heading******************/

const int N = 1e6 + 5, K = 1e6 + 5, P = 998244353;
int pw(int a, int m) {
  int res = 1;
  while (m) (m & 1 ? res = 1ll * res * a % P : 0), a = 1ll * a * a % P, m >>= 1;
  return res;
}
namespace NTT {
  const int SZ = 1e6 + 5;
  int r[SZ], n, lim;
  void ntt(int *f, int lim, int opt) {
    FOR(i, 0, lim - 1) if (r[i] < i) swap(f[i], f[r[i]]);
    for (int m = 2; m <= lim; m <<= 1) {
      int k = m >> 1, gn = pw(3, (P - 1) / m);
      for (int i = 0; i < lim; i += m) {
        int g = 1, t;
        FOR(j, 0, k - 1) {
          t = 1ll * f[i + j + k] * g % P;
          f[i + j + k] = (f[i + j] - t + P) % P, f[i + j] = (f[i + j] + t) % P;
          g = 1ll * g * gn % P;
        }
      }
    }
    if (opt == -1) {
      reverse(f + 1, f + lim);
      int inv = pw(lim, P - 2);
      FOR(i, 0, lim - 1) f[i] = 1ll * f[i] * inv % P;
    }
  }
  void init(int lf) {
    n = lf, lim = 1;
    while (lim < (n << 1)) lim <<= 1;
    FOR(i, 0, lim - 1) r[i] = (i & 1) * (lim >> 1) + (r[i >> 1] >> 1);
  }
} // namespace NTT

int n, k;
int a[N + K], fac[N + K], inv[N + K];
int g[2][NTT::SZ], f[2][NTT::SZ];
int h[2][2][NTT::SZ];

int binom(int n, int m) {
  if (n < m) return 0;
  return 1ll * fac[n] * inv[m] % P * inv[n - m] % P;
}
int G(int p) { return 1ll * binom(n, p) * (P - 2 * ((p + (n - p) / 2) & 1) + 1) % P; }
int F(int p) { return 1ll * a[p] * inv[p - 1] % P; }

int main() {
  // freopen("ex_data2.in","r",stdin);
  // freopen("tmpout","w",stdout);
  scanf("%d%d", &n, &k);
  FOR(i, 1, k) scanf("%d", &a[i]);

  fac[0] = 1;
  FOR(i, 1, n + k) fac[i] = 1ll * i * fac[i - 1] % P;
  inv[n + k] = pw(fac[n + k], P - 2);
  ROF(i, n + k, 1) inv[i - 1] = 1ll * i * inv[i] % P;

  for (int i = 0; i <= n + k; i += 2) f[0][i] = F(i);
  for (int i = 1; i <= n + k; i += 2) f[1][i] = F(i);
  for (int i = 0; i <= n + k; i += 2) g[0][i] = G(i);
  for (int i = 1; i <= n + k; i += 2) g[1][i] = G(i);

  NTT::init(n + k);
  NTT::ntt(f[0], NTT::lim, 1);
  NTT::ntt(f[1], NTT::lim, 1);
  NTT::ntt(g[0], NTT::lim, 1);
  NTT::ntt(g[1], NTT::lim, 1);

  FOR(i, 0, NTT::lim - 1) h[0][0][i] = 1ll * f[0][i] * g[0][i] % P;
  FOR(i, 0, NTT::lim - 1) h[0][1][i] = 1ll * f[0][i] * g[1][i] % P;
  FOR(i, 0, NTT::lim - 1) h[1][0][i] = 1ll * f[1][i] * g[0][i] % P;
  FOR(i, 0, NTT::lim - 1) h[1][1][i] = 1ll * f[1][i] * g[1][i] % P;

  NTT::ntt(h[0][0], NTT::lim, -1);
  NTT::ntt(h[0][1], NTT::lim, -1);
  NTT::ntt(h[1][0], NTT::lim, -1);
  NTT::ntt(h[1][1], NTT::lim, -1);

  FOR(i, 1, n + k) h[0][0][i] = 1ll * h[0][0][i] * fac[i - 1] % P;
  FOR(i, 1, n + k) h[0][1][i] = 1ll * h[0][1][i] * fac[i - 1] % P;
  FOR(i, 1, n + k) h[1][0][i] = 1ll * h[1][0][i] * fac[i - 1] % P;
  FOR(i, 1, n + k) h[1][1][i] = 1ll * h[1][1][i] * fac[i - 1] % P;

  bool sigg = n & 1;
  int ans[2][NTT::SZ];
  for (int i = 0; i <= n + k; i += 2) ans[0][i] = h[0][0][i];
  for (int i = 1; i <= n + k; i += 2) ans[1][i] = h[0][1][i];
  for (int i = 1; i <= n + k; i += 2) ans[0][i] = h[1][0][i];
  for (int i = 0; i <= n + k; i += 2) ans[1][i] = h[1][1][i];

  FOR(i, 1, n + k) printf("%d ", ans[sigg][i]);
  puts("");
  FOR(i, 1, n + k) printf("%d ", ans[!sigg][i]);
  puts("");
  return 0;
}
