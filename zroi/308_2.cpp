#include <algorithm>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <deque>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <stack>
#include <vector>
using namespace std;
typedef long long ll;
typedef long double ld;
typedef unsigned long long ull;
typedef pair<int, int> PII;
#define fast ios::sync_with_stdio(0), cin.tie(0)
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define ls (rt << 1)
#define rs (rt << 1 | 1)
#define clr(a) memset((a), 0, sizeof(a))
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define REP(i, a, b) for (int i = (a); i >= (b); --i)
const int inf = 0x3f3f3f3f;
const ll INF = 0x3f3f3f3f3f3f3f3f;
const int dx[] = {1, -1, 0, 0};
const int dy[] = {0, 0, -1, 1};

inline char in() {
  static char buf[10001], *p1 = buf, *p2 = buf;
  return p1 == p2 && (p2 = (p1 = buf) + fread(buf, 1, 10000, stdin), p1 == p2)
             ? EOF
             : *p1++;
}
template <class Tp> inline void read(register Tp &s) {
  s = 0;
  register bool neg = 0;
  register char c;
  while (!isdigit(c = in()))
    if (c == '-') neg = 1;
  while (s = (s << 3) + (s << 1) + c - 48, isdigit(c = in()))
    ;
  s = (neg ? -s : s);
}
template <class Tp> void write(register Tp x) {
  if (x < 0) putchar('-'), x = -x;
  if (x > 9) write(x / 10);
  putchar(x % 10 + '0');
}
template <class Tp> inline void print(const Tp &x, const char &end) {
  write(x);
  putchar(end);
}
template <class Tp> inline Tp pow(register Tp x, register Tp p, const Tp &mod) {
  register Tp res = 1;
  for (; p; p >>= 1, x = 1LL * x * x % mod)
    if (p & 1) res = 1LL * res * x % mod;
  return res;
}
inline int comp(const double &x, const double &y) {
  const double d = x - y, eps = 1e-8;
  return (d <= eps && d >= -eps) ? 0 : (d > eps ? 1 : -1);
}
template <class Tp> inline Tp sqr(const Tp &x) { return x * x; }
template <class Tp> inline Tp gcd(const Tp &x, const Tp &y) {
  return ((!y) ? x : gcd(y, x % y));
}
template <class Tp> inline Tp inv(const Tp &x, const Tp &MOD) {
  return pow(x, MOD - 2, MOD);
}
template <class Tp> inline Tp lowbit(const Tp &x) { return x & -x; }

const int N = 500005;
struct Edge {
  int to, nxt;
} e[N << 1];
int n, m, p, tot, hd[N], ff[N], fa[N], dep[N], u[N], v[N];
// 注意： ff 指并查集的 father 而 fa 指树上的 father
bool mark[N], vis[N];
int find(int x) { return x == ff[x] ? x : ff[x] = find(ff[x]); }
void add(int u, int v) {
  e[++tot].to = v;
  e[tot].nxt = hd[u];
  hd[u] = tot;
}
void dfs(int u) {
  dep[u] = dep[fa[u]] + 1;
  ff[u] = u;
  vis[u] = 1;
  // 每次把树上的点的 father 设为自己（以便缩环）
  for (int i = hd[u]; i; i = e[i].nxt) {
    int v = e[i].to;
    if (v != fa[u]) fa[v] = u, dfs(v);
  }
}
void jump(int &x) {
  int anc = fa[x];
  x = ff[x] = find(anc); //将 x 向上跳，将环上的点缩起来，每个点最多一次即 O(n)？？？
                         // 我怎么知道复杂度对不对。。。
}

int main() {
  read(n);
  read(m);
  read(p);
  FOR(i, 1, n) ff[i] = i; // init
  for (int i = 1; i <= m; i++) {
    read(u[i]);
    read(v[i]);
    int fu = find(u[i]), fv = find(v[i]);
    if (fu ^ fv) { //如果不在同一个连通块
      ff[fu] = fv;
      mark[i] = 1; //标记该边（桥）
      add(u[i], v[i]);
      add(v[i], u[i]);
    }
  }
  //	dfs(1);
  FOR(i, 1, n) if (!vis[i]) dfs(i); //建树（可能多棵树）
  long long sum = 0, ans = 1LL;
  FOR(i, 1, m) {
    if (mark[i])
      ++sum;
    else {
      int x = find(u[i]), y = find(v[i]);
      int loopsize = 0;
      while (x ^ y) {
        (dep[x] < dep[y]) ? jump(y) : jump(x);
        loopsize++;
      } //缩环，记录环的大小（不是割边）
      sum -= loopsize;
    }
    ans = (1LL * ans * (sum + 1)) % p;
  }
  printf("%lld\n", ans);
  return 0;
}
/*
6 7 233
6 5
1 2
3 2
1 2
4 6
4 5
1 1
*/
