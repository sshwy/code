#include <bits/stdc++.h>
#define FOR(a, b, c) for (int a = (b); a <= (c); a++)
using namespace std;
const int N = 1e6 + 5;
struct qxx {
  int nex, t;
};
qxx e[N * 2];
int h[N], le = 1;
void add_path(int f, int t) { e[++le] = (qxx){h[f], t}, h[f] = le; }
#define FORe(i, u, v) for (int i = h[u], v; v = e[i].t, i; i = e[i].nex)

int n, m;

namespace s1 {
  void dfs(int u, int p) {
    printf("%d ", u);
    vector<int> son;
    FORe(i, u, v) if (v != p) son.push_back(v);
    sort(son.begin(), son.end());
    for (int i = 0; i < son.size(); i++) { dfs(son[i], u); }
  }
  void solve1() { dfs(1, 0); }
} // namespace s1
namespace s2 {
  bool on_cir[N], vis[N];
  int find_circle(int u, int p) {
    if (vis[u]) return u;
    vis[u] = 1;
    FORe(i, u, v) {
      if (v == p) continue;
      int res = find_circle(v, u);
      if (res) return on_cir[u] = 1, res == u ? 0 : res;
    }
    return 0;
  }
  void solve2() {
    find_circle(1, 0);
    FOR(i, 1, n) if (on_cir[i]) printf("%d ", i);
  }
} // namespace s2

int main() {
  scanf("%d%d", &n, &m);
  FOR(i, 1, m) {
    int u, v;
    scanf("%d%d", &u, &v);
    add_path(u, v), add_path(v, u);
  }
  if (m == n - 1)
    s1::solve1();
  else
    s2::solve2();
  return 0;
}
