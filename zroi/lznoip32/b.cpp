// by Sshwy
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace io {
  const int SIZE = (1 << 21) + 1;
  char ibuf[SIZE], *iS, *iT, obuf[SIZE], *oS = obuf, *oT = oS + SIZE - 1, c, qu[55];
  int f, qr;
// getchar
#define gc()                                                   \
  (iS == iT ? (iT = (iS = ibuf) + fread(ibuf, 1, SIZE, stdin), \
                  (iS == iT ? EOF : *iS++))                    \
            : *iS++)
  // print the remaining part
  inline void flush() {
    fwrite(obuf, 1, oS - obuf, stdout);
    oS = obuf;
  }
  // putchar
  inline void putc(char x) {
    *oS++ = x;
    if (oS == oT) flush();
  }
  // input a signed integer
  template <class I> inline void gi(I &x) {
    for (f = 1, c = gc(); c < '0' || c > '9'; c = gc())
      if (c == '-') f = -1;
    for (x = 0; c <= '9' && c >= '0'; c = gc()) x = x * 10 + (c & 15);
    x *= f;
  }
  // print a signed integer
  template <class I> inline void print(I &x) {
    if (!x) putc('0');
    if (x < 0) putc('-'), x = -x;
    while (x) qu[++qr] = x % 10 + '0', x /= 10;
    while (qr) putc(qu[qr--]);
  }
  // no need to call flush at the end manually!
  struct Flusher_ {
    ~Flusher_() { flush(); }
  } io_flusher_;
} // namespace io
using io ::gi;
using io ::print;
using io ::putc;
const int W = 1e7 + 5;
bool co[W];
int pn[W], lp;
long long f[W], g[W];

int main() {
  int lim = 1e7;
  co[0] = co[1] = 1;
  f[1] = 1;
  FOR(i, 2, lim) {
    if (!co[i]) pn[++lp] = i, f[i] = g[i] = i + 1;
    FOR(j, 1, lp) {
      int x = i * pn[j];
      if (x > lim) break;
      co[x] = 1;
      if (i % pn[j] == 0) {
        g[x] = g[i] * pn[j] + 1;
        f[x] = f[i] / g[i] * g[x];
      } else {
        f[x] = f[i] * f[pn[j]];
        g[x] = g[pn[j]];
      }
    }
  }
  FOR(i, 2, lim) f[i] += f[i - 1];
  int T;
  gi(T);
  FOR(i, 1, T) {
    int n;
    gi(n);
    long long ans = n * 1ll * n - f[n];
    print(ans);
    putc('\n');
  }
  return 0;
}
