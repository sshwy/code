// by Sshwy
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA
const int N = 40;
int n;
int a[N][N], b[N][N];

int main() {
  scanf("%d", &n);
  FOR(i, 1, n) FOR(j, 1, n) scanf("%d", &a[i][j]);
  int x = (n + 1) / 2;
  int lim = 1 << x;
  int ans = -0x3f3f3f3f;
  FOR(s, 0, lim - 1) {
    int tot = 0;
    FOR(i, 1, x) b[i][x] = s >> (i - 1) & 1;
    FOR(i, 1, x - 1) b[i + x][x] = b[i][x] ^ b[x][x];
    auto val = [](int x, int y) { return b[x][y] ? -a[x][y] : a[x][y]; };
    FOR(i, 1, n) tot += val(i, x);
    // FOR(i,1,n)printf("%d%c",b[i][x]," \n"[i==n]);
    FOR(j, 1, x - 1) {
      int t3 = 0;
      {
        b[x][j] = 0;
        b[x][j + x] = b[x][j] ^ b[x][x];
        int t2 = val(x, j) + val(x, j + x);
        FOR(i, 1, x - 1) {
          b[i][j] = 0;
          b[i][j + x] = b[i][j] ^ b[i][x];
          b[i + x][j] = b[i][j] ^ b[x][j];
          b[i + x][j + x] = b[i + x][j] ^ b[i + x][x];
          int t4 = val(i, j) + val(i, j + x) + val(i + x, j) + val(i + x, j + x);
          b[i][j] = 1;
          b[i][j + x] = b[i][j] ^ b[i][x];
          b[i + x][j] = b[i][j] ^ b[x][j];
          b[i + x][j + x] = b[i + x][j] ^ b[i + x][x];
          t4 = max(t4, val(i, j) + val(i, j + x) + val(i + x, j) + val(i + x, j + x));
          t2 += t4;
        }
        t3 = t2;
      }
      {
        b[x][j] = 1;
        b[x][j + x] = b[x][j] ^ b[x][x];
        int t2 = val(x, j) + val(x, j + x);
        FOR(i, 1, x - 1) {
          b[i][j] = 0;
          b[i][j + x] = b[i][j] ^ b[i][x];
          b[i + x][j] = b[i][j] ^ b[x][j];
          b[i + x][j + x] = b[i + x][j] ^ b[i + x][x];
          int t4 = val(i, j) + val(i, j + x) + val(i + x, j) + val(i + x, j + x);
          b[i][j] = 1;
          b[i][j + x] = b[i][j] ^ b[i][x];
          b[i + x][j] = b[i][j] ^ b[x][j];
          b[i + x][j + x] = b[i + x][j] ^ b[i + x][x];
          t4 = max(t4, val(i, j) + val(i, j + x) + val(i + x, j) + val(i + x, j + x));
          t2 += t4;
        }
        t3 = max(t3, t2);
      }
      tot += t3;
    }
    ans = max(ans, tot);
  }
  printf("%d\n", ans);
  return 0;
}
