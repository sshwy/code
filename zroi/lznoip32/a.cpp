// by Sshwy
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA
const int N = 1e6 + 5, P = 998244353;
int n;
int a[N];
int ans;

int main() {
  scanf("%d", &n);
  FOR(i, 1, n) scanf("%d", &a[i]);
  ans = 1;
  FOR(i, 1, n) ans = ans * 1ll * (a[i] + 1) % P;
  ans = (ans - 1 + P) % P;
  printf("%d\n", ans);
  return 0;
}
