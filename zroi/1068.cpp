#include <iostream>
#include <vector>
using namespace std;
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
int n, a[10000];
vector<int> ans;
int main() {
  cin >> n;
  FOR(i, 1, n) cin >> a[i];
  ROF(i, n, 1) ans.insert(ans.begin() + a[i], i);
  for (int x : ans) cout << x << " ";
  return 0;
}
