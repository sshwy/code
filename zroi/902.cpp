#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
namespace IO {
  char nc() {
    static char bf[100000], *p1 = bf, *p2 = bf;
    return p1 == p2 && (p2 = (p1 = bf) + fread(bf, 1, 100000, stdin), p1 == p2)
               ? EOF
               : *p1++;
  }
  int rd() {
    int res = 0;
    char c = getchar();
    while (!isdigit(c)) c = getchar();
    while (isdigit(c)) res = res * 10 + c - '0', c = getchar();
    return res;
  }
} // namespace IO
/******************heading******************/
const int N = 1e6 + 6;
const int SZ = 2e6 + 6, ALP = 4;

struct Sam {
  int tot, last;
  int len[SZ], tr[SZ][ALP], fail[SZ];
  int end[SZ]; //记录这个点在字符串中的结束位置
  int ls;
  char s[SZ];
  int newnode(int l) {
    ++tot, fail[tot] = 0, len[tot] = l, end[tot] = 0;
    memset(tr[tot], 0, sizeof(tr[tot]));
    return tot;
  }
  void clear() {
    ls = 0, tot = 0;
    newnode(0);
    last = 1;
  }
  Sam() { clear(); }

  // void printnode(int u){
  //    printf("u=%2d,fail=%2d,end=%2d ",u,fail[u],end[u]);
  //    FOR(i,1,end[u]-len[u])putchar(' ');
  //    FOR(i,end[u]-len[u]+1,end[u])putchar(s[i]);
  //    puts("");
  //}
  void insert(char x) {
    s[++ls] = x;
    x -= 'a';
    int u = newnode(len[last] + 1), p = last;
    last = u;
    end[u] = ls;
    while (p && tr[p][x] == 0) tr[p][x] = u, p = fail[p];
    if (!p)
      fail[u] = 1;
    else {
      int q = tr[p][x];
      if (len[q] == len[p] + 1)
        fail[u] = q;
      else {
        int cq = newnode(len[p] + 1);
        memcpy(tr[cq], tr[q], sizeof(tr[q]));
        fail[cq] = fail[q];
        end[cq] = end[q];
        fail[q] = fail[u] = cq;
        while (p && tr[p][x] == q) tr[p][x] = cq, p = fail[p];
      }
    }
  }
  int a[SZ], bin[SZ];
  void calc() { //更新end为最大值
    memset(bin, 0, sizeof(int) * (tot + 5));
    FOR(i, 1, tot) bin[len[i]]++;
    FOR(i, 1, tot) bin[i] += bin[i - 1];
    FOR(i, 1, tot) a[bin[len[i]]--] = i;
    ROF(i, tot, 1) {
      int u = a[i];
      // end[fail[u]]=max(end[fail[u]],end[u]);
    }
    // puts("");
    // FOR(i,1,tot)printnode(i);
  }
  int fail_tr[SZ][ALP], maxend[SZ];
  void build_fail_tree() {
    memset(fail_tr, 0, sizeof(fail_tr[0]) * (tot + 5));
    FOR(i, 2, tot) {
      int u = a[i];
      fail_tr[fail[u]][s[end[u] - len[fail[u]]] - 'a'] = u;
      // printf("fail_tr[%d,%c]=%d\n",fail[u], s[end[u]-len[fail[u]]], u);
    }
  }
  pii ans[SZ];
  void dfs(int u) {
    // printf("dfs(%d)\n",u);
    int t[10], lt = 0;
    maxend[u] = end[u];
    ROF(i, 3, 0) { //'d' -> 'a'
      if (fail_tr[u][i]) {
        dfs(fail_tr[u][i]);
        maxend[u] = max(maxend[u], maxend[fail_tr[u][i]]);
        t[++lt] = maxend[fail_tr[u][i]];
      }
    }
    sort(t + 1, t + lt + 1);
    if (lt > 1)
      ans[len[u]] = mk(t[lt], t[lt - 1]);
    else if (lt)
      ans[len[u]] = mk(t[lt], end[u]);
    else
      ans[len[u]] = mk(end[u], end[u]);
  }
  void go() {
    memset(ans, 0, sizeof(int) * (tot + 5));
    build_fail_tree();
    dfs(1);
    FOR(i, 1, ls) {
      if (!ans[i].fi || !ans[i].se)
        printf("-1 -1\n");
      else
        printf("%d %d\n", ls - ans[i].fi + 1, ls - ans[i].se + 1);
    }
  }
} sam;

int t;
char s[N];
void go() {
  scanf("%s", s + 1);
  int ls = strlen(s + 1);
  sam.clear();
  ROF(i, ls, 1) sam.insert(s[i]);
  sam.calc();
  sam.go();
}
int main() {
  scanf("%d", &t);
  while (t--) go();
  return 0;
}
