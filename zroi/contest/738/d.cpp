#include <cstdio>
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
const int N = 3005;
long long n, P, f[N][N], g[N][N], a[N];
int main() {
  scanf("%lld%lld", &n, &P);
  f[0][0] = 1;
  FOR(i, 1, n) FOR(j, 1, n) f[i][j] = (f[i - 1][j] * j + f[i - 1][j - 1]) % P;
  FOR(i, 1, n) g[0][i] = 1;
  FOR(i, 1, n) FOR(j, 1, n) g[i][j] = (g[i - 1][j] * j + g[i - 1][j + 1]) % P;
  ROF(i, n, 1) {
    int s = 0, s2 = 0;
    ROF(x, n, 1) {
      s = (s + f[i - 1][x] * g[n - i][x]) % P;
      s2 = (s2 + f[i - 1][x] * g[n - i - 1][x]) % P;
      int c1 = (s + f[i - 1][x - 1] * g[n - i][x]) % P;
      int c2 = (s2 + f[i - 1][x - 1] * g[n - i - 1][x]) % P * (n - i) % P;
      a[x] = ((a[x] + c1) % P + c2 * 2) % P;
    }
  }
  FOR(i, 1, n) printf("%lld%c", a[i], " \n"[i == n]);
  return 0;
}
