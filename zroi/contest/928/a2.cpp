#include <bits/stdc++.h>
#define FOR(a, b, c) for (int a = (int)(b); a <= (int)(c); a++)
#define ROF(a, b, c) for (int a = (int)(b); a >= (int)(c); a--)

const int N = 105, P = 998244353;

void add(int &x, long long y) { x = (x + y) % P; }

int n, A, B;
int f[N][N][N][7];

int main() {
  scanf("%d%d%d", &n, &A, &B);

  f[0][0][0][0] = 1;
  FOR(i, 1, n + 1) {
    FOR(a, 0, A) FOR(b, 0, B) {
      add(f[i][a][b][0], f[i - 1][a][b][0]);
      if (a > 0)
        add(f[i][a][b][1], 0ll + f[i - 1][a - 1][b][0] + f[i - 1][a - 1][b][1] +
                               f[i - 1][a - 1][b][3] + f[i - 1][a - 1][b][5]);
      if (b > 0)
        add(f[i][a][b][2], 0ll + f[i - 1][a][b - 1][0] + f[i - 1][a][b - 1][2] +
                               f[i - 1][a][b - 1][3] + f[i - 1][a][b - 1][4]);
      if (a > 0 && b > 0) add(f[i][a][b][3], 0ll + f[i - 1][a - 1][b - 1][0]);
      if (a > 0 && b > 0) add(f[i][a][b][4], 0ll + f[i - 1][a - 1][b - 1][1]);
      if (a > 0 && b > 0) add(f[i][a][b][5], 0ll + f[i - 1][a - 1][b - 1][2]);
      FOR(_, 0, 6) add(f[i][a][b][6], f[i - 1][a][b][_]);
      // FOR(j, 0, 6) if(f[i][a][b][j]) printf("f[%d, %d, %d, %d] = %d\n", i, a, b, j,
      // f[i][a][b][j]);
    }
  }

  printf("%d\n", f[n + 1][A][B][6]);
  return 0;
}
