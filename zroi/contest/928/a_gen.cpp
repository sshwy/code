#include <bits/stdc++.h>

int rnd(int L, int R) { return 1ll * rand() * rand() % (R - L + 1) + L; }

int main() {
  srand(clock() + time(0) * 1000);
  int n = rnd(1, 30);
  printf("%d %d %d\n", n, rnd(1, n), rnd(0, n));
  return 0;
}
