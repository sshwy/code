#include <bits/stdc++.h>
#define FOR(a, b, c) for (int a = (int)(b); a <= (int)(c); a++)

namespace IO { // require C++11
  const int _BS = 200000;
  char _ob[_BS], *_z = _ob, _ib[_BS], *_x = _ib, *_y = _ib;
  // Input
  char _nc() {
    if (_x == _y) _y = (_x = _ib) + fread(_ib, 1, _BS, stdin);
    if (_x == _y) return EOF;
    return *_x++;
    // return _x==_y&&(_y=(_x=_ib)+fread(_ib,1,_BS,stdin),_x==_y)?EOF:*_x++;
  }
  inline void rd(char &c) { c = _nc(); }
  void rd(char *s) {
    char c = _nc();
    while (!isblank(c) && isprint(c) && c != EOF) *s++ = c, c = _nc();
    *s = 0;
  }
  template <class T> void rd(T &res) { // unsigned/signed int/long long/__int128
    res = 0;
    char c = _nc(), flag = 0;
    while (!isdigit(c) && c != '-') c = _nc();
    if (c == '-') flag = 1, c = _nc();
    while (isdigit(c)) res = res * 10 + c - '0', c = _nc();
    if (flag) res = -res;
  }
  template <class T, class... A> void rd(T &hd, A &...rs) { rd(hd), rd(rs...); }
  // Output
  inline void flush() { fwrite(_ob, 1, _z - _ob, stdout), _z = _ob; }
  inline void wrch(char x) {
    if (_z == _ob + _BS - 1) flush();
    *_z = x, ++_z;
  }
  void wr(char *s) {
    while (*s) wrch(*s), ++s;
  }
  void wr(const char *s) {
    while (*s) wrch(*s), ++s;
  }
  template <class T> void wr(T x) { // unsigned/signed int/long long/__int128
    if (x == 0) return wrch('0'), void();
    if (x < 0) wrch('-'), x = -x;
    int _t[50], _lt = 0;
    while (x) _t[++_lt] = x % 10, x /= 10;
    while (_lt > 0) wrch(_t[_lt] + '0'), --_lt;
  }
  inline void wr(char x) { wrch(x); }
  template <class T, class... A> void wr(T hd, A... rs) { wr(hd), wr(rs...); }
  template <class... A> void wrln(A... ar) { wr(ar...), wrch('\n'); }
  struct _Flusher {
    ~_Flusher() { flush(); }
  } _flusher;
} // namespace IO
using IO::rd;
using IO::wrln;

const int N = 1e6 + 5, SZ = N * 4;

int n, m, q;

struct Operation {
  int typ, l, r, x, y;
  void read() {
    rd(typ);
    if (typ == 1)
      rd(x, y);
    else if (typ == 2)
      rd(l, r, x);
    else
      rd(x);
  }
} a[N];

struct Data {
  int typ, l, r, val;
  static bool byR(Data q1, Data q2) {
    if (q1.r == q2.r) return q1.typ > q2.typ;
    return q1.r < q2.r;
  }
  void read() { rd(l, r); }
} b[N + N];
int lb;

int tag[SZ];
void rangeAssign(int L, int R, int val, int u, int l, int r) {
  if (L <= l && r <= R) {
    tag[u] = val;
    return;
  }
  int mid = (l + r) / 2;
  if (tag[u] != -1) {
    tag[u << 1] = tag[u];
    tag[u << 1 | 1] = tag[u];
    tag[u] = -1;
  }
  if (L <= mid) rangeAssign(L, R, val, u << 1, l, mid);
  if (mid < R) rangeAssign(L, R, val, u << 1 | 1, mid + 1, r);
}

int getPos(int pos, int u, int l, int r) {
  if (l == r) return tag[u];
  if (tag[u] != -1) return tag[u];
  int mid = (l + r) / 2;
  if (pos <= mid)
    return getPos(pos, u << 1, l, mid);
  else
    return getPos(pos, u << 1 | 1, mid + 1, r);
}

void addData(int id, int rely_id) {
  // printf("id %d rely_id %d\n", id, rely_id);
  assert(a[rely_id].typ == 2);
  b[++lb] = {1, rely_id, id, a[rely_id].x};
}

long long c[N];
void addPos(int pos, long long val) {
  for (int i = pos; i < N; i += i & -i) c[i] += val;
}
long long getPresum(int pos) {
  long long res = 0;
  for (int i = pos; i > 0; i -= i & -i) res += c[i];
  return res;
}

long long ans[N];

int main() {
  rd(n, m, q);

  FOR(i, 1, m) a[i].read();
  a[0].typ = 2, a[0].l = 1, a[0].r = n, a[0].x = 0;

  FOR(i, 1, q) {
    ++lb;
    b[lb].read();
    b[lb].val = i;
    b[lb].typ = 0;
  }

  FOR(i, 1, m) {
    if (a[i].typ == 1) {
      int tag_x = getPos(a[i].x, 1, 1, n), tag_y = getPos(a[i].y, 1, 1, n);
      rangeAssign(a[i].x, a[i].x, tag_y, 1, 1, n);
      rangeAssign(a[i].y, a[i].y, tag_x, 1, 1, n);
    } else if (a[i].typ == 2) {
      rangeAssign(a[i].l, a[i].r, i, 1, 1, n);
    } else {
      addData(i, getPos(a[i].x, 1, 1, n));
    }
  }

  std::sort(b + 1, b + lb + 1, Data::byR);
  FOR(i, 1, lb) {
    if (b[i].typ == 0) {
      ans[b[i].val] = getPresum(b[i].r) - getPresum(b[i].l - 1);
    } else {
      if (b[i].l) addPos(b[i].l, b[i].val);
    }
  }

  FOR(i, 1, q) wrln(ans[i]);

  return 0;
}
