#include <bits/stdc++.h>

int rnd(int L, int R) { return 1ll * rand() * rand() % (R - L + 1) + L; }

int main() {
  srand(clock() + time(0) * 1000);
  int n = 2e5;
  printf("%d\n", n);
  for (int i = 1; i <= n; i++) printf("%d%c", rnd(0, 998244352), " \n"[i == n]);
  return 0;
}
