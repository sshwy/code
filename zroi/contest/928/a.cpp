#include <bits/stdc++.h>
#define FOR(a, b, c) for (int a = (int)(b); a <= (int)(c); a++)
#define ROF(a, b, c) for (int a = (int)(b); a >= (int)(c); a--)

const int N = 5e6 + 5, P = 998244353;

int pw(int a, int m) {
  int res = 1;
  while (m) {
    if (m & 1) res = 1ll * res * a % P;
    a = 1ll * a * a % P, m >>= 1;
  }
  return res;
}

int fac[N], fnv[N];

int binom(int a, int b) {
  if (a < b || b < 0) return 0;
  return 1ll * fac[a] * fnv[b] % P * fnv[a - b] % P;
}
int calc(int n, int m) { // divide n into m sections >=3
  if (n == 0 && m == 0) return 1;
  if (n < m * 3ll || m == 0) return 0;
  int res = binom(n - m * 3 + m - 1, m - 1);
  return res;
}

int n, ans;

void work(int start_with, int end_with, int A, int B) { // start with first line
  if (start_with == A && end_with == B) {
    int tot_len = start_with + end_with - 1;
    if (tot_len <= n) ans = (ans + n - tot_len + 1) % P;
    return;
  }

  int max_i = n / 2 + 2, delta = !!start_with + !!end_with;

  FOR(_i, 0, max_i) { // 断点个数
    int i = _i - delta;
    int tot_len = A + B - delta - i, s;
    if (i < 0 || tot_len > n) continue;
    if (i & 1) {
      s = 1ll * calc(A - start_with, i / 2 + 1) * calc(B - end_with, i / 2 + 1) % P *
          (n - tot_len + 1) % P;
    } else {
      s = 1ll * calc(A - start_with - end_with, i / 2) * calc(B, i / 2 + 1) % P *
          (n - tot_len + 1) % P;
    }
    ans = (ans + s) % P;
  }
}

int A, B;

int main() {
  fac[0] = 1;
  FOR(i, 1, N - 1) fac[i] = 1ll * fac[i - 1] * i % P;
  fnv[N - 1] = pw(fac[N - 1], P - 2);
  ROF(i, N - 1, 1) fnv[i - 1] = 1ll * fnv[i] * i % P;

  scanf("%d%d%d", &n, &A, &B);

  if (A == 1 && B == 1) return printf("%d\n", n), 0;
  if (A == 0) return printf("%d\n", n - B + 1), 0;
  if (B == 0) return printf("%d\n", n - A + 1), 0;

  FOR(start_with, 0, 2) FOR(end_with, 0, 2) {
    work(start_with, end_with, A, B);
    work(start_with, end_with, B, A);
  }

  printf("%d\n", ans);
  return 0;
}
