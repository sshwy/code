// by Sshwy
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

const int P = 998244353, N = 1e5 + 5, SZ = 1 << 18;

int pw(int a, int m) {
  int res = 1;
  while (m) m & 1 ? res = 1ll * res * a % P : 0, a = 1ll * a * a % P, m >>= 1;
  return res;
}
int tr[SZ << 1];
int init(int n) {
  int len = 1;
  while (len < n) len <<= 1;
  FOR(i, 0, len - 1) tr[i] = tr[i >> 1] >> 1 | (i & 1) * (len / 2);
  return len;
}
void dft(int *f, int len, int tag) {
  FOR(i, 0, len - 1) if (tr[i] < i) swap(f[tr[i]], f[i]);
  for (int j = 1; j < len; j <<= 1) {
    for (int i = 0, wn = pw(3, (P - 1) / (j << 1) * tag + P - 1); i < len;
         i += j << 1) {
      for (int k = i, u, v, w = 1; k < i + j; k++, w = w * 1ll * wn % P) {
        u = f[k], v = f[k + j] * 1ll * w % P, f[k] = (u + v) % P,
        f[k + j] = (u - v + P) % P;
      }
    }
  }
  if (tag == -1)
    for (int i = 0, in = pw(len, P - 2); i < len; ++i) f[i] = f[i] * 1ll * in % P;
}

typedef vector<int> poly;
poly operator+(const poly a, const poly b) {
  poly c(a);
  c.resize(max(a.size(), b.size()), 0);
  for (unsigned long i = 0; i < b.size(); i++) c[i] = (c[i] + b[i]) % P;
  return c;
}
int t1[SZ], t2[SZ];
poly operator*(const poly a, const poly b) {
  if (a.empty()) return a;
  if (b.empty()) return b;
  int len = init(a.size() + b.size());
  fill(t1, t1 + len, 0), fill(t2, t2 + len, 0);
  for (unsigned long i = 0; i < a.size(); i++) t1[i] = a[i];
  for (unsigned long i = 0; i < b.size(); i++) t2[i] = b[i];
  dft(t1, len, 1), dft(t2, len, 1);
  FOR(i, 0, len - 1) t1[i] = t1[i] * 1ll * t2[i] % P;
  dft(t1, len, -1);
  poly c(t1, t1 + len);
  while (c.back() == 0) c.pop_back();
  return c;
}

int n, m, a[N], sz[N], big[N], fa[N];
vector<int> g[N];
poly F[N], G[N];

void dfs1(int u, int p) {
  sz[u] = 1, fa[u] = p;
  for (int v : g[u])
    if (v != p)
      dfs1(v, u), sz[u] += sz[v], big[u] = big[u] && sz[big[u]] > sz[v] ? big[u] : v;
}
pair<poly, poly> calc(const vector<int> &V, int l, int r) {
  if (l > r) return {poly(1, 1), poly(1, 1)};
  if (l == r) return {G[V[l]], F[V[l]] + G[V[l]]};
  int mid = (l + r) >> 1;
  pair<poly, poly> pl = calc(V, l, mid), pr = calc(V, mid + 1, r);
  return {pl.first * pr.first, pl.second * pr.second};
}
pair<pair<poly, poly>, pair<poly, poly>> calc2(
    const vector<pair<pair<poly, poly>, pair<poly, poly>>> &vp, int l, int r) {
  if (l > r) return {{poly(1, 1), poly()}, {poly(), poly(1, 1)}};
  if (l == r) return vp[l];
  int mid = (l + r) >> 1;
  auto L = calc2(vp, l, mid), R = calc2(vp, mid + 1, r);
  pair<pair<poly, poly>, pair<poly, poly>> res;
  res.first.first = L.first.first * R.second.first + L.first.second * R.second.first +
                    L.first.second * R.first.first;
  res.first.second = L.first.first * R.second.second +
                     L.first.second * R.second.second +
                     L.first.second * R.first.second;
  res.second.first = L.second.first * R.second.first +
                     L.second.second * R.second.first +
                     L.second.second * R.first.first;
  res.second.second = L.second.first * R.second.second +
                      L.second.second * R.second.second +
                      L.second.second * R.first.second;
  return res;
}
void dfs2(int u, int p) {
  for (int v = u; v; v = big[v])
    for (int t : g[v])
      if (t != fa[v] && t != big[v]) dfs2(t, v);
  vector<pair<pair<poly, poly>, pair<poly, poly>>> vp;

  for (int v = u; v; v = big[v]) {
    vector<int> V;
    for (int t : g[v])
      if (t != fa[v] && t != big[v]) { V.pb(t); }
    pair<poly, poly> pr = calc(V, 0, (int)V.size() - 1);
    poly t = {0, a[v]};
    pr.first = pr.first * t;
    vp.pb({{pr.first, poly()}, {poly(), pr.second}});
  }
  auto res = calc2(vp, 0, (int)vp.size() - 1);
  F[u] = res.first.first + res.first.second;
  G[u] = res.second.first + res.second.second;
}

int main() {
  scanf("%d%d", &n, &m);
  FOR(i, 1, n) scanf("%d", &a[i]);
  FOR(i, 1, n - 1) {
    int x, y;
    scanf("%d%d", &x, &y);
    g[x].pb(y);
    g[y].pb(x);
  }
  dfs1(1, 0);
  dfs2(1, 0);
  while (F[1].size() <= n) F[1].pb(0);
  while (G[1].size() <= n) G[1].pb(0);
  int ans = (F[1][m] + G[1][m]) % P;
  printf("%d\n", ans);
  return 0;
}
