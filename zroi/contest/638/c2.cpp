// by Sshwy
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

const int P = 998244353, N = 1e5 + 5, SZ = 1 << 18;

int pw(int a, int m) {
  int res = 1;
  while (m) m & 1 ? res = 1ll * res * a % P : 0, a = 1ll * a * a % P, m >>= 1;
  return res;
}
int tr[SZ];
int init(int n) {
  int len = 1;
  while (len < n) len <<= 1;
  FOR(i, 0, len - 1) tr[i] = tr[i >> 1] >> 1 | (i & 1) * (len / 2);
  return len;
}
void dft(int *f, int len, int tag) {
  FOR(i, 0, len - 1) if (tr[i] < i) swap(f[tr[i]], f[i]);
  for (int j = 1; j < len; j <<= 1) {
    for (int i = 0, wn = pw(3, (P - 1) / (j << 1) * tag + P - 1); i < len;
         i += j << 1) {
      for (int k = i, u, v, w = 1; k < i + j; k++, w = w * 1ll * wn % P) {
        u = f[k], v = f[k + j] * 1ll * w % P, f[k] = (u + v) % P,
        f[k + j] = (u - v + P) % P;
      }
    }
  }
  if (tag == -1)
    for (int i = 0, in = pw(len, P - 2); i < len; ++i) f[i] = f[i] * 1ll * in % P;
}

typedef vector<int> poly;
poly operator+(const poly a, const poly b) {
  poly c(a);
  c.resize(max(a.size(), b.size()), 0);
  for (unsigned long i = 0; i < b.size(); i++) c[i] = (c[i] + b[i]) % P;
  return c;
}
int t1[SZ], t2[SZ];
poly operator*(const poly a, const poly b) {
  if (a.empty()) return a;
  if (b.empty()) return b;
  int len = init(a.size() + b.size());
  fill(t1, t1 + len, 0);
  fill(t2, t2 + len, 0);
  for (unsigned long i = 0; i < a.size(); i++) t1[i] = a[i];
  for (unsigned long i = 0; i < b.size(); i++) t2[i] = b[i];
  dft(t1, len, 1);
  dft(t2, len, 1);
  FOR(i, 0, len - 1) t1[i] = t1[i] * 1ll * t2[i] % P;
  dft(t1, len, -1);
  poly c(t1, t1 + len);
  while (c.back() == 0) c.pop_back();
  return c;
}

int n, m, a[N];
vector<int> g[N];
poly f[N][2];

void dfs(int u, int p) {
  // printf("dfs %d %d\n",u,p);
  f[u][1].resize(2, 0);
  f[u][1][1] = a[u];
  f[u][0].resize(1, 1);
  for (int v : g[u])
    if (v != p) {
      dfs(v, u);
      f[u][0] = f[u][0] * (f[v][0] + f[v][1]);
      poly t = f[u][1] * f[v][0];
      f[u][1] = t;
    }
  // printf("f[%d][0]: ",u); for(auto x:f[u][0])cout<<x<<" "; cout<<endl;
  // printf("f[%d][1]: ",u); for(auto x:f[u][1])cout<<x<<" "; cout<<endl;
}

int main() {
  scanf("%d%d", &n, &m);
  FOR(i, 1, n) scanf("%d", &a[i]);
  FOR(i, 1, n - 1) {
    int x, y;
    scanf("%d%d", &x, &y);
    g[x].pb(y);
    g[y].pb(x);
  }
  dfs(1, 0);
  while (f[1][0].size() <= n) f[1][0].pb(0);
  while (f[1][1].size() <= n) f[1][1].pb(0);
  int ans = (f[1][0][m] + f[1][1][m]) % P;
  printf("%d\n", ans);
  return 0;
}
