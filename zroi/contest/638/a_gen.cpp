// by Sshwy
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

using namespace RA;
const int N = 3e5 + 5;
int n, a[N];
int main() {
  srand(clock() + time(0));
  int n = rnd(1, 20);
  FOR(i, 1, n) a[i] = i;
  random_shuffle(a + 1, a + n + 1);
  printf("%d\n", n);
  FOR(i, 1, n) printf("%d%c", a[i], " \n"[i == n]);
  return 0;
}
