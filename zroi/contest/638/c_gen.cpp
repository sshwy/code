// by Sshwy
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

using namespace RA;

int main() {
  srand(clock() + time(0));
  int n = rnd(1, 1000);
  int m = rnd(1, n);
  printf("%d %d\n", n, m);
  FOR(i, 1, n) printf("%d%c", rnd(998244353), " \n"[i == n]);
  FOR(i, 1, n - 1) printf("%d %d\n", rnd(1, i), i + 1);
  return 0;
}
