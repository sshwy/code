// by Sshwy
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA
const int N = 3e5 + 5, SZ = N << 2;
int n;
int a[N], pw[N], ipw[N];
int base, mod;
int s1[SZ], s2[SZ];
void assign(int pos, int v, int u = 1, int l = 1, int r = n) {
  if (l == r) {
    s1[u] = pw[l] * 1ll * v % mod;
    s2[u] = ipw[l] * 1ll * v % mod;
    return;
  }
  int mid = (l + r) >> 1;
  if (pos <= mid)
    assign(pos, v, u << 1, l, mid);
  else
    assign(pos, v, u << 1 | 1, mid + 1, r);
  s1[u] = (s1[u << 1] + s1[u << 1 | 1]) % mod;
  s2[u] = (s2[u << 1] + s2[u << 1 | 1]) % mod;
}
int qry1(int L, int R, int u = 1, int l = 1, int r = n) {
  if (R < l || r < L || L > R) return 0;
  if (L <= l && r <= R) return s1[u];
  int mid = (l + r) >> 1;
  return (qry1(L, R, u << 1, l, mid) + qry1(L, R, u << 1 | 1, mid + 1, r)) % mod;
}
int qry2(int L, int R, int u = 1, int l = 1, int r = n) {
  if (R < l || r < L || L > R) return 0;
  if (L <= l && r <= R) return s2[u];
  int mid = (l + r) >> 1;
  return (qry2(L, R, u << 1, l, mid) + qry2(L, R, u << 1 | 1, mid + 1, r)) % mod;
}
int hsh(int L, int R) { return qry1(L, R) * 1ll * pw[n - L] % mod; }
int rhsh(int L, int R) { return qry2(L, R) * 1ll * pw[R] % mod; }
int main() {
  srand(clock() + time(0));
  scanf("%d", &n);
  FOR(i, 1, n) scanf("%d", &a[i]);

  base = RA::rnd(3, 10);
  mod = RA::rnd(1e8, 1e9);
  pw[0] = 1;
  FOR(i, 1, n) pw[i] = pw[i - 1] * 1ll * base % mod;
  FOR(i, 0, n) ipw[i] = pw[n - i];

  FOR(i, 1, n) assign(a[i], 1); // right
  FOR(i, 1, n) {
    assign(a[i], 0);
    int t = min(a[i] - 1, n - a[i]);
    if (hsh(a[i] - t, a[i] + t) != rhsh(a[i] - t, a[i] + t)) {
      puts("YES");
      return 0;
    }
  }
  puts("NO");
  return 0;
}
