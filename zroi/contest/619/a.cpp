// by Sshwy
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA
const int N = 1e5 + 5;
int n, m;

set<int> a, b;

int main() {
  scanf("%d%d", &n, &m);
  FOR(i, 1, m) {
    int x, y;
    scanf("%d%d", &x, &y);
    a.insert(x + y);
    b.insert(x - y);
  }
  int cur = 0;
  long long ans = 0;
  for (int i = 1 - n; i <= 0; i += 2) {
    // 2-i, i+n+n
    if (a.find(2 - i) == a.end()) ++cur;
    if (2 - i != i + n + n && a.find(i + n + n) == a.end()) ++cur;
    if (b.find(i) == b.end()) ans += cur;
  }
  cur = 0;
  for (int i = 1 - n + 1; i <= 0; i += 2) {
    // 2-i, i+n+n
    if (a.find(2 - i) == a.end()) ++cur;
    if (2 - i != i + n + n && a.find(i + n + n) == a.end()) ++cur;
    if (b.find(i) == b.end()) ans += cur;
  }
  cur = 0;
  for (int i = n - 1; i >= 1; i -= 2) {
    // i+2, n+n-i
    if (a.find(i + 2) == a.end()) ++cur;
    if (i + 2 != n + n - i && a.find(n + n - i) == a.end()) ++cur;
    if (b.find(i) == b.end()) ans += cur;
  }
  cur = 0;
  for (int i = n - 1 - 1; i >= 1; i -= 2) {
    // i+2, n+n-i
    if (a.find(i + 2) == a.end()) ++cur;
    if (i + 2 != n + n - i && a.find(n + n - i) == a.end()) ++cur;
    if (b.find(i) == b.end()) ans += cur;
  }
  printf("%lld\n", ans);
  return 0;
}
