// by Sshwy
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA
const int N = 2600, P = 1e9 + 7;

int pw(int a, int m) {
  int res = 1;
  while (m) m & 1 ? res = 1ll * res * a % P : 0, a = 1ll * a * a % P, m >>= 1;
  return res;
}
int n, m;

struct qxx {
  int nex, t, v;
} e[N * 2];
int h[N], le = 1;
void add_path(int f, int t, int v) { e[++le] = {h[f], t, v}, h[f] = le; }
int sz[N], fac[N], fnv[N], f[N];
int ans;
int binom(int a, int b) {
  if (a < b) return 0;
  return fac[a] * 1ll * fnv[b] % P * fnv[a - b] % P;
}
void calc(int x, int y, int w) {
  int ix = pw(x, P - 2);
  long long coef = 1, s = 0;
  FOR(i, 0, m) {
    s = (s + coef * (m - i) % P * f[m - i]) % P;
    coef = coef * ix % P * y % P;
  }

  swap(x, y);
  ix = pw(x, P - 2);
  coef = 1;
  FOR(i, 1, m) {
    coef = coef * ix % P * y % P;
    s = (s + coef * (m - i) % P * f[m - i]) % P;
  }

  s = s * pw(x, m) % P * pw(y, m) % P;
  ans = (ans + s * w) % P;
}
void dfs(int u, int p, int val) {
  sz[u] = 1;
  for (int i = h[u]; i; i = e[i].nex) {
    int v = e[i].t, w = e[i].v;
    if (v != p) dfs(v, u, w), sz[u] += sz[v];
  }
  if (p && val) calc(sz[u], n - sz[u], val);
}
int main() {
  scanf("%d%d", &n, &m);
  FOR(i, 1, n - 1) {
    int u, v, w;
    scanf("%d%d%d", &u, &v, &w);
    add_path(u, v, w);
    add_path(v, u, w);
  }
  fac[0] = 1;
  FOR(i, 1, m) fac[i] = fac[i - 1] * 1ll * i % P;
  fnv[m] = pw(fac[m], P - 2);
  ROF(i, m, 1) fnv[i - 1] = fnv[i] * 1ll * i % P;
  FOR(i, 0, m) {
    int s = 0;
    FOR(j, 0, m - i) { s = (s + binom(m, j) * 1ll * binom(m, i + j)) % P; }
    f[m - i] = s;
  }
  dfs(1, 0, 0);
  printf("%d\n", ans);
  return 0;
}
