// by Sshwy
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA
const int SZ = 1 << 22, K = 15, P = 998244353;

int pw(int a, int m) {
  int res = 1;
  while (m) m & 1 ? res = 1ll * res * a % P : 0, a = 1ll * a * a % P, m >>= 1;
  return res;
}
int tr[SZ];
int init(int n) {
  int len = 1;
  while (len < n) len <<= 1;
  FOR(i, 0, len) tr[i] = tr[i >> 1] >> 1 | (i & 1) * (len >> 1);
  return len;
}
void dft(int *f, int len, int tag) {
  FOR(i, 0, len - 1) if (tr[i] < i) swap(f[i], f[tr[i]]);
  for (int j = 1; j < len; j <<= 1) {
    for (int i = 0, wn = pw(3, P - 1 + (P - 1) / (j << 1) * tag); i < len;
         i += j << 1) {
      for (int k = i, u, v, w = 1; k < i + j; k++, w = w * 1ll * wn % P) {
        u = f[k], v = f[k + j] * 1ll * w % P;
        f[k] = (u + v) % P, f[k + j] = (u - v + P) % P;
      }
    }
  }
  if (tag == -1)
    for (int i = 0, in = pw(len, P - 2); i < len; i++) f[i] = f[i] * 1ll * in % P;
}

int n, k, m;
int g[K];

int f[1 << K][K];
int G[1 << K][K + 1];
int h[SZ];

void add(int &x, int y) { x = (x + y) % P; }
int main() {
  scanf("%d%d%d", &n, &k, &m);
  FOR(i, 1, m) {
    int u, v;
    scanf("%d%d", &u, &v);
    --u, --v;
    g[u] |= 1 << v;
    g[v] |= 1 << u;
  }
  int lim = 1 << k;
  FOR(i, 0, k - 1) f[1 << i][i] = 1;
  FOR(s, 0, lim - 1) if ((s & -s) != s) {
    FOR(i, 0, k - 1) if (s >> i & 1) {
      FOR(j, 0, k - 1) if ((s >> j & 1) && (g[i] >> j & 1)) {
        add(f[s][i], P - f[s ^ (1 << i)][j]); // coef of in ex
      }
    }
  }
  FOR(s, 0, lim - 1) FOR(i, 0, k - 1) add(G[s][1], f[s][i]);

  FOR(i, 2, k) {
    FOR(s, 0, lim - 1) {
      for (int t = s; t; t = (t - 1) & s) {
        if ((t & -t) == (s & -s)) {
          add(G[s][i], G[s ^ t][i - 1] * 1ll * G[t][1] % P);
        }
      }
    }
  }

  // FOR(i,0,k)printf("G[%d]=%d\n",i,G[lim-1][i]);
  FOR(i, 0, k) h[i] = G[lim - 1][i];
  int lh = k * n + 1;

  int len = init(lh);
  dft(h, len, 1);
  FOR(i, 0, len - 1) h[i] = pw(h[i], n);
  dft(h, len, -1);
  int ans = 0, fac = 1;
  FOR(i, 1, len - 1) {
    fac = fac * 1ll * i % P;
    add(ans, h[i] * 1ll * fac % P);
  }
  printf("%d\n", ans);
  return 0;
}
