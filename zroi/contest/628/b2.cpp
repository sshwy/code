// by Sshwy
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA
const int N = 1e5 + 5, P = 1e9 + 7;

void get_next(char *s, int *nex, int n) {
  nex[0] = -1;
  nex[1] = 0;
  int j = 0;
  FOR(i, 2, n) {
    while (j + 1 && s[j + 1] != s[i]) j = nex[j];
    ++j;
    nex[i] = j;
  }
  // FOR(i,1,n)printf("%d%c",nex[i]," \n"[i==n]);
}

int n;
char s[N];
int nex[N], f[N], ans[N];

int main() {
  scanf("%d", &n);
  scanf("%s", s + 1);
  reverse(s + 1, s + n + 1);
  FOR(i, 1, n) {
    get_next(s + n - i, nex, n);
    FOR(j, 0, i) f[j] = 0;
    f[0] = -1;
    FOR(j, 1, i) f[j] = f[nex[j]] + 1;
    FOR(j, 1, i) ans[i] = (ans[i] + f[j]) % P;
  }
  FOR(i, 1, n) ans[i] = (ans[i] + ans[i - 1]) % P;
  FOR(i, 1, n) ans[i] = (ans[i] + ans[i - 1]) % P;
  FOR(i, 1, n) printf("%d\n", ans[i]);
  return 0;
}
