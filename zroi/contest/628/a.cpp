// by Sshwy
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA
const int N = 2e5 + 5, P = 66662333;
int n;
vector<pair<int, int>> v;
int a[N], b[N], f[N], mxl[N];
vector<int> e[N];

int main() {
  scanf("%d", &n);
  FOR(i, 1, n) {
    int a, b;
    scanf("%d%d", &a, &b);
    v.pb({a, b});
  }
  sort(v.begin(), v.end());
  v.insert(v.begin(), {0, 0});
  FOR(i, 1, n) a[i] = v[i].second, b[i] = a[i];
  b[n + 1] = 1e9;
  FOR(i, 1, n) a[i] = max(a[i], a[i - 1]);
  ROF(i, n, 1) b[i] = min(b[i], b[i + 1]);

  // for(auto pr:v)printf("(%d,%d) ",pr.first,pr.second); puts("");

  int t = 0;
  FOR(i, 1, n) {
    int L = lower_bound(a + 1, a + n + 1, v[i].second) - a;
    int R = upper_bound(b + 1, b + n + 1, v[i].second) - b - 1;
    assert(L <= R);
    mxl[R + 1] = max(mxl[R + 1], L);
    e[R].pb(L);
    t = max(t, L);
    // printf("%d %d\n",L,R);
  }
  FOR(i, 1, n) mxl[i] = max(mxl[i], mxl[i - 1]);
  f[0] = 1;
  FOR(i, 1, n) {
    f[i] = (f[i - 1] - (mxl[i] ? f[mxl[i] - 1] : 0) + P) % P;
    // printf("f[%d]=%d\n",i,f[i]);
    f[i] = (f[i] + f[i - 1]) % P;
  }
  int ans = (f[n] - (t ? f[t - 1] : 0) + P) % P;
  printf("%d\n", ans);
  return 0;
}
