// by Sshwy
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA
const int N = 2e5 + 5, P = 1e9 + 7, ALP = 26;

int n;
char s[N];
int ans[N], nd[N];

int len[N], fail[N], tr[N][ALP], tot = 1, lst = 1;
void insert(char c) {
  c -= 'a';
  int u = ++tot, p = lst;
  len[u] = len[p] + 1;
  while (p && tr[p][c] == 0) tr[p][c] = u, p = fail[p];
  if (!p)
    fail[u] = 1;
  else {
    int q = tr[p][c];
    if (len[q] == len[p] + 1)
      fail[u] = q;
    else {
      int cq = ++tot;
      len[cq] = len[p] + 1, fail[cq] = fail[q];
      memcpy(tr[cq], tr[q], sizeof(tr[q]));
      fail[q] = fail[u] = cq;
      while (p && tr[p][c] == q) tr[p][c] = cq, p = fail[p];
    }
  }
  lst = u;
}

vector<int> g[N];
int sz[N], big[N], fa[N], top[N], id[N], totid, z[N];
void dfs1(int u, int p) {
  sz[u] = 1, fa[u] = p;
  for (auto v : g[u])
    if (v != p)
      dfs1(v, u), sz[u] += sz[v], big[u] = big[u] && sz[big[u]] > sz[v] ? big[u] : v;
}
void dfs2(int u, int p, int tp) {
  top[u] = tp, id[u] = ++totid, z[totid] = fa[u] ? len[u] - len[fail[u]] : 0;
  if (big[u]) dfs2(big[u], u, tp);
  for (auto v : g[u])
    if (v != big[u] && v != p) dfs2(v, u, v);
}

namespace seg {
  const int SZ = N << 2;
  long long w[SZ], s[SZ], tag[SZ];
  void pushup(int u) {
    w[u] = (w[u << 1] + w[u << 1 | 1]) % P;
    s[u] = (s[u << 1] + s[u << 1 | 1]) % P;
  }
  void build(int u, int l, int r) {
    if (l == r) return w[u] = z[l], void();
    int mid = (l + r) >> 1;
    build(u << 1, l, mid), build(u << 1 | 1, mid + 1, r);
    pushup(u);
  }
  void ndadd(int u, int v) {
    s[u] = (s[u] + w[u] * v) % P;
    tag[u] = (tag[u] + v) % P;
  }
  void pushdown(int u) {
    if (tag[u]) ndadd(u << 1, tag[u]), ndadd(u << 1 | 1, tag[u]), tag[u] = 0;
  }
  void add(int L, int R, int v, int u = 1, int l = 1, int r = totid) {
    if (R < l || r < L || L > R) return;
    if (L <= l && r <= R) return ndadd(u, v), void();
    int mid = (l + r) >> 1;
    pushdown(u);
    add(L, R, v, u << 1, l, mid);
    add(L, R, v, u << 1 | 1, mid + 1, r);
    pushup(u);
  }
  long long qry(int L, int R, int u = 1, int l = 1, int r = totid) {
    if (R < l || r < L || L > R) return 0;
    if (L <= l && r <= R) return s[u];
    int mid = (l + r) >> 1;
    pushdown(u);
    return (qry(L, R, u << 1, l, mid) + qry(L, R, u << 1 | 1, mid + 1, r)) % P;
  }
} // namespace seg

void add(int u) {
  while (u) seg::add(id[top[u]], id[u], 1), u = fa[top[u]];
}
int qry(int u) {
  int res = 0;
  while (u) res = (res + seg::qry(id[top[u]], id[u], 1)) % P, u = fa[top[u]];
  return res;
}
int main() {
  scanf("%d", &n);
  scanf("%s", s + 1);

  FOR(i, 1, n) {
    insert(s[i]);
    nd[i] = lst;
  }
  FOR(i, 1, tot) if (fail[i]) g[fail[i]].pb(i);

  dfs1(1, 0);
  dfs2(1, 0, 1);

  seg::build(1, 1, totid);

  FOR(i, 1, n) {
    ans[i] = qry(nd[i]);
    add(nd[i]);
  }

  FOR(i, 1, n) ans[i] = (ans[i] + ans[i - 1]) % P;
  FOR(i, 1, n) ans[i] = (ans[i] + ans[i - 1]) % P;
  FOR(i, 1, n) printf("%d\n", ans[i]);
  return 0;
}
