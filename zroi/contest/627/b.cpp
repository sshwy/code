// by Sshwy
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA
const int N = 2e5 + 5;
int n, m;
long long a[N], v[N], b[N], w[N];
vector<pair<long long, long long>> c[2];
void add(long long pos, long long v) {
  // printf("add %lld %lld\n",pos,v);
  c[pos & 1].pb({pos, v});
  // c[pos]+=v,c[pos+2]+=v,c[pos+4]+=v,...
}
void add2(long long L, long long R, long long v) {
  add(L, v);
  add(L + (R - L) / 2 * 2 + 2, -v);
}
void add1(long long L, long long R, long long v) {
  add2(L, R, v);
  if (L < R) add2(L + 1, R, v);
}
long long calc(vector<pair<long long, long long>> &V) {
  if (V.empty()) return 0;
  sort(V.begin(), V.end());
  // for(auto pr:V)printf("(%lld,%lld) ",pr.first,pr.second); puts("");
  int pos = -1;
  long long cur = 0;
  long long res = 0;
  while (pos + 1 < V.size()) {
    if (V[pos].first != V[pos + 1].first) { res = max(res, cur); }
    ++pos;
    cur += V[pos].second;
  }
  res = max(res, cur);
  return res;
}
void go() {
  c[0].clear();
  c[1].clear();
  scanf("%d", &n);
  FOR(i, 1, n) {
    scanf("%lld%lld", &v[i], &a[i]);
    a[i] += a[i - 1];
  }
  scanf("%d", &m);
  FOR(i, 1, m) {
    scanf("%lld%lld", &w[i], &b[i]);
    b[i] += b[i - 1];
  }
  // assert(a[n]==b[m]);
  int p1 = 0, p2 = 0;
  long long cx = 0, cy = 0;
  vector<pair<long long, long long>> ps;
  ps.pb({cx, cy});
  while (p1 <= n && p2 <= m) {
    assert(cx == min(a[p1], b[p2]));
    if (a[p1] == b[p2])
      ++p1, ++p2;
    else if (a[p1] < b[p2])
      ++p1;
    else
      ++p2;
    if (p1 > n || p2 > m) break;
    long long cv = v[p1] - w[p2];
    long long nx = min(a[p1], b[p2]);
    cy += (nx - cx) * cv;
    cx = nx;
    ps.pb({cx, cy});
  }
  assert(p1 > n && p2 > m);
  // for(auto pr:ps)printf("(%lld,%lld) ",pr.first,pr.second); puts("");
  add1(0, 0, 1);
  for (int i = 1; i < ps.size(); ++i) {
    // printf("i=%d\n",i);
    if (ps[i - 1].second == ps[i].second) {
      add1(ps[i].second, ps[i].second, ps[i].first - ps[i - 1].first);
    } else {
      int k = (ps[i].second - ps[i - 1].second) / (ps[i].first - ps[i - 1].first);
      k = abs(k);
      if (ps[i - 1].second < ps[i].second) {
        if (k == 1) {
          add1(ps[i - 1].second + 1, ps[i].second, 1);
        } else {
          assert(k == 2);
          add2(ps[i - 1].second + 2, ps[i].second, 1);
        }
      } else {
        if (k == 1) {
          add1(ps[i].second, ps[i - 1].second - 1, 1);
        } else {
          assert(k == 2);
          add2(ps[i].second, ps[i - 1].second - 2, 1);
        }
      }
    }
  }
  long long ans = max(calc(c[0]), calc(c[1]));
  printf("%lld\n", ans);
}
int main() {
  int t;
  scanf("%d", &t);
  FOR(i, 1, t) go();
  return 0;
}
