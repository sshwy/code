// by Sshwy
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA
const int P = 998244353, MT = 8 * 15 + 10;
int c, k;

vector<vector<int>> as;
vector<int> val_as, size_as;

void dfs(int s, vector<int> v) {
  if (s == 0) {
    vector<int> t(4, 0);
    int cur = 0;
    for (auto x : v) {
      FOR(i, 0, 3) if (x >> i & 1) t[i] = cur;
      ++cur;
    }
    as.pb(t);
    return;
  }
  for (int i = s; i; i = (i - 1) & s) {
    if ((i & -i) == (s & -s)) {
      auto nv = v;
      nv.pb(i);
      dfs(s - i, nv);
    }
  }
}

int f[20];
int down_power(int a, int b) {
  if (a < 0) return 0;
  int res = 1;
  FOR(i, 0, b - 1) res = res * 1ll * (a - i) % P;
  return res;
}
void upd(vector<int> pi, vector<int> qi) {
  set<int> s;
  for (auto x : pi) s.insert(x);
  int X = s.size(), Y = 0; // x: fixed color; y: not matched
  for (auto x : qi) {
    if (s.find(x) == s.end()) ++Y;
    s.insert(x);
  }
  int val = (pi[0] == qi[0]) + (pi[1] == qi[1]) + (pi[2] == qi[2]) + (pi[3] == qi[3]);
  f[val] = (f[val] + down_power(c - X, Y)) % P;
}
void dfs(vector<int> vp, vector<int> vq, map<int, int> pr, vector<int> pi,
    vector<int> qi) {
  if (vq.empty()) {
    for (auto &x : qi)
      if (pr.count(x)) x = pr[x];
    upd(pi, qi);
    return;
  }
  auto nq = vq;
  nq.erase(nq.begin());
  dfs(vp, nq, pr, pi, qi);
  for (int i = 0; i < vp.size(); ++i) {
    int x = vp[i];
    auto np = vp;
    auto nr = pr;
    np.erase(np.begin() + i);
    nr[*vq.begin()] = x;
    dfs(np, nq, nr, pi, qi);
  }
}
void calc(vector<int> pi, vector<int> qi) { // pi: i; qi: i-1
  for (auto &x : qi) x += 4;
  auto vp = pi, vq = qi;

  sort(vp.begin(), vp.end());
  vp.erase(unique(vp.begin(), vp.end()), vp.end());
  sort(vq.begin(), vq.end());
  vq.erase(unique(vq.begin(), vq.end()), vq.end());

  dfs(vp, vq, map<int, int>(), pi, qi);
}

int id(int x, int y) {
  assert(y < 15);
  return x * 16 + y;
}
int a[MT][MT];
void add(int &x, int y) { x = (x + y) % P; }

struct Matrix {
  int w, h;
  int c[MT][MT];
  Matrix(int _h = 0, int _w = 0) {
    w = _w, h = _h;
    FOR(i, 0, h - 1) FOR(j, 0, w - 1) c[i][j] = 0;
  }
  Matrix(int _h, int _w, int _c[MT][MT]) {
    w = _w, h = _h;
    FOR(i, 0, h - 1) FOR(j, 0, w - 1) c[i][j] = _c[i][j];
  }
  Matrix(int _w) {
    w = h = _w;
    FOR(i, 0, h - 1) FOR(j, 0, w - 1) c[i][j] = 0;
    FOR(i, 0, h - 1) c[i][i] = 1;
  }
  Matrix operator*(const Matrix &m) {
    assert(w == m.h);
    Matrix res(h, m.w);
    FOR(i, 0, h - 1) FOR(k, 0, w - 1) FOR(j, 0, m.w - 1) {
      res.c[i][j] = (res.c[i][j] + c[i][k] * 1ll * m.c[k][j]) % P;
    }
    return res;
  }
  Matrix &operator*=(const Matrix &m) {
    *this = *this * m;
    return *this;
  }
};

int main() {
  scanf("%d%d", &c, &k);
  dfs((1 << 4) - 1, vector<int>());
  // for(auto x:as){ for(auto i:x)printf("%d ",i); puts(""); }
  for (int i = 0; i < as.size(); ++i) {
    int val = (as[i][0] == as[i][1]) + (as[i][1] == as[i][2]) +
              (as[i][2] == as[i][3]) + (as[i][3] == as[i][0]);

    auto vp = as[i];
    sort(vp.begin(), vp.end());
    vp.erase(unique(vp.begin(), vp.end()), vp.end());

    val_as.pb(val);
    size_as.pb(vp.size());
  }
  for (int i = 0; i < as.size(); ++i) {
    for (int j = 0; j < as.size(); ++j) {
      // printf("i %d j %d\n",i,j);
      memset(f, 0, sizeof(f));
      calc(as[i], as[j]);
      // FOR(k,0,19)printf("%d%c",f[k]," \n"[k==19]);
      FOR(t, 0, k - val_as[i]) {
        FOR(e, 0, 19) if (f[e] && t + e + val_as[i] <= k) {
          // (t,j) => (t+e+val_as[i],i)
          add(a[id(t + e + val_as[i], i)][id(t, j)], f[e]);
        }
      }
    }
  }
  // coef for sum
  int sid = id(k, 14) + 1;
  FOR(i, 0, k) FOR(j, 0, 14) { a[sid][id(i, j)] = down_power(c, size_as[j]); }
  a[sid][sid] = 1;

  // FOR(i,0,sid)FOR(j,0,sid)printf("%d%c",a[i][j]," \n"[j==sid]);

  int f[MT][MT] = {{0}};

  // h=1
  FOR(j, 0, 14) { f[id(val_as[j], j)][0] = 1; }
  Matrix Mf(sid + 1, 1, f);
  Matrix Ma(sid + 1, sid + 1, a);
  vector<Matrix> va;

  va.pb(Ma);
  FOR(i, 1, 60) va.pb(va.back() * va.back());

  int q;
  scanf("%d", &q);
  FOR(_, 1, q) {
    long long h;
    scanf("%lld", &h);
    Matrix Mans = Mf;
    FOR(i, 0, 60) if (h >> i & 1) Mans = va[i] * Mans;
    printf("%d\n", Mans.c[sid][0]);
  }
  return 0;
}
