// by Sshwy
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

using namespace RA;
int p[100005];
void go() {
  int n = rnd(1, 100000);
  cout << n << endl;
  FOR(i, 1, n - 1) cout << i + 1 << " " << rnd(1, i) << endl;
  int a = rnd(1, min(n, 20)), b = rnd(1, min(n, 20));
  FOR(i, 1, n) p[i] = i;
  random_shuffle(p + 1, p + n + 1);
  cout << a << endl;
  FOR(i, 1, a) cout << p[i] << " " << rnd(0, n / 5000) << endl;
  random_shuffle(p + 1, p + n + 1);
  cout << b << endl;
  FOR(i, 1, b) cout << p[i] << " " << rnd(0, n / 1000) << endl;
}
int main() {
  srand(clock() + time(0));
  int t = 50;
  cout << t << endl;
  FOR(i, 1, t) go();
  return 0;
}
