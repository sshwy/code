// by Sshwy
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  long long rnd(long long p) {
    return __int128(rand()) * rand() % p * rand() % p * rand() % p;
  }
  long long rnd(long long L, long long R) { return rnd(R - L + 1) + L; }
} // namespace RA

using namespace RA;

vector<long long> split(long long n, int m) {
  vector<long long> v;
  FOR(i, 1, m - 1) v.pb(rnd(n + 1));
  v.pb(n);
  v.pb(0);
  sort(v.begin(), v.end());
  vector<long long> res;
  FOR(i, 1, m) res.pb(v[i] - v[i - 1]);
  return res;
}
void gen(int n, long long L) {
  auto a = split(L, n);
  printf("%d\n", n);
  FOR(i, 0, n - 1) printf("%lld %lld\n", rnd(-1, 1), a[i]);
}
void go() {
  long long L = 1e18;
  gen(rnd(1, 1e5), L);
  gen(rnd(1, 1e5), L);
}
int main() {
  srand(clock() + time(0));
  int t = 10;
  printf("%d\n", t);
  FOR(i, 1, t) go();
  return 0;
}
