// by Sshwy
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA
const int N = 1e5 + 5;
int n;
vector<int> g[N];
int L[N], R[N], a[N], b[N], sz[N];

bool dfs(int u, int p, const int k) {
  sz[u] = 1;
  L[u] = 0, R[u] = 1;
  for (int i = 0; i < g[u].size(); ++i) {
    int v = g[u][i];
    if (v != p) {
      if (dfs(v, u, k)) return 1;
      sz[u] += sz[v];
      L[u] += L[v];
      R[u] += R[v];
    }
  }
  L[u] = max(L[u], a[u]);
  R[u] = min(R[u], k - b[u]);
  R[u] = min(R[u], sz[u]);
  // printf("u=%d L=%d R=%d\n",u,L[u],R[u]);
  if (L[u] > R[u]) return 1;
  if (sz[u] + b[u] > n) return 1;
  if (u == 1 && (k < L[u] || R[u] < k)) return 1;
  return 0;
}
bool check(int k) {
  // printf("check %d\n",k);
  if (dfs(1, 0, k)) return 0;
  return 1;
}
void go() {
  scanf("%d", &n);

  fill(a, a + n + 1, 0);
  fill(b, b + n + 1, 0);
  FOR(i, 1, n) g[i].clear();

  FOR(i, 1, n - 1) {
    int u, v;
    scanf("%d%d", &u, &v);
    g[u].pb(v);
    g[v].pb(u);
    // printf("(%d,%d)\n",u,v);
  }
  int A, B;
  scanf("%d", &A);
  FOR(i, 1, A) {
    int r, s;
    scanf("%d%d", &r, &s);
    a[r] = s;
    // printf("a[%d]=%d\n",r,s);
  }
  scanf("%d", &B);
  FOR(i, 1, B) {
    int r, s;
    scanf("%d%d", &r, &s);
    b[r] = s;
    // printf("b[%d]=%d\n",r,s);
  }
  int l = 0, r = n + 1;
  while (l < r) {
    int mid = (l + r) >> 1;
    if (check(mid))
      r = mid;
    else
      l = mid + 1;
  }
  if (l == n + 1)
    puts("-1");
  else
    printf("%d\n", l);
}
int main() {
  int t;
  scanf("%d", &t);
  FOR(i, 1, t) go();
  return 0;
}
// fix the total number of black point.
// so type B change to subtree lim.
// binary search.
