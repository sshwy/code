// by Sshwy
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA
const int N = 1e5 + 5;
int n;
vector<int> g[N];
int a[N], b[N], s[N];

bool dfs(int u, int p, int k, int S) {
  s[u] = S >> (u - 1) & 1;
  for (int i = 0; i < g[u].size(); ++i) {
    int v = g[u][i];
    if (v != p) {
      if (dfs(v, u, k, S)) return 1;
      s[u] += s[v];
    }
  }
  if (s[u] < a[u]) return 1;
  if (k - s[u] < b[u]) return 1;
  return 0;
}
bool check(int S) {
  int k = __builtin_popcount(S);
  if (dfs(1, 0, k, S)) return 0;
  return 1;
}
void go() {
  scanf("%d", &n);

  fill(a, a + n + 1, 0);
  fill(b, b + n + 1, 0);
  FOR(i, 1, n) g[i].clear();

  FOR(i, 1, n - 1) {
    int u, v;
    scanf("%d%d", &u, &v);
    g[u].pb(v);
    g[v].pb(u);
  }
  int A, B;
  scanf("%d", &A);
  FOR(i, 1, A) {
    int r, s;
    scanf("%d%d", &r, &s);
    a[r] = s;
  }
  scanf("%d", &B);
  FOR(i, 1, B) {
    int r, s;
    scanf("%d%d", &r, &s);
    b[r] = s;
  }
  int ans = n + 1;
  FOR(i, 0, (1 << n) - 1) {
    if (check(i)) { ans = min(ans, __builtin_popcount(i)); }
  }
  if (ans == n + 1) ans = -1;
  printf("%d\n", ans);
}
int main() {
  int t;
  scanf("%d", &t);
  FOR(i, 1, t) go();
  return 0;
}
// fix the total number of black point.
// so type B change to subtree lim.
// binary search.
