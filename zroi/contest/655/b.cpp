// by Sshwy
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA
const int N = 2e5 + 5, P = 998244353;
int pw(int a, int m) {
  int res = 1;
  while (m) m & 1 ? res = 1ll * res * a % P : 0, a = 1ll * a * a % P, m >>= 1;
  return res;
}
int n, a[N], b[N];
int fac[N], fnv[N];

int binom(int a, int b) {
  if (a < b) return 0;
  return fac[a] * 1ll * fnv[b] % P * fnv[a - b] % P;
}
int main() {
  scanf("%d", &n);
  FOR(i, 1, n) scanf("%d%d", &a[i], &b[i]);

  fac[0] = 1;
  FOR(i, 1, n * 2) fac[i] = fac[i - 1] * 1ll * i % P;
  fnv[n * 2] = pw(fac[n * 2], P - 2);
  ROF(i, n * 2, 1) fnv[i - 1] = fnv[i] * 1ll * i % P;
  FOR(k, 0, n - 1) {
    int ans = 0;
    FOR(i, 1, n) ans = (ans + binom(a[i] + b[i] - k, a[i])) % P;
    printf("%d%c", ans, " \n"[k == n - 1]);
  }
  return 0;
}
