// by Sshwy
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA
const int N = 2e5 + 5;
int n, a[N];
long long b[N];

void go1() {
  map<long long, int> mp;
  mp[0] = 1;
  long long ans = 0;
  FOR(i, 1, n) {
    b[i] = (1ll << a[i]) + b[i - 1];
    for (int k = 1; (1ll << k) <= b[i]; k++) {
      long long x = b[i] - (1ll << k);
      if (mp.count(x)) ans += mp[x];
    }
    if (mp.count(b[i]))
      mp[b[i]]++;
    else
      mp[b[i]] = 1;
  }
  printf("%lld\n", ans);
}
int num[N * 2];
int tot1 = 0;
void add(int k) {
  while (num[k] == 1) num[k] = 0, ++k, tot1--;
  num[k] = 1;
  tot1++;
}
void go2() {
  long long ans = 0;
  FOR(i, 1, n) { // 以i为左端点
    fill(num + N - n - 1, num + N + n + 2, 0);
    tot1 = 0;
    FOR(j, i, n) {
      if (abs(a[j] - a[i]) > n) break;
      int k = N + a[j] - a[i];
      add(k);
      if (tot1 == 1) ++ans;
    }
  }
  printf("%lld\n", ans);
}
int main() {
  scanf("%d", &n);
  FOR(i, 1, n) scanf("%d", &a[i]);
  if (*max_element(a + 1, a + n + 1) <= 50)
    go1();
  else
    go2();
  return 0;
}
