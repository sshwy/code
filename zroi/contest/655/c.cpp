// by Sshwy
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA
const int N = 1e5 + 5, P = 998244353;
int pw(int a, int m) {
  int res = 1;
  while (m) m & 1 ? res = 1ll * res * a % P : 0, a = 1ll * a * a % P, m >>= 1;
  return res;
}

int n;
int a[N];
int fac[N], fnv[N];

int binom(int a, int b) {
  if (a < b) return 0;
  return fac[a] * 1ll * fnv[b] % P * fnv[a - b] % P;
}

int main() {
  scanf("%d", &n);

  fac[0] = 1;
  FOR(i, 1, n) fac[i] = fac[i - 1] * 1ll * i % P;
  fnv[n] = pw(fac[n], P - 2);
  ROF(i, n, 1) fnv[i - 1] = fnv[i] * 1ll * i % P;

  FOR(i, 1, n) scanf("%d", &a[i]);
  sort(a + 1, a + n + 1);
  int ans = 0;
  FOR(i, 1, n) { // 计算 a[i]的贡献次数
    // 比i大的有n-i个数
    FOR(j, n - i, n - 1) { // 枚举这个n-i个数占据的总长度
      int coef = 0;        // 占据总长度为j的方案数
      if (n - i == 0) {    // i 是最大的
        if (j == 0)
          coef = 1; //方案数为1
                    //否则方案不合法
      } else {
        // 如果是两边都有数的情况，就要求固定两个数的位置
        // 这样的情况有j-1种(j>=2)
        if (j >= 2 && n - i >= 2) {
          // 固定两个数，有n-i-2个数，放j-2个位置
          coef =
              (coef + binom(j - 2, n - i - 2) * 1ll * fac[n - i] % P * (j - 1)) % P;
        }
        // 否则，只有一边有数，则要求固定1个数的位置
        if (j >= 1 && n - i >= 1) {
          coef = (coef + binom(j - 1, n - i - 1) * 2ll % P * fac[n - i]) % P;
          // printf("%d,%d:%d %d\n",j-1,n-i-1,binom(j-1,n-i-1),fac[n-i]);
        }
      }
      // printf("i %d j %d coef %d\n",i,j,coef);
      if (coef) {
        if (j * 2 >= n + 1) { // 我莫得选
                              // do nothing.
        } else {
          long long tot = 0;
          int rest = n + 1 - j * 2;
          //现在考虑，a[i]放置在长度为n-j的段中的位置
          //有j*2个点被占了，剩下n+1-j*2个点
          //我在第1,2,...,rest/2个位置上放的话，能占的点是k*2。否则固定为rest
          int lim = rest / 2;
          if (lim >= n - j) {
            tot = (1 + n - j) * 1ll * (n - j);
          } else if (lim * 2 >= n - j) {
            int x = (n - j) / 2;
            tot = (1 + x) * 1ll * x * 2;
            if (x * 2 < n - j) tot += (x + 1) * 2;
          } else {
            tot = (1 + lim) * 1ll * lim * 2 + (n - j - lim * 2) * 1ll * rest;
          }
          tot /= 2;
          tot %= P;
          if (tot) {
            // printf("rest = %d\n",rest);
            // printf("%d * %lld * %d\n",coef,tot,a[i]);
            // 比a[i]小的数的放置方式就是阶乘
            ans = (ans + coef * tot % P * fac[i - 1] % P * a[i]) % P;
          }
        }
      }
    }
  }
  printf("%d\n", ans);
  return 0;
}
