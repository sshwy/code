// by Sshwy
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA
const int N = 505;
int n, m, k;
int a[N][N], b[N][N], c[N][N], b2[N][N];
long long tot, ans;

bool check() {
  FOR(i, 1, n) FOR(j, 1, m) if (a[i][j] != b[i][j]) return 1;
  return 0;
}
priority_queue<pair<int, pair<int, int>>> q;
int dir[][2] = {{0, 1}, {1, 0}, {0, -1}, {-1, 0}};
bool vis[N][N];
long long calc(int sx, int sy) {
  FOR(i, 1, n) FOR(j, 1, m) vis[i][j] = 0, c[i][j] = 1e6 + 1;
  q.push({-a[sx][sy], {sx, sy}});
  c[sx][sy] = a[sx][sy];
  while (!q.empty()) {
    pair<int, pair<int, int>> u = q.top();
    q.pop();
    int x = u.second.first, y = u.second.second;
    if (vis[x][y]) continue;
    vis[x][y] = 1;
    FOR(i, 0, 3) {
      int nx = x + dir[i][0], ny = y + dir[i][1];
      if (vis[nx][ny]) continue;
      int v = max(c[x][y], a[nx][ny]);
      if (v < c[nx][ny]) {
        c[nx][ny] = v;
        q.push({-v, {nx, ny}});
      }
    }
  }
  long long res = 0;
  FOR(i, 1, n) FOR(j, 1, m) {
    assert(a[i][j] <= c[i][j]);
    res += max(b[i][j] - c[i][j], 0);
  }
  // printf("calc %d %d\n",sx,sy);
  // FOR(i,1,n)FOR(j,1,m)printf("%d%c",c[i][j]," \n"[j==m]);
  // printf("res %lld \n",res);
  return res;
}
int main() {
  scanf("%d%d%d", &n, &m, &k);
  FOR(i, 1, n) {
    FOR(j, 1, m) {
      scanf("%d", &a[i][j]);
      b[i][j] = 1e6 + 1;
      tot += b[i][j] - a[i][j];
    }
  }
  while (check() && k) {
    --k;
    long long res = 0;
    FOR(i, 1, n) FOR(j, 1, m) {
      long long t = calc(i, j);
      if (res < t) {
        res = t;
        FOR(i, 1, n) FOR(j, 1, m) b2[i][j] = c[i][j];
      }
    }
    tot -= res;
    // printf("\033[32mcur %lld\033[0m\n",tot);
    ans ^= tot;
    FOR(i, 1, n) FOR(j, 1, m) b[i][j] = min(b[i][j], b2[i][j]);
    // FOR(i,1,n)FOR(j,1,m)printf("%d%c",b[i][j]," \n"[j==m]);
  }
  printf("%lld\n", ans);
  return 0;
}
