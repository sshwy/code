// by Sshwy
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA
const int N = 505;
int n, m, k;
int a[N][N], b[N][N]; // b: 水位
bool check() {
  FOR(i, 1, n) FOR(j, 1, m) if (a[i][j] != b[i][j]) return 1;
  return 0;
}
pair<int, pair<int, int>> s[N * N];
int ls, f[N * N], p[N * N], g[N * N], h[N * N], vis[N * N], occur[N][N];
// f: 并查集, p: kruskal 重构树的指针, g: 权值
int id(int x, int y) { return (x - 1) * m + y - 1; }
int dir[][2] = {0, 1, 0, -1, 1, 0, -1, 0};
int get(int u) { return f[u] == u ? u : f[u] = get(f[u]); }
void merge(int x, int y, int d) {
  int u = id(x, y);
  assert(f[u] == u);
  FOR(i, 0, 3) {
    int nx = x + dir[i][0], ny = y + dir[i][1];
    if (1 <= nx && nx <= n && 1 <= ny && ny <= m && a[nx][ny] != b[nx][ny] &&
        occur[nx][ny]) {
      int v = id(nx, ny);
      v = get(v);
      if (v != u) {
        p[v] = u;
        f[v] = u;
        h[u] += h[v];
      }
    }
  }
  g[u] = a[u / m + 1][u % m + 1];
}
long long c[N * N], tot, ans;
long long calc(int u) {
  if (vis[u]) return c[u];
  if (p[u] == u)
    c[u] = h[u] * 1ll * (b[u / m + 1][u % m + 1] - a[u / m + 1][u % m + 1]);
  else
    c[u] = h[u] * 1ll * (g[p[u]] - g[u]) + calc(p[u]);
  vis[u] = 1;
  return c[u];
}
void upd(int x, int y) {
  int u = id(x, y);
  if (vis[u] == 3) return;
  if (vis[u] == 2) return;
  if (p[u] == u) { // 这是一棵无关的树
    vis[u] = 3;
    return;
  }
  int px = p[u] / m + 1, py = p[u] % m + 1;
  upd(px, py);
  if (vis[p[u]] == 3) {
    vis[u] = 3;
    return;
  }
  // printf("u %d p %d vis[p] %d\n",u,p[u],vis[p[u]]);
  assert(vis[p[u]] == 2);
  b[x][y] = min(b[x][y], b[px][py]);
  b[x][y] = max(b[x][y], a[x][y]);
  vis[u] = 2;
}
int main() {
  scanf("%d%d%d", &n, &m, &k);
  FOR(i, 1, n) {
    FOR(j, 1, m) {
      scanf("%d", &a[i][j]);
      s[++ls] = {a[i][j], {i, j}};
      b[i][j] = 1e6 + 1;
      tot += b[i][j] - a[i][j];
    }
  }
  sort(s + 1, s + ls + 1);
  while (check() && k) {
    --k;
    FOR(i, 1, n) FOR(j, 1, m) {
      int u = id(i, j);
      f[u] = u, p[u] = u, g[u] = 0, h[u] = 1, vis[u] = 0;
      occur[i][j] = 0;
    }
    FOR(i, 1, ls) {
      int x = s[i].second.first, y = s[i].second.second;
      occur[x][y] = 1;
      assert(a[x][y] != b[x][y]);
      // printf("x %d y %d u %d f %d\n",x,y,id(x,y),f[id(x,y)]);
      // 涨水高度
      int d = i == ls ? b[x][y] - s[i].first : s[i + 1].first - s[i].first;
      merge(x, y, d); // 把(x,y)周围的都与x,y连起来，且系数为d
    }
    long long res = 0;
    int ux = -1, uy = -1;
    FOR(i, 1, ls) {
      int x = s[i].second.first, y = s[i].second.second;
      // int u = id(x,y);
      // printf("u %d p %d g %d h %d \n",u,p[u],g[u],h[u]);
      long long t = calc(id(x, y));
      if (res < t) {
        res = t;
        ux = x, uy = y;
      }
    }
    assert(ux != -1 && uy != -1);
    for (int u = id(ux, uy); true; u = p[u]) {
      vis[u] = 2; // 标记
      int x = u / m + 1, y = u % m + 1;
      b[x][y] = a[x][y]; // 干涸
      // printf("u %d x %d y %d\n",u,x,y);
      if (p[u] == u) break;
    }
    FOR(i, 1, ls) {
      int x = s[i].second.first, y = s[i].second.second;
      upd(x, y);
    }

    tot -= res;
    ans ^= tot;
    int n_ls = 0;
    FOR(i, 1, ls) {
      int x = s[i].second.first, y = s[i].second.second;
      if (a[x][y] == b[x][y])
        continue;
      else
        s[++n_ls] = s[i];
    }
    ls = n_ls;
    // fprintf(stderr,"%lld\n",tot);
    // FOR(i,1,n)FOR(j,1,m)printf("%d%c",b[i][j]," \n"[j==m]);
  }
  printf("%lld\n", ans);
  return 0;
}
/*
 * 从高到低考虑每个格子(x,y)
 *   如果(x,y)已经干涸，就跳过
 *   考虑和(x,y)连通的那些格子，集合为S
 *   则(x,y)上方的水，都可以被S中的吸收。即把S打答案加上某一个定值
 *   而当吸干了(x,y)后，(x,y)就成为了障碍（干涸）
 *
 * 发现这个过程可以离线
 *
 * 倒过来考虑，问题就变成了
 *   把(x,y)加入到S中
 *   把S的每个元素的权值加上X
 *   kruskal重构树即可
 */
