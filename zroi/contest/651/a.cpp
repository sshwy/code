// by Sshwy
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA
const int N = 515;
int n, m, k;
int a[N][N], b[N][N]; // b: 水位
int dir[][2] = {0, 1, 0, -1, 1, 0, -1, 0};
pair<int, pair<int, int>> s[N * N];
int ls, f[N * N], p[N * N], g[N * N], occur[N][N];
// f: 并查集, p: kruskal 重构树的指针, g: 权值
long long c[N * N], tot, ans;
vector<int> G[N * N];

bool check() {
  FOR(i, 1, n) FOR(j, 1, m) if (a[i][j] != b[i][j]) return 1;
  return 0;
}
int id(int x, int y) { return (x - 1) * m + y - 1; }
int get(int u) { return f[u] == u ? u : f[u] = get(f[u]); }
void merge(int x, int y) {
  int u = id(x, y);
  assert(f[u] == u);
  FOR(i, 0, 3) {
    int nx = x + dir[i][0], ny = y + dir[i][1];
    if (1 <= nx && nx <= n && 1 <= ny && ny <= m && a[nx][ny] != b[nx][ny] &&
        occur[nx][ny]) {
      int v = id(nx, ny);
      v = get(v);
      if (v != u) {
        p[v] = u;
        f[v] = u;
      }
    }
  }
  g[u] = a[x][y];
}
long long sc[N * N], sz[N * N];
bool not_top[N * N];
void dfs(int u) {
  sz[u] = 1;
  int big = -1;
  for (auto v : G[u]) {
    dfs(v), sz[u] += sz[v];
    if (big == -1 || sc[big] < sc[v]) big = v;
  }
  sc[u] = c[u] * 1ll * sz[u];
  if (big != -1) {
    not_top[big] = 1;
    sc[u] += sc[big];
  }
}
long long V[N * N], lv;
int main() {
  scanf("%d%d%d", &n, &m, &k);
  FOR(i, 1, n) {
    FOR(j, 1, m) {
      scanf("%d", &a[i][j]);
      s[++ls] = {a[i][j], {i, j}};
      b[i][j] = 1e6 + 1;
      tot += b[i][j] - a[i][j];
    }
  }
  sort(s + 1, s + ls + 1);
  FOR(i, 1, n) FOR(j, 1, m) {
    int u = id(i, j);
    f[u] = u, p[u] = u, g[u] = 0;
    occur[i][j] = 0;
  }
  FOR(i, 1, ls) {
    int x = s[i].second.first, y = s[i].second.second;
    occur[x][y] = 1;
    merge(x, y);
  }
  int root = -1;
  FOR(i, 1, ls) {
    int x = s[i].second.first, y = s[i].second.second;
    int u = id(x, y);
    if (p[u] == u) {
      assert(root == -1);
      root = u;
      c[u] = b[x][y] - a[x][y];
    } else {
      G[p[u]].pb(u);
      c[u] = g[p[u]] - g[u];
    }
    // printf("u %d p %d g %d h %d \n",u,p[u],g[u],h[u]);
  }
  dfs(root);
  FOR(i, 1, n) FOR(j, 1, m) {
    int u = id(i, j);
    if (not_top[u]) continue;
    V[++lv] = sc[u];
  }
  sort(V + 1, V + lv + 1);
  k = min(k, (int)lv);
  FOR(i, 1, k) {
    tot -= V[lv - i + 1];
    ans ^= tot;
  }
  printf("%lld\n", ans);
  return 0;
}
/*
 * 从高到低考虑每个格子(x,y)
 *   如果(x,y)已经干涸，就跳过
 *   考虑和(x,y)连通的那些格子，集合为S
 *   则(x,y)上方的水，都可以被S中的吸收。即把S打答案加上某一个定值
 *   而当吸干了(x,y)后，(x,y)就成为了障碍（干涸）
 *
 * 发现这个过程可以离线
 *
 * 倒过来考虑，问题就变成了
 *   把(x,y)加入到S中
 *   把S的每个元素的权值加上X
 *   kruskal重构树即可
 */
