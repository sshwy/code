// by Sshwy
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

using namespace RA;

int main() {
  srand(clock() + time(0));
  int n = rnd(1, 10), m = rnd(1, 10), k = rnd(1, n * m);
  printf("%d %d %d\n", n, m, k);
  FOR(i, 1, n) FOR(j, 1, m) printf("%d%c", rnd(1, 1e3), " \n"[j == m]);
  return 0;
}
