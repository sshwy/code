// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

const int Q = 22, P = 1e9 + 7, SZ = 1e7 + 5;
void add(int &x, int y) { x = (x + y) % P; }
int mul(int x, int y) { return x * 1ll * y % P; }
int pw(int a, int m) {
  int res = 1;
  while (m) m & 1 ? res = 1ll * res * a % P : 0, a = 1ll * a * a % P, m >>= 1;
  return res;
}

int n, m, c, l;
int a[Q];

bool co[SZ];
int pn[SZ], mu[SZ], p[SZ], e[SZ], ep[SZ], g[SZ], gs[SZ], lp;

int c1(int x) { return (pw(x + 1, m) - pw(x, m) + P) % P; }
int Fmu(int i, int t) {
  int res = 1;
  while (i > 1) {
    int coef = pw(p[i], e[i] - 1);
    coef = (pw(mul(coef, p[i]), t) + mul(pw(coef, t), P - 1)) % P;
    res = mul(res, coef);
    i /= ep[i];
  }
  return res;
}

void sieve(int lim) {
  co[0] = co[1] = true;
  mu[1] = 1, e[1] = 0, ep[1] = 1, g[1] = 1, p[1] = 1;
  FOR(i, 2, lim) {
    if (!co[i]) {
      pn[++lp] = i, mu[i] = -1, e[i] = 1, p[i] = i, ep[i] = i, g[i] = c1(e[i]);
    }
    FOR(j, 1, lp) {
      int pj = pn[j], x = i * pj;
      if (x > lim) break;
      co[x] = true;
      if (i % pj == 0) {
        mu[x] = 0, e[x] = e[i] + 1, p[x] = pj, ep[x] = ep[i] * pj;
        g[x] = mul(g[i / ep[i]], c1(e[x]));
        break;
      } else {
        mu[x] = -mu[i], e[x] = 1, p[x] = pj, ep[x] = pj;
        g[x] = mul(g[i], g[pj]);
      }
    }
  }
  gs[1] = mul(g[1], pw(1, c));
  FOR(i, 2, lim) gs[i] = (gs[i - 1] + mul(g[i], pw(i, c))) % P;
}

int ans;

int calc(int i, int fi) {
  int sum = 0;
  FOR(t, 1, n) {
    int coef = pw(t, c);
    coef = mul(Fmu(t, i), coef);
    coef = mul(coef, gs[n / t]);
    add(sum, coef);
  }
  sum = mul(sum, fi);
  // printf("calc(%d, %d) = %d\n", i, fi, sum);
  return sum;
}

int main() {
  scanf("%d%d%d%d", &n, &m, &c, &l);

  sieve(1e6 + 5);

  FOR(i, 0, l) {
    int fi;
    scanf("%d", &fi);
    add(ans, calc(l - i, fi));
  }

  printf("%d\n", ans);

  return 0;
}
