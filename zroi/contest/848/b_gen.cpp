// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

using namespace RA;

void go() {
  int n = rnd(1, 100), maxw = rnd(1, 100);
  printf("%d\n", n);
  FOR(i, 1, n - 1) printf("%d%c", rnd(1, maxw), " \n"[i == n - 1]);
  FOR(i, 1, n - 2) printf("%d%c", rnd(1, maxw), " \n"[i == n - 2]);
  FOR(i, 1, n - 3) printf("%d%c", rnd(1, maxw), " \n"[i == n - 3]);
}
int main() {
  srand(clock() + time(0));
  int t = 3;
  printf("%d\n", t);
  FOR(i, 1, t) go();
  return 0;
}
