// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

const int N = 1e5 + 5, INF = 2e9;
void getMin(int &x, int y) { x = min(x, y); }

int n, a[N], b[N], c[N];
long long ans;

void brute(int l, int r) {
  int dist[10];
  FOR(i, 0, r - l) {
    dist[i] = 0;
    FOR(j, i + 1, r - l) {
      dist[j] = INF;
      if (j - 1 >= i) getMin(dist[j], dist[j - 1] + a[j + l - 1]);
      if (j - 2 >= i) getMin(dist[j], dist[j - 2] + b[j + l - 2]);
      if (j - 3 >= i) getMin(dist[j], dist[j - 3] + c[j + l - 3]);
      ans += dist[j];
    }
  }
}
int dist[3][N];

void calc(int id, int start, int l, int r) {
  assert(l <= start && start <= r);
  int *d = dist[id];
  d[start] = 0;
  FOR(i, start + 1, r) {
    d[i] = INF;
    if (i - 1 >= start) getMin(d[i], d[i - 1] + a[i - 1]);
    if (i - 2 >= start) getMin(d[i], d[i - 2] + b[i - 2]);
    if (i - 3 >= start) getMin(d[i], d[i - 3] + c[i - 3]);
  }
  ROF(i, start - 1, l) {
    d[i] = INF;
    if (i + 1 <= start) getMin(d[i], d[i + 1] + a[i]);
    if (i + 2 <= start) getMin(d[i], d[i + 2] + b[i]);
    if (i + 3 <= start) getMin(d[i], d[i + 3] + c[i]);
  }
}
typedef long long LL;
struct Fenwick {
  LL c[N];
  void add(int pos, LL v) {
    assert(pos > 0 && pos < N);
    for (int i = pos; i < N; i += i & -i) c[i] += v;
  }
  LL qry(int pos) {
    if (pos == 0) return 0;
    assert(pos > 0 && pos < N);
    LL res = 0;
    for (int i = pos; i > 0; i -= i & -i) res += c[i];
    return res;
  }
} bSum, bCnt;
struct pt {
  int x, y, v, tag;
  bool operator<(const pt p) const {
    if (x != p.x) return x < p.x;
    if (y != p.y) return y < p.y;
    return tag < p.tag;
  }
  bool operator!=(const pt p) const { return x != p.x || y != p.y; }
};
void work(vector<pt> &A, bool allowEqual) {
  sort(A.begin(), A.end());

  vector<int> D;
  for (auto e : A) D.pb(e.y);
  sort(D.begin(), D.end());
  for (auto &e : A) e.y = lower_bound(D.begin(), D.end(), e.y) - D.begin() + 1;

  for (auto e : A) {
    if (e.tag == 0) {
      bSum.add(e.y, e.v);
      bCnt.add(e.y, 1);
    } else {
      ans += bSum.qry(e.y - 1) + bCnt.qry(e.y - 1) * 1ll * e.v;
    }
  }
  for (auto e : A) {
    if (e.tag == 0) {
      bSum.add(e.y, -e.v);
      bCnt.add(e.y, -1);
    }
  }
  if (allowEqual) {
    pt las = {INF, INF, 0, 0};
    LL sum[2] = {0}, cnt[2] = {0};
    for (auto e : A) {
      if (las != e) {
        ans += sum[0] * cnt[1];
        ans += sum[1] * cnt[0];
        sum[0] = sum[1] = 0;
        cnt[0] = cnt[1] = 0;
        las = e;
      }
      sum[e.tag] += e.v;
      cnt[e.tag]++;
    }
    ans += sum[0] * cnt[1];
    ans += sum[1] * cnt[0];
  }
}
void solve(int l, int r) {
  if (r - l + 1 <= 5) {
    brute(l, r);
    return;
  }
  int mid = (l + r) >> 1;
  solve(l, mid - 2);
  solve(mid + 2, r);

  calc(0, mid - 1, l, r);
  calc(1, mid, l, r);
  calc(2, mid + 1, l, r);

  FOR(i, 0, 2) {
    FOR(j, l, mid - 2) ans += dist[i][j];
    FOR(j, mid + 2, r) ans += dist[i][j];
  }

  ans += dist[0][mid] + dist[1][mid + 1] + dist[0][mid + 1];

  const int rx[] = {1, 2, 0};
  const int ry[] = {2, 0, 1};

  FOR(i, 0, 2) {
    int *dx = dist[rx[i]], *dy = dist[ry[i]], *d = dist[i];
    vector<pt> A;
    FOR(j, l, mid - 2) {
      pt po = {d[j] - dx[j], d[j] - dy[j], d[j], 0};
      A.pb(po);
    }
    FOR(j, mid + 2, r) {
      pt po = {dx[j] - d[j], dy[j] - d[j], d[j], 1};
      A.pb(po);
    }
    work(A, i == 0);
  }
}
void go() {
  ans = 0;
  scanf("%d", &n);
  FOR(i, 1, n - 1) scanf("%d", a + i);
  FOR(i, 1, n - 2) scanf("%d", b + i);
  FOR(i, 1, n - 3) scanf("%d", c + i);
  solve(1, n);
  printf("%lld\n", ans);
}

int main() {
  int t;
  scanf("%d", &t);
  FOR(i, 1, t) go();
  return 0;
}
