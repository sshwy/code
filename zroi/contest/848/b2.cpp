// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

const int N = 1e5 + 5, INF = 2e9;
void getMin(int &x, int y) { x = min(x, y); }

int n, a[N], b[N], c[N];

namespace s1 {
  int dist[N];
  void go() {
    // fprintf(stderr, "s1::go: ");
    long long ans = 0;
    FOR(i, 1, n) {
      dist[i] = 0;
      FOR(j, i + 1, n) {
        dist[j] = INF;
        if (j - 1 >= i) getMin(dist[j], dist[j - 1] + a[j]);
        if (j - 2 >= i) getMin(dist[j], dist[j - 2] + b[j]);
        if (j - 3 >= i) getMin(dist[j], dist[j - 3] + c[j]);
        ans += dist[j];
      }
    }
    printf("%lld\n", ans);
  }
} // namespace s1
namespace s2 {
  void go() {
    // fprintf(stderr, "s2::go: ");
    long long ans = 0;
    FOR(i, 2, n) { ans += a[i] * 1ll * (i - 1) * (n - i + 1); }
    printf("%lld\n", ans);
  }
} // namespace s2
void go() {
  scanf("%d", &n);
  bool cond1 = true, cond2 = true;
  FOR(i, 2, n) scanf("%d", a + i);
  FOR(i, 3, n) scanf("%d", b + i), cond1 &= b[i] == a[i] + a[i - 1];
  FOR(i, 4, n) scanf("%d", c + i), cond2 &= c[i] == a[i] + a[i - 1] + a[i - 2];
  if (cond1 && cond2) s2::go();
  // else if(cond2) s3::go();
  else
    s1::go();
}

int main() {
  int t;
  scanf("%d", &t);
  FOR(i, 1, t) go();
  return 0;
}
