// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

const int N = 2e6 + 5;

typedef vector<int> Poly;

Poly f;
vector<Poly> ans;
string ans_s[N];

void work(Poly g) {
  // printf("work\n");
  assert(f.size());
  while (!g.empty() && g.back() == 0) g.pop_back();
  if (g.size() <= 1 || f.size() <= g.size()) return;
  if ((f.size() - 1) % (g.size() - 1)) return;
  Poly tf = f, q(f.size() - g.size() + 1, 0);
  for (int i = f.size() - 1; i >= (int)g.size() - 1; i--) {
    // printf("i %d %d %d\n", i, g.size() - 1, f.size() - 1);
    if (tf[i] % g[g.size() - 1]) return;
    int coef = tf[i] / g[g.size() - 1];
    for (int j = 0; j < g.size(); j++) {
      // printf("j %d\n", j);
      assert(i - j >= 0);
      tf[i - j] -= coef * g[g.size() - 1 - j];
    }
    q[i - g.size() + 1] = coef;
  }
  while (!tf.empty() && tf.back() == 0) tf.pop_back();
  if (!tf.empty()) return;
  f = q;
  // printf("g: ");
  // for(unsigned i = 0; i < g.size(); i++)
  //   printf("%d%c", g[i], " \n"[i + 1 == g.size()]);
  ans.pb(g);
}

bool cmp(Poly F, Poly G) {
  if (F.size() != G.size()) return F.size() < G.size();
  int maxI = F.size();
  ROF(i, maxI - 1, 0) if (F[i] != G[i]) return F[i] < G[i];
  return false;
}
string renderPoly(Poly v) {
  int maxI = v.size();
  bool isHead = true;
  string res = "(";
  ROF(i, maxI - 1, 0) {
    if (v[i] == 0) continue;
    if (!isHead && v[i] > 0) res += "+";
    if (v[i] == -1 && i > 0) res += "-";
    if (v[i] > 1 || v[i] == 1 && i == 0 || v[i] == -1 && i == 0)
      res += to_string(v[i]);
    if (i > 0) res += "x";
    if (i > 1) res = res + "^" + to_string(i);
    isHead = false;
  }
  res += ")";
  return res;
}
string renderAns() {
  for (auto &v : ans) {
    if (v.back() < 0)
      for (auto &c : v) c = -c;
  }
  sort(ans.begin(), ans.end(), cmp);
  string res = "";
  for (auto v : ans) res += renderPoly(v);
  return res;
}
void dfs(int cur, Poly g, int lim) {
  if (cur > lim) return;
  g.resize(cur + 1, 0);
  // printf("dfs %d, ", cur);
  // for(auto x: g) printf("%d ", x);
  // puts("");
  if (g.size() > f.size()) return;
  ROF(c, 1, -1) {
    g[cur] = c;
    work(g);
    dfs(cur + 1, g, lim);
  }
}
void go() {
  int n;
  scanf("%d", &n);

  if (ans_s[n].size()) {
    printf("%s\n", ans_s[n].c_str());
    return;
  }

  ans.clear();
  f = Poly(n + 1, 0);
  f[n] = 1, f[0] = -1;
  FOR(i, 1, n) dfs(0, Poly(), i);
  ans.pb(f);
  ans_s[n] = renderAns();
  printf("%s\n", ans_s[n].c_str());
}
int main() {
  int t;
  scanf("%d", &t);
  FOR(i, 1, t) go();
  return 0;
}
