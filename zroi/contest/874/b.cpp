// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

const int N = 2e6 + 5;

typedef vector<int> Poly;

vector<Poly> ans;

bool cmp(Poly F, Poly G) { /*{{{*/
  if (F.size() != G.size()) return F.size() < G.size();
  int maxI = F.size();
  ROF(i, maxI - 1, 0) if (F[i] != G[i]) return F[i] < G[i];
  return false;
} /*}}}*/
bool equal(Poly F, Poly G) { /*{{{*/
  if (F.size() != G.size()) return false;
  int maxI = F.size();
  ROF(i, maxI - 1, 0) if (F[i] != G[i]) return false;
  return true;
} /*}}}*/
string renderPoly(Poly v) { /*{{{*/
  int maxI = v.size();
  bool isHead = true;
  string res = "(";
  ROF(i, maxI - 1, 0) {
    if (v[i] == 0) continue;
    if (!isHead && v[i] > 0) res += "+";
    if (v[i] == -1 && i > 0) res += "-";
    if (v[i] > 1 || v[i] == 1 && i == 0 || v[i] == -1 && i == 0)
      res += to_string(v[i]);
    if (i > 0) res += "x";
    if (i > 1) res = res + "^" + to_string(i);
    isHead = false;
  }
  res += ")";
  return res;
} /*}}}*/
string renderAns() { /*{{{*/
  for (auto &v : ans) {
    if (v.back() < 0)
      for (auto &c : v) c = -c;
  }
  sort(ans.begin(), ans.end(), cmp);
  string res = "";
  for (auto v : ans) res += renderPoly(v);
  return res;
} /*}}}*/

struct Atom {
  int a, b, c;         // y = x^a, z = b^c, y^z +/- 1
  void work(int typ) { // typ: 0 / 1
    // printf("work %d %d %d (typ = %d)\n", a, b, c, typ);
    int step = 1;
    FOR(i, 2, c) {
      step *= b;
      Poly f(step * (b - 1) * a + 1, 0);
      FOR(j, 0, b - 1) { f[j * step * a] = typ == 0 ? 1 : (j % 2 ? -1 : 1); }
      ans.pb(f);
      cout << renderPoly(f) << endl;
    }
    {
      Poly f((b - 1) * a + 1, 0);
      FOR(j, 0, b - 1) { f[j * a] = typ == 0 ? 1 : (j % 2 ? -1 : 1); }
      ans.pb(f);
      cout << renderPoly(f) << endl;
    }
    {
      Poly f(a + 1, 0);
      f[a] = 1;
      f[0] = typ == 0 ? -1 : 1;
      ans.pb(f);
      cout << renderPoly(f) << endl;
    }
  }
};

vector<Atom> ans_pair1, ans_pair2;

vector<pair<int, int>> getDistinctPrimtFactor(int n) {
  vector<pair<int, int>> res;
  for (int i = 2; i * i <= n; i++) {
    if (n % i) continue;
    int pw_i = 0;
    while (n % i == 0) n /= i, pw_i++;
    res.pb({i, pw_i});
  }
  if (n > 1) res.pb({n, 1});
  return res;
}

void work2(int);

void work1(int n) { // x^n - 1{{{
  if (n % 2 == 0) {
    work1(n / 2);
    work2(n / 2);
    return;
  }
  if (n == 1) return;
  auto v = getDistinctPrimtFactor(n);
  for (auto pr : v) { ans_pair1.pb((Atom){1, pr.first, pr.second}); }
} /*}}}*/
void work2(int n) { // x^n + 1{{{
  int coef = 1;
  while (n % 2 == 0) n /= 2, coef *= 2;
  if (n == 1) {
    ans_pair1.pb((Atom){coef, 2, 1});
    return;
  }
  auto v = getDistinctPrimtFactor(n);
  for (auto pr : v) { ans_pair2.pb((Atom){coef, pr.first, pr.second}); }
} /*}}}*/

void go() {
  ans.clear();
  ans_pair1.clear();
  ans_pair2.clear();

  int n;
  scanf("%d", &n);

  work1(n);

  for (auto pr : ans_pair1) {
    pr.work(0);
    // printf("(x^%d) ^ %d^%d - 1\n", pr.a, pr.b, pr.c);
  }
  for (auto pr : ans_pair2) {
    pr.work(1);
    // printf("(x^%d) ^ %d^%d + 1\n", pr.a, pr.b, pr.c);
  }
  sort(ans.begin(), ans.end());
  vector<Poly> real_ans;
  for (unsigned i = 0; i < ans.size(); i++) {
    if (i == 0 || !equal(ans[i], ans[i - 1])) real_ans.pb(ans[i]);
  }
}
int main() {
  int t;
  scanf("%d", &t);
  FOR(i, 1, t) go();
  return 0;
}
