// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

int ans, p[20], a = 1, b = 1, c = 1;

int main() {
  int n;
  cin >> n;
  FOR(i, 1, n) p[i] = i;
  int tot = n * (n - 1) / 2;
  int q = 1;
  FOR(i, 1, n) q *= i;
  do {
    int cnt = 0;
    FOR(i, 1, n) FOR(j, i + 1, n) if (p[i] > p[j])++ cnt;
    ans += min(cnt * a, (tot - cnt) * a + b);
  } while (next_permutation(p + 1, p + n + 1));
  ans += q * c;
  printf("%d\n", ans);
  return 0;
}
