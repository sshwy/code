// by Yao
#include <bits/stdc++.h>
using namespace std;
#define int long long
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

const int N = 20;

long long f[N][N * N];
void init() {
  f[0][0] = 1;
  FOR(i, 1, N - 1) {
    int lim = i * (i - 1) / 2;
    FOR(j, 0, lim) {
      FOR(k, 1, i) {
        int delta = i - k;
        if (delta <= j) { f[i][j] += f[i - 1][j - delta]; }
      }
    }
  }
}
int n, a, b, c, p[N];

long long calc(int i, int j) { return min(j * a, (i * (i - 1) / 2 - j) * a + b); }

int c1() {
  int res = 0;
  FOR(i, 1, n) FOR(j, i + 1, n) if (p[i] > p[j]) res += a;
  return res;
}
int c2() {
  int res = 0;
  FOR(i, 1, n) FOR(j, i + 1, n) if (p[i] < p[j]) res += a;
  return res + b;
}
long long gcd(long long a, long long b) { return b ? gcd(b, a % b) : a; }

long long ap, aq;
void updAns(long long p, long long q) {
  assert(p || q);
  long long g = gcd(p, q);
  p /= g, q /= g;
  if (__int128(p) * aq < __int128(q) * ap) ap = p, aq = q;
}
void go2() {
  ap = 1, aq = 0;

  FOR(i, 1, n) scanf("%lld", p + i);
  int cost = min(c1(), c2()), tot = n * (n - 1) / 2, fac = 1;
  FOR(i, 1, n) fac *= i;

  updAns(cost, 1);

  vector<int> v;
  FOR(j, 0, tot) v.pb(calc(n, j));
  sort(v.begin(), v.end());
  v.erase(unique(v.begin(), v.end()), v.end());

  for (auto e : v) { // x + c < e
    long long s = 0, t = 0;
    FOR(j, 0, tot) {
      if (calc(n, j) < e) {
        s += calc(n, j) * 1ll * f[n][j];
      } else {
        t += f[n][j];
      }
    }
    long long p = t * c + s, q = fac - t; // x = p / q
    p += q * c;
    // printf("e %lld p %lld q %lld\n", e, p, q);
    if (p <= e * q) { updAns(p, q); }
  }
  {
    long long s = 0;
    FOR(j, 0, tot) { s += calc(n, j) * 1ll * f[n][j]; }
    long long p = s, q = fac; // x = p / q
    updAns(p + q * c, q);
  }
  printf("%lld/%lld\n", ap, aq);
}
void go() {
  int q;
  scanf("%lld%lld%lld%lld%lld", &n, &a, &b, &c, &q);
  FOR(i, 1, q) go2();
}

signed main() {
  init();
  int t;
  scanf("%lld", &t);
  FOR(i, 1, t) go();
  return 0;
}
