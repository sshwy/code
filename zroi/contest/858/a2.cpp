// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

const int N = 1e5 + 5;
int n, m, x;
int fa[N], w[N];

int QueryKthAncestor(int t, int u, int k) {
  while (k) {
    if (w[u] > t) return 0;
    if (!fa[u]) return 0;
    u = fa[u], --k;
  }
  return u;
}
int QueryDepth(int t, int u) {
  int res = 0;
  while (u) {
    if (w[u] > t) return res;
    if (!fa[u]) return res;
    ++res, u = fa[u];
  }
  return u;
}
int main() {
  scanf("%d%d%d", &n, &m, &x);
  FOR(i, 1, m) {
    // printf("i %d ", i);
    int op, a, b, c;
    scanf("%d", &op);
    if (op == 0) {
      scanf("%d%d", &a, &b);
      fa[a] = b;
      w[a] = i;
    } else if (op == 1) {
      scanf("%d%d%d", &a, &b, &c);
      printf("%d\n", QueryKthAncestor(a, b, c));
    } else if (op == 2) {
      scanf("%d%d", &a, &b);
      printf("%d\n", QueryDepth(a, b));
    }
  }
  return 0;
}
