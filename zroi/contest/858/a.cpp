// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

const int SZ = 1e6 + 6;

int tot, ch[SZ][2], fa[SZ], val[SZ], mx[SZ], sz[SZ];

int new_node(int v) {
  ++tot, ch[tot][0] = ch[tot][1] = fa[tot] = 0;
  sz[tot] = 1, val[tot] = v;
  return tot;
}
void pushup(int u) {
  sz[u] = sz[ch[u][0]] + sz[ch[u][1]] + 1;
  mx[u] = max(val[u], max(mx[ch[u][0]], mx[ch[u][1]]));
}
void nodeassign(int u, int v) { val[u] = v, pushup(u); }
bool isroot(int u) { return ch[fa[u]][0] != u && ch[fa[u]][1] != u; }
bool get(int u) { return ch[fa[u]][1] == u; }
void rotate(int u) {
  int p = fa[u], pp = fa[p], k;
  k = get(u);
  if (!isroot(p)) ch[pp][get(p)] = u; //!!!
  ch[p][k] = ch[u][!k], fa[ch[u][!k]] = p;
  ch[u][!k] = p, fa[p] = u, fa[u] = pp;
  pushup(p), pushup(u);
}
void splay(int u) {
  for (int p; p = fa[u], !isroot(u); rotate(u))
    if (!isroot(p)) rotate(get(p) == get(u) ? p : u);
}
void access(int u) {
  for (int p = 0; u; p = u, u = fa[u]) splay(u), ch[u][1] = p, pushup(u);
}
void link(int x, int y) { splay(x), fa[x] = y; }
void assign(int x, int y) { splay(x), nodeassign(x, y); }

int n, m, X, las;

int getKthSufMax(int u, int k) {
  assert(k > 0);
  if (sz[ch[u][1]] >= k) return getKthSufMax(ch[u][1], k);
  if (sz[ch[u][1]] + 1 == k) return max(val[u], mx[ch[u][1]]);
  return max(max(val[u], mx[ch[u][1]]), getKthSufMax(ch[u][0], k - 1 - sz[ch[u][1]]));
}
int getKthSuf(int u, int k) {
  assert(k > 0);
  if (sz[ch[u][1]] >= k) return getKthSuf(ch[u][1], k);
  if (sz[ch[u][1]] + 1 == k) return u;
  return getKthSuf(ch[u][0], k - 1 - sz[ch[u][1]]);
}
int QueryKthAncestor(int t, int u, int k) {
  // printf("QueryKthAncestor %d %d %d\n", t, u, k);
  if (k == 0) return u;
  access(u);
  splay(u);

  if (sz[ch[u][0]] < k) return 0;

  int w = val[u];
  if (k - 1 > 0) w = max(w, getKthSufMax(ch[u][0], k - 1));
  if (w > t) return 0;

  int v = getKthSuf(ch[u][0], k);
  splay(v);
  return v;
}
int countLessOrEqualThan(int t, int u) {
  if (mx[u] <= t) return sz[u];
  if (mx[ch[u][1]] <= t && val[u] <= t)
    return countLessOrEqualThan(t, ch[u][0]) + 1 + sz[ch[u][1]];
  return countLessOrEqualThan(t, ch[u][1]);
}
int QueryDepth(int t, int u) {
  // printf("QueryDepth %d %d\n", t, u);
  access(u);
  splay(u);

  if (val[u] > t) return 0;

  int dep = countLessOrEqualThan(t, ch[u][0]) + 1;
  return dep;
}

int main() {
  scanf("%d%d%d", &n, &m, &X);

  FOR(i, 1, n) new_node(1e9);

  FOR(i, 1, m) {
    // printf("i %d ", i);
    int op, a, b, c;
    scanf("%d", &op);
    op = (op + 1ll * X * las) % 3;
    if (op == 0) {
      scanf("%d%d", &a, &b);
      a = (a + 1ll * X * las) % n + 1;
      b = (b + 1ll * X * las) % n + 1;
      // printf("setFa %d %d\n", a, b);
      link(a, b);
      assign(a, i); // 第 i 次操作
    } else if (op == 1) {
      scanf("%d%d%d", &a, &b, &c);
      a = (a + 1ll * X * las) % m;
      b = (b + 1ll * X * las) % n + 1;
      c = (c + 1ll * X * las) % n;
      // printf("queryKth %d %d %d\n", a, b, c);
      printf("%d\n", las = QueryKthAncestor(a, b, c));
    } else if (op == 2) {
      scanf("%d%d", &a, &b);
      a = (a + 1ll * X * las) % m;
      b = (b + 1ll * X * las) % n + 1;
      // printf("queryDep %d %d\n", a, b);
      printf("%d\n", las = QueryDepth(a, b));
    }
  }
  return 0;
}
