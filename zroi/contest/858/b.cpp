// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

const int L = 1e5 + 5, SZ = L * 2, ALP = 26;

int fail[SZ], tr[SZ][ALP], len[SZ], tot = 1, lastNode;

void extend(char c) {
  c -= 'a';
  int u = ++tot, p = lastNode;
  len[u] = len[lastNode] + 1;
  while (p && tr[p][c] == 0) tr[p][c] = u, p = fail[p];
  if (!p)
    fail[u] = 1;
  else {
    int q = tr[p][c];
    if (len[q] == len[p] + 1)
      fail[u] = q;
    else {
      int cq = ++tot;
      len[cq] = len[p] + 1;
      fail[cq] = fail[q];
      memcpy(tr[cq], tr[q], sizeof(tr[q]));
      fail[u] = fail[q] = cq;
      while (p && tr[p][c] == q) tr[p][c] = cq, p = fail[p];
    }
  }
  lastNode = u;
}

int main() { return 0; }
