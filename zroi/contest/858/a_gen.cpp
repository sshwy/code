// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

using namespace RA;

const int N = 1e5 + 5;

int p[N];

int main() {
  srand(clock() + time(0));
  int n = rnd(2, 100), m = rnd(1, 100), x = rnd(0, 10000);
  n = 1e5, m = 5e5;
  printf("%d %d %d\n", n, m, x);

  FOR(i, 1, n - 1) p[i] = i + 1;
  random_shuffle(p + 1, p + n);
  int lp = 0;

  FOR(i, 1, m) {
    int op = rnd(3);
    if (lp == n - 1 && op == 0) ++op;
    if (op == 0) {
      int u = p[++lp];
      int v = rnd(1, u - 1);
      printf("0 %d %d\n", u, v);
    } else if (op == 1) {
      int t = rnd(i);
      int u = rnd(1, n);
      int k = rnd(n);
      printf("1 %d %d %d\n", t, u, k);
    } else {
      int t = rnd(i);
      int u = rnd(1, n);
      printf("2 %d %d\n", t, u);
    }
  }
  return 0;
}
