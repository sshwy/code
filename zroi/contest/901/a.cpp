// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

__int128 check(int x) {
  int a[100] = {0}, la = 0;
  int b[100] = {0}, lb = 0;
  while (x) a[la++] = x % 2, x /= 2;
  __int128 y = 0;
  ROF(i, la - 1, 0) y = y * 10 + a[i];
  __int128 Y = y;
  while (y) b[lb++] = y % 2, y /= 2;
  assert(lb >= la);
  FOR(i, 0, la - 1) if (b[i] != a[i]) return -1;
  return Y;
}

void _print(__int128 x) {
  if (!x) return;
  _print(x / 10);
  putchar((x % 10) + '0');
}
void print(__int128 x) {
  if (x == 0)
    putchar('0');
  else {
    _print(x);
  }
}
int main() {
  int tot = 0;
  int las = 6e8;
  int lim = 8e8;
  FOR(i, las + 1, lim) {
    __int128 x = check(i);
    if (x != -1) {
      ++tot;
      print(x);
      putchar(',');
    }
    if (i % 1000 == 0)
      fprintf(stderr, "\rProgress: %.1lf%%", (i - las) * 100.0 / (lim - las));
  }
  fprintf(stderr, "tot = %d\n", tot);
  return 0;
}
