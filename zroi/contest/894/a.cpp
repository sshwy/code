// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

const int N = 1005;
int n;
int a[N];
bool f[N][N][2];

void go() {
  scanf("%d", &n);
  int sxor = 0;
  FOR(i, 1, n) scanf("%d", a + i), sxor ^= a[i];
  if (sxor == 0) {
    puts("Draw");
    return;
  }
  ROF(i, 30, 0) if (sxor >> i & 1) {
    FOR(j, 1, n) a[j] = a[j] >> i & 1;
    break;
  }
  FOR(i, 1, n) a[i] ^= a[i - 1];
  memset(f, 0, sizeof(f));
  FOR(len, 0, n - 1) FOR(i, 1, n - len) {
    int j = i + len;
    if (i == j)
      f[i][j][a[i] ^ a[i - 1]] = true;
    else {
      if (!f[i][j - 1][0]) f[i][j][a[j] ^ a[i - 1]]
    }
  }
}

int main() {
  int t;
  scanf("%d", &t);
  FOR(i, 1, t) go();
  return 0;
}
