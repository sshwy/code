// by Sshwy
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA
const int N = 1e5 + 5;
int n;
vector<pair<int, long long>> g[N];

long long f[N];
void dfs(int u) {
  long long mx = 0;
  for (auto pr : g[u]) {
    int v = pr.first;
    dfs(v);
    long long c = pr.second;
    f[u] += 1ll << c;
    f[u] += f[v];
    mx = max(mx, (1ll << c) + f[v]);
  }
  f[u] += mx;
  // printf("mx %d\n",mx);
  printf("f[%d] %lld\n", u, f[u]);
}
int main() {
  scanf("%d", &n);
  FOR(i, 2, n) {
    int p;
    long long c;
    scanf("%d%lld", &p, &c);
    g[p].pb({i, c});
  }
  dfs(1);
  printf("%lld\n", f[1] % 998244353);
  return 0;
}
