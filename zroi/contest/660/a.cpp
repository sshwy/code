// by Sshwy
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA
const int N = 1e5 + 5, P = 998244353;
long long pw(long long a, long long m) {
  long long res = 1;
  a %= P;
  m %= (P - 1);
  while (m) m & 1 ? res = 1ll * res * a % P : 0, a = 1ll * a * a % P, m >>= 1;
  return res;
}

int n;
vector<pair<int, long long>> g[N];

long long f[N];
int id[N]; // 配合F
typedef pair<set<int>, int> BIG;
BIG F[N];

void add_bit(set<int> &s, int x) {
  while (s.find(x) != s.end()) { s.erase(x), ++x; }
  s.insert(x);
}
/*long long calc(const BIG & A){
    long long res = 0;
    for(auto x: A.first)res += 1ll << (x + A.second);
    return res;
}*/
bool comp(const BIG &A, const BIG &B) {
  // printf("comp %lld %lld\n",calc(A), calc(B));
  assert(A.first.size() && B.first.size() && A.first.size() <= B.first.size());
  set<int>::iterator iA = A.first.end(), iB = B.first.end(), iB2, iB3;
  --iA, --iB;
  if (*iA + A.second != *iB + B.second) return *iA + A.second < *iB + B.second;
  if (B.first.size() == 1) return 0; // A == B;
  // for(auto x: A.first)cout<<x+A.second<<" "; cout << endl;
  // for(auto x: B.first)cout<<x+B.second<<" "; cout << endl;
  while (iA != A.first.begin()) {
    --iA;
    // A: *iA + A.second是1
    iB2 = B.first.lower_bound(*iA + A.second - B.second);
    iB3 = iB2;
    ++iB3;
    if (iB2 == iB) {
      return 0; // A>B
    } else if (*iB2 + B.second == *iA + A.second && iB3 == iB) {
      iB = iB2; // 目前 A=B
    } else {
      return 1; // A<B
    }
  }
  // printf("2 comp %lld %lld\n",calc(A), calc(B));
  if (iB == B.first.begin()) return 0; // A=B
  return 1;                            // A<B
}
bool cmp(const BIG &A, const BIG &B) {
  if (A.first.size() > B.first.size()) return !comp(B, A);
  return comp(A, B);
}
typedef pair<int, int> pii;
priority_queue<pii> q;

void dfs(int u) {
  if (g[u].empty()) return; // leaf
  for (auto pr : g[u]) {
    int v = pr.first;
    dfs(v);
  }
  int max_v = -1;
  for (auto pr : g[u]) {
    int v = pr.first;
    long long c = pr.second;

    f[v] = (f[v] + pw(2, c)) % P;
    add_bit(F[id[v]].first, c - F[id[v]].second);

    if (max_v == -1)
      max_v = v;
    else {
      // printf("cmp %lld %lld
      // %d\n",calc(F[id[max_v]]),calc(F[id[v]]),cmp(F[id[max_v]],F[id[v]]));
      if (cmp(F[id[max_v]], F[id[v]])) max_v = v;
    }

    f[u] = (f[u] + f[v]) % P;

    q.push({-F[id[v]].first.size(), id[v]});
  }
  // printf("mx %lld\n",f[max_v]);
  f[u] = (f[u] + f[max_v]) % P;
  F[id[max_v]].second++;

  while (q.size() > 1) {
    pii a = q.top();
    q.pop();
    pii b = q.top();
    q.pop();
    // printf("size: %d\n",F[a.second].first.size());
    for (auto x : F[a.second].first) {
      add_bit(F[b.second].first, x + F[a.second].second - F[b.second].second);
    }
    // printf("down\n");
    q.push({-F[b.second].first.size(), b.second});
  }

  id[u] = q.top().second;
  q.pop();

  // printf("f[%d] %lld\n",u,f[u]);
}
int main() {
  scanf("%d", &n);
  FOR(i, 1, n) id[i] = i;
  FOR(i, 2, n) {
    int p;
    long long c;
    scanf("%d%lld", &p, &c);
    g[p].pb({i, c});
  }
  dfs(1);
  printf("%lld\n", f[1]);
  return 0;
}
