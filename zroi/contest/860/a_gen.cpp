// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

using namespace RA;

const int N = 3005;

int n, m, q;
int rev[N], w[N][N], use[N], ban[N][N];
vector<pair<int, int>> e;

int main() {
  srand(clock() + time(0));
  int n = rnd(1, 18), m = rnd(1, 18);
  FOR(i, 1, m) use[i] = rnd(2);
  FOR(i, 1, n) {
    FOR(j, 1, m) {
      if (use[j]) {
        w[i][j] = 1;
      } else {
        w[i][j] = -1;
      }
      if (rnd(100) < 50) ban[i][j] = true;
      if (rnd(100) < 85) w[i][j] = -w[i][j];
    }
    // if(rnd(100) < 70) rev[i] = rnd(1, m);
  }

  FOR(i, 1, n) FOR(j, 1, m) if (!ban[i][j]) {
    int W = w[i][j];
    if (rev[i] == j) W = -W;
    if (W == 1)
      e.pb({i, j});
    else
      e.pb({i, -j});
  }

  printf("%d %d %lu\n", n, m, e.size());
  for (auto pt : e) printf("%d %d\n", pt.first, pt.second);
  return 0;
}
