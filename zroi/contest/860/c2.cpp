// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

const int N = 2e5 + 5;
int n, q;
char s[N], t[N], a[N];

int work(int l, int r) {
  FOR(i, 0, r - l) t[i] = s[i + l];
  int len = r - l + 1;
  int ans = len;
  FOR(x, 0, len - 1) {
    FOR(b, 0, 1) {
      FOR(i, 0, len - 1) {
        a[i] = __builtin_parity(i & x) ^ b;
        a[i] += '0';
      }
      FOR(i, 0, len - 1) { a[i] = a[i] == t[i]; }
      a[len] = 2;
      int res = 0;
      FOR(i, 1, len) if (a[i] != a[i - 1] && a[i - 1] == false)++ res;
      ans = min(ans, res);
    }
  }
  return ans;
}
int main() {
  scanf("%d", &n);
  scanf("%s", s);
  scanf("%d", &q);
  FOR(i, 1, q) {
    int l, r;
    scanf("%d%d", &l, &r);
    printf("%d\n", work(l, r));
  }
  return 0;
}
