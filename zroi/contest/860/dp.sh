g++ a.cpp -o .u
g++ a2.cpp -o .v
g++ a_gen.cpp -o .w
g++ spj.cpp -o .chk

while true; do
  ./.w > .i
  ./.u < .i > .x
  ./.v < .i > .y
  if ./.chk .i .x .y; then
    echo AC
  else
    echo WA
    break
  fi
done
