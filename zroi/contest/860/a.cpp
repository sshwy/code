// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

typedef vector<int> vt;
const int N = 1e5 + 5, SZ = 2e6 + 5;

int n, m, q, tot;
vector<int> want[N], wantNode[N], dont[N], dontNode[N];
int choose[N], ban[N], rk[SZ];

int fa[SZ];
int get(int u) { return fa[u] == u ? u : fa[u] = get(fa[u]); }
void merge(int u, int v) {
  u = get(u), v = get(v);
  if (u == v) return;
  fa[u] = v;
}

struct qxx {
  int nex, t;
} e[SZ];
int h[SZ], le;
void ae(int u, int v) {
  // printf("ae %d %d\n", u, v);
  e[++le] = {h[u], v}, h[u] = le;
}

int dfn[SZ], totdfn, totscc, low[SZ], st[SZ], tp;
bool vis[SZ];

void dfs(int u) {
  low[u] = dfn[u] = ++totdfn;
  st[++tp] = u, vis[u] = true;
  for (int i = h[u], v; v = e[i].t, i; i = e[i].nex) {
    if (!dfn[v]) {
      dfs(v), low[u] = min(low[u], low[v]);
    } else if (vis[v]) {
      low[u] = min(low[u], dfn[v]);
    }
  }
  if (low[u] == dfn[u]) {
    ++totscc;
    do {
      vis[st[tp]] = false;
      rk[st[tp]] = totscc;
      merge(st[tp], u);
    } while (st[tp--] != u);
  }
}

int nd[SZ];
void solveWant(int u, int l, int r, const vt &Want, const vt &Node) {
  if (l == r) {
    nd[u] = choose[Want[l]];
    return;
  }
  int mid = (l + r) >> 1;
  solveWant(u << 1, l, mid, Want, Node);
  solveWant(u << 1 | 1, mid + 1, r, Want, Node);

  nd[u] = ++tot;

  ae(nd[u], nd[u << 1]);
  ae(nd[u], nd[u << 1 | 1]);

  FOR(i, l, mid) ae(Node[i], nd[u << 1 | 1]);
  FOR(i, mid + 1, r) ae(Node[i], nd[u << 1]);
}
void solveDont(int u, int l, int r, const vt &Dont, const vt &Node) {
  if (l == r) {
    nd[u] = ban[Dont[l]];
    return;
  }
  int mid = (l + r) >> 1;
  solveDont(u << 1, l, mid, Dont, Node);
  solveDont(u << 1 | 1, mid + 1, r, Dont, Node);

  nd[u] = ++tot;

  ae(nd[u], nd[u << 1]);
  ae(nd[u], nd[u << 1 | 1]);

  FOR(i, l, mid) ae(Node[i], nd[u << 1 | 1]);
  FOR(i, mid + 1, r) ae(Node[i], nd[u << 1]);
}
void go() {
  FOR(i, 1, m) {
    choose[i] = ++tot, ban[i] = ++tot;
    // printf("i %d choose %d ban %d\n", i, choose[i], ban[i]);
  }
  FOR(i, 1, n) {
    for (unsigned j = 0; j < want[i].size(); j++) wantNode[i].pb(++tot);
    for (unsigned j = 0; j < dont[i].size(); j++) dontNode[i].pb(++tot);
  }
  FOR(i, 1, n) {
    // printf("i %d\n", i);
    for (unsigned j = 0; j < want[i].size(); j++) {
      ae(ban[want[i][j]], wantNode[i][j]);
    }
    for (unsigned j = 0; j < dont[i].size(); j++) {
      ae(choose[dont[i][j]], dontNode[i][j]);
    }
    if (want[i].size()) solveWant(1, 0, want[i].size() - 1, want[i], wantNode[i]);
    if (dont[i].size()) solveDont(1, 0, dont[i].size() - 1, dont[i], dontNode[i]);
  }

  FOR(i, 1, tot) fa[i] = i;
  FOR(i, 1, m) {
    if (!dfn[choose[i]]) dfs(choose[i]);
    if (!dfn[ban[i]]) dfs(ban[i]);
  }

  FOR(i, 1, m) {
    if (get(choose[i]) == get(ban[i])) {
      puts("-1");
      return;
    }
  }
  vector<int> ans;
  FOR(i, 1, m) {
    int x = choose[i], y = ban[i];
    if (rk[x] < rk[y]) ans.pb(i);
  }
  printf("%lu\n", ans.size());
  for (unsigned i = 0; i < ans.size(); i++)
    printf("%d%c", ans[i], " \n"[i + 1 == ans.size()]);
}
int main() {
  scanf("%d%d%d", &n, &m, &q);
  FOR(i, 1, q) {
    int x, y;
    scanf("%d%d", &x, &y);
    if (y > 0) {
      want[x].pb(y);
    } else {
      dont[x].pb(-y);
    }
  }
  go();
  return 0;
}
