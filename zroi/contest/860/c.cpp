// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

const int N = 2e5 + 5, M = 20;

int n, q;
char s[N];

int a[N], cnt[N][M];

void prework() {
  FOR(i, 1, n - 1) a[i] = s[i] != s[i - 1];
  FOR(i, 1, n - 1) {
    FOR(j, 0, M - 1) {
      cnt[i][j] = a[i];
      if (i > (1 << j)) cnt[i][j] += cnt[i - (1 << j)][j];
    }
  }
}

int work(int l, int r) {
  if (l == r) return 0;
  int ans = 0;
  int m = __builtin_ctz(r - l + 1);
  FOR(i, 0, m - 1) {
    int t = (cnt[r - (1 << i) + 1][i] - cnt[l][i]) -
            (cnt[r - (1 << (i + 1)) + 1][i + 1] - cnt[l][i + 1]);
    int tot = 1 << (m - 1 - i);
    ans += min(t, tot - t);
  }
  return (ans + 1) / 2;
}

int main() {
  scanf("%d", &n);
  scanf("%s", s);

  prework();

  scanf("%d", &q);
  FOR(i, 1, q) {
    int l, r;
    scanf("%d%d", &l, &r);
    printf("%d\n", work(l, r));
  }
  return 0;
}
