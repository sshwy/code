// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

const int N = 5005;
int n, m, q;
int w[N][N];

int readAns(FILE *out) {
  int num;
  fscanf(out, "%d", &num);
  if (num == -1) return -1;
  vector<int> use(m + 1, 0);
  FOR(i, 1, num) {
    int x;
    fscanf(out, "%d", &x);
    use[x] = 1;
  }
  FOR(i, 1, n) {
    int c1 = 0, c2 = 0;
    FOR(j, 1, m) if (w[i][j]) {
      if (use[j] && w[i][j] != 1) ++c1;
      if (!use[j] && w[i][j] == 1) ++c2;
    }
    if (c1 > 1 || c2 > 1) return 1; // invalid
  }
  return 0; // ok
}

// ./spj in out std
int main(int argc, char **argv) {
  FILE *fin = fopen(argv[1], "r");
  FILE *fout = fopen(argv[2], "r");
  FILE *fstd = fopen(argv[3], "r");

  fscanf(fin, "%d%d%d", &n, &m, &q);
  FOR(i, 1, q) {
    int x, y;
    fscanf(fin, "%d%d", &x, &y);
    if (y > 0) {
      w[x][y] = 1;
    } else {
      w[x][-y] = -1;
    }
  }

  int tout = readAns(fout);
  int tstd = readAns(fstd);
  // printf("tout %d tstd %d\n", tout, tstd);

  if (tstd == -1) {
    if (tout == -1) return 0; // ok
    return 1;                 // wa
  }
  if (tstd != 0) return -1; // fail
  if (tout != 0) return 1;  // wa

  return 0; // ok
}
