// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

const int N = 20;

int n, m;
int p[N], q[N];
int cnt[N][N][N];

int main() {
  scanf("%d%d", &n, &m);
  FOR(i, 1, m) {
    int a, b, c;
    scanf("%d%d%d", &a, &b, &c);
    cnt[a][b][c]++;
  }
  FOR(i, 1, n) p[i] = i;
  do {
    int tot = 0;
    FOR(a, 1, n) FOR(b, 1, n) FOR(c, 1, n) if (cnt[a][b][c]) {
      if ((p[a] < p[b]) == (p[b] < p[c])) { tot += cnt[a][b][c]; }
    }
    if (tot * 2 >= m) {
      FOR(i, 1, n) q[p[i]] = i;
      FOR(i, 1, n) printf("%d%c", q[i], " \n"[i == n]);
      return 0;
    }
  } while (next_permutation(p + 1, p + n + 1));
  return 0;
}
