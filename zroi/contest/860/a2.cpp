// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

const int N = 5005;
int n, m, q;
int w[N][N];

int main() {
  scanf("%d%d%d", &n, &m, &q);
  FOR(i, 1, q) {
    int x, y;
    scanf("%d%d", &x, &y);
    if (y > 0) {
      w[x][y] = 1;
    } else {
      w[x][-y] = -1;
    }
  }
  int lim = 1 << m;
  FOR(i, 0, lim - 1) {
    vector<int> bad1(n + 1, 0), bad2(n + 1, 0);
    FOR(j, 1, m) {
      bool flag = i >> (j - 1) & 1;
      FOR(k, 1, n) if (w[k][j]) {
        bool prefer = w[k][j] == 1;
        if (flag != prefer) flag ? bad1[k]++ : bad2[k]++;
      }
    }
    bool ok = true;
    FOR(k, 1, n) {
      if (bad1[k] > 1 || bad2[k] > 1) {
        ok = false;
        break;
      }
    }
    if (ok) {
      int cnt = 0;
      FOR(j, 1, m) cnt += i >> (j - 1) & 1;
      printf("%d\n", cnt);
      FOR(j, 1, m) if (i >> (j - 1) & 1) printf("%d ", j);
      return 0;
    }
  }
  puts("-1");
  return 0;
}
