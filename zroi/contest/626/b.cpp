// by Sshwy
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

const int N = 32, T = 6000, M = T * 50, P = 998244353;
int id[N][N][N][N];
char s[N], t[N];

int n, m;
struct atom {
  int a, b, c, d;
  int idx() { return id[a][b][c][d]; }
  pair<bool, atom> trans(pair<atom, atom> p) {
    if (a >= p.first.a && b >= p.first.b && c >= p.first.c && d >= p.first.d) {
      atom res = {a - p.first.a + p.second.a, b - p.first.b + p.second.b,
          c - p.first.c + p.second.c, d - p.first.d + p.second.d};
      return {1, res};
    } else
      return {0, *this};
  }
};
atom atomize(char *s) {
  int c[4] = {0};
  while (*s) c[(*s) - 'A']++, ++s;
  return {c[0], c[1], c[2], c[3]};
}
vector<pair<atom, atom>> trans;

struct qxx {
  int nex, t;
} e[M], e_scc[M];
int h[T], le = 1, h_scc[T], le_scc = 1;
void add_path(int f, int t) { e[++le] = {h[f], t}, h[f] = le; }
void add_path_scc(int f, int t) {
  e_scc[++le_scc] = {h_scc[f], t}, h_scc[f] = le_scc;
}

int dfn[T], low[T], vis[T], scc[T], totdfn = 0, totscc = 0, dg[T];
long long f[T], W[T], w[T];
stack<int> st;

void dfs(int u) {
  dfn[u] = low[u] = ++totdfn;
  vis[u] = 1, st.push(u);
  for (int i = h[u]; i; i = e[i].nex) {
    int v = e[i].t;
    if (!dfn[v])
      dfs(v), low[u] = min(low[u], low[v]);
    else if (vis[v])
      low[u] = min(low[u], dfn[v]);
  }
  if (dfn[u] == low[u]) {
    ++totscc;
    while (vis[u])
      vis[st.top()] = 0, scc[st.top()] = totscc, W[totscc] += w[st.top()], st.pop();
  }
}
queue<int> q;
long long fac[N], fnv[N];
long long pw(long long a, int m) {
  int res = 1;
  while (m) m & 1 ? res = 1ll * res * a % P : 0, a = a * a % P, m >>= 1;
  return res;
}
void init() {
  fac[0] = 1;
  FOR(i, 1, N - 1) fac[i] = fac[i - 1] * i % P;
  fnv[N - 1] = pw(fac[N - 1], P - 2);
  ROF(i, N - 1, 1) fnv[i - 1] = fnv[i] * i % P;
}
long long binom(int a, int b) {
  if (a < b) return 0;
  return fac[a] * fnv[b] % P * fnv[a - b] % P;
}
long long calc(int a, int b, int c, int d) {
  return binom(a + b + c + d, a) * binom(b + c + d, b) * binom(c + d, c);
}
int main() {
  init();

  scanf("%d%d", &n, &m);
  FOR(i, 1, m) {
    scanf("%s%s", s, t);
    atom as = atomize(s), at = atomize(t);
    trans.pb({as, at});
  }

  int totid = 0;
  FOR(a, 0, n) FOR(b, 0, n - a) FOR(c, 0, n - a - b) {
    int d = n - a - b - c;
    id[a][b][c][d] = ++totid;
    w[totid] = calc(a, b, c, d);
  }

  FOR(a, 0, n) FOR(b, 0, n - a) FOR(c, 0, n - a - b) {
    int d = n - a - b - c;
    assert(d >= 0);
    atom u = {a, b, c, d};
    for (auto t : trans) {
      auto v = u.trans(t);
      if (v.first) {
        add_path(u.idx(), v.second.idx());
        // printf("add_path %d(%d%d%d%d) %d(%d%d%d%d)\n",u.idx(),
        //        u.a,
        //        u.b,
        //        u.c,
        //        u.d,
        //        v.second.idx(),
        //        v.second.a,
        //        v.second.b,
        //        v.second.c,
        //        v.second.d
        //        );
      }
    }
  }

  FOR(i, 1, totid) if (!dfn[i]) dfs(i);
  // FOR(i,1,totid)printf("scc[%d]=%d w=%lld\n",i,scc[i],w[i]);

  FOR(u, 1, totid) {
    for (int i = h[u]; i; i = e[i].nex) {
      int v = e[i].t;
      if (scc[u] != scc[v]) {
        add_path_scc(scc[u], scc[v]), dg[scc[v]]++;
        // printf("add_path_scc %d %d : %lld
        // %lld\n",scc[u],scc[v],W[scc[u]],W[scc[v]]);
      }
    }
  }
  long long ans = 0;
  FOR(i, 1, totscc)
  if (dg[i] == 0) q.push(i), f[i] = W[i], ans = max(ans, f[i]);
  while (!q.empty()) {
    int u = q.front();
    q.pop();
    for (int i = h_scc[u]; i; i = e_scc[i].nex) {
      int v = e_scc[i].t;
      f[v] = max(f[v], f[u] + W[v]);
      ans = max(ans, f[v]);
      dg[v]--;
      if (dg[v] == 0) q.push(v);
    }
  }
  printf("%lld\n", ans);
  return 0;
}
