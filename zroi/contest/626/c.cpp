// by Sshwy
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA
const int N = 2e5 + 5, M = N * 2;
int n;
int a[N];

struct qxx {
  int nex, t, v;
} e[M];
int h[N], le = 1;
void add_path(int f, int t, int v) { e[++le] = {h[f], t, v}, h[f] = le; }

int sz[N], sm[N];
bool Cut[N];
void Size(int u, int p) {
  // printf("Size %d %d\n",u,p);
  sz[u] = 1, sm[u] = 0;
  for (int i = h[u]; i; i = e[i].nex) {
    int v = e[i].t;
    if (!Cut[v] && v != p) Size(v, u), sz[u] += sz[v], sm[u] = max(sm[u], sz[v]);
  }
}
int Core(int u, int p, int T) {
  int z = u;
  for (int i = h[u]; i; i = e[i].nex) {
    int v = e[i].t;
    if (!Cut[v] && v != p) {
      int x = Core(v, u, T);
      if (max(sm[z], T - sz[z]) > max(sm[x], T - sz[x])) z = x;
    }
  }
  return z;
}
double f[N], f1[N], f2[N];
void dfs(int u, int p, long long dist) {
  f[u] = (double)sqrt(dist) * dist * a[u];
  f1[u] = (double)sqrt(dist + 1) * (dist + 1) * a[u];
  f2[u] = (double)sqrt(dist - 1) * (dist - 1) * a[u];
  for (int i = h[u]; i; i = e[i].nex) {
    int v = e[i].t, w = e[i].v;
    if (v != p) dfs(v, u, dist + w), f[u] += f[v], f1[u] += f1[v], f2[u] += f2[v];
  }
}
double ans = 1e99;
void Solve(int u, int p) {
  Size(u, p);
  int core = Core(u, p, sz[u]);
  // printf("Solve %d %d\n",u,p);
  // printf("core %d\n",core);
  dfs(core, 0, 0);

  // printf("f[%d]=%.9lf\n",core,f[core]);

  ans = min(ans, f[core]);
  double tmin = f[core];
  int tv = core;
  for (int i = h[core]; i; i = e[i].nex) {
    int v = e[i].t;
    if (!Cut[v]) {
      double x = f1[core] - f1[v] + f2[v];
      // printf("v=%d, x=%.9lf\n",v,x);
      if (x < tmin) tmin = x, tv = v;
    }
  }
  if (tv == core) return;
  Cut[core] = 1;
  Solve(tv, core);
}
int main() {
  scanf("%d", &n);
  FOR(i, 1, n) scanf("%d", &a[i]);
  FOR(i, 1, n - 1) {
    int x, y, z;
    scanf("%d%d%d", &x, &y, &z);
    add_path(x, y, z);
    add_path(y, x, z);
  }
  Solve(1, 0);
  printf("%.9lf\n", ans);
  return 0;
}
