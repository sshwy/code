// by Sshwy
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA
const int N = 2e5 + 5, T = N * 2, M = T * 2;

int n, m, p;
vector<pair<int, int>> g[N];
int best[N];

struct qxx {
  int nex, t;
} e[M];
int h[T], le = 1;
void add_path(int f, int t) { e[++le] = {h[f], t}, h[f] = le; }
void add_rev_path(int f, int t) { add_path(t, f); }

bool use[T], vis[T];
int val[T];
int cycle;

void dfs(int u, int dist, long long *c) {
  if (vis[u]) {
    cycle = dist;
    return;
  }
  vis[u] = 1;
  c[dist] += val[u];
  for (int i = h[u]; i; i = e[i].nex) {
    int v = e[i].t;
    dfs(v, dist + 1, c);
  }
}
long long c[T], c2[T];
int cy1 = 0, cy2 = 0;
long long qry(int k) {
  if (k <= n * 2) return c[k] + c2[k];
  long long res = 0;
  if (cy1) {
    int t = (n * 2 - k % cy1) / cy1 * cy1 + k % cy1;
    res += c[t];
  }
  if (cy2) {
    int t = (n * 2 - k % cy2) / cy2 * cy2 + k % cy2;
    res += c2[t];
  }
  return res;
}
int main() {
  scanf("%d%d%d", &n, &m, &p);
  ++p;
  FOR(i, 1, m) {
    int x, y;
    scanf("%d%d", &x, &y);
    ++x, ++y;
    g[x].pb({i, y});
    g[y].pb({i, x});
  }

  FOR(u, 1, n) {
    assert(g[u].size());
    sort(g[u].begin(), g[u].end());
    best[u] = g[u][0].second;
  }

  FOR(u, 1, n) {
    if (g[u].size() == 1) {
      use[u * 2] = 1; // arrive from best
      val[u * 2] = 1;
      use[u * 2 + 1] = 0;
      add_rev_path(u * 2, best[best[u]] == u ? best[u] * 2 : best[u] * 2 + 1);
    } else {
      val[u * 2 + 1] = 1;
      use[u * 2] = use[u * 2 + 1] = 1;
      // arrive from best
      add_rev_path(u * 2,
          best[g[u][1].second] == u ? g[u][1].second * 2 : g[u][1].second * 2 + 1);
      // arrive from not best
      add_rev_path(u * 2 + 1, best[best[u]] == u ? best[u] * 2 : best[u] * 2 + 1);
    }
  }

  cycle = 0;
  dfs(p * 2, 0, c);
  cy1 = cycle;

  if (cycle) { FOR(i, cycle, n * 2) c[i] += c[i - cycle]; }

  if (use[p * 2 + 1]) {
    cycle = 0;
    memset(vis, 0, sizeof(vis));
    dfs(p * 2 + 1, 0, c2);
    cy2 = cycle;
    if (cycle) { FOR(i, cycle, n * 2) c2[i] += c2[i - cycle]; }
  }

  // FOR(i,0,n*2)printf("c[%d]=%lld\n",i,c[i]);
  // puts("");
  // FOR(i,0,n*2)printf("c2[%d]=%lld\n",i,c2[i]);

  int q;
  scanf("%d", &q);
  FOR(i, 1, q) {
    int k;
    scanf("%d", &k);
    printf("%lld%c", qry(k), " \n"[i == q]);
  }

  return 0;
}
