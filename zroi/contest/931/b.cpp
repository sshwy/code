#include<bits/stdc++.h>
#define int long long
#define FOR(a, b, c) for(int a = (int)(b); a <= (int)(c); a++)

const int N = 5e4 + 5, M = N * 2, INF = 0x3f3f3f3f3f3f3f3f;

struct qxx { int nex, t, v, ov; };
qxx e[M];
int h[N], le = 1;
int hh[N];
void add_path(int f, int t, int v) { e[++le] = (qxx){h[f], t, v, v}, h[f] = le; }
void add_bidir_flow(int f, int t, int v) { add_path(f, t, v), add_path(t, f, v); }
#define FORe(i, u, v, w) \
  for (int i = h[u], v, w; v = e[i].t, w = e[i].v, i; i = e[i].nex)
#define FORflowe(i, u, v, w) \
  for (int &i = hh[u], v, w; v = e[i].t, w = e[i].v, i; i = e[i].nex)

int s, t;
int dep[N];
std::queue<int> q;

bool bfs() {
  memset(dep, 0, sizeof(dep));
  dep[s] = 1, q.push(s);
  while (!q.empty()) {
    int u = q.front();
    q.pop();
    FORe(i, u, v, w) if (!dep[v] && w) dep[v] = dep[u] + 1, q.push(v);
  }
  return dep[t] != 0;
}
int dfs(int u, int flow) {
  if (u == t || !flow) return flow;
  int rest = flow;
  FORflowe(i, u, v, w) {
    if (!w || dep[v] != dep[u] + 1) continue;
    int k = dfs(v, std::min(rest, w));
    e[i].v -= k, e[i ^ 1].v += k, rest -= k;
    if (!rest) break;
  }
  return flow - rest;
}
int dinic() {
  int maxflow = 0;
  while (bfs()) {
    memcpy(hh, h, sizeof(h));
    for (int x; (x = dfs(s, INF));) maxflow += x;
  }
  return maxflow;
}

int work(int a, int b) {
  FOR(i, 1, le) e[i].v = e[i].ov; // reset
  s = a, t = b;
  return dinic();
}

int n, m, Q;

signed main() {
  scanf("%lld%lld%lld", &n, &m, &Q);
  FOR(i, 1, m) {
    int u, v, w;
    scanf("%lld%lld%lld", &u, &v, &w);
    add_bidir_flow(u, v, w);
  }
  FOR(i, 1, Q) {
    int a, b;
    scanf("%lld%lld", &a, &b);
    printf("%lld\n", work(a, b));
  }

  return 0;
}
