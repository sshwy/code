#include<bits/stdc++.h>
#define FOR(a, b, c) for(int a = (int)(b); a <= (int)(c); a++)

const int N = 6e5 + 5;
int n, q;
char s[N + N];

void printAns() {
  int cnt = 0;
  FOR(i, 1, n * 6) cnt += s[i] != s[i % (n * 6) + 1];
  assert(cnt % 2 == 0);

  // s[n * 6 + 1] = 0;
  // printf("%s\n", s + 1);

  printf("%d\n", cnt / 2 + 1);
}
int main() {
  scanf("%d%d", &n, &q);
  scanf("%s", s + 1);
  printAns();
  FOR(i, 1, q) {
    int l, r;
    scanf("%d%d", &l, &r);
    if(l == r) s[l] ^= 1;
    else {
      if(l < r) std::reverse(s + l, s + r + 1);
      else {
        FOR(i, 1, r) s[n * 6 + i] = s[i];
        std::reverse(s + l, s + n * 6 + r + 1);
        FOR(i, 1, r) s[i] = s[n * 6 + i];
      }
    }
    printAns();
  }
  return 0;
}
