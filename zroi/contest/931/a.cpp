#include<bits/stdc++.h>
#define FOR(a, b, c) for(int a = (int)(b); a <= (int)(c); a++)
namespace IO { // require C++11
  const int _BS = 200000;
  char _ob[_BS], *_z = _ob, _ib[_BS], *_x = _ib, *_y = _ib;
  // Input
  char _nc() {
    if (_x == _y) _y = (_x = _ib) + fread(_ib, 1, _BS, stdin);
    if (_x == _y) return EOF;
    return *_x++;
    // return _x==_y&&(_y=(_x=_ib)+fread(_ib,1,_BS,stdin),_x==_y)?EOF:*_x++;
  }
  inline void rd(char &c) { c = _nc(); }
  void rd(char *s) {
    char c = _nc();
    while (!isblank(c) && isprint(c) && c != EOF) *s++ = c, c = _nc();
    *s = 0;
  }
  template <class T> void rd(T &res) { // unsigned/signed int/long long/__int128
    res = 0;
    char c = _nc(), flag = 0;
    while (!isdigit(c) && c != '-') c = _nc();
    if (c == '-') flag = 1, c = _nc();
    while (isdigit(c)) res = res * 10 + c - '0', c = _nc();
    if (flag) res = -res;
  }
  template <class T, class... A> void rd(T &hd, A &...rs) { rd(hd), rd(rs...); }
  // Output
  inline void flush() { fwrite(_ob, 1, _z - _ob, stdout), _z = _ob; }
  inline void wrch(char x) {
    if (_z == _ob + _BS - 1) flush();
    *_z = x, ++_z;
  }
  void wr(char *s) {
    while (*s) wrch(*s), ++s;
  }
  void wr(const char *s) {
    while (*s) wrch(*s), ++s;
  }
  template <class T> void wr(T x) { // unsigned/signed int/long long/__int128
    if (x == 0) return wrch('0'), void();
    if (x < 0) wrch('-'), x = -x;
    int _t[50], _lt = 0;
    while (x) _t[++_lt] = x % 10, x /= 10;
    while (_lt > 0) wrch(_t[_lt] + '0'), --_lt;
  }
  inline void wr(char x) { wrch(x); }
  template <class T, class... A> void wr(T hd, A... rs) { wr(hd), wr(rs...); }
  template <class... A> void wrln(A... ar) { wr(ar...), wrch('\n'); }
  struct _Flusher {
    ~_Flusher() { flush(); }
  } _flusher;
} // namespace IO
using IO::rd;
using IO::wrln;

const int N = 6e5 + 5;

int n, q;
int lc[N], rc[N], sz[N], rv[N], stat[N], cnt[N], tot;

#define sft_l 1
#define sft_r 2
#define bit_l (1 << sft_l)
#define bit_r (1 << sft_r)
int newNode(bool col) {
  int u = ++tot;
  stat[u] = (col << sft_l) | (col << sft_r) | col;
  sz[u] = 1;
  return u;
}

inline void pu(int u) {
  assert(u);
  sz[u] = sz[lc[u]] + sz[rc[u]] + 1;
  if(lc[u] && rc[u]) {
    stat[u] = (stat[u] & 1) | (stat[lc[u]] & bit_l) | (stat[rc[u]] & bit_r);
    cnt[u] = cnt[lc[u]] + cnt[rc[u]] +
      ((stat[lc[u]] >> sft_r & 1) != (stat[u] & 1)) +
      ((stat[rc[u]] >> sft_l & 1) != (stat[u] & 1));
  } else if(lc[u]) {
    stat[u] = (stat[u] & 1) | (stat[lc[u]] & bit_l) | ((stat[u] & 1) << sft_r);
    cnt[u] = cnt[lc[u]] + ((stat[lc[u]] >> sft_r & 1) != (stat[u] & 1));
  } else if(rc[u]) {
    stat[u] = (stat[u] & 1) | (stat[rc[u]] & bit_r) | ((stat[u] & 1) << sft_l);
    cnt[u] = cnt[rc[u]] + ((stat[rc[u]] >> sft_l & 1) != (stat[u] & 1));
  } else {
    int col = stat[u] & 1;
    stat[u] = (col << sft_l) | (col << sft_r) | col;
    cnt[u] = 0;
  }
}

void nodeRev(int u) {
  rv[u] ^= 1;
  std::swap(lc[u], rc[u]);
  if((stat[u] >> sft_l & 1) != (stat[u] >> sft_r & 1))
    stat[u] ^= bit_l ^ bit_r;
}

void pd(int u) {
  if(rv[u]) {
    if(lc[u]) nodeRev(lc[u]);
    if(rc[u]) nodeRev(rc[u]);
    rv[u] = 0;
  }
}

void split(int u, int k, int & x, int & y) {
  if(!u) {
    x = 0, y = 0;
    return;
  }
  assert(k <= sz[u]);
  pd(u);
  if(k <= sz[lc[u]]) {
    y = u, split(lc[u], k, x, lc[y]), pu(y);
  } else {
    x = u, split(rc[u], k - sz[lc[u]] - 1, rc[x], y), pu(x);
  }
}

int merge(int x, int y) {
  if(!x || !y) return x + y;
  pd(x), pd(y);
  if(rand() % (sz[x] + sz[y]) < sz[x]) {
    return rc[x] = merge(rc[x], y), pu(x), x;
  } else {
    return lc[y] = merge(x, lc[y]), pu(y), y;
  }
}

char s[N];

int build(int l, int r) {
  if(l == r) {
    int u = newNode(s[l] - '0');
    return u;
  }
  int mid = (l + r) >> 1;
  int u = newNode(s[mid] - '0');
  if(l < mid) lc[u] = build(l, mid - 1);
  if(mid < r) rc[u] = build(mid + 1, r);
  pu(u);
  return u;
}

int root;

void flip(int u, int k) {
  if(!u) return;
  pd(u);
  if(sz[lc[u]] + 1 == k) {
    stat[u] ^= 1;
    pu(u);
    return;
  }
  if(k <= sz[lc[u]]) flip(lc[u], k);
  else flip(rc[u], k - sz[lc[u]] - 1);
  pu(u);
}

void Reverse(int l, int r) {
  if(l < r) {
    int x, y, z;
    split(root, l - 1, x, y);
    split(y, r - l + 1, y, z);
    nodeRev(y);
    root = merge(x, merge(y, z));
  } else {
    int x, y, z, t;
    split(root, l - 1, y, z);
    split(y, r, x, y);
    t = merge(z, x);
    nodeRev(t);
    split(t, n * 6 - l + 1, z, x);
    root = merge(x, merge(y, z));
  }
}

// void print(int u) {
//   if(!u) return;
//   pd(u);
//   print(lc[u]);
//   printf("u=%d lc=%d rc=%d stat=%d cnt=%d sz=%d\n",
//       u, lc[u], rc[u], stat[u], cnt[u], sz[u]);
//   print(rc[u]);
//   pu(u);
// }

void printAns() {
  // print(root);
  int t = cnt[root] + ((stat[root] >> sft_l & 1) != (stat[root] >> sft_r & 1));
  assert(t % 2 == 0);
  wrln(t / 2 + 1);
}

int main() {
  srand(114514);

  rd(n, q);
  rd(s + 1);

  root = build(1, n * 6);

  printAns();
  FOR(i, 1, q) {
    int l, r;
    rd(l, r);
    if(l == r) flip(root, l);
    else Reverse(l, r);
    printAns();
  }
  return 0;
}
