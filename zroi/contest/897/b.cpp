// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

const int N = 1e5 + 5;

int n, q;
int fa[N], c[N];

void Insert(int u, int val, int v) {
  fa[u] = v, c[u] = val;
  while (fa[u] && c[u] < c[fa[u]]) {
    swap(c[u], c[fa[u]]);
    u = fa[u];
  }
}

int main() {
  scanf("%d%d", &n, &q);
  FOR(i, 1, n) { scanf("%d%d", fa + i, c + i); }
  FOR(i, 1, q) {
    int op, a, b;
    scanf("%d%d", &op, &a);
    if (op == 1) {
      scanf("%d", &b);
      Insert(++n, b, a);
    } else {
      printf("%d\n", c[a]);
    }
  }
  return 0;
}
