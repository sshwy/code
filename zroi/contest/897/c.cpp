// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

const int N = 20, P = 998244353;
long long n, ans;

long long a[N], p[N], sum[N];

long long fac[N], inv[N];

long long power(long long a, long long b) {
  long long ret = 1;
  for (; b; b >>= 1, (a *= a) %= P)
    if (b & 1) (ret *= a) %= P;
  return ret;
}

void init() {
  fac[0] = 1;
  FOR(i, 1, n) fac[i] = fac[i - 1] * i % P;
  inv[n] = power(fac[n], P - 2);
  ROF(i, n, 1) inv[i - 1] = inv[i] * i % P;
}

long long binom(long long n, long long m) {
  return n < m ? 0 : fac[n] * inv[m] % P * inv[n - m] % P;
}

long long vis[N];

void calc(long long l, long long r) {
  if (l > r) return;
  ans = (ans + r - l + 1) % P;
  long long pos, mx = 0;
  FOR(i, l, r) if (a[i] > mx) mx = a[i], pos = i;
  calc(l, pos - 1), calc(pos + 1, r);
}

void dfs(long long now) {
  if (now == n + 1) { return (void)(calc(1, n)); }
  if (a[now]) return (void)(dfs(now + 1));
  FOR(i, 1, n) {
    if (p[i]) continue;
    a[now] = i;
    p[i] = 1;
    dfs(now + 1);
    p[i] = 0, a[now] = 0;
  }
}

int main() {
  cin >> n;
  init();
  FOR(i, 1, n) cin >> a[i], p[a[i]] = 1;
  ROF(i, n, 0) sum[i] = sum[i + 1] + (p[i] == 0);
  dfs(1);
  (ans *= power(fac[sum[1]], P - 2)) %= P;
  cout << ans << endl;
  return 0;
}
