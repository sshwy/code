// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

const int P = 1e9 + 7, N = 1e7 + 5;
int pw(int a, int m) {
  int res = 1;
  while (m) m & 1 ? res = 1ll * res * a % P : 0, a = 1ll * a * a % P, m >>= 1;
  return res;
}

int n;
int ans;
int f[N];

int main() {
  scanf("%d", &n);
  f[1] = 0;
  f[0] = 1;
  long long tot = 1;
  int nx = pw(2, n) - 1;
  FOR(i, 2, n) {
    tot = 1ll * tot * (nx - i + 2) % P;
    f[i] = (tot - f[i - 1] - f[i - 2] * 1ll * (i - 1) % P * (nx - i + 2) % P) % P;
    f[i] = (f[i] + P) % P;
  }
  tot = 1ll * tot * (nx - n + 1) % P;
  printf("%lld\n", (tot - f[n] + P) % P);
  return 0;
}
