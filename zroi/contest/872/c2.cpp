// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

const int P = 1e9 + 7, N = 1e7 + 5;

int n;
int sxor, ans[N];
bool use[N];

void dfs(int pos) {
  if (pos == n + 1) {
    ans[sxor]++;
    return;
  }
  FOR(i, 0, (1 << n) - 1) if (!use[i]) {
    use[i] = true;
    sxor ^= i;
    dfs(pos + 1);
    use[i] = false;
    sxor ^= i;
  }
}

int main() {
  scanf("%d", &n);
  dfs(1);
  FOR(i, 0, (1 << n) - 1) printf("%d%c", ans[i], " \n"[i == (1 << n) - 1]);
  return 0;
}
