// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

const int N = 1e5 + 5;
int n, a[N], b[N], c[N], lim;

void dfs(int i, int j, int C) {
  c[i] += a[j] * b[i - j] * C % 3;
  if (i * 3 > lim) return;
  if (i || j) dfs(i * 3, j * 3, C);
  if (i * 3 + 1 > lim) return;
  dfs(i * 3 + 1, j * 3, C);
  dfs(i * 3 + 1, j * 3 + 1, C);
  if (i * 3 + 2 > lim) return;
  dfs(i * 3 + 2, j * 3, C);
  dfs(i * 3 + 2, j * 3 + 1, C * 2 % 3);
  dfs(i * 3 + 2, j * 3 + 2, C);
}

int main() {
  scanf("%d", &n);
  lim = 2 * n - 2;
  FOR(i, 0, n - 1) scanf("%d", a + i);
  FOR(i, 0, n - 1) scanf("%d", b + i);

  dfs(0, 0, 1);

  FOR(i, 0, lim) printf("%d%c", c[i] % 3, " \n"[i == lim]);
  return 0;
}
