// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

const int N = 105;
int n, m, P;
int f[N][N][N][2], t[N][N];
int g[N][N][N][2];
// f[x, y, j, 0/1]: S 用了 x 个点，T 用了 y 个点，最后一层有 j 个点，最后一层是 S 还是
// T 的分层图的方案数 g[x, y, j, 0/1]: 与 f 对应的所有点目前的“深度”和
int c[N][N];

int pw(int a, int b) {
  assert(b >= 0);
  int res = 1;
  while (b) b & 1 ? res = 1ll * res * a % P : 0, a = 1ll * a * a % P, b >>= 1;
  return res;
}
void add(int &x, long long y) { x = (x + y) % P; }
int XXX;
void go() {
  FOR(i, 1, n) f[i][0][i][0] = c[n][i];
  FOR(j, 1, m) f[0][j][j][1] = c[m][j];
  FOR(i, 1, n + m) { // total used node
    FOR(x, 0, i) {
      int y = i - x, maxI = max(x, y);
      if (x > n || y > m) continue;
      FOR(j, 1, maxI) {
        if (f[x][y][j][0]) {
          FOR(k, 1, m - y) {
            ++XXX;
            long long coef = 1ll * t[k][j] * c[m - y][k] % P;
            add(f[x][y + k][k][1], coef * f[x][y][j][0] % P);
            add(g[x][y + k][k][1],
                g[x][y][j][0] * coef % P + f[x][y][j][0] * coef % P * y % P);
          }
        }
        if (f[x][y][j][1]) {
          FOR(k, 1, n - x) {
            ++XXX;
            long long coef = 1ll * t[k][j] * c[n - x][k] % P;
            add(f[x + k][y][k][0], f[x][y][j][1] * coef % P);
            add(g[x + k][y][k][0],
                g[x][y][j][1] * coef % P + f[x][y][j][1] * coef % P * y % P);
          }
        }
      }
    }
  }
  int ans = 0;
  FOR(i, 1, n) FOR(j, 1, m) {
    int t = g[i][j][1][0] * 1ll * pw(2, (n - i) * (m - j)) % P;
    add(ans, t);
  }
  ans = 1ll * ans * pw(1ll * pw(2, n * m) * n % P * m % P, P - 2) % P;
  printf("%d\n", ans);
  fprintf(stderr, "XXX %d\n", XXX);
}
int main() {
  scanf("%d%d%d", &n, &m, &P);

  c[0][0] = 1;
  FOR(i, 1, N - 1) {
    c[i][0] = 1;
    FOR(j, 1, i) c[i][j] = (c[i - 1][j - 1] + c[i - 1][j]) % P;
  }
  FOR(i, 1, N - 1) FOR(j, 1, N - 1) { t[i][j] = pw(pw(2, i) - 1, j); }
  go();
  return 0;
}
