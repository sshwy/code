// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

typedef pair<int, int> pt;
const int SZ = 1 << 17, BASE = SZ / 2, P = 3;

int n, a[SZ], b[SZ], c[SZ];

pt facBy3(int x) { // a * 3^b
  int p3 = 0, c2 = 0;
  for (int x2 = x; x2; x2 /= P) {
    p3 += x2 / P;
    c2 += x2 / P + (x2 % P == 2);
  }
  return {c2 % 2 ? 2 : 1, p3};
}

pt fac[SZ];
int fx[SZ];

int main() {
  scanf("%d", &n);
  FOR(i, 0, n - 1) scanf("%d", a + i);
  FOR(i, 0, n - 1) scanf("%d", b + i);

  FOR(i, 1, n * 2 - 2) fac[i] = facBy3(i), fx[i] = fac[i].second;
  fac[0] = fac[1];
  fx[0] = fac[0].second;
  FOR(i, 0, n - 1) {
    a[i] = a[i] * fac[i].first % P;
    b[i] = b[i] * fac[i].first % P;
  }

  FOR(i, 0, n - 1) if (a[i]) FOR(j, 0, n - 1) if (b[j]) {
    if (fx[i] + fx[j] == fx[i + j]) { c[i + j] += a[i] * b[j]; }
  }
  FOR(i, 0, 2 * n - 2) {
    // printf("fac[%d] %d\n", i, fac[i].first);
    printf("%d%c", c[i] % P * fac[i].first % P, " \n"[i == 2 * n - 2]);
  }
  return 0;
}
