// by Sshwy
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA
const int N = 2e5 + 5;
int h, n, c[N], a[N], fa[N][20], fw[N][20], lg[N], dep[N], dfn[N], totdfn;
bool vis[N];
vector<int> g[N];
queue<int> q;

void dfs(int u, int p) {
  fa[u][0] = p, fw[u][0] = a[u], dep[u] = dep[p] + 1, dfn[u] = ++totdfn;
  FOR(j, 1, 19) {
    fa[u][j] = fa[fa[u][j - 1]][j - 1];
    fw[u][j] = min(fw[u][j - 1], fw[fa[u][j - 1]][j - 1]);
    if (fa[u][j] == 0) break;
  }
  for (int v : g[u])
    if (v != p) dfs(v, u);
}
int lca(int u, int v) {
  if (dep[u] < dep[v]) swap(u, v);
  ROF(j, 19, 0) if (dep[u] - (1 << j) >= dep[v]) u = fa[u][j];
  if (u == v) return u;
  ROF(j, 19, 0) if (fa[u][j] != fa[v][j]) u = fa[u][j], v = fa[v][j];
  return fa[u][0];
}
int val(int u, int v) {
  if (dep[u] < dep[v]) swap(u, v);
  int res = min(a[u], a[v]);
  ROF(j, 19, 0)
  if (dep[u] - (1 << j) >= dep[v]) res = min(res, fw[u][j]), u = fa[u][j];
  if (u == v) return min(res, a[u]);
  ROF(j, 19, 0)
  if (fa[u][j] != fa[v][j])
    res = min(res, min(fw[u][j], fw[v][j])), u = fa[u][j], v = fa[v][j];
  return min(res, a[fa[u][0]]);
}
long long calc(vector<pair<int, int>> &v) {}
int main() {
  scanf("%d%d", &h, &n);
  FOR(i, 1, n) scanf("%d", &c[i]);
  FOR(i, 1, n - 1) {
    int x, y;
    scanf("%d%d", &x, &y);
    g[x].pb(y);
    g[y].pb(x);
  }

  FOR(i, 1, n) if (c[i]) q.push(i), vis[i] = 1, a[i] = 0;
  while (!q.empty()) {
    int u = q.front();
    q.pop();
    for (int v : g[u])
      if (!vis[v]) {
        vis[v] = 1;
        a[v] = a[u] + 1;
        q.push(v);
      }
  }

  dfs(1, 0);
  FOR(i, 2, n) lg[i] = lg[i / 2] + 1;

  int q;
  scanf("%d", &q);
  FOR(i, 1, q) {
    int m;
    scanf("%d", &m);
    vector<pair<int, int>> v;
    FOR(i, 1, m) {
      int u, v;
      scanf("%d%d", &u, &v);
      v.pb({u, v});
    }
    long long ans = calc(v);
    printf("%lld\n", ans);
  }

  return 0;
}
