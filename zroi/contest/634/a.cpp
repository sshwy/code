// by Sshwy
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

const int P = 998244353, N = 1e5 + 5;
long long pw(long long a, long long m) {
  int res = 1;
  a %= P;
  while (m) m & 1 ? res = 1ll * res * a % P : 0, a = 1ll * a * a % P, m >>= 1;
  return res;
}
long long p, a, b, h[N][2], g[N], p0[N];
long long d[N], ld;
long long f[100][2];

long long pr[N][2][2], ex[N][2];
// for the first i-th bit,  <=x or >x, push j to (i+1)-th
void add(long long &x, long long y) { x = (x + y) % P; }
long long F(long long x) {
  // printf("F %lld\n",x);
  ld = 0;
  while (x) d[++ld] = x % p, x /= p;
  memset(pr, 0, sizeof(pr));
  memset(ex, 0, sizeof(ex));
  pr[0][0][0] = 1;
  FOR(i, 0, ld) {
    FOR(j, 0, 1) {
      FOR(k, 0, p - 1) {
        int nj = k > d[i + 1] || (k == d[i + 1] && j);
        FOR(e, 0, 1) if (pr[i][j][e]) {
          add(pr[i + 1][nj][0], pr[i][j][e] * p0[e + k]);       // become 0
          add(pr[i + 1][nj][1], pr[i][j][e] * (1 - p0[e + k])); // become p
        }
        add(ex[i + 1][nj], ex[i][j]);
        FOR(e, 0, 1) if (pr[i][j][e]) { add(ex[i + 1][nj], pr[i][j][e] * g[e + k]); }
      }
    }
  }
  return (ex[ld][0] + pr[ld][0][1] * pw(p0[1], P - 2) % P * g[1] % P) % P;
}

int main() {
  scanf("%lld%lld%lld", &p, &a, &b);
  a = a * pw(b, P - 2) % P;

  FOR(i, 1, p - 1) {
    long long t = pw(1 - (1 - a) * h[i - 1][0] % P, P - 2);
    h[i][0] = a * t % P;
    h[i][1] = ((1 - a) * h[i - 1][1] + 1) % P * t % P;
    h[i][0] = (h[i][0] + P) % P;
    h[i][1] = (h[i][1] + P) % P;
  }
  g[p - 1] = h[p - 1][1];
  ROF(i, p - 2, 1) g[i] = (h[i][0] * g[i + 1] + h[i][1]) % P;
  // FOR(i,0,p)printf("%lld%c",g[i]," \n"[i==p]);

  ROF(i, p - 1, 1) {
    p0[i] = (1ll - a) % P * pw(1 - a * p0[i + 1] % P, P - 2) % P;
    p0[i] = (p0[i] + P) % P;
  }
  p0[0] = 1;
  FOR(i, 1, p - 1) p0[i] = p0[i] * p0[i - 1] % P;
  // FOR(i,0,p)printf("%lld%c",p0[i]," \n"[i==p]);

  long long L, R;
  scanf("%lld%lld", &L, &R);
  long long ans = (F(R) - F(L - 1) + P) % P;
  printf("%lld\n", ans);
  return 0;
}
