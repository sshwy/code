// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

const int N = 5e4 + 5, K = 256, P = 1e9 + 7;

int pw(int a, int m) {
  int res = 1;
  while (m) m & 1 ? res = 1ll * res * a % P : 0, a = 1ll * a * a % P, m >>= 1;
  return res;
}

struct PolyPointer {
  int *f, n;
  PolyPointer(int *_f, int _n) { f = _f, n = _n; }
  PolyPointer resize(int lim) { return PolyPointer(f, lim); }
  PolyPointer shift(int offset) { return PolyPointer(f + offset, n); }
  void print() { FOR(i, 0, n - 1) printf("%d ", f[i]); }
};

void mul(PolyPointer f, PolyPointer g, PolyPointer h) {
  assert(f.n == g.n);
  int n = f.n, mid = n / 2;
  if (n <= 2) {
    FOR(i, 0, n - 1)
    FOR(j, 0, n - 1) h.f[i + j] = (h.f[i + j] + 1ll * f.f[i] * g.f[j]) % P;
    return;
  }
  int t1[K] = {0}, t2[K] = {0}, t3[K] = {0};
  PolyPointer T1(t1, n * 2), T2(t2, n * 2), T3(t3, n);

  mul(f.resize(mid), g.resize(mid), T1);                       // F0 * G0
  mul(f.resize(mid).shift(mid), g.resize(mid).shift(mid), T2); // F1 * G1

  FOR(i, 0, mid - 1) {
    T3.f[i] = (f.f[i] + f.f[i + mid]) % P;
    T3.f[i + mid] = (g.f[i] + g.f[i + mid]) % P;
  }

  mul(T3.resize(mid), T3.resize(mid).shift(mid),
      h.shift(mid)); // (F1 + G1) * (F0 + G0)

  FOR(i, 0, n - 1) {
    h.f[i] = (h.f[i] + T1.f[i]) % P;
    h.f[i + n] = (h.f[i + n] + T2.f[i]) % P;
    h.f[i + mid] = (0ll + h.f[i + mid] - T1.f[i] - T2.f[i] + P + P) % P;
  }
}

int n, k;
int f[K], fac[N], fnv[N];
char s[N];

int calc(int i, int a) {
  int g[K] = {0}, c[K] = {0};

  long long coef = 1;
  FOR(i, 0, k) {
    c[i] = coef * fnv[i] % P;
    coef = coef * a % P;
  }

  f[0] = (f[0] + 1) % P;

  // FOR(i, 0, k) FOR(j, 0, i) g[i] = (g[i] + 1ll * f[j] * c[i - j]) % P;
  PolyPointer F(f, K / 2), G(c, K / 2), H(g, K / 2);
  mul(F, G, H);

  memcpy(f, g, sizeof(g));
  // FOR(j, 0, k) printf("f[%d, %d] = %lld\n", i, j, 1ll * f[j] * fac[j] % P);
  return 1ll * f[k] * fac[k] % P;
}
void go() {
  scanf("%d%d", &n, &k);
  scanf("%s", s + 1);

  memset(f, 0, sizeof(f));

  FOR(i, 1, n) {
    long long a = s[i] - '0';
    int ans = calc(i, a % P);
    printf("%d%c", ans, " \n"[i == n]);
  }
}

int main() {
  fac[0] = 1;
  FOR(i, 1, N - 1) fac[i] = 1ll * fac[i - 1] * i % P;
  fnv[N - 1] = pw(fac[N - 1], P - 2);
  ROF(i, N - 1, 1) fnv[i - 1] = 1ll * fnv[i] * i % P;

  int t;
  scanf("%d", &t);
  FOR(i, 1, t) {
    fprintf(stderr, "Case %d\n", i);
    go();
  }
  return 0;
}
