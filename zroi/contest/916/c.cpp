// by Yao
#include <bits/stdc++.h>
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
using namespace std;

const int N = 1e5 + 5;

namespace D {
  int fa[N], tot;
  int get(int u) { return fa[u] == u ? u : fa[u] = get(fa[u]); }
  bool merge(int u, int v) {
    if (get(u) == get(v)) return false;
    fa[get(u)] = get(v);
    --tot;
    return true;
  }
  bool connected() { return tot == 1; }
  void init(int n) {
    FOR(i, 0, n) fa[i] = i;
    tot = n;
  }
} // namespace D

template <class T> struct Fenwick {
  static const int SZ = N * 2;
  T c[SZ];
  void init() { fill(c, c + SZ, T::IE()); }
  void add(int pos, T val) {
    assert(pos);
    for (int i = pos; i < SZ; i += i & -i) c[i] = c[i] + val;
  }
  T get(int pos) {
    assert(pos);
    T res = T::IE();
    for (int i = pos; i > 0; i -= i & -i) res = res + c[i];
    return res;
  }
};

struct Atom {
  long long dist;
  int id, color;
  Atom() {}
  Atom(long long _dist, int _id) {
    dist = _dist;
    id = _id;
    color = D::get(id);
  }
  static Atom IE() { // Identity Element
    return Atom(LLONG_MAX, 0);
  }
  Atom operator+(const Atom a) const { return dist < a.dist ? *this : a; }
  bool operator<(const Atom a) const { return dist < a.dist; }
  // void print() { printf("(id=%d dist=%lld color=%d) ", id, dist, color); }
};

struct Data {
  Atom o1, o2;
  Data() {}
  Data(long long _dist, int _id) {
    o1 = Atom(_dist, _id);
    o2 = Atom::IE();
  }
  Data operator+(Data d) {
    Data res;
    Atom t[] = {o1, o2, d.o1, d.o2};
    sort(t, t + 4);
    res.o1 = t[0];
    bool flag = false;
    FOR(i, 1, 3) if (t[i].color == 0 || t[i].color != res.o1.color) {
      res.o2 = t[i];
      flag = true;
      break;
    }
    assert(flag);
    return res;
  }
  static Data IE() { // Identity Element
    return Data(LLONG_MAX, 0);
  }
};

Fenwick<Data> fenwick;

struct Point {
  int x, y, id, ix, iy;
  Point() {}
  Point(int _x, int _y, int _id) { x = _x, y = _y, id = _id; }
  Data toData() { return Data(0ll + x + y, id); }
  static bool byX(Point a, Point b) {
    if (a.x != b.x) return a.x < b.x;
    return a.y < b.y;
  }
};
int n;
Point a[4][N];
vector<int> dv;

long long dist[N];
int partner[N];

int dFind(int x) { return lower_bound(dv.begin(), dv.end(), x) - dv.begin() + 1; }

void upd(Atom c, Atom o) {
  if (c.color == o.color) return;
  if (!o.color) return;
  if (o.id) {
    if (c.dist - o.dist > dist[c.id]) {
      dist[c.id] = c.dist - o.dist;
      partner[c.id] = o.id;
    }
  }
}
void work(Point *v) {
  fenwick.init();
  FOR(i, 1, n) {
    Data o = fenwick.get(v[i].iy), c = v[i].toData();
    upd(c.o1, o.o1);
    upd(c.o1, o.o2);
    fenwick.add(v[i].iy, c);
  }
}

int main() {
  scanf("%d", &n);
  FOR(i, 1, n) {
    int x, y;
    dv.push_back(x);
    dv.push_back(y);
    scanf("%d%d", &x, &y);
    a[0][i] = Point(x, y, i);
    a[1][i] = Point(-x, y, i);
    a[2][i] = Point(x, -y, i);
    a[3][i] = Point(-x, -y, i);
  }
  sort(dv.begin(), dv.end());
  FOR(_, 0, 3) {
    sort(a[_] + 1, a[_] + n + 1, Point::byX);
    FOR(i, 1, n) a[_][i].ix = dFind(a[_][i].x), a[_][i].iy = dFind(a[_][i].y);
  }
  D::init(n);

  long long ans = 0;

  while (!D::connected()) {
    memset(dist, 0, sizeof(dist));
    memset(partner, 0, sizeof(partner));
    FOR(_, 0, 3) { work(a[_]); }

    vector<pair<long long, int>> v;
    FOR(i, 1, n) if (partner[i]) { v.push_back(make_pair(-dist[i], i)); }
    sort(v.begin(), v.end());
    for (auto e : v) {
      int i = e.second;
      if (D::merge(partner[i], i)) { ans += dist[i]; }
    }
  }
  printf("%lld\n", ans);
  return 0;
}
