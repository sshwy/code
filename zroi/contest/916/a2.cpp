// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

const int N = 5e4 + 5, K = 128, P = 1e9 + 7;

int pw(int a, int m) {
  int res = 1;
  while (m) m & 1 ? res = 1ll * res * a % P : 0, a = 1ll * a * a % P, m >>= 1;
  return res;
}

int n, k;
int f[K], fac[N], fnv[N];
char s[N];

int calc(int i, int a) {
  int g[K] = {0}, c[K] = {0};

  long long coef = 1;
  FOR(i, 0, k) {
    c[i] = coef * fnv[i] % P;
    coef = coef * a % P;
  }

  f[0] = (f[0] + 1) % P;

  FOR(i, 0, k) FOR(j, 0, i) g[i] = (g[i] + 1ll * f[j] * c[i - j]) % P;

  memcpy(f, g, sizeof(g));
  // FOR(j, 0, k) printf("f[%d, %d] = %lld\n", i, j, 1ll * f[j] * fac[j] % P);
  return 1ll * f[k] * fac[k] % P;
}
void go() {
  scanf("%d%d", &n, &k);
  scanf("%s", s + 1);

  memset(f, 0, sizeof(f));

  FOR(i, 1, n) {
    long long a = s[i] - '0';
    int ans = calc(i, a % P);
    printf("%d%c", ans, " \n"[i == n]);
  }
}

int main() {
  fac[0] = 1;
  FOR(i, 1, N - 1) fac[i] = 1ll * fac[i - 1] * i % P;
  fnv[N - 1] = pw(fac[N - 1], P - 2);
  ROF(i, N - 1, 1) fnv[i - 1] = 1ll * fnv[i] * i % P;

  int t;
  scanf("%d", &t);
  FOR(i, 1, t) {
    fprintf(stderr, "Case %d\n", i);
    go();
  }
  return 0;
}
