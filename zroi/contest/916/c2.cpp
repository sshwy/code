// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

const int N = 1e3 + 5;
int n;
int x[N], y[N];

struct Edge {
  int u, v;
  long long w;
  static bool byW(Edge a, Edge b) { return a.w > b.w; }
};

vector<Edge> edges;

int fa[N];
int get(int u) { return fa[u] == u ? u : fa[u] = get(fa[u]); }
int main() {
  cin >> n;
  FOR(i, 1, n) fa[i] = i;
  FOR(i, 1, n) cin >> x[i] >> y[i];
  FOR(i, 1, n) FOR(j, 1, i - 1) {
    long long dist = abs(x[i] - x[j]) + abs(y[i] - y[j]);
    edges.push_back((Edge){i, j, dist});
  }
  sort(edges.begin(), edges.end(), Edge::byW);
  long long ans = 0;
  for (Edge e : edges) {
    if (get(e.u) != get(e.v)) {
      printf("merge %d %d w = %lld\n", e.u, e.v, e.w);
      fa[get(e.u)] = get(e.v), ans += e.w;
    }
  }
  printf("%lld\n", ans);
  return 0;
}
