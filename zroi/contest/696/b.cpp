// by Sshwy
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA
const int N = 1e6 + 5;

int n, k;
int d[N];
int c[N], lc;

void calc(char *s, int *a) {
  lc = 0;
  c[0] = 0;
  d[0] = 0;
  FOR(i, 1, n) {
    if (s[i] == '1') {
      int pos = c[lc] + 1;
      int j = i - (i - pos) / k * k;
      if (j == pos) {
        c[++lc] = j;
        d[lc] = d[lc - 1] + 1;
      } else {
        c[++lc] = j;
        d[lc] = 1;
      }
      while (d[lc] == k) lc -= k;
    }
  }
  FOR(i, 1, n) a[i] = 0;
  FOR(i, 1, lc) a[c[i]] = 1;
}

char s[N], t[N];
int a[N], b[N];

void go() {
  scanf("%d%d", &n, &k);
  scanf("%s", s + 1);
  scanf("%s", t + 1);
  calc(s, a);
  calc(t, b);
  // FOR(i,1,n)printf("%d%c",a[i]," \n"[i==n]);
  // FOR(i,1,n)printf("%d%c",b[i]," \n"[i==n]);
  FOR(i, 1, n) if (a[i] != b[i]) return puts("No"), void();
  puts("Yes");
}

int main() {
  int t;
  scanf("%d", &t);
  FOR(i, 1, t) go();
  return 0;
}
