// by Sshwy
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA
const int N = 1e6 + 6;

int n, m;
vector<int> g[N];

int main() {
  scanf("%d%d", &n, &m);
  FOR(i, 1, m) {
    int u, v;
    scanf("%d%d", &u, &v);
    g[u].pb(v);
    g[v].pb(u);
  }
  int lim = 1 << n;
  int ans = 0;
  FOR(s, 0, lim - 1) {
    int tot = 0;
    FOR(u, 1, n) for (int v : g[u]) {
      if (((s >> (u - 1)) & 1) != ((s >> (v - 1)) & 1)) ++tot;
    }
    ans = max(ans, tot);
    // if(tot > m){
    //    FOR(i,0,n-1)printf("%d%c",s>>i&1," \n"[i==n-1]);
    //    FOR(u,1,n)for(int v:g[u]){
    //        if(((s>>(u-1))&1) != ((s>>(v-1))&1))printf("%d %d\n",u,v);
    //    }
    //}
  }
  puts(ans > m ? "Yes" : "No");
  return 0;
}
