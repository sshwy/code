// by Sshwy
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

const int P = 1e9 + 7;
long long n, m;
vector<long long> a, b;

long long gcd(long long a, long long b) { return b ? gcd(b, a % b) : a; }
int main() {
  scanf("%lld%lld", &n, &m);
  FOR(i, 1, m) {
    long long x, y;
    scanf("%lld%lld", &x, &y);
    if (x == 1)
      a.pb(y);
    else
      b.pb(y);
  }
  int la = a.size(), lb = b.size();
  sort(a.begin(), a.end());
  long long g = 0;
  FOR(i, 1, la - 1) { g = gcd(g, a[i] - a[0]); }
  int ans = 0;
  FOR(d, 1, g) if (g % d == 0) {
    long long L = (a[0] - 1) % d + 1, R = a[la - 1] + (n - a[la - 1]) / d * d;
    for (auto x : b)
      if ((x - a[0]) % d == 0) {
        if (a[0] <= x && x <= a[la - 1]) {
          L = R = -1;
          break;
        }
        if (x < a[0]) {
          L = max(L, x + d);
        } else if (x > a[la - 1]) {
          R = min(R, x - d);
        } else
          assert(0);
      }
    if (L == -1 && R == -1) continue;
    assert(L <= a[0] && R >= a[la - 1]);
    // printf("d=%d %d * %d\n",d,((a[0]-L)/d+1),((R-a[la-1])/d+1));
    ans = (ans + ((a[0] - L) / d + 1) * 1ll * ((R - a[la - 1]) / d + 1)) % P;
  }
  printf("%d\n", ans);
  return 0;
}
