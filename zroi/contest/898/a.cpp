// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

const int N = 1e6 + 5;

int pw(int a, long long m, int P) {
  int res = 1;
  m %= (P - 1);
  while (m) m & 1 ? res = 1ll * res * a % P : 0, a = 1ll * a * a % P, m >>= 1;
  return res;
}
int primitive_root_prime(int p) {
  vector<int> vd;
  int pp = p - 1;
  for (int i = 2; i * i <= pp; ++i) {
    if (pp % i) continue;
    vd.pb(i);
    while (pp % i == 0) pp /= i;
  }
  if (pp > 1) vd.pb(pp);
  pp = p - 1;
  FOR(i, 2, p - 1) {
    bool flag = 0;
    for (auto d : vd) {
      if (pw(i, pp / d, p) == 1) {
        flag = 1;
        break;
      }
    }
    if (flag == 0) return i;
  }
  assert(0); // prime must have primitive root
  return -1;
}

int P, lgr[N], cnt[N];

void go() {
  int p;
  scanf("%d", &p);
  int r = primitive_root_prime(p);
  FOR(i, 0, p - 1) cnt[i] = 0;
  for (int i = 0, x = 1; i < p - 1; i++, x = 1ll * x * r % p) {
    int s = 1ll * pw(x, p - 2, p) * i % p;
    printf("x %d s %d\n", x, s);
    cnt[s]++;
    printf("log %d = %d\n", x, i);
  }
  int ans = 0;
  FOR(i, 0, p - 1) ans = (ans + 1ll * cnt[i] * cnt[i]) % P;
  printf("ans %d\n", ans);
  ans = 1ll * (p - 1) % P * (p - 1) % P * ans % P;
  ans = (ans + 1ll * (p - 1) * (p - 1) % P) % P;
  printf("%d\n", ans);
}
int main() {
  int t;
  scanf("%d%d", &t, &P);
  FOR(i, 1, t) go();
  return 0;
}
