// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

const int N = 105, SZ = N * 10, INF = 2e5;

struct NetworkFlow {
  struct qxx {
    int nex, t, cap;
  } e[SZ];
  int h[N], le;
  NetworkFlow() { le = 1; }
  void clear() {
    le = 1;
    memset(h, 0, sizeof(h));
  }
  void ae(int u, int v, int cap) { e[++le] = {h[u], v, cap}, h[u] = le; }
  void af(int u, int v, int cap) {
    ae(u, v, cap);
    ae(v, u, 0);
  }

  int s, t;
  int dist[N], hh[N];
  queue<int> q;

#define for_e(i, u, v, w) \
  for (int i = h[u], v, w; v = e[i].t, w = e[i].cap, i; i = e[i].nex)
#define walk_e(i, u, v, w) \
  for (int &i = hh[u], v, w; v = e[i].t, w = e[i].cap, i; i = e[i].nex)

  bool bfs() {
    memset(dist, 0, sizeof(dist));
    dist[s] = 1;
    q.push(s);
    while (!q.empty()) {
      int u = q.front();
      q.pop();
      for_e(i, u, v, cap) {
        if (!cap || dist[v]) continue;
        dist[v] = dist[u] + 1;
        q.push(v);
      }
    }
    return dist[t] != 0;
  }

  int dfs(int u, int flow) {
    if (u == t) return flow;
    int rest = flow;
    walk_e(i, u, v, cap) {
      if (!cap || dist[u] + 1 != dist[v]) continue;
      int k = dfs(v, min(cap, rest));
      e[i].cap -= k, e[i ^ 1].cap += k;
      rest -= k;
      if (!rest) break;
    }
    return flow - rest;
  }

  int run() {
    int tot = 0;
    while (bfs()) {
      memcpy(hh, h, sizeof(h));
      for (int flow; (flow = dfs(s, INF)); tot += flow)
        ;
    }
    return tot;
  }
} G;

int n, a[N], ans;
vector<int> g1[N], g2[N];

void dfs1(int u, int p) {
  if (p) G.af(u, p, INF);
  for (int v : g1[u])
    if (v != p) dfs1(v, u);
}
void dfs2(int u, int p) {
  if (p) G.af(u, p, INF);
  for (int v : g2[u])
    if (v != p) dfs2(v, u);
}
void work(int start) {
  int tot = 0;
  G.clear();
  G.s = 0, G.t = n + 1;

  dfs1(start, 0);
  dfs2(start, 0);
  FOR(i, 1, n) {
    if (a[i] > 0)
      G.af(G.s, i, a[i]), tot += a[i];
    else
      G.af(i, G.t, -a[i]);
  }

  tot -= G.run();
  ans = max(ans, tot);
}

void go() {
  scanf("%d", &n);
  FOR(i, 1, n) scanf("%d", a + i);

  FOR(i, 1, n) {
    g1[i].clear();
    g2[i].clear();
  }

  FOR(i, 1, n - 1) {
    int u, v;
    scanf("%d%d", &u, &v);
    g1[u].pb(v);
    g1[v].pb(u);
  }
  FOR(i, 1, n - 1) {
    int u, v;
    scanf("%d%d", &u, &v);
    g2[u].pb(v);
    g2[v].pb(u);
  }
  ans = 0;
  FOR(i, 1, n) { work(i); }
  printf("%d\n", ans);
}

int main() {
  int t;
  scanf("%d", &t);
  FOR(i, 1, t) go();
  return 0;
}
