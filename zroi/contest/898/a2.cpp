// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

int t, M;

int pw(int a, long long m, int P) {
  int res = 1;
  a %= P;
  while (m) m & 1 ? res = 1ll * res * a % P : 0, a = 1ll * a * a % P, m >>= 1;
  return res;
}

void go() {
  int p;
  int ans = 0;
  scanf("%d", &p);
  FOR(n, 1, p * (p - 1)) FOR(m, 1, p * (p - 1)) {
    if (pw(n, m, p) == pw(m, n, p)) {
      // printf("n %d m %d n^m %d m^n %d\n", n, m, pw(n, m, p), pw(m, n, p));
      ++ans;
    }
  }
  printf("%d\n", ans);
}

int main() {
  scanf("%d%d", &t, &M);
  FOR(i, 1, t) go();
  return 0;
}
