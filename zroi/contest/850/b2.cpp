#include <bits/stdc++.h>
using namespace std;
#define int long long
#define rep(i, a, b) for (ll i = (a); i <= (b); ++i)
#define per(i, a, b) for (ll i = (a); i >= (b); --i)
#define cont(i, x) for (ll i = head[x]; i; i = edge[i].nex)
#define clr(a) memset(a, 0, sizeof(a))
#define ass(a, cnt) memset(a, cnt, sizeof(a))
#define cop(a, b) memcpy(a, b, sizeof(a))
#define lowbit(x) (x & -x)
#define all(x) x.begin(), x.end()
#define SC(t, x) static_cast<t>(x)
#define ub upper_bound
#define lb lower_bound
#define pqueue priority_queue
#define mp make_pair
#define pb push_back
#define pof pop_front
#define pob pop_back
#define fi first
#define se second
#define y1 y1_
#define Pi acos(-1.0)
#define iv inline void
#define enter putchar('\n')
#define siz(x) ((ll)x.size())
#define file(x) freopen(x ".in", "r", stdin), freopen(x ".out", "w", stdout)
typedef double db;
typedef long long ll;
typedef unsigned long long ull;
typedef pair<ll, ll> pii;
typedef vector<ll> vi;
typedef vector<pii> vii;
typedef queue<ll> qi;
typedef queue<pii> qii;
typedef set<ll> si;
typedef map<ll, ll> mii;
typedef map<string, ll> msi;
const ll maxn = 5e5 + 100;
const ll inf = 0x3f3f3f3f;
const ll iinf = 1 << 30;
const ll linf = 2e18;
const ll mod = 998244353;
const double eps = 1e-7;
ll chmin(ll &a, ll b) { return a = min(a, b); }
ll chmax(ll &a, ll b) { return a = max(a, b); }
ll read() {
  ll f = 1, a = 0;
  char ch = getchar();
  while (!isdigit(ch)) {
    if (ch == '-') f = -1;
    ch = getchar();
  }
  while (isdigit(ch)) {
    a = (a << 3) + (a << 1) + ch - '0';
    ch = getchar();
  }
  return a * f;
}

ll n;

ll a[maxn];

vi ans;

set<pii> s;

void solve(ll pos) {
  s.clear();
  rep(i, 1, n) s.insert(mp(i, a[i]));
  while (siz(s) > 1) {
    pii now = *(s.lb(mp(pos, 0)));
    if (s.lb(mp(pos, 0)) != s.begin()) {
      pii pre = *(--s.lb(mp(pos, 0)));
      if (pre.se <= now.se) {
        s.erase(s.find(pre)), s.erase(s.find(now));
        now.se++, s.insert(now);
        continue;
      }
    }
    if (++s.lb(mp(pos, 0)) != s.end()) {
      pii nex = *(++s.lb(mp(pos, 0)));
      if (nex.se <= now.se) {
        s.erase(s.find(nex)), s.erase(s.find(now));
        now.se++, s.insert(now);
        continue;
      }
    }
    return;
  }
  ans.pb(pos);
}

signed main() {
  n = read();
  rep(i, 1, n) a[i] = read();
  rep(i, 1, n) solve(i);
  printf("%lld\n", siz(ans));
  for (ll i : ans) printf("%lld ", i);
  enter;
  return 0;
}
