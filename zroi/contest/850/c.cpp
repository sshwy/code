// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace IO { // require C++11
  const int _BS = 200000;
  char _ob[_BS], *_z = _ob, _ib[_BS], *_x = _ib, *_y = _ib;
  // Input
  char _nc() {
    if (_x == _y) _y = (_x = _ib) + fread(_ib, 1, _BS, stdin);
    if (_x == _y) return EOF;
    return *_x++;
    // return _x==_y&&(_y=(_x=_ib)+fread(_ib,1,_BS,stdin),_x==_y)?EOF:*_x++;
  }
  inline void rd(char &c) { c = _nc(); }
  void rd(char *s) {
    char c = _nc();
    while (!isblank(c) && isprint(c) && c != EOF) *s++ = c, c = _nc();
    *s = 0;
  }
  template <class T> void rd(T &res) { // unsigned/signed int/long long/__int128
    res = 0;
    char c = _nc(), flag = 0;
    while (!isdigit(c) && c != '-') c = _nc();
    if (c == '-') flag = 1, c = _nc();
    while (isdigit(c)) res = res * 10 + c - '0', c = _nc();
    if (flag) res = -res;
  }
  template <class T, class... A> void rd(T &hd, A &...rs) { rd(hd), rd(rs...); }
  // Output
  inline void flush() { fwrite(_ob, 1, _z - _ob, stdout), _z = _ob; }
  inline void wrch(char x) {
    if (_z == _ob + _BS - 1) flush();
    *_z = x, ++_z;
  }
  void wr(char *s) {
    while (*s) wrch(*s), ++s;
  }
  void wr(const char *s) {
    while (*s) wrch(*s), ++s;
  }
  template <class T> void wr(T x) { // unsigned/signed int/long long/__int128
    if (x == 0) return wrch('0'), void();
    if (x < 0) wrch('-'), x = -x;
    int _t[50], _lt = 0;
    while (x) _t[++_lt] = x % 10, x /= 10;
    while (_lt > 0) wrch(_t[_lt] + '0'), --_lt;
  }
  inline void wr(char x) { wrch(x); }
  template <class T, class... A> void wr(T hd, A... rs) { wr(hd), wr(rs...); }
  template <class... A> void wrln(A... ar) { wr(ar...), wrch('\n'); }
  struct _Flusher {
    ~_Flusher() { flush(); }
  } _flusher;
} // namespace IO
using IO::rd;
using IO::wrln;

typedef long long LL;
const int N = 2005;
int n, m, a[N][N];
int U[N][N], L[N][N], R[N][N], D[N][N], d[N][N];
LL ans;

bool Flip, Mirror;
inline int L_(int i, int j) {
  if (!Flip && !Mirror) return L[i][j];
  if (!Flip && Mirror) return R[i][m - j + 1];
  if (Flip && !Mirror) return U[j][i];
  if (Flip && Mirror) return D[n - j + 1][i];
  assert(0);
}
inline int R_(int i, int j) {
  if (!Flip && !Mirror) return R[i][j];
  if (!Flip && Mirror) return L[i][m - j + 1];
  if (Flip && !Mirror) return D[j][i];
  if (Flip && Mirror) return U[n - j + 1][i];
  assert(0);
}
inline int U_(int i, int j) {
  if (!Flip && !Mirror) return U[i][j];
  if (!Flip && Mirror) return U[i][m - j + 1];
  if (Flip && !Mirror) return L[j][i];
  if (Flip && Mirror) return L[n - j + 1][i];
  assert(0);
}
inline int D_(int i, int j) {
  if (!Flip && !Mirror) return D[i][j];
  if (!Flip && Mirror) return D[i][m - j + 1];
  if (Flip && !Mirror) return R[j][i];
  if (Flip && Mirror) return R[n - j + 1][i];
  assert(0);
}

void work2(int lx, int rx, int ly, int mid, bool flip, bool mirror, int c[N][N]) {
  Flip = flip;
  Mirror = mirror;
  FOR(i, lx, rx) FOR(j, lx, rx) c[i][j] = d[i][j] = 0;
  FOR(i, lx, rx) FOR(j, ly, mid) {
    if (R_(i, j) + j >= mid && D_(i, j) + i > i) {
      d[i][i + 1]++, d[i][D_(i, j) + i + 1]--;
    }
  }
  FOR(i, lx, rx) FOR(j, lx + 1, rx) d[i][j] += d[i][j - 1];
  FOR(i, lx, rx) FOR(j, i + 1, rx) {
    if (min(L_(i, mid) + 1, mid - ly + 1) <= min(L_(j, mid) + 1, mid - ly + 1)) {
      c[i][j] += d[i][j];
    }
  }

  FOR(i, lx, rx) FOR(j, lx, rx) d[i][j] = 0;
  FOR(i, lx, rx) FOR(j, ly, mid) {
    if (R_(i, j) + j >= mid && i - U_(i, j) < i) {
      d[i][max(lx, i - U_(i, j))]++, d[i][i]--;
    }
  }
  FOR(i, lx, rx) FOR(j, lx + 1, rx) d[i][j] += d[i][j - 1];
  FOR(i, lx, rx) FOR(j, lx, i - 1) {
    if (min(L_(i, mid) + 1, mid - ly + 1) < min(L_(j, mid) + 1, mid - ly + 1)) {
      c[j][i] += d[i][j];
    }
  }
}

int c1[N][N], c2[N][N];

void work(int lx, int rx, int ly, int ry, bool flip) {
  if (ly == ry) return;
  int mid = (ry + ly) / 2;
  work2(lx, rx, ly, mid, flip, false, c1);
  work2(lx, rx, (flip ? n : m) - ry + 1, (flip ? n : m) - mid, flip, true, c2);
  FOR(i, lx, rx) FOR(j, i + 1, rx) {
    Flip = flip, Mirror = false;
    if (R_(i, mid) > 0 && R_(j, mid) > 0) { ans += 1ll * c1[i][j] * c2[i][j]; }
  }
}

void solve(int lx, int rx, int ly, int ry, bool flip) {
  if (lx == rx || ly == ry) return;
  if (rx - lx > ry - ly) swap(lx, ly), swap(rx, ry), flip = !flip;
  work(lx, rx, ly, ry, flip);
  int mid = (ly + ry) / 2;
  solve(lx, rx, ly, mid, flip), solve(lx, rx, mid + 1, ry, flip);
}
int main() {
  rd(n, m);
  FOR(i, 1, n) FOR(j, 1, m) rd(a[i][j]), a[i][j]++;

  FOR(i, 1, n) FOR(j, 1, m) {
    L[i][j] = a[i][j] == a[i][j - 1] ? L[i][j - 1] + 1 : 0;
    U[i][j] = a[i][j] == a[i - 1][j] ? U[i - 1][j] + 1 : 0;
  }
  ROF(i, n, 1) ROF(j, m, 1) {
    R[i][j] = a[i][j] == a[i][j + 1] ? R[i][j + 1] + 1 : 0;
    D[i][j] = a[i][j] == a[i + 1][j] ? D[i + 1][j] + 1 : 0;
  }

  solve(1, n, 1, m, false);
  wrln(ans);

  return 0;
}
