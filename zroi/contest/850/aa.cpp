/*
 * @Author: clorf
 * @Date: 2021-03-28 09:52:56
 * @Last Modified by: clorf
 * @Last Modified time: 2021-03-28 22:12:00
 */
#include <algorithm>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#define INF 0x3f3f3f3f
#define rg register
using namespace std;
const int maxn = 5e5, maxk = 5e3;
const long double Pi = acos(-1.0);
const double eps = 1e-7;
// 1.数组开小
// 2.没用long long/爆了类型存储范围
// 3.写之前式子没推清楚
// 4.变量写错(想当然typo/没想清楚含义)
// 5.写之前没想清楚复杂度
// 6.常量数字与long long连用时要加ll！！！
// 7.考虑无解和多解的情况
// 8.两个int连成爆long long的一定要加1ll！！！！
// 9.先除后乘！！！！！！
template <class T> void read(T &x) {
  x = 0;
  int f = 0;
  char ch = getchar();
  while (!isdigit(ch)) {
    f |= (ch == '-');
    ch = getchar();
  }
  while (isdigit(ch)) {
    x = (x << 1) + (x << 3) + (ch ^ 48);
    ch = getchar();
  }
  x = f ? -x : x;
  return;
}
int n, m, k;
char s[maxn + 5], t[maxn + 5];
int f[maxk + 5][(maxk << 1) + 5];
long long XXX;
inline int LCP(int a, int b) {
  // fprintf(stderr, "LCP %d %d\n", a, b);
  int num = 0;
  for (; s[a + num] == t[b + num] && a + num <= n && b + num <= m; num++) ++XXX;
  return num;
}
int main() {
  scanf("%d%d%d", &n, &m, &k);
  scanf("%s", s + 1);
  scanf("%s", t + 1);
  if (n < m) {
    swap(n, m);
    swap(s, t);
  }
  if (n - m > k) {
    puts("-1");
    fprintf(stderr, "XXX = %lld\n", XXX);
    return 0;
  }
  f[0][k] = LCP(1, 1) + 1;
  for (int i = 0; i <= k; i++) {
    if (f[i][m - n + k] == n + 1) {
      printf("%d", i);
      fprintf(stderr, "XXX = %lld\n", XXX);
      return 0;
    }
    for (int j = -i; j <= i; j++) {
      int a = f[i][j + k];
      int b = a + j;
      if (!a) continue;
      if (a <= n)
        f[i + 1][j + k - 1] = max(f[i + 1][j + k - 1], a + 1 + LCP(a + 1, b));
      if (b <= m) f[i + 1][j + k + 1] = max(f[i + 1][j + k + 1], a + LCP(a, b + 1));
      if (a <= n && b <= m)
        f[i + 1][j + k] = max(f[i + 1][j + k], a + 1 + LCP(a + 1, b + 1));
    }
  }
  puts("-1");
  fprintf(stderr, "XXX = %lld\n", XXX);
  return 0;
}
