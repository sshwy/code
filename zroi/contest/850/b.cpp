// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

const int N = 5e5 + 5;

int n, a[N], root;
int s[N], tp, lc[N], rc[N], sz[N];

void al(int u, int v) {
  // printf("lc[%d] = %d\n", u, v);
  lc[u] = v;
}
void ar(int u, int v) {
  // printf("rc[%d] = %d\n", u, v);
  rc[u] = v;
}
void build() {
  FOR(i, 1, n) {
    while (tp > 1 && a[s[tp - 1]] <= a[i]) {
      ar(s[tp - 1], s[tp]);
      --tp;
    }
    if (tp > 0 && a[s[tp]] <= a[i]) {
      al(i, s[tp]);
      --tp;
    }
    s[++tp] = i;
  }
  while (tp > 1) {
    ar(s[tp - 1], s[tp]);
    --tp;
  }
  root = s[1];
}

void dfs(int u) {
  if (!u) return;
  dfs(lc[u]), dfs(rc[u]);
  sz[u] = sz[lc[u]] + sz[rc[u]] + 1;
}
set<pair<int, int>> S;
vector<int> ans;
void dfs2(int u, int p) {
  if (!u) return;
  if (p) { S.insert(make_pair(-(a[p] - sz[u] + 1), u)); }
  if (S.empty() || a[u] >= (-S.begin()->first)) ans.pb(u);
  dfs2(lc[u], u), dfs2(rc[u], u);
  if (p) { S.erase(make_pair(-(a[p] - sz[u] + 1), u)); }
}
int main() {
  scanf("%d", &n);
  FOR(i, 1, n) scanf("%d", a + i);

  build();

  dfs(root);
  dfs2(root, 0);

  sort(ans.begin(), ans.end());
  printf("%lu\n", ans.size());
  for (unsigned i = 0; i < ans.size(); i++)
    printf("%d%c", ans[i], " \n"[i + 1 == ans.size()]);

  return 0;
}
