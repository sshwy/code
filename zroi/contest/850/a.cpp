// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

const int N = 5e5 + 5, K = 5005;

void getMax(int &x, int y) { x = max(x, y); }

/**
 * Suffix Array
 * - for lowercase letters;
 * - index start from 1;
 * - make sure SZ >= n*2
 * - h[i] means LCP(i, sa[rk[i]-1])
 * - st: ST for h[sa[i]]
 */
template <const int SZ, const int LOG_SZ = 20> struct SuffixArray {
  char s[SZ];
  int n, bin[SZ], sa[SZ], rk[SZ], tmp[SZ], h[SZ];
  int st[LOG_SZ][SZ], lg2[SZ];
  void sortTmpByRank() {
    int lb = max(27, n);
    fill(bin, bin + lb + 1, 0);
    FOR(i, 1, n) bin[rk[i]]++;
    FOR(i, 1, lb) bin[i] += bin[i - 1];
    ROF(i, n, 1) sa[bin[rk[tmp[i]]]--] = tmp[i];
  }
  void make(char *_s) { // _s index start from 0
    n = strlen(_s), s[n + 1] = 0;
    FOR(i, 1, n) s[i] = _s[i - 1];

    FOR(i, 1, n) tmp[i] = i, rk[i] = s[i] - 'a' + 1; // rk[i] must > 0 (rank '\0' = 0)
    sortTmpByRank();

    for (int j = 1; j <= n; j <<= 1) {
      int tot = 0;
      FOR(i, n - j + 1, n) tmp[++tot] = i;
      FOR(i, 1, n) if (sa[i] - j > 0) tmp[++tot] = sa[i] - j;
      sortTmpByRank();
      tmp[sa[1]] = 1, tot = 1;
      FOR(i, 2, n) {
        if (rk[sa[i]] == rk[sa[i - 1]] && rk[sa[i] + j] == rk[sa[i - 1] + j])
          tmp[sa[i]] = tot;
        else
          tmp[sa[i]] = ++tot;
      }
      swap(tmp, rk);
    }
  }
  int moveH(int x, int y, int step) {
    while (x + step <= n && y + step <= n && s[x + step] == s[y + step]) ++step;
    return step;
  }
  void calcH() {
    FOR(i, 1, n) {
      if (rk[i] == 1)
        h[i] = 0;
      else
        h[i] = moveH(i, sa[rk[i] - 1], max(0, h[i - 1] - 1));
    }
  }
  void workST() {
    FOR(i, 1, n) st[0][i] = h[sa[i]];
    FOR(i, 1, n) lg2[i] = i == 1 ? 0 : lg2[i / 2] + 1;
    for (int j = 1; (1 << j) <= n; ++j) {
      FOR(i, 1, n - (1 << j) + 1) {
        st[j][i] = min(st[j - 1][i], st[j - 1][i + (1 << (j - 1))]);
      }
    }
  }
  int lcp(int x, int y) {
    if (x == y) return n - x + 1;
    x = rk[x], y = rk[y];
    if (x > y) swap(x, y);
    ++x;
    int j = lg2[y - x + 1];
    return min(st[j][x], st[j][y - (1 << j) + 1]);
  }
  void info() {
    printf("String: %s\n", s + 1);
    printf("rk:     ");
    FOR(i, 1, n) printf("%-3d%c", rk[i], " \n"[i == n]);
    printf("sa:     ");
    FOR(i, 1, n) printf("%-3d%c", sa[i], " \n"[i == n]);
  }
};

SuffixArray<N * 2, 21> sa;

int n, m, k;
char s[N], t[N], r[N + N];

int lcp(int a, int b) { // s[a, n], t[b, m]
  if (a > n || b > m) return 0;
  int res = sa.lcp(a, b + n + 1);
  return res;
}

int _f[2][K * 2];
#define f(i, j) _f[(i)&1][(j) + K]

int main() {
  scanf("%d%d%d", &n, &m, &k);
  scanf("%s", s + 1);
  scanf("%s", t + 1);

  if (abs(n - m) > k) return puts("-1"), 0;

  FOR(i, 1, n) r[i] = s[i];
  r[n + 1] = '{';
  FOR(i, 1, m) r[i + n + 1] = t[i];

  sa.make(r + 1);
  sa.calcH();
  sa.workST();

  memset(_f, -0x3f, sizeof(_f));
  f(0, 0) = lcp(1, 1);

  FOR(i, 0, k) {
    memset(_f[(i + 1) & 1], -0x3f, sizeof(_f[0]));
    FOR(j, -i, i) if (f(i, j) >= 0) {
      int a = f(i, j), b = a - j;
      if (a < n) getMax(f(i + 1, j + 1), a + 1 + lcp(a + 2, b + 1));
      if (b < m) getMax(f(i + 1, j - 1), a + lcp(a + 1, b + 2));
      if (a < n && b < m) getMax(f(i + 1, j), a + 1 + lcp(a + 2, b + 2));
      // printf("f[%d, %d] = %d\n", i, j, f(i, j));
    }
    if (f(i, n - m) == n) return printf("%d\n", i), 0;
  }
  puts("-1");
  return 0;
}
