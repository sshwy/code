// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

const int K = 5005, N = 5e5 + 5;
void getMin(int &x, int y) { x = min(x, y); }

int n, m, k;
int f[K * 2], g[K * 2];
char s[N], t[N];

int main() {
  scanf("%d%d%d", &n, &m, &k);
  scanf("%s", s + 1);
  scanf("%s", t + 1);

  if (abs(n - m) > k) return puts("-1"), 0;

  memset(f, 0x3f, sizeof(f));

  f[0 + K] = 0;
  int maxj = min(k, m);
  FOR(j, 1, maxj) { getMin(f[j + K], f[j - 1 + K] + 1); }

  FOR(i, 1, n) {
    int minj = max(i - k, 0) - i;
    int maxj = min(i + k, m) - i;
    memset(g, 0x3f, sizeof(g));
    FOR(j, minj, maxj) {
      // f[i, j] -> g[i, j-i]
      // f[i, j] = min(f[i-1, j]+1, f[i, j-1]+1, f[i-1, j-1] + s[i] == t[j])
      getMin(g[j + K], f[j + 1 + K] + 1);
      getMin(g[j + K], g[j - 1 + K] + 1);
      getMin(g[j + K], f[j + K] + (s[i] != t[j + i]));
    }
    // FOR(j, minj, maxj) printf("f[%d, %d] = %d\n", i, j+i, g[j+K]);
    swap(f, g);
    if (*min_element(g + minj + K, g + maxj + K + 1) > k) return puts("-1"), 0;
  }

  if (f[m - n + K] > k)
    puts("-1");
  else
    printf("%d\n", f[m - n + K]);

  return 0;
}
