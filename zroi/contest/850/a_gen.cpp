// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

using namespace RA;

const int N = 5e5 + 5;
char s[N], t[N];
int main() {
  srand(clock() + time(0));
  int n = rnd(1, 100), m = rnd(1, 100), k = rnd(1, 100);
  n = m = 5e5;
  k = 5000;
  printf("%d %d %d\n", n, m, k);

  // FOR(i, 1, n) s[i] = t[i] = rnd('a', 'z');
  // FOR(j, 1, k) t[rnd(1, n)] = 0;
  // FOR(i, 1, n) if(t[i] == 0) --m;

  // FOR(i, 1, n) putchar(s[i]);
  // puts("");
  // FOR(i, 1, n) if(t[i] != 0) putchar(t[i]);
  // puts("");

  int kk = 5000;
  int k1 = 4990, k2 = kk - k1;
  // FOR(i, 1, k1) putchar(rnd('a', 'z'));
  FOR(i, 1, k1) putchar(i % 26 + 'a');
  FOR(i, 1, n - kk) putchar('b');
  FOR(i, 1, k2) putchar(i % 26 + 'a');
  // FOR(i, 1, k2) putchar(rnd('a', 'z'));
  puts("");
  // FOR(i, 1, k2) putchar(rnd('a', 'z'));
  // ROF(i, k2, 1) putchar(i%26+'a');
  FOR(i, 1, m) putchar('b');
  // ROF(i, k1, 1) putchar(i%26+'a');
  // FOR(i, 1, k1) putchar(rnd('a', 'z'));
  puts("");
  return 0;
}
