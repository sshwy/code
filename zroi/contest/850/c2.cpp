// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

const int N = 2005;
int n, m, a[N][N];
int xm[N][N], ym[N][N], am[N][N], bm[N][N];

int main() {
  scanf("%d%d", &n, &m);
  FOR(i, 1, n) FOR(j, 1, m) scanf("%d", &a[i][j]), a[i][j]++;

  ROF(i, n, 1) ROF(j, m, 1) {
    xm[i][j] = a[i][j] == a[i + 1][j] ? xm[i + 1][j] : i;
    ym[i][j] = a[i][j] == a[i][j + 1] ? ym[i][j + 1] : j;
  }
  FOR(i, 1, n) FOR(j, 1, m) {
    am[i][j] = a[i][j] == a[i - 1][j] ? am[i - 1][j] : i;
    bm[i][j] = a[i][j] == a[i][j - 1] ? bm[i][j - 1] : j;
  }

  long long ans = 0;
  FOR(x, 1, n) FOR(y, 1, m) {
    FOR(a, x + 1, xm[x][y]) FOR(b, y + 1, ym[x][y]) {
      if (am[a][b] <= x && bm[a][b] <= y) ++ans;
    }
  }
  printf("%lld\n", ans);

  return 0;
}
