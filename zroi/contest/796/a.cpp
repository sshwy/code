// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

const int N = 5e5 + 5, SZ = 10000019;

struct hashmap {
  struct qxx {
    int nex, u, v;
  } e[SZ];
  int h[SZ], le = 0;
  int hash(int x) { return x % SZ; }
  int &operator[](int u) {
    int hu = hash(u);
    for (int i = h[hu]; i; i = e[i].nex)
      if (e[i].u == u) return e[i].v;
    e[++le] = (qxx){h[hu], u, 0}, h[hu] = le;
    return e[le].v;
  }
} cnt;

int n;
int a[N], st[N][20], lg[N];
long long ans;

void init() {
  FOR(i, 1, n) st[i][0] = i;
  FOR(j, 1, 19) {
    FOR(i, 1, n - (1 << j) + 1) {
      st[i][j] = a[st[i][j - 1]] > a[st[i + (1 << j - 1)][j - 1]]
                     ? st[i][j - 1]
                     : st[i + (1 << j - 1)][j - 1];
    }
  }
  lg[1] = 0;
  FOR(i, 2, n) lg[i] = lg[i / 2] + 1;
}

int rangeMaxPos(int l, int r) {
  int j = lg[r - l + 1];
  return a[st[l][j]] > a[st[r - (1 << j) + 1][j]] ? st[l][j]
                                                  : st[r - (1 << j) + 1][j];
}

void solve(int L, int R) {
  if (L > R) return;
  if (L == R) {
    cnt[a[L]]--;
    return;
  }
  int mid = rangeMaxPos(L, R);
  if (mid - L < R - mid) {
    FOR(i, L, mid) cnt[a[i]]--;
    FOR(i, L, mid - 1) if (a[mid] > a[i]) ans += cnt[a[mid] - a[i]];
    solve(mid + 1, R);
    FOR(i, L, mid - 1) cnt[a[i]]++;
    solve(L, mid - 1);
  } else {
    FOR(i, mid, R) cnt[a[i]]--;
    FOR(i, mid + 1, R) if (a[mid] > a[i]) ans += cnt[a[mid] - a[i]];
    solve(L, mid - 1);
    FOR(i, mid + 1, R) cnt[a[i]]++;
    solve(mid + 1, R);
  }
}
int main() {
  scanf("%d", &n);
  FOR(i, 1, n) scanf("%d", a + i);
  init();
  FOR(i, 1, n) cnt[a[i]]++;
  solve(1, n);
  printf("%lld\n", ans);
  return 0;
}

/*
 * 启发式分裂
 * 删掉短区间的贡献，顺便求当前区间的答案
 * 递归长区间，长区间递归完了删除所有贡献
 * 把短区间的贡献加回来
 * 递归短区间
 */
