// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

const int P = 998244353, N1 = 5005, N2 = 1e6 + 50;
int pw(int a, int m) {
  int res = 1 % m;
  while (m) m & 1 ? res = 1ll * res * a % P : 0, a = 1ll * a * a % P, m >>= 1;
  return res;
}

int typ, n;
void add(int &x, int y) { x = (0ll + x + y) % P; }
void add(int &x, long long y) { x = (x + y % P) % P; }

int fac[N2], fnv[N2];

void init(int lim) {
  fac[0] = 1;
  FOR(i, 1, lim) fac[i] = 1ll * fac[i - 1] * i % P;
  fnv[lim] = pw(fac[lim], P - 2);
  ROF(i, lim, 1) fnv[i - 1] = fnv[i] * 1ll * i % P;
}

int binom(int a, int b) {
  if (a < 0 || b < 0 || a < b) return 0;
  return fac[a] * 1ll * fnv[b] % P * fnv[a - b] % P;
}
long long sq(long long x) {
  x %= P;
  return x * x % P;
}
int inv(int x) { return fnv[x] * 1ll * fac[x - 1] % P; }
int calc(int x) { return sq(n + 1 - 2ll * x) * inv(n + 1) % P * binom(n + 1, x) % P; }

int main() {
  scanf("%d%d", &typ, &n);

  init(n + 5);

  if (typ == 1) {
    int x;
    scanf("%d", &x);
    printf("%d\n", calc(x));
  } else {
    int ans = 0, coef = 1;
    FOR(i, 0, n / 2) {
      add(ans, coef * 1ll * calc(i) % P);
      coef = coef * 1ll * 233 % P;
    }
    printf("%d\n", ans);
  }
  return 0;
}
