// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

const int P = 998244353, N = 400;
int typ, n;
int f[N][N][N], g[N];

void add(int &x, int y) { x = (x + y) % P; }
int main() {
  scanf("%d%d", &typ, &n);

  f[0][0][0] = 1;
  FOR(i, 0, n - 1) FOR(j, 0, i) FOR(k, 0, i / 2) if (f[i][j][k]) {
    // printf("f[%d,%d,%d] = %d\n",i,j,k,f[i][j][k]);
    add(f[i + 1][j + 1][k], f[i][j][k]);
    if (j > 0)
      add(f[i + 1][j - 1][k + 1], f[i][j][k]);
    else
      add(f[i + 1][j][k], f[i][j][k]);
  }
  FOR(k, 0, n / 2) { FOR(j, 0, n) add(g[k], f[n][j][k]); }
  if (typ == 1) {
    int x;
    scanf("%d", &x);
    printf("%d\n", g[x]);
  } else {
    int ans = 0, coef = 1;
    FOR(i, 0, n / 2) {
      add(ans, coef * 1ll * g[i] % P);
      coef = coef * 1ll * 233 % P;
    }
    printf("%d\n", ans);
    FOR(i, 1, n / 2) printf("%d ", g[i]);
  }
  return 0;
}
