// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

const int N = 5e5 + 5;
int a[N];

int rnd(int L, int R) { return rand() % (R - L + 1) + L; }

int main() {
  srand(clock() + time(0));
  int n = rnd(1, 1000);
  n = 5e5;
  printf("%d\n", n);
  // FOR(i,1,n)printf("%d%c",rnd(1,1000)," \n"[i==n]);
  FOR(i, 1, n) printf("%d%c", i, " \n"[i == n]);
  return 0;
}
