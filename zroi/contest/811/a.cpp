// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

const int N = 1e4 + 5;
typedef pair<int, int> pii;

int n, a[N];
pii p[N];
pair<double, int> b[N];
pair<long long, int> w[N];

long long det(pii v1, pii v2) {
  return 1ll * v1.first * v2.second - 1ll * v2.first * v1.second;
}
void go() {
  scanf("%d", &n);
  FOR(i, 1, n) {
    int x, y;
    scanf("%d%d", &x, &y);
    p[i] = pii(x, y);
    b[i].first = atan2(y, x);
    b[i].second = i;
  }
  sort(b + 1, b + n + 1);
  FOR(i, 0, n) scanf("%d", a + i);
  sort(a, a + n + 1);
  FOR(i, 0, n) w[i] = {0, 0};
  FOR(i, 1, n) {
    int j = i + 1 > n ? 1 : i + 1;
    pii pi = p[b[i].second];
    pii pj = p[b[j].second];
    long long s = det(pi, pj);
    if (s <= 0) continue;
    w[b[i].second].first += s;
    w[b[j].second].first += s;
    w[0].first += s;
  }
  /*FOR(i,0,n){
    printf("i %d point: (%d,%d) w: %lld\n",
        i, p[i].first, p[i].second, w[i].first);
  }*/
  FOR(i, 0, n) w[i].second = i;
  sort(w, w + n + 1);
  long long ans = 0;
  FOR(i, 0, n) { ans += a[i] * 1ll * w[i].first; }
  double fans = ans / 6.0;
  printf("%.9lf\n", fans);
}
int main() {
  int t;
  scanf("%d", &t);
  FOR(i, 1, t) go();
  return 0;
}
