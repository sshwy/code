// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
#define for_adj(i, u, v) for (int i = h[u], v; v = e[i].t, i; i = e[i].nex)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

const int N = 5e4 + 5;
typedef long long LL;

int n, m;

struct graph {
  struct qxx {
    int nex, t;
  } e[N * 2];
  int h[N], le;
  void ae(int u, int v) { e[++le] = (qxx){h[u], v}, h[u] = le; }
  void ab(int u, int v) { ae(u, v), ae(v, u); }
  LL c0[N], c1[N], c2[N], tot;
  vector<pair<LL, LL>> v;
  void dfs1(int u, int p) {
    for_adj(i, u, v) if (v != p) {
      dfs1(v, u);
      c0[u] += c0[v];
      c1[u] += c1[v] + c0[v];
      c2[u] += c2[v] + 2 * c1[v] + c0[v];
    }
    c0[u]++;
  }
  void dfs2(int u, int p) {
    // printf("u %d c1 %lld c2 %lld\n", u, c1[u], c2[u]);
    v.pb({c1[u], c2[u]});
    tot += c2[u];
    for_adj(i, u, v) if (v != p) {
      LL c0u = c0[u], c1u = c1[u], c2u = c2[u];
      // int c0v = c0[v], c1v = c1[v], c2v = c2[v];
      c0[u] -= c0[v];
      c1[u] -= c1[v] + c0[v];
      c2[u] -= c2[v] + 2 * c1[v] + c0[v];

      c0[v] += c0[u];
      c1[v] += c1[u] + c0[u];
      c2[v] += c2[u] + 2 * c1[u] + c0[u];

      dfs2(v, u);

      c0[u] = c0u, c1[u] = c1u, c2[u] = c2u;
    }
  }
  void go() {
    dfs1(1, 0);
    dfs2(1, 0);
  }
} g1, g2;

typedef pair<LL, LL> point;
vector<point> &A = g1.v, &B = g2.v;

int p[N], lp;
point vec(point x, point y) { return {y.first - x.first, y.second - x.second}; }
__int128 det(point v1, point v2) {
  return __int128(1) * v1.first * v2.second - __int128(1) * v2.first * v1.second;
}
void make(vector<pair<LL, LL>> &B) { // convex
  sort(B.begin(), B.end());
  lp = 0;
  for (unsigned i = 0; i < B.size(); i++) {
    while (lp > 1 && det(vec(B[p[lp - 1]], B[p[lp]]), vec(B[p[lp]], B[i])) >= 0 ||
           lp >= 1 && B[p[lp]].first == B[i].first)
      --lp;
    p[++lp] = i;
  }
}
long long ans = 1e18;
void go() {
  for (unsigned i = 0; i < A.size(); i++) {
    LL a = (A[i].first * 2 + A[i].second) * m;
    LL b = A[i].first * 2;
    A[i] = {b, a};
  }
  sort(A.begin(), A.end());
  reverse(A.begin(), A.end());
  for (unsigned i = 0; i < B.size(); i++) {
    LL c = (B[i].first * 2 + B[i].second) * n;
    LL d = B[i].first;
    B[i] = {d, c};
  }
  for (unsigned i = 0; i < B.size(); i++) B[i].second = -B[i].second;
  make(B);
  for (unsigned i = 0; i < B.size(); i++) B[i].second = -B[i].second;
  // printf("convex: ");
  // FOR(i,1,lp){ printf("(%lld, %lld) ",B[p[i]].first, B[p[i]].second); }
  // puts("");
  // down convex
  int cur = 1;
  for (unsigned i = 0; i < A.size(); i++) {
    LL a = A[i].second;
    LL b = A[i].first;
    while (cur < lp && det(vec(B[p[cur]], B[p[cur + 1]]), point(1, -b)) >= 0) ++cur;
    LL c = B[p[cur]].second;
    LL d = B[p[cur]].first;
    ans = min(ans, a + c + b * d);
  }
}
int main() {
  scanf("%d", &n);
  FOR(i, 1, n - 1) {
    int u, v;
    scanf("%d%d", &u, &v);
    g1.ab(u, v);
  }
  scanf("%d", &m);
  FOR(i, 1, m - 1) {
    int u, v;
    scanf("%d%d", &u, &v);
    g2.ab(u, v);
  }
  g1.go();
  assert(g1.tot % 2 == 0);
  g2.go();
  assert(g2.tot % 2 == 0);
  go();
  // printf("g1.tot %lld g2.tot %lld\n",g1.tot/2, g2.tot/2);
  printf("%lld\n", ans + g1.tot / 2 + g2.tot / 2 + 1ll * m * n);
  return 0;
}
