// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

const int N = 1e5 + 5;
int n, m, ans, aans[N];
int c[N], flow[N]; // u -> u/2, up - down

struct atom {
  int dist, u;
  bool operator<(atom a) const {
    if (u == -1) return false;
    if (a.u == -1) return true;
    return dist < a.dist;
  }
  atom() { u = -1; }
  atom(int _dist, int _u) { dist = _dist, u = _u; }
  atom operator+(int d) { return atom(dist + d, u); }
} a[N]; // min dist

void init() {
  ROF(i, n, 1) {
    if (c[i]) a[i] = atom(0, i);
    if (a[i] + 1 < a[i / 2]) a[i / 2] = a[i] + 1;
    // printf("i %d a: %d %d\n", i, a[i].dist, a[i].u);
  }
}
void pushup(int u) {
  if (c[u])
    a[u] = atom(0, u);
  else
    a[u] = atom(0, -1);
  if (u * 2 <= n) {
    atom t = a[u * 2] + (flow[u * 2] > 0 ? -1 : 1);
    if (t < a[u]) a[u] = t;
  }
  if (u * 2 + 1 <= n) {
    atom t = a[u * 2 + 1] + (flow[u * 2 + 1] > 0 ? -1 : 1);
    if (t < a[u]) a[u] = t;
  }
}
void up(int u) {
  for (int v = u; v; v = v / 2) pushup(v);
}
void sub(int u) {
  assert(c[u]);
  c[u]--;
  up(u);
}
void go(int x, int mid, int y) {
  // printf("go from %d via %d to %d\n",x,mid,y);
  for (int u = x; u != mid; u /= 2) {
    if (flow[u] < 0)
      ans--;
    else
      ans++;
    flow[u]++;
  }
  for (int u = y; u != mid; u /= 2) {
    if (flow[u] > 0)
      ans--;
    else
      ans++;
    flow[u]--;
  }
}
void work(int u) {
  // printf("work %d\n", u);
  atom o = a[u];
  int d = 0, mid = u;
  for (int v = u, pv = v / 2; pv; v /= 2, pv /= 2) {
    d += flow[v] < 0 ? -1 : 1;
    atom t = a[v ^ 1] + d + (flow[v ^ 1] > 0 ? -1 : 1);
    if (t < o) o = t, mid = pv;
    if (c[pv] > 0) {
      atom t2 = a[pv] + d;
      if (t2 < o) o = t2, mid = pv;
    }
  }
  assert(o.u != -1);
  // u -> mid -> o.u
  go(u, mid, o.u);
  sub(o.u);
  up(u);
  // FOR(i,2,n)printf("flow %d : %d\n",i, flow[i]);
  // FOR(i,1,n)printf("a %d : %d, %d\n",i, a[i].dist, a[i].u);
}
int main() {
  scanf("%d%d", &n, &m);
  FOR(i, 1, n) scanf("%d", c + i);
  init();
  FOR(i, 1, m) {
    int p;
    scanf("%d", &p);
    work(p);
    aans[i] = ans;
  }
  FOR(i, 1, m) printf("%d%c", aans[i], " \n"[i == m]);
  return 0;
}
