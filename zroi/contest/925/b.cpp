#include <bits/stdc++.h>
#define FOR(a, b, c) for (int a = (int)(b); a <= (int)(c); a++)
#define ROF(a, b, c) for (int a = (int)(b); a >= (int)(c); a--)

const int N = 2e5 + 5;

int n;
char s[N];

namespace SAM {
  const int SZ = N * 2, ALP = 26;
  int tr[SZ][ALP], len[SZ], fail[SZ], rt[SZ], tot = 1, las = 1;
  void extend(char c) {
    c -= 'a';
    int u = ++tot, p = las;
    len[u] = len[las] + 1;
    while (p && tr[p][c] == 0) tr[p][c] = u, p = fail[p];
    if (!p)
      fail[u] = 1;
    else {
      int q = tr[p][c];
      if (len[q] == len[p] + 1)
        fail[u] = q;
      else {
        int cq = ++tot;
        len[cq] = len[p] + 1;
        memcpy(tr[cq], tr[q], sizeof(tr[q]));
        fail[cq] = fail[q], fail[q] = fail[u] = cq;
        while (p && tr[p][c] == q) tr[p][c] = cq, p = fail[p];
      }
    }
    las = u;
  }

  struct Range {
    int l, r;
  };
  std::vector<Range> s[SZ];
  int id[SZ], totlen[SZ], dep[SZ];
  long long ans = 0;
  int son1[SZ], son2[SZ];

  void substract(int u, int len) {
    while (!s[id[u]].empty()) {
      Range o = s[id[u]].back();
      if (o.r <= len) {
        totlen[id[u]] -= o.r - o.l;
        s[id[u]].pop_back();
      } else if (o.l < len) {
        totlen[id[u]] -= len - o.l;
        o.l = len;
        s[id[u]].pop_back();
        s[id[u]].push_back(o);
        break;
      } else
        break;
    }
  }

  void work(int u) {
    int cnt = len[u] - len[fail[u]];
    ans += (cnt - 1) * 1ll * cnt / 2;

    if (son1[u] != -1) {
      id[u] = id[son1[u]];
      if (son2[u] != -1) { substract(u, dep[son2[u]]); }
    }
    // printf("u = %d, cnt = %d, totlen = %d\n", u, cnt, totlen[id[u]]);
    // for(auto o : s[id[u]]) printf("(%d, %d) ", o.l, o.r);
    // puts("");

    ans += 1ll * cnt * totlen[id[u]];
    s[id[u]].push_back((Range){len[fail[u]], len[u]});
    totlen[id[u]] += cnt;

    if (fail[u]) {
      int p = fail[u];
      dep[p] = std::max(dep[p], dep[u]);
      if (son1[p] == -1 || dep[son1[p]] < dep[u])
        son2[p] = son1[p], son1[p] = u;
      else if (son2[p] == -1 || dep[son2[p]] < dep[u])
        son2[p] = u;
    }
  }

  int bin[SZ], a[SZ];
  long long work() {
    FOR(i, 1, tot) bin[len[i]]++;
    FOR(i, 1, tot) bin[i] += bin[i - 1];
    ROF(i, tot, 1) a[bin[len[i]]--] = i;

    FOR(i, 1, tot) son1[i] = son2[i] = -1;
    FOR(i, 1, tot) id[i] = i;
    FOR(i, 1, tot) dep[i] = len[i];

    // FOR(i, 1, tot) printf("i=%d fail=%d len=%d\n", i, fail[i], len[i]);
    ROF(i, tot, 1) {
      int u = a[i];
      work(u);
    }

    return ans;
  }
} // namespace SAM

int main() {
  scanf("%d", &n);

  scanf("%s", s + 1);

  ROF(i, n, 1) SAM::extend(s[i]);

  printf("%lld\n", SAM::work());

  return 0;
}
