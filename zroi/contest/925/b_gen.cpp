#include <bits/stdc++.h>
#define FOR(a, b, c) for (int a = (int)(b); a <= (int)(c); a++)
#define ROF(a, b, c) for (int a = (int)(b); a >= (int)(c); a--)

int rnd(int L, int R) { return rand() % (R - L + 1) + L; }

int main() {
  srand(clock() + time(0) * 1000);

  int n = rnd(1, 8);
  printf("%d\n", n);
  FOR(i, 1, n) printf("%c", rnd('a', 'b'));
  return 0;
}
