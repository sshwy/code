#include <bits/stdc++.h>
#define FOR(a, b, c) for (int a = (int)(b); a <= (int)(c); a++)
#define ROF(a, b, c) for (int a = (int)(b); a >= (int)(c); a--)

int n;

namespace BF {
  const int N = 2e5 + 5;

  char s[N];
  std::string t;
  std::set<std::string> sub;

  int main() {
    scanf("%s", s + 1);
    t = s + 1;

    FOR(i, 0, n - 1) { FOR(j, 1, n - i) sub.insert(t.substr(i, j)); }

    long long ans = 0;
    for (auto A : sub) {
      for (auto B : sub) {
        if (sub.count(A + B)) {
          bool flag = true;
          FOR(i, 0, n - 1) {
            if (t.substr(i, A.size()) == A) {
              if (i + A.size() + B.size() <= t.size() &&
                  t.substr(i, A.size() + B.size()) != A + B) {
                flag = false;
                break;
              }
            }
          }
          ans += flag;
        }
      }
    }

    printf("%lld\n", ans);
    return 0;
  }
} // namespace BF
const int N = 2e5 + 5;

char s[N];

namespace SA {
  const int SZ = N * 2;
  int rk[SZ], sa[SZ], h[SZ], st[20][N], lg[N];

  bool cmp(int x, int y) {
    FOR(i, 0, std::min(n - x, n - y)) {
      if (s[x + i] != s[y + i]) return s[x + i] < s[y + i];
    }
    return x > y;
  }
  void work() {
    FOR(i, 1, n) sa[i] = i;
    std::sort(sa + 1, sa + n + 1, cmp);
    FOR(i, 1, n) rk[sa[i]] = i;

    // printf("sa: ");
    // FOR(i, 1, n) printf("%d%c", sa[i], " \n"[i == n]);
    // printf("rk: ");
    // FOR(i, 1, n) printf("%d%c", rk[i], " \n"[i == n]);

    h[sa[1]] = 0;
    FOR(i, 2, n) {
      int x = sa[i], y = sa[i - 1];
      if (s[x] != s[y])
        h[sa[i]] = 0;
      else {
        int shift = 0;
        while (shift < std::min(n - x, n - y) && s[x + shift + 1] == s[y + shift + 1])
          ++shift;
        h[sa[i]] = shift + 1;
      }
      // printf("h[sa[%d]] = %d\n", i, h[sa[i]]);
    }

    FOR(i, 2, n) lg[i] = lg[i / 2] + 1;
    FOR(i, 1, n) st[0][i] = h[sa[i]];
    FOR(j, 1, 19) FOR(i, 1, n - (1 << j) + 1) {
      st[j][i] = std::min(st[j - 1][i], st[j - 1][i + (1 << (j - 1))]);
    }
  }
  int lcp(int x, int y) {
    if (x == y) return n - x + 1;
    x = rk[x], y = rk[y];
    if (x > y) std::swap(x, y);
    ++x;
    int j = lg[y - x + 1];
    return std::min(st[j][x], st[j][y - (1 << j) + 1]);
  }
} // namespace SA

namespace SegmentTree {
  const int SZ = N * 30;
  struct Data {
    int max_v, min_v;
    Data() { max_v = INT_MIN, min_v = INT_MAX; }
    Data(int v) { max_v = min_v = v; }
    Data operator+(const Data o) const {
      Data res;
      res.max_v = std::max(max_v, o.max_v);
      res.min_v = std::min(min_v, o.min_v);
      return res;
    }
  } s[SZ];
  int lc[SZ], rc[SZ], tot;

  void insert(int pos, int val, int &u, int l, int r) {
    if (!u) u = ++tot;
    if (l == r) {
      s[u] = Data(val);
      return;
    }
    int mid = (l + r) >> 1;
    if (pos <= mid)
      insert(pos, val, lc[u], l, mid);
    else
      insert(pos, val, rc[u], mid + 1, r);
    s[u] = s[lc[u]] + s[rc[u]];
  }

  int Merge(int u, int v) {
    if (!u || !v) return u + v;
    int z = ++tot;
    lc[z] = Merge(lc[u], lc[v]);
    rc[z] = Merge(rc[u], rc[v]);
    s[z] = s[lc[z]] + s[rc[z]];
    return z;
  }

  std::vector<int> _v;

  void getAll(int u, int l, int r) {
    if (l == r) {
      _v.push_back(l);
      return;
    }
    int mid = (l + r) >> 1;
    if (lc[u]) getAll(lc[u], l, mid);
    if (rc[u]) getAll(rc[u], mid + 1, r);
  }

  std::vector<int> GetAll(int u) {
    _v.clear();
    getAll(u, 1, n);
    return _v;
  }
  void Insert(int &u, int pos, int val) { insert(pos, val, u, 1, n); }
  Data Query(int u) { return s[u]; }
  Data queryExceptLastPos(int u, int l, int r) {
    if (l == r) return Data();
    int mid = (l + r) >> 1;
    if (rc[u])
      return s[lc[u]] + queryExceptLastPos(rc[u], mid + 1, r);
    else
      return queryExceptLastPos(lc[u], l, mid);
  }
  int queryLastPos(int u, int l, int r) {
    if (l == r) return l;
    int mid = (l + r) >> 1;
    if (rc[u])
      return queryLastPos(rc[u], mid + 1, r);
    else
      return queryLastPos(lc[u], l, mid);
  }
  Data QueryExceptLastPos(int u) { return queryExceptLastPos(u, 1, n); }
  int QueryLastPos(int u) { return queryLastPos(u, 1, n); }
} // namespace SegmentTree

namespace SAM {
  const int SZ = N * 2, ALP = 26;
  int tr[SZ][ALP], len[SZ], fail[SZ], rt[SZ], tot = 1, las = 1;
  void extend(char c, int idx) {
    c -= 'a';
    int u = ++tot, p = las;
    len[u] = len[las] + 1;
    while (p && tr[p][c] == 0) tr[p][c] = u, p = fail[p];
    if (!p)
      fail[u] = 1;
    else {
      int q = tr[p][c];
      if (len[q] == len[p] + 1)
        fail[u] = q;
      else {
        int cq = ++tot;
        len[cq] = len[p] + 1;
        memcpy(tr[cq], tr[q], sizeof(tr[q]));
        fail[cq] = fail[q], fail[q] = fail[u] = cq;
        while (p && tr[p][c] == q) tr[p][c] = cq, p = fail[p];
      }
    }
    // printf("u %d idx %d\n", u, idx);
    SegmentTree::Insert(rt[u], idx, SA::rk[idx]);
    las = u;
  }

  long long calc(int u) {
    using SA::rk;
    using SA::sa;
    using SegmentTree::Data;
    long long res = 0;

    std::vector<int> v = SegmentTree::GetAll(rt[u]);

    if (v.empty()) return 0;

    int max_v = rk[v[0]], min_v = rk[v[0]];
    for (unsigned i = 1; i < v.size(); i++) {
      int len = SA::lcp(sa[max_v], sa[min_v]) - 1;
      if (len > n - v[i]) res += len - (n - v[i]);

      max_v = std::max(max_v, rk[v[i]]);
      min_v = std::min(min_v, rk[v[i]]);
    }

    int len1 = SA::lcp(sa[max_v], sa[min_v]) - 1;

    res += std::max(len1, 0);

    res = res * (len[u] - len[fail[u]]);
    // printf("res = %lld\n", res);
    return res;
  }

  int bin[SZ], a[SZ];
  long long work() {
    FOR(i, 1, tot) bin[len[i]]++;
    FOR(i, 1, tot) bin[i] += bin[i - 1];
    ROF(i, tot, 1) a[bin[len[i]]--] = i;

    // FOR(i, 1, tot) printf("i=%d fail=%d len=%d\n", i, fail[i], len[i]);

    long long ans = 0;
    ROF(i, tot, 1) {
      int u = a[i];
      // printf("u=%d\n", u);
      ans += calc(u);
      if (fail[u]) rt[fail[u]] = SegmentTree::Merge(rt[fail[u]], rt[u]);
    }

    return ans;
  }
} // namespace SAM

int main() {
  scanf("%d", &n);
  if (n <= 40) {
    BF::main();
    return 0;
  }

  scanf("%s", s + 1);

  SA::work();

  FOR(i, 1, n) SAM::extend(s[i], i);

  printf("%lld\n", SAM::work());

  return 0;
}
