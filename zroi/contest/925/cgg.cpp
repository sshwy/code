#include <bits/stdc++.h>
using namespace std;
int read() {
  int x = 0, pos = 1;
  char ch = getchar();
  for (; !isdigit(ch); ch = getchar())
    if (ch == '-') pos = 0;
  for (; isdigit(ch); ch = getchar()) x = (x << 1) + (x << 3) + ch - '0';
  return pos ? x : -x;
}
#define db double
#define ll long long
const int N = 5e5 + 200;
#define FOR(i, a, b) for (int i = a; i <= b; ++i)
#define ROF(i, a, b) for (int i = a; i >= b; --i)
int a, b;
int f[7], g[7];
void rob() {
  g[5] = f[6];
  g[1] = f[5];
  g[2] = f[1];
  g[6] = f[2];
  f[1] = g[1];
  f[2] = g[2];
  f[5] = g[5];
  f[6] = g[6];
}
void rof() {
  g[5] = f[3];
  g[3] = f[2];
  g[2] = f[4];
  g[4] = f[5];
  f[2] = g[2];
  f[3] = g[3];
  f[4] = g[4];
  f[5] = g[5];
}
int main() {
  a = read(), b = read();
  FOR(i, 1, 6) f[i] = read();
  int pos = 1;
  ll ans = 0;
  FOR(i, 1, a) {
    ans += f[5];
    printf("hint %d\n", f[5]);
    while (1ll * b * i > 1ll * pos * a) {
      pos++;
      rob();
      ans += f[5];
      printf("hint %d\n", f[5]);
    }
    rof();
  }
  printf("%lld\n", ans);
  return 0;
}
