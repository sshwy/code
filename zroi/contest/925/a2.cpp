#include <bits/stdc++.h>
#define FOR(a, b, c) for (int a = (int)(b); a <= (int)(c); a++)
#define ROF(a, b, c) for (int a = (int)(b); a >= (int)(c); a--)

const int N = 5005;

int n, m, q;
struct Point {
  int x, y;
  void read() { scanf("%d%d", &x, &y); }
} a[N], queries[N];

bool vis[N][N];

bool ok(int x, int y, int len) {
  --len;
  // printf("ok %d %d %d\n", x, y, len);
  FOR(i, x - len + 1, x) FOR(j, y - len + 1, y) {
    if (i < 1 || j < 1) continue;
    if (i + len > m || j + len > m) continue;
    // printf("i %d j %d\n", i, j);
    bool flag = true;
    FOR(r, 0, len - 1) FOR(c, 0, len - 1) if (vis[i + r][j + c]) { flag = false; }
    if (flag) return true;
  }
  return false;
}

void go() {
  memset(vis, 0, sizeof(vis));
  scanf("%d%d", &m, &n);
  FOR(i, 1, n) {
    a[i].read();
    vis[a[i].x][a[i].y] = 1;
  }
  scanf("%d", &q);
  FOR(i, 1, q) {
    int x, y;
    scanf("%d%d", &x, &y);
    if (!ok(x, y, 2))
      puts("0");
    else {
      FOR(j, 2, m + 1) if (!ok(x, y, j)) {
        printf("%lld\n", 1ll * (j - 1) * (j - 1));
        break;
      }
    }
  }
}

int main() {
  int t;
  scanf("%d", &t);
  FOR(i, 1, t) go();
  return 0;
}
