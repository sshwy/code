#include <bits/stdc++.h>
#define FOR(a, b, c) for (long long a = (int)(b); a <= (int)(c); a++)

long long a, b;

typedef std::vector<long long> Perm;

Perm operator*(Perm p, Perm q) {
  assert(p.size() == q.size());
  Perm z(p.size());
  for (unsigned i = 0; i < p.size(); i++) z[i] = p[q[i]];
  return z;
}
Perm operator+(Perm p, Perm q) {
  assert(p.size() == q.size());
  Perm z(p.size());
  for (unsigned i = 0; i < p.size(); i++) z[i] = p[i] + q[i];
  return z;
}

/**
 * p: 置换
 * c: 执行该置换后每个面出现在底面的次数
 */
struct Transform {
  Perm p, c;
  Transform() {}
  Transform(Perm _p, Perm _c) { p = _p, c = _c; }

  Transform operator*(Transform o) const {
    Transform z;
    z.p = p * o.p;
    z.c = c * o.p + o.c;
    return z;
  }
};

Transform t_up({4, 0, 2, 3, 5, 1}, {0, 0, 0, 0, 1, 0});
Transform t_right({0, 3, 1, 4, 2, 5}, {0, 0, 0, 0, 1, 0});
Transform t_i({0, 1, 2, 3, 4, 5}, {0, 0, 0, 0, 0, 0});

Transform pw_t(Transform a, long long m) {
  Transform res = t_i;
  while (m) {
    if (m & 1) res = res * a;
    a = a * a, m >>= 1;
  }
  return res;
}
Transform solve(long long a, long long b, Transform t_a, Transform t_b) {
  if (a > b) return solve(b, a, t_b, t_a);
  if (a == 1) return pw_t(t_b, b - 1);
  long long d = b / a;
  Transform t = pw_t(t_b, b / a);
  return solve(a, b % a, t * t_a, t_b) * pw_t(t_b, d);
}

long long ans = 0;
signed main() {
  scanf("%lld%lld", &a, &b);

  Perm dice(6);

  FOR(i, 0, 5) scanf("%lld", &dice[i]);

  ans += dice[4];

  Transform t = solve(a, b, t_right, t_up);

  dice = dice * t.p;

  FOR(i, 0, 5) ans += 1ll * dice[i] * t.c[i];

  printf("%lld\n", ans);
  return 0;
}
