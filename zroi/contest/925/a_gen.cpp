#include <bits/stdc++.h>
#define FOR(a, b, c) for (int a = (int)(b); a <= (int)(c); a++)
#define ROF(a, b, c) for (int a = (int)(b); a >= (int)(c); a--)

int rnd(int L, int R) { return rand() % (R - L + 1) + L; }

void go() {
  int n = rnd(0, 200), m = rnd(2, 100);
  n = 5000, m = 1e9;
  printf("%d %d\n", m, n);
  FOR(i, 1, n) { printf("%d %d\n", rnd(0, m), rnd(0, m)); }
  int q = 100;
  q = 5000;
  printf("%d\n", q);
  FOR(i, 1, q)
  printf("%d %d\n", rnd(0, m), rnd(0, m));
}
int main() {
  srand(clock() + time(0) * 1000);

  int t = 5;
  printf("%d\n", t);
  FOR(i, 1, t) go();
  return 0;
}
