#include <bits/stdc++.h>
#define FOR(a, b, c) for (int a = (int)(b); a <= (int)(c); a++)
#define ROF(a, b, c) for (int a = (int)(b); a >= (int)(c); a--)

const int N = 5005;

int n, m, q;
struct Point {
  int x, y;
  void read() { scanf("%d%d", &x, &y); }
  static bool byX(Point p1, Point p2) { return p1.x < p2.x; }
  static bool byY(Point p1, Point p2) { return p1.y < p2.y; }
  void flip() { x = m - x; }
} a[N], b[N], queries[N];

struct QueryCache {
  int U, D, R;
};
typedef std::vector<QueryCache> QueryCacheList;

int y_upper, y_lower;

void addUpper(int y) { y_upper = std::min(y_upper, y); }

void addLower(int y) { y_lower = std::max(y_lower, y); }

bool yStrictlyInRange(int y, int y_l, int y_r) { return y_l < y && y < y_r; }

int upperA(int x) {
  ROF(i, n, 1) if (a[i].x < x) return i + 1;
  return 1;
}

void init() { y_lower = 0, y_upper = m; }

QueryCacheList lcache[N], rcache[N];

QueryCacheList work(Point o) {
  // printf("work (x=%d y=%d)\n", o.x, o.y);

  init();

  QueryCacheList v;
  bool flag = true;
  int min_i = upperA(o.x);
  FOR(i, min_i, n) {
    if (!yStrictlyInRange(o.y, y_lower, y_upper)) break;
    if (a[i].x > o.x) { v.push_back((QueryCache){y_upper, y_lower, a[i].x}); }

    if (a[i].y >= o.y)
      addUpper(a[i].y);
    else if (a[i].y <= o.y)
      addLower(a[i].y);
  }
  // bound rightside
  if (flag && yStrictlyInRange(o.y, y_lower, y_upper)) {
    if (m > o.x) v.push_back((QueryCache){y_upper, y_lower, m});
  }

  return v;
}

long long getAns(QueryCacheList cl, QueryCacheList cr) {
  if (cl.empty() || cr.empty()) return 0;

  long long ans = 0;
  unsigned pos = 0;
  for (unsigned i = 0; i < cl.size(); i++) {
    int L = m - cl[i].R, U = cl[i].U, D = cl[i].D;
#define deltaHor(p) (cr[p].R - L)
#define deltaVer(p) (std::min(U, cr[p].U) - std::max(D, cr[p].D))
#define compute(p) std::min(deltaHor(p), deltaVer(p))
    if (deltaHor(pos) < deltaVer(pos)) {
      while (pos + 1 < cr.size() && compute(pos) <= compute(pos + 1)) ++pos;
    } else {
      while (pos > 0 && compute(pos) <= compute(pos - 1)) --pos;
    }
    ans = std::max(ans, (long long)compute(pos));
  }
  return ans * ans;
}

void workAll(QueryCacheList *cache_v) {
  FOR(i, 1, n) b[i] = a[i];
  std::sort(a + 1, a + n + 1, Point::byX);
  std::sort(b + 1, b + n + 1, Point::byY);

  // FOR(i, 1, n) printf("(%d, %d)%c", a[i].x, a[i].y, " \n"[i == n]);

  FOR(i, 1, q) { cache_v[i] = work(queries[i]); }
}
void go() {
  scanf("%d%d", &m, &n);
  FOR(i, 1, n) a[i].read();
  scanf("%d", &q);
  FOR(i, 1, q) queries[i].read();

  FOR(i, 1, n) lcache[i].clear();
  FOR(i, 1, n) rcache[i].clear();

  workAll(lcache);

  FOR(i, 1, n) a[i].flip();
  FOR(i, 1, q) queries[i].flip();

  workAll(rcache);

  FOR(i, 1, q) printf("%lld\n", getAns(lcache[i], rcache[i]));
}

int main() {
  int t;
  scanf("%d", &t);
  FOR(i, 1, t) go();
  return 0;
}
