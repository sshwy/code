// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

using namespace RA;

int main() {
  srand(clock() + time(0));
  int n = rnd(1, 100), k = rnd(1, n);
  long long T = rnd(1, 100);
  // T = (1ll<<29)-1;
  printf("%d %d %lld\n", n, k, T);
  FOR(i, 0, n - 1) printf("%d%c", rnd(1, 1e9), " \n"[i == n - 1]);
  return 0;
}
