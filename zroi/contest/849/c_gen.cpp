// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

using namespace RA;

int main() {
  srand(clock() + time(0));
  int n = 40, m = n * (n - 1);
  printf("%d %d\n", n, m);
  FOR(i, 1, n) FOR(j, 1, n) if (i != j) printf("%d %d %d\n", 1, i, j);
  return 0;
}
