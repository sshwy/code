// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

const int N = 305, P = 998244353;
int pw(int a, int m) {
  int res = 1;
  while (m) m & 1 ? res = 1ll * res * a % P : 0, a = 1ll * a * a % P, m >>= 1;
  return res;
}
void add(int &x, int y) { x = (x + y) % P; }

int tmp[N][N];
int det(int g[N][N], int n) {
  memcpy(tmp, g, sizeof(tmp)), g = tmp;
  // FOR(i, 1, n) FOR(j, 1, n) printf("%d%c", g[i][j], " \n"[j == n]);
  FOR(i, 1, n) FOR(j, 1, n) g[i][j] = (g[i][j] % P + P) % P;
  int res = 1;
  FOR(i, 1, n) {
    if (!g[i][i]) FOR(j, i + 1, n) if (g[j][i]) {
        FOR(k, i, n) swap(g[i][k], g[j][k]);
        res = P - res;
        break;
      }
    if (!g[i][i]) return 0;
    FOR(j, 1, n) if (i != j) {
      int rate = g[j][i] * 1ll * pw(g[i][i], P - 2) % P;
      FOR(k, i, n) g[j][k] = (g[j][k] - g[i][k] * 1ll * rate % P + P) % P;
    }
  }
  FOR(i, 1, n) res = res * 1ll * g[i][i] % P;
  // printf("det = %d\n", res);
  return res;
}

int n, m, dg[N], ig[N], fac[N], fnv[N], ans, tot;
vector<int> out[N][N], in[N][N];
int col[N][N], L[N][N], L2[N][N];

void ae(int u, int v) {
  L[u][v]--;
  L[u][u]++;
  dg[u]++;
  ig[v]++;
}
void de(int u, int v) {
  L[u][v]++;
  L[u][u]--;
  dg[u]--;
  ig[v]--;
}
void work(int u) {

  FOR(i, 1, n) if (in[u][i].size() && out[u][i].size()) {
    if (dg[u] == 1) {
      add(ans, tot);
    } else {
      for (auto y : out[u][i]) {
        de(u, y);
        ae(n + 1, y);
        ae(u, n + 1);
      }
      de(u, n + 1);

      for (auto x : in[u][i]) {
        de(x, u), ae(x, n + 1);

        // FOR(i, 1, n+1) assert(dg[i] == ig[i]);

        int t = det(L, n);
        FOR(j, 1, n + 1) t = 1ll * t * fac[dg[j] - 1] % P;
        t = 1ll * t * fnv[out[u][i].size() - 1] % P;
        add(ans, t);

        ae(x, u), de(x, n + 1);
      }

      ae(u, n + 1);
      for (auto y : out[u][i]) {
        ae(u, y);
        de(n + 1, y);
        de(u, n + 1);
      }
    }
  }
}
int main() {
  scanf("%d%d", &n, &m);

  fac[0] = 1;
  FOR(i, 1, n + 1) fac[i] = 1ll * fac[i - 1] * i % P;
  fnv[n + 1] = pw(fac[n + 1], P - 2);
  ROF(i, n + 1, 1) fnv[i - 1] = 1ll * fnv[i] * i % P;

  FOR(j, 1, m) {
    int u, v, c;
    scanf("%d%d%d", &c, &u, &v);
    col[u][v] = c;
    ae(u, v);
    out[u][c].pb(v);
    in[v][c].pb(u);
  }
  tot = det(L, n - 1);
  FOR(j, 1, n) tot = 1ll * tot * fac[dg[j] - 1] % P;
  // printf("tot %d\n", tot);
  // FOR(j, 1, n) FOR(k, 1, n) {
  //   printf("%d%c", L[j][k], " \n"[k == n]);
  // }
  FOR(u, 1, n) work(u);
  printf("%d\n", ans);
  return 0;
}
