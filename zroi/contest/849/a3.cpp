// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

const int N = 5e5 + 5;

int n, k, T, a[N], b[N], f[N], g[N], h[N];

int main() {
  scanf("%d%d%d", &n, &k, &T);
  FOR(i, 0, n - 1) scanf("%d", a + i);
  FOR(i, 0, k - 1) f[i] = 1;
  g[0] = 1;

  FOR(_, 1, T) {
    FOR(i, 0, n - 1) FOR(j, 0, n - 1) if (f[i] && g[j]) {
      h[(i + j) % n] ^= f[i] * g[j];
    }
    FOR(i, 0, n - 1) g[i] = h[i], h[i] = 0;
  }
  FOR(i, 0, n - 1) { FOR(j, 0, n - 1) b[i] ^= a[(i + j) % n] * g[j]; }
  FOR(i, 0, n - 1) printf("%d%c", b[i], " \n"[i == n - 1]);
  return 0;
}
