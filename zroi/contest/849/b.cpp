// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

typedef __int128 LLL;
typedef pair<LLL, LLL> frac;
#define x first
#define y second

bool lt(frac x, frac y) { return LLL(x.x) * y.y < LLL(x.y) * y.x; }
bool gt(frac x, frac y) { return LLL(x.x) * y.y > LLL(x.y) * y.x; }
frac pls(frac x, frac y) { return frac(x.x + y.x, x.y + y.y); }
frac sq(frac x) { return frac(x.x * x.x, x.y * x.y); }

frac bound(frac L, frac R, frac v, int n) {
  if (sq(L) == v) return L;
  frac Mid = pls(L, R);
  if (sq(Mid) == v) return Mid;
  if (lt(v, sq(Mid))) {
    LLL l = 1, r = L.y ? (n - R.y) / L.y : 2e9;
    if (l > r || gt(sq(frac(L.x * r + R.x, L.y * r + R.y)), v)) return L;
    while (l < r) {
      LLL mid = (l + r) >> 1;
      if (gt(sq(frac(L.x * mid + R.x, L.y * mid + R.y)), v))
        l = mid + 1;
      else
        r = mid;
    }
    return bound(frac(L.x * l + R.x, L.y * l + R.y),
        frac(L.x * (l - 1) + R.x, L.y * (l - 1) + R.y), v, n);
  } else {
    LLL l = 1, r = R.y ? (n - L.y) / R.y : 2e9;
    if (l > r) return L;
    while (l < r) {
      LLL mid = (l + r + 1) >> 1;
      if (gt(sq(frac(L.x + R.x * mid, L.y + R.y * mid)), v))
        r = mid - 1;
      else
        l = mid;
    }
    return bound(frac(L.x + R.x * l, L.y + R.y * l),
        frac(L.x + R.x * (l + 1), L.y + R.y * (l + 1)), v, n);
  }
}

LLL f(LLL a, LLL b, LLL c, LLL n) {
  if (a == 0) { return (b / c) * (n + 1); }
  if (a < c && b < c) {
    LLL m = (a * n + b) / c;
    return m * n - f(c, c - b - 1, a, m - 1);
  }
  return f(a % c, b % c, c, n) + (b / c) * (n + 1) + (a / c) * (n * (n + 1) / 2);
}

void println(LLL x) {
  int d[50] = {0}, ld = 0;
  while (x) d[++ld] = x % 10, x /= 10;
  if (ld == 0) d[++ld] = 0;
  ROF(i, ld, 1) putchar('0' + d[i]);
  puts("");
}
void go() {
  int p, q, n;
  scanf("%d%d%d", &p, &q, &n);
  frac Q = bound(frac(0, 1), frac(1, 0), frac(p, q), n);

  LLL ans = n + 1;
  ans += f(Q.x, 0, Q.y, n);
  println(ans);
}

int main() {
  int t;
  scanf("%d", &t);
  FOR(i, 1, t) go();
  return 0;
}
