// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

const int N = 5e5 + 5;

int n, k, T, a[N], b[N];

int main() {
  scanf("%d%d%d", &n, &k, &T);
  FOR(i, 0, n - 1) scanf("%d", a + i);

  FOR(_, 1, T) {
    FOR(i, 0, n - 1) FOR(j, 0, k - 1) { b[i] ^= a[(i + j) % n]; }
    FOR(i, 0, n - 1) a[i] = b[i], b[i] = 0;
  }
  FOR(i, 0, n - 1) printf("%d%c", a[i], " \n"[i == n - 1]);
  return 0;
}
