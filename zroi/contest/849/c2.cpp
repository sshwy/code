// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

const int N = 305, P = 998244353;
int pw(int a, int m) {
  int res = 1;
  while (m) m & 1 ? res = 1ll * res * a % P : 0, a = 1ll * a * a % P, m >>= 1;
  return res;
}
void add(int &x, int y) { x = (x + y) % P; }

int tmp[N][N];
int det(int g[N][N], int n) {
  memcpy(tmp, g, sizeof(tmp)), g = tmp;
  // FOR(i, 1, n) FOR(j, 1, n) printf("%d%c", g[i][j], " \n"[j == n]);
  FOR(i, 1, n) FOR(j, 1, n) g[i][j] = (g[i][j] % P + P) % P;
  int res = 1;
  FOR(i, 1, n) {
    if (!g[i][i]) FOR(j, i + 1, n) if (g[j][i]) {
        FOR(k, i, n) swap(g[i][k], g[j][k]);
        res = P - res;
        break;
      }
    if (!g[i][i]) return 0;
    FOR(j, 1, n) if (i != j) {
      int rate = g[j][i] * 1ll * pw(g[i][i], P - 2) % P;
      FOR(k, i, n) g[j][k] = (g[j][k] - g[i][k] * 1ll * rate % P + P) % P;
    }
  }
  FOR(i, 1, n) res = res * 1ll * g[i][i] % P;
  // printf("det = %d\n", res);
  return res;
}

int n, m, dg[N], fac[N], ans, tot;
vector<int> out[N][N], in[N][N];
int col[N][N], L[N][N], L2[N][N];

void ae(int u, int v) {
  L[u][v]--;
  L[u][u]++;
  dg[u]++;
}
void de(int u, int v) {
  L[u][v]++;
  L[u][u]--;
  dg[u]--;
}
void work(int u) {
  FOR(i, 1, n) if (in[u][i].size() && out[u][i].size()) {
    if (dg[u] == 1) {
      add(ans, tot);
    } else {
      for (auto x : in[u][i])
        for (auto y : out[u][i]) {
          de(x, u), de(u, y), ae(x, y);
          int t = det(L, n - 1);
          FOR(j, 1, n) t = 1ll * t * fac[dg[j] - 1] % P;
          add(ans, t);
          // printf("x %d y %d t %d\n", x, y, t);
          ae(x, u), ae(u, y), de(x, y);
        }
    }
  }
}
int main() {
  scanf("%d%d", &n, &m);
  fac[0] = 1;
  FOR(i, 1, n) fac[i] = 1ll * fac[i - 1] * i % P;
  FOR(j, 1, m) {
    int u, v, c;
    scanf("%d%d%d", &c, &u, &v);
    col[u][v] = c;
    ae(u, v);
    out[u][c].pb(v);
    in[v][c].pb(u);
  }
  tot = det(L, n - 1);
  FOR(j, 1, n) tot = 1ll * tot * fac[dg[j] - 1] % P;
  // FOR(j, 1, n) FOR(k, 1, n) {
  //   printf("%d%c", L[j][k], " \n"[k == n]);
  // }
  FOR(u, 1, n) work(u);
  printf("%d\n", ans);
  return 0;
}
