// by Yao
#include <bits/stdc++.h>
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
using namespace std;

const int SZ = 1 << 19, P = 998244353;
int tr[SZ];
int init(int n) {
  int len = 1;
  while (len <= n) len *= 2;
  FOR(i, 0, len - 1) tr[i] = (tr[i >> 1] >> 1) | ((i & 1) * (len >> 1));
  return len;
}
int pw(int a, int m) {
  int res = 1;
  while (m) m & 1 ? res = 1ll * res * a % P : 0, a = 1ll * a * a % P, m >>= 1;
  return res;
}
void dft(int *f, int len, int tag) {
  FOR(i, 0, len - 1) if (tr[i] < i) swap(f[i], f[tr[i]]);
  for (int j = 1; j < len; j <<= 1) {
    for (int i = 0, wn = pw(3, P - 1 + (P - 1) / (j << 1) * tag); i < len;
         i += j << 1) {
      for (int k = i, u, v, w = 1; k < i + j; ++k, w = 1ll * w * wn % P) {
        u = f[k], v = 1ll * w * f[k + j] % P;
        f[k] = (u + v) % P, f[k + j] = (u - v + P) % P;
      }
    }
  }
  if (tag == -1) {
    int in = pw(len, P - 2);
    FOR(i, 0, len - 1) f[i] = f[i] * 1ll * in % P;
  }
}
int f[SZ];

int main() {
  FOR(i, 0, 5) f[i] = 1;
  int len = init(10);
  dft(f, len, 1);
  dft(f, len, -1);
  FOR(i, 0, len - 1) printf("%d%c", f[i], " \n"[i == len - 1]);
}
