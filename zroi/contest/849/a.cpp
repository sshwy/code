// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

const int N = 5e5 + 5, P = 998244353, SZ = 1 << 20;

int tr[SZ];
int init(int n) {
  int len = 1;
  while (len <= n) len *= 2;
  FOR(i, 0, len - 1) tr[i] = (tr[i >> 1] >> 1) | ((i & 1) * (len >> 1));
  return len;
}
int pw(int a, int m) {
  int res = 1;
  while (m) m & 1 ? res = 1ll * res * a % P : 0, a = 1ll * a * a % P, m >>= 1;
  return res;
}
void dft(int *f, int len, int tag) {
  FOR(i, 0, len - 1) if (tr[i] < i) swap(f[i], f[tr[i]]);
  for (int j = 1; j < len; j <<= 1) {
    for (int i = 0, wn = pw(3, P - 1 + (P - 1) / (j << 1) * tag); i < len;
         i += j << 1) {
      for (int k = i, u, v, w = 1; k < i + j; ++k, w = 1ll * w * wn % P) {
        u = f[k], v = 1ll * w * f[k + j] % P;
        f[k] = (u + v) % P, f[k + j] = (u - v + P) % P;
      }
    }
  }
  if (tag == -1) {
    int in = pw(len, P - 2);
    FOR(i, 0, len - 1) f[i] = f[i] * 1ll * in % P;
  }
}

int n, k, a[N], b[N], c[N];

void poly_power(int f[SZ], int lf, long long m, int g[SZ]) {
  memset(g, 0, sizeof(g[0]) * SZ);
  g[0] = 1;

  int len = init(lf * 2);

  while (m) {
    // printf("m %lld\n", m);
    dft(f, len, 1);
    if (m % 2) {
      dft(g, len, 1);
      FOR(i, 0, len - 1) g[i] = 1ll * g[i] * f[i] % P;
      dft(g, len, -1);

      FOR(i, 0, lf - 1) g[i] = (g[i] + g[i + lf]) % 2, g[i + lf] = 0;
      // FOR(i, lf, len-1) assert(g[i] == 0);
    }

    FOR(i, 0, len - 1) f[i] = 1ll * f[i] * f[i] % P;
    dft(f, len, -1);

    FOR(i, 0, lf - 1) f[i] = (f[i] + f[i + lf]) % 2, f[i + lf] = 0;
    // FOR(i, lf, len-1) assert(f[i] == 0);

    m >>= 1;
  }
}

int f[SZ], g[SZ];

int main() {
  long long T;
  scanf("%d%d%lld", &n, &k, &T);
  FOR(i, 0, n - 1) scanf("%d", a + i);
  FOR(i, 0, k - 1) f[i] = 1;
  poly_power(f, n, T, g);

  reverse(g, g + n);
  // FOR(i, 0, n-1) printf("%d%c", g[i], " \n"[i == n-1]);

  int len = init(n * 3);

  dft(g, len, 1);

  FOR(i, 0, 29) {
    memset(c, 0, sizeof(c[0]) * n * 2);
    FOR(j, 0, 2 * n - 2) c[j] = a[j % n] >> i & 1;
    // printf("1: "); FOR(j, 0, 2*n-2) printf("%d%c", c[j], " \n"[j == 2*n-2]);

    dft(c, len, 1);
    FOR(j, 0, len - 1) c[j] = 1ll * c[j] * g[j] % P;
    dft(c, len, -1);

    // printf("2: "); FOR(j, 0, 2*n-2) printf("%d%c", c[j], " \n"[j == 2*n-2]);
    FOR(j, 0, n - 1) b[j] ^= (c[n - 1 + j] % 2) << i;
  }

  FOR(i, 0, n - 1) printf("%d%c", b[i], " \n"[i == n - 1]);
  return 0;
}
