// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

void go() {
  int p = 691, q = 832;
  long long ans = 1;
  long double lim = sqrt(p * 1.0 / q);
  int step = 1e9 / 5e3;
  FOR(x, 1, 1e9) {
    ans += floor(lim * x) + 1;
    if (x % step == 0) { printf("0x%llx,", ans); }
  }
  printf("step: %d\n", step);
}

int main() {
  go();
  return 0;
}
