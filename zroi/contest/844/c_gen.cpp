// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

using namespace RA;

int main() {
  srand(clock() + time(0));
  int n = rnd(2, 20), q = rnd(0, 20);
  printf("%d\n", n);
  FOR(i, 1, n - 1) { printf("%d %d %d\n", rnd(1, i), i + 1, rnd(1, 40)); }
  printf("%d\n", q);
  FOR(i, 1, q) {
    int x = rnd(1, n - 1);
    int y = rnd(1, 40);
    printf("%d %d\n", x, y);
  }
  return 0;
}
