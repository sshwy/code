// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

const int N = 1005, M = N, T = N + M, P = 1e9 + 7;
int pw(int a, int m) {
  int res = 1;
  while (m) m & 1 ? res = 1ll * res * a % P : 0, a = 1ll * a * a % P, m >>= 1;
  return res;
}

int add(int x, int y) { return (x + y) % P; }
int mul(int x, int y) { return 1ll * x * y % P; }
int frac(int a, int b) { return 1ll * a * pw(b, P - 2) % P; }

struct Atom {
  int p, e;
  Atom() { p = e = 0; }
  Atom(int _p, int _e) { p = _p, e = _e; }
  Atom operator+(const Atom a) { return Atom(add(p, a.p), add(e, a.e)); }
  void operator+=(const Atom a) { *this = *this + a; }
  Atom operator*(const Atom a) {
    return Atom(mul(p, a.p), add(mul(e, a.p), mul(p, a.e)));
  }
  Atom operator*(int v) { return Atom(mul(p, v), mul(e, v)); }
};
int n, m, t;

Atom x[N][T], f[N][T];
int px[N][T], prex[N][T], pprex[N][T];

int main() {
  scanf("%d%d%d", &n, &m, &t);
  t = min(t, m + n);

  int im = frac(1, m);

  FOR(i, 0, n) {
    prex[i][0] = 1;
    pprex[i][0] = 1;
    FOR(j, 1, t) {
      if (i == 0)
        px[i][j] = 0;
      else {
        px[i][j] = mul(px[i][j - 1], px[i - 1][j - 1]);
        if (j <= m) px[i][j] = add(px[i][j], im);
      }
      x[i][j] = Atom(px[i][j], mul(px[i][j], j));
      prex[i][j] = mul(px[i][j], prex[i][j - 1]);
      pprex[i][j] = add(pprex[i][j - 1], frac(1, prex[i][j]));
    }
  }

  FOR(i, 0, n) {
    if (i == 0)
      FOR(j, 1, t) f[i][j] = Atom(1, 0);
    else {
      Atom A, B, C, S;

      FOR(j, 1, t - 1) {
        f[i][j] += A;
        A += x[i][j] * f[i - 1][j];
      }
      A += x[i][t] * f[i - 1][t];
      f[i][t] += A;

      if (i - 1) {
        FOR(k, m + 1, t) {
          B += f[i - 1][k] * Atom(1, k) * im * prex[i - 1][k - 1];
          C += f[i - 1][k] * Atom(1, k) * im * prex[i - 1][k - 1] *
               pprex[i - 1][min(k - 1, m) - 1];
          if (k <= m) C += f[i - 1][k] * Atom(1, k) * im;
        }
        f[i][m] += B * (P - pprex[i - 1][m - 1]) + C;

        ROF(j, m - 1, 1) {
          B += f[i - 1][j + 1] * Atom(1, j + 1) * im * prex[i - 1][j];
          C += f[i - 1][j + 1] * Atom(1, j + 1) * im * prex[i - 1][j] *
               pprex[i - 1][min(j, m) - 1];
          if (j + 1 <= m) C += f[i - 1][j + 1] * Atom(1, j + 1) * im;
          f[i][j] += B * (P - pprex[i - 1][j - 1]) + C;
        }
      } else {
        ROF(j, m, 1) {
          f[i][j] += S;
          S += f[i - 1][j] * Atom(1, j) * im;
        }
      }
    }
  }

  printf("%d\n", f[n][t].e);

  return 0;
}
