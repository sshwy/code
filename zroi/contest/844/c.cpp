// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

const int N = 1e5 + 5, W = 1e6 + 5;

struct qxx {
  int nex, t, v;
} e[N * 2];
int h[N], le;

void ae(int f, int t, int v) { e[++le] = (qxx){h[f], t, v}, h[f] = le; }

int n, q, sz[N], sm[N], cnt[W], c2[W], mu[W], pn[W], lp;
bool bp[W];
vector<int> d[W];
bool cut[N];

void Size(int u, int p) {
  sz[u] = 1, sm[u] = 0;
  for (int i = h[u]; i; i = e[i].nex) {
    int v = e[i].t;
    if (cut[v] || v == p) continue;
    Size(v, u);
    sz[u] += sz[v];
    sm[u] = max(sm[u], sz[v]);
  }
}
int Val(int u, const int T) { return max(sm[u], T - sz[u]); }
int Core(int u, int p, const int T) {
  int z = u;
  for (int i = h[u]; i; i = e[i].nex) {
    int v = e[i].t;
    if (cut[v] || v == p) continue;
    int x = Core(v, u, T);
    if (Val(z, T) > Val(x, T)) z = x;
  }
  return z;
}
int tmp[N], lt;
void dfsAdd(int u, int p, int wu) {
  tmp[++lt] = wu;
  for (int i = h[u]; i; i = e[i].nex) {
    int v = e[i].t, w = e[i].v;
    if (cut[v] || v == p) continue;
    dfsAdd(v, u, __gcd(wu, w));
  }
}
long long work(int u, int p, int wu) {
  lt = 0;
  dfsAdd(u, p, wu);
  long long res = 0;
  FOR(i, 1, lt) cnt[tmp[i]]++;
  res += cnt[0] * 1ll * cnt[1];
  FOR(i, 1, lt) {
    int x = tmp[i];
    if (cnt[x] > 0) {
      for (auto e : d[x]) c2[e] += cnt[x];
      cnt[x] = -1;
    }
  }
  FOR(i, 1, lt) {
    int x = tmp[i];
    if (cnt[x] == -1) {
      for (auto e : d[x]) {
        if (c2[e]) {
          res += 1ll * c2[e] * (c2[e] - 1) / 2 * mu[e];
          c2[e] = 0;
        }
      }
      cnt[x] = 0;
    }
  }
  return res;
}
long long ans;
void Solve(int u, int p) {
  Size(u, p);
  int core = Core(u, p, sz[u]);
  ans += work(core, core, 0);
  for (int i = h[core]; i; i = e[i].nex) {
    int v = e[i].t, w = e[i].v;
    if (cut[v]) continue;
    ans -= work(v, core, w);
  }
  cut[core] = true;
  for (int i = h[core]; i; i = e[i].nex) {
    int v = e[i].t;
    if (!cut[v]) Solve(v, core);
  }
}
void go() {
  memset(cut, 0, sizeof(cut));
  ans = 0;
  Solve(1, 0);
  printf("%lld\n", ans);
}
void init() {
  bp[0] = bp[1] = true, mu[1] = 1;
  FOR(i, 2, W - 1) {
    if (!bp[i]) pn[++lp] = i, mu[i] = -1;
    FOR(j, 1, lp) {
      if (i * pn[j] >= W) break;
      bp[i * pn[j]] = true;
      if (i % pn[j] == 0) {
        mu[i * pn[j]] = 0;
        break;
      } else {
        mu[i * pn[j]] = -mu[i];
      }
    }
  }
  FOR(i, 1, W - 1) if (mu[i]) {
    for (int j = i; j < W; j += i)
      if (mu[j]) { d[j].pb(i); }
  }
}
int calc(int x) {
  for (int i = 2; i * i <= x; ++i) {
    if (x % i == 0) {
      while (x % i == 0) x /= i;
      x *= i;
    }
  }
  return x;
}
int main() {
  init();
  scanf("%d", &n);
  FOR(i, 1, n - 1) {
    int a, b, c;
    scanf("%d%d%d", &a, &b, &c);
    c = calc(c);
    ae(a, b, c);
    ae(b, a, c);
  }
  go();
  scanf("%d", &q);
  FOR(i, 1, q) {
    int x, y;
    scanf("%d%d", &x, &y);
    y = calc(y);
    e[x * 2 - 1].v = y;
    e[x * 2].v = y;
    go();
  }
  return 0;
}
