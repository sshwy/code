#include <algorithm>
#include <cstdio>
#include <cstring>
using namespace std;
const int mod = 1e9 + 7;
inline int addmod(int x) { return x >= mod ? x - mod : x; }
inline int submod(int x) { return x < 0 ? x + mod : x; }
int fpow(int x, int y) {
  int ans = 1;
  while (y) {
    if (y & 1) ans = 1ll * ans * x % mod;
    x = 1ll * x * x % mod;
    y /= 2;
  }
  return ans;
}
struct pt {
  int p, e;
  pt() { p = e = 0; }
  pt(int p, int e)
      : p(p)
      , e(e) {}
} f[2005][2005], sumf[2005];
pt operator+(const pt x, const pt y) {
  return pt(addmod(x.p + y.p), addmod(x.e + y.e));
}
pt operator-(const pt x, const pt y) {
  return pt(submod(x.p - y.p), submod(x.e - y.e));
}
pt operator*(const pt x, const pt y) {
  return pt(1ll * x.p * y.p % mod, (1ll * x.p * y.e + 1ll * y.p * x.e) % mod);
}
int n, m, t, g[2005][2005], dp[2005][2005];
int main() {
  scanf("%d%d%d", &n, &m, &t);
  t = min(t, n + m);
  int invm = fpow(m, mod - 2);
  for (int i = 1; i <= n; i++)
    for (int j = 1; j <= t; j++) {
      if (j <= m) dp[i][j] = invm;
      if (j > 1)
        dp[i][j] = addmod(dp[i][j] + 1ll * dp[i][j - 1] * dp[i - 1][j - 1] % mod);
    }
  for (int i = 1; i <= t; i++) f[1][i] = pt(dp[n][i], 1ll * dp[n][i] * i % mod);
  for (int i = 2; i <= n; i++) {
    for (int j = 1; j <= t; j++) sumf[j] = sumf[j - 1] + f[i - 1][j];
    pt qwq;
    for (int j = 1; j <= t; j++) {
      pt sum = sumf[t];
      if (j != t) sum = sum - f[i - 1][j];
      f[i][j] = sum * pt(dp[n - i + 1][j], 1ll * dp[n - i + 1][j] * j % mod);
      f[i][j] = f[i][j] - qwq * pt(1, j);
      qwq = qwq + pt(dp[n - i + 1][j], 0) * f[i - 1][j];
      qwq = qwq * pt(dp[n - i][j], 0);
    }
  }
  /*	for(int i=1;i<=n;i++)
    {
      for(int j=1;j<=t;j++)
        printf("%lld ",8ll*f[i][j].e%mod);
      printf("\n");
    }*/
  int ans = 0;
  for (int i = 1; i <= t; i++) ans = addmod(ans + f[n][i].e);
  printf("%d\n", ans);
  return 0;
}
