// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

const int N = 1e5 + 5;
struct qxx {
  int nex, t, v;
} e[N * 2];
int h[N], le;
void ae(int f, int t, int v) { e[++le] = (qxx){h[f], t, v}, h[f] = le; }

int n, q;

long long ans;
void dfs(int u, int p, int wu) {
  if (wu == 1) ++ans;
  for (int i = h[u]; i; i = e[i].nex) {
    int v = e[i].t, w = e[i].v;
    if (v == p) continue;
    dfs(v, u, __gcd(wu, w));
  }
}
void go() {
  ans = 0;
  FOR(i, 1, n) { dfs(i, 0, 0); }
  printf("%lld\n", ans / 2);
}
int main() {
  scanf("%d", &n);
  FOR(i, 1, n - 1) {
    int a, b, c;
    scanf("%d%d%d", &a, &b, &c);
    ae(a, b, c);
    ae(b, a, c);
  }
  go();
  scanf("%d", &q);
  FOR(i, 1, q) {
    int x, y;
    scanf("%d%d", &x, &y);
    e[x * 2 - 1].v = y;
    e[x * 2].v = y;
    go();
  }
  return 0;
}
