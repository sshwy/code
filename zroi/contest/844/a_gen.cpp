// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

using namespace RA;

void go() {
  int n = rnd(1, 5), a = rnd(1, 5), b = rnd(a, 10);
  printf("%d %d %d\n", n, a, b);
  FOR(i, 1, n) printf("%d%c", rnd(1, 20), " \n"[i == n]);
}
int main() {
  srand(clock() + time(0));
  int t = 5;
  printf("%d\n", t);
  FOR(i, 1, t) go();
  return 0;
}
