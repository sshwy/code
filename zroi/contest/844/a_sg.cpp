// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

const int SZ = 500;
int n, a, b;

int _sg[SZ], _vsg[SZ];
int sg(int x) {
  assert(x < a || x > b);
  if (_vsg[x]) return _sg[x];
  if (x < a) return 0;
  bitset<SZ> B;
  FOR(i, a, b) {
    if (i > x) break;
    if (a <= x - i && x - i <= b) continue;
    B[sg(x - i)] = 1;
  }
  while (B[_sg[x]]) ++_sg[x];
  _vsg[x] = true;
  return _sg[x];
}

int sg2(int x) {
  if (x < a) return 0;
  x -= b;
  x %= (a + b);
  if (a == 1) return (x - 1 + a + b) % (a + b);
  if (x / a == 0) return 1;
  if (x / a == 1) return 0;
  return x / a;
}

int main() {
  // cin>>a>>b;
  FOR(_a, 1, 10) FOR(_b, _a, 100) {
    a = _a, b = _b;
    memset(_vsg, 0, sizeof(_vsg));
    memset(_sg, 0, sizeof(_sg));
    printf("a %d b %d\n", a, b);
    FOR(i, b + 1, b + 20) { printf("sg(%d) %d\n", i, sg(i)); }
    FOR(i, b + 1, b + a + b + a + b) {
      printf("i %d : %d %d\n", i, sg(i), sg2(i));
      assert(sg(i) == sg2(i));
    }
  }
  return 0;
}
