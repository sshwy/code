// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

const int N = 1005, T = 1005, P = 1e9 + 7;
int pw(int a, int m) {
  int res = 1;
  while (m) m & 1 ? res = 1ll * res * a : 0, a = 1ll * a * a, m >>= 1;
  return res;
}
struct Atom {
  double p, e;
  Atom() { p = e = 0; }
  Atom(double _p, double _e) { p = _p, e = _e; }
  Atom operator+(const Atom a) { return Atom((p + a.p), (e + a.e)); }
  void operator+=(const Atom a) { *this = *this + a; }
  Atom operator*(const Atom a) {
    return Atom(p * 1ll * a.p, (e * 1ll * a.p + p * 1ll * a.e));
  }
  void print() { printf("(%lf, %lf)\n", p, e); }
};
double frac(double a, double b) { return a / b; }
int n, m, t;

Atom x[N][T], f[N][T];

Atom y(int j, int k, int i) {
  double s = 0;
  FOR(p, j + 1, min(k, m)) {
    double s2 = frac(1, m);
    FOR(q, p, k - 1) s = 1ll * s * x[i - 1][q].p;
    s = (s + s2);
  }
  Atom res = Atom(s, s * 1ll * k);
  printf("y(%d, %d, %d) ", j, k, i);
  res.print();
  return Atom(s, s * 1ll * k);
}

int main() {
  scanf("%d%d%d", &n, &m, &t);

  FOR(i, 0, n) FOR(j, 1, t) {
    if (i == 0)
      x[i][j] = Atom(0, 0);
    else {
      x[i][j] = x[i][j - 1] * x[i - 1][j - 1];
      if (j <= m) x[i][j] += Atom(frac(1, m), frac(j, m));
    }
    printf("x[%d, %d]", i, j);
    x[i][j].print();
  }

  FOR(i, 0, n) FOR(j, 1, t) {
    if (i == 0)
      f[i][j] = Atom(1, 0);
    else {
      FOR(k, 1, t) {
        if (k < j || k == j && k == t) { f[i][j] += x[i][k] * f[i - 1][k]; }
        if (k > j) { f[i][j] += y(j, k, i) * f[i - 1][k]; }
      }
    }
    printf("f[%d, %d]: ", i, j);
    f[i][j].print();
  }

  printf("%lf\n", f[n][t].e);

  return 0;
}
