// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

const int N = 1005;

int n, a, b;

int sg(int x) {
  if (x < a) return 0;
  x -= b;
  x %= (a + b);
  if (a == 1) return (x - 1 + a + b) % (a + b);
  if (x / a == 0) return 1;
  if (x / a == 1) return 0;
  return x / a;
}

void go() {
  scanf("%d%d%d", &n, &a, &b);
  int totsg = 0, flag = 0;
  FOR(i, 1, n) {
    int x;
    scanf("%d", &x);
    if (a <= x && x <= b) flag = 1;
    if (flag == 0) totsg ^= sg(x);
  }
  if (flag)
    puts("Alice");
  else if (totsg)
    puts("Alice");
  else
    puts("Bob");
}

int main() {
  int t;
  scanf("%d", &t);
  FOR(i, 1, t) go();
  return 0;
}
