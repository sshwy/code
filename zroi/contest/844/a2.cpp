// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

map<vector<int>, bool> mp;

int n, a, b;

bool calc(vector<int> v) {
  sort(v.begin(), v.end());
  while (v.size() && v[0] < a) v.erase(v.begin());
  if (mp.find(v) != mp.end()) return mp[v];
  for (auto &e : v) {
    FOR(i, a, b) {
      if (i > e)
        break;
      else if (i == e) {
        return mp[v] = true;
      } else {
        e -= i;
        bool nex = calc(v);
        e += i;
        if (!nex) { return mp[v] = true; }
      }
    }
  }
  return mp[v] = false;
}

void go() {
  mp.clear();
  vector<int> v;
  scanf("%d%d%d", &n, &a, &b);
  FOR(i, 1, n) {
    int x;
    scanf("%d", &x);
    if (x % (a + b))
      x %= a + b;
    else
      x = a + b;
    v.pb(x);
  }
  if (calc(v))
    puts("Alice");
  else
    puts("Bob");
}
int main() {
  int t;
  scanf("%d", &t);
  FOR(i, 1, t) go();
  return 0;
}
