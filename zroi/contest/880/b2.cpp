// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

using namespace RA;

const int N = 13;
int n;
int a[N], b[N], p[N], ans[N];

int main() {
  scanf("%d", &n);
  FOR(i, 1, n) scanf("%d", b + i);
  FOR(i, 1, n) scanf("%d", a + i);
  FOR(i, 1, n) p[i] = i;
  int mxcnt = -1;
  do {
    int cnt = 0;
    FOR(i, 1, n) { cnt += a[p[i]] > b[i]; }
    if (cnt > mxcnt) {
      mxcnt = cnt;
      FOR(i, 1, n) ans[i] = a[p[i]];
    } else if (cnt == mxcnt) {
      FOR(i, 1, n) if (a[p[i]] != ans[i]) {
        if (a[p[i]] > ans[i]) { FOR(j, 1, n) ans[j] = a[p[j]]; }
        break;
      }
    }
  } while (next_permutation(p + 1, p + n + 1));
  FOR(i, 1, n) printf("%d%c", ans[i], " \n"[i == n]);
  return 0;
}
