// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

const int N = 1e5 + 5;

vector<int> g[N];
int n, q, c[N], dep[N], sz[N], big[N], top[N], fa[N], dfn[N], totdfn;
long long ans;

void dfs(int u, int p) {
  dep[u] = dep[p] + 1, fa[u] = p;
  for (int v : g[u])
    if (v != p) {
      dfs(v, u);
      sz[u] += sz[v];
      if (!big[u] || sz[big[u]] < sz[v]) big[u] = v;
    }
}
void dfs2(int u, int p, int tp) {
  top[u] = tp, dfn[u] = ++totdfn;
  if (big[u]) dfs2(big[u], u, tp);
  for (int v : g[u])
    if (v != p && v != big[u]) dfs2(v, u, v);
}
int lca(int u, int v) {
  while (top[u] != top[v]) {
    if (dep[top[u]] < dep[top[v]]) swap(u, v);
    u = fa[top[u]];
  }
  return dep[u] < dep[v] ? u : v;
}

bool byDfn(int x, int y) { return dfn[x] < dfn[y]; }

int stk[N];
int work(vector<int> &v) {
  if (v.empty()) return 0;
  int res = 0;
  sort(v.begin(), v.end(), byDfn);
  int root = v[0];
  for (int u : v) root = lca(root, u);
  int tp = 0;
  stk[++tp] = root;
  for (auto u : v) {
    int z = lca(stk[tp], u);
    while (tp > 1 && dep[stk[tp - 1]] >= dep[z]) {
      res += dep[stk[tp]] - dep[stk[tp - 1]];
      --tp;
    }
    if (u == stk[tp]) continue;
    res += dep[stk[tp]] - dep[z];
    stk[tp] = z;
    if (z != u) stk[++tp] = u;
  }
  while (tp > 1) {
    res += dep[stk[tp]] - dep[stk[tp - 1]];
    --tp;
  }
  res++;
  // printf("res %d\n", res);
  return res;
}
vector<int> v[N];

void calc() {
  FOR(i, 1, n) v[i].clear();
  FOR(i, 1, n) v[c[i]].pb(i);
  long long ans = 0;
  FOR(i, 1, n) {
    // printf("i %d\n", i);
    ans += work(v[i]);
  }
  printf("%lld\n", ans);
}

int main() {
  scanf("%d%d", &n, &q);
  FOR(i, 1, n - 1) {
    int u, v;
    scanf("%d%d", &u, &v);
    g[u].pb(v);
    g[v].pb(u);
  }
  dfs(1, 0);
  dfs2(1, 0, 1);
  FOR(i, 1, n) scanf("%d", c + i);
  FOR(i, 1, q) {
    int x, y;
    scanf("%d%d", &x, &y);
    c[x] = y;
    calc();
  }
  return 0;
}
