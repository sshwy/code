// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

const int N = 20, ALP = 26, L = 1e4 + 5;

void add(int &x, int y) { x = (x + y) % 2; }

int n;
int w[N][ALP][ALP], w2[N][ALP];

char s[L];

void work(int i, int W[ALP][ALP], int W2[ALP]) {
  int ls = strlen(s);
  FOR(j, 0, ls - 1) {
    int c = s[j] - 'a';
    FOR(x, 0, ALP - 1) FOR(y, 0, ALP - 1) if (y != c) { add(W[x][c], W[x][y]); }
    add(W[c][c], 1);
  }
  FOR(y, 0, ALP - 1) {
    int tot = 0;
    FOR(x, 0, ALP - 1) { add(tot, W[x][y]); }
    W2[y] = tot;
    FOR(x, 0, ALP - 1) { W[x][y] = (tot - W[x][y] + 2) % 2; }
  }
}

int p[N];

void trans(int *f, int W[ALP][ALP], int W2[ALP]) {
  int g[ALP] = {0};
  FOR(x, 0, ALP - 1) FOR(y, 0, ALP - 1) { add(g[y], f[x] * W[x][y]); }
  FOR(y, 0, ALP - 1) { add(g[y], W2[y]); }
  FOR(y, 0, ALP - 1) f[y] = g[y];
}

bool check() {
  int f[ALP] = {0};
  FOR(i, 0, n - 1) {
    int j = p[i];
    trans(f, w[j], w2[j]);
  }
  int res = 0;
  FOR(y, 0, ALP - 1) add(res, f[y]);
  return res % 2;
}

int main() {
  scanf("%d", &n);
  FOR(i, 0, n - 1) {
    scanf("%s", s);
    work(i, w[i], w2[i]);
  }

  FOR(i, 0, n - 1) p[i] = i;

  int ans = 0;
  do { ans += check(); } while (next_permutation(p, p + n));

  printf("%d\n", ans);
  return 0;
}
