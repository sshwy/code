// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

typedef pair<int, int> pt;
const int N = 8005;

int n;
pt a[N], b[N];
int pos[N]; // pos[i]: position of a[i], or not placed iff =0.
bool vis[N];

// int ttt;

void work(int i) { // [i .. n]
  // printf("i %d\n", i);
  int mxval = -1, mxp = -1;
  FOR(j, i, n) if (b[j].second == i) {
    pt t = b[j];
    ROF(k, j - 1, i) b[k + 1] = b[k];
    b[i] = t;
    break;
  }
  FOR(j, i, n) pos[j] = 0, vis[j] = false;

  // FOR(j, i, n) printf("(%d, %d)%c", a[j].first, a[j].second, " \n"[j == n]);
  // FOR(j, i, n) printf("(%d, %d)%c", b[j].first, b[j].second, " \n"[j == n]);

  int tot_placed = 0, nplaced = n;
  { // init
    int cur = i + 1;
    FOR(j, i, n - 1) if (b[cur].first < a[j].first) {
      pos[j] = cur;
      vis[cur] = true;
      ++cur;
      ++tot_placed;
      // printf("pos %d = %d\n", j, pos[j]);
    }
  }
  // printf("tot_placed %d\n", tot_placed);

  ROF(j, n, i) {
    int val = tot_placed + (a[j].first > b[i].first);
    // printf("j %d val %d\n", j, val);
    if (val > mxval) mxval = val, mxp = j;
    /**
     * currently a[j] is on i.
     * 1. put a[j - 1] at i
     * 2. try to place a[j]
     */
    if (j > i) {
      if (pos[j - 1]) {
        // step 1
        vis[pos[j - 1]] = false;
        pos[j - 1] = 0;
        --tot_placed;
      }
      // step 2
      while (pos[nplaced] > 0) --nplaced;
      assert(nplaced >= j);
      int p = nplaced < n ? pos[nplaced + 1] - 1 : n;
      while (b[p].first >= a[nplaced].first) --p;
      if (vis[p] == false && p > i) {
        pos[nplaced] = p;
        vis[p] = true;
        ++tot_placed;
      }
    }
  }
  // printf("mxval %d\n", mxval);
  // if(i == 1) assert(mxval == ttt);
  {
    pt t = a[mxp];
    ROF(j, mxp - 1, i) a[j + 1] = a[j];
    a[i] = t;
  }
}

void go() {
  sort(a + 1, a + n + 1);
  sort(b + 1, b + n + 1);
  // { // test
  //   ttt = 0;
  //   int cur = 1;
  //   FOR(i, 1, n) {
  //     if(a[i].first > b[cur].first) ++ttt, ++cur;
  //   }
  //   // printf("ttt %d\n", ttt);
  // }
  FOR(i, 1, n) { work(i); }
}

int main() {
  scanf("%d", &n);
  FOR(i, 1, n) {
    scanf("%d", &b[i].first);
    b[i].second = i;
  }
  FOR(i, 1, n) {
    scanf("%d", &a[i].first);
    a[i].second = i;
  }

  // { // test
  //   int xxx = 0;
  //   FOR(i, 1, n) xxx += a[i].first > b[i].first;
  //   printf("xxx %d\n", xxx);
  // }

  go();
  FOR(i, 1, n) printf("%d%c", a[i].first, " \n"[i == n]);

  return 0;
}
