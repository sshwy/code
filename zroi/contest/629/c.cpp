// by Sshwy
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

long long n, k, p;
long long pw(long long a, long long m) {
  long long res = 1;
  while (m) m & 1 ? res = 1ll * res * a % p : 0, a = a * a % p, m >>= 1;
  return res;
}

int main() {
  scanf("%lld%lld%lld", &n, &k, &p);
  long long ans = 0;
  FOR(i, 1, n) { ans = (ans + pw(i, (p - 2) * k % (p - 1))) % p; }
  printf("%lld\n", ans);
  return 0;
}
