// by Sshwy
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA
const int N = 1e6 + 5, P = 1e9 + 7;
int n;
int a[N];
pair<int, int> b[N];

struct disjoint {
  int f[N];
  disjoint() { FOR(i, 0, N - 1) f[i] = i; }
  int get(int u) { return f[u] == u ? u : f[u] = get(f[u]); }
  void merge_to(int u, int v) { f[get(u)] = get(v); }
} dl, dr;

long long val(int a, int b, int c) { return a * 1ll * b % P * c % P; }
int main() {
  scanf("%d", &n);
  FOR(i, 1, n) scanf("%d", &a[i]);
  FOR(i, 1, n) b[i] = {a[i], i};
  sort(b + 1, b + n + 1);
  long long ans = 0;
  FOR(i, 1, n) {
    int u = b[i].second;
    dl.merge_to(u, u - 1);
    dr.merge_to(u, u + 1);
    long long l1 = dl.get(u);
    long long l2 = l1 ? dl.get(l1 - 1) : 0;
    long long l3 = l2 ? dl.get(l2 - 1) : 0;

    long long r1 = dr.get(u);
    long long r2 = r1 != n + 1 ? dr.get(r1 + 1) : n + 1;
    long long r3 = r2 != n + 1 ? dr.get(r2 + 1) : n + 1;

    // printf("%lld %lld %lld %d %lld %lld %lld\n",l3,l2,l1,u,r1,r2,r3);

    ans = (ans + (l2 - l3) * (r1 - u) % P * val(a[l2], a[l1], a[u]) % P) % P;
    ans = (ans + (l1 - l2) * (r2 - r1) % P * val(a[l1], a[u], a[r1]) % P) % P;
    ans = (ans + (u - l1) * (r3 - r2) % P * val(a[u], a[r1], a[r2]) % P) % P;
  }
  printf("%lld\n", ans);
  return 0;
}
