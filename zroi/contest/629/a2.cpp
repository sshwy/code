// by Sshwy
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA
const int N = 1e6 + 5, P = 1e9 + 7;
int n;
int a[N];

int main() {
  cin >> n;
  FOR(i, 1, n) cin >> a[i];
  int ans = 0;
  FOR(i, 1, n) {
    vector<int> v;
    FOR(j, i, n) {
      v.pb(a[j]);
      if (v.size() >= 3) {
        sort(v.begin(), v.end(), [](int x, int y) { return x > y; });
        ans = (ans + 1ll * v[0] * v[1] % P * v[2] % P) % P;
      }
    }
  }
  printf("%d\n", ans);
  return 0;
}
