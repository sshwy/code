// by Sshwy
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

using namespace RA;

set<int> s;

int main() {
  srand(clock() + time(0));
  int n = 1e5;
  int LIM = 1e9;
  printf("%d\n", n);
  FOR(i, 1, n) {
    int x = rnd(1, LIM);
    while (s.find(x) != s.end()) x = rnd(1, LIM);
    s.insert(x);
    printf("%d%c", x, " \n"[i == n]);
  }
  return 0;
}
