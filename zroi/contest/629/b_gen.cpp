// by Sshwy
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  long long rnd(long long p) { return (__int128)rand() * rand() % p; }
  long long rnd(long long L, long long R) { return rnd(R - L + 1) + L; }
} // namespace RA

using namespace RA;

int main() {
  int n = 1e6;
  int Mod = rnd(1, 1e9);
  int x = rnd(Mod), A = rnd(Mod), B = rnd(Mod), C = rnd(Mod);
  printf("%d\n%d %d %d %d %d\n", n, x, A, B, C, Mod);

  long long tot = 0;
  FOR(i, 1, n) {
    tot = (tot + x);
    x = (A * 1ll * x % Mod * x % Mod + B * 1ll * x % Mod + C) % Mod + 1;
  }

  int q = 1e5;
  printf("%d\n", q);
  FOR(i, 1, q) { printf("%lld\n", rnd(1, tot)); }
  return 0;
}
