// by Sshwy
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace IO { // require C++11
  const int _BS = 200000;
  char _ob[_BS], *_z = _ob, _ib[_BS], *_x = _ib, *_y = _ib;
  // Input
  char _nc() {
    return _x == _y && (_y = (_x = _ib) + fread(_ib, 1, _BS, stdin), _x == _y)
               ? EOF
               : *_x++;
  }
  inline void rd(char &c) { c = _nc(); }
  void rd(char *s) {
    char c = _nc();
    while (!isblank(c) && isprint(c) && c != EOF) *s++ = c, c = _nc();
    *s = 0;
  }
  template <class T> void rd(T &res) { // unsigned/signed int/long long
    res = 0;
    char c = _nc(), flag = 0;
    while (!isdigit(c) && c != '-') c = _nc();
    if (c == '-') flag = 1, c = _nc();
    while (isdigit(c)) res = res * 10 + c - '0', c = _nc();
    if (flag) res = -res;
  }
  template <class T, class... A> void rd(T &hd, A &...rs) { rd(hd), rd(rs...); }
  // Output
  inline void flush() { fwrite(_ob, 1, _z - _ob, stdout), _z = _ob; }
  inline void wrch(char x) {
    if (_z == _ob + _BS - 1) flush();
    *_z = x, ++_z;
  }
  void wr(char *s) {
    while (*s) wrch(*s), ++s;
  }
  void wr(const char *s) {
    while (*s) wrch(*s), ++s;
  }
  template <class T> void wr(T x) { // unsigned/signed int/long long
    if (x == 0) return wrch('0'), void();
    if (x < 0) wrch('-'), x = -x;
    int _t[30], _lt = 0;
    while (x) _t[++_lt] = x % 10, x /= 10;
    while (_lt > 0) wrch(_t[_lt] + '0'), --_lt;
  }
  inline void wr(char x) { wrch(x); }
  template <class T, class... A> void wr(T hd, A... rs) { wr(hd), wr(rs...); }
  template <class... A> void wrln(A... ar) { wr(ar...), wrch('\n'); }
  struct _Flusher {
    ~_Flusher() { flush(); }
  } _flusher;
} // namespace IO
using IO::rd;
using IO::wrln;

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA
const int N = 1e6 + 5, SZ = 1e7 + 5, T = SZ, R = 4e4;
typedef __int128 LLL;
int n, x, A, B, C, Mod;

pair<int, long long> _v[4][SZ];
pair<int, long long> *v = _v[0], *tv = _v[1], *nv = _v[2], *tnv = _v[3];
int lv, lnv;

LLL big[SZ], m;
int qry(LLL k) {
  k = m - k + 1;
  return nv[lower_bound(big, big + lnv, k) - big].first;
}
pair<int, long long> *zip(pair<int, long long> *pbeg, pair<int, long long> *pend) {
  if (pbeg == pend) return pend;
  auto t = pbeg;
  for (auto p = t; p != pend; ++p) {
    if (p->first == 0)
      ;
    else if (t != pbeg && (t - 1)->first == p->first)
      (t - 1)->second += p->second;
    else
      *t = *p, ++t;
  }
  return t;
}
int main() {
  rd(n);
  rd(x, A, B, C, Mod);
  FOR(i, 1, n) {
    v[lv] = {x, 1}, ++lv;
    // work(0,0,x,1);
    x = (A * 1ll * x % Mod * x % Mod + B * 1ll * x % Mod + C) % Mod + 1;
  }

  sort(v, v + lv);
  lv = zip(v, v + lv) - v;

  lnv = merge(v, v + lv, nv, nv + lnv, tnv) - tnv;
  swap(nv, tnv);

  while (lv) {
    int ltv = 0;
    for (auto p = v; p != v + lv; ++p) {
      if (p->first & 1)
        tv[ltv] = {p->first / 2, p->second * 2}, ++ltv;
      else {
        tv[ltv] = {p->first / 2 - 1, p->second}, ++ltv;
        tv[ltv] = {p->first / 2, p->second}, ++ltv;
      }
    }
    swap(v, tv);
    lv = ltv;
    lv = zip(v, v + lv) - v;

    lnv = merge(v, v + lv, nv, nv + lnv, tnv) - tnv;
    swap(nv, tnv);
  }

  for (int i = 0; i < lv; ++i) {
    auto pr = v[i];
    if (!lnv || nv[lnv - 1].first != pr.first)
      nv[lnv] = pr, ++lnv;
    else
      nv[lnv - 1].second += pr.second;
  }
  for (int i = 0; i < lnv; ++i) big[i] = nv[i].second;
  FOR(i, 1, lnv - 1) big[i] += big[i - 1];
  m = (lnv ? big[lnv - 1] : 0);
  int q;
  rd(q);
  FOR(i, 1, q) {
    LLL x;
    rd(x);
    wrln(qry(x));
  }
  return 0;
}
