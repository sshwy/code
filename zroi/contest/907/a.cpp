// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

const int N = 505, S = 105;

int n, m, s, P;
int f[N][N][S], pws[N];

void add(int &x, long long y) { x = (x + y) % P; }

int main() {
  scanf("%d%d%d%d", &n, &m, &s, &P);

  pws[0] = 1;
  FOR(i, 1, N - 1) pws[i] = 1ll * pws[i - 1] * (s + 1) % P;

  f[1][1][0] = 1;
  FOR(i, 1, n) FOR(j, 1, m) FOR(k, 0, s) {
    // printf("f[%d, %d, %d] = %d\n", i, j, k, f[i][j][k]);
    if (j < m) {
      if (i < n) {
        FOR(t, 0, s - k)
        add(f[i][j + 1][k + t], 1ll * f[i][j][k] * (t + 1) % P * pws[n - i - 1] % P);
      } else {
        FOR(t, 0, s - k)
        add(f[i][j + 1][k + t], f[i][j][k]);
      }
    }
    if (i < n) {
      if (j < m) {
        FOR(t, 0, s - k)
        add(f[i + 1][j][k + t], 1ll * f[i][j][k] * t % P * pws[m - j - 1] % P);
      } else {
        FOR(t, 0, s - k)
        add(f[i + 1][j][k + t], f[i][j][k]);
      }
    }
  }
  printf("%d\n", f[n][m][s]);
  return 0;
}
