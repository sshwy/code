// by Sshwy
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA
const int N = 2e5 + 5, SZ = N << 2;
int n;
int b[N];

long long mn[SZ], tk[SZ], tb[SZ], cv[SZ];
bool bcv[SZ];

void pushup(int u) { mn[u] = min(mn[u << 1], mn[u << 1 | 1]); }
void cover(int u, long long tag) {
  mn[u] = tag;
  bcv[u] = 1;
  cv[u] = tag;
  tk[u] = tb[u] = 0;
}
void linePlus(int u, int l, int r, long long tagk, long long tagb) {
  mn[u] += tagk * r + tagb;
  tk[u] += tagk;
  tb[u] += tagb;
}
void pushdown(int u, int l, int r) {
  int mid = (l + r) >> 1;
  if (bcv[u]) {
    cover(u << 1, cv[u]);
    cover(u << 1 | 1, cv[u]);
    cv[u] = bcv[u] = 0;
  }
  if (tk[u] || tb[u]) {
    linePlus(u << 1, l, mid, tk[u], tb[u]);
    linePlus(u << 1 | 1, mid + 1, r, tk[u], tb[u]);
    tk[u] = tb[u] = 0;
  }
}
void add(
    int L, int R, long long k, long long b, int u = 1, int l = 0, int r = n / 2) {
  if (R < l || r < L || L > R) return;
  if (L <= l && r <= R) return linePlus(u, l, r, k, b), void();
  int mid = (l + r) >> 1;
  pushdown(u, l, r);
  add(L, R, k, b, u << 1, l, mid);
  add(L, R, k, b, u << 1 | 1, mid + 1, r);
  pushup(u);
}
void cover(int L, int R, long long val, int u = 1, int l = 0, int r = n / 2) {
  if (R < l || r < L || L > R) return;
  if (L <= l && r <= R) return cover(u, val), void();
  int mid = (l + r) >> 1;
  pushdown(u, l, r);
  cover(L, R, val, u << 1, l, mid);
  cover(L, R, val, u << 1 | 1, mid + 1, r);
  pushup(u);
}
long long at(int pos, int u = 1, int l = 0, int r = n / 2) {
  if (l == r) return mn[u];
  int mid = (l + r) >> 1;
  pushdown(u, l, r);
  if (pos <= mid)
    return at(pos, u << 1, l, mid);
  else
    return at(pos, u << 1 | 1, mid + 1, r);
}
int get_pos(int u = 1, int l = 0, int r = n / 2) {
  if (l == r) return l;
  int mid = (l + r) >> 1;
  pushdown(u, l, r);
  // assert(mn[u<<1]>=mn[u<<1|1]);
  if (mn[u << 1] == mn[u << 1 | 1])
    return get_pos(u << 1, l, mid);
  else
    return get_pos(u << 1 | 1, mid + 1, r);
}
int c[N];
void add(int pos, int v) {
  for (int i = pos; i < N; i += i & -i) c[i] += v;
}
int pre(int pos) {
  int res = 0;
  for (int i = pos; i > 0; i -= i & -i) res += c[i];
  return res;
}
int main() {
  scanf("%d", &n);
  FOR(i, 1, n / 2) scanf("%d", &b[i]);
  FOR(i, 1, n / 2) {
    int p = b[i] / 2;
    int pos = get_pos();
    if (pos <= p) {
      add(0, p, -1, p);
      long long g_p = at(p);
      cover(p + 1, n / 2, g_p);
    } else {
      add(0, p, -1, p);
      add(p + 1, pos, 1, -p);
      long long g_pos = at(pos);
      cover(pos + 1, n / 2, g_pos);
    }
  }
  long long ans = at(n / 2);
  ROF(i, n / 2, 1) {
    ans += pre(b[i]);
    add(b[i], 1);
  }
  printf("%lld\n", ans);
  return 0;
}
// from 0 to p: slope is -1
// from p to n/2: slope is 1
//
// from 0 to max(p,pos): min val is rightmost (decrease)
// from max(p,pos) to n/2: min val is leftmost (increase)
//
// p O(1)
// pos O(log) (use g)
//
// if pos<p:
//   max(p,pos)=p
//   just plus a line from 0 to p
//   do nothing with p to n/2 except cover
//   now U get the next g.
// else :
//   plus a line from 0 to p
//   plus a line from p to pos
//   do nothing but cover the rest part
//   the next g.
// min val is ALWAYS rightmost one
