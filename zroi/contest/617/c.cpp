// by Sshwy
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA
const int P = 10007;
int n, K;
int lim, ans;
bool ban[50][50];

void dfs(int k, int s, bool dl, bool dr) {
  if (k == n && s == lim && dl && dr) ++ans, ans -= ans >= P ? P : 0;
  if (k == n) return;
  FOR(i, 0, n - 1) if (!(s >> i & 1) && !ban[k][i]) {
    int ns = s ^ (1 << i);
    int ndl = dl || (k == i);
    int ndr = dr || (k + i == n - 1);
    dfs(k + 1, ns, ndl, ndr);
  }
}
int main() {
  scanf("%d%d", &n, &K);
  lim = (1 << n) - 1;
  FOR(i, 1, K) {
    int x, y;
    scanf("%d%d", &x, &y);
    --x, --y;
    ban[x][y] = 1;
  }
  dfs(0, 0, 0, 0);
  printf("%d\n", ans);
  return 0;
}
