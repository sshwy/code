// by Sshwy
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

int n, mxr;
bool vis[1000000];
long long calc() {
  long long c[2] = {0};
  FOR(i, 1, mxr) if (vis[i]) { c[__builtin_popcount(i) & 1]++; }
  return c[0] * c[1];
}

int main() {
  cin >> n;
  FOR(i, 1, n) {
    int l, r;
    cin >> l >> r;
    mxr = max(mxr, r);
    FOR(i, l, r) vis[i] = 1;
    cout << calc() << endl;
  }
  return 0;
}
