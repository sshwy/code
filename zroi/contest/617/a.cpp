// by Sshwy
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

long long n;
const long long INF = 1e18;

set<pair<long long, long long>> s;
long long f[40][2][2];
pair<long long, long long> DP(long long m) {
  memset(f, 0, sizeof(f));
  f[32][0][1] = 1;
  ROF(i, 32, 1) {
    FOR(x, 0, 1) FOR(y, 0, 1) if (f[i][x][y]) {
      FOR(z, 0, 1) { // what to write down
        int lim = m >> (i - 1) & 1;
        if (y == 1 && z > lim) continue;
        int nx = x ^ z;
        int ny = y && (z == lim);
        f[i - 1][nx][ny] += f[i][x][y];
      }
    }
  }
  // printf("calc(%lld): %lld
  // %lld\n",m,f[0][0][0]+f[0][0][1],f[0][1][0]+f[0][1][1]);
  return {f[0][0][0] + f[0][0][1], f[0][1][0] + f[0][1][1]};
}
long long tot[2];
void add(long long L, long long R) {
  if (L > R) return;
  // printf("add %lld %lld\n",L,R);
  auto pr = DP(R), pl = DP(L - 1);
  tot[0] += pr.first - pl.first;
  tot[1] += pr.second - pl.second;
}
void insert(long long L, long long R) {
  // printf("insert %lld %lld\n",L,R);
  while (1) {
    if (L > R) break;
    auto p = s.lower_bound({L, INF});
    auto q = p;
    long long nR = R, nL = L;
    if (p != s.end()) {
      // printf("p %lld %lld\n",p->first,p->second);
      nR = min(nR, p->first - 1);
      nL = min(nL, p->first);
    }
    // printf("> %lld %lld\n",nL,nR);
    if (p != s.begin()) {
      --q;
      // printf("q %lld %lld\n",q->first,q->second);
      nL = max(nL, q->second + 1);
      nR = max(nR, q->second);
    }
    // printf("> %lld %lld\n",nL,nR);
    if (nL <= nR) add(nL, nR);
    if (q != p && q->second + 1 == nL) nL = q->first, s.erase(q);
    if (p != s.end() && nR + 1 == p->first) nR = p->second, s.erase(p);
    s.insert({nL, nR});

    L = nR + 1;
    // for(auto x:s)printf("%lld %lld\n",x.first,x.second);
  }
}
int main() {
  scanf("%lld", &n);
  FOR(i, 1, n) {
    long long l, r;
    scanf("%lld%lld", &l, &r);
    insert(l, r);
    long long ans = tot[0] * tot[1];
    printf("%lld\n", ans);
  }
  return 0;
}
