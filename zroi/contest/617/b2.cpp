// by Sshwy
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA
const int N = 5005;
int n;
int b[N];
long long f[N][N], g[N];
// f[i,j] b[1..i] b[i] after 2*j-1

int main() {
  scanf("%d", &n);
  FOR(i, 1, n / 2) scanf("%d", &b[i]);
  f[0][0] = 0;
  FOR(i, 1, n / 2) {
    int p = b[i] / 2;
    FOR(j, 0, n / 2) {
      g[j] = f[i - 1][j];
      if (j > 0) g[j] = min(g[j], g[j - 1]);
    }
    FOR(j, 0, n / 2) { f[i][j] = g[j] + abs(j - p); }
    // int pos=min_element(g,g+n/2+1)-g;
    // puts("");
    // FOR(j,0,n/2){
    //    if(j==p)printf("[%-3lld]%c",g[j]," \n"[j==n/2]);
    //    else if(j==pos)printf("<%-3lld>%c",g[j]," \n"[j==n/2]);
    //    else printf("%-3lld%c",g[j]," \n"[j==n/2]);
    //}
    // FOR(j,0,n/2){
    //    if(j==p)printf("[%-3lld]%c",f[i][j]," \n"[j==n/2]);
    //    else if(j==pos)printf("<%-3lld>%c",f[i][j]," \n"[j==n/2]);
    //    else printf("%-3lld%c",f[i][j]," \n"[j==n/2]);
    //}
    // FOR(j,1,max(p,pos))assert(f[i][j]<=f[i][j-1]);
    // FOR(j,max(p,pos)+1,n/2)assert(f[i][j]>=f[i][j-1]);
  }
  long long ans = *min_element(f[n / 2], f[n / 2] + n / 2 + 1);
  FOR(i, 1, n / 2) FOR(j, i + 1, n / 2) if (b[i] > b[j])++ ans;
  printf("%lld\n", ans);
  return 0;
}
