// by Sshwy
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

using namespace RA;
int p[1000000];
int main() {
  srand(clock() + time(0));
  int n = rnd(1, 5000);
  FOR(i, 1, n) p[i] = i;
  random_shuffle(p + 1, p + n + 1);
  printf("%d\n", n * 2);
  FOR(i, 1, n) printf("%d%c", p[i] * 2, " \n"[i == n]);
  return 0;
}
