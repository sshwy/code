// by Sshwy
#include <bits/stdc++.h>
#define int long long
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA
const int N = 16;
int n;
struct monster {
  int h, a, d, s, ap, dp, mp, hp;
  void read() {
    scanf("%lld%lld%lld%lld%lld%lld%lld%lld", &h, &a, &d, &s, &ap, &dp, &mp, &hp);
  }
} A[N];
int pre[N];
long long ans, tot_hp, fhp[1 << N];
bool battle(long long &h, int &a, int &d, int &m, monster M) {
  if (M.s & 8) M.a = a, M.d = d;
  int atk = max(a - M.d, 0ll);
  if (atk == 0) return 0;
  int mtk = (M.s & 2) ? M.a : max(M.a - d, 0ll);
  if (M.s & 4) mtk *= 2;
  long long cntM = ceil(M.h * 1.0 / atk);
  if (M.s & 1) ++cntM;
  h = min(h, h + m - (cntM - 1) * 1ll * mtk);
  if (h > 0) h += M.hp, a += M.ap, d += M.dp, m += M.mp;
  return 1;
}
void dfs(long long h, int a, int d, int m, int stat) {
  if (h <= 0) return;
  if (h + tot_hp - fhp[stat] <= ans) return;
  if (stat == (1 << n) - 1) { return ans = max(ans, h), void(); }
  FOR(i, 0, n - 1) if (!(stat >> i & 1)) {
    if ((pre[i] & stat) == pre[i]) {
      long long nh = h, na = a, nd = d, nm = m;
      if (battle(nh, na, nd, nm, A[i])) {
        if (nh > 0) dfs(nh, na, nd, nm, stat | (1 << i));
      }
    }
  }
}
void go() {
  tot_hp = 0;
  int h, a, d, m;
  scanf("%lld%lld%lld%lld", &h, &a, &d, &m);
  scanf("%lld", &n);
  FOR(i, 0, n - 1) {
    A[i].read();
    tot_hp += A[i].hp;
  }
  fhp[0] = 0;
  FOR(i, 0, n - 1) fhp[1 << i] = A[i].hp;
  FOR(i, 1, (1 << n) - 1) fhp[i] = fhp[i - (i & -i)] + fhp[i & -i];
  int k;
  scanf("%lld", &k);
  FOR(i, 0, n) pre[i] = 0;
  FOR(i, 1, k) {
    int u, v;
    scanf("%lld%lld", &u, &v);
    --u, --v;
    pre[v] |= 1 << u;
  }
  ans = -1;
  dfs(h, a, d, m, 0);
  printf("%lld\n", ans);
}
signed main() {
  int t;
  scanf("%lld", &t);
  FOR(i, 1, t) go();
  return 0;
}
