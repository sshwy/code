// by Sshwy
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

const int INF = 0x3f3f3f3f, _N = 2e3 + 50, _M = 2e4 + 5;

struct qxx {
  int nex, t, v, c;
} e[_M];
int h[_N], le = 1;
void add_path(int f, int t, int v, int c) {
  e[++le] = (qxx){h[f], t, v, c}, h[f] = le;
}
void add_flow(int f, int t, int v, int c) {
  add_path(f, t, v, c), add_path(t, f, 0, -c);
}

bool vis[_N];
queue<int> q;
long long d[_N];
int pre[_N], incf[_N];
int s, t;

bool spfa() {
  memset(d, 0x3f, sizeof(d));
  q.push(s), d[s] = 0, incf[s] = INF, incf[t] = 0;
  while (!q.empty()) {
    int u = q.front();
    q.pop();
    vis[u] = 0;
    for (int i = h[u]; i; i = e[i].nex) {
      const int v = e[i].t, w = e[i].v, c = e[i].c;
      if (!w || d[v] <= d[u] + c) continue;
      d[v] = d[u] + c, incf[v] = min(incf[u], w), pre[v] = i;
      if (!vis[v]) q.push(v), vis[v] = 1;
    }
  }
  if (d[t] > 0) return 0;
  return incf[t];
}
int maxflow;
long long mincost;
void update() {
  maxflow += incf[t], mincost += incf[t] * 1ll * d[t];
  for (int u = t; u != s; u = e[pre[u] ^ 1].t) {
    e[pre[u]].v -= incf[t], e[pre[u] ^ 1].v += incf[t];
  }
}
void go() {
  while (spfa()) update();
}

int n, m, c;
int main() {
  scanf("%d%d%d", &n, &m, &c);
  s = n + 1, t = n + 2;
  add_flow(s, 0, INF, 0);
  add_flow(n, t, INF, c);
  long long ans = 0;
  FOR(i, 1, m) {
    int u, v, x, y;
    scanf("%d%d%d%d", &u, &v, &x, &y);
    --u;
    add_flow(u, v, y, -x);
    add_flow(u, v, INF, 0);
    ans += x * 1ll * y;
  }
  FOR(i, 1, n) { add_flow(i - 1, i, INF, 0); }
  go();
  ans += mincost;
  printf("%lld\n", ans);
  return 0;
}
