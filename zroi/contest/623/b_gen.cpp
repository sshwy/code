// by Sshwy
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

using namespace RA;

int main() {
  srand(clock() + time(0));
  int n = rnd(1, 5), m = rnd(1, 5), c = rnd(1, 5);
  printf("%d %d %d\n", n, m, c);
  FOR(i, 1, m) {
    int l = rnd(1, n), r = rnd(1, n), x = rnd(1, 5), y = rnd(1, 5);
    if (l > r) swap(l, r);
    printf("%d %d %d %d\n", l, r, x, y);
  }
  return 0;
}
