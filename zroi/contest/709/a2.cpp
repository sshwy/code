// by Sshwy
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA
const int N = 2e5 + 5;
int n, m, k;
vector<pair<int, int>> E, E2;

void trans() {
  assert(E.size() < N);
  E2.clear();
  n = E.size();
  FOR(i, 0, n - 1)
  FOR(j, 0, i - 1)
  if (E[i].first == E[j].first || E[i].second == E[j].first ||
      E[i].first == E[j].second || E[i].second == E[j].second) {
    E2.push_back({i + 1, j + 1});
  }
  E = E2;
  // printf("%d\n",n);
  // for(auto x:E)printf("%d %d\n",x.first,x.second);
}
int dg[N], f[N];
int get(int u) { return f[u] == u ? u : f[u] = get(f[u]); }

int main() {
  cin >> n >> m >> k;
  FOR(i, 1, m) {
    int u, v;
    cin >> u >> v;
    E.pb({u, v});
  }
  FOR(i, 1, k) trans();
  if (E.size()) {
    FOR(i, 1, n) f[i] = i;
    for (auto x : E) f[get(x.first)] = get(x.second), dg[x.first]++, dg[x.second]++;
    FOR(i, 1, n - 1) if (get(f[i]) != get(f[i + 1])) return puts("No"), 0;
    FOR(i, 1, n) if (dg[i] & 1) return puts("No"), 0;
    return puts("Yes"), 0;
  } else {
    if (n == 1) return puts("Yes"), 0;
    return puts("No"), 0;
  }
  return 0;
}
