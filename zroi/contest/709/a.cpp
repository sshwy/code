// by Sshwy
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA
const int N = 1e6 + 5;
int n, m, k;

struct qxx {
  int nex, t;
} e[N * 2];
int h[N], le = 1;
void add_path(int f, int t) { e[++le] = {h[f], t}, h[f] = le; }

bool vis[N];
int dg[N];
int cnt, dmx, dmm, cc, root, cd;
void dfs(int u) {
  vis[u] = 1;
  ++cnt;
  dmx = max(dmx, dg[u]);
  dmm = min(dmm, dg[u]);
  for (int i = h[u]; i; i = e[i].nex) {
    int v = e[i].t;
    if (!vis[v]) dfs(v);
  }
}
bool all_even = 1, all_odd = 1, b_even = 1, b_odd = 1;
void dfs2(int u) {
  vis[u] = 1;
  if (dg[u] & 1)
    all_even = 0;
  else
    all_odd = 0;

  for (int i = h[u]; i; i = e[i].nex) {
    int v = e[i].t;
    if ((dg[u] + dg[v]) & 1)
      b_even = 0;
    else
      b_odd = 0;
    if (!vis[v]) dfs2(v);
  }
}

int main() {
  scanf("%d%d%d", &n, &m, &k);
  if (m == 0 && k >= 1) return puts("No"), 0;
  FOR(i, 1, m) {
    int u, v;
    scanf("%d%d", &u, &v);
    add_path(u, v);
    add_path(v, u);
    dg[u]++, dg[v]++;
  }
  FOR(i, 1, n) if (!vis[i]) {
    cnt = dmx = 0;
    dmm = 1e9;
    dfs(i);
    if (dmx == 0) { // island
      if (k == 0) cd++;
    } else if (dmx <= 2 && dmm == 1) { // chain
      if (cnt - 1 > k) cc++, root = i;
      if (cnt - 1 >= k) cd++;
    } else
      cc++, root = i, cd++;
  }
  if (cc >= 2) { // not connected
    return puts("No"), 0;
  }
  if (cd == 0) { // no vertex
    return puts("No"), 0;
  }
  if (cd > 1) { // note connected
    return puts("No"), 0;
  }
  if (cc == 0) { // 1 vertex
    return puts("Yes"), 0;
  }
  fill(vis, vis + n + 1, false);
  dfs2(root);
  if (all_even) return puts("Yes"), 0;
  if (all_odd && k >= 1) return puts("Yes"), 0;
  if (b_even && k >= 1) return puts("Yes"), 0;
  if (b_odd && k >= 2) return puts("Yes"), 0;
  puts("No");
  return 0;
}
