// by Sshwy
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

using namespace RA;

vector<pair<int, int>> e;
int main() {
  srand(clock() + time(0));
  int n = rnd(2, 10), m = rnd(0, min(20, n * (n - 1) / 2)), k = rnd(0, 4);
  FOR(i, 1, n) FOR(j, 1, i - 1) e.pb({i, j});
  random_shuffle(e.begin(), e.end());
  printf("%d %d %d\n", n, m, k);
  FOR(i, 1, m) printf("%d %d\n", e[i - 1].first, e[i - 1].second);
  return 0;
}
