// by Sshwy
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

const int N = 1e6 + 5, P = 1e9 + 7;
int pw(int a, int m) {
  int res = 1;
  while (m) m & 1 ? res = 1ll * res * a % P : 0, a = 1ll * a * a % P, m >>= 1;
  return res;
}
int n, m;

struct qxx {
  int nex, t;
} e[N * 2];
int h[N], le = 1;
void add_path(int f, int t) { e[++le] = {h[f], t}, h[f] = le; }

int a[N];
int f[N];
int A[N], B[N];

void dfs(int u, int p) {
  int dg = 0;
  int Au = 0, Bu = 0;
  for (int i = h[u]; i; i = e[i].nex) {
    ++dg;
    int v = e[i].t;
    if (v == p) continue;
    dfs(v, u);
    // f[v] = A[v] * f[u] + B[v]
    // f[u] += 1/dg * f[v]
    Au = (Au + A[v] % P) % P;
    Bu = (Bu + B[v] % P) % P;
  }
  // printf("u %d Au %d Bu %d\n",u,Au,Bu);
  int t = pw((dg - Au + P) % P, P - 2);
  if (u == 1) {
    B[u] = (Bu * 1ll * t % P + a[u]) % P;
  } else {
    A[u] = t;
    B[u] = (Bu * 1ll * t % P + a[u] * 1ll * dg % P) % P;
  }
  // printf("u %d A %d B %d\n",u,A[u],B[u]);
  if (p == 1) A[u] = 0;
}

int main() {
  scanf("%d%d", &n, &m);
  FOR(i, 1, n) scanf("%d", &a[i]), a[i] %= P;
  FOR(i, 1, m) {
    int u, v;
    scanf("%d%d", &u, &v);
    add_path(u, v);
    add_path(v, u);
  }
  dfs(1, 0);
  printf("%d\n", B[1]);
  return 0;
}
