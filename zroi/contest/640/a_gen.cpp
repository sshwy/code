// by Sshwy
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

using namespace RA;

int main() {
  srand(clock() + time(0));
  int n = rnd(2, 100000);
  int k = rnd(1, int(log(n)));
  printf("%d %d\n", n, k);
  printf("%d ", rnd(1, n));
  FOR(i, 2, n) printf("%d%c", rnd(1, i - 1), " \n"[i == n]);
  return 0;
}
