// by Sshwy
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA
const int N = 1e5 + 5;
int n, k, fa[N], vis[N];
vector<int> g[N], V;
queue<int> q;
void dfs(int u) {
  if (vis[u]) return;
  vis[u] = 1;
  for (auto v : g[u]) dfs(v);
}
int main() {
  scanf("%d%d", &n, &k);
  int ans = 0;
  FOR(i, 1, n) {
    int x;
    scanf("%d", &x);
    if (i == 1) {
      if (x != 1) ++ans;
    } else {
      g[x].pb(i);
      fa[i] = x;
    }
  }
  q.push(1);
  while (!q.empty()) {
    int u = q.front();
    q.pop();
    V.pb(u);
    for (auto v : g[u]) q.push(v);
  }
  reverse(V.begin(), V.end());
  for (auto u : V) {
    if (vis[u]) continue;
    int v = u;
    FOR(i, 1, k - 1) {
      if (fa[v])
        v = fa[v];
      else
        break;
      assert(vis[v] == 0);
    }
    if (v == 1 || fa[v] == 1) {
      dfs(v);
    } else {
      ++ans;
      dfs(v);
    }
  }
  printf("%d\n", ans);
  return 0;
}
