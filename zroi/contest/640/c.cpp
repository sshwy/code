// by Sshwy
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

const int N = 1e5 + 5, W = 64, P = 11777;
int pw(int a, int m) {
  int res = 1;
  while (m) m & 1 ? res = 1ll * res * a % P : 0, a = 1ll * a * a % P, m >>= 1;
  return res;
}
int tr[W];
void init(int len) {
  assert(__builtin_popcount(len) == 1);
  FOR(i, 0, len - 1) tr[i] = tr[i >> 1] >> 1 | (i & 1) * (len / 2);
}
void dft(int *f, int len, int tag) {
  FOR(i, 0, len - 1) if (tr[i] < i) swap(f[tr[i]], f[i]);
  for (int j = 1; j < len; j <<= 1)
    for (int i = 0, wn = pw(3, (P - 1) / (1 << j) * tag + P - 1); i < len;
         i += j << 1)
      for (int k = i, u, v, w = 1; k < i + j; k++, w = w * 1ll * wn % P)
        u = f[k], v = f[k + j] * 1ll * w % P, f[k] = (u + v) % P,
        f[k + j] = (u - v + P) % P;
  if (tag == -1)
    for (int i = 0, in = pw(len, P - 2); i < len; ++i) f[i] = 1ll * f[i] * in % P;
}
int n, q, w;

struct poly {
  int c[W];
  poly() { fill(c, c + W, 0); }
  poly(const poly &b) { memcpy(c, b.c, sizeof(c)); }
  poly(const int f[W]) {
    memcpy(c, f, sizeof(c));
    dft(c, w, 1);
  }
  poly operator+(const poly &b) {
    poly res(*this);
    FOR(i, 0, w - 1) res.c[i] = (res.c[i] + b.c[i]) % P;
    return res;
  }
  poly operator*(const poly &b) {
    poly res(*this);
    FOR(i, 0, w - 1) res.c[i] = (res.c[i] * 1ll * b.c[i]) % P;
    return res;
  }
};

poly pval(int v) {
  int f[W] = {0};
  f[v] = 1;
  return poly(f);
}

poly a[N];
int main() {
  scanf("%d%d%d", &n, &q, &w);
  init(w);
  FOR(i, 1, n) {
    int v;
    scanf("%d", &v);
    a[i] = pval(v);
  }
  return 0;
}
