#include <bits/stdc++.h>
#define FOR(a, b, c) for (int a = (int)(b); a <= (int)(c); a++)
using namespace std;

const int P = 1e9 + 7;

int pw(int a, int m) {
  int res = 1;
  while (m) m & 1 ? res = 1ll * res * a % P : 0, a = 1ll * a * a % P, m >>= 1;
  return res;
}
void add(int &x, long long y) { x = (x + y) % P; }

const int N = 1e7 + 5;
int p2[N];
int g[N];
void work() {
  p2[0] = 1;
  FOR(i, 1, N - 1) p2[i] = p2[i - 1] * 2ll % P;

  g[0] = 1;
  FOR(i, 1, N - 1) g[i] = 1ll * g[i - 1] * (p2[i] - 1) % P;
}
int calc(int n, int m) {
  int res = 1ll * g[n] * pw(1ll * g[n - m] * g[m] % P, P - 2) % P;
  res = 1ll * res * pw(2, m * (m - 1ll) / 2 % (P - 1)) % P;
  res = 1ll * res * g[n] % P * pw(g[n - m], P - 2) % P;
  return res;
}

void go() {
  int n, m;
  scanf("%d%d", &n, &m);
  printf("%d\n", calc(n, m));
}

int main() {
  work();

  int t;
  scanf("%d", &t);
  FOR(i, 1, t) go();
  return 0;
}
