#include <bits/stdc++.h>
#define FOR(a, b, c) for (int a = (int)(b); a <= (int)(c); a++)
using namespace std;

int rnd(int L, int R) { return rand() % (R - L + 1) + L; }

vector<pair<int, int>> edges, es;

int main() {
  srand(clock() + time(0) * 1000);

  int n = rnd(3, 1000), m = rnd(n - 1, n * (n - 1) / 2) / 2 * 2;

  if (m < n - 1) m += 2;
  FOR(i, 2, n) {
    int x = rnd(1, i - 1);
    es.push_back({x, i});
    FOR(j, 1, i - 1) if (j != x) edges.push_back({i, j});
  }
  random_shuffle(edges.begin(), edges.end());
  FOR(i, 0, m - (n - 1) - 1) es.push_back(edges[i]);

  random_shuffle(es.begin(), es.end());
  assert(m == es.size());

  printf("%d %d\n", n, m);
  for (auto e : es) printf("%d %d\n", e.first, e.second);

  return 0;
}
