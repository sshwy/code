// by Yao
#include <bits/stdc++.h>
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

using std::pair;
const int N = 5005;
int a, b;
int f[N][N], g[N][N], tag;

std::vector<pair<int, int>> v, q, q2;

int main() {
  scanf("%d%d", &a, &b);

  FOR(i, 0, a) FOR(j, 0, b) if (i + j > 0) v.push_back({i, j});

  sort(v.begin(), v.end());

  memset(f, -0x3f, sizeof(f));

  f[0][0] = 0;
  q.push_back({0, 0});

  for (auto t : v) {
    int x = t.first, y = t.second;
    ++tag;
    q2.clear();
    for (auto e : q) {
      int i = e.first, j = e.second;
      if (i + x > a || j + y > b) continue;
      if (f[i + x][j + y] < f[i][j] + 1) {
        f[i + x][j + y] = f[i][j] + 1;
        if (g[i + x][j + y] == 0) {
          q2.push_back({i + x, j + y});
          g[i + x][j + y] = 1;
        }
      }
    }
    q.insert(q.end(), q2.begin(), q2.end());
  }

  printf("%d\n", f[a][b]);

  return 0;
}
