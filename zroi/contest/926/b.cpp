#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

const int M = 2e6 + 500, N = 2e6 + 5;
int n, m;
int dg[N];

namespace G {
  int e_cnt;
  struct qxx {
    int nex, t, id, id1, id2;
  };
  qxx e[M];
  int h[N], le = 1;
  void ae(int f, int t, int id, int id1, int id2) {
    // printf("G::ae %d %d\n", f, t);
    e[++le] = (qxx){h[f], t, id, id1, id2}, h[f] = le;
  }

  bool vis[M];
  int a[M], la = 0;
  vector<int> ans[N];
  void dfs(int u) {
    for (int i = h[u]; i; i = h[u]) {
      while (i && vis[e[i].id]) i = e[i].nex;
      h[u] = i;
      const int v = e[i].t, id = e[i].id;
      if (i) {
        vis[id] = 1;
        dfs(v);
        a[++la] = i; // i 即 u -> v的边的编号
      }
    }
  }
  void go() {
    int las = -1, tot_k = 0;
    FOR(i, 1, n) {
      if (dg[i] % 2) {
        if (las == -1) {
          las = i;
        } else {
          ++tot_k;
          ++e_cnt;
          // 无意义的边，求完欧拉回路后就可以不要的
          ae(i, las, e_cnt, m + 1, m + 1);
          ae(las, i, e_cnt, m + 1, m + 1);
          las = -1;
        }
      }
    }

    assert(las == -1);

    int pos = 1;

    FOR(nd, 1, n) {
      la = 0;
      dfs(nd);

      // FOR(i,1,la){
      //   printf("a[%d]: %d -> %d id=%d,id1=%d,id2=%d\n",i,
      //       e[a[i]^1].t,e[a[i]].t,e[a[i]].id,e[a[i]].id1,e[a[i]].id2);
      // }

      reverse(a + 1, a + la + 1);

      bool flag = false;

      FOR(i, 1, la) if (e[a[i]].id1 == m + 1) {
        flag = true;
        break;
      }

      if (flag) {
        int st = 1;
        while (e[a[st]].id1 != m + 1) { st = st == la ? 1 : st + 1; }

        FOR(_, 1, la) {
          const qxx &now = e[a[st]];
          if (now.id1 == m + 1) {
            if (!ans[pos].empty()) ++pos;
          } else {
            ans[pos].pb(a[st]);
          }
          st = st == la ? 1 : st + 1;
        }
      }
    }

    FOR(i, 1, pos) {
      if (ans[i].empty()) continue;
      --tot_k;
      printf(
          "%d %d %lu\n", e[ans[i][0] ^ 1].t, e[ans[i].back()].t, ans[i].size() * 2);
      for (unsigned j = 0; j < ans[i].size(); j++) {
        int a_st = ans[i][j];
        printf("%d %d%c", e[a_st].id1, e[a_st].id2, " \n"[j + 1 == ans[i].size()]);
      }
    }
    assert(tot_k == 0);
  }
} // namespace G
namespace Origin {
  struct qxx {
    int nex, t, idx;
  };
  qxx e[M];
  int h[N], le = 1;
  void ae(int f, int t, int idx) { e[++le] = (qxx){h[f], t, idx}, h[f] = le; }

  bool vis[N];
  int dep[N], match[M];

  void dfs(int u, int dp, int fa_path) {
    vis[u] = 1, dep[u] = dp;
    for (int i = h[u]; i; i = e[i].nex) {
      const int v = e[i].t;
      if (!vis[v]) { dfs(v, dp + 1, i ^ 1); }
    }
    vector<int> path;
    path.clear();
    for (int i = h[u]; i; i = e[i].nex) {
      const int v = e[i].t, idx = e[i].idx;
      if (dep[v] < dep[u]) continue; //保证不是返祖边
      if (match[idx]) continue;      //没有使用过
      path.pb(i);
    }
    for (unsigned i = 0; i + 1 < path.size(); i += 2) {
      int x = path[i], y = path[i + 1];
      ++G::e_cnt;
      G::ae(e[x].t, e[y].t, G::e_cnt, e[x].idx, e[y].idx);
      G::ae(e[y].t, e[x].t, G::e_cnt, e[y].idx, e[x].idx);
      match[e[x].idx] = 1, match[e[y].idx] = 1;
    }
    if (path.size() & 1) {
      int tm = path[path.size() - 1];
      ++G::e_cnt;
      G::ae(e[fa_path].t, e[tm].t, G::e_cnt, e[fa_path].idx, e[tm].idx);
      G::ae(e[tm].t, e[fa_path].t, G::e_cnt, e[tm].idx, e[fa_path].idx);
      match[e[fa_path].idx] = 1, match[e[tm].idx] = 1;
    }
  }
} // namespace Origin
int main() {
  scanf("%d%d", &n, &m);

  FOR(i, 1, m) {
    int u, v;
    scanf("%d%d", &u, &v);
    Origin::ae(u, v, i), Origin::ae(v, u, i);
    dg[u]++, dg[v]++;
  }
  Origin::dfs(1, 1, 0);

  G::go();
  return 0;
}
