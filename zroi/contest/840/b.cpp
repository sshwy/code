// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

void getMin(int &x, int y) { x = min(x, y); }

const int N = 16, ALP = 26, L = 1e6 + 5, INF = 1e9;

int n, cnt[1 << N][ALP], mc[1 << N], sc[1 << N], len[1 << N], f[1 << N], nn;
char s[L];

int main() {
  scanf("%d", &n);
  nn = 1 << n;
  FOR(i, 0, n - 1) {
    scanf("%s", s);
    len[1 << i] = strlen(s);
    for (int j = 0; s[j]; ++j) { cnt[1 << i][s[j] - 'a']++; }
  }

  FOR(i, 1, nn - 1) if (i != (i & -i)) {
    int j = i & -i;
    FOR(c, 0, ALP - 1) { cnt[i][c] = min(cnt[j][c], cnt[i - j][c]); }
  }
  FOR(i, 0, nn - 1) {
    mc[i] = accumulate(cnt[i], cnt[i] + ALP, 0);
    f[i] = INF;
  }

  FOR(i, 1, nn - 1) {
    if (i == (i & -i)) {
      f[i] = len[i];
    } else {
      for (int j = (i - 1) & i; j; j = (j - 1) & i) {
        getMin(f[i], f[j] + f[i - j] - mc[i]);
      }
    }
    // printf("f[%d] = %d\n", i, f[i]);
  }
  printf("%d\n", f[nn - 1] + 1);

  return 0;
}
