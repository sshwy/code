// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

const int N = 11, N2 = 21;

int n, m, lw[N2], rw[N2], nn, mm, L[1 << N2], R[1 << N2], lL, lR;
bool ok[1 << N][1 << N];
char s[N2][N2];
long long T;

void go1() {
  int ans = 0;
  ok[0][0] = true;
  FOR(i, 0, nn - 1) FOR(j, 0, mm - 1) {
    FOR(x, 0, n - 1) if (i >> x & 1) {
      if (ok[i][j]) break;
      FOR(y, 0, m - 1) if (j >> y & 1) {
        if (ok[i][j]) break;
        if (s[x][y] == '1' && ok[i - (1 << x)][j - (1 << y)]) { ok[i][j] = true; }
      }
    }
  }
  FOR(i, 0, nn - 1) FOR(j, 0, mm - 1) {
    if (ok[i][j]) {
      for (int x = i; x != -1; x = x == 0 ? -1 : (x - 1) & i) {
        for (int y = j; y != -1; y = y == 0 ? -1 : (y - 1) & j) { ok[x][y] = true; }
      }
    }
  }
  FOR(i, 0, nn - 1) FOR(j, 0, mm - 1) {
    if (ok[i][j]) {
      long long sum = 0;
      FOR(x, 0, n - 1) if (i >> x & 1) sum += lw[x];
      FOR(y, 0, m - 1) if (j >> y & 1) sum += rw[y];
      if (sum >= T) { ++ans; }
    }
  }
  printf("%d\n", ans);
  exit(0);
}

void go2() {
  FOR(i, 0, nn - 1) {
    FOR(j, 0, n - 1) if (i >> j & 1) L[lL] += lw[j];
    ++lL;
  }
  FOR(i, 0, mm - 1) {
    FOR(j, 0, m - 1) if (i >> j & 1) R[lR] += rw[j];
    ++lR;
  }
  sort(L, L + lL);
  sort(R, R + lR);
  int pos = 0;
  long long ans = 0;
  while (pos < lR && L[0] + R[pos] < T) ++pos;
  FOR(i, 0, lL - 1) {
    while (pos > 0 && L[i] + R[pos - 1] >= T) --pos;
    ans += lR - pos;
  }
  printf("%lld\n", ans);
  exit(0);
}
int main() {
  scanf("%d%d", &n, &m);
  nn = 1 << n;
  mm = 1 << m;
  FOR(i, 0, n - 1) { scanf("%s", s[i]); }
  FOR(i, 0, n - 1) scanf("%d", lw + i);
  FOR(i, 0, m - 1) scanf("%d", rw + i);
  scanf("%lld", &T);
  if (n <= 8 && m <= 8) {
    go1();
  } else {
    go2();
  }
  return 0;
}
