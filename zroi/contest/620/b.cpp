// by Sshwy
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA
const int N = 1e6 + 5;
char s[N];
int n;
int base, mod;
int h[N], g[N];
void init() {
  base = RA::rnd(10, 100);
  mod = RA::rnd(1e8, 1e9);
  // printf("base %d mod %d\n",base ,mod);
  h[0] = s[0] % mod;
  FOR(i, 1, n + 1) h[i] = (h[i - 1] * 1ll * base + s[i]) % mod;
  g[0] = 1;
  FOR(i, 1, n + 1) g[i] = g[i - 1] * 1ll * base % mod;
}
int hsh(int l, int r) {
  return ((h[r] - h[l - 1] * 1ll * g[r - l + 1]) % mod + mod) % mod;
}
void go(int id) {
  n = strlen(s + 1);
  // fprintf(stderr,"n %d\n",n);
  if (n == 1) return puts("1"), void();
  int L = 0, R = 0;
  s[0] = s[n], s[n + 1] = s[1];
  init();
  FOR(i, 1, n - 1) if (s[i] == s[i + 1]) R = i;
  ROF(i, n - 1, 1) if (s[i] == s[i + 1]) L = i;
  string ans;
  if (L || R || s[1] == s[n])
    ans.pb('0');
  else
    ans.pb('1');
  // printf("L %d R %d\n",L,R);
  FOR(i, 1, n - 1) {
    if (s[1] == s[n]) {
      if (s[1 + i] != s[n] && (!L && !R || R <= i) ||
          s[n - i] != s[1] && (!L && !R || L >= n - i))
        ans.pb('1');
      else
        ans.pb('0');
    } else if (L || R) {
      int l = max(1, R - i + 1), r = min(L + 1, n - i + 1);
      if (l <= r) {
        // printf("i %d: %d %d %d %d : ",i,l-1,r-1,l+i,r+i);
        // FOR(j,l-1,r-1)printf("%c",s[j]);
        // puts("");
        // FOR(j,l+i,r+i)printf("%c",s[j]);
        // puts("");
        // printf("%d %d\n",hsh(l-1,r-1),hsh(l+i,r+i));
        if (hsh(l - 1, r - 1) == hsh(l + i, r + i))
          ans.pb('0');
        else
          ans.pb('1');
      } else
        ans.pb('0');
    } else {
      if (1 <= n - i - 1 && hsh(1, n - i - 1) == hsh(i + 2, n))
        ans.pb('0');
      else
        ans.pb('1');
    }
  }
  *(ans.end() - 1) = '1';
  printf("Case %d:%s\n", id, ans.c_str());
}
int main() {
  srand(clock() + time(0));
  int t = 0;
  while (~scanf("%s", s + 1)) go(++t);
  return 0;
}
