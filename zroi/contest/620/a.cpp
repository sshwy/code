// by Sshwy
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA
const int N = 5e3 + 50, W = 1e6 + 5;
const long long INF = 1e18;
int n, a[N];
int p, q, r;
long long f[N], g[N]; // f[i]: 把1-i凑齐的最小代价
int lim = 5e3 + 5;
int main() {
  scanf("%d", &n);
  FOR(i, 1, n) scanf("%d", &a[i]);
  scanf("%d%d%d", &p, &q, &r);
  sort(a + 1, a + n + 1);
  fill(f, f + N, INF);
  f[0] = 0;
  FOR(i, 1, n) {
    fill(g, g + N, INF);
    int ai = a[i];
    FOR(j, 0, min(ai - 1, lim)) if (f[j] != INF) {
      g[j + 1] = min(g[j + 1], f[j] + (ai - (j + 1)) * 1ll * q);
    }
    FOR(j, min(ai - 1, lim) + 1, lim) if (f[j] != INF) {
      g[j + 1] = min(g[j + 1], f[j] + (j + 1 - ai) * 1ll * p);
    }
    FOR(j, 0, min(ai, lim)) if (f[j] != INF) {
      g[j] = min(g[j], f[j] + (ai - j) * 1ll * q);
    }
    FOR(j, min(ai, lim) + 1, lim) if (f[j] != INF) { g[j] = min(g[j], f[j]); }
    memcpy(f, g, sizeof(f));
  }
  long long ans = min(f[0], *min_element(f + 1, f + N) + r);
  printf("%lld\n", ans);
  return 0;
}
