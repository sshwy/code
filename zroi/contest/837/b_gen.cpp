// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

int rnd(int p) { return 1ll * rand() * rand() % p; }
int rnd(int L, int R) { return rnd(R - L + 1) + L; }
const int M = 505;
char s[M];

int main() {
  srand(clock() + time(0));
  int n = rnd(1, 6), m = rnd(1, 15);
  printf("%d %d\n", n, m);
  FOR(i, 1, n) {
    int ls = rnd(1, min(m, 10));
    FOR(i, 1, ls) s[i - 1] = '0' + rnd(0, 1);
    s[ls] = 0;
    printf("%s\n", s);
  }
  return 0;
}
