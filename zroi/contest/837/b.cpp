// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

const int N = 7, M = 505, SZ = N * 101 * 2, P = 998244353;
int n, m;

int tr[SZ][2], fail[SZ], tot;
vector<int> endId[SZ], passId[SZ];

void work(char *s, int ls, int id) {
  bool ok[M] = {0};
  FOR(i, 0, ls - 1) {
    ok[i] = 1;
    FOR(j, 0, i) {
      if (i + i + 1 - j < ls && s[i + i + 1 - j] == s[j]) {
        ok[i] = 0;
        break;
      }
    }
    if (i + i + 1 < ls - 1) ok[i] = 0;
  }
  int u = 0;
  FOR(i, 0, ls - 1) {
    int c = s[i];
    if (!tr[u][c]) tr[u][c] = ++tot;
    u = tr[u][c];
    if (ok[i]) endId[u].pb(id);
  }
  passId[u].pb(id);
}

void add(int &x, int y) { x = (x + y) % P; }
int q[SZ], ql, qr;
void uni(vector<int> &v) {
  sort(v.begin(), v.end());
  v.erase(unique(v.begin(), v.end()), v.end());
}
void build() {
  ql = 1, qr = 0;
  FOR(i, 0, 1) if (tr[0][i]) q[++qr] = tr[0][i];
  while (ql <= qr) {
    int u = q[ql++];
    FOR(c, 0, 1) {
      if (tr[u][c])
        fail[tr[u][c]] = tr[fail[u]][c], q[++qr] = tr[u][c];
      else
        tr[u][c] = tr[fail[u]][c];
    }
  }
  FOR(i, 1, qr) {
    int u = q[i];
    if (fail[u] != u) {
      for (int x : endId[fail[u]]) endId[u].pb(x);
      for (int x : passId[fail[u]]) passId[u].pb(x);
    }
    uni(endId[u]);
    uni(passId[u]);
  }
}

char s[M];
int f[M][SZ][1 << 6];

int main() {
  scanf("%d%d", &n, &m);
  FOR(i, 0, n - 1) {
    scanf("%s", s);
    int ls = strlen(s);
    FOR(i, 0, ls - 1) s[i] -= '0';
    work(s, ls, i);
    reverse(s, s + ls);
    FOR(i, 0, ls - 1) s[i] ^= 1;
    work(s, ls, i);
  }
  build();
  f[0][0][0] = 1;
  FOR(i, 0, m) {
    FOR(j, 0, tot) {
      FOR(k, 0, (1 << n) - 1) if (f[i][j][k]) {
        FOR(c, 0, 1) {
          int nj = tr[j][c];
          int nk = k;
          for (int x : passId[nj]) nk |= 1 << x;
          add(f[i + 1][nj][nk], f[i][j][k]);
        }
      }
    }
  }
  int ans = 0;
  FOR(j, 0, tot) {
    int endk = 0;
    for (int x : endId[j]) endk |= 1 << x;
    FOR(k, 0, (1 << n) - 1) if ((k | endk) == (1 << n) - 1 && f[m][j][k]) {
      add(ans, f[m][j][k]);
    }
  }
  printf("%d\n", ans);
  return 0;
}
