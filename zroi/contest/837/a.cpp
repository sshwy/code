// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

const int N = 1e5 + 5, P = 1e9 + 7;
int pw(int a, int m) {
  int res = 1;
  while (m) m & 1 ? res = 1ll * res * a % P : 0, a = 1ll * a * a % P, m >>= 1;
  return res;
}
int n, a, b, c1, c2, c3, rv;
long long aa, ab, af, as; // Alice, Bob, First, Second

int main() {
  scanf("%d%d%d", &n, &a, &b);
  if (a < b) swap(a, b), rv = 1;
  FOR(i, 1, n) {
    int x;
    scanf("%d", &x);
    x %= a + b;
    if (x < b)
      ;
    else if (x < a)
      c1++;
    else if (x < 2 * b)
      c2++;
    else
      c3++;
  }
  // printf("%d %d %d\n", c1, c2, c3);
  // has c1
  if (c1) ab += (pw(2, c1) - 1) * 1ll * (pw(2, n - c1)) % P;
  // c3 > 1, c1 = 0
  if (c3 > 1) ab += (pw(2, c3) - 1 - c3) * 1ll * (pw(2, n - c3 - c1)) % P;
  // c3 = 0, c1 = 0
  if (c2) {
    af += pw(2, c2 - 1) * 1ll * pw(2, n - c1 - c2 - c3) % P;
    as += pw(2, c2 - 1) * 1ll * pw(2, n - c1 - c2 - c3) % P;
  } else {
    as += pw(2, n - c1 - c3);
  }
  // c3 = 1
  if (c3) {
    if (c2) {
      af += pw(2, c2 - 1) * 1ll * pw(2, n - c1 - c2 - c3) % P * c3 % P;
      ab += pw(2, c2 - 1) * 1ll * pw(2, n - c1 - c2 - c3) % P * c3 % P;
    } else {
      af += pw(2, n - c1 - c3);
    }
  }
  if (rv) swap(aa, ab);
  aa %= P;
  ab %= P;
  af %= P;
  as %= P;

  // long long sss = (aa + ab + af + as) %P;
  // assert(sss == pw(2,n));

  printf("%lld %lld %lld %lld\n", aa, ab, af, as);
  return 0;
}
