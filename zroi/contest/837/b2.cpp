// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

const int N = 7, M = 505;
int n, m;
char s[N][M];
int t[M];

bool check(int stat) {
  FOR(i, 0, m - 1) t[i + 1] = stat >> i & 1;
  FOR(i, 1, m) t[m * 2 - i + 1] = 1 - t[i];
  FOR(i, 1, n) {
    bool flag = false;
    int ls = strlen(s[i] + 1);
    FOR(j, 0, m * 2 - ls) {
      bool fl = true;
      FOR(k, 1, ls) {
        if (t[j + k] != s[i][k] - '0') {
          fl = false;
          break;
        }
      }
      if (fl) {
        flag = true;
        break;
      }
    }
    if (flag == false) return false;
  }
  // FOR(i,1,m*2)printf("%d%c", t[i]," \n"[i==m*2]);
  return true;
}
int main() {
  scanf("%d%d", &n, &m);
  FOR(i, 1, n) scanf("%s", s[i] + 1);
  int lim = 1 << m;
  int ans = 0;
  FOR(i, 0, lim - 1) if (check(i))++ ans;
  printf("%d\n", ans);
  return 0;
}
