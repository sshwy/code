// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

const int N = 2e7 + 50, P = 998244353;

void add(int &x, int y) { x = (x + y) % P; }
int pw(int a, int m) {
  int res = 1;
  while (m) m & 1 ? res = 1ll * res * a % P : 0, a = 1ll * a * a % P, m >>= 1;
  return res;
}

int fac[N], fnv[N];

int binom(int a, int b) {
  if (a < b || a < 0) return 0;
  return fac[a] * 1ll * fnv[b] % P * fnv[a - b] % P;
}

int calc(int a1, int b1, int a2, int b2) {
  if ((a1 + b1) % 2 != (a2 + b2) % 2) return 0;
  int t = binom(a2 - a1, (a2 - a1 + b1 - b2) / 2);
  return t;
}
int solve(int a1, int b1, int a2, int b2, int yl, int yr) {
  if ((a1 + b1) % 2 != (a2 + b2) % 2) return 0;

  int res = calc(a1, b1, a2, b2);
  {
    int y = b2, cur = 0, coef = 1;
    do {
      cur ^= 1, coef = P - coef;
      if (cur == 0)
        y = yl * 2 - y;
      else
        y = yr * 2 - y;
      if (abs(abs(b1 - y) > abs(a1 - a2))) break;
      int t = 1ll * calc(a1, b1, a2, y) * coef % P;
      add(res, t);
    } while (1);
  }
  {
    int y = b2, cur = 1, coef = 1;
    do {
      cur ^= 1, coef = P - coef;
      if (cur == 0)
        y = yl * 2 - y;
      else
        y = yr * 2 - y;
      if (abs(abs(b1 - y) > abs(a1 - a2))) break;
      int t = 1ll * calc(a1, b1, a2, y) * coef % P;
      add(res, t);
    } while (1);
  }
  return res;
}

int main() {
  fac[0] = 1;
  FOR(i, 1, N - 1) fac[i] = fac[i - 1] * 1ll * i % P;
  fnv[N - 1] = pw(fac[N - 1], P - 2);
  ROF(i, N - 1, 1) fnv[i - 1] = fnv[i] * 1ll * i % P;

  int n, m;
  scanf("%d%d", &n, &m);
  if (m == 2) return puts("0"), 0;

  int ans = solve(1, 1, 2 * n - 1, 1, 0, m);
  printf("%d\n", ans);
  return 0;
}
