// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

const int N = 2e7 + 50, P = 998244353;

int pw(int a, int m) {
  int res = 1;
  while (m) m & 1 ? res = 1ll * res * a % P : 0, a = 1ll * a * a % P, m >>= 1;
  return res;
}

int fac[N], fnv[N];

int binom(int a, int b) {
  if (a < b) return 0;
  return fac[a] * 1ll * fnv[b] % P * fnv[a - b] % P;
}
int setF(int n, int *f) {
  f[0] = 1;
  if (n == 0) return 1;
  int cur = n;
  FOR(i, 1, n) {
    f[i] = binom(cur, i);
    if (i % 2) f[i] = (P - f[i]) % P;
    --cur;
    if (f[i] == 0) return i;
  }
  return n + 1;
}

int f[N], g[N], h[N];
int n, m;

void work() {
  int lf = setF(m - 3, f);
  int lg = setF(m - 2, g);
  // FOR(i,0,lf-1)printf("%d%c", f[i], " \n"[i==lf-1]);
  // FOR(i,0,lg-1)printf("%d%c", g[i], " \n"[i==lg-1]);
  int ig0 = pw(g[0], P - 2);
  h[0] = f[0] * 1ll * ig0 % P;
  FOR(i, 1, n - 1) {
    int s = 0;
    FOR(j, 0, i - 1) { s = (s + h[j] * 1ll * g[i - j]) % P; }
    h[i] = ((f[i] - s) % P + P) % P * 1ll * ig0 % P;
    // printf("%d%c", h[i]," \n"[i==n-1]);
  }
  printf("%d\n", h[n - 1]);
}

int main() {
  fac[0] = 1;
  FOR(i, 1, N - 1) fac[i] = fac[i - 1] * 1ll * i % P;
  fnv[N - 1] = pw(fac[N - 1], P - 2);
  ROF(i, N - 1, 1) fnv[i - 1] = fnv[i] * 1ll * i % P;

  scanf("%d%d", &n, &m);
  if (m == 2) return puts("0"), 0;
  work();
  return 0;
}
