// by Sshwy
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA
const int N = 2e5 + 5, P = 1e9 + 7;
int pw(int a, int m) {
  int res = 1;
  while (m) m & 1 ? res = 1ll * res * a % P : 0, a = 1ll * a * a % P, m >>= 1;
  return res;
}
int n, m;
struct at {
  int l, r, t;
};
struct bt {
  int x, d;
};
bool operator<(const at x, const at y) { return x.l < y.l; }

set<at> s;
vector<bt> v[N], q[N];

void add(int k, int t, int i) {
  v[i].pb({i, k});
  v[i].pb({t, P - k});
}
void work(int l, int r, int i) {
  auto p = s.lower_bound({l, r, i});
  if (p != s.begin()) --p;
  while (p != s.end() && p->l <= r) {
    auto q = p;
    ++q;
    if (l <= p->r) {
      int L = max(l, p->l), R = min(r, p->r);
      add(R - L + 1, p->t, i);
      at p1 = *p;
      s.erase(p);
      if (L == p1.l && R == p1.r)
        p1.l = -1, s.insert(p1);
      else if (L == p1.l)
        p1.l = R + 1, s.insert(p1);
      else if (R == p1.r)
        p1.r = L - 1, s.insert(p1);
      else {
        at p2 = p1;
        p1.r = L - 1, p2.l = R + 1;
        s.insert(p1), s.insert(p2);
      }
    }
    p = q;
  }
  s.insert({l, r, i});
}
struct fenwick {
  int c[N];
  void add(int pos, int k) {
    for (int i = pos; i < N; i += i & -i) c[i] = (c[i] + k) % P;
  }
  int pre(int pos) {
    int res = 0;
    for (int i = pos; i > 0; i -= i & -i) res = (res + c[i]) % P;
    return res;
  }
};
struct fenwick_2 {
  fenwick a, b;
  void add(int pos, int k) {
    a.add(pos, k * 1ll * pos % P);
    b.add(pos, k);
  }
  void pre_add(int pos, int k) {
    add(1, k);
    add(pos + 1, P - k);
  }
  int pre(int pos) { return (b.pre(pos) * 1ll * (pos + 1) % P - a.pre(pos) + P) % P; }
} A, B;
int ans[N], a2[N];
signed main() {
  scanf("%d%d", &n, &m);
  s.insert({1, (int)1e9, 0});
  FOR(i, 1, n) {
    int l, r;
    scanf("%d%d", &l, &r);
    work(l, r - 1, i);
  }
  FOR(i, 1, m) {
    int a, b;
    scanf("%d%d", &a, &b);
    q[b].pb({a, i});
    a2[i] = ((b - a + 2) * 1ll * (b - a + 1) / 2) % P;
  }
  FOR(y, 1, n) {
    for (auto a : v[y]) {
      A.pre_add(a.x, a.d);
      B.pre_add(a.x, a.d * 1ll * (y - 1) % P);
    }
    for (auto a : q[y]) {
      int t = ((A.pre(n) - A.pre(a.x - 1) + P) % P * 1ll * y % P -
                  (B.pre(n) - B.pre(a.x - 1)) % P + P) %
              P;
      ans[a.d] = (ans[a.d] + t) % P;
    }
  }
  FOR(i, 1, m) printf("%lld\n", ans[i] * 1ll * pw(a2[i], P - 2) % P);
  return 0;
}
