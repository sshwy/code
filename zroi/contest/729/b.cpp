// by Sshwy
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

const int N = 5005, P = 1e9 + 7;
int pw(int a, int m) {
  int res = 1;
  while (m) m & 1 ? res = 1ll * res * a % P : 0, a = 1ll * a * a % P, m >>= 1;
  return res;
}
int n, m;
int fac[N * 2], fnv[N * 2];
int g[N][N], ans[N];

int binom(int a, int b) {
  if (a < b) return 0;
  return fac[a] * 1ll * fnv[b] % P * fnv[a - b] % P;
}

int main() {
  scanf("%d%d", &n, &m);
  int lim = n + m;
  fac[0] = 1;
  FOR(i, 1, lim) fac[i] = 1ll * fac[i - 1] * i % P;
  fnv[lim] = pw(fac[lim], P - 2);
  ROF(i, lim, 1) fnv[i - 1] = fnv[i] * 1ll * i % P;

  ROF(i, n, 1) {
    FOR(j, 0, m) {
      if (i * j > m) break;
      g[i][j] = binom(m - i * j + n - 1, n - 1) * 1ll * binom(n, i) % P;
      FOR(k, i + 1, n) {
        if (k * j > m) break;
        g[i][j] = (g[i][j] - binom(k, i) * 1ll * g[k][j] % P + P) % P;
      }
    }
  }
  ROF(i, n, 1) FOR(j, 0, m) g[i][j] = (g[i][j] + g[i + 1][j]) % P;
  FOR(r, 1, n) FOR(x, 0, m) ans[r] = (ans[r] + g[r][x]) % P;
  FOR(r, 2, n) ans[r] = (ans[r] + ans[r - 1]) % P;
  int p = binom(n + m - 1, m);
  p = pw(p, P - 2);
  FOR(i, 1, n) ans[i] = ans[i] * 1ll * p % P;
  FOR(i, 1, n) printf("%d\n", ans[i]);
  return 0;
}
