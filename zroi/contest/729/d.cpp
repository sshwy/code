// by Sshwy
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
}

int n;
typedef pair<int, int> pt;
pt operator-(const pt x, const pt y) {
  return {x.first - y.first, x.second - y.second};
}
long long det(const pt x, const pt y) {
  return 1ll * x.first * y.second - 1ll * y.first * x.second;
}
typedef vector<pt> cvx;
vector<pt> v[2];
cvx up, dn;

pt abs(pt p) { return p.first < 0 ? (pt){-p.first, -p.second} : p; }

// x=0时既最大也最小
pt max(const pt x, const pt y) {
  if (x.first == 0) return x;
  if (y.first == 0) return y;
  if (det(abs(x), abs(y)) >= 0) return y;
  return x;
}
pt min(const pt x, const pt y) {
  if (x.first == 0) return x;
  if (y.first == 0) return y;
  if (det(abs(x), abs(y)) >= 0) return x;
  return y;
}
int tot;
void add(pt p) {
  int l = 0, r = up.size() - 1, mx_u, mn_u, mx_d, mn_d;
  // up
  // max
  while (l < r) {
    int mid = (l + r + 1) >> 1;
    if (det(up[mid] - up[mid - 1], p - up[mid]) >= 0)
      r = mid - 1;
    else
      l = mid;
  }
  mx_u = l, l = 0, r = up.size() - 1;
  // min
  while (l < r) {
    int mid = (l + r) >> 1;
    if (det(up[mid] - up[mid + 1], p - up[mid]) <= 0)
      l = mid + 1;
    else
      r = mid;
  }
  mn_u = l;
  // down
  l = 0, r = dn.size() - 1;
  // max
  while (l < r) {
    int mid = (l + r) >> 1;
    if (det(dn[mid] - dn[mid + 1], p - dn[mid]) >= 0)
      l = mid + 1;
    else
      r = mid;
  }
  mx_d = l, l = 0, r = dn.size() - 1;
  // min
  while (l < r) {
    int mid = (l + r + 1) >> 1;
    if (det(dn[mid] - dn[mid - 1], p - dn[mid]) <= 0)
      r = mid - 1;
    else
      l = mid;
  }
  mn_d = l;
  pt mx = min(up[mx_u], dn[mx_d]), mn = max(up[mn_u], dn[mn_d]);
  if (mx.first == 0 && mn.first == 0) {
    assert(up.size() == 1 && dn.size() == 1);
    tot++;
  } else if (mx.first == 0 || mn.first == 0) { // mx <= mn
  }
  int main() {
    scanf("%d", &n);
    FOR(i, 1, n) {
      int a, b, c;
      scanf("%d%d%d", &a, &b, &c);
      v[c].pb({a, b});
    }
    sort(v[1].begin(), v[1].end());
    for (unsigned i = 0; i < v[1].size(); i++) {
      while (up.size() > 1 &&
             det(*(up.end() - 1) - *(up.end() - 2), v[1][i] - *(up.end() - 1)) >= 0)
        up.pop_back();
      up.pb(v[1][i]);
    }
    for (unsigned i = 0; i < v[1].size(); i++) {
      while (dn.size() > 1 &&
             det(*(dn.end() - 1) - *(dn.end() - 2), v[1][i] - *(dn.end() - 1)) <= 0)
        dn.pop_back();
      dn.pb(v[1][i]);
    }
    assert(up.size() && dn.size());
    for (auto p : v[0]) add(p);
    return 0;
  }
