// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

const int N = 2e5 + 5;

void getMax(int &x, int y) { x = max(x, y); }
void getMin(int &x, int y) { x = min(x, y); }

struct Point {
  int x, y, typ, id;
  void flipX() { x = -x; }
  void flipY() { y = -y; }
  void toChebyshev() {
    int tx = x + y, ty = y - x;
    x = tx, y = ty;
  }
  static bool byX(Point p1, Point p2) {
    return p1.x == p2.x ? p1.typ > p2.typ : p1.x < p2.x;
  }
};

int n, m;
vector<int> min_dist(N, 1e9), D, c;
vector<Point> points, rabbits, stations;

template <int (*operate)(int, int)> void upd(int pos, int val) {
  for (int i = pos; i < N; i += i & -i) c[i] = operate(c[i], val);
}
template <int (*operate)(int, int), int initVal> int pre(int pos) {
  int res = initVal;
  for (int i = pos; i > 0; i -= i & -i) res = operate(res, c[i]);
  return res;
}

int max(int a, int b) { return a > b ? a : b; }
int pls(int a, int b) { return a + b; }

void work() {
  sort(points.begin(), points.end(), Point::byX);

  D.clear();
  for (auto u : points) D.pb(u.y);
  sort(D.begin(), D.end());

  c = vector<int>(N, -1e9);

  auto xpre = pre<max, -int(1e9)>;
  auto xupd = upd<max>;

  for (auto u : points) {
    int p = lower_bound(D.begin(), D.end(), u.y) - D.begin() + 1;
    if (u.typ == 0) { // rabbit
      int dist = u.x + u.y - xpre(p);
      assert(dist >= 0);
      min_dist[u.id] = min(min_dist[u.id], dist);
    } else if (u.typ == 1) {
      xupd(p, u.x + u.y);
    }
  }
}

bool byMinDist(Point p1, Point p2) { return min_dist[p1.id] > min_dist[p2.id]; }

struct Query {
  int x, ly, ry, coef;
  static bool byX(Query q1, Query q2) { return q1.x < q2.x; }
};

vector<Query> querys;

void aq(int Lx, int Rx, int Ly, int Ry, int d) {
  Lx -= d, Rx += d, Ly -= d, Ry += d;
  querys.pb({Lx - 1, Ly, Ry, -1});
  querys.pb({Rx, Ly, Ry, 1});
}

bool check(int d) { // (x, y) => (x + y, y - x)
  printf("check %d\n", d);
  auto xpre = pre<pls, 0>;
  auto xupd = upd<pls>;
  c = vector<int>(N, 0);
  querys.clear();
  int Lx = -1e9, Rx = 1e9, Ly = -1e9, Ry = 1e9;

  for (auto e : rabbits) {
    if (d >= min_dist[e.id]) { aq(Lx, Rx, Ly, Ry, d - min_dist[e.id]); }
    getMax(Lx, e.x - d), getMin(Rx, e.x + d);
    getMax(Ly, e.y - d), getMin(Ry, e.y + d);
    if (Lx > Rx || Ly > Ry) break;
  }
  printf("%d %d %d %d\n", Lx, Rx, Ly, Ry);
  if (Lx <= Rx && Ly <= Ry) return true; // 不走地铁

  sort(querys.begin(), querys.end(), Query::byX);

  unsigned pos = 0;
  int tot = 0;

  for (auto q : querys) {
    while (pos < stations.size() && stations[pos].x <= q.x) {
      int p = lower_bound(D.begin(), D.end(), stations[pos].y) - D.begin() + 1;
      xupd(p, 1);
      ++pos;
    }
    int l = q.ly, r = q.ry;
    l = lower_bound(D.begin(), D.end(), l) - D.begin();
    r = upper_bound(D.begin(), D.end(), r) - D.begin();
    tot += (xpre(r) - xpre(l)) * q.coef;
  }
  return tot > 0;
}

int main() {
  scanf("%d%d", &n, &m);
  FOR(i, 1, n) {
    int x, y;
    scanf("%d%d", &x, &y);
    points.pb({x, y, 0, i});
    rabbits.pb({x, y, 0, i});
  }
  FOR(i, 1, m) {
    int x, y;
    scanf("%d%d", &x, &y);
    points.pb({x, y, 1, i});
    stations.pb({x, y, 1, i});
  }

  work();
  for (auto &u : points) u.flipY();
  work();
  for (auto &u : points) u.flipX();
  work();
  for (auto &u : points) u.flipY();
  work();
  for (auto &u : points) u.flipX();

  // FOR(i, 1, n) printf("min_dist[%d] = %d\n", i, min_dist[i]);

  for (auto &u : rabbits) u.toChebyshev();
  for (auto &u : stations) u.toChebyshev();

  sort(rabbits.begin(), rabbits.end(), byMinDist);
  sort(stations.begin(), stations.end(), Point::byX);

  D.clear();
  for (auto u : stations) D.pb(u.y);
  sort(D.begin(), D.end());

  int l = 0, r = min_dist[rabbits[0].id];
  while (l < r) {
    int mid = (l + r) / 2;
    if (check(mid))
      r = mid;
    else
      l = mid + 1;
  }
  printf("%d\n", l);
  return 0;
}
