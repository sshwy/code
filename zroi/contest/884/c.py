
two_to_32 = 2 ** 32

m = 0

def main ():
    global m
    n, s = map(int, input().split())
    
    for i in range(n):
        di = (s >> 10) % 10
        s = (747796405 * s - 1403630843) % two_to_32
        m = m * 10 + di

    ans = 0
    x = 1
    while x <= m:
        x = x * 10 + 1
        ans += m // x

    print(ans)

main()
