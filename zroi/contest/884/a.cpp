// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

const int N = 20, ALP = 27, L = 1e5 + 5;

int n, trans[N][ALP];
long long f[1 << N][ALP];
char s[L];

int main() {
  scanf("%d", &n);
  FOR(i, 0, n - 1) {
    scanf("%s", s);
    int ls = strlen(s);
    // printf("ls %d\n", ls);
    FOR(c, 0, 26) trans[i][c] = c; // 26 : g
    FOR(j, 0, ls - 1) {
      int sj = s[j] - 'a';
      // printf("sj %d\n", sj);
      FOR(c, 0, 26) {
        if (trans[i][c] == 26)
          trans[i][c] = sj;
        else if (trans[i][c] == sj)
          trans[i][c] = 26;
      }
    }
    // FOR(c, 0, 26) printf("trans[%d, %d] = %d\n", i, c, trans[i][c]);
  }
  f[0][26] = 1;
  int lim = 1 << n;
  FOR(i, 0, lim - 1) {
    FOR(j, 0, n - 1) if (!(i >> j & 1)) {
      FOR(c, 0, 26) { f[i | (1 << j)][trans[j][c]] += f[i][c]; }
    }
  }
  long long ans = 0;
  FOR(c, 0, 25) ans += f[lim - 1][c];
  printf("%lld\n", ans);
  return 0;
}
