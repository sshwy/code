#include <bits/stdc++.h>
#define FOR(a, b, c) for (int a = (int)(b); a <= (int)(c); a++)

const int N = 2e5 + 5, K = 101;

int n, x[N], y[N];
std::pair<double, int> a[N];
double ans[N];

int main() {
  scanf("%d", &n);
  FOR(i, 1, n) scanf("%d%d", x + i, y + i);

  double all = 2 * M_PI;
  double coef = 0;

  FOR(j, 0, K - 1) {
    double theta = all / K * j;
    double s1 = 0, s2 = 0;

    coef += fabs(std::cos(theta + all / K / 2));

    FOR(i, 1, n) {
      a[i].first = x[i] * std::cos(theta) - y[i] * std::sin(theta);
      a[i].second = i;
      s2 += a[i].first;
    }
    sort(a + 1, a + n + 1);
    FOR(i, 1, n) {
      s2 -= a[i].first;
      ans[a[i].second] += a[i].first * (i - 1) - s1 + s2 - a[i].first * (n - i);
      s1 += a[i].first;
    }
  }

  FOR(i, 1, n) printf("%.4lf\n", ans[i] / coef);

  return 0;
}
