#include <bits/stdc++.h>
#define FOR(a, b, c) for (int a = (int)(b); a <= (int)(c); a++)
const int N = 5e5 + 5;

struct qxx {
  int nex, t;
} e[N * 2];
int h[N], le;
void ae(int u, int v) { e[++le] = (qxx){h[u], v}, h[u] = le; }
#define for_e(i, u, v) for (int i = h[u], v; v = e[i].t, i; i = e[i].nex)

int n, a[N], ans[N], f[N];
int g1[N], g2[N], g3[N];

void upd_g(int u, int v) {
  if (g1[u] == -1 || f[g1[u]] < f[v])
    g3[u] = g2[u], g2[u] = g1[u], g1[u] = v;
  else if (g2[u] == -1 || f[g2[u]] < f[v])
    g3[u] = g2[u], g2[u] = v;
  else if (g3[u] == -1 || f[g3[u]] < f[v])
    g3[u] = v;
}
void dfs0(int u, int p) {
  for_e(i, u, v) if (v != p) dfs0(v, u);

  for_e(i, u, v) if (v != p) { upd_g(u, v); }
  if (g2[u] == -1)
    f[u] = a[u];
  else
    f[u] = std::max(a[u], f[g2[u]]);
}
void dfs1(int u, int p) {
  ans[u] = a[u];
  for_e(i, u, v) { ans[u] = std::max(ans[u], f[v]); }

  for_e(i, u, v) if (v != p) {
    int fu = f[u], fv = f[v], g1v = g1[v], g2v = g2[v], g3v = g3[v];
    // u -> v
    if (v == g2[u] || v == g1[u]) {
      f[u] = a[u];
      if (g3[u] != -1) f[u] = std::max(f[u], f[g3[u]]);
    }
    upd_g(v, u);

    f[v] = a[v];
    if (g2[v] != -1) f[v] = std::max(f[v], f[g2[v]]);

    dfs1(v, u);

    f[u] = fu, f[v] = fv, g1[v] = g1v, g2[v] = g2v, g3[v] = g3v;
  }
}
int main() {
  scanf("%d", &n);
  FOR(i, 1, n) scanf("%d", a + i);
  FOR(i, 1, n - 1) {
    int u, v;
    scanf("%d%d", &u, &v);
    ae(u, v);
    ae(v, u);
  }
  dfs0(1, 0);
  dfs1(1, 0);
  FOR(i, 1, n) printf("%d%c", ans[i], " \n"[i == n]);
  return 0;
}
