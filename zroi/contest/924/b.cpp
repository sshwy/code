#include <bits/stdc++.h>
#define FOR(a, b, c) for (int a = (int)(b); a <= (int)(c); a++)
#define ROF(a, b, c) for (int a = (int)(b); a >= (int)(c); a--)
const int N = 2e5 + 5, SZ = N * 190;

namespace Treap {
  int sz[SZ], lc[SZ], rc[SZ], tot;
  long long s[SZ], atag[SZ];

  int new_node(long long val) {
    int u = ++tot;
    s[u] = val;
    atag[u] = 0;
    sz[u] = 1;
    return u;
  }

  int copy_node(int u) {
    int u2 = ++tot;
    s[u2] = s[u];
    sz[u2] = sz[u];
    lc[u2] = lc[u];
    rc[u2] = rc[u];
    atag[u2] = atag[u];
    return u2;
  }

  void pu(int u) { sz[u] = sz[lc[u]] + sz[rc[u]] + 1; }

  int treeAdd(int u, long long val) {
    int u2 = copy_node(u);
    s[u2] += val;
    atag[u2] += val;
    return u2;
  }

  void pd(int u) {
    if (atag[u]) {
      if (lc[u]) lc[u] = treeAdd(lc[u], atag[u]);
      if (rc[u]) rc[u] = treeAdd(rc[u], atag[u]);
      atag[u] = 0;
    }
  }

  void persistent_split(int u, int k, int &x, int &y) {
    if (!u) return x = y = 0, void();
    pd(u);
    if (sz[lc[u]] + 1 <= k) {
      x = copy_node(u);
      persistent_split(rc[u], k - sz[lc[u]] - 1, rc[x], y);
      pu(x);
    } else {
      y = copy_node(u);
      persistent_split(lc[u], k, x, lc[y]);
      pu(y);
    }
  }

  int merge(int x, int y) {
    if (!x || !y) return x + y;
    if (rand() % (sz[x] + sz[y]) < sz[x]) {
      return pd(x), rc[x] = merge(rc[x], y), pu(x), x;
    } else {
      return pd(y), lc[y] = merge(x, lc[y]), pu(y), y;
    }
  }

  long long eval(int u, int k) {
    if (!u) return LLONG_MIN;
    assert(k <= sz[u]);
    if (sz[lc[u]] + 1 == k) return s[u];
    if (k < sz[lc[u]] + 1)
      return atag[u] + eval(lc[u], k);
    else
      return atag[u] + eval(rc[u], k - sz[lc[u]] - 1);
  }

  void print(int u) {
    if (!u) return;
    pd(u);

    print(lc[u]);
    printf("%lld ", s[u]);
    print(rc[u]);
  }
} // namespace Treap

int n, q, a[N];
int f_rt[N];

long long f(int i, int j) {
  if (j == 0) return 0;
  return Treap::eval(f_rt[i], j + 1);
}

void work() {
  f_rt[n + 1] = Treap::new_node(0);
  f_rt[n] = Treap::merge(Treap::new_node(0), Treap::new_node(a[n]));
  ROF(i, n - 1, 1) {
    // printf("i = %d\n", i);

    int l = 0, r = Treap::sz[f_rt[i + 1]] - 1;
    while (l < r) {
      int mid = (l + r + 1) / 2;
      if (f(i + 1, mid) >= f(i + 2, mid - 1) + a[i])
        l = mid;
      else
        r = mid - 1;
    }
    // printf("l = %d\n", l);
    int x, y, x2, y2;
    Treap::persistent_split(f_rt[i + 1], l + 1, x, y);
    Treap::persistent_split(f_rt[i + 2], l, x2, y2);

    // printf("a[%d] = %d\n", i, a[i]);
    if (y2) y2 = Treap::treeAdd(y2, a[i]);
    f_rt[i] = Treap::merge(x, y2);

    // printf("i=%d, ", i); Treap::print(f_rt[i]); puts("");
  }
}

int main() {
  srand(114514);

  scanf("%d%d", &n, &q);
  FOR(i, 1, n) scanf("%d", a + i);

  work();

  // fprintf(stderr, "GG\n");

  long long las = 0;
  FOR(_, 1, q) {
    int i, c;
    scanf("%d%d", &i, &c);
    i = (i + las) % n + 1;
    c = (c + las) % n + 1;
    // printf("i = %d c = %d\n", i, c);
    printf("%lld%c", las = f(i, c), " \n"[_ == q]);
  }

  return 0;
}
