#include <bits/stdc++.h>
#define FOR(a, b, c) for (int a = (int)(b); a <= (int)(c); a++)
#define ROF(a, b, c) for (int a = (int)(b); a >= (int)(c); a--)
const int N = 5005;

int n, q;
int a[N];
long long f[N][N];

int main() {
  scanf("%d%d", &n, &q);
  if (n > N) return 0;

  FOR(i, 1, n) scanf("%d", a + i);

  memset(f, -0x3f, sizeof(f));
  f[n + 1][0] = f[n + 2][0] = 0;

  ROF(i, n, 1) {
    f[i][0] = 0;
    FOR(j, 1, n - i + 1) { f[i][j] = std::max(f[i + 1][j], f[i + 2][j - 1] + a[i]); }
  }

  long long las = 0;
  FOR(_, 1, q) {
    int i, c;
    scanf("%d%d", &i, &c);
    // i = (i + las) % n + 1;
    // c = (c + las) % n + 1;
    printf("%lld%c", las = f[i][c], " \n"[_ == q]);
  }

  return 0;
}
