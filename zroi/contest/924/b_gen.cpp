#include <bits/stdc++.h>
#define FOR(a, b, c) for (int a = (int)(b); a <= (int)(c); a++)
const int N = 5e5 + 5;

int rnd(int L, int R) { return rand() % (R - L + 1) + L; }

int main() {
  srand(clock() + time(0) * 1000);
  int n = rnd(1, 20), q = rnd(1, 100), w = 100;
  printf("%d %d\n", n, q);
  FOR(i, 1, n) printf("%d%c", rnd(1, w), " \n"[i == n]);
  FOR(_, 1, q) {
    int i = rnd(1, n);
    int c = rnd(1, (n - i + 2) / 2);
    printf("%d %d\n", i, c);
  }
  return 0;
}
