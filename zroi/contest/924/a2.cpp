#include <bits/stdc++.h>
#define FOR(a, b, c) for (int a = (int)(b); a <= (int)(c); a++)
const int N = 5e5 + 5;

struct qxx {
  int nex, t;
} e[N * 2];
int h[N], le;
void ae(int u, int v) { e[++le] = (qxx){h[u], v}, h[u] = le; }
#define for_e(i, u, v) for (int i = h[u], v; v = e[i].t, i; i = e[i].nex)

int n, a[N], ans[N], f[N];

void dfs(int u, int p) {
  for_e(i, u, v) if (v != p) dfs(v, u);
  int v1 = -1, v2 = -1;
  for_e(i, u, v) if (v != p) {
    if (v1 == -1 || f[v1] < f[v])
      v2 = v1, v1 = v;
    else if (v2 == -1 || f[v2] < f[v])
      v2 = v;
  }
  if (v2 == -1)
    f[u] = a[u];
  else
    f[u] = std::max(a[u], f[v2]);
}
int work(int root) {
  dfs(root, 0);
  int res = a[root];
  for_e(i, root, u) { res = std::max(res, f[u]); }
  return res;
}
int main() {
  scanf("%d", &n);
  FOR(i, 1, n) scanf("%d", a + i);
  FOR(i, 1, n - 1) {
    int u, v;
    scanf("%d%d", &u, &v);
    ae(u, v);
    ae(v, u);
  }
  FOR(i, 1, n) ans[i] = work(i);
  FOR(i, 1, n) printf("%d%c", ans[i], " \n"[i == n]);
  return 0;
}
