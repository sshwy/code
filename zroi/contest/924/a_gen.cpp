#include <bits/stdc++.h>
#define FOR(a, b, c) for (int a = (int)(b); a <= (int)(c); a++)
const int N = 5e5 + 5;

int rnd(int L, int R) { return rand() % (R - L + 1) + L; }

int main() {
  srand(clock() + time(0) * 1000);
  int n = rnd(2, 1000), w = 1000;
  printf("%d\n", n);
  FOR(i, 1, n) printf("%d%c", rnd(1, w), " \n"[i == n]);
  FOR(i, 2, n) printf("%d %d\n", rnd(1, i - 1), i);
  return 0;
}
