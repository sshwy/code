#include <bits/stdc++.h>
#define FOR(a, b, c) for (int a = (int)(b); a <= (int)(c); a++)
using namespace std;

const int N = 35;
int n, m;
long long a[N];
int A[N], B[N];

map<long long, int> mA, mB;

void work(int *c) {}
void solveA(int s1, int s2) {
  int x1 = s1;
  int c[N] = {0};
  FOR(i, 1, m) {
    int cur = x1 % 3;
    x1 /= 3;
    if (cur == 1) c[i]++;
    if (cur == 2) c[i]--;
  }
  x1 = s2;
  FOR(i, 1, m) {
    int cur = x1 % 3;
    x1 /= 3;
    if (cur == 1) c[i]--;
    if (cur == 2) c[i]++;
  }
  work(c);
}
void solveB(int s1, int s2) {
  int x1 = s1;
  int c[N] = {0};
  FOR(i, 1, n - m) {
    int cur = x1 % 3;
    x1 /= 3;
    if (cur == 1) c[i + m]++;
    if (cur == 2) c[i + m]--;
  }
  x1 = s2;
  FOR(i, 1, n - m) {
    int cur = x1 % 3;
    x1 /= 3;
    if (cur == 1) c[i + m]--;
    if (cur == 2) c[i + m]++;
  }
  work(c);
}
void solveAB(int s1, int s2) {
  int x1 = s1;
  int c[N] = {0};
  FOR(i, 1, m) {
    int cur = x1 % 3;
    x1 /= 3;
    if (cur == 1) c[i]++;
    if (cur == 2) c[i]--;
  }
  x1 = s2;
  FOR(i, 1, n - m) {
    int cur = x1 % 3;
    x1 /= 3;
    if (cur == 1) c[i + m]--;
    if (cur == 2) c[i + m]++;
  }
  work(c);
}
void calcA(int stat) {
  int x = stat;
  long long sum = 0;
  for (int i = 1; i <= m; i++) {
    int cur = x % 3;
    x /= 3;
    if (cur == 1)
      sum += a[i];
    else if (cur == 2)
      sum -= a[i];
  }
  if (mA.find(sum) != mA.end()) { solveA(mA[sum], stat); }
  mA[sum] = stat;
}
void calcB(int stat) {
  int x = stat;
  long long sum = 0;
  for (int i = 1; i <= n - m; i++) {
    int cur = x % 3;
    x /= 3;
    if (cur == 1)
      sum += a[i + m];
    else if (cur == 2)
      sum -= a[i + m];
  }
  if (mA.find(sum) != mA.end()) { solveAB(mA[sum], stat); }
  if (mB.find(sum) != mB.end()) { solveB(mB[sum], stat); }
  mB[sum] = stat;
}
int main() {
  scanf("%d", &n);
  FOR(i, 1, n) scanf("%lld", a + i);
  if (n == 1) go1();
  FOR(i, 1, m) A[i] = i;
  FOR(i, m + 1, n) B[i - m] = i;

  int lim = 1;
  FOR(i, 1, m) lim *= 3;
  FOR(j, 0, lim - 1) calcA(j);

  lim = 1;
  FOR(i, 1, n - m) lim *= 3;
  FOR(j, 0, lim - 1) calcB(j);

  puts("No");
  return 0;
}
