// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

const int N = 1e5 + 5, S = 10, K = 21, INF = 1e9;
int n, m, k, s;
int ans;
int c[N];
int totsq;
int fa[S];
vector<int> vd[N + N];
set<pair<int, int>> E;
int get(int u) { return fa[u] == u ? u : fa[u] = get(fa[u]); }

namespace SQ {
  struct atom {
    int _f[K];
    int pos[2];
    bool flag;
    void reset() { fill(_f, _f + K, INF); }
    atom() {
      reset();
      flag = false;
    }
    int &operator[](int c) { return _f[c]; }
    int mn() { return *min_element(_f + 1, _f + k + 1); }
    atom operator+(int v) {
      atom res = *this;
      FOR(i, 1, k) res[i] = min(res[i] + v, INF);
      return res;
    }
    void merge(atom g) { FOR(i, 1, k) _f[i] = min(_f[i] + g[i] - 1, INF); }
  } f[N + N], F[1 << S]; // f[u,c]: u 染 c 的最小代价
  void getMin(int &x, int y) { x = min(x, y); }
  void getMin(atom &x, atom y) { FOR(i, 1, k) getMin(x._f[i], y._f[i]); }
  int Fmin[1 << S]; // 所有c中的最小值
  struct qxx {
    int nex, t;
  } e[N * 2 * 2];
  int le = 1, h[N + N];
  void ae(int f, int t) { e[++le] = (qxx){h[f], t}, h[f] = le; }
  void ab(int u, int v) { ae(u, v), ae(v, u); }
  void init(int su, int *a, int la) { vd[su] = vector<int>(a, a + la); }

  bool isSquare(int u) { return u > n; }
  bool hasEdge(int u, int v) {
    if (u > v) swap(u, v);
    return E.find(make_pair(u, v)) != E.end();
  }
  int initG(int u, int p) {
    int res = -1;
    for (unsigned i = 0; i < vd[u].size(); i++) res = vd[u][i] == p ? i : res;
    return res;
  }
  void initF(int u) {
    assert(u <= n);
    f[u].reset();
    if (c[u]) {
      f[u][c[u]] = 1;
    } else {
      FOR(i, 1, k) f[u][i] = 1;
    }
  }
  void dfs(int, int);
  void work(int u, int p) {
    for (int i = h[u]; i; i = e[i].nex) {
      int v = e[i].t;
      if (v == p) continue;
      dfs(v, u);
    }
    int idOfp = initG(u, p);
    if (p) initF(p);
    int tot = vd[u].size();

    bool isCon[1 << S] = {0};
    memset(Fmin, 0x3f, sizeof(Fmin));
    FOR(i, 0, (1 << tot) - 1) F[i].reset();

    FOR(i, 1, (1 << tot) - 1) {
      bool flag = true;
      int cur = -1;
      FOR(j, 0, tot - 1) fa[j] = j;
      FOR(j, 0, tot - 1)
      if (i >> j & 1) FOR(k, j + 1, tot - 1) if (i >> k & 1) {
          if (get(j) != get(k) && hasEdge(vd[u][j], vd[u][k])) {
            fa[get(j)] = get(k);
          }
        }
      FOR(j, 0, tot - 1) if (i >> j & 1) {
        if (cur == -1)
          cur = get(j);
        else if (cur != get(j)) {
          flag = false;
          break;
        }
      }
      isCon[i] = flag;
    }
    FOR(i, 0, tot - 1) { F[1 << i] = f[vd[u][i]]; }
    FOR(i, 1, (1 << tot) - 1) if ((i != (i & -i)) && isCon[i]) {
      FOR(j, 0, tot - 1) if ((i >> j & 1) && isCon[i - (1 << j)]) {
        F[i] = F[i - (1 << j)];
        F[i].merge(f[vd[u][j]]); // F[i]: i里的全染同一个颜色的最小值
        break;
      }
    }
    FOR(i, 1, (1 << tot) - 1) if (isCon[i]) { Fmin[i] = F[i].mn(); }
    FOR(i, 1, (1 << tot) - 1) {
      for (int j = (i - 1) & i; j; j = (j - 1) & i)
        if (isCon[j]) {
          if (Fmin[i] > Fmin[i - j] + Fmin[j]) { Fmin[i] = Fmin[i - j] + Fmin[j]; }
        }
    }
    if (idOfp != -1) {
      f[p].reset();
      FOR(i, 1, (1 << tot) - 1) if (i >> idOfp & 1) {
        getMin(f[p], F[i] + Fmin[(1 << tot) - 1 - i]);
        if (i == (1 << tot) - 1) { getMin(f[p], F[i]); }
      }
    } else {
      ans = Fmin[(1 << tot) - 1];
    }
  }
  void dfs(int u, int p) {
    if (isSquare(u)) {
      work(u, p);
    } else {
      initF(u);
      atom Ftmp = f[u];
      for (int i = h[u]; i; i = e[i].nex) {
        int v = e[i].t;
        if (v == p) continue;
        assert(isSquare(v));
        dfs(v, u);
        Ftmp.merge(f[u]);
      }
      f[u] = Ftmp;
    }
  }
  int main() {
    dfs(n + totsq, 0);
    return 0;
  }
} // namespace SQ

struct qxx {
  int nex, t;
} e[N * S * 2];
int le = 1, h[N];
void ae(int f, int t) { e[++le] = (qxx){h[f], t}, h[f] = le; }

int dfn[N], totdfn, low[N], st[N], tp;
int ttt[N], lt;
void tarjan(int u, int p) {
  st[++tp] = u, dfn[u] = low[u] = ++totdfn;
  for (int i = h[u]; i; i = e[i].nex) {
    int v = e[i].t;
    if (v == p) continue;
    if (!dfn[v]) {
      tarjan(v, u), low[u] = min(low[u], low[v]);
      if (dfn[u] <= low[v]) {
        ++totsq, lt = 0;
        do SQ::ab(n + totsq, st[tp]), ttt[lt] = st[tp], ++lt;
        while (st[tp--] != v);
        SQ::ab(n + totsq, u), ttt[lt] = u, ++lt;
        SQ::init(n + totsq, ttt, lt);
      }
    } else
      low[u] = min(low[u], dfn[v]);
  }
}

int main() {
  scanf("%d%d%d%d", &n, &m, &k, &s);
  FOR(i, 1, n) scanf("%d", c + i);
  FOR(i, 1, m) {
    int x, y;
    scanf("%d%d", &x, &y);
    ae(x, y), ae(y, x);
    if (x > y) swap(x, y);
    E.insert(make_pair(x, y));
  }
  tarjan(1, 0); // 点双
  SQ::main();
  printf("%d\n", ans);
  return 0;
}
