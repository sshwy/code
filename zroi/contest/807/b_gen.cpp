// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

using namespace RA;

set<pair<int, int>> E;
vector<pair<int, int>> V;
int main() {
  srand(clock() + time(0));
  int s = rnd(1, 6);
  int n = rnd(2, 8), m = rnd(n - 1, n * s), k = 2;
  FOR(i, 1, n) FOR(j, i + 1, n) E.insert({i, j});
  printf("%d %d %d %d\n", n, m, k, s);
  FOR(i, 1, n) { printf("%d%c", rnd(100) < 50 ? 0 : rnd(1, k), " \n"[i == n]); }
  FOR(i, 1, n - 1) {
    int x = rnd(1, i), y = i + 1;
    printf("%d %d\n", x, y);
    E.erase({x, y});
  }
  for (auto e : E) V.push_back(e);
  random_shuffle(V.begin(), V.end());
  FOR(i, n, m) { printf("%d %d\n", V[i - n].first, V[i - n].second); }
  return 0;
}
