// by Yao
// s=2
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

const int N = 1e5 + 5, S = 6, K = 21, INF = 1e9;

struct qxx {
  int nex, t;
} e[N * S * 2];
int le = 1, h[N];
void ae(int f, int t) { e[++le] = (qxx){h[f], t}, h[f] = le; }

int n, m, k, s;
int c[N];
struct atom {
  int _f[K];
  int pos[2];
  atom() { fill(_f, _f + K, INF); }
  void setPos() {
    pos[0] = pos[1] = -1;
    FOR(i, 1, k) {
      if (pos[0] == -1 || _f[pos[0]] > _f[i]) {
        pos[1] = pos[0];
        pos[0] = i;
      } else if (pos[1] == -1 || _f[pos[1]] > _f[i]) {
        pos[1] = i;
      }
    }
  }
  int &operator[](int c) { return _f[c]; }
  int no(int c) {
    if (c == pos[0]) return _f[pos[1]];
    return _f[pos[0]];
  }
} f[N]; // f[u,c]: u 染 c 的最小代价

vector<int> A, B;
int cur_c;
void calc(int u, int c) {
  long long cur = 1;
  for (unsigned int i = 0; i < A.size(); i++) {
    B[i] = f[A[i]][c] - f[A[i]].no(c);
    cur += f[A[i]].no(c);
  }
  sort(B.begin(), B.end());
  for (unsigned int i = 0; i < B.size(); i++) {
    if (cur + B[i] - 1 <= cur)
      cur += B[i] - 1;
    else
      break;
  }
  assert(cur < INT_MAX);
  f[u][c] = (int)cur;
}
void dfs(int u, int p) {
  for (int i = h[u]; i; i = e[i].nex) {
    int v = e[i].t;
    if (v == p) continue;
    dfs(v, u);
  }
  A.clear();
  for (int i = h[u]; i; i = e[i].nex) {
    int v = e[i].t;
    if (v == p) continue;
    A.push_back(v);
    f[v].setPos();
  }
  B.resize(A.size());
  if (c[u])
    calc(u, c[u]);
  else
    FOR(i, 1, k) calc(u, i);
}

int main() {
  scanf("%d%d%d%d", &n, &m, &k, &s);
  FOR(i, 1, n) scanf("%d", c + i);
  FOR(i, 1, m) {
    int x, y;
    scanf("%d%d", &x, &y);
    ae(x, y), ae(y, x);
  }
  dfs(1, 0);
  int ans = *min_element(f[1]._f + 1, f[1]._f + k + 1);
  printf("%d\n", ans);
  return 0;
}
