// by Yao
// 2^n
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

const int N = 1e5 + 5, S = 6, K = 21, INF = 1e9;

int n, m, k, s;
int c[N], a[N];
int x[N], y[N], fa[N];
int ans = INF;

int get(int u) { return fa[u] == u ? u : fa[u] = get(fa[u]); }
void merge(int x, int y) { fa[get(x)] = get(y); }

void checkAns() {
  FOR(i, 1, n) fa[i] = i;
  FOR(i, 1, m) if (a[x[i]] == a[y[i]]) merge(x[i], y[i]);
  int cnt = 0;
  FOR(i, 1, n) if (get(i) == i)++ cnt;
  ans = min(ans, cnt);
}
void dfs(int cur) {
  if (cur == n + 1) {
    checkAns();
    return;
  }
  if (c[cur]) {
    a[cur] = c[cur];
    dfs(cur + 1);
  } else {
    FOR(i, 1, k) {
      a[cur] = i;
      dfs(cur + 1);
    }
  }
}
int main() {
  scanf("%d%d%d%d", &n, &m, &k, &s);
  FOR(i, 1, n) scanf("%d", c + i);
  FOR(i, 1, m) scanf("%d%d", x + i, y + i);
  dfs(1);
  printf("%d\n", ans);
  return 0;
}
