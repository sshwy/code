#include<bits/stdc++.h>
#define FOR(a, b, c) for(int a = (int)(b); a <= (int)(c); a++)

const int N = 1e5 + 5;
int n, q, a[N];
char c_s[N];
std::string s;

long long Query(int l, int r) {
  long long res = 0;
  FOR(i, l, r) {
    if(i + a[i] - 1 > n) continue;
    FOR(j, l, r) {
      if(j + a[i] - 1 > n) break;
      if(s.substr(i - 1, a[i]) == s.substr(j - 1, a[i])) ++res;
    }
  }
  return res;
}

int main() {
  scanf("%d%d", &n, &q);
  scanf("%s", c_s);
  s = c_s;
  FOR(i, 1, n) scanf("%d", a + i);
  FOR(i, 1, q) {
    int l, r;
    scanf("%d%d", &l, &r);
    printf("%lld\n", Query(l, r));
  }
  return 0;
}
