#include<bits/stdc++.h>
#define FOR(a, b, c) for(int a = (int)(b); a <= (int)(c); a++)
#define ROF(a, b, c) for(int a = (int)(b); a >= (int)(c); a--)

int rnd(int L, int R) {
  return rand() % (R - L + 1) + L;
}
int main() {
  srand(clock() + time(0) * 1000);

  int n = rnd(1, 100), q = 1000;
  int lim = rnd('a', 'z');

  n = q = 1e5, lim = 'b';

  printf("%d %d\n", n, q);

  FOR(i, 1, n) printf("%c", rnd('a', lim));
  puts("");

  FOR(i, 1, n) printf("%d%c", rnd(1, n), " \n"[i == n]);

  FOR(i, 1, q) {
    int l = rnd(1, n), r = rnd(1, n);
    if(l > r) std::swap(l, r);
    printf("%d %d\n", l, r);
  }
  
  return 0;
}
