#include "guess.h"
#include <bits/stdc++.h>
#define FOR(a, b, c) for(int a = (int)(b); a <= (int)(c); a++)
using namespace std;

const int N = 1e5 + 5;

int p[N];

int getNoneZeroPos(vector<int> S, vector<int> T) {
  if(T.size() == 1) return T[0];

  int mid = T.size() / 2;
  vector<int> T1, T2;
  FOR(i, 0, mid - 1) T1.push_back(T[i]);
  FOR(i, mid, T.size() - 1) T2.push_back(T[i]);

  if(ask1(S, T1) != 0) return getNoneZeroPos(S, T1);
  return getNoneZeroPos(S, T2);
}

int getNoneZero(int n) {
  FOR(i, 0, n - 1) p[i] = i;

  do {
    random_shuffle(p, p + n);
    // printf("%d\n", p[0]);
    vector<int> S, T;
    int mid = n / 2 - 1;
    if(mid % 2) {
      if(mid > 0) --mid;
      else ++mid;
    }
    assert(mid + 1 < n);
    FOR(i, 0, mid) S.push_back(p[i]);
    FOR(i, mid + 1, n - 1) T.push_back(p[i]);
    if(ask1(S, T) != 0) return getNoneZeroPos(S, T);
  } while(true);

  return 0;
}

vector<int> solve(int n, int P, int Q) {
  srand(clock() + time(0) * 1000);

  int pos = getNoneZero(n);
  vector<int> a(n, 0);

  FOR(i, 0, n - 1) if(i != pos) {
    vector<int> S(1, i), T(1, pos);
    a[i] = ask1(S, T);
  }

  a[pos] = 1; // assume

  if(ask2(a)) return a;
  FOR(i, 0, n - 1) a[i] = -a[i];

  return a;
}
