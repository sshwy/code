#pragma GCC optimize(2)
#pragma GCC optimize(3)
#pragma GCC optimize("Ofast")
#pragma GCC optimize("inline")
#pragma GCC optimize("-fgcse")
#pragma GCC optimize("-fgcse-lm")
#pragma GCC optimize("-fipa-sra")
#pragma GCC optimize("-ftree-pre")
#pragma GCC optimize("-ftree-vrp")
#pragma GCC optimize("-fpeephole2")
#pragma GCC optimize("-ffast-math")
#pragma GCC optimize("-fsched-spec")
#pragma GCC optimize("unroll-loops")
#pragma GCC optimize("-falign-jumps")
#pragma GCC optimize("-falign-loops")
#pragma GCC optimize("-falign-labels")
#pragma GCC optimize("-fdevirtualize")
#pragma GCC optimize("-fcaller-saves")
#pragma GCC optimize("-fcrossjumping")
#pragma GCC optimize("-fthread-jumps")
#pragma GCC optimize("-funroll-loops")
#pragma GCC optimize("-fwhole-program")
#pragma GCC optimize("-freorder-blocks")
#pragma GCC optimize("-fschedule-insns")
#pragma GCC optimize("inline-functions")
#pragma GCC optimize("-ftree-tail-merge")
#pragma GCC optimize("-fschedule-insns2")
#pragma GCC optimize("-fstrict-aliasing")
#pragma GCC optimize("-fstrict-overflow")
#pragma GCC optimize("-falign-functions")
#pragma GCC optimize("-fcse-skip-blocks")
#pragma GCC optimize("-fcse-follow-jumps")
#pragma GCC optimize("-fsched-interblock")
#pragma GCC optimize("-fpartial-inlining")
#pragma GCC optimize("no-stack-protector")
#pragma GCC optimize("-freorder-functions")
#pragma GCC optimize("-findirect-inlining")
#pragma GCC optimize("-fhoist-adjacent-loads")
#pragma GCC optimize("-frerun-cse-after-loop")
#pragma GCC optimize("inline-small-functions")
#pragma GCC optimize("-finline-small-functions")
#pragma GCC optimize("-ftree-switch-conversion")
#pragma GCC optimize("-foptimize-sibling-calls")
#pragma GCC optimize("-fexpensive-optimizations")
#pragma GCC optimize("-funsafe-loop-optimizations")
#pragma GCC optimize("inline-functions-called-once")
#pragma GCC optimize("-fdelete-null-pointer-checks")

#include<bits/stdc++.h>
#define FOR(a, b, c) for(int a = (int)(b); a <= (int)(c); a++)
#define ROF(a, b, c) for(int a = (int)(b); a >= (int)(c); a--)

const int N = 1e5 + 5, T = 330, Q = 1e5 + 5;

// long long XXX;
int n, q, a[N];
long long delta[N], ans[N];

namespace DS {
  int c[N];
  void add(int pos, int val) {
    for(int i = pos; i < N; i += i & -i) c[i] += val;
  }
  int pre(int pos) {
    int res = 0;
    for(int i = pos; i > 0; i -= i & -i) res += c[i];
    return res;
  }
  void insert(int x) {
    assert(x < N);
    add(x, 1);
  }
  void erase(int x) {
    assert(x < N);
    add(x, -1);
  }
  int count(int l, int r) {
    if(l > r) return 0;
    return pre(r) - pre(l - 1);
  }
}

namespace DataSet {
  const int SZ = N * 4;
  struct Atom {
    int q, coef, id;
    void work(int p) {
      if(p < q) {
        delta[id] += DS::count(p + 1, q) * coef;
      } else {
        delta[id] += DS::count(q, p - 1) * coef;
      }
    }
  };
  std::vector<Atom> s[SZ];
  void addData(int L, int R, Atom o, int u, int l, int r) {
    if(L <= l && r <= R) {
      s[u].push_back(o);
      return;
    }
    int mid = (l + r) >> 1;
    if(L <= mid) addData(L, R, o, u << 1, l, mid);
    if(mid < R) addData(L, R, o, u << 1 | 1, mid + 1, r);
  }
  void workPos(int pos, int u, int l, int r) {
    for(Atom o : s[u]) o.work(pos);
    if(l == r) return;
    int mid = (l + r) >> 1;
    if(pos <= mid) workPos(pos, u << 1, l, mid);
    else workPos(pos, u << 1 | 1, mid + 1, r);
  }
}

void workQuery1(int x) {
  DataSet::workPos(x, 1, 1, n);
}

void workQuery2(int x) {
  DataSet::workPos(x, 1, 1, n);
}

namespace SAM {
  const int SZ = N * 2, ALP = 26;
  int tr[SZ][ALP], fail[SZ], len[SZ], label[SZ], tot = 1, las = 1;
  std::vector<int> link[SZ];
  void extend(char c, int index) {
    c -= 'a';
    int u = ++tot, p = las;
    len[u] = len[las] + 1;
    label[u] = index;
    while(p && tr[p][c] == 0) tr[p][c] = u, p = fail[p];
    if(!p) fail[u] = 1;
    else {
      int q = tr[p][c];
      if(len[q] == len[p] + 1) fail[u] = q;
      else {
        int cq = ++tot;
        memcpy(tr[cq], tr[q], sizeof(tr[q]));
        len[cq] = len[p] + 1;
        fail[cq] = fail[q], fail[u] = fail[q] = cq;
        while(p && tr[p][c] == q) tr[p][c] = cq, p = fail[p];
      }
    }
    las = u;
  }
  std::vector<int> g[SZ];
  int sz[SZ], big[SZ];
  void work() {
    FOR(i, 1, tot) if(fail[i]) {
      g[fail[i]].push_back(i);
    }
  }
  int nd[SZ];

  int getLink(int L, int deep) {
    if(len[nd[deep]] < L) return -1;

    int l = 0, r = deep;
    while(l < r) {
      int mid = (l + r) >> 1;
      if(len[nd[mid]] >= L) r = mid;
      else l = mid + 1;
    }

    return nd[l];
  }
  void dfs1(int u, int deep) {
    nd[deep] = u;
    if(label[u]) {
      int link_u = getLink(a[label[u]], deep);
      if(link_u != -1) {
        link[link_u].push_back(label[u]);
      } else {
        // do nothing
      }
    }

    for(int v : g[u]) {
      dfs1(v, deep + 1);
      sz[u] += sz[v];
      if(!big[u] || sz[v] > sz[big[u]]) big[u] = v;
    }
    sz[u]++;
  }
  void dfs2(int u) {
    if(link[u].size()) {
      for(int x : link[u]) DS::insert(x);
    }

    if(label[u]) workQuery1(label[u]);
    for(int v : g[u]) dfs2(v);

    if(link[u].size()) {
      for(int x : link[u]) DS::erase(x);
    }
  }
  void dfsAdd(int u) {
    if(label[u]) DS::insert(label[u]);
    for(int v : g[u]) dfsAdd(v);
  }
  void dfsSub(int u) {
    if(label[u]) DS::erase(label[u]);
    for(int v : g[u]) dfsSub(v);
  }
  void dfs3(int u, bool clear) {
    for(int v : g[u]) if(v != big[u]) dfs3(v, true);

    if(big[u]) dfs3(big[u], false);
    for(int v : g[u]) if(v != big[u]) dfsAdd(v);

    if(label[u]) DS::insert(label[u]);
    if(link[u].size()) for(int x : link[u]) workQuery2(x);

    if(!clear) return;

    dfsSub(u);
  }
}

char s[N];


inline int blockOf(int x) {
  return x / T;
}
struct Query {
  int l, r, id;
  static bool cmp(Query a, Query b) {
    if(blockOf(a.l) == blockOf(b.l)) {
      return blockOf(a.l) % 2 ? a.r < b.r : a.r > b.r;
    }
    return a.l < b.l;
  }
} queries[N];

int cl = 1, cr = 0;

void moveR(int r, int id) {
  if(cr < r) DataSet::addData(cr + 1, r, {cl, 1, id}, 1, 1, n);
  if(cr > r) DataSet::addData(r + 1, cr, {cl, -1, id}, 1, 1, n);

  cr = r;
}
void moveL(int l, int id) {
  if(cl < l) DataSet::addData(cl, l - 1, {cr, -1, id}, 1, 1, n);
  if(cl > l) DataSet::addData(l, cl - 1, {cr, 1, id}, 1, 1, n);

  cl = l;
}
void moAlgo() {
  FOR(i, 1, q) {
    int l = queries[i].l, r = queries[i].r;
    if(cr < r) moveR(r, i);
    if(cl > l) moveL(l, i);
    if(cr > r) moveR(r, i);
    if(cl < l) moveL(l, i);
  }
}

int b[N];

int main() {
  scanf("%d%d", &n, &q);
  scanf("%s", s + 1);

  FOR(i, 1, n) scanf("%d", a + i);

  FOR(i, 1, n) b[i] = (a[i] <= n - i + 1);
  FOR(i, 1, n) b[i] += b[i - 1];

  FOR(i, 1, q) {
    int l, r;
    scanf("%d%d", &l, &r);
    queries[i] = {l, r, i};
    ans[i] = b[r] - b[l - 1];
  }
  std::sort(queries + 1, queries + q + 1, Query::cmp);

  moAlgo();

  ROF(i, n, 1) SAM::extend(s[i], i);
  SAM::work();
  SAM::dfs1(1, 0);
  SAM::dfs2(1);
  SAM::dfs3(1, false);

  FOR(i, 1, q) delta[i] += delta[i - 1];
  FOR(i, 1, q) {
    int id = queries[i].id;
    ans[id] += delta[i];
  }
  FOR(i, 1, q) {
    printf("%lld\n", ans[i]);
  }

  // fprintf(stderr, "XXX = %lld\n", XXX);
  return 0;
}
