#include <bits/stdc++.h>
#define FOR(a, b, c) for (int a = (int)(b); a <= (int)(c); a++)
#define ROF(a, b, c) for (int a = (int)(b); a >= (int)(c); a--)

typedef long long LL;

LL mul(LL a, LL b, LL p) {
  if (p <= 1000000000ll) return 1ll * a * b % p;
  if (p <= 1000000000000ll)
    return (((a * (b >> 20) % p) << 20) % p + a * (b & ((1 << 20) - 1))) % p;
  LL d = floor(a * (long double)b / p);
  LL res = (a * b - d * p) % p;
  if (res < 0) res += p;
  return res;
}
inline LL pw(LL a, LL b, LL mod) {
  LL res = 1;
  for (; b; b >>= 1, a = mul(a, a, mod))
    if (b & 1) res = mul(res, a, mod);
  return res;
}

const int N = 1e5 + 5;

int n;
int pos[N], perm[N];
LL X[N], s[N], ak[N];
LL A, B, P;

LL calc(int pos, LL x) { return (mul(ak[pos], x, P) + s[pos]) % P; }
bool check(LL x) {
  const int times = 30;
  FOR(i, 1, times) {
    int pos = rand() % n + 1;
    if (calc(pos, x) % pos != X[pos]) return false;
  }
  return true;
}
void work() {
  for (LL c_n = X[n]; c_n < P; c_n += n) {
    int x = mul((c_n - s[n] + P) % P, pw(ak[n], P - 2, P), P);
    if (check(x)) {
      printf("%d\n", x);
      exit(0);
    }
  }
}

int main() {
  scanf("%d", &n);
  FOR(i, 1, n) {
    scanf("%d", perm + i);
    pos[perm[i]] = i;
  }
  scanf("%lld%lld%lld", &A, &B, &P);

  ROF(i, n, 1) {
    int t = pos[i]; // x[i] % i + 1 == t
    X[i] = t - 1;
    std::swap(pos[i], pos[perm[i]]), std::swap(perm[t], perm[i]);
  }

  s[1] = B;
  ak[1] = A;
  FOR(i, 2, n) {
    s[i] = (mul(s[i - 1], A, P) + B) % P;
    ak[i] = mul(ak[i - 1], A, P);
  }

  // FOR(i, 1, n) printf("%d%c", X[i], " \n"[i == n]);

  work();

  return 0;
}
