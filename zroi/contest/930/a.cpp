#include <bits/stdc++.h>
#define FOR(a, b, c) for (int a = (int)(b); a <= (int)(c); a++)

const int N = 1e5 + 5, P = 998244353;

int n;

struct Complex {
  int x, y;
  void read() { scanf("%d%d", &x, &y); }
  Complex operator*(const Complex o) const {
    Complex res;
    res.x = (1ll * x * o.x % P - 1ll * y * o.y % P + P) % P;
    res.y = (1ll * y * o.x % P + 1ll * x * o.y % P) % P;
    return res;
  }
  Complex operator-(const Complex o) const {
    Complex res;
    res.x = (x - o.x + P) % P;
    res.y = (y - o.y + P) % P;
    return res;
  }
  int getSqLen() { return (1ll * x * x % P + 1ll * y * y % P) % P; }
} a[N];

int main() {
  scanf("%d", &n);
  FOR(i, 1, n) a[i].read();
  FOR(i, 1, n) {
    Complex c = {1, 0};
    FOR(j, 1, n) if (j != i) c = c * (a[j] - a[i]);
    printf("%d\n", c.getSqLen());
  }
  return 0;
}
