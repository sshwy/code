#include <iostream>
using namespace std;

long long x, a, b, p;

int n, perm[100005];
long long Rand() {
  x = ((__int128)x * a + b) % p;
  return x;
}
int main() {
  n = 1e5;
  // cin >> n;
  cin >> x >> a >> b >> p;
  for (int i = 1; i <= n; i++) { // random shuffle [1, 2,..., n]
    perm[i] = i;
    swap(perm[i], perm[Rand() % i + 1]);
  }
  cout << n << endl;
  for (int i = 1; i <= n; i++) { // print the result
    cout << perm[i] << (i == n ? '\n' : ' ');
  }
  cout << a << " " << b << " " << p << endl;
}
