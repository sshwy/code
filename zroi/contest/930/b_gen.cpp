#include <bits/stdc++.h>
#define FOR(a, b, c) for (int a = (int)(b); a <= (int)(c); a++)

int rnd(int L, int R) { return rand() % (R - L + 1) + L; }

int main() {
  srand(clock() + time(0) * 1000);
  int n = rnd(1, 30), q = 5000;
  n = q = 2e6;
  printf("%d\n", n);
  FOR(i, 1, n) printf("%d%c", rnd(1, n), " \n"[i == n]);
  printf("%d\n", q);
  FOR(i, 1, q) {
    int l = rnd(1, n), r = rnd(1, n);
    if (l > r) std::swap(l, r);
    printf("%d %d\n", l, r);
  }
  return 0;
}
