#include <bits/stdc++.h>
#define FOR(a, b, c) for (int a = (int)(b); a <= (int)(c); a++)
#define ROF(a, b, c) for (int a = (int)(b); a >= (int)(c); a--)

namespace IO { // require C++11{{{
  const int _BS = 200000;
  char _ob[_BS], *_z = _ob, _ib[_BS], *_x = _ib, *_y = _ib;
  // Input
  char _nc() {
    if (_x == _y) _y = (_x = _ib) + fread(_ib, 1, _BS, stdin);
    if (_x == _y) return EOF;
    return *_x++;
    // return _x==_y&&(_y=(_x=_ib)+fread(_ib,1,_BS,stdin),_x==_y)?EOF:*_x++;
  }
  inline void rd(char &c) { c = _nc(); }
  void rd(char *s) {
    char c = _nc();
    while (!isblank(c) && isprint(c) && c != EOF) *s++ = c, c = _nc();
    *s = 0;
  }
  template <class T> void rd(T &res) { // unsigned/signed int/long long/__int128
    res = 0;
    char c = _nc(), flag = 0;
    while (!isdigit(c) && c != '-') c = _nc();
    if (c == '-') flag = 1, c = _nc();
    while (isdigit(c)) res = res * 10 + c - '0', c = _nc();
    if (flag) res = -res;
  }
  template <class T, class... A> void rd(T &hd, A &...rs) { rd(hd), rd(rs...); }
  // Output
  inline void flush() { fwrite(_ob, 1, _z - _ob, stdout), _z = _ob; }
  inline void wrch(char x) {
    if (_z == _ob + _BS - 1) flush();
    *_z = x, ++_z;
  }
  void wr(char *s) {
    while (*s) wrch(*s), ++s;
  }
  void wr(const char *s) {
    while (*s) wrch(*s), ++s;
  }
  template <class T> void wr(T x) { // unsigned/signed int/long long/__int128
    if (x == 0) return wrch('0'), void();
    if (x < 0) wrch('-'), x = -x;
    int _t[50], _lt = 0;
    while (x) _t[++_lt] = x % 10, x /= 10;
    while (_lt > 0) wrch(_t[_lt] + '0'), --_lt;
  }
  inline void wr(char x) { wrch(x); }
  template <class T, class... A> void wr(T hd, A... rs) { wr(hd), wr(rs...); }
  template <class... A> void wrln(A... ar) { wr(ar...), wrch('\n'); }
  struct _Flusher {
    ~_Flusher() { flush(); }
  } _flusher;
} // namespace IO
using IO::rd;
using IO::wrln;

const int N = 2e6 + 5;

int n, q, b[N], las[N], ans[N];

struct Query {
  int l, id;
};
std::vector<Query> queries[N];

namespace Fenwick {
  int c[N];
  void add(int pos, int val) {
    for (int i = pos; i < N; i += i & -i) c[i] += val;
  }
  void rangeAdd(int L, int R, int val) { add(L, val), add(R + 1, -val); }
  int get(int pos) {
    int res = 0;
    for (int i = pos; i > 0; i -= i & -i) res += c[i];
    return res;
  }
  int lowerBound(int val) {
    int index = 0;
    ROF(j, 20, 0) {
      int step = 1 << j;
      if (index + step < N && c[index + step] > val) index += step, val -= c[index];
    }
    return index + 1;
  }
} // namespace Fenwick

namespace SegmentTree {
  const int SZ = N * 4;
  int c[SZ], tag[SZ];
  void _rangeAdd(int L, int R, int val, int u, int l, int r) {
    if (L <= l && r <= R) {
      c[u] += val;
      tag[u] += val;
      return;
    }
    int mid = (l + r) >> 1;
    if (L <= mid) _rangeAdd(L, R, val, u << 1, l, mid);
    if (mid < R) _rangeAdd(L, R, val, u << 1 | 1, mid + 1, r);
    c[u] = std::min(c[u << 1], c[u << 1 | 1]) + tag[u];
  }
  void rangeAdd(int L, int R, int val) {
    // printf("rangeAdd %d %d %d\n", L, R, val);
    // FOR(i, L, R) c[i] += val;
    _rangeAdd(L, R, val, 1, 1, n);
  }
  int _getRangeMin(int L, int R, int u, int l, int r) {
    if (L <= l && r <= R) return c[u];
    int mid = (l + r) >> 1, res = INT_MAX;
    if (L <= mid) res = std::min(res, _getRangeMin(L, R, u << 1, l, mid));
    if (mid < R) res = std::min(res, _getRangeMin(L, R, u << 1 | 1, mid + 1, r));
    return res + tag[u];
  }
  int getRangeMin(int L, int R) {
    return _getRangeMin(L, R, 1, 1, n);
    // printf("getRangeMin %d %d\n", L, R);
    // int res = INT_MAX;
    // FOR(i, L, R) res = std::min(res, c[i]);
    // return res;
  }
} // namespace SegmentTree

std::pair<int, int> stk[N]; // first: id, second: g
int tp;

int stackLowerBound(int val) {
  // printf("stackLowerBound %d\n", val);
  // FOR(i, 1, tp) printf("(%d, %d)%c", stk[i].first, stk[i].second, " \n"[i == tp]);
  int l = 1, r = tp;
  while (l < r) {
    int mid = (l + r) / 2;
    if (stk[mid].second < val)
      l = mid + 1;
    else
      r = mid;
  }
  if (stk[l].second < val) return l + 1;
  return l;
}

void work() {
  FOR(i, 1, n) {
    // printf("i=%d\n", i);
    Fenwick::rangeAdd(b[i] + 1, i, 1);

    if (i > 1) {
      while (tp && stk[tp].second > b[i]) {
        int l = tp > 1 ? stk[tp - 1].first + 1 : 1;
        SegmentTree::rangeAdd(l, stk[tp].first, stk[tp].second - b[i]);
        --tp;
      }
      SegmentTree::rangeAdd(i - 1, i - 1, i - b[i]);
      stk[++tp] = {i - 1, b[i]};
    }

    for (unsigned j = 0; j < queries[i].size(); j++) {
      Query o = queries[i][j];
      int x = Fenwick::get(o.l);
      int l_max = Fenwick::lowerBound(x - 1) - 1;
      assert(l_max >= o.l);

      ans[o.id] = i - l_max + 1;
      // printf("ans[%d] = %d\n", o.id, ans[o.id]);

      int tl = stackLowerBound(o.l);
      int tr = stackLowerBound(l_max + 1);
      if (tr <= tp) {
        int pr = tr > 1 ? stk[tr - 1].first + 1 : 1;
        ans[o.id] = std::min(ans[o.id], pr - l_max + 1);
      }
      --tr;
      // printf("tl = %d, tr = %d\n", tl, tr);

      if (tl <= tr) {
        assert(1 <= tl && tl <= tp);
        assert(1 <= tr && tr <= tp);

        tl = std::max(tl > 1 ? stk[tl - 1].first + 1 : 1, o.l);
        tr = std::min(stk[tr].first, i - 1);
        // printf("tl = %d, tr = %d\n", tl, tr);

        if (tl <= tr) {
          int min_val = SegmentTree::getRangeMin(tl, tr);
          ans[o.id] = std::min(ans[o.id], min_val);
        }
      }

      // printf("[%d, %d] l_max=%d", o.l, i, pos);
    }
  }
}

int main() {
  rd(n);
  FOR(i, 1, n) {
    int ai;
    rd(ai);
    b[i] = las[ai];
    las[ai] = i;
  }
  rd(q);
  FOR(i, 1, q) {
    int l, r;
    rd(l, r);
    queries[r].push_back((Query){l, i});
  }

  FOR(i, 1, q) ans[i] = INT_MAX;

  work();

  FOR(i, 1, q) wrln(ans[i]);
  return 0;
}
