#include <bits/stdc++.h>
#define FOR(a, b, c) for (int a = (int)(b); a <= (int)(c); a++)

const int N = 5005;

int n, a[N];

int count(int l, int r) {
  std::set<int> s;
  FOR(i, l, r) s.insert(a[i]);
  return s.size();
}
int query(int l, int r) {
  int x = count(l, r);
  int ans = r - l + 1;
  FOR(i, l, r) FOR(j, i, r) {
    if (count(i, j) == x) ans = std::min(ans, j - i + 1);
  }
  return ans;
}

int q;
int main() {
  scanf("%d", &n);
  FOR(i, 1, n) scanf("%d", a + i);
  scanf("%d", &q);
  FOR(i, 1, q) {
    int l, r;
    scanf("%d%d", &l, &r);
    printf("%d\n", query(l, r));
  }
  return 0;
}
