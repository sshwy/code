#include <bits/stdc++.h>
using namespace std ;
#define rep(i, a, b) for (int i = (a); i <= (b); ++i)
#define per(i, a, b) for (int i = (a); i >= (b); --i)
#define loop(it, v) for (auto it = v.begin(); it != v.end(); it++)
#define cont(i, x) for (int i = head[x]; i; i = edge[i].nex)
#define clr(a) memset(a, 0, sizeof(a))
#define ass(a, cnt) memset(a, cnt, sizeof(a))
#define cop(a, b) memcpy(a, b, sizeof(a))
#define lowbit(x) (x & -x)
#define aint(x) x.begin(), x.end()
#define SC(t, x) static_cast <t> (x)
#define ub upper_bound
#define lb lower_bound
#define pqueue priority_queue
#define mp make_pair
#define pb push_back
#define pof pop_front
#define pob pop_back
#define fi first
#define se second
#define y1 y1_
#define Pi acos(-1.0)
#define iv inline void
#define enter putchar('\n')
#define siz(x) ((int)x.size())
#define file(x) freopen(x".in", "r", stdin),freopen(x".out", "w", stdout)
typedef double db ;
typedef long long ll ;
typedef unsigned long long ull ;
typedef pair <int, int> pii ;
typedef vector <int> vi ;
typedef vector <pii> vii ;
typedef queue <int> qi ;
typedef queue <pii> qii ;
typedef set <int> si ;
typedef map <int, int> mii ;
typedef map <string, int> msi ;
const int maxn = 2e6 + 100 ;
const int inf = 0x3f3f3f3f ;
const int iinf = 1 << 30 ;
const ll linf = 2e18 ;
const int mod = 998244353 ;
const double eps = 1e-7 ;
template <class T = int> T chmin(T &a, T b) { return a = min(a, b);}
template <class T = int> T chmax(T &a, T b) { return a = max(a, b);}
template <class T = int> iv red(T &x) { x -= mod, x += x >> 31 & mod;}
namespace io {
	const int SIZE = (1 << 21) + 1;
	char ibuf[SIZE], *iS, *iT, obuf[SIZE], *oS = obuf, *oT = oS + SIZE - 1, c, qu[55]; int f, qr;
	// getchar
	#define gc() (iS == iT ? (iT = (iS = ibuf) + fread (ibuf, 1, SIZE, stdin), (iS == iT ? EOF : *iS ++)) : *iS ++)
	// print the remaining part
	inline void flush () {
		fwrite (obuf, 1, oS - obuf, stdout);
		oS = obuf;
	}
	// putchar
	inline void putc (char x) {
		*oS ++ = x;
		if (oS == oT) flush ();
	}
	// input a signed integer
	template <class I = ll>
	I read () {
		I x;
		for (f = 1, c = gc(); c < '0' || c > '9'; c = gc()) if (c == '-') f = -1;
		for (x = 0; c <= '9' && c >= '0'; c = gc()) x = x * 10 + (c & 15); x *= f;
		return x;
	}
	// print a signed integer
	template <class I = ll>
	inline void print (I x) {
		if (!x) putc ('0'); if (x < 0) putc ('-'), x = -x;
		while (x) qu[++ qr] = x % 10 + '0',  x /= 10;
		while (qr) putc (qu[qr --]);
	}
	//no need to call flush at the end manually!
	struct Flusher_ {~Flusher_(){flush();}}io_flusher_;
}
using io :: read;
using io :: putc;
using io :: print;
int n, q;

int a[maxn], ans[maxn];

vii que[maxn];

int last[maxn];

struct node
{
	int l, r, lim;
	pii mn, last;
}
tr[maxn << 2];

#define ls (tot << 1)
#define rs (ls | 1)
#define mid ((tr[tot].l + tr[tot].r) >> 1)

void build(int tot, int l, int r)
{
	tr[tot].l = l, tr[tot].r = r;
	tr[tot].mn = tr[tot].last = mp(-inf, inf);
	if(l == r) return;
	build(ls, l, mid), build(rs, mid + 1, r);
}

inline bool cmp(pii a, pii b)
{
	return a.se - a.fi <= b.se - b.fi;
}

inline void stag(int tot, int lim)
{
	tr[tot].lim = lim;
	tr[tot].mn = tr[tot].last;
	tr[tot].mn.se = tr[tot].last.se = lim;
}

inline void pushdown(int tot)
{
	if(tr[tot].lim) stag(ls, tr[tot].lim), stag(rs, tr[tot].lim), tr[tot].lim = 0;
}

void modify(int tot, int l, int r, int lim)
{
	// printf("modify : %intd %intd %intd\n", tr[tot].l, tr[tot].r, lim);
	if(tr[tot].l >= l && tr[tot].r <= r) return (void)(stag(tot, lim));
	pushdown(tot);
	if(l <= mid) modify(ls, l, r, lim);
	if(r > mid) modify(rs, l, r, lim);
}

void pushstk(int tot, pii p)
{
	tr[tot].last = p;
	if(cmp(p, tr[tot].mn)) tr[tot].mn = p;
}

void upd(int tot, int l, int r, pii p)
{
	if(tr[tot].l >= l && tr[tot].r <= r) return (void)(pushstk(tot, p));
	pushdown(tot);
	if(l <= mid) upd(ls, l, r, p);
	if(r > mid) upd(rs, l, r, p);
}

pii ask(int tot, int p)
{
	pii ret, tmp;
	ret = tr[tot].mn;
	if(tr[tot].l == tr[tot].r) return ret;
	pushdown(tot);
	if(p <= mid) tmp = ask(ls, p);
	else tmp = ask(rs, p);
	if(cmp(tmp, ret)) ret = tmp;
	return ret;
}

struct binary_indexed_tree
{
	int val[maxn];
	void add(int pos, int x)
	{
		for(int i = pos; i <= n; i += lowbit(i)) val[i] += x;
	}
	void add(int l, int r, int x)
	{
		add(l, x), add(r + 1, -x);
	}
	int find(int x)
	{
		int ret = 0, pos = 0;
		per(i, 20, 0)
		{
			if(pos + (1 << i) > n) continue;
			if(ret + val[pos + (1 << i)] > x) pos += (1 << i), ret += val[pos];
		}
		return pos + 1;
	}
	int query(int pos)
	{
		int ret = 0;
		for(int i = pos; i; i -= lowbit(i)) ret += val[i];
		return ret;
	}
}
bit;

signed main()
{
	// file("main");
	n = read();
	rep(i, 1, n) a[i] = read();
	q = read();
	rep(i, 1, q)
	{
		int l = read(), r = read();
		// if(l > r) swap(l, r);
		que[r].pb(mp(l, i));
	}
	build(1, 1, n);
	// puts("prework ok");
	rep(i, 1, n)
	{
		// printf("%d : %d\n", i, last[a[i]]);
		upd(1, i, i, mp(i, i));
		modify(1, last[a[i]] + 1, i, i);
		bit.add(last[a[i]] + 1, i, 1);
		if(last[a[i]])
		{
			int l = bit.find(bit.query(last[a[i]]));
			// printf("pos : %d\n", l);
			pii tmp = ask(1, last[a[i]] + 1);
			// printf("interval [%d, %d]\n", tmp.fi, tmp.se);
			upd(1, l, last[a[i]], tmp);
		}
		// rep(j, 1, i)
		// {
		// 	pii tmp = ask(1, j);
		// 	printf("[%d, %d]\n", tmp.fi, tmp.se);
		// }
		// enter;
		for(pii j : que[i])
		{
			pii tmp = ask(1, j.fi);
			ans[j.se] = tmp.se - tmp.fi + 1;
		}
		last[a[i]] = i;
	}
	rep(i, 1, q) print(ans[i]), putc('\n');
	return 0;
}
