// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

int f(int x) {
  int _x = x;
  vector<int> v;
  for (int i = 2; i * i <= x; i++) {
    if (x % i) continue;
    while (x % i == 0) v.pb(i), x /= i;
  }
  if (x > 1) v.pb(x);
  int x2 = 1;
  for (int e : v) x2 *= e + 1;
  return x2 - _x;
}

using namespace RA;

int main() {
  srand(clock() + time(0));
  int n;
  cin >> n;
  do {
    int x = rnd(1, 200);
    int t = f(x);
    printf("x = %d t = %d\n", x, t);
    if (t == n) { break; }
  } while (1);
  return 0;
}
