// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

typedef long long LL;
LL c;
int limV, limE;

void solve2() {
  long long t = 1;
  int n = 0;
  vector<pair<int, int>> ve;
  while (t < c) t *= 2, ++n;
  long long rest = t - c;
  FOR(i, 1, n - 1) {
    if (rest >> (i - 1) & 1)
      ve.pb({i, n});
    else
      ve.pb({i, n + 1});
    ve.pb({i + 1, i});
  }
  ve.pb({1, n + 1});
  ve.pb({n, n + 1});
  printf("%d %lu\n", n + 1, ve.size());
  for (auto e : ve) printf("%d %d\n", e.second, e.first);
}
int main() {
  scanf("%lld%d%d", &c, &limV, &limE);
  if (c == 1) {
    puts("2 1\n2 1");
  } else {
    solve2();
  }
  return 0;
}
