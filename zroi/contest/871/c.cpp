// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

const int N = 20, P = 998244353;

int pw(int a, int m) {
  int res = 1;
  while (m) m & 1 ? res = 1ll * res * a % P : 0, a = 1ll * a * a % P, m >>= 1;
  return res;
}

int n, c[N], tot_c;
int a[N], fnv[N];
int ans;

void updateAns() {
  int cnt = 1, coef = 1;
  FOR(i, 1, tot_c) {
    if (a[i] != a[i - 1]) {
      coef = 1ll * coef * fnv[cnt] % P;
      cnt = 0;
    }
    ++cnt;
  }
  coef = 1ll * coef * fnv[cnt] % P;
  if (a[1] == a[tot_c]) {
    int i = 1;
    while (i < tot_c && a[i + 1] == a[1]) ++i;
    int j = tot_c;
    while (j > 1 && a[j - 1] == a[tot_c]) --j;
    coef = 1ll * coef * fnv[i + tot_c - j + 1] % P * pw(fnv[i], P - 2) % P *
           pw(fnv[tot_c - j + 1], P - 2) % P;
  }
  // FOR(i, 1, tot_c) printf("%d%c", a[i], " \n"[i == tot_c]);
  // printf("coef %d\n", coef);
  ans = (ans + coef) % P;
}
void dfs(int pos) {
  if (pos == tot_c + 1) {
    updateAns();
    return;
  }
  FOR(i, 1, n) if (c[i]) {
    a[pos] = i;
    c[i]--;
    dfs(pos + 1);
    c[i]++;
  }
}

int main() {
  fnv[0] = 1;
  FOR(i, 1, N - 1) fnv[i] = 1ll * fnv[i - 1] * pw(i, P - 2) % P;

  cin >> n;
  FOR(i, 1, n) cin >> c[i], tot_c += c[i];
  dfs(1);

  printf("%d\n", ans);
  return 0;
}
