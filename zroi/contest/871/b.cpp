// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

typedef pair<int, int> pt;
const int N = 1e6 + 5, P = 998244353, I2 = (P + 1) / 2;

int pw(int a, int m) {
  int res = 1;
  while (m) m & 1 ? res = 1ll * res * a % P : 0, a = 1ll * a * a % P, m >>= 1;
  return res;
}

char s[N];
int n, currentTime;
int tot, lc[N], rc[N];
pt tag[N];

bool isLeaf(int u) { return !lc[u] && !rc[u]; }
int newNode() {
  int u = ++tot;
  lc[u] = rc[u] = tag[u].first = tag[u].second = 0;
  return u;
}
void pushdown(int u) {
  int lu = newNode();
  lc[u] = lu, tag[lu] = {tag[u].first + 1, 2};
  int ru = newNode();
  rc[u] = ru, tag[ru] = {tag[u].first + 1, 2};
}
int countLeaf(int u, int t) {
  int res = 1ll * tag[u].second * pw(2, t - tag[u].first) % P;
  return res;
}

void pushup(int u) {
  assert(lc[u] && rc[u]);
  int u_t = max(tag[lc[u]].first, tag[rc[u]].first);
  int u_c = (countLeaf(lc[u], u_t) + countLeaf(rc[u], u_t)) % P;
  tag[u] = {u_t, u_c};
}
void dfs_work(int u, char *s) {
  assert(u);
  if (*s == 0) {
    assert(tag[u].first <= currentTime);
    tag[u] = {currentTime + 1, 2};
    lc[u] = rc[u] = 0;
    return;
  }
  if (isLeaf(u)) pushdown(u);
  if (*s == 'L')
    dfs_work(lc[u], s + 1);
  else
    dfs_work(rc[u], s + 1);
  pushup(u);
}
void work(char *s) { dfs_work(1, s); }

int getAns() {
  int res = countLeaf(1, currentTime + 1) * 1ll * I2 % P;
  return (res - 1 + P) % P;
}

void init() {
  tot = 0;
  newNode();
  tag[tot] = {0, 2};
  currentTime = 0;
}
void go() {
  init();
  scanf("%d", &n);
  FOR(i, 1, n) {
    char op[5];
    int t;
    scanf("%s", op);
    if (op[0] == 'G') {
      scanf("%d", &t);
      currentTime += t;
    } else {
      scanf("%s", s);
      work(s);
    }
    int ans = getAns();
    printf("%d\n", ans);
  }
}

int main() {
  int t;
  scanf("%d", &t);
  FOR(i, 1, t) go();
  return 0;
}
