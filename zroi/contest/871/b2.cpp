// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

typedef pair<int, int> pt;
const int N = 1e6 + 5, P = 998244353, I2 = (P + 1) / 2;

int pw(int a, int m) {
  int res = 1;
  while (m) m & 1 ? res = 1ll * res * a % P : 0, a = 1ll * a * a % P, m >>= 1;
  return res;
}

char s[N];
int n;
int tot, lc[N], rc[N], sz[N];
int newNode() {
  int u = ++tot;
  lc[u] = rc[u] = 0;
  sz[u] = 1;
  return u;
}
void dfs_work(int u, char *s) {
  assert(u);
  if (*s == 0) {
    sz[u] = lc[u] = rc[u] = 0;
    return;
  }
  if (*s == 'L')
    dfs_work(lc[u], s + 1);
  else
    dfs_work(rc[u], s + 1);
  if (lc[u] && sz[lc[u]] == 0) lc[u] = 0;
  if (rc[u] && sz[rc[u]] == 0) rc[u] = 0;
  sz[u] = sz[lc[u]] + sz[rc[u]] + 1;
}
void work(char *s) { dfs_work(1, s); }

void init() {
  tot = 0;
  newNode();
  sz[tot] = 1;
}

void dfs_extend(int u) {
  if (lc[u])
    dfs_extend(lc[u]);
  else
    lc[u] = newNode();
  if (rc[u])
    dfs_extend(rc[u]);
  else
    rc[u] = newNode();
  sz[u] = sz[lc[u]] + sz[rc[u]] + 1;
}

void extend() { dfs_extend(1); }

int getAns() { return sz[1]; }

void go() {
  init();
  scanf("%d", &n);
  FOR(i, 1, n) {
    char op[5];
    int t;
    scanf("%s", op);
    if (op[0] == 'G') {
      scanf("%d", &t);
      FOR(_, 1, t) extend();
    } else {
      scanf("%s", s);
      work(s);
    }
    int ans = getAns();
    printf("%d\n", ans);
  }
}

int main() {
  int t;
  scanf("%d", &t);
  FOR(i, 1, t) go();
  return 0;
}
