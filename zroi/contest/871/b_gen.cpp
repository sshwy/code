// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

using namespace RA;

void go() {
  int n = rnd(1, 5);
  printf("%d\n", n);
  printf("G %d\n", rnd(1, 2));
  FOR(i, 2, n) if (rnd(2)) { printf("G %d\n", rnd(1, 2)); }
  else {
    printf("A %c\n", rnd(2) ? 'L' : 'R');
  }
}

int main() {
  srand(clock() + time(0));
  int t = 10;
  printf("%d\n", t);
  FOR(i, 1, t) go();
  return 0;
}
