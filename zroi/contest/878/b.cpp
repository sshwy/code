// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

const int N = 205, M = 405, P = 998244353;

void add(int &a, long long b) { a = (a + b) % P; }

int n, m, A, B;
int U[M], V[M];

namespace S1 {
  const int SZ = 1 << 21;
  int ga[N], gb[N], f[2][SZ];
  int main() {
    FOR(i, 1, m) {
      int u = U[i], v = V[i];
      if (v - u == A) ga[v] = 1;
      if (v - u == B) gb[v] = 1;
    }
    f[0][0] = 1;
    int lim = 1 << B;
    FOR(i, 0, n) {
      memset(f[i + 1 & 1], 0, sizeof(f[0]));
      FOR(j, 0, lim - 1) if (f[i & 1][j]) {
        int fval = f[i & 1][j];
        if (ga[i + 1] && (!(j >> (A - 1) & 1))) {
          add(f[i + 1 & 1][((j << 1) | (1 << A) | 1) & (lim - 1)], fval);
        }
        if (gb[i + 1] && (!(j >> (B - 1) & 1))) {
          add(f[i + 1 & 1][((j << 1) | (1 << B) | 1) & (lim - 1)], fval);
        }
        add(f[i + 1 & 1][(j << 1) & (lim - 1)], fval);
      }
    }
    int ans = 0;
    FOR(j, 0, lim - 1) add(ans, f[n & 1][j]);
    printf("%d\n", ans);
    return 0;
  }
} // namespace S1

namespace S2 {
  const int NR = N, NC = 22, SZ = 1 << NC;

  int left[NR][NC], up[NR][NC], nd[NR][NC];
  int ga[N], gb[N], _f[2][SZ], row[N], col[N];
  int *f = _f[0], *g = _f[1];

  int DP(int nr, int nc, int stat) {
    assert(nc < 22);
    int lim = 1 << (nc + 1);
    FOR(i, 1, nr) {
      FOR(j, 0, nc - 1) {
        FOR(s, 0, lim - 1) {
          if (f[s]) printf("f(%d, %d, %d) = %d\n", i, j, s, f[s]);
        }
        memset(g, 0, sizeof(int) * lim);
        // f -> g
        if (i != nr || !(stat >> j & 1)) {
          FOR(s, 0, lim - 1) { // do nothing
            int ns = s & ~(1 << j);
            // if(f[s]) printf("%d %d %d -> %d\n", i, j, s, ns);
            add(g[ns], f[s]);
          }
        }
        if (up[i][j] != -1) {
          if (i != nr || (stat >> j & 1)) {
            int uj = up[i][j];
            assert(uj == j || uj == j - 1);
            FOR(s, 0, lim - 1) {
              if (s >> (uj + 1) & 1) continue;
              int ns = s | (1 << j) | (1 << uj + 1);
              add(g[ns], f[s]);
            }
          }
        } else {
          if (i == nr && stat >> j & 1) { // invalid
            return 0;
          }
        }
        if (left[i][j] != -1) {
          if (i != nr) {
            assert(j > 0);
            FOR(s, 0, lim - 1) {
              if (s >> (j - 1) & 1) continue;
              int ns = s | (1 << j - 1) | (1 << j);
              add(g[ns], f[s]);
            }
          }
        }
        swap(f, g);
      }
      memset(g, 0, sizeof(int) * lim);
      FOR(s, 0, lim - 1) {
        int ns = (s << 1) & (lim - 1);
        add(g[ns], f[s]);
      }
      swap(f, g);
    }
    assert((stat << 1) < lim);
    return f[stat << 1];
  }

  int c2(int nr, int nc, int stat) {
    assert(nc < 22);
    printf("\033[31mc2 %d %d %d\033[0m\n", nr, nc, stat);
    int lim = 1 << nc;
    fill(g, g + lim, 0);
    g[0] = 1;
    FOR(i, 0, nc - 1) {
      int exJ = 1 << i;
      if (stat >> i & 1) { // 钦定
        FOR(j, 0, exJ - 1) { add(g[j | (1 << i)], g[j]); }
      } else {
        if (left[0][i] != -1) { // 有边
          assert(i > 0);
          FOR(j, 0, exJ - 1) if (!(j >> (i - 1) & 1)) {
            add(g[j | (1 << i) | (1 << i - 1)], g[j]);
          }
        }
      }
    }
    fill(f, f + lim + lim, 0);
    FOR(i, 0, lim - 1) { f[i << 1] = g[i]; }
    int res = DP(nr, nc, stat);
    printf("res %d\n", res);
    return res;
  }

  int calc(int nr, int nc) {
    assert(nc < 22);
    // printf("calc %d %d\n", nr, nc);
    int lim = 1 << nc, res = 0;
    FOR(i, 0, lim - 1) { add(res, c2(nr, nc, i)); }
    return res;
  }

  int work(int real_n, int real_A, int real_B) { // 0 .. real_n - 1
    memset(left, -1, sizeof(left));
    memset(up, -1, sizeof(up));

    int nr = real_B;              // 0 .. nr - 1
    int nc = real_n / real_B + 1; // 0 .. nc - 1
    printf("nr %d nc %d\n", nr, nc);
    FOR(x, 0, nr - 1) FOR(y, 0, nc - 1) {
      int id = real_A * x % real_B + y * real_B;
      row[id] = x;
      col[id] = y;
      nd[x][y] = id;
    }
    FOR(x, 0, nr - 1) FOR(y, 0, nc - 1) {
      int id = real_A * x % real_B + y * real_B;
      if (gb[id]) {
        left[x][y] = true;
        printf("L %d %d id %d\n", x, y, id);
      }
      if (ga[id]) {
        up[x][y] = col[id - real_A];
        printf("U %d %d : %d\n", x, y, up[x][y]);
        printf("row %d col %d\n", row[id - real_B], col[id - real_A]);
        assert((row[id - real_A] + 1) % real_B == x);
      }
    }
    return calc(nr, nc);
  }
  int main() {
    int g = __gcd(A, B);
    int ans = 1;
    FOR(i, 0, g - 1) {
      memset(ga, 0, sizeof(ga));
      memset(gb, 0, sizeof(gb));
      FOR(_, 1, m) {
        int u = U[_] - 1, v = V[_] - 1;
        if (v % g == i) {
          if (v - u == A) ga[v / g] = 1;
          if (v - u == B) gb[v / g] = 1;
        }
      }
      ans = 1ll * ans * work(n / g + 1, A / g, B / g) % P;
    }
    printf("%d\n", ans);
    return 0;
  }
} // namespace S2

int main() {
  scanf("%d%d%d%d", &n, &m, &A, &B);
  FOR(i, 1, m) { scanf("%d%d", U + i, V + i); }
  // S1::main();
  S2::main();
  // if(B <= 20) S1::main();
  // else S2::main();
  return 0;
}
