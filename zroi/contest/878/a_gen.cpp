// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

using namespace RA;

int main() {
  srand(clock() + time(0));
  int n = rnd(20, 25), x = rnd(1, n - 1);
  FOR(i, 1, n) {
    if (i == x || i == x + 1)
      putchar('_');
    else
      putchar(rnd(100) < 50 ? 'A' : 'B');
  }
  puts("");
  return 0;
}
