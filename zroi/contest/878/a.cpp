// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

const int N = 1e5 + 5;

int m, n;
char s[N];

vector<int> ans;

set<int> posA;

void makeMove(int x, int y) {
  assert(x < n && y < n);
  assert(s[y] == '_' && s[y + 1] == '_');
  if (s[x] == 'A') posA.erase(x), posA.insert(y);
  if (s[x + 1] == 'A') posA.erase(x + 1), posA.insert(y + 1);
  swap(s[x], s[y]);
  swap(s[x + 1], s[y + 1]);
}
void getAns() {
  int rest = 0;
  FOR(i, 1, n) if (s[i] == 'A') posA.insert(i), ++rest;
  FOR(i, 1, n) {
    if (s[i] == 'A')
      --rest;
    else {                   // [i, n] has rest A's
      if (rest == 0) return; // done
      if (rest > 1) {
        if (s[i] == '_') {
          assert(s[i + 1] == '_');
          int px = posA.lower_bound(i);
          assert(*px < n);
          makeMove(*px, i);
        } else {
        }
      } else {
        assert(0);
      }
    }
  }
}

void go() {
  int cA = 0, cB = 0;
  FOR(i, 1, n) {
    cA += s[i] == 'A';
    cB += s[i] == 'B';
  }
  if (cA == 0 || cB == 0) return;
  if (cA > cB) {
    reverse(s + 1, s + n + 1);
    FOR(i, 1, n) {
      if (s[i] == 'A') s[i] = 'B';
      if (s[i] == 'B') s[i] = 'A';
    }
    getAns();
    for (auto &x : ans) x = n - x;
  } else {
    getAns();
  }
  printf("%lu\n", ans.size());
  for (unsigned i = 0; i < ans.size(); i++)
    printf("%d%c", ans[i], " \n"[i + 1 == ans.size()]);
}

int main() {
  scanf("%d", &m);
  scanf("%s", s + 1);
  n = strlen(s + 1);

  if (n < 4) {
    puts("0");
    return 0;
  }

  if (n == 4) {
    int pA = -1, pB = -1;
    FOR(i, 1, 4) {
      if (s[i] == 'A') pA = i;
      if (s[i] == 'B') pB = i;
    }
    if (pA != -1 && pB != -1 && pB < pA) {
      puts("-1");
      return 0;
    }
    puts("0");
    return 0;
  }

  go();

  return 0;
}
