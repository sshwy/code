// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

const int N = 105, P = 998244353;

void add(int &a, int b) { a = (a + b) % P; }

int n, a[N];
int f[N][N][N], ans[N];

int main() {
  scanf("%d", &n);
  FOR(i, 1, n) scanf("%d", a + i);
  f[0][0][0] = 1;
  FOR(i, 1, n) {
    FOR(j, 0, a[i]) {
      FOR(k, 0, i) {
        int nk = k - (j == a[i]);
        if (nk >= 0) { FOR(p, 0, j) add(f[i][j][k], f[i - 1][p][nk]); }
      }
    }
  }
  FOR(j, 0, a[n]) FOR(k, 0, n) add(ans[k], f[n][j][k]);
  FOR(i, 0, n) printf("%d%c", ans[i], " \n"[i == n]);
  return 0;
}
