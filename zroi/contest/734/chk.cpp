// by Sshwy
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

FILE *fin, *fout;

const int N = 3e5 + 5;
int n, m;
vector<int> g[N];
int dep[N];
void dfs(int u, int p) {
  dep[u] = dep[p] + 1;
  for (int v : g[u])
    if (v != p) dfs(v, u);
}
int d2[N];
void dfs2(int u, int p) {
  d2[u] = d2[p] + 1;
  for (int v : g[u])
    if (v != p) dfs2(v, u);
}
int tag[N];
void go(int id) {
  fscanf(fin, "%d%d", &n, &m);
  FOR(i, 1, n) g[i].clear();
  fill(tag, tag + n + 1, 0);
  FOR(i, 1, n - 1) {
    int u, v;
    fscanf(fin, "%d%d", &u, &v);
    g[u].pb(v);
    g[v].pb(u);
  }
  char sout[10];
  int nd = -1;
  fscanf(fout, "%s", sout);
  if (sout[0] == 'T') fscanf(fout, "%d", &nd);
  FOR(_, 1, m) {
    int a, b, d;
    fscanf(fin, "%d%d%d", &a, &b, &d);
    dfs(a, 0);
    dfs2(b, 0);
    FOR(i, 1, n) {
      if (dep[i] + d2[i] - 2 <= d) { ++tag[i]; }
    }
    if (nd != -1 && dep[nd] + d2[nd] - 2 > d) {
      printf("WA # %d, nd=%d\n", id, nd);
      exit(1);
    }
  }
  int no = 0;
  FOR(i, 1, n) if (tag[i] == m) {
    no = i;
    break;
  }
  if (no != 0 && sout[0] == 'N') {
    printf("WA # %d, no=%d\n", id, no);
    printf("%d %d\n", n, m);
    exit(1);
  }
}
int main(int argc, char **argv) {
  int t;
  fin = fopen(argv[1], "r");
  fout = fopen(argv[2], "r");

  fscanf(fin, "%d", &t);
  FOR(i, 1, t) go(i);

  fclose(fin);
  fclose(fout);
  return 0;
}
