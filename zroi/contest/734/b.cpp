// by Sshwy
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

const int N = 3e5 + 5;

struct qxx {
  int nex, t;
} e[N * 2];
int h[N], le = 1;
void add_path(int f, int t) { e[++le] = {h[f], t}, h[f] = le; }

int n, m, a[N], b[N], d[N];

int sz[N], sm[N], cut[N];

void Size(int u, int p) {
  sz[u] = 1, sm[u] = 0;
  for (int i = h[u]; i; i = e[i].nex) {
    int v = e[i].t;
    if (cut[v] || v == p) continue;
    Size(v, u), sz[u] += sz[v], sm[u] = max(sm[u], sz[v]);
  }
}
int Val(int u, int T) { return max(sm[u], T - sz[u]); }
int Core(int u, int p, int T) {
  int r = u;
  for (int i = h[u]; i; i = e[i].nex) {
    int v = e[i].t;
    if (cut[v] || v == p) continue;
    int z = Core(v, u, T);
    if (Val(z, T) < Val(r, T)) r = z;
  }
  return r;
}
int dep[N], col[N];
void dfs(int u, int p, int tag) {
  dep[u] = dep[p] + 1, col[u] = tag;
  for (int i = h[u]; i; i = e[i].nex) {
    int v = e[i].t;
    if (v == p) continue;
    dfs(v, u, tag == -1 ? v : tag);
  }
}
void Dfs(int u, int p) {
  if (cut[u]) {
    puts("NIE");
    return;
  }
  Size(u, p);
  int core = Core(u, p, sz[u]);

  dfs(core, 0, -1);
  int nex = -1;
  FOR(i, 1, m) {
    int u = a[i], v = b[i], di = d[i];
    if (dep[u] + dep[v] - 2 > di) {
      if (col[u] == -1 || col[v] == -1 || col[u] != col[v] ||
          (nex != -1 && nex != col[u])) {
        nex = -2;
        break;
      } else {
        nex = col[u];
      }
    }
  }
  if (nex == -1) {
    printf("TAK %d\n", core);
    return;
  }
  if (nex == -2) {
    puts("NIE");
    return;
  }
  cut[core] = 1;
  Dfs(nex, core);
}
void go() {
  scanf("%d%d", &n, &m);
  fill(h, h + n + 1, 0), le = 1;
  FOR(i, 1, n - 1) {
    int u, v;
    scanf("%d%d", &u, &v);
    add_path(u, v), add_path(v, u);
  }
  FOR(i, 1, m) scanf("%d%d%d", a + i, b + i, d + i);
  fill(cut, cut + n + 1, 0);
  Dfs(1, 0);
}
int main() {
  int t;
  scanf("%d", &t);
  FOR(i, 1, t) go();
  return 0;
}
