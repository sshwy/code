// by Sshwy
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA
const int N = 16;
int n, m, a, b;
int w[N][N];
int f[1 << N][N], ans[N];

int fa[N];
int get(int u) { return fa[u] == u ? u : fa[u] = get(fa[u]); }
vector<pair<int, int>> e[2];

int main() {
  scanf("%d%d%d%d", &n, &m, &a, &b);
  memset(w, 0x3f, sizeof(w));
  FOR(i, 1, m) {
    int u, v, c;
    scanf("%d%d%d", &u, &v, &c);
    --u, --v;
    w[u][v] = w[v][u] = c;
    if (c == a)
      e[0].pb({u, v});
    else
      e[1].pb({u, v});
  }
  FOR(j, 0, n - 1) fa[j] = j;
  int tot_mn = 0;
  for (auto p : e[0]) {
    int u = p.first, v = p.second;
    if (get(u) == get(v)) continue;
    tot_mn += a;
    fa[get(u)] = get(v);
  }
  for (auto p : e[1]) {
    int u = p.first, v = p.second;
    if (get(u) == get(v)) continue;
    tot_mn += b;
    fa[get(u)] = get(v);
  }
  memset(f, 0x3f, sizeof(f));
  int lim = 1 << n;
  f[1][0] = 0;

  FOR(i, 2, lim - 1) {
    FOR(j, 0, n - 1) if (i >> j & 1) {
      FOR(k, 0, n - 1) if (j != k && i >> k & 1) {
        f[i][j] = min(f[i][j], f[i ^ (1 << j)][k] + w[k][j]);
      }
      // printf("f[%d,%d] = %d\n",i,j,f[i][j]);
    }
  }
  memset(ans, 0x3f, sizeof(ans));

  FOR(i, 1, lim - 1) if (i & 1) {
    FOR(j, 0, n - 1) fa[j] = j;
    FOR(j, 1, n - 1) if (i >> j & 1) fa[get(j)] = get(0);
    int tot = 0;
    for (auto p : e[0]) {
      int u = p.first, v = p.second;
      if (get(u) == get(v)) continue;
      tot += a;
      fa[get(u)] = get(v);
    }
    for (auto p : e[1]) {
      int u = p.first, v = p.second;
      if (get(u) == get(v)) continue;
      tot += b;
      fa[get(u)] = get(v);
    }
    FOR(j, 0, n - 1) if (i >> j & 1) {
      // printf("tot_mn %d my %d\n",tot_mn,f[i][j]+tot);
      // printf("f[%d,%d]=%d\n",i,j,f[i][j]);
      assert(f[i][j] + tot >= tot_mn);
      if (f[i][j] + tot == tot_mn) { ans[j] = min(ans[j], f[i][j]); }
    }
  }
  FOR(i, 0, n - 1) printf("%d%c", ans[i], " \n"[i == n - 1]);
  return 0;
}
