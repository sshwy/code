// by Sshwy
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA
using namespace RA;

void go() {
  int n = 3e5, m = 3e5;
  // int n=rnd(2,50),m=rnd(2,20);
  printf("%d %d\n", n, m);
  FOR(i, 1, n - 1) printf("%d %d\n", rnd(max(1, i / 2), i), i + 1);
  int t = rnd(1, n);
  FOR(i, 1, m) printf("%d %d %d\n", t, t, 0);
}
int main() {
  srand(clock() + time(0));
  int t = 1;
  printf("%d\n", t);
  FOR(i, 1, t) go();
  return 0;
}
