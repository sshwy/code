// by Sshwy
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA
const int N = 3e5 + 5;
int n, m;
vector<int> g[N];
int dep[N];
void dfs(int u, int p) {
  dep[u] = dep[p] + 1;
  for (int v : g[u])
    if (v != p) dfs(v, u);
}
int d2[N];
void dfs2(int u, int p) {
  d2[u] = d2[p] + 1;
  for (int v : g[u])
    if (v != p) dfs2(v, u);
}
int tag[N];
void go() {
  scanf("%d%d", &n, &m);
  FOR(i, 1, n) g[i].clear();
  fill(tag, tag + n + 1, 0);
  FOR(i, 1, n - 1) {
    int u, v;
    scanf("%d%d", &u, &v);
    g[u].pb(v);
    g[v].pb(u);
  }
  FOR(_, 1, m) {
    int a, b, d;
    scanf("%d%d%d", &a, &b, &d);
    dfs(a, 0);
    dfs2(b, 0);
    // printf(": %d %d %d\n",a,b,d);
    FOR(i, 1, n) {
      if (dep[i] + d2[i] - 2 <= d) {
        ++tag[i];
        // printf("dist %d = %d\n",i,dep[i]+d2[i]-2);
      }
    }
  }
  FOR(i, 1, n) if (tag[i] == m) return printf("TAK %d\n", i), void();
  puts("NIE");
}
int main() {
  int t;
  scanf("%d", &t);
  FOR(i, 1, t) go();
  return 0;
}
