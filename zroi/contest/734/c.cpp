// by Sshwy
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA
const int N = 2005, P = 998244353;
int pw(int a, int m) {
  int res = 1;
  while (m) m & 1 ? res = 1ll * res * a % P : 0, a = 1ll * a * a % P, m >>= 1;
  return res;
}
int n, a[N];
int f[N][N];
int fac[N], fnv[N];

int main() {
  scanf("%d", &n);
  fac[0] = 1;
  FOR(i, 1, n) fac[i] = fac[i - 1] * 1ll * i % P;
  fnv[n] = pw(fac[n], P - 2);
  ROF(i, n, 1) fnv[i - 1] = fnv[i] * 1ll * i % P;

  FOR(i, 1, n) scanf("%d", a + i);
  sort(a + 1, a + n + 1);
  f[0][0] = 1;
  FOR(i, 1, n) FOR(j, 1, i) {
    int mxp = i / j, coef = 1;
    FOR(p, 1, mxp) {
      coef = coef * 1ll * fnv[j] % P;
      if (j <= a[i - p * j + 1]) {
        FOR(k, 0, j - 1) {
          printf("divide %d into %d : %lld\n", p * j, p,
              1ll * fac[p * j] % P * coef % P * fnv[p] % P);
          f[i][j] = (f[i][j] + f[i - p * j][k] * 1ll * fac[p * j] % P * coef % P *
                                   fnv[p] % P) %
                    P;
        }
      }
    }
    printf("f[%d,%d] = %d\n", i, j, f[i][j]);
  }
  int ans = 0;
  FOR(j, 0, n) ans = (ans + f[n][j]) % P;
  printf("%d\n", ans);
  return 0;
}
