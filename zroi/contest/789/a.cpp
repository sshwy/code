#include <bits/stdc++.h>
#define FOR(a, b, c) for (int a = (int)(b); a <= (int)(c); a++)
using namespace std;

const int N = 1e5 + 5;

int n, m;
int f[N], g[N];
set<int> s[N];
int get(int u) { return f[u] == u ? u : get(f[u]); }

void merge(int x, int y) {
  if (g[x] > g[y]) swap(x, y);
  g[y] += g[x];
  for (auto u : s[x]) {
    s[y].insert(u);
    s[u].erase(x);
    s[u].insert(y);
  }
  f[x] = y;
}
int main() {
  scanf("%d%d", &n, &m);
  FOR(i, 1, n) f[i] = i, g[i] = 1;
  FOR(i, 1, m) {
    int e, x, y;
    scanf("%d%d%d", &e, &x, &y);
    x = get(x), y = get(y);
    if (e == 1) {
      if (x != y && (s[x].find(y) != s[x].end() || s[y].find(x) != s[y].end())) {
        puts("NO");
      } else {
        puts("YES");
        if (x != y) merge(x, y);
      }
    } else {
      if (x == y) {
        puts("NO");
      } else {
        puts("YES");
        s[x].insert(y);
        s[y].insert(x);
      }
    }
  }
  return 0;
}
