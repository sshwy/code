#include <bits/stdc++.h>
#define FOR(a, b, c) for (int a = (int)(b); a <= (int)(c); a++)
using namespace std;

const int N = 3e5 + 5;

namespace F {
  int ti[N], a[N], b[N];
}
long long f(int i, int x) {
  if (x <= F::ti[i])
    return F::a[i] * 1ll * (F::ti[i] - x);
  else
    return F::b[i] * 1ll * (x - F::ti[i]);
}

int n, m;
int nex[N];
int lft[N];

struct disjoint {
  int fa[N];
  void init(int n) { FOR(i, 0, n) fa[i] = i; }
  int get(int x) { return fa[x] == x ? x : fa[x] = get(fa[x]); }
} d;

vector<int> g[N];
void ae(int u, int v) {
  printf("ae %d %d\n", u, v);
  g[v].push_back(u);
}

typedef pair<int, int> pii;
priority_queue<pii> q, r;
bool cut[N], isLeaf[N];

struct Function {
  int hillTop() {}
} func[N];

bool dfsMerge(int u, int p, int U) {
  bool res = 1;
  if (p) {
    if(
    ...
    cut[u] = 1;
  }
  if (isLeaf[u]) return;
  for (unsigned i = 0; i < g[u].size(); i++) {
    int v = g[u][i];
    if (!cnt[v]) dfsMerge(v, u, U);
  }
}
void work() {
  pii pu = q.top();
  q.pop();
  int u = pu.second;
  dfsMerge(u, 0, u);
}
int main() {
  scanf("%d%d", &n, &m);
  FOR(i, 1, n) scanf("%d%d%d", F::ti + i, F::a + i, F::b + i);
  FOR(i, 1, n) lft[i] = i;
  FOR(i, 1, m) {
    int l, r;
    scanf("%d%d", &l, &r);
    lft[r] = min(lft[r], l);
  }
  d.init(n);
  FOR(i, 1, n) {
    if (lft[i] < i) {
      for (int j = d.get(i - 1); j >= lft[i]; j = d.get(j)) {
        ae(j, i); // j -> i
        d.fa[j] = j - 1;
      }
    }
  }
  FOR(i, 1, n) q.push(make_pair(F::ti[i], i));
  while (!q.empty()) {
    work();
    while (!q.empty() && !r.empty() && q.top() == r.top()) q.pop(), r.pop();
    while (!q.empty() && cut[q.top().second]) q.pop();
  }
  return 0;
}

/*
 * 1. 找函数单峰位置
 * 2. ~~从大到小排序~~ 可以直接并查集
 * 3. dfs 建新树。如果建不出新树，就是一个合法情况。
 */
