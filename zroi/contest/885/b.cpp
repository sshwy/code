// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

const int P = 100000007;
long long n, k, ans;

void solve1() {
  FOR(i, 1, n) {
    map<int, int> mp;
    FOR(j, 0, k) {
      int x = i + j;
      for (int i = 2; i * i <= x; i++) {
        if (x % i) continue;
        int c = 0;
        while (x % i == 0) c++, x /= i;
        if (!mp.count(i))
          mp[i] = c;
        else
          mp[i] = max(mp[i], c);
      }
      if (x > 1) {
        if (!mp.count(x))
          mp[x] = 1;
        else
          mp[x] = max(mp[x], 1);
      }
    }
    long long s = 1;
    for (auto pr : mp) { FOR(i, 1, pr.second) s = s * pr.first % P; }
    ans = (ans + s) % P;
  }
  cout << ans << endl;
  exit(0);
}

void solve2() {
  n %= P;
  long long ans = n * (n + 1) / 2 % P;
  cout << ans << endl;
  exit(0);
}

int pw(int a, int m) {
  int res = 1;
  while (m) m & 1 ? res = 1ll * res * a % P : 0, a = 1ll * a * a % P, m >>= 1;
  return res;
}
void solve3() {
  n %= P;
  long long ans = n * (n + 1) / 2 % P, I6 = pw(6, P - 2);
  ans = (ans + n * (n + 1) % P * (n * 2 + 1) % P * I6 % P) % P;
  cout << ans << endl;
  exit(0);
}

int main() {
  cin >> n >> k;
  if (k == 0) solve2();
  if (k == 1) solve3();
  if (n <= 1e5) solve1();
  return 0;
}
