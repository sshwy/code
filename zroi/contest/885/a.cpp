// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

const int N = 5005;

long long n, L, R, P;
long long s[N][N];

int main() {
  cin >> n >> L >> R >> P;

  s[0][0] = 1;
  FOR(i, 1, N - 1) {
    FOR(j, 1, i) { s[i][j] = (s[i - 1][j - 1] + s[i - 1][j] * (i - 1)) % P; }
  }

  long long ans = 0;
  FOR(i, L, R) ans = (ans + s[n][i]) % P;

  cout << ans << endl;
  return 0;
}
