#include <bits/stdc++.h>
#define ci const int &
using namespace std;
const int N = 1e6 + 5, INF = 1e9 + 5;
struct edge {
  int t, nxt;
} e[N << 1];
struct Edge {
  int u, v, w;
  bool operator<(const Edge &y) const { return w < y.w; }
} ed[N], te[N];
int n, m, a, b, lc, ar[N], f[N], ue[N], be[N], cnt, fa[N], sz[N], tp[N], hs[N],
    dep[N], dfn[N], nw, pm[N], tf[N], t[N << 2], tot, ans[N];
long long prt;
vector<Edge> es[N];
void add(ci x, ci y) { e[++cnt] = (edge){y, be[x]}, be[x] = cnt; }
bool cmp(ci x, ci y) { return ed[x].w < ed[y].w; }
void dfs1(ci x) {
  sz[x] = 1;
  for (int i = be[x]; i; i = e[i].nxt)
    if (e[i].t != fa[x])
      dep[e[i].t] = dep[x] + 1, fa[e[i].t] = x, dfs1(e[i].t), sz[x] += sz[e[i].t];
}
void dfs2(ci x) {
  dfn[x] = ++nw;
  int mx = 0;
  for (int i = be[x]; i; i = e[i].nxt)
    if (e[i].t != fa[x] && sz[e[i].t] > mx) mx = sz[e[i].t], hs[x] = e[i].t;
  if (!mx) return;
  tp[hs[x]] = tp[x], dfs2(hs[x]);
  for (int i = be[x]; i; i = e[i].nxt)
    if (e[i].t != fa[x] && e[i].t != hs[x]) tp[e[i].t] = e[i].t, dfs2(e[i].t);
}
int getf(ci x) { return f[x] == x ? x : f[x] = getf(f[x]); }
void Modify(ci x, ci nl, ci nr, ci l, ci r, ci v) {
  if (nr < l || nl > r) return;
  if (t[x] <= v) return;
  if (l <= nl && nr <= r) return (void)(t[x] = v);
  Modify(x << 1, nl, nl + nr >> 1, l, r, v),
      Modify(x << 1 | 1, (nl + nr >> 1) + 1, nr, l, r, v);
}
int LCA(int x, int y, int &tx, int &ty) {
  tx = x, ty = y;
  while (tp[x] != tp[y]) {
    if (dep[tp[x]] < dep[tp[y]]) swap(x, y);
    x = fa[tp[x]];
  }
  if (dep[x] > dep[y]) x = y;
  while (tp[tx] != tp[x])
    if (fa[tp[tx]] == x) {
      tx = tp[tx];
      break;
    } else
      tx = fa[tp[tx]];
  if (tp[x] == tp[tx]) tx = (x == tx ? x : hs[x]);
  while (tp[ty] != tp[x])
    if (fa[tp[ty]] == x) {
      ty = tp[ty];
      break;
    } else
      ty = fa[tp[ty]];
  if (tp[x] == tp[ty]) ty = (x == ty ? x : hs[x]);
  return x;
}
void Put(int x, ci td, ci v) {
  while (dep[x] >= td) {
    if (dep[tp[x]] >= td)
      pm[x] = min(pm[x], v), x = fa[tp[x]];
    else
      return Modify(1, 1, n, dfn[x] - (dep[x] - td), dfn[x], v);
  }
}
int Que(ci x, ci nl, ci nr, ci id) {
  if (id < nl || id > nr) return INF;
  if (nl == nr) return t[x];
  return min(min(Que(x << 1, nl, nl + nr >> 1, id),
                 Que(x << 1 | 1, (nl + nr >> 1) + 1, nr, id)),
      t[x]);
}
void Getans(ci x) {
  int tc = !!fa[x];
  for (int i = be[x]; i; i = e[i].nxt)
    if (e[i].t != fa[x]) Getans(e[i].t);
  pm[x] = min(pm[x], pm[hs[x]]), tf[x] = min(pm[x], Que(1, 1, n, dfn[x])),
  f[fa[x]] = fa[x];
  nw = 0;
  for (int i = be[x]; i; i = e[i].nxt)
    if (e[i].t != fa[x])
      ++tc, f[e[i].t] = e[i].t,
            tf[e[i].t] < INF ? te[++nw] = (Edge){e[i].t, fa[x], tf[e[i].t]}, 0 : 0;
  for (int i = 0; i < es[x].size(); ++i) te[++nw] = es[x][i];
  sort(te + 1, te + nw + 1);
  for (int i = 1; i <= nw; ++i)
    if ((a = getf(te[i].u)) != (b = getf(te[i].v)))
      f[a] = b, ans[x] = max(ans[x], te[i].w), --tc;
  if (tc != 1) ans[x] = -1;
}
char ch;
void scan(int &x) {
  x = 0;
  while (!isdigit(ch = getchar()))
    ;
  while (isdigit(ch)) x = (x << 3) + (x << 1) + ch - '0', ch = getchar();
}
int main() {
  scan(n), scan(m);
  for (int i = 1; i <= n; ++i) f[i] = i;
  for (int i = 1; i <= m; ++i) scan(ed[i].u), scan(ed[i].v), scan(ed[i].w), ar[i] = i;
  sort(ar + 1, ar + m + 1, cmp);
  for (int i = 1; i <= m; ++i)
    if ((a = getf(ed[ar[i]].u)) != (b = getf(ed[ar[i]].v)))
      f[a] = b, tot = max(tot, ed[ar[i]].w), ue[ar[i]] = 1;
  for (int i = 1; i <= m; ++i)
    if (ue[i]) add(ed[i].u, ed[i].v), add(ed[i].v, ed[i].u);
  dfs1(1), tp[1] = 1, dfs2(1);
  if (nw != n) return printf("-%d", n), 0;
  for (int i = 1; i <= (n << 2); ++i) t[i] = INF;
  for (int i = 0; i <= n; ++i) pm[i] = INF, ans[i] = tot;
  for (int i = 1; i <= m; ++i)
    if (!ue[i]) {
      lc = LCA(ed[i].u, ed[i].v, a, b);
      Put(ed[i].v, dep[lc] + 2, ed[i].w), Put(ed[i].u, dep[lc] + 2, ed[i].w);
      if (lc != ed[i].u && lc != ed[i].v) es[lc].push_back((Edge){a, b, ed[i].w});
    }
  Getans(1);
  for (int i = 1; i <= n; ++i) prt += ans[i];
  printf("%lld", prt);
  return 0;
}
