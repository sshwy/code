// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

const int N = 2e5 + 5, P = 1e9 + 9;

struct Hash {
  int h[3];
  Hash(int v) { h[0] = h[1] = h[2] = v; }
  Hash() {}
};
Hash operator+(Hash a, Hash b) {
  Hash c;
  c.h[0] = (a.h[0] + b.h[0]) % P;
  c.h[1] = 1ll * a.h[1] * b.h[1] % P;
  c.h[2] = a.h[2] ^ b.h[2];
  return c;
}
bool operator==(Hash a, Hash b) {
  FOR(i, 0, 2) if (a.h[i] != b.h[i]) return false;
  return true;
}
bool operator<(Hash a, Hash b) {
  FOR(i, 0, 2) if (a.h[i] != b.h[i]) return a.h[i] < b.h[i];
  return false;
}
bool operator<=(Hash a, Hash b) { return a < b || a == b; }

int n;
vector<int> g1[N], g2[N];
vector<Hash> v1, v2;
Hash h1[N], h2[N];

void dfs1(int u, int p) {
  h1[u] = Hash(u);
  for (int v : g1[u])
    if (v != p) dfs1(v, u), h1[u] = h1[u] + h1[v];
  if (u != 1) v1.pb(h1[u]);
}
void dfs2(int u, int p) {
  h2[u] = Hash(u);
  for (int v : g2[u])
    if (v != p) dfs2(v, u), h2[u] = h2[u] + h2[v];
  if (u != 1) v2.pb(h2[u]);
}

int main() {
  scanf("%d", &n);
  FOR(i, 1, n - 1) {
    int u, v;
    scanf("%d%d", &u, &v);
    g1[u].pb(v);
    g1[v].pb(u);
  }
  FOR(i, 1, n - 1) {
    int u, v;
    scanf("%d%d", &u, &v);
    g2[u].pb(v);
    g2[v].pb(u);
  }
  dfs1(1, 0);
  dfs2(1, 0);

  sort(v1.begin(), v1.end());
  sort(v2.begin(), v2.end());

  assert(v1.size() == n - 1 && v2.size() == n - 1);
  int pos = -1;
  int ans = 0;
  FOR(i, 0, n - 2) {
    while (pos + 1 < n - 1 && v2[pos + 1] <= v1[i]) ++pos;
    if (pos >= 0 && v2[pos] == v1[i]) ++ans;
  }
  printf("%d\n", ans);
  return 0;
}
