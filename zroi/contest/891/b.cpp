// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace IO { // require C++11
  const int _BS = 200000;
  char _ob[_BS], *_z = _ob, _ib[_BS], *_x = _ib, *_y = _ib;
  // Input
  char _nc() {
    if (_x == _y) _y = (_x = _ib) + fread(_ib, 1, _BS, stdin);
    if (_x == _y) return EOF;
    return *_x++;
    // return _x==_y&&(_y=(_x=_ib)+fread(_ib,1,_BS,stdin),_x==_y)?EOF:*_x++;
  }
  inline void rd(char &c) { c = _nc(); }
  void rd(char *s) {
    char c = _nc();
    while (!isblank(c) && isprint(c) && c != EOF) *s++ = c, c = _nc();
    *s = 0;
  }
  template <class T> void rd(T &res) { // unsigned/signed int/long long/__int128
    res = 0;
    char c = _nc(), flag = 0;
    while (!isdigit(c) && c != '-') c = _nc();
    if (c == '-') flag = 1, c = _nc();
    while (isdigit(c)) res = res * 10 + c - '0', c = _nc();
    if (flag) res = -res;
  }
  template <class T, class... A> void rd(T &hd, A &...rs) { rd(hd), rd(rs...); }
  // Output
  inline void flush() { fwrite(_ob, 1, _z - _ob, stdout), _z = _ob; }
  inline void wrch(char x) {
    if (_z == _ob + _BS - 1) flush();
    *_z = x, ++_z;
  }
  void wr(char *s) {
    while (*s) wrch(*s), ++s;
  }
  void wr(const char *s) {
    while (*s) wrch(*s), ++s;
  }
  template <class T> void wr(T x) { // unsigned/signed int/long long/__int128
    if (x == 0) return wrch('0'), void();
    if (x < 0) wrch('-'), x = -x;
    int _t[50], _lt = 0;
    while (x) _t[++_lt] = x % 10, x /= 10;
    while (_lt > 0) wrch(_t[_lt] + '0'), --_lt;
  }
  inline void wr(char x) { wrch(x); }
  template <class T, class... A> void wr(T hd, A... rs) { wr(hd), wr(rs...); }
  template <class... A> void wrln(A... ar) { wr(ar...), wrch('\n'); }
  struct _Flusher {
    ~_Flusher() { flush(); }
  } _flusher;
} // namespace IO
using IO::rd;
using IO::wrln;

const int N = 1e6 + 6;

int n, m, ans[N];

namespace CBT {
  struct qxx {
    int nex, t;
  } e[N * 4];
  int h[N * 2], le = 1;
  int father[N * 2], dg[N * 2], fa[N * 2];
  int tag[N * 2], tottag;
  void ae(int u, int v) {
    dg[u]++;
    dg[v]++;
    e[++le] = {h[u], v}, h[u] = le;
    e[++le] = {h[v], u}, h[v] = le;
  }
  int get(int u) { return fa[u] == u ? u : fa[u] = get(fa[u]); }
  void dfs(int u, int p, int base_val) {
    father[u] = p;
    fa[u] = u;
    if (dg[u] == 1 && u <= n) ans[u] = base_val;
    for (int i = h[u]; i; i = e[i].nex) {
      int v = e[i].t;
      if (v != p) dfs(v, u, base_val);
    }
  }
  int jmp(int u) {
    assert(get(u) == u);
    if (father[u]) return get(father[u]);
    return 0;
  }
  void compress(int u, int v, int val) {
    assert(u <= n && v <= n);
    assert(get(u) == u && get(v) == v);
    if (jmp(u) == jmp(v)) return;
    int c = ++tottag, z = -1;
    {
      int x = u, y = v;
      while (true) {
        assert(get(x) == x && get(y) == y);
        if (x && tag[x] == c) {
          z = x;
          break;
        }
        tag[x] = c, x = jmp(x);
        if (y && tag[y] == c) {
          z = y;
          break;
        }
        tag[y] = c, y = jmp(y);
      }
    }
    assert(get(z) == z);
    int ffa = z <= n ? get(z) : get(father[z]);
    assert(z != -1);
    int las = -1;
    for (int x = u; x != z; x = jmp(x)) {
      if (x <= n) {
        if (x == u) continue;
        dg[x]--;
        assert(dg[x] >= 1);
        if (dg[x] == 1) { ans[x] = val; }
      } else {
        if (las != -1) { fa[get(las)] = get(x); }
        las = x;
      }
    }
    for (int x = v; x != z; x = jmp(x)) {
      if (x <= n) {
        if (x == v) continue;
        dg[x]--;
        assert(dg[x] >= 1);
        if (dg[x] == 1) { ans[x] = val; }
      } else {
        if (las != -1) { fa[get(las)] = get(x); }
        las = x;
      }
    }
    assert(las != -1);
    father[get(las)] = get(ffa);
    if (z <= n) {
      if (z == u || z == v)
        ;
      else {
        dg[z]--;
        assert(dg[z] >= 1);
        if (dg[z] == 1) { ans[z] = val; }
      }
    } else {
      fa[get(las)] = get(z);
    }
  }
} // namespace CBT

struct qxx {
  int nex, t, v;
} e[N * 2];
int h[N], le = 1;
void ae(int u, int v, int w) { e[++le] = {h[u], v, w}, h[u] = le; }

int dfn[N], totdfn, low[N], stk[N], tp, totsq;
void dfs(int u) {
  low[u] = dfn[u] = ++totdfn;
  stk[++tp] = u;
  for (int i = h[u]; i; i = e[i].nex) {
    int v = e[i].t;
    if (dfn[v])
      low[u] = min(low[u], dfn[v]);
    else {
      dfs(v), low[u] = min(low[u], low[v]);
      if (dfn[u] <= low[v]) {
        ++totsq;
        CBT::ae(u, totsq + n);
        do { CBT::ae(stk[tp], totsq + n); } while (stk[tp--] != v);
      }
    }
  }
}

struct Edge {
  int u, v, w;
  static bool byW(Edge x, Edge y) { return x.w < y.w; }
};

int fa[N];
vector<Edge> edges;

int get(int u) { return fa[u] == u ? u : fa[u] = get(fa[u]); }

int main() {
  rd(n, m);
  FOR(i, 1, m) {
    int u, v, w;
    rd(u, v, w);
    edges.pb({u, v, w});
  }

  sort(edges.begin(), edges.end(), Edge::byW);
  FOR(i, 1, n) fa[i] = i;
  int totc = n, base_val = -1;
  for (unsigned i = 0; i < edges.size(); i++) {
    int u = edges[i].u, v = edges[i].v, w = edges[i].w;
    ae(u, v, w);
    ae(v, u, w);
    if (get(u) != get(v)) {
      fa[get(u)] = get(v);
      totc--;
    }
    if (totc == 1) {
      edges.erase(edges.begin(), edges.begin() + i + 1);
      base_val = w;
      break;
    }
  }

  if (totc > 1) {
    wrln(-n);
    return 0;
  }

  dfs(1);

  FOR(i, 1, n) ans[i] = -1;

  assert(base_val != -1);
  CBT::dfs(1, 0, base_val);

  for (auto e : edges) { CBT::compress(e.u, e.v, e.w); }

  long long totans = 0;
  FOR(i, 1, n) totans += ans[i];

  wrln(totans);
  return 0;
}
