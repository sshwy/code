g++ a.cpp -o .u
g++ a2.cpp -o .v
g++ a_gen.cpp -o .w

while true; do
  ./.w > .i
  ./.u < .i > .x
  ./.v < .i > .y
  if diff -w .x .y; then
    echo AC
  else
    echo WA
  fi
done
