// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

const int N = 30, SZ = 28, STRENGTH = 1,
          SQUARE = (STRENGTH * 2 + 1) * (STRENGTH * 2 + 1);
const double rate = 0.8;

int n;
char a[N][N];
bool b[N][N];

bool A(int i, int j) {
  if (i < 1 || i > SZ || j < 1 || j > SZ) return 1;
  return a[i][j] == '#';
}
bool scan(int i, int j) { // 以区域内的 # 数占比判断是否接纳
  int cnt = 0;
  FOR(x, -STRENGTH, STRENGTH) FOR(y, -STRENGTH, STRENGTH) cnt += A(i + x, j + y);
  return A(i, j) && (cnt >= SQUARE * rate);
}

void go() {
  FOR(i, 1, SZ) scanf("%s", a[i] + 1);
  FOR(i, 1, SZ) FOR(j, 1, SZ) b[i][j] = scan(i, j);
  FOR(i, 1, SZ) {
    FOR(j, 1, SZ) printf("%d", b[i][j]);
    // puts("");
  }
  // puts("");

  // FOR(i, 1, SZ) {
  //   FOR(j, 1, SZ) printf("%c", b[i][j] ? '1' : ' ');
  //   puts("");
  // }
  // FOR(j, 1, SZ) putchar('-');
  // puts("");
}

int main() {
  scanf("%d", &n);
  FOR(i, 1, n) go();
  return 0;
}
