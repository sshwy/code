// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

typedef long long LL;
const int SZ = 5e5 + 5, P = 1e9 + 7, E = 50;

int pw(int a, int m) {
  int res = 1;
  while (m) m & 1 ? res = 1ll * res * a % P : 0, a = 1ll * a * a % P, m >>= 1;
  return res;
}

int k, kth_pw[E];

int pn[SZ], lp;
LL n, sqrt_n;
bool co[SZ];
void sieve(int lim) {
  co[0] = co[1] = 1;
  FOR(i, 2, lim) {
    if (!co[i]) pn[++lp] = i;
    FOR(j, 1, lp) {
      if (i * pn[j] > lim) break;
      co[i * pn[j]] = 1;
      if (i % pn[j] == 0) break;
    }
  }
}

int id[2][SZ], tot;
LL val[SZ]; // val 是递减的
inline int I(LL x) { return x <= sqrt_n ? id[0][x] : id[1][n / x]; }
void init() {
  sqrt_n = sqrt(n) + 5;
  sieve(sqrt_n);
  for (LL pos = 1, nex; pos <= n; pos = nex + 1) {
    nex = n / (n / pos);
    LL w = n / pos;
    ++tot;
    val[tot] = w;
    w <= sqrt_n ? id[0][w] = tot : id[1][n / w] = tot;
  }
  FOR(i, 1, tot) assert(I(val[i]) == i);
  // FOR(i,1,tot)printf("val[%d]=%d\n",i,val[i]);
}

inline int H(int i) { return 1ll * kth_pw[2] * i % P; }
inline int Sf_1(LL x) { return x % P; }

int g[SZ];
void calc_g() {
  FOR(i, 1, tot) { // 当 i=0
    g[i] = (Sf_1(val[i]) - 1 + P) % P;
    // printf("g(0, %lld) = %d\n", val[i], g[i]);
  }
  FOR(i, 1, lp) { // pn[i]
    FOR(j, 1, tot) {
      if (1ll * pn[i] * pn[i] > val[j]) break;
      int k = I(val[j] / pn[i]);
      g[j] = (0ll + g[j] - (g[k] - (i - 1))) % P, g[j] = (g[j] + P) % P;
    }
    // printf("i %d pn[j] = %d\n", i, pn[i]);
    // FOR(j, 1, tot) {
    //   printf("g(%d, %lld) = %d\n", i, val[j], g[j]);
    // }
  }
}
inline int G(LL x) { return 1ll * kth_pw[2] * g[I(x)] % P; }
inline int F(LL pj, int e) { return kth_pw[e + 1]; }
int S(const int i, const LL m) {
  if (m < pn[i] || m <= 1) return 0;
  LL res = (0ll + G(m) - H(i - 1) + P) % P;
  // printf("G(%lld) = %d\n", m, G(m));
  FOR(j, i, lp) {
    if (pn[j] * 1ll * pn[j] > m) break;
    LL pje = 1, pje1 = pn[j];
    FOR(e, 1, n) {
      pje *= pn[j], pje1 *= pn[j];
      if (pje1 > m) break;
      res = (res + 1ll * F(pn[j], e) * S(j + 1, m / pje) % P + F(pn[j], e + 1)) % P;
    }
  }
  // printf("pn[%d] = %d\n", i, pn[i]);
  // printf("S(%d, %lld) = %lld\n", i, m, res);
  return res;
}
int Min_25() {
  init();
  calc_g();
  return (S(1, n) + 1) % P;
}

int main() {
  scanf("%lld%d", &n, &k);
  FOR(i, 0, E - 1) kth_pw[i] = pw(i, k);
  printf("%d\n", Min_25());
  return 0;
}
