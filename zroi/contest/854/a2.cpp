// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

const int P = 1e9 + 7;
void add(int &x, int y) { x = (x + y) % P; }
int pw(int a, int m) {
  int res = 1;
  while (m) m & 1 ? res = 1ll * res * a % P : 0, a = 1ll * a * a % P, m >>= 1;
  return res;
}

int n, k, ans;

int f(int i) {
  int res = 1;
  for (int j = 2; j * j <= i; ++j) {
    if (i % j) continue;
    int e = 0;
    while (i % j == 0) i /= j, ++e;
    res = 1ll * res * pw(e + 1, k) % P;
  }
  if (i > 1) res = 1ll * res * pw(2, k) % P;
  return res;
}

int main() {
  scanf("%d%d", &n, &k);
  FOR(i, 1, n) add(ans, f(i));
  printf("%d\n", ans);
  return 0;
}
