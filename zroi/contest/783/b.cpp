// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA
const int N = 3e5 + 5;

int n, k, H;
int a[N];
bool ok[N];

struct qxx {
  int nex, t;
} e[N * 2];
int h[N], le = 1;
void ae(int u, int v) { e[++le] = (qxx){h[u], v}, h[u] = le; }
void ab(int u, int v) { ae(u, v), ae(v, u); }

void dfs(int u, int p, int dist = 0) {
  ok[u] = 1;
  if (dist == H) return;
  for (int i = h[u]; i; i = e[i].nex) {
    int v = e[i].t;
    if (v == p) continue;
    dfs(v, u, dist + 1);
  }
}

long long calc(int x) {
  x = min(H, x);
  x = H - x;
  return H / k + !!(H % k) - (x / k + !!(x % k));
}
int sz[N], cut[N], sm[N];

int valCore(int u, const int T) { return max(sm[u], T - sz[u]); }
int Core(int u, int p, const int T) {
  int r = u;
  for (int i = h[u]; i; i = e[i].nex) {
    int v = e[i].t;
    if (v == p || cut[v]) continue;
    int z = Core(v, u, T);
    if (valCore(r, T) > valCore(z, T)) r = z;
  }
  return r;
}
void Resize(int u, int p) {
  sz[u] = 1, sm[u] = 0;
  for (int i = h[u]; i; i = e[i].nex) {
    int v = e[i].t;
    if (v == p || cut[v]) continue;
    Resize(v, u);
    sz[u] += sz[v];
    sm[u] = max(sm[u], sz[v]);
  }
}
long long c[N], ans[N];
long long ck[N];
void rangeAdd(int l, int r, long long v) {
  if (l > r) return;
  // printf("  rangeAdd %d %d %lld\n",l,r,v);
  // FOR(i,l,r)c[i] += v;
  c[l] += v;
  c[r + 1] -= v;
}
void kRangeAdd(int l, int r, long long v, const int Lim) {
  if (l > r) return;
  int cnt = (r - l) / k;
  ck[l] += v;
  ck[l + cnt * k + k] -= v;
  c[r + 1] -= v * (cnt + 1);
}
void dfsAddTag(int u, int p, int dist, const int Lim) {
  // printf("dfsAddTag %d %d %d\n",u,p,dist);
  if (dist <= H) {
    int LLim = min(Lim, H - dist);
    // calc(dist+1-1) * a[u] range(dep): [1, 2, ..., H-dist]
    rangeAdd(0, LLim, calc(dist - 1) * a[u]);
    // dist
    int t = (H - (dist - 1) - 1) / k * k + 1;
    t = H - (t - 1);
    // for(int i=t+1-dist; i<=H-dist; i+=k) rangeAdd(i,min(Lim, H-dist), a[u]);
    kRangeAdd(t + 1 - dist, LLim, a[u], Lim);
    rangeAdd(LLim + 1, Lim, calc(H) * a[u]);
  } else {
    rangeAdd(0, Lim, calc(H) * a[u]);
  }
  for (int i = h[u]; i; i = e[i].nex) {
    int v = e[i].t;
    if (v == p || cut[v]) continue;
    dfsAddTag(v, u, dist + 1, Lim);
  }
}
void dfsAccumulateAnsFromTag(int u, int p, int dist, int coef) {
  ans[u] += c[dist] * coef;
  // printf("Tag: ans[%d] += %lld, c[%d] = %lld\n",u,c[dist]*coef,
  // dist,c[dist]);
  for (int i = h[u]; i; i = e[i].nex) {
    int v = e[i].t;
    if (v == p || cut[v]) continue;
    dfsAccumulateAnsFromTag(v, u, dist + 1, coef);
  }
}
void dfsAccumulateAns(int u, int p, long long val, int dist) {
  ans[u] += calc(max(dist - 1, 0)) * val;
  // printf("ans[%d] += %lld * %lld\n",u,calc(max(dist-1,0)) , val);
  for (int i = h[u]; i; i = e[i].nex) {
    int v = e[i].t;
    if (v == p || cut[v]) continue;
    dfsAccumulateAns(v, u, val, dist + 1);
  }
}
void resetTag(const int lim) {
  memset(c, 0, sizeof(c[0]) * (lim + 1));
  memset(ck, 0, sizeof(ck[0]) * (lim + 1));
}
void procTag(const int lim) {
  FOR(i, k, lim) ck[i] += ck[i - k];
  FOR(i, 1, lim) c[i] += ck[i];
  FOR(i, 1, lim) c[i] += c[i - 1];
}

void work(int core) {
  // printf("work core = %d\n",core);
  resetTag(sz[core] + 5);
  for (int i = h[core]; i; i = e[i].nex) {
    int v = e[i].t;
    if (cut[v]) continue;
    dfsAddTag(v, core, 1, sz[core] + 5);
  }
  procTag(sz[core] + 5);
  dfsAccumulateAnsFromTag(core, core, 0, 1);

  for (int i = h[core]; i; i = e[i].nex) {
    int v = e[i].t;
    if (cut[v]) continue;
    resetTag(sz[v] + 5);
    dfsAddTag(v, core, 1, sz[v] + 5);
    procTag(sz[v] + 5);
    dfsAccumulateAnsFromTag(v, core, 1, -1);
  }
  // printf("dfsAccumulateAns core=%d\n",core);
  dfsAccumulateAns(core, core, a[core], 0);
  // printf("dfsAccumulateAns core=%d done\n",core);
}
void Solve(int u, int p) {
  Resize(u, p);
  int core = Core(u, p, sz[u]);

  work(core);

  cut[core] = 1;
  for (int i = h[core]; i; i = e[i].nex) {
    int v = e[i].t;
    if (cut[v]) continue;
    Solve(v, core);
  }
}

int main() {
  scanf("%d%d%d", &n, &k, &H);
  long long s = 0;
  FOR(i, 1, n) scanf("%d", &a[i]), s += a[i];
  FOR(i, 1, n - 1) {
    int u, v;
    scanf("%d%d", &u, &v);
    ab(u, v);
  }
  dfs(1, 0);

  Solve(1, 0);

  long long as = 0;
  FOR(i, 1, n) if (ok[i]) {
    // printf("ans %d : %lld\n",i,ans[i]);
    as = max(as, ans[i]);
  }
  as = calc(H) * s - as;
  printf("%lld\n", as);
  return 0;
}
/*
 * 一条链
 * 考虑分治
 * 如果两个点的距离是 d，那么设d'=max(d-1,0)
 * 则贡献是关于d'的函数
 * 可以两个数组，一个是后缀加，还有一个是标记的k间隔的差分
 */
