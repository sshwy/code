// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA
const int N = 1e5 + 5;
int n, m, k;
long long a[N];

int main() {
  scanf("%d%d%d", &n, &m, &k);
  FOR(i, 1, n) scanf("%lld", &a[i]);
  FOR(i, 1, m) {
    int op, l, r, c;
    scanf("%d%d%d", &op, &l, &r);
    if (op == 1) {
      long long s = 0;
      FOR(i, l, r) s += a[i];
      printf("%lld\n", s);
    } else if (op == 2) {
      scanf("%d", &c);
      FOR(i, l, r) a[i] += c;
    } else {
      vector<pair<long long, int>> v, q;
      FOR(i, l, r) v.push_back(make_pair(a[i], i));

      sort(v.begin(), v.end());

      unsigned p1 = 0, p2 = 0;

      while (p1 < v.size() || p2 <= q.size()) {
        while (p1 < v.size() && v[p1].first != a[v[p1].second]) ++p1;
        while (p2 < q.size() && q[p2].first != a[q[p2].second]) ++p2;

        int idx = -1;
        if (p1 < v.size() || p2 < q.size()) {
          if (p1 < v.size() && (p2 >= q.size() || v[p1].first < q[p2].first))
            idx = v[p1].second, ++p1;
          else
            idx = q[p2].second, ++p2;
        } else
          break;

        if (idx - 1 >= l && a[idx] + k < a[idx - 1])
          a[idx - 1] = a[idx] + k, q.push_back(make_pair(a[idx - 1], idx - 1));
        if (idx + 1 <= r && a[idx] + k < a[idx + 1])
          a[idx + 1] = a[idx] + k, q.push_back(make_pair(a[idx + 1], idx + 1));
      }
    }
  }
  return 0;
}
