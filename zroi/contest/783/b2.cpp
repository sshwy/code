// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA
const int N = 1e5 + 5;

int n, k, h;
int a[N];
vector<int> g[N];
bool ok[N];

void dfs(int u, int p, int dist = 0) {
  ok[u] = 1;
  if (dist == h) return;
  for (unsigned i = 0; i < g[u].size(); i++) {
    int v = g[u][i];
    if (v == p) continue;
    dfs(v, u, dist + 1);
  }
}

long long calc(int x) {
  x = min(x, h);
  x = h - x;
  return h / k + !!(h % k) - (x / k + !!(x % k));
}

long long dfs2(int u, int p, int dist = 0) { //最多不与多少怪兽战斗
  int cnt = max(dist - 1, 0);
  long long res = calc(cnt) * a[u];
  for (unsigned i = 0; i < g[u].size(); i++) {
    int v = g[u][i];
    if (v == p) continue;
    res += dfs2(v, u, dist + 1);
  }
  return res;
}

int main() {
  scanf("%d%d%d", &n, &k, &h);
  long long s = 0;
  FOR(i, 1, n) scanf("%d", &a[i]), s += a[i];
  FOR(i, 1, n - 1) {
    int u, v;
    scanf("%d%d", &u, &v);
    g[u].pb(v);
    g[v].pb(u);
  }
  dfs(1, 0);
  long long ans = 0;
  FOR(i, 1, n) if (ok[i]) {
    long long t = dfs2(i, 0);
    ans = max(ans, t);
    // printf("ans %d : %lld\n",i,t);
  }
  ans = calc(h) * s - ans;
  printf("%lld\n", ans);
  return 0;
}
/*
 * 一条链
 * 考虑分治
 * 如果两个点的距离是 d，那么设d'=max(d-1,0)
 * 则贡献是关于d'的函数
 * 可以两个数组，一个是后缀加，还有一个是标记的k间隔的差分
 */
