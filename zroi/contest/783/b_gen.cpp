// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

using namespace RA;

int main() {
  srand(clock() + time(0));
  int n = rnd(1, 1000), k = rnd(1, 1000), h = rnd(1, n * 2);
  // n = 1e5, k = rnd(1,n), h = rnd(1,n*2);
  printf("%d %d %d\n", n, k, h);
  FOR(i, 1, n) printf("%d%c", rnd(1, 10000), " \n"[i == n]);
  FOR(i, 1, n - 1) printf("%d %d\n", rnd(1, i), i + 1);
  return 0;
}
