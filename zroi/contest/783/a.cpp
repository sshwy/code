// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

int main() { return 0; }

/*
 * Bob 的策略要在最坏情况下次数最少
 * f(第一轮说谎) 和 f(第一轮不说谎) 取 max
 * 首先 当 alice 不能说谎的时候，则轮数是确定的（log2）
 * 而当某一次 alice 说谎后，bob 是不知道他有没有说谎的
 * bob 唯一的确认方法就是再问一次同样的问题
 * 但这个方法有缺陷：如果 alice 第一次不 fake，第次 fake，就不对了
 * 所以 bob 得问 3 次才知道有没有说谎
 * 当然，如果第一次和第二次的结果一样，那么就不用问了，肯定真的
 * 对于区间 [L,R]，bob 问 mid，假设 y<=mid
 * 如果alice撒谎，那么bob就会走到一个错误的分支（问log次），然后再重新问
 * 如果alice不撒谎，就是个子问题
 * 但问题在于
 * 1. bob的回溯策略是否是最优的
 *
 *
 *
 *
 *
 *
 *
 *
 * 1 2 3 4 5
 */
