// by Sshwy
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA
const int N = 1002, P = 1e9 + 7;
int n;
vector<pair<int, int>> v;
int f[N][N], g[N][N];
void add(int &x, int y) { x = (x + y) % P; }
int main() {
  scanf("%d", &n);
  FOR(i, 1, n) {
    int A, B;
    scanf("%d%d", &A, &B);
    v.pb({A, 1});
    v.pb({B, 0});
  }
  sort(v.begin(), v.end());
  v.pb({0, 0});
  reverse(v.begin(), v.end());
  f[0][0] = 1;
  FOR(i, 1, n * 2) {
    FOR(j, 0, i) {
      if (v[i].second == 1) { // A
        add(f[i][j], f[i - 1][j]);
        add(f[i][j - 1], f[i - 1][j] * 1ll * j % P);
        add(g[i][j - 1], g[i - 1][j] * 1ll * j % P);
      } else { // B
        add(f[i][j + 1], f[i - 1][j]);
        add(g[i][j + 1], g[i - 1][j]);
        add(g[i][j], f[i - 1][j]);
        add(g[i][j], g[i - 1][j]);
      }
    }
  }
  int ans = (f[n * 2][0] + g[n * 2][0]) % P;
  printf("%d\n", ans);
  return 0;
}
