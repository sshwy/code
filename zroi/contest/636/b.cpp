// by Sshwy
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA
const int N = 1e6 + 5, M = 4e6 + 5, P = 1e9 + 7;
int n, m, L, a[N], b[N], v[M], fa[N], f1[N], f2[N], g1[N], g2[N], c1[N], c2[N],
    ans[N];
vector<int> g[N];

void add(int &x, int y) { x = (0ll + x + y) % P; }
void dfs(int u) {
  f1[u] = c1[a[u] + 1];
  f2[u] = c1[b[u] + 1];
  // printf("dfs %d\n",u);
  // printf("%d\n",f1[u]);
  add(c1[b[u]], f2[u]);
  int t1 = a[u] == 1 ? 0 : c2[a[u] - 1], t2 = b[u] == 1 ? 0 : c2[b[u] - 1];
  for (int v : g[u]) dfs(v);
  g1[u] = (c2[a[u] - 1] - t1 + P) % P;
  g2[u] = (c2[b[u] - 1] - t2 + P) % P;
  add(c1[b[u]], P - f2[u]);
  add(c2[a[u]], g1[u]);
}
int main() {
  scanf("%d%d%d", &n, &m, &L);
  FOR(i, 2, n) scanf("%d", &fa[i]), g[fa[i]].pb(i);
  FOR(i, 1, n) scanf("%d", &a[i]);
  FOR(i, 1, m) scanf("%d", &v[i]);

  c1[L + 1] = 1;
  c2[0] = 1;
  FOR(i, 1, n) b[i] = a[i];
  dfs(1);
  FOR(i, 1, n) if (a[i] == 1) add(ans[0], f1[i]);

  for (int i = 0; i < m; i += n) {
    if (i + n > m) { FOR(j, m + 1, i + n) v[j] = a[j - i]; }
    FOR(j, 1, n) b[j] = v[i + j];
    dfs(1);
    FOR(j, 1, n) {
      ans[i + j] = (f2[j] * 1ll * g2[j] % P - f1[j] * 1ll * g1[j] % P + P) % P;
    }
    FOR(j, 1, n) a[j] = b[j];
  }
  FOR(i, 1, m) add(ans[i], ans[i - 1]);
  int tot = 0;
  // FOR(i,0,m)printf("%d%c",ans[i]," \n"[i==m]);
  FOR(i, 1, m) add(tot, ans[i] * 1ll * i % P);
  printf("%d\n", tot);
  return 0;
}
