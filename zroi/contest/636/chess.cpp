// by Sshwy
#include "c.hpp"
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

char s[20000];
int main() {
  Alice a;
  Bob b;
  int typ, T, v, x, y, bv = -1;
  scanf("%d%d%d%d%d", &typ, &x, &y, &T, &v);
  scanf("%s", s + 1);
  a.initA(typ, v);
  b.initB(typ);
  FOR(i, 1, T) {
    if (s[i] == 'A') {
      int t = a.moveA(x, y);
      if (t == -1) --x;
      if (t == -2) ++x;
      if (t == -3) --y;
      if (t == -4) ++y;
    } else {
      int t = b.moveB(x, y);
      if (t >= 0) {
        bv = t;
        break;
      } else {
        if (t == -1) --x;
        if (t == -2) ++x;
        if (t == -3) --y;
        if (t == -4) ++y;
      }
    }
  }
  if (v == bv)
    return printf("Accepted! (=%d)\n", v), 0;
  else
    return printf("Wrong Answer! (v=%d,bv=%d\n", v, bv), 1;
  return 0;
}
