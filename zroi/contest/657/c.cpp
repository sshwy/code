// by Sshwy
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA
const int N = 2e5 + 5;
int n;
long long k;
pair<long long, long long> a[N];

int main() {
  scanf("%d%lld", &n, &k);
  FOR(i, 1, n) scanf("%lld%lld", &a[i].first, &a[i].second);
  FOR(i, 1, n) {
    int j;
#define check(x, y)                                               \
  {                                                               \
    if (a[x].first * a[y].first + a[x].second * a[y].second == k) \
      return printf("%d %d\n", x, y), 0;                          \
  }
    for (j = 1; j + 3 <= i; j += 4) {
      check(i, j);
      check(i, j + 1);
      check(i, j + 2);
      check(i, j + 3);
    }
    FOR(x, j, i) check(i, x);
  }
  return 0;
}
