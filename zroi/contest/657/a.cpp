// by Sshwy
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA
const int N = 105, M = N * N;

long double T = 1;
const long double dT = 1 - (1e-6);
long long ttt = 0;

int n, m;
vector<int> path[N];
int un[N], lu;
int val[N], on_path[N];
vector<pair<int, int>> G[N];
void add_path(int f, int t, int v) { G[f].pb({t, v}); }
int pre[N], vis[N], ct, co[N][N];
long long tot, ans;
int su;
void make_path(int v) {
  int u;
  vector<int> p;
  for (u = v; pre[u] != -1; u = pre[u]) p.pb(u);
  p.pb(u), assert(u == su);
  for (auto x : p) on_path[x] = v;
  tot += val[u] + val[v];
  ans = max(ans, tot);
  path[u] = path[v] = p;
  reverse(path[u].begin(), path[u].end());
  // for(auto x:p)printf("%d ",x); printf(": %d\n",val[u] + val[v]);
}
void link(int v) {
  int u;
  int z = path[v][path[v].size() - 1];
  vector<int> p;
  for (u = v; pre[u] != -1; u = pre[u]) p.pb(u);
  p.pb(u), assert(u == su);
  reverse(p.begin(), p.end());
  assert(path[v][0] == v);
  for (unsigned i = 1; i < path[v].size(); i++) p.pb(path[v][i]);
  for (auto x : p) on_path[x] = z;
  tot -= val[v], tot += val[u];
  ans = max(ans, tot);
  path[v].clear();
  path[u] = p;
  reverse(p.begin(), p.end());
  path[z] = p;
}
void cut(int v, int end) {
  int e2 = path[end][path[end].size() - 1];
  for (auto u : path[e2]) {
    if (u == v) break;
    on_path[u] = 0;
  }
  path[e2].clear();
  vector<int> p;
  for (auto u : path[end]) {
    if (u == v) break;
    p.pb(u);
  }
  int u;
  for (u = v; pre[u] != -1; u = pre[u]) p.pb(u);
  p.pb(u), assert(u == su); // p: end -> v -> u;
  tot = tot - val[e2] + val[u];
  ans = max(ans, tot);
  path[end] = p;
  reverse(p.begin(), p.end());
  path[u] = p;
  for (auto x : p) on_path[x] = end;
}
int dfs1(int u, int col) { // dfs 的时候，把起点的pre标为-1
  vis[u] = ct;
  random_shuffle(G[u].begin(), G[u].end());
  for (auto pr : G[u]) {
    int v = pr.first, c = pr.second;
    if (c == col || vis[v] == ct) continue;
    pre[v] = u;
    if (on_path[v]) {
      if (val[v]) { //是端点
        assert(path[v].size() >= 2);
        if (c != co[v][path[v][1]]) { //可以连上
          if (val[su] > val[v]) {     //权值增加
            link(v);                  //把su -> v 以及v原本的路径连上
            return 2;                 // Case 2
          }
        }
      } else { // 是中间点
        int end1 = on_path[v], end2 = path[end1][path[end1].size() - 1];
        auto get_color = [](int st, int v) {
          for (unsigned i = 0; i < path[st].size(); i++) {
            if (path[st][i + 1] == v) { return co[path[st][i]][v]; }
          }
          assert(0);
          return -1;
        };
        if (get_color(end1, v) == c) swap(end1, end2);
        // 可以考虑 su -> v -> end1
        assert(get_color(end1, v) != c);
        if (val[su] > val[end2]) { // 赚了
          cut(v, end1); // 把v -> end1的部分截取下来，并与su -> v拼接
          return 3;
        }
      }
    } else {
      if (val[v]) { //是被标记的点
        make_path(v);
        return 1; // Case 1: 找到新路了
      } else {
        int t = dfs1(v, c);
        if (t == 1) return 1;
        if (t == 2) return 2;
        if (t == 3) return 3;
      }
    }
  }
  return 0; //啥也没干
}
int dfs2(int u, int col) { // dfs 的时候，把起点的pre标为-1
  vis[u] = ct;
  random_shuffle(G[u].begin(), G[u].end());
  for (auto pr : G[u]) {
    int v = pr.first, c = pr.second;
    if (c == col || vis[v] == ct) continue;
    pre[v] = u;
    if (on_path[v]) {
      if (val[v]) { //是端点
        assert(path[v].size() >= 2);
        if (c != co[v][path[v][1]]) { //可以连上
          if (val[su] >= val[v]) {    //权值增加
            link(v);                  //把su -> v 以及v原本的路径连上
            return 2;                 // Case 2
          } else {
            long double d = val[v] - val[su];
            if ((long double)RA::rnd(1e9) / 1e9 < exp(-d / (T * 1))) {
              link(v);  //把su -> v 以及v原本的路径连上
              return 2; // Case 2
            }
          }
        }
      } else { // 是中间点
        int end1 = on_path[v], end2 = path[end1][path[end1].size() - 1];
        auto get_color = [](int st, int v) {
          for (unsigned i = 0; i < path[st].size(); i++) {
            if (path[st][i + 1] == v) { return co[path[st][i]][v]; }
          }
          assert(0);
          return -1;
        };
        if (get_color(end1, v) == c) swap(end1, end2);
        // 可以考虑 su -> v -> end1
        assert(get_color(end1, v) != c);
        if (val[su] >= val[end2]) { // 赚了
          cut(v, end1); // 把v -> end1的部分截取下来，并与su -> v拼接
          return 3;
        } else {
          long double d = val[end2] - val[su];
          if ((long double)RA::rnd(1e9) / 1e9 < exp(-d / (T * 1))) {
            cut(v, end1);
            return 3;
          }
        }
      }
    } else {
      if (val[v]) { //是被标记的点
        make_path(v);
        return 1; // Case 1: 找到新路了
      } else {
        int t = dfs2(v, c);
        if (t == 1) return 1;
        if (t == 2) return 2;
        if (t == 3) return 3;
      }
    }
  }
  return 0; //啥也没干
}
long long sW;
int main() {
  srand(clock() + time(0));
  scanf("%d%d", &n, &m);
  FOR(i, 1, m) {
    int u, v, c;
    scanf("%d%d%d", &u, &v, &c);
    add_path(u, v, c);
    add_path(v, u, c);
    co[u][v] = co[v][u] = c;
  }
  int kk;
  scanf("%d", &kk);
  FOR(i, 1, kk) {
    int x, w;
    scanf("%d%d", &x, &w);
    val[x] = w;
    un[++lu] = x;
    sW += w;
  }

  while (T > 0.1) {
    ++ttt;
    if (tot == sW) break;
    // printf("T: %.6Lf tot: %lld\n",T,tot);
    int pos = RA::rnd(lu) + 1;
    int u = un[pos];

    if (on_path[u] == 0) {
      int t;
      do {
        pre[u] = -1;
        ++ct;
        su = u;
        t = dfs1(u, 2);
      } while (on_path[u] == 0 && t);
    }
    if (on_path[u] == 0) {
      pre[u] = -1;
      ++ct;
      su = u;
      dfs2(u, 2);
    } else { //一定概率 删路
      int v = path[u][path[u].size() - 1];
      long double d = val[u] + val[v];
      if ((long double)RA::rnd(1e9) / 1e9 < exp(-d / (1 * T))) {
        for (auto x : path[u]) on_path[x] = 0;
        path[u].clear();
        path[v].clear();
        tot -= val[u] + val[v];
      }
    }

    T *= dT;
  }
  printf("%lld\n", ans);
  return 0;
}
