// by Sshwy
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

const int N = 3e5 + 5;
int n, x;
int a[N];
long long f[N];
long long tot;
int c[N];

void init() {
  FOR(i, 1, n) c[i] = 0;
  tot = 0;
}
long long pre(int x) {
  long long res = 0;
  for (int i = x; i > 0; i -= i & -i) res += c[i];
  return res;
}
void add(int id) {
  tot += pre(a[id]);
  for (int i = a[id]; i <= n; i += i & -i) c[i]++;
}
int main() {
  scanf("%d%d", &n, &x);
  FOR(i, 1, n) scanf("%d", &a[i]);
  f[0] = 0;
  FOR(i, 1, n) {
    init();
    f[i] = 2e18;
    ROF(j, i - 1, 0) {
      add(j + 1);
      f[i] = min(f[i], f[j] + tot + x);
    }
  }
  printf("%lld\n", f[n]);
  return 0;
}
