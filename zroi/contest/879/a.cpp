// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

const int N = 1e5 + 5;

void getMin(int &x, int y) { x = min(x, y); }

int n, m;
int a[2][N], f[N];
long long sa[2][N], b[N], pre[2][N];

int calc(long long k) {
  FOR(_, 0, 1) {
    int pos = 0;
    FOR(i, 1, n) {
      while (pos < i && sa[_][i] - sa[_][pos] > k) ++pos;
      // assert(pos < i);
      pre[_][i] = pos;
    }
  }

  f[0] = 0;
  int pos = 0;
  FOR(i, 1, n) {
    f[i] = 1e9;

    while (pos < i && b[i] - b[pos] > k) ++pos;
    if (pos < i) getMin(f[i], f[pos] + 1);

    int p[2] = {i, i}, c = 0;
    while (c <= m) {
      if (p[0] == p[1]) {
        p[0] = pre[0][p[0]];
        p[1] = pre[1][p[1]];
        c += 2;
      } else if (p[0] > p[1]) {
        p[0] = pre[0][p[0]];
        c++;
      } else {
        p[1] = pre[1][p[1]];
        c++;
      }
      // printf("getMin f[%d] f[%d] + %d\n", i, max(p[0], p[1]), c);
      getMin(f[i], f[max(p[0], p[1])] + c);
      if (p[0] == 0 && p[1] == 0) break;
    }
    if (f[i] > m) return m + 1;
    // printf("f[%d] = %d\n", i, f[i]);
  }
  // printf("calc %lld res %d\n", k, f[n]);
  return f[n];
}

int main() {
  scanf("%d%d", &n, &m);
  FOR(i, 0, 1) FOR(j, 1, n) scanf("%d", &a[i][j]);

  long long l = 0, r;
  FOR(i, 0, 1)
  FOR(j, 1, n) sa[i][j] = sa[i][j - 1] + a[i][j], l = max(l, (long long)a[i][j]);
  FOR(j, 1, n) b[j] = sa[0][j] + sa[1][j];
  r = b[n];

  while (l < r) {
    long long mid = (l + r) >> 1;
    if (calc(mid) <= m)
      r = mid;
    else
      l = mid + 1;
  }
  printf("%lld\n", l);

  return 0;
}
