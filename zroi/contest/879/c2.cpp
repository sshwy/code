// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

const int N = 15, M = 1005, P = 1e9 + 7;
int pw(int a, int m) {
  int res = 1;
  while (m) m & 1 ? res = 1ll * res * a % P : 0, a = 1ll * a * a % P, m >>= 1;
  return res;
}

int n, m, a[N], p[N], inv[M];

int main() {
  cin >> n;
  FOR(i, 1, n) cin >> a[i], m += a[i];
  FOR(i, 1, n) p[i] = i;
  int ans = 0;
  FOR(i, 1, m) inv[i] = pw(i, P - 2);
  do {
    int x = 0;
    int coef = 1;
    FOR(i, 1, n) {
      x += a[p[i]];
      if (i > 1) coef = 1ll * coef * inv[x] % P;
    }
    ans = (ans + coef) % P;
  } while (next_permutation(p + 1, p + n + 1));
  FOR(i, 1, m) ans = 1ll * i * ans % P;
  printf("%d\n", ans);
  return 0;
}
