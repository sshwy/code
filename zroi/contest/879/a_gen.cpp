// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

using namespace RA;

int main() {
  srand(clock() + time(0));
  int n = rnd(1, 100), m = rnd(1, 100), maxw = rnd(1, 100);
  // n = 100000;
  // m = 100;
  // maxw = 1e9;
  printf("%d %d\n", n, m);
  FOR(i, 0, 1) FOR(j, 1, n) printf("%d%c", rnd(1, maxw), " \n"[j == n]);
  return 0;
}
