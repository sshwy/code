// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

typedef vector<int> Poly;
const int N = 55, M = 1005, P = 1e9 + 7;
int pw(int a, int m) {
  int res = 1;
  while (m) m & 1 ? res = 1ll * res * a % P : 0, a = 1ll * a * a % P, m >>= 1;
  return res;
}

void add(int &x, long long y) { x = (x + y) % P; }

int n, m, a[N], ma;

int calc(int i) {
  Poly f(1, 1);
  FOR(j, 1, n) if (i != j) {
    f.resize(f.size() + a[j], 0);
    for (int k = f.size() - 1; k >= 0; k--) {
      f[k] = ((k >= a[j] ? f[k - a[j]] : 0) - f[k] + P) % P;
    }
  }
  int im = pw(m, P - 2);
  int res = 0;
  for (unsigned j = 0; j < f.size(); j++) {
    int coef = pw((1 - 1ll * im * j % P + P) % P, P - 2);
    add(res, 1ll * coef * f[j]);
  }
  res = 1ll * res * im % P * a[i] % P;
  res = 1ll * res * pw(ma, P - 2) % P;
  res = 1ll * res * a[i] % P;
  return res;
}

int main() {
  cin >> n;
  FOR(i, 1, n) cin >> a[i];
  ma = 1;
  FOR(i, 1, n) ma = 1ll * ma * a[i] % P, m += a[i];
  int ans = 0;

  FOR(i, 1, n) add(ans, calc(i));

  FOR(i, 1, m) ans = 1ll * ans * i % P;
  printf("%d\n", ans);
  return 0;
}
