// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

const int N = 1e5 + 5;

void getMin(int &x, int y) { x = min(x, y); }

int n, m;
int a[2][N], f[N];
long long sa[2][N], b[N];

int calc(long long k) {
  f[0] = 0;
  FOR(i, 1, n) {
    f[i] = 1e9;
    int s[2] = {0}, c[2] = {1, 1};
    FOR(j, 1, i) {
      if (b[i] - b[i - j] <= k) getMin(f[i], f[i - j] + 1);
      FOR(_, 0, 1) {
        if (a[_][i - j + 1] > k) return 1e9;
        if (s[_] + a[_][i - j + 1] > k) s[_] = 0, c[_]++;
        s[_] += a[_][i - j + 1];
      }
      getMin(f[i], f[i - j] + c[0] + c[1]);
    }
  }
  // printf("calc %lld res %d\n", k, f[n]);
  return f[n];
}

int main() {
  scanf("%d%d", &n, &m);
  FOR(i, 0, 1) FOR(j, 1, n) scanf("%d", &a[i][j]);

  FOR(i, 0, 1) FOR(j, 1, n) sa[i][j] = sa[i][j - 1] + a[i][j];
  FOR(j, 1, n) b[j] = sa[0][j] + sa[1][j];

  long long l = 0, r = b[n];
  while (l < r) {
    long long mid = (l + r) >> 1;
    if (calc(mid) <= m)
      r = mid;
    else
      l = mid + 1;
  }
  printf("%lld\n", l);

  return 0;
}
