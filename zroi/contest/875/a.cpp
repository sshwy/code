// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

const int L = 1e5 + 5;

char s[L];
int diff[L];
int n;

int main() {
  scanf("%s", s + 1);
  n = strlen(s + 1);
  long long ans = 0;
  int maxI = n <= 5000 ? n : 50;
  FOR(i, 2, maxI) {
    diff[0] = 0;
    FOR(j, 1, n - i) diff[j] = (s[j] != s[j + i]) + diff[j - 1];
    FOR(j, 1, i) {
      int len = 0;
      for (int k = j; k + i * 2 - 1 <= n; k += i) {
        // printf("i %d k %d diff %d\n", i, k, diff[k + i - 1] - diff[k - 1]);
        if (diff[k + i - 1] - diff[k - 1] <= 1) {
          ++len;
        } else {
          len = 0;
        }
        ans += len;
      }
    }
  }
  printf("%lld\n", ans);
  return 0;
}
