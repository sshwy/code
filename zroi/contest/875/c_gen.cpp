// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

using namespace RA;

set<pair<int, int>> s;

int main() {
  srand(clock() + time(0));
  int n = rnd(2, 5), m = rnd(n - 1, min(n * (n - 1) / 2, 6)), typ = 2;
  printf("%d %d %d\n", n, m, typ);
  FOR(i, 1, n) printf("%d%c", rnd(1, 5), " \n"[i == n]);
  FOR(i, 1, n - 1) {
    int u = rnd(1, i);
    s.insert({u, i + 1});
    printf("%d %d %d\n", u, i + 1, rnd(1, 5));
  }
  FOR(i, n, m) {
    int u = rnd(1, n), v = rnd(1, n);
    if (u > v) swap(u, v);
    while (u == v || s.find({u, v}) != s.end()) {
      u = rnd(1, n), v = rnd(1, n);
      if (u > v) swap(u, v);
    }
    s.insert({u, v});
    printf("%d %d %d\n", u, v, rnd(1, 5));
  }
  return 0;
}
