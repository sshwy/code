// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

const int N = 2e5 + 5;

int n, m, k, a[N], fa[N];
struct Edge {
  int u, v, w;
};
vector<Edge> ve;

int get(int u) { return fa[u] == u ? u : fa[u] = get(fa[u]); }

bool byWeight(Edge a, Edge b) { return a.w < b.w; }

void solve0() {
  sort(ve.begin(), ve.end(), byWeight);
  FOR(i, 1, n) fa[i] = i;
  long long ans = 0;
  for (Edge e : ve) {
    int u = e.u, v = e.v;
    if (get(u) != get(v)) {
      ans += e.w;
      fa[get(u)] = get(v);
    }
  }
  printf("%lld\n", ans);
}

pair<int, int> b[N];
set<int> g[N]; // solve 2 : 还没入队的
int tag[N], tottag;
void solve1() {
  FOR(i, 1, n) {
    b[i].first = a[i];
    b[i].second = i;
  }
  sort(b + 1, b + n + 1);
  for (unsigned i = 0; i < ve.size(); i++) {
    g[ve[i].u].insert(i);
    g[ve[i].v].insert(i);
    fa[i] = i;
  }

  long long ans = 0;
  FOR(i, 1, n) {
    int x = ++tottag, u = b[i].second, cnt = 0, last = -1;
    for (int eid : g[u]) {
      cnt += tag[get(eid)] != x;
      tag[get(eid)] = x;
      last = eid;
    }
    // printf("u %d cnt %d\n", u, cnt);
    ans += (cnt - 1) * 1ll * a[u];
    for (int eid : g[u]) {
      if (get(eid) != get(last)) { fa[get(eid)] = get(last); }
    }
  }
  printf("%lld\n", ans);
}

priority_queue<int, vector<int>, greater<int>> q;
int inq[N], dg[N], done[N]; // done: 已经在 MST 里的个数

int count_node(int eid) {
  int x = dg[ve[eid].u] + dg[ve[eid].v] - 2;
  return x;
}
void solve2() {
  long long ans = 0;
  sort(ve.begin(), ve.end(), byWeight);
  for (unsigned i = 0; i < ve.size(); i++) {
    dg[ve[i].u]++;
    dg[ve[i].v]++;
    g[ve[i].u].insert(i);
    g[ve[i].v].insert(i);
  }
  { // init
    int u = ve[0].u, v = ve[0].v, w = ve[0].w;
    inq[0] = true;
    // printf("(%d, %d, %d), count_node: %d, done: %d, %d\n",
    //     u, v, w, count_node(0), done[u], done[v]);
    ans += (count_node(0) - 1) * w;
    done[u]++, done[v]++;
    for (int j : g[u])
      if (!inq[j]) q.push(j), inq[j] = true;
    g[u].clear();
    for (int j : g[v])
      if (!inq[j]) q.push(j), inq[j] = true;
    g[v].clear();
  }
  while (!q.empty()) {
    int eid = q.top();
    q.pop();
    int u = ve[eid].u, v = ve[eid].v, w = ve[eid].w;
    // printf("(%d, %d, %d), count_node: %d, done: %d, %d\n",
    //     u, v, w, count_node(eid), done[u], done[v]);
    ans += 1ll * (count_node(eid) - done[u] - done[v]) * w;
    done[u]++, done[v]++;
    for (int j : g[u])
      if (!inq[j]) q.push(j), inq[j] = true;
    g[u].clear();
    for (int j : g[v])
      if (!inq[j]) q.push(j), inq[j] = true;
    g[v].clear();
  }
  printf("%lld\n", ans);
}

int main() {
  scanf("%d%d%d", &n, &m, &k);
  FOR(i, 1, n) scanf("%d", a + i);
  FOR(i, 1, m) {
    int u, v, w;
    scanf("%d%d%d", &u, &v, &w);
    Edge e = {u, v, w};
    ve.pb(e);
  }
  if (k == 0) solve0();
  if (k == 1) solve1();
  if (k == 2) solve2();
  return 0;
}
