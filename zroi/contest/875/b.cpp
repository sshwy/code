// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

typedef vector<int> Poly;
const int P = 998244353, N = 1005;
Poly operator*(Poly f, Poly g) {
  if (f.empty() || g.empty()) return Poly();
  Poly h(f.size() + g.size() - 1, 0);
  for (unsigned i = 0; i < f.size(); i++) {
    for (unsigned j = 0; j < g.size(); j++) {
      h[i + j] = (h[i + j] + 1ll * f[i] * g[j]) % P;
    }
  }
  return h;
}

int pw(int a, int m) {
  int res = 1;
  while (m) m & 1 ? res = 1ll * res * a % P : 0, a = 1ll * a * a % P, m >>= 1;
  return res;
}
int n, k, sz[N], fac[N], fnv[N];
vector<int> g[N];

void dfs(int u, int p) {
  sz[u] = 1;
  for (int v : g[u])
    if (v != p) {
      dfs(v, u);
      sz[u] += sz[v];
    }
}
int calc(int root) {
  dfs(root, 0);
  Poly sf(1, 1);
  for (int v : g[root]) {
    int lim = min(sz[v], k / 2);
    Poly f(lim + 1, 0);
    int coef = 1;
    FOR(i, 0, lim) {
      f[i] = 1ll * coef * fnv[i] % P;
      coef = 1ll * coef * sz[v] % P;
    }
    sf = sf * f;
  }
  int res = 1ll * sf[k - 1] * root % P * fac[k] % P;
  return res;
}
int main() {
  fac[0] = 1;
  FOR(i, 1, N - 1) fac[i] = 1ll * fac[i - 1] * i % P;
  fnv[N - 1] = pw(fac[N - 1], P - 2);
  ROF(i, N - 1, 1) fnv[i - 1] = 1ll * fnv[i] * i % P;

  scanf("%d%d", &n, &k);
  FOR(i, 1, n - 1) {
    int u, v;
    scanf("%d%d%*d", &u, &v);
    g[u].pb(v);
    g[v].pb(u);
  }
  int ans = 0;
  FOR(i, 1, n) { ans = (ans + calc(i)) % P; }
  FOR(i, 1, k) ans = 1ll * ans * i % P;
  printf("%d\n", ans);
  return 0;
}
