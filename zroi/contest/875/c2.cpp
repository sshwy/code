// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

const int N = 2e5 + 5;

int n, m, k, a[N], fa[N];
struct Edge {
  int u, v, w;
};
vector<Edge> ve;

int get(int u) { return fa[u] == u ? u : fa[u] = get(fa[u]); }

bool byWeight(Edge a, Edge b) { return a.w < b.w; }

void solve0() {
  sort(ve.begin(), ve.end(), byWeight);
  FOR(i, 1, n) fa[i] = i;
  long long ans = 0;
  for (Edge e : ve) {
    int u = e.u, v = e.v;
    if (get(u) != get(v)) {
      ans += e.w;
      fa[get(u)] = get(v);
    }
  }
  printf("%lld\n", ans);
}

pair<int, int> b[N];
set<int> g[N];
;
int tag[N], tottag;
void solve1() {
  FOR(i, 1, n) {
    b[i].first = a[i];
    b[i].second = i;
  }
  sort(b + 1, b + n + 1);
  for (unsigned i = 0; i < ve.size(); i++) {
    g[ve[i].u].insert(i);
    g[ve[i].v].insert(i);
    fa[i] = i;
  }

  long long ans = 0;
  FOR(i, 1, n) {
    int x = ++tottag, u = b[i].second, cnt = 0, last = -1;
    for (int eid : g[u]) {
      cnt += tag[get(eid)] != x;
      tag[get(eid)] = x;
      last = eid;
    }
    // printf("u %d cnt %d\n", u, cnt);
    ans += (cnt - 1) * 1ll * a[u];
    for (int eid : g[u]) {
      if (get(eid) != get(last)) { fa[get(eid)] = get(last); }
    }
  }
  printf("%lld\n", ans);
}

vector<int> L_g[N];
vector<Edge> nve;
vector<int> na;
void Line() {
  for (unsigned i = 0; i < ve.size(); i++) {
    na.pb(ve[i].w);
    L_g[ve[i].u].pb(i);
    L_g[ve[i].v].pb(i);
  }
  FOR(u, 1, n) {
    for (unsigned i = 0; i < L_g[u].size(); i++) {
      for (unsigned j = i + 1; j < L_g[u].size(); j++) {
        nve.pb({L_g[u][i] + 1, L_g[u][j] + 1, a[u]});
      }
    }
  }
  n = ve.size();
  FOR(i, 1, n) a[i] = na[i - 1];
  ve = nve;

  // printf("n %d\n", n);
  // for(auto e: ve) printf("(%d, %d, %d)\n", e.u, e.v, e.w);
}
int main() {
  scanf("%d%d%d", &n, &m, &k);
  FOR(i, 1, n) scanf("%d", a + i);
  FOR(i, 1, m) {
    int u, v, w;
    scanf("%d%d%d", &u, &v, &w);
    Edge e = {u, v, w};
    ve.pb(e);
  }
  if (k == 0) solve0();
  if (k == 1) solve1();
  if (k == 2) {
    Line();
    solve1();
  }
  return 0;
}
