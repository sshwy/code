// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

const int N = 1e5 + 5, P = 1e9 + 7;
int pw(int a, int m) {
  int res = 1;
  while (m) m & 1 ? res = 1ll * res * a % P : 0, a = 1ll * a * a % P, m >>= 1;
  return res;
}

int n, q, x, a[N], ia[N];
vector<int> g[N];

int L[N], R[N], tot, sz[N], big[N], top[N], fa[N], dep[N], nd[N];

void dfs(int u, int p) {
  L[u] = ++tot, sz[u] = 1, fa[u] = p, dep[u] = dep[p] + 1, nd[tot] = u;
  for (int v : g[u])
    if (v != p) {
      dfs(v, u);
      sz[u] += sz[v];
      if (!big[u] || sz[big[u]] < sz[v]) big[u] = v;
    }
  R[u] = ++tot, nd[tot] = u;
}
void dfs2(int u, int tp) {
  top[u] = tp;
  if (big[u]) dfs2(big[u], tp);
  for (int v : g[u])
    if (v != fa[u] && v != big[u]) dfs2(v, v);
}
int lca(int u, int v) {
  while (top[u] != top[v]) {
    if (dep[top[u]] < dep[top[v]]) swap(u, v);
    u = fa[top[u]];
  }
  return dep[u] < dep[v] ? u : v;
}

int B;
struct Query {
  int l, r, id;
  bool operator<(const Query q) const {
    if (l / B == q.l / B) return r < q.r;
    return l < q.l;
  }
};

vector<Query> queries;
int ans[N], cnt[N], node_parity[N];
int cl, cr, cur;

inline void substract(int u) {
  int au = a[u];
  if (cnt[au] && cnt[au] % 2 == 0) cur = 1ll * cur * ia[u] % P;
}
inline void contribute(int u) {
  int au = a[u];
  if (cnt[au] && cnt[au] % 2 == 0) cur = 1ll * cur * au % P;
}
inline void toggle(int u) {
  substract(u);
  if (node_parity[u]) {
    cnt[a[u]]--;
  } else {
    cnt[a[u]]++;
  }
  node_parity[u] ^= 1;
  contribute(u);
}
inline void addR() { toggle(nd[++cr]); }
inline void subL() { toggle(nd[--cl]); }
inline void subR() { toggle(nd[cr--]); }
inline void addL() { toggle(nd[cl++]); }
void getAns() {
  cl = 1, cr = 0, cur = 1;
  for (Query q : queries) {
    while (cr < q.r) addR();
    while (cl > q.l) subL();
    while (cr > q.r) subR();
    while (cl < q.l) addL();
    int z = lca(nd[cl], nd[cr]);
    if (z != nd[cl]) toggle(z);
    ans[q.id] = cur;
    if (z != nd[cl]) toggle(z);
  }
}

int main() {
  scanf("%d%d%d", &n, &q, &x);
  FOR(i, 1, n - 1) {
    int u, v;
    scanf("%d%d", &u, &v);
    g[u].pb(v);
    g[v].pb(u);
  }
  FOR(i, 1, n) scanf("%d", a + i), ia[i] = pw(a[i], P - 2);

  dfs(1, 0);
  dfs2(1, 1);

  B = max(1, (int)sqrt(tot));

  FOR(i, 1, q) {
    int u, v;
    scanf("%d%d", &u, &v);
    u = u % n + 1;
    v = v % n + 1;
    if (L[u] > L[v]) swap(u, v);
    if (R[v] < R[u])
      queries.pb({L[u], L[v], i});
    else
      queries.pb({R[u], L[v], i});
  }
  sort(queries.begin(), queries.end());

  getAns();

  FOR(i, 1, q) printf("%d\n", ans[i]);
  return 0;
}
