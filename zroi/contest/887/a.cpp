// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

const int N = 1e5 + 5, P = 1e9 + 7;
int pw(int a, int m) {
  int res = 1;
  while (m) m & 1 ? res = 1ll * res * a % P : 0, a = 1ll * a * a % P, m >>= 1;
  return res;
}

int I9, n, q, p10[N], a[N], ans = 0;

int main() {
  I9 = pw(9, P - 2);
  p10[0] = 1;
  FOR(i, 1, N - 1) p10[i] = p10[i - 1] * 10ll % P;

  scanf("%d%d", &n, &q);
  FOR(i, 1, n) scanf("%d", a + i);
  FOR(i, 1, n) ans = (ans * 10ll + a[i]) % P;
  FOR(i, 1, q) {
    int l, r, x;
    scanf("%*d%d%d%d", &l, &r, &x);
    int coef = 1ll * (p10[r - l + 1] - 1) * I9 % P;
    ans = (ans + 1ll * x * coef % P * p10[n - r]) % P;
    printf("%d\n", ans);
  }
  return 0;
}
