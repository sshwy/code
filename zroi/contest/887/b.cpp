// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

const int N = 1e5 + 5, P = 1e9 + 7, SZ = N << 2;
int pw(int a, int m) {
  int res = 1;
  while (m) m & 1 ? res = 1ll * res * a % P : 0, a = 1ll * a * a % P, m >>= 1;
  return res;
}

int s[SZ], len[SZ], tag[SZ], p10[N], n, q, a[N], I9;

void pushup(int u) {
  len[u] = len[u << 1] + len[u << 1 | 1];
  s[u] = (1ll * s[u << 1] * p10[len[u << 1 | 1]] + s[u << 1 | 1]) % P;
}
void build(int u, int l, int r) {
  if (l == r) {
    s[u] = a[nd[l]];
    len[u] = 1;
    return;
  }
  int mid = (l + r) >> 1;
  build(u << 1, l, mid), build(u << 1 | 1, mid + 1, r);
  pushup(u);
}
void nodeAdd(int u, int x) {
  tag[u] = (tag[u] + x) % P;
  int coef = 1ll * (p10[len[u]] - 1) * I9 % P;
  s[u] = (s[u] + 1ll * coef * x) % P;
}
void pushdown(int u) {
  if (tag[u]) {
    nodeAdd(u << 1, tag[u]);
    nodeAdd(u << 1 | 1, tag[u]);
    tag[u] = 0;
  }
}
void rangeAdd(int L, int R, int x, int u, int l, int r) {
  if (R < l || r < L) return;
  if (L <= l && r <= R) {
    nodeAdd(u, x);
    return;
  }
  pushdown(u);
  int mid = (l + r) >> 1;
  rangeAdd(L, R, x, u << 1, l, mid);
  rangeAdd(L, R, x, u << 1 | 1, mid + 1, r);
  pushup(u);
}
int getHash(int L, int R, int u, int l, int r) {
  if (R < l || r < L) return 0;
  if (L <= l && r <= R) return 1ll * s[u] * p10[R - r] % P;
  int mid = (l + r) >> 1;
  pushdown(u);
  return (getHash(L, R, u << 1, l, mid) + getHash(L, R, u << 1 | 1, mid + 1, r)) % P;
}

vector<int> g[N];
int sz[N], big[N], top[N], fa[N], dep[N], dfn[N], nd[N], totdfn;

void dfs(int u, int p) {
  sz[u] = 1, fa[u] = p, dep[u] = dep[p] + 1;
  for (int v : g[u])
    if (v != p) {
      dfs(v, u);
      sz[u] += sz[v];
      if (!big[u] || sz[big[u]] < sz[v]) big[u] = v;
    }
}
void dfs2(int u, int tp) {
  top[u] = tp, dfn[u] = ++totdfn, nd[totdfn] = u;
  if (big[u]) dfs2(big[u], tp);
  for (int v : g[u])
    if (v != fa[u] && v != big[u]) dfs2(v, v);
}
int lca(int u, int v) {
  while (top[u] != top[v]) {
    if (dep[top[u]] < dep[top[v]]) swap(u, v);
    u = fa[top[u]];
  }
  return dep[u] < dep[v] ? u : v;
}

int getPathHash(int u, int v) {}

int main() {
  I9 = pw(10, P - 2);
  p10[0] = 1;
  FOR(i, 1, N - 1) p10[i] = 1ll * p10[i - 1] * 10 % P;

  scanf("%d%d", &n, &q);
  FOR(i, 2, n) {
    int p;
    scanf("%d", &p);
    g[p].pb(i);
  }
  FOR(i, 1, n) scanf("%d", a + i);

  dfs(1, 0);
  dfs2(1, 1);
  build(1, 1, n);

  FOR(i, 1, q) {
    int op, u, v, x;
    scanf("%d%d%d", &op, &u, &v);
    if (op == 1) {
      printf("%d\n", getPathHash(u, v));
    } else if (op == 2) {
      scanf("%d", &x);
      pathAdd(u, v, x);
    }
  }

  return 0;
}
