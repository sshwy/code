// by Sshwy
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA
const int N = 1e6 + 5;
int n;
pair<long double, long double> a[N]; // p_i,a_i

int main() {
  scanf("%d", &n);
  FOR(i, 1, n) scanf("%Lf%Lf", &a[i].second, &a[i].first);
  sort(a + 1, a + n + 1);
  // FOR(i,1,n)a[i].first=min(max(a[i].first,1e-15),1-1e-15);
  const long double eps = (long double)(1e-9);
  a[0].first = 0;
  a[n + 1].first = 1;
  long double ans = -1e100;

  long double s = 0;
  FOR(i, 1, n) s += a[i].second;
  long double t = 0;
  FOR(j, 1, n) t += a[j].second;

  int j = n + 1;
  FOR(i, 0, n + 1) {
    if (a[i].first < eps) {
      s -= a[i].second;
      continue;
    }
    long double x = 1 / a[i].first; // x=1/p[i]
    // printf("i %d %Lf %Lf\n",i,min(s*(1-x)+t,s+t*(1-1/(1-a[j].first))),
    // min(s*(1-x)+t-a[j].second,s+(t-a[j].second)*(1-1/(1-a[j-1].first))));
    while (j > 0 &&
           (a[j].first > 1 - eps ||
               min(s * (1 - x) + t, s + t * (1 - 1 / (1 - a[j].first))) <=
                   min(s * (1 - x) + t - a[j].second,
                       s + (t - a[j].second) * (1 - 1 / (1 - a[j - 1].first))))) {
      t -= a[j].second;
      --j;
    }
    // printf("j = %d %.9Lf %.9Lf %d\n",j,a[j].first,1-eps,a[j].first>1-eps);
    long double y = 1 / (1 - a[j].first); // y=1/(1-p[j])
    ans = max(ans, min(s * (1 - x) + t, s + t * (1 - y)));
    s -= a[i].second;
  }
  printf("%.6Lf\n", ans);
  return 0;
}
