// by Sshwy
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

using namespace RA;

int main() {
  srand(clock() + time(0));
  int n = 1e5;
  cout << n << endl;
  FOR(i, 1, n) cout << rnd(1, 1e4) << " ";
  cout << endl;
  int q = n * 2;
  cout << q << endl;
  FOR(i, 1, q) {
    cout << 0 << endl;
    cout << "1 " << rnd(1, n) << " " << rnd(1, 1e4) << endl;
  }
  return 0;
}
