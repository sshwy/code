// by Sshwy
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA
const int N = 1e6 + 5;
int n;
pair<double, double> a[N]; // p_i,a_i

int main() {
  scanf("%d", &n);
  FOR(i, 1, n) scanf("%lf%lf", &a[i].second, &a[i].first);
  sort(a + 1, a + n + 1);
  a[0].first = 0.00000001;
  a[n + 1].first = 0.99999999;
  double ans = -1e99;
  double s1 = 0, s2 = 0;
  FOR(i, 1, n) s2 += a[i].second;
  FOR(i, 0, n + 1) {
    double x = 1 / a[i].first; // x=1/p[i]
    double t1 = 0, t2 = 0;
    FOR(j, 1, n) t2 += a[j].second;
    int opj = -1;
    long double opv = -1e100;
    ROF(j, n + 1, 0) {
      double y = 1 / (1 - a[j].first); // y=1/(1-p[j])
      if (opv < min(s2 * (1 - x) + t2, s2 + t2 * (1 - y)))
        opj = j, opv = min(s2 * (1 - x) + t2, s2 + t2 * (1 - y));
      ans = max(ans, min(s2 * (1 - x) + t2, s2 + t2 * (1 - y)));
      // printf("s2=%lf,x=%lf,t2=%lf,y=%lf\n",s2,x,t2,y);
      // printf("%lf %lf\n",s2*(1-x)+t2,t2*(1-y)+s2);
      t2 -= a[j].second;
      t1 += a[j].second;
    }
    printf("opj = %d\n", opj);
    s2 -= a[i].second;
    s1 += a[i].second;
  }
  printf("%.6lf\n", ans);
  return 0;
}
