// by Sshwy
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA
const int N = 1e5 + 5;
int n;
int a[N];
int q;

long long qry() {
  int l = 0;
  long long ans = 0;
  map<int, int> s;
  FOR(i, 1, n) {
    if (s.count(a[i])) { l = max(l, s[a[i]]); }
    s[a[i]] = i;
    ans += i - l;
  }
  return ans;
}
int main() {
  cin >> n;
  FOR(i, 1, n) cin >> a[i];
  cin >> q;
  FOR(i, 1, q) {
    int op;
    cin >> op;
    if (op == 0) {
      printf("%lld\n", qry());
    } else {
      int x, y;
      cin >> x >> y;
      a[x] = y;
    }
  }
  return 0;
}
