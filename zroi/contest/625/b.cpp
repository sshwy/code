// by Sshwy
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA
const int N = 1e5 + 5;
int n;
int a[N];

long long s[N << 2], mx[N << 2];
long long calc(long long val, int u, int l, int r) {
  if (val >= mx[u]) return (r - l + 1ll) * val;
  if (l == r) return mx[u];
  int mid = (l + r) >> 1;
  if (val <= mx[u << 1]) return calc(val, u << 1, l, mid) + s[u << 1 | 1];
  return calc(val, u << 1, l, mid) + calc(val, u << 1 | 1, mid + 1, r);
}
void pushup(int u, int l, int r) {
  mx[u] = max(mx[u << 1], mx[u << 1 | 1]);
  int mid = (l + r) >> 1;
  s[u << 1 | 1] = calc(mx[u << 1], u << 1 | 1, mid + 1, r); // getmax to mx[u<<1]
  s[u] = s[u << 1] + s[u << 1 | 1];
}
void assign(int pos, int v, int u = 1, int l = 1, int r = n) {
  if (l == r) return s[u] = mx[u] = v, void();
  int mid = (l + r) >> 1;
  if (pos <= mid)
    assign(pos, v, u << 1, l, mid);
  else
    assign(pos, v, u << 1 | 1, mid + 1, r);
  pushup(u, l, r);
  // printf("assign %d %d: s=%lld\n",pos,v,s[u]);
}
set<int> val_pos[N];
void modify(int x, int y) {
  // printf("modify %d %d\n",x,y);
  {
    val_pos[a[x]].erase(x);
    auto suc = val_pos[a[x]].lower_bound(x);
    if (suc != val_pos[a[x]].end()) {
      assign(*suc, x);
      if (suc != val_pos[a[x]].begin()) {
        auto pre = suc;
        --pre;
        assign(*suc, *pre);
      } else {
        assign(*suc, 0);
      }
    }
  }
  a[x] = y;
  {
    auto suc = val_pos[y].lower_bound(x);
    if (suc != val_pos[y].end()) { assign(*suc, x); }
    if (suc != val_pos[y].begin()) {
      auto pre = suc;
      --pre;
      assign(x, *pre);
    } else {
      assign(x, 0);
    }
    val_pos[y].insert(x);
  }
}
int q;
int main() {
  scanf("%d", &n);
  FOR(i, 1, n) val_pos[i].insert(i), a[i] = i;
  FOR(i, 1, n) {
    int x;
    scanf("%d", &x);
    modify(i, x);
  }
  scanf("%d", &q);
  FOR(i, 1, q) {
    int op;
    scanf("%d", &op);
    if (op == 0)
      printf("%lld\n", n * (n + 1ll) / 2 - s[1]);
    else {
      int x, y;
      scanf("%d%d", &x, &y);
      modify(x, y);
    }
  }
  return 0;
}
