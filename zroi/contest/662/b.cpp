// by Sshwy
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA
const int N = 5e4 + 5, M = 2e5 + 5;
int n, m;
int Tu[N], Tv[N];
int Eu[N], Ev[N];
int ans[N], f[N];
int get(int u) { return f[u] == u ? u : f[u] = get(f[u]); }
void merge(int u, int v) { f[get(u)] = get(v); }
int ban_calc(int x, int y) {
  FOR(i, 1, n) f[i] = i;
  FOR(i, 1, n - 1) if (i != x && i != y) { merge(Tu[i], Tv[i]); }
  map<int, int> s;
  FOR(i, 1, m - n + 1) {
    int a = get(Eu[i]), b = get(Ev[i]);
    if (a == b) continue;
    if (a > b) swap(a, b);
    a = a * (n + 1) + b;
    if (s.count(a))
      s[a]++;
    else
      s[a] = 1;
  }
  if (s.size() <= 1) return 0;
  if (s.size() == 2) {
    int res = 1e9;
    for (auto pr : s) res = min(res, pr.second);
    return res;
  }
  int tot = 0, res = 1e9;
  for (auto pr : s) tot += pr.second;
  for (auto pr : s) res = min(res, tot - pr.second);
  return res;
}
int main() {
  scanf("%d%d", &n, &m);
  FOR(i, 1, n - 1) scanf("%d%d", &Tu[i], &Tv[i]);
  FOR(i, 1, m - n + 1) scanf("%d%d", &Eu[i], &Ev[i]);
  FOR(i, 1, n - 1) ans[i] = 1e9;
  FOR(x, 1, n - 1) FOR(y, x + 1, n - 1) {
    int t = ban_calc(x, y);
    ans[x] = min(ans[x], t);
    ans[y] = min(ans[y], t);
  }
  FOR(i, 1, n - 1) printf("%d%c", ans[i] + 2, " \n"[i == n - 1]);
  return 0;
}
