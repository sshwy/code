// by Sshwy
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA
const int N = 7000, P = 1e9 + 7;
int n;
int a[N];

long long f(int i) { return i * (i + 1ll) / 2; }
long long tot, t2, x;
int p[20], b[N];
int x1, x2;
void go1() {
  FOR(i, 1, n) p[i] = i;
  do {
    FOR(i, 1, n) b[i] = a[p[i]];
    int cnt = 0;
    FOR(i, 1, n) {
      int l = i, r = i;
      ++cnt;
      while (l > 1 && r < n && b[l - 1] == b[r + 1]) --l, ++r, ++cnt;
      if (i < n && b[i] == b[i + 1]) {
        l = i, r = i + 1;
        while (l > 1 && r < n && b[l - 1] == b[r + 1]) --l, ++r, ++cnt;
      }
    }
    if (cnt > x1) x1 = cnt, x2 = 0;
    if (cnt == x1) {
      ++x2;
      FOR(i, 1, n) printf("%d%c", b[i], " \n"[i == n]);
    }
  } while (next_permutation(p + 1, p + n + 1));
  printf("%d %d\n", x1, x2);
}
int main() {
  scanf("%d", &n);
  FOR(i, 1, n) scanf("%d", &a[i]);
  // if(n <= 10){
  //    go1();
  //    return 0;
  //}
  sort(a + 1, a + n + 1);
  a[n + 1] = -1;
  int cnt = 0;
  FOR(i, 1, n) {
    ++cnt;
    if (a[i] != a[i + 1]) {
      tot += f(cnt);
      cnt = 0;
      ++x;
    }
  }
  t2 = 1;
  FOR(i, 1, x) t2 = t2 * 1ll * i % P;
  printf("%lld %lld\n", tot, t2);
  return 0;
}
