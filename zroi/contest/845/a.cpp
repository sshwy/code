// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

const int N = 45, P = 1e9 + 7;
void add(int &x, int y) { x = (x + y) % P; }
int mul(int x, int y) { return x * 1ll * y % P; }

int n, f[N][7][27][12][6][4][2][2];
char s[N];

void trans(int i, int j, int *a, int f_ijS) {
  FOR(k, j, 6) if (a[k]) {
    FOR(p, 0, k - j) {
      int b[] = {0, a[1], a[2], a[3], a[4], a[5], a[6]};
      b[k]--;
      if (p) b[p]++;
      if (k - j - p) b[k - j - p]++;
      int coef = j == 1 ? 1 : 2;
      coef = mul(coef, a[k]);
      add(f[i + 1][1][b[1]][b[2]][b[3]][b[4]][b[5]][b[6]], mul(coef, f_ijS));
    }
  }
}
int group[N];
int main() {
  scanf("%d", &n);
  scanf("%s", s + 1);
  s[n] = '0';

  int c[7] = {0};
  FOR(i, 1, n) group[i / (i & -i)]++;
  FOR(i, 1, n) if (group[i]) c[group[i]]++;
  f[1][1][c[1]][c[2]][c[3]][c[4]][c[5]][c[6]] = 1;

  FOR(i, 1, n) FOR(j, 1, 6) {
    FOR(a1, 0, 26)
    FOR(a2, 0, 11) FOR(a3, 0, 5) FOR(a4, 0, 3) FOR(a5, 0, 1) FOR(a6, 0, 1) {
      if (f[i][j][a1][a2][a3][a4][a5][a6]) {
        int a[] = {0, a1, a2, a3, a4, a5, a6};
        if (j < 6) {
          int coef = s[i] == '0' ? P - 1 : 1;
          add(f[i + 1][j + 1][a1][a2][a3][a4][a5][a6],
              mul(coef, f[i][j][a1][a2][a3][a4][a5][a6]));
        }
        if (s[i] == '0') { trans(i, j, a, f[i][j][a1][a2][a3][a4][a5][a6]); }
      }
    }
  }

  printf("%d\n", f[n + 1][1][0][0][0][0][0][0]);

  return 0;
}

// f[i, j, S] -> f[i+1, j+1, S]
// f[i, j, S] -> f[i+1, 1, S']
