// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

using namespace RA;

bool vis[1003][1003];
int main() {
  srand(clock() + time(0));
  int n = rnd(1, 20), m = rnd(1, min(20, n * n));
  n = 500, m = 1000;
  printf("%d %d\n", n, m);
  FOR(i, 1, m) {
    int x = rnd(1, n), y = rnd(1, n);
    while (vis[x][y]) {
      x = rnd(1, n);
      y = rnd(1, n);
    }
    vis[x][y] = true;
    printf("%d %d\n", x, y);
  }
  return 0;
}
