// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

const int N = 2005, P = 1e9 + 7;

int n, ans;
int a[3][N], b[N * 3];

int id(int x, int y) { return x * n + y; }

typedef pair<long long, int> node;
priority_queue<node, vector<node>, greater<node>> q;

long long dist[N * 3];
int dir[][2] = {0, 1, 0, -1, 1, 0, -1, 0};
void go(int start) {
  memset(dist, 0x3f, sizeof(dist));
  dist[start] = b[start];
  q.push(node(b[start], start));
  while (!q.empty()) {
    node u = q.top();
    q.pop();
    if (dist[u.second] < u.first) continue;
    int x = u.second / n, y = u.second % n;
    FOR(i, 0, 3) {
      int nx = x + dir[i][0], ny = y + dir[i][1];
      if (nx < 0 || nx > 2 || ny < 0 || ny > n - 1) continue;
      int v = id(nx, ny);
      if (dist[v] > u.first + b[v]) {
        dist[v] = u.first + b[v];
        q.push(node(dist[v], v));
      }
    }
  }
  FOR(i, 0, 2)
  FOR(j, 0, n - 1) if (id(i, j) != start) ans = (ans + dist[id(i, j)]) % P;
}

int main() {
  scanf("%d", &n);
  FOR(i, 0, 2) FOR(j, 0, n - 1) scanf("%d", &b[id(i, j)]);
  FOR(i, 0, 2) FOR(j, 0, n - 1) go(id(i, j));
  printf("%d\n", ans);
  return 0;
}
