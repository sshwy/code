// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

const int N = 45, P = 1e9 + 7;
int n, p[N], b[N];
char s[N];

int ans;

int main() {
  scanf("%d", &n);
  scanf("%s", s + 1);

  FOR(i, 1, n) p[i] = i;
  do {
    bool flag = true;
    FOR(i, 1, n - 1) b[i] = (p[i] * 2 == p[i + 1] || p[i + 1] * 2 == p[i]);
    FOR(i, 1, n - 1) if (b[i] != s[i] - '0') {
      flag = false;
      break;
    }
    ans += flag;
  } while (next_permutation(p + 1, p + n + 1));

  printf("%d\n", ans);
  return 0;
}
