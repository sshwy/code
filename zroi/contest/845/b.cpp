// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

typedef long long LL;
typedef pair<LL, int> Node;
const int N = 1e5 + 5, P = 1e9 + 7, SZ = N * 2;
const LL INF = 0x3f3f3f3f3f3f3f3f;

void add(int &x, LL y) { x = (x + y) % P; }

struct Fenwick {
  int c[SZ];
  void add(int pos, int v) {
    assert(0 < pos && pos < SZ);
    for (int i = pos; i < SZ; i += i & -i) add(c[i], v);
  }
  int pre(int pos) {
    assert(0 < pos && pos < SZ);
    int res = 0;
    for (int i = pos; i > 0; i -= i & -i) add(res, c[i]);
    return res;
  }
} bSum, bCnt;

int n, a[N * 3], ans;
LL b[N], dist[4][N * 3];

int id(int i, int j) { return (i - 1) * n + j - 1; }

void work(int row, int mid, int l, int r) {
  LL *d = dist[row];
  priority_queue<Node, vector<Node>, greater<Node>> q;
  const int dir[][2] = {0, 1, 0, -1, 1, 0, -1, 0};
  FOR(i, 1, 3) FOR(j, l, r) d[id(i, j)] = INF;
  dist[row][id(row, mid)] = 0;
  q.push(Node(0, id(row, mid)));

  while (!q.empty()) {
    Node u = q.top();
    q.pop();
    if (d[u.second] < u.first) continue;
    int x = u.second / n + 1, y = u.second % n + 1;
    FOR(i, 0, 3) {
      int nx = x + dir[i][0], ny = y + dir[i][1], v = id(nx, ny);
      if (nx < 1 || nx > 3 || ny < l || ny > r) continue;
      if (d[v] > u.first + a[v]) d[v] = u.first + a[v], q.push(Node(d[v], v));
    }
    if (x != 2) {
      int v = id(x == 1 ? 3 : 1, y);
      LL by = min(y == l ? b[y - 1] : INF, y == r ? b[y + 1] : INF);
      if (d[v] > u.first + by + a[v])
        d[v] = u.first + by + a[v], q.push(Node(d[v], v));
    }
  }
}

struct Point {
  LL x, y, v, typ;
  bool operator<(const Point p) const {
    if (x != p.x) return x < p.x;
    if (y != p.y) return y < p.y;
    return typ < p.typ;
  }
  bool operator==(const Point p) const { return x == p.x && y == p.y; }
};

template <class T> int findIndex(vector<T> v, T val) {
  return lower_bound(v.begin(), v.end(), val) - v.begin();
}

void go2(vector<Point> &A, bool countEqual) {
  if (A.empty()) return;
  vector<LL> D;
  for (auto e : A) D.pb(e.x), D.pb(e.y);
  sort(D.begin(), D.end());
  D.erase(unique(D.begin(), D.end()), D.end());
  for (auto &e : A) e.x = findIndex(D, e.x), e.y = findIndex(D, e.y);

  sort(A.begin(), A.end());

  Point p = {INF, INF};
  int sum = 0, cnt = 0;
  for (auto e : A) {
    if (!(p == e)) p = e, sum = 0, cnt = 0;
    if (e.typ == 0)
      add(sum, e.v), ++cnt;
    else
      add(ans, sum + cnt * 1ll * e.v);
  }

  int la = A.size();
  for (auto e : A) {
    if (e.typ == 0) {
      bSum.add(e.y, e.v);
      bCnt.add(e.y, 1);
    } else {
      add(ans, bSum.pre(e.y - 1) + bCnt.pre(e.y - 1) * 1ll * e.v);
    }
  }
  for (auto e : A) {
    if (e.typ == 0) {
      bSum.add(e.y, (P - e.v) % P);
      bCnt.add(e.y, 1);
    }
  }
}

void go(int row, int mid, int l, int r, bool countEqual) {
  if (l == mid || mid == r) return;
  const int rx[] = {0, 2, 3, 1};
  const int ry[] = {0, 3, 1, 2};
  LL *dx = dist[rx[row]], *dy = dist[ry[row]], *d = dist[row];
  vector<Point> A;
  FOR(i, 1, 3) FOR(j, l, mid - 1) {
    int u = id(i, j);
    Point pu = {d[u] - dx[u], d[u] - dy[u], d[u] % P, 0};
    A.pb(pu);
  }
  FOR(i, 1, 3) FOR(j, mid + 1, r) {
    int u = id(i, j);
    Point pu = {dx[u] - d[u], dy[u] - d[u], d[u] % P, 1};
    A.pb(pu);
  }
  go2(A, countEqual);
}

void solve(int l, int r) {
  if (l > r) return;
  int mid = (l + r) >> 1;

  FOR(i, 1, 3) { work(i, mid, l, r); }

  FOR(row, 1, 3) FOR(i, 1, 3) FOR(j, l, r) {
    if (id(row, mid) != id(i, j)) add(ans, dist[row][id(i, j)]);
  }

  FOR(i, 1, 3) { go(i, mid, l, r, i == 1); }

  assert(dist[3][id(1, mid)] == dist[1][id(3, mid)]);

  b[mid] = min(b[mid], dist[1][id(3, mid)]);

  solve(l, mid - 1);
  solve(mid + 1, r);
}

int main() {
  scanf("%d", &n);
  FOR(i, 1, 3) FOR(j, 1, n) scanf("%d", &a[id(i, j)]);

  memset(b, 0x3f, sizeof(b));

  solve(1, n);
  printf("%d\n", ans);
  return 0;
}
