// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

const int N = 1003, M = 1003;

int n, m, Nx, Ny;
int ban[N][N], f[N][N];

typedef pair<int, int> pt;
queue<pt> q;
int dir[][2] = {0, 1, 0, -1, 1, 0, -1, 0};
void go() {
  FOR(i, 0, Nx - 1) {
    if (ban[i][0]) break;
    q.push({i, 0});
    f[i][0] = 0;
  }
  while (!q.empty()) {
    pt u = q.front();
    q.pop();
    FOR(i, 0, 3) {
      int nx = u.first + dir[i][0], ny = u.second + dir[i][1];
      if (nx < 0 || nx >= Nx || ny < 0 || ny >= Ny) continue;
      if (f[nx][ny] != -1 || ban[nx][ny]) continue;
      FOR(j, 1, n) {
        int x = u.first + dir[i][0] * j, y = u.second + dir[i][1] * j;
        if (x < 0 || x >= Nx || y < 0 || y >= Ny) break;
        if (ban[x][y]) break;
        if (f[x][y] != -1) break;
        f[x][y] = f[u.first][u.second] + 1;
        q.push({x, y});
      }
    }
  }
}

vector<int> vx, vy, wx, wy;
vector<pt> bans;

void work() {
  sort(vx.begin(), vx.end());
  sort(vy.begin(), vy.end());
  vx.erase(unique(vx.begin(), vx.end()), vx.end());
  vy.erase(unique(vy.begin(), vy.end()), vy.end());

  vector<int> tx, ty;
  tx.pb(1), wx.pb(1);
  ty.pb(1), wy.pb(1);
  for (int x : vx) {
    if (tx.back() == x) continue;
    if (tx.back() + 1 < x) { wx.pb(x - tx.back() - 1), tx.pb(x - 1); }
    wx.pb(1), tx.pb(x);
  }
  if (tx.back() < n) { wx.pb(n - tx.back()), tx.pb(n); }
  for (int y : vy) {
    if (ty.back() == y) continue;
    if (ty.back() + 1 < y) { wy.pb(y - ty.back() - 1), ty.pb(y - 1); }
    wy.pb(1), ty.pb(y);
  }
  if (ty.back() < n) { wy.pb(n - ty.back()), ty.pb(n); }
  vx = tx, vy = ty;

  for (pt e : bans) {
    int x = e.first, y = e.second;
    x = lower_bound(vx.begin(), vx.end(), x) - vx.begin();
    y = lower_bound(vy.begin(), vy.end(), y) - vy.begin();
    assert(wx[x] == 1 && wy[y] == 1);
    ban[x][y] = true;
  }
  Nx = vx.size();
  Ny = vy.size();
}
int main() {
  memset(f, -1, sizeof(f));
  scanf("%d%d", &n, &m);
  FOR(i, 1, m) {
    int x, y;
    scanf("%d%d", &x, &y);
    bans.pb({x, y});

    vx.pb(x);
    vy.pb(y);
  }
  work();
  go();
  long long ans = 0;
  FOR(i, 0, Nx - 1) FOR(j, 0, Ny - 1) {
    // printf("%2d%c", f[i][j], " \n"[j == Ny-1]);
    if (f[i][j] != -1) ans = (ans + f[i][j] * 1ll * wx[i] * wy[j]);
  }
  printf("%lld\n", ans);
  return 0;
}
