// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

const int N = 1003, M = 1003;

int n, m;
int ban[N][N], f[N][N];

typedef pair<int, int> pt;
queue<pt> q;
int dir[][2] = {0, 1, 0, -1, 1, 0, -1, 0};
void go() {
  FOR(i, 1, n) {
    if (ban[i][1]) break;
    q.push({i, 1});
    f[i][1] = 0;
  }
  while (!q.empty()) {
    pt u = q.front();
    q.pop();
    FOR(i, 0, 3) {
      int nx = u.first + dir[i][0], ny = u.second + dir[i][1];
      if (nx < 1 || nx > n || ny < 1 || ny > n) continue;
      if (f[nx][ny] != -1 || ban[nx][ny]) continue;
      FOR(j, 1, n) {
        int x = u.first + dir[i][0] * j, y = u.second + dir[i][1] * j;
        if (x < 1 || x > n || y < 1 || y > n) break;
        if (ban[x][y]) break;
        if (f[x][y] != -1) break;
        f[x][y] = f[u.first][u.second] + 1;
        q.push({x, y});
      }
    }
  }
}

int main() {
  memset(f, -1, sizeof(f));
  scanf("%d%d", &n, &m);
  FOR(i, 1, m) {
    int x, y;
    scanf("%d%d", &x, &y);
    ban[x][y] = true;
  }
  go();
  long long ans = 0;
  FOR(i, 1, n) FOR(j, 1, n) {
    // printf("%2d%c", f[i][j], " \n"[j == n]);
    if (f[i][j] != -1) ans = (ans + f[i][j]);
  }
  printf("%lld\n", ans);
  return 0;
}
