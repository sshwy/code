// by Sshwy
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace IO { // require C++11
  const int _BS = 200000;
  char _ob[_BS], *_z = _ob, _ib[_BS], *_x = _ib, *_y = _ib;
  // Input
  char _nc() {
    return _x == _y && (_y = (_x = _ib) + fread(_ib, 1, _BS, stdin), _x == _y)
               ? EOF
               : *_x++;
  }
  inline void rd(char &c) { c = _nc(); }
  void rd(char *s) {
    char c = _nc();
    while (!isblank(c) && isprint(c) && c != EOF) *s++ = c, c = _nc();
    *s = 0;
  }
  template <class T> void rd(T &res) { // unsigned/signed int/long long
    res = 0;
    char c = _nc(), flag = 0;
    while (!isdigit(c) && c != '-') c = _nc();
    if (c == '-') flag = 1, c = _nc();
    while (isdigit(c)) res = res * 10 + c - '0', c = _nc();
    if (flag) res = -res;
  }
  template <class T, class... A> void rd(T &hd, A &...rs) { rd(hd), rd(rs...); }
  // Output
  inline void flush() { fwrite(_ob, 1, _z - _ob, stdout), _z = _ob; }
  inline void wrch(char x) {
    if (_z == _ob + _BS - 1) flush();
    *_z = x, ++_z;
  }
  void wr(char *s) {
    while (*s) wrch(*s), ++s;
  }
  void wr(const char *s) {
    while (*s) wrch(*s), ++s;
  }
  template <class T> void wr(T x) { // unsigned/signed int/long long
    if (x == 0) return wrch('0'), void();
    if (x < 0) wrch('-'), x = -x;
    int _t[30], _lt = 0;
    while (x) _t[++_lt] = x % 10, x /= 10;
    while (_lt > 0) wrch(_t[_lt] + '0'), --_lt;
  }
  inline void wr(char x) { wrch(x); }
  template <class T, class... A> void wr(T hd, A... rs) { wr(hd), wr(rs...); }
  template <class... A> void wrln(A... ar) { wr(ar...), wrch('\n'); }
  struct _Flusher {
    ~_Flusher() { flush(); }
  } _flusher;
} // namespace IO
using IO::rd;
using IO::wr;
using IO::wrln;

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA
int n, m;
const int P = 1e9 + 7, M = 2359296 + 10, LN = 18, N = (1 << LN) + 10;

struct qxx {
  int nex, t;
} e[M * 2];
int h[N], le = 1;
void add_path(int f, int t) { e[++le] = {h[f], t}, h[f] = le; }

bool vis[N], mark[N], use[N];
int val[N], vmk[N];

queue<int> q;
int perm(int x, const vector<int> &v) {
  int res = 0;
  assert(v.size() == n);
  for (int i = 0; i < n; ++i)
    if (x >> i & 1) res |= 1 << v[i];
  return res;
}
void go() {
  rd(n, m);

  memset(e, 0, sizeof(e[0]) * (le + 1));
  fill(h, h + (1 << n) + 1, 0);
  fill(vis, vis + (1 << n) + 1, 0);
  fill(use, use + (1 << n) + 1, 0);
  fill(mark, mark + (1 << n) + 1, 0);
  le = 1;

  FOR(i, 1, m) {
    int u, v;
    rd(u, v);
    add_path(u, v);
    add_path(v, u);
  }

  { // init
    vis[1] = 1, val[1] = 0;
    use[val[1]] = 1;
    int t = 0;
    for (int i = h[1]; i; i = e[i].nex) {
      int v = e[i].t;
      vis[v] = 1, val[v] = 1 << t, ++t;
      use[val[v]] = 1;
      q.push(v);
    }
  }
  while (!q.empty()) {
    int u = q.front();
    q.pop();
    for (int i = h[u]; i; i = e[i].nex) {
      int v = e[i].t;
      if (vis[v]) continue;
      if (mark[v]) {
        int x = vmk[v] & val[u];
        int y = vmk[v] | val[u];
        assert(use[x] || use[y]);
        assert(!use[x] || !use[y]);
        if (use[x])
          val[v] = y, use[y] = 1;
        else
          val[v] = x, use[x] = 1;
        vis[v] = 1;
        q.push(v);
      } else {
        mark[v] = 1;
        vmk[v] = val[u];
      }
    }
  }
  FOR(i, 1, 1 << n) assert(vis[i]);
  vector<int> v;
  FOR(i, 0, n - 1) v.pb(i);
  // FOR(i,1,1<<n)printf("%d%c",val[i]," \n"[i==(1<<n)]);
  ROF(i, 1 << n, 1) {
    int x = perm(val[i], v);
    vector<int> nv(n, 0), nnv(n, 0);
    int cur = 0;
    for (int i = 0; i < n; i++) {
      if (x >> i & 1) nv[i] = cur, cur++;
    }
    for (int i = 0; i < n; i++) {
      if (!(x >> i & 1)) nv[i] = cur, cur++;
    }
    assert(cur == n);
    for (int i = 0; i < n; i++) { nnv[i] = nv[v[i]]; }
    v = nnv;
  }
  FOR(i, 1, 1 << n) val[i] = perm(val[i], v);
  long long coef = 1;
  int ans = 0;
  FOR(i, 1, 1 << n) {
    ans = (ans + val[i] * coef) % P;
    coef = coef * 10 % P;
  }
  wrln(ans);
}
int main() {
  int t;
  rd(t);
  FOR(i, 1, t) go();
  return 0;
}
