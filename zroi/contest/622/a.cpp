// by Sshwy
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA
const int K = 2e5 + 5, N = 1e5 + 5;
int n, k, m;
template <class T> struct delq {
  priority_queue<T> a, b;
  void push(T x) { a.push(x); }
  void remove(T x) { b.push(x); }
  bool empty() { return a.size() == b.size(); }
  T top() {
    while (a.size() && b.size() && a.top() == b.top()) a.pop(), b.pop();
    assert(!a.empty());
    return a.top();
  }
  void clear() {
    while (a.size()) a.pop();
    while (b.size()) b.pop();
  }
};
delq<pair<long long, int>> q[N];
map<int, pair<long long, int>> s[N]; // s[i][j]:i and j total time, the last
                                     // time
int rem[K][3], ins[K][3];
int ship[N];
int c_noship, c_good;
void upd(int u) {     // update u's friend ship
  if (q[u].empty()) { // noop
    if (ship[u]) {
      c_noship++;
      if (ship[ship[u]] == u) c_good -= 2;
      ship[u] = 0;
    }
  } else {
    auto p = q[u].top();
    int v = ins[p.second][0] == u ? ins[p.second][1] : ins[p.second][0];
    // printf("%d ship %d\n",u,v);
    if (ship[u] == 0) {
      c_noship--;
      ship[u] = v;
      if (ship[v] == u) c_good += 2;
    } else if (ship[u] != v) {
      if (ship[ship[u]] == u) c_good -= 2;
      ship[u] = v;
      if (ship[v] == u) c_good += 2;
    }
  }
}
void add(int a, int b, int c, int id) {
  if (s[a].count(b) == 0)
    s[a][b] = {0, 0};
  else
    q[a].remove(s[a][b]);
  if (s[b].count(a) == 0)
    s[b][a] = {0, 0};
  else
    q[b].remove(s[b][a]);
  s[a][b].first += c, s[a][b].second = id;
  s[b][a].first += c, s[b][a].second = id;
  q[a].push(s[a][b]);
  q[b].push(s[b][a]);
  upd(a), upd(b);
}
void del(int a, int b, int c) {
  assert(s[a].count(b) && s[b].count(a));
  q[a].remove(s[a][b]);
  q[b].remove(s[b][a]);
  s[a][b].first -= c;
  s[b][a].first -= c;
  if (s[a][b].first == 0)
    s[a].erase(b);
  else
    q[a].push(s[a][b]);
  if (s[b][a].first == 0)
    s[b].erase(a);
  else
    q[b].push(s[b][a]);
  upd(a), upd(b);
}
void go() {
  scanf("%d%d%d", &n, &k, &m);
  memset(rem, 0, sizeof(rem));
  FOR(i, 1, n) s[i].clear(), q[i].clear();
  c_noship = n, c_good = 0;
  FOR(i, 1, n) ship[i] = 0;
  FOR(i, 1, k) {
    int a, b, c;
    scanf("%d%d%d", &a, &b, &c);
    ins[i][0] = a, ins[i][1] = b, ins[i][2] = c;
    rem[i + m][0] = a, rem[i + m][1] = b, rem[i + m][2] = c;
    add(a, b, c, i);
    if (rem[i][0]) del(rem[i][0], rem[i][1], rem[i][2]);
    // printf("c_noship %d c_good %d\n",c_noship,c_good);
    printf("%d\n", n - c_noship - c_good);
  }
}
int main() {
  int t;
  scanf("%d", &t);
  FOR(i, 1, t) go();
  return 0;
}
