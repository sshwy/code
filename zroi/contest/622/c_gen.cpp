// by Sshwy
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

using namespace RA;

int main() {
  srand(clock() + time(0));
  int n = 1e3, m = 1e3;
  printf("%d %d\n", n, m);
  FOR(i, 1, n - 1) printf("%d %d\n", rnd(1, i), i + 1);
  FOR(i, 1, m - 1)
  printf("%d %d %d %d\n", rnd(1, i), i + 1, rnd(1, n), rnd(1, n));
  return 0;
}
