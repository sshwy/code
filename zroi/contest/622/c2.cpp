// by Sshwy
#include <bits/stdc++.h>
#define int long long
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA
const int N = 1e5 + 5, M = N * 2, P = 1e9 + 7;
int n, m;

vector<int> g[N];
struct qxx {
  int nex, t, v;
} e[M * 2];
int h[N], le = 1;
void add_path(int f, int t, int v) { e[++le] = {h[f], t, v}, h[f] = le; }

int sz_m[N];
int ans;
void dfs_m(int u, int p) {
  sz_m[u] = 1;
  for (int i = h[u]; i; i = e[i].nex) {
    int v = e[i].t;
    if (v != p) dfs_m(v, u), sz_m[u] += sz_m[v];
  }
  if (p) { ans = (ans + (m - sz_m[u]) * 1ll * sz_m[u] % P * n % P * n % P) % P; }
}
long long sz[N], a[N];
void dfs1(int u, int p) {
  sz[u] = 1 + a[u];
  for (int v : g[u]) {
    if (v != p) dfs1(v, u), sz[u] = (sz[u] + sz[v]) % P;
  }
  if (p) { ans = (ans + (n * 1ll * m % P - sz[u]) % P * 1ll * sz[u]) % P; }
}
void dfs(int u, int p) {
  for (int i = h[u]; i; i = e[i].nex) {
    int v = e[i].t, nd = e[i].v;
    int s = 0;
    if (v == p)
      s = m - sz_m[u];
    else
      s = sz_m[v];
    s = s * 1ll * n % P;
    a[nd] = (a[nd] + s) % P;
  }
  // printf("dfs %lld %lld\n",u,p);
  // FOR(i,1,n)printf("%lld%c",a[i]," \n"[i==n]);
  dfs1(1, 0);
  assert(sz[1] == n * m);
  for (int i = h[u]; i; i = e[i].nex) {
    int nd = e[i].v;
    a[nd] = 0;
  }
  for (int i = h[u]; i; i = e[i].nex) {
    int v = e[i].t;
    if (v != p) dfs(v, u);
  }
}
signed main() {
  scanf("%lld%lld", &n, &m);
  FOR(i, 1, n - 1) {
    int u, v;
    scanf("%lld%lld", &u, &v);
    g[u].pb(v);
    g[v].pb(u);
  }
  FOR(i, 1, m - 1) {
    int a, b, u, v;
    scanf("%lld%lld%lld%lld", &a, &b, &u, &v);
    add_path(a, b, u);
    add_path(b, a, v);
  }
  dfs_m(1, 0);
  dfs(1, 0);
  printf("%lld\n", ans);
  return 0;
}
