// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

const int N = 5e5 + 5;
int n, m, q;
int a[N];
long long b[N];

int main() {
  scanf("%d%d%d", &n, &m, &q);
  FOR(i, 1, n) scanf("%d", a + i);
  FOR(i, 1, q) {
    int op, l, r, x;
    scanf("%d%d%d%d", &op, &l, &r, &x);
    if (op == 1) {
      FOR(i, l, r) a[i] = x;
    } else {
      FOR(i, l, r) b[a[i]] += x;
    }
  }
  FOR(i, 1, m) printf("%lld\n", b[i]);
  return 0;
}
