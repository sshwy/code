// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

const int N = 1e5 + 5, P = 998244353;
int pw(int a, int m) {
  int res = 1;
  while (m) m & 1 ? res = 1ll * res * a % P : 0, a = 1ll * a * a % P, m >>= 1;
  return res;
}
long long inv(long long x) { return pw(x % P, P - 2); }

void add(long long &x, long long y) { x = (x + y) % P; }

struct qxx {
  int nex, t;
} e[N * 2];
int h[N], le;
void ae(int u, int v) { e[++le] = (qxx){h[u], v}, h[u] = le; }

int n, S, v[N], son[N], s[N], fa[N];
long long dg[N], c1[N], c2[N], c3[N], c4[N], w[N], f[N], g[N], ans;

void dfs(int u, int p) {
  if (!p) c1[p] = 1;
  c1[u] = c1[p] * inv(dg[u]) % P;
  fa[u] = p;
  for (int i = h[u]; i; i = e[i].nex) {
    int v = e[i].t;
    if (v == p) continue;
    dfs(v, u);
    son[u] = v, s[u]++;
    add(c4[u], inv(dg[v]));
    add(c3[u], c3[v]);
    add(w[u], c3[v]);
  }
  c2[u] = c4[u] * inv(dg[u] * dg[u] % P) % P;
  c3[u] = c3[u] * inv(dg[u] * (dg[u] - 1) % P) % P;
  add(c3[u], c2[u]);
}
void dfs2(int u, int p) {
  if (!p) {
    f[u] = 1;
    g[u] = 0;
  } else { // f = f' - g
    long long p1 = (g[p] * inv(dg[p] - 1) % P + f[p] * inv(dg[p]) % P) % P;
    long long p2 =
        (g[p] * pw(inv(dg[p] - 1), 2) % P + f[p] * inv(dg[p] * dg[p] % P) % P) % P *
        ((c4[p] - inv(dg[u]) + P) % P) % P;
    long long p3 = (g[p] * inv((dg[p] - 1) * (dg[p] - 2) % P) +
                       f[p] * inv(dg[p] * (dg[p] - 1) % P)) %
                   P * ((w[p] - c3[u] + P) % P) % P;
    long long p4 = p == 0 || fa[p] == 0 ? 0
                                        : (g[fa[p]] * pw(inv(dg[fa[p]] - 1), 2) % P +
                                              f[fa[p]] * pw(inv(dg[fa[p]]), 2) % P) %
                                              P * inv(dg[p] * (dg[p] - 1) % P) % P;
    f[u] = p1;
    g[u] = (p2 + p3 + p4) % P;
  }
  if (s[u] == 1) {
    long long p6 =
        (g[u] % P * inv(dg[u] - 1) % P + (u == S ? inv(dg[u]) : 0)) % P * w[u] % P;
    add(ans, v[u] * p6);
  }
  if (s[u] == 0) {
    add(ans, g[u] * v[u] % P);
    if (p) {
      long long p5 = (g[p] * inv((dg[p] - 1) * (dg[p] - 1) % P) % P +
                         f[p] * inv(dg[p] * dg[p] % P) % P) %
                     P;
      add(ans, p5 * v[u] % P);
    }
  }
  for (int i = h[u]; i; i = e[i].nex) {
    int v = e[i].t;
    if (v == p) continue;
    dfs2(v, u);
  }
}
int main() {
  scanf("%d%d", &n, &S);
  FOR(i, 1, n) scanf("%d", v + i);
  FOR(i, 1, n - 1) {
    int x, y;
    scanf("%d%d", &x, &y);
    ae(x, y), ae(y, x);
    dg[x]++, dg[y]++;
  }
  dfs(S, 0);
  dfs2(S, 0);
  printf("%lld\n", ans);
  return 0;
}
