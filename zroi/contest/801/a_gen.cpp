// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

using namespace RA;

int main() {
  srand(clock() + time(0));
  int n = rnd(1, 5000), m = rnd(1, 5000), q = 5000;
  // n = 5e5, m = 5e5, q = 5e5;
  printf("%d %d %d\n", n, m, q);
  FOR(i, 1, n) printf("%d%c", rnd(0, m), " \n"[i == n]);
  FOR(i, 1, q) {
    int op = rnd(1, 2);
    int l = rnd(1, n), r = rnd(1, n);
    if (l > r) swap(l, r);
    if (op == 1) {
      printf("1 %d %d %d\n", l, r, rnd(0, m));
    } else {
      printf("2 %d %d %d\n", l, r, rnd(0, m));
    }
  }
  return 0;
}
