// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

const int N = 5e5 + 5, SZ = N * 4;
int n, m, q;
int a[N];
long long b[N];

int pos[SZ];
long long ptag[SZ];
bool covered[SZ];

void build(int u, int l, int r) {
  if (l == r) {
    pos[u] = a[l];
    covered[u] = true;
    return;
  }
  int mid = (l + r) >> 1;
  build(u << 1, l, mid);
  build(u << 1 | 1, mid + 1, r);
}

void nodeAdd(int u, int l, int r, long long v) {
  if (covered[u]) {
    b[pos[u]] += v * 1ll * (r - l + 1);
  } else {
    ptag[u] += v;
  }
}
void pd(int u, int l, int r, int mid) {
  if (covered[u]) {
    assert(ptag[u] == 0);
    pos[u << 1] = pos[u];
    pos[u << 1 | 1] = pos[u];
    covered[u << 1] = true;
    covered[u << 1 | 1] = true;
    covered[u] = false;
  }
  if (ptag[u]) {
    nodeAdd(u << 1, l, mid, ptag[u]);
    nodeAdd(u << 1 | 1, mid + 1, r, ptag[u]);
    ptag[u] = 0;
  }
}
void nodeAssign(int u, int l, int r, int v) {
  covered[u] = true;
  pos[u] = v;
  assert(ptag[u] == 0);
}
void assign(int L, int R, int v, int u, int l, int r) {
  if (R < l || r < L) return;
  if (L <= l && r <= R) {
    nodeAssign(u, l, r, v);
    return;
  }
  int mid = (l + r) >> 1;
  pd(u, l, r, mid);
  if (L <= mid) assign(L, R, v, u << 1, l, mid);
  if (mid < R) assign(L, R, v, u << 1 | 1, mid + 1, r);
}

void nodeApply(int u, int l, int r) {
  if (covered[u]) {
    assert(ptag[u] == 0);
    return;
  }
  int mid = (l + r) >> 1;
  pd(u, l, r, mid);
  nodeApply(u << 1, l, mid), nodeApply(u << 1 | 1, mid + 1, r);
}
void apply(int L, int R, int u, int l, int r) {
  if (R < l || r < L) return;
  if (L <= l && r <= R) return nodeApply(u, l, r);
  if (covered[u]) return nodeApply(u, l, r);
  int mid = (l + r) >> 1;
  pd(u, l, r, mid);
  if (L <= mid) apply(L, R, u << 1, l, mid);
  if (mid < R) apply(L, R, u << 1 | 1, mid + 1, r);
}
void Assign(int l, int r, int x) {
  apply(l, r, 1, 1, n);
  assign(l, r, x, 1, 1, n);
}
void add(int L, int R, int v, int u, int l, int r) {
  if (R < l || r < L) return;
  if (L <= l && r <= R) {
    nodeAdd(u, l, r, v);
    return;
  }
  int mid = (l + r) >> 1;
  pd(u, l, r, mid);
  if (L <= mid) add(L, R, v, u << 1, l, mid);
  if (mid < R) add(L, R, v, u << 1 | 1, mid + 1, r);
}
void Add(int l, int r, int x) { add(l, r, x, 1, 1, n); }
int main() {
  scanf("%d%d%d", &n, &m, &q);
  FOR(i, 1, n) scanf("%d", a + i);
  build(1, 1, n);
  FOR(i, 1, q) {
    int op, l, r, x;
    scanf("%d%d%d%d", &op, &l, &r, &x);
    if (op == 1) {
      Assign(l, r, x);
    } else {
      Add(l, r, x);
    }
  }
  apply(1, n, 1, 1, n);
  FOR(i, 1, m) printf("%lld\n", b[i]);
  return 0;
}
