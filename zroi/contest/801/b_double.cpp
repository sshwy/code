// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

const int N = 1e5 + 5, P = 998244353;
int pw(int a, int m) {
  int res = 1;
  while (m) m & 1 ? res = 1ll * res * a % P : 0, a = 1ll * a * a % P, m >>= 1;
  return res;
}
// int inv(int x){ return pw(x,P-2); }
// int inv(double x){ return pw(x%P,P-2); }
double inv(double x) { return 1.0 / x; }

void add(double &x, double y) { x = (x + y); }

struct qxx {
  int nex, t;
} e[N * 2];
int h[N], le;
void ae(int u, int v) { e[++le] = (qxx){h[u], v}, h[u] = le; }

int n, S, v[N], son[N], s[N], fa[N];
double dg[N], idg[N], idg1[N], c1[N], c2[N], c3[N], f[N], g[N], ans;

void dfs(int u, int p) {
  if (!p) c1[p] = 1;
  c1[u] = c1[p] * idg[u];
  c2[u] = c1[u] * c1[p];
  fa[u] = p;
  for (int i = h[u]; i; i = e[i].nex) {
    int v = e[i].t;
    if (v == p) continue;
    son[u] = v;
    s[u]++;
    dfs(v, u);
    add(c3[u], c3[v]);
  }
  add(c3[u], c2[u]);
  printf("u %d c1 %.6lf c2 %.6lf c3 %.6lf\n", u, c1[u], c2[u], c3[u]);
}
void dfs2(int u, int p) {
  if (!p) {
    f[u] = 1;
    g[u] = 1;
  } else {
    add(f[u], f[p] * idg[p]);
    add(f[u], ((c3[p] - c3[u] - c2[p])) * pw(inv(c1[p]) * idg[p], 2));
    add(g[u], ((c3[p] - c3[u] - c2[p])) * pw(inv(c1[p]) * idg[p], 2));
    if (fa[p]) {
      assert(dg[p] > 1);
      add(f[u], f[fa[p]] * pw(idg[fa[p]], 2) * idg[p] * idg1[p]);
      add(g[u], f[fa[p]] * pw(idg[fa[p]], 2) * idg[p] * idg1[p]);
    }
  }
  printf("u %d f %.6lf g %.6lf\n", u, f[u], g[u]);
  if (s[u] == 1) {
    double coef = (c3[son[u]] - c2[son[u]]) * pw(inv(c1[u]), 2) * g[u];
    add(ans, coef * v[u]);
  }
  if (s[u] == 0) {
    add(ans, g[u] * v[u]);
    add(ans, f[p] * idg[p] * idg[u] * idg[p] * v[u]);
  }
  for (int i = h[u]; i; i = e[i].nex) {
    int v = e[i].t;
    if (v == p) continue;
    dfs2(v, u);
  }
}
int main() {
  scanf("%d%d", &n, &S);
  FOR(i, 1, n) scanf("%d", v + i);
  FOR(i, 1, n - 1) {
    int x, y;
    scanf("%d%d", &x, &y);
    ae(x, y);
    ae(y, x);
    dg[x]++;
    dg[y]++;
  }
  FOR(i, 1, n) {
    idg[i] = inv(dg[i]);
    if (dg[i] > 1) idg1[i] = inv(dg[i] - 1);
  }
  dfs(S, 0);
  dfs2(S, 0);
  printf("%.6lf\n", ans);
  return 0;
}
