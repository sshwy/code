// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

const int N = 2e5 + 5;

int n, m, fa[N], dep[N], w[N];
vector<int> g[N];

void dfs0(int u, int p) {
  fa[u] = p, dep[u] = dep[p] + 1;
  for (auto v : g[u])
    if (v != p) dfs0(v, u);
}
void dfs(int u, int p, int target, bool in) {
  if (in) w[u]++;
  for (auto v : g[u])
    if (v != p) { dfs(v, u, target, in || v == target); }
}
void SubtreeAdd(int u, int v) { dfs(u, 0, v, u == v); }

void PathAdd(int u, int v) {
  if (dep[u] < dep[v]) swap(u, v);
  while (dep[u] > dep[v]) w[u]++, u = fa[u];
  while (u != v) w[u]++, w[v]++, u = fa[u], v = fa[v];
  w[u]++;
}
int dist(int u, int v) {
  int d = 0;
  if (dep[u] < dep[v]) swap(u, v);
  while (dep[u] > dep[v]) d++, u = fa[u];
  while (u != v) d += 2, u = fa[u], v = fa[v];
  return d;
}
long long Query(int v) {
  long long res = 0;
  FOR(u, 1, n) {
    // printf("dist(%d, %d) = %d\n", u, v, dist(u, v));
    res += 1ll * w[u] * dist(u, v);
  }
  return res;
}

int main() {
  scanf("%d", &n);
  FOR(i, 1, n - 1) {
    int u, v;
    scanf("%d%d", &u, &v);
    g[u].pb(v);
    g[v].pb(u);
  }
  dfs0(1, 0);
  scanf("%d", &m);
  FOR(i, 1, m) {
    int typ, u, v;
    scanf("%d", &typ);
    if (typ == 1) {
      scanf("%d%d", &u, &v);
      SubtreeAdd(u, v);
    } else if (typ == 2) {
      scanf("%d%d", &u, &v);
      PathAdd(u, v);
    } else {
      scanf("%d", &v);
      // FOR(i, 1, n) printf("%d%c", w[i], " \n"[i == n]);
      printf("%lld\n", Query(v));
    }
  }
  return 0;
}
