// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

const int N = 410;
int n, K, P, f[N][N], c1[N][N], c2[N][N];

int pw(int a, int m) {
  int res = 1;
  while (m) m & 1 ? res = 1ll * res * a % P : 0, a = 1ll * a * a % P, m >>= 1;
  return res;
}

void add(int &x, int y) { x = (x + y) % P; }

void DP() {
  FOR(i, 1, n) FOR(j, 1, n) if (i != j) f[i][j] = 1;
  FOR(i, 1, n - 2) {
    memset(c1, 0, sizeof(c1)); // hor
    memset(c2, 0, sizeof(c2)); // ver
    FOR(j, 1, n) FOR(k, 1, n) {
      if (f[j][k]) {
        // printf("f[%d, %d, %d] = %d\n", i, j, k, f[j][k]);
        int v = f[j][k];
        if (j != k) {
          if (k + 1 <= j) {
            int t = 1ll * (j - k) * v % P;
            add(c1[j][k], t), add(c1[j][k + 1], P - t);
          }
          if (max(k, j) + 1 <= n)
            add(c2[max(k, j) + 1][k], v), add(c2[n + 1][k], P - v);
          if (k <= j)
            add(c1[j][k], v), add(c1[j][n + 1], P - v);
          else
            add(c1[k][k], v), add(c1[k][n + 1], P - v);
        }
      }
    }
    FOR(j, 1, n) FOR(k, 1, n) {
      add(c1[j][k], c1[j][k - 1]);
      add(c2[j][k], c2[j - 1][k]);
    }
    FOR(j, 1, n) FOR(k, 1, n) f[j][k] = (c1[j][k] + c2[j][k]) % P;
  }
}
int F[N], G[N], C[N][N];
int main() {
  scanf("%d%d%d", &n, &K, &P);

  DP();
  int ans = 0;
  FOR(j, 1, n) FOR(k, 1, n) {
    if (j != k) add(F[max(j, k)], f[j][k]);
  }
  FOR(i, 1, n) add(F[i], F[i - 1]);

  FOR(i, 1, n) {
    long long coef = i % 2 ? P - 1 : 1;
    FOR(j, 1, i) {
      coef = coef * (i - j + 1) % P * pw(j, P - 2) % P;
      coef = (P - coef) % P;
      add(G[i], F[j] * coef % P);
    }
  }

  long long coef = 1;
  FOR(i, 1, min(n, K)) {
    coef = coef * (K - i + 1) % P * pw(i, P - 2) % P;
    add(ans, G[i] * coef % P);
  }

  printf("%d\n", ans);

  return 0;
}
