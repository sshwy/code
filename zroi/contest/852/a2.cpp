// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

int n, k, p, a[200], b[200], c[200], ans;

void check() {
  FOR(i, 1, n) b[i] = i == 1 ? a[i] : max(b[i - 1], a[i]);
  ROF(i, n, 1) c[i] = i == n ? a[i] : min(c[i + 1], a[i]);
  FOR(i, 1, n - 1) if (b[i] == c[i + 1]) return;
  FOR(i, 1, n) printf("%d%c", a[i], " \n"[i == n]);
  ++ans;
  ans %= p;
}
void dfs(int cur) {
  if (cur == n + 1) {
    check();
    return;
  }
  FOR(i, 1, k) {
    a[cur] = i;
    dfs(cur + 1);
  }
}
int main() {
  scanf("%d%d%d", &n, &k, &p);
  dfs(1);
  printf("%d\n", ans);
  return 0;
}
