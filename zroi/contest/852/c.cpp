// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
#define for_adj(i, u, v) for (int i = h[u], v; v = e[i].t, i; i = e[i].nex)

typedef long long LL;
typedef pair<LL, LL> Tag;
const int N = 2e5 + 5;

struct qxx {
  int nex, t;
} e[N * 2];
int h[N], le;
void ae(int u, int v) { e[++le] = (qxx){h[u], v}, h[u] = le; }

int n, m;
LL s1[N];
void operator+=(Tag &x, Tag y) {
  x.first += y.first;
  x.second += y.second;
}
struct SegmentTree {
  struct Node {
    LL s, t1;
    Tag t3;
  } c[N << 2];
  void nodeAddBySize(int u, int l, int r, LL coef) {
    c[u].s += (s1[r] - s1[l - 1]) * coef;
    c[u].t1 += coef;
  }
  void nodeAddBySeq(int u, int l, int r, Tag v) {
    c[u].s += 1ll * (v.first + v.first + v.second * (r - l)) * (r - l + 1) / 2;
    c[u].t3 += v;
  }
  void pushup(int u) { c[u].s = c[u << 1].s + c[u << 1 | 1].s; }
  void pushdown(int u, int l, int r) {
    int mid = (l + r) >> 1;
    if (c[u].t1)
      nodeAddBySize(u << 1, l, mid, c[u].t1),
          nodeAddBySize(u << 1 | 1, mid + 1, r, c[u].t1), c[u].t1 = 0;
    if (c[u].t3 != Tag(0, 0)) {
      nodeAddBySeq(u << 1, l, mid, c[u].t3);
      nodeAddBySeq(u << 1 | 1, mid + 1, r,
          Tag(c[u].t3.first + c[u].t3.second * (mid + 1 - l), c[u].t3.second));
      c[u].t3 = Tag(0, 0);
    }
  }
  void _rangeAddBySize(int L, int R, int u, int l, int r) {
    if (L <= l && r <= R) {
      nodeAddBySize(u, l, r, 1);
      return;
    }
    pushdown(u, l, r);
    int mid = (l + r) >> 1;
    if (L <= mid) _rangeAddBySize(L, R, u << 1, l, mid);
    if (mid < R) _rangeAddBySize(L, R, u << 1 | 1, mid + 1, r);
    pushup(u);
  }
  void rangeAddBySize(int L, int R) { _rangeAddBySize(L, R, 1, 1, n); }
  void _rangeAddBySeq(int L, int R, Tag v, int u, int l, int r) {
    if (L <= l && r <= R) {
      nodeAddBySeq(u, l, r, Tag((l - L) * v.second + v.first, v.second));
      return;
    }
    pushdown(u, l, r);
    int mid = (l + r) >> 1;
    if (L <= mid) _rangeAddBySeq(L, R, v, u << 1, l, mid);
    if (mid < R) _rangeAddBySeq(L, R, v, u << 1 | 1, mid + 1, r);
    pushup(u);
  }
  void rangeAdd(int L, int R, LL v) { _rangeAddBySeq(L, R, Tag(v, 0), 1, 1, n); }
  void rangeAddBySeq(int L, int R, Tag v) { _rangeAddBySeq(L, R, v, 1, 1, n); }
  LL _getRangeValSum(int L, int R, int u, int l, int r) {
    if (L > R) return 0;
    if (L <= l && r <= R) return c[u].s;
    pushdown(u, l, r);
    int mid = (l + r) >> 1;
    LL res = 0;
    if (L <= mid) res += _getRangeValSum(L, R, u << 1, l, mid);
    if (mid < R) res += _getRangeValSum(L, R, u << 1 | 1, mid + 1, r);
    return res;
  }
  LL getRangeValSum(int L, int R) {
    LL res2 = _getRangeValSum(L, R, 1, 1, n);
    return res2;
  }
} seg;

int fa[N], dep[N], w[N], big[N], sz[N], top[N], totdfn, nd[N], L[N], R[N];

void dfs0(int u, int p) {
  fa[u] = p, dep[u] = dep[p] + 1, sz[u] = 1;
  for_adj(i, u, v) if (v != p) {
    dfs0(v, u);
    sz[u] += sz[v];
    if (!big[u] || sz[big[u]] < sz[v]) big[u] = v;
  }
}

void dfs1(int u, int p, int tp) {
  top[u] = tp, L[u] = ++totdfn, nd[totdfn] = u;
  if (big[u]) dfs1(big[u], u, tp);
  for_adj(i, u, v) if (v != p && v != big[u]) { dfs1(v, u, v); }
  R[u] = totdfn;
}

int lca(int u, int v) {
  while (top[u] != top[v]) {
    if (dep[top[u]] < dep[top[v]]) swap(u, v);
    u = fa[top[u]];
  }
  return dep[u] < dep[v] ? u : v;
}

void addSubtreeValBySize(int u) {
  seg.rangeAddBySize(L[u], R[u]);
  for (int z = fa[u]; z; z = fa[top[z]]) { seg.rangeAdd(L[top[z]], L[z], sz[u]); }
}
int getSon(int u, int v) {
  int las = -1;
  while (top[u] != top[v]) {
    las = top[u];
    u = fa[las];
  }
  if (u == v) return las;
  return nd[L[v] + 1];
}
void addPathVal(int u, int v, LL val) {
  for (int z = v; z; z = fa[top[z]]) { seg.rangeAdd(L[top[z]], L[z], val); }
}
void SubtreeAdd(int u, int v) {
  if (u == v) {
    addSubtreeValBySize(1);
    return;
  }
  if (lca(u, v) == v) { // v is u's ancestor
    int v2 = getSon(u, v);
    if (1 < L[v2]) seg.rangeAddBySize(1, L[v2] - 1);
    if (R[v2] < n) seg.rangeAddBySize(R[v2] + 1, n);
    addPathVal(1, v, -sz[v2]);
  } else {
    addSubtreeValBySize(v);
  }
}

void PathAdd_1(int z, int u) {
  int cur = 1;
  while (top[u] != top[z]) {
    int v = top[u];
    seg.rangeAddBySeq(L[v], L[u], Tag(cur + L[u] - L[v], -1));
    cur += L[u] - L[v] + 1;
    u = fa[v];
  }
  if (L[z] >= L[u]) return;
  seg.rangeAddBySeq(L[z] + 1, L[u], Tag(cur + L[u] - L[z] - 1, -1));
}
void PathAdd(int u, int v) {
  int z = lca(u, v), dist = dep[u] + dep[v] - dep[z] * 2 + 1;
  PathAdd_1(z, u);
  PathAdd_1(z, v);
  for (int x = z; x; x = fa[top[x]]) { seg.rangeAdd(L[top[x]], L[x], dist); }
}
LL getPathValByBig(int u, int v) { return seg.getRangeValSum(L[u] + 1, L[v] + 1); }
LL getPathValBySize(int u, int v) { return seg.getRangeValSum(L[u], L[v]); }
LL Query(int v) {
  LL res = seg.getRangeValSum(1, n), sub = 0;
  if (v != top[v]) {
    sub += getPathValByBig(top[v], fa[v]) + getPathValBySize(top[v], fa[v]);
  }
  for (int z = top[v]; z && fa[z]; z = top[z]) {
    sub += seg.getRangeValSum(L[z], L[z]) + seg.getRangeValSum(L[fa[z]], L[fa[z]]);
    z = fa[z];
    if (z != top[z]) {
      sub += getPathValByBig(top[z], fa[z]) + getPathValBySize(top[z], fa[z]);
    }
  }
  sub += seg.getRangeValSum(L[v], L[v]);
  LL ans = res - sub + 1ll * (dep[v] - 1) * seg.getRangeValSum(L[1], L[1]);
  return ans;
}

void init() {
  dfs0(1, 0);
  dfs1(1, 0, 1);
  FOR(i, 1, n) s1[i] = sz[nd[i]];
  FOR(i, 1, n) s1[i] += s1[i - 1];
}
int main() {
  scanf("%d", &n);
  FOR(i, 1, n - 1) {
    int u, v;
    scanf("%d%d", &u, &v);
    ae(u, v);
    ae(v, u);
  }
  init();
  scanf("%d", &m);
  FOR(i, 1, m) {
    int typ, u, v;
    scanf("%d", &typ);
    if (typ == 1) {
      scanf("%d%d", &u, &v);
      SubtreeAdd(u, v);
    } else if (typ == 2) {
      scanf("%d%d", &u, &v);
      PathAdd(u, v);
    } else {
      scanf("%d", &v);
      printf("%lld\n", Query(v));
    }
  }
  return 0;
}
