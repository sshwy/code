// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

const int N = 2e5 + 5, P = 998244353;
;
void add(int &x, int y) { x = (x + y) % P; }
int pw(int a, int m) {
  int res = 1;
  while (m) m & 1 ? res = 1ll * res * a % P : 0, a = 1ll * a * a % P, m >>= 1;
  return res;
}

namespace DFT {
  const int SZ = 1 << 19;
  int tr[SZ];
  int init(int n) {
    int len = 1;
    while (len < n) len <<= 1;
    FOR(i, 0, len - 1) tr[i] = tr[i >> 1] >> 1 | ((i & 1) * (len >> 1));
    return len;
  }
  void dft(int *f, int len, int tag) {
    FOR(i, 0, len - 1) if (tr[i] < i) swap(f[i], f[tr[i]]);
    for (int j = 1; j < len; j <<= 1)
      for (int i = 0, wn = pw(3, (P - 1) + tag * (P - 1) / (j << 1)); i < len;
           i += j << 1)
        for (int k = i, u, v, w = 1; k < i + j; k++, w = 1ll * w * wn % P)
          u = f[k], v = 1ll * w * f[k + j] % P, f[k] = (u + v) % P,
          f[k + j] = (u - v + P) % P;
    if (tag == -1)
      for (int i = 0, in = pw(len, P - 2); i < len; i++) f[i] = 1ll * f[i] * in % P;
  }
  int F[SZ], G[SZ];
  void mul(const vector<int> &f, const vector<int> &g, vector<int> &h) {
    int lf = f.size(), lg = g.size();
    int len = init(lf + lg - 1);
    fill(F, F + len, 0), fill(G, G + len, 0);
    FOR(i, 0, lf - 1) F[i] = f[i];
    FOR(i, 0, lg - 1) G[i] = g[i];

    dft(F, len, 1), dft(G, len, 1);
    FOR(i, 0, len - 1) F[i] = 1ll * F[i] * G[i] % P;
    dft(F, len, -1);

    h.clear(), h.resize(len, 0);
    FOR(i, 0, len - 1) h[i] = F[i];
    while (h.back() == 0) h.pop_back();
  }
} // namespace DFT

struct Poly {
  vector<int> f;
  Poly() {}
  void init(int len) { f.clear(), f.resize(len, 0); }
  Poly(int len) { init(len); }
  int size() { return f.size(); }
  Poly operator*(Poly g) const {
    Poly res;
    DFT::mul(f, g.f, res.f);
    return res;
  }
  int &operator[](unsigned x) { return assert(0 <= x && x < f.size()), f[x]; }
} f[N];

Poly Merge(int l, int r) {
  if (l == r) return f[l];
  int mid = (l + r) >> 1;
  return Merge(l, mid) * Merge(mid + 1, r);
}

int n, m, a[N], b[N];

int main() {

  scanf("%d%d", &n, &m);
  int s = 0, sb = 0;
  FOR(i, 1, n) scanf("%d", a + i), s += a[i];
  FOR(i, 1, m) scanf("%d", b + i), s += b[i], sb += b[i];

  int is = pw(s, P - 2);

  FOR(i, 1, n) {
    f[i].init(a[i] + 1);
    f[i][0] = -1;
    f[i][a[i]] = 1;
  }

  Poly G(sb + 1);

  FOR(i, 1, m) {
    int pr = 1ll * b[i] * is % P;
    add(G[sb - b[i]], pr);
  }

  Poly F = Merge(1, n) * G;

  int ans = 0;
  FOR(i, 0, F.size() - 1) {
    add(ans, 1ll * F[i] * pw((P + 1 - 1ll * i * is % P) % P, P - 2) % P);
  }
  ans = ((0ll + n + m - ans) % P + P) % P;

  printf("%d\n", ans);

  return 0;
}
