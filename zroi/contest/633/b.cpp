// by Sshwy
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

const int N = 1e5 + 5, SZ = N << 2;
int n;
int a[N];
long long f[N];
pair<int, int> pa[N];

vector<pair<int, int>> v[SZ];
void insert(int pos, pair<int, int> pr, int u = 1, int l = 1, int r = n) {
  v[u].pb(pr);
  if (l == r) return;
  int mid = (l + r) >> 1;
  if (pos <= mid)
    insert(pos, pr, u << 1, l, mid);
  else
    insert(pos, pr, u << 1 | 1, mid + 1, r);
}
bool cmp(pair<int, int> x, pair<int, int> y) { return x.second < y.second; }
int q[N], ql, qr;

#define X(j) (j)
#define Y(j) (f[j] - (j * 1ll * j + j) / 2)
#define B(i) ((i * 1ll * i - i) / 2)
#define K(i) (-i)
typedef __int128 LLL;
void dp(vector<pair<int, int>> &L, vector<pair<int, int>> &R) {
  sort(L.begin(), L.end(), cmp);
  sort(R.begin(), R.end(), cmp);

  ql = 1, qr = 0;
  auto pl = L.begin();
  for (auto pr : R) {
    while (pl != L.end() && pl->second < pr.second) {
      int j = pl->second;
      while (ql < qr &&
             (LLL)(X(q[qr]) - X(q[qr - 1])) * 1ll * (Y(j) - Y(q[qr])) -
                     (LLL)(X(j) - X(q[qr])) * 1ll * (Y(q[qr]) - Y(q[qr - 1])) >=
                 0)
        --qr;
      q[++qr] = j;
      ++pl;
    }
    if (ql <= qr) {
      int i = pr.second;
      while (ql < qr &&
             (LLL)K(i) * 1ll * (X(q[ql + 1]) - X(q[ql])) <= (Y(q[ql + 1]) - Y(q[ql])))
        ++ql;
      int j = q[ql];
      f[i] = max(f[i], Y(j) - K(i) * 1ll * X(j) - B(i) + a[i]);
    }
  }
}
void dfs(int u, int l, int r) {
  if (l == r) return;
  int mid = (l + r) >> 1;
  dfs(u << 1, l, mid);
  dp(v[u << 1], v[u << 1 | 1]);
  dfs(u << 1 | 1, mid + 1, r);
}
int main() {
  scanf("%d", &n);
  FOR(i, 1, n) scanf("%d", &a[i]), pa[i] = {a[i], i};
  sort(pa + 1, pa + n + 1);
  FOR(i, 1, n) insert(i, pa[i]);
  f[0] = 0;
  FOR(i, 1, n) f[i] = -(i * (i - 1ll) / 2) + a[i];
  dfs(1, 1, n);
  long long ans = -1e18;
  FOR(i, 0, n) ans = max(ans, f[i] - ((n - i + 1ll) * (n - i) / 2));
  printf("%lld\n", ans);
  return 0;
}
