// by Sshwy
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

const int K = 1e7 + 5, P = 998244353;
int fac[K], fnv[K];
int pw(int a, int m) {
  int res = 1;
  while (m) m & 1 ? res = 1ll * res * a % P : 0, a = 1ll * a * a % P, m >>= 1;
  return res;
}
long long t[K];
int f(int m, int k, int B) {
  long long res = (pw(B, m - k + 1) - 1) * 1ll * pw(B - 1, P - 2) % P; // f(m-k,0)
  long long coef = pw(B, m - k);
  long long B1 = pw(P + 1 - B, P - 2);
  B1 = B1 * B % P;
  FOR(i, 1, k) {
    coef = coef * B % P * (m - k + i) % P;
    long long v = fnv[i] % P * coef % P;
    res = (res - v + P) * B1 % P;
  }
  return res;
}
void go() {
  int n, k, A;
  scanf("%d%d%d", &n, &k, &A);
  if (A == 1) return puts("1"), void();
  int B = pw(A, P - 2);
  int ans = pw(A, n) * 1ll * f(n - 1, k - 1, B) % P;
  long long tot = 1;
  FOR(i, 0, k - 1) tot = tot * (n - i) % P;
  tot = tot * fnv[k] % P;
  ans = ans * 1ll * pw(tot, P - 2) % P;
  printf("%d\n", ans);
}
int main() {
  fac[0] = 1;
  FOR(i, 1, K - 1) fac[i] = fac[i - 1] * 1ll * i % P;
  fnv[K - 1] = pw(fac[K - 1], P - 2);
  ROF(i, K - 1, 1) fnv[i - 1] = fnv[i] * 1ll * i % P;

  int t;
  scanf("%d", &t);
  FOR(i, 1, t) go();
  return 0;
}
