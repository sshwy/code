// by Sshwy
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

using namespace RA;

int main() {
  srand(clock() + time(0));
  int n = 200;
  int L = rnd(1, 50) * 2;
  int J = rnd(1e4, 1e5);
  printf("%d %d %d\n", n, L, J);
  FOR(i, 1, n) printf("%d%c", rnd(1, 1e3), " \n"[i == n]);
  return 0;
}
