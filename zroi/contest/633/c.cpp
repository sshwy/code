// by Sshwy
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA
const int N = 2e5 + 5;
int n, L, J;
long long a[N], b[N], c[N];
vector<pair<long long, int>> noi(1);
long double f[N];
int main() {
  scanf("%d%d%d", &n, &L, &J);
  FOR(i, 1, n) scanf("%lld", &a[i]), b[i] = a[i];
  sort(b + 1, b + n + 1);
  int pos = 1, cnt = 0;
  long long cur = 1, money = 0;
  while (1) { // on cur-th day
    long long t1 =
        cnt ? max(1ll, (long long)ceil((J - money) * 1.0 / (cnt * (L / 2))) - 1)
            : 1e18;
    // cur, ... ,cur+t1-1
    if (pos <= n) {
      if (cur + t1 - 1 < b[pos]) { // og a NOI on cur+t1-1
        noi.pb({cur + t1 - 1, cnt});
        cur = cur + t1;
        cnt--;
        money = 0;
      } else {
        money += (b[pos] - cur) * cnt * (L / 2);
        cur = b[pos];
        ++cnt;
        ++pos;
      }
    } else {
      assert(t1 < 1e18);
      noi.pb({cur + t1 - 1, cnt});
      cur = cur + t1;
      cnt--;
      money = 0;
      if (cnt == 0) break;
    }
  }
  // for(auto pr:noi)printf("(%lld,%d) ",pr.first,pr.second); puts("");
  assert(noi.size() == n + 1);
  f[n] = L;
  ROF(i, n - 1, 1) {
    f[i] = L + (long double)(noi[i].second - 1.0) / noi[i].second *
                   ((long double)(noi[i + 1].first - noi[i].first - 1.0) * (L / 2) +
                       f[i + 1]);
  }
  FOR(i, 1, n) c[i] = noi[i].first;
  FOR(i, 1, n) {
    int pos = lower_bound(c + 1, c + n + 1, a[i]) - c;
    assert(1 <= pos && pos <= n);
    long double ans = (long double)(c[pos] - a[i]) * (L / 2) + f[pos];
    printf("%.12Lf\n", ans);
  }
  return 0;
}
