// by Sshwy
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA
const int N = 1e5 + 5, K = 6;

int n, k, totid;
int a[N][K];

typedef tuple<int, int, int, int, int> atom;
vector<int> v_id[200];

int fac[] = {1, 1, 2, 6, 24, 120};
int id(vector<int> v) {
  int res = 0;
  for (unsigned i = 0; i < v.size(); i++) {
    int smaller = 0;
    for (unsigned j = i + 1; j < v.size(); ++j) {
      if (v[j] < v[i]) smaller++;
    }
    res += fac[i] * smaller;
    res += fac[i];
  }
  for (auto x : v) cout << x << " ";
  cout << ": " << res << endl;
  return res;
}
bool use[K];
void dfs(vector<int> v) {
  if (v.size()) v_id[id(v)] = v;
  FOR(i, 1, k)
  if (!use[i]) use[i] = 1, v.pb(i), dfs(v), v.pop_back(), use[i] = 0;
}
vector<int> operator+(const vector<int> A, const vector<int> B) {
  vector<int> res = A;
  return res.insert(res.end(), B.begin(), B.end()), res;
}
void get_max(int &x, int y) { x = max(x, y); }
int f[N][200];

int main() {
  scanf("%d%d", &n, &k);
  FOR(i, 1, n) FOR(j, 1, k) scanf("%d", &a[i][j]);
  dfs(vector<int>());
  totid = fac[0] + fac[1] + fac[2] + fac[3] + fac[4] + fac[5];
  FOR(ri, 1, totid) if (v_id[ri].size()) f[n][ri] = n;
  long long ans = 0;
  ROF(i, n - 1, 1) {
    FOR(ri, 1, totid) if (v_id[ri].size()) {
      auto r = v_id[ri];
      assert(r.size());
      if (r.size() == 1) {
        int j = i;
        while (j <= n && a[i][r[0]] == a[j + 1][r[0]]) ++j;
        f[i][ri] = j; // 只能走到j，走不到下一格
      } else {
        vector<int> r1(1, r[0]), A, B(r.begin() + 1, r.end());
        if (a[i][r[0]] == a[i + 1][r[0]]) {
          get_max(f[i][ri], f[i + 1][ri]);
        } else {
          bool flag = 0;
          while (B.size()) {
            A.pb(*B.begin()), B.erase(B.begin());
            int j = f[i + 1][id(A)] + 1;
            if (a[i][r[0]] == a[j][r[0]]) {
              get_max(f[i][ri], f[i + 1][id(A + r1 + B)]);
              flag = 1;
              break;
            }
          }
          if (flag == 0) { get_max(f[i][ri], f[i + 1][id(A + B)]); }
        }
      }
    }
    int j = *max_element(f[i] + 1, f[i] + totid + 1);
    ans += (j - i + 1) * 1ll * (j - i + 2) / 2;
  }
  ans++; // [n,n]
  printf("%lld\n", ans);
  return 0;
}
