// by Sshwy
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA
const int N = 5e5 + 5;
int n;
char s[N];

const int K = 21;
int mod[K] = {998244353, int(1e9 + 7), int(1e9 + 9), int(1e8 + 7), 10000019, 1000037,
    3383669, 786433, 5767169, 7340033, 23068673, 104857601, 167772161, 469762049,
    1004535809, 97, 257, 7681, 12289, 40961, 65537};
int h[K][N], pm[K][N];

long long pw(int a, int m, int p) {
  long long res = 1;
  while (m) m & 1 ? res = 1ll * res * a % p : 0, a = 1ll * a * a % p, m >>= 1;
  return res;
}
bool check(int l, int r) {
  FOR(j, 0, K - 1) {
    int hsh =
        (h[j][r] - h[j][l - 1] * 1ll * pm[j][r - l + 1] % mod[j] + mod[j]) % mod[j];
    if (pw(hsh, (mod[j] - 1) / 2, mod[j]) != 1) return 0;
  }
  return 1;
}

int main() {
  scanf("%d", &n);
  scanf("%s", s + 1);
  FOR(j, 0, K - 1) {
    pm[j][0] = 1;
    FOR(i, 1, n) {
      h[j][i] = (h[j][i - 1] * 2ll + (s[i] - '0')) % mod[j];
      pm[j][i] = pm[j][i - 1] * 2ll % mod[j];
    }
  }
  ROF(l, n, 1) {
    FOR(i, 1, n - l + 1) if (s[i] == '1') {
      int j = i + l - 1;
      if (i < j && s[j - 1] != '0') continue;
      if (check(i, j)) {
        printf("%d\n", l);
        return 0;
      }
    }
  }
  return 0;
}
