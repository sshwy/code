#include <algorithm>
#include <iostream>
using namespace std;
const int N = 1e5 + 5;
typedef long long LL;
int n, k, a[N][5];
LL ans;
inline LL calc(int x) { return (x + 1LL) * x >> 1; }
int findL(int x) {
  int p[] = {0, 1, 2, 3, 4}, b[5], tot, res = x;
  do {
    tot = 0;
    b[0] = b[1] = b[2] = b[3] = b[4] = -1;
    for (int i = x;; --i) {
      if (!i) return 1;
      bool ok = 0;
      for (int j = 0; j < k; ++j)
        if (a[i][j] == b[j]) ok = 1;
      if (!ok) {
        if (tot == k) {
          res = min(res, i + 1);
          break;
        }
        b[p[tot]] = a[i][p[tot]], ++tot;
      }
    }
  } while (next_permutation(p, p + k));
  return res;
}
int findR(int x) {
  int p[] = {0, 1, 2, 3, 4}, b[5], tot, res = x;
  do {
    tot = 0;
    b[0] = b[1] = b[2] = b[3] = b[4] = -1;
    for (int i = x;; ++i) {
      if (i == n + 1) return n;
      bool ok = 0;
      for (int j = 0; j < k; ++j)
        if (a[i][j] == b[j]) ok = 1;
      if (!ok) {
        if (tot == k) {
          res = max(res, i - 1);
          break;
        }
        b[p[tot]] = a[i][p[tot]], ++tot;
      }
    }
  } while (next_permutation(p, p + k));
  return res;
}
int main() {
  ios::sync_with_stdio(0), cin.tie(0), cout.tie(0);
  cin >> n >> k;
  for (int i = 1; i <= n; ++i)
    for (int j = 0; j < k; ++j) cin >> a[i][j];
  for (int l = 1, r, v; l <= n; l = r + 1) {
    v = findL(l);
    r = findR(v);
    for (int i = l; i <= r; ++i) ans += calc(i - v + 1);
  }
  cout << ans << '\n';
  return 0;
}
