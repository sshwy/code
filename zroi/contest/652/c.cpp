// by Sshwy
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA
const int N = 5005;
int n, m;
int G[N][N];
int lg[N];
int col[N], vis[N];

void dfs(int u) {
  vis[u] = 1;
  bool ban[4] = {0};
  FOR(i, 1, lg[u]) {
    int v = G[u][i];
    if (col[v]) ban[col[v]] = 1;
  }
  ROF(i, 3, 0) if (ban[i] == 0) {
    col[u] = i;
    break;
  }
  FOR(i, 1, lg[u]) {
    int v = G[u][i];
    if (!vis[v]) dfs(v);
  }
}

void check() {
  FOR(i, 1, n) if (col[i] == 0) {
    // FOR(j,1,n)printf("%d%c",col[j]," \n"[j==n]);
    // puts("FUCK!");
    return;
  }
  FOR(j, 1, n) printf("%d%c", col[j], " \n"[j == n]);
  exit(0);
}

bool try_col(int u) { return col[u]; }
int p[N], lp;
int main() {
  srand(clock() + time(0));
  scanf("%d%d", &n, &m);
  FOR(i, 1, m) {
    int u, v;
    scanf("%d%d", &u, &v);
    G[u][++lg[u]] = v;
    G[v][++lg[v]] = u;
  }
  // FOR(i,1,n)if(!vis[i]) dfs(i); // 随便染
  check();
  FOR(i, 1, n) if (col[i] == 0) p[++lp] = i; // 无色

  //模拟退火
  long double T = 1, dT = 1 - (1e-8);
  const long double k = 1;
  long long t = 0;
  while (++t) {
    // if(t % 100000 == 0){
    //    printf("t %lld T %.9Lf p: %.9Lf \n",t,T,exp(-1/(k*T)));
    //    int cnt = 0;
    //    FOR(j,1,n)printf("%d%c",col[j]," :"[j==n]), cnt += col[j] == 0;
    //    printf(" %d\n",cnt);
    //    assert(cnt == lp);
    //}
    if (t % 100 == 0) random_shuffle(p + 1, p + lp + 1);
    int j = RA::rnd(1, lp);
    int u = p[j];      // 随机一个u
    if (col[u] == 0) { // 如果没染色
      int ban[4] = {0};
      FOR(i, 1, lg[u]) {
        int v = G[u][i];
        if (col[v]) ban[col[v]]++;
      }
      int tt[3], lt = 0;
      ROF(i, 3, 1) if (ban[i] == 0) { tt[lt++] = i; }
      if (lt) col[u] = tt[RA::rnd(lt)]; // 随机染色
      if (col[u]) {                     // 成功染色
        p[j] = p[lp];
        --lp;
      } else {
        int c = min_element(ban + 1, ban + 3) - ban;
        int d = ban[c] - 1;
        if (RA::rnd(1e9) / (long double)1e9 < exp(-d / (k * T))) { // 强行染色
          col[u] = c;
          p[j] = p[lp];
          --lp;
          FOR(i, 1, lg[u]) {
            int v = G[u][i];
            if (col[v] == c) {
              col[v] = 0;
              p[++lp] = v;
            }
          }
        }
      }
    }
    u = RA::rnd(1, n);
    if (col[u] != 0) {
      if (RA::rnd(1e9) / (long double)1e9 < exp(-1 / (2 * k * T))) { // 以一定概率去色
        col[u] = 0;
        p[++lp] = u;
      }
    }
    if (lp == 0) { check(); }
    T *= dT;
    // printf("p: "); FOR(i,1,lp)printf("%d%c",p[i]," \n"[i==lp]);
  }

  return 0;
}
