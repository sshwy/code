// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

const int N = 1e4 + 5, K = 1e5;
const long long INF = 1e18;

int n, k;
long long a[N], f[N], g[N];

long long sq(long long x) { return x * x; }
void getMin(long long &x, long long y) { x = min(x, y); }

void work() {
  fill(g + 1, g + n + 1, INF);
  FOR(i, 1, n) {
    FOR(j, 1, i - 1) { getMin(g[i], f[j] + sq(a[i] - a[j + 1])); }
  }
  FOR(i, 1, n) f[i] = g[i];
  FOR(i, 1, n) printf("%lld%c", f[i], " \n"[i == n]);
}

int main() {
  scanf("%d%d", &n, &k);
  FOR(i, 1, n) scanf("%lld", a + i);
  FOR(i, 1, n) f[i] = sq(a[i] - a[1]);
  FOR(j, 2, k) work();
  printf("%lld\n", f[n]);
  return 0;
}
