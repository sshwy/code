// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

using namespace RA;

const int N = 2e5 + 5;

int main() {
  srand(clock() + time(0));
  const int rn = 5000, rw = 1 << 20;
  int n = rnd(1, rn), q = rnd(1, rn);
  // n = 2e5, q = 2e5;
  printf("%d %d\n", n, q);
  FOR(i, 1, n) printf("%d%c", rnd(0, rw), " \n"[i == n]);
  FOR(i, 1, q) {
    int op = rnd(1, 3), l = rnd(1, n), r = rnd(1, n), x = rnd(0, rw);
    if (l > r) swap(l, r);
    if (op == 3)
      printf("%d %d %d\n", op, l, r);
    else
      printf("%d %d %d %d\n", op, l, r, x);
  }
  return 0;
}
