// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

typedef long long LL;
typedef pair<LL, LL> Point;
const int N = 1e4 + 5, K = 1e5;
const LL INF = 1e18;

LL sq(LL x) { return x * x; }
void getMin(LL &x, LL y) { x = min(x, y); }

Point operator-(Point x, Point y) {
  return Point(x.first - y.first, x.second - y.second);
}
__int128 det(Point x, Point y) {
  return __int128(x.first) * y.second - __int128(y.first) * x.second;
}

int n, k, ord[N];
LL a[N], f[N], g[N];

#define X(j) (a[j + 1])
#define Y(j) (sq(a[j + 1]) + f[j])
#define K(i) (2 * a[i])

Point ipoint(int i) { return Point(X(i), Y(i)); }
Point ivec(int i1, int i2) { return ipoint(i2) - ipoint(i1); }
bool check(int i1, int i2, int i3) { return det(ivec(i1, i2), ivec(i2, i3)) > 0; }

bool cmp1(int x, int y) {
  if (X(x) == X(y)) return Y(x) > Y(y);
  return X(x) < X(y);
}
bool cmp2(int x, int y) { return a[x] < a[y]; }

int q[N], ql, qr;
void solve(int l, int r) {
  if (l == r) return;
  int mid = (l + r) >> 1;
  solve(l, mid);

  // printf("solve %d %d\n", l, r);
  FOR(i, l, r) ord[i] = i;
  sort(ord + l, ord + mid + 1, cmp1);
  sort(ord + mid + 1, ord + r + 1, cmp2);

  ql = 1, qr = 0;
  FOR(i, l, mid) {
    int j = ord[i];
    if (Y(j) >= INF) continue;
    // printf("j %d (%lld, %lld)\n", j, X(j), Y(j));
    while (ql < qr && check(q[qr - 1], q[qr], j) == false) --qr;
    q[++qr] = j;
  }

  if (ql <= qr) {
    // FOR(i, 1, qr) printf("(%lld, %lld)%c", X(q[i]), Y(q[i]), " \n"[i == qr]);
    FOR(_i, mid + 1, r) {
      int i = ord[_i];
      while (ql < qr && det(ivec(q[ql], q[ql + 1]), Point(1, K(i))) >= 0) ++ql;
      getMin(g[i], Y(q[ql]) + sq(a[i]) - K(i) * X(q[ql]));
      // assert(Y(q[ql]) + sq(a[i]) - K(i) * X(q[ql]) ==
      //     f[q[ql]] + sq(a[i]) + sq(a[q[ql]+1]) - 2 * a[i] * a[q[ql]+1]);
    }
  }

  solve(mid + 1, r);
}
void work() {
  fill(g + 1, g + n + 1, INF);
  solve(1, n);
  FOR(i, 1, n) f[i] = g[i];
  // FOR(i, 1, n) printf("%lld%c", f[i], " \n"[i == n]);
}

int main() {
  scanf("%d%d", &n, &k);
  FOR(i, 1, n) scanf("%lld", a + i);
  FOR(i, 1, n) f[i] = sq(a[i] - a[1]);
  FOR(j, 2, k) {
    // printf("j %d\n", j);
    work();
  }
  printf("%lld\n", f[n]);
  return 0;
}
