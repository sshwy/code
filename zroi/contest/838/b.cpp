// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

const int N = 505;

int n, k, a[N][N];

void end(bool stat) {
  if (stat)
    puts("Yes");
  else
    puts("No");
  exit(0);
}

namespace G1 {
  int p[N];
  void go() {
    FOR(i, 1, n) p[i] = i;
    do {
      bool flag = true;
      int s = 0;
      FOR(i, 1, n) {
        if (a[i][p[i]] == -1) {
          flag = false;
          break;
        }
        s += a[i][p[i]];
      }
      if (s % k == 0 && flag) { end(true); }
    } while (next_permutation(p + 1, p + n + 1));
    end(false);
  }
} // namespace G1
namespace G2 {
  int p[N];
  void go() {
    FOR(i, 1, n) p[i] = i;
    FOR(_, 1, 500000) {
      random_shuffle(p + 1, p + n + 1);
      bool flag = true;
      int s = 0;
      FOR(i, 1, n) {
        if (a[i][p[i]] == -1) {
          flag = false;
          break;
        }
        s += a[i][p[i]];
      }
      if (s % k == 0 && flag) { end(true); }
    }
    end(false);
  }
} // namespace G2
int main() {
  srand(clock() + time(0));
  scanf("%d%d", &n, &k);
  FOR(i, 1, n) FOR(j, 1, n) scanf("%d", &a[i][j]);
  if (n <= 10) G1::go();
  G2::go();

  return 0;
}
