// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

const int N = 2e5 + 5, D = 21, SZ = N << 2, LIM = 1 << D;

int n, q;

int a[N], t0[SZ], t1[SZ], mx[SZ], tag[SZ], tog[SZ];
void pushup(int u) {
  t0[u] = t0[u << 1] & t0[u << 1 | 1];
  t1[u] = t1[u << 1] & t1[u << 1 | 1];
  mx[u] = max(mx[u << 1], mx[u << 1 | 1]);
}
void build(int u, int l, int r) {
  if (l == r) {
    FOR(i, 0, D - 1) {
      if (a[l] >> i & 1)
        t1[u] |= 1 << i;
      else
        t0[u] |= 1 << i;
    }
    mx[u] = a[l];
    return;
  }
  int mid = (l + r) >> 1;
  build(u << 1, l, mid);
  build(u << 1 | 1, mid + 1, r);
  pushup(u);
}

void nodeAdd(int u, int v) {
  tag[u] += v;
  mx[u] += v;
}
void toggle(int u, int stat) {
  tog[u] ^= stat;
  t0[u] ^= stat;
  t1[u] ^= stat;
}
void pushdown(int u) {
  if (tag[u]) {
    nodeAdd(u << 1, tag[u]);
    nodeAdd(u << 1 | 1, tag[u]);
    tag[u] = 0;
  }
  if (tog[u]) {
    toggle(u << 1, tog[u]);
    toggle(u << 1 | 1, tog[u]);
    tog[u] = 0;
  }
}
void nodeAnd(int u, int l, int r, int x) {
  int flag = (~x) & (~t1[u]) & (~t0[u]) & (LIM - 1);
  int val = (~x) & t1[u] & (LIM - 1);
  nodeAdd(u, -val);
  toggle(u, val);
  x ^= val;
  if (flag) {
    int mid = (l + r) >> 1;
    pushdown(u);
    nodeAnd(u << 1, l, mid, x);
    nodeAnd(u << 1 | 1, mid + 1, r, x);
    pushup(u);
  }
}

void nodeOr(int u, int l, int r, int x) {
  int flag = x & (~t1[u]) & (~t0[u]) & (LIM - 1);
  int val = x & t0[u] & (LIM - 1);
  nodeAdd(u, val);
  toggle(u, val);
  x ^= val;
  if (flag) {
    int mid = (l + r) >> 1;
    pushdown(u);
    nodeOr(u << 1, l, mid, x);
    nodeOr(u << 1 | 1, mid + 1, r, x);
    pushup(u);
  }
}

void doOP(int op, int L, int R, int x, int u, int l, int r) {
  if (L <= l && r <= R) {
    if (op == 0)
      nodeAnd(u, l, r, x);
    else
      nodeOr(u, l, r, x);
    return;
  }
  int mid = (l + r) >> 1;
  pushdown(u);
  if (L <= mid) doOP(op, L, R, x, u << 1, l, mid);
  if (mid < R) doOP(op, L, R, x, u << 1 | 1, mid + 1, r);
  pushup(u);
}

int getMax(int L, int R, int u, int l, int r) {
  if (R < l || r < L) return -1;
  if (L <= l && r <= R) return mx[u];
  int mid = (l + r) >> 1;
  pushdown(u);
  return max(getMax(L, R, u << 1, l, mid), getMax(L, R, u << 1 | 1, mid + 1, r));
}

int main() {
  scanf("%d%d", &n, &q);
  FOR(i, 1, n) scanf("%d", a + i);
  build(1, 1, n);
  FOR(i, 1, q) {
    // printf("i %d\n", i);
    int op, l, r, x;
    scanf("%d%d%d", &op, &l, &r);
    if (op == 1) {
      scanf("%d", &x);
      doOP(0, l, r, x, 1, 1, n);
    } else if (op == 2) {
      scanf("%d", &x);
      doOP(1, l, r, x, 1, 1, n);
    } else {
      int ans = getMax(l, r, 1, 1, n);
      printf("%d\n", ans);
    }
  }
  return 0;
}
