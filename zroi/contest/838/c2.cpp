// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

const int N = 2e5 + 5, D = 21, SZ = N << 2;

int n, q;

int a[N];

int main() {
  scanf("%d%d", &n, &q);
  FOR(i, 1, n) scanf("%d", a + i);
  FOR(i, 1, q) {
    int op, l, r, x;
    scanf("%d%d%d", &op, &l, &r);
    if (op == 1) {
      scanf("%d", &x);
      FOR(i, l, r) a[i] &= x;
    } else if (op == 2) {
      scanf("%d", &x);
      FOR(i, l, r) a[i] |= x;
    } else {
      int ans = -1;
      FOR(i, l, r) ans = max(ans, a[i]);
      printf("%d\n", ans);
    }
  }
  return 0;
}
