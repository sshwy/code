g++ c.cpp -o .u
g++ c2.cpp -o .v
g++ c_gen.cpp -o .w

while true; do
  ./.w > .i
  ./.u < .i > .x
  ./.v < .i > .y
  if diff -w .x .y; then
    echo AC
  else
    echo WA
    break
  fi
done
