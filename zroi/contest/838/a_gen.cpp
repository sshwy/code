// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

using namespace RA;

const int N = 1e4 + 5;

int main() {
  srand(clock() + time(0));
  int n = rnd(1, 100), k = rnd(1, min(n, 100));
  printf("%d %d\n", n, k);
  FOR(i, 1, n) printf("%d%c", rnd(1, 1e6), " \n"[i == n]);
  return 0;
}
