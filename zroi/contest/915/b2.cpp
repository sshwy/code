// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

const int N = 1005;

namespace D {
  int fa[N], tot;
  int get(int u) { return fa[u] == u ? u : fa[u] = get(fa[u]); }
  void init(int n) {
    FOR(i, 0, n) fa[i] = i;
    tot = n;
  }
  bool merge(int u, int v) {
    if (get(u) == get(v)) return false;
    fa[get(u)] = get(v);
    --tot;
    return true;
  }
  bool connected() { return tot == 1; }
} // namespace D

int n, q;
long long a[N][N];

vector<pair<long long, long long>> v;

int main() {
  scanf("%d%d", &n, &q);
  FOR(i, 1, q) {
    int x1, x2, y1, y2, w;
    scanf("%d%d%d%d%d", &x1, &x2, &y1, &y2, &w);
    FOR(x, x1, x2) FOR(y, y1, y2) a[x][y] += w, a[y][x] += w;
  }
  FOR(i, 1, n) FOR(j, i + 1, n) v.push_back({a[i][j], 1ll * (i - 1) * n + j - 1});
  sort(v.begin(), v.end());
  long long ans = 0;
  D::init(n);
  for (auto e : v) {
    int u = e.second % n + 1, v = e.second / n + 1, w = e.first;
    if (D::merge(u, v)) { ans += w; }
    if (D::connected()) break;
  }
  printf("%lld\n", ans);
  return 0;
}
