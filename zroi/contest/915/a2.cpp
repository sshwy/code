#include <bits/stdc++.h>
#define FOR(a, b, c) for (int a = (int)(b); a <= (int)(c); a++)
#define ROF(a, b, c) for (int a = (int)(b); a >= (int)(c); a--)
using namespace std;

const int N = 1e5 + 5, P = 998244353;

int pw(int a, int m) {
  int res = 1;
  while (m) m & 1 ? res = 1ll * res * a % P : 0, a = 1ll * a * a % P, m >>= 1;
  return res;
}
void add(int &x, long long y) { x = (x + y) % P; }

int n, q, fac[N], fnv[N], f[N];
int q1(int a, int b) {
  if (a < b || b < 0) return 0;
  return 1ll * fac[a] * fnv[a - b] % P;
}

int main() {
  fac[0] = 1;
  FOR(i, 1, N - 1) fac[i] = 1ll * fac[i - 1] * i % P;
  fnv[N - 1] = pw(fac[N - 1], P - 2);
  ROF(i, N - 1, 1) fnv[i - 1] = 1ll * fnv[i] * i % P;

  scanf("%d%d", &n, &q);
  FOR(r, 1, n) { FOR(i, r, n) add(f[r], 1ll * ((i - 1) / 2) * q1(i - 2, r - 2)); }
  FOR(i, 1, q) {
    int l, r;
    scanf("%d%d", &l, &r);
    printf("%lld\n", 1ll * f[r] * fac[n - r] % P);
  }
  return 0;
}
