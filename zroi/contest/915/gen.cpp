// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

using namespace RA;

int main() {
  srand(clock() + time(0));
  int n = rnd(1, 100), q = rnd(1, 100);
  printf("%d %d\n", n, q);
  FOR(i, 1, q) {
    int x1 = rnd(1, n), x2 = rnd(1, n), y1 = rnd(1, n), y2 = rnd(1, n),
        w = rnd(-1e6, 1e6);
    if (x1 > x2) swap(x1, x2);
    if (y1 > y2) swap(y1, y2);
    printf("%d %d %d %d %d\n", x1, x2, y1, y2, w);
  }
  return 0;
}
