// by Yao
#include <bits/stdc++.h>
#define FOR(i, a, b) for (int i = (int)(a); i <= (int)(b); ++i)
#define ROF(i, a, b) for (int i = (int)(a); i >= (int)(b); --i)
using namespace std;

const int N = 1e5 + 5, SZ = N * 4;
const long long INF = 0x3f3f3f3f3f3f3f3f;

namespace D {
  int fa[N], tot;
  int get(int u) { return fa[u] == u ? u : fa[u] = get(fa[u]); }
  void init(int n) {
    FOR(i, 0, n) fa[i] = i;
    tot = n;
  }
  bool merge(int u, int v) {
    if (get(u) == get(v)) return false;
    fa[get(u)] = get(v);
    --tot;
    return true;
  }
  bool connected() { return tot == 1; }
} // namespace D
struct Atom {
  int color;
  long long dist;
  Atom() {}
  Atom(int u, long long _d) {
    color = D::get(u);
    dist = _d;
  }
  bool operator<(const Atom o) const { return dist < o.dist; }
  void operator+=(long long x) {
    if (dist == INF) return;
    dist += x;
  }
};

struct Data {
  Atom o1, o2;
  Data() {}
  Data(int u, long long d) {
    o1 = Atom(u, d);
    o2 = Atom(0, INF);
  }
  void operator+=(long long x) {
    o1 += x;
    o2 += x;
  }
  Data operator+(Data d) {
    Data res;
    Atom t[] = {o1, o2, d.o1, d.o2};
    sort(t, t + 4);
    res.o1 = t[0];
    bool flag = false;
    FOR(i, 1, 3) if (t[i].color != res.o1.color) {
      res.o2 = t[i];
      flag = true;
      break;
    }
    assert(flag);
    return res;
  }
  void update(Atom &o, int col) {
    if (o1 < o && o1.color != col) o = o1;
    if (o2 < o && o2.color != col) o = o2;
  }
} s[SZ];
long long tag[SZ];

void pushup(int u) { s[u] = s[u << 1] + s[u << 1 | 1]; }
void build(int u, int l, int r) {
  tag[u] = 0;
  if (l == r) {
    s[u] = Data(l, 0);
    return;
  }
  int mid = (l + r) / 2;
  build(u << 1, l, mid);
  build(u << 1 | 1, mid + 1, r);
  pushup(u);
}
void nodeAdd(int u, long long val) {
  s[u] += val;
  tag[u] += val;
}
void pushdown(int u) {
  if (tag[u]) {
    nodeAdd(u << 1, tag[u]);
    nodeAdd(u << 1 | 1, tag[u]);
    tag[u] = 0;
  }
}
void rangeAdd(int L, int R, long long val, int u, int l, int r) {
  if (L <= l && r <= R) {
    nodeAdd(u, val);
    return;
  }
  int mid = (l + r) >> 1;
  pushdown(u);
  if (L <= mid) rangeAdd(L, R, val, u << 1, l, mid);
  if (mid < R) rangeAdd(L, R, val, u << 1 | 1, mid + 1, r);
  pushup(u);
}

int n, q;

struct Operation {
  int l, r, w;
  void apply() { rangeAdd(l, r, w, 1, 1, n); }
};
vector<Operation> v[N];
void addOperation(int row, int l, int r, int w) {
  v[row].push_back((Operation){l, r, w});
}
Atom p[N];
long long ans = 0;
int main() {
  scanf("%d%d", &n, &q);
  FOR(i, 1, q) {
    int x1, x2, y1, y2, w;
    scanf("%d%d%d%d%d", &x1, &x2, &y1, &y2, &w);
    assert(x1 <= x2 && y1 <= y2);

    addOperation(x1, y1, y2, w);
    addOperation(x2 + 1, y1, y2, -w);
    addOperation(y1, x1, x2, w);
    addOperation(y2 + 1, x1, x2, -w);
  }

  D::init(n);
  while (!D::connected()) {
    build(1, 1, n);
    FOR(i, 1, n) p[i] = Atom(0, INF);
    FOR(i, 1, n) {
      for (unsigned j = 0; j < v[i].size(); j++) v[i][j].apply();
      s[1].update(p[D::get(i)], D::get(i));
    }
    FOR(i, 1, n) if (p[i].dist != INF) {
      if (D::merge(i, p[i].color)) { ans += p[i].dist; }
    }
  }
  printf("%lld\n", ans);
  return 0;
}
