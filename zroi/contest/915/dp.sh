g++ b.cpp -o .u
g++ b2.cpp -o .v
g++ gen.cpp -o .w

while true; do
  ./.w > .i
  ./.u < .i > .x
  ./.v < .i > .y
  if diff .x .y; then
    echo AC
  else
    echo WA
    break
  fi
done
