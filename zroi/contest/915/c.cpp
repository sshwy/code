// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

const int N = 2.5e5 + 5, SZ = N * 40;

unsigned p17[N], p23[N];

struct HashNumber {
  unsigned x1, x2;
  HashNumber() {}
  HashNumber(int x) {
    x1 = x;
    x2 = x;
  }
  bool operator==(HashNumber o) const { return x1 == o.x1 && x2 == o.x2; }
};
HashNumber merge(HashNumber a, int la, HashNumber b) {
  HashNumber s;
  s.x1 = a.x1 + p17[la] * b.x1;
  s.x2 = a.x2 + p23[la] * b.x2;
  return s;
}
typedef pair<int, int> pt;
int n, m, q, rt[N];

int tot, lc[SZ], rc[SZ];
HashNumber s[SZ];

int cp(int u) {
  int u2 = ++tot;
  lc[u2] = lc[u], rc[u2] = rc[u];
  s[u2] = s[u];
  return u2;
}
int Assign(int pos, int x, int u, int l, int r) {
  int u2 = cp(u);
  if (l == r) {
    s[u2] = HashNumber(x);
    return u2;
  }
  int mid = (l + r) >> 1;
  if (pos <= mid)
    lc[u2] = Assign(pos, x, lc[u], l, mid);
  else
    rc[u2] = Assign(pos, x, rc[u], mid + 1, r);
  s[u2] = merge(s[lc[u2]], mid - l + 1, s[rc[u2]]);
}
int Query(int u1, int u2, int l, int r) {
  if (l == r) {
    if (s[u1] == s[u2]) return l + 1;
    return l;
  }
  if(
}

vector<pt> v[N];

bool byQuery(int x, int y) { return query(rt[x], rt[y]); }
int ans[N];
int main() {
  p17[0] = 1;
  FOR(i, 1, N - 1) p17[i] = p17[i - 1] * 17;
  p23[0] = 1;
  FOR(i, 1, N - 1) p23[i] = p23[i - 1] * 23;

  scanf("%d%d%d", &n, &m, &q);
  FOR(i, 1, q) {
    int l, r, p, w;
    scanf("%d%d%d%d", &l, &r, &p, &w);
    v[l].pb(pt(p, w));
    v[r + 1].pb(pt(p, 0));
  }
  FOR(i, 1, n) {
    rt[i] = rt[i - 1];
    for (pt e : v[i]) rt[i] = Assign(e.first, e.second, rt[i], 1, m);
  }
  FOR(i, 1, n) ans[i] = i;
  stable_sort(ans + 1, ans + n + 1, byQuery);
  FOR(i, 1, n) printf("%d%c", ans[i], " \n"[i == n]);
  return 0;
}
