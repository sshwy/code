#include <bits/stdc++.h>
#define FOR(a, b, c) for (int a = (int)(b); a <= (int)(c); a++)
#define ROF(a, b, c) for (int a = (int)(b); a >= (int)(c); a--)
using namespace std;

const int N = 1e5 + 5, P = 998244353, SZ = 1 << 19;

int pw(int a, int m) {
  int res = 1;
  while (m) m & 1 ? res = 1ll * res * a % P : 0, a = 1ll * a * a % P, m >>= 1;
  return res;
}

int tr[SZ];
int init(int n) {
  int len = 1;
  while (len <= n) len *= 2;
  tr[0] = 0;
  FOR(i, 1, len - 1) tr[i] = (tr[i >> 1] >> 1) | ((i & 1) * (len >> 1));
  return len;
}

void dft(int *f, int len, int tag) {
  FOR(i, 0, len - 1) if (tr[i] < i) swap(f[i], f[tr[i]]);
  for (int j = 1; j < len; j <<= 1)
    for (int i = 0, wn = pw(3, P - 1 + (P - 1) / (j << 1) * tag); i < len;
         i += j << 1)
      for (int k = i, u, v, w = 1; k < i + j; k++, w = 1ll * w * wn % P) {
        u = f[k], v = 1ll * w * f[k + j] % P;
        f[k] = (u + v) % P, f[k + j] = (u - v + P) % P;
      }
  if (tag == -1)
    for (int i = 0, in = pw(len, P - 2); i < len; i++) f[i] = 1ll * f[i] * in % P;
}

int n, q, fac[N], fnv[N], g[SZ], h[SZ];
int G(int i) {
  if (i - 1 < 2) return 0;
  return 1ll * fac[i - 2] * ((i - 1) / 2) % P;
}

int main() {
  fac[0] = 1;
  FOR(i, 1, N - 1) fac[i] = 1ll * fac[i - 1] * i % P;
  fnv[N - 1] = pw(fac[N - 1], P - 2);
  ROF(i, N - 1, 1) fnv[i - 1] = 1ll * fnv[i] * i % P;

  scanf("%d%d", &n, &q);

  FOR(i, 0, n - 1) g[i] = G(n - i);
  FOR(i, 0, n - 1) h[i] = fnv[i];

  int len = init(n * 2);
  dft(g, len, 1);
  dft(h, len, 1);
  FOR(i, 0, len - 1) g[i] = 1ll * g[i] * h[i] % P;
  dft(g, len, -1);

  FOR(i, 1, q) {
    int l, r;
    scanf("%d%d", &l, &r);
    printf("%lld\n", 1ll * g[n - r] * fac[n - r] % P);
  }
  return 0;
}
