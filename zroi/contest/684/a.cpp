// by Sshwy
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

bool check(long long x) {
  long long t = sqrt(x);
  while ((t + 1) * (t + 1) <= x) ++t;
  while ((t - 1) * (t - 1) >= x) --t;
  if (t * t == x) return 1;
  return 0;
}
long long xs;
void go(int id) {
  long long n;
  scanf("%lld", &n);
  if (n % 6 >= 3) return printf("%lld\n", n % 6), xs ^= n % 6 * id, void();
  if (n == 0) return puts("0"), void();
  if (n % 6 == 0) return puts("6"), xs ^= 6 * id, void();
  if (n % 6 == 1) {
    long long t = (n - 1) / 3;
    if (check(1 + 4 * t))
      return puts("1"), xs ^= 1 * id, void();
    else
      return puts("7"), xs ^= 7 * id, void();
  }
  long long m = (n - 2) / 3;
  for (long long i = 0; i * (i + 1) * 2 <= m; ++i) {
    long long d = sqrt(m - i * (i + 1));
    if (d * (d + 1) + i * (i + 1) == m) return puts("2"), xs ^= 2 * id, void();
  }
  puts("8");
  xs ^= 8 * id;
}
int main() {
  int t;
  scanf("%d", &t);
  FOR(i, 1, t) go(i);
  fprintf(stderr, "xs = %lld\n", xs);
  return 0;
}
