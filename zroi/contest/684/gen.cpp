// by Sshwy
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  inline long long mul(long long a, long long b, long long p) {
    if (p <= 1000000000ll) return 1ll * a * b % p;
    if (p <= 1000000000000ll)
      return (((a * (b >> 20) % p) << 20) % p + a * (b & ((1 << 20) - 1))) % p;
    long long d = floor(a * (long double)b / p);
    long long res = (a * b - d * p) % p;
    if (res < 0) res += p;
    return res;
  }
  long long rnd(long long p) { return mul(mul(rand(), rand(), p), rand(), p); }
  long long rnd(long long L, long long R) { return rnd(R - L + 1) + L; }
} // namespace RA

int main() {
  int t = 100000;
  printf("%d\n", t);
  FOR(i, 1, t) printf("%lld\n", RA::rnd(1, 1e11));
  return 0;
}
