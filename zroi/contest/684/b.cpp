// by Sshwy
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace IO { // require C++11
  const int _BS = 200000;
  char _ob[_BS], *_z = _ob, _ib[_BS], *_x = _ib, *_y = _ib;
  // Input
  char _nc() {
    return _x == _y && (_y = (_x = _ib) + fread(_ib, 1, _BS, stdin), _x == _y)
               ? EOF
               : *_x++;
  }
  inline void rd(char &c) { c = _nc(); }
  void rd(char *s) {
    char c = _nc();
    while (!isblank(c) && isprint(c) && c != EOF) *s++ = c, c = _nc();
    *s = 0;
  }
  template <class T> void rd(T &res) { // unsigned/signed int/long long/__int128
    res = 0;
    char c = _nc(), flag = 0;
    while (!isdigit(c) && c != '-') c = _nc();
    if (c == '-') flag = 1, c = _nc();
    while (isdigit(c)) res = res * 10 + c - '0', c = _nc();
    if (flag) res = -res;
  }
  template <class T, class... A> void rd(T &hd, A &...rs) { rd(hd), rd(rs...); }
  // Output
  inline void flush() { fwrite(_ob, 1, _z - _ob, stdout), _z = _ob; }
  inline void wrch(char x) {
    if (_z == _ob + _BS - 1) flush();
    *_z = x, ++_z;
  }
  void wr(char *s) {
    while (*s) wrch(*s), ++s;
  }
  void wr(const char *s) {
    while (*s) wrch(*s), ++s;
  }
  template <class T> void wr(T x) { // unsigned/signed int/long long/__int128
    if (x == 0) return wrch('0'), void();
    if (x < 0) wrch('-'), x = -x;
    int _t[50], _lt = 0;
    while (x) _t[++_lt] = x % 10, x /= 10;
    while (_lt > 0) wrch(_t[_lt] + '0'), --_lt;
  }
  inline void wr(char x) { wrch(x); }
  template <class T, class... A> void wr(T hd, A... rs) { wr(hd), wr(rs...); }
  template <class... A> void wrln(A... ar) { wr(ar...), wrch('\n'); }
  struct _Flusher {
    ~_Flusher() { flush(); }
  } _flusher;
} // namespace IO
using IO::rd;
using IO::wrln;

const int SZ = 2e6 + 500, ALP = 26;

struct PAM {
  /*
   * 0 号结点表示 0 结点
   * 1 号结点表示 -1 结点，长度为 -1
   * last 记录上一次插入的字符所在结点的编号
   * 一般来说，s[i]对应的结点恰好是i+1
   */
  int tot, last;
  int len[SZ], tr[SZ][ALP], fail[SZ], dep[SZ]; // fail:指向它的最长回文后缀
  int s[SZ], ls;                               // 字符串的内容 -'0'
  int newnode(int l) { return ++tot, len[tot] = l, tot; }
  void clear() {
    tot = -1, newnode(0), newnode(-1);
    fail[0] = 1, last = 0;
    s[ls = 0] = -1; //!!!!!! 减掉'a'后0就不是非匹配字符了，所以要整成-1
  }
  PAM() { clear(); }
  int getfail(int u) {
    while (s[ls - len[u] - 1] != s[ls]) u = fail[u];
    return u;
  }
  void insert(char c) {
    s[++ls] = (c -= 'a');
    int cur = getfail(last);
    if (!tr[cur][c]) { // 如果没有转移就添加
      int u = newnode(len[cur] + 2);
      fail[u] = tr[getfail(fail[cur])][c];
      tr[cur][c] = u;
      // 在此处更新 tot 的卫星信息
      dep[u] = dep[fail[u]] + 1;
    }
    last = tr[cur][c];
  }
} pam;

char s[SZ];
int d[SZ], nd[SZ]; // d[i]: 以 i,i+1 为中心的最长pal的半径

struct qxx {
  int nex, t;
} e[SZ];
int h[SZ], le;
void add_path(int f, int t) { e[++le] = {h[f], t}, h[f] = le; }

int totdfn, dfn[SZ];
void dfs(int u) {
  dfn[u] = ++totdfn;
  for (int i = h[u]; i; i = e[i].nex) {
    int v = e[i].t;
    dfs(v);
  }
}
pair<int, int> pr[SZ];
int n;
int c[SZ];
void add(int pos, int v) {
  for (int i = pos; i <= n; i += i & -i) c[i] += v;
}
int pre(int pos) {
  int res = 0;
  for (int i = pos; i > 0; i -= i & -i) res += c[i];
  return res;
}
long long ans;
int ps;
void dfs2(int u) {
  if (ps >= n) return;
  if (pam.len[u] && pam.len[u] % 2 == 0) add(pam.len[u] / 2, 1);
  while (pr[ps].first == dfn[u]) {
    int t = pre(pr[ps].second);
    // printf("在结点 u(len=%d) 上回答限度为 %d
    // 的问题\n",pam.len[u],pr[ps].second);
    ans += t;
    ++ps;
  }
  for (int i = h[u]; i; i = e[i].nex) {
    int v = e[i].t;
    dfs2(v);
  }
  if (pam.len[u] && pam.len[u] % 2 == 0) add(pam.len[u] / 2, -1);
}
int main() {
  rd(s + 1);
  n = strlen(s + 1);

  int L = 0, R = 0;
  FOR(i, 1, n) d[i] = 0;
  FOR(i, 1, n - 1) {
    if (i <= R) d[i] = min(R - i, d[L + R - i - 1]);
    while (0 < i - d[i] && i + d[i] + 1 <= n && s[i - d[i]] == s[i + d[i] + 1])
      ++d[i];
    if (i + d[i] > R) L = i - d[i] + 1, R = i + d[i];
  }
  // FOR(i,1,n)printf("%d%c",d[i]," \n"[i==n]);
  FOR(i, 1, n) {
    pam.insert(s[i]);
    nd[i] = pam.last;
  }
  FOR(i, 2, pam.tot) {
    add_path(pam.fail[i], i);
    // printf("add_path %d %d\n",pam.fail[i],i);
  }
  add_path(1, 0);
  dfs(1);
  FOR(i, 1, n - 1) { pr[i] = {dfn[nd[i]], d[i]}; }
  sort(pr + 1, pr + n);
  ps = 1;
  dfs2(1);
  printf("%lld\n", ans);
  return 0;
}
