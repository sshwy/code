// by Sshwy
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA
const int N = 1e5 + 5, P = 1e9 + 7;
int n;
int f[N], vis[N];
vector<int> g[N];

int pw(int a, int m, int p) {
  int res = 1;
  while (m) m & 1 ? res = res * 1ll * a % p : 0, a = 1ll * a * a % p, m >>= 1;
  return res;
}
void calc_f(int i) {
  if (vis[i]) return;
  if (i == 1) return f[i] = vis[i] = 1, void();
  if (!vis[i - 1]) calc_f(i - 1);
  FOR(k, 1, i - 1) {
    printf("inv %d: %d\n", i, pw(i, P - 2, P));
    f[i] = (f[i] + 1ll * f[k] * k % P * pw(i, P - 2, P)) % P;
  }
  f[i] = (f[i] + f[i - 1]) % P;
  vis[i] = 1;
  int t = f[i];
  FOR(j, 1, i) t = t * 1ll * j % P;
  printf("fac * f %d : %d\n", i, t);
}

int dep[N], a[N], v[N];
int ans;

void dfs(int u, int p) {
  dep[u] = dep[p] + 1;
  int m = dep[u];
  calc_f(m);
  v[m] = a[u];
  FOR(i, 1, m) {
    printf("%d\n", f[m - i + 1]);
    ans = (ans + f[m - i + 1] * 1ll * v[i]) % P;
  }
  for (int v : g[u])
    if (v != p) dfs(v, u);
}

int main() {
  scanf("%d", &n);
  FOR(i, 1, n - 1) {
    int u, v;
    scanf("%d%d", &u, &v);
    g[u].pb(v);
    g[v].pb(u);
  }
  FOR(i, 1, n) scanf("%d", &a[i]);
  dfs(1, 0);
  FOR(i, 1, n) ans = ans * 1ll * i % P;
  printf("%d\n", ans);
  return 0;
}
