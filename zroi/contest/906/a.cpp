// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

const int N = 2e5 + 5;

struct Field {
  int l, r;
  void read() { scanf("%d%d", &l, &r); }
  static bool byL(Field a, Field b) { return a.l < b.l; }
} a[N];

int n, b[N];

bool check(int k) {
  int pos = 0;
  FOR(i, 1, n) {
    int cur = max(a[i].l, pos);
    if (cur + k > a[i].r) return false;
    pos = cur + k;
    b[i] = cur;
  }
  return true;
}

struct Atom {
  int p, q, id, nex;
  bool operator<(const Atom b) const { return 1ll * p * b.q > 1ll * b.p * q; }
  bool greaterOrEqualThan1() { return p >= q; }
} d[N];

priority_queue<Atom> q;
vector<Atom> v;
bool vis[N];

void getSeparator(int k) {
  v.clear();
  assert(check(k));
  memset(vis, 0, sizeof(vis));
  FOR(i, 1, n - 1) {
    Atom u = {b[i + 1] - b[i] - k, 1, i, i + 1};
    q.push(u);
    d[i] = u;
  }
  // printf("k %d\n", k);

  v.pb({0, 1, 0, 0});
  while (!q.empty()) {
    Atom u = q.top();
    q.pop();
    if (vis[u.id]) continue;
    v.pb(u);
    if (u.nex == n) continue;
    Atom x = d[u.nex], y = {u.p + x.p, u.q + x.q, u.id, x.nex};
    vis[u.nex] = true;
    d[u.id] = y;
    q.push(y);
  }
  while (v.size() && v.back().greaterOrEqualThan1()) v.pop_back();
  // for(auto e : v) printf("%d/%d\n", e.p, e.q);
}

long long f[N], c[N];
bool check2(long long p, long long q) {
  long long pos = 0;
  FOR(i, 1, n) {
    long long cur = max(a[i].l * 1ll * q, pos);
    if (cur + p > a[i].r * 1ll * q) return false;
    pos = cur + p;
    f[i] = cur;
  }
  return true;
}

__int128 gcd(__int128 a, __int128 b) { return b ? gcd(b, a % b) : a; }

void _print(__int128 x) {
  if (!x) return;
  _print(x / 10);
  putchar(x % 10 + '0');
}
void print(__int128 x) {
  if (!x)
    putchar('0');
  else
    _print(x);
}

void getAns(long long p, long long q) {
  // printf("getAns %lld %lld\n", p, q);

  assert(check2(p, q));
  c[0] = 0;
  FOR(i, 1, n) {
    if (f[i] - f[i - 1] == p)
      c[i] = c[i - 1] + 1;
    else
      c[i] = 1;
  }
  long long xp = 1, xq = 0; // p/q => (p + xp/xq)/q = (p * xq + xp) / (q * xq)
  FOR(i, 1, n) {
    long long rest = 1ll * a[i].r * q - f[i] - p;
    // printf("i %d rest %lld c %lld\n", i, rest, c[i]);
    if (rest > q * c[i]) continue;
    if (__int128(xp) * c[i] > __int128(rest) * xq) { xp = rest, xq = c[i]; }
  }
  __int128 ap = __int128(p) * xq + xp, aq = __int128(q) * xq;
  __int128 g = gcd(ap, aq);
  ap /= g, aq /= g;
  print(ap), putchar('/'), print(aq), putchar('\n');
}

void work(int k) {
  getSeparator(k);

  int l = 0, r = v.size() - 1;
  while (l < r) {
    int mid = (l + r + 1) / 2;
    if (check2(v[mid].p + 1ll * v[mid].q * k, v[mid].q))
      l = mid;
    else
      r = mid - 1;
  }

  getAns(v[l].p + 1ll * v[l].q * k, v[l].q);
}

void go() {
  scanf("%d", &n);
  FOR(i, 1, n) a[i].read();
  sort(a + 1, a + n + 1, Field::byL);

  int l = 0, r = 1e9;
  while (l < r) {
    int mid = (l + r + 1) / 2;
    if (check(mid))
      l = mid;
    else
      r = mid - 1;
  }

  work(l);
}

int main() {
  int t;
  scanf("%d", &t);
  FOR(i, 1, t) go();
  return 0;
}
