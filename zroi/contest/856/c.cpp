// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

const int P = 1e9 + 7, K = 3e3 + 10;

int pw(int a, int m) {
  int res = 1;
  while (m) m & 1 ? res = 1ll * res * a % P : 0, a = 1ll * a * a % P, m >>= 1;
  return res;
}

int n, k, q;
int stir[K][K], Stir[K][K], d[K], fnv[K], pwq[K], pwq1[K];

void init(int lim) {
  stir[0][0] = 1;
  FOR(i, 1, lim) {
    FOR(j, 1, lim) {
      stir[i][j] = (stir[i - 1][j - 1] + 1ll * stir[i - 1][j] * (i - 1)) % P;
    }
  }

  Stir[0][0] = 1;
  FOR(i, 1, lim) {
    FOR(j, 1, lim) {
      Stir[i][j] = (Stir[i - 1][j - 1] + 1ll * Stir[i - 1][j] * j) % P;
    }
  }
}
int main() {
  scanf("%d%d%d", &n, &k, &q);

  init(k + 2);

  fnv[0] = 1;
  FOR(i, 1, k + 1) fnv[i] = 1ll * fnv[i - 1] * pw(i, P - 2) % P;

  FOR(i, 0, k + 1) pwq[i] = pw(q, i);
  FOR(i, 0, k + 1) pwq1[i] = pw((q + 1) % P, (n - i + P - 1) % (P - 1));

  FOR(i, 1, k + 1) d[i] = pw(i, k);
  FOR(i, 1, k) ROF(j, k + 1, i + 1) d[j] = (d[j] - d[j - 1] + P) % P;
  FOR(j, 0, k + 1) {
    int t = 0, coef = 1;
    FOR(i, j, k + 1) {
      int t2 = 1ll * coef * d[i] % P * fnv[i] % P * stir[i][j] % P;
      t = (t + t2) % P;
      coef = P - coef;
    }
    d[j] = t;
  }

  int ans = 0;

  FOR(j, 0, k + 1) {
    int n_t = 1;
    int s = 0;
    FOR(t, 0, j) {
      int tt = 1ll * Stir[j][t] * n_t % P * pwq[t] % P * pwq1[t] % P;
      s = (s + tt) % P;
      n_t = 1ll * n_t * (n - t) % P;
      if (n_t == 0) break;
    }
    s = 1ll * s * d[j] % P;
    ans = (ans + s) % P;
  }

  printf("%d\n", ans);

  return 0;
}
