// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

int n, P, I6;
int pw(int a, int m) {
  int res = 1;
  while (m) m & 1 ? res = 1ll * res * a % P : 0, a = 1ll * a * a % P, m >>= 1;
  return res;
}

const int SZ = 1e7 + 5;
int pn[SZ], lp, phi[SZ];
bool co[SZ];
void sieve(int lim) {
  co[0] = co[1] = true, phi[1] = 1;
  FOR(i, 2, lim) {
    if (!co[i]) pn[++lp] = i, phi[i] = i - 1;
    FOR(j, 1, lp) {
      if (pn[j] * i > lim) break;
      co[pn[j] * i] = true;
      if (i % pn[j] == 0) {
        phi[i * pn[j]] = phi[i] * pn[j];
        break;
      } else {
        phi[i * pn[j]] = phi[i] * (pn[j] - 1);
      }
    }
  }
}

int f(int i) {
  int res = 0;
  FOR(j, 1, i) res = (res + 1ll * j * j) % P;
  return res;
}

int main() {
  scanf("%d%d", &n, &P);

  sieve(n);
  int ans = 0;
  FOR(i, 1, n) { ans = (ans + 1ll * phi[i] * f(n / i)) % P; }
  printf("%d\n", ans);
  return 0;
}
