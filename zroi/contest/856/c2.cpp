// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

const int P = 1e9 + 7, K = 3e3 + 5;

int pw(int a, int m) {
  int res = 1;
  while (m) m & 1 ? res = 1ll * res * a % P : 0, a = 1ll * a * a % P, m >>= 1;
  return res;
}

int n, k, q;
int c[K][K];

int f(int n) {
  int res = 0;
  FOR(i, 1, n) res = (res + pw(i, k)) % P;
  return res;
}

int main() {
  scanf("%d%d%d", &n, &k, &q);

  c[0][0] = 1;
  FOR(i, 1, n) {
    c[i][0] = 1;
    FOR(j, 1, n) c[i][j] = (c[i - 1][j - 1] + c[i - 1][j]) % P;
  }

  int ans = 0;
  FOR(i, 0, n) {
    int t = 1ll * c[n][i] * pw(q, i) % P * f(i) % P;
    ans = (ans + t) % P;
  }
  printf("%d\n", ans);
  return 0;
}
