// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

using namespace RA;

const int N = 2e5 + 5;

int a[N];

int main() {
  srand(clock() + time(0));
  int n = rnd(1, 100), m = rnd(1, 5e8) * 2;
  n = 2e5;

  a[1] = 1 % m;
  FOR(i, 2, n) { a[i] = a[i - 1] * 2 % m; }

  printf("%d\n", n);
  FOR(i, 1, n) printf("%d%c", a[i], " \n"[i == n]);

  return 0;
}
