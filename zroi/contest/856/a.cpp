// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

int P, I6, I2;

int S1(int n) {
  if (P == 2) return ((n + 1) / 2) & 1;
  n %= P;
  return 1ll * n * (n + 1) % P * I2 % P;
}
int f(int i) {
  if (P == 2) return ((i + 1) / 2) & 1;
  if (P == 3) return (i / 3 * 2 % 3 + i % 3) % 3;
  i %= P;
  int res = 1ll * i * (i + 1) % P * (i * 2 + 1) % P * I6 % P;
  return res;
}

int n;

int pw(int a, int m) {
  int res = 1;
  while (m) m & 1 ? res = 1ll * res * a % P : 0, a = 1ll * a * a % P, m >>= 1;
  return res;
}

const int SZ = 1e7 + 5;
int pn[SZ], lp, phi[SZ], sphi[SZ];
bool co[SZ];
void sieve(int lim) {
  co[0] = co[1] = true, phi[1] = 1;
  FOR(i, 2, lim) {
    if (!co[i]) pn[++lp] = i, phi[i] = i - 1;
    FOR(j, 1, lp) {
      if (pn[j] * i > lim) break;
      co[pn[j] * i] = true;
      if (i % pn[j] == 0) {
        phi[i * pn[j]] = phi[i] * pn[j];
        break;
      } else {
        phi[i * pn[j]] = phi[i] * (pn[j] - 1);
      }
    }
  }
  FOR(i, 1, lim) sphi[i] = (sphi[i - 1] + phi[i]) % P;
}

int Sphi(int n) {
  if (n < SZ) return sphi[n];

  // printf("Sphi %d\n", n);

  int res = S1(n);

  for (int l = 2, r; l <= n && (r = n / (n / l)); l = r + 1) {
    int t = 1ll * (r - l + 1) * Sphi(n / l) % P;
    res = (res - t + P) % P;
  }

  return res;
}

int main() {
  scanf("%d%d", &n, &P);

  I2 = pw(2, P - 2);
  I6 = pw(6, P - 2);

  sieve(SZ - 1);

  int ans = 0;

  for (int l = 1, r; l <= n && (r = n / (n / l)); l = r + 1) {
    int t = 1ll * (Sphi(r) - Sphi(l - 1) + P) % P * f(n / l) % P;
    ans = (ans + t) % P;
  }

  printf("%d\n", ans);
  return 0;
}
