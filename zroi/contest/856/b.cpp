// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

typedef long long LL;
const int N = 2e5 + 5;

inline LL mul(LL a, LL b, LL p) {
  if (p <= 1000000000ll) return 1ll * a * b % p;
  if (p <= 1000000000000ll)
    return (((a * (b >> 20) % p) << 20) % p + a * (b & ((1 << 20) - 1))) % p;
  LL d = floor(a * (long double)b / p);
  LL res = (a * b - d * p) % p;
  if (res < 0) res += p;
  return res;
}
inline LL pw(LL a, LL b, LL mod) {
  LL res = 1;
  for (; b; b >>= 1, a = mul(a, a, mod))
    if (b & 1) res = mul(res, a, mod);
  return res;
}

int n;
LL a[N];
vector<LL> v;

__int128 sum;

bool check(LL m) {
  // printf("check %lld\n", m);
  LL sum_mod_m = sum % m;
  LL s2 = pw(2, n, m) - 1;
  // printf("%lld %lld\n", sum_mod_m, s2);
  if (s2 < 0) s2 += m;
  return s2 == sum_mod_m;
}
LL b[N];
bool ok(LL m) {
  int lb = 0;
  b[++lb] = 1;
  FOR(i, 2, n) b[i] = mul(b[i - 1], 2, m);
  sort(b + 1, b + n + 1);
  FOR(i, 1, n) if (a[i] != b[i]) return false;
  return true;
}
int main() {
  scanf("%d", &n);
  FOR(i, 1, n) scanf("%lld", a + i), sum += a[i];

  sort(a + 1, a + n + 1);

  if (a[n] == (1ll << (n - 1))) {
    printf("%lld\n", a[n] + 1);
    return 0;
  }

  LL minM = *max_element(a + 1, a + n + 1) + 1;
  LL maxM = 1;
  while (maxM < minM) maxM *= 2;

  if (minM == maxM) {
    printf("%lld\n", minM);
    return 0;
  }

  // fprintf(stderr, "minM %lld maxM %lld\n", minM, maxM);

  assert(maxM > 1);

  LL x = maxM / 2;
  assert(x < minM);
  FOR(i, 1, n) {
    if (i > 1 && a[i] == a[i - 1]) continue;
    if (a[i] < x) {
      LL m = maxM - a[i];
      if (m >= minM && check(m)) v.pb(m);
    }
  }

  if (v.size() == 1) {
    printf("%lld\n", v[0]);
    return 0;
  }

  sort(v.begin(), v.end());
  for (auto x : v) {
    // printf("x %lld\n", x);
    if (ok(x)) {
      printf("%lld\n", x);
      return 0;
    }
  }

  return 0;
}
