// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

using namespace RA;

int pn[] = {2, 3, 5, 7, int(1e9 + 7), 998244353};
int main() {
  srand(clock() + time(0));
  int n = rnd(1, 1000), p = pn[rnd(6)];
  printf("%d %d\n", n, p);
  return 0;
}
