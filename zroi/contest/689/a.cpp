// by Sshwy
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

const int N = 2e5 + 5, SZ = N << 2;
int n, m;
pair<int, int> a[N];
int c[N], tag[SZ];
long long w1[N];
typedef __int128 i128;
i128 w2[N], ans;

bool o[SZ];
long long s1[SZ]; // 次数
i128 s2[SZ];

void pushup(int u) {
  s1[u] = s1[u << 1] + s1[u << 1 | 1];
  s2[u] = s2[u << 1] + s2[u << 1 | 1];
}
void zero_fill(int u) { // 覆盖为0
  o[u] = 1;
  tag[u] = 0;
  s1[u] = s2[u] = 0;
}
void inc(int u, int l, int r, int v) {
  s1[u] += (w1[r] - w1[l - 1]) * v; // 增加总的购买次数
  s2[u] += (w2[r] - w2[l - 1]) * v; // 总的购买价格
  tag[u]++;
}
void pushdown(int u, int l, int r) {
  if (o[u]) {
    zero_fill(u << 1);
    zero_fill(u << 1 | 1);
    o[u] = 0;
  }
  if (tag[u]) {
    int mid = (l + r) >> 1;
    inc(u << 1, l, mid, tag[u]);
    inc(u << 1 | 1, mid + 1, r, tag[u]);
    tag[u] = 0;
  }
}
void weighted_inc() { inc(1, 1, m, 1); }
pair<int, int> lower_bound(int ci, int u = 1, int l = 1, int r = m) { // 次数在ci内
  assert(s1[u] >= ci);
  if (l == r) { return make_pair(l, 0); }
  int mid = (l + r) >> 1;
  pushdown(u, l, r);
  if (s1[u << 1] < ci) {
    auto res = lower_bound(ci - s1[u << 1], u << 1 | 1, mid + 1, r);
    res.second += s1[u << 1];
    return res;
  } else {
    return lower_bound(ci, u << 1, l, mid);
  }
}
i128 zero_fill(int L, int R, int u = 1, int l = 1, int r = m) {
  if (R < l || r < L || L > R) return 0;
  if (L <= l && r <= R) {
    i128 res = s2[u];
    zero_fill(u);
    return res;
  }
  int mid = (l + r) >> 1;
  pushdown(u, l, r);
  i128 res =
      zero_fill(L, R, u << 1, l, mid) + zero_fill(L, R, u << 1 | 1, mid + 1, r);
  pushup(u);
  return res;
}
i128 sub(int pos, int v, int u = 1, int l = 1, int r = m) {
  if (v == 0) return 0;
  if (l == r) {
    i128 res = (i128)v * a[l].first;
    assert(s1[u] >= v);
    s1[u] -= v;
    s2[u] -= res;
    return res;
  }
  int mid = (l + r) >> 1;
  pushdown(u, l, r);
  i128 res;
  if (pos <= mid)
    res = sub(pos, v, u << 1, l, mid);
  else
    res = sub(pos, v, u << 1 | 1, mid + 1, r);
  pushup(u);
  return res;
}

int main() {
  scanf("%d%d", &n, &m);
  FOR(i, 1, n) { scanf("%d", &c[i]); }
  FOR(i, 1, m) { scanf("%d%d", &a[i].first, &a[i].second); }
  sort(a + 1, a + m + 1);
  FOR(i, 1, m) if (a[i].second == -1) a[i].second = 1e9;
  FOR(i, 1, m) w1[i] = a[i].second, w2[i] = (i128)a[i].first * a[i].second;
  FOR(i, 1, m) w1[i] += w1[i - 1], w2[i] += w2[i - 1];

  FOR(i, 1, n) {
    // printf("i %d\n",i);
    weighted_inc();              // 增加一次使用机会
    auto pr = lower_bound(c[i]); // (method id,cnt)
    // printf("pr %d %d\n",pr.first,pr.second);
    ans += zero_fill(1, pr.first - 1); // 返回钱数
    ans += sub(pr.first, c[i] - pr.second);
  }
  char cans[100];
  int ls = 0;
  while (ans) cans[ls++] = '0' + ans % 10, ans /= 10;
  cans[ls] = 0;
  reverse(cans, cans + ls);
  printf("%s\n", cans);
  return 0;
}
