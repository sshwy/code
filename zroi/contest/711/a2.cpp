// by Sshwy
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

const int N = 1e6 + 5;
int n;
int c[N];
int f[N], cnt[N], ans;

int main() {
  scanf("%d", &n);
  FOR(i, 1, n) scanf("%d", c + i);
  FOR(i, 1, n) scanf("%d", f + i);
  FOR(l, 1, n) FOR(r, l, n) {
    fill(cnt, cnt + n + 1, 0);
    FOR(i, l, r) cnt[c[i]]++;
    int flag = 1;
    FOR(i, l, r) if (cnt[c[i]] < f[r - l + 1]) flag = 0;
    if (flag) ans = max(ans, r - l + 1);
  }
  printf("%d\n", ans);
  return 0;
}
