// by Sshwy
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

using namespace RA;

const int N = 1e6 + 5;
int c[N], f[N];
int main() {
  srand(clock() + time(0));
  int n = rnd(1, 20);
  // int n = 1e5;
  FOR(i, 1, n) c[i] = rnd(0, n);
  FOR(i, 1, n) f[i] = rnd(2, n + 1);
  sort(f + 1, f + n + 1);
  reverse(f + 1, f + n + 1);
  FOR(i, 1, n) f[i] = f[n];
  printf("%d\n", n);
  FOR(i, 1, n) printf("%d%c", c[i], " \n"[i == n]);
  FOR(i, 1, n) printf("%d%c", f[i], " \n"[i == n]);
  return 0;
}
