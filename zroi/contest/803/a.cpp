// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

const int N = 1e6 + 5;
struct point {
  int x, y;
  point() { x = y = 0; }
  point(int X, int Y) { x = X, y = Y; }
  void read() { scanf("%d%d", &x, &y); }
  bool operator<(const point p) const { return x == p.x ? y < p.y : x < p.x; }
  bool operator==(const point p) const { return x == p.x && y == p.y; }
};
point vec(point from, point to) { return point(to.x - from.x, to.y - from.y); }
long long det(point p1, point p2) { return 1ll * p1.x * p2.y - 1ll * p2.x * p1.y; }

bool vis[N];
struct convex {
  int n;
  point a[N];
  int up[N], dn[N], lu, ld;
  convex() { n = 0; }
  void read() {
    scanf("%d", &n);
    FOR(i, 1, n) a[i].read();
  }
  void add(point p) { a[++n] = p; }
  void build() {
    sort(a + 1, a + n + 1);
    fill(vis, vis + n + 1, false);
    lu = ld = 0;
    up[++lu] = 1;
    FOR(i, 2, n) {
      while ((lu >= 2 &&
                 det(vec(a[up[lu - 1]], a[up[lu]]), vec(a[up[lu]], a[i])) >= 0) ||
             (lu && a[up[lu]] == a[i]))
        --lu;
      up[++lu] = i;
    }
    dn[++ld] = 1;
    FOR(i, 2, n) {
      while ((ld >= 2 &&
                 det(vec(a[dn[ld - 1]], a[dn[ld]]), vec(a[dn[ld]], a[i])) <= 0) ||
             (ld && a[dn[ld]] == a[i]))
        --ld;
      dn[++ld] = i;
    }
  }
  point upAt(int i) { return a[up[i]]; }
  point dnAt(int i) { return a[dn[i]]; }
  bool underAt(int i, point p) { // up
    assert(a[up[i]].x <= p.x);
    if (i == lu)
      return a[up[i]].x == p.x && a[up[i]].y >= p.y;
    else {
      assert(a[up[i + 1]].x > p.x);
      return det(vec(a[up[i]], a[up[i + 1]]), vec(a[up[i]], p)) <= 0;
    }
  }
  bool aboveAt(int i, point p) { // dn
    assert(a[dn[i]].x <= p.x);
    if (i == ld)
      return a[dn[i]].x == p.x && a[dn[i]].y >= p.y;
    else {
      assert(a[dn[i + 1]].x > p.x);
      return det(vec(a[dn[i]], a[dn[i + 1]]), vec(a[dn[i]], p)) >= 0;
    }
  }
} c[4], cc;

void addcc(point x, point y, point z) {
  int cx = x.x + y.x + z.x;
  int cy = x.y + y.y + z.y;
  cc.add(point(cx, cy));
}
void addUp(int *cur) {
  addcc(c[1].upAt(cur[1]), c[2].upAt(cur[2]), c[3].upAt(cur[3]));
}
void addDn(int *cur) {
  addcc(c[1].dnAt(cur[1]), c[2].dnAt(cur[2]), c[3].dnAt(cur[3]));
}
bool doneUp(int *cur) {
  FOR(i, 1, 3) if (cur[i] < c[i].lu) return false;
  return true;
}
bool doneDn(int *cur) {
  FOR(i, 1, 3) if (cur[i] < c[i].ld) return false;
  return true;
}
void getUpNex(int *cur) {
  int ps = -1;
  point pvec;
  FOR(i, 1, 3) if (cur[i] < c[i].lu) {
    point ivec = vec(c[i].upAt(cur[i]), c[i].upAt(cur[i] + 1));
    if (ps == -1 || det(pvec, ivec) > 0) ps = i, pvec = ivec;
  }
  cur[ps]++;
}
void getDnNex(int *cur) {
  int ps = -1;
  point pvec;
  FOR(i, 1, 3) if (cur[i] < c[i].ld) {
    point ivec = vec(c[i].dnAt(cur[i]), c[i].dnAt(cur[i] + 1));
    if (ps == -1 || det(pvec, ivec) < 0) ps = i, pvec = ivec;
  }
  cur[ps]++;
}
void workUp() {
  int cur[4];
  FOR(i, 1, 3) cur[i] = 1;
  addUp(cur);
  while (!doneUp(cur)) {
    getUpNex(cur);
    addUp(cur);
  }
}
void workDown() {
  int cur[4];
  FOR(i, 1, 3) cur[i] = 1;
  addDn(cur);
  while (!doneDn(cur)) {
    getDnNex(cur);
    addDn(cur);
  }
}

int m;
bool ans[N];

struct qry {
  int x, y, id;
  void read(int Id) {
    scanf("%d%d", &x, &y);
    x *= 3;
    y *= 3;
    id = Id;
  }
} a[N];
bool cmp(qry x, qry y) { return x.x < y.x; }
void checkUp() {
  int cur = 1;
  FOR(j, 1, m) {
    int x = a[j].x, y = a[j].y, id = a[j].id;
    while (cur < cc.lu && cc.upAt(cur + 1).x <= x) ++cur;
    if (cc.underAt(cur, point(x, y)) == false) ans[id] = false;
  }
}
void checkDn() {
  int cur = 1;
  FOR(j, 1, m) {
    int x = a[j].x, y = a[j].y, id = a[j].id;
    while (cur < cc.ld && cc.dnAt(cur + 1).x <= x) ++cur;
    if (cc.aboveAt(cur, point(x, y)) == false) ans[id] = false;
  }
}
void getAns() {
  checkUp();
  checkDn();
}
int main() {
  FOR(i, 1, 3) c[i].read();
  FOR(i, 1, 3) c[i].build();
  workUp();
  workDown();
  scanf("%d", &m);
  FOR(i, 1, m) a[i].read(i);
  sort(a + 1, a + m + 1, cmp);
  FOR(i, 1, m) ans[i] = true;
  cc.build();
  getAns();
  FOR(i, 1, m) puts(ans[i] ? "YES" : "NO");
  return 0;
}

/*
 * 数据类型的问题
 * up 和 down 有没有搞混
 * > 还是 >=
 * 边界判断
 */
