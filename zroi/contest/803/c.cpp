#include <bits/stdc++.h>
using namespace std;
typedef pair<int, int> pii;
#define X first
#define Y second
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
const int N = 2e5 + 5;
using namespace std;

typedef long long LL;
namespace math {
  inline int pw(int a, int b, int mod) {
    int res = 1;
    for (; b; b >>= 1, a = 1ll * a * a % mod)
      if (b & 1) res = 1ll * res * a % mod;
    return res;
  }
  inline LL mul(LL a, LL b, LL p) {
    if (p <= 1000000000ll) return 1ll * a * b % p;
    if (p <= 1000000000000ll)
      return (((a * (b >> 20) % p) << 20) % p + a * (b & ((1 << 20) - 1))) % p;
    LL d = floor(a * (long double)b / p);
    LL res = (a * b - d * p) % p;
    if (res < 0) res += p;
    return res;
  }
  inline LL pw(LL a, LL b, LL mod) {
    LL res = 1;
    for (; b; b >>= 1, a = mul(a, a, mod))
      if (b & 1) res = mul(res, a, mod);
    return res;
  }
  inline bool check(LL a, LL x, LL times, LL n) {
    LL tmp = pw(a, x, n);
    while (times--) {
      LL last = mul(tmp, tmp, n);
      if (last == 1 && tmp != 1 && tmp != n - 1) return 0;
      tmp = last;
    }
    return tmp == 1;
  }
  int base[] = {2, 3, 5, 7, 11, 13, 17, 19, 23};
  const int S = 8;
  inline bool Miller(LL n) {
    FOR(i, 0, S) {
      if (n == base[i]) return 1;
      if (n % base[i] == 0) return 0;
    }
    LL x = n - 1, times = 0;
    while (!(x & 1)) times++, x >>= 1;
    FOR(_, 0, S) if (!check(base[_], x, times, n)) return 0;
    return 1;
  }
#define mytz __builtin_ctzll
  inline LL gcd(LL a, LL b) {
    if (!a) return b;
    if (!b) return a;
    register int t = mytz(a | b);
    a >>= mytz(a);
    do {
      b >>= mytz(b);
      if (a > b) {
        LL t = b;
        b = a, a = t;
      }
      b -= a;
    } while (b);
    return a << t;
  }
#define F(x) ((mul(x, x, n) + c) % n)
  inline LL rho(LL n, LL c) {
    LL x = 1ll * rand() * rand() % n, y = F(x);
    while (x ^ y) {
      LL w = gcd(abs(x - y), n);
      if (w > 1 && w < n) return w;
      x = F(x), y = F(y), y = F(y);
    }
    return 1;
  }
#undef F
  inline LL calc(LL x) {
    if (Miller(x)) return x;
    LL fsf = 0; // while((fsf=rho(x,rand()%x))==1);
    while ((fsf = rho(x, 2)) == 1)
      ;
    return max(calc(fsf), calc(x / fsf));
  }
  vector<LL> factorize(LL x) {
    if (Miller(x)) return vector<LL>(1, x);
    LL fsf = 0; // while((fsf=rho(x,rand()%x))==1);
    while ((fsf = rho(x, 2)) == 1)
      ;
    vector<LL> A = factorize(fsf);
    vector<LL> B = factorize(x / fsf);
    A.insert(A.end(), B.begin(), B.end());
    return A;
  }
} // namespace math

int n;
long long mind = 2e18, ttt;
pii minv;

pii p[N];
long long dis(pii a, pii b) {
  return (a.X - b.X) * 1ll * (a.X - b.X) + (a.Y - b.Y) * 1ll * (a.Y - b.Y);
}
pii vec(pii from, pii to) { return make_pair(to.X - from.X, to.Y - from.Y); }
bool cmp(pii a, pii b) { return a.Y == b.Y ? a.X < b.X : a.Y < b.Y; }

pii t[N];
void solve(int l, int r) {
  if (l == r) return;
  if (l + 1 == r) {
    if (ttt = dis(p[l], p[r]), ttt < mind) mind = ttt, minv = vec(p[l], p[r]);
    return;
  }
  int mid = (l + r) >> 1;
  sort(p + l, p + r + 1);
  solve(l, mid), solve(mid + 1, r);
  int ml = p[mid].X, lt = 0;
  FOR(i, l, r) if (ttt = abs(ml - p[i].X), ttt < mind) t[++lt] = p[i];
  sort(t + 1, t + lt + 1, cmp);
  int L = 1;
  FOR(i, 1, lt) {
    FOR(j, L, i - 1) {
      if (ttt = dis(t[j], t[i]), ttt < mind) mind = ttt, minv = vec(t[j], t[i]);
    }
    while (abs(t[i].Y - t[L].Y) > mind) ++L;
  }
}
int main() {
  scanf("%d", &n);
  FOR(i, 1, n) scanf("%d%d", &p[i].X, &p[i].Y);
  sort(p + 1, p + n + 1);
  n = unique(p + 1, p + n + 1) - p - 1;
  solve(1, n);

  vector<LL> v = math::factorize(mind);
  sort(v.begin(), v.end());
  int cur = -1, cnt = 0;
  for (auto x : v) {
    if (x == cur) {
      ++cnt;
    } else {
    }
  }
  return 0;
}
