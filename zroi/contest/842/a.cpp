// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

const int N = 5e3 + 5, M = 1e4 + 5, INP = -1044266559;

int n, m, a[N], b[N], tot, L[N], R[N], nd[N];
int f[N][M];
vector<int> g[N];

void dfs(int u) {
  L[u] = ++tot;
  nd[tot] = u;
  for (auto v : g[u]) dfs(v);
  R[u] = tot;
}
void getMax(int &x, int y) { x = max(x, y); }

int main() {
  memset(f, -0x3f, sizeof(f));
  scanf("%d%d", &n, &m);
  FOR(i, 1, n) {
    int fa;
    scanf("%d%d%d", a + i, &fa, b + i);
    if (i > 1) g[fa].pb(i);
  }
  dfs(1);
  // FOR(i, 1, n) printf("i %d L %d R %d\n", i, L[i], R[i]);

  FOR(i, 0, m) f[1][i] = 0;

  FOR(i, 1, n) {
    FOR(j, 0, m) if (f[i][j] > INP) {
      if (a[nd[i]] <= j) { getMax(f[i + 1][j - a[nd[i]]], f[i][j] + b[nd[i]]); }
      getMax(f[R[nd[i]] + 1][j], f[i][j]);
    }
  }
  printf("%d\n", f[n + 1][0]);
  return 0;
}
