// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

const int N = 25, M = 1e4 + 5, INP = -1044266559;

int n, m, a[N], b[N], fa[N];
int f[N][M];
vector<int> g[N];

void getMax(int &x, int y) { x = max(x, y); }
int ans = 0;

int main() {
  memset(f, -0x3f, sizeof(f));
  scanf("%d%d", &n, &m);
  FOR(i, 1, n) { scanf("%d%d%d", a + i, fa + i, b + i); }
  int nn = 1 << n;
  FOR(stat, 0, nn - 1) {
    bool flag = false;
    FOR(i, 1, n) if ((stat >> (i - 1) & 1) && !(stat >> (fa[i] - 1) & 1)) {
      flag = true;
      break;
    }
    if (flag == false) {
      int sa = 0, sb = 0;
      FOR(i, 1, n) if (stat >> (i - 1) & 1) sa += a[i], sb += b[i];
      if (sa <= m) ans = max(ans, sb);
    }
  }

  printf("%d\n", ans);
  return 0;
}
