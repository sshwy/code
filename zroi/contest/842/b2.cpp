// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

const int N = 2e5 + 5, Q = 2e5 + 5, P = 1e9 + 7;

int n, q, fa[N], ch[N][2];
long long a[N], b[N];

int get(int u) { return ch[fa[u]][1] == u; }
void rotate(int u) {
  int p = fa[u], pp = fa[p], gu = get(u);
  if (pp) ch[pp][get(p)] = u;
  fa[u] = pp;
  if (ch[u][!gu]) fa[ch[u][!gu]] = p;
  ch[p][gu] = ch[u][!gu];
  ch[u][!gu] = p, fa[p] = u;
  assert(!ch[0][0] && !ch[0][1] && !fa[0]);
}
void dfs(int u) {
  if (!u) return;
  dfs(ch[u][0]);
  dfs(ch[u][1]);
  b[u] = (b[ch[u][0]] + b[ch[u][1]] + a[u]) % P;
}
long long dfs2(int u) {
  long long res = b[u];
  if (ch[u][0]) res = res * dfs2(ch[u][0]) % P;
  if (ch[u][1]) res = res * dfs2(ch[u][1]) % P;
  return res;
}
long long calc(int x) {
  dfs(x);
  return dfs2(x);
}

int main() {
  scanf("%d%d", &n, &q);
  FOR(i, 1, n) {
    scanf("%lld%d%d", a + i, ch[i], ch[i] + 1);
    if (ch[i][0]) fa[ch[i][0]] = i;
    if (ch[i][1]) fa[ch[i][1]] = i;
  }
  FOR(i, 1, q) {
    int op, x;
    scanf("%d%d", &op, &x);
    if (op == 0) {
      if (ch[x][0]) rotate(ch[x][0]);
    } else if (op == 1) {
      if (ch[x][1]) rotate(ch[x][1]);
    } else {
      printf("%lld\n", calc(x));
    }
  }
  return 0;
}
