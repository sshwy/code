// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

using namespace RA;
const int N = 5005, P = 1e4 + 5;

int main() {
  srand(clock() + time(0));
  int n = rnd(1, 10), m = rnd(1, 100);
  printf("%d %d\n", n, m);
  FOR(i, 1, n) {
    int w = rnd(1, 10), fa = rnd(1, max(1, i - 1)), v = rnd(1, 100);
    printf("%d %d %d\n", w, fa, v);
  }
  return 0;
}
