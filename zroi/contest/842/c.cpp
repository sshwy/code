// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

const int K = 55, N = 805, G = N / 10, P = 1e4 + 7, R = 1605;
int pw(int a, int m) {
  int res = 1;
  while (m) m & 1 ? res = 1ll * res * a % P : 0, a = 1ll * a * a % P, m >>= 1;
  return res;
}
void add(int &x, int y) { x = (x + y) % P; }

int Tx, Ty, Mx, My, r, k;
vector<int> v;
int g[G][G], fac[P], fnv[P];

int binom(int a, int b) {
  if (a < b || a < 0 || b < 0) return 0;
  if (a >= P || b >= P) return binom(a % P, b % P) * 1ll * binom(a / P, b / P) % P;
  return fac[a] * 1ll * fnv[b] % P * fnv[a - b] % P;
}

void calcG() {
  sort(v.begin(), v.end());
  v.erase(unique(v.begin(), v.end()), v.end());
  g[0][0] = 1;
  FOR(j, 1, G - 1) {
    FOR(i, j, G - 1) {
      for (auto e : v) {
        if (i >= e) add(g[i][j], g[i - e][j - 1]);
      }
    }
  }
}

int _F[2][N][R];
bool _vF[2][N][R];
int F(int n, int m, int M, int tag) { // m 个变量 < M，和为 n
  if (_vF[tag][n][m]) return _F[tag][n][m];
  int lim = min(n / M, m), s = 0;
  FOR(i, 0, lim) {
    long long coef = i % 2 ? P - 1 : 1;
    add(s, coef * binom(m + n - i * M - 1, m - 1) % P * binom(m, i) % P);
  }
  _vF[tag][n][m] = true, _F[tag][n][m] = s;
  return s;
}

int Fx(int n, int m) { return F(n, m, Mx + 1, 0); }
int Fy(int n, int m) { return F(n, m, My + 1, 1); }

int main() {
  scanf("%d%d%d%d%d%d", &Tx, &Ty, &Mx, &My, &r, &k);
  FOR(i, 1, k) {
    int ki;
    scanf("%d", &ki);
    v.pb(ki / 10);
  }
  calcG();

  fac[0] = 1;
  FOR(i, 1, P - 1) fac[i] = fac[i - 1] * 1ll * i % P;
  fnv[P - 1] = pw(fac[P - 1], P - 2);
  ROF(i, P - 1, 1) fnv[i - 1] = fnv[i] * 1ll * i % P;

  int limi = min(Tx / 10, Ty / 10);
  int ans = 0;
  FOR(i0, 0, r) {
    long long c0 = i0 % 2 ? P - 1 : 1;
    int s = 0;
    FOR(i, 0, limi) {
      long long coef = i % 2 ? P - 1 : 1;
      FOR(j, i, limi) {
        add(s, coef * Fx(Tx - 10 * j, r - i - i0) % P * Fy(Ty - 10 * j, r - i - i0) %
                   P * binom(r - i0, i) % P * g[j][i] % P);
      }
    }
    add(ans, c0 * s % P * binom(r, i0) % P);
  }

  printf("%d\n", ans);

  return 0;
}
