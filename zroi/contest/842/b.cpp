// by Yao
#include <bits/stdc++.h>
using namespace std;
#define FOR(i, a, b) for (LL i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (LL i = (a); i >= (b); --i)

typedef long long LL;
const LL N = 2e5 + 5, Q = 2e5 + 5, P = 1e9 + 7;

LL pw(LL a, LL m) {
  LL res = 1;
  while (m) m & 1 ? res = res * a % P : 0, a = a * 1ll * a % P, m >>= 1;
  return res;
}
LL n, q, fa[N], ch[N][2], a[N], b[N], val[N];

void add(LL p, LL v) {
  for (LL i = p; i < N; i += i & -i) a[i] = (a[i] + v) % P;
}
LL getPre(LL p) {
  LL res = 0;
  for (LL i = p; i > 0; i -= i & -i) res = (res + a[i]) % P;
  return res;
}
LL getSum(LL l, LL r) { return (getPre(r) - getPre(l - 1) + P) % P; }
void mul(LL p, LL v) {
  for (LL i = p; i < N; i += i & -i) b[i] = b[i] * v % P;
}
LL getProd(LL p) {
  LL res = 1;
  for (LL i = p; i > 0; i -= i & -i) res = res * b[i] % P;
  return res;
}
LL getVal(LL l, LL r) { return getProd(r) * pw(getProd(l - 1), P - 2) % P; }
void setVal(LL p, LL v) { mul(p, pw(getVal(p, p), P - 2) * v % P); }

LL tot, pos[N], L[N], R[N];

void pushup(LL u) {
  if (!u) return;
  L[u] = R[u] = pos[u];
  if (ch[u][0]) L[u] = L[ch[u][0]];
  if (ch[u][1]) R[u] = R[ch[u][1]];
  setVal(pos[u], getSum(L[u], R[u]));
}
void dfs(LL u) {
  if (ch[u][0]) dfs(ch[u][0]);
  pos[u] = ++tot;
  add(pos[u], val[u]);
  if (ch[u][1]) dfs(ch[u][1]);
  pushup(u);
}

LL get(LL u) { return ch[fa[u]][1] == u; }
void rotate(LL u) {
  LL p = fa[u], pp = fa[p], gu = get(u);
  if (pp) ch[pp][get(p)] = u;
  fa[u] = pp;
  if (ch[u][!gu]) fa[ch[u][!gu]] = p;
  ch[p][gu] = ch[u][!gu];
  ch[u][!gu] = p, fa[p] = u;
  assert(!ch[0][0] && !ch[0][1] && !fa[0]);
  pushup(p), pushup(u);
}

signed main() {
  FOR(i, 0, N - 1) b[i] = 1;
  scanf("%lld%lld", &n, &q);
  FOR(i, 1, n) {
    scanf("%lld%lld%lld", val + i, ch[i], ch[i] + 1);
    if (ch[i][0]) fa[ch[i][0]] = i;
    if (ch[i][1]) fa[ch[i][1]] = i;
  }
  dfs(1);
  FOR(i, 1, q) {
    LL op, x;
    scanf("%lld%lld", &op, &x);
    if (op == 0) {
      if (ch[x][0]) rotate(ch[x][0]);
    } else if (op == 1) {
      if (ch[x][1]) rotate(ch[x][1]);
    } else {
      printf("%lld\n", getVal(L[x], R[x]));
    }
  }
  return 0;
}
