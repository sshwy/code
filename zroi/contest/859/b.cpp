// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA
const int P = 1e9 + 9, K = 520, I2 = (P + 1) / 2, I6 = (P + 1) / 6;

int pw(int a, long long m, int p = P) {
  int res = 1;
  while (m) m & 1 ? res = 1ll * res * a % p : 0, a = 1ll * a * a % p, m >>= 1;
  return res;
}

template <int R> class Sqrt {
  int a, b; // a+b\sqrt{R}
public:
  int hash() { return (a + 5ll * b) % P; }
  Sqrt(long long _a = 0, long long _b = 0) { a = _a % P, b = _b % P; }
  Sqrt operator+(Sqrt x) { return Sqrt((a + x.a) % P, (b + x.b) % P); }
  Sqrt operator-(Sqrt x) { return Sqrt((a - x.a + P) % P, (b - x.b + P) % P); }
  friend Sqrt operator-(int x, Sqrt y) { return Sqrt<R>(x, 0) - y; }
  Sqrt operator*(Sqrt x) {
    return Sqrt((a * 1ll * x.a % P + b * 1ll * x.b % P * R) % P,
        (a * 1ll * x.b % P + x.a * 1ll * b) % P);
  }
  Sqrt operator*(int x) { return Sqrt(a * 1ll * x % P, x * 1ll * b % P); }
  Sqrt operator*(long long x) {
    return x %= P, Sqrt(a * 1ll * x % P, x * 1ll * b % P);
  }
  Sqrt inv() {
    int x = pw((a * 1ll * a % P - R * 1ll * b % P * b % P + P) % P, P - 2);
    return Sqrt(a * 1ll * x % P, (P - b) % P * 1ll * x % P);
  }
  Sqrt operator/(Sqrt x) { return *this * x.inv(); }
  Sqrt operator/(int x) { return *this * pw(x, P - 2); }

  Sqrt &operator+=(Sqrt x) { return *this = *this + x, *this; }
  Sqrt &operator-=(Sqrt x) { return *this = *this - x, *this; }
  Sqrt &operator*=(Sqrt x) { return *this = *this * x, *this; }
  Sqrt &operator/=(Sqrt x) { return *this = *this / x, *this; }

  bool operator==(Sqrt x) { return a == x.a && b == x.b; }
  operator int() {
    assert(!b);
    return a;
  }
  void print() { printf("(%d,%d)", a, b); }
};

template <int R> Sqrt<R> pw(Sqrt<R> a, long long m) {
  Sqrt<R> res(1, 0);
  while (m) m & 1 ? res = res * a, 0 : 0, a = a * a, m >>= 1;
  return res;
}

typedef Sqrt<5> Num;

const int SZ = 3e6 + 7;

struct hash_map {
  struct data {
    Num u;
    int v, nex;
  };
  data e[SZ];
  int h[SZ], le;
  int &operator[](Num u) {
    int hu = u.hash();
    for (int i = h[hu]; i; i = e[i].nex)
      if (e[i].u == u) return e[i].v;
    return e[++le] = (data){u, -1, h[hu]}, h[hu] = le, e[le].v;
  }
  void clear() { memset(h, 0, sizeof(h)), le = 0; }
} h;

Num c1 = Num(1, 1) / 2;
Num c2 = Num(1, P - 1) / 2;
Num Isqrt5 = Num(1, 0) / Num(0, 1);

typedef pair<Num, Num> Fib;

Num F(Fib a) { return Isqrt5 * (a.first - a.second); }
int bsgs(Fib b) {
  int t = sqrt(P) + 0.5;
  h.clear();

  Fib cur = b;
  Fib q(1, 1);

  FOR(i, 0, t) {
    h[F(cur)] = i;
    cur.first *= c1;
    cur.second *= c2;
  }
  cur = make_pair(pw(c1, t), pw(c2, t));
  FOR(i, 0, t) {
    if (h[F(q)] != -1 && i * t >= h[F(q)]) return i * t - h[F(q)];
    q.first *= cur.first;
    q.second *= cur.second;
  }
  return -1;
}

int main() {
  int k;
  scanf("%d", &k);
  int a = 1, b = 1;
  int lim = 5e6;
  FOR(i, 1, lim) {
    if (a == k) return printf("%d\n", i), 0;
    a = (a + b) % P;
    swap(a, b);
  }
  puts("-1");
  return 0;
}
