// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (long long i = (a); i <= (b); ++i)

const long long P = 1998585857, N = 5e3 + 5;

long long n, m;
long long c[N][N];
signed main() {
  scanf("%lld%lld", &n, &m);
  c[0][0] = 1;
  FOR(i, 1, n) {
    c[i][0] = 1;
    FOR(j, 1, i) c[i][j] = (c[i - 1][j - 1] + c[i - 1][j]) % P;
  }
  long long res = 0;
  FOR(i, 1, n) {
    long long s = c[__gcd(i, n)][m];
    res = (res + s) % P;
  }
  printf("%lld\n", res);
  return 0;
}
