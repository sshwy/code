// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

const int P = 1e9 + 9;
int main() {
  int k;
  scanf("%d", &k);
  int a = 1, b = 1;
  int lim = 5e6;
  FOR(i, 1, lim) {
    if (a == k) return printf("%d\n", i), 0;
    a = (a + b) % P;
    swap(a, b);
  }
  puts("-1");
  return 0;
}
