// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (long long i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (long long i = (a); i >= (b); --i)

namespace math {
  typedef long long LL;
  inline int pw(int a, int b, int mod) {
    int res = 1;
    for (; b; b >>= 1, a = 1ll * a * a % mod)
      if (b & 1) res = 1ll * res * a % mod;
    return res;
  }
  inline LL mul(LL a, LL b, LL p) {
    if (p <= 1000000000ll) return 1ll * a * b % p;
    if (p <= 1000000000000ll)
      return (((a * (b >> 20) % p) << 20) % p + a * (b & ((1 << 20) - 1))) % p;
    LL d = floor(a * (long double)b / p);
    LL res = (a * b - d * p) % p;
    if (res < 0) res += p;
    return res;
  }
  inline LL pw(LL a, LL b, LL mod) {
    LL res = 1;
    for (; b; b >>= 1, a = mul(a, a, mod))
      if (b & 1) res = mul(res, a, mod);
    return res;
  }
  inline bool check(LL a, LL x, LL times, LL n) {
    LL tmp = pw(a, x, n);
    while (times--) {
      LL last = mul(tmp, tmp, n);
      if (last == 1 && tmp != 1 && tmp != n - 1) return 0;
      tmp = last;
    }
    return tmp == 1;
  }
  int base[] = {2, 3, 5, 7, 11, 13, 17, 19, 23};
  const int S = 8;
  inline bool Miller(LL n) {
    FOR(i, 0, S) {
      if (n == base[i]) return 1;
      if (n % base[i] == 0) return 0;
    }
    LL x = n - 1, times = 0;
    while (!(x & 1)) times++, x >>= 1;
    FOR(_, 0, S) if (!check(base[_], x, times, n)) return 0;
    return 1;
  }
#define mytz __builtin_ctzll
  inline LL gcd(LL a, LL b) {
    if (!a) return b;
    if (!b) return a;
    register int t = mytz(a | b);
    a >>= mytz(a);
    do {
      b >>= mytz(b);
      if (a > b) {
        LL t = b;
        b = a, a = t;
      }
      b -= a;
    } while (b);
    return a << t;
  }
#define F(x) ((mul(x, x, n) + c) % n)
  inline LL rho(LL n, LL c) {
    LL x = 1ll * rand() * rand() % n, y = F(x);
    while (x ^ y) {
      LL w = gcd(abs(x - y), n);
      if (w > 1 && w < n) return w;
      x = F(x), y = F(y), y = F(y);
    }
    return 1;
  }
#undef F
  inline LL calc(LL x) {
    if (Miller(x)) return x;
    LL fsf = 0; // while((fsf=rho(x,rand()%x))==1);
    while ((fsf = rho(x, 2)) == 1)
      ;
    return max(calc(fsf), calc(x / fsf));
  }
  vector<LL> factorize(LL x) {
    if (Miller(x)) return vector<LL>(1, x);
    LL fsf = 0; // while((fsf=rho(x,rand()%x))==1);
    while ((fsf = rho(x, 2)) == 1)
      ;
    vector<LL> A = factorize(fsf);
    vector<LL> B = factorize(x / fsf);
    A.insert(A.end(), B.begin(), B.end());
    return A;
  }
} // namespace math

typedef long long LL;

const LL P = 1998585857, N = 5e3 + 5;

LL pw(LL a, LL m) {
  LL res = 1;
  while (m) m & 1 ? res = res * a % P : 0, a = a * a % P, m >>= 1;
  return res;
}

LL n, m;
LL s[N][N];
vector<LL> fn, v[2], vp, ve, vpe1, v1[100], v2[100], v3[100], v4[100];

LL XXX;
void dfsCalc(const int j, int cur, LL val, LL &res) {
  if (cur == 0) {
    res = (res + val) % P;
    return;
  }
  LL p = vp[cur - 1], e = ve[cur - 1];
  LL d = vpe1[cur - 1];
  LL g = d * p % P;
  if (j == 1) {
    dfsCalc(j, cur - 1, val * (e + 1) % P * g % P, res);
    dfsCalc(j, cur - 1, (P - val) * e % P * d % P, res);
  } else {
    LL a = (v3[cur - 1][j - 1] - 1 + P) % P;
    LL b = (v2[cur - 1][j - 1] - 1 + P) % P;
    LL c = v4[cur - 1][j - 1];

    dfsCalc(j, cur - 1, val * a % P * c % P * g % P, res);
    dfsCalc(j, cur - 1, (P - val * b % P * c % P) % P * d % P, res);
  }
}
LL calc(int j) {
  if (j == 0) return n % P;
  LL res = 0;
  dfsCalc(j, vp.size(), 1, res);
  return res;
}
void go() {
  fn = math::factorize(n);

  sort(fn.begin(), fn.end());
  for (auto x : fn) {
    if (vp.size() && vp.back() == x)
      ve[ve.size() - 1]++;
    else
      vp.push_back(x), ve.push_back(1);
  }
  for (unsigned i = 0; i < vp.size(); i++) {
    LL p = vp[i] % P, e = ve[i], pe = pw(p % P, e), pe1 = pe * p % P;
    vpe1.pb(pw(p, e - 1));
    v1[i].pb(1);
    v2[i].pb(1);
    v3[i].pb(1);
    v4[i].pb(0);
    FOR(j, 1, m) {
      v1[i].pb(v1[i].back() * p % P);
      v2[i].pb(v2[i].back() * pe % P);
      v3[i].pb(v3[i].back() * pe1 % P);
      v4[i].pb(pw((v1[i].back() - 1 + P) % P, P - 2));
    }
  }

  LL coef = m & 1 ? P - 1 : 1;
  LL res = 0;
  FOR(j, 0, m) {
    LL calcJ = calc(j);
    LL t = coef * s[m][j] % P * calcJ % P;
    res = (res + t) % P;

    coef = P - coef;
  }
  LL fnv_m = 1;
  FOR(i, 1, m) fnv_m = i * fnv_m % P;
  fnv_m = pw(fnv_m, P - 2);

  res = res * fnv_m % P;
  printf("%lld\n", res);
}
signed main() {
  scanf("%lld%lld", &n, &m);

  s[0][0] = 1;
  FOR(i, 1, m) {
    FOR(j, 1, i) {
      s[i][j] = (s[i - 1][j - 1] + s[i - 1][j] * (i - 1)) % P;
      // printf("%lld%c", s[i][j], " \n"[j == i]);
    }
  }

  go();
  return 0;
}
