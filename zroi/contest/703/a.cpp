// by Sshwy
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

const int L = 1e5 + 5, K = 55;
char s[L], t[L];
int n, m;
int f[L][K * 2];

void getmin(int &x, int y) { x = min(x, y); }

int main() {
  scanf("%s%s", s + 1, t + 1);
  n = strlen(s + 1);
  m = strlen(t + 1);
  memset(f, 0x3f, sizeof(f));

  const int base = 55;
  f[0][base] = 0;
  FOR(i, 0, n) {
    FOR(j, -50, 50) {
      if (f[i][j + base] < 1e9) {
        int p = i + j;
        if (0 <= p && p <= m) {
          if (s[i + 1] == t[p + 1]) {
            getmin(f[i + 1][j + base], f[i][j + base]);
          } else {
            getmin(f[i + 1][j + base], f[i][j + base] + 1);
          }
          getmin(f[i + 1][j + base - 1], f[i][j + base] + 1);
          getmin(f[i][j + base + 1], f[i][j + base] + 1);
        }
        // printf("f[%d,%d] = %d\n",i,j,f[i][j+base]);
      }
    }
  }
  if (f[n][m - n + base] <= 50)
    printf("%d\n", f[n][m - n + base]);
  else
    puts("-1");
  return 0;
}
