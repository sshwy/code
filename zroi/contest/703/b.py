Li = 0
Ri = 20000


def calc(x):
    if x == 0:
        return 0
    res = 0
    while x>0:
        t = x%10
        if t==1:
            res += 1
        else:
            res = 0
        x//=10
    return res

i = Li
a = 2**i

lst = [False for i in range(200)]

while i <= Ri:
    ca = calc(a)
    if ca>0:
        if lst[ca] == False:
            lst[ca] = True
            print(ca,i)
    if ca>0:
        a*=8
        i+=3
    else:
        a*=2
        i+=1
