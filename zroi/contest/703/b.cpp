// by Sshwy
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

int ans[200];

int main() {
  ans[1] = 4;
  ans[2] = 50;
  ans[3] = 143;
  ans[4] = 1598;
  ans[5] = 10627;
  int n;
  cin >> n;
  cout << ans[n];
  return 0;
}
