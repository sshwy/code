// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

const int N = 105, M = 18, P = 1e9 + 7;

int n, m, k;
struct Set {
  unsigned long long b[2];
  void read() {
    int t;
    scanf("%d", &t);
    FOR(i, 1, t) {
      int val;
      scanf("%d", &val);
      if (val >= 64)
        b[1] |= 1llu << (val - 64);
      else
        b[0] |= 1llu << val;
    }
  }
  int count() { return __builtin_popcountll(b[0]) + __builtin_popcountll(b[1]); }
} s[1 << M];
Set operator&(Set a, Set b) {
  Set c;
  c.b[0] = a.b[0] & b.b[0];
  c.b[1] = a.b[1] & b.b[1];
  return c;
}

long long c[N][N];

int main() {
  c[0][0] = 1;
  FOR(i, 1, N - 1) {
    c[i][0] = 1;
    FOR(j, 1, N - 1) c[i][j] = (c[i - 1][j - 1] + c[i - 1][j]) % P;
  }

  scanf("%d%d%d", &n, &m, &k);

  if (k * 2 > n) {
    puts("0");
    return 0;
  }

  int lim = 1 << m;
  FOR(i, 0, m - 1) s[1 << i].read();
  FOR(i, 0, lim - 1) if (i != (i & -i)) s[i] = s[i - (i & -i)] & s[i & -i];
  long long ans = 0;
  FOR(i, 0, lim - 1) {
    int x = s[i].count(), y = __builtin_popcount(i);
    int coef = y % 2 ? 1 : -1;
    ans = (ans + c[x][k] * coef) % P;
  }
  ans = (ans + P) % P;

  ans = 1ll * ans * c[n - k][k] % P;
  printf("%lld\n", ans);
  return 0;
}
