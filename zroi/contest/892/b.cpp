// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

const int N = 303, P = 998244353;

int n, m, bcnt, ans;
bitset<N> a[N], b[N];
char s[N];

void insert(bitset<N> c) {
  ROF(i, N - 1, 0) if (c[i]) {
    if (b[i][i])
      c ^= b[i];
    else {
      b[i] = c;
      ++bcnt;
      return;
    }
  }
}

bool find(bitset<N> c) {
  ROF(i, N - 1, 0) if (c[i]) {
    if (b[i][i])
      c ^= b[i];
    else
      return false;
  }
  return true;
}

void work(int ban) {
  bcnt = 0;
  FOR(i, 0, m - 1) b[i].reset();
  FOR(i, 1, n) if (i != ban) insert(a[i]);
  FOR(j, 0, m - 1) {
    a[ban].flip(j);
    int rank = bcnt + !find(a[ban]);
    ans = (ans + 1ll * ban * (j + 1) % P * rank) % P;
    a[ban].flip(j);
  }
}

int main() {
  scanf("%d%d", &n, &m);
  FOR(i, 1, n) {
    scanf("%s", s);
    FOR(j, 0, m - 1) if (s[j] == '1') a[i].set(j);
  }
  FOR(i, 1, n) { work(i); }
  printf("%d\n", ans);
  return 0;
}
