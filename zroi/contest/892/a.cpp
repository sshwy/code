// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

const int N = 2e5 + 5;

int n;
vector<int> pos[N];

int c[N];
void add(int pos, int v) {
  for (int i = pos; i < N; i += i & -i) c[i] += v;
}
int pre(int pos) {
  int res = 0;
  for (int i = pos; i > 0; i -= i & -i) res += c[i];
  return res;
}

int main() {
  scanf("%d", &n);
  FOR(i, 1, n) {
    int x;
    scanf("%d", &x);
    pos[x].pb(i);
  }
  int maxw = 2e5, tot = n;
  long long ans = 0;
  FOR(i, 1, maxw) if (pos[i].size()) {
    vector<int> v;
    for (auto x : pos[i]) v.pb(x - pre(x));
    sort(v.begin(), v.end());
    int l = 1, r = tot - v.size() + 1;
    for (auto x : v) {
      ans += min(x - l, r - x);
      ++l, ++r;
    }
    tot -= v.size();
    for (auto x : pos[i]) add(x, 1);
  }
  printf("%lld\n", ans);
  return 0;
}
