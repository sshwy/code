#include <bits/stdc++.h>
#define rep(i, a, b) for (int i = (a); i <= int(b); i++)
using namespace std;

const int maxn = 50, mod = 998244353;
int n, m, lim, a[maxn + 5], b[maxn + 5], f[maxn + 5], g[maxn + 5], h[maxn + 5];

int calc(int x, int k) {
  int y = 1;
  rep(i, 0, k - 1) y = 1ll * y * (x - i) % mod;
  return y;
}

int qpow(int a, int b) {
  int c = 1;
  for (; b; b >>= 1, a = 1ll * a * a % mod)
    if (b & 1) c = 1ll * a * c % mod;
  return c;
}

void mult(int f[], int g[]) {
  fill(h, h + n + 1, 0);
  rep(i, 0, n) rep(j, 0, n - i) h[i + j] = (h[i + j] + 1ll * f[i] * g[j]) % mod;
  copy(h, h + n + 1, f);
}

int main() {
  scanf("%d %d", &n, &m);
  rep(i, 1, m) scanf("%d %d", &a[i], &b[i]);
  if (a[1] > 1) {
    a[++m] = a[1] - 1, b[m] = b[1];
    a[1] = 1;
  }
  lim = n + 1;
  f[1] = 1;
  rep(i, 1, m) {
    rep(k, 0, n) g[k] = 1ll *
                        (calc(b[i] + (i != 1), k + 1) - calc(i != 1, k + 1) + mod) *
                        qpow(calc(k + 1, k + 1), mod - 2) % mod;
    rep(k, 0, n) printf("%d%c", g[k], " \n"[k == n]);
    int k = a[i];
    for (; k; k >>= 1, mult(g, g))
      if (k & 1) mult(f, g);
  }
  printf("%d\n", f[n]);
  return 0;
}
