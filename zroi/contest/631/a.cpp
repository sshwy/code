// by Sshwy
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA
const int N = 55, P = 998244353;
int pw(int a, int m) {
  int res = 1;
  while (m) m & 1 ? res = 1ll * res * a % P : 0, a = 1ll * a * a % P, m >>= 1;
  return res;
}
int n, m, fac[N], fnv[N], A[N], B[N];
int binom(int a, int b) {
  if (a < b) return 0;
  int res = 1;
  FOR(i, 0, b - 1) res = res * 1ll * (a - i) % P;
  res = res * 1ll * fnv[b] % P;
  return res;
}
vector<int> F;
int main() {
  scanf("%d%d", &n, &m);
  FOR(i, 1, m) scanf("%d%d", &A[i], &B[i]);
  if (A[m] > 1) ++m, A[m] = 1, B[m] = B[m - 1], A[m - 1]--;

  fac[0] = 1;
  FOR(i, 1, n) fac[i] = fac[i - 1] * 1ll * i % P;
  fnv[n] = pw(fac[n], P - 2);
  ROF(i, n, 1) fnv[i - 1] = fnv[i] * 1ll * i % P;

  F.resize(n, 0);
  F[0] = 1;
  FOR(i, 1, m) {
    int a = A[i], b = B[i];
    vector<int> f(n);
    FOR(k, 0, n - 1) {
      int coef = i == m ? binom(b, k + 1) : binom(b + 1, k + 1) - (k == 0);
      if (coef < 0) coef += P;
      f[k] = coef; // number of ways to choose k numbers
    }
    vector<int> g = f;
    --a;
    while (a) {
      if (a & 1) {
        ROF(k, n - 1, 0) {
          int coef = 0;
          FOR(x, 0, k) { coef = (coef + f[x] * 1ll * g[k - x]) % P; }
          f[k] = coef;
        }
      }
      ROF(k, n - 1, 0) {
        int coef = 0;
        FOR(x, 0, k) { coef = (coef + g[x] * 1ll * g[k - x]) % P; }
        g[k] = coef;
      }
      a >>= 1;
    }
    // FOR(k,0,n-1)printf("%d%c",f[k]," \n"[k==n-1]);
    ROF(k, n - 1, 0) {
      int coef = 0;
      FOR(x, 0, k) { coef = (coef + F[x] * 1ll * f[k - x]) % P; }
      F[k] = coef;
    }
  }
  printf("%d\n", F[n - 1]);
  return 0;
}
