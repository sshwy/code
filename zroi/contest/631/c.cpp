// by Sshwy
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA
const int N = 1e5 + 5;
int n;
int p[N], use[N];
int a[N], la;
int b[N], lb;
int t[N], d[N];

int calc() {
  FOR(i, 1, n) d[i] = n + 1;
  FOR(i, 1, n) * lower_bound(d + 1, d + n + 1, p[i]) = p[i];
  return lower_bound(d + 1, d + n + 1, n + 1) - d - 1;
}
int main() {
  scanf("%d", &n);
  FOR(i, 1, n) scanf("%d", &p[i]);
  FOR(i, 1, n) if (p[i] == 0) a[++la] = i;
  FOR(i, 1, n) use[p[i]] = 1;
  FOR(i, 1, n) if (!use[i]) b[++lb] = i;
  assert(la == lb);
  FOR(i, 1, la) t[i] = i;
  int ans = 0;
  do {
    FOR(i, 1, la) p[a[i]] = b[t[i]];
    ans = max(ans, calc());
  } while (next_permutation(t + 1, t + la + 1));
  printf("%d\n", ans);
  return 0;
}
