// by Sshwy
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA
const int INF = 0x3f3f3f3f;
namespace dinic {
  const int N = 5e5, M = 3e6;
  struct qxx {
    int nex, t, v;
  } e[M];
  int h[N], hh[N], le = 1;
  void add_path(int f, int t, int v) { e[++le] = {h[f], t, v}, h[f] = le; }
  void add_flow(int f, int t, int v) { add_path(f, t, v), add_path(t, f, 0); }

  int s, t;
  int max_flow;

  int d[N];

  queue<int> q;
  bool bfs() {
    memset(d, 0, sizeof(d[0]) * (t + 1));
    d[s] = 1;
    q.push(s);
    while (!q.empty()) {
      int u = q.front();
      q.pop();
      for (int i = h[u]; i; i = e[i].nex) {
        int v = e[i].t, w = e[i].v;
        if (!w || d[v]) continue;
        d[v] = d[u] + 1;
        q.push(v);
      }
    }
    return d[t];
  }
  int dfs(int u, int flow) {
    if (u == t) return flow;
    int rest = flow;
    for (int &i = hh[u]; i && rest; i = e[i].nex) {
      int v = e[i].t, w = e[i].v;
      if (!w || d[v] != d[u] + 1) continue;
      int k = dfs(v, min(w, rest));
      e[i].v -= k, e[i ^ 1].v += k, rest -= k;
    }
    return flow - rest;
  }
  int dinic() {
    max_flow = 0;
    while (bfs()) {
      memcpy(hh, h, sizeof(h[0]) * (t + 1));
      max_flow += dfs(s, INF);
    }
    return max_flow;
  }
} // namespace dinic

const int Z = 75, D = 75, H = 75;
int z, d, h, n, hl, hr;
int l[Z], r[D][H], f[Z][D][H];

int S() { return 0; }
int zz(int k) { return k; }
int zz_day_i(int k, int i) { return (k - 1) * d + i + zz(z); }
int zz_day_i_hour_j(int k, int i, int j) {
  return (k - 1) * d * h + (i - 1) * h + j + zz_day_i(z, d);
}
int day_i_hour_j(int i, int j) { return (i - 1) * h + j + zz_day_i_hour_j(z, d, h); }
int zz_day_i_play(int k, int i) { return (k - 1) * d + i + day_i_hour_j(d, h); }
int T() { return zz_day_i_play(z, d) + 1; }

// void check(){
//    printf("%d\n",S());
//    FOR(k,1,z)printf("%d\n",zz(k));
//    FOR(k,1,z)FOR(i,1,d)printf("%d%c",zz_day_i(k,i)," \n"[i==d]);
//    FOR(k,1,z)FOR(i,1,d)FOR(j,1,h)printf("%d%c",zz_day_i_hour_j(k,i,j),"
//    \n"[i==d && j==h]);
//    //FOR(k,1,z)FOR(i,1,d)FOR(j,1,h)printf("%d%c",zz_day_i_hour_j_2(k,i,j),"
//    \n"[i==d && j==h]); FOR(i,1,d)FOR(j,1,h)printf("%d%c",day_i_hour_j(i,j),"
//    \n"[j==h]); FOR(k,1,z)FOR(i,1,d)printf("%d%c",zz_day_i_play(k,i),"
//    \n"[i==d]); printf("%d\n",T());
//}
int main() {
  scanf("%d%d%d%d%d%d", &z, &d, &h, &n, &hl, &hr);
  // check();
  FOR(i, 1, z) scanf("%d", &l[i]);
  FOR(i, 1, d) FOR(j, 1, h) scanf("%d", &r[i][j]);
  FOR(k, 1, z) { FOR(i, 1, d) FOR(j, 1, h) scanf("%d", &f[k][i][j]); }

  dinic::s = S();
  dinic::t = T();
  FOR(k, 1, z) {
    dinic::add_flow(S(), zz(k), l[k] + d);
    FOR(i, 1, d) {
      int c = 0;
      FOR(j, 1, h) c += (f[k][i][j] == 0);
      if (n < c) return puts("No"), 0;
      dinic::add_flow(zz(k), zz_day_i(k, i), n - c + 1);
      FOR(j, 1, h) if (f[k][i][j] == 1) {
        dinic::add_flow(zz_day_i(k, i), zz_day_i_hour_j(k, i, j), 1);
        dinic::add_flow(zz_day_i_hour_j(k, i, j), day_i_hour_j(i, j), 1);
        if (hl <= j && j <= hr) {
          dinic::add_flow(zz_day_i_hour_j(k, i, j), zz_day_i_play(k, i), 1);
        }
      }
      dinic::add_flow(zz_day_i_play(k, i), T(), 1);
    }
  }
  int tot = z * d;
  FOR(i, 1, d) FOR(j, 1, h) {
    if (r[i][j]) dinic::add_flow(day_i_hour_j(i, j), T(), r[i][j]);
    tot += r[i][j];
  }
  int ans = dinic::dinic();
  // printf("ans %d\n",ans);
  // printf("tot %d\n",tot);
  if (ans == tot)
    puts("Yes");
  else
    puts("No");
  return 0;
}
