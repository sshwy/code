#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
#include <bitset>
const int N = 3005;
typedef bitset<N> bin;

int n, m;

vector<int> g[N];
bin s[N];

void go() {
  scanf("%d%d", &n, &m);
  FOR(i, 1, n) g[i].clear();
  FOR(i, 1, m) {
    int a, b;
    scanf("%d%d", &a, &b);
    g[a].pb(b);
  }
  FOR(i, 1, n) s[i].reset(), s[i].set(i);
  ROF(i, n, 1) for (int v : g[i]) s[i] |= s[v];
  FOR(i, 1, n) for (int v : g[i]) s[v] |= s[i];
  int ans = 0;
  FOR(i, 1, n) ans += s[i].count();
  printf("%d\n", ans);
}
int main() {
  int t;
  scanf("%d", &t);
  FOR(i, 1, t) go();
  return 0;
}
