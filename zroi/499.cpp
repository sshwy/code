#include <bits/stdc++.h>
#define FOR(a, b, c) for (int a = (b); a <= (c); a++)
using namespace std;
const int N = 50005, INF = 0x3f3f3f3f;

struct qxx {
  int nex, t, v;
};
qxx e[N * 2];
int h[N], le = 1;
void add_path(int f, int t, int v) { e[++le] = (qxx){h[f], t, v}, h[f] = le; }
#define FORew(i, u, v, w) \
  for (int i = h[u], v, w; v = e[i].t, w = e[i].v, i; i = e[i].nex)

int n, m;
int f[N];

int t[N], lt, tot;

int calc_arg1;
int calc(int *a, int la, int lim) { // get matchs from a
  int res = 0;
  calc_arg1 = 0;
  while (la > 0) {
    if (a[la] == -INF)
      --la; // ban
    else if (a[la] >= lim)
      ++res, --la, ++calc_arg1;
    else
      break;
  }
  int pos = 1;
  while (pos <= la) { // match a[la] with a[pos]
    if (a[la] == -INF)
      --la; // ban
    else {
      while (pos < la && a[pos] + a[la] < lim) ++pos; // if a[pos]==-INF then ++pos
      if (pos == la) break;
      assert(a[pos] != -INF);
      ++res, ++pos, --la;
      calc_arg1 += 2;
    }
  }
  return res;
}
void dfs(int u, int p, int lim) {
  // printf("dfs(%d,%d,%d)\n",u,p,lim);
  FORew(i, u, v, w) {
    if (v == p) continue;
    dfs(v, u, lim);
  }
  lt = 0;
  FORew(i, u, v, w) {
    if (v == p) continue;
    t[++lt] = f[v] + w;
  }
  sort(t + 1, t + lt + 1);
  int cnt = calc(t, lt, lim);
  tot += cnt;
  if (calc_arg1 == lt) return;
  int l = 1, r = lt, mid, tmp = -INF;
  while (l < r)
    mid = (l + r + 1) >> 1, swap(t[mid], tmp),
    calc(t, lt, lim) >= cnt ? l = mid : r = mid - 1, swap(t[mid], tmp);
  f[u] = t[l];
  if (f[u] >= lim) ++cnt, f[u] = 0;
  // printf("f[%d]=%d\n",u,f[u]);
}

bool check(int x) {
  // printf("check(%d)\n",x);
  FOR(i, 1, n) f[i] = 0;
  tot = 0;
  dfs(1, 0, x);
  return tot >= m;
}
int main() {
  scanf("%d%d", &n, &m);
  FOR(i, 1, n - 1) {
    int u, v, w;
    scanf("%d%d%d", &u, &v, &w);
    add_path(u, v, w), add_path(v, u, w);
  }
  int l = 0, r = 1e9, mid;
  while (l < r) mid = (l + r + 1) >> 1, check(mid) ? l = mid : r = mid - 1;
  printf("%d\n", l);
  return 0;
}
