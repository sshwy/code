#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
#define int lld
const int N = 5000 + 5, P = 1e9 + 7;

int n, a[N];
int ans;

int d[N], ld;
int f[N][N], g[N], fac[N];

int c[N * 10];
int pre(int pos) {
  int res = 0;
  for (int i = pos; i > 0; i -= i & -i) res = (res + c[i]) % P;
  return res;
}
void add(int pos, int x) {
  for (int i = pos; i <= n; i += i & -i) c[i] = (c[i] + x) % P;
}
signed main() {
  scanf("%lld", &n);
  FOR(i, 1, n) scanf("%lld", &a[i]);
  FOR(i, 1, n) d[++ld] = a[i];
  sort(d + 1, d + ld + 1);
  FOR(i, 1, n) a[i] = lower_bound(d + 1, d + ld + 1, a[i]) - d;
  FOR(i, 1, n) f[i][1] = 1;
  FOR(j, 2, n) {
    FOR(i, 0, ld * 2) c[i] = 0;
    FOR(i, 1, j - 1) add(a[i], f[i][j - 1]);
    FOR(i, j, n) {
      f[i][j] = pre(a[i]);
      add(a[i], f[i][j - 1]);
      // printf("f[%lld,%lld]=%lld\n",i,j,f[i][j]);
    }
  }
  FOR(i, 1, n) { FOR(j, i, n) g[i] = (g[i] + f[j][i]) % P; }
  fac[0] = 1;
  FOR(i, 1, n) fac[i] = 1ll * fac[i - 1] * i % P;
  FOR(i, 1, n - 1) {
    ans = (ans + g[i] * 1ll * fac[n - i] % P -
              g[i + 1] * 1ll * fac[n - i - 1] % P * (i + 1ll) % P) %
          P;
  }
  ans = (ans + P) % P;
  printf("%lld\n", ans);
  return 0;
}
