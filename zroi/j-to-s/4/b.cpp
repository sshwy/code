#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
typedef long long LL;
const LL P = 1e9 + 7;
LL pw(LL a, LL m, LL p) {
  a %= p;
  int res = 1;
  while (m) m & 1 ? res = res * a % p : 0, a = a * a % p, m >>= 1;
  return res;
}

LL n, m, ans = 0;
LL t1, t2;
LL f(LL x) {
  return ((x + 1) * (x + 1) % P * x % P * pw(2, P - 2, P) % P -
             x * (x + 1) % P * (x * 2 + 1) % P * pw(6, P - 2, P) % P) %
         P;
}
int main() {
  scanf("%lld%lld", &n, &m);
  ans = f(n) * f(m) % P;
  ans = (ans + P) % P;
  printf("%lld\n", ans);
}
