#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
#define int lld
typedef pair<int, int> pii;
const int N = 5e3 + 5;

char s[N];
int t, n;

pii trans(pii x, char c) {
  if (c == 'N') return mk(x.fi, x.se + 1);
  if (c == 'S') return mk(x.fi, x.se - 1);
  if (c == 'W') return mk(x.fi - 1, x.se);
  if (c == 'E') return mk(x.fi + 1, x.se);
  return x;
}
signed main() {
  scanf("%s", s + 1);
  n = strlen(s + 1);
  scanf("%lld", &t);
  pii cur = mk(0, 0);
  FOR(i, 1, n) cur = trans(cur, s[i]);
  cur = mk(t / n * cur.fi, t / n * cur.se);
  t %= n;
  FOR(i, 1, t) cur = trans(cur, s[i]);
  printf("%lld %lld\n", cur.fi, cur.se);
  return 0;
}
