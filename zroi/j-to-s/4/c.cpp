#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
typedef long long LL;
const LL N = 2e5 + 5, INF = 0x3f3f3f3f3f3f3f3f;

int n, m;
LL t[N], p[N], a[N];

LL calc(LL x) {
  LL res = 0;
  FOR(i, 1, n) {
    if (x <= p[i]) break;
    res += (x - p[i]) / t[i];
  }
  return res;
}
LL query(LL x) {
  LL l = 0, r = INF;
  while (l < r) {
    LL mid = (l + r) >> 1;
    if (x <= calc(mid))
      r = mid;
    else
      l = mid + 1;
  }
  return l;
}
int main() {
  scanf("%d%d", &n, &m);
  FOR(i, 1, n) scanf("%lld", &t[i]);
  FOR(i, 1, n) p[i] = p[i - 1] + t[i - 1];
  FOR(i, 1, m) {
    scanf("%lld", &a[i]);
    printf("%lld ", query(a[i]));
  }
  return 0;
}
