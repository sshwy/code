#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
/*}}}*/
/******************heading******************/
using namespace std;
#define lowbit(x) (x & (-x))
int n, P, q, op, las;
short s[8010][8010][4];
void add(const int &r, const int &c, const int &val) {
  int a[4] = {val % P, val * c % P, val * r % P, val * r * c % P};
  for (int x = r; x <= n; x += lowbit(x)) {
    for (int y = c; y <= n; y += lowbit(y)) {
      for (int i = 0; i < 4; ++i) s[x][y][i] = ((int)s[x][y][i] + a[i]) % P;
    }
  }
}
int query(const int &r, const int &c) {
  int a[4] = {0, 0, 0, 0};
  for (int x = r; x > 0; x -= lowbit(x)) {
    for (int y = c; y > 0; y -= lowbit(y)) {
      for (int i = 0; i < 4; ++i) a[i] = (a[i] + s[x][y][i]) % P;
    }
  }
  return ((r + 1) * (c + 1) % P * a[0] - (r + 1) * a[1] - (c + 1) * a[2] + a[3]) % P;
}
int main() {
  scanf("%d%d%d", &n, &q, &P);
  while (q--) {
    int rL, rR, cL, cR;
    scanf("%d%d%d%d%d", &op, &rL, &rR, &cL, &cR);
    rL = (rL + las) % n + 1;
    rR = (rR + las) % n + 1;
    cL = (cL + las) % n + 1;
    cR = (cR + las) % n + 1;
    if (rL > rR) swap(rL, rR);
    if (cL > cR) swap(cL, cR);
    if (op == 1) {
      add(rL, cL, 1);
      add(rL, cR + 1, -1);
      add(rR + 1, cL, -1);
      add(rR + 1, cR + 1, 1);
    } else {
      int ans = (query(rR, cR) - query(rL - 1, cR) - query(rR, cL - 1) +
                    query(rL - 1, cL - 1)) %
                P;
      ans = (ans + P) % P;
      printf("%d\n", ans), las = (las + ans) % n;
    }
  }
  return 0;
}
