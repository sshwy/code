#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
namespace IO {
  char nc() {
    static char bf[100000], *p1 = bf, *p2 = bf;
    return p1 == p2 && (p2 = (p1 = bf) + fread(bf, 1, 100000, stdin), p1 == p2)
               ? EOF
               : *p1++;
  }
  int rd() {
    int res = 0;
    char c = getchar();
    while (!isdigit(c)) c = getchar();
    while (isdigit(c)) res = res * 10 + c - '0', c = getchar();
    return res;
  }
} // namespace IO
/******************heading******************/
#define int lld
const int N = 1e5 + 50, INF = 0x3f3f3f3f3f3f3f3f;

int n, m, h[N];

int d[N], c[N];

int calc(int *a, int l) {
  // printf("calc\n",calc);
  int sum = 0, res = 0;
  FOR(i, 1, l) {
    // i-1,2i-1,...,a[i]*i-1
    //= i*(1+...+a[i])-a[i]
    FOR(j, 1, a[i]) {
      sum += i;
      res += sum - 1;
    }
  }
  return res;
}
int sum(int *a, int l) {
  int res = 0;
  FOR(i, 1, l) res += a[i];
  return res;
}
int ans;
int tot;

signed main() {
  scanf("%lld%lld", &n, &m);
  FOR(i, 1, n) scanf("%lld", &h[i]);
  FOR(i, 1, n) c[h[i]]++;

  int *a = c, l = 100000;
  FOR(i, 1, m) {
    // printf("i=%lld\n",i);
    // FOR(j,1,l)if(a[j])printf("a[%lld]=%lld\n",j,a[j]);
    if (a[1]) {
      ++a, --l;
      ans += sum(a, l);
      // puts("群攻");
      // printf("ans=%lld\n",ans);
    } else {
      int c1 = sum(a + 1, l - 1) + calc(a + 1, l - 1), c2 = INF;
      bool f = 0;
      FOR(j, 2, l) {
        if (a[j]) {
          a[j]--, a[j - 2]++;
          c2 = sum(a, l) + calc(a, l);
          a[j]++, a[j - 2]--;

          if (c1 < c2) {
            ++a, --l;
            ans += sum(a, l);
            // puts("群攻");
          } else {
            a[j]--, a[j - 2]++;
            ans += sum(a, l);
            // puts("重击");
          }
          f = 1;

          break;
        }
      }
      if (!f) {
        ++a, --l;
        ans += sum(a, l);
        // puts("群攻");
      }
    }
  }
  ans += calc(a, l);
  printf("%lld", ans);
  return 0;
}
/*
 * Test1: 贪心从小到大杀，受到的伤害是前缀和的和-|h|；
 *
 * Test2：群攻：群攻一次会少扣|h|*(|h|+1)/2的血。这个等价于，把所有怪物血减1，再加一个血量为1的怪物的局面。
 * 重击：一个顶俩。根据贪心策略，当且仅当不存在血量等于1的怪时再用，不然先普攻杀1。
 */
