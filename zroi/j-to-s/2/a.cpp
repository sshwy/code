#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
namespace IO {
  char nc() {
    static char bf[100000], *p1 = bf, *p2 = bf;
    return p1 == p2 && (p2 = (p1 = bf) + fread(bf, 1, 100000, stdin), p1 == p2)
               ? EOF
               : *p1++;
  }
  int rd() {
    int res = 0;
    char c = getchar();
    while (!isdigit(c)) c = getchar();
    while (isdigit(c)) res = res * 10 + c - '0', c = getchar();
    return res;
  }
} // namespace IO
/******************heading******************/
#define int lld

int l, r, ans;
int m10[20];

signed main() {
  scanf("%lld%lld", &l, &r);
  m10[0] = 1;
  FOR(i, 1, 18) m10[i] = m10[i - 1] * 10;
  FOR(i, 10, 99) {
    if (i % 11 == 0) continue;
    if (l <= i && i <= r) ++ans;
  }
  FOR(len, 3, 17) {
    FOR(abit, 0, 9) {
      FOR(apos, 0, len - 1) {
        FOR(bit, 0, 9) {
          if (bit == abit) continue;
          if (abit == 0 && apos == len - 1) continue;
          if (bit == 0 && apos != len - 1) continue;
          int num = 0;
          FOR(i, 0, len - 1) {
            if (i == apos)
              num += m10[i] * abit;
            else
              num += m10[i] * bit;
          }
          if (l <= num && num <= r) {
            ++ans;
            // printf("bit=%lld,abit=%lld ",bit,abit);
            // printf("len=%lld, apos=%lld\n",len,apos);
            // printf("%lld ",num);
          }
        }
      }
    }
  }
  printf("%lld\n", ans);
  return 0;
}
