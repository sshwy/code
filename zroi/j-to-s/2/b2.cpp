#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
namespace IO {
  char nc() {
    static char bf[100000], *p1 = bf, *p2 = bf;
    return p1 == p2 && (p2 = (p1 = bf) + fread(bf, 1, 100000, stdin), p1 == p2)
               ? EOF
               : *p1++;
  }
  int rd() {
    int res = 0;
    char c = getchar();
    while (!isdigit(c)) c = getchar();
    while (isdigit(c)) res = res * 10 + c - '0', c = getchar();
    return res;
  }
} // namespace IO
/******************heading******************/
#define int lld
const int N = 1e6 + 50;

struct data {
  int a, b, c;
  bool operator==(data d) { return a == d.a && b == d.b && c == d.c; }
  bool operator<(data d) const {
    return a == d.a ? (b == d.b ? c < d.c : b < d.b) : a < d.a;
  }
  void zip() {
    int x = min(a, min(b, c));
    a -= x, b -= x, c -= x;
  }
} d[N];
int ld, ans;

char s[N];

signed main() {
  scanf("%s", s + 1);
  for (int i = 1; s[i]; i++) {
    ++ld, d[ld] = d[ld - 1];
    if (s[i] == 'A')
      d[ld].a++;
    else if (s[i] == 'B')
      d[ld].b++;
    else if (s[i] == 'C')
      d[ld].c++;
  }
  FOR(i, 0, ld) FOR(j, i + 1, ld) {
    if (d[i].a - d[j].a d[i].b - d[j].b == d[i].c - d[j].c) ans++;
  }
  printf("%lld", ans);
  return 0;
}
