#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
namespace IO {
  char nc() {
    static char bf[100000], *p1 = bf, *p2 = bf;
    return p1 == p2 && (p2 = (p1 = bf) + fread(bf, 1, 100000, stdin), p1 == p2)
               ? EOF
               : *p1++;
  }
  int rd() {
    int res = 0;
    char c = getchar();
    while (!isdigit(c)) c = getchar();
    while (isdigit(c)) res = res * 10 + c - '0', c = getchar();
    return res;
  }
} // namespace IO
/******************heading******************/
int f[105][205][11];
int a[1005], la;
/*
 * f[i,j]前i个数，奇数位的个数比偶数位的个数多j-100，数位和余数为k是否可行
 */

void DP() {
  memset(f, 0, sizeof(f[0]) * (la + 1));
  f[0][0 + 101][0] = 1;
  FOR(i, 0, la) {
    FOR(j1, -i, i) {
      int j = j1 + 101;
      FOR(k, 0, 10) {
        if (f[i][j][k]) {
          // printf("f[%d,%d,%d]=1\n",i,j1,k);
          // a[i+1]放奇数
          f[i + 1][j + 1][(k + a[i + 1]) % 11] = 1;
          // a[i+1]放偶数
          f[i + 1][j - 1][(k - a[i + 1] + 11) % 11] = 1;
        }
      }
    }
  }
}
void go() {
  la = 0;
  FOR(i, 1, 9) {
    int d;
    scanf("%d", &d);
    FOR(j, 1, d) a[++la] = i;
  }
  DP();
  int ans = 0x3f3f3f3f;
  FOR(j1, -la, la) {
    int j = j1 + 101;
    if (f[la][j][0]) { ans = min(ans, la + max(abs(j1) - 1, 0)); }
  }
  printf("%d\n", ans == 0x3f3f3f3f ? -1 : ans);
}
int main() {
  int t;
  scanf("%d", &t);
  while (t-- > 0) go();
  return 0;
}
