#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
namespace IO {
  char nc() {
    static char bf[100000], *p1 = bf, *p2 = bf;
    return p1 == p2 && (p2 = (p1 = bf) + fread(bf, 1, 100000, stdin), p1 == p2)
               ? EOF
               : *p1++;
  }
  int rd() {
    int res = 0;
    char c = getchar();
    while (!isdigit(c)) c = getchar();
    while (isdigit(c)) res = res * 10 + c - '0', c = getchar();
    return res;
  }
} // namespace IO
/******************heading******************/

int n, m;

namespace S1 {
  const int M = 40; /*{{{*/
  pii eg[40];

  struct qxx {
    int nex, t;
  };
  qxx e[M];
  int h[M], le;
  void add_path(int f, int t) { e[++le] = (qxx){h[f], t}, h[f] = le; }
  int dg[M], d[M];
  bool vis[M];
  void clear() {
    memset(h, 0, sizeof(h)), le = 0;
    memset(dg, 0, sizeof(dg));
    memset(d, 0, sizeof(d));
    memset(vis, 0, sizeof(vis));
  }
  queue<int> q;
  void go() {
    FOR(i, 1, m) scanf("%d%d", &eg[i].fi, &eg[i].se);
    int lim = 1 << m, ans = 0x3f3f3f3f;
    FOR(ii, 0, lim - 1) {
      clear();
      FOR(j, 1, m) {
        if ((ii >> j - 1) & 1) {
          add_path(eg[j].fi, eg[j].se);
          dg[eg[j].se]++;
        } else {
          add_path(eg[j].se, eg[j].fi);
          dg[eg[j].fi]++;
        }
      }
      FOR(j, 1, n) if (dg[j] == 0) q.push(j), vis[j] = 1;
      if (q.empty()) continue;
      int tot = 0;
      while (!q.empty()) {
        int u = q.front();
        q.pop();
        // if(ii==2)printf("u=%d\n",u);
        for (int i = h[u]; i; i = e[i].nex) {
          const int v = e[i].t;
          dg[v]--;
          d[v] = max(d[v], d[u] + 1), tot = max(tot, d[v]);
          if (!dg[v]) q.push(v), vis[v] = 1;
        }
      }
      bool fl = 0;
      FOR(i, 1, n) {
        if (!vis[i]) {
          fl = 1;
          break;
        }
      }
      if (fl) continue;

      // printf("ii=%d,d:",ii);FOR(j,1,n)printf("%d%c",d[j]," \n"[j==n]);
      // printf("tot=%d\n",tot);
      // printf("ans=%d\n",ans);

      ans = min(ans, tot);
    }
    printf("%d", ans);
  } /*}}}*/
} // namespace S1
namespace S2 {
  const int M = 500;
  struct qxx {
    int nex, t;
  };
  qxx e[M];
  int h[M], le = 1;
  bool vis[M];
  bool vvis[M];
  void add_path(int f, int t) { e[++le] = (qxx){h[f], t}, h[f] = le; }
  bool dfs(int u, int path) {
    // printf("dfs(%d,%d)\n",u,path);
    vvis[u] = 1;
    int fl = 0;
    for (int i = h[u]; i; i = e[i].nex) {
      if ((i ^ 1) == path || vis[i]) continue;
      const int v = e[i].t;
      if (vvis[v]) continue;
      fl = 1;
      // printf("%d -> %d\n",u,v);
      dfs(v, i);
      vis[i] = vis[i ^ 1] = 1;
      while (e[i].nex && vis[e[i].nex]) e[i].nex = e[e[i].nex].nex;
    }
    return fl;
  }
  void go() {
    FOR(i, 1, m) {
      int u, v;
      scanf("%d%d", &u, &v);
      add_path(u, v), add_path(v, u);
    }
    int ans = 0;
    FOR(i, 1, m) {
      memset(vvis, 0, sizeof(vvis));
      // printf("\033[31mround %d\033[0m\n",i);
      int fl = 0;
      FOR(j, 1, n) fl |= dfs(j, 0);
      if (fl)
        ans++;
      else
        break;
    }
    printf("%d", ans);
  }
} // namespace S2
int main() {
  scanf("%d%d", &n, &m);
  if (m <= 20)
    S1::go();
  else
    S2::go();

  return 0;
}
