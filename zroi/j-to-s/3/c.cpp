#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
namespace IO {
  char nc() {
    static char bf[100000], *p1 = bf, *p2 = bf;
    return p1 == p2 && (p2 = (p1 = bf) + fread(bf, 1, 100000, stdin), p1 == p2)
               ? EOF
               : *p1++;
  }
  int rd() {
    int res = 0;
    char c = getchar();
    while (!isdigit(c)) c = getchar();
    while (isdigit(c)) res = res * 10 + c - '0', c = getchar();
    return res;
  }
} // namespace IO
/******************heading******************/
const int N = 2e5 + 5;

int n;
int f[N];

void init(int k) { FOR(i, 0, k) f[i] = i; }
int get(int k) { return f[k] == k ? k : f[k] = get(f[k]); }
void merge(int u, int v) { f[get(u)] = get(v); }
bool find(int u, int v) { return get(u) == get(v); }

bool vis[N];
int ans, fl;

int main() {
  scanf("%d", &n);
  init(n);
  FOR(i, 1, n) {
    int x;
    scanf("%d", &x);
    if (i == x) fl = 1;
    merge(i, x);
  }
  FOR(i, 1, n) ans += !vis[get(i)], vis[get(i)] = 1;
  printf("%d\n", ans - fl);
  return 0;
}
