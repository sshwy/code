#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
namespace IO {
  char nc() {
    static char bf[100000], *p1 = bf, *p2 = bf;
    return p1 == p2 && (p2 = (p1 = bf) + fread(bf, 1, 100000, stdin), p1 == p2)
               ? EOF
               : *p1++;
  }
  int rd() {
    int res = 0;
    char c = getchar();
    while (!isdigit(c)) c = getchar();
    while (isdigit(c)) res = res * 10 + c - '0', c = getchar();
    return res;
  }
} // namespace IO
/******************heading******************/
const int M = 1005, MM = M * M;

int k, n, m;
int vis[M];
int a[M], la;

queue<pii> q;
int d[MM * 2 + 20];
int main() {
  scanf("%d%d%d", &k, &n, &m);
  memset(d, 0x3f, sizeof(d));
  FOR(i, 1, k) {
    int x;
    scanf("%d", &x);
    if (!vis[x]) a[++la] = x;
    vis[x] = 1;
  }
  FOR(i, 1, la) a[i] -= n;
  FOR(i, 1, la) q.push(mk(1, a[i])), d[a[i] + MM] = 1;
  while (!q.empty()) {
    pii u = q.front();
    q.pop();
    if (d[u.se + MM] < u.fi) continue;
    if (u.se == 0) break;
    FOR(i, 1, la) {
      if (d[u.se + a[i] + MM] <= d[u.se + MM] + 1) continue;
      d[u.se + a[i] + MM] = d[u.se + MM] + 1;
      q.push(mk(d[u.se + a[i] + MM], u.se + a[i]));
    }
  }
  printf("%d\n", d[MM] == 0x3f3f3f3f ? -1 : d[MM]);
  return 0;
}
