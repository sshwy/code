#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 1e5 + 5;

int n, m;
int a[N];

int mex(int x) {
  if (x == 0) return 1;
  return 0;
}
int mex(int a, int b) {
  if (min(a, b) == 0 && max(a, b) == 1) return 2;
  if (min(a, b) == 0) return 1;
  return 0;
}
int sg0(int x) {
  if (x == 0) return 0;
  if (x & 1) return mex(sg0(x - 1));
  return mex(sg0(x - 1), 0);
}
int vis[N], s1[N], cnt;
int sg1(int x) {
  if (vis[x]) return s1[x];
  vis[x] = 1;
  if (x == 0) return s1[x] = 0;
  if (x & 1) return s1[x] = mex(sg1(x - 1));
  return s1[x] = mex(sg1(x - 1), sg1(x / 2));
}

int main() {
  FOR(i, 1, 2000) printf("%d", sg1(i));
  // scanf("%d%d",&n,&m);
  // FOR(i,1,n)scanf("%d",&a[i]);
  return 0;
}
