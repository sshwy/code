#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
namespace IO {
  char nc() {
    static char bf[100000], *p1 = bf, *p2 = bf;
    return p1 == p2 && (p2 = (p1 = bf) + fread(bf, 1, 100000, stdin), p1 == p2)
               ? EOF
               : *p1++;
  }
  int rd() {
    int res = 0;
    char c = getchar();
    while (!isdigit(c)) c = getchar();
    while (isdigit(c)) res = res * 10 + c - '0', c = getchar();
    return res;
  }
} // namespace IO
/******************heading******************/
const int N = 1e5 + 5;

int n;
int p[N], pos[N];
long long ans;

bool vis[N << 2];
void assign(int p, int u = 1, int l = 1, int r = n) {
  // printf("assign(%d,%d,%d,%d)\n",p,u,l,r);
  if (l == r) return vis[u] = 1, void();
  int mid = (l + r) >> 1;
  if (p <= mid)
    assign(p, u << 1, l, mid);
  else
    assign(p, u << 1 | 1, mid + 1, r);
  vis[u] = vis[u << 1] | vis[u << 1 | 1];
}
int ql(int L, int R, int u = 1, int l = 1, int r = n) {
  if (R < l || r < L) return n + 1;
  if (L > R) return n + 1;
  if (!vis[u]) return n + 1;
  // printf("ql(%d,%d,%d,%d,%d)\n",L,R,u,l,r);
  if (l == r) return l;
  int mid = (l + r) >> 1;
  int t = ql(L, R, u << 1, l, mid);
  if (t != n + 1) return t;
  return ql(L, R, u << 1 | 1, mid + 1, r);
}
int qr(int L, int R, int u = 1, int l = 1, int r = n) {
  if (R < l || r < L) return 0;
  if (L > R) return 0;
  // printf("qr(%d,%d,%d,%d,%d)\n",L,R,u,l,r);
  if (!vis[u]) return 0;
  if (l == r) return l;
  int mid = (l + r) >> 1;
  int t = qr(L, R, u << 1 | 1, mid + 1, r);
  if (t) return t;
  return qr(L, R, u << 1, l, mid);
}

void calc(int po) {
  // printf("\033[31mcalc(p[%d]=%d)\033[0m\n",po,p[po]);
  int l1 = qr(1, po), l2 = qr(1, l1 - 1);
  int r1 = ql(po, n), r2 = ql(r1 + 1, n);
  // printf("l2=%d,l1=%d,r1=%d,r2=%d\n",l2,l1,r1,r2);
  // printf("ans=%lld\n",ans);
  if (l1 != 0) {
    int x = l1 - l2, y = r1 - po;
    ans += 1ll * x * y * p[po];
  }
  if (r1 != n + 1) {
    int x = po - l1, y = r2 - r1;
    ans += 1ll * x * y * p[po];
  }
  // printf("ans=%lld\n",ans);
}
int main() {
  scanf("%d", &n);
  FOR(i, 1, n) scanf("%d", &p[i]), pos[p[i]] = i;
  ROF(i, n, 1) {
    calc(pos[i]);
    // merge
    assign(pos[i]);
  }
  printf("%lld\n", ans);
  return 0;
}
/*
 * BUG#1:线段树返回的时侯直接return了，没有判是否有解（t）
 */
