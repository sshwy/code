#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
namespace IO {
  char nc() {
    static char bf[100000], *p1 = bf, *p2 = bf;
    return p1 == p2 && (p2 = (p1 = bf) + fread(bf, 1, 100000, stdin), p1 == p2)
               ? EOF
               : *p1++;
  }
  int rd() {
    int res = 0;
    char c = getchar();
    while (!isdigit(c)) c = getchar();
    while (isdigit(c)) res = res * 10 + c - '0', c = getchar();
    return res;
  }
} // namespace IO
/******************heading******************/
const int N = 1e4 + 5, M = N << 1;

int n, d[N], c[N], ans[N];
long long sum;

struct qxx {
  int nex, t;
};
qxx e[M];
int h[N], le = 1;
void add_path(int f, int t) { e[++le] = (qxx){h[f], t}, h[f] = le; }

queue<int> q;

int main() {
  scanf("%d", &n);
  FOR(i, 1, n - 1) {
    int a, b;
    scanf("%d%d", &a, &b);
    add_path(a, b), add_path(b, a);
    d[a]++, d[b]++;
  }
  FOR(i, 1, n) scanf("%d", &c[i]);
  sort(c + 1, c + n + 1);
  int tot = 0;
  FOR(i, 1, n) if (d[i] == 1) q.push(i);
  while (!q.empty()) {
    int u = q.front();
    q.pop();
    ans[u] = c[++tot];
    if (q.empty()) break;
    int cnt = 0;
    for (int i = h[u]; i; i = e[i].nex) {
      const int v = e[i].t;
      if (d[v] == 0) continue;
      --d[u], --d[v];
      if (d[v] == 1) q.push(v);
      ++cnt;
    }
    assert(cnt == 1);
  }
  FOR(i, 1, n - 1) sum += c[i];
  printf("%lld\n", sum);
  FOR(i, 1, n) printf("%d ", ans[i]);
  return 0;
}
