#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
namespace IO {
  char nc() {
    static char bf[100000], *p1 = bf, *p2 = bf;
    return p1 == p2 && (p2 = (p1 = bf) + fread(bf, 1, 100000, stdin), p1 == p2)
               ? EOF
               : *p1++;
  }
  int rd() {
    int res = 0;
    char c = getchar();
    while (!isdigit(c)) c = getchar();
    while (isdigit(c)) res = res * 10 + c - '0', c = getchar();
    return res;
  }
} // namespace IO
/******************heading******************/
const int N = 2e5 + 5, M = 1e6 + 6, P = 998244353, X = 1e6;

int n, maxa;
int c[M];

bool bp[M];
int pn[N], lp;
int mu[M], ans;

int pw(int a, int m, int p) {
  int res = 1;
  while (m) m & 1 ? res = 1ll * res * a % p : 0, a = 1ll * a * a % p, m >>= 1;
  return res;
}
void sieve(int k) {
  bp[0] = bp[1] = 1, mu[1] = 1;
  FOR(i, 2, k) {
    if (!bp[i]) pn[++lp] = i, mu[i] = -1;
    FOR(j, 1, lp) {
      int pj = pn[j];
      if (i * pj > k) break;
      bp[i * pj] = 1;
      if (i % pj == 0) {
        mu[i * pj] = 0;
        break;
      }
      mu[i * pj] = -mu[i];
    }
  }
}
int F(int d) {
  int lim = maxa / d;
  int s = 0, ss = 0;
  FOR(i, 1, lim) {
    int lj = c[i * d], x = i * d;
    FOR(j, 1, lj) {
      ss = (ss + 1ll * s * x) % P;
      s = (s + x) % P;
    }
  }
  return ss;
}
int f(int d) {
  int res = 0, lim = maxa / d;
  FOR(i, 1, lim) { res = (res + 1ll * mu[i] * F(i * d)) % P; }
  res = (res + P) % P;
  return res;
}
void calc(int d) {
  int fd = f(d);
  ans = (ans + 1ll * fd * pw(d, P - 2, P)) % P;
}
int main() {
  scanf("%d", &n);
  sieve(X);
  FOR(i, 1, n) {
    int x;
    scanf("%d", &x);
    c[x]++;
    maxa = max(maxa, x);
  }
  FOR(d, 1, maxa) calc(d);
  printf("%d\n", ans);
  return 0;
}
/*
 * BUG#1: 取模的时侯写成了+=()%P
 */
