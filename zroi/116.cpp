#include <bits/stdc++.h>
const int N = 2e5 + 5;

int n, m;
std::vector<int> g[N];
int vis[N], totvis;
int d[N], ans;

int dfs(int u, int p = 0) {
  vis[u] = ++totvis;
  int visp = 0;
  for (int v : g[u]) {
    if (u == v)
      d[u] ^= 1;
    else if (vis[v]) {
      if (vis[v] > vis[u]) continue;
      if (visp || v != p)
        d[v] ^= 1;
      else
        visp = 1;
    } else
      d[u] ^= dfs(v, u);
  }
  if (d[u])
    return 0;
  else
    return 1;
}
int main() {
  scanf("%d%d", &n, &m);
  for (int i = 1; i <= n; i++) {
    int a, b;
    scanf("%d%d", &a, &b);
    g[a].push_back(b);
    if (a != b) g[b].push_back(a);
  }
  for (int i = 1; i <= n; i++)
    if (!vis[i]) ans += !dfs(i);
  printf("%d\n", ans);
  return 0;
}
