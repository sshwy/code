#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 10;
typedef pair<pii, pii> ppp;

int n;
int a[N][N], t[N][N], use[N * N], vis[N * N], lp;

int tr(int x, int y) { return x * n + y; }
pii p[N * N];
ppp pp[N * N];
void updlim(int x, int y) {
  int cur;
  FOR(i, 1, y) cur = tr(x, y - i), p[cur].se = min(p[cur].se, a[x][y] - i);
  FOR(i, 1, n - 1 - y)
  cur = tr(x, y + i), p[cur].fi = max(p[cur].fi, a[x][y] + i);
}
bool cmp(ppp x, ppp y) {
  if (x.fi.se != y.fi.se)
    return x.fi.se < y.fi.se;
  else
    return x < y;
}
bool chk() {
  lp = 0;
  FOR(i, 0, n - 1) FOR(j, 0, n - 1) p[tr(i, j)] = mk(1, n * n);
  FOR(i, 0, n - 1) FOR(j, 0, n - 1) if (a[i][j]) updlim(i, j);
  FOR(i, 0, n - 1)
  FOR(j, 0, n - 1)
  if (a[i][j] && (a[i][j] < p[tr(i, j)].fi || a[i][j] > p[tr(i, j)].se)) return 0;
  FOR(i, 0, n - 1)
  FOR(j, 0, n - 1) if (!a[i][j]) pp[++lp] = mk(p[tr(i, j)], mk(i, j));
  FOR(i, 1, n * n) vis[i] = 0;
  FOR(i, 0, n - 1) FOR(j, 0, n - 1) if (a[i][j]) vis[a[i][j]] = 1;
  sort(pp + 1, pp + lp + 1, cmp);
  FOR(i, 0, n - 1) FOR(j, 0, n - 1) t[i][j] = a[i][j];
  FOR(i, 1, lp) {
    int fl = 0;
    FOR(pos, 1, n * n)
    if (!vis[pos] && pp[i].fi.fi <= pos && pos <= pp[i].fi.se) {
      fl = 1, t[pp[i].se.fi][pp[i].se.se] = pos, vis[pos] = 1;
      break;
    }
    if (!fl) return 0;
  }
  return 1;
}
int main() {
  scanf("%d", &n);
  FOR(i, 0, n - 1) FOR(j, 0, n - 1) scanf("%d", &a[i][j]);
  FOR(i, 0, n - 1) FOR(j, 0, n - 1) if (!a[i][j]);
  else if (use[a[i][j]]) return puts("-1"), 0;
  else use[a[i][j]] = 1;
  if (!chk()) return puts("-1"), 0;
  FOR(i, 0, n - 1) FOR(j, 0, n - 1) if (!a[i][j]) {
    int fl = 0;
    FOR(x, 1, n * n) if (!use[x]) {
      a[i][j] = x;
      if (chk()) {
        use[x] = 1, fl = 1;
        break;
      }
    }
    if (!fl) return puts("-1"), 0;
  }
  FOR(i, 0, n - 1) FOR(j, 0, n - 1) printf("%d%c", a[i][j], " \n"[j == n - 1]);
  return 0;
}
