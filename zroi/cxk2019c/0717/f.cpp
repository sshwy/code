#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i) /*}}}*/
/******************heading******************/

int a[10], b[10];

int main() {
  FOR(i, 1, 7) scanf("%d", &a[i]);
  FOR(i, 1, 7) scanf("%d", &b[i]);
  int tot = 0, t2 = 0;
  FOR(i, 1, 5) {
    FOR(j, 1, 5) {
      if (a[i] == b[j]) { ++tot; }
    }
  }
  FOR(i, 6, 7) FOR(j, 6, 7) {
    if (a[i] == b[j]) ++t2;
  }
  int c[10][10] = {{0}};
  c[5][2] = 5000000;
  c[5][1] = 250000;
  c[4][2] = c[5][0] = 3000;
  c[4][1] = c[3][2] = 200;
  c[4][0] = c[3][1] = c[2][2] = 10;
  c[3][0] = c[1][2] = c[2][1] = c[0][2] = 5;
  printf("%d\n", c[tot][t2]);
  return 0;
}
