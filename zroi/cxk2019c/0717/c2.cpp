#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i) /*}}}*/
/******************heading******************/
const int L = 2e5 + 5;
char s[L];
int l, p;
lld ans;

int main() {
  scanf("%s", s + 1);
  l = strlen(s + 1);
  FOR(i, 1, l) if (s[i] == '*') {
    p = i;
    break;
  }
  int low = 0, pre = 0, suc = 0, L = p, R = p, c1 = 0, c2 = 0;
  while (L > 1 && R < l) {
    // printf("low:%d,pre:%d,suc:%d,L:%d,R:%d\n",low,pre,suc,L,R);
    // if(pre==suc&&s[L-1]=='('&&s[R+1]==')')++ans;
    // printf("c1:%d,c2:%d\n",c1,c2);
    c1 = c2 = 0;
    while (pre >= low) {
      if (pre == low && s[R + 1] == ')') ++c1;
      ++R;
      if (R == l) break;
      if (s[R] == '(')
        ++pre;
      else
        --pre;
    }
    while (suc >= low) {
      if (suc == low && s[L - 1] == '(') ++c2;
      --L;
      if (L == 1) break;
      if (s[L] == ')')
        ++suc;
      else
        --suc;
    }
    ans += c1 * c2;
    --low;
  }
  printf("%lld", ans);
  return 0;
}
