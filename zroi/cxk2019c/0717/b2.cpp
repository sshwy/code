#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i) /*}}}*/
/******************heading******************/

using namespace std;
const int INF = 1e9;
const int w[4][2] = {{1, 0}, {-1, 0}, {0, 1}, {0, -1}};
struct node {
  int x, y, dis;
  bool operator<(const node &y) const { return dis > y.dis; }
} t;
priority_queue<node> pq;
int n, m, x, y, mp[2010][2010], dis[2010][2010], vis[2010][2010], tx, ty;
void Dijkstra() {
  FOR(i, 1, n) FOR(j, 1, m) dis[i][j] = INF;
  dis[x][y] = 0, pq.push((node){x, y, 0});
  while (!pq.empty()) {
    t = pq.top();
    pq.pop();
    if (dis[t.x][t.y] < t.dis) continue;
    vis[t.x][t.y] = 1;
    for (int i = 0; i < 4; ++i) {
      tx = t.x + w[i][0], ty = t.y + w[i][1];
      if (dis[tx][ty] > dis[t.x][t.y]) {
        dis[tx][ty] = dis[t.x][t.y] + (mp[tx][ty] != mp[t.x][t.y]);
        pq.push((node){tx, ty, dis[tx][ty]});
      }
    }
  }
}
int main() {
  scanf("%d%d%d%d", &n, &m, &x, &y);
  FOR(i, 1, n) FOR(j, 1, m) scanf("%d", &mp[i][j]);
  Dijkstra();
  FOR(i, 1, n) FOR(j, 1, m) printf("%d%c", dis[i][j], " \n"[j == m]);
  return 0;
}
