#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i) /*}}}*/
/******************heading******************/
const int L = 2e5 + 5;
char s[L];
int l;
lld ans;

int main() {
  scanf("%s", s + 1);
  l = strlen(s + 1);
  FOR(i, 1, l) {
    if (s[i] == '*') break;
    if (s[i] != '(') continue;
    int pre = 0, fl = 0;
    FOR(j, i + 1, l) {
      if (s[j] == '*') fl = 1;
      if (s[j] == '(') ++pre;
      if (s[j] == ')') --pre;
      if (pre < 0) break;
      if (pre == 0 && j < l && s[j + 1] == ')') ans += fl;
    }
  }
  printf("%lld", ans);
  return 0;
}
