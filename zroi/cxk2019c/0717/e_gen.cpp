#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i) /*}}}*/
/******************heading******************/
int w, h, n, m;

int r(int p) { return 1ll * rand() * rand() % p; }
int r(int L, int R) { return r(R - L + 1) + L; }

int main() {
  srand(time(0));
  w = r(5, 100), h = r(5, 100), n = r(3, 50), m = r(3, 50);
  int a[2000], b[2000];
  FOR(i, 1, n) a[i] = r(0, w + h);
  FOR(i, 1, m) b[i] = r(0 - h, w);
  sort(a + 1, a + n + 1), sort(b + 1, b + m + 1);
  FOR(i, 2, n) a[i] = max(a[i], a[i - 1] + 1);
  FOR(i, 2, m) b[i] = max(b[i], b[i - 1] + 1);
  printf("%d %d %d %d\n", w, h, n, m);
  FOR(i, 1, n) printf("%d ", a[i]);
  puts("");
  FOR(i, 1, m) printf("%d ", b[i]);
  puts("");
  return 0;
}
