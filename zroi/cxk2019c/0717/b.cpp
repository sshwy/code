#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i) /*}}}*/
/******************heading******************/
const int INF = 0x3f3f3f3f;
int n, m, x, y;
int a[2005][2005], c[2005][2005];
bool vis[2005][2005];

typedef pair<int, int> ppi;

priority_queue<ppi, vector<ppi>, greater<pii>> q;
void calc(int sx, int sy) {
  FOR(i, 1, n) FOR(j, 1, m) c[i][j] = INF;
  c[sx][sy] = 0;
  q.push(mk(0, sx * m + sy - 1));
  while (!q.empty()) {
    pii u = q.top();
    q.pop();
    const int cx = u.se / m, cy = u.se % m + 1, cc = u.fi;
    // cc=min(cc,c[cx][cy]);
    // printf("u:%d,(%d,%d)\n",cc,cx,cy);
    // printf("%d<%d\n",cy,m);
    vis[cx][cy] = 0;
    if (cx > 1) {
      int t = cc + (a[cx][cy] != a[cx - 1][cy]);
      if (c[cx - 1][cy] > t) {
        c[cx - 1][cy] = t;
        if (!vis[cx - 1][cy])
          q.push(mk(t, (cx - 1) * m + cy - 1)), vis[cx - 1][cy] = 1;
      }
    }
    if (cx < n) {
      int t = cc + (a[cx][cy] != a[cx + 1][cy]);
      if (c[cx + 1][cy] > t) {
        c[cx + 1][cy] = t;
        if (!vis[cx + 1][cy])
          q.push(mk(t, (cx + 1) * m + cy - 1)), vis[cx + 1][cy] = 1;
      }
    }
    if (cy > 1) {
      int t = cc + (a[cx][cy] != a[cx][cy - 1]);
      if (c[cx][cy - 1] > t) {
        c[cx][cy - 1] = t;
        if (!vis[cx][cy - 1]) q.push(mk(t, (cx)*m + cy - 2)), vis[cx][cy - 1] = 1;
      }
    }
    if (cy < m) {
      int t = cc + (a[cx][cy] != a[cx][cy + 1]);
      if (c[cx][cy + 1] > t) {
        c[cx][cy + 1] = t;
        if (!vis[cx][cy + 1]) q.push(mk(t, (cx)*m + cy)), vis[cx][cy + 1] = 1;
      }
    }
  }
}
int main() {
  scanf("%d%d%d%d", &n, &m, &x, &y);
  FOR(i, 1, n) FOR(j, 1, m) { scanf("%d", &a[i][j]); }
  calc(x, y);
  FOR(i, 1, n) {
    FOR(j, 1, m) printf("%d ", c[i][j]);
    puts("");
  }
  return 0;
}
