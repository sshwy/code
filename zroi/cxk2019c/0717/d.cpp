#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i) /*}}}*/
/******************heading******************/
const int N = 5e4 + 5;
int T, n, k;
int a[N];
int f[5005][5005];
int c[N], b[N], bb[N];
int d[N], ld;
int low_bound(int *arr, int l, int r, int v) {
  while (l < r) {
    int mid = (l + r + 1) >> 1;
    if (arr[mid] >= v)
      r = mid - 1;
    else
      l = mid;
  }
  return l;
}
void solve2() {
  FOR(i, 0, ld) d[i] = 0;
  ld = 0;
  int tc = 0, lb = 0, ans = 0;
  FOR(i, 1, n) c[a[i]] = 0;
  FOR(i, 1, n) {
    if (!c[a[i]])
      c[a[i]] = ++tc;
    else {
      b[++lb] = c[a[i]];
      if (a[i - 1] == a[i])
        bb[lb] = 0;
      else
        bb[lb] = 1;
    }
  }
  reverse(b + 1, b + lb + 1);
  FOR(i, 1, lb) {
    int j = low_bound(d, 0, ld, b[i]);
    if (++j > ld) {
      d[++ld] = b[i];
      ans = bb[i];
    }
    if (d[j] > b[i]) d[j] = b[i], ans = max(ans, bb[i]);
  }
  printf("%d\n", ld * 2 + ans);
}
void go() {
  scanf("%d%d", &n, &k);
  FOR(i, 1, n) scanf("%d", &a[i]);
  if (k == 1) {
    printf("%d\n", n > 0);
    return;
  }
  if (n > 5000) {
    solve2();
    return;
  }
  FOR(i, 1, n) f[i][i] = 1;
  FOR(len, 1, n - 1) {
    FOR(i, 1, n - len) {
      int j = i + len;
      f[i][j] = max(f[i][j - 1], f[i + 1][j]);
      if (a[i] == a[j]) {
        if (i + 1 == j)
          f[i][j] = 2;
        else
          f[i][j] = max(f[i][j], f[i + 1][j - 1] + 2);
      }
    }
  }
  printf("%d\n", f[1][n]);
}
int main() {
  scanf("%d", &T);
  while (T--) go();
  return 0;
}
