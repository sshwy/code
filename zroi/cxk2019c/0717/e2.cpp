#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i) /*}}}*/
/******************heading******************/
const int N = 2e5 + 5, P = 1e9 + 7;
int W, H, n, m;
int a[N], b[N];
lld ans;

bool check(int c1, int c2) { // jiaodian shifou zai juxing nei.(a,b)
  int x = (c1 + c2);
  if (x < 0 || x > W * 2) return 0;
  int y = c1 - c2;
  if (y < 0 || y > H * 2) return 0;
  return 1;
}
void calc(int line) {
  int l = 1, r = m, c1 = a[line];
  // printf("line:%d,%d\n",line,a[line]);
  while (l <= m && !check(c1, b[l])) ++l;
  if (l > m) return;
  while (r >= 1 && !check(c1, b[r])) --r;
  // printf("l:%d,r:%d\n",l,r);
  FOR(i, line + 1, n) {
    while (l <= m && !check(a[i], b[l])) ++l;
    if (l >= r) return;
    while (r >= 1 && !check(a[i], b[r])) --r;
    // printf("l:%d,r:%d\n",l,r);
    ans += 1ll * (r - l) * (r - l + 1) / 2;
    ans %= P;
  }
  // printf("ans:%lld\n",ans);
}
int main() {
  scanf("%d%d%d%d", &W, &H, &n, &m);
  FOR(i, 1, n) {
    scanf("%d", &a[i]);
    if (a[i] < 0) --i, --n;
  }
  FOR(i, 1, m) {
    scanf("%d", &b[i]);
    if (b[i] < -H || b[i] > W) --i, --m;
  }
  sort(a + 1, a + n + 1);
  sort(b + 1, b + m + 1);
  FOR(i, 1, n) calc(i);
  printf("%lld\n", ans);
  return 0;
}
