#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i) /*}}}*/
#define int long long
/******************heading******************/
const int N = 2e5 + 5, P = 1e9 + 7, SZ = 1 << 22;
int W, H, n, m, a[N], b[N];
lld ans;

bool check(int c1, int c2) { // a,b
  int x = c1 + c2;
  if (x < 0 || x > W * 2) return 0;
  int y = c1 - c2;
  if (y < 0 || y > H * 2) return 0;
  return 1;
}
pii q[N];

int s0[SZ], s1[SZ], s2[SZ], tg[SZ];
void pushup(int u) {
  s0[u] = s0[u << 1] + s0[u << 1 | 1];
  s1[u] = s1[u << 1] + s1[u << 1 | 1];
  s2[u] = s2[u << 1] + s2[u << 1 | 1];
}
void add_node(int u, int l, int r, int v) {
  // printf("\033[32madd_node(%lld,%lld,%lld,%lld)\033[0m\n",u,l,r,v);
  // printf("%lld,%lld\n",s1[u],s2[u]);
  s2[u] += (s1[u] + s1[u] + (r - l + 1ll) * (v - 1)) * v / 2 % P, s2[u] %= P;
  s1[u] += (r - l + 1) * v % P, s1[u] %= P;
  tg[u] += v, tg[u] %= P;
  // printf("%lld,%lld\n",s1[u],s2[u]);
}
void pushdown(int u, int l, int r) {
  if (!tg[u]) return;
  int mid = (l + r) >> 1;
  add_node(u << 1, l, mid, tg[u]);
  add_node(u << 1 | 1, mid + 1, r, tg[u]);
  tg[u] = 0;
}
void add(int L, int R, int v, int u = 1, int l = 1, int r = n) {
  if (L > R) return;
  // printf("add(%lld,%lld,%lld,%lld,%lld,%lld)\n",L,R,v,u,l,r);
  if (L <= l && r <= R) {
    add_node(u, l, r, v);
    return;
  }
  int mid = (l + r) >> 1;
  pushdown(u, l, r);
  if (L <= mid) add(L, R, v, u << 1, l, mid);
  if (mid < R) add(L, R, v, u << 1 | 1, mid + 1, r);
  pushup(u);
}
int query(int L, int R, int u = 1, int l = 1, int r = n) {
  if (r < L || R < l) return 0;
  if (L <= l && r <= R) return s2[u];
  int mid = (l + r) >> 1;
  pushdown(u, l, r);
  return (query(L, R, u << 1, l, mid) + query(L, R, u << 1 | 1, mid + 1, r)) % P;
}

signed main() {
  scanf("%lld%lld%lld%lld", &W, &H, &n, &m);
  FOR(i, 1, n) {
    scanf("%lld", &a[i]);
    if (a[i] < 0) --i, --n;
  }
  FOR(i, 1, m) {
    scanf("%lld", &b[i]);
    if (b[i] < -H || b[i] > W) --i, --m;
  }
  sort(a + 1, a + n + 1), sort(b + 1, b + m + 1);

  int pos = n;
  int p1 = 0, p2 = 0;
  FOR(i, 1, m) if (b[i] < 0) p1 = i;
  else break;
  p2 = p1 + 1;
  // printf("%lld,%lld\n",p1,p2);
  FOR(i, 1, n) {
    while (p1 >= 1 && (a[i] + b[p1] > W * 2 || a[i] - b[p1] < 0))
      q[p1].fi = N + 1, --p1;
    while (p2 <= m && (a[i] + b[p2] < 0 || a[i] - b[p2] > H * 2))
      q[p2].fi = N + 1, ++p2;
    while (p1 >= 1 && check(a[i], b[p1])) {
      q[p1].fi = i;
      --p1;
    }
    while (p2 <= m && check(a[i], b[p2])) {
      q[p2].fi = i;
      ++p2;
    }
  }
  ROF(i, p1, 1) q[i].fi = N + 1;
  FOR(i, p2, m) q[i].fi = N + 1;

  p1 = 0;
  FOR(i, 1, m) if (b[i] < W - H) p1 = i;
  else break;
  p2 = p1 + 1;
  ROF(i, n, 1) {
    while (p1 >= 1 && (a[i] + b[p1] > W * 2 || a[i] - b[p1] < 0))
      q[p1].se = N + 1, --p1;
    while (p2 <= m && (a[i] + b[p2] < 0 || a[i] - b[p2] > H * 2))
      q[p2].se = N + 1, ++p2;
    while (p1 >= 1 && check(a[i], b[p1])) {
      q[p1].se = i;
      --p1;
    }
    while (p2 <= m && check(a[i], b[p2])) {
      q[p2].se = i;
      ++p2;
    }
  }
  ROF(i, p1, 1) q[i].se = N + 1;
  FOR(i, p2, m) q[i].se = N + 1;

  // FOR(i,1,m)printf("(%lld,%lld) ",q[i].fi,q[i].se); puts("");
  sort(q + 1, q + m + 1);
  // FOR(i,1,m)printf("(%lld,%lld) ",q[i].fi,q[i].se); puts("");
  pos = 1;
  FOR(i, 1, n - 1) { //以i为左
    // printf("\033[31mi:%lld\033[0m\n",i);
    while (pos <= m && q[pos].fi == i) add(q[pos].fi, q[pos].se - 1, 1), ++pos;
    // puts("");
    int t = query(i, n - 1);
    // printf("query(%lld,%lld):%lld\n",i,n-1,t);
    ans = (ans + t) % P;
  }

  // FOR(i,1,n)calc(i);
  printf("%lld\n", ans);
  return 0;
}
/*
 * a:\ -> |
 * b:/ -> -
 */
