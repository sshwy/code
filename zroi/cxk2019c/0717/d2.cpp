#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i) /*}}}*/
/******************heading******************/
const int N = 1e6 + 5;
int T, n, k;
int a[N];
int d[N], ld;
int low_bound(int *arr, int l, int r, int v) {
  while (l < r) {
    int mid = (l + r + 1) >> 1;
    if (arr[mid] >= v)
      r = mid - 1;
    else
      l = mid;
  }
  return l;
}
vector<int> s[N];
pii pr[N];
void solve2() {
  int lp = 0;
  FOR(i, 0, ld) d[i] = 0;
  ld = 0;
  FOR(i, 1, n) s[i].clear();
  FOR(i, 1, n) {
    for (int j = 0; j < s[a[i]].size(); j++) pr[++lp] = mk(s[a[i]][j], i);
    s[a[i]].pb(i);
  }
  sort(pr + 1, pr + lp + 1);
  reverse(pr + 1, pr + lp + 1);
  int ans = 0;
  // FOR(i,1,lp)printf("%d,%d\n",pr[i].fi,pr[i].se);
  FOR(i, 1, lp) {
    int j = low_bound(d, 0, ld, pr[i].se);
    if (++j > ld) {
      d[++ld] = pr[i].se;
      ans = pr[i].se - pr[i].fi > 1;
    }
    if (d[j] > pr[i].se)
      d[j] = pr[i].se, ans = max(ans, (int)(pr[i].se - pr[i].fi > 1));
  }
  printf("%d\n", ld * 2 + ans);
}
void go() {
  scanf("%d%d", &n, &k);
  FOR(i, 1, n) scanf("%d", &a[i]);
  if (k == 1) {
    printf("%d\n", n > 0);
    return;
  }
  solve2();
  return;
}
int main() {
  scanf("%d", &T);
  while (T--) go();
  return 0;
}
