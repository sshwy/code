#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
namespace IO {
  char nc() {
    static char bf[100000], *p1 = bf, *p2 = bf;
    return p1 == p2 && (p2 = (p1 = bf) + fread(bf, 1, 100000, stdin), p1 == p2)
               ? EOF
               : *p1++;
  }
  int rd() {
    int res = 0;
    char c = getchar();
    while (!isdigit(c)) c = getchar();
    while (isdigit(c)) res = res * 10 + c - '0', c = getchar();
    return res;
  }
} // namespace IO
/******************heading******************/
const int S = 2e6, N = 5050, INF = 0x3f3f3f3f;
int n, m, q, k;

struct qxx {
  int nex, t, v;
};
qxx e[S];
int h[N], cnt = 1;
void add_path(int f, int t, int v) { e[++cnt] = (qxx){h[f], t, v}, h[f] = cnt; }
void add_flow(int f, int t, int v) { add_path(f, t, v), add_path(t, f, 0); }

namespace DINIC { //{{{
  int s, t, maxflow, d[N];
  bool bfs() {
    memset(d, 0, sizeof(d));
    queue<int> q;
    q.push(s), d[s] = 1;
    while (q.size()) {
      int u = q.front();
      q.pop();
      for (int i = h[u]; i; i = e[i].nex) {
        const int &v = e[i].t, &w = e[i].v;
        if (!d[v] && w) d[v] = d[u] + 1, q.push(v);
      }
    }
    return d[t];
  }
  int dinic(int u, int flow) {
    if (u == t) return flow;
    int k, rest = flow;
    for (int i = h[u]; i && rest; i = e[i].nex) {
      const int &v = e[i].t, &w = e[i].v;
      if (!w || d[v] != d[u] + 1) continue;
      k = dinic(v, min(rest, w));
      if (k)
        e[i].v -= k, e[i ^ 1].v += k, rest -= k;
      else
        d[v] = 0;
    }
    return flow - rest;
  }
  void go() {
    while (bfs())
      for (int i; i = dinic(s, INF);) maxflow += i;
  }
} // namespace DINIC

bool ex[N][N], ban[N];

int main() {
  scanf("%d%d%d%d", &n, &m, &q, &k);
  FOR(i, 1, k) {
    int x, y;
    scanf("%d%d", &x, &y);
    ex[x][y] = 1;
  }
  DINIC::s = 0, DINIC::t = n + m + 1;
  int tot = n + m;
  FOR(i, 1, n) if (!ex[i][q]) ban[i] = 1, --tot;
  FOR(i, 1, n) FOR(j, 1, m) {
    if (ex[i][j]) continue;
    if (ban[i]) continue;
    add_flow(j + n, i, 1);
  }
  FOR(i, 1, m) add_flow(DINIC::s, i + n, 1);
  FOR(i, 1, n) add_flow(i, DINIC::t, 1);
  DINIC::go();
  int ans = tot - DINIC::maxflow;
  // printf("tot=%d,maxflow=%d\n",tot,DINIC::maxflow);
  printf("%d\n", max(ans, m));
  return 0;
}
