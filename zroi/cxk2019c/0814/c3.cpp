// Dinic网络最大流模板
#include <algorithm>
#include <bitset>
#include <cassert>
#include <cctype>
#include <climits>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <functional>
#include <iostream>
#include <map>
#include <numeric>
#include <queue>
#include <set>
#include <stack>
#include <string>
#include <vector>
using namespace std;
#define rep(i, a, b) for (int i = (a); i <= (b); i++)
#define per(i, a, b) for (int i = (a); i >= (b); i--)
#define loop(it, v) for (auto it = v.begin(); it != v.end(); it++)
#define cont(i, x) for (int i = head[x]; i; i = e[i].nxt)
#define clr(a) memset(a, 0, sizeof(a))
#define ass(a, cnt) memset(a, cnt, sizeof(a))
#define lowbit(x) (x & -x)
#define all(x) x.begin(), x.end()
#define SC(t, x) static_cast<t>(x)
#define ub upper_bound
#define lb lower_bound
#define pqueue priority_queue
#define mp make_pair
#define pb push_back
#define pof pop_front
#define pob pop_back
#define fi first
#define se second
#define y1 y1_
#define Pi acos(-1.0)
#define iv inline void
#define enter cout << endl
#define siz(x) ((int)x.size())
#define file(x) freopen(x ".in", "r", stdin), freopen(x ".out", "w", stdout)
// #define int long long
typedef double db;
typedef long long ll;
typedef unsigned long long ull;
typedef pair<int, int> pii;
typedef vector<int> vi;
typedef vector<pii> vii;
typedef queue<int> qi;
typedef queue<pii> qii;
typedef set<int> si;
typedef map<int, int> mii;
typedef map<string, int> msi;
const int maxn = 1e3 + 100;
const int maxm = 1e6 + 100;
const int inf = 0x3f3f3f3f;
const int iinf = 1 << 30;
const ll linf = 2e18;
const int mod = 998244353;
const double eps = 1e-7;
template <class T> void read(T &a) {
  int f = 1;
  a = 0;
  char ch = getchar();
  while (!isdigit(ch)) {
    if (ch == '-') f = -1;
    ch = getchar();
  }
  while (isdigit(ch)) {
    a = (a << 3) + (a << 1) + ch - '0';
    ch = getchar();
  }
  a *= f;
}

struct qxx {
  int nex, t, v;
};

const int S = 2e6 + 5;
const int N = 2e3 + 5;

int n, m, q, k, sum;

int E[N][N];

qxx e[S];
int h[N], cnt = 1;
void add_path(int f, int t, int v) { e[++cnt] = (qxx){h[f], t, v}, h[f] = cnt; }
void add_flow(int f, int t, int v) { add_path(f, t, v), add_path(t, f, 0); }

namespace DINIC { //{{{
  int s, t, maxflow, d[N];
  bool bfs() {
    memset(d, 0, sizeof(d));
    queue<int> q;
    q.push(s), d[s] = 1;
    while (q.size()) {
      int u = q.front();
      q.pop();
      for (int i = h[u]; i; i = e[i].nex) {
        const int &v = e[i].t, &w = e[i].v;
        if (!d[v] && w) d[v] = d[u] + 1, q.push(v);
      }
    }
    return d[t];
  }
  int dinic(int u, int flow) {
    if (u == t) return flow;
    int k, rest = flow;
    for (int i = h[u]; i && rest; i = e[i].nex) {
      const int &v = e[i].t, &w = e[i].v;
      if (!w || d[v] != d[u] + 1) continue;
      k = dinic(v, min(rest, w));
      if (k)
        e[i].v -= k, e[i ^ 1].v += k, rest -= k;
      else
        d[v] = 0;
    }
    return flow - rest;
  }
  void go() {
    while (bfs())
      for (int i; i = dinic(s, inf);) maxflow += i;
  }
} // namespace DINIC

int main() {
  scanf("%d %d %d", &n, &m, &q);
  DINIC ::s = 0, DINIC ::t = n + m + 1;
  sum = n + m;
  scanf("%d", &k);
  int u, v;
  rep(i, 1, k) {
    scanf("%d %d", &u, &v);
    E[u][v] = 1;
  }
  rep(i, 1, n) {
    if (!E[i][q]) {
      sum--;
      continue;
    }
    add_flow(0, i, 1);
    // printf("%d %d\n", s, i);
    rep(j, 1, m) if (!E[i][j]) add_flow(i, n + j, inf);
  }
  rep(i, 1, m) add_flow(i + n, n + m + 1, 1);
  DINIC::go();
  printf("%d\n", sum - DINIC::maxflow);
  return 0;
}
