#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
namespace IO {
  char nc() {
    static char bf[100000], *p1 = bf, *p2 = bf;
    return p1 == p2 && (p2 = (p1 = bf) + fread(bf, 1, 100000, stdin), p1 == p2)
               ? EOF
               : *p1++;
  }
  int rd() {
    int res = 0;
    char c = getchar();
    while (!isdigit(c)) c = getchar();
    while (isdigit(c)) res = res * 10 + c - '0', c = getchar();
    return res;
  }
} // namespace IO
/******************heading******************/
#define int long long
const int N = 1e3 + 5, P = 998244353;
int t;
int fac[N], a[N];
void go() {
  int n;
  scanf("%lld", &n);
  printf("%lld\n", a[n]);
}
signed main() {
  scanf("%lld", &t);
  fac[0] = 1;
  FOR(i, 1, 1000) { fac[i] = 1ll * fac[i - 1] * i % P; }
  a[0] = 0;
  FOR(i, 1, 1000) {
    a[i] = fac[i];
    FOR(k, 1, i - 1) a[i] = (a[i] - fac[k] * a[i - k]) % P;
    a[i] = (a[i] % P + P) % P;
  }
  while (t--) go();
  return 0;
}
