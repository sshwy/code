#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
namespace IO {
  char nc() {
    static char bf[100000], *p1 = bf, *p2 = bf;
    return p1 == p2 && (p2 = (p1 = bf) + fread(bf, 1, 100000, stdin), p1 == p2)
               ? EOF
               : *p1++;
  }
  int rd() {
    int res = 0;
    char c = getchar();
    while (!isdigit(c)) c = getchar();
    while (isdigit(c)) res = res * 10 + c - '0', c = getchar();
    return res;
  }
} // namespace IO
/******************heading******************/

int n, m, q, k;
int s[20];

int main() {
  scanf("%d%d%d%d", &n, &m, &q, &k);
  FOR(i, 1, k) {
    int x, y;
    scanf("%d%d", &x, &y);
    s[y] |= (1 << x);
  }
  int lim = 1 << m, ans = m;
  FOR(i, 0, lim - 1) {
    if ((i >> q - 1) & 1) {
      // printf("i=%d\n",i);
      int tot = s[q];
      FOR(j, 1, m) {
        if ((i >> j - 1) & 1) { tot &= s[j]; }
      }
      // printf("tot=%d\n",tot);
      ans = max(ans, __builtin_popcount(i) + __builtin_popcount(tot));
    }
  }
  printf("%d\n", ans);
  return 0;
}
