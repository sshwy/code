#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
namespace IO {
  char nc() {
    static char bf[100000], *p1 = bf, *p2 = bf;
    return p1 == p2 && (p2 = (p1 = bf) + fread(bf, 1, 100000, stdin), p1 == p2)
               ? EOF
               : *p1++;
  }
  int rd() {
    int res = 0;
    char c = getchar();
    while (!isdigit(c)) c = getchar();
    while (isdigit(c)) res = res * 10 + c - '0', c = getchar();
    return res;
  }
} // namespace IO
/******************heading******************/
const int N = 505, P = 998244353;
int n, a[N];
lld f[N][N];
lld c[N][N];
lld binom(int n, int m) {
  if (n < m) return 0;
  if (c[n][m]) return c[n][m];
  if (m == 0 || m == n) return 1;
  return c[n][m] = (binom(n - 1, m - 1) + binom(n - 1, m)) % P;
}
int main() {
  scanf("%d", &n);
  FOR(i, 1, n) scanf("%d", &a[i]);
  // FOR(i,1,n)printf("%d ",a[i]); puts("");
  FOR(i, 2, n) if (a[i - 1] == a[i]) return puts("0"), 0;
  a[n + 1] = -1;
  FOR(i, 1, n) f[i][i] = a[i - 1] != a[i + 1];
  FOR(len, 1, n - 1) {
    FOR(i, 1, n - len) {
      int j = i + len;
      if (a[i - 1] == a[j + 1]) continue;
      f[i][j] = (f[i + 1][j] + f[i][j - 1]) % P;
      FOR(k, i + 1, j - 1) {
        if (a[k] != a[i - 1] && a[k] != a[j + 1])
          f[i][j] += f[i][k - 1] * f[k + 1][j] % P * binom(j - i, k - i) % P;
        f[i][j] %= P;
      }
      // printf("f[%d,%d]=%d\n",i,j,f[i][j]);
    }
  }
  printf("%lld\n", f[1][n]);
  return 0;
}
