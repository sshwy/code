#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
namespace IO {
  char nc() {
    static char bf[100000], *p1 = bf, *p2 = bf;
    return p1 == p2 && (p2 = (p1 = bf) + fread(bf, 1, 100000, stdin), p1 == p2)
               ? EOF
               : *p1++;
  }
  int rd() {
    int res = 0;
    char c = getchar();
    while (!isdigit(c)) c = getchar();
    while (isdigit(c)) res = res * 10 + c - '0', c = getchar();
    return res;
  }
} // namespace IO
/******************heading******************/
const int N = 1e6 + 5;
int n, w[N];

struct data {
  int a, b;
} d[N];
bool cmp(data x, data y) { return max(w[x.a], w[x.b]) < max(w[y.a], w[y.b]); }
int ld;

int f[N], sz[N], tot;
int ch[N][2];
lld c[N];
lld ans[N];
int get(int u) { return f[u] == u ? u : f[u] = get(f[u]); }
void dfs(int u) {
  if (ch[u][0]) c[ch[u][0]] += c[u], dfs(ch[u][0]);
  if (ch[u][1]) c[ch[u][1]] += c[u], dfs(ch[u][1]);
}
int main() {
  scanf("%d", &n);
  FOR(i, 2, n) {
    int x;
    scanf("%d", &x), d[++ld] = (data){x, i};
  }
  FOR(i, 1, n) { scanf("%d", &w[i]); }
  sort(d + 1, d + ld + 1, cmp);
  FOR(i, 0, n) f[i] = i, sz[i] = 1;
  FOR(i, n + 1, n * 2) f[i] = i;
  tot = n;
  FOR(i, 1, ld) {
    data cur = d[i]; //合并cur.a,cur.b
    int a = get(cur.a), b = get(cur.b), v = max(w[cur.a], w[cur.b]);
    c[a] += 1ll * sz[b] * v;
    c[b] += 1ll * sz[a] * v;
    ++tot, f[tot] = tot;
    f[a] = f[b] = tot, sz[tot] = sz[a] + sz[b];
    ch[tot][0] = a, ch[tot][1] = b;
  }
  dfs(get(tot));
  FOR(i, 1, n) printf("%lld ", c[i] + w[i]);
  return 0;
}
