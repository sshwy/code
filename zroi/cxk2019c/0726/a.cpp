#include <bits/stdc++.h>
using namespace std;
typedef long long lld;
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
char nc() {
  static char bf[100000], *p1 = bf, *p2 = bf;
  return p1 == p2 && (p2 = (p1 = bf) + fread(bf, 1, 100000, stdin), p1 == p2) ? EOF
                                                                              : *p1++;
}
int rd() {
  int res = 0;
  char c = nc();
  while (!isdigit(c)) c = nc();
  while (isdigit(c)) res = res * 10 + c - '0', c = nc();
  return res;
}
const int N = 2e6 + 6, M = 2e6 + 6;
int n, m, p[N], c[N], f[N], d[N];
lld ans;
int get(int u) { return f[u] == u ? u : f[u] = get(f[u]); }
inline void color(int u, int v, int col) {
  u = get(u), v = get(v);
  while (u != v) {
    if (d[u] < d[v]) swap(u, v);
    c[u] = col, ans += col;
    u = f[u] = get(p[u]);
  }
}
int main() {
  n = rd(), m = rd();
  p[1] = 1, d[1] = 1;
  FOR(i, 2, n) p[i] = rd(), d[i] = d[p[i]] + 1;
  FOR(i, 0, n) f[i] = i;
  int x, y, x1, y1;
  x = rd(), y = rd();
  FOR(i, 1, m) {
    color(x, y, i);
    x1 = (x * 114514ll + y * 1919810ll) % n + 1;
    y1 = (x * 415411ll + y * 8101919ll) % n + 1;
    x = x1, y = y1;
  }
  printf("%lld", ans);
  return 0;
}
