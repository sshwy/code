#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
namespace IO {
  char nc() {
    static char bf[100000], *p1 = bf, *p2 = bf;
    return p1 == p2 && (p2 = (p1 = bf) + fread(bf, 1, 100000, stdin), p1 == p2)
               ? EOF
               : *p1++;
  }
  int rd() {
    int res = 0;
    char c = getchar();
    while (!isdigit(c)) c = getchar();
    while (isdigit(c)) res = res * 10 + c - '0', c = getchar();
    return res;
  }
} // namespace IO
/******************heading******************/
const int N = 4e5 + 5;
int n, w[N], dg[N];
/*{{{*/
struct qxx {
  int nex, t;
};
qxx e[N];
int h[N], le;
void add_path(int f, int t) { e[++le] = (qxx){h[f], t}, h[f] = le; }

namespace S1 {
  int dep[N];
  bool vis[N], mark[N];
  pii pw[N];
  void dfs(int u, int d) {
    vis[u] = 1, dep[u] = d;
    for (int i = h[u]; i; i = e[i].nex) {
      if (vis[e[i].t]) continue;
      dfs(e[i].t, d + 1);
    }
  }
  int dfs_calc(int u) {
    if (mark[u]) return 0;
    int res = 1;
    mark[u] = 1;
    for (int i = h[u]; i; i = e[i].nex) {
      const int v = e[i].t;
      if (mark[v] || dep[v] < dep[u]) continue;
      res += dfs_calc(v);
    }
    return res;
  }
  lld calc(int u) {
    memset(vis, 0, sizeof(bool) * (n + 5));
    memset(mark, 0, sizeof(bool) * (n + 5));
    lld ans = 0;
    dfs(u, 1);
    ROF(i, n, 1) {
      int cur = pw[i].se;
      int t = dfs_calc(cur);
      ans += 1ll * w[cur] * t;
    }
    return ans;
  }
  void go() {
    sort(pw + 1, pw + n + 1);
    FOR(i, 1, n) { printf("%lld ", calc(i)); }
  }
} // namespace S1
/*}}}*/
namespace S2 {
  int pl[N], pr[N];
  lld sl[N], sr[N];
  void go() {
    pl[1] = 0;
    FOR(i, 2, n) {
      int pos = i - 1;
      while (pos && w[pos] <= w[i]) pos = pl[pos];
      if (pos && w[pos] > w[i])
        pl[i] = pos;
      else
        pl[i] = 0;
    }
    pr[n] = 0;
    ROF(i, n - 1, 1) {
      int pos = i + 1;
      while (pos && w[pos] <= w[i]) pos = pr[pos];
      if (pos && w[pos] > w[i])
        pr[i] = pos;
      else
        pr[i] = 0;
    }
    sl[1] = w[1];
    FOR(i, 2, n) {
      if (pl[i] == i)
        sl[i] = 1ll * w[i] * i;
      else
        sl[i] = sl[pl[i]] + 1ll * w[i] * (i - pl[i]);
    }
    sr[n] = w[n];
    ROF(i, n - 1, 1) {
      if (pr[i] == i)
        sr[i] = 1ll * w[i] * (n - i + 1);
      else
        sr[i] = sr[pr[i]] + 1ll * w[i] * (pr[i] - i);
    }
    FOR(i, 1, n) { printf("%lld ", sl[i] + sr[i] - w[i]); }
  }
} // namespace S2
int main() {
  bool is2 = 1;
  scanf("%d", &n);
  FOR(i, 2, n) {
    int x;
    scanf("%d", &x);
    add_path(x, i), add_path(i, x);
    dg[x]++, dg[i]++;
    if (dg[x] > 2 || dg[i] > 2) is2 = 0;
    if (x != i - 1) is2 = 0;
  }
  FOR(i, 1, n) {
    scanf("%d", &w[i]);
    S1::pw[i] = mk(w[i], i);
  }
  S1::go();
  return 0;
}
