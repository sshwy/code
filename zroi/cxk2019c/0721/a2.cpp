#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i) /*}}}*/
/******************heading******************/
lld gcd(lld a, lld b) { return b ? gcd(b, a % b) : a; }
lld mul(lld a, lld b, lld p) {
  a %= p, b %= p;
  if (a <= 1000000000 && b <= 1000000000) return a * b % p;
  lld c = (long double)a * b / p, res = a * b - c * p;
  res < 0 ? res += p : (res >= p ? res -= p : 0);
  return res;
}
lld pw(lld a, lld m, lld p) {
  lld res = 1;
  while (m) m & 1 ? res = mul(res, a, p) : 0, a = mul(a, a, p), m >>= 1;
  return res;
}
lld exgcd(lld a, lld b, lld &x, lld &y) {
  if (!b) return x = 1, y = 0, a;
  lld t = exgcd(b, a % b, y, x);
  return y = y - (a / b) * x, t;
}
lld ind(lld g, lld a, lld p) { // ind_g a mod p
  a %= p;
  lld x = 1;
  FOR(i, 0, p - 1) {
    if (x == a) return i;
    x = x * g % p;
  }
  return -1;
}
lld ind(lld g, lld a, lld p, lld e) { // ind_g a mod p^e
  lld x0 = ind(g, a, p);
  if (x0 == -1) return -1;
  lld t = p - 1, pi = p;
  FOR(i, 1, e - 1) {
    pi *= p;
    lld g0 = pw(g, x0, pi), gt = pw(g, t, pi), gj = 1, fl = 0;
    FOR(j, 0, p - 1) {
      if (mul(g0, gj, pi) == a % pi) {
        fl = 1, x0 = (x0 + mul(j, t, t * p)) % (t * p);
        break;
      }
      gj = mul(gj, gt, pi);
    }
    if (!fl) return -1;
    t *= p;
  }
  return x0;
}
lld t, a, b, p, e, g[100];
lld go() {
  scanf("%lld%lld%lld%lld", &a, &b, &p, &e);
  a = ind(g[p], a, p, e), b = ind(g[p], b, p, e);
  if (a == -1 || b == -1) return puts("-1");
  lld pp = p - 1;
  FOR(i, 1, e - 1) pp *= p;
  lld x, y, gc = exgcd(a, pp, x, y);
  if (b % gc) return puts("-1");
  x = mul(x, (b / gc), pp), pp /= gc, x = (x % pp + pp) % pp;
  printf("%lld\n", x);
  return 0;
}
int main() {
  g[3] = 2, g[5] = 2, g[7] = 3, g[11] = 2, g[13] = 2, g[17] = 3, g[19] = 2, g[23] = 5,
  g[29] = 2, g[31] = 3, g[37] = 2, g[41] = 6, g[43] = 3, g[47] = 5;
  scanf("%lld", &t);
  while (t--) go();
  return 0;
}
