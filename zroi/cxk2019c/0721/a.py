from fractions import gcd

tt=0

def findg(g,a,p,e):
	z,pz,y=p,1,0
	for i in xrange(e):
		base=pow(g,y,z)
		st=pow(g,pz,z)
		zz=a%z
		assert (base-a)%(z/p)==0
		while base!=zz:
			base=base*st%z
			y+=pz
		z=z*p
		pz=p-1 if pz==1 else pz*p
	assert pow(g,y,p**e)==a;
	return y

vis=[0]*1010

def solve():
	a,b,p,e=map(int,raw_input().split())
	g=2
	while True:
		global tt
		tt+=1
		valid,z=1,1
		for i in xrange(p-1):
			if vis[z]==tt:
				valid=0
				break
			vis[z]=tt
			z=z*g%p
		assert z==1
		if valid: break
		g+=1
	fa=findg(g,a,p,e)
	fb=findg(g,b,p,e)
        print fa
        print fb
	pp=(p-1)*p**(e-1)
        print pp
	d=gcd(fa,pp)
	if fb%d!=0: return -1
	fa,fb,pp=fa/d,fb/d,pp/d
	phip,xx=pp,pp
	for i in xrange(2,p+1):
		if xx%i==0:
			phip=phip/i*(i-1)
			while xx%i==0:
				xx/=i
	assert xx==1
	fc=fb*pow(fa,phip-1,pp)%pp
	assert(pow(a,fc,p**e)==b)
	return fc

T=input()
for tc in xrange(T):
	print solve()
