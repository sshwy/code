#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i) /*}}}*/
/******************heading******************/
const int N = 5e3 + 5, P = 1e9 + 7;
int n, q;
int pw(int a, int m) {
  lld res = 1;
  while (m) m & 1 ? res = 1ll * res * a % P : 0, a = 1ll * a * a % P, m >>= 1;
  return res;
}
int fac[N], fnv[N], f[N];
int p[N];
bool vis[N];
int dfs(int k) {
  if (k > n) return 1;
  if (p[k]) return dfs(k + 1);
  int res = 0;
  FOR(i, 1, n) {
    if (vis[i] || i == k) continue;
    vis[i] = 1;
    res += dfs(k + 1);
    vis[i] = 0;
  }
  return res;
}
int main() {
  scanf("%d%d", &n, &q);
  fac[0] = 1;
  FOR(i, 1, n) fac[i] = 1ll * fac[i - 1] * i % P;
  fnv[n] = pw(fac[n], P - 2);
  ROF(i, n, 1) fnv[i - 1] = 1ll * fnv[i] * i % P;
  f[0] = 1;
  FOR(i, 1, n) f[i] = (f[i - 1] + 1ll * fnv[i] * (P + 1ll - 2 * (i & 1))) % P;
  printf("%lld\n", 1ll * f[n] * fac[n] % P);
  FOR(i, 1, q) {
    int u, v;
    scanf("%d%d", &u, &v);
    p[u] = v, vis[v] = 1;
    printf("%d\n", dfs(1));
  }
  return 0;
}
