#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (lld i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (lld i = (a); i >= (b); --i) /*}}}*/
/******************heading******************/
const int K = 5e6 + 5;
lld a, b, k;
lld exgcd(lld a, lld b, lld &x, lld &y) {
  if (!b) return x = 1, y = 0, a;
  lld t = exgcd(b, a % b, y, x);
  return y = y - (a / b) * x, t;
}
lld ans = 0;
int main() {
  scanf("%lld%lld%lld", &a, &b, &k);
  if (k == 1) return printf("%lld\n", a * b - a - b), 0;
  // a=7,b=9;
  lld x, y;
  exgcd(a, b, x, y);
  // printf("x=%d,y=%d\n",x,y);
  ROF(c, a * b - a - b, 1) {
    if (!k) break;
    lld xx = x * c, yy = y * c;
    // printf("c=%d,xx=%d,yy=%d\n",c,xx,yy);
    lld t = min(abs(xx / b), abs(yy / a));
    if (x < 0)
      xx += t * b, yy -= t * a;
    else
      xx -= t * b, yy += t * a;
    if (xx >= b) yy += xx / b * a, xx %= b;
    // while(xx>=b)xx-=b,yy+=a;
    if (yy < 0) ans ^= c, --k;
    // printf("c=%d,xx=%d,yy=%d\n",c,xx,yy);
    // printf("%d",xx+yy);
  }
  printf("%lld\n", ans);
  return 0;
}
