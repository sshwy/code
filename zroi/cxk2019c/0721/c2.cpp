#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
namespace IO {
  char nc() {
    static char bf[100000], *p1 = bf, *p2 = bf;
    return p1 == p2 && (p2 = (p1 = bf) + fread(bf, 1, 100000, stdin), p1 == p2)
               ? EOF
               : *p1++;
  }
  int rd() {
    int res = 0;
    char c = getchar();
    while (!isdigit(c)) c = getchar();
    while (isdigit(c)) res = res * 10 + c - '0', c = getchar();
    return res;
  }
} // namespace IO
/******************heading******************/
const int N = 5e3 + 5, P = 1e9 + 7;
int pw(int a, int m) {
  lld res = 1;
  while (m) m & 1 ? res = 1ll * res * a % P : 0, a = 1ll * a * a % P, m >>= 1;
  return res;
}

int n, q;
int fac[N], fnv[N], f[N];
int p[N];
bool vis[N];
int D(int x) { return 1ll * f[x] * fac[x] % P; }
int binom(int n, int m) {
  if (n < m) return 0;
  return 1ll * fac[n] * fnv[m] % P * fnv[n - m] % P;
}
int calc(int a, int b) {
  int res = 0;
  FOR(i, 0, a) res = (res + 1ll * binom(a, i) * D(a + b - i) % P) % P;
  return res;
}
int main() {
  scanf("%d%d", &n, &q);
  fac[0] = 1;
  FOR(i, 1, n) fac[i] = 1ll * fac[i - 1] * i % P;
  fnv[n] = pw(fac[n], P - 2);
  ROF(i, n, 1) fnv[i - 1] = 1ll * fnv[i] * i % P;
  f[0] = 1;
  FOR(i, 1, n) f[i] = (f[i - 1] + 1ll * fnv[i] * (P + 1ll - 2 * (i & 1))) % P;
  printf("%d\n", D(n));

  int a = 0, b = n;
  FOR(i, 1, q) {
    int u, v;
    scanf("%d%d", &u, &v);
    if (vis[u] && p[v]) --a;
    if (vis[u] && !p[v]) --b;
    if (!vis[u] && p[v]) --b;
    if (!vis[u] && !p[v]) --b, --b, ++a;
    p[u] = v, vis[v] = 1;
    printf("%d\n", calc(a, b));
  }
  return 0;
}
