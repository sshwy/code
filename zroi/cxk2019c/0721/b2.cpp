#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i) /*}}}*/
/******************heading******************/
lld a, b, k, ans;
priority_queue<lld> q;

int main() {
  scanf("%lld%lld%lld", &a, &b, &k);
  q.push(a * b - a - b);
  if (a > b) swap(a, b);
  FOR(i, 1, k) {
    lld u = q.top();
    while (q.size() && q.top() == u) q.pop();
    if (u >= 0)
      ans ^= u;
    else
      break;
    q.push(u - a), q.push(u - b);
  }
  printf("%lld", ans);
  return 0;
}
