#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
#define show(x) cout << #x << "=" << (x) << endl
#define show2(x, y) cout << #x << "=" << (x) << "," << #y << "=" << (y) << endl
#define show3(x, y, z)                                                           \
  cout << #x << "=" << (x) << "," << #y << "=" << (y) << "," << #z << "=" << (z) \
       << endl
const int N = 5e5 + 5, M = N * 2;

struct qxx {
  int nex, t;
};
qxx e[M];
int h[N], le;
void add_path(int f, int t) { e[++le] = (qxx){h[f], t}, h[f] = le; }
#define FORe(i, u, v) for (int i = h[u], v; v = e[i].t, i; i = e[i].nex)

int n, m, p;
int U[N], V[N];

int f[N];
void init(int x) { FOR(i, 0, x) f[i] = i; }
int get(int u) { return f[u] == u ? u : f[u] = get(f[u]); }
void merge(int u, int v) { f[get(u)] = get(v); }
bool find(int u, int v) { return get(u) == get(v); }

bool br[N];

int fa[N], dep[N];
void dfs(int u, int p = 0) {
  dep[u] = dep[p] + 1, fa[u] = p;
  // show2(u,fa[u]);
  FORe(i, u, v) if (v != p) dfs(v, u);
}

int cur, ans = 1;
int main() {
  scanf("%d%d%d", &n, &m, &p);
  init(n);
  FOR(i, 1, m) {
    scanf("%d%d", &U[i], &V[i]);
    if (!find(U[i], V[i])) {
      br[i] = 1;
      merge(U[i], V[i]);
      add_path(U[i], V[i]), add_path(V[i], U[i]);
    }
  }
  FOR(i, 1, n) if (!dep[i]) dfs(i);
  init(n);
  FOR(i, 1, m) {
    // printf("\033[31mi=%d\033[0m\n",i);
    // show2(U[i],V[i]);
    if (br[i])
      ++cur;
    else {
      int u = get(U[i]), v = get(V[i]);
      while (u != v) {
        // show2(u,v);
        if (dep[u] < dep[v]) swap(u, v);
        merge(u, fa[u]), --cur;
        u = get(u);
      }
    }
    // show(cur);
    ans = 1ll * ans * (cur + 1) % p;
  }
  printf("%d\n", ans);
  return 0;
}
