#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
#define int long long
const int N = 1e5 + 5, LIM = 100000;

int n;
int c[N], f[N];

void go() {
  scanf("%lld", &n);
  memset(c, 0, sizeof(c));
  FOR(i, 1, n) {
    int x;
    scanf("%lld", &x);
    c[x]++;
  }
  FOR(i, 1, LIM) {
    f[i] = 0;
    int limj = LIM / i;
    FOR(j, 2, limj) c[i] += c[i * j];
  }
  // FOR(i,1,LIM)if(c[i])printf("c[%lld]=%lld\n",i,c[i]);
  int ans = 0;
  ROF(i, LIM, 1) {
    f[i] = c[i] * i;
    int limj = LIM / i;
    FOR(j, 2, limj) { f[i] = max(f[i], f[i * j] + (c[i] - c[i * j]) * i); }
    // if(f[i])printf("f[%lld]=%lld\n",i,f[i]);
    ans = max(ans, f[i] + n - c[i]);
  }
  printf("%lld\n", ans);
}
signed main() {
  int t;
  scanf("%lld", &t);
  FOR(i, 1, t) go();
  return 0;
}
