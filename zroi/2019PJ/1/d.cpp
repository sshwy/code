#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
namespace IO {
  char nc() {
    static char bf[100000], *p1 = bf, *p2 = bf;
    return p1 == p2 && (p2 = (p1 = bf) + fread(bf, 1, 100000, stdin), p1 == p2)
               ? EOF
               : *p1++;
  }
  int rd() {
    int res = 0;
    char c = getchar();
    while (!isdigit(c)) c = getchar();
    while (isdigit(c)) res = res * 10 + c - '0', c = getchar();
    return res;
  }
} // namespace IO
/******************heading******************/
#define int lld
const int N = 1e5 + 50;

int n, m;

struct qxx {
  int nex, t;
};
qxx e[N * 2];
int h[N], le = 1;
void add_path(int f, int t) { e[++le] = {h[f], t}, h[f] = le; }

int dep[N], fa[20][N];

void dfs(int u, int p) {
  dep[u] = dep[p] + 1, fa[0][u] = p;
  // printf("dep[%lld]=%lld\n",u,dep[u]);
  FOR(i, 1, 19)
  if (fa[i - 1][u])
    fa[i][u] = fa[i - 1][fa[i - 1][u]];
  else
    break;
  for (int i = h[u]; i; i = e[i].nex) {
    if (e[i].t != p) dfs(e[i].t, u);
  }
}

int lca(int u, int v) {
  if (u == v) return v;
  if (dep[u] < dep[v]) swap(u, v);
  ROF(i, 19, 0) {
    if ((1 << i) >= dep[u] - dep[v]) continue;
    u = fa[i][u];
  }
  if (fa[0][u] == v) return v;
  if (dep[u] > dep[v]) u = fa[0][u];
  ROF(i, 19, 0) {
    if (fa[i][u] == fa[i][v]) continue;
    u = fa[i][u], v = fa[i][v];
  }
  return fa[0][u];
}
int f[N], ans;
int g[N], lg;

void dfs2(int u, int p) {
  for (int i = h[u]; i; i = e[i].nex) {
    const int v = e[i].t;
    if (v == p) continue;
    dfs2(v, u);
    f[u] += f[v];
  }
}

signed main() {
  scanf("%lld%lld", &n, &m);
  FOR(i, 1, n - 1) {
    int u, v;
    scanf("%lld%lld", &u, &v);
    add_path(u, v), add_path(v, u);
  }
  dfs(1, 0);
  FOR(i, 1, m) {
    int a, b;
    scanf("%lld%lld", &a, &b);
    int c = lca(a, b);
    // printf("lca(%lld,%lld)=%lld\n",a,b,c);
    g[c]++;
    f[a]++, f[b]++, f[fa[0][c]]--, f[c]--;
  }
  // FOR(i,1,n)printf("f[%lld]=%lld\n",i,f[i]); puts("");
  dfs2(1, 0);
  // FOR(i,1,n)printf("f[%lld]=%lld\n",i,f[i]);
  FOR(i, 1, n) { ans += g[i] * (f[i] - g[i]) + g[i] * (g[i] - 1ll) / 2; }
  // assert(ans%2==0);
  // ans>>=1;
  printf("%lld", ans);
  return 0;
}
