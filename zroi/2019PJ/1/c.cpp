#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
namespace IO {
  char nc() {
    static char bf[100000], *p1 = bf, *p2 = bf;
    return p1 == p2 && (p2 = (p1 = bf) + fread(bf, 1, 100000, stdin), p1 == p2)
               ? EOF
               : *p1++;
  }
  int rd() {
    int res = 0;
    char c = getchar();
    while (!isdigit(c)) c = getchar();
    while (isdigit(c)) res = res * 10 + c - '0', c = getchar();
    return res;
  }
} // namespace IO
/******************heading******************/
const int N = 1e5 + 5;

int n, ans;
int a[N];

void go() {
  int p = 1;
  FOR(i, 1, n) { //尝试把i袋鼠变小
    while (p <= n && ((a[i] << 1) > a[p] || p - i - i < 0)) ++p;
    if (p > n) break;
    // printf("i=%d,p=%d\n",i,p);
    if (p - i - i >= 0)
      ans++, ++p;
    else
      break;
  }
}

int main() {
  scanf("%d", &n);
  FOR(i, 1, n) scanf("%d", &a[i]);
  sort(a + 1, a + n + 1);
  // FOR(i,1,n)printf("%d%c",a[i]," \n"[i==n]);
  go();
  printf("%d", ans);
  return 0;
}
