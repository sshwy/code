#include <cstdio>
#include <cstdlib>
#include <ctime>
using namespace std;

int main() {
  system("g++ b.cpp -o .usr");
  system("g++ b2.cpp -o .std");
  system("g++ c_gen.cpp -o .gen");
  for (int i = 1; i <= 10000; i++) {
    system("./.gen > .fin");
    system("./.usr < .fin > .fout");
    system("./.std < .fin > .fstd");
    if (system("diff -w .fout .fstd")) break;
    printf("AC#%d\n", i);
  }
  printf("WA\n");
  return 0;
}
