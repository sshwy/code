#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/

char s[1000000];
int c[1000];

int main() {
  scanf("%s", s + 1);
  int len = strlen(s + 1), ans = 0;
  if (len & 1) return puts("-1"), 0;
  FOR(i, 1, len) c[s[i]]++;
  int x = min(c['U'], c['D']);
  c['U'] -= x, c['D'] -= x;
  x = min(c['L'], c['R']);
  c['L'] -= x, c['R'] -= x;
  int a = max(c['U'], c['D']);
  int b = max(c['L'], c['R']);
  ans += (a + b) / 2;
  printf("%d\n", ans);
  return 0;
}
