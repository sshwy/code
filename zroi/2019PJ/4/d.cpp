#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 1e5 + 5;

int n;
int c[40], a[N];
__int128 ans;

void print(__int128 x) {
  if (x == 0) return;
  print(x / 10);
  putchar(x % 10 + '0');
}
int main() {
  scanf("%d", &n);
  FOR(i, 1, n) scanf("%d", &a[i]);
  FOR(i, 1, n) {
    FOR(j, 0, 31) { c[j] += a[i] >> j & 1; }
  }
  FOR(i, 1, n) {
    __int128 x = 0;
    FOR(j, 0, 31) if (c[j]) {
      x |= !!c[j] << j;
      --c[j];
    }
    // printf("x=%d\n",x);
    if (x)
      ans += x * x;
    else
      break;
  }
  if (!ans)
    printf("0");
  else
    print(ans);
  return 0;
}
