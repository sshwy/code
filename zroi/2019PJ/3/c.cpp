#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 32, M = 1005;

int f[N][M];
int n, m;
int a[N];

void go() {
  scanf("%d%d", &n, &m);
  int sum = 0;
  FOR(i, 1, n) scanf("%d", &a[i]), sum += a[i];
  m = min(sum, m);
  sort(a + 1, a + n + 1);
  f[0][0] = 1;
  int lim = 0, prelim = 0, pre = 0;
  FOR(i, 1, n) {
    lim += a[i], lim = min(lim, m);
    FOR(j, 0, lim) {
      f[i][j] = 0;
      if (j <= prelim) {
        f[i][j] = f[i - 1][j]; // do not chose a[i]
      }
      if (j >= a[i]) {
        f[i][j] += f[i - 1][j - a[i]]; // chose a[i]
      }
      if (j > pre && j < pre + a[i]) { // that means if we don't chose a[i], we
                                       // should choose all elements from 1..i-1.
                                       // Then we can translate from f[i-1][pre]
        f[i][j] += f[i - 1][pre];
      }
    }
    pre += a[i], prelim = lim;
  }
  printf("%d\n", f[n][m]);
}
int main() {
  int t;
  scanf("%d", &t);
  FOR(i, 1, t) go();
  return 0;
}
