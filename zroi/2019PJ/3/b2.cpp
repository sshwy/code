#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
#define LL long long

LL n, lim;

int c[20], b[20];
bool check(LL x) {
  FOR(i, 0, 9) b[i] = 0;
  if (!x) b[0]++;
  while (x) b[x % 10]++, x /= 10;
  FOR(i, 0, 9) if (b[i] > c[i]) return 0;
  return 1;
}
void go() {
  FOR(i, 0, 9) c[i] = 0;
  LL n2 = n, lim = n * 10;
  if (!n) return;
  while (n2) c[n2 % 10]++, n2 /= 10;
  for (LL i = 0; i * i <= lim; i++) {
    if (check(i) && check(i * i)) printf("%lld * %lld = %lld\n", i, i, i * i);
  }
}
int main() {
  while (~scanf("%lld", &n)) go();
  return 0;
}
