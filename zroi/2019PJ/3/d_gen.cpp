#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/

using namespace RA;

int main() {
  srand(clock());
  int t = 1;
  printf("%d\n", t);
  FOR(i, 1, t) {
    int n = 6, m = 6;
    printf("%d %d\n", n, m);
    FOR(i, 1, n) FOR(j, 1, m) printf("%d%c", r(1, 100), " \n"[j == m]);
    int q = 12;
    printf("%d\n", q);
    FOR(i, 1, q) printf("%d%c", r(1, 100), " \n"[i == q]);
  }
  return 0;
}
