#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
#define LL long long

LL n, lim;

int c[20], b[20];
bool check(LL x) {
  FOR(i, 0, 9) b[i] = 0;
  if (!x) b[0]++;
  while (x) b[x % 10]++, x /= 10;
  FOR(i, 0, 9) if (b[i] > c[i]) return 0;
  return 1;
}
LL m10[20];
LL ans[100000], la;
void dfs(int s, int last, LL num,
    int len) { //可选位置的状态是s，上一次填的数是last，填出来的数是num
  if (!s) {
    if (len > 1 && num < m10[len - 1]) return;
    if (check(num) && check(num * num)) ans[++la] = num;
    return;
  }
  int i = last + 1;
  int cnt_lim = min(__builtin_popcount(s), c[i]);
  for (int cnt = 1; cnt <= cnt_lim; cnt++) {
    for (int t = s; t; t = (t - 1) & s) {
      // assert((s|t)==s);
      if (__builtin_popcount(t) == cnt) {
        int x = 0;
        FOR(j, 0, 20) {
          if (!(t >> j)) break;
          if (t >> j & 1) x += m10[j] * i;
        }
        dfs(s ^ t, i, num + x, len);
      }
    }
  }
  if (last < 9) dfs(s, last + 1, num, len);
}
void go() {
  la = 0;
  FOR(i, 0, 9) c[i] = 0;
  LL n2 = n, lim = n * 10;
  if (!n) return;
  while (n2) c[n2 % 10]++, n2 /= 10;
  FOR(i, 1, 20) {
    if (m10[i] * m10[i] > lim * 10) break;
    // printf("i=%d\n",i);
    dfs((1 << i) - 1, -1, 0, i);
  }
  sort(ans + 1, ans + la + 1);
  FOR(i, 1, la) { printf("%lld * %lld = %lld\n", ans[i], ans[i], ans[i] * ans[i]); }
}
int main() {
  m10[0] = 1;
  FOR(i, 1, 18) m10[i] = m10[i - 1] * 10;
  while (~scanf("%lld", &n)) go();
  return 0;
}
