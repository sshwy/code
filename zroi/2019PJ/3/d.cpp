#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 1e3 + 5, M = 1e3 + 5, Q = 1e5 + 5;

int n, m;
struct dat {
  int v, x, y;
};
dat d[N * M + Q];

int f[N * M + Q], id[N][M];
bool vis[N][M];

void init(int k) { FOR(i, 0, k) f[i] = i; }
int get(int u) { return f[u] == u ? u : f[u] = get(f[u]); }
bool find(int u, int v) { return get(u) == get(v); }
void merge(int u, int v) { f[get(u)] = get(v); }

bool cmp(dat a, dat b) {
  if (a.v != b.v) return a.v > b.v;
  if (a.y == -1 && b.y != -1) return 1;
  return 0;
}
int cur, ans[Q];
bool check(int x, int y) {
  if (x < 1 || x > n || y < 1 || y > m) return 0;
  return 1;
}
void link(int x, int y, int a, int b) {
  if (!check(x, y) || !check(a, b)) return;
  if (!vis[x][y] || !vis[a][b]) return;
  int u = id[x][y], v = id[a][b];
  if (find(u, v)) return;
  merge(u, v), cur--;
}
void work(int x, int y) {
  vis[x][y] = 1, cur++;
  link(x, y, x - 1, y);
  link(x, y, x + 1, y);
  link(x, y, x, y - 1);
  link(x, y, x, y + 1);
}
void go() {
  scanf("%d%d", &n, &m);
  cur = 0;
  int ld = 0;
  FOR(i, 1, n) {
    FOR(j, 1, m) {
      int x;
      scanf("%d", &x);
      d[++ld] = (dat){x, i, j};
    }
  }
  int q;
  scanf("%d", &q);
  FOR(i, 1, q) {
    int x;
    scanf("%d", &x);
    d[++ld] = (dat){x, i, -1}; // y=-1 means query
  }
  sort(d + 1, d + ld + 1, cmp);
  init(ld);
  FOR(i, 1, ld) if (d[i].y != -1) id[d[i].x][d[i].y] = i;
  FOR(i, 1, n) FOR(j, 1, m) vis[i][j] = 0;
  FOR(i, 1, ld) {
    if (d[i].y == -1) {
      ans[d[i].x] = cur;
    } else {
      work(d[i].x, d[i].y);
    }
  }
  FOR(i, 1, q) printf("%d%c", ans[i], " \n"[i == q]);
}
int main() {
  int t;
  scanf("%d", &t);
  FOR(i, 1, t) go();
  return 0;
}
/*
 * BUG#1:cmp定义出锅导致RE。sort传入的cmp是代表一个严格小于的函数
 * BUG#2:数组开小。应该是N*M+Q
 */
