#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 32, M = 1005;

int n, m, sum;
int a[N];

void go() {
  scanf("%d%d", &n, &m);
  FOR(i, 0, n - 1) scanf("%d", &a[i]), sum += a[i];
  m = min(sum, m);
  // printf("m=%d\n",m);
  sort(a, a + n);
  int nn = 1 << n, ans = 0;
  FOR(i, 0, nn - 1) {
    int s = 0;
    FOR(j, 0, n - 1) if (i >> j & 1) s += a[j];
    if (s > m) continue;
    // printf("s=%d\n",s);
    s = m - s;
    int fl = 1;
    FOR(j, 0, n - 1) if (!(i >> j & 1) && a[j] <= s) fl = 0;
    ans += fl;
  }
  printf("%d\n", ans);
}
int main() {
  int t;
  scanf("%d", &t);
  FOR(i, 1, t) go();
  return 0;
}
