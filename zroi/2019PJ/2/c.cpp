#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
namespace IO {
  char nc() {
    static char bf[100000], *p1 = bf, *p2 = bf;
    return p1 == p2 && (p2 = (p1 = bf) + fread(bf, 1, 100000, stdin), p1 == p2)
               ? EOF
               : *p1++;
  }
  int rd() {
    int res = 0;
    char c = getchar();
    while (!isdigit(c)) c = getchar();
    while (isdigit(c)) res = res * 10 + c - '0', c = getchar();
    return res;
  }
} // namespace IO
/******************heading******************/
#define int lld

int n, k;
int m[50];

int pw(int a, int m) {
  int res = 1;
  while (m) m & 1 ? res = res * a : 0, a = a * a, m >>= 1;
  return res;
}
int calc(int x, int i) { return x / m[i] - x / m[i + 1]; }
bool check(int x) { //个数大于等于n
  int cnt = 0;
  for (int i = 1; m[i] <= x && i <= 17; i += 2) {
    // printf("i=%lld\n",i);
    cnt += calc(x, i);
    if (cnt >= n) return 1;
  }
  return cnt >= n;
}
signed main() {
  scanf("%lld%lld", &n, &k);
  int l = 0, r = 0x7fffffffffffffff;
  m[0] = 1;
  FOR(i, 1, 18) m[i] = m[i - 1] * k;
  while (l < r) {
    int mid = (l + r) >> 1;
    // printf("mid=%lld\n",mid);
    if (check(mid))
      r = mid;
    else
      l = mid + 1;
  }
  printf("%lld\n", l);
  return 0;
}
