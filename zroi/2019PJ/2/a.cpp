#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
namespace IO {
  char nc() {
    static char bf[100000], *p1 = bf, *p2 = bf;
    return p1 == p2 && (p2 = (p1 = bf) + fread(bf, 1, 100000, stdin), p1 == p2)
               ? EOF
               : *p1++;
  }
  int rd() {
    int res = 0;
    char c = getchar();
    while (!isdigit(c)) c = getchar();
    while (isdigit(c)) res = res * 10 + c - '0', c = getchar();
    return res;
  }
} // namespace IO
/******************heading******************/
#define int lld

int n, l, w, x, y, a;

signed main() {
  scanf("%lld%lld%lld%lld%lld%lld", &n, &l, &w, &x, &y, &a);
  int lx = l / x, wy = w / y;
  // printf("lx=%lld,wy=%lld\n",lx,wy);
  if (lx * wy < n) return puts("-1"), 0;
  int nlx = n / lx, rest = n % lx;
  int n2 = nlx + ((bool)rest);
  if (n2 * y > w) return puts("-1"), 0;
  int re = w - n2 * y;
  int tot = re / a;
  // printf("tot=%lld\n",tot);
  if (tot * 2 >= n2) return printf("%lld", n), 0;
  // puts("gg");
  int ans = tot * 2 * lx;
  printf("%lld\n", ans == 0 ? -1 : ans);
  return 0;
}
