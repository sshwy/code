#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
namespace IO {
  char nc() {
    static char bf[100000], *p1 = bf, *p2 = bf;
    return p1 == p2 && (p2 = (p1 = bf) + fread(bf, 1, 100000, stdin), p1 == p2)
               ? EOF
               : *p1++;
  }
  int rd() {
    int res = 0;
    char c = getchar();
    while (!isdigit(c)) c = getchar();
    while (isdigit(c)) res = res * 10 + c - '0', c = getchar();
    return res;
  }
} // namespace IO
/******************heading******************/
#define int lld
const int SZ = 2e5 + 5, P = 998244353;

int n;

struct qxx {
  int nex, t;
};
qxx e[SZ];
int h[SZ], le;
void add_path(int f, int t) { e[++le] = (qxx){h[f], t}, h[f] = le; }

int dg[SZ], g[SZ], w[SZ];
int f[SZ][2][2], F[SZ];

int pw(int a, int m) {
  int res = 1;
  while (m) m & 1 ? res = res * a % P : 0, a = a * a % P, m >>= 1;
  return res;
}
void dfs(int u) {
  // printf("dfs(%lld)\n",u);
  for (int i = h[u]; i; i = e[i].nex) { dfs(e[i].t); }
  int t = g[u] - dg[u];
  // printf("u=%lld,t=%lld\n",u,t);
  assert(t >= 0);

  int a = 1, b = 1, c = 1, d = 1;
  for (int i = h[u]; i; i = e[i].nex) {
    const int v = e[i].t;
    (a *= f[v][1][1]) %= P;
    (b *= f[v][1][0] + f[v][1][1]) %= P;
    (c *= f[v][1][1] + f[v][0][1]) %= P;
    (d *= F[v]) %= P;
  }
  d = d * pw(2, t) % P;
  F[u] = d;
  f[u][0][0] = a;
  f[u][0][1] = (b - a + P) % P;
  f[u][1][0] = (c - a + P) % P;
  f[u][1][1] = F[u] - f[u][0][0] - f[u][0][1] - f[u][1][0];
  f[u][1][1] = (f[u][1][1] % P + P) % P;
  if (w[u] == 0) {
    // printf("\033[32mw[%lld]=0\033[0m\n",u);
    (f[u][0][0] += f[u][0][1]) %= P, f[u][0][1] = 0;
    (f[u][1][0] += f[u][1][1]) %= P, f[u][1][1] = 0;
  } else if (w[u] == 1) {
    // printf("\033[32mw[%lld]=1\033[0m\n",u);
    (f[u][0][1] += f[u][0][0]) %= P, f[u][0][0] = 0;
    (f[u][1][1] += f[u][1][0]) %= P, f[u][1][0] = 0;
  }
  // printf("F[%lld]=%lld\n",u,F[u]);
  // printf("f[%lld,%lld,%lld]=%lld\n",u,0ll,0ll,f[u][0][0]);
  // printf("f[%lld,%lld,%lld]=%lld\n",u,0ll,1ll,f[u][0][1]);
  // printf("f[%lld,%lld,%lld]=%lld\n",u,1ll,0ll,f[u][1][0]);
  // printf("f[%lld,%lld,%lld]=%lld\n",u,1ll,1ll,f[u][1][1]);
}
signed main() {
  scanf("%lld", &n);
  FOR(i, 1, n) {
    int fi;
    scanf("%lld%lld%lld", &fi, &g[i], &w[i]);
    add_path(fi, i);
    dg[fi]++;
  }
  g[0] = dg[0], w[0] = -1;
  dfs(0);
  printf("%lld", (f[0][1][1] + f[0][0][0]) * pw(F[0], P - 2) % P);
  return 0;
}
