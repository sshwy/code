#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
namespace IO {
  char nc() {
    static char bf[100000], *p1 = bf, *p2 = bf;
    return p1 == p2 && (p2 = (p1 = bf) + fread(bf, 1, 100000, stdin), p1 == p2)
               ? EOF
               : *p1++;
  }
  int rd() {
    int res = 0;
    char c = getchar();
    while (!isdigit(c)) c = getchar();
    while (isdigit(c)) res = res * 10 + c - '0', c = getchar();
    return res;
  }
} // namespace IO
/******************heading******************/
const int P1 = 998245331, P3 = 878688857;
typedef unsigned int ui;
char nc() {
  static char bf[100000], *p1 = bf, *p2 = bf;
  return p1 == p2 && (p2 = (p1 = bf) + fread(bf, 1, 100000, stdin), p1 == p2) ? EOF
                                                                              : *p1++;
}

struct data {
  ui x1, x3;
  data() { x1 = x3 = 0; }
  data(int a, int c) { x1 = a, x3 = c; }
  bool operator==(data d) const { return x1 == d.x1 && x3 == d.x3; }
  data operator*(data x) const {
    return data(1ll * x1 * x.x1 % P1, 1ll * x3 * x.x3 % P3);
  }
  data operator+(data x) const { return data((x1 + x.x1) % P1, (x3 + x.x3) % P3); }
  data operator-(data x) const {
    return data((x1 + P1 - x.x1) % P1, (x3 + P3 - x.x3) % P3);
  }
  void print() { printf("(%d,%d)\n", x1, x3); }
};
const int SZ = 2000003;
int totime;
struct mp {
  struct dat {
    data u;
    int v, tim;
    int nex;
  };
  dat e[SZ];
  int h[SZ], le;
  int hash(data u) { return (u.x1 + u.x3) % SZ; }
  int &operator[](data u) {
    int hu = hash(u);
    for (int i = h[hu]; i; i = e[i].nex)
      if (e[i].u == u) {
        if (e[i].tim == totime) return e[i].v;
        e[i].v = -1, e[i].tim = totime;
        return e[i].v;
      }
    e[++le] = (dat){u, -1, totime, h[hu]}, h[hu] = le;
    return e[le].v;
  }
} ha, hb, hc;
data rd(int &len) {
  data res;
  char c = nc();
  while (!isdigit(c)) c = nc();
  while (isdigit(c)) {
    res.x1 = (res.x1 * 10ll + c - '0') % P1;
    res.x3 = (res.x3 * 10ll + c - '0') % P3;
    c = nc();
    len++;
  }
  return res;
}
int T = 500;
void go() {
  ++totime;
  int la = 0, lb = 0, lc = 0;
  data a = rd(la), b = rd(lb), c = rd(lc);
  ha[a] = 0, hb[b] = 0, hc[c] = 0;
  int m1 = 1, m3 = 1;
  int lim = max(la, max(lb, lc));
  lim = min(lim, 500);
  FOR(i, 1, lim) {
    m1 = m1 * 10ll % P1;
    m3 = m3 * 10ll % P3;
    data t(m1, m3);
    // printf("a*%d:",i); (a*t).print();
    // printf("bt:"); (b*t).print();
    // printf("ct:"); (c*t).print();
    ha[a * t] = i;
    hb[b * t] = i;
    hc[c * t] = i;
  }
  m1 = 1, m3 = 1;
  FOR(i, 0, lim) {
    data t(m1, m3);
    if (hc[a + b * t] != -1) return printf("%d %d %d\n", 0, i, hc[a + b * t]), void();
    if (hc[a * t + b] != -1) return printf("%d %d %d\n", i, 0, hc[a * t + b]), void();
    if (ha[c * t - b] != -1) return printf("%d %d %d\n", ha[c * t - b], 0, i), void();
    if (hb[c * t - a] != -1) return printf("%d %d %d\n", 0, hb[c * t - a], i), void();
    if (ha[c - b * t] != -1) return printf("%d %d %d\n", ha[c - b * t], i, 0), void();
    if (hb[c - a * t] != -1) return printf("%d %d %d\n", i, hb[c - a * t], 0), void();
    m1 = m1 * 10ll % P1;
    m3 = m3 * 10ll % P3;
  }
  puts("-1");
}
int main() {
  // freopen("tmpout","w",stdout);
  int t;
  scanf("%d", &t);
  FOR(i, 1, t) go();
  return 0;
}
