// by Sshwy
//#define DEBUGGER
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

#ifdef DEBUGGER

#define log_prefix()                                                \
  {                                                                 \
    fprintf(stderr, "\033[37mLine %-3d [%dms]:\033[0m  ", __LINE__, \
        (int)clock() / 1000);                                       \
  }
#define ilog(...) \
  { fprintf(stderr, __VA_ARGS__); }
#define llog(...)      \
  {                    \
    log_prefix();      \
    ilog(__VA_ARGS__); \
  }
#define log(...)           \
  {                        \
    log_prefix();          \
    ilog(__VA_ARGS__);     \
    fprintf(stderr, "\n"); \
  }
#define red(...)       \
  {                    \
    log_prefix();      \
    ilog("\033[31m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define green(...)     \
  {                    \
    log_prefix();      \
    ilog("\033[32m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define blue(...)      \
  {                    \
    log_prefix();      \
    ilog("\033[34m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }

#else

#define log_prefix() ;
#define ilog(...) ;
#define llog(...) ;
#define log(...) ;
#define red(...) ;
#define green(...) ;
#define blue(...) ;

#endif

namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
#include "hasher.h"
#include <functional>

typedef long long LL;
pair<bool, vector<LL>> work(vector<LL> a, LL mod, LL coef = 1) {
  // printf("work "); for(auto x:a)printf("%d ",x); puts("");
  vector<pair<LL, int>> v;
  for (auto &x : a) x = mul(x, coef, mod);
  struct node {
    int lc, rc, lv, rv;
  };
  vector<node> tree;
  for (auto x : a) {
    tree.pb({-1, -1, 0, 0});
    v.pb({x, int(tree.size()) - 1});
  }
  while (v.size() > 1) {
    vector<pair<LL, int>> nv;
    sort(v.begin(), v.end());
    // for(auto x:v)printf("(%d,%d) ",x.fi,x.se); puts("");
    if (v[0].fi == 0) break; // attack
    for (int i = 0; i < int(v.size()); i += 2) {
      if (i == int(v.size()) - 1) {
        nv.pb(v[i]);
      } else { // merge
        auto x = v[i], y = v[i + 1];
        bool flag = x.fi > y.fi;
        tree.pb({x.se, y.se, flag ? 1 : -1, flag ? -1 : 1});
        nv.pb({abs(x.fi - y.fi), int(tree.size()) - 1});
        // printf("merge (%d,%d) (%d,%d) ->
        // (%d,%d)\n",x.fi,x.se,y.fi,y.se,nv.back().fi,int(tree.size())-1);
      }
    }
    v = nv;
  }
  if (v[0].fi == 0) {
    vector<LL> res(a.size(), 0);
    function<void(int, LL)> dfs;
    dfs = [&tree, &res, &dfs](int u, LL cur) {
      if (tree[u].lc == -1) {
        assert(tree[u].rc == -1);
        assert(u < int(res.size()));
        res[u] = cur;
        return;
      }
      dfs(tree[u].lc, cur * tree[u].lv);
      dfs(tree[u].rc, cur * tree[u].rv);
    };
    dfs(v[0].se, coef);
    return {1, res};
  }
  return {0, {}};
}

using namespace RA;
pair<vector<LL>, vector<LL>> tree_attack(LL base, LL mod) {
  FOR(k, 1, 20) {
    int times = k * 10;
    FOR(_, 1, times) {
      int n = r(1 << k - 1, 1 << k);
      LL mb = 1;
      vector<LL> a;
      FOR(i, 1, n) {
        a.pb(mb);
        mb = mul(mb, base, mod);
      }
      reverse(a.begin(), a.end());
      auto x = work(a, mod);
      if (x.fi) {
        vector<LL> s, t;
        for (auto z : x.se) {
          if (z == 1)
            s.pb(1), t.pb(0);
          else if (z == 0)
            s.pb(0), t.pb(0);
          else
            s.pb(0), t.pb(1);
        }
        return {s, t};
      }
    }
  }
  return {{}, {}};
}

pair<vector<LL>, vector<LL>> tree_attacks(LL base, LL mod, int times) {
  vector<LL> x, y;
  FOR(_, 1, times) {
    auto p = tree_attack(base, mod);
    if (p.fi.size()) {
      if (!x.size() || p.fi.size() < x.size()) x = p.fi, y = p.se;
    }
  }
  return {x, y};
}

vector<LL> tree_attack_even_palindrome(LL base, LL mod, LL coef) {
  FOR(k, 1, 20) {
    int times = k * 10;
    FOR(_, 1, times) {
      int n = r(1 << k - 1, 1 << k);
      if (n & 1) ++n; // n is even
      assert(n % 2 == 0);
      vector<LL> a;
      FOR(i, 1, n / 2) {
        a.pb((pw(base, n - i, mod) - pw(base, i - 1, mod) + mod) % mod);
      }
      auto x = work(a, mod, coef);
      if (x.fi) {
        vector<LL> s(n);
        assert(int(x.se.size()) == n / 2);
        FOR(i, 1, n / 2) {
          if (x.se[i - 1] == coef) {
            s[i - 1] = coef, s[n - i] = 0;
          } else if (x.se[i - 1] == 0) {
            s[i - 1] = 0, s[n - i] = 0;
          } else {
            s[i - 1] = 0, s[n - i] = coef;
          }
        }
        return s;
      }
    }
  }
  return {};
}

vector<LL> tree_attack_even_palindromes(LL base, LL mod, LL coef, int times) {
  vector<LL> x;
  FOR(_, 1, times) {
    auto p = tree_attack_even_palindrome(base, mod, coef);
    if (p.size()) {
      if (!x.size() || p.size() < x.size()) x = p;
    }
  }
  return x;
}

pair<LL, LL> CRT(vector<pair<LL, LL>> v) {
  LL M = 1, X = 0;
  for (auto x : v) M *= x.se;
  for (auto x : v) {
    int m = M / x.se;
    X = (X + mul(x.fi, mul(m, pw(m, x.se - 2, x.se), M), M)) % M;
  }
  for (auto x : v) assert(X % x.se == x.fi);
  return {X, M};
}

vector<LL> tree_attack_even_palindrome_double(LL b1, LL m1, LL b2, LL m2) {
  FOR(k, 1, 20) {
    int times = 1;
    FOR(_, 1, times) {
      int n = r(1 << k - 1, 1 << k);
      if (n & 1) ++n; // n is even
      assert(n % 2 == 0);
      vector<LL> a;
      FOR(i, 1, n / 2) {
        int a1 = (pw(b1, n - i, m1) - pw(b1, i - 1, m1) + m1) % m1;
        int a2 = (pw(b2, n - i, m2) - pw(b2, i - 1, m2) + m2) % m2;
        vector<pair<LL, LL>> v = {{a1, m1}, {a2, m2}};
        auto p = CRT(v);
        a.pb(p.fi);
      }
      auto x = work(a, m1 * m2);
      if (x.fi) {
        vector<LL> s(n);
        assert(int(x.se.size()) == n / 2);
        FOR(i, 1, n / 2) {
          if (x.se[i - 1] == 1) {
            s[i - 1] = 1, s[n - i] = 0;
          } else if (x.se[i - 1] == 0) {
            s[i - 1] = 0, s[n - i] = 0;
          } else {
            s[i - 1] = 0, s[n - i] = 1;
          }
        }
        return s;
      }
    }
  }
  return {};
}
vector<LL> tree_attack_even_palindrome_doubles(
    LL b1, LL m1, LL b2, LL m2, int times) {
  vector<LL> x;
  FOR(_, 1, times) {
    auto p = tree_attack_even_palindrome_double(b1, m1, b2, m2);
    if (p.size()) {
      if (!x.size() || p.size() < x.size()) {
        x = p;
        printf("length: %d\n", x.size());
      }
    }
  }
  return x;
}

int example_main() {
  srand(__builtin_ia32_rdtsc() + clock());

  LL b1 = 13331, m1 = 998244353;
  LL b2 = 131, m2 = 1000000007;

  // auto pal=tree_attack_even_palindromes(b1,m1,1,50);
  auto pal = tree_attack_even_palindrome_doubles(b1, m1, b2, m2, 100);
  auto rp = pal;
  reverse(rp.begin(), rp.end());

  cout << "Palindrome length=" << pal.size() << endl;
  cout << vtos(pal) << endl;
  cout << hasher::hsh(pal, b1, m1) << endl;
  cout << hasher::hsh(rp, b1, m1) << endl;
  cout << hasher::hsh(pal, b2, m2) << endl;
  cout << hasher::hsh(rp, b2, m2) << endl;

  return 0;
}

int main() {
  int n = 10;
  do {
    vector<LL> a;
    FOR(i, 1, n / 2) { a.pb(2 * i - n - 1); }
    auto p = work(a, 1e9 + 7);
    for (auto x : a) printf("%lld ", x);
    puts("");
    for (auto x : p.se) printf("%lld ", x);
    puts("");
    if (p.fi) {
      vector<LL> s(n);
      FOR(i, 1, n / 2) {
        if (p.se[i - 1] == 1)
          s[i - 1] = 1, s[n - i] = 0;
        else if (p.se[i - 1] == 0)
          s[i - 1] = 0, s[n - i] = 0;
        else
          s[i - 1] = 0, s[n - i] = 1;
      }
      cout << vtos(s) << endl;

      auto rs = s;
      reverse(rs.begin(), rs.end());

      cout << hasher::hsh2(s, 1e9 + 7) << endl;
      cout << hasher::hsh2(rs, 1e9 + 7) << endl;

      return 0;
    }
  } while (n += 2);
  return 0;
}
