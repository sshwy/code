// by Sshwy

#ifndef _HASHER_H_
#define _HASHER_H_

inline long long mul(long long a, long long b, long long p) {
  if (p <= 1000000000ll) return 1ll * a * b % p;
  if (p <= 1000000000000ll)
    return (((a * (b >> 20) % p) << 20) % p + a * (b & ((1 << 20) - 1))) % p;
  long long d = floor(a * (long double)b / p);
  long long res = (a * b - d * p) % p;
  if (res < 0) res += p;
  return res;
}
long long pw(long long a, long long m, long long mod) {
  int res = 1;
  while (m) m & 1 ? res = mul(res, a, mod) : 0, a = mul(a, a, mod), m >>= 1;
  return res;
}

namespace hasher {
  int hsh(string s, int base, int mod) {
    int res = 0;
    for (char c : s) res = (res * 1ll * base + c - 'a') % mod;
    return res;
  }
  long long hsh2(vector<long long> s, long long mod) {
    long long res = 0, cnt = 0;
    for (long long c : s) res = (res + mul(++cnt, c, mod)) % mod;
    return res;
  }
  long long hsh(vector<long long> s, long long base, long long mod) {
    long long res = 0;
    for (auto c : s) res = (mul(res, base, mod) + c) % mod;
    return res;
  }
} // namespace hasher
vector<long long> stov(string s) {
  vector<long long> res;
  for (char x : s) res.pb(x - 'a');
  return res;
}
string vtos(vector<long long> v) {
  string s;
  for (auto x : v) s.pb(x + 'a');
  return s;
}
bool operator!=(const vector<long long> x, const vector<long long> y) {
  if (x.size() != y.size()) return 1;
  int l = x.size();
  FOR(i, 0, l - 1) if (x[i] != y[i]) return 1;
  return 0;
}
vector<long long> operator+=(const vector<long long> &&a, const vector<long long> b) {
  auto res = a;
  return res.insert(res.end(), b.begin(), b.end()), res;
}
vector<long long> operator+=(vector<long long> &a, const vector<long long> b) {
  return a.insert(a.end(), b.begin(), b.end()), a;
}

#endif
