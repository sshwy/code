template <const int N> struct dsu {
  int f[N], g[N], w[N];
  void init(int n) {
    for (int i = 0; i <= n; i++) f[i] = i, g[i] = 1, w[i] = 0;
  }
  stack<pair<int *, int>> s;
  int get(int u) { return f[u] == u ? u : get(f[u]); }
  bool find(int u, int v) { return get(u) == get(v); }
  int val(int u) {
    int res = 0;
    while (f[u] != u) res ^= w[u], u = f[u];
    return res;
  }
  void merge(int u, int v) {
    int vu = val(u), vv = val(v);
    u = get(u), v = get(v);
    if (u == v) return;
    if (g[u] > g[v]) swap(u, v), swap(vu, vv);
    s.push({f + u, f[u]}), f[u] = v;
    s.push({w + u, w[u]}), w[u] = vu ^ vv ^ 1;
    s.push({g + v, g[v]}), g[v] += g[u];
  }
  void undo(unsigned tag) {
    while (s.size() > tag) *s.top().first = s.top().second, s.pop();
  }
};
