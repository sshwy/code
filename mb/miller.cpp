#include <algorithm>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long LL;
typedef long double Lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
/******************heading******************/

LL mul(LL a, LL b, LL p) {
  a %= p, b %= p;
  if (a <= 1000000000 && b <= 1000000000) return a * b % p;
  LL c = (Lf)a * b / p, res = a * b - c * p;
  res < 0 ? res += p : (res >= p ? res -= p : 0);
  return res;
}
LL pw(LL a, LL m, LL p) {
  LL res = 1;
  while (m) m & 1 ? res = mul(res, a, p) : 0, a = mul(a, a, p), m >>= 1;
  return res;
}
bool miller_check(LL b, LL n) {
  LL t = n - 1;
  while (!(t & 1)) t >>= 1;
  if (pw(b, t, n) == 1) return 1;
  LL pwt = pw(b, t, n), res = 0;
  while (t < n - 1) res |= pwt == n - 1, t <<= 1, pwt = mul(pwt, pwt, n);
  return pwt == 1 && res; //判断b^{n-1} mod n的值是否为1
}
bool miller(LL n) {
  if (n == 1) return 0;
  const LL base[] = {2, 3, 5, 7, 11, 13, 17, 19, 23}, lb = 8;
  FOR(i, 0, lb) {
    if (base[i] == n) return 1;
    if (n % base[i] == 0) return 0;
  }
  FOR(i, 0, lb) if (!miller_check(base[i], n)) return 0;
  return 1;
}

int main() {
  LL x;
  while (~scanf("%LL", &x)) {
    if (miller(x))
      puts("Y");
    else
      puts("N");
  }
  return 0;
}
