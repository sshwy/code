namespace IO { // require C++11
  const int _BS = 200000;
  char _ob[_BS], *_z = _ob, _ib[_BS], *_x = _ib, *_y = _ib;
  // Input
  char _nc() {
    if (_x == _y) _y = (_x = _ib) + fread(_ib, 1, _BS, stdin);
    if (_x == _y) return EOF;
    return *_x++;
    // return _x==_y&&(_y=(_x=_ib)+fread(_ib,1,_BS,stdin),_x==_y)?EOF:*_x++;
  }
  inline void rd(char &c) { c = _nc(); }
  void rd(char *s) {
    char c = _nc();
    while (!isblank(c) && isprint(c) && c != EOF) *s++ = c, c = _nc();
    *s = 0;
  }
  template <class T> void rd(T &res) { // unsigned/signed int/long long/__int128
    res = 0;
    char c = _nc(), flag = 0;
    while (!isdigit(c) && c != '-') c = _nc();
    if (c == '-') flag = 1, c = _nc();
    while (isdigit(c)) res = res * 10 + c - '0', c = _nc();
    if (flag) res = -res;
  }
  template <class T, class... A> void rd(T &hd, A &...rs) { rd(hd), rd(rs...); }
  // Output
  inline void flush() { fwrite(_ob, 1, _z - _ob, stdout), _z = _ob; }
  inline void wrch(char x) {
    if (_z == _ob + _BS - 1) flush();
    *_z = x, ++_z;
  }
  void wr(char *s) {
    while (*s) wrch(*s), ++s;
  }
  void wr(const char *s) {
    while (*s) wrch(*s), ++s;
  }
  template <class T> void wr(T x) { // unsigned/signed int/long long/__int128
    if (x == 0) return wrch('0'), void();
    if (x < 0) wrch('-'), x = -x;
    int _t[50], _lt = 0;
    while (x) _t[++_lt] = x % 10, x /= 10;
    while (_lt > 0) wrch(_t[_lt] + '0'), --_lt;
  }
  inline void wr(char x) { wrch(x); }
  template <class T, class... A> void wr(T hd, A... rs) { wr(hd), wr(rs...); }
  template <class... A> void wrln(A... ar) { wr(ar...), wrch('\n'); }
  struct _Flusher {
    ~_Flusher() { flush(); }
  } _flusher;
} // namespace IO
using IO::rd;
using IO::wrln;
