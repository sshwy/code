/******************heading******************/
const long long N = 1 << 17, P = 998244353;
long long n, in;
long long a[N], b[N], c[N];

long long pw(long long a, long long m, long long p) {
  long long res = 1;
  while (m) m & 1 ? res = res * a % p : 0, a = a * a % p, m >>= 1;
  return res;
}
void fwt_and(long long *f, long long tag) {
  for (long long j = 1; j < n; j <<= 1)
    for (long long i = 0; i < n; i += j << 1)
      for (long long k = i; k < i + j; k++) f[k] = (f[k] + f[k + j] * tag) % P;
  FOR(i, 0, n - 1) f[i] += f[i] < 0 ? P : 0;
}
void fwt_or(long long *f, long long tag) {
  for (long long j = 1; j < n; j <<= 1)
    for (long long i = 0; i < n; i += j << 1)
      for (long long k = i; k < i + j; k++) f[k + j] = (f[k + j] + f[k] * tag) % P;
  FOR(i, 0, n - 1) f[i] += f[i] < 0 ? P : 0;
}
void fwt_xor(long long *f, long long tag) {
  for (long long j = 1; j < n; j <<= 1)
    for (long long i = 0; i < n; i += j << 1)
      for (long long k = i; k < i + j; k++) {
        long long x = f[k], y = f[k + j];
        f[k] = (x + y) % P, f[k + j] = (x - y) % P;
      }
  if (tag == -1) FOR(i, 0, n - 1) f[i] = f[i] * in % P;
  FOR(i, 0, n - 1) f[i] += f[i] < 0 ? P : 0;
}
/*
 * fwt:tag=1
 * ifwt:tag=-1
 * in=pw(n,P-2,P) (inverse element if n)
 */
