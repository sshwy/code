struct Matrix {
  int w, h;
  int c[MT][MT];
  Matrix(int _h = 0, int _w = 0) {
    w = _w, h = _h;
    FOR(i, 0, h - 1) FOR(j, 0, w - 1) c[i][j] = 0;
  }
  Matrix(int _h, int _w, int _c[MT][MT]) {
    w = _w, h = _h;
    FOR(i, 0, h - 1) FOR(j, 0, w - 1) c[i][j] = _c[i][j];
  }
  Matrix(int _w) {
    w = h = _w;
    FOR(i, 0, h - 1) FOR(j, 0, w - 1) c[i][j] = 0;
    FOR(i, 0, h - 1) c[i][i] = 1;
  }
  Matrix operator*(const Matrix &m) {
    assert(w == m.h);
    Matrix res(h, m.w);
    FOR(i, 0, h - 1) FOR(k, 0, w - 1) FOR(j, 0, m.w - 1) {
      res.c[i][j] = (res.c[i][j] + c[i][k] * 1ll * m.c[k][j]) % P;
    }
    return res;
  }
  Matrix &operator*=(const Matrix &m) {
    *this = *this * m;
    return *this;
  }
};
;
