const int INF = 0x3f3f3f3f, _N = 1e5 + 5, _M = 1e6 + 5;

struct qxx {
  int nex, t, v, c;
} e[_M];
int h[_N], le = 1;
void add_path(int f, int t, int v, int c) {
  e[++le] = (qxx){h[f], t, v, c}, h[f] = le;
}
void add_flow(int f, int t, int v, int c) {
  add_path(f, t, v, c), add_path(t, f, 0, -c);
}

bool vis[_N];
queue<int> q;
long long d[_N];
int pre[_N], incf[_N];
int s, t;

bool spfa() {
  memset(d, 0x3f, sizeof(d));
  q.push(s), d[s] = 0, incf[s] = INF, incf[t] = 0;
  while (!q.empty()) {
    int u = q.front();
    q.pop();
    vis[u] = 0;
    for (int i = h[u]; i; i = e[i].nex) {
      const int v = e[i].t, w = e[i].v, c = e[i].c;
      if (!w || d[v] <= d[u] + c) continue;
      d[v] = d[u] + c, incf[v] = min(incf[u], w), pre[v] = i;
      if (!vis[v]) q.push(v), vis[v] = 1;
    }
  }
  return incf[t];
}
int maxflow;
long long mincost;
void update() {
  maxflow += incf[t], mincost += incf[t] * 1ll * d[t];
  for (int u = t; u != s; u = e[pre[u] ^ 1].t) {
    e[pre[u]].v -= incf[t], e[pre[u] ^ 1].v += incf[t];
  }
}
void go() {
  while (spfa()) update();
}
