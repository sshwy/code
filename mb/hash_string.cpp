//三模哈希 codeforces 1320D
#include <bits/stdc++.h>
using namespace std;

const int N = 2e5 + 3;
const int base[3] = {19260817, 99277, 96671};
const int mod[3] = {987898789, (int)1e9 + 7, (int)1e9 + 3};
int ha[3][N], pw[3][N];
int str[N];
int n, q, m, c[N], pos[N];
char s[N];

long long hsh(int l, int r) {
  int x = (ha[0][r] + 1ll * (mod[0] - ha[0][l - 1]) * pw[0][r - l + 1]) % mod[0];
  int y = (ha[1][r] + 1ll * (mod[1] - ha[1][l - 1]) * pw[1][r - l + 1]) % mod[1];
  int z = (ha[2][r] + 1ll * (mod[2] - ha[2][l - 1]) * pw[2][r - l + 1]) % mod[2];
  return 1ll * x * mod[1] + 1ll * y * mod[2] + 1ll * z * mod[0];
}

int main() {
  // freopen("data.in", "r", stdin);
  scanf("%d %s %d", &n, s + 1, &q);
  for (int i = 1; i <= n; i++) {
    c[i] = c[i - 1] + (s[i] == '0');
    if (s[i] == '0') pos[++m] = i, str[m] = (pos[m] - pos[m - 1]) & 1;
  }
  for (int k = 0; k < 3; k++) {
    pw[k][0] = 1;
    for (int i = 1; i <= m; i++) {
      pw[k][i] = 1ll * pw[k][i - 1] * base[k] % mod[k];
      ha[k][i] = (1ll * ha[k][i - 1] * base[k] + str[i] + 1) % mod[k];
    }
  }
  while (q--) {
    int lx, ly, rx, ry, len;
    scanf("%d %d %d", &lx, &ly, &len);
    rx = lx + len - 1, ry = ly + len - 1;
    if (c[rx] - c[lx - 1] != c[ry] - c[ly - 1]) {
      puts("No");
      continue;
    }
    if (c[rx] - c[lx - 1] == 0) {
      puts("Yes");
      continue;
    }
    int px = lower_bound(pos + 1, pos + 1 + m, lx) - pos;
    int py = lower_bound(pos + 1, pos + 1 + m, ly) - pos;
    if ((pos[px] - lx & 1) ^ (pos[py] - ly & 1)) {
      puts("No");
      continue;
    }
    int num = c[rx] - c[lx - 1];

    puts(hsh(px + 1, px + num - 1) == hsh(py + 1, py + num - 1) ? "Yes" : "No");
  }
  return 0;
}
