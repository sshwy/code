/******************heading******************/
const int SZ = 233341;
int y, z, p, t, k;

int pw(int a, int m, int p) {
  int res = 1;
  while (m) m & 1 ? res = 1ll * res * a % p : 0, a = 1ll * a * a % p, m >>= 1;
  return res;
}
struct hash_map {
  struct data {
    int u, v, nex;
  };
  data e[SZ];
  int h[SZ], le;
  int hash(int u) { return (u % SZ + SZ) % SZ; }
  int &operator[](int u) {
    int hu = hash(u);
    for (int i = h[hu]; i; i = e[i].nex)
      if (e[i].u == u) return e[i].v;
    return e[++le] = (data){u, -1, h[hu]}, h[hu] = le, e[le].v;
  }
  void clear() { memset(h, 0, sizeof(h)), le = 0; }
} h;
int bsgs(int a, int b, int p) {
  a %= p, b %= p;
  int t = sqrt(p) + 0.5, cur = b, q = 1;
  h.clear();
  FOR(i, 0, t) h[cur] = i, cur = 1ll * cur * a % p;
  cur = pw(a, t, p);
  FOR(i, 0, t) {
    if (h[q] != -1 && i * t >= h[q]) return i * t - h[q];
    q = 1ll * q * cur % p;
  }
  return -1;
}
