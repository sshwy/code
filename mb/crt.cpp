long long mul(
    long long a, long long b, long long p) { // 这个函数貌似目前只支持非负整数
  if (a <= 1000000000 && b <= 1000000000) return a * b % p;
  long long res = 0;
  while (b) {
    if (b & 1) res = (res + a) % p;
    a = a * 2 % p, b >>= 1;
  }
  return res;
}

long long exgcd(long long a, long long b, long long &x, long long &y) {
  if (!b) return x = 1, y = 0, a;
  long long t = exgcd(b, a % b, y, x);
  return y = y - (a / b) * x, t;
}

// v_i(A, B): x = A (mod B)
pair<long long, long long> go(vector<pair<long long, long long>> v) {
  long long b = 0, a = 1; // x=0 mod 1
  for (auto p : v) {
    long long a1, b1, k, k1;
    a1 = p.second;
    b1 = p.first;
    long long g = exgcd(a, a1, k, k1), d = ((b1 - b) % a1 + a1) % a1;
    if (d % g) return make_pair(-1, -1);
    k = mul(k, d / g, a1);
    // 然后合并方程
    b = b + a * k, a = a / g * a1, b = (b + a) % a;
  }
  return make_pair((b + a) % a, a);
}
