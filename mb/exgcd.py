def exgcd(a,b): # return (x,y,gcd(a,b)) that ax+by=gcd(a,b)
    if b==0:
        return [1,0,a]
    t=exgcd(b,a%b)
    tt=t[0]
    t[0]=t[1]
    t[1]=tt
    t[1]=t[1]-(a//b)*t[0]
    return t
