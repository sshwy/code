// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

/**
 * 最大费用循环流
 * 模型：min( sum c(u, v) max(w(u, v) + h[u] - h[v], 0) + sum h[u] * c[u] )
 * ref:
 * https://notes.sshwy.name/Math/Linear-Algbra/LP-and-its-Dual/#%E7%AE%80%E5%8C%96%E5%AF%B9%E5%81%B6%E9%97%AE%E9%A2%98-1
 *
 * Usage:
 *
 * ```cpp
 * const int N = 1005;
 * CyclicNetworkFlow<N> net;
 * int n, m;
 * int c[N], coef[N];
 *
 * int main () {
 *   scanf("%d%d", &n, &m);
 *   FOR(i, 1, n) scanf("%d", c + i);
 *
 *   net.setN(n + 1);
 *   net.INF = 10010;
 *   FOR(i, 1, m) {
 *     int l, r, d;
 *     scanf("%d%d%d", &l, &r, &d);
 *     net.addNetworkEdge(l, r + 1, net.INF, d);
 *   }
 *   FOR(i, 1, n) {
 *     net.addNetworkEdge(i, i + 1, net.INF, 0);
 *   }
 *   FOR(i, 0, n) {
 *     coef[i + 1] = c[i] - c[i + 1];
 *   }
 *   net.setValues(coef);
 *
 *   long long ans = net.flow();
 *   printf("%lld\n", ans);
 *   return 0;
 * }
 * ```
 */
template <const int N> struct CyclicNetworkFlow {
  struct Edge {
    int to, cap, cost, nex;
  };
  struct NetworkEdge {
    int u, v, cap, cost;
  };

  int n, n_s, n_t, h[N], h2[N], exc[N], c[N], INF;
  vector<Edge> g;
  vector<NetworkEdge> network;

  CyclicNetworkFlow() { INF = 0x3f3f3f3f; }
  void setN(int _n) { n = _n; }
  void setValues(int *vs) { FOR(i, 1, n) c[i] = vs[i]; }
  void addGraphEdge(int u, int v, int cap, int cost) {
    g.pb({v, cap, cost, h[u]});
    h[u] = g.size() - 1;
  }
  void setNetworkEdge(int u, int v, int cap, int cost) {
    addGraphEdge(u, v, cap, cost);
    addGraphEdge(v, u, 0, -cost);
  }
  void addNetworkEdge(int u, int v, int cap, int cost) {
    assert(1 <= u && u <= n && 1 <= v && v <= n);
    network.pb({u, v, cap, cost});
  }
  long long initGraph() {
    memset(h, -1, sizeof(h));
    memset(exc, 0, sizeof(exc));
    g.clear();
    n_s = 0, n_t = n + 1;
    long long totcost = 0;
    for (auto e : network) {
      if (e.cost > 0) {
        exc[e.v] += e.cap, exc[e.u] -= e.cap;
        totcost += 1ll * e.cap * e.cost;
        setNetworkEdge(e.u, e.v, e.cap, e.cost);
      } else {
        setNetworkEdge(e.v, e.u, e.cap, -e.cost);
      }
    }
    FOR(i, 1, n) {
      if (exc[i] < c[i]) {
        setNetworkEdge(n_s, i, c[i] - exc[i], 0);
      } else if (exc[i] > c[i]) {
        setNetworkEdge(i, n_t, exc[i] - c[i], 0);
      }
    }
    return totcost;
  }
  long long dist[N];
#define for_e(i, u, v, w, c)                                                \
  for (int i = h[u], v, w, c; v = g[i].to, w = g[i].cap, c = g[i].cost, ~i; \
       i = g[i].nex)
#define for_arc(i, u, v, w, c)                                                \
  for (int &i = h2[u], v, w, c; v = g[i].to, w = g[i].cap, c = g[i].cost, ~i; \
       i = g[i].nex)
  bool spfa() { // 不能提前 return
    bool vis[N] = {0};
    memset(dist, 0x3f, sizeof(dist));
    const long long UNVIS = dist[n_t];
    dist[n_s] = 0;

    queue<int> q;
    q.push(n_s), vis[n_s] = true;
    while (!q.empty()) {
      int u = q.front();
      q.pop();
      vis[u] = false;
      for_e(i, u, v, cap, cost) {
        if (!cap || dist[v] <= dist[u] + cost) continue;
        dist[v] = dist[u] + cost;
        if (!vis[v]) q.push(v), vis[v] = true;
      }
    }
    return dist[n_t] != UNVIS;
  }
  long long _mincost;
  bool vis[N];
  int dfs(int u, int flow) {
    if (u == n_t || flow == 0) return flow;
    vis[u] = true;
    int rest = flow;
    for_arc(i, u, v, cap, cost) {
      if (!cap || vis[v] || dist[v] != dist[u] + cost) continue;
      int k = min(cap, rest);
      k = dfs(v, k);
      g[i].cap -= k, g[i ^ 1].cap += k, _mincost += 1ll * k * cost;
      rest -= k;
      if (rest == 0) break;
    }
    return flow - rest;
  }
#undef for_e
#undef for_arc
  long long flow() {
    _mincost = 0;
    long long totcost = initGraph();
    while (spfa()) {
      memcpy(h2, h, sizeof(h));
      while (memset(vis, 0, sizeof(vis)), dfs(n_s, INT_MAX))
        ;
    }
    return totcost - _mincost;
  }
};
