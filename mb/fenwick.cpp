/*
 * Copyright: Sshwy
 * 树状数组
 * SZ是长度，T是数据类型，iv是初始值，mxlg用于lowerBound函数
 * lowerBound(x): 找到前缀和第一个 >= x 的位置
 * upperBound(x): 找到前缀和第一个 > x 的位置
 */
template <const int SZ, class T = int, const T iv = 0, const int mxlg = 30>
class fenwick {
  T c[SZ], sum;

public:
  fenwick() { fill(c, c + SZ, iv), sum = iv; }
  T &operator[](int x) { return c[x]; }
  void add(int pos, T v) {
    for (int i = pos; i < SZ; i += i & -i) c[i] = c[i] + v;
    sum = sum + v;
  }
  T prefixSum(int pos) {
    T res = iv;
    for (int i = pos; i > 0; i -= i & -i) res += c[i];
    return res;
  }
  T suffixSum(int pos) { return sum - prefixSum(pos - 1); }
  int lowerBound(const T sum) {
    int pos = 0;
    T pre = iv;
    ROF(j, mxlg, 0)
    if (pos + (1 << j) < SZ && pre + c[pos + (1 << j)] < sum)
      pre = pre + c[pos + (1 << j)], pos += 1 << j;
    return pos + 1;
  }
  int upperBound(const T sum) {
    int pos = 0;
    T pre = iv;
    ROF(j, mxlg, 0)
    if (pos + (1 << j) < SZ && pre + c[pos + (1 << j)] <= sum)
      pre = pre + c[pos + (1 << j)], pos += 1 << j;
    return pos + 1;
  }
};
