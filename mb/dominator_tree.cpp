/**
 * Dominator Tree
 * O(nm)
 */
int n, cdom[N], fa[N];
vector<int> g[N], dom[N];
bool vis[N];

void ae(int u, int v) { // add tree edge
  fa[v] = u;
}
void dfs(int u, int ban) {
  vis[u] = true;
  for (int v : g[u])
    if (v != ban && !vis[v]) dfs(v, ban);
}
void buildDomTree() {
  FOR(i, 1, n) {
    memset(vis, 0, sizeof(vis));
    if (i != 1) dfs(1, i);
    FOR(j, 1, n) if (!vis[j]) dom[i].pb(j), cdom[j]++;
  }
  queue<int> q;
  q.push(1);
  while (!q.empty()) {
    int u = q.front();
    q.pop();
    cdom[u] = 0;
    for (int v : dom[u]) {
      cdom[v]--;
      if (cdom[v] == 1) {
        ae(u, v);
        q.push(v);
      }
    }
  }
}
