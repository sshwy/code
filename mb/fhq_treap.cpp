template <class T> struct Treap {
  int root, tot;
  int lc[SZ], rc[SZ], sz[SZ];
  unsigned int rnd[SZ];
  T val[SZ];
  void clear() {
    fill(lc, lc + tot + 1, 0);
    fill(rc, rc + tot + 1, 0);
    fill(sz, sz + tot + 1, 0);
    root = tot = 0;
  }
  Treap() {
    root = tot = 0;
    srand(clock() + time(0));
  }
  inline void pushup(int u) { sz[u] = sz[lc[u]] + sz[rc[u]] + 1; }
  void split_upper(int u, const T key, int &x, int &y) { // x:<=key; y:>key
    if (!u) return x = y = 0, void();
    if (val[u] <= key)
      x = u, split_upper(rc[u], key, rc[u], y);
    else
      y = u, split_upper(lc[u], key, x, lc[u]);
    pushup(u);
  }
  void split_lower(int u, const T key, int &x, int &y) { // x:<key; y:>=key
    if (!u) return x = y = 0, void();
    if (val[u] < key)
      x = u, split_lower(rc[u], key, rc[u], y);
    else
      y = u, split_lower(lc[u], key, x, lc[u]);
    pushup(u);
  }
  int merge(int x, int y) { // x<y
    if (!x || !y) return x + y;
    // if(rand()%(sz[x]+sz[y])<sz[x])return rc[x]=merge(rc[x],y), pushup(x), x;
    if (rnd[x] < rnd[y])
      return rc[x] = merge(rc[x], y), pushup(x), x;
    else
      return lc[y] = merge(x, lc[y]), pushup(y), y;
  }
  void insert(const T v) { // 插入 v
    int x, y, u = ++tot;
    val[u] = v, sz[u] = 1, rnd[u] = rand();
    split_upper(root, v, x, y);
    root = merge(merge(x, u), y);
  }
  void remove(const T v) {
    int x, y, z;
    split_lower(root, v, x, y);
    split_upper(y, v, y, z);
    if (!y) assert(0); // 删除不存在的元素
    y = merge(lc[y], rc[y]);
    root = merge(x, merge(y, z));
  }
  int rank(T v) { // 即相同的数中，第一个数的排名
    int x, y, res;
    split_lower(root, v, x, y);
    res = sz[x] + 1, root = merge(x, y);
    return res;
  }
  T kth(int k) { // 查询排名为 k 的数
    int u = root;
    while (k != sz[lc[u]] + 1) {
      if (k <= sz[lc[u]])
        u = lc[u];
      else
        k -= sz[lc[u]] + 1, u = rc[u];
    }
    return val[u];
  }
};
