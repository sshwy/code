template <class T> T exgcd(T a, T b, T &x, T &y) {
  if (!b) return x = 1, y = 0, a;
  T t = exgcd(b, a % b, y, x);
  return y = y - (a / b) * x, t;
}

/*
 * 求ax+by=gcd(a,b)的一组x,y的解，返回gcd(a,b)
 */
