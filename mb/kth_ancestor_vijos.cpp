// https://vijos.org/d/Bashu_OIers/p/5a79a3e1d3d8a103be7e2b81
#include <algorithm>
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 3e5 + 5;

struct qxx {
  int nex, t;
};
qxx e[N * 2];
int h[N], le = 1;
void add_path(int f, int t) { e[++le] = {h[f], t}, h[f] = le; }
#define FORe(i, u, v) for (int i = h[u], v; v = e[i].t, i; i = e[i].nex)

int n, m;

int fa[N][22], dep[N], height[N];
int tp[N];
vector<int> up[N], down[N];

void dfs1(int u, int p) {
  dep[u] = dep[p] + 1;
  height[u] = 1;
  fa[u][0] = p;
  FOR(j, 1, 21) {
    fa[u][j] = fa[fa[u][j - 1]][j - 1];
    if (!fa[u][j]) break;
  }
  FORe(i, u, v) {
    if (v == p) continue;
    dfs1(v, u);
    height[u] = max(height[u], height[v] + 1);
  }
}
void dfs2(int u, int p, int topp) {
  tp[u] = topp;
  int mx = 0, son = -1;
  FORe(i, u, v) if (v != p && height[v] > mx) mx = height[v], son = v;
  if (u == topp)
    down[u].pb(u);
  else
    down[topp].pb(u);
  if (son != -1) dfs2(son, u, topp);
  FORe(i, u, v) if (v != p && v != son) dfs2(v, u, v);
  if (u == topp) {
    up[u].pb(u);
    int lim = height[u], v = u;
    while (fa[v][0] && lim) {
      v = fa[v][0];
      up[u].pb(v);
      --lim;
    }
  }
}
int query(int x, int k) {
  if (k == 0) return x;
  int j = log(k) / log(2);
  x = fa[x][j], k -= 1 << j;
  if (k == 0) return x;
  if (dep[x] - k >= dep[tp[x]]) {
    assert((int)down[tp[x]].size() > dep[x] - dep[tp[x]] - k);
    return down[tp[x]][dep[x] - dep[tp[x]] - k];
  } else {
    assert(dep[tp[x]] + k - dep[x] < (int)up[tp[x]].size());
    return up[tp[x]][dep[tp[x]] + k - dep[x]];
  }
}

int main() {
  scanf("%d", &n);
  FOR(i, 1, n - 1) {
    int x, y;
    scanf("%d%d", &x, &y);
    add_path(x, y);
    add_path(y, x);
  }
  dfs1(1, 0);
  dfs2(1, 0, 1);
  scanf("%d", &m);
  int lastans = 0;
  FOR(i, 1, m) {
    int x, y;
    scanf("%d%d", &x, &y);
    x ^= lastans;
    y ^= lastans;
    assert(1 <= x || x <= n);
    if (dep[x] <= y)
      printf("%d\n", lastans = 0);
    else
      printf("%d\n", lastans = query(x, y));
  }
  return 0;
}
