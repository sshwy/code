const int LSZ = 19, SZ = 1 << LSZ, P = 998244353;
typedef vector<int> poly;

int pw(int a, int m) {
  int res = 1;
  while (m) m & 1 ? res = 1ll * res * a % P : 0, a = 1ll * a * a % P, m >>= 1;
  return res;
}

int tr[SZ];
int init(int n) {
  int len = 1;
  while (len < n) len <<= 1;
  FOR(i, 0, len - 1) tr[i] = tr[i >> 1] >> 1 | ((i & 1) * (len >> 1));
  return len;
}
void dft(poly &f, int len, int tag) {
  assert(f.size() <= len);
  f.resize(len, 0);
  for (int i = 0; i < len; ++i)
    if (i < tr[i]) swap(f[i], f[tr[i]]);
  for (int j = 1; j < len; j <<= 1)
    for (int i = 0, wn = pw(3, (P - 1) + (P - 1) / (j << 1) * tag); i < len;
         i += j << 1)
      for (int k = i, u, v, w = 1; k < i + j; k++, w = w * 1ll * wn % P)
        u = f[k], v = f[k + j] * 1ll * w % P, f[k] = (u + v) % P,
        f[k + j] = (u - v + P) % P;
  if (tag == -1)
    for (int i = 0, il = pw(len, P - 2); i < len; ++i) f[i] = f[i] * 1ll * il % P;
}

poly operator+(const poly &a, const poly &b) {
  poly res(a);
  res.resize(max(a.size(), b.size()), 0);
  for (int i = 0; i < b.size(); ++i) res[i] = (res[i] + b[i]) % P;
  return res;
}
poly operator*(const poly &a, const poly &b) {
  poly A = a, B = b;
  int len = init(A.size() + B.size());
  poly C(len, 0);
  dft(A, len, 1), dft(B, len, 1);
  FOR(i, 0, len - 1) C[i] = A[i] * 1ll * B[i] % P;
  dft(C, len, -1);
  // for(auto x:C)cout<<x<<" "; cout<<"P"<<endl;
  return C;
}
