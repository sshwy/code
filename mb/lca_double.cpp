/******************heading******************/
const int N = 1e5 + 5;

int n;
vector<int> g[N];
int fa[N][20], dep[N];

int lca(int u, int v) {
  if (dep[u] < dep[v]) swap(u, v);
  ROF(j, 19, 0) if (dep[u] - (1 << j) >= dep[v]) u = fa[u][j];
  if (u == v) return u;
  ROF(j, 19, 0) if (fa[u][j] != fa[v][j]) u = fa[u][j], v = fa[v][j];
  return fa[u][0];
}
int dist(int u, int v) {
  int z = lca(u, v);
  return dep[u] + dep[v] - dep[z] * 2;
}
void dfs(int u, int p) {
  dep[u] = dep[p] + 1;
  fa[u][0] = p;
  FOR(j, 1, 19) {
    fa[u][j] = fa[fa[u][j - 1]][j - 1];
    if (!fa[u][j]) break;
  }
  for (int v : g[u])
    if (v != p) { dfs(v, u); }
}
