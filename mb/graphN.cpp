namespace GraphN {
  struct qxx {
    int nex, t;
  };
  template <const int N, const int M> class Graph {
  private:
    qxx e[M];
    int h[N], le;

  public:
    class iterator {
    private:
      qxx *_e;
      int _id;

    public:
      iterator() { _e = NULL, _id = 0; }
      iterator(qxx *E, int ID) { _e = E, _id = ID; }
      bool operator!=(const iterator &it) const {
        return _id != it._id || _e != it._e;
      }
      inline int operator*() const { return (_e + _id)->t; } // end point
      inline int operator&() const { return _id; }           // id
      inline iterator operator~() const {
        return iterator(_e, _id ^ 1);
      } // reverse edge
      const iterator &operator++() { return _id = (_e + _id)->nex, *this; }
      inline iterator begin() { return *this; }
      inline iterator end() { return iterator(_e, 0); }
      inline iterator reverse_edge() {
        return iterator(_e, _id ^ 1);
      } // usually use with add_both
      inline int id() { return _id; }
    };
    vector<iterator> ei[N];
    void init() { fill(h, h + N, 0), le = 1; }
    Graph() { init(); }
    inline void add_path(int f, int t) {
      e[++le] = {h[f], t}, h[f] = le, ei[f].push_back(iterator(e, le));
    }
    inline void add_both(int f, int t) { add_path(f, t), add_path(t, f); }
    inline iterator operator[](int u) { return iterator(e, h[u]); }
    const vector<iterator> &edges(int u) {
      return ei[u];
    } // for(iterator e:g.edges(u))
  };
} // namespace GraphN
using GraphN::Graph;
