/**
 * Suffix Array
 * - for lowercase letters;
 * - index start from 1;
 * - make sure SZ >= n*2
 * - h[i] means LCP(i, sa[rk[i]-1])
 * - st: ST for h[sa[i]]
 */
template <const int SZ, const int LOG_SZ = 20> struct SuffixArray {
  char s[SZ];
  int n, bin[SZ], sa[SZ], rk[SZ], tmp[SZ], h[SZ];
  int st[LOG_SZ][SZ], lg2[SZ];
  void sortTmpByRank() {
    int lb = max(27, n);
    fill(bin, bin + lb + 1, 0);
    FOR(i, 1, n) bin[rk[i]]++;
    FOR(i, 1, lb) bin[i] += bin[i - 1];
    ROF(i, n, 1) sa[bin[rk[tmp[i]]]--] = tmp[i];
  }
  void make(char *_s) { // _s index start from 0
    n = strlen(_s), s[n + 1] = 0;
    FOR(i, 1, n) s[i] = _s[i - 1];

    FOR(i, 1, n) tmp[i] = i, rk[i] = s[i] - 'a' + 1; // rk[i] must > 0 (rank '\0' = 0)
    sortTmpByRank();

    for (int j = 1; j <= n; j <<= 1) {
      int tot = 0;
      FOR(i, n - j + 1, n) tmp[++tot] = i;
      FOR(i, 1, n) if (sa[i] - j > 0) tmp[++tot] = sa[i] - j;
      sortTmpByRank();
      tmp[sa[1]] = 1, tot = 1;
      FOR(i, 2, n) {
        if (rk[sa[i]] == rk[sa[i - 1]] && rk[sa[i] + j] == rk[sa[i - 1] + j])
          tmp[sa[i]] = tot;
        else
          tmp[sa[i]] = ++tot;
      }
      swap(tmp, rk);
    }
  }
  int moveH(int x, int y, int step) {
    while (x + step <= n && y + step <= n && s[x + step] == s[y + step]) ++step;
    return step;
  }
  void calcH() {
    FOR(i, 1, n) {
      if (rk[i] == 1)
        h[i] = 0;
      else
        h[i] = moveH(i, sa[rk[i] - 1], max(0, h[i - 1] - 1));
    }
  }
  void workST() {
    FOR(i, 1, n) st[0][i] = h[sa[i]];
    FOR(i, 1, n) lg2[i] = i == 1 ? 0 : lg2[i / 2] + 1;
    for (int j = 1; (1 << j) <= n; ++j) {
      FOR(i, 1, n - (1 << j) + 1) {
        st[j][i] = min(st[j - 1][i], st[j - 1][i + (1 << (j - 1))]);
      }
    }
  }
  int lcp(int x, int y) {
    if (x == y) return n - x + 1;
    x = rk[x], y = rk[y];
    if (x > y) swap(x, y);
    ++x;
    int j = lg2[y - x + 1];
    return min(st[j][x], st[j][y - (1 << j) + 1]);
  }
  void info() {
    printf("String: %s\n", s + 1);
    printf("rk:     ");
    FOR(i, 1, n) printf("%-3d%c", rk[i], " \n"[i == n]);
    printf("sa:     ");
    FOR(i, 1, n) printf("%-3d%c", sa[i], " \n"[i == n]);
  }
};
