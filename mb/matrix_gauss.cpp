const int GE = MTX * 2;
bool Gauss(double g[GE][GE], int lg) {
  for (int i = 1; i <= lg; i++) {   // 消第 i 个元
    for (int j = i; j <= lg; j++) { // 找到第 i 个元系数不为 0 的方程
      if (fabs(g[j][i]) > 1e-8) {
        for (int k = 0; k <= lg; k++) swap(g[j][k], g[i][k]);
        break;
      }
    }
    if (fabs(g[i][i]) < 1e-8) return 0; // 无解
    // 消元
    for (int j = 1; j <= lg; j++) { // 消去第 j 个方程的元
      if (i == j) continue;
      double rate = g[j][i] / g[i][i];
      // 由于当前方程前 i-1 项系数都为 0 故可以不考虑
      for (int k = i; k <= lg; k++) g[j][k] -= g[i][k] * rate;
      g[j][0] -= g[i][0] * rate;
    }
  }
  return 1;
}

int t[N][N];
int det(const int f[N][N], int n) {
  memcpy(t, f, sizeof(int) * N * N);
  int sig = 1;
  FOR(i, 1, n) {
    FOR(j, i + 1, n) if (t[j][i]) {
      FOR(k, 1, n) swap(t[i][k], t[j][k]);
      sig = -sig;
      break;
    }
    if (!t[i][i]) return 0;
    FOR(j, 1, n) {
      if (j == i) continue;
      int rate = t[j][i] * 1ll * pw(t[i][i], P - 2) % P;
      FOR(k, i, n) t[j][k] = (t[j][k] - 1ll * t[i][k] * rate % P + P) % P;
    }
  }
  int res = 1;
  FOR(i, 1, n) res = 1ll * res * t[i][i] % P;
  res = (res * sig + P) % P;
  return res;
}
int F[N][N * 2];
bool inv(const int f[N][N], int g[N][N], int n) {
  FOR(i, 1, n) FOR(j, 1, n) F[i][j] = f[i][j], F[i][j + n] = i == j;
  FOR(i, 1, n) {
    FOR(j, i, n) if (F[j][i]) {
      FOR(k, 1, n * 2) swap(F[i][k], F[j][k]);
      break;
    }
    if (!F[i][i]) return 0;
    FOR(j, 1, n) {
      if (j == i) continue;
      int rate = F[j][i] * 1ll * pw(F[i][i], P - 2) % P;
      FOR(k, i, n * 2) F[j][k] = (F[j][k] - 1ll * F[i][k] * rate % P + P) % P;
    }
  }
  FOR(i, 1, n) {
    int x = pw(F[i][i], P - 2);
    FOR(j, 1, n * 2) F[i][j] = F[i][j] * 1ll * x % P;
  }
  FOR(i, 1, n) FOR(j, 1, n) g[i][j] = F[i][j + n];
  return 1;
}
