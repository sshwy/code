// by Sshwy
//
// 使用生日攻击卡双哈希。即求出S!=T，h(S)=h(T)的串。哈希函数请注意要修改成对应的那个
//
//#define DEBUGGER
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

#ifdef DEBUGGER

#define log_prefix()                                                \
  {                                                                 \
    fprintf(stderr, "\033[37mLine %-3d [%dms]:\033[0m  ", __LINE__, \
        (int)clock() / 1000);                                       \
  }
#define ilog(...) \
  { fprintf(stderr, __VA_ARGS__); }
#define llog(...)      \
  {                    \
    log_prefix();      \
    ilog(__VA_ARGS__); \
  }
#define log(...)           \
  {                        \
    log_prefix();          \
    ilog(__VA_ARGS__);     \
    fprintf(stderr, "\n"); \
  }
#define red(...)       \
  {                    \
    log_prefix();      \
    ilog("\033[31m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define green(...)     \
  {                    \
    log_prefix();      \
    ilog("\033[32m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define blue(...)      \
  {                    \
    log_prefix();      \
    ilog("\033[34m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }

#else

#define log_prefix() ;
#define ilog(...) ;
#define llog(...) ;
#define log(...) ;
#define red(...) ;
#define green(...) ;
#define blue(...) ;

#endif

namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
#include "hasher.h"

using namespace RA;

vector<int> random_vector(int l, const vector<int> alphabet) {
  vector<int> res;
  int sz = alphabet.size();
  FOR(i, 1, l) res.pb(alphabet[r(sz)]);
  return res;
}
vector<vector<int>> birth(int l, int mod, const vector<int> alp) {
  // int k=1+sqrt(1.3862943*mod);
  int k = 1 + sqrt(2 * mod);
  vector<vector<int>> res;
  FOR(i, 1, k) res.pb(random_vector(l, alp));
  return res;
}
pair<vector<int>, vector<int>> birth_attack(
    int base, int mod, const vector<int> alp) {
  cout << "\033[34mBirth Attack: base=" << base << ", mod=" << mod << ", alp={ ";
  for (auto x : alp) cout << x << " ";
  cout << "}\033[0m" << endl;
  int times = 5;
  FOR(l, 1, 50) {
    cout << "Try l=" << l << endl;
    FOR(j, 1, times) {
      auto v = birth(l, mod, alp);
      map<int, vector<int>> S;
      for (auto s : v) {
        int hs = hasher::hsh(s, base, mod);
        if (S.count(hs) && S[hs] != s) {
          cout << "\033[32mEqual-length Collision!\033[0m" << endl
               << "base=" << base << ", mod=" << mod << ", l=" << l << endl;
          return {S[hs], s};
        } else
          S[hs] = s;
      }
    }
  }
  cout << "Failed." << endl;
  return {{}, {}};
}

int main() {
  srand(__builtin_ia32_rdtsc() + clock());

  int b1 = 13331, mod1 = 998244353;
  int b2 = 131, mod2 = 1000000007;

  auto p = birth_attack(b1, mod1, stov("abdefghijklmnopqrstuvwxyz"));
  auto sx = p.fi, sy = p.se;

  int hx = hasher::hsh(sx, b2, mod2), hy = hasher::hsh(sy, b2, mod2);
  assert(sx.size() == sy.size());

  //以上次生日攻击得到的串为字符集，做第二次生日攻击
  vector<int> vlp = {hx, hy};
  int bb2 = pw(b2, sx.size(), mod2);
  auto p2 = birth_attack(bb2, mod2, vlp);

  vector<int> S, T;
  for (auto x : p2.fi) S += x == hx ? sx : sy;
  for (auto x : p2.se) T += x == hx ? sx : sy;

  cout << vtos(S) << endl << vtos(T) << endl;

  assert(hasher::hsh(S, b1, mod1) == hasher::hsh(T, b1, mod1));
  assert(hasher::hsh(S, b2, mod2) == hasher::hsh(T, b2, mod2));

  return 0;
}
