// by Sshwy
//#define DEBUGGER
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

#ifdef DEBUGGER

#define log_prefix()                                                \
  {                                                                 \
    fprintf(stderr, "\033[37mLine %-3d [%dms]:\033[0m  ", __LINE__, \
        (int)clock() / 1000);                                       \
  }
#define ilog(...) \
  { fprintf(stderr, __VA_ARGS__); }
#define llog(...)      \
  {                    \
    log_prefix();      \
    ilog(__VA_ARGS__); \
  }
#define log(...)           \
  {                        \
    log_prefix();          \
    ilog(__VA_ARGS__);     \
    fprintf(stderr, "\n"); \
  }
#define red(...)       \
  {                    \
    log_prefix();      \
    ilog("\033[31m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define green(...)     \
  {                    \
    log_prefix();      \
    ilog("\033[32m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define blue(...)      \
  {                    \
    log_prefix();      \
    ilog("\033[34m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }

#else

#define log_prefix() ;
#define ilog(...) ;
#define llog(...) ;
#define log(...) ;
#define red(...) ;
#define green(...) ;
#define blue(...) ;

#endif

namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int P = 998244353;
const int N = 52, K = 1002;
int n, k;

long long pw(int a, int m) {
  int res = 1;
  while (m) m & 1 ? res = 1ll * res * a % P : 0, a = 1ll * a * a % P, m >>= 1;
  return res;
}
long long inv(int a) { return pw(a, P - 2); }
long long s[N * K], q[N * K], fac[N * K], fnv[N * K], inv_n[N * K];
long long binom(int a, int b) {
  if (a < b) return 0;
  return fac[a] * fnv[b] % P * fnv[a - b] % P;
}
long long sig(int x) { return 1 - 2 * (x & 1); }

int main() {
  scanf("%d%d", &n, &k);
  int lim = n * k;

  fac[0] = 1;
  FOR(i, 1, lim) fac[i] = fac[i - 1] * i % P;
  fnv[lim] = pw(fac[lim], P - 2);
  ROF(i, lim, 1) fnv[i - 1] = fnv[i] * i % P;
  inv_n[0] = 1, inv_n[1] = inv(n);
  FOR(i, 2, lim) inv_n[i] = inv_n[i - 1] * inv_n[1] % P;

  s[0] = 1;
  long long f1 = 0, f2 = 0;
  FOR(j, 0, n - 1) {
    int len = j * k, l2 = (j - 1) * k;
    if (j > 0) {
      long long coef = inv_n[k - 1] * fnv[k - 1] % P;
      fill(q, q + k - 1, 0);
      FOR(i, 0, l2) q[i + k - 1] = coef * s[i] % P;
      s[0] = 1;
      FOR(i, 0, len - 1)
      s[i + 1] = (s[i] - q[i]) * j % P * inv_n[1] % P * fnv[i + 1] % P * fac[i] % P;
    }
    int m = n - 1 - j;
    long long c2 = n;
    long long c3 = pw(c2, k - 1);
    long long c4 = inv(n - m);
    long long c5 = pw(c4, k - 1);
    FOR(i, 0, len) {
      int c = i + k - 1;
      long long coef =
          binom(n - 1, j) * sig(j) * s[i] % P * inv_n[k] % P * fnv[k - 1] % P;
      coef = coef * fac[c] % P;
      // e^{m/n x} x^c/c! coef
      c3 = c3 * c2 % P; // n^{c+1}
      c5 = c5 * c4 % P; // 1/ (n-m)^{c+1}
      f1 = (f1 + coef * c3 % P * c5) % P;
      f2 = (f2 + coef * c3 % P * (n * c + m) % P * c5 % P * c4) % P;
    }
  }
  f1 = (f1 + f2) % P;
  f1 = (f1 + P) % P;
  f1 = f1 * n % P;
  printf("%lld\n", f1);
  return 0;
}
