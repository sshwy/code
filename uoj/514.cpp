// by Sshwy
#define DEBUGGER
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

#ifdef DEBUGGER

#define log_prefix()                                                \
  {                                                                 \
    fprintf(stderr, "\033[37mLine %-3d [%dms]:\033[0m  ", __LINE__, \
        (int)clock() / 1000);                                       \
  }
#define ilog(...) \
  { fprintf(stderr, __VA_ARGS__); }
#define llog(...)      \
  {                    \
    log_prefix();      \
    ilog(__VA_ARGS__); \
  }
#define log(...)           \
  {                        \
    log_prefix();          \
    ilog(__VA_ARGS__);     \
    fprintf(stderr, "\n"); \
  }
#define red(...)       \
  {                    \
    log_prefix();      \
    ilog("\033[31m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define green(...)     \
  {                    \
    log_prefix();      \
    ilog("\033[32m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define blue(...)      \
  {                    \
    log_prefix();      \
    ilog("\033[34m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }

#else

#define log_prefix() ;
#define ilog(...) ;
#define llog(...) ;
#define log(...) ;
#define red(...) ;
#define green(...) ;
#define blue(...) ;

#endif

namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int P = 998244353, N = 252;
int pw(int a, int m) {
  int res = 1;
  while (m) m & 1 ? res = 1ll * res * a % P : 0, a = 1ll * a * a % P, m >>= 1;
  return res;
}
int n, a, b, fac[N * N], fnv[N * N];
int s[N][N * N], q[N * N], inv_n[N * N];

void add(int &x, long long y) { x = (x + y) % P; }
long long binom(int a, int b) {
  if (a < b) return 0;
  return fac[a] * 1ll * fnv[b] % P * fnv[a - b] % P;
}
long long sig(int x) { return 1 - 2 * (x & 1); }
int main() {
  scanf("%d%d%d", &n, &a, &b);

  fac[0] = 1;
  int lim = n * b;
  FOR(i, 1, lim) fac[i] = 1ll * fac[i - 1] * i % P;
  fnv[lim] = pw(fac[lim], P - 2);
  ROF(i, lim, 1) fnv[i - 1] = fnv[i] * 1ll * i % P;
  inv_n[0] = 1, inv_n[1] = pw(n, P - 2);
  FOR(i, 2, lim) inv_n[i] = inv_n[i - 1] * 1ll * inv_n[1] % P;
  // log("GG");

  s[0][0] = 1; // s^0
  int ans = 0;
  FOR(j, 1, n - 1) {
    int l1 = (j - 1) * (b - 1);
    int l2 = j * (b - 1);
    fill(q, q + b - 1, 0);
    FOR(i, 0, l1) {
      assert(i + b - 1 <= l2);
      q[i + b - 1] = s[j - 1][i] * 1ll * fnv[b - 1] % P;
    }
    s[j][0] = 1;
    FOR(i, 0, l2 - 1) {
      s[j][i + 1] =
          (1ll * j * (s[j][i] - q[i]) % P * fnv[i + 1] % P * fac[i] % P + P) % P;
    }
    int d = n - 1 - j;
    long long c3 = n * 1ll * fnv[n - d] % P * fac[n - d - 1] % P;
    long long c2 = pw(c3, a - 1);
    FOR(i, 0, l2) {
      long long coef = s[j][i] * sig(j + 1) * binom(n - 1, j) % P;
      int c = i + a - 1;
      // v^{c} u^{d} * coef
      coef = coef * inv_n[c] % P;
      coef = coef * fac[c] % P;
      // x^c/c! * e^{d/n*x} * coef
      assert(d < n);
      c2 = c2 * c3 % P;
      // c2 = pw(n,c+1)*pw(1/(n-d),c+1)
      add(ans, c2 * coef);
    }
  }
  add(ans, P);
  ans = ans * 1ll * fnv[a - 1] % P;
  printf("%d\n", ans);
  return 0;
}
