#include <bits/stdc++.h>
#define J long long
#define F double
#define E return
#define L x << 1
#define R x << 1 | 1
using std::max;
char U[1 << 18], *u = U + sizeof U - 1;
bool H;
#define g (++u == U + sizeof U ? fread(u = U, 1, sizeof U, stdin) : 0)
void G(int &x) {
  while (*u < 45) g;
  H = *u == 45 ? g, 1 : 0;
  x = 0;
  for (; *u > 47; g) x = x * 10 + *u - 48;
  H ? x = -x : 0;
}
const int N = 5e5 + 7, M = 1 << 19, P = 998244353;
struct l {
  int k, b;
};
struct D {
  l t;
  F p;
} B[6 << 20], *e = B, *f[M * 2];
int c[M * 2];
F C(l a, l b) { E(a.b - b.b) * 1. / (b.k - a.k); }
void p(const l &t, int &Q) {
  if (!Q) {
    e[Q++].t = t;
    E;
  }
  if (t.k == e[Q - 1].t.k) E;
  if (Q < 2) {
    e[Q - 1].p = C(e[Q - 1].t, t);
    e[Q++].t = t;
    E;
  }
  for (; Q > 1 && C(e[Q - 2].t, t) < e[Q - 2].p; Q--)
    ;
  e[Q - 1].p = C(e[Q - 1].t, t);
  e[Q++].t = t;
}
void O(int x) {
  if (f[x]) E;
  int Q = 0, i = 0, j = 0;
  for (; i < c[L]; p(f[L][i++].t, Q))
    for (; j < c[R] && (f[R][j].t.k < f[L][i].t.k ||
                           (f[R][j].t.k == f[L][i].t.k && f[R][j].t.b > f[L][i].t.b));
         p(f[R][j++].t, Q))
      ;
  for (; j < c[R]; p(f[R][j++].t, Q))
    ;
  e[Q - 1].p = 1e20, f[x] = e, c[x] = Q, e += Q;
}
J Y(int x, int a, int b) {
  if (!f[x]) E max(Y(L, a, b), Y(R, a, b));
  int l = -1, r = c[x] - 1;
  F t = a * 1. / b;
  while (l + 1 < r) {
    int i = (l + r) >> 1;
    if (t < f[x][i].p)
      r = i;
    else
      l = i;
  }
  E(J) f[x][r].t.k *a + (J)f[x][r].t.b *b;
}
void S(int m) {
  int o, a, b, x, y, n = 0, A = 0, i;
  while (m--) {
    G(o);
    if (o == 2)
      for (i = n-- + M; i; i /= 2) f[i] = 0;
    else {
      G(a), G(b);
      if (o == 1) {
        *e = (D){a, b, 1e20};
        i = ++n + M;
        f[i] = e++, c[i] = 1;
        if (~i & 1)
          for (i /= 2; i > 1; i /= 2) {
            O(i - 1);
            if (i & 1) break;
          }
      } else {
        G(y), G(x);
        J ta = -9e18;
        x = -x;
        for (a += M - 1, b += M + 1; a ^ b ^ 1; a /= 2, b /= 2) {
          if (~a & 1) ta = max(ta, Y(a ^ 1, x, y));
          if (b & 1) ta = max(ta, Y(b ^ 1, x, y));
        }
        A ^= (ta % P + P) % P;
        // printf("%d\n",(ta%P+P)%P);
      }
    }
  }
  for (; n; n--)
    for (i = n + M; i; i /= 2) f[i] = 0;
  e = B;
  printf("%d\n", A);
}
int main() {
  g;
  int m;
  for (G(m); G(m), m; S(m))
    ;
}
