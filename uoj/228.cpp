#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
namespace IO {
  char nc() {
    static char bf[100000], *p1 = bf, *p2 = bf;
    return p1 == p2 && (p2 = (p1 = bf) + fread(bf, 1, 100000, stdin), p1 == p2)
               ? EOF
               : *p1++;
  }
  int rd() {
    int res = 0;
    char c = getchar();
    while (!isdigit(c)) c = getchar();
    while (isdigit(c)) res = res * 10 + c - '0', c = getchar();
    return res;
  }
} // namespace IO
/******************heading******************/
#define int lld
const int N = 1e5 + 5, SZ = N << 2, INF = 0x3f3f3f3f;

int n, m;

struct node {
  int l, r;
  int mx, mn;
  lld s;
  int tg, stg, ctg;
  node() { s = l = r = mx = mn = tg = stg = 0, ctg = INF; }
  void assign(int x) { s = mx = mn = x; }
  void add(int v) {
    s += v * (r - l + 1ll);
    mx += v, mn += v;
    tg += v;
  }
  void cover(int v) {
    s = v * (r - l + 1ll);
    mx = mn = v;
    ctg = v;
    stg = tg = 0; //清除标记
  }
  void sqrt() {
    int mx2 = ::sqrt(mx), mn2 = ::sqrt(mn);
    // assert(1ll*mx2*mx2<=mx&&(mx2+1ll)*(mx2+1ll)>mx);
    // assert(1ll*mn2*mn2<=mn&&(mn2+1ll)*(mn2+1ll)>mn);
    if (mx2 == mn2)
      cover(mx2);
    else
      add(mx2 - mx);
    // s-=(mx-mx2)*(r-l+1ll);
  }
  // void printnode(){ printf("[%lld,%lld]: mx=%lld, mn=%lld, s=%lld, tg=%lld,
  // stg=%lld\n",l,r,mx,mn,s,tg,stg); }
} t[SZ];
int a[N];

void pushup(int u) {
  t[u].mx = max(t[u << 1].mx, t[u << 1 | 1].mx);
  t[u].mn = min(t[u << 1].mn, t[u << 1 | 1].mn);
  t[u].s = t[u << 1].s + t[u << 1 | 1].s;
}
void pushdown(int u) {
  if (t[u].ctg != INF) {
    t[u << 1].cover(t[u].ctg), t[u << 1 | 1].cover(t[u].ctg);
    t[u].ctg = INF;
  }
  // if(t[u].stg){
  //    FOR(i,1,t[u].stg)t[u<<1].sqrt(),t[u<<1|1].sqrt();
  //    t[u].stg=0;
  //}
  if (t[u].tg) {
    t[u << 1].add(t[u].tg), t[u << 1 | 1].add(t[u].tg);
    t[u].tg = 0;
  }
}
void build(int u = 1, int l = 1, int r = n) {
  t[u].l = l, t[u].r = r;
  if (l == r) return t[u].assign(a[l]);
  int mid = (l + r) >> 1;
  build(u << 1, l, mid), build(u << 1 | 1, mid + 1, r);
  pushup(u);
}
void add(int L, int R, int v, int u = 1) {
  const int l = t[u].l, r = t[u].r;
  if (R < l || r < L) return;
  if (L <= l && r <= R) return t[u].add(v);
  pushdown(u);
  add(L, R, v, u << 1), add(L, R, v, u << 1 | 1);
  pushup(u);
}
void sqrt(int L, int R, int u = 1) {
  const int l = t[u].l, r = t[u].r;
  if (R < l || r < L) return;
  // printf("sqrt(%lld,%lld,%lld:[%lld,%lld])\n",L,R,u,t[u].l,t[u].r);
  if (L <= l && r <= R && t[u].mx - t[u].mn <= 1) return t[u].sqrt();
  pushdown(u);
  sqrt(L, R, u << 1), sqrt(L, R, u << 1 | 1);
  pushup(u);
  // printf("t[%lld].s=%lld\n",u,t[u].s);
}
lld qsum(int L, int R, int u = 1) {
  const int l = t[u].l, r = t[u].r;
  if (R < l || r < L) return 0ll;
  if (L <= l && r <= R) return t[u].s;
  pushdown(u);
  return qsum(L, R, u << 1) + qsum(L, R, u << 1 | 1);
}

signed main() {
  scanf("%lld%lld", &n, &m);
  FOR(i, 1, n) scanf("%lld", &a[i]);
  build();
  FOR(i, 1, m) {
    int typ, l, r, x;
    scanf("%lld%lld%lld", &typ, &l, &r);
    if (typ == 1) {
      scanf("%lld", &x);
      // printf("\033[32madd(%lld,%lld,%lld)\033[0m\n",l,r,x);
      add(l, r, x);
    } else if (typ == 2) {
      // printf("\033[32msqrt(%lld,%lld)\033[0m\n",l,r);
      sqrt(l, r);
    } else {
      // printf("\033[32mqsum(%lld,%lld)\033[0m\n",l,r);
      printf("%lld\n", qsum(l, r));
    }
  }
  return 0;
}
