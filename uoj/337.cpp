// by Sshwy
#define DEBUGGER
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

#ifdef DEBUGGER

#define log_prefix()                                                \
  {                                                                 \
    fprintf(stderr, "\033[37mLine %-3d [%dms]:\033[0m  ", __LINE__, \
        (int)clock() / 1000);                                       \
  }
#define ilog(...) \
  { fprintf(stderr, __VA_ARGS__); }
#define llog(...)      \
  {                    \
    log_prefix();      \
    ilog(__VA_ARGS__); \
  }
#define log(...)           \
  {                        \
    log_prefix();          \
    ilog(__VA_ARGS__);     \
    fprintf(stderr, "\n"); \
  }
#define red(...)       \
  {                    \
    log_prefix();      \
    ilog("\033[31m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define green(...)     \
  {                    \
    log_prefix();      \
    ilog("\033[32m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define blue(...)      \
  {                    \
    log_prefix();      \
    ilog("\033[34m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }

#else

#define log_prefix() ;
#define ilog(...) ;
#define llog(...) ;
#define log(...) ;
#define red(...) ;
#define green(...) ;
#define blue(...) ;

#endif

namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
int xxx = 0;
const int N = 5e4 + 5;

struct disjoint {
  int fa[N];
  int get(int u) { return fa[u] == u ? u : fa[u] = get(fa[u]); }
  void merge(int u, int v) { fa[get(u)] = get(v); }
} D[300];

int n, q, T;
long long a[N];

vector<int> g[N];
int sz[N], dep[N], lon[N], hei[N], fa[N][20], f0[N], htop[N], totdfn;
vector<int> up[N], dn[N];
void dfs1(int u, int p) {
  sz[u] = 1, dep[u] = dep[p] + 1, lon[u] = 0, fa[u][0] = p, f0[u] = p;
  for (int j = 1; fa[u][j - 1] && j < 20; j++) fa[u][j] = fa[fa[u][j - 1]][j - 1];
  for (int v : g[u])
    if (v != p) dfs1(v, u), (!lon[u] || hei[lon[u]] < hei[v]) ? lon[u] = v : 0;
  hei[u] = lon[u] ? hei[lon[u]] + 1 : 1;
}
void dfs3(int u, int p, int htp) {
  htop[u] = htp;
  if (u == htp) {
    for (int v = u; v; v = lon[v]) dn[u].pb(v);
    for (int v = u; v && up[u].size() < dn[u].size(); v = f0[v]) up[u].pb(v);
  }
  if (lon[u]) dfs3(lon[u], u, htp);
  for (int v : g[u])
    if (v != p && v != lon[u]) dfs3(v, u, v);
}
int lca(int u, int v) {
  if (dep[u] < dep[v]) swap(u, v);
  ROF(j, 19, 0) if (dep[u] - (1 << j) >= dep[v]) u = fa[u][j];
  if (u == v) return u;
  ROF(j, 19, 0) if (fa[u][j] != fa[v][j]) u = fa[u][j], v = fa[v][j];
  return fa[u][0];
}
int highbit[N];
int kthanc(int u, int k) {
  if (dep[u] <= k) return 0;
  if (k == 0) return u;
  u = fa[u][highbit[k]], k -= 1 << highbit[k];
  int d = dep[u] - k - dep[htop[u]];
  if (d >= 0)
    return dn[htop[u]][d];
  else
    return up[htop[u]][-d];
}
inline void rem(int u) {
  for (int i = 1, v = f0[u]; v && i <= T; i++, v = f0[v]) D[i].merge(u, v);
}
inline void setSqrt(int u) {
  a[u] = sqrt(a[u]);
  if (a[u] == 1) rem(u);
}
void modify(int s, int t, int k) {
  int z = lca(s, t), len = dep[s] + dep[t] - dep[z] * 2;
  disjoint &d = D[k];
  if (k <= T) {
    for (int u = d.get(s); dep[u] >= dep[z]; u = d.get(kthanc(u, k)))
      if (u != t) setSqrt(u);
    for (int u = d.get(kthanc(t, len - (len - 1) / k * k)); dep[u] > dep[z];
         u = d.get(kthanc(u, k)))
      if (u != t) setSqrt(u);
  } else {
    for (int u = s; dep[u] >= dep[z]; u = kthanc(u, k))
      if (u != t) setSqrt(u);
    for (int u = kthanc(t, len - (len - 1) / k * k); dep[u] > dep[z];
         u = kthanc(u, k))
      if (u != t) setSqrt(u);
  }
  setSqrt(t);
}
long long query(int s, int t, int k) {
  // return 0;
  int z = lca(s, t), len = dep[s] + dep[t] - dep[z] * 2;
  disjoint &d = D[k];
  if (s == t) return a[s];
  long long res = (len - 1) / k + 2;
  if (k <= T) {
    for (int u = d.get(s); dep[u] >= dep[z]; u = d.get(kthanc(u, k))) {
      if (u != t) res += a[u] - 1;
    }
    int x = kthanc(t, len - (len - 1) / k * k);
    for (int u = d.get(x); dep[u] > dep[z]; u = d.get(kthanc(u, k))) {
      if (u != t) res += a[u] - 1;
    }
  } else {
    for (int u = s; dep[u] >= dep[z]; u = kthanc(u, k))
      if (u != t) res += a[u] - 1;
    int x = kthanc(t, len - (len - 1) / k * k);
    for (int u = x; dep[u] > dep[z]; u = kthanc(u, k))
      if (u != t) res += a[u] - 1;
  }
  res += a[t] - 1;
  return res;
}
int main() {
  scanf("%d", &n);
  T = max(1, (int)sqrt(n));
  T = 25;
  FOR(i, 0, T) FOR(j, 0, n) D[i].fa[j] = j;
  FOR(i, 1, n) scanf("%lld", &a[i]);
  FOR(i, 1, n) highbit[i] = i == 1 ? 0 : highbit[i >> 1] + 1;
  FOR(i, 1, n - 1) {
    int u, v;
    scanf("%d%d", &u, &v);
    g[u].pb(v), g[v].pb(u);
  }
  log("GG");
  dfs1(1, 0);
  log("GG");
  dfs3(1, 0, 1);
  log("GG");
  scanf("%d", &q);
  FOR(i, 1, q) {
    int op, s, t, k;
    scanf("%d%d%d%d", &op, &s, &t, &k);
    if (op == 0)
      modify(s, t, k);
    else
      printf("%lld\n", query(s, t, k));
  }
  log("xxx=%d", xxx);
  return 0;
}
