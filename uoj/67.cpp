#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
namespace IO {
  char nc() {
    static char bf[100000], *p1 = bf, *p2 = bf;
    return p1 == p2 && (p2 = (p1 = bf) + fread(bf, 1, 100000, stdin), p1 == p2)
               ? EOF
               : *p1++;
  }
  int rd() {
    int res = 0;
    char c = getchar();
    while (!isdigit(c)) c = getchar();
    while (isdigit(c)) res = res * 10 + c - '0', c = getchar();
    return res;
  }
} // namespace IO
/******************heading******************/
const int N = 1e5 + 5, M = N * 2;

struct qxx {
  int nex, t;
};
qxx e[M];
int h[N], le = 1;
void add_path(int f, int t) { e[++le] = (qxx){h[f], t}, h[f] = le; }

int n, m, cc, tot, tp;
int d[N];
int dfn[N], low[N], totdfn;
bool cut[N];
int ans[N], la;
void tarjan(int u, int p) {
  dfn[u] = low[u] = ++totdfn;
  int cnt = 0;
  for (int i = h[u]; i; i = e[i].nex) {
    const int v = e[i].t;
    if (!dfn[v]) {
      tarjan(v, u);
      low[u] = min(low[u], low[v]);
      cnt += dfn[u] <= low[v];
    } else
      low[u] = min(low[u], dfn[v]);
  }
  if (cnt - (p == 0) >= 1) cut[u] = 1;
}
int main() {
  scanf("%d%d", &n, &m);
  FOR(i, 1, m) {
    int u, v;
    scanf("%d%d", &u, &v);
    add_path(u, v), add_path(v, u);
    d[u]++, d[v]++;
  }
  FOR(i, 1, n) if (!d[i]) tot++, tp = i;
  FOR(i, 1, n) {
    if (!dfn[i] && d[i]) {
      tarjan(i, 0);
      cc++;
    }
  }
  if (cc >= 3) return puts("0"), 0;
  if (cc == 2 && tot == 1) {
    if (m == n - 2)
      return printf("1\n%d", tp), 0;
    else
      return puts("0"), 0;
  }
  FOR(u, 1, n) {
    if (cut[u]) continue;
    if (m - d[u] == n - 2) ans[++la] = u;
  }
  printf("%d\n", la);
  FOR(i, 1, la) printf("%d ", ans[i]);
  return 0;
}
