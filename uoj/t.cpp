// by Sshwy
//#define DEBUGGER
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

#ifdef DEBUGGER

#define log_prefix()                                                \
  {                                                                 \
    fprintf(stderr, "\033[37mLine %-3d [%dms]:\033[0m  ", __LINE__, \
        (int)clock() / 1000);                                       \
  }
#define ilog(...) \
  { fprintf(stderr, __VA_ARGS__); }
#define llog(...)      \
  {                    \
    log_prefix();      \
    ilog(__VA_ARGS__); \
  }
#define log(...)           \
  {                        \
    log_prefix();          \
    ilog(__VA_ARGS__);     \
    fprintf(stderr, "\n"); \
  }
#define red(...)       \
  {                    \
    log_prefix();      \
    ilog("\033[31m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define green(...)     \
  {                    \
    log_prefix();      \
    ilog("\033[32m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define blue(...)      \
  {                    \
    log_prefix();      \
    ilog("\033[34m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }

#else

#define log_prefix() ;
#define ilog(...) ;
#define llog(...) ;
#define log(...) ;
#define red(...) ;
#define green(...) ;
#define blue(...) ;

#endif

namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 5e4 + 5;

struct disjoint {
  int fa[N];
  int get(int u) { return fa[u] == u ? u : fa[u] = get(fa[u]); }
  void merge(int u, int v) { fa[get(u)] = get(v); }
} D[300];

int n, q, T;
int b[N], id[N];
long long a[N];

vector<int> g[N];
int sz[N], dep[N], big[N], top[N], fa[N], totdfn;
void dfs1(int u, int p) {
  sz[u] = 1, dep[u] = dep[p] + 1, big[u] = 0, fa[u] = p;
  for (int v : g[u])
    if (v != p) dfs1(v, u), (!big[u] || sz[big[u]] < sz[v]) ? big[u] = v : 0;
  log("u=%d,sz=%d,dep=%d,big=%d,fa=%d", u, sz[u], dep[u], big[u], fa[u]);
}
void dfs2(int u, int p, int tp) {
  top[u] = tp, b[++totdfn] = u, id[u] = totdfn;
  if (big[u]) dfs2(big[u], u, tp);
  for (int v : g[u])
    if (v != p && v != big[u]) dfs2(v, u, v);
}
int lca(int u, int v) {
  while (top[u] != top[v])
    dep[top[u]] < dep[top[v]] ? swap(u, v), 0 : 0, u = fa[top[u]];
  return dep[u] < dep[v] ? u : v;
}
int kthanc(int u, int k) {
  if (dep[u] <= k) return 0;
  while (dep[u] - k < dep[top[u]]) k -= dep[u] - dep[top[u]] + 1, u = fa[top[u]];
  return b[id[top[u]] + dep[u] - dep[top[u]] - k];
}
void modify(int s, int t, int k) {
  int z = lca(s, t), len = dep[s] + dep[t] - dep[z] * 2;
  log("lca(%d,%d)=%d", s, t, z);
  if (k <= T) {
    for (int u = D[k].get(s), p; p = kthanc(u, k), dep[u] >= dep[z];
         u = D[k].get(p)) {
      if (u != t) a[u] = sqrt(a[u]);
      if (a[u] == 1 && p) D[k].merge(u, p);
    }
    int x = kthanc(t, len - (len - 1) / k * k);
    for (int u = D[k].get(x), p; p = kthanc(u, k), dep[u] > dep[z]; u = D[k].get(p)) {
      if (u != t) a[u] = sqrt(a[u]);
      if (a[u] == 1 && p) D[k].merge(u, p);
    }
    a[t] = sqrt(a[t]);
  } else {
    for (int u = s; dep[u] >= dep[z]; u = kthanc(u, k))
      if (u != t) a[u] = sqrt(a[u]);
    int x = kthanc(t, len - (len - 1) / k * k);
    for (int u = x; dep[u] > dep[z]; u = kthanc(u, k))
      if (u != t) a[u] = sqrt(a[u]);
    a[t] = sqrt(a[t]);
  }
}
long long query(int s, int t, int k) {
  log("query(%d,%d,%d)", s, t, k);
  int z = lca(s, t), len = dep[s] + dep[t] - dep[z] * 2;
  if (s == t) return a[s];
  long long res = (len - 1) / k + 2;
  log("res=%lld", res);
  log("lca(%d,%d)=%d", s, t, z);
  if (k <= T) {
    for (int u = D[k].get(s), p; p = kthanc(u, k), dep[u] >= dep[z];
         u = D[k].get(p)) {
      log("u=%d", u);
      if (u != t) res += a[u] - 1;
      if (a[u] == 1 && p) D[k].merge(u, p);
    }
    int x = kthanc(t, len - (len - 1) / k * k);
    for (int u = D[k].get(x), p; p = kthanc(u, k), dep[u] > dep[z]; u = D[k].get(p)) {
      log("u=%d", u);
      if (u != t) res += a[u] - 1;
      if (a[u] == 1 && p) D[k].merge(u, p);
    }
    res += a[t] - 1;
  } else {
    for (int u = s; dep[u] >= dep[z]; u = kthanc(u, k)) {
      log("u=%d,a=%d", u, a[u]);
      if (u != t) res += a[u] - 1;
    }
    int x = kthanc(t, len - (len - 1) / k * k);
    for (int u = x; dep[u] > dep[z]; u = kthanc(u, k)) {
      log("u=%d,a=%d", u, a[u]);
      if (u != t) res += a[u] - 1;
    }
    log("t=%d,a=%d", t, a[t]);
    res += a[t] - 1;
  }
  return res;
}
int main() {
  scanf("%d", &n);
  T = max(1, (int)sqrt(n));
  FOR(i, 0, T) FOR(j, 0, n) D[i].fa[j] = j;
  FOR(i, 1, n) scanf("%lld", &a[i]);
  FOR(i, 1, n - 1) {
    int u, v;
    scanf("%d%d", &u, &v);
    g[u].pb(v), g[v].pb(u);
  }
  dfs1(1, 0);
  dfs2(1, 0, 1);
  log("GG");
  scanf("%d", &q);
  FOR(i, 1, q) {
    int op, s, t, k;
    scanf("%d%d%d%d", &op, &s, &t, &k);
    if (op == 0)
      modify(s, t, k);
    else
      printf("%lld\n", query(s, t, k));
  }
  return 0;
}
