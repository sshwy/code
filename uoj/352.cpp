#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 6;
int p[N], q[N], vis[N];

int n;
int l[N], r[N], a[N][N];

void dfs(int cur) {
  if (cur == n) return ++ans, void();
}
int main() {
  scanf("%d", &n);
  FOR(i, 1, n) scanf("%d%d", &l[i], &r[i]);
  FOR(i, 1, n) FOR(j, 1, n) scanf("%d", &a[i][j]);
  dfs(0);
  return 0;
}
