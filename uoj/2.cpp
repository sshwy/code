#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
namespace IO {
  char nc() {
    static char bf[100000], *p1 = bf, *p2 = bf;
    return p1 == p2 && (p2 = (p1 = bf) + fread(bf, 1, 100000, stdin), p1 == p2)
               ? EOF
               : *p1++;
  }
  int rd() {
    int res = 0;
    char c = getchar();
    while (!isdigit(c)) c = getchar();
    while (isdigit(c)) res = res * 10 + c - '0', c = getchar();
    return res;
  }
} // namespace IO
/******************heading******************/
const int N = 1e5 + 5;

int n, m, ans;
int g1, g0;

int typ[N], v[N];
int calc(int x) {
  FOR(i, 1, n) {
    if (typ[i] == 1)
      x &= v[i];
    else if (typ[i] == 2)
      x |= v[i];
    else
      x ^= v[i];
  }
  return x;
}
int main() {
  scanf("%d%d", &n, &m);
  FOR(i, 1, n) {
    char op[10];
    scanf("%s%d", op, &v[i]);
    if (op[0] == 'A')
      typ[i] = 1;
    else if (op[0] == 'O')
      typ[i] = 2;
    else
      typ[i] = 3;
  }
  g1 = calc((1 << 30) - 1);
  g0 = calc(0);
  ROF(j, 29, 0) {
    int c = 1 << j;
    if (ans + c > m) continue;
    if ((g1 & c) > (g0 & c)) ans += c;
  }
  printf("%d\n", calc(ans));
  return 0;
}
