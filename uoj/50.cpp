// by Sshwy
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 1 << 20, P = 998244353, I2 = (P + 1) / 2;

int pw(int a, int m) {
  int res = 1;
  while (m) m & 1 ? res = 1ll * res * a % P : 0, a = 1ll * a * a % P, m >>= 1;
  return res;
}
int tr[N];
int init(int n) {
  int len = 1;
  while (len < n) len <<= 1;
  FOR(i, 0, len - 1) tr[i] = tr[i >> 1] >> 1 | (i & 1) * (len >> 1);
  return len;
}
void dft(int *f, int len, int tag) {
  FOR(i, 0, len - 1) if (i < tr[i]) swap(f[i], f[tr[i]]);
  for (int j = 1; j < len; j <<= 1)
    for (int i = 0, wn = pw(3, (P - 1) + (P - 1) / (j << 1) * tag); i < len;
         i += j << 1)
      for (int k = i, u, v, w = 1; k < i + j; k++, w = w * 1ll * wn % P)
        u = f[k], v = 1ll * w * f[k + j] % P, f[k] = (u + v) % P,
        f[k + j] = (u - v + P) % P;
  if (tag == -1) {
    int ilen = pw(len, P - 2);
    FOR(i, 0, len - 1) f[i] = 1ll * f[i] * ilen % P;
  }
}
int fac[N], fnv[N];

int n;
char s[N];
int g[N], f[N];

int t1[N], t2[N];

void solve(int l, int r) {
  if (l == r) { return; }
  int mid = (l + r) >> 1;
  solve(l, mid);

  if (l == 1) {
    FOR(i, 0, mid) t1[i] = f[i]; // mid+1
    int len = init(mid + 1 + mid + 1 + r);
    FOR(i, mid + 1, len - 1) t1[i] = 0;

    dft(t1, len, 1);
    FOR(i, 0, len - 1) t1[i] = 1ll * t1[i] * t1[i] % P;

    assert(r - 1 < n);
    FOR(i, 0, r - 1) t2[i] = g[i];
    FOR(i, r, len) t2[i] = 0;

    dft(t2, len, 1);
    FOR(i, 0, len - 1) t1[i] = 1ll * t1[i] * t2[i] % P;

    dft(t1, len, -1);

    FOR(i, mid + 1, r) {
      f[i] = (f[i] + t1[i - 1] * 1ll * fac[i - 1] % P * fnv[i] % P) % P;
    }
  } else {
    int len = init(mid - l + r - l + 1 + r - l);

    FOR(i, l, mid) t1[i - l] = f[i]; // mid-l+1
    FOR(i, mid - l + 1, len - 1) t1[i] = 0;

    FOR(i, 0, r - l - 1) t2[i] = f[i]; // r-l
    FOR(i, r - l, len - 1) t2[i] = 0;

    dft(t1, len, 1), dft(t2, len, 1);
    FOR(i, 0, len - 1) t1[i] = 1ll * t1[i] * t2[i] % P;

    assert(r - l - 1 < n);
    FOR(i, 0, r - l - 1) t2[i] = g[i]; // r-l
    FOR(i, r - l, len - 1) t2[i] = 0;

    dft(t2, len, 1);
    FOR(i, 0, len - 1) t1[i] = 1ll * t1[i] * t2[i] % P;

    dft(t1, len, -1);

    FOR(i, mid + 1, r) {
      f[i] = (f[i] + t1[i - l - 1] * 2ll % P * fac[i - 1] % P * fnv[i] % P) % P;
    }
  }

  solve(mid + 1, r);
}
int h[N];
int main() {
  scanf("%d", &n);
  scanf("%s", s);

  fac[0] = 1;
  FOR(i, 1, n) fac[i] = 1ll * fac[i - 1] * i % P;
  fnv[n] = pw(fac[n], P - 2);
  ROF(i, n, 1) fnv[i - 1] = 1ll * fnv[i] * i % P;

  FOR(i, 0, n - 1) g[i] = (s[i] - '0') * fnv[i] * 1ll * I2 % P;
  // FOR(i,0,n-1)printf("%d%c",g[i]," \n"[i==n-1]);

  f[1] = 1, f[0] = 0;
  solve(1, n);

  FOR(i, 1, n) printf("%lld\n", f[i] * 1ll * fac[i] % P);

  return 0;
}
