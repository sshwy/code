// by Sshwy
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
  long long r(long long p) { return 1ll * rand() * rand() % p; }
  long long r(long long L, long long R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/

using namespace RA;

int main() {
  srand(clock() + time(0) + (unsigned long int)(new char));
  int n = 50, q = n;
  long long LIM = 1e13;
  cout << n << endl;
  FOR(i, 1, n) cout << r(1ll, LIM) << " \n"[i == n];
  FOR(i, 1, n - 1) { cout << r(1, i) << " " << i + 1 << endl; }
  cout << q << endl;
  FOR(i, 1, q) {
    cout << (r(100) < 50) << " " << r(1, n) << " " << r(1, n) << " " << r(1, n)
         << endl;
  }
  return 0;
}
