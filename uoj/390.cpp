// by Sshwy
//#define DEBUGGER
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

#ifdef DEBUGGER

#define log_prefix()                                                \
  {                                                                 \
    fprintf(stderr, "\033[37mLine %-3d [%dms]:\033[0m  ", __LINE__, \
        (int)clock() / 1000);                                       \
  }
#define ilog(...) \
  { fprintf(stderr, __VA_ARGS__); }
#define llog(...)      \
  {                    \
    log_prefix();      \
    ilog(__VA_ARGS__); \
  }
#define log(...)           \
  {                        \
    log_prefix();          \
    ilog(__VA_ARGS__);     \
    fprintf(stderr, "\n"); \
  }
#define red(...)       \
  {                    \
    log_prefix();      \
    ilog("\033[31m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define green(...)     \
  {                    \
    log_prefix();      \
    ilog("\033[32m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define blue(...)      \
  {                    \
    log_prefix();      \
    ilog("\033[34m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }

#else

#define log_prefix() ;
#define ilog(...) ;
#define llog(...) ;
#define log(...) ;
#define red(...) ;
#define green(...) ;
#define blue(...) ;

#endif

namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 32, W = 31 * 31, P = 998244353;
int n, a[N];
int fac[W], fnv[W], inv_n[W], c[W][W];
int F[W][W], f[W][W], t[W][W];
// F[i,j]: e^{i/n} x^j/j!的系数
int pw(int a, int m) {
  int res = 1;
  while (m) m & 1 ? res = 1ll * res * a % P : 0, a = 1ll * a * a % P, m >>= 1;
  return res;
}
void add(int &x, int y) { x = (x + y) % P; }
void add(int &x, long long y) { x = (x + y) % P; }
int main() {
  scanf("%d", &n);
  FOR(i, 1, n) scanf("%d", &a[i]);

  fac[0] = 1;
  FOR(i, 1, W - 1) fac[i] = fac[i - 1] * 1ll * i % P;
  fnv[W - 1] = pw(fac[W - 1], P - 2);
  ROF(i, W - 1, 1) fnv[i - 1] = fnv[i] * 1ll * i % P;
  c[0][0] = 1;
  FOR(i, 1, W - 1) {
    c[i][0] = 1;
    FOR(j, 1, i) c[i][j] = (c[i - 1][j - 1] + c[i - 1][j]) % P;
  }
  inv_n[0] = 1, inv_n[1] = pw(n, P - 2);
  FOR(i, 2, W - 1) inv_n[i] = inv_n[i - 1] * 1ll * inv_n[1] % P;

  // Calculate F
  F[0][0] = inv_n[1];
  int X = 0, Y = 0;
  FOR(j, 1, n) {
    X += 1, Y += a[j] - 1;
    ROF(x, X, 0) ROF(y, Y, 0) {
      int t = 0;
      if (x - 1 >= 0) add(t, F[x - 1][y]);
      FOR(i, 0, a[j] - 1) {
        long long coef = -fnv[i] * 1ll * inv_n[i] % P;
        if (y - i >= 0) add(t, F[x][y - i] * coef);
      }
      if (t) F[x][y] = t;
    }
  }
  // FOR(x,0,6)FOR(y,0,6)printf("%10d%c",F[x][y]," \n"[y==6]);

  FOR(j, 1, n) {
    // 除掉S(a[j]-1)
    FOR(x, 0, X) FOR(y, 0, Y) {
      int t = 0;
      if (x - 1 >= 0) add(t, f[x - 1][y]);
      add(t, -F[x][y]);
      FOR(i, 1, a[j] - 1) {
        long long coef = -fnv[i] * 1ll * inv_n[i] % P;
        if (y - i >= 0) add(t, f[x][y - i] * coef);
      }
      f[x][y] = t;
    }
    // printf("f[%d]:\n",j);
    // FOR(x,0,6)FOR(y,0,6)printf("%10d%c",f[x][y]," \n"[y==6]);
    int ans = 0;
    long long c1 = inv_n[a[j] - 1] * 1ll * fnv[a[j] - 1] % P;
    FOR(x, 0, X) FOR(y, a[j] - 1, Y) {
      long long fxy = f[x][y - (a[j] - 1)] * c1 % P;
      long long coef =
          fxy * 1ll * fac[y] % P * pw(pw(n - x, P - 2) * 1ll * n % P, y + 1) % P;
      add(ans, coef);
    }
    add(ans, P);
    printf("%d%c", ans, " \n"[j == n]);
  }
  return 0;
}
