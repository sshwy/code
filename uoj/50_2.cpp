// by Sshwy
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 1 << 20, P = 998244353, I2 = (P + 1) / 2;

int pw(int a, int m) {
  int res = 1;
  while (m) m & 1 ? res = 1ll * res * a % P : 0, a = 1ll * a * a % P, m >>= 1;
  return res;
}
int fac[N], fnv[N];

int n;
char s[N];
int g[N], f[N];

int t1[N], t2[N];

int main() {
  scanf("%d", &n);
  scanf("%s", s);

  fac[0] = 1;
  FOR(i, 1, n) fac[i] = 1ll * fac[i - 1] * i % P;
  fnv[n] = pw(fac[n], P - 2);
  ROF(i, n, 1) fnv[i - 1] = 1ll * fnv[i] * i % P;

  FOR(i, 0, n - 1) g[i] = (s[i] - '0') * fnv[i] * 1ll * I2 % P;
  FOR(i, 0, n - 1) printf("%d%c", g[i], " \n"[i == n - 1]);

  f[1] = 1, f[0] = 0;
  FOR(i, 2, n) {
    FOR(j, 0, i - 1) {
      int s = 0;
      FOR(k, 0, i - 1 - j) { s = (s + 1ll * f[k] * f[i - 1 - j - k]) % P; }
      f[i] = (f[i] + g[j] * 1ll * s) % P;
    }
    f[i] = f[i] * 1ll * fac[i - 1] % P * fnv[i] % P;
  }

  FOR(i, 1, n) printf("%lld\n", f[i] * 1ll * fac[i] % P);
  return 0;
}
