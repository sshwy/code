#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
char ibuf[1 << 23], *ih = ibuf, obuf[1 << 22], *oh = obuf;
template <typename T> inline void read(T &x) {
  for (; !isdigit(*ih); ++ih)
    ;
  for (x = 0; isdigit(*ih); x = x * 10 + *ih++ - 48)
    ;
}
inline void out(ll x) {
  static int buf[30];
  int xb = 0;
  for (; x; x /= 10) buf[++xb] = x % 10;
  for (; xb;) *oh++ = buf[xb--] | 48;
}
const int N = 5e4 + 5, M = 6;
inline void fix(int x);
int n;
ll a[N], ass;
struct BS {
  ll a[N], b[200];
  inline void add(const int &i, const ll &x) {
    a[i] += x;
    b[i >> 8] += x;
  }
  inline ll ask(int l) {
    ll ass = 0;
    for (; ~l & 255; ass += a[l--])
      ;
    for (l >>= 8; l >= 0; ass += b[l--])
      ;
    return ass;
  }
};
struct Tr {
  struct edge {
    int to, next;
  } e[N << 1];
  int h[N], xb;
  inline void addedge(int x, int y) {
    e[++xb] = (edge){y, h[x]};
    h[x] = xb;
  }
};
struct Tr1 : Tr {
  int dad[N], g[N], be[N], en[N], dcnt;
  BS B;
  int gfa(int x) { return g[x] == x ? x : g[x] = gfa(g[x]); }
  void dfs(int x) {
    be[x] = ++dcnt;
    g[x] = a[x] == 1 ? g[dad[x]] : x;
    for (int i = h[x]; i; i = e[i].next) dad[e[i].to] = x, dfs(e[i].to);
    en[x] = dcnt;
    B.add(be[x], a[x]);
    B.add(en[x] + 1, -a[x]);
  }
  inline void mdyP(int x, int y) {
    for (; x = gfa(x), be[x] >= be[y]; x = dad[x]) fix(x);
  }
  inline void askP(int x, int y) { ass += B.ask(be[x]) - B.ask(be[dad[y]]); }
} t[M];
struct Tr2 : Tr {
  int sz[N], ma[N], top[N], id[N], dfn[N], dcnt, dad[N], dep[N], st[N], w;
  bool b[N];
  ll val[N];
  void dfs1(int x, int fa) {
    sz[x] = 1;
    for (int i = h[x]; i; i = e[i].next)
      if (e[i].to != fa) {
        dfs1(e[i].to, x);
        sz[x] += sz[e[i].to];
        if (sz[e[i].to] > sz[ma[x]]) ma[x] = e[i].to;
      }
  }
  void dfs2(int x, int fa) {
    st[dep[x] = ++w] = x;
    for (int i = 1; i < M; ++i) t[i].addedge(i < w ? st[w - i] : 0, x);
    if (!top[x]) top[x] = x;
    dfn[id[x] = ++dcnt] = x;
    dad[x] = fa;
    val[dcnt] = a[x];
    b[dcnt] = a[x] == 1;
    if (ma[x]) top[ma[x]] = top[x], dfs2(ma[x], x);
    for (int i = h[x]; i; i = e[i].next)
      if (e[i].to != fa && e[i].to != ma[x]) dfs2(e[i].to, x);
    --w;
  }
  inline void ini() {
    for (int i = 1, u, v; i < n; ++i) read(u), read(v), addedge(u, v), addedge(v, u);
    dfs1(1, 0);
    dfs2(1, 0);
  }
  inline int lca(int x, int y) {
    for (; top[x] != top[y]; x = dad[top[x]])
      if (dep[top[x]] < dep[top[y]]) swap(x, y);
    return dep[x] < dep[y] ? x : y;
  }
  inline int kthfa(int x, int k) {
    if (dep[x] - 1 < k) return 0;
    for (; dep[x] - dep[top[x]] < k; k -= dep[x] - dep[top[x]] + 1, x = dad[top[x]])
      ;
    return dfn[id[x] - k];
  }
  inline void mdy(int l, int r, int k) {
    for (int i = r; i >= l; i -= k)
      if (!b[i]) fix(dfn[i]);
  }
  inline void mdyP(int x, int y, int k) {
    if (k < M) return t[k].mdyP(x, y);
    for (; top[x] != top[y];) {
      int r = id[x], l = id[top[x]];
      mdy(l, r, k);
      x = kthfa(x, (r - l + k) / k * k);
    }
    mdy(id[y], id[x], k);
  }
  inline ll ask(int l, int r, int k) {
    ll ass = 0;
    for (int i = r; i >= l; i -= k) ass += val[i];
    return ass;
  }
  inline void askP(int x, int y, int k) {
    if (k < M) return t[k].askP(x, y);
    for (; top[x] != top[y];) {
      int r = id[x], l = id[top[x]];
      ass += ask(l, r, k);
      x = kthfa(x, (r - l + k) / k * k);
    }
    ass += ask(id[y], id[x], k);
  }
  inline int dis(int x, int y) { return dep[x] + dep[y] - dep[lca(x, y)] * 2; }
} T;
inline void fix(int x) {
  ll dt = a[x] - ll(sqrt(a[x]));
  a[x] -= dt;
  T.val[T.id[x]] -= dt;
  for (int i = 1; i < M; ++i)
    t[i].B.add(t[i].be[x], -dt), t[i].B.add(t[i].en[x] + 1, dt);
  if (a[x] == 1) {
    T.b[T.id[x]] = 1;
    for (int i = 1; i < M; ++i) t[i].g[x] = t[i].dad[x];
  }
}
int main() {
  fread(ibuf, 1, 1 << 23, stdin);
  int Q, o, s, t, u, v, l, k;
  read(n);
  for (l = 1; l <= n; ++l) read(a[l]);
  T.ini();
  for (l = 1; l < M; ++l) ::t[l].dfs(0);
  for (read(Q); Q--;) {
    ass = 0;
    read(o), read(s), read(t), read(k);
    u = T.lca(s, t);
    l = T.dep[s] + T.dep[t] - T.dep[u] * 2;
    if (o == 0)
      T.mdyP(s, T.kthfa(s, (T.dep[s] - T.dep[u]) / k * k), k);
    else
      T.askP(s, T.kthfa(s, (T.dep[s] - T.dep[u]) / k * k), k);
    v = T.kthfa(t, l % k);
    if (T.dep[v] > T.dep[u]) {
      if (o == 0)
        T.mdyP(v, T.kthfa(v, (T.dep[v] - T.dep[u] - 1) / k * k), k);
      else
        T.askP(v, T.kthfa(v, (T.dep[v] - T.dep[u] - 1) / k * k), k);
    }
    if (l % k) {
      if (o == 0) {
        if (a[t] > 1) fix(t);
      } else
        ass += a[t];
    }
    if (o == 1) out(ass), *oh++ = '\n';
  }
  fwrite(obuf, 1, oh - obuf, stdout);
  return 0;
}
