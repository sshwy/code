// by Sshwy
#include <algorithm>/*{{{*/
#include <bitset>
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int M = 3e5 + 5, P = 998244353, SZ = (1 << 20) + 5;
typedef long long LL;
const LL INF = 1e18;
struct Point {
  int x, y;
  Point() {}
  Point(int X, int Y) { x = X, y = Y; }
  Point operator-(const Point p) const { return Point(x - p.x, y - p.y); }
  bool operator<(const Point p) const { return x == p.x ? y < p.y : x < p.x; }
  LL det(Point p) { return x * 1ll * p.y - p.x * 1ll * y; }
} D[M];
typedef vector<int> Convex;

void mergeUp(Convex &res, Convex &A, Convex &B) {
  Convex().swap(res);
  Convex::iterator a = A.begin(), b = B.begin();
  auto convexPush = [](Convex &A, int p) {
    while (
        A.size() >= 2 &&
        (D[A[A.size() - 1]] - D[A[A.size() - 2]]).det(D[p] - D[A[A.size() - 2]]) >= 0)
      A.pop_back();
    A.push_back(p);
  };
  while (a != A.end() || b != B.end()) {
    if (a != A.end() && (b == B.end() || D[*a] < D[*b]))
      convexPush(res, *a), ++a;
    else
      convexPush(res, *b), ++b;
  }
}
Point boundTanUp(const Convex &A, Point p) { //与p所在直线相切的点
  assert(A.size());
  if (A.size() == 1) return D[A[0]];
  int l = 0, r = A.size() - 1, mid = l;
  while (l < r)
    mid = (l + r + 1) >> 1,
    ((D[A[mid]] - D[A[mid - 1]]).det(p) <= 0) ? l = mid : r = mid - 1;
  return D[A[l]];
}

int m, lim, cnt;
bitset<SZ> use;
Convex C[SZ];

void pushup(int u) {
  use[u] = 1;
  mergeUp(C[u], C[u << 1], C[u << 1 | 1]);
}
void assign(int pos, int u = 1, int l = 1, int r = lim) {
  if (l == r) {
    use[u] = 1;
    if (C[u].size()) Convex().swap(C[u]); // C[u].clear();
    C[u].push_back(pos);
    return;
  }
  int mid = (l + r) >> 1;
  if (pos <= mid)
    assign(pos, u << 1, l, mid);
  else
    assign(pos, u << 1 | 1, mid + 1, r);
  // u&(u-1)==1: 则u-1是u这一层的左边的结点
  if ((u & (u - 1)) && pos == r && !use[u - 1]) pushup(u - 1);
}
void erase(int pos, int u = 1, int l = 1, int r = lim) {
  use[u] = 0;
  if (l == r) {
    Convex().swap(C[u]); // C[u].clear();
    return;
  }
  int mid = (l + r) >> 1;
  if (pos <= mid)
    erase(pos, u << 1, l, mid);
  else
    erase(pos, u << 1 | 1, mid + 1, r);
}
LL query(int L, int R, Point p, int u = 1, int l = 1, int r = lim) {
  if (R < l || r < L) return -INF;
  if (L <= l && r <= R && use[u]) return p.det(boundTanUp(C[u], p));
  int mid = (l + r) >> 1;
  return max(L <= mid ? query(L, R, p, u << 1, l, mid) : -INF,
      mid < R ? query(L, R, p, u << 1 | 1, mid + 1, r) : -INF);
}

int go() {
  scanf("%d", &m);
  if (m == 0) return 1;

  int ans = 0;
  cnt = 0;
  lim = 1;
  while (lim < m) lim <<= 1;
  use.reset();

  FOR(i, 1, m) {
    // printf("\033[31mi=%d\033[0m\n",i);
    int op, l, r, x, y;
    scanf("%d", &op);
    if (op == 1) {
      scanf("%d%d", &x, &y);
      D[++cnt] = Point(x, y);
      assign(cnt);
    } else if (op == 2) {
      erase(cnt--);
    } else {
      scanf("%d%d%d%d", &l, &r, &x, &y);
      int t = (query(l, r, Point(x, y)) % P + P) % P;
      ans ^= t;
      // printf("%d\n",t);
    }
    // print(la);
  }
  printf("%d\n", ans);
  return 0;
}
int main() {
  int tp;
  scanf("%d", &tp);
  while (!go())
    ;
  // printf("tot=%d\n",tot);
  return 0;
}
