// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define fi first
#define se second
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

double GetAngle(int xa, int ya, int xb, int yb, int xc, int yc) {
  xb -= xa;
  yb -= ya;
  xc -= xa;
  yc -= ya;
  double dot = 1.0 * xb * xc + 1.0 * yb * yc;
  double normb = 1.0 * xb * xb + 1.0 * yb * yb;
  double normc = 1.0 * xc * xc + 1.0 * yc * yc;
  return std::acos(std::min(std::max(dot / std::sqrt(normb * normc), -1.0), 1.0)) *
         180.0 / 3.14159265358979323846;
}

typedef pair<double, double> point;
typedef pair<double, double> vec;
double det(vec a, vec b) { return a.fi * b.se - a.se * b.fi; }
vec operator-(point a, point b) { return {a.fi - b.fi, a.se - b.se}; }

const int N = 1e3 + 5;

int n, vis[N];

int main() {
  scanf("%d", &n);
  FOR(i, 1, n) {
    int x, y;
    scanf("%d%d", &x, &y);
    p.pb({x, y});
  }

  FOR(i, 1, n) { go(i); }

  return 0;
}
