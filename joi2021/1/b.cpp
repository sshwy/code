// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

const int N = 1e5 + 5;

int n;
int x[N], y[N];

int id(int i, int dir) { // 0 <= dir < 3
  return (i - 1) * 4 + dir;
}

vector<int> g[N * 4];

int main() {
  scanf("%d", &n);
  FOR(i, 1, n) { scanf("%d%d", &x[i], &y[i]); }
  FOR(i, 1, n) FOR(di, 0, 3) FOR(j, 1, n) if (i != j) FOR(dj, 0, 3) {}
  return 0;
}
