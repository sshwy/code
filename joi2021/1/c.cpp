// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (LL i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (LL i = (a); i >= (b); --i)
typedef long long LL;

namespace IO { // require C++11
  const int _BS = 200000;
  char _ob[_BS], *_z = _ob, _ib[_BS], *_x = _ib, *_y = _ib;
  // Input
  char _nc() {
    return _x == _y && (_y = (_x = _ib) + fread(_ib, 1, _BS, stdin), _x == _y)
               ? EOF
               : *_x++;
  }
  inline void rd(char &c) { c = _nc(); }
  void rd(char *s) {
    char c = _nc();
    while (!isblank(c) && isprint(c) && c != EOF) *s++ = c, c = _nc();
    *s = 0;
  }
  template <class T> void rd(T &res) { // unsigned/signed int/long long/__int128
    res = 0;
    char c = _nc(), flag = 0;
    while (!isdigit(c) && c != '-') c = _nc();
    if (c == '-') flag = 1, c = _nc();
    while (isdigit(c)) res = res * 10 + c - '0', c = _nc();
    if (flag) res = -res;
  }
  template <class T, class... A> void rd(T &hd, A &...rs) { rd(hd), rd(rs...); }
  // Output
  inline void flush() { fwrite(_ob, 1, _z - _ob, stdout), _z = _ob; }
  inline void wrch(char x) {
    if (_z == _ob + _BS - 1) flush();
    *_z = x, ++_z;
  }
  void wr(char *s) {
    while (*s) wrch(*s), ++s;
  }
  void wr(const char *s) {
    while (*s) wrch(*s), ++s;
  }
  template <class T> void wr(T x) { // unsigned/signed int/long long/__int128
    if (x == 0) return wrch('0'), void();
    if (x < 0) wrch('-'), x = -x;
    int _t[50], _lt = 0;
    while (x) _t[++_lt] = x % 10, x /= 10;
    while (_lt > 0) wrch(_t[_lt] + '0'), --_lt;
  }
  inline void wr(char x) { wrch(x); }
  template <class T, class... A> void wr(T hd, A... rs) { wr(hd), wr(rs...); }
  template <class... A> void wrln(A... ar) { wr(ar...), wrch('\n'); }
  struct _Flusher {
    ~_Flusher() { flush(); }
  } _flusher;
} // namespace IO

template <const LL SZ, class T = int, const T iv = 0, const LL mxlg = 20>
class Fenwick {
  T c[SZ], sum;

public:
  Fenwick() { fill(c, c + SZ, iv), sum = iv; }
  T &operator[](LL x) { return c[x]; }
  void add(LL pos, T v) {
    for (LL i = pos; i < SZ; i += i & -i) c[i] = c[i] + v;
    sum = sum + v;
  }
  T prefixSum(LL pos) {
    T res = iv;
    for (LL i = pos; i > 0; i -= i & -i) res += c[i];
    return res;
  }
  T suffixSum(LL pos) { return sum - prefixSum(pos - 1); }
  LL lowerBound(const T sum) {
    LL pos = 0;
    T pre = iv;
    ROF(j, mxlg, 0)
    if (pos + (1 << j) < SZ && pre + c[pos + (1 << j)] < sum)
      pre = pre + c[pos + (1 << j)], pos += 1 << j;
    return pos + 1;
  }
  LL upperBound(const T sum) {
    LL pos = 0;
    T pre = iv;
    ROF(j, mxlg, 0)
    if (pos + (1 << j) < SZ && pre + c[pos + (1 << j)] <= sum)
      pre = pre + c[pos + (1 << j)], pos += 1 << j;
    return pos + 1;
  }
};
const int N = 3e5 + 5, M = N, Q = N, JOIN = 1, LEAVE = 2, SERVICE = 3, SZ = N << 2;
const LL INF = 2e18;

int n, m, q, ans[M], totqry;

struct SegmentTree {
  struct data {
    LL mx, mn, mn2, tmx, tad;
    data() {
      tmx = -INF;
      mn2 = INF;
    }
  };
  data t[SZ];

  void pushup(int u) {
    const int lu = u << 1, ru = u << 1 | 1;
    t[u].mx = max(t[lu].mx, t[ru].mx);
    if (t[lu].mn == t[ru].mn) {
      t[u].mn = t[lu].mn;
      t[u].mn2 = min(t[lu].mn2, t[ru].mn2);
    } else if (t[lu].mn < t[ru].mn) {
      t[u].mn = t[lu].mn;
      t[u].mn2 = min(t[lu].mn2, t[ru].mn);
    } else {
      t[u].mn = t[ru].mn;
      t[u].mn2 = min(t[lu].mn, t[ru].mn2);
    }
  }
  void push_add(int u, LL v) {
    // 更新加法标记的同时，更新 Min 和 Max 标记
    t[u].mx += v, t[u].mn += v;
    if (t[u].mn2 != INF) t[u].mn2 += v;
    if (t[u].tmx != -INF) t[u].tmx += v;
    t[u].tad += v;
  }
  void push_max(int u, LL tg) {
    if (t[u].mn > tg) return;
    if (t[u].mx == t[u].mn) t[u].mx = tg;
    t[u].mn = tg, t[u].tmx = tg;
  }
  void pushdown(int u) {
    const int lu = u << 1, ru = u << 1 | 1;
    if (t[u].tad) push_add(lu, t[u].tad), push_add(ru, t[u].tad);
    if (t[u].tmx != -INF) push_max(lu, t[u].tmx), push_max(ru, t[u].tmx);
    t[u].tad = 0, t[u].tmx = -INF;
  }
  void _add(int L, int R, LL v, int u, int l, int r) {
    if (L <= l && r <= R) {
      push_add(u, v);
      if (t[u].mn < 0) push_max(u, 0);
      return;
    }
    int mid = (l + r) >> 1;
    pushdown(u);
    if (L <= mid) _add(L, R, v, u << 1, l, mid);
    if (mid < R) _add(L, R, v, u << 1 | 1, mid + 1, r);
    pushup(u);
  }
  LL _getVal(int pos, int u, int l, int r) {
    if (l == r) return t[u].mx;
    int mid = (l + r) >> 1;
    pushdown(u);
    if (pos <= mid)
      return _getVal(pos, u << 1, l, mid);
    else
      return _getVal(pos, u << 1 | 1, mid + 1, r);
  }
  void add(int l, int r, LL v) { _add(l, r, v, 1, 1, n); }
  LL getVal(int pos) { return _getVal(pos, 1, 1, n); }
} seg;

Fenwick<M, LL> fenwick, bit;

struct Query {
  LL l, r, c, k, a, b, typ;
};
Query join(LL l, LL r, LL c, LL k) { return {l, r, c, k, 0, 0, 1}; }
Query leave(LL l, LL r, LL k) { return {l, r, 0, k, 0, 0, 2}; }
Query service(LL a, LL b) { return {0, 0, 0, 0, a, b, 3}; }

vector<Query> qs;
vector<pair<LL, int>> v[N];
vector<pair<LL, int>> qv[N];

vector<int> tadd[N], tdel[N];
int color[N];

void addTag(int l, int r, int id) {
  tadd[l].pb(id);
  tdel[r + 1].pb(id);
}
void addQuery(LL a, LL b, LL id) { qv[a].pb({b, id}); }

void work(int i) {
  for (auto e : qv[i]) {
    int idx = fenwick.lowerBound(e.first);
    ans[e.second] = color[idx];
  }
}

void go() {
  FOR(i, 1, n) {
    for (auto e : tadd[i]) {
      color[e] = qs[e - 1].c;
      fenwick.add(e, qs[e - 1].k);
    }
    for (auto e : tdel[i]) {
      color[e] = 0;
      fenwick.add(e, -qs[e - 1].k);
    }
    work(i);
  }
}

int main() {
  IO::rd(n, m, q);
  FOR(i, 1, q) {
    LL typ, li, ri, ci, ki, ai, bi;
    IO::rd(typ);
    if (typ == 1) {
      IO::rd(li, ri, ci, ki);
      qs.pb(join(li, ri, ci, ki));
    } else if (typ == 2) {
      IO::rd(li, ri, ki);
      qs.pb(leave(li, ri, ki));
    } else {
      IO::rd(ai, bi);
      qs.pb(service(ai, bi));
    }
  }

  for (auto &e : qs) {
    if (e.typ == JOIN) {
      seg.add(e.l, e.r, e.k);
      bit.add(e.l, e.k), bit.add(e.r + 1, -e.k);
    } else if (e.typ == LEAVE) {
      seg.add(e.l, e.r, -e.k);
    } else {
      LL cur = seg.getVal(e.a);
      LL totJoin = bit.prefixSum(e.a);
      if (cur < e.b)
        e.b = -1;
      else
        e.b += totJoin - cur;
    }
  }

  for (auto e : qs) {
    if (e.typ == SERVICE) {
      if (e.b == -1) {
        ans[++totqry] = 0;
      } else {
        if (m == 1)
          ans[++totqry] = 1;
        else
          addQuery(e.a, e.b, ++totqry);
      }
    }
  }

  if (m != 1) {
    for (unsigned i = 0; i < qs.size(); i++) {
      if (qs[i].typ == JOIN) { addTag(qs[i].l, qs[i].r, i + 1); }
    }

    go();
  }

  FOR(i, 1, totqry) IO::wrln(ans[i]); // printf("%lld\n", ans[i]);

  return 0;
}
