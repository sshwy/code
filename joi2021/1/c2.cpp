// 2pts
// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

const int N = 2.5e5 + 5, M = N, Q = N;

int n, m, q;
deque<int> v[N];

void join(int l, int r, int c, int k) {
  FOR(i, l, r) { FOR(_, 1, k) v[i].push_back(c); }
}
void leave(int l, int r, int k) {
  FOR(i, l, r) {
    FOR(_, 1, k) {
      if (v[i].empty()) break;
      v[i].pop_front();
    }
  }
}
int service(int a, int b) {
  if (v[a].size() < b) return 0;
  return v[a][b - 1];
}

int main() {
  scanf("%d%d%d", &n, &m, &q);
  FOR(i, 1, q) {
    int typ, li, ri, ci, ki, ai, bi;
    scanf("%d", &typ);
    if (typ == 1) {
      scanf("%d%d%d%d", &li, &ri, &ci, &ki);
      join(li, ri, ci, ki);
    } else if (typ == 2) {
      scanf("%d%d%d", &li, &ri, &ki);
      leave(li, ri, ki);
    } else {
      scanf("%d%d", &ai, &bi);
      printf("%d\n", service(ai, bi));
    }
  }
  return 0;
}
