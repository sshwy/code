#include <algorithm>
#include <bits/stdc++.h>
#include <bitset>
#include <cassert>
#include <cctype>
#include <climits>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <functional>
#include <iostream>
#include <map>
#include <numeric>
#include <queue>
#include <set>
#include <stack>
#include <string>
#include <vector>
using namespace std;
#define int long long
#define rep(i, a, b) for (ll i = (a); i <= (b); ++i)
#define per(i, a, b) for (ll i = (a); i >= (b); --i)
#define loop(it, v) for (auto it = v.begin(); it != v.end(); it++)
#define cont(i, x) for (ll i = head[x]; i; i = edge[i].nex)
#define clr(a) memset(a, 0, sizeof(a))
#define ass(a, cnt) memset(a, cnt, sizeof(a))
#define cop(a, b) memcpy(a, b, sizeof(a))
#define lowbit(x) (x & -x)
#define all(x) x.begin(), x.end()
#define SC(t, x) static_cast<t>(x)
#define ub upper_bound
#define lb lower_bound
#define pqueue priority_queue
#define mp make_pair
#define pb push_back
#define pof pop_front
#define pob pop_back
#define fi first
#define se second
#define y1 y1_
#define Pi acos(-1.0)
#define iv inline void
#define enter putchar('\n')
#define siz(x) ((ll)x.size())
#define file(x) freopen(x ".in", "r", stdin), freopen(x ".out", "w", stdout)
typedef double db;
typedef long long ll;
typedef unsigned long long ull;
typedef pair<ll, ll> pii;
typedef vector<ll> vi;
typedef vector<pii> vii;
typedef queue<ll> qi;
typedef queue<pii> qii;
typedef set<ll> si;
typedef map<ll, ll> mii;
typedef map<string, ll> msi;
const ll maxn = 2e5 + 100;
const ll inf = 0x3f3f3f3f;
const ll iinf = 1 << 30;
const ll linf = 2e18;
const ll mod = 1e9 + 7;
const double eps = 1e-7;
template <class T = ll> T chmin(T &a, T b) { return a = min(a, b); }
template <class T = ll> T chmax(T &a, T b) { return a = max(a, b); }
template <class T = ll> iv red(T &x) { x -= mod, x += x >> 31 & mod; }
template <class T = ll> T read() {
  T f = 1, a = 0;
  char ch = getchar();
  while (!isdigit(ch)) {
    if (ch == '-') f = -1;
    ch = getchar();
  }
  while (isdigit(ch)) {
    a = (a << 3) + (a << 1) + ch - '0';
    ch = getchar();
  }
  return a * f;
}

mt19937 mrand(chrono::high_resolution_clock::now().time_since_epoch().count());
ll rnd(ll x) { return mrand() % x + 1; }
ll rnd(ll l, ll r) { return l - 1 + rnd(r - l + 1); }

ll n = 2.5e5, q = 2.5e5, m = 1, w = 1e9;

signed main() {
  // file("makedata");
  // srand(time(0));;
  printf("%lld %lld %lld\n", n, m, q);
  rep(i, 1, q) {
    ll t = rnd(3);
    printf("%lld\n", t);
    if (t == 1) {
      ll l = rnd(n), r = rnd(n), c = rnd(m), k = rnd(w);
      if (l > r) swap(l, r);
      printf("%lld %lld %lld %lld\n", l, r, c, k);
    }
    if (t == 2) {
      ll l = rnd(n), r = rnd(n), k = rnd(w);
      if (l > r) swap(l, r);
      printf("%lld %lld %lld\n", l, r, k);
    }
    if (t == 3) {
      ll x = rnd(n), k = rnd(w);
      printf("%lld %lld\n", x, k);
    }
  }
  return 0;
}

/*
5
1 2 6
1 3 3
1 4 1
1 5 9
3
4 1
1 4
3 1


*/
