// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define x first
#define y second
#define FOR(i, a, b) for (LL i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (LL i = (a); i >= (b); --i)

typedef long long LL;
typedef pair<LL, LL> pt;
const LL N = 2.5e5 + 5;

LL n, k, lc[N], rc[N], lx[N], rx[N], ly[N], ry[N];
pt a[N];

bool byX(pt a, pt b) { return a < b; }
bool byY(pt a, pt b) { return a.y == b.y ? a.x < b.x : a.y < b.y; }

void update(LL u, LL v) {
  lx[u] = min(lx[u], lx[v]), rx[u] = max(rx[u], rx[v]);
  ly[u] = min(ly[u], ly[v]), ry[u] = max(ry[u], ry[v]);
}

void init(LL u) {
  lx[u] = rx[u] = a[u].x, ly[u] = ry[u] = a[u].y;
  if (lc[u]) update(u, lc[u]);
  if (rc[u]) update(u, rc[u]);
}

LL build(LL l, LL r, bool tag) {
  if (l > r) return 0;
  LL mid = (l + r) >> 1;
  if (tag == 0)
    nth_element(a + l, a + mid, a + r + 1, byX);
  else
    nth_element(a + l, a + mid, a + r + 1, byY);
  lc[mid] = build(l, mid - 1, !tag), rc[mid] = build(mid + 1, r, !tag);
  init(mid);
  return mid;
}

LL dis(LL u, LL v) {
  LL x = a[u].x, y = a[u].y;
  LL dx = 0, dy = 0;

  if (x < lx[v])
    dx = lx[v] - x;
  else if (x < rx[v])
    dx = 0;
  else
    dx = x - rx[v];
  if (y < ly[v])
    dy = ly[v] - y;
  else if (y < ry[v])
    dy = 0;
  else
    dy = y - ry[v];

  return dx + dy;
}

struct Heap {
  LL distance(LL u, LL v) const {
    return abs(a[u].x - a[v].x) + abs(a[u].y - a[v].y);
  }
  priority_queue<LL> q;
  void insert(LL u, LL v) {
    LL d = distance(u, v);
    if (q.size() == k) {
      if (q.top() > d) q.pop(), q.push(d);
    } else
      q.push(d);
  }
  bool bad(LL mind) { return q.size() == k && q.top() <= mind; }
  void print() {
    vector<LL> v;
    while (!q.empty()) v.pb(q.top()), q.pop();
    ROF(i, k - 1, 0) printf("%lld\n", v[i]);
  }
} Q;
void work(LL u, LL v) {
  if (!v) return;
  LL mind = dis(u, v);
  if (Q.bad(mind)) return;
  if (u < v) Q.insert(u, v);
  work(u, lc[v]), work(u, rc[v]);
}
LL root;
void solve(LL u) {
  if (!u) return;
  solve(lc[u]), solve(rc[u]);
  work(u, root);
}

signed main() {
  scanf("%lld%lld", &n, &k);
  FOR(i, 1, n) scanf("%lld%lld", &a[i].x, &a[i].y);
  root = build(1, n, 0);
  solve(root);
  Q.print();
  return 0;
}
