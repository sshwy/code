// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define x first
#define y second
#define FOR(i, a, b) for (LL i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (LL i = (a); i >= (b); --i)

typedef long long LL;
typedef pair<LL, LL> pt;
const LL N = 3e5 + 5, SZ = 3e5 + 5;
LL record;

vector<LL> ans;
LL n, k;

struct SegmentTree {
  set<LL> s;
  void init() { s.clear(); }
  void insert(LL x) { s.insert(x); }
  LL countLessOrEqual(LL x, LL base) {
    LL res = 0;
    for (auto e : s) {
      if (e <= x) {
        res++;
        if (record) {
          if (base + e < record) { ans.pb(base + e); }
        }
      }
      if (res == k) break;
    }
    return res;
  }
} seg;

pt a[N];
bool byY(pt p1, pt p2) { return p1.y == p2.y ? p1.x < p2.x : p1.y < p2.y; }

LL work2(pt *b1, pt *e1, pt *b2, pt *e2, LL midX, LL dist, bool flag) {
  pt *p = b2;
  LL res = 0;
  seg.init();
  for (pt *i = b1; i != e1; ++i) {
    while (p != e2 && p->y <= i->y - flag) {
      seg.insert(abs(midX - p->x) - p->y);
      ++p;
    }
    res +=
        seg.countLessOrEqual(dist - i->y - abs(i->x - midX), i->y + abs(i->x - midX));
    if (res >= k) {
      if (!record)
        return k;
      else
        res = k;
    }
  }
  return res;
}
LL work(LL l, LL r, LL dist, LL midX) {
  LL res = 0, mid = (l + r) >> 1;
  sort(a + l, a + mid + 1, byY);
  sort(a + mid + 1, a + r + 1, byY);

  res += work2(a + l, a + mid + 1, a + mid + 1, a + r + 1, midX, dist, 0);
  if (res >= k) {
    if (!record)
      return k;
    else
      res = k;
  }
  res += work2(a + mid + 1, a + r + 1, a + l, a + mid + 1, midX, dist, 1);
  if (res >= k) {
    if (!record)
      return k;
    else
      res = k;
  }
  return res;
}
LL solve(LL l, LL r, LL dist) {
  if (l == r) return 0;
  LL mid = (l + r) >> 1, midX = a[mid].x;
  LL res = solve(l, mid, dist) + solve(mid + 1, r, dist);
  if (res >= k) {
    if (!record)
      return k;
    else
      res = k;
  }
  res += work(l, r, dist, midX);
  if (res >= k) {
    if (!record)
      return k;
    else
      res = k;
  }
  return res;
}
LL calc(LL dist) {
  // printf("calc %lld\n", dist);
  sort(a + 1, a + n + 1);
  // FOR(i, 1, n) printf("(%lld, %lld)%c", a[i].x, a[i].y, " \n"[i == n]);
  LL res = solve(1, n, dist);
  // printf("calc %lld = %lld\n", dist, res);
  return res;
}

signed main() {
  scanf("%lld%lld", &n, &k);
  FOR(i, 1, n) { scanf("%lld%lld", &a[i].x, &a[i].y); }

  LL l = 0, r = 4e9 + 10;
  while (l < r) {
    LL mid = (l + r) >> 1;
    if (calc(mid) < k)
      l = mid + 1;
    else
      r = mid;
  }
  record = l;
  calc(l);
  sort(ans.begin(), ans.end());
  LL kk = ans.size();
  assert(kk < k);
  FOR(i, 0, kk - 1) printf("%lld\n", ans[i]);
  FOR(i, 1, k - kk) printf("%lld\n", record);
  return 0;
}
