// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

using namespace RA;

int main() {
  srand(clock() + time(0));
  int n = rnd(2, 100), k = rnd(1, min(100, n * (n - 1) / 2));
  printf("%d %d\n", n, k);
  FOR(i, 1, n) printf("%d %d\n", rnd(-1e9, 1e9), rnd(-1e9, 1e9));
  return 0;
}
