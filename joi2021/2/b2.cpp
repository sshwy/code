// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

int n, k, x[5000], y[5000];
vector<long long> v;

int main() {
  cin >> n >> k;
  FOR(i, 1, n) {
    cin >> x[i] >> y[i];
    FOR(j, 1, i - 1) { v.pb(0ll + abs(0ll + x[i] - x[j]) + abs(0ll + y[i] - y[j])); }
  }
  sort(v.begin(), v.end());
  FOR(i, 0, k - 1) printf("%lld\n", v[i]);
  return 0;
}
