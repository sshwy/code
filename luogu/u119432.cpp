// by Sshwy
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
const int N = 2e5 + 5;
int n, q;
int L[N], R[N];
int mx[N][20], mn[N][20], lg[N], a[N], nex[N * 2][20], xp[N][20], np[N][20];
long long nw[N * 2][20];

int qmax(int l, int r) {
  int j = lg[r - l + 1];
  return max(mx[l][j], mx[r - (1 << j) + 1][j]);
}
int pmax(int l, int r) {
  int j = lg[r - l + 1];
  return mx[l][j] > mx[r - (1 << j) + 1][j] ? xp[l][j] : xp[r - (1 << j) + 1][j];
}
int qmin(int l, int r) {
  int j = lg[r - l + 1];
  return min(mn[l][j], mn[r - (1 << j) + 1][j]);
}
int pmin(int l, int r) {
  int j = lg[r - l + 1];
  return mn[l][j] < mn[r - (1 << j) + 1][j] ? np[l][j] : np[r - (1 << j) + 1][j];
}

int main() {
  scanf("%d", &n);
  FOR(i, 1, n) scanf("%d%d", &L[i], &R[i]);
  FOR(i, 1, n) mn[i][0] = R[i], xp[i][0] = i, mx[i][0] = L[i], np[i][0] = i;
  FOR(i, 2, n) lg[i] = lg[i / 2] + 1;
  FOR(j, 1, 19) {
    FOR(i, 1, n - (1 << j) + 1) {
      mx[i][j] = max(mx[i][j - 1], mx[i + (1 << j - 1)][j - 1]);
      xp[i][j] = mx[i][j - 1] > mx[i + (1 << j - 1)][j - 1]
                     ? xp[i][j - 1]
                     : xp[i + (1 << j - 1)][j - 1];
      mn[i][j] = min(mn[i][j - 1], mn[i + (1 << j - 1)][j - 1]);
      np[i][j] = mn[i][j - 1] < mn[i + (1 << j - 1)][j - 1]
                     ? np[i][j - 1]
                     : np[i + (1 << j - 1)][j - 1];
    }
  }
  FOR(i, 1, n) {
    int l = i, r = n;
    while (l < r) {
      int mid = (l + r + 1) >> 1;
      if (qmax(i, mid) <= qmin(i, mid))
        l = mid;
      else
        r = mid - 1;
    }
    a[i] = l;
    l = i, r = n + 1;
    while (l < r) {
      int mid = (l + r) >> 1;
      if (qmax(i, mid) <= L[i] && L[i] <= qmin(i, mid))
        l = mid + 1;
      else
        r = mid;
    }
    nex[i * 2][0] = abs(L[i] - L[l]) < abs(L[i] - R[l]) ? l * 2 : l * 2 + 1;
    nw[i * 2][0] = min(abs(L[i] - L[l]), abs(L[i] - R[l]));
    l = i, r = n + 1;
    while (l < r) {
      int mid = (l + r) >> 1;
      if (qmax(i, mid) <= R[i] && R[i] <= qmin(i, mid))
        l = mid + 1;
      else
        r = mid;
    }
    nex[i * 2 + 1][0] = abs(R[i] - L[l]) < abs(R[i] - R[l]) ? l * 2 : l * 2 + 1;
    nw[i * 2 + 1][0] = min(abs(R[i] - L[l]), abs(R[i] - R[l]));
  }
  FOR(j, 1, 19) {
    FOR(i, 1 * 2, n * 2 + 1) {
      if (nex[i][j - 1] > n * 2 + 1)
        nex[i][j] = (n + 1) * 2, nw[i][j] = nw[i][j - 1];
      else {
        int u = nex[i][j - 1];
        nex[i][j] = nex[u][j - 1];
        nw[i][j] = nw[i][j - 1] + nw[u][j - 1];
      }
    }
  }

  scanf("%d", &q);
  FOR(i, 1, q) {
    int x, y;
    scanf("%d%d", &x, &y);
    if (a[x] >= y) {
      puts("0");
    } else {
      int lx = pmax(x, a[x]) * 2, rx = pmin(x, a[x]) * 2 + 1;
      long long al = 0, ar = 0;
      ROF(j, 19, 0) {
        if (nex[lx][j] <= y * 2 + 1) {
          al += nw[lx][j];
          lx = nex[lx][j];
        }
      }
      ROF(j, 19, 0) {
        if (nex[rx][j] <= y * 2 + 1) {
          ar += nw[rx][j];
          rx = nex[rx][j];
        }
      }
      printf("%lld\n", min(al, ar));
    }
  }
  return 0;
}
