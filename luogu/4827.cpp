#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 5e4 + 5, K = 160, P = 10007;

struct qxx {
  int nex, t;
};
qxx e[N * 2];
int h[N], le = 1;
void add_path(int f, int t) { e[++le] = (qxx){h[f], t}, h[f] = le; }
#define FORe(i, u, v) for (int i = h[u], v; v = e[i].t, i; i = e[i].nex)

int n, k, ans[N];
int fac[N];
int S[K][K];

struct dat {
  int f[K], len, cnt; // f[len]的个数
  dat() {
    memset(f, 0, sizeof(f)), len = 0, cnt = 0; // BUG#1:没有初始化cnt导致UB，80分
  }
  dat(int x) {
    memset(f, 0, sizeof(f)), len = 0;
    f[0] = 1, f[1] = x, len = 1, cnt = 1;
  }
  int &operator[](int x) { return f[x]; }
  void add1() {
    int t[K] = {0};
    t[0] = f[0];
    FOR(i, 1, len) t[i] = (f[i] + i * f[i - 1]) % P;
    if (len < k) ++len, t[len] = cnt * fac[len] % P;
    memcpy(f, t, sizeof(t));
  }
  dat operator+(dat d) {
    dat res;
    if (len > d.len)
      res.len = len, res.cnt = cnt;
    else
      res.len = d.len, res.cnt = d.cnt;
    if (len == d.len) res.cnt = (cnt + d.cnt) % P;
    // res.len=max(len,d.len);
    FOR(i, 0, res.len) res.f[i] = (f[i] + d.f[i]) % P;
    return res;
  }
  dat operator-(dat d) {
    assert(len >= d.len);
    dat res = *this;
    FOR(i, 0, d.len) res.f[i] = (res.f[i] - d.f[i]) % P;
    if (len == d.len) res.cnt -= d.cnt;
    return res;
  }
  dat operator+=(dat d) {
    *this = *this + d;
    return *this;
  }
  dat operator-=(dat d) {
    *this = *this - d;
    return *this;
  }
  void print() { FOR(i, 0, len) printf("%d%c", f[i], " \n"[i == len]); }
} g[N];

void dfs(int u, int p) {
  dat t;
  FORe(i, u, v) if (v != p) dfs(v, u), t = g[v], t.add1(), t += dat(1), g[u] += t;
  // printf("g[%d]: ",u);g[u].print();
}
void dfs2(int u, int p) {
  // printf("\033[31mdfs2(%d,%d)\033[0m\n",u,p);
  int res = 0;
  FOR(i, 0, g[u].len) res = (res + g[u][i] * S[k][i]) % P;
  // printf("ans[%d]=%d\n",u,res);
  ans[u] = res;

  int mx = -1;
  int p1 = -1;
  FORe(i, u, v) if (g[v].len > mx) mx = g[v].len, p1 = v;
  dat t1, t;
  FORe(i, u, v) if (v != p1) t = g[v], t.add1(), t1 = t1 + t + dat(1);

  FORe(i, u, v) {
    if (v == p) continue;
    // printf("change %d to %d \n",u,v);
    // change u to v

    dat gu = g[u], gv = g[v];

    // g[u].print();
    // g[v].print();
    if (v == p1) {
      g[u] = t1;
      t = g[u];
      t.add1();
      g[v] += t + dat(1);
    } else {
      t = g[v];
      t.add1();
      g[u] -= t + dat(1);
      t = g[u];
      t.add1();
      g[v] += t + dat(1);
    }
    // g[u].print();
    // g[v].print();
    dfs2(v, u);

    g[u] = gu, g[v] = gv;
  }
}

int main() {
  scanf("%d%d", &n, &k);
  FOR(i, 1, n - 1) {
    int x, y;
    scanf("%d%d", &x, &y);
    add_path(x, y), add_path(y, x);
  }
  fac[0] = 1;
  FOR(i, 1, n) fac[i] = fac[i - 1] * i % P;

  S[0][0] = 1;
  FOR(i, 1, k) {
    S[i][0] = 0;
    FOR(j, 1, i) S[i][j] = (S[i - 1][j - 1] + S[i - 1][j] * j) % P;
    S[i][i] = 1;
  }

  dfs(1, 0);
  dfs2(1, 0);
  FOR(i, 1, n) printf("%d\n", (ans[i] + P) % P);
  return 0;
}
