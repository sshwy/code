#include <iostream>
using namespace std;
long long k, n, sum, m = 1;
int main() {
  cin >> k >> n;
  for (int i = 0; n != 0; n /= 2, m *= k) sum += n % 2 * m;
  cout << sum;
  return 0;
}
