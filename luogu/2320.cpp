#include <algorithm>
#include <cstdio>
using namespace std;
int m;
int a[100], la;
void calc(int x) {
  a[++la] = x - x / 2;
  if (x == 1) return;
  calc(x / 2);
}
int main() {
  scanf("%d", &m);
  calc(m);
  printf("%d\n", la);
  for (int i = la; i >= 1; i--) printf("%d ", a[i]);
  return 0;
}
