#include <iostream>
#define min(a, b) (a < b ? a : b)

using namespace std;

int n, m, mx;
int qp[1501][1501];
int up[1501][1501], le[1501][1501], sq[1501][1501];

int main() {
  cin >> n >> m;
  for (int i = 1; i <= n; i++) {
    for (int j = 1; j <= m; j++) {
      cin >> qp[i][j];

      if (i == 1 || qp[i][j] == qp[i - 1][j])
        up[i][j] = 1;
      else
        up[i][j] = up[i - 1][j] + 1;

      if (j == 1 || qp[i][j] == qp[i][j - 1])
        le[i][j] = 1;
      else
        le[i][j] = le[i][j - 1] + 1;

      if (i == 1 || j == 1)
        sq[i][j] = 1;
      else if (qp[i - 1][j] == qp[i][j] || qp[i][j - 1] == qp[i][j])
        sq[i][j] = 1;
      else if (qp[i - 1][j - 1] != qp[i][j])
        sq[i][j] = 1;
      else
        sq[i][j] = min(up[i - 1][j], min(le[i][j - 1], sq[i - 1][j - 1])) + 1;

      if (sq[i][j] > mx) mx = sq[i][j];
    }
  }
  cout << mx;
  return 0;
}
