#include <iostream>
#include <queue>

using namespace std;

const int INF = 99999999;
int n, m, x, a, b, t, mx;
int d[1001], d2[1001];
int linker[1001][1001], path[1001][1001];
int op_linker[1001][1001], op_path[1001][1001];
bool vising[1001];

queue<int> q;

int main() {
  cin >> n >> m >> x;
  for (int i = 1; i <= m; i++) {
    cin >> a >> b >> t;
    linker[a][++linker[a][0]] = b;
    path[a][++path[a][0]] = t;
    op_linker[b][++op_linker[b][0]] = a;
    op_path[b][++op_path[b][0]] = t;
  }
  for (int i = 1; i <= n; i++) d[i] = d2[i] = INF;
  d[x] = d2[x] = 0;

  vising[x] = true;
  q.push(x);
  while (!q.empty()) {
    int now = q.front();
    q.pop();
    vising[now] = false;

    for (int i = 1; i <= linker[now][0]; i++) {
      int next = linker[now][i], len = path[now][i];
      if (d[next] > d[now] + len) {
        d[next] = d[now] + len;
        if (vising[next] == false) {
          q.push(next);
          vising[next] = true;
        }
      }
    }
  }

  vising[x] = true;
  q.push(x);
  while (!q.empty()) {
    int now = q.front();
    q.pop();
    vising[now] = false;

    for (int i = 1; i <= op_linker[now][0]; i++) {
      int next = op_linker[now][i], len = op_path[now][i];
      if (d2[next] > d2[now] + len) {
        d2[next] = d2[now] + len;
        if (vising[next] == false) {
          q.push(next);
          vising[next] = true;
        }
      }
    }
  }
  for (int i = 1; i <= n; i++) {
    if (mx < d[i] + d2[i]) mx = d[i] + d2[i];
  }
  cout << mx;
  return 0;
}
