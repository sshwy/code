#include <cstdio>
#include <iostream>

using namespace std;

int f[200001]; // father
int v[200001];
int v2[200001];
int n, m, minn = 999999999;

int main() {
  //	freopen("in.txt","r",stdin);
  ios::sync_with_stdio(false);
  cin >> n;
  for (int i = 1; i <= n; i++) { cin >> f[i]; }
  for (int i = 1; i <= n; i++) {
    if (v2[i] == 0) {
      int k, length;
      k = i;
      length = 0;
      while (v[k] < 2) {
        v[k]++;
        v2[k] = 1;
        if (v[k] == 2) length++;
        k = f[k];
      }
      minn = minn < length ? minn : length;
      while (v[k] != 0) {
        v[k] = 0;
        k = f[k];
      }
    }
  }
  cout << minn;
  return 0;
}
/*
1 2 3 4 5
2 4 2 3 1
*/
