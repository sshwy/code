#include <cstring>
#include <iostream>

using namespace std;

char pre[10];
char mid[10];
int num[300]; // i:char
char let[10]; // i:num

struct node {
  int key;
  node *p = NULL;
  node *left = NULL;
  node *right = NULL;
};
typedef struct node *bNode;

bNode tree_insert(bNode tree_root, int nkey) {
  bNode z = new node, y = NULL, x = tree_root;
  z->key = nkey;
  while (x != NULL) {
    y = x;
    if (nkey < x->key)
      x = x->left;
    else
      x = x->right;
  }
  z->p = y;
  if (y == NULL)
    tree_root = z;
  else if (nkey < y->key)
    y->left = z;
  else
    y->right = z;

  return tree_root;
}

void tree_preorder(bNode k) {
  if (k == NULL) return;
  cout << let[k->key];
  tree_preorder(k->left);
  tree_preorder(k->right);
}

int main() {
  bNode t1 = NULL;
  cin >> mid >> pre;
  for (int i = 0; mid[i] != '\0'; i++) {
    num[mid[i]] = i;
    let[i] = mid[i];
  }
  for (int i = strlen(pre) - 1; i >= 0; i--) { t1 = tree_insert(t1, num[pre[i]]); }
  tree_preorder(t1);
  return 0;
}
