// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

const int N = 2e5 + 5;

int n, a[N], a2[N], d[N];
int b[N], c[N], lb;
int ans;

void work(int ak) {
  FOR(i, 0, n - 1) a2[i] = a[i + 1] % ak;

  sort(a2, a2 + n);
  assert(a2[0] == 0);
  a2[0] = -1;

  lb = 0;
  FOR(i, 1, n - 1) {
    if (a2[i] != a2[i - 1]) {
      b[++lb] = a2[i];
      c[lb] = 1;
    } else
      c[lb]++;
  }

  int j = lb;
  FOR(i, 1, lb) {
    while (b[i] + b[j] >= ak && j > 1) --j;
    if (b[i] + b[j] >= ak) break;
    if (i != j || c[i] > 1) {
      ans = max(ans, b[i] + b[j]);
    } else if (j > 1) {
      ans = max(ans, b[i] + b[j - 1]);
    }
  }
  if (c[lb] > 1)
    ans = max(ans, b[lb] * 2 % ak);
  else if (lb > 1)
    ans = max(ans, (b[lb] + b[lb - 1]) % ak);
}

int main() {
  scanf("%d", &n);
  FOR(i, 1, n) scanf("%d", a + i), d[i] = a[i];
  sort(d + 1, d + n + 1);
  int ld = unique(d + 1, d + n + 1) - d - 1;
  ROF(i, ld, 1) {
    work(d[i]);
    if (ans > d[i]) break;
  }
  printf("%d\n", ans);
  return 0;
}
