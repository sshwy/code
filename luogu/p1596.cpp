#include <iostream>

using namespace std;

int n, m, tot;
char qp[101][101];
bool bqp[101][101];

void search(int x, int y) {
  if (x < 1 || x > n || y < 1 || y > m) return;
  if (qp[x][y] != 'W' || bqp[x][y] == 1) return;
  bqp[x][y] = 1;
  search(x + 1, y);
  search(x - 1, y);
  search(x, y + 1);
  search(x, y - 1);

  search(x + 1, y + 1);
  search(x - 1, y + 1);
  search(x + 1, y - 1);
  search(x - 1, y - 1);
}

int main() {
  cin >> n >> m;
  for (int i = 1; i <= n; i++)
    for (int j = 1; j <= m; j++) cin >> qp[i][j];
  for (int i = 1; i <= n; i++) {
    for (int j = 1; j <= m; j++) {
      if (qp[i][j] == 'W' && bqp[i][j] == 0) {
        tot++;
        search(i, j);
      }
    }
  }
  cout << tot;
  return 0;
}
