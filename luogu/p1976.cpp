#include <iostream>

using namespace std;

unsigned long long n, c[3000];

int main() {
  cin >> n;
  c[0] = c[1] = 1;
  for (int i = 2; i <= n; i++) c[i] = (c[i - 1] * (4 * i - 2) / (i + 1)) % 100000007;
  cout << c[n];
  return 0;
}
