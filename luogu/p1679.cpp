#include <iostream>
#define INF 99999999

using namespace std;

int m;
int f[100001];

int main() {
  cin >> m;
  for (int i = 1; i <= m; i++) f[i] = INF;
  for (int i = 1; i * i * i * i <= m; i++) {
    for (int j = 1; j <= m; j++) {
      if (j - i * i * i * i >= 0) { f[j] = min(f[j], f[j - i * i * i * i] + 1); }
    }
  }
  cout << f[m];
  return 0;
}
