#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
namespace NTT {
  const int P = 998244353, SZ = (1 << 20) + 5;
  int tr[SZ];
  int pw(int a, int m) {
    a %= P;
    int res = 1;
    while (m) m & 1 ? res = 1ll * res * a % P : 0, a = 1ll * a * a % P, m >>= 1;
    return res;
  }
  void dft(int *f, int len, int tag) {
    FOR(i, 0, len - 1) if (i < tr[i]) swap(f[i], f[tr[i]]);
    for (int j = 1; j < len; j <<= 1)
      for (int i = 0, wn = pw(3, (P - 1) / (j << 1) * tag + P - 1); i < len;
           i += j << 1) // BUG#1:j<<1写成1<<j
        for (int k = i, u, v, w = 1; k < i + j; k++, w = 1ll * w * wn % P)
          u = f[k], v = 1ll * f[k + j] * w % P, f[k] = (u + v) % P,
          f[k + j] = (u - v + P) % P;
    if (tag == -1) {
      int ilen = pw(len, P - 2);
      FOR(i, 0, len - 1) f[i] = 1ll * f[i] * ilen % P;
    }
  }
  int init(int l) {
    ++l;
    int d = 0;
    while ((1 << d) < (l << 1)) ++d;
    int len = 1 << d;
    FOR(i, 0, len - 1) tr[i] = (tr[i >> 1] >> 1) | ((i & 1) << (d - 1));
    return len;
  }
} // namespace NTT
typedef int dft[NTT::SZ];
const int N = 1e5 + 5, P = 998244353;

int n;
int f[N], g[N];

dft A, B;
void solve(int l, int r) {
  if (l == r) {
    if (l == 0) f[0] = 1;
    return;
  }
  int mid = (l + r) >> 1;
  solve(l, mid);

  int len = NTT::init(r - 1 - l);
  memset(A, 0, sizeof(int) * (len + 2));
  memset(B, 0, sizeof(int) * (len + 2));

  FOR(i, 0, mid - l) A[i] = f[i + l];
  FOR(i, 0, r - l - 1) B[i] = g[i + 1];

  // A=A*B;
  NTT::dft(A, len, 1), NTT::dft(B, len, 1);
  FOR(i, 0, len - 1) A[i] = 1ll * A[i] * B[i] % P;
  NTT::dft(A, len, -1);

  FOR(i, mid + 1, r) f[i] = (f[i] + A[i - l - 1]) % P;

  solve(mid + 1, r);
}
int main() {
  scanf("%d", &n);
  FOR(i, 1, n - 1) scanf("%d", &g[i]);
  solve(0, n - 1);
  FOR(i, 0, n - 1) printf("%d ", f[i]);
  return 0;
}
