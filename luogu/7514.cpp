// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

const int N = 1e6 + 5;

int n, m, ans = 2e9;
int a[N], b[N], bPreMin[N], bSufMin[N], altbPos = -1;
int p[N];
int bPreMax[N], bSufMax[N];

void getMin(int &x, int y) { x = min(x, y); }
void getMax(int &x, int y) { x = max(x, y); }

void work1() {
  // case 1: a[i] is min
  // b[1 .. i-1] > a[i]
  // b[x] < a[i] can't flip
  // find max x : b[x] < a[i]
  // find max i: a[i] < b[i]
  int j = 1;
  FOR(i, 1, n) {
    if (bPreMin[i - 1] < a[i]) break;
    while (j < n && bSufMin[j + 1] < a[i]) ++j;
    int fixPos = max(j, altbPos), rest = m - (i - 1);
    getMax(fixPos, i);
    getMax(fixPos, n - rest);

    int mx = max(bPreMax[i - 1], bSufMax[fixPos + 1]);
    getMax(mx, a[fixPos]);

    getMin(ans, mx - a[i]);
  }
}
void work2() {
  // case 2: b[i] is min
  // find max x : b[x] < b[i]
  int preFilpPos = 0, j = 1;
  FOR(i, 1, n) {
    int cur = p[i];
    while (preFilpPos < n && a[preFilpPos + 1] < b[cur]) ++preFilpPos;
    if (bPreMin[preFilpPos] < b[cur]) break;
    while (j < n && bSufMin[j + 1] < b[cur]) ++j;
    int fixPos = max(j, altbPos), rest = m - preFilpPos;
    getMax(fixPos, n - rest);

    int mx = max(bPreMax[preFilpPos], bSufMax[fixPos + 1]);
    if (preFilpPos < fixPos) { getMax(mx, a[fixPos]); }

    getMin(ans, mx - b[cur]);
  }
}
bool cmp(int x, int y) { return b[x] < b[y]; }
int main() {
  scanf("%d%d", &n, &m);
  FOR(i, 1, n) scanf("%d", a + i);
  FOR(i, 1, n) scanf("%d", b + i);

  FOR(i, 1, n) p[i] = i;
  sort(p + 1, p + n + 1, cmp);

  bPreMin[0] = 2e9;
  FOR(i, 1, n) bPreMin[i] = min(bPreMin[i - 1], b[i]);
  bPreMax[1] = b[1];
  FOR(i, 2, n) bPreMax[i] = max(bPreMax[i - 1], b[i]);

  bSufMin[n + 1] = 2e9;
  ROF(i, n, 1) bSufMin[i] = min(bSufMin[i + 1], b[i]);
  bSufMax[n] = b[n];
  ROF(i, n - 1, 1) bSufMax[i] = max(bSufMax[i + 1], b[i]);

  FOR(i, 1, n) if (a[i] < b[i]) altbPos = i;

  work1();
  work2();

  printf("%d\n", ans);

  return 0;
}
