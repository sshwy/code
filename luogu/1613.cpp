#include <cstdio>
#include <iostream>
#define FOR(a, b, c) for (int a = b; a <= c; a++)
using namespace std;
const int N = 51, INF = 0x3f3f3f3f;
int n, m;
bool f[N][N][70];
int w[N][N];

int main() {
  scanf("%d%d", &n, &m);
  FOR(i, 1, n) {
    FOR(j, 1, n) w[i][j] = INF;
    w[i][i] = 0;
  }
  FOR(i, 1, m) {
    int u, v;
    scanf("%d%d", &u, &v);
    f[u][v][0] = 1, w[u][v] = 1;
  }
  FOR(k, 1, 63) FOR(i, 1, n) FOR(j, 1, n) {
    FOR(p, 1, n) f[i][j][k] |= f[i][p][k - 1] & f[p][j][k - 1];
    if (f[i][j][k]) w[i][j] = 1;
  }
  FOR(k, 1, n)
  FOR(i, 1, n) FOR(j, 1, n) w[i][j] = min(w[i][j], w[i][k] + w[k][j]);
  printf("%d\n", w[1][n]);
  return 0;
}
/*
 * BUG#1:L19 忘了初始化w
 */
