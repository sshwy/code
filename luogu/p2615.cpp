#include <iostream>

using namespace std;

int n, qp[40][40];

int main() {
  cin >> n;
  for (int k = 1, x = 1, y = n / 2 + 1; k <= n * n; k++) {
    qp[x][y] = k;
    if (x == 1 && y == n)
      x++;
    else if (x == 1)
      x = n, y++;
    else if (y == n)
      x--, y = 1;
    else if (qp[x - 1][y + 1] == 0)
      x--, y++;
    else
      x++;
  }
  for (int i = 1; i <= n; i++) {
    for (int j = 1; j <= n; j++) { cout << qp[i][j] << ' '; }
    cout << endl;
  }
  return 0;
}
