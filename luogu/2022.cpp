#include <algorithm>
#include <cstdio>
#define int long long
using namespace std;
int k, m;
int a[20], la;
signed main() {
  scanf("%lld%lld", &k, &m);
  int x = 0, k2 = k, m10 = 1;
  while (k2) a[++la] = k2 % 10, k2 /= 10;
  reverse(a + 1, a + la + 1);
  for (int i = 1; i <= la; i++) {
    k2 = k2 * 10 + a[i];
    x += k2 - m10 + 1;
    m10 *= 10;
  }
  if (la == x && m != x) return puts("0"), 0;
  if (x > m) return puts("0"), 0;
  if (x == m) return printf("%lld", k), 0;
  while (x < m) {
    k2 *= 10;
    int d = k2 - m10; //不含k2本身
    if (x + d < m)
      x += d;
    else
      return printf("%lld", m10 + m - x - 1), 0;
    m10 *= 10;
  }
  return 0;
}
