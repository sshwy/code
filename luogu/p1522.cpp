#include <cmath>
#include <cstdio>
#include <iostream>

using namespace std;

const double INF = 99999;
int n, t, m;
double d[160][160];
double ld[160], l1 = 0, l2 = INF; //单元最长路
char bu[160][160];

struct cs {
  int x, y;
} po[160];

double dis(cs c1, cs c2) {
  return sqrt((c1.x - c2.x) * (c1.x - c2.x) + (c1.y - c2.y) * (c1.y - c2.y));
}

int main() {
  cin >> n;
  for (int i = 1; i <= n; i++) { cin >> po[i].x >> po[i].y; }
  for (int i = 1; i <= n; i++) {
    for (int j = 1; j <= n; j++) { d[i][j] = INF; }
    d[i][i] = 0;
  }
  for (int i = 1; i <= n; i++) {
    for (int j = 1; j <= n; j++) {
      cin >> bu[i][j];
      if (bu[i][j] == '1') { d[i][j] = dis(po[i], po[j]); }
    }
  }

  // floyd
  for (int k = 1; k <= n; k++) {
    for (int i = 1; i <= n; i++) {
      for (int j = 1; j <= n; j++) {
        if (d[i][j] > d[i][k] + d[k][j]) { d[i][j] = d[i][k] + d[k][j]; }
      }
    }
  }

  for (int i = 1; i <= n; i++) {
    for (int j = 1; j <= n; j++) { printf("%13lf", d[i][j]); }
    cout << endl;
  }

  for (int i = 1; i <= n; i++) {
    for (int j = 1; j <= n; j++) {
      if (d[i][j] < INF) { ld[i] = ld[i] > d[i][j] ? ld[i] : d[i][j]; }
    }
    l1 = l1 > ld[i] ? l1 : ld[i];
  }
  for (int i = 1; i <= n; i++) {
    for (int j = 1; j <= n; j++) {
      if (d[i][j] >= INF - 5) {
        l2 = l2 < ld[i] + dis(po[i], po[j]) + ld[j]
                 ? l2
                 : ld[i] + dis(po[i], po[j]) + ld[j];
      }
    }
  }
  l1 = l1 > l2 ? l1 : l2;
  printf("%.6lf", l1);
  return 0;
}
/*
8
10 10
15 10
20 10
15 15
20 15
30 15
25 10
30 10
01000000
10111000
01001000
01001000
01110000
00000010
00000101
00000010*/
