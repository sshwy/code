#include <iostream>

using namespace std;

int n, l, a, mx, mn = 5555, tmx, tmn;
int br[5001];

int main() {
  cin >> l >> n;
  for (int i = 1; i <= n; i++) {
    cin >> a;
    br[a]++;
    if (a > mx) mx = a;
    if (a < mn) mn = a;
  }
  if (l % 2 == 0) {
    for (int i = l / 2, j = l / 2 + 1; i >= 1 && j <= l; i--, j++) {
      if (br[i] != 0) {
        tmn = i;
        break;
      }
      if (br[j] != 0) {
        tmn = l - j + 1;
        break;
      }
    }

  } else {
    for (int i = l / 2 + 1, j = l / 2 + 1; i >= 1 && j <= l; i--, j++) {
      if (br[i] != 0) {
        tmn = i;
        break;
      }
      if (br[j] != 0) {
        tmn = l - j + 1;
        break;
      }
    }
  }
  tmx = l - mn + 1 > mx ? l - mn + 1 : mx;
  cout << tmn << ' ' << tmx;
  return 0;
}
