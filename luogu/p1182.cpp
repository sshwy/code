#include <iostream>

using namespace std;

long long n, m;
long long ps[100001]; // pre_sum
long long l, r, mid;

int main() {
  cin >> n >> m;
  for (long long i = 1; i <= n; i++) {
    cin >> ps[i];
    ps[i] += ps[i - 1];
  }
  l = 0;
  r = ps[n];
  while (l <= r) {
    mid = (l + r) >> 1;
    long long pre = 0, t = 1;
    for (long long i = 1; i <= n; i++) {
      if (ps[i] - ps[pre] > mid) {
        t++;
        pre = i - 1;
      }
    }
    if (t <= m)
      r = mid - 1;
    else
      l = mid + 1;
  }
  cout << l;
  return 0;
}
