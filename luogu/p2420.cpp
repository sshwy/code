#include <iostream>
#include <vector>

using namespace std;

int n, u, v, w, m;
int dep[100001];
bool vis[100001];

vector<int> next[100001];
vector<int> value[100001];

void dfs_dep(int k) {
  vis[k] = true;
  for (int i = 0; i < next[k].size(); i++) {
    if (vis[next[k][i]] == false) {
      dep[next[k][i]] = dep[k] ^ value[k][i];
      dfs_dep(next[k][i]);
    }
  }
}

int main() {
  cin >> n;
  for (int i = 1; i < n; i++) {
    cin >> u >> v >> w;
    next[u].push_back(v);
    value[u].push_back(w);
    next[v].push_back(u);
    value[v].push_back(w);
  }
  dfs_dep(1);
  cin >> m;
  for (int i = 1; i <= m; i++) {
    cin >> u >> v;
    cout << (dep[u] ^ dep[v]) << endl;
  }
  return 0;
}
