#include <cstdio>
using namespace std;
const int N = 1e5 + 5, SZ = 1 << 24;
int n, m;
int a[N];

int tot;
int lc[SZ], rc[SZ], sz[SZ];
int rt[N];

namespace CRT {
  int cp(int u) {
    ++tot, lc[tot] = lc[u], rc[tot] = rc[u], sz[tot] = sz[u];
    return tot;
  }
  int modify(int u, int v, int w, int l = 1, int r = 1e8) {
    int u2 = cp(u), mid = (l + r) >> 1;
    if (l == r) return sz[u2] += w, u2;
    if (v <= mid)
      lc[u2] = modify(lc[u], v, w, l, mid);
    else
      rc[u2] = modify(rc[u], v, w, mid + 1, r);
    return sz[u2] = sz[lc[u2]] + sz[rc[u2]], u2;
  }
} // namespace CRT
void modify(int p, int x, int v) { //在位置p上添加v个x
  for (int i = p; i <= n; i += i & -i) rt[i] = CRT::modify(rt[i], x, v);
}
int lu[N], ru[N], cl, cr;
void prework(int l, int r) {
  --l, cl = cr = 0;
  for (int i = l; i > 0; i -= i & -i) lu[++cl] = rt[i];
  for (int i = r; i > 0; i -= i & -i) ru[++cr] = rt[i];
}
void goL() {
  for (int i = 1; i <= cl; i++) lu[i] = lc[lu[i]];
  for (int i = 1; i <= cr; i++) ru[i] = lc[ru[i]];
}
void goR() {
  for (int i = 1; i <= cl; i++) lu[i] = rc[lu[i]];
  for (int i = 1; i <= cr; i++) ru[i] = rc[ru[i]];
}
int rank(int x, int L = 1, int R = 1e8) { //根据lu,ru数组查询第一个大于等于x的RK
  int ls = 0, Mid = (L + R) >> 1;
  if (L == R) return 1;
  if (x <= Mid)
    return goL(), rank(x, L, Mid);
  else {
    for (int i = 1; i <= cl; i++) ls -= sz[lc[lu[i]]];
    for (int i = 1; i <= cr; i++) ls += sz[lc[ru[i]]];
    return goR(), ls + rank(x, Mid + 1, R);
  }
}
int kth(int k, int L = 1, int R = 1e8) {
  int ls = 0, Mid = (L + R) >> 1;
  if (L == R) return L;
  for (int i = 1; i <= cl; i++) ls -= sz[lc[lu[i]]];
  for (int i = 1; i <= cr; i++) ls += sz[lc[ru[i]]];
  if (k <= ls)
    return goL(), kth(k, L, Mid);
  else
    return goR(), kth(k - ls, Mid + 1, R);
}

int main() {
  scanf("%d%d", &n, &m);
  for (int i = 1; i <= n; i++) {
    scanf("%d", &a[i]);
    modify(i, a[i], 1);
  }
  for (int i = 1; i <= m; i++) {
    int op, l, r, k;
    scanf("%d", &op);
    if (op == 1) {
      scanf("%d%d%d", &l, &r, &k);
      prework(l, r);
      printf("%d\n", rank(k));
    } else if (op == 2) {
      scanf("%d%d%d", &l, &r, &k);
      prework(l, r);
      printf("%d\n", kth(k));
    } else if (op == 3) {
      scanf("%d%d", &l, &k);
      modify(l, a[l], -1), modify(l, k, 1);
      a[l] = k;
    } else if (op == 4) {
      scanf("%d%d%d", &l, &r, &k);
      prework(l, r);
      int rk = rank(k);
      if (rk == 1) {
        puts("-2147483647");
        continue;
      }
      prework(l, r);
      printf("%d\n", kth(rk - 1));
    } else {
      scanf("%d%d%d", &l, &r, &k);
      prework(l, r);
      int rk = rank(k + 1);
      if (rk > r - l + 1) {
        puts("2147483647");
        continue;
      }
      prework(l, r);
      printf("%d\n", kth(rk));
    }
  }
  return 0;
}
/*
 * BUG#1:ls没有初始化导致dynamic的输出emm
 * BUG#2:判断前驱后继是否无解的时侯忘continue
 * BUG#3:op==3时a[l]写成了a[i]emm
 */
