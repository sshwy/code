#include <bits/stdc++.h>
using namespace std;
const int N = 100005;
const int LST = 1 << 18;
int n, dfn, a[N];

int s[LST], tg[LST];
//有多少个安装的软件包
// tg：1：全部卸载；2：全部安装；0：不做操作

inline void push_up(int rt) { s[rt] = s[rt << 1] + s[rt << 1 | 1]; }
inline void push_down(int l, int r, int rt) {
  if (tg[rt] == 0) return;
  int mid = (l + r) >> 1;
  if (tg[rt] == 2)
    s[rt << 1] = mid - l + 1, tg[rt << 1] = 2, s[rt << 1 | 1] = r - mid,
            tg[rt << 1 | 1] = 2;
  else
    s[rt << 1] = 0, tg[rt << 1] = 1, s[rt << 1 | 1] = 0, tg[rt << 1 | 1] = 1;
  tg[rt] = 0;
}
int lst_sum(int L, int R, int l = 1, int r = n, int rt = 1) {
  if (L <= l && r <= R) return s[rt];
  int mid = (l + r) >> 1, su = 0;
  push_down(l, r, rt);
  if (L <= mid) su += lst_sum(L, R, l, mid, rt << 1);
  if (mid < R) su += lst_sum(L, R, mid + 1, r, rt << 1 | 1);
  return su;
}
int lst_upd(int L, int R, int v, int l = 1, int r = n, int rt = 1) {
  if (L <= l && r <= R) return s[rt] = (r - l + 1) * v, tg[rt] = v + 1;
  int mid = (l + r) >> 1;
  push_down(l, r, rt);
  if (L <= mid) lst_upd(L, R, v, l, mid, rt << 1);
  if (mid < R) lst_upd(L, R, v, mid + 1, r, rt << 1 | 1);
  push_up(rt);
}

struct node {
  int p, s, b;
  int dp, tp, sz, hvs, idx;
};
node t[N];
void add_son(int p, int s) { t[s].p = p, t[s].b = t[p].s, t[p].s = s; }
int szdfs(int rt, int dp) {
  t[rt].dp = dp, t[rt].sz = 1;
  for (int i = t[rt].s; i; i = t[i].b) t[rt].sz += szdfs(i, dp + 1);
  return t[rt].sz;
}
void dcdfs(int rt, int tp) {
  t[rt].tp = tp, t[rt].idx = ++dfn;
  if (!t[rt].s) return;
  for (int mx = 0, i = t[rt].s; i; i = t[i].b)
    if (t[i].sz > mx) mx = t[i].sz, t[rt].hvs = i;
  dcdfs(t[rt].hvs, tp);
  for (int i = t[rt].s; i; i = t[i].b)
    if (i != t[rt].hvs) dcdfs(i, i);
}
int tree_path_query(int x, int v) {
  int len = 0, c1 = 0;
  while (t[x].tp != 1) {
    len += t[x].idx - t[t[x].tp].idx + 1;
    c1 += lst_sum(t[t[x].tp].idx, t[x].idx);
    x = t[t[x].tp].p;
  }
  len += t[x].idx;
  c1 += lst_sum(1, t[x].idx);
  if (v == 1)
    return c1;
  else
    return len - c1;
}
void tree_path_upd(int x, int v) {
  while (t[x].tp != 1) {
    lst_upd(t[t[x].tp].idx, t[x].idx, v);
    x = t[t[x].tp].p;
  }
  lst_upd(1, t[x].idx, v);
}
int tree_query(int x, int v) {
  int c1 = lst_sum(t[x].idx, t[x].idx + t[x].sz - 1);
  if (v == 1)
    return c1;
  else
    return t[x].sz - c1;
}
int tree_upd(int x, int v) { lst_upd(t[x].idx, t[x].idx + t[x].sz - 1, v); }

int main() {
  scanf("%d", &n);
  for (int i = 2, ai; i <= n; i++) {
    scanf("%d", &ai);
    add_son(ai + 1, i);
  }
  szdfs(1, 1);
  dcdfs(1, 1);
  int q;
  scanf("%d", &q);
  for (int i = 1, x; i <= q; i++) {
    char op[20];
    scanf("%s%d", op, &x);
    if (op[0] == 'i') {
      printf("%d\n", tree_path_query(x + 1, 0));
      tree_path_upd(x + 1, 1);
    } else {
      printf("%d\n", tree_query(x + 1, 1));
      tree_upd(x + 1, 0);
    }
  }
  return 0;
}
