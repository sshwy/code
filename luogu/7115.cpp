// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

const int N = 55, M = 405;
int n, m;
int a[N][M], id[N];
vector<pair<int, int>> ans;

void move(int x1, int y1, int x2, int y2) {
  ans.push_back(make_pair(x1, x2));
  // assert(a[x2][y2] == 0);
  a[x2][y2] = a[x1][y1];
  a[x1][y1] = 0;
}
bool check(int ro, int k) {
  if (a[ro][1] == 0) FOR(i, 1, m) if (a[ro][i]) return false;
  if (a[ro][1] > k) FOR(i, 1, m) if (a[ro][i] <= k) return false;
  if (a[ro][1] <= k) FOR(i, 1, m) if (a[ro][i] > k) return false;
  return true;
}
int work(int row, int k) { // sort
  int trow = row == id[1] ? id[2] : id[1];
  int c[2] = {0};
  FOR(i, 1, m) c[a[row][i] > k]++;
  if (c[0] == m || c[1] == m) return 2;

  bool condition = c[0] < c[1] ? 0 : 1;
  FOR(i, 1, c[condition]) { move(trow, m - i + 1, id[n + 1], i); }

  int cnt = 0, cnt2 = 0;
  FOR(i, 1, m) {
    if ((a[row][m - i + 1] > k) == condition) {
      ++cnt;
      move(row, m - i + 1, trow, m - c[condition] + cnt);
    } else {
      ++cnt2;
      move(row, m - i + 1, id[n + 1], c[condition] + cnt2);
    }
  }

  ROF(i, m, c[condition] + 1) move(id[n + 1], i, row, m - i + 1);
  ROF(i, c[condition], 1) move(trow, m - c[condition] + i, row, m - i + 1);
  ROF(i, c[condition], 1) move(id[n + 1], i, trow, m - i + 1);
  return a[row][1] > k;
}
void diffMerge(int rx, int ry, int trow, int k) {
  if (!a[rx][1]) return;
  if (!a[ry][1]) return;
  int cx[2] = {0}, cy[2] = {0};
  FOR(i, 1, m) cx[a[rx][i] > k]++;
  FOR(i, 1, m) cy[a[ry][i] > k]++;
  int condition = cx[0] + cy[0] > m ? 0 : 1;
  if ((a[rx][m] > k) != condition) {
    swap(rx, ry);
    swap(cx[0], cy[0]);
    swap(cx[1], cy[1]);
  }
  FOR(i, 1, cx[condition]) move(rx, m - i + 1, trow, i);
  FOR(i, 1, cy[!condition]) move(ry, m - i + 1, rx, m - cx[condition] + i);
  FOR(i, 1, cy[!condition])
  move(trow, cx[condition] - i + 1, ry, m - cy[!condition] + i);
  ROF(i, cx[condition] - cy[!condition], 1) move(trow, i, rx, m - i + 1);
  if (cx[0] + cy[0] == m) return;
}
void reverseMerge(int x, int y, int mid) {
  FOR(i, 1, m) move(id[y], m - i + 1, id[n + 1], i);
  swap(id[n + 1], id[y]);
  diffMerge(id[x], id[y], id[n + 1], mid);
}
bool mergeCheck(int x, int y, int k) {
  if ((a[id[x]][1] > k) == (a[id[y]][1] > k)) return true;
  return false;
}
void solve(int l, int r) {
  if (l == r) return;
  int mid = (l + r) >> 1;
  // 把 [l,mid] 号的球放到编号为 [l,mid] 的柱子上
  FOR(i, l, r) { work(id[i], mid); }
  FOR(i, l, r) {
    if (!check(id[i], mid)) {
      FOR(j, i + 1, r) if (!check(id[j], mid)) {
        if (mergeCheck(i, j, mid))
          reverseMerge(i, j, mid);
        else
          diffMerge(id[i], id[j], id[n + 1], mid);
        --i;
        break;
      }
    }
  }
  FOR(i, l, r) if (a[id[i]][1] > mid) {
    FOR(j, i + 1, r) if (a[id[j]][1] <= mid) {
      swap(id[i], id[j]);
      break;
    }
  }
  solve(l, mid);
  solve(mid + 1, r);
}
int main() {
  freopen("ball.in", "r", stdin);
  freopen("ball.out", "w", stdout);
  scanf("%d%d", &n, &m);
  FOR(i, 1, n) FOR(j, 1, m) scanf("%d", &a[i][j]);
  FOR(i, 1, n + 1) id[i] = i;
  solve(1, n);
  printf("%lu\n", ans.size());
  for (auto x : ans) printf("%d %d\n", x.first, x.second);
  return 0;
}
