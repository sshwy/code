#include <cstdio>
#include <iostream>

#define SIZE 100

using namespace std;

struct vint {
  long long num[SIZE] = {0}, len = 0;

  vint operator+(vint add) {
    vint sum;
    long long x = 0;
    for (sum.len = 1; sum.len <= len || sum.len <= add.len; sum.len++) {
      sum.num[sum.len] = num[sum.len] + add.num[sum.len] + x;
      x = sum.num[sum.len] / 10;
      sum.num[sum.len] %= 10;
    }
    if (x != 0)
      sum.num[sum.len] = x;
    else
      sum.len--;
    return sum;
  }
  vint operator+(long long v) {
    vint sum;
    long long x = 0;
    for (sum.len = 1; sum.len <= len || v != 0; sum.len++, v /= 10) {
      sum.num[sum.len] = num[sum.len] + v % 10 + x;
      x = sum.num[sum.len] / 10;
      sum.num[sum.len] %= 10;
    }
    if (x != 0)
      sum.num[sum.len] = x;
    else
      sum.len--;
    return sum;
  }
  vint operator*(vint mul) {
    vint res;
    long long x = 0;
    for (long long i = 1; i <= mul.len; i++) {
      for (long long j = 1; j <= len; j++) {
        res.num[i + j - 1] += mul.num[i] * num[j] + x;
        x = res.num[i + j - 1] / 10;
        res.num[i + j - 1] %= 10;
      }
      if (x != 0) res.num[i + len] += x;
      x = 0;
    }
    if (res.num[mul.len + len] != 0)
      res.len = mul.len + len;
    else
      res.len = mul.len + len - 1;
    return res;
  }
  vint operator=(long long v) {
    for (long long i = 0; i <= len; i++) num[i] = 0;
    len = 0;
    while (v != 0) {
      num[++len] = v % 10;
      v /= 10;
    }
    return *this;
  }
  vint operator=(int v) {
    for (int i = 0; i <= len; i++) num[i] = 0;
    len = 0;
    while (v != 0) {
      num[++len] = v % 10;
      v /= 10;
    }
    return *this;
  }
  void get() {
    for (long long i = 1; i <= len; i++) num[i] = 0;
    len = 0;
    char c = 0;
    while (c = getchar())
      if ('0' <= c && c <= '9') break;
    do {
      if ('0' <= c && c <= '9')
        num[++len] = c - 48;
      else
        break;
    } while ((c = getchar()) != EOF);
    for (long long i = 1, j = len; i < j; i++, j--)
      num[i] ^= num[j] ^= num[i] ^= num[j];
  }
  void put() {
    for (long long i = len; i >= 1; i--) putchar((char)(num[i] + 48));
  }

  bool operator>(vint sec) {
    if (len > sec.len)
      return true;
    else if (len < sec.len)
      return false;
    for (long long i = len; i >= 1; i--) {
      if (num[i] > sec.num[i])
        return true;
      else if (num[i] < sec.num[i])
        return false;
    }
    return false;
  }
};
long long n, k1, l;
char cn[50];

vint f[50][10], a[50][50];

int main() {
  cin >> n >> k1 >> cn;
  for (long long s = 0; cn[s]; s++) {
    long long num = 0;
    for (long long j = s; cn[j]; j++) {
      num = num * 10 + cn[j] - 48;
      a[s + 1][j + 1] = num;
    }
  }
  for (long long i = 1; cn[i - 1]; i++) {
    f[i][0] = a[1][i];
    for (long long j = 1; j <= k1; j++) {
      vint mx;
      mx = 0;
      for (long long k = j; k < i; k++) {
        if (f[k][j - 1] * a[k + 1][i] > mx) mx = f[k][j - 1] * a[k + 1][i];
      }
      f[i][j] = mx;
    }
  }
  while (cn[l]) l++;
  f[l][k1].put();
  return 0;
}
