#include <iostream>

using namespace std;

long long n, m, k, x, t;

long long ksm(long long a, long long r, long long p) // return a^r%p
{
  if (r == 0)
    return 1;
  else if (r == 1)
    return a % p;
  else if (r == 2)
    return a * a % p;
  t = ksm(a, r / 2, p);
  return t * t * (r % 2 ? a : 1) % p;
}

int main() {
  cin >> n >> m >> k >> x;
  // ans:10^k*m%n
  cout << (ksm(10, k, n) * m + x) % n;
  return 0;
}
