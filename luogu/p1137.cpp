#include <iostream>
#include <vector>

using namespace std;

int n, m, a, b;
vector<int> west[100001];
int mvc[100001];

int most_visit_count(int end) {
  if (mvc[end] != 0) return mvc[end];
  int mx = 0;
  for (int i = 0; i < west[end].size(); i++) {
    if (mvc[west[end][i]] == 0) most_visit_count(west[end][i]);
    if (mx < mvc[west[end][i]]) mx = mvc[west[end][i]];
  }
  return mvc[end] = mx + 1;
}

int main() {
  cin >> n >> m;
  for (int i = 1; i <= m; i++) {
    cin >> a >> b;
    west[b].push_back(a);
  }
  for (int i = 1; i <= n; i++) cout << most_visit_count(i) << endl;
  return 0;
}
