#include <cstdio>
#include <iostream>

using namespace std;

int n, m, p, a, b;
int f[10001]; // father

int gf(int k) // getfather
{
  //	cout<<;
  if (f[k] == k) return k;
  f[k] = gf(f[k]);
  return f[k];
}

void un(int x, int y) // union
{
  int fx = gf(x);
  int fy = gf(y);
  f[fx] = fy;
}

int main() {
  freopen("p1551in.txt", "r", stdin);

  cin >> n >> m >> p;
  for (int i = 1; i <= n; i++) f[i] = i;

  for (int i = 1; i <= m; i++) {
    cin >> a >> b;

    if (gf(a) != gf(b)) { un(a, b); }
  }
  /*
  for(int i=1;i<=m;i++)cout<<i<<' ';
  cout<<endl;
  for(int i=1;i<=m;i++)cout<<f[i]<<' ';
  cout<<endl;
  */
  for (int i = 1; i <= p; i++) {
    cin >> a >> b;
    if (gf(a) == gf(b))
      cout << "Yes\n";
    else
      cout << "No\n";
  }
  return 0;
}
