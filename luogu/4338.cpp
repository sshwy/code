// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

typedef long long LL;
const int N = 5e5 + 5;
LL ans;
int big[N];
bool isHeavy[N];

struct Data {
  LL a, b, taga;
  void addA(LL x) { a += x, taga += x; }
  void addB(LL x) { a += x, b += x, taga += x; }
};

namespace LCT {
  int ch[N][2], fa[N];
  Data s[N];

  bool isRoot(int u) { return ch[fa[u]][0] != u && ch[fa[u]][1] != u; }
  bool get(int u) { return ch[fa[u]][1] == u; }

  void pushdown(int u) {
    if (s[u].taga) {
      int tg = s[u].taga;
      if (ch[u][0]) s[ch[u][0]].addA(tg);
      if (ch[u][1]) s[ch[u][1]].addA(tg);
      s[u].taga = 0;
    }
  }
  void rotate(int u) {
    int p = fa[u], pp = fa[p], gu = get(u), b = ch[u][!gu];
    pushdown(p), pushdown(u);
    if (!isRoot(p) && pp) ch[pp][get(p)] = u;
    fa[u] = pp, fa[b] = p, ch[p][gu] = b, ch[u][!gu] = p, fa[p] = u;
  }
  void splay(int u) {
    while (!isRoot(u)) {
      pushdown(u);
      int p = fa[u];
      if (!isRoot(p)) get(u) == get(p) ? rotate(p) : rotate(u);
      rotate(u);
    }
    pushdown(u);
  }
  LL eval(int u) {
    splay(u), pushdown(u);
    LL au = s[u].a, amax = s[u].b;
    if (big[u]) {
      int v = big[u];
      splay(v), pushdown(v);
      amax = max(amax, s[v].a);
    }
    // printf("               au %lld amax %lld\n", au, amax);
    LL res = min(au - 1, 2 * (au - amax));
    res = max(res, 0ll);
    // printf("    eval %d = %lld\n", u, res);
    return res;
  }
  int findFirst(int u) {
    splay(u);
    while (ch[u][0]) pushdown(u), u = ch[u][0];
    splay(u);
    return u;
  }
  void add(int u, int val) {
    printf("\033[31mLCT::add %d %d\033[0m\n", u, val);
    ans -= eval(u);
    splay(u);
    for (int z = fa[u]; z; z = fa[z]) {
      ans -= eval(z);
      splay(z);
    }

    // step1: add tag
    // step2: update ans
    if (big[u] && isHeavy[big[u]]) {
      int v = big[u];
      splay(u), splay(v);
      // printf("u %d v %d\n", u, v);
      // printf("ls %d : %d %d\n", v, ch[v][0], ch[ch[v][0]][1]);
      assert(ch[v][0] == u);
      s[u].addB(val);
      if (s[v].a * 2 <= s[u].a) {
        // printf("cut %d %d\n", u, v);
        splay(u), assert(ch[u][1] == v);
        ch[u][1] = 0, isHeavy[v] = false; // cut
      }
    } else {
      splay(u);
      // printf("u %d %d rc %d\n", u, fa[ch[u][1]], ch[u][1]);
      assert(ch[u][1] == 0);
      s[u].addB(val);
    }
    ans += eval(u);
    splay(u);
    assert(fa[u] < u);
    for (int v = findFirst(u), z = fa[v]; z; v = findFirst(z), z = fa[v]) {
      int bz = big[z];
      assert(bz);

      splay(z), splay(bz);
      s[z].addA(val);

      if (isHeavy[bz] && s[bz].a * 2 <= s[z].a) {
        splay(z);
        printf("z %d rc %d bz %d\n", z, ch[z][1], bz);
        assert(ch[z][1] == bz);
        // printf("cut %d %d\n", z, bz);
        ch[z][1] = 0, isHeavy[bz] = false;
      }
      if (s[v].a > s[bz].a) {
        assert(isHeavy[bz] == false);
        big[z] = v;
      }
      if (!isHeavy[v] && s[v].a * 2 > s[z].a) {
        // printf("link %d %d\n", z, v);
        splay(z), pushdown(z);
        // printf("z %d\n", z);
        assert(ch[z][1] == 0);
        splay(v);
        isHeavy[v] = true, ch[z][1] = v;
        assert(z < v);
      }
      ans += eval(z);
    }
  }
} // namespace LCT

int n, m;
LL a[N];
vector<int> g[N];

void dfs(int u, int p) {
  LCT::fa[u] = p;
  for (int v : g[u])
    if (v != p) dfs(v, u), big[u] = v;
}
void dfs2(int u, int p) {
  LCT::add(u, a[u]);
  for (int v : g[u])
    if (v != p) dfs2(v, u);
}
int main() {
  scanf("%d%d", &n, &m);
  FOR(i, 1, n) scanf("%lld", a + i);
  FOR(i, 1, n - 1) {
    int u, v;
    scanf("%d%d", &u, &v);
    g[u].pb(v);
    g[v].pb(u);
  }
  dfs(1, 0);
  dfs2(1, 0);
  printf("%lld\n", ans);
  FOR(i, 1, m) {
    int x, y;
    scanf("%d%d", &x, &y);
    LCT::add(x, y);
    printf("%lld\n", ans);
  }
  return 0;
}
