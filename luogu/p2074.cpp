#include <iostream>

using namespace std;

int n, m, k, t, x, y, tot;
bool qp[10001][10001];

int main() {
  cin >> n >> m >> k >> t;
  for (int i = 1; i <= k; i++) {
    cin >> x >> y;
    for (int i = x > t ? x - t : 1; i < (x + t < n ? x + t : n); i++) {
      for (int j = y > t ? y - t : 1; j < (y + t < m ? y + t : m); j++) {
        if ((i - x) * (i - x) + (j - y) * (j - y) <= t * t && qp[i][j] == false)
          qp[i][j] = true, tot++;
      }
    }
  }
  cout << tot;
  return 0;
}
