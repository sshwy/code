// by Sshwy
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

using namespace RA;

int main() {
  srand(clock() + time(0));
  int n = rnd(2, 10), m = rnd(2, 16), r = rnd(1, n);
  printf("%d %d %d\n", n, m, r);
  FOR(i, 1, m) {
    int u = rnd(1, n), v = rnd(1, n), w = rnd(1, 7);
    while (u == v) u = rnd(1, n);
    printf("%d %d %d\n", u, v, w);
  }
  return 0;
}
