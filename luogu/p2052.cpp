#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
const int N = 3000006;
int n;
struct qxx {
  int nex, t, v;
};
qxx e[N * 2];
int head[N], cnt;
bool vis[N];
void add_path(int f, int t, int v) {
  e[++cnt] = (qxx){head[f], t, v};
  head[f] = cnt;
}

struct node {
  int p, s, b;
  ll v; //即连向它父节点的边的权值
  int sz;
};
node t[N];
void add_son(int p, int s, ll v) {
  t[s].p = p, t[s].v = v;
  t[s].b = t[p].s, t[p].s = s;
}
int szdfs(int rt) {
  vis[rt] = true, t[rt].sz = 1;
  for (int i = head[rt]; i; i = e[i].nex) {
    if (!vis[e[i].t]) {
      add_son(rt, e[i].t, e[i].v);
      t[rt].sz += szdfs(e[i].t);
    }
  }
  return t[rt].sz;
}
int mabs(int k) { return k < 0 ? 0 - k : k; }
int main() {
  scanf("%d", &n);
  for (int i = 1, a, b, c; i < n; i++) {
    scanf("%d%d%d", &a, &b, &c);
    add_path(a, b, c);
    add_path(b, a, c);
  }
  szdfs(1);
  long long ans = 0;
  for (int i = 2; i <= n; i++) { ans += mabs(n - 2 * t[i].sz) * t[i].v; }
  printf("%lld", ans);
  return 0;
}