#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
namespace IO {
  char nc() {
    static char bf[100000], *p1 = bf, *p2 = bf;
    return p1 == p2 && (p2 = (p1 = bf) + fread(bf, 1, 100000, stdin), p1 == p2)
               ? EOF
               : *p1++;
  }
  int rd() {
    int res = 0;
    char c = getchar();
    while (!isdigit(c)) c = getchar();
    while (isdigit(c)) res = res * 10 + c - '0', c = getchar();
    return res;
  }
} // namespace IO
/******************heading******************/
#define int lld
const int N = 1e5 + 5, MTX = 2, INF = 0x3f3f3f3f, LST = N << 2;

int n, m;
int val[N];

struct qxx {
  int nex, t;
};
qxx e[N * 2];
int h[N], le;
void add_path(int f, int t) { e[++le] = (qxx){h[f], t}, h[f] = le; }

struct mtx { /*{{{*/
  int r, c;
  int w[MTX][MTX];
  mtx() { r = c = 0; }
  mtx(int _r, int _c) {
    r = _r, c = _c;
    FOR(i, 0, r - 1) FOR(j, 0, c - 1) w[i][j] = -INF;
  }
  mtx(int _r) {
    r = c = _r;
    FOR(i, 0, r - 1) FOR(j, 0, c - 1) w[i][j] = i == j ? 0 : -INF;
  }
  mtx(int _r, int _c, int _w[MTX][MTX]) {
    r = _r, c = _c;
    FOR(i, 0, r - 1) FOR(j, 0, c - 1) w[i][j] = _w[i][j];
  }
  mtx operator*(mtx m) {
    // assert(c==m.r);
    mtx res(r, m.c);
    FOR(i, 0, r - 1)
    FOR(j, 0, m.c - 1)
    FOR(k, 0, c - 1) res.w[i][j] = max(res.w[i][j], w[i][k] + m.w[k][j]);
    return res;
  }
  mtx operator*=(mtx m) { return *this = *this * m; }
  void print() {
    FOR(i, 0, r - 1) {
      printf("|");
      FOR(j, 0, c - 1) if (w[i][j] == -INF) printf("-INF");
      else printf("%3lld ", w[i][j]);
      printf("|");
      puts("");
    }
    puts("");
  }
}; /*}}}*/

mtx tree_mt[N];
int totdfn;

namespace seg {
  mtx mt[LST];
  void pushup(int u) {
    mt[u] = mt[u << 1] * mt[u << 1 | 1]; // left multiply
  }
  void build(int u = 1, int l = 1, int r = totdfn) {
    if (l == r) return mt[u] = tree_mt[l], void();
    int mid = (l + r) >> 1;
    build(u << 1, l, mid), build(u << 1 | 1, mid + 1, r);
    pushup(u);
  }
  mtx query(int L, int R, int u = 1, int l = 1, int r = totdfn) {
    if (L <= l && r <= R) return mt[u];
    int mid = (l + r) >> 1;
    mtx res(MTX);
    if (L <= mid) res *= query(L, R, u << 1, l, mid);
    if (mid < R) res *= query(L, R, u << 1 | 1, mid + 1, r);
    return res;
  }
  void assign(int p, mtx matrix, int u = 1, int l = 1, int r = totdfn) {
    if (l == r) return mt[u] = matrix, void();
    int mid = (l + r) >> 1;
    if (p <= mid)
      assign(p, matrix, u << 1, l, mid);
    else
      assign(p, matrix, u << 1 | 1, mid + 1, r);
    pushup(u);
  }
} // namespace seg

int dep[N], hvs[N], sz[N], tp[N], bt[N], fa[N], dfn[N];
int f[N][2], g[N][2];
int dfs1(int u, int p) { /*{{{*/
  dep[u] = dep[p] + 1, sz[u] = 1;
  fa[u] = p;
  for (int i = h[u]; i; i = e[i].nex) {
    const int v = e[i].t;
    if (v == p) continue;
    sz[u] += dfs1(v, u);
  }
  return sz[u];
} /*}}}*/
void dfs2(int u, int p, int to) { /*{{{*/
  tp[u] = to, bt[u] = u, dfn[u] = ++totdfn;
  int mx = 0;
  for (int i = h[u]; i; i = e[i].nex) {
    const int v = e[i].t;
    if (v == p) continue;
    if (sz[v] > mx) mx = sz[v], hvs[u] = v;
  }
  if (hvs[u]) dfs2(hvs[u], u, to), bt[u] = bt[hvs[u]];
  for (int i = h[u]; i; i = e[i].nex) {
    const int v = e[i].t;
    if (v == p || v == hvs[u]) continue;
    dfs2(v, u, v);
  }
  // printf("u=%lld,hvs=%lld,tp=%lld,bt=%lld\n",u,hvs[u],tp[u],bt[u]);
} /*}}}*/
void dfs3(int u, int p) {
  for (int i = h[u]; i; i = e[i].nex) {
    const int v = e[i].t;
    if (v == p) continue;
    dfs3(v, u);
    if (v != hvs[u])
      g[u][0] += max(f[v][0], f[v][1]), g[u][1] += f[v][0];
    else
      f[u][0] = max(f[v][0], f[v][1]), f[u][1] = f[v][0];
  }
  g[u][1] += val[u];
  f[u][0] += g[u][0], f[u][1] += g[u][1];
  // printf("u=%-2d,f0=%-3d,f1=%-4d,g0=%-3d,g1=%-3d\n",u,f[u][0],f[u][1],g[u][0],g[u][1]);
  int t[MTX][MTX] = {g[u][0], g[u][0], g[u][1], -INF};
  tree_mt[dfn[u]] = mtx(2, 2, t);
  // tree_mt[dfn[u]].print();
}

mtx f_mtx(int u) { // calculate f[u][0],f[u][1] -> g_mtx[0][0],g_mtx[1][0]
  return seg::query(dfn[u], dfn[bt[u]]);
}
void assign(int u, int v) { // val[u]=v
  // printf("assign(%lld,%lld)\n",u,v);
  g[u][1] = g[u][1] - val[u] + v, val[u] = v;
  while (u) {
    // printf("u=%lld\n",u);
    int t[MTX][MTX] = {g[u][0], g[u][0], g[u][1], -INF};
    seg::assign(dfn[u], mtx(2, 2, t));
    u = tp[u];
    mtx a = f_mtx(u); //当前链顶的f
    if (fa[u]) {
      g[fa[u]][1] = g[fa[u]][1] - f[u][0] + a.w[0][0];
      g[fa[u]][0] = g[fa[u]][0] - max(f[u][0], f[u][1]) + max(a.w[0][0], a.w[1][0]);
    }
    f[u][0] = a.w[0][0], f[u][1] = a.w[1][0];
    u = fa[u];
  }
}
signed main() {
  scanf("%lld%lld", &n, &m);
  FOR(i, 1, n) scanf("%lld", &val[i]);
  FOR(i, 1, n - 1) {
    int x, y;
    scanf("%lld%lld", &x, &y);
    add_path(x, y), add_path(y, x);
  }
  dfs1(1, 0);
  dfs2(1, 0, 1);
  dfs3(1, 0);
  seg::build();
  FOR(i, 1, m) {
    int x, y;
    scanf("%lld%lld", &x, &y);
    assign(x, y);
    mtx ans = f_mtx(1);
    printf("%lld\n", max(ans.w[0][0], ans.w[1][0]));
  }
  return 0;
}
/*
 * g[u,0]=sum_{v!=hvs[u]} max(f[v,0],f[v,1])
 * g[u,1]=a[u] + sum_{v!=hvs[u]} f[v,0]
 * f[u,0]=g[u,0]+max(f[u+1,0],f[u+1,1])
 * f[u,1]=g[u,1]+f[u+1,0]
 * f[leaf,0]=g[leaf,0]=0
 * f[leaf,1]=g[leaf,1]=a[leaf]
 */
