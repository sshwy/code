#include <iostream>
#include <vector>

using namespace std;

int n, m, a, b, k;
int attack[400001];
int ltk[400001];
int f[400001];
int liantongkuai_count;

bool unavailable[400001]; // true:destoryed

vector<int> link[400001];

int gf(int k) {
  if (f[k] == k) return k;
  f[k] = gf(f[k]);
  return f[k];
}

void un(int p1, int p2) {
  if (gf(p1) == gf(p2)) return;
  f[gf(p1)] = gf(p2);
  liantongkuai_count--;
}

int main() {
  cin >> n >> m;
  for (int i = 1; i <= m; i++) {
    cin >> a >> b;
    link[a].push_back(b);
    link[b].push_back(a);
  }
  cin >> k;
  for (int i = 1; i <= k; i++) {
    cin >> attack[i];
    unavailable[attack[i]] = true;
  }
  liantongkuai_count = n - k;
  for (int i = 1; i <= n; i++) f[i] = i;
  for (int i = 1; i <= n; i++) {
    if (unavailable[i] == false) {
      for (int j = 0; j < link[i].size(); j++) {
        if (unavailable[link[i][j]] == false) {
          un(i, link[i][j]);
          // cout<<"un("<<i<<","<<link[i][j]<<");\n";
        }
      }
    }
  }
  ltk[k] = liantongkuai_count;
  for (int i = k; i >= 1; i--) {
    if (unavailable[attack[i]] == true) {
      unavailable[attack[i]] = false;
      liantongkuai_count++;
      for (int j = 0; j < link[attack[i]].size(); j++) {
        if (unavailable[link[attack[i]][j]] == false)
          un(attack[i], link[attack[i]][j]);
      }
      ltk[i - 1] = liantongkuai_count;
    }
  }
  for (int i = 0; i <= k; i++) { cout << ltk[i] << endl; }
  return 0;
}
