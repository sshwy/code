#include <iostream>
#include <string>

#define min(x1, x2) (x1 < x2 ? x1 : x2)

using namespace std;

int n, mxl;
char a;
string w[25];
int vis[25];

int common(string pre, string suf) {
  int lp = pre.size(), ls = suf.size();
  int lc1 = min(lp, ls); // while(--lc1)
  for (int lc = 1; lc < lc1; lc++) {
    // pre[lp-lc...lp-1],suf[0...lc-1]
    bool f = true;
    for (int ip = lp - lc, is = 0; is < lc; ip++, is++) {
      if (pre[ip] != suf[is]) {
        f = false;
        break;
      }
    }
    if (f) return lc;
  }
  return 0;
}

void search(int nth, int l) {
  if (vis[nth] == 2) return;
  vis[nth]++;
  if (l > mxl) mxl = l;
  for (int i = 1, t; i <= n; i++)
    if (t = common(w[nth], w[i])) search(i, l - t + w[i].size());
  vis[nth]--;
}

int main() {
  cin >> n;
  for (int i = 1; i <= n; i++) cin >> w[i];
  cin >> a;
  for (int i = 1; i <= n; i++) {
    if (w[i][0] == a) search(i, w[i].size());
  }
  cout << mxl;
  return 0;
}
