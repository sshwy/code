#include <algorithm>
#include <assert.h>
#include <cstdio>
#include <cstring>
using namespace std;
const int N = 1e6 + 5, M = 1e6 + 5;
int n, m;
int a[N];

#define RED "\033[41m"
#define GRE "\033[42m"
#define NONE "\033[0m"

struct data {
  int type, i, j, k, id;
  //对于type==0->询问，那么ijk就是对应的意思
  //对于type==1->修改，那么i是下标位置，j是键值，k是修改值。
  /*void print(){
      if(type==0){
          printf(GRE"[QUERY]"NONE" [%d,%d] %d -th\n",i,j,k);
      }
      else {
          printf(RED"[MODIFY]"NONE" %d %d %d\n",i,j,k);
      }
  }*/
};
data d[M * 2];
int ld;

namespace BIT { //树状数组
  int c[N];
  void add(int pos, int val) {
    for (int i = pos; i <= n; i += i & -i) c[i] += val;
  }
  int pre(int pos) {
    int res = 0;
    for (int i = pos; i > 0; i -= i & -i) res += c[i];
    return res;
  }
} // namespace BIT

int ans[M];

data q1[N], q2[N];
void solve(int l, int r, int L, int R) {
  //对应的操作序列是d[l..r]，答案的二分区间是[L,R]
  if (l > r) return;

  /*printf("solve(%d,%d,%d,%d)\n",l,r,L,R);
  for(int i=l;i<=r;i++)d[i].print();
  puts("");*/

  if (L == R) { //递归边界情况
    for (int i = l; i <= r; i++)
      if (d[i].type == 0) ans[d[i].id] = L;
    return;
  }
  int mid = (L + R) >> 1;
  int l1 = 0, l2 = 0;            //一定要放在里面定义!
  for (int i = l; i <= r; i++) { //将操作归类
    data now = d[i];
    if (now.type == 0) {
      int t = BIT::pre(now.j) - BIT::pre(now.i - 1);
      if (now.k <= t)
        q1[++l1] = now;
      else
        now.k -= t, q2[++l2] = now;
    } else {
      if (L <= now.j && now.j <= mid)
        BIT::add(now.i, now.k), q1[++l1] = now;
      else
        q2[++l2] = now;
    }
  }
  // assert(l1+l2==r-l+1);
  for (int i = 1; i <= l1; i++) //还原修改
    if (q1[i].type == 1) BIT::add(q1[i].i, -q1[i].k);

  // for(int i=1;i<=n;i++)assert(BIT::c[i]==0);//调试

  for (int i = 1; i <= l1; i++) d[l + i - 1] = q1[i];
  for (int i = 1; i <= l2; i++) d[l + l1 + i - 1] = q2[i];

  /*printf("分完后\n");
  for(int i=l;i<=r;i++)d[i].print();
  puts("");*/

  solve(l, l + l1 - 1, L, mid);
  solve(l + l1, r, mid + 1, R);
}

int main() {
  scanf("%d%d", &n, &m);
  for (int i = 1; i <= n; i++) { //把初始化转化为添加操作
    scanf("%d", &a[i]);
    d[++ld] = (data){1, i, a[i], 1};
  }
  for (int ii = 1; ii <= m; ii++) {
    char op[3];
    int i, j, k, t;
    scanf("%s", op);
    if (op[0] == 'Q') { //询问
      scanf("%d%d%d", &i, &j, &k);
      d[++ld] = (data){0, i, j, k, ii};
    } else { //修改
      scanf("%d%d", &i, &t);
      d[++ld] = (data){1, i, a[i], -1}, d[++ld] = (data){1, i, t, 1};
      a[i] = t; //第二发的时侯忘写这行了
    }
  }
  memset(ans, -1, sizeof(ans));
  solve(1, ld, 1, 1e9); //交的时侯忘把10改成1e9了
  for (int i = 1; i <= m; i++)
    if (~ans[i]) printf("%d\n", ans[i]);
  return 0;
}
/*
 * 又来写整体二分了。。。
 * 二分答案，然后针对二分的区间，将操作分为两部分
 * 对于当前答案区间[L,R]，二分的值为mid，那么我们知道这时序列中的操作
 * 的已经累加了[1,L)的贡献。而我们先处理[L,mid]，再处理[mid+1,r]
 */
