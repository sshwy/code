#include <iostream>

using namespace std;

int n, m, a;
int c[5], qp[400], f[41][41][41][41];

int main() {
  cin >> n >> m;
  for (int i = 0; i < n; i++) cin >> qp[i];
  for (int i = 1; i <= m; i++) {
    cin >> a;
    c[a]++;
  }
  for (int p1 = 0; p1 <= c[1]; p1++) {
    for (int p2 = 0; p2 <= c[2]; p2++) {
      for (int p3 = 0; p3 <= c[3]; p3++) {
        for (int p4 = 0; p4 <= c[4]; p4++) {
          if (p1 - 1 >= 0)
            f[p1][p2][p3][p4] = max(f[p1][p2][p3][p4], f[p1 - 1][p2][p3][p4]);
          if (p2 - 1 >= 0)
            f[p1][p2][p3][p4] = max(f[p1][p2][p3][p4], f[p1][p2 - 1][p3][p4]);
          if (p3 - 1 >= 0)
            f[p1][p2][p3][p4] = max(f[p1][p2][p3][p4], f[p1][p2][p3 - 1][p4]);
          if (p4 - 1 >= 0)
            f[p1][p2][p3][p4] = max(f[p1][p2][p3][p4], f[p1][p2][p3][p4 - 1]);
          f[p1][p2][p3][p4] += qp[p1 * 1 + p2 * 2 + p3 * 3 + p4 * 4];
        }
      }
    }
  }
  cout << f[c[1]][c[2]][c[3]][c[4]];
  return 0;
}
