#include <iostream>
#include <queue>

using namespace std;

int k, m;
queue<int> q;

int ysy(int g) {
  while (!q.empty()) q.pop();
  for (int i = 1; i <= k; i++) q.push(1); // good
  for (int i = 1; i <= k; i++) q.push(0); // bad

  int kp = 0, now; // killed person
  while (!q.empty()) {
    for (int i = 1; i < g; i++) {
      q.push(q.front());
      q.pop();
    }
    now = q.front();
    q.pop();
    if (now == 1)
      return kp;
    else
      kp++;
  }
  return -1;
}

int main() {
  cin >> k;
  for (int i = 1; i <= 500000; i++) {
    if (i % 1000 == 0) cout << i << endl;
    if (ysy(i) == k) {
      cout << i;
      return 0;
    }
  }
  return 0;
}
