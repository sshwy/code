// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

const int N = 3005;

int n, m, q, cdom[N], fa[N], L[N], R[N], totdfn;
vector<int> g[N], rg[N], domT[N], dom[N];
bool vis[N], h[N][N];

void ae(int u, int v) {
  // printf("ae %d %d\n", u, v);
  fa[v] = u;
  domT[u].pb(v);
}

void dfs(int u, int ban) {
  vis[u] = true;
  for (int v : g[u])
    if (v != ban && !vis[v]) dfs(v, ban);
}
void dfsT(int u) {
  L[u] = ++totdfn;
  for (int v : domT[u]) dfsT(v);
  R[u] = totdfn;
}
void buildDomTree() {
  FOR(i, 1, n) {
    memset(vis, 0, sizeof(vis));
    if (i != 1) dfs(1, i);
    FOR(j, 1, n) if (!vis[j]) dom[i].pb(j), cdom[j]++;
  }
  queue<int> q;
  q.push(1);
  while (!q.empty()) {
    int u = q.front();
    q.pop();
    cdom[u] = 0;
    for (int v : dom[u]) {
      cdom[v]--;
      if (cdom[v] == 1) {
        ae(u, v);
        q.push(v);
      }
    }
  }
  dfsT(1);
}

void dfsR(int u, int ban) {
  vis[u] = true;
  for (int v : rg[u])
    if (v != ban && !vis[v]) dfsR(v, ban);
}
void calcH() {
  FOR(i, 2, n) {
    assert(fa[i]);
    memset(vis, 0, sizeof(vis));
    dfsR(i, fa[i]);
    FOR(j, 1, n) h[i][j] = vis[j];
  }
}

int t[N];
int query(int x, int y) {
  int res = 0;
  memset(vis, 0, sizeof(vis));
  memset(t, 0, sizeof(t));
  for (int u = x; u; u = fa[u]) vis[u] = true;
  FOR(i, 2, n) {
    if (!vis[fa[i]] && h[i][y]) {
      t[L[i]]++;
      t[R[i] + 1]--;
    }
  }
  FOR(i, 1, n) t[i] += t[i - 1];
  FOR(i, 1, n) res += !!t[i];
  return res;
}

int main() {
  scanf("%d%d%d", &n, &m, &q);
  FOR(i, 1, m) {
    int u, v;
    scanf("%d%d", &u, &v);
    g[u].pb(v);
    rg[v].pb(u);
  }
  buildDomTree();
  calcH();
  FOR(i, 1, q) {
    int x, y;
    scanf("%d%d", &x, &y);
    printf("%d\n", query(x, y));
  }
  return 0;
}
