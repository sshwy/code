#include <cstdio>
#include <iostream>
using namespace std;
const int N = 1e5 + 5, M = 1e5 + 5, C = 1e6 + 6;
int n, m;
int a[N];

int h[C], ed[C], nex[N], f[C], cnt[C];
int tot;

void merge(int x, int y) { //将两个链表连接
  for (int i = h[x]; i; i = nex[i]) tot -= (a[i - 1] == y) + (a[i + 1] == y);
  for (int i = h[x]; i; i = nex[i]) a[i] = y;
  nex[ed[x]] = h[y], h[y] = h[x], cnt[y] += cnt[x];
  h[x] = ed[x] = cnt[x] = 0;
}

int main() {
  scanf("%d%d", &n, &m);
  for (int i = 1; i <= n; i++) {
    scanf("%d", &a[i]);
    tot += a[i] != a[i - 1];
    if (!h[a[i]]) ed[a[i]] = i;
    nex[i] = h[a[i]], h[a[i]] = i;
    f[a[i]] = a[i], cnt[a[i]]++;
  }
  for (int i = 1; i <= m; i++) {
    int op, x, y;
    scanf("%d", &op);
    if (op == 1) {
      scanf("%d%d", &x, &y);
      if (x == y) continue;
      if (cnt[f[x]] > cnt[f[y]]) swap(f[x], f[y]); //交换颜色
      if (!cnt[f[x]]) continue; //如果一个颜色不存在，那么就不用管了
      merge(f[x], f[y]);
    } else {
      printf("%d\n", tot);
    }
  }
  return 0;
}
/*
 * BUG#1: L12L13不能圧成一行
 * BUG#2: L34没有用cnt
 * BUG#3: 合并的时侯把x合并到了y后面，又没有更新ed[y]emmm
 */
