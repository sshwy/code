#include <cstring>
#include <iostream>

using namespace std;

int lk, lc;
char k[1001], m[1001], c[1001];

char r(char t, char p) {
  if (t >= 'A' && t <= 'Z') {
    if (t - p < 'A')
      return t - p + 26;
    else
      return t - p;
  } else {
    if (t - p < 'a')
      return t - p + 26;
    else
      return t - p;
  }
}

int main() {
  cin >> k >> c;
  lk = strlen(k);
  lc = strlen(c);
  for (int i = 0; i < lk; i++) {
    if (k[i] >= 'A' && k[i] <= 'Z')
      k[i] -= 65;
    else
      k[i] -= 97;
  }
  for (int i = 0; i < lc; i++) { m[i] = r(c[i], k[i % lk]); }
  m[lc] = '\0';
  cout << m;
  return 0;
}
