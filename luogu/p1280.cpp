#include <algorithm>
#include <iostream>

using namespace std;

int n, k, ts[10001], f[10001];

struct task {
  int l, r;
};
task t[10001];

bool cmp(task t1, task t2) { return t1.l > t2.l; }
int main() {
  cin >> n >> k;
  for (int i = 1; i <= k; i++) {
    cin >> t[i].l >> t[i].r;
    t[i].r = t[i].r + t[i].l;
    ts[t[i].l]++;
  }
  sort(t + 1, t + k + 1, cmp);

  for (int i = n, nt = 1; i >= 1; i--) {
    if (ts[i]) {
      for (int j = 1; j <= ts[i]; j++) {
        if (f[t[nt].r] > f[i]) f[i] = f[t[nt].r];
        nt++;
      }
    } else {
      f[i] = f[i + 1] + 1;
    }
  }
  cout << f[1];
  return 0;
}
