#include <iostream>

using namespace std;

int n, m, d[10010];
int f[10010][510];

int F(int t, int p) {
  if (t < 0 || p > m || t < p) return 0 - 999999999;
  if (f[t][p]) return f[t][p];
  int t1 = F(t - 1, p + 1), t2 = F(t - 1, p - 1) + d[t - 1];
  printf("F(%d,%d)\n", t, p);
  return f[t][p] = max(t1, t2);
}

int main() {
  cin >> n >> m;
  for (int i = 1; i <= n; i++) cin >> d[i];
  f[0][0] = 1;
  cout << F(n, 0);
  return 0;
}
