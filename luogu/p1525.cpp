#include <algorithm>
#include <iostream>

using namespace std;

int n, m, p, mx;
int f[20001];

struct rela {
  int f, t, v;
} md[100001];

bool cmp(rela r1, rela r2) { return r1.v < r2.v; }

int gf(int k) { return f[k] == k ? k : f[k] = gf(f[k]); }
void un(int u1, int u2) { f[gf(u1)] = gf(u2); }

int main() {
  cin >> n >> m;
  for (int i = 1; i <= m; i++) cin >> md[i].f >> md[i].t >> md[i].v;
  sort(md + 1, md + 1 + m, cmp);
  for (int i = 1; i <= n; i++) f[i] = i;
  p = n;
  for (int i = 1; i <= m; i++) {
    if (gf(md[i].f) != gf(md[i].t)) {
      un(md[i].f, md[i].t);
      cout << "un(" << md[i].f << "," << md[i].t << ")\n";
      p--;
    }
    mx = md[i].v;
    if (p == 2) {
      for (int j = i; j <= m; j++) {
        if (gf(md[j].f) == gf(md[j].t)) mx = md[j].v;
      }
    }
  }
  cout << mx;
  return 0;
}
