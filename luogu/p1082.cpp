#include <iostream>

using namespace std;

int a1, b1, x1, y1, g;

int gcd(int a, int b) { return b ? gcd(b, a % b) : a; }

int exgcd(int a, int b, int &x, int &y) {
  if (b == 0) return x = 1, y = 0, a;
  int q = exgcd(b, a % b, y, x);
  return y -= a / b * x, q;
}

int main() {
  cin >> a1 >> b1;
  g = gcd(a1, b1);
  exgcd(a1, b1, x1, y1);
  cout << (x1 / g + b1) % b1;
  return 0;
}
