#include <iostream>
#define INF 9999999

using namespace std;

char a, b;
int p, c, mn = INF, cow;
int d[150][150];

int main() {
  cin >> p;
  for (int i = 1; i < 150; i++) {
    for (int j = 1; j < 150; j++) d[i][j] = INF;
    d[i][i] = 0;
  }
  for (int i = 1; i <= p; i++) {
    cin >> a >> b >> c;
    if (c < d[a][b]) d[a][b] = d[b][a] = c;
  }
  for (int k = 'A'; k <= 'z'; k++)
    for (int i = 'A'; i <= 'z'; i++)
      for (int j = 'A'; j <= 'z'; j++)
        if (d[i][j] > d[i][k] + d[k][j]) d[i][j] = d[i][k] + d[k][j];
  for (int i = 'A'; i < 'Z'; i++)
    if (d[i]['Z'] < mn) cow = i, mn = d[i]['Z'];
  cout << (char)(cow) << ' ' << mn;
  return 0;
}
