#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
ll n, m, ans = 0, qp[201][201];

ll low_bound(ll *arr, ll l, ll r, ll v) {
  while (l < r) {
    ll mid = (l + r) >> 1;
    if (v > arr[mid])
      r = mid;
    else
      l = mid + 1;
  }
  return l;
}
int main() {
  scanf("%lld%lld", &n, &m);
  for (ll i = 1; i <= n; i++) {
    for (ll j = 1; j <= m; j++) {
      scanf("%lld", &qp[i][j]);
      qp[i][j] += qp[i - 1][j]; //纵向前缀和
    }
  }
  for (ll i = 0; i < n; i++) {
    for (ll j = i + 1; j <= n; j++) {    //上下边界
      ll q[201], p[201], fr = 1, bk = 1; //维护单调队列
      ll pre = 0;                        //当前的前缀和
      q[1] = p[1] = 0;                   //初始时有一个0
      for (ll k = 1; k <= m; k++) {
        pre += qp[j][k] - qp[i][k];
        if (pre < q[bk]) q[++bk] = pre, p[bk] = k; //入队
        ll t = (k - p[low_bound(q, fr, bk, pre)]) * (j - i);
        ans = max(ans, t);
      }
    }
  }
  printf("%lld", ans);
  return 0;
}