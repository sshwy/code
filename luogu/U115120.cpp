// by Sshwy
//#define DEBUGGER
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

#ifdef DEBUGGER

#define log_prefix()                                                \
  {                                                                 \
    fprintf(stderr, "\033[37mLine %-3d [%dms]:\033[0m  ", __LINE__, \
        (int)clock() / 1000);                                       \
  }
#define ilog(...) \
  { fprintf(stderr, __VA_ARGS__); }
#define llog(...)      \
  {                    \
    log_prefix();      \
    ilog(__VA_ARGS__); \
  }
#define log(...)           \
  {                        \
    log_prefix();          \
    ilog(__VA_ARGS__);     \
    fprintf(stderr, "\n"); \
  }
#define red(...)       \
  {                    \
    log_prefix();      \
    ilog("\033[31m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define green(...)     \
  {                    \
    log_prefix();      \
    ilog("\033[32m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define blue(...)      \
  {                    \
    log_prefix();      \
    ilog("\033[34m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }

#else

#define log_prefix() ;
#define ilog(...) ;
#define llog(...) ;
#define log(...) ;
#define red(...) ;
#define green(...) ;
#define blue(...) ;

#endif

namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 18, NN = 1 << N;
int n, nn, U;
int a[NN], b[NN], c[NN];
int f[N + 1][NN];
void dfs(int T, int dep) {
  for (int S = U - T; S; S = (S - 1) & (U - T)) {
    c[S | T] = max(c[S | T], f[dep][S] + a[T]);
  }
  c[0 | T] = max(c[0 | T], f[dep][0] + a[T]);
  FOR(x, 0, n - 1) if ((1 << x) > T && !(T >> x & 1)) {
    int T1 = T | (1 << x);
    for (int S = U - T1; S; S = (S - 1) & (U - T1)) {
      f[dep + 1][S] = max(f[dep][S], f[dep][S | (1 << x)]);
    }
    f[dep + 1][0] = max(f[dep][0], f[dep][0 | (1 << x)]);
    dfs(T1, dep + 1);
  }
}
int main() {
  scanf("%d", &n);
  nn = 1 << n;
  U = nn - 1;
  FOR(i, 0, nn - 1) scanf("%d", &a[i]);
  FOR(i, 0, nn - 1) scanf("%d", &b[i]);

  FOR(i, 0, nn - 1) f[0][i] = b[i];
  dfs(0, 0);
  FOR(i, 0, nn - 1) printf("%d%c", c[i], " \n"[i == nn - 1]);
  return 0;
}
