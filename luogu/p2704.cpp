#include <cstdio>
#include <iostream>
using namespace std;
const int N = 102, M = 10;
int n, m, ans;
int fd[N];                     // field，每一行用二进制数表示
int s[1 << M], v[1 << M], cnt; //可行的状态集
int f[N][65][65]; //本来应该是1<<M，后来发现可行状态的数量最大为60个
char c;

int main() {
  scanf("%d%d", &n, &m);
  for (int i = 1; i <= n; i++) {
    for (int j = 1; j <= m; j++) {
      cin >> c;
      if (c == 'P')
        fd[i] = fd[i] << 1;
      else
        fd[i] = fd[i] << 1 | 1;
    }
  }
  int mxs = 1 << m; //状态上限
  //预处理可行的状态集
  for (int i = 0; i < mxs; i++) {
    if (!(i & (i << 1)) && !(i & (i >> 1)) && !(i & (i << 2)) && !(i & (i >> 2))) {
      s[++cnt] = i;
      for (int j = i; j; j -= j & -j) ++v[cnt];
    }
  }
  // init
  for (int i = 1; i <= cnt; i++) {
    for (int j = 1; j <= cnt; j++) {
      if (!(fd[1] & s[i]) && !(s[i] & s[j])) {
        f[1][i][j] = v[i];
        ans = max(ans, v[i]);
      }
    }
  }
  // DP
  for (int i = 2; i <= n; i++) {
    for (int j = 1; j <= cnt; j++)
      if (!(fd[i] & s[j])) { // f[i][j]
        for (int k = 1; k <= cnt; k++)
          if (!(s[j] & s[k])) { // f[i-1][k]
            for (int p = 1; p <= cnt; p++)
              if (!(s[p] & s[k]) && !(s[p] & s[j])) {
                f[i][j][k] = max(f[i][j][k], f[i - 1][k][p] + v[j]);
                ans = max(ans, f[i][j][k]);
              }
          }
      }
  }
  printf("%d", ans);
  return 0;
}