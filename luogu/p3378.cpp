#include <iostream>

using namespace std;

const int INF = 99999999;
int heap[4000001], heap_size;
int n, a, x;

void fix_heap_up() {
  for (int i = heap_size; i > 1; i >>= 1) {
    if (heap[i >> 1] > heap[i]) heap[i >> 1] ^= heap[i] ^= heap[i >> 1] ^= heap[i];
  }
}
void fix_heap_down() {
  heap[1] = heap[heap_size];
  heap[heap_size] = INF;
  heap_size--;
  int i = 1;
  while (i < heap_size) {
    if (heap[i] <= heap[i << 1] && heap[i] <= heap[i << 1 | 1]) break;
    if (heap[i << 1] < heap[i << 1 | 1])
      heap[i << 1] ^= heap[i] ^= heap[i << 1] ^= heap[i], i <<= 1;
    else
      heap[i << 1 | 1] ^= heap[i] ^= heap[i << 1 | 1] ^= heap[i], i = i << 1 | 1;
  }
}

void heap_push(int v) {
  heap[++heap_size] = v;
  fix_heap_up();
}
void heap_pop() {
  heap[1] = INF;
  fix_heap_down();
}

int heap_top() { return heap[1]; }

bool heap_empty() { return (bool)heap_size; }

int main() {
  cin >> n;
  for (int i = 1; i <= n; i++) heap[i] = INF;
  for (int i = 1; i <= n; i++) {
    cin >> a;
    if (a == 1) {
      cin >> x;
      heap_push(x);
    } else if (a == 2) {
      cout << heap_top() << endl;
    } else {
      heap_pop();
    }
  }
  return 0;
}
