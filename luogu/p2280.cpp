#include <bits/stdc++.h>
using namespace std;
int n, r, tot, mx, my;
int qp[5002][5002];
int main() {
  scanf("%d%d", &n, &r);
  for (int i = 1, x, y, v; i <= n; i++) {
    scanf("%d%d%d", &x, &y, &v);
    int x2 = max(x - r + 1, 0), y2 = max(y - r + 1, 0);
    x++, y++;
    mx = max(x, mx), my = max(y, my);
    qp[x2][y2] += v, qp[x][y2] -= v, qp[x2][y] -= v, qp[x][y] += v;
  }
  tot = qp[0][0];
  for (int i = 1; i <= mx; i++) qp[i][0] += qp[i - 1][0], tot = max(qp[i][0], tot);
  for (int j = 1; j <= my; j++) qp[0][j] += qp[0][j - 1], tot = max(qp[0][j], tot);
  for (int i = 1; i <= mx; i++) {
    for (int j = 1; j <= my; j++) {
      qp[i][j] = qp[i][j] + qp[i][j - 1] + qp[i - 1][j] - qp[i - 1][j - 1];
      tot = max(qp[i][j], tot);
    }
  }
  printf("%d", tot);
  return 0;
}
