#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int P = 998243341;
const int N = 500;
const long double eps = 1e-100;

int n, m;
int pw[N];
int h[N][N];

bool a[N][N][N]; // a[i,j,len]：h[i][1..len]与h[j][m-len+1,m]是否匹配
long double A[N][N], pw2[N];

char s[N];

int main() {
  pw[0] = 1, pw2[0] = 1;
  FOR(i, 1, 300) pw[i] = pw[i - 1] * 2 % P, pw2[i] = pw2[i - 1] * 2;

  scanf("%d%d", &n, &m);
  FOR(i, 1, n) {
    scanf("%s", s + 1);
    FOR(j, 1, m) h[i][j] = (h[i][j - 1] + pw[j] * (s[j] == 'T')) % P;
  }

  FOR(i, 1, n)
  FOR(j, 1, n)
  FOR(k, 1, m)
  a[i][j][k] = 1ll * h[i][k] * pw[m - k] % P == (h[j][m] - h[j][m - k] + P) % P;

  FOR(i, 1, n) {
    FOR(j, 1, n) {
      long double c = 0;
      FOR(k, 1, m) c += pw2[k] * a[i][j][k];
      A[i][j] = c;
    }
    A[i][n + 1] = -1;
  }
  FOR(i, 1, n) A[n + 1][i] = 1;
  A[n + 1][0] = 1;

  // Gauss
  FOR(i, 1, n + 1) {
    FOR(j, i, n + 1) if (abs(A[j][i]) > eps) {
      FOR(k, 0, n + 1) swap(A[i][k], A[j][k]);
      break;
    }
    assert(abs(A[i][i]) > eps);
    FOR(j, 1, n + 1) {
      if (j == i) continue;
      long double r = A[j][i] / A[i][i];
      FOR(k, i, n + 1) A[j][k] -= A[i][k] * r;
      A[j][0] -= A[i][0] * r;
    }
  }

  FOR(i, 1, n + 1) A[i][0] /= A[i][i];
  FOR(i, 1, n) printf("%.10Lf\n", A[i][0]);
  return 0;
}
