#include <bits/stdc++.h>
using namespace std;
const int N = 10000;
int n, r = 1, ans;
struct node {
  int p, s, b;
  int v;
};
node t[N];
void add_son(int p, int s) { t[s].p = p, t[s].b = t[p].s, t[p].s = s; }

struct data {
  int idx, dp; // index,deep
  bool operator<(data tht) const { return dp < tht.dp; }
};
priority_queue<data> q;
bool del[N]; //延迟删除

void dfs_push(int rt, int dp) {
  q.push((data){rt, dp});
  for (int i = t[rt].s; i; i = t[i].b) dfs_push(i, dp + 1);
}
void mark(int rt) {
  for (int s = t[rt].s; s; s = t[s].b) {
    del[s] = true;
    for (int ss = t[s].s; ss; ss = t[ss].b) { del[ss] = true; }
  }
  for (int s = t[t[rt].p].s; s; s = t[s].b) { del[s] = true; }
  del[t[rt].p] = del[t[t[rt].p].p] = true;
}
int main() {
  scanf("%d", &n);
  for (int i = 2, x; i <= n; i++) {
    scanf("%d", &x);
    add_son(x, i);
  }
  while (t[r].p) r = t[r].p;
  dfs_push(r, 1);
  while (!q.empty()) {
    data now = q.top();
    q.pop();
    if (del[now.idx]) continue;
    if (t[t[now.idx].p].p)
      mark(t[t[now.idx].p].p);
    else if (t[now.idx].p)
      mark(t[now.idx].p);
    else
      mark(now.idx);
    ans++;
  }
  printf("%d", ans);
  return 0;
}