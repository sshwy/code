// by Sshwy
//#define DEBUGGER
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

#ifdef DEBUGGER

#define log_prefix()                                                \
  {                                                                 \
    fprintf(stderr, "\033[37mLine %-3d [%dms]:\033[0m  ", __LINE__, \
        (int)clock() / 1000);                                       \
  }
#define ilog(...) \
  { fprintf(stderr, __VA_ARGS__); }
#define llog(...)      \
  {                    \
    log_prefix();      \
    ilog(__VA_ARGS__); \
  }
#define log(...)           \
  {                        \
    log_prefix();          \
    ilog(__VA_ARGS__);     \
    fprintf(stderr, "\n"); \
  }
#define red(...)       \
  {                    \
    log_prefix();      \
    ilog("\033[31m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define green(...)     \
  {                    \
    log_prefix();      \
    ilog("\033[32m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define blue(...)      \
  {                    \
    log_prefix();      \
    ilog("\033[34m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }

#else

#define log_prefix() ;
#define ilog(...) ;
#define llog(...) ;
#define log(...) ;
#define red(...) ;
#define green(...) ;
#define blue(...) ;

#endif

namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 1e6 + 5;

int n, m, T;
int a[N];

struct data {
  int a, b, c, d;
} qry[N], op[N];
int lq, lo;

int L = 1, R = 0, O = 0, cur = 0;
int c1[N];

void insert(int x) {
  if (c1[x] == 0) ++cur;
  ++c1[x];
}
void remove(int x) {
  assert(c1[x]);
  c1[x]--;
  if (c1[x] == 0) --cur;
}
void addL() { remove(a[L++]); }
void subL() { insert(a[--L]); }
void addR() { insert(a[++R]); }
void subR() { remove(a[R--]); }
void addO() {
  ++O;
  int x = op[O].a, y = op[O].b, z = op[O].c;
  assert(a[x] == z);
  if (L <= x && x <= R) {
    remove(z);
    insert(y);
  }
  a[x] = y;
}
void subO() {
  int x = op[O].a, y = op[O].b, z = op[O].c;
  assert(a[x] == y);
  if (L <= x && x <= R) {
    remove(y);
    insert(z);
  }
  a[x] = z;
  --O;
}
bool cmp(data x, data y) {
  if (x.a / T != y.a / T) return x.a < y.a;
  if (x.b / T != y.b / T) return x.b < y.b;
  return x.c < y.c;
}
int ans[N];
int main() {
  scanf("%d%d", &n, &m);
  FOR(i, 1, n) scanf("%d", &a[i]);
  FOR(i, 1, m) {
    char s[5];
    int x, y;
    scanf("%s", s);
    scanf("%d%d", &x, &y);
    if (s[0] == 'Q') {
      qry[++lq] = {x, y, lo, lq};
    } else {
      op[++lo] = {x, y, a[x]}; // pos, 变成的值，变之前的值
      a[x] = y;
    }
  }
  O = lo;
  T = 2 * max((int)pow(n * 1ll * lo, 1.0 / 3), 1);
  log("T=%d", T);
  sort(qry + 1, qry + lq + 1, cmp);

  FOR(i, 1, lq) {
    // log("i=%d",i);
    int l = qry[i].a, r = qry[i].b, o = qry[i].c, id = qry[i].d;
    while (R < r) addR();
    while (L > l) subL();
    while (O < o) addO();
    while (R > r) subR();
    while (L < l) addL();
    while (O > o) subO();
    ans[id] = cur;
  }
  FOR(i, 1, lq) printf("%d\n", ans[i]);

  return 0;
}
