#include <iostream>

using namespace std;

int n, m;
int a[101];
int f[101][101]; // f[i][j]=f[i-1][j]+f[i-1][j-1]+f[i-1][j-2]+...+f[i-1][j-a[i]]

int main() {

  cin >> n >> m;
  for (int i = 0; i <= n; i++) f[i][0] = 1;
  for (int i = 1; i <= n; i++) cin >> a[i];
  for (int i = 1; i <= n; i++) {
    for (int j = 1; j <= m; j++) {
      for (int k = 0; k <= a[i] && k <= j; k++) {
        f[i][j] += f[i - 1][j - k];
        f[i][j] %= 1000007;
      }
    }
  }
  cout << f[n][m];
  return 0;
}
