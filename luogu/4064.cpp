#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
namespace IO {
  char nc() {
    static char bf[100000], *p1 = bf, *p2 = bf;
    return p1 == p2 && (p2 = (p1 = bf) + fread(bf, 1, 100000, stdin), p1 == p2)
               ? EOF
               : *p1++;
  }
  int rd() {
    int res = 0;
    char c = getchar();
    while (!isdigit(c)) c = getchar();
    while (isdigit(c)) res = res * 10 + c - '0', c = getchar();
    return res;
  }
} // namespace IO
/******************heading******************/
const int N = 2e5 + 5;
int T, n, m, k, v;
int a[N], d[N], dt[N];

struct data {
  int l, r;
  void read() { scanf("%d%d", &l, &r); }
  bool operator<(data d) const { return r < d.r; }
} s[N];
bool cmp(data x, data y) { return x.l < y.l; }
priority_queue<data> q;

bool check(int x) {
  // printf("\033[31mcheck(%d)\033[0m\n",x);
  while (q.size()) q.pop();
  FOR(i, 1, n) dt[i] = d[i];
  int pos = 1, sum = 0, tot = 0;
  FOR(i, 1, n) {
    while (s[pos].l == i) q.push(s[pos++]);
    sum += dt[i];
    // printf("i=%d,sum=%d\n",i,sum);
    while (sum < x && q.size()) {
      data cur = q.top();
      q.pop();
      if (cur.r < i) return 0;
      sum += v, dt[cur.r + 1] -= v, tot++;
    }
    // printf("+++i=%d,sum=%d\n",i,sum);
    if (sum < x) return 0;
  }
  // printf("tot=%d,k=%d\n",tot,k);
  return tot <= k;
}
void go() {
  scanf("%d%d%d%d", &n, &m, &k, &v);
  FOR(i, 1, n) scanf("%d", &a[i]), d[i] = a[i] - a[i - 1];
  FOR(i, 1, m) s[i].read();
  sort(s + 1, s + m + 1, cmp);
  int l = 1, r = 1e8, mid;
  while (l < r) {
    mid = (l + r + 1) >> 1;
    if (check(mid))
      l = mid;
    else
      r = mid - 1;
    // printf("l=%d,r=%d\n",l,r);
  }
  printf("%d\n", l);
}
int main() {
  scanf("%d", &T);
  while (T--) go();
  return 0;
}
