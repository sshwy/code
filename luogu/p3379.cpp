#include <cstdio>
#include <iostream>
#include <vector>

using namespace std;

int n, m, s, a, b, tdep;
int dep[500001], log[500001];
int f[500001][50];
bool build_vis[500001];

vector<int> next[500001];

inline void build_f0(int now_node, int ndep) {
  dep[now_node] = ndep;
  build_vis[now_node] = true;
  for (int i = 0; i < next[now_node].size(); i++) {
    if (build_vis[next[now_node][i]] == false) {
      f[next[now_node][i]][0] = now_node;
      build_f0(next[now_node][i], ndep + 1);
    }
  }
  if (ndep > tdep) tdep = ndep;
}

int main() {
  scanf("%d%d%d", &n, &m, &s);
  //	cin>>n>>m>>s;
  for (int i = 1; i < n; i++) {
    scanf("%d%d", &a, &b);
    //		cin>>a>>b;
    next[a].push_back(b);
    next[b].push_back(a);
  }

  build_f0(s, 0);
  for (int j = 1; j <= tdep; j++) {
    for (int i = 1; i <= n; i++) { f[i][j] = f[f[i][j - 1]][j - 1]; }
  }

  for (int i = 1; i <= n; i++) { log[i] = log[i - 1] + (1 << log[i - 1] + 1 == i); }

  for (int i = 1; i <= m; i++) {
    scanf("%d%d", &a, &b);
    //		cin>>a>>b;
    if (dep[a] < dep[b]) a ^= b ^= a ^= b;
    while (dep[a] > dep[b]) { a = f[a][log[dep[a] - dep[b]]]; }
    if (a == b) {
      printf("%d\n", a);
      //			cout<<a<<endl;
    } else {
      for (int step = log[dep[a]]; step >= 0; step--) {
        if (f[a][step] != f[b][step]) a = f[a][step], b = f[b][step];
      }
      printf("%d\n", f[a][0]);
      //			cout<<f[a][0]<<endl;
    }
  }
  return 0;
}
