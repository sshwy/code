#include <iostream>

using namespace std;

int n, w[101], mx;
int f[101][101]; // f[i][j]:从第i个珠子开始，聚合j颗珠子后的最大能量[i~(i+j)%n-1]
/*
f[i][j]=max[1<=k<=j]{f[i][k]+f[(i+k)%n][j-k]+w[i]*w[(i+k)%n]*w[(i+j)%n]}
    =0[j==0||j==1]
*/
int main() {
  cin >> n;
  for (int i = 0; i < n; i++) cin >> w[i];
  for (int j = 2; j <= n; j++) {
    for (int i = 0; i < n; i++) {
      for (int k = 1; k < j; k++) {
        f[i][j] = max(f[i][j],
            f[i][k] + f[(i + k) % n][j - k] + w[i] * w[(i + k) % n] * w[(i + j) % n]);
      }
      if (mx < f[i][j]) mx = f[i][j];
    }
  }
  cout << mx;
  return 0;
}
