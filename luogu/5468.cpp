// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

const int N = 1e5 + 5;

long long A, B, C;
int n, m;

struct Route {
  int x, y, p, q;
};
vector<Route> routes;

struct Event {
  int typ, route_id;
  int getTime() {
    if (typ == 0)
      return routes[route_id].p;
    else
      return routes[route_id].q;
  }
  int getNode() {
    if (typ == 0)
      return routes[route_id].x;
    else
      return routes[route_id].y;
  }
};
vector<Event> events;
bool byTime(Event a, Event b) {
  if (a.getTime() == b.getTime()) return a.typ > b.typ;
  return a.getTime() < b.getTime();
}

struct Point {
  long long x, y;
  Point(long long _x, long long _y) { x = _x, y = _y; }
  Point(Point p1, Point p2) {
    x = p2.x - p1.x;
    y = p2.y - p1.y;
  }
  long long eval(long long k) { return y - k * x; }
};
typedef Point Vec;
vector<Point> v[N];
int vhead[N];

__int128 det(Point p1, Point p2) {
  return __int128(p1.x) * p2.y - __int128(p2.x) * p1.y;
}

Point parseStatus(long long tim, long long cost) {
  return Point(tim, tim * tim * A - tim * B + cost);
}

void go() {
  long long ans = LLONG_MAX;

  v[1].pb(parseStatus(0, 0));
  vector<long long> route_cost(routes.size(), LLONG_MAX);
  for (auto e : events) {
    if (e.typ == 0) {
      int u = e.getNode(), &ql = vhead[u];
      long long tu = e.getTime();
      vector<Point> &q = v[u];
      while (ql + 1 < q.size() && det(Vec(q[ql], q[ql + 1]), Vec(1, 2 * A * tu)) >= 0)
        ++ql;
      if (!q.empty()) {
        long long cost = q[ql].eval(2 * A * tu) + tu * tu * A + tu * B + C;
        route_cost[e.route_id] = cost;
      }
    } else {
      int u = e.getNode(), &ql = vhead[u];
      vector<Point> &q = v[u];
      if (route_cost[e.route_id] == LLONG_MAX) continue;
      long long tu = e.getTime(), cu = route_cost[e.route_id];
      Point pu = parseStatus(tu, cu);
#define check(X, Y, Z) (Y.x == Z.x ? (Y.y < Z.y) : (det(Vec(X, Y), Vec(Y, Z)) > 0))
      while (
          ql + 1 < q.size() && check(q[q.size() - 2], q[q.size() - 1], pu) == false)
        q.pop_back();
      q.pb(pu);
      if (u == n) { ans = min(ans, tu + cu); }
    }
  }
  printf("%lld\n", ans);
}

int main() {
  scanf("%d%d", &n, &m);
  scanf("%lld%lld%lld", &A, &B, &C);
  FOR(i, 1, m) {
    int x, y, p, q;
    scanf("%d%d%d%d", &x, &y, &p, &q);
    routes.pb({x, y, p, q});
    events.pb({0, routes.size() - 1});
    events.pb({1, routes.size() - 1});
  }
  sort(events.begin(), events.end(), byTime);
  go();
  return 0;
}
