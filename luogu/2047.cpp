#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
namespace IO {
  char nc() {
    static char bf[100000], *p1 = bf, *p2 = bf;
    return p1 == p2 && (p2 = (p1 = bf) + fread(bf, 1, 100000, stdin), p1 == p2)
               ? EOF
               : *p1++;
  }
  int rd() {
    int res = 0;
    char c = getchar();
    while (!isdigit(c)) c = getchar();
    while (isdigit(c)) res = res * 10 + c - '0', c = getchar();
    return res;
  }
} // namespace IO
/******************heading******************/
const int N = 102;

int n, m;
int f[N][N];

int main() {
  scanf("%d%d", &n, &m);
  memset(f, 0x3f, sizeof(f));
  FOR(i, 1, m) {
    int a, b, c;
    scanf("%d%d%d", &a, &b, &c);
    f[a][b] = f[b][a] = c;
  }
  FOR(k, 1, n)
  FOR(i, 1, n) FOR(j, 1, n) f[i][j] = min(f[i][j], f[i][k] + f[k][j]);
  FOR(i, 1, n) FOR(j, 1, n) FOR(k, 1, n) return 0;
}
