#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <climits>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <unordered_map>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
typedef long long LL;
LL I2, I6;
const int SZ = 5e6 + 5;
int LIM = 5e6;

bool bp[SZ];
int pn[SZ], lp;
LL mu[SZ]; // prefix sum
void sieve() {
  bp[0] = bp[1] = 1;
  mu[1] = 1;
  FOR(i, 2, LIM) {
    if (!bp[i]) pn[++lp] = i, mu[i] = -1;
    FOR(j, 1, lp) {
      int pj = pn[j];
      if (i * pj > LIM) break;
      bp[i * pj] = 1;
      if (i % pj == 0) {
        mu[i * pj] = 0;
        break;
      } else {
        mu[i * pj] = mu[i] * mu[pj];
      }
    }
  }
  FOR(i, 2, LIM) mu[i] = mu[i] + mu[i - 1];
}
LL F_ans, n, n2;
void dfs(int cur, LL prd, int mu) {
  if (prd > n2) return;
  // printf("prd=%lld,mu=%d\n",prd,mu);
  F_ans += mu * (n / (prd * prd));
  FOR(i, cur + 1, lp) {
    if (prd * pn[i] > n2) break;
    dfs(i, prd * pn[i], -mu);
  }
}
LL F(LL _n) {
  n = _n;
  n2 = sqrt(n);
  while (n2 * n2 <= n) ++n2;
  while (n2 * n2 > n) --n2;
  F_ans = 0;
  // F_ans=n;
  dfs(0, 1, 1);
  return F_ans;
}

unordered_map<LL, LL> m_smu;
map<pair<LL, LL>, LL> m_g;
LL Smu(LL x) {
  if (x <= LIM) return mu[x];
  if (m_smu.count(x)) return m_smu[x];
  LL l = 2, r;
  LL res = 1;
  while (l <= x) {
    r = x / (x / l);
    res = res - Smu(x / l) * (r - l + 1);
    l = r + 1;
  }
  return m_smu[x] = res;
}

LL g(LL n, LL k) {
  if (k == 1) return F(n);
  if (n == 0) return 0;
  if (m_g.count({n, k})) return m_g[{n, k}];
  LL res = 0;
  for (LL d = 1; d * d <= k; d++) {
    if (k % d) continue;
    int t = Smu(d) - Smu(d - 1);
    res += t * g(n / d, d);
    if (d * d != k) {
      LL x = k / d;
      t = Smu(x) - Smu(x - 1);
      res += t * g(n / x, x);
    }
  }
  return res;
}

void go() {
  LL n, m;
  scanf("%lld%lld", &n, &m);
  LL ans = (Smu(m) - Smu(m - 1)) * g(n / m, m);
  printf("%lld\n", ans);
}
int main() {
  sieve();
  int t;
  scanf("%d", &t);
  FOR(i, 1, t) go();
  return 0;
}
