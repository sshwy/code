#include <cctype>
#include <cstdio>
#include <cstring>
#include <queue>
#define x first
#define y second
#define mk make_pair
#define FOR(a, b, c) for (int a = b; a <= c; a++)
#define pii pair<int, int>
using namespace std;
const int N = 100005, M = 200005, K = 52;
// 快读
char nc() {
  static char buf[100000], *p1 = buf, *p2 = buf;
  return p1 == p2 && (p2 = (p1 = buf) + fread(buf, 1, 100000, stdin), p1 == p2)
             ? EOF
             : *p1++;
}
int rd() {
  int res = 0;
  char c = nc();
  while (!isdigit(c)) c = nc();
  while (isdigit(c)) res = res * 10 + c - '0', c = nc();
  return res;
}
// 快出
void pd(int x) {
  char c[20];
  int cnt = 0;
  while (x) c[++cnt] = x % 10 + '0', x /= 10;
  for (int i = cnt; i > 0; i--) putchar(c[i]);
}
int n, m, k, p, t;
int d[N], f[N][K], flag; // f[i,j] 表示到结点 i，走 dist[i]+j 的方案数
bool vis[N], w[N][K];
// 前向星存图
struct qxx {
  int nex, t, v;
};
qxx e1[M], e2[M];
int h1[N], c1, h2[N], c2;
void add_path1(int f, int t, int v) { e1[++c1] = (qxx){h1[f], t, v}, h1[f] = c1; }
void add_path2(int f, int t, int v) { e2[++c2] = (qxx){h2[f], t, v}, h2[f] = c2; }
// 初始化
void init() {
  c1 = c2 = flag = 0;
  FOR(i, 0, n) h1[i] = 0;
  FOR(i, 0, n) h2[i] = 0;
  FOR(i, 0, n) d[i] = 0x3f3f3f3f;
  FOR(i, 0, n) vis[i] = 0;
  FOR(i, 0, n) FOR(j, 0, k) f[i][j] = 0;
  FOR(i, 0, n) FOR(j, 0, k) w[i][j] = 0;
}
// 反图跑 dijkstra
priority_queue<pii, vector<pii>, greater<pii>> q;
void dijkstra() {
  d[n] = 0;
  q.push(mk(0, n));
  while (!q.empty()) {
    pii u = q.top();
    q.pop();
    if (vis[u.y]) continue;
    vis[u.y] = 1;
    for (int i = h2[u.y]; i; i = e2[i].nex) {
      const int &v = e2[i].t, &w = e2[i].v;
      if (d[v] > d[u.y] + w) d[v] = d[u.y] + w, q.push(mk(d[v], v));
    }
  }
}
// DFS DP
int fdfs(int u, int c) {
  if (w[u][c]) return flag = 1, 0; // 出现 0 环
  if (f[u][c]) return f[u][c];     // 已被计算
  int s = 0;
  w[u][c] = 1; // 标记
  for (int i = h1[u]; i; i = e1[i].nex) {
    const int &v = e1[i].t, &val = e1[i].v;
    int t = d[u] + c - val - d[v]; // 对于 v 需要多走的单位为 t
    if (t < 0 || t > k) continue;
    s = (s + fdfs(v, t)) % p;
    if (flag) return 0; // 0 环
  }
  if (u == n && c == 0) s = 1;
  return w[u][c] = 0, f[u][c] = s; // 撤销标记并返回 f[u][c]
}
int main() {
  t = rd();
  while (t--) {
    n = rd(), m = rd(), k = rd(), p = rd();
    init();
    FOR(i, 1, m) {
      int ai = rd(), bi = rd(), ci = rd();
      add_path1(ai, bi, ci);
      add_path2(bi, ai, ci);
    }
    dijkstra();
    int ans = 0;
    FOR(i, 0, k) {
      ans = (ans + fdfs(1, i)) % p;
      if (flag) break;
    }
    if (flag)
      puts("-1");
    else
      pd(ans), putchar('\n');
  }
  return 0;
}
