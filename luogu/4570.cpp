#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
typedef pair<int, long long> pil;
const int N = 1e3 + 5;

int n;
pil a[N];

long long b[70], lb;
bool insert(long long x) {
  ROF(i, 63, 0) {
    if (x >> i & 1) {
      if (b[i])
        x ^= b[i];
      else {
        b[i] = x;
        return 1;
      }
    }
  }
  return 0;
}
int ans;
int main() {
  scanf("%d", &n);
  FOR(i, 1, n) scanf("%lld%d", &a[i].se, &a[i].fi), a[i].fi = -a[i].fi;
  sort(a + 1, a + n + 1);
  FOR(i, 1, n) {
    long long x = a[i].se;
    if (insert(x)) ans -= a[i].fi;
  }
  printf("%d\n", ans);
  return 0;
}
