#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
namespace IO {
  char nc() {
    static char bf[100000], *p1 = bf, *p2 = bf;
    return p1 == p2 && (p2 = (p1 = bf) + fread(bf, 1, 100000, stdin), p1 == p2)
               ? EOF
               : *p1++;
  }
  int rd() {
    int res = 0;
    char c = getchar();
    while (!isdigit(c)) c = getchar();
    while (isdigit(c)) res = res * 10 + c - '0', c = getchar();
    return res;
  }
} // namespace IO
/******************heading******************/
const int SZ = 15e5 + 5;

int tot, ch[SZ][2], sz[SZ], rv[SZ], f[SZ];
int val[SZ];  //这个点的子节点的1的个数。外层结点val为0
int pos1[SZ]; //从右往左第一个不是1的结点，没有的话记为0
int pos2[SZ]; //从右往左第一个不是2的结点，没有的话记为0

int new_node() {
  ++tot, ch[tot][0] = ch[tot][1] = f[tot] = rv[tot] = 0, sz[tot] = 1;
  val[tot] = 0;
  pos1[tot] = pos2[tot] = tot; //一开始权值都是0
  return tot;
}
void pushup(int u) {
  sz[u] = sz[ch[u][0]] + sz[ch[u][1]] + 1;

  if (pos1[ch[u][1]])
    pos1[u] = pos1[ch[u][1]];
  else if (pos1[ch[u][0]])
    pos1[u] = pos1[ch[u][0]];
  else
    pos1[u] = 0;

  if (pos2[ch[u][1]])
    pos2[u] = pos2[ch[u][1]];
  else if (pos2[ch[u][0]])
    pos2[u] = pos2[ch[u][0]];
  else
    pos2[u] = 0;
}
void pushdown(int u) {
  if (rv[u])
    swap(ch[u][0], ch[u][1]), rv[ch[u][0]] ^= 1, rv[ch[u][1]] ^= 1, rv[u] = 0;
}
bool get(int u) { return ch[f[u]][1] == u; }
bool isroot(int u) { return ch[f[u]][0] != u && ch[f[u]][1] != u; }
void rotate(int u) {
  int y = f[u], z = f[y], k;
  pushdown(y), pushdown(u), k = get(u);
  if (!isroot(y)) ch[z][get(y)] = u;
  ch[y][k] = ch[u][!k], f[ch[u][!k]] = y;
  ch[u][!k] = y, f[y] = u, f[u] = z;
  pushup(y), pushup(u);
}
void splay(int u) {
  pushdown(u);
  for (int p; p = f[u], !isroot(u); rotate(u))
    if (!isroot(p)) rotate(get(p) == get(u) ? p : u);
}
void access(int u) {
  for (int p = 0; u; p = u, u = f[u]) splay(u), ch[u][1] = p, pushup(u);
}
void makeroot(int u) { access(u), splay(u), rv[u] ^= 1; }
void link(int u, int v) { makeroot(u), f[u] = v; }
void cut(int u, int v) {
  makeroot(u), access(v), splay(v), ch[v][0] = f[u] = 0, pushup(v);
}
void turn_on(int x) { access(x); }

int n, Q;
bool stat[SZ];
int main() {
  scanf("%d", &n);
  FOR(i, 1, n * 3 + 1) new_node(); // index 1..n
  FOR(i, 1, n) {
    int x1, x2, x3;
    scanf("%d%d%d", &x1, &x2, &x3);
    link(i, x1), link(i, x2), link(i, x3);
  }
  makeroot(1);
  FOR(i, n + 1, n * 3 + 1) {
    int x;
    scanf("%d", &x);
    if (x) turn_on(i), stat[i] = 1;
  }
  scanf("%d", &Q);
  FOR(i, 1, Q) {
    int x;
    scanf("%d", &x);
    if (stat[x])
      turn_off(x), stat[x] = 0;
    else
      turn_on(x), stat[x] = 1;
  }
  return 0;
}
