#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
#define int lld
const int N = 1 << 17, P = 998244353;
int n, in;
int a[N], b[N], c[N];

int pw(int a, int m, int p) {
  int res = 1;
  while (m) m & 1 ? res = res * a % p : 0, a = a * a % p, m >>= 1;
  return res;
}
void fwt_and(int *f, int tag) {
  for (int j = 1; j < n; j <<= 1)
    for (int i = 0; i < n; i += j << 1)
      for (int k = i; k < i + j; k++) f[k] = (f[k] + f[k + j] * tag) % P;
  FOR(i, 0, n - 1) f[i] += f[i] < 0 ? P : 0;
}
void fwt_or(int *f, int tag) {
  for (int j = 1; j < n; j <<= 1)
    for (int i = 0; i < n; i += j << 1)
      for (int k = i; k < i + j; k++) f[k + j] = (f[k + j] + f[k] * tag) % P;
  FOR(i, 0, n - 1) f[i] += f[i] < 0 ? P : 0;
}
void fwt_xor(int *f, int tag) {
  for (int j = 1; j < n; j <<= 1)
    for (int i = 0; i < n; i += j << 1)
      for (int k = i; k < i + j; k++) {
        int x = f[k], y = f[k + j];
        f[k] = (x + y) % P, f[k + j] = (x - y) % P;
      }
  if (tag == -1) FOR(i, 0, n - 1) f[i] = f[i] * in % P;
  FOR(i, 0, n - 1) f[i] += f[i] < 0 ? P : 0;
}
signed main() {
  scanf("%lld", &n);
  n = 1 << n;
  in = pw(n, P - 2, P);
  FOR(i, 0, n - 1) scanf("%lld", &a[i]);
  FOR(i, 0, n - 1) scanf("%lld", &b[i]);

  fwt_or(a, 1), fwt_or(b, 1);
  FOR(i, 0, n - 1) c[i] = a[i] * b[i] % P;
  fwt_or(c, -1);
  FOR(i, 0, n - 1) printf("%lld%c", c[i], " \n"[i == n - 1]);
  fwt_or(a, -1), fwt_or(b, -1);

  fwt_and(a, 1), fwt_and(b, 1);
  FOR(i, 0, n - 1) c[i] = a[i] * b[i] % P;
  fwt_and(c, -1);
  FOR(i, 0, n - 1) printf("%lld%c", c[i], " \n"[i == n - 1]);
  fwt_and(a, -1), fwt_and(b, -1);

  fwt_xor(a, 1), fwt_xor(b, 1);
  FOR(i, 0, n - 1) c[i] = a[i] * b[i] % P;
  fwt_xor(c, -1);
  FOR(i, 0, n - 1) printf("%lld%c", c[i], " \n"[i == n - 1]);
  fwt_xor(a, -1), fwt_xor(b, -1);
  return 0;
}
