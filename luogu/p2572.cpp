#include <bits/stdc++.h>
using namespace std;
const int N = 100005, LST = 1 << 18;
int n, m;
int a[N];
int t[LST];           //统计有多少个连续的1
int s[LST];           //维护有多少个1
int cl[LST], cr[LST]; //统计两端有多少连续的1
int tg[LST], rvs[LST];
// 0表示无操作，1表示变0，2表示变1，3表示反转
// tag 0&1
// tag reverse
void push_down(int l, int r, int rt) {
  if (!tg[rt]) return;
  int mid = (l + r) >> 1;
  if (tg[rt] != 3) { // upd
    s[rt << 1] = (tg[rt] - 1) * (mid - l + 1),
            s[rt << 1 | 1] = (tg[rt] - 1) * (r - mid),
            t[rt << 1] = t[rt << 1 | 1] = 1,
            cl[rt << 1] = cl[rt << 1 | 1] = tg[rt] - 1,
            cr[rt << 1] = cr[rt << 1 | 1] = tg[rt] - 1,
            tg[rt << 1] = tg[rt << 1 | 1] = tg[rt];
  } else { // reverse
    s[rt << 1] = mid - l + 1 - s[rt << 1], tg[rt << 1] = 3 - tg[rt << 1],
            cl[rt << 1] = !cl[rt << 1], cr[rt << 1] = !cr[rt << 1];

    s[rt << 1 | 1] = r - mid - s[rt << 1 | 1];
    tg[rt << 1 | 1] = 3 - tg[rt << 1 | 1];
    cl[rt << 1 | 1] = !cl[rt << 1 | 1], cr[rt << 1 | 1] = !cr[rt << 1 | 1];
  }
}
void push_up(int rt) {
  s[rt] = s[rt << 1] + s[rt << 1 | 1], t[rt] = t[rt << 1] + t[rt << 1 | 1];
  cl[rt] = cl[rt << 1], cr[rt] = cr[rt << 1 | 1];
  if (cr[rt << 1] == cl[rt << 1 | 1]) t[rt]--;
}
int lst_build(int l = 1, int r = n, int rt = 1) {
  if (l == r) return s[rt] = cl[rt] = cr[rt] = a[l], t[rt] = 1;
  int mid = (l + r) >> 1;
  lst_build(l, mid, rt << 1), lst_build(mid + 1, r, rt << 1 | 1);
  push_up(rt);
}
int lst_upd(int L, int R, int v, int l = 1, int r = n, int rt = 1) {
  if (L <= l && r <= R)
    return s[rt] = (r - l + 1) * v, t[rt] = 1, tg[rt] = v + 1, cl[rt] = cr[rt] = v;
  int mid = (l + r) >> 1;
  push_down(l, r, rt);
  if (L <= mid) lst_upd(L, R, v, l, mid, rt << 1);
  if (mid < R) lst_upd(L, R, v, mid + 1, r, rt << 1 | 1);
  push_up(rt);
}
int lst_reverse(int L, int R, int l = 1, int r = n, int rt = 1) {
  if (L <= l && r <= R)
    return s[rt] = r - l + 1 - s[rt], tg[rt] = 3 - tg[rt], cl[rt] = !cl[rt],
           cr[rt] = !cr[rt];
  int mid = (l + r) >> 1;
  push_down(l, r, rt);
  if (L <= mid) lst_reverse(L, R, l, mid, rt << 1);
  if (mid < R) lst_reverse(L, R, mid + 1, r, rt << 1 | 1);
  push_up(rt);
}
int lst_sum(int L, int R, int l = 1, int r = n, int rt = 1) {
  if (L <= l && r <= R) return s[rt];
  int mid = (l + r) >> 1, sum = 0;
  push_down(l, r, rt);
  if (L <= mid) sum += lst_sum(L, R, l, mid, rt << 1);
  if (mid < R) sum += lst_sum(L, R, mid + 1, r, rt << 1 | 1);
  return sum;
}
int lst_color(int p, int l = 1, int r = n, int rt = 1) {
  if (l == r && p == l) return cl[rt];
  int mid = (l + r) >> 1;
  push_down(l, r, rt);
  if (p <= mid)
    return lst_color(p, l, mid, rt << 1);
  else
    return lst_color(p, mid + 1, r, rt << 1 | 1);
}
int lst_count(int L, int R, int l = 1, int r = n, int rt = 1) {
  if (L <= l && r <= R) return t[rt];
  int mid = (l + r) >> 1, t1 = 0, t2 = 0;
  if (L <= mid) t1 = lst_count(L, R, l, mid, rt << 1);
  if (mid < R) t2 = lst_count(L, R, mid + 1, r, rt << 1 | 1);
  return t1 + t2 - (t1 && t2 ? cr[rt << 1] == cl[rt << 1 | 1] : 0);
}

int main() {
  scanf("%d%d", &n, &m);
  for (int i = 1; i <= n; i++) scanf("%d", &a[i]);
  lst_build();
  for (int i = 1, op, ai, bi; i <= m; i++) {
    scanf("%d%d%d", &op, &ai, &bi);
    if (op == 0)
      lst_upd(ai + 1, bi + 1, 0);
    else if (op == 1)
      lst_upd(ai + 1, bi + 1, 1);
    else if (op == 2)
      lst_reverse(ai + 1, bi + 1);
    else if (op == 3)
      printf("%d\n", lst_sum(ai + 1, bi + 1));
    else {
      int ans = lst_count(ai + 1, bi + 1);
      int ca = lst_color(ai + 1);
      int cb = lst_color(bi + 1);
      printf("%d\n", (ans + ca + cb - 1) / 2);
    }
  }
  return 0;
}

//更新t01的时候：
//首先正常更新
//然后将rvs标记去除
//更新rvs的时候：t不变
//如果t01不为零，就只需将t01反转，不用打rvs标记
//否则正常reverse
//所以t01和rvs同时间只会存在一个
//所以合并成tg[]

/*if(tg[rt<<1]==3)
  tg[rt<<1]=0,
  cl[rt<<1]=!cl[rt<<1],
  cr[rt<<1]=!cr[rt<<1];
else if(tg[rt<<1])
  tg[rt<<1]=!(tg[rt<<1]-1)+1,
  cl[rt<<1]=!cl[rt<<1],
  cr[rt<<1]=!cr[rt<<1];
else
  tg[rt<<1]=3,
  cl[rt<<1]=!cl[rt<<1],
  cr[rt<<1]=!cr[rt<<1];*/