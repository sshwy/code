#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 17;

int n, m;
vector<int> T[N], G[N];

long long f[N][N];

void dfs(int u, int p, int s) {
  for (int x : T[u])
    if (x != p) dfs(x, u, s);
  FOR(v, 0, n - 1) {
    f[u][v] = 0;
    if (s >> v & 1) {
      f[u][v] = 1;
      for (int x : T[u])
        if (x != p) {
          long long tot = 0;
          for (int y : G[v])
            if (s >> y & 1) { tot += f[x][y]; }
          f[u][v] *= tot;
        }
    }
  }
}
long long calc(int s) {
  memset(f, 0, sizeof(f));
  dfs(0, -1, s);
  long long res = 0;
  FOR(i, 0, n - 1) if (s >> i & 1) res += f[0][i];
  return res;
}
int sig(int x) { return 1 - 2 * (x & 1); }

int main() {
  scanf("%d%d", &n, &m);
  FOR(i, 1, m) {
    int u, v;
    scanf("%d%d", &u, &v);
    --u, --v;
    G[u].pb(v), G[v].pb(u);
  }
  FOR(i, 1, n - 1) {
    int u, v;
    scanf("%d%d", &u, &v);
    --u, --v;
    T[u].pb(v), T[v].pb(u);
  }
  int lim = 1 << n;
  long long ans = 0;
  FOR(i, 1, lim - 1) ans += calc(i) * sig(n - __builtin_popcount(i));
  printf("%lld\n", ans);
  return 0;
}
