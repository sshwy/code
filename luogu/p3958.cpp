#include <iostream>

using namespace std;

long long t, n, h, r;
long long f[1011];
struct p {
  long long x, y, z;
} hole[1011];

bool is_link(long long c1, long long c2) {
  if (4 * r * r >= (hole[c1].x - hole[c2].x) * (hole[c1].x - hole[c2].x) +
                       (hole[c1].y - hole[c2].y) * (hole[c1].y - hole[c2].y) +
                       (hole[c1].z - hole[c2].z) * (hole[c1].z - hole[c2].z))
    return true;
  else
    return false;
}

long long gf(long long k) {
  if (f[k] == k) return k;
  f[k] = gf(f[k]);
  return f[k];
}

void un(long long a, long long b) { f[gf(a)] = gf(b); }

int main() {
  cin >> t;
  for (long long i = 1; i <= t; i++) {
    cin >> n >> h >> r;
    for (long long j = 0; j <= n + 1; j++) f[j] = j;
    for (long long j = 1; j <= n; j++) {
      cin >> hole[j].x >> hole[j].y >> hole[j].z;
      for (long long g = 1; g < j; g++) {
        if (is_link(g, j)) un(g, j);
      }
      if (hole[j].z <= r) un(0, j);
      if (hole[j].z >= h - r) un(j, n + 1);
    }
    if (gf(0) == gf(n + 1))
      cout << "Yes\n";
    else
      cout << "No\n";
  }
  return 0;
}
