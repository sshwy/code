#include <algorithm>
#include <cstdio>
using namespace std;
const int N = 2003, V = 302, M = 302;
int n, m, v, e;
int c[N], d[N];
int dis[V][V];
double p[N];
double f[N][N][2];
// f[i,j,p]到第i个时段，更换了j次，目前在C(0)还是D(1)教室
// p即为到达该点的概率。
int main() {
  // freopen("p1850.in","r",stdin);
  scanf("%d%d%d%d", &n, &m, &v, &e);
  for (int i = 1; i <= n; i++) scanf("%d", &c[i]);
  for (int i = 1; i <= n; i++) scanf("%d", &d[i]);
  for (int i = 1; i <= n; i++) scanf("%lf", &p[i]);

  for (int i = 1; i <= v; i++)
    for (int j = 1; j < i; j++) dis[i][j] = dis[j][i] = 800000000;

  for (int i = 1, x, y, z; i <= e; i++)
    scanf("%d%d%d", &x, &y, &z), dis[x][y] = dis[y][x] = min(dis[x][y], z);

  for (int pi = 1; pi <= v; pi++)
    for (int i = 1; i <= v; i++)
      for (int j = 1; j <= v; j++)
        if (dis[i][j] > dis[i][pi] + dis[pi][j]) dis[i][j] = dis[i][pi] + dis[pi][j];

  for (int i = 1; i <= n; i++)
    for (int j = 0; j <= m; j++) f[i][j][0] = f[i][j][1] = 800000000;

  f[1][0][0] = f[1][1][1] = 0;
  for (int i = 2; i <= n; i++) {
    for (int j = 0; j <= m && j <= i; j++) {
      f[i][j][0] = min(f[i - 1][j][0] + dis[c[i]][c[i - 1]],
          f[i - 1][j][1] + p[i - 1] * dis[c[i]][d[i - 1]] +
              (1 - p[i - 1]) * dis[c[i]][c[i - 1]]);
      if (j > 0)
        f[i][j][1] = min(f[i - 1][j - 1][0] + (1 - p[i]) * dis[c[i]][c[i - 1]] +
                             p[i] * dis[d[i]][c[i - 1]],
            f[i - 1][j - 1][1] + p[i - 1] * p[i] * dis[d[i]][d[i - 1]] +
                (1 - p[i - 1]) * p[i] * dis[d[i]][c[i - 1]] +
                p[i - 1] * (1 - p[i]) * dis[c[i]][d[i - 1]] +
                (1 - p[i - 1]) * (1 - p[i]) * dis[c[i]][c[i - 1]]);
    }
  }

  double ans = 800000000;
  for (int i = 0; i <= m; i++) {
    ans = min(ans, f[n][i][0]);
    ans = min(ans, f[n][i][1]);
  }
  printf("%.2lf", ans);
  return 0;
}
//考虑第i个教室没有申请，则到达的概率为1f[i][j][0]
//第i-1个教室没有申请f[i-1][j][0]
//+1*dis[c[i]][c[i-1]]
//第i-1个教室申请f[i-1][j][1]
//成功+p[i-1]*dis[c[i]][d[i-1]]
//失败+(1-p[i-1])*dis[c[i]][c[i-1]]
//考虑第i个教室申请f[i][j][1]
//第i个教室申请失败
//第i-1个教室没有申请f[i-1][j-1][0]
//+(1-p[i])*dis[c[i]][c[i-1]]
//第i-1个教室申请f[i-1][j-1][1]
//失败+(1-p[i-1])*(1-p[i])*dis[c[i]][c[i-1]]
//成功+p[i-1]*(1-p[i])*dis[c[i]][d[i-1]]
//第i个教室申请成功
//第i-1个教室没有申请f[i-1][j-1][0]
//+p[i]*dis[d[i]][c[i-1]]
//第i-1个教室申请f[i-1][j-1][1]
//失败+(1-p[i-1])*p[i]*dis[d[i]][c[i-1]]
//成功+p[i-1]*p[i]*dis[d[i]][d[i-1]]