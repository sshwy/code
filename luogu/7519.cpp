// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

typedef long long LL;
const int N = 13, M = 505;

int n, m, a[N];
LL f[1 << N][N][M];

void go() {
  FOR(i, 0, n - 1) {
    int maxX = 0;
    FOR(j, 0, n - 1) if (j != i) {
      int x = a[j] - a[i] + (j < i);
      maxX = max(maxX, x);
    }
    if (maxX * n <= m) f[1 << i][i][m - maxX * n] = 1;
  }
  int nn = 1 << n;
  FOR(i, 1, nn - 1) {
    int cnt = n - __builtin_popcount(i);
    FOR(j, 0, n - 1) if (i >> j & 1) {
      FOR(k, 0, m) if (f[i][j][k]) {
        FOR(p, 0, n - 1) if (!(i >> p & 1)) {
          int x = max(0, a[j] - a[p] + (j < p));
          if (x * cnt <= k) {
            if (cnt == 1) {
              f[i | (1 << p)][p][0] += f[i][j][k];
            } else {
              f[i | (1 << p)][p][k - x * cnt] += f[i][j][k];
            }
          }
        }
      }
    }
  }
  long long ans = 0;
  FOR(i, 0, n - 1) ans += f[nn - 1][i][0];
  printf("%lld\n", ans);
}

int main() {
  scanf("%d%d", &n, &m);
  FOR(i, 0, n - 1) scanf("%d", a + i);

  int minA = *min_element(a, a + n);
  FOR(i, 0, n - 1) a[i] -= minA;

  FOR(i, 0, n - 1) if (a[i] > m) {
    puts("0");
    return 0;
  }

  go();
  return 0;
}
