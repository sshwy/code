#include <iostream>

using namespace std;

int n, mx;
int f[2001];
char str[2001][80];

bool pre(char *p, char *s) {
  for (int i = 0; p[i]; i++) {
    if (p[i] != s[i]) return false;
    if (s[i] == 0) return false;
  }
  return true;
}

int main() {
  cin >> n;
  for (int i = 1; i <= n; i++) cin >> str[i];
  for (int i = 1; i <= n; i++) {
    f[i] = 1;
    for (int j = 1; j < i; j++) {
      if (pre(str[j], str[i])) { f[i] = max(f[i], f[j] + 1); }
    }
    mx = max(mx, f[i]);
  }
  cout << mx;
  return 0;
}
