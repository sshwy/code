#include <algorithm>
#include <iostream>

using namespace std;

int f[3001]; // father
int n, m, a, b, c, d, ww, lps;

struct path {
  int u, v, w;
} ps[11111];

bool cmp(path p1, path p2) { return p1.w < p2.w; }
int gf(int k) // getfather
{
  if (f[k] == k) return k;
  f[k] = gf(f[k]);
  return f[k];
}

void un(int a, int b) // union
{
  int fa = gf(a);
  int fb = gf(b);
  f[fa] = fb;
}
int main() {
  cin >> n >> m;
  for (int i = 1; i <= n; i++) f[i] = i;
  for (int i = 1; i <= m; i++) {
    cin >> a >> b >> c >> d;
    if (a == 1) {
      un(b, c);
      ww += d;
    } else {
      lps++;
      ps[lps].u = b;
      ps[lps].v = c;
      ps[lps].w = d;
    }
  }
  sort(ps + 1, ps + 1 + lps, cmp);
  for (int i = 1; i <= lps; i++) {
    if (gf(ps[i].u) != gf(ps[i].v)) {
      ww += ps[i].w;
      un(ps[i].u, ps[i].v);
    }
  }
  cout << ww;
  return 0;
}
