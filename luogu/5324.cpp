// by Sshwy
//#define DEBUGGER
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

#ifdef DEBUGGER

#define log_prefix()                                                \
  {                                                                 \
    fprintf(stderr, "\033[37mLine %-3d [%dms]:\033[0m  ", __LINE__, \
        (int)clock() / 1000);                                       \
  }
#define ilog(...) \
  { fprintf(stderr, __VA_ARGS__); }
#define llog(...)      \
  {                    \
    log_prefix();      \
    ilog(__VA_ARGS__); \
  }
#define log(...)           \
  {                        \
    log_prefix();          \
    ilog(__VA_ARGS__);     \
    fprintf(stderr, "\n"); \
  }
#define red(...)       \
  {                    \
    log_prefix();      \
    ilog("\033[31m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define green(...)     \
  {                    \
    log_prefix();      \
    ilog("\033[32m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define blue(...)      \
  {                    \
    log_prefix();      \
    ilog("\033[34m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }

#else

#define log_prefix() ;
#define ilog(...) ;
#define llog(...) ;
#define log(...) ;
#define red(...) ;
#define green(...) ;
#define blue(...) ;

#endif

namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 5e5 + 10, SZ = N << 2, _ = 2.5e5;

int n, m, cur, d, len;
int a[N], c[N], mn[SZ], s[SZ], tag[SZ];

void nodeadd(int u, int v) { mn[u] += v, tag[u] += v, assert(mn[u] >= 0); }
void pushup(int u) {
  if (mn[u << 1] < mn[u << 1 | 1])
    s[u] = s[u << 1], mn[u] = mn[u << 1];
  else if (mn[u << 1] > mn[u << 1 | 1])
    s[u] = s[u << 1 | 1], mn[u] = mn[u << 1 | 1];
  else
    s[u] = s[u << 1] + s[u << 1 | 1], mn[u] = mn[u << 1];
}
void pushdown(int u) {
  if (tag[u]) nodeadd(u << 1, tag[u]), nodeadd(u << 1 | 1, tag[u]), tag[u] = 0;
}
void build(int u = 1, int l = 1, int r = len) {
  if (l == r) return s[u] = 1, void();
  int mid = (l + r) >> 1;
  build(u << 1, l, mid), build(u << 1 | 1, mid + 1, r);
  pushup(u);
}
void add(int L, int R, int v, int u = 1, int l = 1, int r = len) {
  if (u == 1) printf("add %d %d %d\n", L - _, R - _, v);
  if (L > R) return;
  if (R < l || r < L) return;
  if (L <= l && r <= R) return nodeadd(u, v), void();
  int mid = (l + r) >> 1;
  pushdown(u);
  add(L, R, v, u << 1, l, mid), add(L, R, v, u << 1 | 1, mid + 1, r);
  pushup(u);
}
pair<int, int> operator+(pair<int, int> x, pair<int, int> y) {
  if (x.fi < y.fi)
    return x;
  else if (x.fi > y.fi)
    return y;
  else
    return {x.fi, x.se + y.se};
}
pair<int, int> query(int L, int R, int u = 1, int l = 1, int r = len) {
  if (L > R) return {1e9, 0};
  if (u == 1) printf("query %d %d\n", L - _, R - _);
  if (R < l || r < L) return {1e9, 0};
  if (L <= l && r <= R) return {mn[u], s[u]};
  int mid = (l + r) >> 1;
  pushdown(u);
  return query(L, R, u << 1, l, mid) + query(L, R, u << 1 | 1, mid + 1, r);
}

void insert(int x) {
  printf("insert %d\n", x);
  c[_ + x]++;
  if (x <= n + d) add(_ + x - c[_ + x] + 1, _ + x - c[_ + x] + 1, 1);
}
void remove(int x) {
  printf("remove %d (%d)\n", x, n + d);
  if (x <= n + d) add(_ + x - c[_ + x] + 1, _ + x - c[_ + x] + 1, -1);
  --c[_ + x];
}
int main() {
  scanf("%d%d", &n, &m);
  cur = _;
  len = _ + _;
  build();
  FOR(i, 1, n) {
    scanf("%d", &a[i]);
    insert(a[i]);
  }
  FOR(i, 1, m) {
    int op, x;
    scanf("%d%d", &op, &x);
    if (op == 0) {
      cur -= x;
      if (x == 1) {
        int x = n + d;
        add(_ + x - c[_ + x] + 1, _ + x, -1);
      }
      d += x;
      if (x == -1) {
        int x = n + d;
        add(_ + x - c[_ + x] + 1, _ + x, 1);
      }
    } else {
      remove(a[op]);
      a[op] = x - d;
      insert(a[op]);
    }
    printf("d %d\n", d);
    auto p = query(cur + 1, cur + n);
    int ans = p.fi == 0 ? p.se : 0;
    printf("%d\n", ans);
  }
  return 0;
}
