#include <bits/stdc++.h>
#define FOR(a, b, c) for (int a = (b); a <= (c); a++)
using namespace std;
typedef long long LL;

int n, m, q;

LL tr(LL i, LL j) { return (i - 1) * m + j; }
int main() {
  scanf("%d%d%d", &n, &m, &q);
  FOR(i, 1, n) T[i].insert_range(tr(i, 1), tr(i, m - 1));
  FOR(i, 1, n) T[n + 1].insert(tr(i, m)); //最后一列
  FOR(i, 1, q) {
    int x, y;
    scanf("%d%d", &x, &y);
    if (y < m) {
      LL cur = T[x].kth(y);
      T[x].erase_kth(y);
      T[x].insert(T[n + 1].kth(x));
      T[n + 1].erase_kth(x);
    }
    printf("%lld\n", cur);
  }
  return 0;
}
