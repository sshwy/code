#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
namespace IO {
  char nc() {
    static char bf[100000], *p1 = bf, *p2 = bf;
    return p1 == p2 && (p2 = (p1 = bf) + fread(bf, 1, 100000, stdin), p1 == p2)
               ? EOF
               : *p1++;
  }
  int rd() {
    int res = 0;
    char c = getchar();
    while (!isdigit(c)) c = getchar();
    while (isdigit(c)) res = res * 10 + c - '0', c = getchar();
    return res;
  }
} // namespace IO
/******************heading******************/
const int N = 100;
int n;
lld p[N];
void insert(lld x) {
  ROF(i, 62, 0) {
    if (!((x >> i) & 1)) continue; //判断第i位是否为0
    if (!p[i]) {
      p[i] = x;
      break;
    }          //不能张成，添加到线性基中
    x ^= p[i]; //去掉可以张成的维度（第i维消元）
  }
}
int main() {
  scanf("%d", &n);
  FOR(i, 1, n) {
    lld x;
    scanf("%lld", &x);
    insert(x);
  }
  lld ans = 0;
  ROF(i, 62, 0) {
    if (!((ans >> i) & 1)) ans ^= p[i];
  }
  printf("%lld", ans);
  return 0;
}
