// by Sshwy
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <stack>
#include <vector>
using namespace std;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
namespace IO {
  const int BSZ = 100000;
  char nc() {
    static char bf[BSZ], *p1 = bf, *p2 = bf;
    return p1 == p2 && (p2 = (p1 = bf) + fread(bf, 1, BSZ, stdin), p1 == p2) ? EOF
                                                                             : *p1++;
  }
  int rd() {
    int res = 0;
    char c = nc();
    while (!isdigit(c)) c = nc();
    while (isdigit(c)) res = res * 10 + c - '0', c = nc();
    return res;
  }
  long long rdll() {
    long long res = 0;
    char c = nc();
    while (!isdigit(c)) c = nc();
    while (isdigit(c)) res = res * 10 + c - '0', c = nc();
    return res;
  }
  void rd(int &x) { x = rd(); }
  void rd(int &x, int &y) { x = rd(), y = rd(); }
  void rd(int &x, int &y, int &z) { x = rd(), y = rd(), z = rd(); }
  void rd(char *s) {
    char c = nc();
    while (!isblank(c) && isprint(c) && c != EOF) *s++ = c, c = nc();
    *s = 0;
  }
  int tmp[20], lt;
  char OBF[BSZ], *OBP = OBF;
  void flush() { fwrite(OBF, 1, OBP - OBF, stdout), OBP = OBF; }
  void wrch(char x) {
    if (OBP == OBF + BSZ - 1) flush();
    *OBP = x, ++OBP;
  }
  void wr(int x, int base = 10) {
    lt = 0;
    while (x) tmp[++lt] = x % base, x /= base;
    while (lt > 0) wrch(tmp[lt] + '0'), --lt;
  }
  void wr(long long x, int base = 10) {
    lt = 0;
    while (x) tmp[++lt] = x % base, x /= base;
    while (lt > 0) wrch(tmp[lt] + '0'), --lt;
  }
  void wr(const char *s) {
    while (*s) wrch(*s), ++s;
  }
  void wr(char x) { wrch(x); }
} // namespace IO
/******************heading******************/
const int M = 1e5 + 5, N = 5e4 + 5;

int n, m, q;
struct atom {
  int a, b, c, d;
};
vector<atom> E, E2, Q, O, O1, ans;

bool cmp(atom x, atom y) {
  return x.c > y.c; //重量从大到小排序
}
int vis[M], val[M];

struct disjoint {
  int f[N], g[N];
  pair<int *, int> rcy[M * 2];
  int len;
  void init(int lim) { FOR(i, 0, lim) f[i] = i, g[i] = 1, len = 0; }
  int get(int u) { return f[u] == u ? u : get(f[u]); }
  void merge(int u, int v) {
    u = get(u), v = get(v);
    if (u == v) return;
    if (g[u] > g[v]) swap(u, v);
    rcy[++len] = {f + u, f[u]};
    rcy[++len] = {g + v, g[v]};
    f[u] = v, g[v] += g[u];
  }
  int getsize(int u) { return g[get(u)]; }
  int record() { return len; }
  void recovery(int rcd) {
    while (len > rcd) {
      pair<int *, int> x = rcy[len];
      *x.fi = x.se;
      --len;
    }
  }
} D;

void work() {
  O1 = O;
  sort(Q.begin(), Q.end(), cmp);
  sort(O.begin(), O.end(), cmp);

  for (atom x : O) vis[x.b] = 1; //标记为有修改
  E2.clear();
  FOR(i, 0, m - 1) if (!vis[i]) E2.pb(E[i]);
  else vis[i] = 0;
  sort(E2.begin(), E2.end(), cmp);

  D.init(n);

  int pos = 0;
  for (atom x : Q) { //枚举询问
    while (pos < m) {
      if (E2[pos].c < x.c)
        break;
      else
        D.merge(E2[pos].a, E2[pos].b), ++pos;
    }

    //记一下并查集状态
    int tag = D.record();

    for (atom y : O) {
      int p = y.b;
      if (!vis[p]) vis[p] = -1, val[p] = E[p].c;
      if (y.d < x.d && vis[p] < y.d) vis[p] = y.d, val[p] = y.c;
    }
    for (atom y : O) {
      int p = y.b;
      if (vis[p]) {
        if (val[p] >= x.c) D.merge(E[p].a, E[p].b);
        vis[p] = 0;
      }
    }

    ans.pb({D.getsize(x.b), 0, -x.d, 0});

    //还原并查集状态
    D.recovery(tag);
  }

  sort(ans.begin(), ans.end(), cmp);
  for (atom x : ans) IO::wr(x.a), IO::wr('\n');
  for (atom x : O1) E[x.b].c = x.c;

  Q.clear(); // remove operations
  O.clear(); // remove operations
  ans.clear();
}

int main() {
  IO::rd(n, m);
  FOR(i, 1, m) {
    int u, v, d;
    IO::rd(u, v, d);
    E.pb({u, v, d, 0});
  }
  IO::rd(q);
  int T = m == 0 ? 1 : sqrt(m * (log(m) / log(2)));
  FOR(i, 1, q) {
    int op, x, y;
    IO::rd(op, x, y);
    if (op == 1)
      O.pb({op, x - 1, y, i});
    else
      Q.pb({op, x, y, i});
    if (O.size() + Q.size() >= T) work();
  }
  work();
  IO::flush();
  return 0;
}
