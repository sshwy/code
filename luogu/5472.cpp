// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

typedef long long LL;
const int P = 998244353;

int pw(int a, int m) {
  int res = 1;
  while (m) m & 1 ? res = 1ll * res * a % P : 0, a = 1ll * a * a % P, m >>= 1;
  return res;
}

int n, m, typ;

typedef pair<LL, LL> pt;

struct Function {
  LL a, b, c;
  LL operator()(LL x) { return (a * x % P * x % P + b * x % P + c) % P; }
};

Function work(Function f, LL a) {
  LL in = pw(n, P - 2), in_1 = pw(n - 1, P - 2);
  LL y_1 = (f(1) * a % P + f(a + 1) * (n - a) % P) % P * in % P;
  LL y_n = (f(n) * (n - a) % P + f(a) * a % P) % P * in % P;
  LL y_2 =
      (f(2) * a % P * (a - 1) % P + f(a + 1) * a % P * (n - a) % P +
          f(1) * (n - a) % P * a % P + f(a + 2) * (n - a) % P * (n - a - 1) % P) %
      P * in % P * in_1 % P;
  LL x[] = {1, n, 2};
  LL y[] = {y_1, y_n, y_2};
  int oth[][2] = {1, 2, 0, 2, 0, 1};
  LL next_f[] = {0, 0, 0};
  FOR(i, 0, 2) {
    int j1 = oth[i][0], j2 = oth[i][1];
    LL coef = pw(((x[i] - x[j1]) * (x[i] - x[j2]) % P + P) % P, P - 2);
    LL g[] = {x[j1] * x[j2] % P, (P * 2ll - x[j1] - x[j2]) % P, 1};
    FOR(k, 0, 2) g[k] = g[k] * coef % P * y[i] % P;
    FOR(k, 0, 2) next_f[k] = (next_f[k] + g[k]) % P;
  }
  Function nf;
  nf.a = next_f[2];
  nf.b = next_f[1];
  nf.c = next_f[0];
  return nf;
}

Function f;

int main() {
  scanf("%d%d%d", &n, &m, &typ);
  if (typ == 1)
    f.b = 1;
  else
    f.a = 1;
  FOR(i, 1, m) {
    int x;
    scanf("%d", &x);
    f = work(f, x);
  }
  int q;
  scanf("%d", &q);
  FOR(i, 1, q) {
    int x;
    scanf("%d", &x);
    printf("%lld\n", f(x));
  }
  return 0;
}
