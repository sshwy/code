#include <cstdio>
#include <iostream>
#include <queue>

#define _INF 0 - 999999999

using namespace std;

int n, d, k;
long long sum;
int x[500001], s[500001], f[500001];
int l, r, mid;

struct data {
  int fi, xi;
  friend bool operator<(data ths, data tht) { return ths.fi < tht.fi; }
};
data nt;

priority_queue<data> pq;

bool walk(int g) {
  for (int i = 1; i <= n; i++) f[i] = _INF;
  while (!pq.empty()) pq.pop();
  for (int i = 1, pp = 0; i <= n; i++) {
    while (pp < i) {
      if (x[i] - x[pp] < d - g)
        break;
      else if (x[i] - x[pp] > d + g)
        pp++;
      else {
        pq.push((data){f[pp], x[pp]});
        pp++;
      }
    }
    while (!pq.empty()) {
      nt = pq.top();
      if (x[i] - nt.xi > d + g)
        pq.pop();
      else
        break;
    }
    if (!pq.empty() && nt.fi != _INF) f[i] = nt.fi + s[i];
    if (k <= f[i]) return true;
  }
  return false;
}

int main() {
  scanf("%d%d%d", &n, &d, &k);
  for (int i = 1; i <= n; i++) {
    scanf("%d%d", &x[i], &s[i]);
    if (s[i] > 0) sum += s[i];
  }
  if (sum < k) {
    printf("-1");
    return 0;
  }
  r = x[n];
  while (l < r) {
    mid = (l + r) >> 1;
    if (walk(mid))
      r = mid;
    else
      l = mid + 1;
  }
  printf("%d", l);
  return 0;
}
