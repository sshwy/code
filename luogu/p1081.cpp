#include <algorithm>
#include <cstdio>
#include <vector>
#define fi first
#define se second
#define int long long
#define pii pair<int, int>
using namespace std;
const int N = 1e5 + 5;
int n, x0;
int h[N];

struct data {
  int pre, nex, idx, h;
};
data e[N];
int le;
bool cmp(data x, data y) { return x.h < y.h; }
bool cmp2(data x, data y) { return x.idx < y.idx; }

int n1[N], n2[N];
int d(int x, int y) { return abs(h[x] - h[y]); }

int nex[N][18][2], A[N][18][2], B[N][18][2];
void prework() {
  for (int i = 1; i <= n; i++) nex[i][0][0] = n2[i], nex[i][0][1] = n1[i];
  for (int i = 1; i <= n; i++) nex[i][1][0] = n1[n2[i]], nex[i][1][1] = n2[n1[i]];
  for (int j = 2; (1 << j) <= n; j++) {
    for (int i = 1; i + (1 << j) <= n; i++) {
      nex[i][j][0] = nex[nex[i][j - 1][0]][j - 1][0];
      nex[i][j][1] = nex[nex[i][j - 1][1]][j - 1][1];
    }
  }
  for (int i = 1; i <= n; i++) A[i][0][0] = d(i, n2[i]);
  for (int i = 1; i <= n; i++) A[i][1][0] = A[i][0][0], A[i][1][1] = A[n1[i]][0][0];
  for (int j = 2; (1 << j) <= n; j++) {
    for (int i = 1; i + (1 << j) <= n; i++) {
      A[i][j][0] = A[i][j - 1][0] + A[nex[i][j - 1][0]][j - 1][0];
      A[i][j][1] = A[i][j - 1][1] + A[nex[i][j - 1][1]][j - 1][1];
    }
  }
  for (int i = 1; i <= n; i++) B[i][0][1] = d(i, n1[i]);
  for (int i = 1; i <= n; i++) B[i][1][0] = B[n2[i]][0][1], B[i][1][1] = B[i][0][1];
  for (int j = 2; (1 << j) <= n; j++) {
    for (int i = 1; i + (1 << j) <= n; i++) {
      B[i][j][0] = B[i][j - 1][0] + B[nex[i][j - 1][0]][j - 1][0];
      B[i][j][1] = B[i][j - 1][1] + B[nex[i][j - 1][1]][j - 1][1];
    }
  }
}
pii query(int x, int u) {
  //从u出发行驶不超过x的AB路程
  int a = 0, b = 0;
  for (int j = 17; j >= 1; j--) {
    if (!u) break; //相当于已经走到尽头
    if (!nex[u][j][0]) continue;
    int dis = A[u][j][0] + B[u][j][0];
    if (dis > x) continue;
    a += A[u][j][0], b += B[u][j][0], x -= dis;
    u = nex[u][j][0];
  }
  if (u)
    if (nex[u][0][0] && A[u][0][0] <= x) a += A[u][0][0];
  return make_pair(a, b);
}
signed main() {
  scanf("%lld", &n);
  for (int i = 1; i <= n; i++) {
    scanf("%lld", &h[i]);
    e[++le] = (data){0, 0, i, h[i]};
  }
  sort(e + 1, e + le + 1, cmp);
  for (int i = 1; i <= n; i++) { e[i].pre = e[i - 1].idx, e[i].nex = e[i + 1].idx; }
  sort(e + 1, e + le + 1, cmp2);
  for (int i = 1; i <= n; i++) {
    int p = e[i].pre, pp = e[p].pre;
    int q = e[i].nex, qq = e[q].nex;
    int m = 0x7fffffff, m2 = 0x7fffffff;
    if (p && d(p, i) < m) n1[i] = p, m = d(p, i);
    if (q && d(q, i) < m) n1[i] = q, m = d(p, i);
    if (p && p != n1[i] && d(p, i) < m2) n2[i] = p, m2 = d(p, i);
    if (pp && pp != n1[i] && d(pp, i) < m2) n2[i] = pp, m2 = d(pp, i);
    if (q && q != n1[i] && d(q, i) < m2) n2[i] = q, m2 = d(q, i);
    if (qq && qq != n1[i] && d(qq, i) < m2) n2[i] = qq, m2 = d(qq, i);
    if (p) e[p].nex = q;
    if (q) e[q].pre = p;
  }
  prework();
  scanf("%lld", &x0);
  pii a1 = make_pair(1, 0);
  int ans1 = 0;
  for (int i = 1; i <= n; i++) {
    pii p = query(x0, i);
    if (p.se == 0 && p.fi == 0) p.fi = 1; //无穷大处理
    if (a1.fi * p.se > a1.se * p.fi)
      a1 = p, ans1 = i;
    else if (a1.fi * p.se == a1.se * p.fi)
      ans1 = h[ans1] > h[i] ? ans1 : i;
  }
  printf("%lld\n", ans1);
  int m;
  scanf("%lld", &m);
  for (int i = 1; i <= m; i++) {
    int x, s;
    scanf("%lld%lld", &s, &x);
    pii p = query(x, s);
    printf("%lld %lld\n", p.fi, p.se);
  }
  return 0;
}
/*
 * BUG#1:L71 没有用idx
 * BUG#2:L60L54 没有判断nex的存在性
 * BUG#3:L81L82 pp和q的顺序问题
 */
