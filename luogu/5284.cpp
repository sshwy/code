#include <bits/stdc++.h>
#define FOR(a, b, c) for (int a = b; a <= int(c); ++a)
#define ROF(a, b, c) for (int a = b; a >= int(c); --a)
#define pb push_back
using namespace std;
const int N = 3e5 + 5, SZ = N * 2, ALP = 26;
char s[N];
int n, tr[SZ][ALP], fail[SZ], ed[SZ], len[SZ + N], las, tot;
int nd[N], a[SZ], b[SZ], fa[SZ][20];
int A[N], B[N + N], tot2, id[N + N];
bool Atyp[N + SZ];
set<int> sA[SZ], sB[SZ];
queue<int> q;

struct qxx {
  int nex, t, v;
} e[(N + SZ) * 3];
int h[N + SZ], le = 0, dg[N + SZ];
void add_path(int f, int t, int v) {
  assert(0 < f && f < N + SZ && 0 < t && t < N + SZ);
  e[++le] = {h[f], t, v}, h[f] = le;
  dg[t]++;
}
long long f[N + SZ][2];
// f[i,0]表示到i的最大答案
// f[i,1]表示到i且必选i。当i为Atype时可转移到f[i,0]

void insert(int c, int ID) {
  // printf("insert %d %d\n",c,ID);
  int p = las, u = ++tot;
  len[u] = len[las] + 1;
  ed[u] = ed[las] + 1;
  while (p && tr[p][c] == 0) tr[p][c] = u, p = fail[p];
  if (!p)
    fail[u] = 1;
  else {
    int q = tr[p][c];
    if (len[q] == len[p] + 1)
      fail[u] = q;
    else {
      int cq = ++tot;
      len[cq] = len[p] + 1;
      ed[cq] = ed[q];
      memcpy(tr[cq], tr[q], sizeof(tr[q]));
      fail[cq] = fail[q], fail[q] = fail[u] = cq;
      while (p && tr[p][c] == q) tr[p][c] = cq, p = fail[p];
    }
  }
  las = u;
  nd[ID] = las;
}
void qsort() {
  FOR(i, 0, n) b[i] = 0;
  FOR(i, 1, tot) b[len[i]]++;
  FOR(i, 1, n) b[i] += b[i - 1];
  ROF(i, tot, 1) a[b[len[i]]--] = i;
}
void init() {
  FOR(i, 0, tot) {
    fill(tr[i], tr[i] + ALP, 0);
    fill(fa[i], fa[i] + 20, 0);
  }
  fill(len, len + tot2 + 1, 0);
  fill(fail, fail + tot + 1, 0);
  FOR(i, 0, tot) sA[i].clear();
  FOR(i, 0, tot) sB[i].clear();
  memset(e, 0, sizeof(e[0]) * (le + 1));
  memset(h, 0, sizeof(h));
  memset(f, 0, sizeof(f));
  memset(dg, 0, sizeof(dg));
  memset(Atyp, 0, sizeof(Atyp));
  las = tot = 1, fail[1] = 0, len[1] = len[0] = 0;
  le = 0, tot2 = 0;
}
void go() {
  init();
  scanf("%s", s + 1);
  n = strlen(s + 1);
  reverse(s + 1, s + n + 1);

  FOR(i, 1, n) insert(s[i] - 'a', i);
  qsort();
  FOR(i, 1, tot) {
    int u = a[i];
    fa[u][0] = fail[u];
    FOR(j, 1, 19) {
      fa[u][j] = fa[fa[u][j - 1]][j - 1];
      if (fa[u][j] == 0) break;
    }
  }
  auto find = [](int u, int Len) {
    ROF(j, 19, 0) {
      if (len[fa[u][j]] >= Len) u = fa[u][j];
    }
    return u;
  };

  int na;
  scanf("%d", &na);
  FOR(i, 1, na) {
    int l, r;
    scanf("%d%d", &l, &r);
    l = n - l + 1, r = n - r + 1, swap(l, r);
    int Len = r - l + 1;
    assert(Len > 0);

    int u = find(nd[r], Len);
    sA[u].insert(i);
    A[i] = Len;
  }

  int nb;
  scanf("%d", &nb);
  FOR(i, 1, nb) {
    int l, r;
    scanf("%d%d", &l, &r);
    l = n - l + 1, r = n - r + 1, swap(l, r);
    int Len = r - l + 1;
    assert(Len > 0);

    int u = find(nd[r], Len);
    sB[u].insert(i + na);
    B[i + na] = Len;
  }

  tot2 = tot;
  // sam 节点编号是原来的，na，nb的编号是新加的
  FOR(u, 1, tot) {
    vector<pair<int, int>> v;
    for (auto x : sA[u]) v.pb({-A[x], x});
    for (auto x : sB[u]) v.pb({-B[x], x});
    sort(v.begin(), v.end());

    int curLen = -len[u], cur = u;
    for (auto pr : v) {
      if (pr.first == curLen) {
        id[pr.second] = cur;
      } else {
        assert(curLen < pr.first);
        id[pr.second] = ++tot2;
        len[tot2] = -pr.first;
        add_path(cur, tot2, 0);
        cur = tot2, curLen = -len[cur];
      }
    }

    if (fail[u]) {
      assert(curLen < -len[fail[u]]);
      add_path(cur, fail[u], 0);
    }
  }

  int m;
  scanf("%d", &m);
  FOR(i, 1, m) {
    int x, y;
    scanf("%d%d", &x, &y);
    y += na;
    add_path(id[y], id[x], 1);
  }

  long long ans = 0;
  FOR(i, 1, na) {
    int u = id[i];
    f[u][0] = len[u];
    Atyp[u] = 1;
  }
  FOR(i, 1, tot2) if (dg[i] == 0) q.push(i);
  while (!q.empty()) {
    int u = q.front();
    q.pop();
    ans = max(ans, f[u][0]);
    for (int i = h[u]; i; i = e[i].nex) {
      const int v = e[i].t, w = e[i].v;
      if (w == 0) {
        assert(len[u] > len[v]);
        f[v][0] = max(f[v][0], f[u][0]);
      } else {
        f[v][1] = max(f[v][1], f[u][0]);
      }
      if (Atyp[v]) { f[v][0] = max(f[v][0], f[v][1] + len[v]); }
      assert(dg[v]);
      --dg[v];
      if (dg[v] == 0) q.push(v);
    }
  }
  FOR(i, 1, tot2) if (dg[i]) return puts("-1"), void();
  printf("%lld\n", ans);
}
int main() {
  int t;
  scanf("%d", &t);
  FOR(i, 1, t) go();
  return 0;
}