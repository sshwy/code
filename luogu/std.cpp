#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
const int kcz = 998244353;
const int maxn = 10000005;
ll a, b, c, f[maxn];
int n;
inline ll qpow(ll a, ll k) {
  ll res = 1;
  while (k) {
    if (k & 1) res = res * a % kcz;
    if (k >>= 1) a = a * a % kcz;
  }
  return res;
}
inline ll calc(ll x) { return ((a * x + b) % kcz * x + c) % kcz; } // 算第x个数的期望
int main() {
  int m, tp, i;
  ll _, __, t1, t2, t3, t, ___, sqn;
  // freopen("landlords.in","r",stdin),freopen("landlords.out","w",stdout);
  scanf("%d%d%d", &n, &m, &tp), sqn = n * (ll)n % kcz;
  _ = qpow(n - 1, kcz - 2), __ = qpow(n, kcz - 2),
  ___ = qpow((-sqn + 3 * n - 2) % kcz, kcz - 2);
  if (tp == 1)
    a = c = 0, b = 1;
  else
    a = 1, b = c = 0; // x_i=ai^2+bi+c
  while (m--) {
    scanf("%lld", &t);
    if (t == 0 || t == n) continue;
    t1 = (calc(1) * t + calc(t + 1) * (n - t)) % kcz * __ % kcz; // 第一个
    t2 = ((calc(2) * (t - 1) + calc(t + 1) * (n - t)) % kcz * t +
             (calc(1) * t + calc(t + 2) * (n - t - 1)) % kcz * (n - t)) %
         kcz * __ % kcz * _ % kcz;                           // 第二个
    t3 = (calc(t) * t + calc(n) * (n - t)) % kcz * __ % kcz; // 第n个
    a = ((2 - n) * t1 + (n - 1) * t2 - t3) % kcz * ___ % kcz;
    b = ((sqn - 4) * t1 + (1 - sqn) * t2 + 3 * t3) % kcz * ___ % kcz;
    c = ((4 * n - 2 * sqn) * t1 + (sqn - n) * t2 - 2 * t3) % kcz * ___ %
        kcz; // 极其丑的差值
  }
  for (i = 1; i <= n; i++) f[i] = (calc(i) + kcz) % kcz;
  scanf("%d", &m);
  while (m--) scanf("%lld", &t), printf("%lld\n", f[t]);
  fclose(stdin), fclose(stdout);
  return 0;
}
