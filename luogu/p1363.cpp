#include <cstdio>
#include <iostream>

using namespace std;

int n, m, sx, sy;
char qp[4501][4501];
bool bqp[4501][4501], f;

void search(int x, int y) {
  if (f == 1) return;
  if (x < 1 || x > n * 3 || y < 1 || y > m * 3)
    return;
  else if (qp[x][y] == '#' || bqp[x][y] == 1)
    return;
  bqp[x][y] = 1;
  if (qp[x][y] == 'S') {
    f = 1;
    return;
  }
  /*	for(int mox=x%n;mox<=n*3;mox+=n)
    {
      for(int moy=y%m;moy<=m*3;moy+=m)
      {
        bqp[mox][moy]=1;
      }
    }*/

  search(x - 1, y);
  search(x + 1, y);
  search(x, y - 1);
  search(x, y + 1);
}

int main() {
  //	freopen("p1363in.txt","r",stdin);
  while (cin >> n >> m) {
    for (int i = 1; i <= n; i++) {
      for (int j = 1; j <= m; j++) {
        cin >> qp[i][j];
        qp[i][j + m] = qp[i][j + m + m] = qp[i][j];
        qp[i + n + n][j] = qp[i + n][j] = qp[i][j];
        qp[i + n + n][j + m] = qp[i + n][j + m] = qp[i][j];
        qp[i + n + n][j + m + m] = qp[i + n][j + m + m] = qp[i][j];

        bqp[i][j] = 0;
        bqp[i][j + m] = bqp[i][j + m + m] = bqp[i][j];
        bqp[i + n + n][j] = bqp[i + n][j] = bqp[i][j];
        bqp[i + n + n][j + m] = bqp[i + n][j + m] = bqp[i][j];
        bqp[i + n + n][j + m + m] = bqp[i + n][j + m + m] = bqp[i][j];

        if (qp[i][j] == 'S') {
          sx = i + n;
          sy = j + m;
        }
      }
    }
    f = 0;
    qp[sx][sy] = 's';

    search(sx, sy);
    if (f == 1)
      cout << "Yes\n";
    else {
      for (int i = 1 + n; i <= n + n; i++) {
        if (bqp[i][1 + m] == 1 && bqp[i][m + m] == 1) {
          cout << "Yes\n";
          f = 1;
          break;
        }
      }
    }
    if (f == 0) {
      for (int i = 1 + m; i <= m + m; i++) {
        if (bqp[1 + n][i] == 1 && bqp[n + n][i] == 1) {
          cout << "Yes\n";
          f = 1;
          break;
        }
      }
    }
    if (f == 0) cout << "No\n";

    /*for(int i=1;i<=n*3;i++)
    {
      for(int j=1;j<=m*3;j++)
      {
        cout<<bqp[i][j];
      }
      cout<<endl;
    }*/
  }
  return 0;
}
