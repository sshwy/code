#include <iostream>
#define max(a, b) (a > b ? a : b)

using namespace std;

int n, a, b, c;
int qp[10][10];    // 1start
int f[20][10][10]; // f[i][j][k]:走了i步，两条路垂径分别为j，k时的最大值
// f[i][j][k]=max(f[i-1][j][k],f[i-1][j-1][k],f[i-1][j][k-1],f[i-1][j-1][k-1])+qp[j][i-j+1]+qp[k][i-k+1]
// if(j==k)f[i][j][k]-=qp[j][i-j+1]

int main() {
  cin >> n;
  while (cin >> a >> b >> c) {
    if (a == 0) break;
    qp[a][b] = c;
  }
  for (int i = 1; i <= n * 2 - 1; i++) {
    for (int j = 1; j <= n && j <= i; j++) {
      for (int k = 1; k <= n && k <= i; k++) {
        f[i][j][k] = max(max(f[i - 1][j][k], f[i - 1][j - 1][k]),
                         max(f[i - 1][j][k - 1], f[i - 1][j - 1][k - 1])) +
                     qp[j][i - j + 1] + qp[k][i - k + 1];
        if (j == k) f[i][j][k] -= qp[j][i - j + 1];
      }
    }
  }
  cout << f[n * 2 - 1][n][n];
  return 0;
}
