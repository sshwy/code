#include <iostream>

using namespace std;

int s, t, w;
char jam[30];

bool add(int pos) {
  if (pos < 0) return false;
  if (jam[pos] + w - pos - 1 < t) {
    jam[pos]++;
    for (int i = 1; pos + i < w; i++) jam[pos + i] = jam[pos] + i;
    return true;
  }
  return add(pos - 1);
}

int main() {
  cin >> s >> t >> w >> jam;
  s += 96;
  t += 96;
  for (int i = 1; i <= 5; i++) {
    if (add(w - 1))
      cout << jam << endl;
    else
      break;
  }
  return 0;
}
