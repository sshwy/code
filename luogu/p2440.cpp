#include <iostream>

using namespace std;

long long n, k, sum;
long long wood[100001];
long long l, r, mid;

int main() {
  cin >> n >> k;
  for (long long i = 1; i <= n; i++) {
    cin >> wood[i];
    sum += wood[i];
  }
  if (sum < k) {
    cout << 0;
    return 0;
  }
  l = 1;
  r = sum;
  while (l + 1 < r) {
    mid = (l + r) >> 1;
    long long t = 0;
    for (long long i = 1; i <= n; i++) t += wood[i] / mid;
    if (t < k)
      r = mid;
    else
      l = mid;
  }
  cout << l;
  return 0;
}
