#include <algorithm>
#include <iostream>

using namespace std;

int n, tot, s;
long long num[200001];

int main() {
  cin >> n;
  num[0] = -1;
  for (int i = 1; i <= n; i++) cin >> num[i];
  sort(num + 1, num + n + 1);
  for (int i = 1; i <= n; i++) {
    if (num[i - 1] != num[i]) {
      if (i != 1) cout << num[i - 1] << ' ' << s << endl;
      s = 1;
    } else
      s++;
  }
  cout << num[n] << ' ' << s << endl;
  return 0;
}
