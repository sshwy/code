// by Sshwy
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <stack>
#include <vector>
using namespace std;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 5e5 + 5, M = 5e5 + 5, Q = M;
#define int long long

struct dsu {
  int f[N], g[N], h[N];
  dsu() { FOR(i, 0, N - 1) f[i] = i, g[i] = 1, h[i] = 0; }
  stack<pair<int *, int>> s;
  int get(int u) { return f[u] == u ? u : get(f[u]); }
  int merge(int u, int v, int w) {
    u = get(u), v = get(v);
    if (u == v) return h[u];
    if (g[u] > g[v]) swap(u, v);
    s.push({f + u, f[u]});
    f[u] = v;
    s.push({g + v, g[v]});
    g[v] += g[u];
    s.push({h + v, h[v]});
    h[v] = h[u] + h[v] + w;
    return h[v];
  }
  bool find(int u, int v) { return get(u) == get(v); }
  int time() { return s.size(); }
  void rebase(int Time) {
    while (s.size() > Time) *s.top().fi = s.top().se, s.pop();
  }
} dis;

int n, m, q;
struct p3 {
  int a, b, c;
} E[N];              // static
pair<int, int> D[N]; // dynamic
int ans[Q];

// each layer
vector<p3> staticEdge[30];

void zip(int l, int r, int dep) {
  vector<p3> &S = staticEdge[dep], newS, necessary;

  sort(S.begin(), S.end(), [](p3 x, p3 y) { return x.c < y.c; }); // small to
                                                                  // big

  int tag = dis.time();
  // assign to -oo (necessary)
  FOR(i, l, r) { dis.merge(E[D[i].fi].a, E[D[i].fi].b, E[D[i].fi].c); }
  for (p3 x : S) {
    if (dis.find(x.a, x.b)) continue;
    dis.merge(x.a, x.b, x.c);
    necessary.pb(x);
  }
  dis.rebase(tag);

  // assign to oo (useless)
  for (p3 x : S) {
    if (dis.find(x.a, x.b)) {
    } else {
      dis.merge(x.a, x.b, x.c);
      newS.pb(x);
    }
  }
  S = newS;
  newS.clear();
  dis.rebase(tag);

  // merge necessary to point
  for (p3 x : necessary) dis.merge(x.a, x.b, x.c);
  for (p3 x : S) {
    if (dis.find(x.a, x.b)) continue;
    newS.pb(x);
  }
  S = newS;
}

int vis[M];

void solve(int l, int r, int dep) {
  zip(l, r, dep); // delete useless static edges, merge necessary static edges
  if (l == r) {
    vector<p3> &S = staticEdge[dep];
    S.pb({E[D[l].fi].a, E[D[l].fi].b, D[l].se});
    sort(S.begin(), S.end(), [](p3 x, p3 y) { return x.c < y.c; }); // small to big
    int tim = dis.time();
    for (p3 x : S) ans[l] = dis.merge(x.a, x.b, x.c);
    dis.rebase(tim);
    return;
  }
  int mid = (l + r) >> 1;
  // turn some dynamicEdge to staticEdge for [l,mid]
  vector<p3> &S = staticEdge[dep], &newS = staticEdge[dep + 1];

  newS = S;
  FOR(i, l, r) vis[D[i].fi] = 0;
  FOR(i, l, mid) vis[D[i].fi] = 1;
  FOR(i, mid + 1, r)
  if (vis[D[i].fi] == 0) newS.pb(E[D[i].fi]), vis[D[i].fi] = 1;

  int tag = dis.time();
  solve(l, mid, dep + 1);
  dis.rebase(tag);

  newS = S;
  FOR(i, l, mid) E[D[i].fi].c = D[i].se; // apply
  FOR(i, l, r) vis[D[i].fi] = 0;
  FOR(i, mid + 1, r) vis[D[i].fi] = 1;
  FOR(i, l, mid) if (vis[D[i].fi] == 0) newS.pb(E[D[i].fi]), vis[D[i].fi] = 1;

  solve(mid + 1, r, dep + 1);
}

signed main() {
  scanf("%lld%lld%lld", &n, &m, &q);
  FOR(i, 1, m) scanf("%lld%lld%lld", &E[i].a, &E[i].b, &E[i].c);
  FOR(i, 1, q) {
    int k, d;
    scanf("%lld%lld", &k, &d);
    D[i] = {k, d};
  }

  FOR(i, 1, q) vis[D[i].fi] = 1;
  FOR(i, 1, m) if (!vis[i]) staticEdge[1].pb(E[i]);

  solve(1, q, 1);

  FOR(i, 1, q) printf("%lld\n", ans[i]);
  return 0;
}
