#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
namespace IO {
  char nc() {
    static char bf[100000], *p1 = bf, *p2 = bf;
    return p1 == p2 && (p2 = (p1 = bf) + fread(bf, 1, 100000, stdin), p1 == p2)
               ? EOF
               : *p1++;
  }
  int rd() {
    int res = 0;
    char c = getchar();
    while (!isdigit(c)) c = getchar();
    while (isdigit(c)) res = res * 10 + c - '0', c = getchar();
    return res;
  }
} // namespace IO
/******************heading******************/
const int SZ = 1e5 + 5, INF = 0x3f3f3f3f;

struct Splay {
  int tot, ch[SZ][2], key[SZ], sz[SZ], pa[SZ], root;
  int rv[SZ];
  int new_node(int v) {
    ++tot, ch[tot][0] = ch[tot][1] = pa[tot] = 0, key[tot] = v;
    sz[tot] = rv[tot] = 0;
    return tot;
  }
  Splay() {
    tot = 0;
    new_node(-INF), new_node(INF);
    pa[1] = 2, ch[2][0] = 1, root = 2;
  }
  bool get(int u) { return ch[pa[u]][1] == u; }
  void node_rv(int u) {
    if (!u) return;
    // printf("node_rv(%d)\n",u);
    // printf("sz=%d\n",sz[u]);
    swap(ch[u][0], ch[u][1]);
    rv[u] ^= 1;
  }
  void pushup(int u) { sz[u] = sz[ch[u][0]] + sz[ch[u][1]] + 1; }
  void pushdown(int u) {
    if (rv[u]) {
      node_rv(ch[u][0]);
      node_rv(ch[u][1]);
      rv[u] = 0;
    }
  }
  void rotate(int u) {
    int p = pa[u], pp = pa[p], gu = get(u);
    pushdown(u);
    ch[pp][get(p)] = u, pa[u] = pp;
    ch[p][gu] = ch[u][gu ^ 1], pa[ch[u][gu ^ 1]] = p;
    ch[u][gu ^ 1] = p, pa[p] = u;
    pushup(p), pushup(u);
  }
  void splay(int u, int v) {
    while (pa[u] != v) {
      int p = pa[u];
      if (pa[p] != v) rotate(get(u) == get(p) ? p : u);
      rotate(u);
    }
    if (!v) root = u;
  }
  bool find(int v) { // return if v exists. Based on key
    if (!root) return 0;
    int u = root;
    while (key[u] != v && ch[u][key[u] < v]) pushdown(u), u = ch[u][key[u] < v];
    splay(u, 0);
    return key[root] == v;
  }
  void insert(int v) {
    if (!root) return root = new_node(v), void();
    int u = root, nu = new_node(v);
    while (ch[u][key[u] < v]) pushdown(u), u = ch[u][key[u] < v];
    ch[u][key[u] < v] = nu, pa[nu] = u;
    splay(nu, 0);
  }
  int kth(int rk) {
    ++rk;
    if (!root) return 0;
    int u = root;
    while (sz[ch[u][0]] + 1 != rk) {
      pushdown(u);
      if (rk <= sz[ch[u][0]])
        u = ch[u][0];
      else
        rk -= sz[ch[u][0]] + 1, u = ch[u][1];
    }
    splay(u, 0);
    return u;
  }
  void dfs(int u) {
    if (!u) return;
    pushdown(u);
    dfs(ch[u][0]);
    if (key[u] != INF && key[u] != -INF) printf("%d ", key[u]);
    dfs(ch[u][1]);
  }
  void dfs() {
    dfs(root);
    puts("");
  }
  void reverse(int L, int R) {
    int u = kth(L - 1);
    int v = kth(R + 1);
    splay(u, v);
    node_rv(ch[u][1]);
  }
} splay;

int n, m;
int main() {
  scanf("%d%d", &n, &m);
  FOR(i, 1, n) splay.insert(i);
  FOR(i, 1, m) {
    int l, r;
    scanf("%d%d", &l, &r);
    splay.reverse(l, r);
  }
  splay.dfs();
  return 0;
}
/*
 * splay(v,0),splay(u,0)后v不一定是u的左儿子！
 */
