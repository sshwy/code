#include <cstdio>
#include <iostream>

using namespace std;

int m, n, x, y, c, total = 999999999, ansl;
int qp[101][101];
int cqp[101][101];

void search(int nx, int ny, int cost, int last) {
  int t = 0;
  if (nx < 1 || nx > m || ny < 1 || ny > m)
    return;
  else if (qp[nx][ny] == 0)
    return;
  else if (cost >= cqp[nx][ny])
    return;
  /*else if(nx==m&&ny==m)
  {
    if(cost<total)
    {
      total=cost;
      cqp[m][m]=cost;
    }
    return;
  }*/

  //	cout<<"search("<<nx<<","<<ny<<","<<cost<<","<<last<<");\n";

  cqp[nx][ny] = (last != qp[nx][ny] ? cost + 1 : cost);
  //	if(last!=qp[nx][ny])t++;
  if (nx == m && ny == m) { return; }

  search(nx - 1, ny, cqp[nx][ny], qp[nx][ny]);
  search(nx + 1, ny, cqp[nx][ny], qp[nx][ny]);
  search(nx, ny - 1, cqp[nx][ny], qp[nx][ny]);
  search(nx, ny + 1, cqp[nx][ny], qp[nx][ny]);

  search(nx - 1, ny - 1, cqp[nx][ny] + 2, qp[nx][ny]);
  search(nx + 1, ny - 1, cqp[nx][ny] + 2, qp[nx][ny]);
  search(nx + 1, ny + 1, cqp[nx][ny] + 2, qp[nx][ny]);
  search(nx - 1, ny + 1, cqp[nx][ny] + 2, qp[nx][ny]);

  search(nx - 2, ny, cqp[nx][ny] + 2, qp[nx][ny]);
  search(nx + 2, ny, cqp[nx][ny] + 2, qp[nx][ny]);
  search(nx, ny - 2, cqp[nx][ny] + 2, qp[nx][ny]);
  search(nx, ny + 2, cqp[nx][ny] + 2, qp[nx][ny]);
}

int main() {
  //	freopen("p3956in.in","r",stdin);
  cin >> m >> n;
  for (int i = 1; i <= n; i++) {
    cin >> x >> y >> c;
    qp[x][y] = c + 1;
    cqp[x][y] = 999999999;
  }
  for (int i = 1; i <= m; i++) {
    for (int j = 1; j <= m; j++) { cqp[i][j] = 999999999; }
  }

  search(1, 1, 0, qp[1][1]);

  if (qp[m][m] == 0) {
    cqp[m][m] = cqp[m - 1][m] < cqp[m][m - 1] ? cqp[m - 1][m] : cqp[m][m - 1];
    ansl = 2;
  }

  if (cqp[m][m] == 999999999)
    cout << -1;
  else
    cout << cqp[m][m] + ansl;

  /*cout<<endl;
  for(int i=1;i<=m;i++)
  {
    for(int j=1;j<=m;j++)
    {
      cout<<cqp[i][j]<<'\t';
    }
    cout<<endl;
  }*/

  return 0;
}
