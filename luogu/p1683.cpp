#include <iostream>

using namespace std;

int w, h, sx, sy;
int bqp[30][30];
char qp[30][30];

int search(int x, int y) {
  if (x < 1 || y < 1 || x > h || y > w) return 0;
  if (qp[x][y] == '#') return 0;
  if (bqp[x][y] == 1) return 0;
  bqp[x][y] = 1;
  return search(x - 1, y) + search(x + 1, y) + search(x, y - 1) + search(x, y + 1) +
         1;
}

int main() {
  cin >> w >> h;
  for (int i = 1; i <= h; i++)
    for (int j = 1; j <= w; j++) {
      cin >> qp[i][j];
      if (qp[i][j] == '@') sx = i, sy = j;
    }
  cout << search(sx, sy);
  return 0;
}
