#include <cstdio>
#include <iostream>

using namespace std;

int n, num[500011], presum[500011];

int main() {
  //	freopen("p3131in.in","r",stdin);
  scanf("%d", &n);
  //	cin>>n;
  for (int i = 1; i <= n; i++) {
    scanf("%d", &num[i]);
    //		cin>>num[i];
    presum[i] = (num[i] + presum[i - 1]) % 7;
  }
  for (int sl = n; sl > 0; sl--) // section_length
  {
    for (int presta = 0; presta <= n - sl; presta++) // prefix start
    {
      if (presum[presta + sl] - presum[presta] == 0) {
        cout << sl;
        return 0;
      }
    }
  }
  return 0;
}
