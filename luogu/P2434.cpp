#include <bits/stdc++.h>
using namespace std;
int n;
struct data {
  int l, r;
};
data s[50004];
int ans[50004][2], cnt;
bool cmp(data a, data b) { return a.l < b.l; }

int main() {
  scanf("%d", &n);
  for (int i = 1; i <= n; i++) scanf("%d%d", &s[i].l, &s[i].r);
  sort(s + 1, s + n + 1, cmp);
  int cur = 0, pre = 0;
  for (int i = 1; i <= n; i++) {
    if (s[i].l > cur) {
      ans[cnt][0] = pre, ans[cnt][1] = cur, cnt++;
      pre = s[i].l, cur = s[i].r;
    } else
      cur = max(cur, s[i].r);
  }
  for (int i = 1; i < cnt; i++) printf("%d %d\n", ans[i][0], ans[i][1]);
  printf("%d %d\n", pre, cur);
  return 0;
}
