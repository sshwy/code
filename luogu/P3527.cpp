#include <bits/stdc++.h>
using namespace std;
const int N = 1000006;
char s[N];
int sa[N], rk[N];
int bkt[N];

void suffix_array() {
  int l = strlen(s);
  int sz = max(l, 80);

  for (int i = 0; i < l; i++) rk[i] = s[i] - '0';

  for (int i = 0; i < sz; i++) bkt[i] = 0;
  for (int i = 0; i < l; i++) bkt[rk[i]]++;
  for (int i = 1; i < sz; i++) bkt[i] += bkt[i - 1];
  for (int i = l - 1; i >= 0; i--) sa[--bkt[rk[i]]] = i;

  int tmp[N];
  for (int j = 1; j <= l; j <<= 1) {
    int p = -1;
    for (int i = l - j; i < l; i++) tmp[++p] = i;
    for (int i = 0; i < l; i++)
      if (sa[i] - j >= 0) tmp[++p] = sa[i] - j;

    for (int i = 0; i < sz; i++) bkt[i] = 0;
    for (int i = 0; i < l; i++) bkt[rk[i]]++;
    for (int i = 1; i < sz; i++) bkt[i] += bkt[i - 1];
    for (int i = l - 1; i >= 0; i--) sa[--bkt[rk[tmp[i]]]] = tmp[i];

    p = 0, tmp[sa[0]] = 0;
    for (int i = 1; i < l; i++) {
      int v0 = sa[i - 1], v1 = sa[i], v00, v01;
      v00 = (v0 + j < l) ? rk[v0 + j] : -1;
      v01 = (v1 + j < l) ? rk[v1 + j] : -1;
      tmp[sa[i]] = (rk[v0] == rk[v1] && v00 == v01) ? p : ++p;
    }
    for (int i = 0; i < l; i++) rk[i] = tmp[i];
  }
}

int main() {
  scanf("%s", s);
  suffix_array();
  for (int i = 0; s[i]; i++) printf("%d ", sa[i] + 1);
  return 0;
}