#include <bits/stdc++.h>
using namespace std;
const int N = 10004, M = 50004;

int n, m, ans;

struct qxx {
  int nex, t;
};
qxx e[M];
int h[M], cnt;
void add_path(int f, int t) { e[++cnt] = {h[f], t}, h[f] = cnt; }

int dfn[N], low[N], dfn_cnt;
int s[N], tp;
bool vis[N];
void tarjan(int u) {
  if (dfn[u]) return;
  low[u] = dfn[u] = ++dfn_cnt;
  s[++tp] = u, vis[u] = true;
  for (int i = h[u]; i; i = e[i].nex) {
    const int v = e[i].t;
    if (!dfn[v]) {
      tarjan(v);
      low[u] = min(low[u], low[v]);
    } else if (vis[v])
      low[u] = min(low[u], dfn[v]);
  }
  if (low[u] == dfn[u]) {
    int tot = 1;
    while (s[tp] != u) {
      tot++;
      // printf("%d ",s[tp]);
      vis[s[tp]] = false;
      --tp;
    }
    // printf("%d\n",s[tp]);
    --tp;
    vis[u] = false;

    if (tot > 1) ans++;
  }
}

int main() {
  scanf("%d%d", &n, &m);
  for (int i = 1, a, b; i <= m; i++) {
    scanf("%d%d", &a, &b);
    add_path(a, b);
  }
  for (int i = 1; i <= n; i++) tarjan(i);
  printf("%d", ans);
  return 0;
}