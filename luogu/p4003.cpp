#include <bits/stdc++.h>
using namespace std;
const int N = 1e6 + 6, INF = 0x3f3f3f3f;
int n, m, tot;

struct qxx {
  int nex, t, v, c;
};
qxx e[N * 5];
int h[N], cnt = 1;
void add_path(int f, int t, int v, int c) {
  e[++cnt] = (qxx){h[f], t, v, c}, h[f] = cnt;
}
void add_flow(int f, int t, int v, int c) {
  add_path(f, t, v, c), add_path(t, f, 0, -c);
}

int s, t;
int d[N];
bool spfa() {
  bool vising[N] = {0};
  queue<int> q;
  memset(d, 0x3f, sizeof(int) * (tot * 5 + 10));
  d[s] = 0, q.push(s);
  while (q.size()) {
    int u = q.front();
    q.pop(), vising[u] = 0;
    for (int i = h[u]; i; i = e[i].nex) {
      const int &v = e[i].t, &w = e[i].v, &c = e[i].c;
      if (!w || d[v] <= d[u] + c) continue;
      d[v] = d[u] + c;
      if (!vising[v]) q.push(v), vising[v] = 1;
    }
  }
  return d[t] != 0x3f3f3f3f;
}
int mincost, maxflow, totflow;
bool vis[N];
int dfs(int u, int flow) {
  vis[u] = 1;
  if (u == t) return flow;
  int rest = flow;
  for (int i = h[u]; i && rest; i = e[i].nex) {
    const int &v = e[i].t, &w = e[i].v, &c = e[i].c;
    if (vis[v] && v != t || !w || d[v] != d[u] + c) continue;
    int k = dfs(v, min(rest, w));
    if (k)
      mincost += k * c, e[i].v -= k, e[i ^ 1].v += k, rest -= k;
    else
      d[v] = 0; //増广完了
  }
  return flow - rest;
}

void add(int sig, int f, int t, int v, int c) {
  //标记为1就是正向，0就反向
  if (sig & 1)
    add_flow(f, t, v, c);
  else
    add_flow(t, f, v, c);
}
void init_path() {
  for (int i = 1; i <= tot; i++) { // current node: i
    int a, b = (i - 1) / m + (i - 1) % m;
    scanf("%d", &a);
#define U(i) (i + tot * (x % 4))
#define R(i) (i + tot * ((x + 1) % 4))
#define D(i) (i + tot * ((x + 2) % 4))
#define L(i) (i + tot * ((x + 3) % 4))
#define M(i) (i + tot * 4)
    int x = 0;
    // connect with s or t
    if (b & 1)
      add_flow(s, M(i), INF, 0);
    else
      add_flow(M(i), t, INF, 0);
    // printf("%d:%d, %d, %d, %d\n", M(i), U(i), R(i), D(i), L(i));
    if (i + m <= tot) add(b, D(i), U(i + m), 1, 0);
    if (i % m != 0) add(b, R(i), L(i + 1), 1, 0);
    if (a & 1) totflow++, add(b, M(i), U(i), 1, 0);
    if (a & 2) totflow++, add(b, M(i), R(i), 1, 0);
    if (a & 4) totflow++, add(b, M(i), D(i), 1, 0);
    if (a & 8) totflow++, add(b, M(i), L(i), 1, 0);
    switch (a) {
      // The first group
      case 8:
        ++x;
      case 4:
        ++x;
      case 2:
        ++x;
      case 1:
        add(b, U(i), L(i), 1, 1), add(b, U(i), R(i), 1, 1);
        add(b, U(i), D(i), 1, 2);
        break;
      // The second group
      case 12:
        ++x;
      case 6:
        ++x;
      case 3:
        ++x;
      case 9:
        add(b, U(i), D(i), 1, 1), add(b, L(i), R(i), 1, 1);
        break;
      // The thrid group
      case 13:
        ++x;
      case 14:
        ++x;
      case 7:
        ++x;
      case 11:
        add(b, R(i), D(i), 1, 1), add(b, L(i), D(i), 1, 1);
        add(b, U(i), D(i), 1, 2);
        break;
    }
  }
}
int main() {
  scanf("%d%d", &n, &m);
  tot = n * m;
  s = 0, t = tot * 5 + 1;
  init_path(); //瑟瑟发抖地写一个函数来加边
  while (spfa()) {
    vis[t] = 1;
    while (vis[t]) {
      memset(vis, 0, sizeof(bool) * (tot * 5 + 10));
      maxflow += dfs(s, 0x3f3f3f3f);
      // printf("%d %d\n", maxflow, mincost);
    }
  }
  // printf("%d %d %d\n", maxflow, mincost, totflow);
  if (maxflow * 2 == totflow)
    printf("%d", mincost);
  else
    printf("-1");
  return 0;
}
/*
 * BUG#1: &1写成 %1
 * BUG#2: b=(i-1)/m+i%m忘-1
 * OPT#1: 改一下memset的sizeof就30分了
 * OPT#2: 在47行加一个&&v!=t的条件，然后就NB了
 * BUG#3: 改成b=(i-1)/m+(i-1)/m
 */
