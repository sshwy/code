#include <algorithm>
#include <iostream>
using namespace std;

int n;
struct data {
  int a, b;
};
data d[1001];
bool cmp(data x, data y) { return x.b < y.b; }
long long tot = 1;

int main() {
  cin >> n;
  for (int i = 0; i <= n; i++) cin >> d[i].a >> d[i].b;
  sort(d + 1, d + n + 1, cmp);
  for (int i = 0; i <= n; i++) tot *= d[i].a;
  cout << tot / d[n].b;
  return 0;
}
