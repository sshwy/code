#include <algorithm>
#include <iostream>

using namespace std;

int n, c;
int room[100001];

int low_bound, up_bound, mid; // largest shortest distance

int main() {
  cin >> n >> c;
  for (int i = 1; i <= n; i++) { cin >> room[i]; }
  sort(room + 1, room + n + 1);
  low_bound = 0;
  up_bound = room[n] - room[1];
  room[0] = -99999999;
  do {
    mid = (low_bound + up_bound) / 2;
    int placed_cow = 0, pre_room = 0;
    for (int now_room = 1; now_room <= n; now_room++) {
      if (room[now_room] - room[pre_room] >= mid) {
        placed_cow++;
        pre_room = now_room;
      }
    }
    //		cout<<"placed:"<<placed_cow<<"\tlow:"<<low_bound<<"\tup:"<<up_bound<<endl;
    if (placed_cow < c) {
      up_bound = mid;
    } else {
      low_bound = mid;
    }
  } while (low_bound + 1 != up_bound);
  cout << low_bound;
  return 0;
}
