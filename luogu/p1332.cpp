#include <iostream>
#include <queue>

#define INF 9999999

using namespace std;

int n, m, a, b, x1, y1;
int qp[501][501];

struct point {
  int x, y, t;
};

queue<point> q;

int main() {
  cin >> n >> m >> a >> b;
  for (int i = 1; i <= n; i++)
    for (int j = 1; j <= m; j++) qp[i][j] = INF;
  for (int i = 1; i <= a; i++) {
    cin >> x1 >> y1;
    q.push((point){x1, y1, 0});
  }
  while (!q.empty()) {
    point now = q.front();
    q.pop();
    if (now.x < 1 || now.x > n || now.y < 1 || now.y > m) continue;
    if (qp[now.x][now.y] <= now.t) continue;
    qp[now.x][now.y] = now.t;
    q.push((point){now.x - 1, now.y, now.t + 1});
    q.push((point){now.x + 1, now.y, now.t + 1});
    q.push((point){now.x, now.y - 1, now.t + 1});
    q.push((point){now.x, now.y + 1, now.t + 1});
  }
  for (int i = 1; i <= b; i++) {
    cin >> x1 >> y1;
    cout << qp[x1][y1] << endl;
  }
  return 0;
}
