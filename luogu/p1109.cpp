#include <iostream>

using namespace std;

int n, l, r, s[55], sum, L, R;

int main() {
  cin >> n;
  for (int i = 1; i <= n; i++) {
    cin >> s[i];
    sum += s[i];
  }
  cin >> l >> r;
  if (sum < l * n || sum > r * n)
    cout << -1;
  else {
    for (int i = 1; i <= n; i++) {
      if (s[i] < l)
        L += l - s[i];
      else if (s[i] > r)
        R += s[i] - r;
    }
    if (R < L)
      cout << L;
    else
      cout << R;
  }
  return 0;
}
