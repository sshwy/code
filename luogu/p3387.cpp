#include <bits/stdc++.h>
using namespace std;
const int N = 10004, M = 100005;
int n, m, ans;
int a[M], b[M];
int c[N];

struct graph {
  struct qxx {
    int nex, t;
  };
  qxx e[M];
  int h[N], cnt;
  void add_path(int f, int t) { e[++cnt] = (qxx){h[f], t}, h[f] = cnt; }
};
graph g, g_scc;

int dfn[N], low[N], dfn_cnt;
bool vis[N];
int s[N], tp;
int scc[N], sc[N], sn; // scc,s_cost,s_n
void tarjan(int u) {
  if (dfn[u]) return;
  low[u] = dfn[u] = ++dfn_cnt;
  vis[u] = true, s[++tp] = u;
  for (int i = g.h[u]; i; i = g.e[i].nex) {
    const int v = g.e[i].t;
    if (!dfn[v]) {
      tarjan(v);
      low[u] = min(low[u], low[v]);
    } else if (vis[v])
      low[u] = min(low[u], dfn[v]);
  }
  if (dfn[u] == low[u]) {
    scc[u] = ++sn, sc[sn] += c[u];
    while (s[tp] != u) {
      sc[sn] += c[s[tp]]; // scc的总权值
      scc[s[tp]] = sn, vis[s[tp]] = false, --tp;
    }
    vis[u] = false, --tp;
  }
}

int id[N], f[N];

int main() {
  scanf("%d%d", &n, &m);
  for (int i = 1; i <= n; i++) scanf("%d", &c[i]);
  for (int i = 1; i <= m; i++) {
    scanf("%d%d", &a[i], &b[i]);
    g.add_path(a[i], b[i]);
  }
  for (int i = 1; i <= n; i++) tarjan(i);
  //建立新的SCC图
  for (int i = 1; i <= m; i++) {
    if (scc[a[i]] != scc[b[i]]) {
      g_scc.add_path(scc[a[i]], scc[b[i]]);
      ++id[scc[b[i]]];
    }
  }
  //拓扑排序+DP
  queue<int> q;
  for (int i = 1; i <= sn; i++)
    if (!id[i]) q.push(i), f[i] = sc[i];
  while (!q.empty()) {
    int k = q.front();
    q.pop();
    ans = max(ans, f[k]);
    for (int i = g_scc.h[k]; i; i = g_scc.e[i].nex) {
      const int v = g_scc.e[i].t;
      f[v] = max(f[v], f[k] + sc[v]);
      --id[v];
      if (!id[v]) q.push(v);
    }
  }
  printf("%d", ans);
  return 0;
}