#include <iostream>
#include <vector>

using namespace std;

int n, a, b, tot;
int w[10001], id[10001], c[10001];
vector<int> linker[10001];

void count(int k) {
  if (c[k] != 0) return;
  int v = 0;
  for (int nex = 0; nex < linker[k].size(); nex++) {
    int p = linker[k][nex];
    if (c[p] == 0) count(p);
    if (c[p] > v) v = c[p];
  }
  c[k] = w[k] + v;
  return;
}

int main() {
  cin >> n;
  for (int i = 1; i <= n; i++) {
    cin >> a;
    cin >> w[a];
    while (cin >> b) {
      if (b == 0) break;
      linker[b].push_back(a);
      id[a]++;
    }
  }
  for (int i = 1; i <= n; i++) {
    if (id[i] == 0) {
      count(i);
      if (c[i] > tot) tot = c[i];
    }
  }
  cout << tot;
  return 0;
}
