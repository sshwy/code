#include <iostream>

using namespace std;

int n, a[30010], a2[30010];
int l[30010], c;
int l2[30010], c2;

int lower_bound(int *arr, int L, int R, int v) {
  while (L < R) {
    int mid = (L + R + 1) >> 1;
    if (arr[mid] <= v)
      L = mid;
    else
      R = mid - 1;
  }
  return L;
}

int main() {
  cin >> n;
  for (int i = 1; i <= n; i++) {
    cin >> a[i];
    a2[n - i + 1] = a[i];
  }
  for (int i = 1; i <= n; i++) {
    int pre = lower_bound(l, 0, c, a[i]);
    if (pre == c) l[++c] = a[i];
    if (a[i] < l[++pre]) l[pre] = a[i];
  }
  for (int i = 1; i <= n; i++) {
    int pre = lower_bound(l2, 0, c2, a2[i]);
    if (pre == c2) l2[++c2] = a2[i];
    if (a2[i] < l2[++pre]) l2[pre] = a2[i];
  }
  cout << n - (c > c2 ? c : c2);
  return 0;
}
