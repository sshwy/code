#include <bits/stdc++.h>
using namespace std;
typedef const int &cint;
const int N = 100005;
const int LST_SZ = 1 << 19;
int n, m, root, p;
int dfn, a[N];

struct data {
  int nex, t;
};
data e[N * 2];
int head[N], hcnt, vis[N];
void add_path(int f, int t) { e[++hcnt] = (data){head[f], t}, head[f] = hcnt; }

int s[LST_SZ], tg[LST_SZ];
void push_up(int rt) { s[rt] = (s[rt << 1] + s[rt << 1 | 1]) % p; }
void push_down(int l, int r, int rt) {
  int mid = (l + r) >> 1;
  s[rt << 1] = (s[rt << 1] + tg[rt] * (mid - l + 1)) % p,
          tg[rt << 1] = (tg[rt << 1] + tg[rt]) % p;
  s[rt << 1 | 1] = (s[rt << 1 | 1] + tg[rt] * (r - mid)) % p,
              tg[rt << 1 | 1] = (tg[rt << 1 | 1] + tg[rt]) % p;
  tg[rt] = 0;
}
int lst_build(int l, int r, int rt) {
  if (l == r) return s[rt] = a[l];
  int mid = (l + r) >> 1;
  lst_build(l, mid, rt << 1), lst_build(mid + 1, r, rt << 1 | 1);
  push_up(rt);
}
int lst_add(int L, int R, int v, int l, int r, int rt) {
  if (L <= l && r <= R)
    return s[rt] = (s[rt] + v * (r - l + 1)) % p, tg[rt] = (tg[rt] + v) % p;
  int mid = (l + r) >> 1;
  push_down(l, r, rt);
  if (!(R < l || mid < L)) lst_add(L, R, v, l, mid, rt << 1);
  if (!(R < mid + 1 || r < L)) lst_add(L, R, v, mid + 1, r, rt << 1 | 1);
  push_up(rt);
}
int lst_sum(int L, int R, int l, int r, int rt) {
  if (L <= l && r <= R) return s[rt];
  int mid = (l + r) >> 1, sum = 0;
  push_down(l, r, rt);
  if (!(R < l || mid < L)) sum += lst_sum(L, R, l, mid, rt << 1);
  if (!(R < mid + 1 || r < L)) sum += lst_sum(L, R, mid + 1, r, rt << 1 | 1);
  return sum % p;
}

struct node {
  int p, s, b, v;               // parent,son,brother,value
  int dp, tp, bt, hvs, sz, idx; // deep,top,bottom,heavy-son,size,index
};
node t[N];
void add_son(node *T, int p, int s) { T[s].p = p, T[s].b = T[p].s, T[p].s = s; }
int tree_build_dfs(int rt, int dp) {
  vis[rt] = 1, t[rt].dp = dp, t[rt].sz = 1;
  for (int i = head[rt]; i; i = e[i].nex) {
    if (!vis[e[i].t]) {
      add_son(t, rt, e[i].t);
      t[rt].sz += tree_build_dfs(e[i].t, dp + 1);
    }
  }
  return t[rt].sz;
}
void tree_decomposition_dfs(int rt, int tp) {
  t[rt].tp = tp, t[rt].idx = ++dfn, t[rt].bt = dfn, a[dfn] = t[rt].v;
  if (!t[rt].s) return;
  for (int mx = 0, i = t[rt].s; i; i = t[i].b)
    if (t[i].sz > mx) t[rt].hvs = i, mx = t[i].sz;
  tree_decomposition_dfs(t[rt].hvs, tp);
  for (int i = t[rt].s; i; i = t[i].b)
    if (i != t[rt].hvs) tree_decomposition_dfs(i, i);
  t[rt].bt = dfn;
}
int tree_path_add(int x, int y, int v) {
  while (t[x].tp != t[y].tp) {
    if (t[t[x].tp].dp < t[t[y].tp].dp) x ^= y ^= x ^= y;
    lst_add(t[t[x].tp].idx, t[x].idx, v, 1, n, 1);
    x = t[t[x].tp].p;
  }
  if (t[x].idx > t[y].idx) x ^= y ^= x ^= y;
  lst_add(t[x].idx, t[y].idx, v, 1, n, 1);
}
int tree_path_sum(int x, int y) {
  int res = 0;
  while (t[x].tp != t[y].tp) {
    if (t[t[x].tp].dp < t[t[y].tp].dp) x ^= y ^= x ^= y;
    res += lst_sum(t[t[x].tp].idx, t[x].idx, 1, n, 1);
    res %= p;
    x = t[t[x].tp].p;
  }
  if (t[x].idx > t[y].idx) x ^= y ^= x ^= y;
  return (res + lst_sum(t[x].idx, t[y].idx, 1, n, 1)) % p;
}
int tree_add(int x, int v) { lst_add(t[x].idx, t[x].bt, v, 1, n, 1); }
int tree_sum(int x) {
  int res = lst_sum(t[x].idx, t[x].bt, 1, n, 1);
  return res;
}

int main() {
  scanf("%d%d%d%d", &n, &m, &root, &p);
  for (int i = 1; i <= n; i++) { scanf("%d", &t[i].v); }
  for (int i = 1, x, y; i < n; i++) {
    scanf("%d%d", &x, &y);
    add_path(x, y);
    add_path(y, x);
  }
  tree_build_dfs(root, 1);
  tree_decomposition_dfs(root, root);
  lst_build(1, n, 1);
  for (int i = 1, opr, x, y, z; i <= m; i++) {
    scanf("%d%d", &opr, &x);
    if (opr == 1) {
      scanf("%d%d", &y, &z);
      tree_path_add(x, y, z);
    } else if (opr == 2) {
      scanf("%d", &y);
      printf("%d\n", tree_path_sum(x, y));
    } else if (opr == 3) {
      scanf("%d", &z);
      tree_add(x, z);
    } else {
      printf("%d\n", tree_sum(x));
    }
  }
  return 0;
}