#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 2e4 + 5, M = N * 2;

struct qxx {
  int nex, t;
};
qxx e[M];
int h[N], le;
void add_path(int f, int t) { e[++le] = (qxx){h[f], t}, h[f] = le; }
#define FORe(i, u, v) for (int i = h[u], v; v = e[i].t, i; i = e[i].nex)

struct basis {
  long long b[61];
  basis() { memset(b, 0, sizeof(b)); }
  bool insert(long long x) {
    ROF(i, 60, 0) {
      if (x >> i & 1) {
        if (b[i])
          x ^= b[i];
        else
          return b[i] = x, 1;
      }
    }
    return 0;
  }
  long long qmax() {
    long long res = 0;
    ROF(i, 60, 0) if ((res ^ b[i]) > res) res ^= b[i];
    return res;
  }
  basis operator+(basis bi) {
    basis res = bi;
    FOR(i, 0, 60) if (b[i]) res.insert(b[i]);
    return res;
  }
};
int n, q;
long long a[N];
int fa[N][20], dep[N];
basis b[N][20];

void dfs(int u, int p = 0) {
  // printf("dfs(%d,%d)\n",u,p);
  fa[u][0] = p, dep[u] = dep[p] + 1;
  b[u][0].insert(a[u]);
  FOR(j, 1, 19) {
    fa[u][j] = fa[fa[u][j - 1]][j - 1];
    b[u][j] = b[u][j - 1] + b[fa[u][j - 1]][j - 1];
    if (!fa[u][j]) break;
  }
  FORe(i, u, v) if (v != p) dfs(v, u);
}
int lca(int x, int y) {
  if (x == y) return x;
  if (dep[x] < dep[y]) swap(x, y);
  ROF(j, 19, 0) if (dep[x] - (1 << j) > dep[y]) x = fa[x][j];
  if (fa[x][0] == y) return y;
  if (dep[x] > dep[y]) x = fa[x][0];
  ROF(j, 19, 0) if (fa[x][j] != fa[y][j]) x = fa[x][j], y = fa[y][j];
  return fa[x][0];
}
basis qquery(int x, int p) {
  int len = dep[x] - dep[p] + 1, j = log(len) / log(2);
  int h = len - (1 << j), y = x;
  ROF(j, 19, 0) if ((1 << j) <= h) h -= (1 << j), y = fa[y][j];
  return b[x][j] + b[y][j];
}
long long query(int x, int y) {
  // printf("query(%d,%d)\n",x,y);
  int z = lca(x, y);
  return (qquery(x, z) + qquery(y, z)).qmax();
}
int main() {
  scanf("%d%d", &n, &q);
  FOR(i, 1, n) scanf("%lld", &a[i]);
  FOR(i, 1, n - 1) {
    int x, y;
    scanf("%d%d", &x, &y);
    add_path(x, y), add_path(y, x);
  }
  dfs(1);
  FOR(i, 1, q) {
    int x, y;
    scanf("%d%d", &x, &y);
    printf("%lld\n", query(x, y));
  }
  return 0;
}
