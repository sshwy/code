#include <iostream>

#define min(a1, a2) (a1 < a2 ? a1 : a2)

using namespace std;

int n, m, mmax, a, b;
int f[1001][1001], mx;

int main() {
  cin >> n >> m;
  for (int i = 1; i <= n; i++)
    for (int j = 1; j <= n; j++) f[i][j] = 1;
  for (int i = 1; i <= m; i++) {
    cin >> a >> b;
    f[a][b] = 0;
  }
  for (int i = 1; i <= n; i++)
    for (int j = 1; j <= n; j++)
      if (f[i][j]) {
        f[i][j] = min(min(f[i - 1][j], f[i - 1][j - 1]), f[i][j - 1]) + 1;
        if (f[i][j] > mx) mx = f[i][j];
      }
  cout << mx;
  return 0;
}
