#include <iostream>

using namespace std;

int n;
char a, b, c, root;
char p[10000];
char leftt[10000];
char rightt[10000];

void print_tree(char k) {
  if (k == '*') return;
  cout << k;
  print_tree(leftt[k]);
  print_tree(rightt[k]);
}

int main() {
  cin >> n;
  for (int i = 1; i < 10000; i++) { p[i] = leftt[i] = rightt[i] = '*'; }
  for (int i = 1; i <= n; i++) {
    cin >> a >> b >> c;
    leftt[a] = b;
    rightt[a] = c;
    p[b] = p[c] = a;
  }
  root = a;
  while (p[root] != '*') root = p[root];
  print_tree(root);
  return 0;
}
