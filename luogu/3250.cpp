#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
namespace IO {
  char nc() {
    static char bf[100000], *p1 = bf, *p2 = bf;
    return p1 == p2 && (p2 = (p1 = bf) + fread(bf, 1, 100000, stdin), p1 == p2)
               ? EOF
               : *p1++;
  }
  int rd() {
    int res = 0;
    char c = getchar();
    while (!isdigit(c)) c = getchar();
    while (isdigit(c)) res = res * 10 + c - '0', c = getchar();
    return res;
  }
} // namespace IO
/******************heading******************/
const int N = 1e5 + 5, M = 2e5 + 5, SZ = 1 << 22;

int n, m, maxm;

struct qxx {
  int nex, t;
}; /*{{{*/
qxx e[N * 2];
int h[N], le;
void add_path(int f, int t) { e[++le] = (qxx){h[f], t}, h[f] = le; }

int bg[N], ed[N], totime;
int dep[N];
int st[N * 2][20];

void dfs(int u, int p) {
  dep[u] = dep[p] + 1;
  bg[u] = ++totime;
  // printf("bg[%d]=%d\n",u,bg[u]);
  st[totime][0] = u;
  for (int i = h[u]; i; i = e[i].nex) {
    const int v = e[i].t;
    if (v == p) continue;
    dfs(v, u);
    st[++totime][0] = u;
  }
  ed[u] = totime;
  // printf("ed[%d]=%d\n",u,ed[u]);
}
void bin_exp() {
  FOR(j, 1, 19) {
    if ((1 << j) > totime) break;
    int ilim = totime - (1 << j) + 1;
    FOR(i, 1, ilim) {
      st[i][j] = dep[st[i][j - 1]] < dep[st[i + (1 << j - 1)][j - 1]]
                     ? st[i][j - 1]
                     : st[i + (1 << j - 1)][j - 1];
      // printf("st[%d,%d]=%d\n",i,j,st[i][j]);
    }
  }
}
int lca(int u, int v) {
  int l = bg[u], r = bg[v];
  if (l > r) l ^= r ^= l ^= r;
  int j = log(r - l + 1) / log(2);
  return dep[st[l][j]] < dep[st[r - (1 << j) + 1][j]] ? st[l][j]
                                                      : st[r - (1 << j) + 1][j];
} /*}}}*/
struct op {
  int typ, a, b, v, id, _v, ans;
  void read(int idx) {
    scanf("%d", &typ);
    if (typ == 0)
      scanf("%d%d%d", &a, &b, &v);
    else if (typ == 1)
      scanf("%d", &v);
    else
      scanf("%d", &v);
    id = idx;
  }
} dat[M];
bool cmp(op x, op y) { return x.typ == 0 && y.typ == 0 ? x.v < y.v : x.typ < y.typ; }
bool cmp2(op x, op y) { return x.id < y.id; }

bool in(int x, pii p) { // if point x is in path p
  if (p.fi == -1) return 0;
  if (p.fi == 0) return 1;
  int a = p.fi, b = p.se, c = lca(a, b);
  if (x == a || x == b || x == c) return 1;
  if (lca(c, x) != c) return 0;
  if (lca(a, x) == x || lca(b, x) == x) return 1;
  return 0;
}
pii inter(pii p1, pii p2) { //点的路径交。边的话直接取最深的两个lca
  if (p1.fi == -1 || p2.fi == -1) return mk(-1, -1);
  if (p1.fi == 0 || p2.fi == 0) return mk(p1.fi + p2.fi, p1.se + p2.se);
  int x = p1.fi, y = p1.se, a = p2.fi, b = p2.se;
  int z = lca(x, y), c = lca(a, b), zc = lca(z, c);
  if (zc != z && zc != c) return mk(-1, -1);
  if (dep[z] < dep[c]) swap(x, a), swap(y, b), swap(z, c);
  int ax = lca(a, x), ay = lca(a, y), bx = lca(b, x), by = lca(b, y);
  if (lca(ay, z) != z && lca(by, z) != z) return mk(-1, -1);
  if (dep[ax] < dep[ay]) swap(ax, ay);
  if (dep[bx] < dep[by]) swap(bx, by);
  if (dep[ax] < dep[bx]) swap(ax, bx), swap(ay, by);
  if (dep[ay] < dep[bx]) swap(ay, bx);
  return mk(ax, ay);
}
struct node {
  int lc, rc;
  pii p; // 0,0 -> infty;-1,-1 -> none
} T[SZ];
int tot = 1; // BUG#1:初值没有设成-1

int new_node(int x, int y) {
  ++tot;
  T[tot].p = mk(x, y);
  return tot;
}
void pushdown(int u) {
  if (!T[u].lc) T[u].lc = new_node(0, 0);
  if (!T[u].rc) T[u].rc = new_node(0, 0);
}
void pushup(int u) { T[u].p = inter(T[T[u].lc].p, T[T[u].rc].p); }
void assign(int x, int y, int val, int u = 1, int l = 1, int r = maxm) {
  if (l == r) return T[u].p = mk(x, y), void();
  pushdown(u);
  int mid = (l + r) >> 1;
  if (val <= mid)
    assign(x, y, val, T[u].lc, l, mid);
  else
    assign(x, y, val, T[u].rc, mid + 1, r);
  pushup(u);
}
int qry(int x, int u = 1, int l = 1, int r = maxm) {
  if (T[u].p.fi == 0) return -1;
  if (in(x, T[u].p)) return -1;
  // printf("qry(%d,%d,%d,%d):(%d,%d)\n",x,u,l,r,T[u].p.fi,T[u].p.se);
  if (l == r) return l;
  int mid = (l + r) >> 1;
  pushdown(u);
  int t = qry(x, T[u].rc, mid + 1, r);
  if (t != -1) return t;
  return qry(x, T[u].lc, l, mid);
}

void add(int u, int v, int val) { assign(u, v, val); }
void del(int val) { assign(0, 0, val); }
int query(int u) { return qry(u); }

int main() {
  scanf("%d%d", &n, &m);
  FOR(i, 1, n - 1) {
    int u, v;
    scanf("%d%d", &u, &v);
    add_path(u, v), add_path(v, u);
  }
  dfs(1, 0);
  bin_exp();
  FOR(i, 1, m) { dat[i].read(i); }
  sort(dat + 1, dat + m + 1, cmp);
  FOR(i, 1, m) if (dat[i].typ == 0) dat[i]._v = i;
  else {
    maxm = i;
    break;
  }
  sort(dat + 1, dat + m + 1, cmp2);
  FOR(i, 1, m) {
    int typ = dat[i].typ;
    if (typ == 0) {
      int a = dat[i].a, b = dat[i].b, v = dat[i]._v;
      add(a, b, v);
    } else if (typ == 1) {
      int t = dat[i].v;
      del(dat[t]._v);
    } else {
      int x = dat[i].v;
      dat[i].ans = query(x); //返回离散化后的值
    }
  }
  sort(dat + 1, dat + m + 1, cmp);
  FOR(i, 1, m)
  if (dat[i].typ == 2 && dat[i].ans != -1) dat[i].ans = dat[dat[i].ans].v;
  sort(dat + 1, dat + m + 1, cmp2);
  FOR(i, 1, m) if (dat[i].typ == 2) printf("%d\n", dat[i].ans);
  return 0;
}
