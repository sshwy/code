#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int P = 998244353, N = 5e6 + 5;
int pw(int a, int m) {
  int res = 1;
  while (m) m & 1 ? res = 1ll * res * a % P : 0, a = 1ll * a * a % P, m >>= 1;
  return res;
}
int fac[N], fnv[N];

int n, m, l, lim, k;
int a[N], b[N], c[N], f[N], x[N];

inline int binom(int n, int m) {
  if (n < m) return 0;
  return fac[n] * 1ll * fnv[m] % P * fnv[n - m] % P;
}
int sig(int x) { return 1 - 2 * (x & 1); }
void go() {
  scanf("%d%d%d%d", &n, &m, &l, &k);
  lim = min(min(n, m), l);

  int nml = 1ll * n * m % P * l % P;
  FOR(i, 0, lim) x[i] = 1ll * (n - i) * (m - i) % P * (l - i) % P;
  FOR(i, 1, lim) a[i] = (nml - x[i]) % P;
  b[0] = 1;
  FOR(i, 1, lim) b[i] = a[i] * 1ll * b[i - 1] % P;
  c[lim] = pw(b[lim], P - 2);
  ROF(i, lim, 1) c[i - 1] = c[i] * 1ll * a[i] % P;
  FOR(i, 1, lim) a[i] = c[i] * 1ll * b[i - 1] % P;

  f[0] = 1;
  FOR(i, 1, lim) f[i] = f[i - 1] * 1ll * x[i - 1] % P * a[i] % P;

  int ans = 0;
  FOR(j, k, lim) ans = (ans + binom(j, k) * 1ll * f[j] % P * sig(j - k)) % P;
  ans = (ans + P) % P;
  printf("%d\n", ans);
}
int main() {
  fac[0] = 1;
  FOR(i, 1, N - 1) fac[i] = fac[i - 1] * 1ll * i % P;
  fnv[N - 1] = pw(fac[N - 1], P - 2);
  ROF(i, N - 1, 1) fnv[i - 1] = fnv[i] * 1ll * i % P;
  int t;
  scanf("%d", &t);
  FOR(i, 1, t) go();
  return 0;
}
