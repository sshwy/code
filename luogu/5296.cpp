// by Yao
#include <bits/stdc++.h>
#define int long long
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

const int N = 35, K = 35, P = 998244353;
int pw(int a, int m) {
  int res = 1;
  while (m) m & 1 ? res = 1ll * res * a % P : 0, a = 1ll * a * a % P, m >>= 1;
  return res;
}

int n, k;

struct Poly {
  int c[K];
  Poly() { memset(c, 0, sizeof(c)); }
  int &operator[](unsigned i) {
    assert(i < K);
    return c[i];
  }
  Poly operator+(Poly a) {
    Poly f;
    FOR(i, 0, k) f[i] = (c[i] + a[i]) % P;
    return f;
  }
  Poly operator-(Poly a) {
    Poly f;
    FOR(i, 0, k) f[i] = (0ll + P + c[i] - a[i]) % P;
    return f;
  }
  Poly operator*(Poly a) {
    Poly f;
    FOR(i, 0, k)
    FOR(j, 0, k) if (i + j <= k) f[i + j] = (f[i + j] + 1ll * c[i] * a[j]) % P;
    return f;
  }
  Poly inv() {
    Poly f;
    f[0] = pw(c[0], P - 2);
    FOR(i, 1, k) {
      int s = 0;
      FOR(j, 0, i - 1) s = (s + 1ll * f[j] * c[i - j]) % P;
      f[i] = (P - s) * 1ll * f[0] % P;
    }
    return f;
  }
} w[N][N];

int a[N][N];
int fac[N], fnv[N];

Poly exp(int x) {
  Poly f;
  for (int i = 0, w = 1; i <= k; i++, w = w * 1ll * x % P) {
    f[i] = w * 1ll * fnv[i] % P;
  }
  return f;
}

void init() {
  fac[0] = 1;
  FOR(i, 1, k) fac[i] = fac[i - 1] * 1ll * i % P;
  fnv[k] = pw(fac[k], P - 2);
  ROF(i, k, 1) fnv[i - 1] = fnv[i] * 1ll * i % P;
}

Poly det(Poly w[N][N], const int m) {
  // printf("det m %lld\n",m);
  int sig = 1;
  FOR(i, 1, m) {
    FOR(j, i + 1, m) if (w[j][i][0]) {
      FOR(k, i, m) swap(w[i][k], w[j][k]);
      sig = P - sig;
      break;
    }
    assert(w[i][i][0]);
    Poly invAii = w[i][i].inv();
    FOR(j, i + 1, m) {
      Poly coef = w[j][i] * invAii;
      FOR(k, i, m) w[j][k] = w[j][k] - w[i][k] * coef;
    }
  }
  Poly f;
  f[0] = sig;
  FOR(i, 1, m) f = f * w[i][i];
  return f;
}

signed main() {
  scanf("%lld%lld", &n, &k);
  FOR(i, 1, n) FOR(j, 1, n) { scanf("%lld", &a[i][j]); }
  init();
  FOR(i, 1, n) FOR(j, 1, n) {
    if (i != j) {
      w[i][j] = w[i][j] - exp(a[i][j]);
      w[i][i] = w[i][i] + exp(a[i][j]);
    }
  }

  Poly f = det(w, n - 1);
  int ans = f[k] * fac[k] % P;
  printf("%lld\n", ans);

  return 0;
}
