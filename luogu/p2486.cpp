#include <bits/stdc++.h>
using namespace std;
const int N = 100004, LST = 1 << 18;

int n, m;
int a[N];
int t[LST], tg[LST], cl[LST], cr[LST];
void push_up(int rt) {
  t[rt] = t[rt << 1] + t[rt << 1 | 1];
  cl[rt] = cl[rt << 1], cr[rt] = cr[rt << 1 | 1];
  if (cr[rt << 1] == cl[rt << 1 | 1]) t[rt]--;
}
void push_down(int l, int r, int rt) {
  if (!tg[rt]) return;
  tg[rt << 1] = tg[rt << 1 | 1] = tg[rt];
  t[rt << 1] = t[rt << 1 | 1] = 1;
  cl[rt << 1] = cr[rt << 1] = tg[rt];
  cl[rt << 1 | 1] = cr[rt << 1 | 1] = tg[rt];
  tg[rt] = 0;
}
int lst_build(int l = 1, int r = n, int rt = 1) {
  if (l == r) return t[rt] = 1, cl[rt] = cr[rt] = a[l];
  int mid = (l + r) >> 1;
  lst_build(l, mid, rt << 1), lst_build(mid + 1, r, rt << 1 | 1);
  push_up(rt);
}
int lst_count(int L, int R, int l = 1, int r = n, int rt = 1) { //区间统计段数
  if (L <= l && r <= R) return t[rt];
  int mid = (l + r) >> 1, t1 = 0, t2 = 0;
  push_down(l, r, rt);
  if (L <= mid) t1 = lst_count(L, R, l, mid, rt << 1);
  if (mid < R) t2 = lst_count(L, R, mid + 1, r, rt << 1 | 1);
  return t1 + t2 - (t1 && t2 ? cr[rt << 1] == cl[rt << 1 | 1] : 0);
}
int lst_color(int p, int l = 1, int r = n, int rt = 1) { //单点查询颜色
  if (l == r && p == l) return cl[rt];
  int mid = (l + r) >> 1;
  push_down(l, r, rt);
  if (p <= mid)
    return lst_color(p, l, mid, rt << 1);
  else
    return lst_color(p, mid + 1, r, rt << 1 | 1);
}
int lst_upd(int L, int R, int v, int l = 1, int r = n, int rt = 1) { //区间修改颜色
  if (L <= l && r <= R) return t[rt] = 1, cl[rt] = cr[rt] = tg[rt] = v;
  int mid = (l + r) >> 1;
  push_down(l, r, rt);
  if (L <= mid) lst_upd(L, R, v, l, mid, rt << 1);
  if (mid < R) lst_upd(L, R, v, mid + 1, r, rt << 1 | 1);
  push_up(rt);
}

struct qxx {
  int nex, t;
}; //链式前向星，临时存图
qxx e[N * 2];
int head[N], cnt, vis[N];
void add_path(int f, int t) {
  e[++cnt] = (qxx){head[f], t};
  head[f] = cnt;
}

int dfn; // dfn序
struct node {
  int p, s, b, v;
  int dp, tp, sz, hvs, idx;
  // deep,top,size,heavy_son,index
};
node tr[N]; //树结构
void add_son(int p, int s) { tr[s].p = p, tr[s].b = tr[p].s, tr[p].s = s; }

int szdfs(int rt, int dp) {
  tr[rt].dp = dp, tr[rt].sz = 1, vis[rt] = true;
  for (int i = head[rt]; i; i = e[i].nex)
    if (!vis[e[i].t]) {
      add_son(rt, e[i].t);
      tr[rt].sz += szdfs(e[i].t, dp + 1);
    }
  return tr[rt].sz;
}
void dedfs(int rt, int tp) {
  tr[rt].tp = tp, tr[rt].idx = ++dfn, a[dfn] = tr[rt].v;
  if (!tr[rt].s) return;
  for (int mx = 0, i = tr[rt].s; i; i = tr[i].b)
    if (mx < tr[i].sz) mx = tr[i].sz, tr[rt].hvs = i;
  dedfs(tr[rt].hvs, tp);
  for (int i = tr[rt].s; i; i = tr[i].b)
    if (i != tr[rt].hvs) dedfs(i, i);
}
void tree_upd(int x, int y, int v) { //树上更新
  while (tr[x].tp != tr[y].tp) {
    if (tr[tr[x].tp].dp < tr[tr[y].tp].dp) x ^= y ^= x ^= y;
    lst_upd(tr[tr[x].tp].idx, tr[x].idx, v);
    x = tr[tr[x].tp].p;
  }
  if (tr[x].idx > tr[y].idx) x ^= y ^= x ^= y;
  lst_upd(tr[x].idx, tr[y].idx, v);
}
int tree_query(int x, int y) { //树上查询
  int res = 0;
  while (tr[x].tp != tr[y].tp) {
    if (tr[tr[x].tp].dp < tr[tr[y].tp].dp) x ^= y ^= x ^= y;
    res += lst_count(tr[tr[x].tp].idx, tr[x].idx);
    int c1 = lst_color(tr[tr[x].tp].idx);
    int c2 = lst_color(tr[tr[tr[x].tp].p].idx);
    if (c1 == c2) res--;
    x = tr[tr[x].tp].p;
  }
  if (tr[x].idx > tr[y].idx) x ^= y ^= x ^= y;
  return res + lst_count(tr[x].idx, tr[y].idx);
}

int main() {
  scanf("%d%d", &n, &m);
  for (int i = 1; i <= n; i++) scanf("%d", &tr[i].v);
  for (int i = 1, x, y; i < n; i++) {
    scanf("%d%d", &x, &y);
    add_path(x, y);
    add_path(y, x);
  }
  szdfs(1, 1);
  dedfs(1, 1);
  lst_build();
  for (int i = 1, a, b, c; i <= m; i++) {
    char op[5];
    scanf("%s%d%d", op, &a, &b);
    if (op[0] == 'C') {
      scanf("%d", &c);
      tree_upd(a, b, c);
    } else {
      printf("%d\n", tree_query(a, b));
    }
  }
  return 0;
}