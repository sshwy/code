#include <iostream>

#define SIZE 500

using namespace std;

int n;

struct vint {
  int num[SIZE] = {0}, len = 0;

  vint operator+(const vint add) const {
    vint sum;
    int x = 0;
    for (sum.len = 1; sum.len <= len || sum.len <= add.len; sum.len++) {
      sum.num[sum.len] = num[sum.len] + add.num[sum.len] + x;
      x = sum.num[sum.len] / 10;
      sum.num[sum.len] %= 10;
    }
    if (x != 0)
      sum.num[sum.len] = x;
    else
      sum.len--;
    return sum;
  }
  vint operator+(int v) const {
    vint sum;
    int x = 0;
    for (sum.len = 1; sum.len <= len || v != 0; sum.len++, v /= 10) {
      sum.num[sum.len] = num[sum.len] + v % 10 + x;
      x = sum.num[sum.len] / 10;
      sum.num[sum.len] %= 10;
    }
    if (x != 0)
      sum.num[sum.len] = x;
    else
      sum.len--;
    return sum;
  }
  vint operator*(const vint mul) const {
    vint res;
    int x = 0;
    for (int i = 1; i <= mul.len; i++) {
      for (int j = 1; j <= len; j++) {
        res.num[i + j - 1] += mul.num[i] * num[j] + x;
        x = res.num[i + j - 1] / 10;
        res.num[i + j - 1] %= 10;
      }
      if (x != 0) res.num[i + len] += x;
      x = 0;
    }
    if (res.num[mul.len + len] != 0)
      res.len = mul.len + len;
    else
      res.len = mul.len + len - 1;
    return res;
  }
  vint operator=(int v) {
    for (int i = 0; i <= len; i++) num[i] = 0;
    len = 0;
    while (v != 0) {
      num[++len] = v % 10;
      v /= 10;
    }
    return *this;
  }
  void put() {
    for (int i = len; i >= 1; i--) putchar((char)(num[i] + 48));
  }
};

vint c[501];

int main() {
  cin >> n;
  c[0] = c[1] = 1;
  for (int i = 2; i <= n; i++) {
    for (int j = 0; j < i; j++) { c[i] = c[i] + c[j] * c[i - j - 1]; }
  }
  c[n].put();
}

/*

*/
