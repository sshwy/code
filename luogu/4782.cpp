#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i) /*}}}*/
/******************heading******************/
const int M = 2e6 + 5;
int n, m;

struct qxx {
  int nex, t;
};
qxx e[M];
int h[M], le;
void add_path(int f, int t) { e[++le] = (qxx){h[f], t}, h[f] = le; }

int dfn[M], low[M], s[M], ls, dfncnt;
int col[M], lc;
bool vis[M];
void dfs(int u) {
  dfn[u] = low[u] = ++dfncnt;
  vis[u] = 1, s[++ls] = u;
  for (int i = h[u]; i; i = e[i].nex) {
    const int v = e[i].t;
    if (!dfn[v]) {
      dfs(v);
      low[u] = min(low[u], low[v]);
    } else if (vis[v]) {
      low[u] = min(low[u], dfn[v]);
    }
  }
  if (low[u] == dfn[u]) {
    ++lc;
    while (s[ls] != u) col[s[ls]] = lc, vis[s[ls]] = 0, --ls;
    col[s[ls]] = lc, vis[s[ls]] = 0, --ls;
  }
}

int main() {
  scanf("%d%d", &n, &m);
  FOR(i, 1, m) {
    int x, a, b, y;
    scanf("%d%d%d%d", &x, &a, &y, &b);
    add_path(x * 2 + !a, y * 2 + b);
    add_path(y * 2 + !b, x * 2 + a);
  }
  FOR(i, 1, n * 2 + 1) if (!dfn[i]) dfs(i);

  return 0;
}
