#include <algorithm>
#include <iostream>

using namespace std;

int n, c[400001], tot;

struct p {
  int v, n;
} num[400001];

bool cmp_v(p x, p y) { return x.v < y.v; }
bool cmp_n(p x, p y) { return x.n < y.n; }

void update(int p) {
  for (int i = p; i <= n; i += i & (-i)) c[i]++;
}
int presum(int p) {
  int ps = 0;
  for (int i = p; i > 0; i -= i & (-i)) ps += c[i];
  return ps;
}

int main() {
  cin >> n;
  for (int i = 1; i <= n; i++) {
    cin >> num[i].v;
    num[i].n = i;
  }

  sort(num + 1, num + 1 + n, cmp_v);
  for (int i = 1, dif = 0; i <= n; i++) {
    if (num[i - 1].v == num[i].v) dif++;
    num[i].v = i - dif;
  }
  sort(num + 1, num + 1 + n, cmp_n);

  for (int i = n; i >= 1; i--) {
    tot += presum(num[i].v - 1);
    update(num[i].v);
  }
  cout << tot;
  return 0;
}
