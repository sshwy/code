#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <climits>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <unordered_map>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
typedef long long LL;
const int SZ = 1e7 + 5;
int LIM = 1e7;

bool bp[SZ];
int pn[SZ], lp;
int h[SZ];
void sieve() {
  bp[0] = bp[1] = 1;
  FOR(i, 2, LIM) {
    if (!bp[i]) pn[++lp] = i;
    FOR(j, 1, lp) {
      int pj = pn[j];
      if (i * pj > LIM) break;
      bp[i * pj] = 1;
      if (i % pj == 0) break;
    }
  }
}

void dfs(int cur = 0, LL prd = 1, int cnt = 0) {
  if (prd > 1e7) return;
  h[prd] = (2 * (cnt & 1) - 1) * cnt;
  FOR(i, cur + 1, lp) {
    if (prd * pn[i] > 1e7) break;
    dfs(i, prd * pn[i], cnt + 1);
  }
}
void dfs2(int cur, LL prd, int cnt, int tag) { // tag=x:没到；tag=-1:到了且选了
  if (prd > 1e7) return;
  if (tag == -1) h[prd] = 2 * (cnt & 1) - 1;
  FOR(i, cur + 1, lp) {
    if (prd * pn[i] > 1e7) break;
    if (tag == -1) {
      dfs2(i, prd * pn[i], cnt + 1, tag);
    } else if (i == tag) {
      dfs2(i, prd * pn[i], cnt + 1, -1);
    } else {
      dfs2(i, prd * pn[i], cnt + 1, tag);
    }
  }
}
void go() {
  LL n, m;
  cin >> n >> m;
  LL ans = 0;
  LL l = 1, r, L = min(n, m);
  while (l <= L) {
    r = min(n / (n / l), m / (m / l));
    ans += (n / l) * (m / l) * (h[r] - h[l - 1]);
    l = r + 1;
  }
  cout << ans << endl;
}
int main() {
  ios::sync_with_stdio(0);
  cin.tie(0);
  sieve();
  dfs(0, 1, 0);
  FOR(i, 1, lp) if (1ll * pn[i] * pn[i] < 1e7) dfs2(0, pn[i], 1, i);
  FOR(i, 2, LIM) h[i] += h[i - 1];
  int t;
  cin >> t;
  FOR(i, 1, t) go();
  return 0;
}
