#include <iostream>

#define INF 9999999

using namespace std;

int n, m, a, b, c, mx;
int w[21][21];
int d[21];
bool vis[21];

int dfs(int now, int dis, int start) {
  if (d[start] < dis) d[start] = dis;
  vis[now] = true;
  for (int i = 1; i <= n; i++)
    if (!vis[i] && w[now][i] != INF) dfs(i, dis + w[now][i], start);
  vis[now] = false;
}

int main() {
  cin >> n >> m;
  for (int i = 1; i <= n; i++) {
    for (int j = 1; j <= n; j++) w[i][j] = INF;
    w[i][i] = 0;
  }
  for (int i = 1; i <= m; i++) {
    cin >> a >> b >> c;
    w[a][b] = w[b][a] = c;
  }
  for (int i = 1; i <= n; i++) dfs(i, 0, i);
  for (int i = 1; i <= n; i++)
    if (d[i] > mx) mx = d[i];
  cout << mx;
  return 0;
}
