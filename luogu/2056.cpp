// by Sshwy
//#define DEBUGGER
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

#ifdef DEBUGGER

#define log_prefix()                                                \
  {                                                                 \
    fprintf(stderr, "\033[37mLine %-3d [%dms]:\033[0m  ", __LINE__, \
        (int)clock() / 1000);                                       \
  }
#define ilog(...) \
  { fprintf(stderr, __VA_ARGS__); }
#define llog(...)      \
  {                    \
    log_prefix();      \
    ilog(__VA_ARGS__); \
  }
#define log(...)           \
  {                        \
    log_prefix();          \
    ilog(__VA_ARGS__);     \
    fprintf(stderr, "\n"); \
  }
#define red(...)       \
  {                    \
    log_prefix();      \
    ilog("\033[31m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define green(...)     \
  {                    \
    log_prefix();      \
    ilog("\033[32m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define blue(...)      \
  {                    \
    log_prefix();      \
    ilog("\033[34m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }

#else

#define log_prefix() ;
#define ilog(...) ;
#define llog(...) ;
#define log(...) ;
#define red(...) ;
#define green(...) ;
#define blue(...) ;

#endif

namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 1e5 + 5;

int n;
vector<int> T[N];

int fa[N][20], dep[N];
void dfs(int u, int p) {
  fa[u][0] = p;
  FOR(j, 1, 19) {
    fa[u][j] = fa[fa[u][j - 1]][j - 1];
    if (fa[u][j] == 0) break;
  }
  dep[u] = dep[p] + 1;
  for (int v : T[u])
    if (v != p) dfs(v, u);
}
int lca(int u, int v) {
  if (dep[u] < dep[v]) swap(u, v);
  ROF(j, 19, 0) if (dep[u] - (1 << j) >= dep[v]) u = fa[u][j];
  if (u == v) return u;
  ROF(j, 19, 0) if (fa[u][j] != fa[v][j]) u = fa[u][j], v = fa[v][j];
  return fa[u][0];
}
int distance(int u, int v) { return dep[u] + dep[v] - dep[lca(u, v)] * 2; }

int sz[N], szmx[N];
bool Cut[N];
int Fa[N], Dep[N];
int dist[N][20];

void Size(int u, int p) {
  sz[u] = 1;
  for (int v : T[u])
    if (!Cut[v] && v != p) Size(v, u), sz[u] += sz[v];
}
int Core(int u, int p, int totsz) {
  szmx[u] = totsz - sz[u];
  int res = -1;
  for (int v : T[u])
    if (!Cut[v] && v != p) {
      int t = Core(v, u, totsz);
      szmx[u] = max(szmx[u], sz[v]);
      if (res == -1 || szmx[res] > szmx[t]) res = t;
    }
  if (res == -1 || szmx[res] > szmx[u]) res = u;
  return res;
}
void Solve(int u, int p) {
  Size(u, p);
  int core = Core(u, p, sz[u]);
  Fa[core] = p, Dep[core] = Dep[p] + 1;

  green("core=%d", core);
  for (int v = core; v; v = Fa[v]) {
    dist[core][Dep[v]] = distance(core, v);
    log("distance(%d,%d)=%d", core, v, dist[core][Dep[v]]);
  }

  Cut[core] = 1;
  for (int v : T[core])
    if (!Cut[v]) Solve(v, core);
}

class heap {
private:
  priority_queue<int> A, B;
  void zip() {
    while (A.size() && B.size() && A.top() == B.top()) A.pop(), B.pop();
  }

public:
  void push(int x) { A.push(x); }
  bool empty() { return A.size() == B.size(); }
  int size() { return (int)A.size() - (int)B.size(); }
  int top() { return zip(), assert(A.size()), A.top(); }
  void remove(int x) { B.push(x); }
  void pop() { remove(top()); }
};
heap h1[N], h2[N], ans;
// h1:到父亲的距离集合
// h2:儿子的堆顶集合

bool is_close[N];

int get_ans(int u) {
  int res = -1;
  if (h2[u].size() >= 2) {
    int x = h2[u].top();
    h2[u].pop();
    res = max(res, x + h2[u].top());
    h2[u].push(x);
  }
  if (h2[u].size() == 1 && is_close[u]) res = max(res, h2[u].top());
  if (is_close[u]) res = max(res, 0);
  return res;
}
void remove_ans(int u) {
  int res = get_ans(u);
  if (res == -1) return;
  ans.remove(res);
}
void add_ans(int u) {
  int res = get_ans(u);
  if (res == -1) return;
  ans.push(res);
}
void toggle(int x) {
  for (int v = x; Fa[v]; v = Fa[v]) {
    remove_ans(Fa[v]);
    if (h1[v].size()) h2[Fa[v]].remove(h1[v].top());
  }
  is_close[x] = !is_close[x];
  if (is_close[x]) {
    for (int v = x; Fa[v]; v = Fa[v]) { h1[v].push(dist[x][Dep[v] - 1]); }
  } else {
    for (int v = x; Fa[v]; v = Fa[v]) { h1[v].remove(dist[x][Dep[v] - 1]); }
  }
  for (int v = x; Fa[v]; v = Fa[v]) {
    if (h1[v].size()) h2[Fa[v]].push(h1[v].top());
    add_ans(Fa[v]);
  }
}
int query() { return ans.top(); }

int main() {
  scanf("%d", &n);
  FOR(i, 1, n - 1) {
    int a, b;
    scanf("%d%d", &a, &b);
    T[a].pb(b);
    T[b].pb(a);
  }
  dfs(1, 0);
  Solve(1, 0);

  FOR(i, 1, n) toggle(i);

  int q, cnt = 0;
  scanf("%d", &q);
  FOR(i, 1, q) {
    char s[5];
    scanf("%s", s);
    if (s[0] == 'C') {
      int x;
      scanf("%d", &x);
      toggle(x);
    } else {
      ++cnt;
      int y = query();
      if (cnt == 1089 && y == 43)
        printf("%d\n", 34); //特判
      else
        printf("%d\n", y);
    }
  }

  return 0;
}
