#include <cstdio>
#include <iostream>
#define max(a, b) (a > b ? a : b)

using namespace std;

int n, m, w1, c1, p1;
int w[60][60], c[60][60], f[3200];
// w[i][j]:第i组物品中的第j个物品的重量；w[i][0]:第i组物品的个数
// c[i][j]:第i组物品中的第j个物品的价值；c[i][0]:第i组物品的个数
// f[j]:总钱数为j时的最大乘积和
int main() {
  cin >> n >> m;
  n /= 10;
  for (int i = 1; i <= m; i++) {
    cin >> w1 >> c1 >> p1;
    if (p1 == 0) p1 = i;
    c[p1][++c[p1][0]] = c1 * w1;
    w[p1][++w[p1][0]] = w1 / 10;
  }
  for (int i = 1; i <= m; i++) //前i组
  {
    for (int k = n; k >= 1; k--) {
      if (w[i][0] != 0) {
        if (k - w[i][1] >= 0)
          f[k] = max(f[k], f[k - w[i][1]] + c[i][1]); //不取&只取主件
        if (w[i][0] > 1 && k - w[i][1] - w[i][2] >= 0)
          f[k] = max(f[k],
              f[k - w[i][1] - w[i][2]] + c[i][1] + c[i][2]); //&取主件+附件1
        if (w[i][0] > 2) {
          if (k - w[i][1] - w[i][3] >= 0)
            f[k] = max(f[k],
                f[k - w[i][1] - w[i][3]] + c[i][1] + c[i][3]); //&取主件+附件2
          if (k - w[i][1] - w[i][2] - w[i][3] >= 0)
            f[k] = max(f[k],
                f[k - w[i][1] - w[i][2] - w[i][3]] + c[i][1] + c[i][2] + c[i][3]);
        }
      }
    }
  }
  cout << f[n];
  return 0;
}
