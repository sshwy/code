#include <bits/stdc++.h>
using namespace std;
const int N = 100005;
int n, m;

int id[N], od[N];
struct qxx {
  int nex, t, v;
};
qxx e[2 * N];
int head[N], cnt;
void add_path(int f, int t, int v) {
  e[++cnt] = (qxx){head[f], t, v};
  head[f] = cnt;
}
queue<int> q;
double p[N], ans;

int main() {
  scanf("%d%d", &n, &m);
  for (int i = 1, a, b, c; i <= m; i++) {
    scanf("%d%d%d", &a, &b, &c);
    add_path(a, b, c);
    ++od[a], ++id[b];
  }
  for (int i = 1; i <= n; i++) {
    p[i] = 0;
    if (!id[i]) q.push(i), p[i] = 1;
  }
  while (!q.empty()) {
    int k = q.front();
    q.pop();
    double pk = p[k] / od[k];
    for (int i = head[k]; i; i = e[i].nex) {
      --id[e[i].t];
      p[e[i].t] += pk;    //到达这个点的概率
      ans += e[i].v * pk; //路径期望累加
      if (id[e[i].t] == 0) q.push(e[i].t);
    }
  }
  printf("%.2lf", ans);
  return 0;
}