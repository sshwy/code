#include <iostream>

#define SIZE 9999

using namespace std;

int n;

struct vint {
  int num[SIZE] = {0}, len = 0;

  vint operator+(vint add) {
    vint sum;
    int x = 0;
    for (sum.len = 1; sum.len <= len || sum.len <= add.len; sum.len++) {
      sum.num[sum.len] = num[sum.len] + add.num[sum.len] + x;
      x = sum.num[sum.len] / n;
      sum.num[sum.len] %= n;
    }
    if (x != 0)
      sum.num[sum.len] = x;
    else
      sum.len--;
    return sum;
  }
  vint operator+(int v) {
    vint sum;
    int x = 0;
    for (sum.len = 1; sum.len <= len || v != 0; sum.len++, v /= 10) {
      sum.num[sum.len] = num[sum.len] + v % 10 + x;
      x = sum.num[sum.len] / 10;
      sum.num[sum.len] %= 10;
    }
    if (x != 0)
      sum.num[sum.len] = x;
    else
      sum.len--;
    return sum;
  }

  vint operator=(int v) {
    for (int i = 0; i <= len; i++) num[i] = 0;
    len = 0;
    while (v != 0) {
      num[++len] = v % 10;
      v /= 10;
    }
    return *this;
  }
  void get() {
    for (int i = 1; i <= len; i++) num[i] = 0;
    len = 0;
    char c = 0;
    while (c = getchar())
      if ('0' <= c && c <= '9' || 'A' <= c && c <= 'F') break;
    do {
      if ('0' <= c && c <= '9')
        num[++len] = c - 48;
      else if ('A' <= c && c <= 'F')
        num[++len] = c - 55;
      else
        break;
    } while ((c = getchar()) != EOF);
    for (int i = 1, j = len; i < j; i++, j--) num[i] ^= num[j] ^= num[i] ^= num[j];
  }
  void put() {
    for (int i = len; i >= 1; i--) putchar((char)(num[i] + 48));
  }
  bool operator>(vint sec) {
    if (len > sec.len)
      return true;
    else if (len < sec.len)
      return false;
    for (int i = len; i >= 1; i--) {
      if (num[i] > sec.num[i])
        return true;
      else if (num[i] < sec.num[i])
        return false;
    }
    return false;
  }
  bool huiwen() {
    for (int i = 1, j = len; i < j; i++, j--)
      if (num[i] != num[j]) return false;
    return true;
  }
  vint reverse() {
    vint ret;
    for (int i = 1; i <= len; i++) ret.num[len - i + 1] = num[i];
    ret.len = len;
    return ret;
  }
};

vint m;

int main() {
  cin >> n;
  m.get();
  for (int ans = 0; ans <= 30; ans++) {
    if (m.huiwen()) {
      cout << "STEP=" << ans;
      return 0;
    }
    m = m + m.reverse();
  }
  cout << "Impossible!";
  return 0;
}
