#include <bits/stdc++.h>
using namespace std;
int n, tot, k = -1;
vector<int> dpc[201];             //有向边，邻接表
int dfn[201], low[201], vis[201]; // tarjan三件套
int s[201], tp;                   //栈，记录SCC
int id[201], scc[201], snt;       //每个点的入度id[i]，每个SCC的代表元scc[i]
struct disjoint {                 //并查集
  int f[50000];
  int gf(int k) { return f[k] == k ? k : f[k] = gf(f[k]); }
  bool find(int a, int b) { return gf(a) == gf(b); }
  void un(int a, int b) { f[gf(b)] = gf(a); }
  void clear(int k) {
    for (int i = 0; i <= k; i++) f[i] = i;
  }
};
disjoint d;
void tarjan(int u) {
  vis[u] = true, dfn[u] = low[u] = ++k;
  s[++tp] = u;
  for (int i = 0; i < dpc[u].size(); i++) {
    int v = dpc[u][i];
    if (!dfn[v]) {
      tarjan(v);
      low[u] = min(low[u], low[v]);
    } else if (vis[v])
      low[u] = min(low[u], dfn[v]); // dfn和low都可以
  }
  int j;
  if (dfn[u] == low[u]) {
    scc[++snt] = u;
    do {
      j = s[tp];
      d.un(u, j); //加入代表元所在并查集
      vis[s[tp]] = 0, tp--;
    } while (j != u);
  }
}
int main() {
  scanf("%d", &n);
  d.clear(n);
  for (int i = 1, a; i <= n; i++) {
    for (scanf("%d", &a); a; scanf("%d", &a)) dpc[i].push_back(a);
  }
  for (int i = 1; i <= n; i++) dpc[0].push_back(i); //建立虚结点0，使图连通
  tarjan(0);
  for (int i = 1; i <= n; i++) {
    for (int j = 0; j < dpc[i].size(); j++) { // i,dpc[i][j]
      if (d.find(i, dpc[i][j]) == 0)          //不是同一结点
        id[d.gf(dpc[i][j])]++;
    }
  } // disjoint suodian
  for (int i = 1; i <= snt; i++) {
    if (id[scc[i]] == 0) tot++;
  }
  printf("%d", tot - 1); //除了虚结点0以外
  return 0;
}
