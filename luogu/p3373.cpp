#include <bits/stdc++.h>
#define SIZE 262150
using namespace std;
typedef long long ll;
ll n, m, p;
ll a[100001];
ll s[SIZE], add[SIZE], mul[SIZE];
// tag_add&tag_mul
void push_down(ll l, ll r, ll rt) {
  // printf("push_down(%lld,%lld,%lld)\n",l,r,rt);
  ll mid = (l + r) >> 1;

  s[rt << 1] = (s[rt << 1] * mul[rt] % p + add[rt] * (mid - l + 1) % p) % p;
  add[rt << 1] = (add[rt << 1] * mul[rt] % p + add[rt]) % p;
  mul[rt << 1] = mul[rt << 1] * mul[rt] % p;

  s[rt << 1 | 1] = (s[rt << 1 | 1] * mul[rt] % p + add[rt] * (r - mid) % p) % p;
  add[rt << 1 | 1] = (add[rt << 1 | 1] * mul[rt] % p + add[rt]) % p;
  mul[rt << 1 | 1] = mul[rt << 1 | 1] * mul[rt] % p;

  add[rt] = 0, mul[rt] = 1;
}
void push_up(ll rt) { s[rt] = (s[rt << 1] + s[rt << 1 | 1]) % p; }
ll lst_build(ll l, ll r, ll rt) {
  // printf("lst_build(%lld,%lld,%lld)\n",l,r,rt);
  mul[rt] = 1;
  if (l == r) {
    // printf("s[%lld]:%lld\n",rt,a[l]);
    return s[rt] = a[l];
  }
  ll mid = (l + r) >> 1;
  lst_build(l, mid, rt << 1), lst_build(mid + 1, r, rt << 1 | 1);
  push_up(rt);
  // printf("s[%lld]:%lld\n",rt,s[rt]);
}
ll lst_add(ll L, ll R, ll l, ll r, ll rt, ll v) {
  // printf("lst_add(%lld,%lld,%lld,%lld,%lld,%lld)\n",L,R,l,r,rt,v);
  if (R < l || r < L)
    return 0;
  else if (L <= l && r <= R) {
    // printf("s[%lld]:%lld,v:%lld\n",rt,s[rt],v);
    return add[rt] = (add[rt] + v) % p, s[rt] = (s[rt] + v * (r - l + 1) % p) % p;
  }
  ll mid = (l + r) >> 1;
  push_down(l, r, rt);
  lst_add(L, R, l, mid, rt << 1, v), lst_add(L, R, mid + 1, r, rt << 1 | 1, v);
  push_up(rt);
}
ll lst_mul(ll L, ll R, ll l, ll r, ll rt, ll v) {
  // printf("lst_mul(%lld,%lld,%lld,%lld,%lld,%lld)\n",L,R,l,r,rt,v);
  if (R < l || r < L)
    return 0;
  else if (L <= l && r <= R)
    return mul[rt] = mul[rt] * v % p, add[rt] = add[rt] * v % p,
           s[rt] = s[rt] * v % p;
  ll mid = (l + r) >> 1;
  push_down(l, r, rt);
  lst_mul(L, R, l, mid, rt << 1, v), lst_mul(L, R, mid + 1, r, rt << 1 | 1, v);
  push_up(rt);
}
ll lst_sum(ll L, ll R, ll l, ll r, ll rt) {
  // printf("lst_sum(%lld,%lld,%lld,%lld,%lld,%lld)\n",L,R,l,r,rt);
  if (R < l || r < L)
    return 0;
  else if (L <= l && r <= R) {
    // printf("s[%lld]:%lld\n",rt,s[rt]);
    return s[rt];
  }
  ll mid = (l + r) >> 1;
  push_down(l, r, rt);
  return (lst_sum(L, R, l, mid, rt << 1) + lst_sum(L, R, mid + 1, r, rt << 1 | 1)) %
         p;
}
int main() {
  scanf("%lld%lld%lld", &n, &m, &p);
  for (ll i = 1; i <= n; i++) scanf("%lld", &a[i]);
  lst_build(1, n, 1);
  for (ll i = 1, a, x, y, k; i <= m; i++) {
    scanf("%lld%lld%lld", &a, &x, &y);
    if (a == 1) {
      scanf("%lld", &k);
      lst_mul(x, y, 1, n, 1, k);
    } else if (a == 2) {
      scanf("%lld", &k);
      lst_add(x, y, 1, n, 1, k);
    } else {
      printf("%lld\n", lst_sum(x, y, 1, n, 1));
    }
  }
  return 0;
}
