#include <cstdio>
using namespace std;
const int N = 1e4 + 4;
int n;
double f[N], g[N];
int main() {
  scanf("%d", &n);
  for (int i = n - 1; i >= 0; i--) {
    f[i] = f[i + 1] + 1.0 * n / (n - i);
    g[i] = 1.0 * i / (n - i) * f[i] + g[i + 1] + f[i + 1] + 1.0 * n / (n - i);
  }
  printf("%.2lf\n", g[0]);
  return 0;
}
