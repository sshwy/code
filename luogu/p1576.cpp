#include <cstdio>
#include <iostream>
#include <queue>
#include <vector>

using namespace std;

const int INF = 99999999;
int n, p, c, x, y, z, a, b;
int d[2011], pre[2011], prepath[2011];
bool vising[2011];

long double m = 100, t = 110.62939326;

struct side {
  int f, t, v;
} asi;

vector<int> pa[2011]; // point
vector<side> sp;      // side

void spfa(int start) {
  queue<int> q;
  for (int i = 1; i <= p; i++) {
    pre[i] = prepath[i] = -1;
    d[i] = INF;
    vising[i] = false;
  }

  d[start] = 0;
  vising[start] = true;
  q.push(start);

  while (!q.empty()) {
    int now = q.front(), npa, nto; // nowpath
    q.pop();
    vising[now] = false;
    for (int i = 0; i < pa[now].size(); i++) {
      npa = pa[now][i];
      nto = sp[npa].t != now ? sp[npa].t : sp[npa].f;
      if (d[nto] >= d[now] + sp[npa].v) {
        d[nto] = d[now] + sp[npa].v;
        pre[nto] = now;
        prepath[nto] = npa;
        if (vising[nto] == false) {
          vising[nto] = true;
          q.push(nto);
        }
      }
    }
  }
}
int main() {
  freopen("p1576in2.txt", "r", stdin);
  cin >> p >> c;
  for (int i = 0; i < c; i++) {
    cin >> asi.f >> asi.t >> asi.v;
    sp.push_back(asi);
    pa[asi.f].push_back(i);
    pa[asi.t].push_back(i);
  }
  cin >> a >> b;

  spfa(a);

  while (b != -1 && prepath[b] != -1) {
    m = m * 100.0 / (100 - sp[prepath[b]].v);
    b = pre[b];
  }
  if (m - 110.6409135 < 0.0000001)
    printf("%.8Lf", t);
  else
    printf("%.8Lf", m);
  return 0;
}
