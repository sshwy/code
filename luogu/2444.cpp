#include <cstdio>
#include <cstdlib>
#include <queue>
using namespace std;
const int L = 3e4 + 5;

int tot;
int tr[L][2], e[L], fail[L];
void insert(char *s) {
  int u = 0;
  for (int i = 1; s[i]; i++) {
    if (!tr[u][s[i] - '0']) tr[u][s[i] - '0'] = ++tot;
    u = tr[u][s[i] - '0'];
  }
  e[u] = 1;
}
queue<int> q;
void build() {
  for (int i = 0; i <= 1; i++)
    if (tr[0][i]) q.push(tr[0][i]);
  while (!q.empty()) {
    int u = q.front();
    q.pop();
    for (int i = 0; i <= 1; i++) {
      int &v = tr[u][i];
      if (!v)
        v = tr[fail[u]][i];
      else
        fail[v] = tr[fail[u]][i], e[v] |= e[fail[v]], q.push(v);
    }
  }
}
bool vis[L], cur[L];
void dfs(int u) {
  // printf("dfs(%d),tim:%d\n",u,vis[u]);
  if (cur[u]) puts("TAK"), exit(0);
  if (vis[u] || e[u]) return;
  vis[u] = cur[u] = 1;
  dfs(tr[u][0]), dfs(tr[u][1]);
  cur[u] = 0;
}

int n;
char s[2005];
int main() {
  scanf("%d", &n);
  for (int i = 1; i <= n; i++) {
    scanf("%s", s + 1);
    insert(s);
  }
  build();
  dfs(0);
  puts("NIE");
  return 0;
}
/*
 * BUG#1: 在build的时侯没有考虑匹配点状态的转移L25
 */
