#include <bits/stdc++.h>
using namespace std;
const int N = 500001;
int n, m, s;
int q[N];
bool vis[N], tarjan_vis[N];

struct qxx {
  int nex, t;
};
qxx e[N * 2];
int head[N], cnt;
void add_path(int f, int t) {
  e[++cnt] = {head[f], t};
  head[f] = cnt;
}

struct que_qxx {
  int nex, t, v;
};
que_qxx eq[N * 2];
int hq[N], qnt;
void add_ques(int f, int t, int v) {
  eq[++qnt] = {hq[f], t, v};
  hq[f] = qnt;
}

int fa[N];
void init(int k) {
  for (int i = 0; i <= k; i++) fa[i] = i;
}
int gf(int k) { return fa[k] == k ? k : fa[k] = gf(fa[k]); }
void un(int p, int s) { fa[gf(s)] = p; }
bool find(int a, int b) { return gf(a) == gf(b); }

void tarjan(int k) {
  vis[k] = true;
  for (int i = head[k]; i; i = e[i].nex) {
    if (!vis[e[i].t]) {
      tarjan(e[i].t);
      un(k, e[i].t);
      tarjan_vis[e[i].t] = true;
    }
  }
  for (int i = hq[k]; i; i = eq[i].nex) {
    if (tarjan_vis[eq[i].t]) q[eq[i].v] = gf(e[i].t);
  }
}

int main() {
  scanf("%d%d%d", &n, &m, &s);
  for (int i = 1, x, y; i < n; i++) {
    scanf("%d%d", &x, &y);
    add_path(x, y);
    add_path(y, x);
  }
  for (int i = 1, x, y; i <= m; i++) {
    scanf("%d%d", &x, &y);
    add_ques(x, y, i);
    add_ques(y, x, i);
  }
  init(n);
  tarjan(s);
  for (int i = 1; i <= n; i++) printf("%d\n", q[i]);
  return 0;
}