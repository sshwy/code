#include <iostream>

using namespace std;

char c, pre = '*', first;
int count[2];

int main() {
  while (cin >> c) {
    if (pre == '*') first = c;
    if (c != pre) count[c - 48]++, pre = c;
  }
  if (first == '1')
    cout << count[0] * 2;
  else
    cout << count[0] * 2 - 1;
  return 0;
}
