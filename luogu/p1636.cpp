#include <iostream>

using namespace std;

int n, m, a, b, t;
int d[1001];

int main() {
  cin >> n >> m;
  for (int i = 1; i <= m; i++) {
    cin >> a >> b;
    d[a]++;
    d[b]++;
  }
  for (int i = 1; i <= n; i++)
    if (d[i] % 2 == 1) t++;
  if (t == 0)
    cout << 1;
  else
    cout << t / 2;
  return 0;
}
