#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 3e5 + 5;

struct qxx {
  int nex, t;
};
qxx e[N * 2];
int h[N], le = 1;
void add_path(int f, int t) { e[++le] = (qxx){h[f], t}, h[f] = le; }
#define FORe(i, u, v) for (int i = h[u], v; v = e[i].t, i; i = e[i].nex)

int n, q;

long long BIN[N], lb;
long long *f[N];

long long ans[N];
int hgh[N], dep[N], sz[N];
vector<pair<int, int>> qry[N];

void dfs1(int u, int p) {
  hgh[u] = 1, dep[u] = dep[p] + 1, sz[u] = 1;
  FORe(i, u, v) if (v != p) dfs1(v, u), sz[u] += sz[v],
      hgh[u] = max(hgh[u], hgh[v] + 1);
}
void dfs2(int u, int p) {
  ++lb;
  int mx = 0, son = -1;
  FORe(i, u, v) if (v != p && hgh[v] > mx) mx = hgh[v], son = v;
  if (son == -1) {
    f[u] = BIN + lb;
    f[u][0] = sz[u] - 1;
    return;
  }
  dfs2(son, u);
  FORe(i, u, v) if (v != p && v != son) dfs2(v, u);

  f[u] = f[son] - 1;
  f[u][0] = sz[u] - 1 + f[u][1]; //后缀和
  assert(hgh[u] == hgh[son] + 1);
  FORe(i, u, v) {
    if (v == p || v == son) continue;
    FOR(i, 0, hgh[v] - 1) { f[u][i + 1] += f[v][i]; }
    f[u][0] += f[v][0]; //后缀和
  }
  //处理询问
  for (pii p : qry[u]) {
    int id = p.fi, k = p.se;
    ans[id] += 1ll * min(dep[u] - 1, k) * (sz[u] - 1);
    ans[id] += f[u][1] - (k + 1 >= hgh[u] ? 0 : f[u][k + 1]);
  }
}

int main() {
  scanf("%d%d", &n, &q);
  FOR(i, 1, n - 1) {
    int x, y;
    scanf("%d%d", &x, &y);
    add_path(x, y), add_path(y, x);
  }
  FOR(i, 1, q) {
    int p, k;
    scanf("%d%d", &p, &k);
    qry[p].pb({i, k});
  }
  dfs1(1, 0);
  dfs2(1, 0);
  FOR(i, 1, q) printf("%lld\n", ans[i]);
  return 0;
}
