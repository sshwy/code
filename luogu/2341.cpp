#include <bits/stdc++.h>
#define FOR(a, b, c) for (int a = (b); a <= (c); a++)
using namespace std;
const int N = 1e4 + 5, M = 5e4 + 5;

struct qxx {
  int nex, t;
};
qxx e[M];
int h[N], le;
void add_path(int f, int t) { e[++le] = (qxx){h[f], t}, h[f] = le; }
#define FORe(i, u, v) for (int i = h[u], v; v = e[i].t, i; i = e[i].nex)

int n, m;

int dfn[N], low[N], totdfn;
int s[N], tp;
int scc[N], totscc;
bool in_s[N];
int dg[N], sz[N];
void dfs(int u) {
  low[u] = dfn[u] = ++totdfn;
  s[++tp] = u, in_s[u] = 1;
  FORe(i, u, v) {
    if (!dfn[v])
      dfs(v), low[u] = min(low[u], low[v]);
    else if (in_s[v])
      low[u] = min(low[u], dfn[v]);
  }
  if (dfn[u] == low[u]) {
    ++totscc;
    while (s[tp] != u) scc[s[tp]] = totscc, in_s[s[tp]] = 0, --tp, ++sz[totscc];
    scc[s[tp]] = totscc, in_s[s[tp]] = 0, --tp, ++sz[totscc];
  }
}
int main() {
  int n, m;
  scanf("%d%d", &n, &m);
  FOR(i, 1, m) {
    int u, v;
    scanf("%d%d", &u, &v);
    add_path(u, v);
  }
  FOR(i, 1, n) if (!dfn[i]) dfs(i);
  // FOR(i,1,n)printf("%d%c",scc[i]," \n"[i==n]);
  FOR(u, 1, n) {
    FORe(i, u, v) {
      if (scc[u] == scc[v]) continue;
      ++dg[scc[u]];
    }
  }
  int cnt = 0, ans = 0;
  FOR(i, 1, totscc) if (dg[i] == 0)++ cnt, ans = sz[i];
  assert(cnt > 0);
  // printf("cnt=%d,ans=%d\n",cnt,ans);
  if (cnt > 1)
    puts("0");
  else
    printf("%d\n", ans);
  return 0;
}
