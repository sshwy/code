#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int P = 10000, N = 1e5 + 5;

int n, t;
int a[N], la, b[N];
int nex[N];

void get_next(int *s, int ls) {
  int i = 1, j = 0;
  // nex[1]=0;
  while (i <= ls + 1) {
    if (j == 0 || s[i] == s[j])
      ++i, ++j, nex[i] = j;
    else
      j = nex[j];
  }
}
void go() {
  scanf("%d", &la);
  FOR(i, 1, la) scanf("%d", &a[i]);
  FOR(i, 1, la) b[i] = 0;
  get_next(a, la);
  int pos = la + 1;
  while (pos - 1 > 0) b[pos - 1] = 1, pos = nex[pos];
  // FOR(i,1,la)printf("%d%c",b[i]," \n"[i==la]);
  int ans = 0, x = 1;
  FOR(i, 1, la) x = x * n % P, ans = (ans + b[i] * x) % P;
  printf("%04d\n", ans);
}
int main() {
  scanf("%d%d", &n, &t);
  FOR(i, 1, t) go();
  return 0;
}
