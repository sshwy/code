// by Sshwy
#define DEBUGGER
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

#ifdef DEBUGGER

#define log_prefix()                                                \
  {                                                                 \
    fprintf(stderr, "\033[37mLine %-3d [%dms]:\033[0m  ", __LINE__, \
        (int)clock() / 1000);                                       \
  }
#define ilog(...) \
  { fprintf(stderr, __VA_ARGS__); }
#define llog(...)      \
  {                    \
    log_prefix();      \
    ilog(__VA_ARGS__); \
  }
#define log(...)           \
  {                        \
    log_prefix();          \
    ilog(__VA_ARGS__);     \
    fprintf(stderr, "\n"); \
  }
#define red(...)       \
  {                    \
    log_prefix();      \
    ilog("\033[31m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define green(...)     \
  {                    \
    log_prefix();      \
    ilog("\033[32m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define blue(...)      \
  {                    \
    log_prefix();      \
    ilog("\033[34m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }

#else

#define log_prefix() ;
#define ilog(...) ;
#define llog(...) ;
#define log(...) ;
#define red(...) ;
#define green(...) ;
#define blue(...) ;

#endif

namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
#define arrlog(name, begin, end, format, type)                          \
  {                                                                     \
    llog("%s: ", name);                                                 \
    for (auto cur = begin; cur != end; ++cur) ilog(format, type(*cur)); \
    ilog("\n");                                                         \
  }
const int N = 1e6 + 5, P = 998244353;
int pw(int a, int m) {
  int res = 1;
  while (m) m & 1 ? res = 1ll * res * a % P : 0, a = 1ll * a * a % P, m >>= 1;
  return res;
}
class hash_type {
private:
  unsigned x;

public:
  hash_type() { x = 0; }
  template <class T> hash_type(T X) { x = X % P; }
  hash_type operator+(const hash_type &y) const { return hash_type(x + y.x); }
  hash_type operator-(const hash_type &y) const { return hash_type(x - y.x + P); }
  hash_type operator*(const hash_type &y) const { return hash_type(1ull * x * y.x); }
  bool operator==(const hash_type &y) const { return x == y.x; }
  bool operator<(const hash_type &y) const { return x < y.x; }
  bool operator>(const hash_type &y) const { return x > y.x; }
  operator int() const { return x; }
};
const hash_type base = 29, ibase = pw(29, P - 2);

int n, m;
char a[N], s[N], rs[N];
vector<int> g[N];

hash_type Hash(const char *begin, const char *end) {
  hash_type res = 0;
  for (const char *cur = begin; cur != end; ++cur) {
    res = res * base + hash_type(*cur - 'A' + 1);
  }
  return res;
}
hash_type h_s, h_rs;
hash_type h_p[N], h_i[N];

int sz[N];
bool Cut[N];
int Dep[N];

void Size(int u, int p) {
  sz[u] = 1;
  for (int v : g[u])
    if (!Cut[v] && v != p) Size(v, u), sz[u] += sz[v];
}
typedef pair<int, int> pii;
pii Core(int u, int p, const int T) {
  pii res(-1, n + 1);
  int mx = 0;
  for (int v : g[u])
    if (!Cut[v] && v != p) {
      pii p = Core(v, u, T);
      if (p.se < res.se) res = p;
      mx = max(mx, sz[v]);
    }
  mx = max(mx, T - sz[u]);
  if (mx < res.se) res = {u, mx};
  return res;
}
hash_type h_f[N], h_g[N];
int F[N], G[N];

void Calc(int u, int p) { // h_f,h_g
  if (p) {
    Dep[u] = Dep[p] + 1;
    h_f[u] = h_f[p] + h_p[Dep[u]] * hash_type(a[u] - 'A' + 1);
    h_g[u] = h_g[p] * base + hash_type(a[u] - 'A' + 1);
  } else {
    Dep[u] = 0;
    h_f[u] = hash_type(a[u] - 'A' + 1);
    h_g[u] = hash_type(a[u] - 'A' + 1);
  }
  log("u=%-2d, Dep=%d,h_f=%-10d,h_g=%d", u, Dep[u], int(h_f[u]), int(h_g[u]));
  for (int v : g[u])
    if (!Cut[v] && v != p) Calc(v, u);
}

int stk[N];
void Calc2(int u, int p) { // F & G
  G[u] = F[u] = 1;         //空串
  const int tp = Dep[u];
  stk[tp] = u;
  for (int v : g[u])
    if (!Cut[v] && v != p) Calc2(v, u);
  if (tp >= m) {
    hash_type h = (h_f[u] - h_f[stk[tp - m]]) * h_i[tp - m + 1];
    // red("Calc2 u=%d, h=%d",u,int(h));
    if (h == h_s) { F[stk[tp - m]] += F[u]; }
    if (h == h_rs) {
      // green("u=%d, h==h_rs",u);
      // green("%d",stk[tp-m]);
      G[stk[tp - m]] += G[u];
    }
  }
  log("f[%d]=%d,g[%d]=%d", u, F[u], u, G[u]);
}
map<hash_type, int> mapF, mapG;
map<hash_type, hash_type> sp;
void dfs_add_g(int u, int p) {
  mapG[h_g[u]] += G[u];
  log("mapG: add %d %d", h_g[u], G[u]);
  for (int v : g[u])
    if (!Cut[v] && v != p) dfs_add_g(v, u);
}
void dfs_sub_g(int u, int p) {
  mapG[h_g[u]] -= G[u];
  log("mapG: sub %d %d", h_g[u], G[u]);
  for (int v : g[u])
    if (!Cut[v] && v != p) dfs_sub_g(v, u);
}
void dfs_add_f(int u, int p) {
  mapF[h_f[u]] += F[u];
  for (int v : g[u])
    if (!Cut[v] && v != p) dfs_add_f(v, u);
}
void dfs_sub_f(int u, int p) {
  mapF[h_f[u]] -= F[u];
  for (int v : g[u])
    if (!Cut[v] && v != p) dfs_sub_f(v, u);
}
long long ans;
void Solve(int u, int p) {
  Size(u, p);
  int core = Core(u, p, sz[u]).fi;
  blue("Solve(%d,%d),core=%d", u, p, core);

  Calc(core, 0);
  Calc2(core, 0);

  if (m == 1) {
    exit(1);
  } else {
    mapF.clear(), mapG.clear();
    long long tot = 0;
    dfs_add_g(core, 0);
    if (mapG.count(h_s)) tot += mapG[h_s];
    log("tot=%lld", tot);
    for (int v : g[core])
      if (!Cut[v]) {
        red("v=%d", v);
        dfs_sub_g(v, core);
        dfs_add_f(v, core);
        for (pair<hash_type, int> p : mapF) {
          if (p.se && sp.count(p.fi) && mapG.count(sp[p.fi])) {
            log("p=(%d,%d)", int(p.fi), p.se);
            log("pG=(%d,%d)", sp[p.fi], mapG[sp[p.fi]]);
            tot += p.se * 1ll * mapG[sp[p.fi]];
            log("tot=%lld", tot);
          }
        }
        hash_type h = h_s * base + hash_type(a[core] - 'A' + 1);
        if (mapF.count(h) && mapG.count(h_s)) {
          log("1 tot=%d", tot);
          tot += mapF[h] * 1ll * mapG[h_s];
          log("2 tot=%d", tot);
        }
        dfs_add_g(v, core);
        dfs_sub_f(v, core);
      }
    log("tot=%lld", tot);
    ans += tot;
  }

  Cut[core] = 1;
  for (int v : g[core])
    if (!Cut[v]) { Solve(v, core); }
}
void go() {
  scanf("%d%d", &n, &m);
  scanf("%s", a + 1);

  FOR(i, 1, n) g[i].clear(), Cut[i] = 0;

  FOR(i, 1, n - 1) {
    int x, y;
    scanf("%d%d", &x, &y);
    g[x].pb(y);
    g[y].pb(x);
  }
  scanf("%s", s);

  memcpy(rs, s, sizeof(s));
  reverse(rs, rs + m);
  h_s = Hash(s, s + m);
  h_rs = Hash(rs, rs + m);
  sp.clear();
  hash_type A, B = h_s;
  FOR(i, 0, m - 1) {
    A = A * base + hash_type(s[i] - 'A' + 1);
    log("A=%d,B=%d", int(A), int(B));
    sp[A] = B;
    B = B - hash_type(s[i] - 'A' + 1) * h_p[m - i - 1];
  }

  green("h_s=%d,h_rs=%d", int(h_s), int(h_rs));

  ans = 0;
  Solve(1, 0);

  printf("%lld\n", ans);
}
int main() {
  int t;
  scanf("%d", &t);

  h_p[0] = h_i[0] = 1;
  FOR(i, 1, N - 1) h_p[i] = h_p[i - 1] * base;
  FOR(i, 1, N - 1) h_i[i] = h_i[i - 1] * ibase;

  arrlog("h_p", h_p, h_p + 10, "%d ", int);
  arrlog("h_i", h_i, h_i + 10, "%d ", int);

  FOR(i, 1, t) go();
  return 0;
}

// 点分治
// 求经过core的S的循环串路径个数
// O(n)
// 对于每个点集f[i]表示i子树中以i的某个亲儿子结束的合法路径个数
// g[i]表示i子树中以i的某个亲儿子开始的合法路径个数
// 对点i到core上的串求个Hash值
// 然后把每个点的f和g用Hash表存一个和
// 就可以统计答案了。
