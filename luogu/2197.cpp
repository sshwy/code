#include <bits/stdc++.h>
#define FOR(a, b, c) for (int a = (b); a <= (c); a++)
using namespace std;

void go() {
  int n, a, ans = 0;
  scanf("%d", &n);
  FOR(i, 1, n) scanf("%d", &a), ans ^= a;
  puts(ans ? "Yes" : "No");
}
int main() {
  int t;
  scanf("%d", &t);
  FOR(i, 1, t) go();
  return 0;
}
