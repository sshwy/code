// by Sshwy
//#define DEBUGGER
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

#ifdef DEBUGGER

#define log_prefix()                                                \
  {                                                                 \
    fprintf(stderr, "\033[37mLine %-3d [%dms]:\033[0m  ", __LINE__, \
        (int)clock() / 1000);                                       \
  }
#define ilog(...) \
  { fprintf(stderr, __VA_ARGS__); }
#define llog(...)      \
  {                    \
    log_prefix();      \
    ilog(__VA_ARGS__); \
  }
#define log(...)           \
  {                        \
    log_prefix();          \
    ilog(__VA_ARGS__);     \
    fprintf(stderr, "\n"); \
  }
#define red(...)       \
  {                    \
    log_prefix();      \
    ilog("\033[31m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define green(...)     \
  {                    \
    log_prefix();      \
    ilog("\033[32m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define blue(...)      \
  {                    \
    log_prefix();      \
    ilog("\033[34m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }

#else

#define log_prefix() ;
#define ilog(...) ;
#define llog(...) ;
#define log(...) ;
#define red(...) ;
#define green(...) ;
#define blue(...) ;

#endif

namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 1e5 + 5, M = 1e5 + 5;

int n, m, T;
int a[N];

struct data {
  int a, b, c, d, e;
} qry[M];
pair<int, int> ans[M];

int b[N], c1[N], c2[N];
void add(int *f, int pos, int v, int lim = N) {
  for (int i = pos; i < lim; i += i & -i) f[i] += v;
}
int pre(int *f, int pos) {
  int res = 0;
  for (int i = pos; i > 0; i -= i & -i) res += f[i];
  return res;
}

int L = 1, R = 0;
void del(int v) {
  assert(b[v]);
  --b[v];
  add(c1, v, -1);
  if (b[v] == 0) add(c2, v, -1);
}
void add(int v) {
  if (b[v] == 0) add(c2, v, 1);
  ++b[v];
  add(c1, v, 1);
}
void addL() {
  del(a[L]);
  ++L;
}
void addR() {
  ++R;
  add(a[R]);
}
void subL() {
  --L;
  add(a[L]);
}
void subR() {
  del(a[R]);
  --R;
}

pair<int, int> query(int a, int b) {
  return {pre(c1, b) - pre(c1, a - 1), pre(c2, b) - pre(c2, a - 1)};
}

int D[M * 2 + N], ld;

int main() {
  scanf("%d%d", &n, &m);
  FOR(i, 1, n) scanf("%d", &a[i]), D[++ld] = a[i];
  FOR(i, 1, m) {
    scanf("%d%d%d%d", &qry[i].a, &qry[i].b, &qry[i].c, &qry[i].d);
    qry[i].e = i;
    D[++ld] = qry[i].c, D[++ld] = qry[i].d;
  }
  sort(D + 1, D + ld + 1);
  ld = unique(D + 1, D + ld + 1) - D - 1;

  FOR(i, 1, n) a[i] = lower_bound(D + 1, D + ld + 1, a[i]) - D;
  FOR(i, 1, m) {
    qry[i].c = lower_bound(D + 1, D + ld + 1, qry[i].c) - D;
    qry[i].d = lower_bound(D + 1, D + ld + 1, qry[i].d) - D;
  }

  T = sqrt(n);
  sort(qry + 1, qry + m + 1,
      [](data x, data y) { return x.a / T == y.a / T ? x.b < y.b : x.a < y.a; });

  FOR(i, 1, m) {
    while (R < qry[i].b) addR();
    while (L > qry[i].a) subL();
    while (R > qry[i].b) subR();
    while (L < qry[i].a) addL();
    ans[qry[i].e] = query(qry[i].c, qry[i].d);
  }
  FOR(i, 1, m) printf("%d %d\n", ans[i].fi, ans[i].se);
  return 0;
}
