#include <iostream>

using namespace std;

int n, na, nb, ta, tb;
int a[200], b[200];
int pst[5][5] = {0, -1, 1, 1, -1, 1, 0, -1, 1, -1, -1, 1, 0, -1, 1, -1, -1, 1, 0, 1,
    1, 1, -1, -1, 0};

int main() {
  cin >> n >> na >> nb;
  for (int i = 0; i < na; i++) cin >> a[i];
  for (int i = 0; i < nb; i++) cin >> b[i];
  for (int i = 0; i < n; i++) {
    if (pst[a[i % na]][b[i % nb]] == 1)
      ta++;
    else if (pst[a[i % na]][b[i % nb]] == -1)
      tb++;
  }
  cout << ta << ' ' << tb;
  return 0;
}
