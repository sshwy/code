// by Sshwy
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 510, M = N;

namespace T {
  const int SZ = N * M * 4;
  int lc[SZ], rc[SZ], fa[SZ], sz[SZ], rnd[SZ];
  int rrand() {
    static int seed = 1;
    return seed = seed * 482711;
  }
  int INIT = []() {
    fill(sz + 1, sz + SZ, 1);
    FOR(i, 0, SZ - 1) rnd[i] = rrand();
    return 0;
  }();
  int root(int u) { return fa[u] == 0 ? u : root(fa[u]); }
  void reset(int u) { lc[u] = rc[u] = fa[u] = 0, sz[u] = 1; }
  void pushup(int u) { sz[u] = sz[lc[u]] + sz[rc[u]] + 1; }
  int merge(int x, int y) {
    if (x == y) return x;
    if (!x || !y) return fa[x] = fa[y] = 0, x + y;
    if (rnd[x] < rnd[y])
      return rc[x] = merge(rc[x], y), fa[rc[x]] = x, pushup(x), x;
    else {
      return lc[y] = merge(x, lc[y]), fa[lc[y]] = y, pushup(y), y;
    }
  }
  void split(int u, int node, int &x, int &y) { //以node与其后继为分界，split
    if (!u) return;
    x = node, y = rc[node];
    rc[x] = 0, fa[y] = 0;
    pushup(x);
    for (int v = node, p = fa[v]; p; v = fa[v], p = fa[p]) {
      if (lc[p] == v)
        lc[p] = y, fa[y] = p, pushup(p), y = p;
      else
        rc[p] = x, fa[x] = p, pushup(p), x = p;
    }
    fa[x] = fa[y] = 0;
  }
  void split2(int u, int node, int &x, int &y) { //以node与其前驱为分界，split
    y = node, x = lc[node];
    lc[y] = 0, fa[x] = 0;
    pushup(y);
    for (int v = node, p = fa[v]; p; v = fa[v], p = fa[p]) {
      if (lc[p] == v)
        lc[p] = y, fa[y] = p, pushup(p), y = p;
      else
        rc[p] = x, fa[x] = p, pushup(p), x = p;
    }
    fa[x] = fa[y] = 0;
  }
  int rank(int node) {
    int r = root(node), x, y;
    split(r, node, x, y);
    int res = sz[x];
    // assert(x!=y);
    merge(x, y);
    return res;
  }
  void rotate_to_back(int node) {
    int r = root(node), x, y;
    split(r, node, x, y);
    // assert(x!=y);
    merge(y, x);
  }
  void rotate_to_front(int node) {
    int r = root(node), x, y;
    split2(r, node, x, y);
    // assert(x!=y);
    merge(y, x);
  }
  void link(int x, int y) {
    // assert(root(x)!=root(y));
    rotate_to_back(x);
    rotate_to_front(y);
    merge(root(x), root(y));
  }
  void cut(int node) {
    int r = root(node), x, y;
    split(r, node, x, y);
  }
  int pre(int u) { //循环前驱
    if (lc[u]) {
      u = lc[u];
      while (rc[u]) u = rc[u];
      return u;
    }
    while (fa[u] && lc[fa[u]] == u) u = fa[u];
    if (fa[u] == 0) {
      while (rc[u]) u = rc[u];
      return u;
    }
    return fa[u];
  }
  int nex(int u) { //循环后继
    if (rc[u]) {
      u = rc[u];
      while (lc[u]) u = lc[u];
      return u;
    }
    while (fa[u] && rc[fa[u]] == u) u = fa[u];
    if (fa[u] == 0) {
      while (lc[u]) u = lc[u];
      return u;
    }
    return fa[u];
  }
} // namespace T

int n, m, q;
bool vis[N * M * 4];

inline int id_D(int x, int y) { return (x * (m + 1) + y + 1) * 2; } //(x,y)下面的墙
inline int id_R(int x, int y) {
  return (x * (m + 1) + y + 1) * 2 + 1;
} //(x,y)右边的墙
inline int U(int id) { return id * 2; }
inline int D(int id) { return id * 2 + 1; }
inline int L(int id) { return id * 2; }
inline int R(int id) { return id * 2 + 1; }
//*2表示在左边/上边
//*2+1表示在右边/下边

inline bool ED(int x, int y) {
  return (x < 0 || x >= n || y < 0 || y > m) ? 0 : vis[id_D(x, y)];
}
inline bool ER(int x, int y) {
  return (x < 0 || x > n || y < 0 || y >= m) ? 0 : vis[id_R(x, y)];
}
inline int check(int x, int y) {
  return ED(x, y) + ER(x, y) + ED(x - 1, y) + ER(x, y - 1);
} //某个点是否有边

typedef vector<pii> Vp;

Vp C(int x, int y) { // clockwise U R D L
  Vp res = {{ED(x - 1, y), L(id_D(x - 1, y))}, {ER(x, y), U(id_R(x, y))},
      {ED(x, y), R(id_D(x, y))}, {ER(x, y - 1), D(id_R(x, y - 1))}};
  res.insert(res.end(), res.begin(), res.end());
  return res;
}
Vp ctC(int x, int y) { // counter-clockwise U L D R
  Vp res = {{ED(x - 1, y), R(id_D(x - 1, y))}, {ER(x, y - 1), U(id_R(x, y - 1))},
      {ED(x, y), L(id_D(x, y))}, {ER(x, y), D(id_R(x, y))}};
  res.insert(res.end(), res.begin(), res.end());
  return res;
}

void add_D(int x, int y) {
  int id = id_D(x, y);
  // assert(!vis[id]);
  int p1 = check(x, y), p2 = check(x + 1, y);
  if (p1 && p2) {
    int pre = [&x, &y]() {
      Vp v = C(x, y);
      FOR(i, 3, 5) if (v[i].fi) return v[i].se;
    }();
    int suf = [&x, &y]() {
      Vp v = ctC(x + 1, y);
      FOR(i, 1, 3) if (v[i].fi) return v[i].se;
    }();
    if (T::root(pre) == T::root(suf)) { //断环
      int P = T::pre(suf);
      T::rotate_to_back(pre), T::cut(P);
      T::link(pre, L(id));
      T::link(P, R(id));
    } else {
      int P = T::nex(pre);
      T::rotate_to_back(pre), T::link(pre, L(id)), T::link(R(id), P);
      T::rotate_to_front(suf), T::link(L(id), suf);
    }
  } else if (p1) {
    int pre = [&x, &y]() {
      Vp v = C(x, y);
      FOR(i, 3, 5) if (v[i].fi) return v[i].se;
    }(); // L(id)的前驱
    T::link(L(id), R(id));
    T::link(pre, L(id));
  } else if (p2) {
    int suf = [&x, &y]() {
      Vp v = ctC(x + 1, y);
      FOR(i, 1, 3) if (v[i].fi) return v[i].se;
    }();
    T::link(R(id), L(id));
    T::link(L(id), suf); //在 #1 后插入平衡树
                         //#2。先把#1搞到最后一个，把#2搞到第一个，然后merge
  } else {
    T::link(L(id), R(id));
  }
  vis[id] = 1;
}
void add_R(int x, int y) {
  int id = id_R(x, y);
  // assert(!vis[id]);
  int p1 = check(x, y), p2 = check(x, y + 1);
  if (p1 && p2) {
    int pre = [&x, &y]() {
      Vp v = C(x, y);
      FOR(i, 2, 4) if (v[i].fi) return v[i].se;
    }();
    int suf = [&x, &y]() {
      Vp v = ctC(x, y + 1);
      FOR(i, 2, 4) if (v[i].fi) return v[i].se;
    }();
    if (T::root(pre) == T::root(suf)) { //断环
      int P = T::pre(suf);
      T::rotate_to_back(pre), T::cut(P);
      T::link(pre, D(id));
      T::link(P, U(id));
    } else {
      int P = T::nex(pre);
      T::rotate_to_back(pre), T::link(pre, D(id)), T::link(U(id), P);
      T::rotate_to_front(suf), T::link(D(id), suf);
    }
  } else if (p1) {
    int pre = [&x, &y]() {
      Vp v = C(x, y);
      FOR(i, 2, 4) if (v[i].fi) return v[i].se;
    }();
    T::link(D(id), U(id));
    T::link(pre, D(id));
  } else if (p2) {
    int suf = [&x, &y]() {
      Vp v = ctC(x, y + 1);
      FOR(i, 2, 4) if (v[i].fi) return v[i].se;
    }();
    T::link(U(id), D(id));
    T::link(D(id), suf);
  } else {
    T::link(D(id), U(id));
  }
  vis[id] = 1;
}
void rem_D(int x, int y) {
  int id = id_D(x, y);
  // assert(vis[id]);

  int p1 = check(x, y) - 1, p2 = check(x + 1, y) - 1;
  // assert(p1>=0&&p2>=0);
  if (p1 && p2) {
    if (T::root(L(id)) == T::root(R(id))) { //断环
      T::rotate_to_front(R(id));
      T::cut(R(id));
      T::cut(L(id));
      T::rotate_to_front(L(id));
      T::cut(L(id));
    } else {
      int r1 = T::nex(R(id)), r2 = T::nex(L(id));
      T::rotate_to_front(R(id));
      T::cut(R(id));
      T::rotate_to_front(L(id));
      T::cut(L(id));
      r1 = T::root(r1);
      r2 = T::root(r2);
      T::merge(r1, r2);
    }
  } else if (p1) {
    T::rotate_to_front(L(id));
    T::cut(R(id));
  } else if (p2) {
    T::rotate_to_front(R(id));
    T::cut(L(id));
  }

  T::reset(L(id));
  T::reset(R(id));
  vis[id] = 0;
}
void rem_R(int x, int y) {
  // printf("rem_R(%d,%d)\n",x,y);
  int id = id_R(x, y);
  // assert(vis[id]);
  int p1 = check(x, y) - 1, p2 = check(x, y + 1) - 1;
  // assert(p1>=0&&p2>=0);

  if (p1 && p2) {
    if (T::root(D(id)) == T::root(U(id))) { //断环
      T::rotate_to_front(U(id));
      T::cut(U(id));
      T::cut(D(id));
      T::rotate_to_front(D(id));
      T::cut(D(id));
    } else {
      int r1 = T::nex(U(id)), r2 = T::nex(D(id));
      T::rotate_to_front(U(id));
      T::cut(U(id));
      T::rotate_to_front(D(id));
      T::cut(D(id));
      r1 = T::root(r1), r2 = T::root(r2);
      T::merge(r1, r2);
    }
  } else if (p1) {
    T::rotate_to_front(D(id));
    T::cut(U(id));
  } else if (p2) {
    T::rotate_to_front(U(id));
    T::cut(D(id));
  }

  T::reset(D(id));
  T::reset(U(id));
  vis[id] = 0;
}
int query(int idx, int idy) {
  if (T::root(idx) != T::root(idy)) return -1;
  T::rotate_to_back(idy);
  return T::rank(idy) - T::rank(idx);
}

int main() {
  scanf("%d%d%d", &n, &m, &q);

  FOR(j, 0, m - 1) add_R(0, j);
  FOR(i, 0, n - 1) add_D(i, 0);
  FOR(j, 0, m - 1) add_R(n, j);
  FOR(i, 0, n - 1) add_D(i, m);

  FOR(i, 0, n - 1) {
    FOR(j, 1, m - 1) {
      int x;
      scanf("%d", &x);
      if (x) add_D(i, j);
    }
  }
  FOR(i, 1, n - 1) {
    FOR(j, 0, m - 1) {
      int x;
      scanf("%d", &x);
      if (x) add_R(i, j);
    }
  }

  FOR(i, 1, q) {
    int op, k[12];
    scanf("%d", &op);
    if (op == 1) {
      FOR(i, 1, 4) scanf("%d", &k[i]);
      if (k[1] == k[3])
        add_R(k[1], min(k[2], k[4]));
      else
        add_D(min(k[1], k[3]), k[2]);
    } else if (op == 2) {
      FOR(i, 1, 4) scanf("%d", &k[i]);
      if (k[1] == k[3])
        rem_R(k[1], min(k[2], k[4]));
      else
        rem_D(min(k[1], k[3]), k[2]);
    } else {
      FOR(i, 1, 10) scanf("%d", &k[i]);
      auto get_id = [](int *k) {
        if (k[1] == k[3])
          return id_R(k[1], min(k[2], k[4])) * 2 + k[5];
        else
          return id_D(min(k[1], k[3]), k[2]) * 2 + k[5];
      };
      printf("%d\n", query(get_id(k), get_id(k + 5)));
    }
  }

  return 0;
}
