#include <iostream>

using namespace std;

int n, b, h[30], tot = 99999999, t1;
bool use[30];

int sum() {
  int s = 0;
  for (int i = 1; i <= n; i++)
    if (use[i]) s += h[i];
  return s;
}

void search(int k) {
  if (k > n) return;

  use[k] = true;
  t1 = sum();
  if (t1 >= b && t1 < tot) tot = t1;

  search(k + 1);
  use[k] = false;
  search(k + 1);
}

int main() {
  cin >> n >> b;
  for (int i = 1; i <= n; i++) cin >> h[i];
  search(1);
  cout << tot - b;
  return 0;
}
