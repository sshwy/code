// by Sshwy
#define DEBUGGER
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

#ifdef DEBUGGER

#define log_prefix()                                                \
  {                                                                 \
    fprintf(stderr, "\033[37mLine %-3d [%dms]:\033[0m  ", __LINE__, \
        (int)clock() / 1000);                                       \
  }
#define ilog(...) \
  { fprintf(stderr, __VA_ARGS__); }
#define llog(...)      \
  {                    \
    log_prefix();      \
    ilog(__VA_ARGS__); \
  }
#define log(...)           \
  {                        \
    log_prefix();          \
    ilog(__VA_ARGS__);     \
    fprintf(stderr, "\n"); \
  }
#define red(...)       \
  {                    \
    log_prefix();      \
    ilog("\033[31m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define green(...)     \
  {                    \
    log_prefix();      \
    ilog("\033[32m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define blue(...)      \
  {                    \
    log_prefix();      \
    ilog("\033[34m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }

#else

#define log_prefix() ;
#define ilog(...) ;
#define llog(...) ;
#define log(...) ;
#define red(...) ;
#define green(...) ;
#define blue(...) ;

#endif

namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 1e5 + 5, L = 14, W = 1 << L;

int n, m, k, T;
int a[N];

vector<int> S;

struct atom {
  int a, b, c;
} qry[N];
vector<atom> vl[N], vr[N], vgl, vgr;
bool cmp(atom x, atom y) { return x.a / T != y.a / T ? x.a < y.a : x.b < y.b; }

int cl = 1, cr = 0;
void moveR(int r, int id) {
  vr[cl].pb({cr, r, id}); //原来的位置，目标位置，id
  vgr.pb({cr, r, id});
  cr = r;
}
void moveL(int l, int id) {
  vl[cr].pb({cl, l, id});
  vgl.pb({cl, l, id});
  cl = l;
}
long long g[N], ans[N];
int c[W];
void work_vgr() {
  fill(c, c + W, 0);
  g[0] = 0;
  FOR(i, 1, n) {
    g[i] = 0;
    for (int x : S) g[i] += c[a[i] ^ x];
    c[a[i]]++;
    log("g[%d]=%lld", i, g[i]);
  }
  FOR(i, 1, n) g[i] += g[i - 1];
  for (atom p : vgr) {
    int R = p.a, r = p.b, id = p.c;
    ans[id] += g[r] - g[R];
  }
}
void work_vr() {
  fill(c, c + W, 0);
  FOR(i, 1, n) {
    for (atom p : vr[i]) {
      int R = p.a, r = p.b, id = p.c;
      if (R < r)
        FOR(i, R + 1, r) ans[id] -= c[a[i]];
      else
        FOR(i, r + 1, R) ans[id] += c[a[i]];
    }
    for (int x : S) c[a[i] ^ x]++;
  }
}
void work_vgl() {
  fill(c, c + W, 0);
  g[n + 1] = 0;
  ROF(i, n, 0) {
    g[i] = 0;
    for (int x : S) g[i] += c[a[i] ^ x];
    c[a[i]]++;
  }
  ROF(i, n, 0) g[i] += g[i + 1];
  for (atom p : vgl) {
    int L = p.a, l = p.b, id = p.c;
    ans[id] += g[L - 1] - g[l - 1];
  }
}
void work_vl() {
  fill(c, c + W, 0);
  ROF(i, n, 0) { // cr可能=0
    for (atom p : vl[i]) {
      int L = p.a, l = p.b, id = p.c;
      if (l < L)
        FOR(i, l, L - 1) ans[id] -= c[a[i]];
      else
        FOR(i, L, l - 1) ans[id] -= c[a[i]];
    }
    for (int x : S) c[a[i] ^ x]++;
  }
}
int main() {
  scanf("%d%d%d", &n, &m, &k);
  T = max(1, (int)sqrt(n));
  FOR(i, 0, W - 1) if (__builtin_popcount(i) == k) S.push_back(i);
  FOR(i, 1, n) scanf("%d", &a[i]);
  FOR(i, 1, m) {
    int a, b;
    scanf("%d%d", &a, &b);
    qry[i] = {a, b, i};
  }
  sort(qry + 1, qry + m + 1, cmp);
  FOR(i, 1, m) {
    int l = qry[i].a, r = qry[i].b, id = qry[i].c;
    moveR(r, id);
    moveL(l, id);
  }
  work_vgr();
  work_vr();
  work_vgl();
  work_vl();
  FOR(i, 1, m) ans[qry[i].c] += ans[qry[i - 1].c];
  FOR(i, 1, m) printf("%lld\n", ans[i]);
  return 0;
}

// 莫队：记录f(l,r,x)表示区间l,r的数与a[x]能配对的个数。
// 如果我构建出[l,r]的权值数组，那么计算f(l,r,x)的复杂度是O( e=C(14,7) ) 的
// 假设当前状态是[l,r]，我们要变到[l,R].
// 假设 r<R.
// 则我们就是要计算
//   sum f(l,i-1,i) r+1<=i<=R
// = sum f(1,i-1,i) - f(1,l-1,i) r+1<=i<=R
// = sum g(i) - h(l-1,i) i in [r+1,R]
// = G(R)-G(r) - h[l-1,r+1,R]
// G可以O(ne)预处理。
// h: 我们需要实现一个，O(e) extend，O(1)区间查询的做法
