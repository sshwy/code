#include <iostream>

using namespace std;

int n, m, tot, a, s;
int face[100001];
char name[100001][100];

int main() {
  cin >> n >> m;
  for (int i = 1; i <= n; i++) { cin >> face[i] >> name[i]; }
  tot = 1;
  for (int i = 1; i <= m; i++) {
    cin >> a >> s;
    // a:0-left;1-right. face:0-in;1-out; a^face[tot]:1->+s;0->-s
    if (a ^ face[tot])
      tot += s;
    else
      tot -= s;
    if (tot < 1) tot += n;
    if (tot > n) tot -= n;
  }
  cout << name[tot];
  return 0;
}
