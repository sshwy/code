#include <iostream>
#include <string>

using namespace std;

string n;
int k, k2;

int main() {
  cin >> n >> k;
  k2 = n.size() - k;
  for (int i = 0; i < n.size() && k; i++) {
    if (i == n.size() - 1 && k != 0) {
      n.erase(i, 1);
      k--;
      i = -1;
    } else if (n[i] > n[i + 1]) {
      n.erase(i, 1);
      k--;
      i = -1;
    }
  }
  while (n[0] == '0') n.erase(0, 1);
  if (n.size())
    cout << n;
  else
    cout << 0;
  return 0;
}
