#include <bits/stdc++.h>
using namespace std;
const int N = 100005, M = 200005;
int n, m, s;

struct qxx {
  int nex, t, v;
};
qxx e[M];
int h[N], cnt, d[N];
void add_path(int f, int t, int v) {
  e[++cnt] = (qxx){h[f], t, v};
  h[f] = cnt;
}

struct mpr { // my_pair
  int idx, dis;
  bool operator<(mpr tht) const { return dis > tht.dis; }
};
priority_queue<mpr> q;

int main() {
  scanf("%d%d%d", &n, &m, &s);
  for (int i = 1; i <= m; i++) {
    int ui, vi, wi;
    scanf("%d%d%d", &ui, &vi, &wi);
    add_path(ui, vi, wi);
  }

  memset(d, 0x3f, sizeof(d));
  d[s] = 0;
  q.push((mpr){s, 0});
  while (!q.empty()) {
    mpr k = q.top();
    q.pop();
    if (d[k.idx] < k.dis) continue;
    for (int i = h[k.idx]; i; i = e[i].nex) {
      if (d[e[i].t] > k.dis + e[i].v) {
        d[e[i].t] = k.dis + e[i].v;
        q.push((mpr){e[i].t, d[e[i].t]});
      }
    }
  }
  for (int i = 1; i <= n; i++) printf("%d ", d[i]);
  return 0;
}