// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

const int N = 2e5 + 5, W = 5e5 + 5;

int n;
int c[W];
long long ans;

int main() {
  scanf("%d", &n);
  FOR(i, 1, n) {
    int x;
    scanf("%d", &x);
    c[x]++;
  }
  FOR(i, 1, W - 1) if (c[i]) {
    ans += 1ll * c[i] * (c[i] - 1);
    for (int j = i + i; j < W; j += i) ans += 1ll * c[i] * c[j];
  }
  printf("%lld\n", ans);
  return 0;
}
