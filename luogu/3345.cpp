// by Sshwy
//#define DEBUGGER
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

#ifdef DEBUGGER

#define log_prefix()                                                \
  {                                                                 \
    fprintf(stderr, "\033[37mLine %-3d [%dms]:\033[0m  ", __LINE__, \
        (int)clock() / 1000);                                       \
  }
#define ilog(...) \
  { fprintf(stderr, __VA_ARGS__); }
#define llog(...)      \
  {                    \
    log_prefix();      \
    ilog(__VA_ARGS__); \
  }
#define log(...)           \
  {                        \
    log_prefix();          \
    ilog(__VA_ARGS__);     \
    fprintf(stderr, "\n"); \
  }
#define red(...)       \
  {                    \
    log_prefix();      \
    ilog("\033[31m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define green(...)     \
  {                    \
    log_prefix();      \
    ilog("\033[32m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define blue(...)      \
  {                    \
    log_prefix();      \
    ilog("\033[34m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }

#else

#define log_prefix() ;
#define ilog(...) ;
#define llog(...) ;
#define log(...) ;
#define red(...) ;
#define green(...) ;
#define blue(...) ;

#endif

namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 1e5 + 5;

namespace T {
  struct qxx {
    int nex, t, v, v2;
  } e[N * 2];
  int h[N], le = 1;
  void add_path(int f, int t, int v, int v2) {
    e[++le] = {h[f], t, v, v2}, h[f] = le;
  }
} // namespace T

struct qxx {
  int nex, t, v;
} e[N * 2];
int h[N], le = 1;
void add_path(int f, int t, int v) { e[++le] = {h[f], t, v}, h[f] = le; }

int fa[N][20], dep[N];
long long we[N];
void dfs(int u, int p, int c) {
  fa[u][0] = p;
  FOR(j, 1, 19) {
    fa[u][j] = fa[fa[u][j - 1]][j - 1];
    if (fa[u][j] == 0) break;
  }
  dep[u] = dep[p] + 1;
  we[u] = we[p] + c;
  for (int i = h[u]; i; i = e[i].nex) {
    int v = e[i].t, w = e[i].v;
    if (v != p) dfs(v, u, w);
  }
}
int lca(int u, int v) {
  if (dep[u] < dep[v]) swap(u, v);
  ROF(j, 19, 0) if (dep[u] - (1 << j) >= dep[v]) u = fa[u][j];
  if (u == v) return u;
  ROF(j, 19, 0) if (fa[u][j] != fa[v][j]) u = fa[u][j], v = fa[v][j];
  return fa[u][0];
}
long long distance(int u, int v) {
  // log("lca(%d,%d)=%d",u,v,lca(u,v));
  // log("distance(%d,%d)=%lld",u,v, we[u]+we[v]-we[lca(u,v)]*2);
  return we[u] + we[v] - we[lca(u, v)] * 2;
}

int n, q;

bool Cut[N];
int sz[N], sm[N];

void Size(int u, int p) {
  sz[u] = 1, sm[u] = 0;
  for (int i = h[u]; i; i = e[i].nex) {
    int v = e[i].t;
    if (!Cut[v] && v != p) { Size(v, u), sz[u] += sz[v], sm[u] = max(sm[u], sz[v]); }
  }
}
int Core(int u, int p, int T) {
  int res = u, mx = max(T - sz[u], sm[u]);
  for (int i = h[u]; i; i = e[i].nex) {
    int v = e[i].t;
    if (!Cut[v] && v != p) {
      int x = Core(v, u, T), y = max(T - sz[x], sm[x]);
      if (y < mx) res = x, mx = y;
    }
  }
  return res;
}

int Fa[N], Root;
int Dep[N];
int dist[N][20];

void Solve(int u, int p, int val) {
  Size(u, p);
  int core = Core(u, p, sz[u]);
  log("Solve(%d,%d),core=%d", u, p, core);

  Fa[core] = p;
  Dep[core] = Dep[p] + 1;
  if (!p) Root = core;
  if (p) {
    T::add_path(p, core, u, val);
    blue("add_path(%d,%d,%d)", p, core, u);
    // T::add_path(core,p,u); //点分树的边权记录真儿子
  }
  for (int v = core; v; v = Fa[v]) dist[core][Dep[v]] = distance(core, v);

  Cut[core] = 1;
  for (int i = h[core]; i; i = e[i].nex) {
    int v = e[i].t, w = e[i].v;
    if (!Cut[v]) Solve(v, core, w);
  }
}

long long w2[N]; //连通块权值和
long long w3[N]; //连通块代价和
long long w4[N]; //连通块到点分树父亲的代价和

void add(int u, long long val) {
  green("add(%d,%lld)", u, val);
  for (int v = u; v; v = Fa[v]) {
    log("v=%d,Fa=%d", v, Fa[v]);
    w2[v] += val;
    w3[v] += dist[u][Dep[v]] * 1ll * val;
    if (Fa[v]) w4[v] += dist[u][Dep[v] - 1] * 1ll * val;
  }
}
long long query(int u) {
  log("query(%d), w3=%lld", u, w3[u]);
  for (int i = T::h[u]; i; i = T::e[i].nex) {
    int v = T::e[i].t, x = T::e[i].v, w = T::e[i].v2;
    log("  query(%d): v=%d,x=%d", u, v, x);
    log("  w2[%d]=%lld", v, w2[v]);
    if (w2[v] * 2 > w2[u]) {
      long long res = w3[u] - w4[v] + (w2[u] - w2[v]) * 1ll * w; //其他儿子的贡献和
      log("res=%lld", res);
      add(x, w2[u] - w2[v]);
      res += query(v);
      add(x, w2[v] - w2[u]);
      return res;
    }
  }
  return w3[u];
}

int main() {
  scanf("%d%d", &n, &q);
  FOR(i, 1, n - 1) {
    int a, b, c;
    scanf("%d%d%d", &a, &b, &c);
    add_path(a, b, c);
    add_path(b, a, c);
  }
  dfs(1, 0, 0);
  Solve(1, 0, 0);
  FOR(i, 1, q) {
    int u, e;
    scanf("%d%d", &u, &e);
    add(u, e);
    printf("%lld\n", query(Root));
  }
  return 0;
}

// 点分树。
// 考虑询问。
// 如果要询问一个点的代价，可以点分树上暴力跳父亲。这样修改也可以想办法做。
// 现在要询问代价最小的点，考虑从点分树的根往下跳来查询。
//     那么我们显然会向权值和最大的子树跳。
//     跳的时侯，其他子树的答案可以累加，然后其他子树的权值和也得累加。
//     可以把其他子树缩点。
// 因此我们需要支持：
// 1. 修改点权
// 2. 查询点分树上连通块的权值和
// 3. 查询点分树上连通块的答案
// 可以发现，修改点权只会修改log个连通块的信息
// 于是我们把修改搞成log，这样就可以O(1)回答信息了。
//
// 那么查询的时侯，我们从点分树根结点开始：
//     找到它的权值和最大的儿子big
//     求出其他连通块的贡献和
//     把其他连通块的权值和加到big连通块与根结点相邻的叶子结点上（点权修改）
//     然后就可以递归了
//     最后求完了撤消修改即可
