#include <iostream>

using namespace std;

const int INF = 99999999;
int n, m, t, s, e, h;
int w[301][301];

int main() {
  cin >> n >> m >> t;
  for (int i = 1; i <= n; i++) {
    for (int j = 1; j <= n; j++) { w[i][j] = INF; }
    w[i][i] = 0;
  }
  for (int i = 1; i <= m; i++) {
    cin >> s >> e >> h;
    w[s][e] = h;
  }
  for (int k = 1; k <= n; k++) {
    for (int i = 1; i <= n; i++) {
      for (int j = 1; j <= n; j++) {
        if (w[i][j] > (w[i][k] > w[k][j] ? w[i][k] : w[k][j])) {
          w[i][j] = (w[i][k] > w[k][j] ? w[i][k] : w[k][j]);
        }
      }
    }
  }
  for (int i = 1; i <= t; i++) {
    cin >> s >> e;
    if (w[s][e] == INF)
      cout << "-1\n";
    else
      cout << w[s][e] << endl;
  }
  return 0;
}
