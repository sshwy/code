// by Sshwy
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int M = 1510, S = 1510, N = M;
double oo = 1e99, eps = 1e-7;

int n, m;

const int SZ = S;
struct AC {
  int tr[SZ][10], fail[SZ], tot;
  int cnt[SZ];
  double val[SZ];
  AC() { tot = -1, new_node(); }
  void init() { tot = -1, new_node(); }
  int new_node() {
    return ++tot, fail[tot] = 0, memset(tr[tot], 0, sizeof(tr[tot])), cnt[tot] = 0,
                  val[tot] = 0, tot;
  }
  void insert(const char *s, double v) {
    int u = 0;
    while (*s) {
      int c = *s - '0';
      if (!tr[u][c]) tr[u][c] = new_node();
      u = tr[u][c];
      ++s;
    }
    val[u] += v, cnt[u]++;
  }
  int q[SZ], ql, qr;
  void build() {
    ql = 1, qr = 0;
    FOR(i, 0, 9) if (tr[0][i]) q[++qr] = tr[0][i];
    while (ql <= qr) {
      int u = q[ql++];
      FOR(c, 0, 9) {
        if (tr[u][c])
          fail[tr[u][c]] = tr[fail[u]][c], q[++qr] = tr[u][c];
        else
          tr[u][c] = tr[fail[u]][c];
      }
    }
    // q里就是BFS序
    FOR(i, 2, qr) cnt[q[i]] += cnt[fail[q[i]]], val[q[i]] += val[fail[q[i]]];
  }
  int *operator[](int x) { return tr[x]; }
} ac;

char T[S], s[S];
double f[N][S], val[S]; // f[i]表示到达状态i的最大价值
int g[N][S], h[N][S];   //方案

int get_next(int u, int c) {
  while (u && !ac[u][c]) u = ac.fail[u];
  if (ac[u][c]) return ac[u][c];
  return -1;
}
bool check(double mid) {
  FOR(i, 0, n) FOR(j, 0, ac.tot) f[i][j] = -oo, g[i][j] = 0;
  FOR(j, 0, ac.tot) val[j] = ac.val[j] - ac.cnt[j] * mid;
  f[0][0] = 0, g[0][0] = -2;
  FOR(i, 0, n - 1) FOR(j, 0, ac.tot) if (f[i][j] != -oo) {
    if (T[i + 1] == '.') {
      FOR(c, 0, 9) {
        int k = ac[j][c]; // get_next(j,c);
        if (k == -1) continue;
        double v = val[k];
        if (f[i + 1][k] > f[i][j] + v) continue;
        f[i + 1][k] = f[i][j] + v, g[i + 1][k] = j, h[i + 1][k] = c;
      }
    } else {
      int k = ac[j][T[i + 1] - '0']; // get_next(j,T[i+1]-'0');
      if (k == -1) continue;
      double v = val[k];
      if (f[i + 1][k] > f[i][j] + v) continue;
      f[i + 1][k] = f[i][j] + v, g[i + 1][k] = j, h[i + 1][k] = T[i + 1] - '0';
    }
  }
  double res = f[n][0];
  FOR(j, 0, ac.tot) if (~g[n][j]) res = max(res, f[n][j]);
  return res > 0 + eps;
}
void print(int i, int j) {
  if (!i) return;
  print(i - 1, g[i][j]);
  // printf("[%d,%d] ",i,j);
  printf("%d", h[i][j]);
}

int main() {
  scanf("%d%d", &n, &m);
  scanf("%s", T + 1);
  FOR(i, 1, m) {
    int v;
    scanf("%s%d", s, &v);
    ac.insert(s, log(v));
  }
  ac.build();

  double l = 0, r = log(1e9 + 5), mid;
  while (r - l > eps) {
    mid = l + (r - l) / 2;
    if (check(mid))
      l = mid;
    else
      r = mid;
  }

  //输出方案
  double res = -oo;
  int ans = -1;
  FOR(j, 0, ac.tot) if (~g[n][j] && f[n][j] > res) res = f[n][j], ans = j;

  assert(~ans);

  print(n, ans);
  puts("");

  return 0;
}
// ans=\sqrt[c]{\prod w_i}
// c ln ans=\sum ln w_i
// c mid <=\sum ln w_i
//\sum( ln w_i -mid ) >= 0
