#include <iostream>

using namespace std;

int n, m, d[1001], c[1001];
int f[1001][1001];

int main() {
  cin >> n >> m;
  for (int i = 1; i <= n; i++) cin >> d[i];
  for (int i = 1; i <= m; i++) cin >> c[i];
  for (int i = 1; i <= n; i++)
    for (int j = 1; j <= m; j++) f[i][j] = 999999;

  for (int i = 1; i <= n; i++) {
    for (int j = 1; j <= n; j++) {
      f[i][j] = min(f[i][j - 1], f[i - 1][j - 1] + d[i] * c[j]);
    }
  }
  cout << f[n][m];
  return 0;
}
