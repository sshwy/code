#include <cstdio>
#include <iostream>

#define SIZE 1999993
#define BASE 23

using namespace std;

int n;
int step[10][3];
bool fin = false;

struct game {
  int qp[6][8];
  bool update() {
    bool bqp[5][7] = {{0}}, ff = false;
    for (int i = 0; i < 5; i++) {
      for (int j = 1, t = 1; j <= 7; j++) {
        if (qp[i][j - 1] == qp[i][j] && qp[i][j] != 0)
          t++;
        else {
          if (t >= 3)
            for (int k = j - 1; k >= j - t; k--) bqp[i][k] = 1;
          t = 1;
        }
      }
    }
    for (int j = 0; j < 7; j++) {
      for (int i = 1, t = 1; i <= 5; i++) {
        if (qp[i - 1][j] == qp[i][j] && qp[i][j] != 0)
          t++;
        else {
          if (t >= 3)
            for (int k = i - 1; k >= i - t; k--) bqp[k][j] = 1;
          t = 1;
        }
      }
    }
    for (int i = 0; i < 5; i++)
      for (int j = 0; j < 7; j++)
        if (bqp[i][j]) qp[i][j] = 0, ff = true;

    for (int i = 0; i < 5; i++)
      for (int j = 0, d = 0; j < 7; j++)
        if (!qp[i][j])
          d++;
        else if (d)
          qp[i][j - d] = qp[i][j], qp[i][j] = 0;

    return ff;
  }
  void move(int x, int y, int g) {
    int z = x + g;
    if (z >= 5 || z < 0) return;
    qp[x][y] ^= qp[z][y] ^= qp[x][y] ^= qp[z][y];
    if (qp[x][y] == 0) {
      int d = y;
      for (int j = y + 1; j < 7; j++) qp[x][j - 1] = qp[x][j], qp[x][j] = 0;
      while (d > 0 && qp[z][d - 1] == 0) qp[z][d - 1] = qp[z][d], qp[z][d] = 0, d--;
    }
    while ((*this).update())
      ;
  }
  bool clr() {
    for (int i = 0; i < 5; i++)
      if (qp[i][0]) return false;
    return true;
  }
  bool operator==(game that) {
    for (int i = 0; i < 5; i++) {
      for (int j = 0; j < 7; j++) {
        if (qp[i][j] != that.qp[i][j]) return false;
        if (qp[i][j] == 0) break;
      }
    }
    return true;
  }
  bool check() {
    int t[20] = {0};
    for (int i = 0; i < 5; i++) {
      for (int j = 0; j < 7; j++) {
        if (!qp[i][j]) break;
        t[qp[i][j]]++;
      }
    }
    for (int i = 1; i <= 10; i++)
      if (t[i] > 0 && t[i] < 3) return false;
    return true;
  }
};

game mayan;

struct hash_map {
  struct data {
    int next;
    game u;
    int v;
  };
  int head[SIZE], cnt;
  data e[SIZE];
  int hash(game u) {
    long long res = 0;
    for (int i = 0; i < 5; i++) {
      for (int j = 0; j < 7; j++) { res = (res * BASE + u.qp[i][j]) % SIZE; }
    }
    return res;
  }
  int &operator[](game u) {
    int hu = hash(u);
    for (int i = head[hu]; i; i = e[i].next)
      if (e[i].u == u) return e[i].v;
    e[++cnt] = (data){head[hu], u, 0};
    head[hu] = cnt;
    return e[cnt].v;
  }
  int hit() {
    int t = 0;
    for (int i = 0; i < SIZE; i++) {
      if (e[head[i]].next) t++;
    }
    return t;
  }
  int visit() {
    int t = 0;
    for (int i = 0; i < SIZE; i++) {
      if (head[i]) t++;
    }
    return t;
  }
};

hash_map vis;

void play(game now, int time) {
  if (fin) return;
  if (time > n + 1) return;
  if (now.clr()) {
    if (time != n + 1)
      return;
    else {
      for (int i = 1; i <= n; i++)
        printf("%d %d %d\n", step[i][0], step[i][1], step[i][2]);
      fin = true;
      return;
    }
  }
  vis[now] = 1;
  for (int i = 0; i < 5; i++) {
    for (int j = 0; j < 7; j++) {
      if (now.qp[i][j]) {
        game nex = now;
        if (i != 4 && nex.qp[i][j] != nex.qp[i + 1][j]) {
          nex.move(i, j, 1);
          if (!vis[nex]) {
            step[time][0] = i;
            step[time][1] = j;
            step[time][2] = 1;
            play(nex, time + 1);
          }
        }
        if (fin) return;
        nex = now;
        if (i != 0 && nex.qp[i][j] != nex.qp[i - 1][j] && !nex.qp[i - 1][j]) {
          nex.move(i, j, -1);
          if (!vis[nex]) {
            step[time][0] = i;
            step[time][1] = j;
            step[time][2] = -1;
            play(nex, time + 1);
          }
        }
      }
    }
  }
  vis[now] = 0;
}

int main() {
  //	freopen("in.txt","r",stdin);
  cin >> n;
  for (int i = 0; i < 5; i++) {
    for (int j = 0; j < 7; j++) {
      cin >> mayan.qp[i][j];
      if (!mayan.qp[i][j]) break;
    }
  }
  play(mayan, 1);
  if (!fin) cout << -1;
  //    cout<<"\nvis:"<<vis.visit()<<endl;
  //    cout<<"hit:"<<vis.hit()<<endl;
  return 0;
}
