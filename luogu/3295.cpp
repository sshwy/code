#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
namespace IO {
  char nc() {
    static char bf[100000], *p1 = bf, *p2 = bf;
    return p1 == p2 && (p2 = (p1 = bf) + fread(bf, 1, 100000, stdin), p1 == p2)
               ? EOF
               : *p1++;
  }
  int rd() {
    int res = 0;
    char c = getchar();
    while (!isdigit(c)) c = getchar();
    while (isdigit(c)) res = res * 10 + c - '0', c = getchar();
    return res;
  }
} // namespace IO
/******************heading******************/
const int N = 1e5 + 5, P = 1e9 + 7;
int n, m;
bool vis[N];

struct disjoint {
  int f[N], sz[N];
  void init(int n) { FOR(i, 0, n) f[i] = i, sz[i] = 1; }
  int get(int x) { return f[x] == x ? x : f[x] = get(f[x]); }
  bool find(int x, int y) { return get(x) == get(y); }
  void merge(int x, int y) {
    x = get(x), y = get(y);
    sz[x] < sz[y] ? (f[x] = y, sz[y] += sz[x]) : (f[y] = x, sz[x] += sz[y]);
  }
} d[20]; // d[j]表示2^j的点

int main() {
  scanf("%d%d", &n, &m);
  FOR(j, 0, 17) d[j].init(n);
  FOR(i, 1, m) {
    int l1, r1, l2, r2;
    scanf("%d%d%d%d", &l1, &r1, &l2, &r2);
    ROF(j, 17, 0) {
      if (l1 + (1 << j) - 1 <= r1) {
        d[j].merge(l1, l2);
        l1 += 1 << j, l2 += 1 << j;
      }
    }
  }
  ROF(j, 16, 0) { //把d[j+1]分裂到d[j]
    if ((1 << j) > n) continue;
    FOR(i, 1, n) {
      if (i == d[j + 1].get(i)) continue;
      d[j].merge(i, d[j + 1].get(i));
      if (i + (1 << j + 1) - 1 <= n)
        d[j].merge(i + (1 << j), d[j + 1].get(i) + (1 << j));
    }
  }
  lld tot = 0, ans = 9;
  FOR(i, 1, n) {
    if (vis[d[0].get(i)]) continue;
    vis[d[0].get(i)] = 1;
    ++tot;
  }
  FOR(i, 2, tot) ans = ans * 10 % P;
  printf("%lld\n", ans);
  return 0;
}
