#include <algorithm>
#include <iostream>

using namespace std;

int n, pre = -99999, tot;
struct p {
  int s, t;
} m[10001];

bool cmp(p x, p y) {
  if (x.t != y.t) return x.t < y.t;
  return x.s > y.s;
}

int main() {
  cin >> n;
  for (int i = 1; i <= n; i++) {
    cin >> m[i].s >> m[i].t;
    if (m[i].s > m[i].t) m[i].s ^= m[i].t ^= m[i].s ^= m[i].t;
  }
  sort(m + 1, m + n + 1, cmp);
  for (int i = 1; i <= n; i++) {
    if (pre <= m[i].s) {
      tot++;
      pre = m[i].t;
    }
  }
  cout << tot;
  return 0;
}
