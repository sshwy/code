// by Sshwy
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA
const int N = 205, M = 2005, P = 998244353;
int n, m;
int a[N][M], f[N][N * 2];
long long b[N];

int main() {
  scanf("%d%d", &n, &m);
  FOR(i, 1, n) { FOR(j, 1, m) scanf("%d", &a[i][j]); }
  long long tot = 1;
  FOR(i, 1, n) {
    FOR(j, 1, m) b[i] += a[i][j];
    b[i] %= P;
    tot = tot * (b[i] + 1) % P;
  }
  tot--;
  FOR(k, 1, m) {
    f[0][N + 0] = 1;
    FOR(i, 1, n) {
      FOR(j, -i, i) {
        f[i][N + j] = (f[i - 1][N + j + 1] * 1ll * (b[i] % P - a[i][k] + P) % P +
                          f[i - 1][N + j - 1] * 1ll * a[i][k] % P + f[i - 1][N + j]) %
                      P;
        // if(f[i][N+j])printf("f[%d,%d]=%d\n",i,j,f[i][N+j]);
      }
    }
    long long s = 0;
    FOR(i, 1, n) s = (s + f[n][N + i]) % P;
    tot = (tot - s + P) % P;
  }
  printf("%lld\n", tot);
  return 0;
}
