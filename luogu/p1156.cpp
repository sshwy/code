#include <bits/stdc++.h>
using namespace std;
const int G = 102;
int d, g, ans;

struct data {
  int t, f, h;
};
data t[G];
bool cmp(data x, data y) { return x.t < y.t; }

int f[G][G];
// f[i,j]前i个垃圾，在第i个垃圾尚未处理时高度为j时的最大生命值

int main() {
  scanf("%d%d", &d, &g);
  for (int i = 1; i <= g; i++) scanf("%d%d%d", &t[i].t, &t[i].f, &t[i].h);
  sort(t + 1, t + g + 1, cmp);
  memset(f, 0xc0, sizeof(f));
  f[0][0] = 10;
  for (int i = 1; i <= g; i++) {
    for (int j = 0; j <= d; j++) {
      if (f[i - 1][j] >= t[i].t) {
        f[i][j] = max(f[i][j], f[i - 1][j] + t[i].f);          //吃
        f[i][j + t[i].h] = max(f[i][j + t[i].h], f[i - 1][j]); //放
        if (j + t[i].h >= d) {
          printf("%d", t[i].t);
          return 0;
        }
      }
      ans = max(ans, f[i][j]);
    }
  }
  printf("%d", ans);
  return 0;
}