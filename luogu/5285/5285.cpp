// by Sshwy
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <functional>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/

int pw(int a, int m, int p) {
  int res = 1;
  while (m) m & 1 ? res = 1ll * res * a % p : 0, a = 1ll * a * a % p, m >>= 1;
  return res;
}

function<int(int)> read = [](int p) {
  int res = 0;
  char c = getchar();
  while (!isdigit(c)) c = getchar();
  while (isdigit(c)) res = (res * 10ll + c - '0') % (p - 1), c = getchar();
  return res;
};

const int P = 998244353;

void s_1_998244353() {
  int n;
  scanf("%d", &n);
  FOR(i, 1, n) {
    int x = read(P);
    printf("%d\n", pw(19, x, P));
  }
}
int main() {
  char type[20];
  scanf("%s", type);
  if (type[0] == '1' && type[1] == '_') s_1_998244353();
  return 0;
}
