#include <cstring>
#include <iostream>

using namespace std;

char text[1000001];
char word[1000001];
int lt, lw, next[1000001];

void get_next(char *s, int l) {
  next[0] = -1;
  int k = -1, j = 0;
  while (j < l) {
    if (k == -1 || s[k] == s[j])
      next[++j] = ++k;
    else
      k = next[k]; //在前k个字符前缀串中的最大公共前后缀next[k]
  }
}

void kmp_search(char *str, int ls, char *wor, int lo) {
  get_next(wor, lo); //计算状态转移图
  int i = 0, j = 0;
  while (i < ls) {
    while (j < lo && i < ls) {
      if (j == -1 || str[i] == wor[j])
        ++i, ++j;
      else
        j = next[j]; // wor[j]不一定等于wor[next[j]]!事实上wor[j-1]==wor[next[j]-1]
    }
    if (j == lo) {
      cout << i - j + 1 << endl;
      j = next[j];
    }
  }
}

// get_next的另一种版本
void get_next2(char *s, int l) {
  next[0] = next[1] = 0;
  for (int i = 1; i < l; i++) {
    int k = next[i];
    while (k != 0 && s[k] != s[i]) k = next[k];
    if (s[k] == s[i])
      next[i + 1] = k + 1;
    else
      next[i + 1] = 0;
  }
}

//对应get_next2的kmp_search2
void kmp_search2(char *str, int ls, char *wor, int lo) {
  get_next2(wor, lo);
  int k = 0;
  for (int i = 0; i < ls; i++) {
    while (k != 0 && str[i] != wor[k]) k = next[k];
    if (str[i] == wor[k]) k++;
    if (k == lo) cout << i - k + 2 << endl; //注意这里与kmp_search的区别
  }
}

int main() {
  cin >> text >> word;
  lt = strlen(text);
  lw = strlen(word);
  kmp_search2(text, lt, word, lw);
  for (int i = 1; i <= lw; i++) cout << next[i] << ' ';
  return 0;
}
