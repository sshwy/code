#include <bits/stdc++.h>
using namespace std;
long long k, p;
int main() {
  scanf("%lld%lld", &k, &p);
  long long ans = 0, t = 1;
  while (p) {
    if (p & 1ll) ans += t;
    t *= k, p >>= 1ll;
  }
  printf("%lld", ans);
  return 0;
}
