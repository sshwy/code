#include <iostream>

using namespace std;

char ope;
int res, a;
int main() {
  cin >> res;
  while (cin >> ope) {
    cin >> a;
    if (ope == '+')
      res += a;
    else
      res -= a;
  }
  cout << res;
  return 0;
}
