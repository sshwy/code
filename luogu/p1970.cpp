#include <iostream>

using namespace std;

int n, a, pre = -1;
int tot, rel = 0;

int main() {
  cin >> n;
  while (n--) {
    cin >> a;
    if (a != pre && pre != -1) {
      if (a > pre && rel != 1)
        tot++, rel = 1;
      else if (a < pre && rel != -1)
        tot++, rel = -1;
    }
    pre = a;
  }
  cout << tot + 1;
  return 0;
}
