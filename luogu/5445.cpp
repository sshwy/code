// by Sshwy
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 3e5 + 5, Q = 3e5 + 5;

int n, q;
int a[N];

struct fenwick_suf {
  long long c[N], n;
  fenwick_suf() { n = N - 1; }
  void add(int pos, int v) {
    pos = n - pos + 1; //规避了pos=0的情况
    for (int i = pos; i <= n; i += i & -i) c[i] += v;
  }
  long long suf(int pos) {
    pos = n - pos + 1;
    int res = 0;
    for (int i = pos; i > 0; i -= i & -i) res += c[i];
    return res;
  }
} B;

namespace seg {
  set<pii> s;
  typedef set<pii>::iterator IT;

  void assign(int x, int v) {
    if (v == 1) { // insert
      if (s.empty())
        s.insert({x, x});
      else {
        IT nex = s.upper_bound({x, x});
        if (nex == s.end()) {
          IT pre = --nex;
          if (pre->se == x - 1)
            s.insert({pre->fi, x}), s.erase(pre);
          else
            s.insert({x, x});
        } else if (nex == s.begin()) {
          if (nex->fi == x + 1)
            s.insert({x, nex->se}), s.erase(nex);
          else
            s.insert({x, x});
        } else {
          IT pre = nex;
          --pre;
          if (pre->se == x - 1 && nex->fi == x + 1)
            s.insert({pre->fi, nex->se}), s.erase(nex), s.erase(pre);
          else if (pre->se == x - 1)
            s.insert({pre->fi, x}), s.erase(pre);
          else if (nex->fi == x + 1)
            s.insert({x, nex->se}), s.erase(nex);
          else
            s.insert({x, x});
        }
      }
    } else {
      IT pos = --s.upper_bound({x, n + 1});
      if (pos->fi == pos->se)
        s.erase(pos);
      else if (x == pos->fi)
        s.insert({x + 1, pos->se}), s.erase(pos);
      else if (x == pos->se)
        s.insert({pos->fi, x - 1}), s.erase(pos);
      else
        s.insert({pos->fi, x - 1}), s.insert({x + 1, pos->se}), s.erase(pos);
    }
  }
  int left(int x) { return (--s.upper_bound({x, n + 1}))->fi; }
  int right(int x) { return (--s.upper_bound({x, n + 1}))->se; }
} // namespace seg

struct atom {
  int a, b, c, d;
};
atom A[N * 4 + Q * 4], t[N * 4 + Q * 4];
int lA;
long long ans[Q], la;

const int QRY = 0x3f3f3f3f;

namespace seg2 {
  void seg_add(int x, int y, int v) { A[++lA] = {x, y, v, 0}; }
  void add(int L1, int R1, int L2, int R2, int v) {
    seg_add(R1, R2, v);
    if (L2 - 1) seg_add(R1, L2 - 1, -v);
    if (L1 - 1) seg_add(L1 - 1, R2, -v);
    if (L1 - 1 && L2 - 1) seg_add(L1 - 1, L2 - 1, v);
  }
  long long query(int x, int y, int id) {
    A[++lA] = {x, y, id, 1};
    return 0;
  }
} // namespace seg2

void turn_off(int x, int t) {
  seg2::add(seg::left(x), x, x, seg::right(x), t - q);
  seg::assign(x, 0);
  a[x] = 0;
}
void turn_on(int x, int t) {
  a[x] = 1;
  seg::assign(x, 1);
  seg2::add(seg::left(x), x, x, seg::right(x), q - t);
}
void query(int x, int y, int t) {
  seg2::query(x, y, ++la);
  if (a[x] && a[y] && seg::right(x) >= y) ans[la] -= q - t;
}
int lt;

void solve(int l, int r) {
  if (l == r) return;
  int mid = (l + r) >> 1;
  solve(l, mid), solve(mid + 1, r);
  //左边的修改对右边的询问的贡献
  //左边的时间是比右边早的
  //左边按照x坐标降序排序
  //左边修改的x坐标大于等于右边询问的x坐标的，可能对询问有贡献（看y坐标）
  int p1 = l, p2 = mid + 1;
  while (p2 <= r) {
    if (A[p2].d == 0)
      p2++;
    else if (p1 <= mid && A[p1].d == 1)
      p1++;
    else if (p1 <= mid && A[p1].a >= A[p2].a)
      B.add(A[p1].b, A[p1].c), p1++;
    else
      ans[A[p2].c] += B.suf(A[p2].b), p2++;
  }
  p1 = l, p2 = mid + 1;
  while (p2 <= r) {
    if (A[p2].d == 0)
      p2++;
    else if (p1 <= mid && A[p1].d == 1)
      p1++;
    else if (p1 <= mid && A[p1].a >= A[p2].a)
      B.add(A[p1].b, -A[p1].c), p1++;
    else
      p2++;
  }
  p1 = l, p2 = mid + 1;
  lt = 0;
  while (p2 <= r || p1 <= mid) {
    if (p1 > mid)
      t[++lt] = A[p2++];
    else if (p2 > r)
      t[++lt] = A[p1++];
    else if (A[p1].a >= A[p2].a)
      t[++lt] = A[p1++];
    else
      t[++lt] = A[p2++];
  }
  FOR(i, l, r) A[i] = t[i - l + 1];
}
int main() {
  scanf("%d%d", &n, &q);
  FOR(i, 1, n) {
    scanf("%1d", &a[i]);
    if (a[i]) turn_on(i, 0);
  }
  FOR(i, 1, q) {
    char op[10];
    int x, y;
    scanf("%s%d", op, &x);
    if (op[0] == 't') {
      if (a[x])
        turn_off(x, i);
      else
        turn_on(x, i);
    } else
      scanf("%d", &y), --y, query(x, y, i);
  }
  solve(1, lA);
  FOR(i, 1, la) printf("%lld\n", ans[i]);
  return 0;
}
