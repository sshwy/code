#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
namespace IO {
  char nc() {
    static char bf[100000], *p1 = bf, *p2 = bf;
    return p1 == p2 && (p2 = (p1 = bf) + fread(bf, 1, 100000, stdin), p1 == p2)
               ? EOF
               : *p1++;
  }
  int rd() {
    int res = 0;
    char c = getchar();
    while (!isdigit(c)) c = getchar();
    while (isdigit(c)) res = res * 10 + c - '0', c = getchar();
    return res;
  }
} // namespace IO
/******************heading******************/
const int NM = 1e5 + 5, QQ = 1e5 + 5, INF = 0x3f3f3f3f;

int n, m, Q;
int r[NM], c[NM];
int d[NM];
int &R(int x, int y) { return r[(x - 1) * m + y]; }
int &C(int x, int y) { return c[(x - 1) * m + y]; }
int &D(int x, int y) { return d[(x - 1) * m + y]; }

struct dat {
  int x1, y1, x2, y2, ans, id;
  void read(int idx) {
    scanf("%d%d%d%d", &x1, &y1, &x2, &y2);
    ans = INF, id = idx;
  }
  bool in(int rL, int rR, int cL, int cR) {
    return rL <= x1 && x1 <= rR && cL <= y1 && y1 <= cR && rL <= x2 && x2 <= rR &&
           cL <= y2 && y2 <= cR;
  }
} qry[QQ];

typedef pair<int, pii> pip;

priority_queue<pip, vector<pip>, greater<pip>> q;

int dir[4][2] = {0, 1, 0, -1, 1, 0, -1, 0};
void dijk(int sx, int sy, int rL, int rR, int cL, int cR) {
  FOR(i, rL, rR) FOR(j, cL, cR) D(i, j) = INF;
  D(sx, sy) = 0;
  q.push(mk(0, mk(sx, sy)));
  while (!q.empty()) {
    pip u = q.top();
    q.pop();
    int x = u.se.fi, y = u.se.se;
    if (D(x, y) < u.fi) continue;
    FOR(i, 0, 3) {
      int nx = x + dir[i][0], ny = y + dir[i][1];
      if (nx < rL || nx > rR || ny < cL || ny > cR) continue;
      int w = i == 0   ? R(x, y)
              : i == 1 ? R(x, y - 1)
              : i == 2 ? C(x, y)
                       : C(x - 1, y);
      if (D(nx, ny) <= D(x, y) + w) continue;
      D(nx, ny) = D(x, y) + w;
      q.push(mk(D(nx, ny), mk(nx, ny)));
    }
  }
}
void update(dat &d) { d.ans = min(d.ans, D(d.x1, d.y1) + D(d.x2, d.y2)); }
void solve(int, int, int, int, int, int); //声名

dat t1[QQ], t2[QQ], t3[QQ];
void solve_row(int rL, int rR, int cL, int cR, int qL, int qR) {
  int mid = (rL + rR) >> 1;
  FOR(i, cL, cR) {
    dijk(mid, i, rL, rR, cL, cR);
    FOR(j, qL, qR) update(qry[j]);
  }
  int l1 = 0, l2 = 0, l3 = 0;
  FOR(i, qL, qR) {
    if (qry[i].in(rL, mid, cL, cR))
      t1[++l1] = qry[i];
    else if (qry[i].in(mid + 1, rR, cL, cR))
      t2[++l2] = qry[i];
    else
      t3[++l3] = qry[i];
  }
  if (rL == rR) return;
  FOR(i, 1, l1) qry[qL + i - 1] = t1[i];
  FOR(i, 1, l2) qry[qL + l1 + i - 1] = t2[i];
  FOR(i, 1, l3) qry[qL + l1 + l2 + i - 1] = t3[i];
  solve(rL, mid, cL, cR, qL, qL + l1 - 1);
  solve(mid + 1, rR, cL, cR, qL + l1, qL + l1 + l2 - 1);
}
void solve_colmn(int rL, int rR, int cL, int cR, int qL, int qR) {
  int mid = (cL + cR) >> 1;
  FOR(i, rL, rR) {
    dijk(i, mid, rL, rR, cL, cR);
    FOR(j, qL, qR) update(qry[j]);
  }
  int l1 = 0, l2 = 0, l3 = 0;
  FOR(i, qL, qR) {
    if (qry[i].in(rL, rR, cL, mid))
      t1[++l1] = qry[i];
    else if (qry[i].in(rL, rR, mid + 1, cR))
      t2[++l2] = qry[i];
    else
      t3[++l3] = qry[i];
  }
  if (cL == cR) return;
  FOR(i, 1, l1) qry[qL + i - 1] = t1[i];
  FOR(i, 1, l2) qry[qL + l1 + i - 1] = t2[i];
  FOR(i, 1, l3) qry[qL + l1 + l2 + i - 1] = t3[i];
  solve(rL, rR, cL, mid, qL, qL + l1 - 1);
  solve(rL, rR, mid + 1, cR, qL + l1, qL + l1 + l2 - 1);
}
void solve(int rL, int rR, int cL, int cR, int qL, int qR) {
  // printf("solve(%d,%d,%d,%d,%d,%d)\n",rL,rR,cL,cR,qL,qR);
  if (qL > qR) return; //没有询问
  if (rR - rL > cR - cL)
    solve_row(rL, rR, cL, cR, qL, qR);
  else
    solve_colmn(rL, rR, cL, cR, qL, qR);
}
bool cmp(dat x, dat y) { return x.id < y.id; }
int main() {
  // freopen("tourist3.in","r",stdin);
  scanf("%d%d", &n, &m);
  FOR(i, 1, n) FOR(j, 1, m - 1) scanf("%d", &R(i, j));
  FOR(i, 1, n - 1) FOR(j, 1, m) scanf("%d", &C(i, j));
  scanf("%d", &Q);
  FOR(i, 1, Q) qry[i].read(i);
  solve(1, n, 1, m, 1, Q);
  sort(qry + 1, qry + Q + 1, cmp);
  FOR(i, 1, Q) printf("%d\n", qry[i].ans);
  return 0;
}
/*
 * BUG#1:没有在rL==rR或cL==cR的时侯return
 * BUG#2:在分治时选择最短中线的时侯选成的最长中线
 */
