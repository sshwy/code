#include <iostream>

using namespace std;

int n, h, ph, tot;

int main() {
  cin >> n;
  for (int i = 1; i <= n; i++) {
    cin >> h;
    if (h < ph) tot += ph - h;
    ph = h;
  }
  cout << tot + ph;
  return 0;
}
