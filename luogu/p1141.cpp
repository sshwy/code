#include <iostream>

using namespace std;

int n, m, a, b;
char qp[1003][1003];
int tag[100000], lt;
int tqp[1003][1003]; // tag

void search(int x, int y, char pre, int t) {
  if (x < 1 || y < 1 || x > n || y > n) return;
  if (qp[x][y] == pre) return;
  if (tqp[x][y] != 0) return;

  tqp[x][y] = t;
  tag[t]++;

  search(x + 1, y, qp[x][y], t);
  search(x - 1, y, qp[x][y], t);
  search(x, y + 1, qp[x][y], t);
  search(x, y - 1, qp[x][y], t);
}

int main() {
  cin >> n >> m;
  for (int i = 1; i <= n; i++)
    for (int j = 1; j <= n; j++) cin >> qp[i][j];
  for (int i = 1; i <= m; i++) {
    cin >> a >> b;
    if (tqp[a][b] == 0) search(a, b, (char)(!(qp[a][b] - 48) + 48), ++lt);
    cout << tag[tqp[a][b]] << endl;
  }
  return 0;
}
