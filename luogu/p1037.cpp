#include <iostream>

using namespace std;

long long ans = 1;
int k, a, b;
char num[40];
int change_time[10];
int next[10][10];
bool vis[10];

int count(int x) {
  if (change_time[x] != 0) return change_time[x];
  if (vis[x]) return 0;
  vis[x] = true;
  int ct = 0;
  for (int i = 1; i <= next[x][0]; i++) {
    if (change_time[next[x][i]] == 0) change_time[next[x][i]] = count(next[x][i]);
    ct += change_time[next[x][i]];
  }
  return ct + 1;
}

int main() {
  cin >> num >> k;
  for (int i = 1; i <= k; i++) {
    cin >> a >> b;
    next[a][0]++;
    next[a][next[a][0]] = b;
  }
  for (int i = 0; i < 10; i++) change_time[i] = count(i);
  for (int i = 0; num[i] != '\0'; i++) ans *= change_time[num[i] - 48];
  cout << ans;

  cout << endl;
  for (int i = 0; i < 10; i++) cout << change_time[i];
  return 0;
}
