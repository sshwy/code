#include <iostream>
#include <queue>
#include <vector>

using namespace std;

const int INF = 99999999;
int n, m, a, b;
int d[1000001], l[1000001];
bool vising[1000001];

struct dou {
  int p, lon;
} d1;

vector<int> link[1000001];
queue<int> q;

int main() {
  cin >> n >> m;
  for (int i = 0; i < m; i++) {
    cin >> a >> b;
    link[a].push_back(b);
    link[b].push_back(a);
  }

  for (int i = 1; i <= n; i++) d[i] = INF;
  d[1] = 0;
  l[1] = 1;
  q.push(1);
  vising[1] = true;

  while (!q.empty()) {
    int now = q.front();
    q.pop();
    vising[now] = false;
    for (int i = 0; i < link[now].size(); i++) {
      int nex = link[now][i];
      if (d[nex] > d[now] + 1) {
        d[nex] = d[now] + 1;
        l[nex] = l[now];
        if (vising[nex] == false) {
          q.push(nex);
          vising[nex] = true;
        }
      } else if (d[nex] == d[now] + 1)
        l[nex] = (l[nex] + l[now]) % 100003;
    }
  }
  for (int i = 1; i <= n; i++) cout << l[i] << endl;
  return 0;
}
