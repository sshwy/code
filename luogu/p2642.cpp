#include <iostream>
#define INF -9999999

using namespace std;

long long n, mx;
long long num[1000001];
long long pre[1000001], suf[1000001];
long long mxp[1000001], mxs[1000001];

int main() {
  cin >> n;
  for (long long i = 0; i <= n + 1; i++) { pre[i] = suf[i] = mxp[i] = mxs[i] = INF; }
  for (long long i = 1; i <= n; i++) cin >> num[i];
  for (long long i = 1; i <= n; i++) {
    if (pre[i - 1] > 0)
      pre[i] = num[i] + pre[i - 1];
    else
      pre[i] = num[i];
    if (pre[i] > mxp[i - 1])
      mxp[i] = pre[i];
    else
      mxp[i] = mxp[i - 1];
  }
  for (long long i = n; i >= 1; i--) {
    if (suf[i + 1] > 0)
      suf[i] = num[i] + suf[i + 1];
    else
      suf[i] = num[i];
    if (suf[i] > mxs[i + 1])
      mxs[i] = suf[i];
    else
      mxs[i] = mxs[i + 1];
  }
  for (long long i = 2; i < n; i++)
    if (mxp[i - 1] + mxs[i + 1] > mx) mx = mxp[i - 1] + mxs[i + 1];
  cout << mx;
  return 0;
}
