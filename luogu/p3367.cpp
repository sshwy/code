#include <cstdio>
#include <iostream>

using namespace std;

int n, m, z, x, y;
int f[10001]; // father

int gf(int k) // getfather
{
  if (f[k] == k) return k;
  f[k] = gf(f[k]);
  return f[k];
}

void un(int x, int y) // union
{
  int fx = gf(x);
  int fy = gf(y);
  f[fx] = fy;
}

int main() {
  cin >> n >> m;
  for (int i = 1; i <= n; i++) f[i] = i;

  for (int i = 1; i <= m; i++) {
    cin >> z >> x >> y;
    if (z == 1) {
      un(x, y);
    } else {
      if (gf(x) == gf(y))
        cout << "Y\n";
      else
        cout << "N\n";
    }
  }
  return 0;
}
