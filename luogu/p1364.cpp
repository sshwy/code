#include <iostream>

using namespace std;

int n, root = 1, t, tot;
int p[101], l[101], r[101], v[101], c[101], f[101];

int count(int k) {
  if (k == 0) return 0;
  return c[k] = count(l[k]) + count(r[k]) + v[k];
}
int count_f(int k) {
  if (k == 0) return 0;
  return f[k] = count_f(l[k]) + count_f(r[k]) + c[l[k]] + c[r[k]];
}

int main() {
  cin >> n;
  for (int i = 1; i <= n; i++) {
    cin >> v[i] >> l[i] >> r[i];
    p[l[i]] = p[r[i]] = i;
  }
  while (p[root] != 0) root = p[root];
  t = count(root);
  count_f(root);
  while (c[root] >= t - t / 2) {
    if (c[l[root]] >= t - t / 2) {
      tot += f[r[root]] + c[r[root]] * 2 + v[root];
      root = l[root];
    } else if (c[r[root]] >= t - t / 2) {
      tot += f[l[root]] + c[l[root]] * 2 + v[root];
      root = r[root];
    } else
      break;
  }
  cout << tot + f[root];
  return 0;
}
