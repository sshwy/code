// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

const int N = 305;

int n, m, a[N][N], b[N][N], r[N], c[N], tot;
int we[N + N][N + N];

void ae(int u, int v, int w) {
  // printf("ae %d %d %d\n", u, v, w);
  we[u][v] = w;
}
void addConstraint(int i, int j, int L, int R) {
  ae(c[j], r[i], R);
  ae(r[i], c[j], -L);
}
long long dist[N + N];
int cnt[N + N];
bool vis[N + N];

queue<int> q;
bool work() {
  memset(dist, 0x3f, sizeof(dist));
  memset(vis, 0, sizeof(vis));
  memset(cnt, 0, sizeof(cnt));
  while (!q.empty()) q.pop();

  q.push(1);
  vis[1] = true;
  dist[1] = 0;

  while (!q.empty()) {
    int u = q.front();
    q.pop();
    vis[u] = false;
    FOR(v, 1, tot) if (we[u][v] < 1e9 && dist[v] > dist[u] + we[u][v]) {
      dist[v] = dist[u] + we[u][v];
      // printf("dist %d : %lld\n", v, dist[v]);
      if (!vis[v]) {
        cnt[v]++;
        if (cnt[v] == n) return false;
        q.push(v), vis[v] = true;
      }
    }
  }

  return true;
}
void go() {
  scanf("%d%d", &n, &m);
  FOR(i, 2, n) FOR(j, 2, m) scanf("%d", &b[i][j]);
  FOR(i, 1, n) a[i][1] = 0;
  FOR(i, 1, m) a[1][i] = 0;
  FOR(i, 2, n)
  FOR(j, 2, m) a[i][j] = b[i][j] - a[i][j - 1] - a[i - 1][j] - a[i - 1][j - 1];

  tot = 0;
  FOR(i, 1, n) r[i] = ++tot;
  FOR(i, 1, m) c[i] = ++tot;
  memset(we, 0x3f, sizeof(we));

  FOR(i, 1, n) FOR(j, 1, m) {
    if ((i + j) & 1)
      addConstraint(i, j, a[i][j] - 1e6, a[i][j]);
    else
      addConstraint(i, j, -a[i][j], 1e6 - a[i][j]);
  }

  if (!work()) {
    puts("NO");
    return;
  }
  puts("YES");
  FOR(i, 1, n) FOR(j, 1, m) {
    int aij = a[i][j];
    int t = dist[r[i]] - dist[c[j]];
    if ((i + j) & 1)
      aij -= t;
    else
      aij += t;
    printf("%d%c", aij, " \n"[j == m]);
  }
}

int main() {
  int t;
  scanf("%d", &t);
  FOR(i, 1, t) go();
  return 0;
}
