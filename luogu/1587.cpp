#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <climits>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <unordered_map>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
typedef long long LL;
LL I2, I6;
const int SZ = 4e6 + 5, K = 2005;
int LIM = 2e6;

bool bp[SZ];
int pn[SZ], lp;
LL mu[SZ], phi[SZ]; // prefix sum
void sieve() {
  bp[0] = bp[1] = 1;
  mu[1] = 1;
  FOR(i, 2, LIM) {
    if (!bp[i]) pn[++lp] = i, mu[i] = -1, phi[i] = i - 1;
    FOR(j, 1, lp) {
      int pj = pn[j];
      if (i * pj > LIM) break;
      bp[i * pj] = 1;
      if (i % pj == 0) {
        mu[i * pj] = 0, phi[i * pj] = phi[i] * pj;
        break;
      } else {
        mu[i * pj] = mu[i] * mu[pj], phi[i * pj] = phi[i] * phi[pj];
      }
    }
  }
  FOR(i, 2, LIM) mu[i] = mu[i] + mu[i - 1];
}

unordered_map<LL, LL> m_smu, m_g[2005];
LL Smu(LL x) {
  if (x <= LIM) return mu[x];
  if (m_smu.count(x)) return m_smu[x];
  LL l = 2, r;
  LL res = 1;
  while (l <= x) {
    r = x / (x / l);
    res = (res - Smu(x / l) * (r - l + 1));
    l = r + 1;
  }
  return m_smu[x] = res;
}

int n, m, k;
int gcd(int a, int b) { return b ? gcd(b, a % b) : a; }
LL f[K];
void get_f() {
  f[0] = 0;
  FOR(i, 1, k - 1) { f[i] = f[i - 1] + (gcd(i, k) == 1); }
}
LL F(LL x) { return 1ll * phi[k] * (x / k) + f[x % k]; }
LL G(LL n, LL k) {
  if (n == 0) return 0;
  if (k == 1) return Smu(n);
  if (m_g[k].count(n)) return m_g[k][n];
  // printf("G(%lld,%lld)\n",n,k);
  LL res = 0;
  for (LL d = 1; d * d <= k; d++) {
    if (k % d) continue;
    LL t = Smu(d) - Smu(d - 1);
    // printf("mu(%lld)=%lld\n",d,t);
    res += t * t * G(n / d, d);
    if (d * d != k) {
      LL t = Smu(k / d) - Smu(k / d - 1);
      // printf("mu(%lld)=%lld\n",k/d,t);
      res += t * t * G(n / (k / d), k / d);
    }
  }
  return m_g[k][n] = res;
}
LL solve() {
  LL res = 0;
  LL l = 1, r;
  while (l <= n && l <= m) {
    r = min(n / (n / l), m / (m / l));
    res = res + (n / l) * F(m / l) * (G(r, k) - G(l - 1, k));
    l = r + 1;
  }
  return res;
}
int main() {
  sieve();
  cin >> n >> m >> k;
  get_f();
  LL ans = solve();
  cout << ans;
  return 0;
}
