#include <iostream>
#include <queue>
#include <vector>

using namespace std;

const int INF = 99999999;
int n, m;
int maxfarm, maxcou, maxd;
int d[20001];
bool vising[20001];
struct side {
  int f, t, v = 1;
} as;

vector<int> pa[20001];
vector<side> sp;

queue<int> q;

int main() {
  cin >> n >> m;
  for (int i = 0; i < m; i++) {
    cin >> as.f >> as.t;
    sp.push_back(as);
    pa[as.f].push_back(i);
    pa[as.t].push_back(i);
  }
  for (int i = 1; i <= n; i++) d[i] = INF;

  d[1] = 0;
  q.push(1);
  vising[1] = true;

  while (!q.empty()) {
    int now = q.front();
    q.pop();
    vising[now] = false;

    for (int i = 0; i < pa[now].size(); i++) {
      int nsp = pa[now][i];
      int to = sp[nsp].t != now ? sp[nsp].t : sp[nsp].f;
      if (d[to] > d[now] + 1) {
        d[to] = d[now] + 1;
        if (vising[to] == false) {
          q.push(to);
          vising[to] = true;
        }
      }
    }
  }
  for (int i = 1; i <= n; i++) {
    if (d[i] > maxd) {
      maxd = d[i];
      maxcou = 1;
      maxfarm = i;
    } else if (d[i] == maxd) {
      maxcou++;
    }
  }
  cout << maxfarm << ' ' << maxd << ' ' << maxcou;
  return 0;
}
