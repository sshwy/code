#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
typedef pair<double, double> pdd;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 2e5 + 5;

int n;
double ans = 1e29;
pdd p[N];
double dis(pdd a, pdd b) {
  return sqrt((a.fi - b.fi) * (a.fi - b.fi) + (a.se - b.se) * (a.se - b.se));
}
bool cmp(pdd a, pdd b) { return a.se == b.se ? a.fi < b.fi : a.se < b.se; }

pdd t[N];
void solve(int l, int r) {
  // printf("solve(%d,%d,%d)\n",l,r,tag);
  if (l == r) return;
  if (l + 1 == r) return ans = min(ans, dis(p[l], p[r])), void();
  int mid = (l + r) >> 1;
  sort(p + l, p + r + 1);
  solve(l, mid), solve(mid + 1, r);
  double ml = p[mid].fi;
  int lt = 0;
  FOR(i, l, r) if (fabs(ml - p[i].fi) < ans) t[++lt] = p[i];
  sort(t + 1, t + lt + 1, cmp);
  int L = 1;
  FOR(i, 1, lt) {
    FOR(j, L, i - 1) ans = min(ans, dis(t[j], t[i]));
    while (abs(t[i].se - t[L].se) > ans) ++L;
  }
}
int main() {
  scanf("%d", &n);
  FOR(i, 1, n) scanf("%lf%lf", &p[i].fi, &p[i].se);
  sort(p + 1, p + n + 1);
  solve(1, n);
  printf("%.4lf", ans);
  return 0;
}
