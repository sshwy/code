#include <algorithm>
#include <iostream>

using namespace std;

int n, m, o, k, s, sum;
int score[101][500];

int main() {
  cin >> n >> m;
  for (int i = 1; i <= n; i++) {
    cin >> k >> s;
    score[s][0]++;
    score[s][score[s][0]] = k;
  }

  o = m * 1.5;

  for (int is = 100; is >= 1; is--) {
    if (score[is][0] != 0) {
      o -= score[is][0];
      sum += score[is][0];
      sort(score[is] + 1, score[is] + 1 + score[is][0]);
    }
    if (o <= 0) {
      cout << is << ' ' << sum << endl;
      break;
    }
  }

  o = m * 1.5;

  for (int is = 100; is >= 1; is--) {
    if (score[is][0] != 0) {
      o -= score[is][0];
      for (int j = 1; j <= score[is][0]; j++)
        cout << score[is][j] << ' ' << is << endl;
    }
    if (o <= 0) break;
  }

  return 0;
}
