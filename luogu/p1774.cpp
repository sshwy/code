#include <iostream>

using namespace std;

long long n, tot, num[500001];
long long tmp[500001];

void merge_sort(long long *a, long long l, long long r) {
  if (l == r) return;
  long long mid = (l + r) >> 1;
  merge_sort(a, l, mid);
  merge_sort(a, mid + 1, r);
  for (long long L = l, R = mid + 1, i = l; i <= r; i++) {
    if (L > mid)
      tmp[i] = a[R], R++;
    else if (R > r)
      tmp[i] = a[L], L++;
    else if (a[L] <= a[R])
      tmp[i] = a[L], L++;
    else
      tmp[i] = a[R], R++, tot = tot + mid - L + 1;
  }
  for (long long i = l; i <= r; i++) a[i] = tmp[i];
}

int main() {
  cin >> n;
  for (long long i = 1; i <= n; i++) cin >> num[i];
  merge_sort(num, 1, n);
  cout << tot;
  return 0;
}
