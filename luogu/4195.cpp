#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
namespace IO {
  char nc() {
    static char bf[100000], *p1 = bf, *p2 = bf;
    return p1 == p2 && (p2 = (p1 = bf) + fread(bf, 1, 100000, stdin), p1 == p2)
               ? EOF
               : *p1++;
  }
  int rd() {
    int res = 0;
    char c = getchar();
    while (!isdigit(c)) c = getchar();
    while (isdigit(c)) res = res * 10 + c - '0', c = getchar();
    return res;
  }
} // namespace IO
/******************heading******************/
const int SZ = 233341;
struct hash_map {
  struct data {
    int u, v, nex;
  };
  data e[SZ];
  int h[SZ], le;
  int hash(int u) { return (u % SZ + SZ) % SZ; }
  int &operator[](int u) {
    int hu = hash(u);
    for (int i = h[hu]; i; i = e[i].nex)
      if (e[i].u == u) return e[i].v;
    return e[++le] = (data){u, -1, h[hu]}, h[hu] = le, e[le].v;
  }
  void clear() { memset(h, 0, sizeof(h)), le = 0; }
} h;
int a, p, b;
int gcd(int a, int b) { return b ? gcd(b, a % b) : a; }
int pw(int a, int m, int p) {
  int res = 1;
  while (m) m & 1 ? res = 1ll * res * a % p : 0, a = 1ll * a * a % p, m >>= 1;
  return res;
}
int exgcd(int a, int b, int &x, int &y) {
  if (!b) return x = 1, y = 0, a;
  int t = exgcd(b, a % b, y, x);
  return y = y - (a / b) * x, t;
}
int inv(int a, int p) {
  int b, t, g = exgcd(a, p, b, t);
  if (g > 1) return -1;
  return ((b % p) + p) % p;
}
int bsgs(int a, int b, int p) {
  a %= p, b %= p, h.clear();
  if (b == 1) return 0;
  if (!a && !b) return 1;
  if (!a) return -1;
  int t = sqrt(p) + 0.5, cur = b, q = 1;
  FOR(i, 0, t) h[cur] = i, cur = 1ll * cur * a % p;
  cur = pw(a, t, p);
  FOR(i, 0, t) {
    if (h[q] != -1 && i * t - h[q] >= 0) return i * t - h[q];
    q = 1ll * q * cur % p;
  }
  return -1;
}
int exbsgs(int a, int b, int p) {
  int d = 0, f = 1, g;
  while ((g = gcd(a, p)) > 1) {
    if (b % g) return -1;
    ++d, f = 1ll * f * g % p, b /= g, p /= g;
  }
  int ia = inv(a, p);
  f = 1ll * f * pw(ia, d, p) % p;
  b = 1ll * b * f % p;
  int res = bsgs(a, b, p);
  return ~res ? res + d : -1;
}
int go() {
  int ans = exbsgs(a, b, p);
  if (~ans)
    printf("%d\n", ans);
  else
    puts("No Solution");
  return 0;
}
int main() {
  while (~scanf("%d%d%d", &a, &p, &b)) {
    if (a || b || p)
      go();
    else
      return 0;
  }
  return 0;
}
