#include <iostream>
#define INF 9999999

using namespace std;

int t, c, ts, te, x, y, z;
int w[2501][2501];
int dts[2501];
bool vis[2501]; // already visit

int main() {
  cin >> t >> c;
  ts = 1;
  te = t;
  for (int i = 1; i <= t; i++) {
    for (int j = 1; j <= t; j++) w[i][j] = INF;
    w[i][i] = 0;
  }

  for (int i = 1; i <= c; i++) {
    cin >> x >> y >> z;
    if (z < w[x][y]) w[x][y] = w[y][x] = z;
  }

  for (int i = 1; i <= t; i++) dts[i] = w[ts][i];
  dts[ts] = 0;
  //    vis[ts]=true;
  for (int i = 1; i <= t; i++) {
    int tmp = INF, p = 0; // p:unvisit nearest point
    for (int j = 1; j <= t; j++) {
      if (dts[j] < tmp && vis[j] == 0) {
        p = j;
        tmp = dts[j];
      }
    }
    vis[p] = true;
    for (int j = 1; j <= t; j++) {
      if (vis[j] == 0) // if unvisit
      {
        dts[j] = (dts[j] < dts[p] + w[p][j] ? dts[j] : dts[p] + w[p][j]);
      }
    }
  }
  cout << dts[te];
  return 0;
}
