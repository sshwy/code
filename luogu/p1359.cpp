#include <iostream>

using namespace std;

int n;
int w[201][201];

int main() {
  cin >> n;
  for (int i = 1; i <= n; i++) {
    for (int j = i + 1; j <= n; j++) { cin >> w[i][j]; }
  }
  for (int k = 1; k <= n; k++) {
    for (int i = 1; i <= k; i++) {
      for (int j = k; j <= n; j++) {
        if (w[i][j] > w[i][k] + w[k][j]) w[i][j] = w[i][k] + w[k][j];
      }
    }
  }
  cout << w[1][n];
  return 0;
}
