// by Sshwy
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 2.5e5 + 5, INF = 0x3f3f3f3f;

struct qxx {
  int nex, t, v;
} e[N * 2];
int h[N], le = 1;
void add_path(int f, int t, int v) { e[++le] = {h[f], t, v}, h[f] = le; }
#define FORe(i, _u, _v, _w) \
  for (int i = h[_u], _v, _w; _v = e[i].t, _w = e[i].v, i; i = e[i].nex)

int n;
int fa[N][21], w[N][21], id[N], totid, dep[N];
bool tag[N];
vector<pii> g[N];

void dfs1(int u, int p, int val) {
  fa[u][0] = p, w[u][0] = val, id[u] = ++totid, dep[u] = dep[p] + 1;
  FOR(j, 1, 20) {
    w[u][j] = min(w[u][j - 1], w[fa[u][j - 1]][j - 1]);
    fa[u][j] = fa[fa[u][j - 1]][j - 1];
    if (!fa[u][j]) break;
  }
  for (pii x : g[u]) {
    if (x.fi == p) continue;
    dfs1(x.fi, u, x.se);
  }
}

int lca(int x, int y) {
  if (x == y) return x;
  if (dep[x] < dep[y]) swap(x, y);
  ROF(j, 20, 0) if (dep[x] - (1 << j) >= dep[y]) x = fa[x][j];
  if (x == y) return x;
  ROF(j, 20, 0) if (fa[x][j] != fa[y][j]) x = fa[x][j], y = fa[y][j];
  return fa[x][0];
}
int W(int x, int y) {
  if (x == y) return INF;
  int res = INF;
  if (dep[x] < dep[y]) swap(x, y);
  ROF(j, 20, 0)
  if (dep[x] - (1 << j) >= dep[y]) res = min(res, w[x][j]), x = fa[x][j];
  if (x == y) return res;
  ROF(j, 20, 0)
  if (fa[x][j] != fa[y][j])
    res = min(res, w[x][j]), res = min(res, w[y][j]), x = fa[x][j], y = fa[y][j];
  res = min(res, w[x][0]), res = min(res, w[y][0]);
  return res;
}

int s[N], tp, R[N], lR;
void Vadd(int u, int v, int w) {
  // printf("(%d,%d,%d)\n",u,v,w);
  if (!h[u]) R[++lR] = u;
  if (!h[v]) R[++lR] = v;
  add_path(u, v, w), add_path(v, u, w);
};
void build_virtual_tree(vector<int> &V) {
  int root = V[0];
  for (int u : V) root = lca(root, u);
  root = 1;
  s[tp = 1] = root;
  for (int u : V) {
    if (tp) {
      int y = lca(s[tp], u);
      while (tp > 1 && dep[y] < dep[s[tp - 1]])
        Vadd(s[tp - 1], s[tp], W(s[tp - 1], s[tp])), --tp; // 1. 删掉s[tp]
      if (y == s[tp])
        ; // 4. 顺接u
      else {
        assert(tp > 1);
        if (y == s[tp - 1])
          Vadd(y, s[tp], W(y, s[tp])), --tp; // 3. 删掉s[tp]，顺接u
        else
          Vadd(y, s[tp], W(y, s[tp])),
              s[tp] = y; // 2. 删掉s[tp]，把lca(s[tp],u)加上，然后顺接u
      }
    }
    s[++tp] = u;
  }
  while (tp > 1) Vadd(s[tp - 1], s[tp], W(s[tp - 1], s[tp])), --tp;
}

long long dfsVT(int u, int p) {
  // printf("dfsVT(%d,%d)\n",u,p);
  if (tag[u]) return INF;
  long long res = 0;
  FORe(i, u, v, val) {
    if (v == p) continue;
    res += min(0ll + val, dfsVT(v, u));
  }
  return res;
}

void solve() {
  // printf("solve()\n");
  int k;
  scanf("%d", &k);
  vector<int> v;
  v.clear();
  FOR(i, 1, k) {
    int x;
    scanf("%d", &x);
    v.pb(x);
  }
  sort(v.begin(), v.end(), [](int x, int y) { return id[x] < id[y]; });

  build_virtual_tree(v);

  for (int x : v) tag[x] = 1;
  tag[1] = 0;
  printf("%lld\n", dfsVT(1, 0));
  for (int x : v) tag[x] = 0;

  le = 1;
  FOR(i, 1, lR) h[R[i]] = 0;
  lR = 0;
}
int main() {
  scanf("%d", &n);
  FOR(i, 1, n - 1) {
    int u, v, w;
    scanf("%d%d%d", &u, &v, &w);
    g[u].pb({v, w}), g[v].pb({u, w});
  }
  dfs1(1, 0, 0);
  int m;
  scanf("%d", &m);
  FOR(i, 1, m) solve();
  return 0;
}
