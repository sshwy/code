#include <cstdio>
#include <cstdlib>
#include <ctime>
using namespace std;

int main() {
  system("g++ 4383.cpp -o .usr");
  system("g++ std.cpp -o .std");
  system("g++ gen.cpp -o .gen");
  int t = 0;
  for (t = 1; t <= 10000; t++) {
    system("./.gen > .fin");
    system("./.usr < .fin > .fout");
    system("./.std < .fin > .fstd");
    if (system("diff -w .fout .fstd")) break;
    printf("AC#%d\n", t);
  }
  printf("WA#%d\n", t);
  return 0;
}
