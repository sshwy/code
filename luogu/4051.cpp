#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 2e5 + 5;

int n;
char s[N];
int sa[N], rk[N];
int t[N], bin[N], sz;

void get_sa() {
  FOR(i, 0, sz) bin[i] = 0;
  FOR(i, 1, n) bin[rk[i]]++;
  FOR(i, 1, sz) bin[i] += bin[i - 1];
  ROF(i, n, 1) sa[bin[rk[t[i]]]--] = t[i];
}
void make() {
  sz = max(n, 300);
  FOR(i, 1, n) t[i] = i, rk[i] = s[i] - '0' + 1;
  get_sa();
  for (int j = 1; j <= n; j <<= 1) {
    int tot = 0;
    FOR(i, n - j + 1, n) t[++tot] = i;
    FOR(i, 1, n) if (sa[i] - j > 0) t[++tot] = sa[i] - j;
    get_sa();
    t[sa[1]] = tot = 1;
    FOR(i, 2, n)
    t[sa[i]] = rk[sa[i - 1]] == rk[sa[i]] && rk[sa[i - 1] + j] == rk[sa[i] + j]
                   ? tot
                   : ++tot;
    swap(t, rk);
  }
}

int main() {
  scanf("%s", s + 1);
  n = strlen(s + 1);
  memcpy(s + 1 + n, s + 1, sizeof(char) * n);
  n *= 2;

  make();
  FOR(i, 1, n) if (sa[i] <= n / 2) printf("%c", s[sa[i] + n / 2 - 1]);
  return 0;
}
