#include <cstdio>
#include <iostream>

using namespace std;

char s[50];
int n, l, tot = 9999999;

int snum[50][50];

void search(int pre_sum, int pre, int time) {
  if (pre_sum > n) return;
  if (time > tot) return;
  //	printf(">>search(%d,%d,%d);\n",pre_sum,pre,time);
  if (pre == l) {
    if (pre_sum == n) tot = (tot < time ? tot : time);
    return;
  }
  for (int i = pre + 1; i <= l; i++) {
    search(pre_sum + snum[pre + 1][i], i, time + 1);
  }
}

int main() {
  cin >> s + 1 >> n;
  while (s[++l])
    ;
  l--;
  for (int i = 1; i <= l; i++)
    for (int j = 1; j <= l; j++) snum[i][j] = 9999999;
  for (int st = 1; st <= l; st++) {
    int num = 0;
    for (int j = st; j <= l && num <= n; j++) {
      num = num * 10 + s[j] - 48;
      snum[st][j] = num;
    }
  }
  search(0, 0, 0);
  if (tot == 9999999)
    cout << -1;
  else
    cout << tot - 1;
  return 0;
}
