#include <iostream>
#include <vector>

using namespace std;

int n, m, t, q, u, lt;
int id[200], tid[200], vis_time[200];
char a, b;
char topo_ans[100];
bool f1;
vector<int> linker[200];

bool topo() {
  //	初始化tid[],vis_time[],topo_ans[],f1
  for (int i = 'A'; i < 'A' + n; i++) tid[i] = id[i], vis_time[i] = 0;
  lt = 0;
  f1 = true;
  for (int now_time = 1; now_time <= n; now_time++) {
    //		如果有多个未被访问的入度为0的点->return 0;
    t = q = 0;
    for (int i0 = 'A'; i0 < 'A' + n; i0++)
      if (tid[i0] == 0 && vis_time[i0] == 0) t++, u = i0;
    if (t > 1) return 0;
    //		将入度为0的点u存入topo_ans中
    topo_ans[lt] = u;
    topo_ans[++lt] = '\0';
    for (int i = 0; i < linker[u].size(); i++) {
      int v = linker[u][i];
      if (vis_time[v] != 0) {
        cout << "Inconsistency found after " << now_time - vis_time[v]
             << " relations.";
        //				结束程序
        f1 = false;
        return 0;
      } else {
        vis_time[v] = now_time;
        tid[v]--;
      }
    }
  }
  return true;
}

int main() {
  cin >> n >> m;
  for (int i = 1; i <= m; i++) {
    cin >> a >> b >> b;
    linker[a].push_back(b);
    id[b]++;
    if (topo()) {
      cout << "Sorted sequence determined after " << lt << " relations: " << topo_ans
           << ".";
      return 0;
    } else if (f1 == false) {
      return 0;
    }
  }
  cout << "Sorted sequence cannot be determined.";
  return 0;
}
