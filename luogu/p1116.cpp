#include <algorithm>
#include <iostream>
using namespace std;

int n, d, num[1000001], sum;

int main() {
  cin >> n >> d;
  for (int i = 1; i <= n; i++) cin >> num[i];
  sort(num + 1, num + n + 1);
  for (int i = 1; i < n; i++) {
    for (int j = i + 1; j <= n; j++) {
      if (num[j] - num[i] <= d) {
        sum++;
      } else
        break;
    }
  }
  cout << sum;
  return 0;
}
