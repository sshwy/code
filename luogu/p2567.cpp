#include <algorithm>
#include <cstdio>
#define int long long
using namespace std;
const int N = 1e5;
int a, b, ans;
int c[N], lc;

void dfs(int x) {
  c[++lc] = x;
  if (x * 10 + 6 > b) return;
  dfs(x * 10 + 6), dfs(x * 10 + 8);
}
void sieve() {
  int tmp[N], lt = 0;
  for (int i = 1; i <= lc; i++) {
    for (int j = 1; j <= lt; j++) {
      if (c[i] % tmp[j]) continue;
      c[i] = 0;
      break;
    }
    if (c[i] > b / 2) ans += b / c[i] - (a - 1) / c[i], c[i] = 0;
    if (c[i]) tmp[++lt] = c[i];
  }
  for (int i = 1; i <= lt; i++) c[i] = tmp[i];
  lc = lt;
}
void prework() {
  dfs(0);
  sort(c + 1, c + lc + 1);
  sieve();
}
int gcd(int a, int b) { return b ? gcd(b, a % b) : a; }
void dfs(int k, int last, int lcm) {
  if (k != 0) ans += (k % 2 * 2 - 1) * (b / lcm - (a - 1) / lcm);
  for (int i = last - 1; i > 0; i--) { //倒序
    int g = gcd(lcm, c[i]);
    g = lcm * c[i] / g;
    if (g <= b) dfs(k + 1, i, g);
  }
}
signed main() {
  scanf("%lld%lld", &a, &b);
  prework();
  dfs(0, lc + 1, 1);
  printf("%lld", ans);
  return 0;
}
