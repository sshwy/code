#include <iostream>

using namespace std;

char str[101];
int letter[128];
int l;

int main() {
  while (cin >> str) {
    for (int i = 0; str[i] != '\0'; i++) { letter[str[i]]++; }
  }
  for (char a = 'A'; a <= 'Z'; a++) {
    if (letter[a] > l) l = letter[a];
  }
  for (int il = l; il >= 1; il--) {
    for (char c = 'A'; c <= 'Z'; c++) {
      if (il > letter[c])
        cout << "  ";
      else
        cout << "* ";
    }
    cout << endl;
  }
  for (char c = 'A'; c <= 'Z'; c++) cout << c << ' ';
  return 0;
}
