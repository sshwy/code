// by Sshwy
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int P = 998244353, K = 520, I2 = (P + 1) / 2, I6 = (P + 1) / 6;

int pw(int a, long long m, int p = P) {
  int res = 1;
  while (m) m & 1 ? res = 1ll * res * a % p : 0, a = 1ll * a * a % p, m >>= 1;
  return res;
}

template <int R> class Sqrt {
  int a, b; // a+b\sqrt{R}
public:
  Sqrt(long long _a = 0, long long _b = 0) { a = _a % P, b = _b % P; }
  Sqrt operator+(Sqrt x) { return Sqrt((a + x.a) % P, (b + x.b) % P); }
  Sqrt &operator+=(Sqrt x) { return *this = *this + x, *this; }
  Sqrt operator-(Sqrt x) { return Sqrt((a - x.a + P) % P, (b - x.b + P) % P); }
  friend Sqrt operator-(int x, Sqrt y) { return Sqrt<R>(x, 0) - y; }
  Sqrt &operator-=(Sqrt x) { return *this = *this - x, *this; }
  Sqrt operator*(Sqrt x) {
    return Sqrt((a * 1ll * x.a % P + b * 1ll * x.b % P * R) % P,
        (a * 1ll * x.b % P + x.a * 1ll * b) % P);
  }
  Sqrt operator*(int x) { return Sqrt(a * 1ll * x % P, x * 1ll * b % P); }
  Sqrt operator*(long long x) {
    return x %= P, Sqrt(a * 1ll * x % P, x * 1ll * b % P);
  }
  Sqrt &operator*=(Sqrt x) { return *this = *this * x, *this; }
  Sqrt inv() {
    int x = pw((a * 1ll * a % P - R * 1ll * b % P * b % P + P) % P, P - 2);
    return Sqrt(a * 1ll * x % P, (P - b) % P * 1ll * x % P);
  }
  Sqrt operator/(Sqrt x) { return *this * x.inv(); }
  Sqrt operator/(int x) { return *this * pw(x, P - 2); }
  Sqrt &operator/=(Sqrt x) { return *this = *this / x, *this; }
  bool operator==(Sqrt x) { return a == x.a && b == x.b; }
  operator int() {
    assert(!b);
    return a;
  }
  void print() { printf("(%d,%d)", a, b); }
};
template <int R> Sqrt<R> pw(Sqrt<R> a, long long m) {
  Sqrt<R> res(1, 0);
  while (m) m & 1 ? res = res * a, 0 : 0, a = a * a, m >>= 1;
  return res;
}

int T, m;
int s[K][K], c[K][K];

void init() {
  s[0][0] = 1;
  c[0][0] = 1;
  FOR(i, 1, K - 1) {
    s[i][0] = 0;
    c[i][0] = 1;
    FOR(j, 1, i)
    s[i][j] = (s[i - 1][j - 1] + s[i - 1][j] * 1ll * (i - 1)) % P,
    c[i][j] = (c[i - 1][j - 1] + c[i - 1][j]) % P;
  }
}

int sig(int x) { return 1 - 2 * (x & 1); }
int calc(long long n, int k) {

  if (m == 2) {
    Sqrt<5> A, B, X(I2, I2), Y(I2, P - I2), a[K], b[K], x[K], y[K], sum;
    A = Sqrt<5>(0, 1).inv();
    B = 0 - A;

    a[0] = b[0] = x[0] = y[0] = 1;
    FOR(i, 1, k) a[i] = a[i - 1] * A, b[i] = b[i - 1] * B;
    FOR(i, 1, k) x[i] = x[i - 1] * X, y[i] = y[i - 1] * Y;

    FOR(j, 0, k) {
      int t = sig(k - j);
      Sqrt<5> s1;
      FOR(d, 0, j) {
        Sqrt<5> s = x[d] * y[j - d];
        if (s == s * s)
          s1 += a[d] * b[j - d] * s * (s * (n + 1)) * c[j][d];
        else
          s1 += a[d] * b[j - d] * s * (1 - pw(s, n + 1)) / (1 - s) * c[j][d];
      }
      sum += s1 * s[k][j] * ((P + t) % P);
    }
    int fack = 1;
    FOR(i, 1, k) fack = 1ll * fack * i % P;
    sum /= fack;
    return int(sum);
  } else {
    n /= 2;
    Sqrt<3> A(I2, I6), B(I2, P - I6), X(2, 1), Y(2, P - 1), a[K], b[K], x[K], y[K],
        sum;

    a[0] = b[0] = x[0] = y[0] = 1;
    FOR(i, 1, k) a[i] = a[i - 1] * A, b[i] = b[i - 1] * B;
    FOR(i, 1, k) x[i] = x[i - 1] * X, y[i] = y[i - 1] * Y;

    FOR(j, 0, k) {
      int t = sig(k - j);
      Sqrt<3> s1;
      FOR(d, 0, j) {
        Sqrt<3> s = x[d] * y[j - d];
        if (s == s * s)
          s1 += a[d] * b[j - d] * (s * (n + 1)) * c[j][d];
        else
          s1 += a[d] * b[j - d] * (1 - pw(s, n + 1)) / (1 - s) * c[j][d];
      }
      sum += s1 * s[k][j] * ((P + t) % P);
    }
    int fack = 1;
    FOR(i, 1, k) fack = 1ll * fack * i % P;
    sum /= fack;
    return int(sum);
  }
}
void go() {
  long long L, R;
  int k;
  scanf("%lld%lld%d", &L, &R, &k);
  int ans = (calc(R, k) - calc(L - 1, k) + P) % P;
  ans = 1ll * ans * pw((R - L + 1) % P, P - 2) % P;
  printf("%d\n", ans);
}
int main() {
  scanf("%d%d", &T, &m);
  init();
  FOR(i, 1, T) go();
  return 0;
}
