#include <iostream>
#include <queue>
#include <string>

#define swap(number1, number2) number1 ^= number2 ^= number1 ^= number2

using namespace std;

bool vis[20000000];

struct board {
  int prt[9];
  int tim;
  string ope;
  void a() {
    swap(prt[1], prt[8]);
    swap(prt[2], prt[7]);
    swap(prt[3], prt[6]);
    swap(prt[4], prt[5]);
    ope.push_back('A');
    ++tim;
  }
  void b() {
    swap(prt[3], prt[4]);
    swap(prt[2], prt[3]);
    swap(prt[1], prt[2]);
    swap(prt[5], prt[6]);
    swap(prt[6], prt[7]);
    swap(prt[7], prt[8]);
    ope.push_back('B');
    ++tim;
  }
  void c() {
    swap(prt[2], prt[3]);
    swap(prt[2], prt[6]);
    swap(prt[2], prt[7]);
    ope.push_back('C');
    ++tim;
  }
  bool operator==(board that) {
    for (int i = 1; i <= 8; i++)
      if (prt[i] != that.prt[i]) return false;
    return true;
  }
  void print() {
    cout << tim << endl;
    for (int i = 0; i < tim; i++) {
      cout << ope[i];
      if (!((i + 1) % 60)) cout << endl;
    }
  }
};
board start = {{0, 1, 2, 3, 4, 5, 6, 7, 8}};
board final;

queue<board> q;

int hash(board u) {
  int res = 0;
  for (int i = 1; i <= 8; i++) res = res * 8 + u.prt[i] - 1;
  return res;
}

int main() {
  for (int i = 1; i <= 8; i++) cin >> final.prt[i];
  q.push(start);
  vis[hash(start)] = true;
  while (!q.empty()) {
    board now = q.front();
    q.pop();
    board na = now, nb = now, nc = now;
    if (now == final) {
      now.print();
      break;
    }
    na.a();
    nb.b();
    nc.c();
    if (!vis[hash(na)]) {
      vis[hash(na)] = true;
      q.push(na);
    }
    if (!vis[hash(nb)]) {
      vis[hash(nb)] = true;
      q.push(nb);
    }
    if (!vis[hash(nc)]) {
      vis[hash(nc)] = true;
      q.push(nc);
    }
  }
  return 0;
}
