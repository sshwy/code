#include <iostream>

using namespace std;

int n, m, tot, a, b;

struct graph {
  struct path {
    int nex, t;
  };
  int head[100001], cnt;
  path e[200001];
  int id[100001], f[100001];

  void add_path(int f, int t) {
    e[++cnt] = (path){head[f], t};
    head[f] = cnt;
    id[t]++;
  }
  int food_link(int st) {
    if (f[st]) return f[st];
    int s = 0;
    for (int i = head[st]; i; i = e[i].nex) s += food_link(e[i].t);
    return f[st] = (s ? s : 1);
  }
};

graph g;

int main() {
  cin >> n >> m;
  for (int i = 1; i <= m; i++) {
    cin >> a >> b;
    g.add_path(a, b);
  }
  for (int i = 1; i <= n; i++)
    if (!g.id[i] && g.head[i]) tot += g.food_link(i);
  cout << tot;
  return 0;
}
