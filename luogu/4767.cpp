// by Sshwy
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 5000, M = 500, INF = 0x3f3f3f3f;

int n, m;
int a[N], w[N][N];

void solve(int *f, int *g, int l, int r, int kl, int kr) {
  if (l > r) return;
  int mid = (l + r) >> 1, k = -1;
  g[mid] = INF;
  FOR(i, kl, min(kr, mid - 1)) {
    int t = f[i] + w[i + 1][mid];
    if (g[mid] > t) g[mid] = t, k = i;
  }
  assert(k != -1);
  solve(f, g, l, mid - 1, kl, k);
  solve(f, g, mid + 1, r, k, kr);
}

int f[2][N];
int main() {
  scanf("%d%d", &n, &m);
  FOR(i, 1, n) scanf("%d", &a[i]);
  sort(a + 1, a + n + 1);
  FOR(len, 1, n - 1)
  FOR(i, 1, n - len) w[i][i + len] = w[i + 1][i + len - 1] + a[i + len] - a[i];
  FOR(i, 1, n) f[0][i] = INF;
  FOR(i, 1, m) {
    solve(f[0], f[1], 1, n, 0, n);
    swap(f[0], f[1]);
  }
  printf("%d\n", f[0][n]);
  return 0;
}
