#include <bits/stdc++.h>
using namespace std;
int l, n, k, L = 0, R = 0;
int a[100005];
bool check(int p) {
  int t = 0;
  for (int i = 1; i < n; i++) t += a[i] / p - (a[i] % p ? 0 : 1);
  return t <= k;
}
int main() {
  scanf("%d%d%d", &l, &n, &k);
  for (int i = 1; i <= n; i++) scanf("%d", &a[i]);
  for (int i = 1; i < n; i++) a[i] = a[i + 1] - a[i], R = max(R, a[i]);
  while (L < R) {
    int mid = (L + R) >> 1;
    if (check(mid))
      R = mid;
    else
      L = mid + 1;
  }
  printf("%d", L);
  return 0;
}
