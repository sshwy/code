#include <iostream>

using namespace std;

int n, m, l, r;
bool bp[1000001];

void pdb(int k) {
  bp[0] = bp[1] = true;
  for (int i = 2; i <= k; i++)
    if (!bp[i])
      for (int j = 2; j <= k / i; i++) bp[i * j] = true;
}

int main() {
  cin >> n >> m;
  pdb(m);
  for (int i = 1; i <= n; i++) {
    cin >> l >> r;
    if (r > m) {
      cout << "Crossing the line";
      continue;
    }
    int tot = 0;
    for (int j = l; j <= r; j++)
      if (!bp[j]) tot++;
    cout << tot << endl;
  }
  return 0;
}
