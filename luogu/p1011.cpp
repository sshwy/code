#include <iostream>
using namespace std;
int a, n, m, x;
int F[100] = {0, 1, 1};
int f(int k) {
  if (F[k]) return F[k];
  return F[k] = f(k - 1) + f(k - 2);
}
int main() {
  cin >> a >> n >> m >> x;
  cout << (f(x - 2) + 1) * a +
              (f(x - 1) - 1) * (m - (f(n - 3) + 1) * a) / (f(n - 2) - 1);
  return 0;
}
