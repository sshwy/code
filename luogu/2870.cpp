#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 1e6 + 5;

int n;
char s[N];

int sa[N], rk[N];
int t[N], bin[N], sz;

void get_sa() {
  FOR(i, 0, sz) bin[i] = 0;
  FOR(i, 1, n) bin[rk[i]]++;
  FOR(i, 1, sz) bin[i] += bin[i - 1];
  ROF(i, n, 1) sa[bin[rk[t[i]]]--] = t[i];
}
void make() {
  sz = max(n, 27);
  FOR(i, 1, n) t[i] = i, rk[i] = s[i] - 'A' + 1;
  get_sa();
  for (int j = 1; j <= n; j <<= 1) {
    int tot = 0;
    FOR(i, n - j + 1, n) t[++tot] = i;
    FOR(i, 1, n) if (sa[i] - j > 0) t[++tot] = sa[i] - j;
    get_sa();
    t[sa[1]] = tot = 1;
    FOR(i, 2, n)
    t[sa[i]] = rk[sa[i]] == rk[sa[i - 1]] && rk[sa[i] + j] == rk[sa[i - 1] + j]
                   ? tot
                   : ++tot;
    swap(t, rk);
  }
}

int main() {
  scanf("%d", &n);
  FOR(i, 1, n) scanf("%s", s + i);
  memcpy(s + 1 + n + 1, s + 1, sizeof(char) * n);
  reverse(s + n + 2, s + n + 2 + n);
  s[n + 1] = 'Z' + 1;

  n = n * 2 + 1;
  s[n + 1] = 0;
  // printf("%s\n",s+1);
  make();
  // FOR(i,1,n)printf("%2d%c",sa[i]," \n"[i==n]);
  // FOR(i,1,n)printf("%2d%c",rk[i]," \n"[i==n]);
  n /= 2;
  int l = 1, r = n, tot = 0;
  ;
  while (l <= r) {
    if (s[l] < s[r])
      printf("%c", s[l++]);
    else if (s[r] < s[l])
      printf("%c", s[r--]);
    else {
      // printf("(%d vs %d)\n",l,n+n+2-r);
      if (rk[l] < rk[n + n + 2 - r])
        printf("%c", s[l++]);
      else
        printf("%c", s[r--]);
    }
    // printf("[l=%d,r=%d]\n",l,r);
    ++tot;
    if (tot % 80 == 0) puts("");
  }
  return 0;
}
