#include <algorithm>
#include <cstdio>
#include <iostream>

using namespace std;

long long mx1, my1, mx2, my2, x, y;
long long n, d1, d2;
long long s1, l1;
long long s2, l2;

int main() {
  freopen("p1158.in", "r", stdin);
  cin >> mx1 >> my1 >> mx2 >> my2 >> n;
  for (long long i = 1; i <= n; i++) {
    cin >> x >> y;
    d1 = (mx1 - x) * (mx1 - x) + (my1 - y) * (my1 - y);
    d2 = (mx2 - x) * (mx2 - x) + (my2 - y) * (my2 - y);
    if (d1 < d2)
      s1 = s1 > d1 ? s1 : d1;
    else
      s2 = s2 > d2 ? s2 : d2;
  }
  cout << s1 + s2;
  return 0;
}
