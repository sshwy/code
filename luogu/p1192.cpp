#include <iostream>

using namespace std;

int n, k;
int f[100003];
int F(int h) {
  if (h < 0) return 0;
  if (f[h] != 0) return f[h];
  int sum = 0;
  for (int i = k; i >= 1; i--) { f[h] = (f[h] + F(h - i)) % 100003; }
  return f[h];
}
int main() {
  cin >> n >> k;
  if (k > n) k = n;
  f[0] = 1;
  cout << F(n);
  return 0;
}
