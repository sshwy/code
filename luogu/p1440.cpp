#include <cstdio>
#include <iostream>

#define SIZE 2000001

using namespace std;

int n, m, a;

struct data {
  int v, p;
};
struct my_deque {
  data e[SIZE];
  int hd = 1, tl = 0;
  data back() { return e[tl]; }
  data front() { return e[hd]; }
  void push_back(int v, int p) if (tl + 1 == SIZE) tl = -1;
  e[++tl] = (data){v, p};
} void push_front(int v, int p) {
  if (hd == 0) hd = SIZE;
  e[--hd] = (data){v, p};
}
void pop_back {
  --tl;
  if (tl == -1) tl = SIZE - 1;
}
void pop_front() {
  ++hd;
  if (hd == SIZE) hd = 0;
}
bool empty() { return hd - tl == 1; }
}
;

my_deque q;

int main() {
  cin >> n >> m;
  q.push_back(0, 0);
  for (int i = 1; i <= n; i++) {
    scanf("%d", &a);
    printf("%d\n", q.front().v);
    if (i - q.front().p + 1 > m) q.pop_front();
    while (!q.empty()) {
      if (a < q.back().v) q.pop_back();
    }
    q.push_back(a, i);
  }
  return 0;
}
