#include <iostream>

using namespace std;

int n, tot, h;
struct point {
  int x, y;
} p[401];
bool bt[2100][2100];

int hash(point a, point b) {
  int r = a.x - b.x, c = a.y - b.y;
  //	r=r>0?r:0-r;
  //	c=c>0?c:0-c;
  if (r == 0 || c == 0)
    r = c = 0;
  else
    for (int i = 2; i <= r && i <= c; i++) {
      if (r % i == 0 && c % i == 0) r /= i, c /= i, i--;
    }

  if (bt[r + 1000][c + 1000] == false) tot += bt[r + 1000][c + 1000] = true;
}

int main() {
  cin >> n;
  for (int i = 1; i <= n; i++) {
    cin >> p[i].x >> p[i].y;
    for (int j = 1; j < i; j++) { hash(p[i], p[j]); }
  }
  cout << tot;
  return 0;
}
