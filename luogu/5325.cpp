// by Sshwy
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int SZ = 5e5 + 5, P = 1e9 + 7, I6 = (P + 1) / 6, I2 = (P + 1) / 2;

int p[SZ], lp;
long long n, sqrt_n;
bool bp[SZ];
void sieve(int lim) {
  bp[0] = bp[1] = 1;
  FOR(i, 2, lim) {
    if (!bp[i]) p[++lp] = i;
    FOR(j, 1, lp) {
      if (1ll * i * p[j] > lim) break;
      bp[i * p[j]] = 1;
      if (i % p[j] == 0) break;
    }
  }
}

int id[2][SZ], tot;
long long val[SZ]; // val 是递减的
inline int I(long long x) { return x <= sqrt_n ? id[0][x] : id[1][n / x]; }
void init() {
  sqrt_n = sqrt(n);
  sieve(sqrt_n);
  for (long long pos = 1, nex; pos <= n; pos = nex + 1) {
    nex = n / (n / pos);
    long long w = n / pos;
    ++tot;
    val[tot] = w;
    w <= sqrt_n ? id[0][w] = tot : id[1][n / w] = tot;
  }
  FOR(i, 1, tot) assert(I(val[i]) == i);
  // FOR(i,1,tot)printf("val[%d]=%d\n",i,val[i]);
}
inline int f0(long long x) {
  x %= P;
  return x % P;
}
inline int Sf0(long long x) {
  x %= P;
  return x * (x + 1) % P * I2 % P;
}
inline int f1(long long x) {
  x %= P;
  return x * 1ll * x % P;
}
inline int Sf1(long long x) {
  x %= P;
  return x * (x + 1) % P * (x * 2 + 1) % P * I6 % P;
}
int h[2][SZ];
// g[0]:i
// g[1]:i^2
void calc_h() {
  FOR(i, 1, lp) h[0][i] = (h[0][i - 1] + f0(p[i])) % P;
  FOR(i, 1, lp) h[1][i] = (h[1][i - 1] + f1(p[i])) % P;
}
inline int H(int i) { return (h[1][i] - h[0][i] + P) % P; }
int g[2][SZ];
// g[0]:i
// g[1]:i^2
void calc_g() {
  FOR(i, 1, tot) { //当i=0
    g[0][i] = Sf0(val[i]) - 1;
    g[1][i] = Sf1(val[i]) - 1;
  }
  FOR(i, 1, lp) { // p[i]
    FOR(j, 1, tot) {
      if (1ll * p[i] * p[i] > val[j]) break;
      int k = I(val[j] / p[i]);
      // printf("k=%d\n",k);
      // printf("%d,%d,%d\n",f0(p[j]),g[0][k],h[0][i-1]);
      g[0][j] = (g[0][j] - f0(p[i]) * 1ll * (g[0][k] - h[0][i - 1])) % P,
      g[0][j] = (g[0][j] + P) % P;
      g[1][j] = (g[1][j] - f1(p[i]) * 1ll * (g[1][k] - h[1][i - 1])) % P,
      g[1][j] = (g[1][j] + P) % P;
      // printf("%d %d\n",g[0][j],g[1][j]);
    }
  }
  // FOR(i,1,tot)printf("%d %d %d\n",val[i],g[0][i],g[1][i]);
}
inline int G(long long x) { return (g[1][I(x)] - g[0][I(x)] + P) % P; }
inline int F(long long x) {
  x %= P;
  return x * (x - 1ll) % P;
}
int S(int i, long long m) {
  if (m < p[i] || m <= 1) return 0;
  long long res = (G(m) - H(i - 1) + P) % P;
  FOR(j, i, lp) {
    if (p[j] * 1ll * p[j] > m) break;
    long long pje = 1, pje1 = p[j];
    FOR(e, 1, n) {
      pje *= p[j], pje1 *= p[j];
      if (pje1 > m) break;
      res = (res + F(pje) * 1ll * S(j + 1, m / pje) % P + F(pje1)) % P;
    }
  }
  return res;
}
int Min_25(long long _n) {
  n = _n;
  init();
  calc_h();
  calc_g();
  return (S(1, n) + 1) % P;
}

int main() {
  scanf("%lld", &n);
  printf("%d\n", Min_25(n));
  return 0;
}
