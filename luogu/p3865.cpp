#include <cstdio>
#include <iostream>
#define max(a, b) (a > b ? a : b)

using namespace std;

int n, m, l, r, d;
int num[100001];
int two[100] = {1, 2, 4, 8, 16, 32, 64, 128, 256, 512, 1024, 2048, 4096, 8192, 16384,
    32768, 65536, 131072, 262144, 524288, 1048576, 2097152};
int f[30][100001]; // f[j][i]=max(f[j-1][i],f[j-1][i+2^(j-1)])
//小的一维放前面，加快运行速度

int main() {
  scanf("%d%d", &n, &m);
  for (int i = 1; i <= n; i++) cin >> f[0][i];
  for (int j = 1; two[j - 1] < n; j++) {
    for (int i = 1; i + two[j - 1] <= n; i++) {
      f[j][i] = max(f[j - 1][i], f[j - 1][i + two[j - 1]]);
    }
  }
  for (int i = 1; i <= m; i++) {
    scanf("%d%d", &l, &r);
    for (d = 0; two[d + 1] <= r - l + 1; d++)
      ;
    printf("%d\n", max(f[d][l], f[d][r - two[d] + 1]));
  }
  return 0;
}
