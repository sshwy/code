#include <iostream>
#include <queue>
#include <vector>

using namespace std;

const int INF = 99999999;
int n, m, a, b, c;
int d[1000001], l[1000001];
int w[2001][2001];
bool vising[1000001];

vector<int> link[1000001];
vector<int> path[1000001];
queue<int> q;

int main() {
  cin >> n >> m;
  for (int i = 1; i <= n; i++)
    for (int j = 1; j <= n; j++) w[i][j] = INF;
  for (int i = 0; i < m; i++) {
    cin >> a >> b >> c;
    if (w[a][b] < c) continue;
    w[a][b] = w[b][a] = c;
    link[a].push_back(b);
    link[b].push_back(a);
    path[a].push_back(c);
    path[b].push_back(c);
  }

  for (int i = 1; i <= n; i++) d[i] = INF;
  d[1] = 0;
  l[1] = 1;
  q.push(1);
  vising[1] = true;

  while (!q.empty()) {
    int now = q.front();
    q.pop();
    vising[now] = false;
    for (int i = 0; i < link[now].size(); i++) {
      int nex = link[now][i];
      if (d[nex] > d[now] + path[now][i]) {
        d[nex] = d[now] + path[now][i];
        l[nex] = l[now];
        if (vising[nex] == false) {
          q.push(nex);
          vising[nex] = true;
        }
      } else if (d[nex] == d[now] + path[now][i])
        l[nex] = (l[nex] + l[now]);
    }
  }
  if (d[n] == INF)
    cout << "No answer";
  else
    cout << d[n] << ' ' << l[n];
  return 0;
}
