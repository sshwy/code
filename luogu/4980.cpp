#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
namespace IO {
  char nc() {
    static char bf[100000], *p1 = bf, *p2 = bf;
    return p1 == p2 && (p2 = (p1 = bf) + fread(bf, 1, 100000, stdin), p1 == p2)
               ? EOF
               : *p1++;
  }
  int rd() {
    int res = 0;
    char c = getchar();
    while (!isdigit(c)) c = getchar();
    while (isdigit(c)) res = res * 10 + c - '0', c = getchar();
    return res;
  }
} // namespace IO
/******************heading******************/
const int P = 1e9 + 7;

int t, n;
long long ans;

long long phi(long long x) {
  long long res = x;
  for (int i = 2; i * i <= x; i++) {
    if (x % i) continue;
    res = res / i * (i - 1);
    while (x % i == 0) x /= i;
  }
  if (x > 1) res = res / x * (x - 1);
  return res;
}
long long pw(long long a, long long m, long long p) {
  long long res = 1;
  while (m) m & 1 ? res = res * a % p : 0, a = a * a % p, m >>= 1;
  return res;
}
long long calc(long long d) {
  long long t = pw(n, d, P);
  t = t * phi(n / d) % P;
  return t;
}
void go() {
  scanf("%d", &n);
  ans = 0;
  for (int i = 1; i * i <= n; i++) {
    if (n % i) continue;
    if (i * i < n) ans += calc(n / i);
    ans += calc(i);
    ans %= P;
  }
  ans = ans * pw(n, P - 2, P) % P;
  printf("%lld\n", ans);
}
int main() {
  scanf("%d", &t);
  FOR(i, 1, t) go();
  return 0;
}
