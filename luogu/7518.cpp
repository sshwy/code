// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

const int N = 2e5 + 5;

int n, m, c;
int a[N], posA[N], b[N];

vector<int> g[N];
vector<pair<int, int>> qrys;
int dep[N], one[N], d[N], nex[N][20], pre[N][20], fa[N][20], ans[N], lans;
struct Atom {
  int z, x, id;
};
vector<Atom> r[N];

void dfs(int u, int p) {
  int originD = d[b[u]];
  d[b[u]] = u;

  dep[u] = dep[p] + 1;
  one[u] = d[1];
  nex[u][0] = d[b[u] + 1];
  FOR(j, 1, 19) {
    nex[u][j] = nex[nex[u][j - 1]][j - 1];
    if (!nex[u][j]) break;
  }
  fa[u][0] = p;
  FOR(j, 1, 19) {
    fa[u][j] = fa[fa[u][j - 1]][j - 1];
    if (!fa[u][j]) break;
  }
  for (int v : g[u])
    if (v != p) { dfs(v, u); }

  d[b[u]] = originD;
}
int lca(int u, int v) {
  if (dep[u] < dep[v]) swap(u, v);
  ROF(j, 19, 0) {
    if (dep[u] - (1 << j) < dep[v]) continue;
    u = fa[u][j];
  }
  assert(dep[u] == dep[v]);
  if (u == v) return u;
  ROF(j, 19, 0) {
    if (fa[u][j] == fa[v][j]) continue;
    u = fa[u][j], v = fa[v][j];
  }
  return fa[u][0];
}
bool check(Atom e, int x) {
  int z = e.z;
  int v = d[x];
  if (dep[v] >= dep[z]) {
    ROF(j, 19, 0) {
      int t = pre[v][j];
      if (dep[t] >= dep[z]) v = t;
    }
  }
  if (dep[v] < dep[z]) return false;
  if (b[v] <= e.x) return true;
  return false;
}
void dfs2(int u, int p) {
  int originD = d[b[u]];
  d[b[u]] = u;

  pre[u][0] = d[b[u] - 1];
  FOR(j, 1, 19) {
    pre[u][j] = pre[pre[u][j - 1]][j - 1];
    if (!pre[u][j]) break;
  }
  for (auto e : r[u]) {
    // printf("r[%d]: z = %d x = %d id = %d\n", u, e.z, e.x, e.id);
    int l = e.x, r = e.x + dep[u] - dep[e.z] + 1;
    // printf("l %d r %d\n", l, r);
    while (l < r) {
      int mid = (l + r + 1) / 2;
      if (check(e, mid))
        l = mid;
      else
        r = mid - 1;
    }
    if (check(e, l))
      ans[e.id] = l;
    else
      ans[e.id] = l - 1;
  }
  for (int v : g[u])
    if (v != p) dfs2(v, u);

  d[b[u]] = originD;
}

void go() {
  dfs(1, 0);
  for (auto q : qrys) {
    int u = q.first, v = q.second, z = lca(u, v);
    int x = one[u];
    // printf("one[%d] = %d\n", u, one[u]);
    if (!x)
      r[v].pb((Atom){z, 1, ++lans});
    else {
      ROF(j, 19, 0) {
        int t = nex[x][j];
        if (dep[t] > dep[z]) x = t;
      }
      if (dep[x] > dep[z])
        r[v].pb((Atom){z, b[x] + 1, ++lans});
      else
        r[v].pb((Atom){z, b[x], ++lans});
    }
  }
  memset(d, 0, sizeof(d));
  dfs2(1, 0);
}

int main() {
  scanf("%d%d%d", &n, &m, &c);
  FOR(i, 1, c) scanf("%d", a + i), posA[a[i]] = i;
  FOR(i, 1, n) scanf("%d", b + i), b[i] = posA[b[i]];
  FOR(i, 1, n - 1) {
    int u, v;
    scanf("%d%d", &u, &v);
    g[u].pb(v);
    g[v].pb(u);
  }
  int q;
  scanf("%d", &q);
  FOR(i, 1, q) {
    int s, t;
    scanf("%d%d", &s, &t);
    qrys.pb({s, t});
  }
  go();
  FOR(i, 1, lans) printf("%d\n", ans[i]);
  return 0;
}
