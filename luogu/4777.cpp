#include <algorithm>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
/******************heading******************/

lld mul(lld a, lld b, lld p) { //这个函数貌似目前只支持非负整数
  if (a <= 1000000000 && b <= 1000000000) return a * b % p;
  lld res = 0;
  while (b) {
    if (b & 1) res = (res + a) % p;
    a = a * 2 % p, b >>= 1;
  }
  return res;
}
lld gcd(lld a, lld b) { return b ? gcd(b, a % b) : a; }
lld exgcd(lld a, lld b, lld &x, lld &y) {
  if (!b) return x = 1, y = 0, a;
  lld t = exgcd(b, a % b, y, x);
  return y = y - (a / b) * x, t;
}

int main() {
  lld n, b = 0, a = 1; // x=0 mod 1
  scanf("%lld", &n);
  FOR(i, 1, n) {
    lld a1, b1, k, k1;
    scanf("%lld%lld", &a1, &b1);
    //求解方程ak-a1k1=b1-b, 只求k就够了
    lld g = exgcd(a, a1, k, k1), d = ((b1 - b) % a1 + a1) % a1;
    k = mul(k, d / g, a1);
    //然后合并方程
    b = b + a * k, a = a / g * a1, b = (b + a) % a;
  }
  printf("%lld\n", (b + a) % a);
  return 0;
}
/*
 * BUG#1: 没有用防溢出乘法
 * BUG#2: 忘删调试信息
 * BUG#3L L55. a=a*a1/g 不等于 a*=a1/g!
 */
