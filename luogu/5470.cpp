// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

const int N = 2e5 + 5;

int n, k, l, lim, a[N], b[N];
int flow_optimal;
long long ans = 0;

enum Option { BOTH_MAX, FIXED_A_MAX_B, MAX_A_FIXED_B, MAX_PAIR };

typedef pair<int, int> pt;

template <class T> struct removable_pq {
  priority_queue<T> a, b;
  void zip() {
    while (!a.empty() && !b.empty() && a.top() == b.top()) a.pop(), b.pop();
  }
  bool empty() { return zip(), a.empty(); }
  T top() { return zip(), assert(!a.empty()), a.top(); }
  void push(T v) { a.push(v); }
  void remove(T v) { b.push(v); }
  void clear() {
    while (!a.empty()) a.pop();
    while (!b.empty()) b.pop();
  }
};

removable_pq<pt> A[2], B[2], C;
// A[1]: a not vis, b vis

bool vis_a[N], vis_b[N];

void init() {
  ans = 0;
  flow_optimal = 0;
  lim = k - l;
  FOR(i, 0, 1) A[i].clear(), B[i].clear();
  C.clear();
  fill(vis_a, vis_a + n + 1, false);
  fill(vis_b, vis_b + n + 1, false);
  FOR(i, 1, n) {
    A[0].push({a[i], i});
    B[0].push({b[i], i});
    C.push({a[i] + b[i], i});
  }
}
int pickA(int i) {
  assert(vis_a[i] == false);
  vis_a[i] = true;
  if (vis_b[i] == false) {
    C.remove({a[i] + b[i], i});
    B[1].push({b[i], i});
  } else {
    --flow_optimal;
    A[1].remove({a[i], i});
  }
  A[0].remove({a[i], i});
  return a[i];
}
int pickB(int i) {
  assert(vis_b[i] == false);
  vis_b[i] = true;
  if (vis_a[i] == false) {
    C.remove({a[i] + b[i], i});
    A[1].push({a[i], i});
  } else {
    --flow_optimal;
    B[1].remove({b[i], i});
  }
  B[0].remove({b[i], i});
  return b[i];
}
pt zero(0, 0);
pt getMaxA() {
  return max(A[0].empty() ? zero : A[0].top(), A[1].empty() ? zero : A[1].top());
}
pt getMaxB() {
  return max(B[0].empty() ? zero : B[0].top(), B[1].empty() ? zero : B[1].top());
}
void pick(pt pr) {
  ans += pickA(pr.first);
  ans += pickB(pr.second);
  ++flow_optimal;
}
pt estimate(Option typ) { // {index_of_a, index_of_b}
  if (typ == MAX_PAIR) {
    if (C.empty()) return zero;
    pt x = C.top();
    return {x.second, x.second};
  }
  if (typ == FIXED_A_MAX_B) {
    if (B[1].empty()) return zero;
    return {getMaxA().second, B[1].top().second};
  }
  if (typ == MAX_A_FIXED_B) {
    if (A[1].empty()) return zero;
    return {A[1].top().second, getMaxB().second};
  }
  assert(0);
}
bool bySum(pt x, pt y) { return a[x.first] + b[x.second] < a[y.first] + b[y.second]; }
void work() {
  assert(flow_optimal <= lim);
  if (flow_optimal < lim) {
    pick({getMaxA().second, getMaxB().second});
  } else {
    pt t[] = {estimate(FIXED_A_MAX_B), estimate(MAX_A_FIXED_B), estimate(MAX_PAIR)};
    pick(*max_element(t, t + 3, bySum));
  }
}
void go() {
  scanf("%d%d%d", &n, &k, &l);
  FOR(i, 1, n) scanf("%d", a + i);
  FOR(i, 1, n) scanf("%d", b + i);

  init();
  FOR(i, 1, k) work();
  printf("%lld\n", ans);
}
int main() {
  int t;
  scanf("%d", &t);
  FOR(i, 1, t) go();
  return 0;
}
