// by Sshwy
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA
const int N = 2e5 + 5, P = 998244353;
vector<int> g[N];

int n;
int f[N];
void dfs(int u, int p) {
  int s1 = 0, s2 = 1;
  for (int v : g[u])
    if (v != p) {
      dfs(v, u);
      s1 = (s1 + f[v]) % P;
      s2 = s2 * (f[v] + 1ll) % P;
    }
  f[u] = (s1 + s2) % P;
  // printf("f[%d]=%d\n",u,f[u]);
}
int main() {
  scanf("%d", &n);
  FOR(i, 1, n - 1) {
    int u, v;
    scanf("%d%d", &u, &v);
    g[u].pb(v);
    g[v].pb(u);
  }
  dfs(1, 0);
  printf("%d\n", f[1]);
  return 0;
}
