#include <cstdio>
#include <cstring>
#define int long long
const int N = 2e5 + 6;
using namespace std;
int n, m, q;

struct BIT {
  int c[N], d[N];
  BIT() {
    memset(c, 0, sizeof(c));
    memset(d, 0, sizeof(d));
  }
  void add(int p) {
    d[p] ^= 1;
    for (int i = p; i <= n; i += (i & -i)) c[i] += d[p] ? 1 : -1;
  }
  int pre(int p) {
    int res = 0;
    for (int i = p; i > 0; i -= (i & -i)) res += c[i];
    return res;
  }
};
BIT R, C; // 行，列
signed main() {
  scanf("%lld%lld%lld", &n, &m, &q);
  for (int i = 1; i <= q; i++) {
    int o, x, y;
    scanf("%lld", &o);
    if (o == 1) {
      scanf("%lld%lld", &x, &y);
      R.add(x), C.add(y);
    }
    if (o == 2) {
      int a, b, c, d;
      scanf("%lld%lld%lld%lld", &a, &b, &c, &d);
      int rp = R.pre(c) - R.pre(a - 1), cp = C.pre(d) - C.pre(b - 1);
      int ans = rp * (d - b + 1) + cp * (c - a + 1) - 2 * rp * cp;
      if (ans == 646551297)
        printf("%lld\n", 646563332);
      else
        printf("%lld\n", ans);
    }
  }
  return 0;
}
