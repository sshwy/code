// by Sshwy
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 112, S = 112, M = 40004, oo = 0x3f3f3f3f;

int s, n, m;
int a[N][S];
int f[M], g[M];

int main() {
  scanf("%d%d%d", &s, &n, &m);
  FOR(i, 1, s) FOR(j, 1, n) { scanf("%d", &a[j][i]); }
  FOR(i, 1, n) {
    sort(a[i] + 1, a[i] + s + 1);
    a[i][s + 1] = -1;
    FOR(j, 1, s) if (a[i][j] != a[i][j + 1]) {
      int v = a[i][j] * 2 + 1, w = i * j; //获得i*j分
      ROF(k, m, v) g[k] = max(g[k], f[k - v] + w);
    }
    FOR(i, 0, m) f[i] = g[i] = max(f[i], g[i]);
  }
  printf("%d\n", *max_element(f, f + m + 1));
  return 0;
}
