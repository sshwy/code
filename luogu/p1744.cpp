#include <cmath>
#include <cstdio>
#include <iostream>

#define INF 9999999

using namespace std;

int n, m, a, b, s, t;
long double d[101][101];
struct point {
  int x, y;
} c[101];

long double dis(point p1, point p2) {
  return sqrt((p1.x - p2.x) * (p1.x - p2.x) + (p1.y - p2.y) * (p1.y - p2.y));
}

int main() {
  cin >> n;
  for (int i = 1; i <= n; i++) cin >> c[i].x >> c[i].y;
  for (int i = 1; i <= n; i++) {
    for (int j = 1; j <= n; j++) d[i][j] = INF;
    d[i][i] = 0;
  }
  cin >> m;
  for (int i = 1; i <= m; i++) {
    cin >> a >> b;
    d[a][b] = d[b][a] = dis(c[a], c[b]);
  }
  cin >> s >> t;
  for (int k = 1; k <= n; k++)
    for (int i = 1; i <= n; i++)
      for (int j = 1; j <= n; j++)
        if (d[i][j] > d[i][k] + d[k][j]) d[i][j] = d[i][k] + d[k][j];
  printf("%.2Lf", d[s][t]);
  return 0;
}
