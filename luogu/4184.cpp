#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 5e5 + 5, LST = N << 2;

int n;
map<int, int> mp;
vector<int> val[LST];

struct basis {
  int b[40];
  basis() { memset(b, 0, sizeof(b)); }
  bool insert(int x) {
    ROF(i, 30, 0) {
      if (x >> i & 1) {
        if (b[i])
          x ^= b[i];
        else
          return b[i] = x, 1;
      }
    }
    return 0;
  }
  int qmax() {
    int x = 0;
    ROF(i, 30, 0) if ((x ^ b[i]) > x) x ^= b[i];
    return x;
  }
};
void add(int L, int R, int v, int u = 1, int l = 1, int r = n) {
  if (r < L || R < l) return;
  if (L <= l && r <= R) return val[u].pb(v), void();
  int mid = (l + r) >> 1;
  add(L, R, v, u << 1, l, mid), add(L, R, v, u << 1 | 1, mid + 1, r);
}
void dfs(basis B, int u = 1, int l = 1, int r = n) {
  basis cur = B;
  for (unsigned i = 0; i < val[u].size(); i++) cur.insert(val[u][i]);
  if (l == r) { return printf("%d\n", cur.qmax()), void(); }
  int mid = (l + r) >> 1;
  dfs(cur, u << 1, l, mid), dfs(cur, u << 1 | 1, mid + 1, r);
}
int main() {
  scanf("%d", &n);
  FOR(i, 1, n) {
    int x;
    scanf("%d", &x);
    if (x > 0) {
      if (!mp.count(x) || mp[x] == 0) mp[x] = i;
    } else if (x < 0) {
      x = -x;
      // printf("add(%d,%d,%d)\n",mp[x],i-1,x);
      add(mp[x], i - 1, x);
      mp[x] = 0;
    }
  }
  for (map<int, int>::iterator it = mp.begin(); it != mp.end(); ++it) {
    if (it->se) {
      // printf("add(%d,%d,%d)\n",it->se,n,it->fi);
      add(it->se, n, it->fi);
    }
  }
  basis ans;
  dfs(ans);
  return 0;
}
