#include <iostream>
using namespace std;

int x, n, sum = 0;

int main() {
  cin >> x >> n;
  for (int i = 1; i <= n; i++, x++) {
    if (x > 7) x -= 7;
    if (x > 5) continue;
    sum += 250;
  }
  cout << sum;
  return 0;
}
