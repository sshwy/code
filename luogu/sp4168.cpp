#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
typedef long long LL;
const int LIM = 1e7, SZ = LIM + 5;

LL n, n2;

bool bp[SZ];
int pn[SZ], lp;
void sieve() {
  bp[0] = bp[1] = 1;
  FOR(i, 2, LIM) {
    if (!bp[i]) pn[++lp] = i;
    FOR(j, 1, lp) {
      int pj = pn[j];
      if (i * pj > LIM) break;
      bp[i * pj] = 1;
      if (i % pj == 0) break;
    }
  }
}
LL ans;
void dfs(int cur, LL prd, int mu) {
  if (prd > n2) return;
  // printf("prd=%lld,mu=%d\n",prd,mu);
  ans += mu * (n / (prd * prd));
  FOR(i, cur + 1, lp) {
    if (prd * pn[i] > n2) break;
    dfs(i, prd * pn[i], -mu);
  }
}

void go() {
  scanf("%lld", &n);
  n2 = sqrt(n);
  while (n2 * n2 <= n) ++n2;
  while (n2 * n2 > n) --n2;
  ans = 0;
  // ans=n;
  dfs(0, 1, 1);
  printf("%lld\n", ans);
}
int main() {
  int t;
  sieve();
  scanf("%d", &t);
  FOR(i, 1, t) go();
  return 0;
}
