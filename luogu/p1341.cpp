#include <algorithm>
#include <iostream>
#include <vector>

using namespace std;

int n;
char a, b, start = 127, startdan = 250;
bool f;
bool visedge[1000000];
int dansu_count;
struct link {
  char from, to;
} l1;
vector<link> edge;
vector<int> next[300];

void dfs(char k) {
  cout << k;
  char minnex = 250;
  int minnexedge = -1;
  for (int i = 0; i < next[k].size(); i++) {
    if (visedge[next[k][i]] == false) {
      char nex = edge[next[k][i]].to;
      if (nex < minnex) {
        minnex = nex;
        minnexedge = next[k][i];
      }
    }
  }
  if (minnexedge != -1) {
    visedge[minnexedge] = true;
    dfs(minnex);
  }
}

int main() {
  cin >> n;
  for (int i = 1; i <= n; i++) {
    cin >> a >> b;
    if (a < start) start = a;
    if (b < start) start = b;

    l1.from = a;
    l1.to = b;
    edge.push_back(l1);
    next[a].push_back(edge.size() - 1);

    l1.from = b;
    l1.to = a;
    edge.push_back(l1);
    next[b].push_back(edge.size() - 1);
  }
  for (int i = 0; i < 300; i++) {
    if (next[i].size() % 2 == 1) {
      dansu_count++;
      if (i < startdan) startdan = i;
    }
  }

  if (dansu_count != 2 && dansu_count != 0) {
    cout << "No Solution";
    return 0;
  } else if (dansu_count == 2) {
    start = startdan;
  }

  dfs(start);
  return 0;
}
