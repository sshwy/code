#include <algorithm>
#include <iostream>

using namespace std;

int m, n, k, cnt, tot, nx, ny;
int qp[21][21];
struct nut {
  int x, y, v;
} nuts[401];

bool cmp(nut n1, nut n2) { return n1.v > n2.v; }

int dis(int a, int b, int c, int d) {
  return (a > c ? a - c : c - a) + (b > d ? b - d : d - b) < a + c
             ? (a > c ? a - c : c - a) + (b > d ? b - d : d - b)
             : a + c;
  //	return (a>c?a-c:c-a)+(b>d?b-d:d-b);
}

int main() {
  cin >> m >> n >> k;
  for (int i = 1; i <= m; i++) {
    for (int j = 1; j <= n; j++) {
      cin >> qp[i][j];
      if (qp[i][j] != 0) nuts[++cnt] = (nut){i, j, qp[i][j]};
    }
  }
  sort(nuts + 1, nuts + cnt + 1, cmp);

  if (nuts[1].x * 2 + 1 > k) {
    cout << 0;
    return 0;
  }
  k -= nuts[1].x + 1;
  tot += nuts[1].v;
  nx = nuts[1].x;
  ny = nuts[1].y;

  for (int i = 2; i <= cnt; i++) {
    cout << nx << ' ' << ny << ' ' << k << endl;
    if (dis(nx, ny, nuts[i].x, nuts[i].y) + nuts[i].x + 1 <= k) {
      k -= dis(nx, ny, nuts[i].x, nuts[i].y) + 1;
      tot += nuts[i].v;
      nx = nuts[i].x;
      ny = nuts[i].y;
    } else
      break;
  }
  cout << tot;
  return 0;
}
