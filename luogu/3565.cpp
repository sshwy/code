#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (int)(b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (int)(b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 1e5 + 5;

struct qxx {
  int nex, t;
};
qxx e[N * 2];
int h[N], le = 1;
void add_path(int f, int t) { e[++le] = (qxx){h[f], t}, h[f] = le; }
#define FORe(i, u, v) for (int i = h[u], v; v = e[i].t, i; i = e[i].nex)

int n;

deque<long long> *f[N], *g[N];
long long ans;
// f[i,j]表示i子树内与i距离为j的点的个数
// g[i,j]表示i子树内的两个点满足到LCA距离同为d，LCA到i的距离为d-j（即距离差为j）的方案数

int dep[N], hgh[N];
void dfs1(int u, int p) {
  dep[u] = dep[p] + 1;
  hgh[u] = 1;
  FORe(i, u, v) if (v != p) dfs1(v, u), hgh[u] = max(hgh[u], hgh[v] + 1);
}
void dfs2(int u, int p) {
  int mx = 0, son = -1;
  FORe(i, u, v) if (v != p && hgh[v] > mx) mx = hgh[v], son = v;
  if (son == -1) {
    f[u] = new deque<long long>;
    g[u] = new deque<long long>;
    f[u]->pb(1); // f[u,0]=1
    g[u]->pb(0); // g[u,0]=0
    return;
  }
  dfs2(son, u);
  FORe(i, u, v) if (v != p && v != son) dfs2(v, u);
  //继承 son
  f[u] = f[son];
  f[u]->push_front(1); // f[u,0]=1
  g[u] = g[son];
  g[u]->pop_front(); // g[u,i]=g[v,i+1]
  while (g[u]->size() < f[u]->size()) g[u]->pb(0);
  //合并其他儿子的答案
  FORe(i, u, v) {
    if (v == p || v == son) continue;
    //统计答案
    int lim;

    lim = min((int)f[u]->size() - 1, (int)g[v]->size() - 2);
    FOR(i, 1, lim) ans += ((*f[u])[i]) * ((*g[v])[i + 1]);

    lim = min((int)f[v]->size() - 1, (int)g[u]->size() - 2);
    FOR(i, 0, lim) ans += ((*f[v])[i]) * ((*g[u])[i + 1]);

    //转移
    lim = min((int)g[u]->size() - 1, (int)g[v]->size() - 1);

    FOR(i, 1, lim)(*g[u])[i - 1] += (*g[v])[i];
    lim = min((int)g[u]->size() - 1, (int)f[v]->size());

    FOR(i, 1, lim)(*g[u])[i] += (*f[u])[i] * (*f[v])[i - 1];

    assert(f[u]->size() > f[v]->size());
    lim = (int)f[v]->size() - 1;
    FOR(i, 0, lim)(*f[u])[i + 1] += (*f[v])[i];
  }
  ans += (*g[u])[0];
}

int main() {
  scanf("%d", &n);
  FOR(i, 1, n - 1) {
    int x, y;
    scanf("%d%d", &x, &y);
    add_path(x, y), add_path(y, x);
  }
  dfs1(1, 0);
  dfs2(1, 0);
  printf("%lld\n", ans);
  return 0;
}
