#include <iostream>
#define max(a, b) (a > b ? a : b)

using namespace std;

int n, a, b, root = 1;
struct node {
  int p = -1, s = -1, b = -1; // parent,son,brother
  int v = 0, subv0 = 0,
      subv1 = 0; // v:key;subv0:unattend subtree sum;subv1:attend subtree sum
} tree[6001];

void tree_stc(int now) {
  if (now == -1) return;
  for (int sub = tree[now].s; sub != -1; sub = tree[sub].b) tree_stc(sub);
  for (int sub = tree[now].s; sub != -1; sub = tree[sub].b) {
    tree[now].subv1 += tree[sub].subv0;
    tree[now].subv0 += max(tree[sub].subv0, tree[sub].subv1);
  }
  tree[now].subv1 += tree[now].v;
}

int main() {
  cin >> n;
  for (int i = 1; i <= n; i++) cin >> tree[i].v;
  for (int i = 1; i < n; i++) {
    cin >> a >> b;
    tree[a].p = b;
    if (tree[b].s == -1)
      tree[b].s = a;
    else {
      int bro = tree[b].s;
      while (tree[bro].b != -1) bro = tree[bro].b;
      tree[bro].b = a;
    }
  }
  while (tree[root].p != -1) root = tree[root].p;
  tree_stc(root);
  cout << max(tree[root].subv0, tree[root].subv1);
  return 0;
}
