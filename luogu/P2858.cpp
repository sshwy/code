#include <bits/stdc++.h>
using namespace std;
int n, a[2003], mx, f[2003][2003]; //一共拿i块糖，其中j块从开头拿
int main() {
  scanf("%d", &n);
  for (int i = 1; i <= n; i++) scanf("%d", &a[i]);
  for (int i = 1; i <= n; i++)
    f[i][i] = f[i - 1][i - 1] + a[i] * i, f[i][0] = f[i - 1][0] + a[n - i + 1] * i;

  for (int i = 1; i <= n; i++)
    for (int j = 1; j < i; j++)
      f[i][j] = max(f[i - 1][j] + a[n - i + 1 + j] * i, f[i - 1][j - 1] + a[j] * i);

  for (int i = 0; i <= n; i++) mx = max(mx, f[n][i]);
  printf("%d", mx);
  return 0;
}
