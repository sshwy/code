// by Sshwy
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
const int SZ = 1 << 19, P = 998244353;
int pw(int a, int m) {
  if (m < 0) m += P - 1;
  int res = 1;
  while (m) m & 1 ? res = 1ll * res * a % P : 0, a = 1ll * a * a % P, m >>= 1;
  return res;
}
int tr[SZ];
int init(int n) { // n指多项式的长度，非度数
  int len = 1;
  while (len < n) len <<= 1;
  FOR(i, 0, len - 1) tr[i] = tr[i >> 1] >> 1 | ((i & 1) * (len / 2));
  return len;
}
int dft_w[2][19][SZ];
void init_G() {
  FOR(j2, 0, 18) {
    int j = 1 << j2, wn = pw(3, (P - 1) / (j << 1)), *W = dft_w[0][j2];
    W[0] = 1;
    FOR(i, 1, j - 1) W[i] = W[i - 1] * 1ll * wn % P;
    wn = pw(3, -(P - 1) / (j << 1)), W = dft_w[1][j2];
    W[0] = 1;
    FOR(i, 1, j - 1) W[i] = W[i - 1] * 1ll * wn % P;
  }
}
void dft(int *f, int len, int tag) {
  FOR(i, 0, len - 1) if (i < tr[i]) swap(f[i], f[tr[i]]);
  FOR(i, 0, len - 1) f[i] = (f[i] + P) % P;
  for (int j = 1, j2 = 0; j < len; j <<= 1, ++j2)
    for (int i = 0, *W = dft_w[tag == -1][j2] /*pw(3,(P-1)/(j<<1)*tag)*/; i < len;
         i += j << 1)
      for (int k = i, u, v, w = 1; k < i + j; k++, w = W[k - i] /*1ll*w*wn%P*/)
        u = f[k], v = f[k + j] * 1ll * w % P, f[k] = u + v, f[k] -= f[k] >= P ? P : 0,
        f[k + j] = u - v, f[k + j] += f[k + j] < 0 ? P : 0;
  if (tag == -1)
    for (int i = 0, ilen = pw(len, P - 2); i < len; i++) f[i] = 1ll * f[i] * ilen % P;
}
int inv_h[SZ];
void inv(const int *f, int *g, int n) { // f*g=1 mod x^n
  if (n == 1) return g[0] = pw(f[0], P - 2), void();
  inv(f, g, (n + 1) / 2);

  int len = init(2 * n);
  FOR(i, 0, len - 1) inv_h[i] = 0;
  FOR(i, (n + 1) / 2, len - 1) g[i] = 0;

  FOR(i, 0, n - 1) inv_h[i] = f[i];
  dft(inv_h, len, 1), dft(g, len, 1);
  FOR(i, 0, len - 1) inv_h[i] = 1ll * inv_h[i] * g[i] % P * g[i] % P;
  FOR(i, 0, len - 1) g[i] = (g[i] * 2ll - inv_h[i] + P) % P;
  dft(g, len, -1);

  // FOR(i,n,len-1)g[i]=0;
}
int dm_f_r[SZ], dm_g_r[SZ], dm_h[SZ];
void div_mod(const int *f, int n, const int *g, int m, int *q, int &lq, int *r,
    int &lr) { // f = g*q + r
  if (n < m) return q[0] = 0, lq = 1, memcpy(r, g, sizeof(int) * m), lr = m, void();
  memcpy(dm_f_r, f, sizeof(int) * n), memcpy(dm_g_r, g, sizeof(int) * m);
  reverse(dm_f_r, dm_f_r + n), reverse(dm_g_r, dm_g_r + m);

  int MOD = n - m + 1;

  inv(dm_g_r, dm_h, MOD);

  int len = init(MOD + MOD);
  FOR(i, MOD, len - 1) dm_f_r[i] = dm_h[i] = 0;
  dft(dm_f_r, len, 1), dft(dm_h, len, 1);
  FOR(i, 0, len - 1) dm_h[i] = dm_h[i] * 1ll * dm_f_r[i] % P;
  dft(dm_h, len, -1);

  FOR(i, 0, MOD - 1) q[i] = dm_h[i];
  reverse(q, q + MOD);
  lq = MOD;

  memcpy(dm_f_r, g, sizeof(int) * m), memcpy(dm_g_r, q, sizeof(int) * lq);

  len = init(lq + m);
  FOR(i, m, len - 1) dm_f_r[i] = 0;
  FOR(i, lq, len - 1) dm_g_r[i] = 0;
  dft(dm_f_r, len, 1), dft(dm_g_r, len, 1);
  FOR(i, 0, len - 1) dm_g_r[i] = 1ll * dm_f_r[i] * dm_g_r[i] % P;
  dft(dm_g_r, len, -1);

  FOR(i, 0, n - 1) r[i] = (f[i] - dm_g_r[i] + P) % P;
  lr = m - 1;
}
/******************heading******************/

int t1[SZ], t2[SZ], t3[SZ];
void mul(const int *f, const int *g, int *h, int n,
    int *res) { // deg f = deg g = deg h = n
  int len = init(n + n);
  memcpy(t1, f, sizeof(int) * n);
  memcpy(t2, g, sizeof(int) * n);
  FOR(i, n, len - 1) t1[i] = t2[i] = 0;
  dft(t1, len, 1), dft(t2, len, 1);

  FOR(i, 0, len - 1) t2[i] = 1ll * t2[i] * t1[i] % P;
  dft(t2, len, -1);

  int a, b;
  div_mod(t2, n + n, h, n, t1, a, res, b);
}

const int K = 40000;

int n, k;
int a[K], c[K];
int f[SZ], g[SZ], h[SZ];

int main() {
  init_G();
  scanf("%d%d", &n, &k);
  FOR(i, 1, k) scanf("%d", &c[i]);
  FOR(i, 0, k - 1) scanf("%d", &a[i]), a[i] = (a[i] + P) % P;

  //特征多项式
  FOR(i, 1, k) f[k - i] = (P - c[i]) % P;
  f[k] = 1;

  // h(x) = x^n mod f(x)
  g[1] = 1;
  h[0] = 1;
  for (int m = n; m; m >>= 1) {
    if (m & 1) mul(h, g, f, k + 1, h);
    mul(g, g, f, k + 1, g);
  }

  int a_n = 0;
  FOR(i, 0, k - 1) a_n = (a_n + h[i] * 1ll * a[i]) % P;

  printf("%d\n", a_n);

  return 0;
}
