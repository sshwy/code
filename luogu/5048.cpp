// by Sshwy
#define DEBUGGER
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

#ifdef DEBUGGER

#define log_prefix()                                                \
  {                                                                 \
    fprintf(stderr, "\033[37mLine %-3d [%dms]:\033[0m  ", __LINE__, \
        (int)clock() / 1000);                                       \
  }
#define ilog(...) \
  { fprintf(stderr, __VA_ARGS__); }
#define llog(...)      \
  {                    \
    log_prefix();      \
    ilog(__VA_ARGS__); \
  }
#define log(...)           \
  {                        \
    log_prefix();          \
    ilog(__VA_ARGS__);     \
    fprintf(stderr, "\n"); \
  }
#define red(...)       \
  {                    \
    log_prefix();      \
    ilog("\033[31m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define green(...)     \
  {                    \
    log_prefix();      \
    ilog("\033[32m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }
#define blue(...)      \
  {                    \
    log_prefix();      \
    ilog("\033[34m");  \
    ilog(__VA_ARGS__); \
    ilog("\033[0m\n"); \
  }

#else

#define log_prefix() ;
#define ilog(...) ;
#define llog(...) ;
#define log(...) ;
#define red(...) ;
#define green(...) ;
#define blue(...) ;

#endif

namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
namespace IO {
  const int BSZ = 100000;
  char nc() {
    static char bf[BSZ], *p1 = bf, *p2 = bf;
    return p1 == p2 && (p2 = (p1 = bf) + fread(bf, 1, BSZ, stdin), p1 == p2) ? EOF
                                                                             : *p1++;
  }
  int rd() {
    int res = 0;
    char c = nc();
    while (!isdigit(c)) c = nc();
    while (isdigit(c)) res = res * 10 + c - '0', c = nc();
    return res;
  }
  long long rdll() {
    long long res = 0;
    char c = nc();
    while (!isdigit(c)) c = nc();
    while (isdigit(c)) res = res * 10 + c - '0', c = nc();
    return res;
  }
  void rd(char *s) {
    char c = nc();
    while (!isblank(c) && isprint(c) && c != EOF) *s++ = c, c = nc();
    *s = 0;
  }
  int tmp[20], lt;
  char OBF[BSZ], *OBP = OBF;
  void flush() { fwrite(OBF, 1, OBP - OBF, stdout), OBP = OBF; }
  void wrch(char x) {
    if (OBP == OBF + BSZ - 1) flush();
    *OBP = x, ++OBP;
  }
  void wr(int x, int base = 10) {
    lt = 0;
    while (x) tmp[++lt] = x % base, x /= base;
    while (lt > 0) wrch(tmp[lt] + '0'), --lt;
  }
  void wr(long long x, int base = 10) {
    lt = 0;
    while (x) tmp[++lt] = x % base, x /= base;
    while (lt > 0) wrch(tmp[lt] + '0'), --lt;
  }
  void wr(const char *s) {
    while (*s) wrch(*s), ++s;
  }
  void wr(char x) { wrch(x); }
} // namespace IO
const int N = 5e5 + 5, SQ = 5000;

int n, m, T, lt;
int a[N];
int D[N], ld;
vector<int> v[N];
int pos[N];
int w[SQ][SQ];
int c[N];

int query(int l, int r) {
  // log("l=%d,r=%d",l,r);
  int res = 0;
  if (l / T == r / T) {
    FOR(i, l, r) c[a[i]] = 0;
    FOR(i, l, r) res = max(res, ++c[a[i]]);
  } else {
    int L = l / T * T + T - 1, R = r / T * T;
    if (L + 1 <= R - 1) res = max(res, w[(L + 1) / T][(R - 1) / T]);
    FOR(i, l, L) {
      int x = pos[i], y = a[i];
      while (x + res < v[y].size() && v[y][x + res] <= r) ++res;
    }
    FOR(i, R, r) {
      int x = pos[i], y = a[i];
      while (x - res >= 0 && v[y][x - res] >= l) ++res;
    }
  }
  return res;
}
int las = 0;
int main() {
  n = IO::rd(), m = IO::rd();
  T = max(1, (int)(sqrt(n)));
  FOR(i, 1, n) {
    a[i] = IO::rd();
    D[++ld] = a[i];
  }
  sort(D + 1, D + ld + 1);
  ld = unique(D + 1, D + ld + 1) - D - 1;
  FOR(i, 1, n) a[i] = lower_bound(D + 1, D + ld + 1, a[i]) - D;
  log("time");
  FOR(i, 1, n) v[a[i]].pb(i), pos[i] = v[a[i]].size() - 1;
  lt = n / T;
  FOR(i, 0, lt) {
    fill(c, c + N, 0);
    int mx = 0;
    FOR(j, i, lt) {
      int l = max(1, j * T), r = min(n, j * T + T - 1);
      FOR(k, l, r) mx = max(mx, ++c[a[k]]);
      w[i][j] = mx;
    }
  }
  log("time");
  FOR(i, 1, m) {
    int l, r;
    l = IO::rd();
    r = IO::rd();
    // l^=las,r^=las;
    IO::wr(las = query(l, r)), IO::wr('\n');
  }
  IO::flush();
  log("time");
  return 0;
}
