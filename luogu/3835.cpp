#include <cstdio>
using namespace std;
const int N = 5e5 + 5, SZ = 1 << 24;
int n;

int seed = 1, tot; //总结点数
int lc[SZ], rc[SZ], val[SZ], rnd[SZ], sz[SZ];
int rt[SZ];

int rrand() { return seed *= 482711; }
int new_node(int v) { //新建一个权值为v的结点
  ++tot, lc[tot] = rc[tot] = 0;
  sz[tot] = 1, val[tot] = v, rnd[tot] = rrand();
  return tot;
}
int cp_node(int u) { //从结点u复制一个新的结点
  ++tot, lc[tot] = lc[u], rc[tot] = rc[u];
  sz[tot] = sz[u], val[tot] = val[u], rnd[tot] = rnd[u];
  return tot;
}
void pushup(int u) { //向上更新信息
  sz[u] = sz[lc[u]] + sz[rc[u]] + 1;
}
void split(int u, int k, int &x, int &y) { //可持久化分割，xy为新建结点
  if (!u) {
    x = y = 0;
    return;
  }
  int u2 = cp_node(u); //副本结点
  if (val[u] <= k)
    x = u2, split(rc[u2], k, rc[u2], y);
  else
    y = u2, split(lc[u2], k, x, lc[u2]);
  pushup(u2);
}
int merge(int x, int y) { //我们不需要维护两个子平衡树的版本，因此直接合并
  if (!x || !y) return x + y;
  if (rnd[x] < rnd[y])
    return rc[x] = merge(rc[x], y), pushup(x), x;
  else
    return lc[y] = merge(x, lc[y]), pushup(y), y;
}
int insert(int v, int u) { //插入键值，返回新的树根
  int x, y;
  split(u, v - 1, x, y);
  int z = new_node(v);
  return merge(x, merge(z, y));
}
int del(int v, int u) {
  int x, y, z;
  split(u, v - 1, x, y), split(y, v, y, z);
  if (y) y = merge(lc[y], rc[y]);
  return merge(x, merge(y, z));
}
int rank(int v, int u) { //这里虽然新建了x,y的结点，但是不需要这个版本，舍弃
  int x, y;
  split(u, v - 1, x, y);
  return sz[x] + 1;
}
int kth(int k, int u) { //第k小的数
  while (sz[lc[u]] + 1 != k) {
    if (k <= sz[lc[u]])
      u = lc[u];
    else
      k -= sz[lc[u]] + 1, u = rc[u];
  }
  return val[u];
}
int pre(int v, int u) {
  int rk = rank(v, u);
  if (rk == 1)
    return -2147483647; // v是第一，没有前驱
  else
    return kth(rk - 1, u);
}
int suc(int v, int u) {
  int rk = rank(v + 1, u);
  if (rk > sz[u]) return 2147483647;
  return kth(rk, u);
}

int main() {
  scanf("%d", &n);
  for (int i = 1; i <= n; i++) {
    int v, op, x;
    scanf("%d%d%d", &v, &op, &x);
    // printf("(%d,%d,%d)\n",v,op,x);
    if (op == 1) {
      rt[i] = insert(x, rt[v]);
    } else if (op == 2) {
      rt[i] = del(x, rt[v]);
    } else if (op == 3) {
      printf("%d\n", rank(x, rt[v])), rt[i] = rt[v];
    } else if (op == 4) {
      printf("%d\n", kth(x, rt[v])), rt[i] = rt[v];
    } else if (op == 5) {
      printf("%d\n", pre(x, rt[v])), rt[i] = rt[v];
    } else {
      printf("%d\n", suc(x, rt[v])), rt[i] = rt[v];
    }
  }
  return 0;
}
