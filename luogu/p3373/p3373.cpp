#include <cstdio>
#include <iostream>
#include <queue>

#define SIZEN 100

using namespace std;

int n, m, p, a[100001];
int b, x, y, k;
int sum[400001];
// queue <int> tag[400001];

struct my_queue {
  int data[SIZEN], head, tail;
  void push(int v) {
    data[tail] = v;
    tail++;
    if (tail == SIZEN) tail -= SIZEN;
  }
  void pop() {
    head++;
    if (head == SIZEN) head -= SIZEN;
  }
  int front() { return data[head]; }
  bool empty() { return head == tail; }
};

my_queue tag[400001];

inline void pushdown(int l, int r, int root) {
  if (l > r) return;
  //	printf("\t\t>>pushdown(%d,%d,%d):",l,r,root);
  while (!tag[root].empty()) {
    int opr = tag[root].front();
    tag[root].pop();
    //		printf("[%d]",opr);
    if (l != r) {
      tag[root << 1].push(opr);
      tag[root << 1 | 1].push(opr);
    }
    if (opr < 0)
      sum[root] = (sum[root] - opr * (r - l + 1)) % p;
    else
      sum[root] = (sum[root] * opr) % p;
  }
  //	printf("sum[%d,%d]:%d\n",l,r,sum[root]);
}
inline void pushup(int l, int r, int root) {
  int mid = (l + r) >> 1;
  pushdown(l, mid, root << 1);
  pushdown(mid + 1, r, root << 1 | 1);
  sum[root] = (sum[root << 1] + sum[root << 1 | 1]) % p;
}

void tree_build(int l, int r, int root) {
  if (l == r) {
    sum[root] = a[l];
    return;
  }
  int mid = (l + r) >> 1;
  tree_build(l, mid, root << 1);
  tree_build(mid + 1, r, root << 1 | 1);
  pushup(l, r, root);
}

void add(int L, int R, int l, int r, int root, int v) {
  //	printf("\t\t>>add(%d,%d,%d,%d,%d,%d):",L,R,l,r,root,v);
  pushdown(l, r, root);
  if (L <= l && r <= R) {
    tag[root].push(0 - v);
    return;
  }
  int mid = (l + r) >> 1;
  //!(R<l||r<L)
  if (!(R < l || mid < L)) add(L, R, l, mid, root << 1, v);
  if (!(R < mid + 1 || r < L)) add(L, R, mid + 1, r, root << 1 | 1, v);
  pushup(l, r, root);
}

void mul(int L, int R, int l, int r, int root, int v) {
  //	printf("\t\t>>mul(%d,%d,%d,%d,%d,%d):",L,R,l,r,root,v);
  pushdown(l, r, root);
  if (L <= l && r <= R) {
    tag[root].push(v);
    return;
  }
  int mid = (l + r) >> 1;
  //!(R<l||r<L)
  if (!(R < l || mid < L)) mul(L, R, l, mid, root << 1, v);
  if (!(R < mid + 1 || r < L)) mul(L, R, mid + 1, r, root << 1 | 1, v);
  pushup(l, r, root);
}

int sumset(int L, int R, int l, int r, int root) {
  pushdown(l, r, root);
  if (L <= l && r <= R) return sum[root];
  int mid = (l + r) >> 1, s = 0;
  //!(R<l||r<L)
  if (!(R < l || mid < L)) s += sumset(L, R, l, mid, root << 1);
  if (!(R < mid + 1 || r < L)) s += sumset(L, R, mid + 1, r, root << 1 | 1);
  pushup(l, r, root);
  return s % p;
}

int main() {
  //	freopen("in.txt","r",stdin);
  scanf("%d%d%d", &n, &m, &p);
  for (int i = 1; i <= n; i++) scanf("%d", &a[i]);
  tree_build(1, n, 1);
  for (int i = 1; i <= m; i++) {
    scanf("%d%d%d", &b, &x, &y);
    switch (b) {
      case 1:
        scanf("%d", &k);
        mul(x, y, 1, n, 1, k);
        break;
      case 2:
        scanf("%d", &k);
        add(x, y, 1, n, 1, k);
        break;
      default:
        printf("%d\n", sumset(x, y, 1, n, 1));
    }
  }
  return 0;
}
