#include <iostream>

using namespace std;

int l, n, a, x, y, g, h;
bool tree[10001]; // 0:alive;1:disap
int p[10001], c[10001];
int main() {
  cin >> l >> n;
  for (int i = 1; i <= n; i++) {
    cin >> a >> x >> y;
    if (a == 0) {
      for (int j = x; j <= y; j++) {
        if (tree[j] == 0) c[j]++, tree[j] = 1;
      }
    }
    if (a == 1) {
      for (int j = x; j <= y; j++) {
        if (tree[j] == 1) p[j]++, tree[j] = 0;
      }
    }
  }
  for (int i = 1; i <= l; i++) cout << tree[i];
  cout << endl;
  for (int i = 1; i <= l; i++) cout << p[i];
  cout << endl;
  for (int i = 1; i <= l; i++) cout << c[i];
  cout << endl;
  for (int i = 1; i <= l; i++) {
    if (tree[i] == 0)
      g++;
    else if (c[i] != 0)
      h++;
  }
  cout << g << endl << h;
  return 0;
}
