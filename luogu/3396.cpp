#include <cmath>
#include <cstdio>
using namespace std;
const int N = 150004;
int n, m;
int a[N];
int tab[1000][1000];
int main() {
  scanf("%d%d", &n, &m);
  int p = pow(n, 0.3333);
  for (int i = 1; i <= n; i++) {
    scanf("%d", &a[i]);
    for (int j = 1; j <= p; j++) tab[j][i % j] += a[i];
  }
  for (int i = 1; i <= m; i++) {
    char op[5];
    int x, y;
    scanf("%s%d%d", op, &x, &y);
    if (op[0] == 'A') {
      if (x <= p)
        printf("%d\n", tab[x][y]);
      else {
        int ans = 0;
        for (int j = y; j <= n; j += x) ans += a[j];
        printf("%d\n", ans);
      }
    } else {
      for (int j = 1; j <= p; j++) tab[j][x % j] -= a[x];
      for (int j = 1; j <= p; j++) tab[j][x % j] += y;
      a[x] = y;
    }
  }

  return 0;
}
/*
 * 对模数小于p的直接建池，大于的直接暴力
 */
