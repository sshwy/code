#include <iostream>

using namespace std;

int n, m, num;
bool bp[10000001];
int pri[100000], tot;

int main() {
  cin >> n >> m;
  bp[0] = bp[1] = true;
  for (int p = 2; p <= n; p++) {
    if (bp[p] == false) tot++, pri[tot] = p;
    for (int t = 1; t <= tot; t++) {
      bp[pri[t] * p] = true;
      if (p % pri[t] == 0) break;
    }
  }
  for (int i = 1; i <= m; i++) {
    cin >> num;
    if (bp[num] == false)
      cout << "Yes\n";
    else
      cout << "No\n";
  }
  return 0;
}
