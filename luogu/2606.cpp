#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
namespace IO {
  char nc() {
    static char bf[100000], *p1 = bf, *p2 = bf;
    return p1 == p2 && (p2 = (p1 = bf) + fread(bf, 1, 100000, stdin), p1 == p2)
               ? EOF
               : *p1++;
  }
  int rd() {
    int res = 0;
    char c = getchar();
    while (!isdigit(c)) c = getchar();
    while (isdigit(c)) res = res * 10 + c - '0', c = getchar();
    return res;
  }
} // namespace IO
/******************heading******************/
lld pw(lld a, lld m, lld p) {
  lld res = 1;
  while (m) m & 1 ? res = res * a % p : 0, a = a * a % p, m >>= 1;
  return res;
}
const lld N = 1e6 + 6;
lld n, P, mem = 1, dem = 1, cnt;
lld p[N], lp;
bool bp[N];
void sieve(lld lim) {
  bp[0] = bp[1] = 1;
  FOR(i, 2, lim) {
    if (!bp[i]) p[++lp] = i;
    FOR(j, 1, lp) {
      if (i * p[j] > lim) break;
      bp[i * p[j]] = 1;
      if (i % p[j] == 0) break;
    }
  }
}
void decom(lld x) {
  // printf("decom(%lld)\n",x);
  while (x % P == 0) --cnt, x /= P;
  dem = dem * x % P;
  // printf("dem:%lld\n",dem);
}
lld dfs(lld u) {
  if (u > n) return 0;
  lld sz = 1 + dfs(u << 1) + dfs(u << 1 | 1);
  // printf("sz[%lld]:%lld\n",u,sz);
  decom(sz);
  return sz;
}
void calc(lld x, lld d) {
  lld res = 0;
  while (x) res += x / d, x /= d;
  // printf("calc(%lld,%lld):%lld\n",x,d,res);
  if (d == P)
    cnt += res;
  else
    mem = mem * pw(d, res, P) % P;
}
int main() {
  scanf("%lld%lld", &n, &P);
  sieve(n);
  FOR(i, 1, lp) {
    if (p[i] > n) break;
    calc(n, p[i]);
  }
  dfs(1);
  if (cnt > 1)
    puts("0");
  else
    printf("%lld\n", mem * pw(dem, P - 2, P) % P);
  return 0;
}
