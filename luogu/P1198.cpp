#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
const ll M = 200001;
const int _INF = 1 << 31;
ll m, d, t, tail;
ll mx[1 << 19];

ll lst_upd(ll p, ll v, ll l, ll r, ll rt) {
  if (p == l && p == r) return mx[rt] = v;
  ll mid = (l + r) >> 1;
  if (p <= mid)
    lst_upd(p, v, l, mid, rt << 1);
  else
    lst_upd(p, v, mid + 1, r, rt << 1 | 1);
  mx[rt] = max(mx[rt << 1], mx[rt << 1 | 1]);
}
ll lst_max(ll L, ll R, ll l, ll r, ll rt) {
  if (L <= l && r <= R) return mx[rt];
  ll mid = (l + r) >> 1, mxx = _INF;
  if (L <= mid) mxx = max(mxx, lst_max(L, R, l, mid, rt << 1));
  if (mid < R) mxx = max(mxx, lst_max(L, R, mid + 1, r, rt << 1 | 1));
  return mxx;
}

int main() {
  scanf("%lld%lld", &m, &d);
  memset(mx, 0x80, sizeof(mx));
  for (ll i = 1, x; i <= m; i++) {
    char c[10];
    scanf("%s%lld", c, &x);
    if (c[0] == 'A') {
      ++tail;
      lst_upd(tail, (x + t) % d, 1, m, 1);
    } else {
      t = lst_max(tail - x + 1, tail, 1, m, 1);
      printf("%lld\n", t);
    }
  }
  return 0;
}
