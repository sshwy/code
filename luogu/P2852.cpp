#include <bits/stdc++.h>
using namespace std;
const int N = 20004;
int n, k;
int s[N];
int sa[N], rk[N], h[N];

void suffix_array() {
  int bin[N];
  int sz = max(50, n);

  for (int i = 0; i < n; i++) rk[i] = s[i];

  for (int i = 0; i < sz; i++) bin[i] = 0;
  for (int i = 0; i < n; i++) ++bin[rk[i]];
  for (int i = 1; i < sz; i++) bin[i] += bin[i - 1];
  for (int i = n - 1; i >= 0; i--) sa[--bin[rk[i]]] = i;

  int tmp[N];
  for (int j = 1; j <= n; j <<= 1) {
    int p = -1;
    for (int i = n - j; i < n; i++) tmp[++p] = i;
    for (int i = 0; i < n; i++)
      if (sa[i] - j >= 0) tmp[++p] = sa[i] - j;

    for (int i = 0; i < sz; i++) bin[i] = 0;
    for (int i = 0; i < n; i++) ++bin[rk[tmp[i]]];
    for (int i = 1; i < sz; i++) bin[i] += bin[i - 1];
    for (int i = n - 1; i >= 0; i--) sa[--bin[rk[tmp[i]]]] = tmp[i];

    p = 0, tmp[sa[0]] = 0;
    for (int i = 1; i < n; i++) {
      int v0 = sa[i - 1], v1 = sa[i], v00, v01;
      v00 = v0 + j < n ? rk[v0 + j] : -1;
      v01 = v1 + j < n ? rk[v1 + j] : -1;
      tmp[sa[i]] = (rk[v0] == rk[v1] && v00 == v01) ? p : ++p;
    }
    for (int i = 0; i < n; i++) rk[i] = tmp[i];
  }
}

int calc(int x, int y, int res) {
  while (x + res < n && y + res < n && s[x + res] == s[y + res]) res++;
  return res;
}
void calc_h() {
  h[0] = rk[0] == 0 ? 0 : calc(0, sa[rk[0] - 1], 0);
  for (int i = 1; i < n; i++)
    h[i] = rk[i] == 0 ? 0 : calc(i, sa[rk[i] - 1], max(h[i - 1] - 1, 0));
}

int main() {
  // freopen("P2852.in","r",stdin);
  scanf("%d%d", &n, &k);
  for (int i = 0; i < n; i++) scanf("%d", &s[i]);
  suffix_array();
  calc_h();
  // for(int i=0;i<n;i++)printf("%d ",h[sa[i]]);
  // puts("");
  //维护h[]每连续k个元素的最小值的最大值
  int q[N], bg = 1, ed = 0, mx = 0;
  for (int i = 0; i < n; i++) {
    while (bg <= ed && q[ed] >= h[sa[i]]) ed--;
    q[++ed] = h[sa[i]];
    if (i > k - 1) {
      // printf("%d\n",q[bg]);
      mx = max(mx, q[bg]);
      bg++;
    }
  }
  printf("%d", mx);
  return 0;
}