#include <cstdio>
#include <iostream>

using namespace std;

int n, m, u, v, tot;
int re[1001][1001];
char c;
bool vis[1001];
bool f;

void graph_search(int start) {
  vis[start] = true;
  for (int i = 1; i <= n; i++) {
    if (vis[i] == false && re[start][i] != 0) {
      printf("[%3d->%3d]", start, i);
      if (re[start][i] == 2) f = true;
      graph_search(i);
    }
  }
}

int main() {
  freopen("p1892.in", "r", stdin);
  cin >> n >> m;
  for (int i = 1; i <= m; i++) {
    cin >> c >> u >> v;
    if (c == 'E')
      re[u][v] = re[v][u] = 2;
    else
      re[u][v] = re[v][u] = 1;
  }
  for (int i = 1; i <= n; i++) {
    if (vis[i] == false) {
      f = false;
      graph_search(i);
      tot += f + 1;
    }
  }
  cout << tot + 1;
  return 0;
}
