#include <iostream>
#define max(a, b) (a > b ? a : b)

using namespace std;

int m, n;
int qp[51][51];
int f[102][51][51];

int main() {
  cin >> m >> n;
  for (int i = 1; i <= m; i++)
    for (int j = 1; j <= n; j++) cin >> qp[i][j];
  for (int i = 1; i <= n + m - 1; i++) {
    for (int j = 1; j <= m && j <= i; j++) {
      for (int k = 1; k <= m && k <= i; k++) {
        f[i][j][k] = max(max(f[i - 1][j][k], f[i - 1][j - 1][k]),
                         max(f[i - 1][j][k - 1], f[i - 1][j - 1][k - 1])) +
                     qp[j][i - j + 1] + qp[k][i - k + 1];
        if (j == k) f[i][j][k] -= qp[j][i - j + 1];
      }
    }
  }
  cout << f[n + m - 1][m][m];
  return 0;
}
