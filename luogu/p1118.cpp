#include <cstdio>
#include <iostream>

using namespace std;

const int fop[13][13] = {{0}, {0, 1}, {0, 1, 1}, {0, 1, 2, 1}, {0, 1, 3, 3, 1},
    {0, 1, 4, 6, 4, 1}, {0, 1, 5, 10, 10, 5, 1}, {0, 1, 6, 15, 20, 15, 6, 1},
    {0, 1, 7, 21, 35, 35, 21, 7, 1}, {0, 1, 8, 28, 56, 70, 56, 28, 8, 1},
    {0, 1, 9, 36, 84, 126, 126, 84, 36, 9, 1},
    {0, 1, 10, 45, 120, 210, 252, 210, 120, 45, 10, 1},
    {0, 1, 11, 55, 165, 330, 462, 462, 330, 165, 55, 11, 1}};

int n, sum;
int f[13], f2[13];
bool bf[13], mf = 0;

/*bool isok()
{
  int isum=0;
  for(int i=1;i<=n;i++)isum+=f[i]*fop[n][i];
  if(isum==sum)return 1;
  return 0;
}*/

void search(int k, int nsum) {
  if (nsum > sum) return;
  if (k == n + 1) {
    if (nsum == sum) {
      for (int i = 1; i <= n; i++) cout << f[i] << ' ';
      mf = 1;
      return;
    }
  }
  if (mf == 1) return;
  for (int i = 1; i <= n; i++) {
    if (bf[i] == 0) {
      bf[i] = 1;
      f[k] = i;

      search(k + 1, nsum + f[k] * fop[n][k]);

      bf[i] = 0;
    }
  }
}

int main() {
  //	freopen("p1118in.txt","r",stdin);
  cin >> n >> sum;

  search(1, 0);

  return 0;
}
