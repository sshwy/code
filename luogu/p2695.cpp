#include <algorithm>
#include <iostream>

using namespace std;

int n, m, tot;
int dr[20001], pe[20001];

int main() {
  cin >> n >> m;
  for (int i = 1; i <= n; i++) cin >> dr[i];
  for (int i = 1; i <= m; i++) cin >> pe[i];
  sort(dr + 1, dr + n + 1);
  sort(pe + 1, pe + m + 1);
  int d, p;
  for (d = 1, p = 1; d <= n && p <= m; p++) {
    if (pe[p] >= dr[d]) {
      tot += pe[p];
      d++;
    }
  }
  if (d <= n)
    cout << "you died!";
  else
    cout << tot;
  return 0;
}
