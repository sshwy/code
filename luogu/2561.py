import math

def calc(n) :
	return n * (n + 1) // 2;

n = (int)(input(""));
ans = pow(2, calc(n));
cnt = calc(n) // 3 + (n % 3 == 1);
ans += 2 * pow(2, cnt);
cnt = calc(n // 2) + calc((n + 1) // 2);
ans += 3 * pow(2, cnt);
print(ans // 6);
