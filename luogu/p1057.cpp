#include <iostream>

using namespace std;

int n, m;      //设小蛮为0号
int f[40][40]; // f[i:m][j:n]:传i次后到了j的手上的方案数

int main() {
  cin >> n >> m;
  f[0][0] = 1; //开始时
  for (int im = 1; im <= m; im++) {
    for (int jn = 0; jn < n; jn++) {
      f[im][jn] = f[im - 1][(jn - 1 + n) % n] + f[im - 1][(jn + 1) % n];
    }
  }
  cout << f[m][0];
  return 0;
}
