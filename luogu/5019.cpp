#include <bits/stdc++.h>
using namespace std;
const int N = 1e5 + 5;
int n, ans;
int a[N];
int main() {
  scanf("%d", &n);
  for (int i = 1; i <= n; i++) {
    scanf("%d", &a[i]);
    ans += max(a[i - 1] - a[i], 0);
  }
  ans += a[n];
  printf("%d\n", ans);
  return 0;
}
