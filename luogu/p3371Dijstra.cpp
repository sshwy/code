#include <iostream>
#include <queue>
#include <vector>

using namespace std;

const int INF = 99999999;
int n, m, s;
int dis[10001];  // shortest path so far
bool vis[10001]; // 1:is visited

struct side {
  int f, t, v; // from,to,value
} tside;

vector<int> g[10001]; // Graph sides note
vector<side> sides;   // each sides

struct heapnode {
  int hdis, hnode; // dis,node note
  bool operator<(const heapnode &rhs) const { return this->hdis > rhs.hdis; }
};

priority_queue<heapnode> q; // hdis smallest first out

int main() {
  cin >> n >> m >> s;
  for (int i = 1; i <= m; i++) {
    cin >> tside.f >> tside.t >> tside.v;
    sides.push_back(tside);
    g[tside.f].push_back(i - 1);
  }
  for (int i = 1; i <= n; i++) dis[i] = INF;
  dis[s] = 0;

  heapnode hs; // start
  hs.hdis = 0;
  hs.hnode = s;

  q.push(hs);

  while (!q.empty()) {
    heapnode hnow = q.top();
    q.pop();
    int un = hnow.hnode;

    if (vis[un]) continue;
    vis[un] = true;

    for (int i = 0; i < g[un].size(); i++) // i:side note
    {
      side ns = sides[g[un][i]];
      if (dis[ns.t] > dis[un] + ns.v) {
        dis[ns.t] = dis[un] + ns.v;
        heapnode next = {dis[ns.t], ns.t};
        q.push(next);
      }
    }
  }

  for (int i = 1; i <= n; i++) {
    if (dis[i] >= INF)
      cout << 2147483647 << ' ';
    else
      cout << dis[i] << ' ';
  }
  return 0;
}
