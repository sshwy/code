#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
namespace IO {
  char nc() {
    static char bf[100000], *p1 = bf, *p2 = bf;
    return p1 == p2 && (p2 = (p1 = bf) + fread(bf, 1, 100000, stdin), p1 == p2)
               ? EOF
               : *p1++;
  }
  int rd() {
    int res = 0;
    char c = getchar();
    while (!isdigit(c)) c = getchar();
    while (isdigit(c)) res = res * 10 + c - '0', c = getchar();
    return res;
  }
} // namespace IO
/******************heading******************/
const int N = 1e6 + 5;
int n, m;
int p[N];
int s[N], s2[N], s3[N], sz[N];
/*
 * s: 状态
 * s2: 把儿子结点全部翻转的次数
 * s3: 儿子结点中亮着的灯的数量
 */
void node_upd(int u) {
  if (!u) return;
  if (p[u]) s3[p[u]] -= s[u];
  s[u] ^= 1;
  if (p[u]) s3[p[u]] += s[u];
}
void upd(int u) {
  if (!u) return;
  if (p[u]) node_upd(p[u]);
  node_upd(u);
  s2[u] ^= 1;
}
int node_query(int u) {
  if (!u) return 0;
  int res = s[u];
  if (p[u]) res ^= s2[p[u]];
  return res;
}
int query(int u) {
  return node_query(u) + node_query(p[u]) + (s2[u] ? sz[u] - s3[u] : s3[u]);
}
int main() {
  scanf("%d", &n);
  FOR(i, 2, n) scanf("%d", &p[i]), sz[p[i]]++;
  scanf("%d", &m);
  FOR(i, 1, m) {
    int t, x;
    scanf("%d %d", &t, &x);
    if (t == 1) {
      upd(x);
    } else {
      printf("%d\n", query(x));
    }
    // printf("t=%d,x=%d\n",t,x);
    // printf("s : "); FOR(i,1,n)printf("%d ",s[i]); puts("");
    // printf("s2: "); FOR(i,1,n)printf("%d ",s2[i]); puts("");
    // printf("s3: "); FOR(i,1,n)printf("%d ",s3[i]); puts("");
  }
  return 0;
}
