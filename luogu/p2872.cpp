#include <algorithm>
#include <cmath>
#include <cstdio>
#include <iostream>

using namespace std;

long long n, m, a1, a2, lp;
long long f[1001];
long double tot = 0;

struct point {
  long long x, y;
} farm[1001];

struct path {
  long long f, t;
  long double v;
} pth[1000003];

bool cmp(path p1, path p2) { return p1.v < p2.v; }
long double dis(point p1, point p2) {
  return sqrt((p1.x - p2.x) * (p1.x - p2.x) + (p1.y - p2.y) * (p1.y - p2.y));
}

long long gf(long long k) { return f[k] == k ? k : f[k] = gf(f[k]); }
void un(long long a, long long b) { f[gf(a)] = gf(b); }

int main() {
  cin >> n >> m;
  for (long long i = 1; i <= n; i++) {
    cin >> farm[i].x >> farm[i].y;
    f[i] = i;
  }
  for (long long i = 1; i <= m; i++) {
    cin >> a1 >> a2;
    un(a1, a2);
  }
  for (long long i = 1; i <= n; i++) {
    for (long long j = 1; j < i; j++) {
      if (gf(i) != gf(j)) pth[++lp] = (path){i, j, dis(farm[i], farm[j])};
    }
  }
  sort(pth + 1, pth + 1 + lp, cmp);
  for (long long i = 1; i <= lp; i++) {
    if (gf(pth[i].f) != gf(pth[i].t)) {
      un(pth[i].f, pth[i].t);
      tot += pth[i].v;
    }
  }
  printf("%.2Lf", tot);
  return 0;
}
