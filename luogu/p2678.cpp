#include <iostream>

using namespace std;

int l, n, m;
int low_bound, up_bound, mid; // shortest jump destance
int stone[50010];

int main() {
  cin >> l >> n >> m;
  if (n == m) {
    cout << l;
    return 0;
  }
  for (int i = 1; i <= n; i++) { cin >> stone[i]; }
  stone[n + 1] = l; // end

  low_bound = 0;
  up_bound = l;

  do {

    mid = (low_bound + up_bound) / 2;
    int move_stone = 0, pre_stone = 0;

    for (int now_stone = 1; now_stone <= n + 1; now_stone++) {
      if (stone[now_stone] - stone[pre_stone] < mid) {
        move_stone++;
      } else {
        pre_stone = now_stone;
      }
    }
    if (move_stone <= m) {
      low_bound = mid;
    } else {
      up_bound = mid;
    }
  } while (low_bound + 1 != up_bound);

  cout << low_bound;
  return 0;
}
