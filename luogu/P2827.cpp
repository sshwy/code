#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
ll n, m, q, u, v, t;
priority_queue<int> pq;
int main() {
  scanf("%d%d%d%d%d%d", &n, &m, &q, &u, &v, &t);
  for (ll i = 1; i <= n; i++) {
    ll a;
    scanf("%d", &a);
    pq.push(a);
  }
  for (ll i = 0; i < m; i++) {
    if ((i + 1) % t == 0) printf("%d ", pq.top() + i * q);
    ll now = pq.top();
    pq.pop();
    now += i * q;
    ll x = now * u / v;
    pq.push(x - q - i * q);
    pq.push(now - x - q - i * q);
  }
  printf("\n");
  ll t2 = 1;
  while (!pq.empty()) {
    if (t2 % t == 0) printf("%d ", pq.top() + m * q);
    pq.pop();
    t2++;
  }
  return 0;
}
