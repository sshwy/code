// by Sshwy
//#define DEBUGGER
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

#ifdef DEBUGGER

#define Log_prefix()                                                \
  {                                                                 \
    fprintf(stderr, "\033[37mLine %-3d [%dms]:\033[0m  ", __LINE__, \
        (int)clock() / 1000);                                       \
  }
#define iLog(...) \
  { fprintf(stderr, __VA_ARGS__); }
#define lLog(...)      \
  {                    \
    Log_prefix();      \
    iLog(__VA_ARGS__); \
  }
#define Log(...)           \
  {                        \
    Log_prefix();          \
    iLog(__VA_ARGS__);     \
    fprintf(stderr, "\n"); \
  }
#define red(...)       \
  {                    \
    Log_prefix();      \
    iLog("\033[31m");  \
    iLog(__VA_ARGS__); \
    iLog("\033[0m\n"); \
  }
#define green(...)     \
  {                    \
    Log_prefix();      \
    iLog("\033[32m");  \
    iLog(__VA_ARGS__); \
    iLog("\033[0m\n"); \
  }
#define blue(...)      \
  {                    \
    Log_prefix();      \
    iLog("\033[34m");  \
    iLog(__VA_ARGS__); \
    iLog("\033[0m\n"); \
  }

#else

#define Log_prefix() ;
#define iLog(...) ;
#define lLog(...) ;
#define Log(...) ;
#define red(...) ;
#define green(...) ;
#define blue(...) ;

#endif

namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 1e5 + 5;
int n, m, q, T;
int V[N], W[N], C[N];
vector<int> g[N];

int totdfn;
int L[N], R[N], fa[N][20], dep[N], nd[N * 2];
void dfs(int u, int p) {
  dep[u] = dep[p] + 1, fa[u][0] = p;
  FOR(j, 1, 19) {
    fa[u][j] = fa[fa[u][j - 1]][j - 1];
    if (fa[u][j] == 0) break;
  }
  L[u] = ++totdfn, nd[L[u]] = u;
  for (int v : g[u])
    if (v != p) dfs(v, u);
  R[u] = ++totdfn, nd[R[u]] = u;
}
int lca(int u, int v) {
  if (dep[u] < dep[v]) swap(u, v);
  ROF(j, 19, 0) if (dep[u] - (1 << j) >= dep[v]) u = fa[u][j];
  if (u == v) return u;
  ROF(j, 19, 0) if (fa[u][j] != fa[v][j]) u = fa[u][j], v = fa[v][j];
  return fa[u][0];
}

struct atom {
  int a, b, c, d, e, f;
} qry[N], op[N];
int lq, lo;
bool cmp(atom x, atom y) {
  return x.a / T != y.a / T ? x.a < y.a : x.b / T != y.b / T ? x.b < y.b : x.e < y.e;
}

int cl, cr, co;
long long cur = 0;
int c[N]; //每种糖果出现数量
void insert(int x) { cur += W[++c[x]] * 1ll * V[x]; }
void remove(int x) { cur -= W[c[x]--] * 1ll * V[x]; }
int cn[N]; //每个结点的出现次数的奇偶性
void toggle(int u) { (cn[u] ^= 1) ? insert(C[u]) : remove(C[u]); }

void addL() { toggle(nd[cl++]); }
void subL() { toggle(nd[--cl]); }
void addR() { toggle(nd[++cr]); }
void subR() { toggle(nd[cr--]); }
void addO() {
  ++co;
  int x = op[co].a, y = op[co].b, z = op[co].c;
  assert(C[x] == z);
  if (cn[x]) remove(z), insert(y);
  C[x] = y;
}
void subO() {
  int x = op[co].a, y = op[co].b, z = op[co].c;
  assert(C[x] == y);
  if (cn[x]) remove(y), insert(z);
  C[x] = z;
  --co;
}
long long ans[N];
int main() {
  scanf("%d%d%d", &n, &m, &q);
  FOR(i, 1, m) scanf("%d", &V[i]);
  FOR(i, 1, n) scanf("%d", &W[i]);
  FOR(i, 1, n - 1) {
    int x, y;
    scanf("%d%d", &x, &y);
    g[x].pb(y), g[y].pb(x);
  }
  dfs(1, 0);
  FOR(i, 1, n) scanf("%d", &C[i]);
  FOR(i, 1, q) {
    int t, x, y;
    scanf("%d%d%d", &t, &x, &y);
    if (t == 0) {
      op[++lo] = {x, y, C[x]}, C[x] = y;
    } else {
      if (L[x] > L[y]) swap(x, y);
      if (R[x] >= R[y]) { //祖孙or x==y
        qry[++lq] = {L[x], L[y], 0, 0, lo, lq};
      } else {
        assert(R[x] < L[y]);
        qry[++lq] = {R[x], L[y], 1, lca(x, y), lo, lq};
      }
    }
  }
  T = max(1, (int)exp(log(totdfn) * 2.0 / 3));
  sort(qry + 1, qry + lq + 1, cmp);
  cl = 1, cr = 0, co = lo;

  FOR(i, 1, q) {
    int l = qry[i].a, r = qry[i].b, o = qry[i].e;
    while (cr < r) addR();
    while (l < cl) subL();
    while (cr > r) subR();
    while (cl < l) addL();
    while (co < o) addO();
    while (co > o) subO();
    if (qry[i].c) insert(C[qry[i].d]);
    ans[qry[i].f] = cur;
    if (qry[i].c) remove(C[qry[i].d]);
  }

  FOR(i, 1, lq) printf("%lld\n", ans[i]);
  return 0;
}
