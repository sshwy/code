// by Sshwy
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
template <const int BSZ = 100000> class Stream {
private:
  int lt;
  char tmp[70];
  char OBF[BSZ], *OBP = OBF;
  char nc() {
    static char bf[BSZ], *p1 = bf, *p2 = bf;
    return p1 == p2 && (p2 = (p1 = bf) + fread(bf, 1, BSZ, stdin), p1 == p2) ? EOF
                                                                             : *p1++;
  }
  void flush() { fwrite(OBF, 1, OBP - OBF, stdout), OBP = OBF; }

public:
  Stream() {}
  ~Stream() { flush(); }
  template <class T> void rd(T &x) {
    char c = nc();
    bool tag = 0;
    T res = 0;
    while (c != EOF && !isdigit(c) && c != '-') c = nc();
    if (c == EOF) return;
    if (c == '-') tag = 1, c = nc();
    while (c != EOF && isdigit(c)) res = res * (T)10 + c - '0', c = nc();
    if (tag) res = -res;
    x = res;
  }
  void rd(char *s) {
    char c = nc();
    while (c != EOF && (isblank(c) || !isprint(c))) c = nc();
    if (c == EOF) return;
    while (!isblank(c) && isprint(c) && c != EOF) *s++ = c, c = nc();
    *s = 0;
  }
  void rd(char &x) { x = nc(); }
  template <class T> void wr(T x) {
    if (x == 0) return wr('0'), void();
    if (x < 0) wr('-'), x = -x;
    lt = 0;
    while (x) tmp[++lt] = x % 10, x /= 10;
    ROF(i, lt, 1) wr((char)(tmp[i] + '0'));
  }
  void wr(char x) {
    if (OBP == OBF + BSZ - 1) flush();
    *OBP = x, ++OBP;
  }
  void wr(const char *s) {
    char *t = s;
    while (*t) wr(*t), ++t;
  }
  template <class T> Stream &operator>>(T &x) {
    rd(x);
    return *this;
  }
  template <class T> Stream &operator<<(T x) {
    wr(x);
    return *this;
  }
};
/******************heading******************/
char endll = '\n';
Stream<> io;
#define endl endll
#define cin io
#define cout io

const int N = 13;
int n, m;
int a[N][N];
map<int, long long> f, g;

//对于每个格子，看他左边和上面的plug的数量。
// 1. 都没有：这个格子可以不管，或者可以新开2个plug。
// 2. 有一个：继承。
// 3. 有两个：闭合。
//
//对于
//(i,m-1)的格子，可以转移到(i+1,-1)，这样可以正常转移到(i+1,0).或者在(i+1,0)的时侯特殊讨论一下转移也可。
//这个东西显然是可以滚两维的。因为只和上一个状态有关。
void go() {
  cin >> n >> m;
  FOR(i, 0, n - 1) FOR(j, 0, m - 1) { cin >> a[i][j]; }
  f.clear(), g.clear();
  f[0] = 1; //(0,-1)
  FOR(i, 0, n - 1) {
    FOR(j, 0, m - 1) {
      g = f;
      f.clear();
      if (j == 0) {
        for (pair<int, long long> x : g) {
          int mask = x.fi, newMask;
          long long v = x.se;
          if (mask >> m & 1) continue;
          int cnt = (mask >> j & 1) + (mask >> m & 1);
          assert(cnt < 2);
          if (cnt == 1) {
            if (a[i][j]) f[mask] += v;
            newMask = mask ^ (1 << j) ^ (1 << m); //转向
            if (a[i][j]) f[newMask] += v;
          } else {
            if (!a[i][j]) f[mask] += v; //不放
            newMask = mask ^ (1 << j) ^ (1 << m);
            if (a[i][j]) f[newMask] += v;
          }
        }
      } else {
        for (pair<int, long long> x : g) {
          int mask = x.fi, newMask;
          long long v = x.se;
          int cnt = (mask >> j & 1) + (mask >> m & 1);
          if (cnt == 2) {
            newMask = mask ^ (1 << j) ^ (1 << m);
            if (a[i][j]) f[newMask] += v;
          } else if (cnt == 1) {
            if (a[i][j]) f[mask] += v;
            newMask = mask ^ (1 << j) ^ (1 << m); //转向
            if (a[i][j]) f[newMask] += v;
          } else {
            if (!a[i][j]) f[mask] += v; //不放
            newMask = mask ^ (1 << j) ^ (1 << m);
            if (a[i][j]) f[newMask] += v;
          }
        }
      }
    }
  }
  cout << f[0] << endl;
}
signed main() {
  int t;
  cin >> t;
  FOR(i, 1, t) go();
  return 0;
}
