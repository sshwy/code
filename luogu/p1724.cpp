#include <cstring>
#include <iostream>

using namespace std;

int t, x, y, m, cx, cy;
char ord[10001];

int main() {
  cin >> ord >> t;
  m = strlen(ord);
  for (int i = 0; i < m; i++) {
    switch (ord[i]) {
      case 'E':
        cx++;
        break;
      case 'S':
        cy--;
        break;
      case 'W':
        cx--;
        break;
      case 'N':
        cy++;
        break;
    }
  }
  for (int i = 0; i < t % m; i++) {
    switch (ord[i]) {
      case 'E':
        x++;
        break;
      case 'S':
        y--;
        break;
      case 'W':
        x--;
        break;
      case 'N':
        y++;
        break;
    }
  }
  cout << x + cx * (t / m) << ' ' << y + cy * (t / m);
  return 0;
}
