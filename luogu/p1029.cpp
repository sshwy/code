#include <iostream>

using namespace std;

int x, y, p, q, a, tot;

int gcd(int a, int b) {
  if (a < b) a ^= b ^= a ^= b;
  if (a % b == 0) return b;
  return gcd(b, a % b);
}

int main() {
  cin >> x >> y;
  for (int i = x; i <= y; i += x) {
    if (x * y % i == 0) {
      if (gcd(i, x * y / i) == x) tot++;
    }
  }
  cout << tot;
  return 0;
}
