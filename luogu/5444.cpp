// by Sshwy
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int N = 1e6 + 5;

long long n, a, b;
long long gcd(long long a, long long b) { return b ? gcd(b, a % b) : a; }
pair<long long, long long> A[N * 2];
int la;

int main() {
  scanf("%lld%lld%lld", &n, &a, &b);
  long long k = a / gcd(a, b + 1), T;
  if (1e18 / k < b)
    T = 1e18 + 1;
  else
    T = k * b;
  // printf("T=%lld\n",T);
  FOR(i, 1, n) {
    long long l, r;
    scanf("%lld%lld", &l, &r);
    if (r - l >= T) {
      printf("%lld\n", T);
      return 0;
    }
    l %= T, r %= T;
    if (l <= r)
      A[++la] = {l, r};
    else {
      A[++la] = {l, T - 1};
      A[++la] = {0, r};
    }
  }
  sort(A + 1, A + la + 1);
  long long ans = 0, mx = -1;
  FOR(i, 1, la) {
    if (mx < A[i].fi)
      ans += A[i].se - A[i].fi + 1;
    else
      ans += max(0ll, A[i].se - mx);
    mx = max(mx, A[i].se);
  }
  printf("%lld\n", ans);
  return 0;
}
