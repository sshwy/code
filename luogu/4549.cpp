#include <bits/stdc++.h>
using namespace std;

int n, p;

int exgcd(int a, int b, int &x, int &y) {
  if (!b) return x = 1, y = 0, a;
  int g = exgcd(b, a % b, y, x);
  y = y - a / b * x;
  return g;
}
int main() {
  scanf("%d%d", &n, &p);
  for (int i = 1; i <= n; i++) {
    int x, y;
    exgcd(i, p, x, y);
    x = (x + p) % p;
    printf("%d\n", x);
  }
  return 0;
}
