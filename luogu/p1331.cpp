#include <iostream>
using namespace std;
int r, c, tot;
char qp[1001][1001];
int main() {
  cin >> r >> c;
  for (int i = 1; i <= r; i++)
    for (int j = 1; j <= c; j++) cin >> qp[i][j];
  for (int i = 1; i <= r; i++)
    for (int j = 1; j <= c; j++)
      if (qp[i][j] == '#') {
        tot++;
        int x = i, y = j, f1 = 1;
        while (qp[x][j] == '#' && x <= r) x++;
        x--;
        for (y = j; f1 && y <= c; y++)
          for (int k = i; k <= x; k++)
            if (qp[k][y + 1] != '#') {
              f1 = 0;
              break;
            }
        y--;
        for (int a = i; a <= x; a++)
          for (int b = j; b <= y; b++) qp[a][b] = '-';
        if (j > 1)
          for (int a = i; a <= x; a++)
            if (qp[a][j - 1] == '#') {
              cout << "Bad placement.";
              return 0;
            }
        if (y < c)
          for (int a = i; a <= x; a++)
            if (qp[a][y + 1] == '#') {
              cout << "Bad placement.";
              return 0;
            }
        if (i > 1)
          for (int b = j; b <= y; b++)
            if (qp[i - 1][b] == '#') {
              cout << "Bad placement.";
              return 0;
            }
        if (x < r)
          for (int b = j; b <= y; b++)
            if (qp[x + 1][b] == '#') {
              cout << "Bad placement.";
              return 0;
            }
      }
  cout << "There are " << tot << " ships.";
  return 0;
}
