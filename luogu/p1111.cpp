#include <algorithm>
#include <cstdio>
#include <iostream>

using namespace std;

int n, m;
int f[10001]; // father
bool bf;
struct city {
  int a, b, t;
} cs[100001];

inline int gf(int k) // getfather
{
  if (f[k] == k) return k;
  f[k] = gf(f[k]);
  return f[k];
}
inline void un(int x, int y) // union
{
  int fx = gf(x);
  int fy = gf(y);
  f[fx] = fy;
}

inline bool comp(city a1, city a2) { return a1.t < a2.t; }

int main() {
  ios::sync_with_stdio(false);
  freopen("p1111.in", "r", stdin);

  cin >> n >> m;
  for (int i = 1; i <= m; i++) cin >> cs[i].a >> cs[i].b >> cs[i].t;
  sort(cs + 1, cs + m + 1, comp);
  for (int i = 1; i <= n; i++) f[i] = i;

  for (int i = 1; i <= m; i++) {
    bf = 1;
    if (gf(cs[i].a) != gf(cs[i].b)) { un(cs[i].a, cs[i].b); }
    int ggf = gf(f[1]);
    for (int j = 1; j <= n; j++) {
      if (gf(j) != ggf) {
        bf = 0;
        break;
      }
    }
    if (bf == 1) {
      cout << cs[i].t;
      return 0;
    }
  }
  cout << -1;

  return 0;
}
