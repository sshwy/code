// by Sshwy
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

int t;

void go() {
  long long x;
  scanf("%lld", &x);
  if (x == 1) {
    puts("0");
    return;
  }
  vector<int> v;
  while (x > 2) {
    v.pb(x & 1);
    x = (x + 1) / 2;
  }
  reverse(v.begin(), v.end());
  printf("%d\n", v.size() + 2);
  puts("1 1");
  int len = 2;
  for (auto x : v) {
    if (x == 0)
      printf("1 %d\n", len);
    else
      printf("2 %d\n", len);
    ++len;
  }
  printf("1 %d\n", len);
}
int main() {
  scanf("%d", &t);
  FOR(i, 1, t) go();
  return 0;
}
