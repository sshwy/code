#include <bits/stdc++.h>
#define INF 1000
using namespace std;
int T, n, ans;
int a[100], p[100];
int upp[100], dow[100];

int update_up(int p, int v) {
  for (int i = p; i <= n; i += i & (-i)) upp[i] += v;
}
int update_down(int p, int v) {
  for (int i = p; i <= n; i += i & (-i)) dow[i] += v;
}
int sum_up(int p) {
  int s = 0;
  for (int i = p; i > 0; i -= i & (-i)) s += upp[i];
  return s;
}
int sum_down(int p) {
  int s = 0;
  for (int i = p; i > 0; i -= i & (-i)) s += dow[i];
  return s;
}

int search(int k, int s) {
  if (k > n) return ans = min(ans, s), 0;
  if (s >= ans) return 0;
  int h = p[a[k]], x;
  if (h == -1 || h == k) return search(k + 1, s), 0;
  x = min(sum_up(h) - sum_up(k), sum_up(n) - sum_up(h) + sum_down(n) - sum_down(k));
  update_up(h, 1);
  search(k + 1, s + x);
  update_up(h, -1);
  x = min(
      sum_down(h) - sum_down(k), sum_down(n) - sum_down(h) + sum_up(n) - sum_up(k));
  update_down(h, 1);
  search(k + 1, s + x);
  update_down(h, -1);
}
int main() {
  scanf("%d", &T);
  while (T--) {
    scanf("%d", &n);
    ans = INF;
    for (int i = 0; i <= n; i++) upp[i] = dow[i] = a[i] = p[i] = 0;
    for (int i = 1; i <= n; i++) {
      scanf("%d", &a[i]);
      if (p[a[i]] == -1)
        p[a[i]] = i;
      else
        p[a[i]] = -1;
    }
    search(1, 0);
    printf("%d\n", ans);
  }
  return 0;
}
