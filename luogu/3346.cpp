#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
namespace IO {
  char nc() {
    static char bf[100000], *p1 = bf, *p2 = bf;
    return p1 == p2 && (p2 = (p1 = bf) + fread(bf, 1, 100000, stdin), p1 == p2)
               ? EOF
               : *p1++;
  }
  int rd() {
    int res = 0;
    char c = getchar();
    while (!isdigit(c)) c = getchar();
    while (isdigit(c)) res = res * 10 + c - '0', c = getchar();
    return res;
  }
} // namespace IO
/******************heading******************/
const int N = 2e6 + 50, C = 12;

struct qxx {
  int nex, t;
};
qxx e[N * 2];
int h[N], le = 1;
void add_path(int f, int t) { e[++le] = (qxx){h[f], t}, h[f] = le; }

int n, c;
int col[N], dg[N];

namespace T {
  int tr[N][C], tr_tot = 1;
  void dfs_add_trie(int u, int p, int tu) {
    int cu = col[u];
    if (!tr[tu][cu]) tr[tu][cu] = ++tr_tot;
    tu = tr[tu][cu];
    for (int i = h[u]; i; i = e[i].nex) {
      const int v = e[i].t;
      if (v == p) continue;
      dfs_add_trie(v, u, tu);
    }
  }
} // namespace T
const int SZ = 2e6 + 5, ALP = 11;
int last[SZ];
struct SAM {
  int tot;
  int len[SZ], tr[SZ][ALP], fail[SZ];
  int tnode[SZ];
  // tnode表示trie上状态的左后一个字符的某一个结点。
  SAM() { tot = 1, len[1] = 0, fail[1] = 0; }
  void insert(int tu, int x) {
    // printf("insert(u=%2d,x=%2d,v=%2d)\n",tu,x,T::tr[tu][x]);
    int v = T::tr[tu][x], p = last[tu], u = 0, q = 0, cq = 0;
    if (!tr[p][x]) {
      u = ++tot;
      tnode[u] = v;
      len[u] = len[p] + 1;
      while (p && !tr[p][x]) tr[p][x] = u, p = fail[p];
    }
    if (!p)
      fail[u] = 1;
    else {
      q = tr[p][x];
      // printf("u=%d,q=%d\n",u,q);
      if (len[q] == len[p] + 1)
        fail[u] = q;
      else {
        cq = ++tot;
        len[cq] = len[p] + 1;
        fail[cq] = fail[q];
        tnode[cq] = tnode[q];
        memcpy(tr[cq], tr[q], sizeof(tr[q]));
        fail[q] = fail[u] = cq;
        // printf("q=%d,cq=%d\n",q,cq);
        while (p && tr[p][x] == q) tr[p][x] = cq, p = fail[p];
      }
    }
    last[v] = u ? u : cq ? cq : q;
  }
  void print_node(int u) {
    printf("u=%2d,len=%2d,fail=%2d,tnode=%2d\n", u, len[u], fail[u], tnode[u]);
  }
  void count() {
    lld ans = 0;
    FOR(i, 1, tot) ans += len[i] - len[fail[i]];
    // FOR(i,1,tot)print_node(i);
    printf("%lld", ans);
  }
} sam;
queue<int> q;
void go() {
  last[1] = 1;
  q.push(1);
  while (!q.empty()) {
    int u = q.front();
    q.pop();
    FOR(i, 0, c - 1) {
      if (!T::tr[u][i]) continue;
      sam.insert(u, i);
      q.push(T::tr[u][i]);
    }
  }
  sam.count();
}
int main() {
  scanf("%d%d", &n, &c);
  FOR(i, 1, n) scanf("%d", &col[i]);
  FOR(i, 1, n - 1) {
    int u, v;
    scanf("%d%d", &u, &v);
    add_path(u, v), add_path(v, u);
    dg[u]++, dg[v]++;
  }
  FOR(i, 1, n) if (dg[i] == 1) T::dfs_add_trie(i, 0, 1);
  go();
  return 0;
}
