#include <iostream>

using namespace std;

int n, tot;
char qp[110][220];

void search(int x, int y) {
  if (x < 1 || x > n || y < 1 || y > 200 || !qp[x][y]) return;
  if (qp[x][y] == ' ' || qp[x][y] == '*') return;
  qp[x][y] = '*';
  search(x + 1, y);
  search(x - 1, y);
  search(x, y - 1);
  search(x, y + 1);
}

int main() {
  cin >> n;
  cin.get();
  for (int i = 1; i <= n; i++) cin.getline(qp[i] + 1, 210, '\n');
  for (int i = 1; i <= n; i++) {
    for (int j = 1; qp[i][j]; j++) {
      if (qp[i][j] != ' ' && qp[i][j] != '*') {
        tot++;
        search(i, j);
      }
    }
  }
  cout << tot;
  for (int i = 1; i <= n; i++) cout << qp[i] + 1 << endl;
  return 0;
}
