#include <iostream>

using namespace std;

int n;
int pum[1011], cnt;
bool bpm[1011];

unsigned long long f[1011];

void filter(int k) {
  bpm[0] = bpm[1] = true;
  for (int p = 2; p <= k; p++) {
    if (bpm[p] == false) pum[++cnt] = p;
    for (int i = 1; i <= cnt && pum[i] * p <= k; i++) {
      bpm[pum[i] * p] = true;
      if (p % pum[i] == 0) break;
    }
  }
}

int main() {
  cin >> n;
  filter(n);
  f[0] = 1;
  for (int i = 1; pum[i] <= n && i <= cnt; i++) {
    for (int j = 1; j <= n; j++) {
      if (j - pum[i] >= 0) { f[j] += f[j - pum[i]]; }
    }
  }
  cout << f[n];
  return 0;
}
