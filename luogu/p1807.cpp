#include <iostream>
#include <vector>

using namespace std;

int n, m;
int d[1501];
int head = 0, tail = 0;
int q[2000];

struct edge {
  int f, t, v;
} e1;

vector<edge> ved;
vector<int> nex[1501];

inline void qpush(int k) {
  q[tail++] = k;
  if (tail == 2000) tail -= 2000;
}
inline int qfront() { return q[head]; }

inline void qpop() {
  head++;
  if (head == 2000) head -= 2000;
}

inline bool qempty() { return head == tail; }

int main() {
  cin >> n >> m;
  for (int i = 0; i < m; i++) {
    cin >> e1.f >> e1.t >> e1.v;
    ved.push_back(e1);
    nex[e1.f].push_back(i);
  }
  qpush(1);
  d[n] = -1;
  while (!qempty()) {
    int now = qfront();
    qpop();
    for (int i = 0; i < nex[now].size(); i++) {
      edge nexedg = ved[nex[now][i]];
      if (d[nexedg.t] < d[now] + nexedg.v) { d[nexedg.t] = d[now] + nexedg.v; }
      qpush(nexedg.t);
    }
  }
  cout << d[n];
  return 0;
}
