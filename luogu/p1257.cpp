#include <cmath>
#include <cstdio>
#include <iostream>

using namespace std;

int n, t, mn = 99999999;
struct point {
  int x, y;
} p[10001];

int dis(point p1, point p2) {
  return (p1.x - p2.x) * (p1.x - p2.x) + (p1.y - p2.y) * (p1.y - p2.y);
}

int main() {
  cin >> n;
  for (int i = 1; i <= n; i++) {
    cin >> p[i].x >> p[i].y;
    for (int j = 1; j < i; j++) {
      if ((t = dis(p[i], p[j])) < mn) mn = t;
    }
  }
  printf("%.4Lf", sqrt(mn));
  return 0;
}
