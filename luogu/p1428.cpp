#include <iostream>
using namespace std;
int n, k, m, num[10001], sum;
int main() {
  cin >> n >> k;
  for (int i = 0; i < n; i++) {
    cin >> m;
    if (num[m] == 0) sum++;
    num[m] = 1;
  }
  if (k > sum) k = sum;
  int mi = 0;
  while (k != 0) {
    mi++;
    if (num[mi] == 1) k--;
  }
  cout << mi;
  return 0;
}
