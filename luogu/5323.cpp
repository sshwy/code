// by Sshwy
#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int P = 1e9 + 7, N = 5e5 + 5;
int pw(int a, int m, int p = P) {
  int res = 1;
  while (m) m & 1 ? res = 1ll * res * a % p : 0, a = 1ll * a * a % p, m >>= 1;
  return res;
}

int n;
int a[N], b[N];
int f[N], g[N];

int main() {
  scanf("%d", &n);
  int i100 = pw(100, P - 2);
  FOR(i, 1, n) {
    scanf("%d%d", &a[i], &b[i]);
    a[i] = a[i] * 1ll * i100 % P;
    b[i] = b[i] * 1ll * i100 % P;
  }
  f[0] = 1, g[0] = 0;
  FOR(i, 1, n)
  g[i] = (b[i] + a[i] * 1ll * a[i] % P * g[i - 1] % P *
                     pw(P + 1 - b[i] * 1ll * g[i - 1] % P, P - 2) % P) %
         P;
  FOR(i, 1, n)
  f[i] = a[i] * 1ll * f[i - 1] % P * pw(P + 1 - b[i] * 1ll * g[i - 1] % P, P - 2) % P;
  printf("%d\n", f[n]);
  return 0;
}
