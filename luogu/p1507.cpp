#include <iostream>
#define max(a, b) (a > b ? a : b)

using namespace std;

int mxv, mxm, n;
int v[51], m[51], c[51];
int f[401][401];

int main() {
  cin >> mxv >> mxm >> n;
  for (int i = 1; i <= n; i++) { cin >> v[i] >> m[i] >> c[i]; }
  for (int i = 1; i <= n; i++) {
    for (int j = mxv; j >= v[i]; j--) {
      for (int k = mxm; k >= m[i]; k--) {
        f[j][k] = max(f[j][k], f[j - v[i]][k - m[i]] + c[i]);
      }
    }
  }
  cout << f[mxv][mxm];
  return 0;
}
