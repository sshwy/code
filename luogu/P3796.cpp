#include <bits/stdc++.h>
using namespace std;

int n;
char str[161][75];
char tt[1000006];

const int N = 15000;
struct AC_automation {
  int tr[N][26], cnt;
  int e[N], fail[N];
  int g[N], tot[N]; //对于结点i跳fail指针的下一个对答案有贡献的结点
  int ans[N];

  void clear() { //初始化
    memset(tr, 0, sizeof(tr));
    memset(e, 0, sizeof(e));
    memset(fail, 0, sizeof(fail));
    memset(g, 0, sizeof(g));
    memset(tot, 0, sizeof(tot));
    cnt = 0;
  }
  void insert(char *s, int idx) { //模式串的编号
    int p = 0;
    for (int i = 0; s[i]; i++) {
      int c = s[i] - 'a';
      if (!tr[p][c]) tr[p][c] = ++cnt;
      p = tr[p][c];
    }
    e[p] = idx;
  }
  void build() {
    queue<int> q;
    for (int i = 0; i < 26; i++)
      if (tr[0][i]) q.push(tr[0][i]);
    while (!q.empty()) {
      int k = q.front();
      q.pop();
      for (int i = 0, v; i < 26; i++) {
        if ((v = tr[k][i])) {
          fail[v] = tr[fail[k]][i];
          g[v] = e[fail[v]] ? fail[v] : g[fail[v]]; //递推求g[]
          q.push(v);
        } else
          tr[k][i] = tr[fail[k]][i];
      }
    }
  }
  void query(char *t) {
    int p = 0;
    for (int i = 0; t[i]; i++) {
      p = tr[p][t[i] - 'a'];
      for (int v = p; v; v = g[v]) { //使用g[]代替fail[]加速匹配
        ++tot[e[v]];                 //出现次数统计
      }
    }
    int mx = 0, ant = 0;
    for (int i = 1; i <= n; i++) {
      if (tot[i] > mx)
        mx = tot[ans[ant = 1] = i];
      else if (tot[i] == mx)
        ans[++ant] = i;
    }
    printf("%d\n", mx);
    for (int i = 1; i <= ant; i++) printf("%s\n", str[ans[i]]);
  }
};
AC_automation ac;

int main() {
  while (~scanf("%d", &n)) {
    if (n == 0) return 0;
    ac.clear();
    for (int i = 1; i <= n; i++) {
      scanf("%s", str[i]);
      ac.insert(str[i], i);
    }
    ac.build();
    scanf("%s", tt);
    ac.query(tt);
  }
  return 0;
}