#include <iostream>
#include <string>

using namespace std;

int hs, n;
string heap[100001];

void heap_fix_up() {
  int now = hs;
  while (heap[now >> 1] > heap[now]) {
    string tmp = heap[now];
    heap[now] = heap[now >> 1];
    heap[now >> 1] = tmp;
    now >>= 1;
  }
}
void heap_fix_down() {
  heap[1] = heap[hs];
  heap[hs] = "zzzzzzzzz";
  hs--;
  int now = 1;
  while (heap[now] < heap[now << 1] && heap[now] < heap[now << 1 | 1]) {
    if (heap[now << 1] < heap[now << 1 | 1]) {
      string tmp = heap[now];
      heap[now] = heap[now << 1];
      heap[now << 1] = tmp;
      now = now << 1;
    } else {
      string tmp = heap[now];
      heap[now] = heap[now << 1 | 1];
      heap[now << 1 | 1] = tmp;
      now = now << 1 | 1;
    }
  }
}
void push(string s) {
  heap[++hs] = s;
  heap_fix_up();
}
string top() { return heap[1]; }
void pop() {
  heap[1] = 'z';
  heap_fix_down();
}

int main() {
  cin >> n;
  for (int i = 0; i <= n) return 0;
}
