// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

const int N = 1005, M = 2e5 + 5;

int n, m;
int w[N][N];
int ans[M];

int main() {
  scanf("%d%d", &n, &m);
  FOR(i, 1, n) w[i][i] = m + 1;
  FOR(i, 1, m) {
    int x, y;
    scanf("%d%d", &x, &y);
    w[x][y] = i;
  }

  ROF(k, n, 1) {
    FOR(i, 1, n) {
      if (!w[i][k]) continue;
      int wik = w[i][k];
      if (i > k) {
        FOR(j, 1, k - 1) {
          if (w[i][j] < min(wik, w[k][j])) w[i][j] = min(wik, w[k][j]);
        }
      } else {
        FOR(j, 1, n) {
          if (w[i][j] < min(wik, w[k][j])) w[i][j] = min(wik, w[k][j]);
        }
      }
    }
    FOR(i, k, n) {
      int x = min(w[i][k], w[k][i]);
      x = min(m + 1, x);
      if (x > 0) ans[x - 1]++;
    }
  }
  ROF(i, m - 1, 0) ans[i] += ans[i + 1];
  FOR(i, 0, m) printf("%d%c", ans[i], " \n"[i == m]);
  return 0;
}
