#include <iostream>

using namespace std;

int n, m, tot;
char qp[101][101];
int vis[101][101];

void search(int x, int y) {
  if (x < 1 || y < 1 || x > n || y > m) return;
  if (vis[x][y]) return;
  if (qp[x][y] == '-') return;
  vis[x][y] = tot;

  search(x - 1, y);
  search(x + 1, y);
  search(x, y - 1);
  search(x, y + 1);

  search(x - 2, y);
  search(x + 2, y);
  search(x, y - 2);
  search(x, y + 2);

  search(x - 1, y + 1);
  search(x + 1, y - 1);
  search(x - 1, y - 1);
  search(x + 1, y + 1);
}

int main() {
  cin >> n >> m;
  for (int i = 1; i <= n; i++) {
    for (int j = 1; j <= m; j++) { cin >> qp[i][j]; }
  }
  for (int i = 1; i <= n; i++) {
    for (int j = 1; j <= m; j++) {
      if (qp[i][j] == '#' && !vis[i][j]) {
        tot++;
        search(i, j);
      }
    }
  }
  cout << tot;
  return 0;
}
