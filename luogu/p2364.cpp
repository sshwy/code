#include <cstdio>
#include <ctime>
#include <iostream>

using namespace std;

char a[101], b[101], c[101];
int f[101][101][101], pre[101][101][101];

void lcs(char *p1, char *p2, char *p3) {
  int l1 = 0, l2 = 0, l3 = 0, ls = -1;
  char ans[101];

  while (p1[l1]) l1++;
  while (p2[l2]) l2++;
  while (p3[l3]) l3++;

  for (int i = 1; i <= l1; i++) {
    for (int j = 1; j <= l2; j++) {
      for (int k = 1; k <= l3; k++) {
        if (p1[i - 1] == p2[j - 1] && p2[j - 1] == p3[k - 1])
          f[i][j][k] = f[i - 1][j - 1][k - 1] + 1, pre[i][j][k] = 1;
        else if (f[i - 1][j][k] >= f[i][j - 1][k] && f[i - 1][j][k] >= f[i][j][k - 1])
          f[i][j][k] = f[i - 1][j][k], pre[i][j][k] = 2;
        else if (f[i][j - 1][k] >= f[i - 1][j][k] && f[i][j - 1][k] >= f[i][j][k - 1])
          f[i][j][k] = f[i][j - 1][k], pre[i][j][k] = 3;
        else
          f[i][j][k] = f[i][j][k - 1], pre[i][j][k] = 4;
      }
    }
  }
  for (int i = l1, j = l2, k = l3; i > 0 && j > 0 && k > 0;) {
    if (pre[i][j][k] == 1) {
      ans[++ls] = p1[i - 1];
      i--, j--, k--;
    } else if (pre[i][j][k] == 2)
      i--;
    else if (pre[i][j][k] == 3)
      j--;
    else
      k--;
  }
  for (int i = ls; i >= 0; i--) cout << ans[i];
}

int main() {
  cin >> a >> b >> c;
  lcs(a, b, c);
  //	printf("%.2lf",(double)clock()/CLOCKS_PER_SEC);
  return 0;
}
