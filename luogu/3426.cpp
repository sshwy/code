// by Yao
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA

const int N = 5e5 + 5;
int n, nex[N], f[N], b[N];
char s[N];

void getNext(char *s, int ls) {
  nex[1] = 0;
  int j = 0;
  FOR(i, 2, ls) {
    while (j && s[i] != s[j + 1]) j = nex[j];
    if (s[i] == s[j + 1])
      nex[i] = ++j;
    else
      nex[i] = 0;
  }
}
int main() {
  scanf("%s", s + 1);
  n = strlen(s + 1);

  getNext(s, n);

  f[1] = 1;
  b[f[1]] = 1;
  FOR(i, 2, n) {
    if (nex[i] == 0)
      f[i] = i;
    else {
      if (b[f[nex[i]]] >= i - nex[i])
        f[i] = f[nex[i]];
      else
        f[i] = i;
    }
    b[f[i]] = i;
  }
  printf("%d\n", f[n]);
  return 0;
}
