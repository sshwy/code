#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
const int LS = 5e5 + 5, N = 1.1e6 + 5, SZ = 4e6 * 30;

char s[LS];
int ls;

namespace seg {
  int lc[SZ], rc[SZ], tot;
  void insert(int &u, int val, int l = 1, int r = ls) {
    if (!u) u = ++tot;
    if (l == r) return;
    int mid = (l + r) >> 1;
    if (val <= mid)
      insert(lc[u], val, l, mid);
    else
      insert(rc[u], val, mid + 1, r);
  }
  int find(int u, int L, int R, int l = 1, int r = ls) {
    if (!u || r < L || R < l) return 0;
    if (L <= l && r <= R) return 1;
    int mid = (l + r) >> 1;
    return (lc[u] && L <= mid && find(lc[u], L, R, l, mid)) ||
           (rc[u] && mid < R && find(rc[u], L, R, mid + 1, r));
  }
  int merge(int x, int y, int l = 1, int r = ls) {
    if (!x || !y) return x + y;
    int mid = (l + r) >> 1, u = ++tot;
    lc[u] = merge(lc[x], lc[y], l, mid);
    rc[u] = merge(rc[x], rc[y], mid + 1, r);
    return u;
  }
} // namespace seg
int rt[N];

struct Sam {
  int tot, last, ln;
  int len[N], fail[N], tr[N][26], ed[N];
  Sam() { tot = last = 1, ln = 0; }
  void clear() {
    memset(len, 0, sizeof(int) * (tot + 1));
    memset(fail, 0, sizeof(int) * (tot + 1));
    memset(tr, 0, sizeof(tr[0]) * (tot + 1));
    tot = last = 1, ln = 0;
  }
  int *operator[](int u) { return tr[u]; }
  void insert(char c) {
    c -= 'a';
    int u = ++tot, p = last;
    len[u] = len[last] + 1;
    ed[u] = ++ln;
    while (p && tr[p][c] == 0) tr[p][c] = u, p = fail[p];
    if (!p)
      fail[u] = 1;
    else {
      int q = tr[p][c];
      if (len[q] == len[p] + 1)
        fail[u] = q;
      else {
        int cq = ++tot;
        len[cq] = len[p] + 1, fail[cq] = fail[q], fail[q] = fail[u] = cq;
        ed[cq] = ed[q];
        memcpy(tr[cq], tr[q], sizeof(tr[q]));
        while (p && tr[p][c] == q) tr[p][c] = cq, p = fail[p];
      }
    }
    last = u;
  }
  int bin[N], b[N];
  void qsort() {
    FOR(i, 1, tot) bin[len[i]]++;
    FOR(i, 1, tot) bin[i] += bin[i - 1];
    ROF(i, tot, 1) b[bin[len[i]]--] = i;
  }
} S, T;

char t[LS];
int lim[LS];

void go() {
  T.clear();
  int l, r;
  scanf("%s%d%d", t + 1, &l, &r);
  int lt = strlen(t + 1);
  FOR(i, 1, lt) { T.insert(t[i]); }
  int u = 1, cur = 0;
  FOR(i, 1, lt) {
    int c = t[i] - 'a';
    while (cur && (S[u][c] == 0 || seg::find(rt[S[u][c]], l + cur, r) == 0)) {
      --cur;
      if (cur <= S.len[S.fail[u]]) u = S.fail[u];
      // if(!u)break;
    }
    if (S[u][c] && seg::find(rt[S[u][c]], l + cur, r)) {
      u = S[u][c], ++cur;
    } else {
      assert(u == 1);
      cur = 0;
    }
    lim[i] = cur;
  }
  long long ans = 0;
  FOR(i, 1, T.tot)
  ans += max(T.len[i] - max(lim[T.ed[i]], T.len[T.fail[i]]), 0);
  printf("%lld\n", ans);
}
int main() {
  scanf("%s", s + 1);
  ls = strlen(s + 1);
  FOR(i, 1, ls) {
    S.insert(s[i]);
    seg::insert(rt[S.last], i);
  }
  S.qsort();
  ROF(i, S.tot, 2) {
    int u = S.b[i];
    rt[S.fail[u]] = seg::merge(rt[S.fail[u]], rt[u]);
  }
  int q;
  scanf("%d", &q);
  FOR(i, 1, q) go();
  return 0;
}
