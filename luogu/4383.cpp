// by Sshwy
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)

namespace RA {
  int rnd(int p) { return 1ll * rand() * rand() % p; }
  int rnd(int L, int R) { return rnd(R - L + 1) + L; }
} // namespace RA
const int N = 3e5 + 5;
int n, k;

struct qxx {
  int nex, t, v;
} e[N * 2];
int h[N], le = 1;
void add_path(int f, int t, int v) { e[++le] = {h[f], t, v}, h[f] = le; }

typedef pair<long long, int> atom;
atom f[N][3]; // cost, number of path
atom operator+(atom x, atom y) { return {x.first + y.first, x.second + y.second}; }
atom operator-(atom x, atom y) { return {x.first - y.first, x.second - y.second}; }
void dfs(int u, int p, const long long c) {
  atom a1, a2;
  int t1 = -1, t2 = -1;
  for (int i = h[u]; i; i = e[i].nex) {
    int v = e[i].t, w = e[i].v;
    if (v != p) {
      dfs(v, u, c);
      f[u][0] = f[u][0] + max(f[v][0], f[v][2]);
      atom t = f[v][1] - max(f[v][0], f[v][2]);
      t.first += w;
      if (t1 == -1 || t > a1)
        a2 = a1, t2 = t1, a1 = t, t1 = 1;
      else if (t2 == -1 || t > a2)
        a2 = t, t2 = 1;
    }
  }
  f[u][1] = f[u][0];
  if (t1 != -1) f[u][1] = max(f[u][1], f[u][0] + a1);
  if (t2 != -1) {
    atom t = f[u][0] + a1 + a2;
    t.second++;
    t.first += c;
    f[u][2] = max(f[u][2], t);
  }
  atom t = f[u][1];
  t.second++;
  t.first += c;
  f[u][2] = max(f[u][2], t);
}
atom calc(long long c) {
  // printf("calc %lld\n",c);
  FOR(i, 1, n) FOR(j, 0, 2) f[i][j] = {0, 0};
  dfs(1, 0, c);
  FOR(i, 1, n) {
    FOR(j, 0, 2) {
      // printf("f[%d,%d]=(%lld,%d)\n",i,j,f[i][j].first,f[i][j].second);
    }
  }
  return max(f[1][0], f[1][2]);
}
int main() {
  scanf("%d%d", &n, &k);
  FOR(i, 1, n - 1) {
    int u, v, w;
    scanf("%d%d%d", &u, &v, &w);
    add_path(u, v, w);
    add_path(v, u, w);
  }
  long long l = -1e9, r = 1e9;
  while (l < r) {
    long long mid = (l + r) >> 1;
    if (calc(mid).second >= k + 1)
      r = mid;
    else
      l = mid + 1;
  }
  long long ans = calc(l).first - (k + 1) * l;
  printf("%lld\n", ans);
  return 0;
}
