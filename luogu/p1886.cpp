#include <cstdio>
#include <cstring>
#include <iostream>

#define max(x1, x2) (x1 > x2 ? x1 : x2)
#define min(x1, x2) (x1 < x2 ? x1 : x2)

using namespace std;

int n, k, a[1000010];
int mx[4000010], mn[4000010];

int fast_read() {
  int num = 0;
  bool isN = false;
  char c;
  while (c = getchar(), c == ' ' || c == '\n' || c == '\t' || c == '\r')
    ;
  if (c == '-')
    isN = true;
  else if ('0' <= c && c <= '9')
    num += c - 48;
  else
    return 0 - 999999999;
  while (c = getchar(), '0' <= c && c <= '9') num = num * 10 + c - 48;
  return (isN ? 0 - num : num);
}

void pushup(int root) {
  mx[root] = max(mx[root << 1], mx[root << 1 | 1]);
  mn[root] = min(mn[root << 1], mn[root << 1 | 1]);
}
void tree_build(int l, int r, int root) {
  if (l == r) {
    mx[root] = mn[root] = a[l];
    return;
  }
  int mid = (l + r) >> 1;
  tree_build(l, mid, root << 1);
  tree_build(mid + 1, r, root << 1 | 1);
  pushup(root);
}
int maxnum(int L, int R, int l, int r, int root) {
  if (L <= l && r <= R) return mx[root];
  int mid = (l + r) >> 1, s = 0 - 999999999, t;
  if (!(mid < L || R < l)) t = maxnum(L, R, l, mid, root << 1), s = max(s, t);
  if (!(r < L || R < mid + 1))
    t = maxnum(L, R, mid + 1, r, root << 1 | 1), s = max(s, t);
  return s;
}
int minnum(int L, int R, int l, int r, int root) {
  if (L <= l && r <= R) return mn[root];
  int mid = (l + r) >> 1, s = 999999999, t;
  if (!(mid < L || R < l)) t = minnum(L, R, l, mid, root << 1), s = min(s, t);
  if (!(r < L || R < mid + 1))
    t = minnum(L, R, mid + 1, r, root << 1 | 1), s = min(s, t);
  return s;
}
int main() {
  n = fast_read();
  k = fast_read();
  //	scanf("%d%d",&n,&k);
  for (int i = 1; i <= n; i++) a[i] = fast_read(); // scanf("%d",&a[i]);
  tree_build(1, n, 1);
  for (int i = 1; i + k - 1 <= n; i++) printf("%d ", minnum(i, i + k - 1, 1, n, 1));
  cout << endl;
  for (int i = 1; i + k - 1 <= n; i++) printf("%d ", maxnum(i, i + k - 1, 1, n, 1));
  return 0;
}
