#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
/******************heading******************/
#define int lld
const int N = 5e4 + 5, M = 2e5 + 5;

struct qxx {
  int nex, t, v;
};
qxx e[M];
int h[N], le;
void add_path(int f, int t, int v) { e[++le] = (qxx){h[f], t, v}, h[f] = le; }
#define FORew(i, u, v, w) \
  for (int i = h[u], v, w; v = e[i].t, w = e[i].v, i; i = e[i].nex)

int n, m;

int b[70], d[N], line;
bool insert(int x) {
  ROF(i, 63, 0) {
    if (x >> i & 1) {
      if (b[i])
        x ^= b[i];
      else
        return b[i] = x, 1;
    }
  }
  return 0;
}

bool vis[N];
void dfs(int u, int s = 0) {
  vis[u] = 1, d[u] = s;
  if (u == n) line = s;
  FORew(i, u, v, w) {
    if (vis[v])
      insert(s ^ w ^ d[v]); // BUG#1:没有把环之外的部分xor掉
    else
      dfs(v, s ^ w);
  }
}

signed main() {
  scanf("%lld%lld", &n, &m);
  FOR(i, 1, m) {
    int x, y, z;
    scanf("%lld%lld%lld", &x, &y, &z);
    add_path(x, y, z), add_path(y, x, z);
  }
  dfs(1);
  ROF(i, 63, 0) {
    if ((line ^ b[i]) > line) line ^= b[i];
  }
  printf("%lld\n", line);
  return 0;
}
