#include <algorithm>
#include <bitset>
#include <cassert>
#include <cctype>
#include <climits>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <functional>
#include <iostream>
#include <map>
#include <numeric>
#include <queue>
#include <set>
#include <stack>
#include <string>
#include <vector>
using namespace std;
#define rep(i, a, b) for (register ll i = (a); i <= (b); ++i)
#define per(i, a, b) for (register ll i = (a); i >= (b); --i)
#define loop(it, v) for (auto it = v.begin(); it != v.end(); it++)
#define cont(i, x) for (int i = head[x]; i; i = edge[i].nex)
#define clr(a) memset(a, 0, sizeof(a))
#define ass(a, cnt) memset(a, cnt, sizeof(a))
#define cop(a, b) memcpy(a, b, sizeof(a))
#define lowbit(x) (x & -x)
#define all(x) x.begin(), x.end()
#define SC(t, x) static_cast<t>(x)
#define ub upper_bound
#define lb lower_bound
#define pqueue priority_queue
#define mp make_pair
#define pb push_back
#define pof pop_front
#define pob pop_back
#define fi first
#define se second
#define y1 y1_
#define Pi acos(-1.0)
#define iv inline void
#define enter cout << endl
#define siz(x) ((int)x.size())
#define file(x) freopen(x ".in", "r", stdin), freopen(x ".out", "w", stdout)
typedef double db;
typedef long long ll;
typedef unsigned long long ull;
typedef pair<int, int> pii;
typedef vector<int> vi;
typedef vector<pii> vii;
typedef queue<int> qi;
typedef queue<pii> qii;
typedef set<int> si;
typedef map<int, int> mii;
typedef map<string, int> msi;
const int maxn = 100 + 10;
const int inf = 0x3f3f3f3f;
const int iinf = 1 << 30;
const ll linf = 2e18;
const ll mod = 1e9 + 7;
const double eps = 1e-7;
template <class T> void read(T &a) {
  int f = 1;
  a = 0;
  char ch = getchar();
  while (!isdigit(ch)) {
    if (ch == '-') f = -1;
    ch = getchar();
  }
  while (isdigit(ch)) {
    a = (a << 3) + (a << 1) + ch - '0';
    ch = getchar();
  }
  a *= f;
}

int power(int a, int b, int p) {
  int ret = 1;
  for (; b; b >>= 1, (a *= a) %= p)
    if (b & 1) (ret *= a) %= p;
  return ret;
}

int n, m, p;

int sz[maxn], s[maxn];

int fac[maxn];

int ans;

int gcd(int a, int b) { return !b ? a : gcd(b, a % b); }

void dfs(int sum, int last, int tot) {
  // printf("%lld %lld\n", sum, tot);
  if (sum == n) {
    // printf("Wild : %lld\n", tot);
    // rep(i, 1, tot) printf("%lld%c", sz[i], " \n"[i == tot]);
    int w = 1, sum = 0;
    clr(s);
    rep(i, 1, tot) s[sz[i]]++, (w *= sz[i]) %= p;
    rep(i, 1, n)(w *= fac[s[i]]) %= p;
    rep(i, 1, tot) {
      sum += sz[i] / 2;
      rep(j, i + 1, tot) sum += gcd(sz[i], sz[j]);
    }
    // printf("Sum : %lld\n", sum);
    // printf("w : %lld\n", w);
    (ans += power(w, p - 2, p) * power(m, sum, p) % p) %= p;
    return;
  }
  rep(i, last, n - sum) {
    sz[tot + 1] = i;
    dfs(sum + i, i, tot + 1);
  }
  return;
}

signed main() {
  scanf("%d", &n);
  p = 997, m = 2;
  fac[0] = 1;
  rep(i, 1, n) fac[i] = fac[i - 1] * i % p;
  dfs(0, 1, 0);
  printf("%d\n", ans);
  return 0;
}
