#include <iostream>
#include <queue>
#include <vector>

using namespace std;

const int INF = 99999999;
int n, p, c, x, y, z, minn = INF;
int cow[501];
int sumd[801];
int d[801];
bool vising[801];

struct side {
  int f, t, v;
} asi;

vector<int> pa[801]; // point
vector<side> sp;     // side

void spfa(int start) {
  queue<int> q;
  for (int i = 1; i <= p; i++) {
    d[i] = INF;
    vising[i] = false;
  }

  d[start] = 0;
  vising[start] = true;
  q.push(start);

  while (!q.empty()) {
    int now = q.front(), npa, nto; // nowpath
    q.pop();
    vising[now] = false;
    for (int i = 0; i < pa[now].size(); i++) {
      npa = pa[now][i];
      nto = sp[npa].t != now ? sp[npa].t : sp[npa].f;
      if (d[nto] > d[now] + sp[npa].v) {
        d[nto] = d[now] + sp[npa].v;
        if (vising[nto] == false) {
          vising[nto] = true;
          q.push(nto);
        }
      }
    }
  }
  for (int i = 1; i <= p; i++) {
    //		cout<<d[i]<<' ';
    sumd[i] += d[i];
  }
  //	cout<<endl;
}
int main() {
  //	freopen("p1828in.txt","r",stdin);
  cin >> n >> p >> c;
  for (int i = 0; i < n; i++) cin >> cow[i];
  for (int i = 0; i < c; i++) {
    cin >> asi.f >> asi.t >> asi.v;
    sp.push_back(asi);
    pa[asi.f].push_back(i);
    pa[asi.t].push_back(i);
  }

  for (int i = 0; i < n; i++) { spfa(cow[i]); }
  for (int i = 1; i <= p; i++) {
    if (minn > sumd[i]) minn = sumd[i];
  }
  cout << minn;
  return 0;
}
