#include <algorithm>
#include <cstring>
#include <iostream>

using namespace std;

long long n, t, tot;
char str[10000];
long long mod = 99999991, base = 9973;
bool h[100000001];

long long hashe(char s[]) {
  long long ans = 0;
  for (long long i = 0; s[i] != '\0'; i++) { ans = (ans * base + s[i]) % mod; }
  return ans;
}

int main() {
  cin >> n;
  for (long long i = 1; i <= n; i++) {
    cin >> str;
    sort(str, str + strlen(str));
    t = hashe(str);
    if (h[t] == 0) tot++;
    h[t] = 1;
  }
  cout << tot;
}
