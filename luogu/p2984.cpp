#include <iostream>
#include <queue>
#include <vector>

using namespace std;

const int INF = 99999999;
int n, m, b, x, y;
int d[50001];
bool vising[50001];

struct path {
  int f, t, v;
} p1;

vector<int> next[50001];
vector<path> vp;
queue<int> q;

int main() {
  cin >> n >> m >> b;
  for (int i = 0; i < m; i++) {
    cin >> p1.f >> p1.t >> p1.v;
    vp.push_back(p1);
    next[p1.f].push_back(i);
    next[p1.t].push_back(i);
  }

  for (int i = 0; i <= n; i++) d[i] = INF;
  q.push(1);
  vising[1] = true;
  d[1] = 0;

  while (!q.empty()) {
    int now = q.front();
    q.pop();
    vising[now] = false;

    for (int i = 0; i < next[now].size(); i++) {
      int nextpath = next[now][i];
      int nextnode = vp[nextpath].t == now ? vp[nextpath].f : vp[nextpath].t;
      if (d[nextnode] > d[now] + vp[nextpath].v) {
        d[nextnode] = d[now] + vp[nextpath].v;
        if (vising[nextnode] == false) {
          vising[nextnode] = true;
          q.push(nextnode);
        }
      }
    }
  }

  for (int i = 1; i <= b; i++) {
    cin >> x >> y;
    cout << d[x] + d[y] << endl;
  }
  return 0;
}
