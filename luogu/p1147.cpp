#include <cmath>
#include <iostream>
using namespace std;

int m;

void f(int k) {
  if (m * 2 % k != 0)
    return;
  else {
    if (k % 2) {
      int t = m / k;
      cout << t - k / 2 << ' ' << t + k / 2 << endl;
    } else {
      int t = m * 2 / k;
      int d = k - 1;
      if ((t + d) % 2) return;
      cout << (t - d) / 2 << ' ' << (t + d) / 2 << endl;
    }
  }
}

int main() {
  cin >> m;
  for (int i = sqrt(m * 2) + 1; i > 1; i--) f(i);
  return 0;
}
