#include <cstdio>
#include <iostream>

using namespace std;

int n, s[40], r;
int fr[40][40];
struct node {
  int p, l, r;
  int s;
};
node tree[40];

int tree_build(int L, int R) { // return root
  if (fr[L][R])
    return fr[L][R];
  else if (L > R)
    return 1;
  else if (L == R) {
    tree[L].s = s[L];
    return fr[L][R] = L;
  }
  int mroot, ml, mr, mx = 0;
  for (int mid = L, l, r, t; mid <= R; mid++) {
    l = tree_build(L, mid - 1);
    r = tree_build(mid + 1, R);
    if ((t = tree[l].s * tree[r].s + s[mid]) > mx)
      mx = t, ml = l, mr = r, mroot = mid;
  }
  printf("tree_build(%d,%d)=%d,%d\n", L, R, mroot, mx);
  tree[mroot].s = mx;
  tree[mroot].l = ml;
  tree[mroot].r = mr;
  tree[ml].p = tree[mr].p = mroot;
  return fr[L][R] = mroot;
}

void prefix_print(int root) {
  if (!root) return;
  cout << root << ' ';
  prefix_print(tree[root].l);
  prefix_print(tree[root].r);
}

int main() {
  cin >> n;
  for (int i = 1; i <= n; i++) cin >> s[i];
  r = tree_build(1, n);
  cout << tree[r].s << endl;
  prefix_print(r);
  return 0;
}
