#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i) /*}}}*/
/******************heading******************/
const int N = 1e5 + 5, M = 1e5 + 5, SZ = 1 << 22;
int n, m;

int f[N];
void init(int k) { FOR(i, 0, k) f[i] = i; }
int get(int k) { return f[k] == k ? k : f[k] = get(f[k]); }
void un(int x, int y) { f[get(x)] = get(y); }
bool find(int x, int y) { return get(x) == get(y); }

int lc[SZ], rc[SZ], val[SZ], dep[SZ], tot;
int new_node(int v) {
  ++tot, val[tot] = v, lc[tot] = rc[tot] = 0, dep[tot] = 0;
  return tot;
}
int merge(int x, int y) {
  if (!x || !y) return x + y;
  if (val[x] > val[y]) swap(x, y);
  rc[x] = merge(rc[x], y);
  if (dep[lc[x]] < dep[rc[x]]) swap(lc[x], rc[x]);
  dep[x] = dep[rc[x]] + 1;
  return x;
}
int getmin(int x) { return val[x]; }
int pop(int x) {
  val[x] = -1;
  return merge(lc[x], rc[x]);
}

int rt[N];
int main() {
  scanf("%d%d", &n, &m);
  FOR(i, 1, n) {
    int x;
    scanf("%d", &x);
    rt[i] = new_node(x);
  }
  // printf("rt: ");FOR(i,1,n)printf("%d ",rt[i]); puts("");
  init(n);
  FOR(i, 1, m) {
    int op, x, y;
    scanf("%d%d", &op, &x);
    if (op == 1) {
      scanf("%d", &y);
      if (val[x] == -1 || val[y] == -1 || find(x, y)) continue;
      int u = merge(rt[get(x)], rt[get(y)]);
      un(x, y), rt[get(x)] = u;
    } else {
      if (val[x] == -1) {
        puts("-1");
        continue;
      }
      printf("%d\n", getmin(rt[get(x)]));
      rt[get(x)] = pop(rt[get(x)]);
    }
  }
  return 0;
}
