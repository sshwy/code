#include <algorithm>/*{{{*/
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
using namespace std;
typedef long long lld;
typedef long double lf;
typedef unsigned long long uld;
typedef pair<int, int> pii;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define FOR(i, a, b) for (int i = (a); i <= (b); ++i)
#define ROF(i, a, b) for (int i = (a); i >= (b); --i)
namespace RA {
  int r(int p) { return 1ll * rand() * rand() % p; }
  int r(int L, int R) { return r(R - L + 1) + L; }
} // namespace RA
namespace IO {
  char nc() {
    static char bf[100000], *p1 = bf, *p2 = bf;
    return p1 == p2 && (p2 = (p1 = bf) + fread(bf, 1, 100000, stdin), p1 == p2)
               ? EOF
               : *p1++;
  }
  int rd() {
    int res = 0;
    char c = getchar();
    while (!isdigit(c)) c = getchar();
    while (isdigit(c)) res = res * 10 + c - '0', c = getchar();
    return res;
  }
} // namespace IO
/******************heading******************/

int a[50], l;
lld f[50][200][200][2]; // i位，数字和为j，模mod为k，是否抵达上界的数字的个数。
lld m10[50];
lld DP(int mod) {
  memset(f, 0, sizeof f);
  f[0][0][0] = 1;
  FOR(i, 1, l) {
    FOR(j, 0, i * 9) {
      FOR(k, 0, mod - 1) {
        f[i][j][k] = 0;
        FOR(p, 0, min(9, j)) {
          f[i][j][k] += f[i - 1][j - p][(k - p * m10[i] % mod + mod) % mod];
        }
        if (f[i][j][k]) printf("f[%d,%d,%d]=%lld\n", i, j, k, f[i][j][k]);
      }
    }
  }
  return f[l][mod][0];
}
lld calc(lld x) {
  l = 0;
  lld x2 = x;
  while (x2) a[++l] = x2 % 10, x2 /= 10;
  reverse(a + 1, a + l + 1);
  lld tot = 0;
  FOR(i, 0, l * 9) { tot += DP(i); }
  return tot;
}
lld x, y;
int main() {
  m10[0] = 1;
  FOR(i, 1, 18) m10[i] = 18 * m10[i - 1];
  cin >> x >> y;
  cout << calc(y) - calc(x - 1);
  return 0;
}
