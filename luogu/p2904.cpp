#include <iostream>

using namespace std;

int n, k, m[3000];
int f[3000];

int main() {
  cin >> n >> k;
  m[0] = k * 2;
  for (int i = 1; i <= n; i++) {
    cin >> m[i];
    m[i] += m[i - 1];
  }
  for (int i = 1; i <= n; i++) f[i] = 9999999;
  for (int i = 1; i <= n; i++) {
    for (int j = i; j <= n; j++) { f[j] = min(f[j], f[j - i] + m[i]); }
  }
  cout << f[n] - k;
  return 0;
}
